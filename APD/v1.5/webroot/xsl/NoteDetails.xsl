<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:local="http://local.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt">

  <xsl:import href="msxsl/msjs-client-library-htc.js"/>
  <xsl:import href="msxsl/msxsl-function-library.xsl"/>

  <msxsl:script language="JScript" implements-prefix="local">
    <![CDATA[
      function AdjustCRUD( strNoteCRUD, strCurrentDate, strCreatedDate, strCurrentUserID, strCreatedUserID )
      {
          var varCurDate = new Date(strCurrentDate);
          var varCreatedDate = new Date(strCreatedDate);

          if ( ( strCreatedUserID != "" ) && ( ( strCurrentUserID != strCreatedUserID )
              || ( ( varCurDate.getTime() - varCreatedDate.getTime() ) > 1800000 ) ) )
              strNoteCRUD = strNoteCRUD.substr( 0, 2 ) + "_" + strNoteCRUD.substr( 3, 1 );

          return strNoteCRUD;
      }

      function GetDefaultContext( strContext )
      {
          return ( strContext == "" ) ? "clm" : strContext;
      }

     ]]>
  </msxsl:script>


  <!-- Permissions params - names must end in 'CRUD' -->
  <xsl:param name="NoteCRUD" select="Note"/>

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="RawContext"/>
  <xsl:param name="UserId"/>
  <xsl:param name="WindowID"/>

  <xsl:variable name="SupervisorFlag" select="session:XslGetSession('SupervisorFlag')"/>

  <!-- APDXSL:C:\websites\apd\v1.2\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspNoteGetDetailXML,NoteDetails.xsl, 147468, 0   -->

  <xsl:template match="/">

    <xsl:variable name="PertainsTo">
      <xsl:value-of select="/Root/Note/@ClaimAspectCode"/>
      <xsl:if test="/Root/Note/@ClaimAspectNumber != '0'"><xsl:value-of select="/Root/Note/@ClaimAspectNumber"/></xsl:if>
    </xsl:variable>

    <html>
      <head>

        <title>Note Details</title>
        <link rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>

        <style type="text/css">
            A {color:black; text-decoration:none;}
        </style>

        <xsl:variable name="CurrentUserID"><xsl:value-of select="$UserId"/></xsl:variable>
        <xsl:variable name="CreatedUserID"><xsl:value-of select="/Root/Note/@CreatedUserID"/></xsl:variable>
        <xsl:variable name="CurrentDate"><xsl:value-of select="js:formatSQLDateTime( string(/Root/Note/@CurrentDate) )"/></xsl:variable>
        <xsl:variable name="CreatedDate"><xsl:value-of select="js:formatSQLDateTime( string(/Root/Note/@CreatedDate) )"/></xsl:variable>
        <xsl:variable name="AdjustedCrud"><xsl:value-of select="local:AdjustCRUD( string($NoteCRUD), string($CurrentDate),string($CreatedDate), string($CurrentUserID), string($CreatedUserID) )"/></xsl:variable>
        <script language="JavaScript" src="js/apdcontrols.js"></script>
    		<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>
    		<script type="text/javascript">
          document.onhelp=function(){
		        RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,10);
		        event.returnValue=false;
		      };
     		</script>

        <script language="JavaScript" runat="Client">
          var strCurrentDateSQL = '<xsl:value-of select="/Root/Note/@CurrentDate"/>';
      			var strCurrentDate = extractSQLDateTime( strCurrentDateSQL );
      			var varCurrentDate = new Date( strCurrentDate );

            var strCreatedDateSQL = '<xsl:value-of select="/Root/Note/@CreatedDate"/>';
            var strCreatedDate    = extractSQLDateTime( strCreatedDateSQL );
            var varCreatedDate    = new Date( strCreatedDate );
            var strCrud           = '<xsl:value-of select="$AdjustedCrud"/>';
            var strPertainsTo     = '<xsl:value-of select="$PertainsTo"/>';
            var strOrigPertainsTo = '<xsl:value-of select="$PertainsTo"/>';
            var strContext = '<xsl:value-of select="local:GetDefaultContext(string($RawContext))"/>';
            var gsSupervisorFlag = '<xsl:value-of select="$SupervisorFlag"/>';
            var iNoteMaxLength = <xsl:value-of select="/Root/Metadata[@Entity='Note']/Column[@Name='Note']/@MaxLength"/>;
            var strDocumentID = '<xsl:value-of select="/Root/Note/@DocumentID"/>';
            var strClaimAspectID = '<xsl:value-of select="/Root/Note/@ClaimAspectID"/>';
            var gsChannelID = '<xsl:value-of select="/Root/Note/@ClaimAspectServiceChannelID"/>';
            var gsOldChannelID = gsChannelID;
            var strUserID = "<xsl:value-of select="string($UserId)"/>";
            var strSysLastUpdatedDate = '<xsl:value-of select="/Root/Note/@SysLastUpdatedDate"/>';
            var strNoteTypeID = '<xsl:value-of select="/Root/Note/@NoteTypeID"/>';
            var gsStatusID = '<xsl:value-of select="/Root/Note/@StatusID"/>';
            var strClaimMessagesSP = "uspClaimMessageGetDetailXML";
            var blnMessageChanging = false;
            var blnClaimInfoReady = false;
            var strLynxID = "";
            var strClaimAspectID = "";
            var strWindowID = "";

            //With the current cntrols, we cannot specify a single control to be updatable.
            //Therefore, all controls are updatable.
            //When the new controls are implemented, make only the Private Note checkbox updatable by the supervisor.
            <!-- var gsSupervisorFlag = '<xsl:value-of select="$SupervisorFlag"/>'; -->
            <![CDATA[
            
			//--------------------------------------//
			//-- 04Jan2013 -- TVD Please Wait Fix --//
			//--------------------------------------//
			function doAgain() {
				try {
					pageLoaded = document.getElementById('btnCancel');
					__pageInit();
        
        //--- 23Jul2014 - TVD - Make the textbox the focus 
        document.getElementById("txtNote").setFocus();

				} 
				catch(err) {
					pageLoading.style.display = 'block';
					document.getElementById('selNoteType').disabled=true;
					document.getElementById('chkPrivate').disabled=true;
					document.getElementById('selPertainsTo').disabled=true;
					document.getElementById('selChannel').disabled=true;
					document.getElementById('selTemplate').disabled=true;
					document.getElementById('txtNote').disabled=true;
					document.getElementById('btnCancel').disabled=true;
					document.getElementById('btnSave').disabled=true;

					window.setTimeout("doAgain()", 3000); 
				}
			}
			//--  04Jan2013 -- TVD END --//			
			
			//function initPage()
            function __pageInit()
            {
				//--------------------------------------//
				//-- 04Jan2013 -- TVD Please Wait Fix --//
				//--------------------------------------//
				var pageLoading = document.getElementById('pageLoading');
				pageLoading.style.display = 'none';
				document.getElementById('selNoteType').disabled=false;
				document.getElementById('chkPrivate').disabled=false;
				document.getElementById('selPertainsTo').disabled=false;
				document.getElementById('selChannel').disabled=false;
				document.getElementById('selTemplate').disabled=false;
				document.getElementById('txtNote').disabled=false;
				document.getElementById('btnCancel').disabled=false;
				document.getElementById('btnSave').disabled=false;
				//--  04Jan2013 -- TVD END --//

				if (strCrud.indexOf("U") == -1 || TimedOut() == true)
              {
                selNoteType.CCDisabled = selPertainsTo.CCDisabled = txtNote.CCDisabled =
                  chkPrivate.CCDisabled = btnSave.CCDisabled = selChannel.CCDisabled = 
                  selTemplate.CCDisabled = true;
              }

              if (strCrud.indexOf("R") == -1){ //no read permission
                doCancel();
              }

              if (gsSupervisorFlag == "1"){
                btnSave.CCDisabled = chkPrivate.CCDisabled = false;
              }

              refreshChannelList();

              try {
                //if (selNoteType.CCDisabled == false) selNoteType.setFocus();
              } catch (e) {} // no need to throw error
              
              strLynxID = dialogArguments.vNoteLynxID;
              strClaimAspectID = dialogArguments.vNoteClaimAspectID;
              
              if (strClaimAspectID !== null && strClaimAspectID != "")
                  selPertainsTo.value = strClaimAspectID;
              
              /*if (strDocumentID != "")
               selTemplate.CCDisabled = true;*/
              
              getClaimInfo();
             
  		}

            function onPertainsToChanged()
            {
                refreshChannelList();
                setCurrentStatus();
            }

            // Reset service channels in the service channel combo based upon the selected pertains to.
            function refreshChannelList()
            {
                gsClaimAspectID = selPertainsTo.value;
                var bDisabled = selChannel.CCDisabled;
                selChannel.CCDisabled = false;
                selChannel.Clear();

                if ( gsClaimAspectID != null && parseInt( gsClaimAspectID, 10 ) > 0 )
                {
                    var sXPath = "/Root/Reference[@List='ServiceChannel' and @ClaimAspectCode='" + gsClaimAspectID + "']";
                    var oChannels = xmlReference.selectNodes( sXPath );
                    if ( oChannels != null )
                    {
                        for (var i = 0; i < oChannels.length; i++)
                        {
                            selChannel.AddItem(oChannels[i].getAttribute("ReferenceID"), oChannels[i].getAttribute("Name"), "", i);
                        }
                        if (strDocumentID == ""){
                           if (selChannel.Options.length == 1){
                              selChannel.selectedIndex = 0;
                           }
                        }
                    }
                    // Try to select a service channel.
                    var nChannelID = parseInt(gsChannelID, 10);
                    if ( nChannelID > 0 )
                    {
                        selChannel.value = nChannelID;
                    }
                }
                selChannel.CCDisabled = bDisabled;
            }

            function setCurrentStatus()
            {

               for (var i = selTemplate.Options.length - 1; i >= 0 ; i--){
                  selTemplate.RemoveItem(i);
               }

                gsChannelID = selChannel.value;
                gsStatusID = 0;
                if ( typeof(selStatus) != "undefined" )
                    selStatus.value = "Unknown";

                if ( gsChannelID != null )
                {
                    var sServiceChannelCD = ""
                    var oServiceChannelCD = xmlReference.selectSingleNode("/Root/Reference[@List='ServiceChannel' and @ReferenceID='" + gsChannelID + "']");
                    if (oServiceChannelCD)
                    {
                        sServiceChannelCD = oServiceChannelCD.getAttribute("ServiceChannelCD");
                        if (sServiceChannelCD != "")
                        {
                            var oStatus = xmlReference.selectSingleNode("/Root/Reference[@List='Status' and @ServiceChannelCD='" + sServiceChannelCD + "']");
                            if (oStatus)
                            {
                                gsStatusID = oStatus.getAttribute("ReferenceID");
                                if ( typeof(selStatus) != "undefined" )
                                    selStatus.value = oStatus.getAttribute("Name");
                            }

                            //load the templates
                            var oTemplates = xmlReference.selectNodes("/Root/Reference[@List='MessageTemplate' and (@ServiceChannelCD='" + sServiceChannelCD + "' or @ServiceChannelCD='')]");
                            for (var i = 0; i < oTemplates.length; i++){
                              selTemplate.AddItem(oTemplates[i].getAttribute("ReferenceID"),
                                                  oTemplates[i].getAttribute("Name").split("|")[0]);
                            }
                        }
                    }
                }
            }

            // Returns true if this note is older than 30 minutes.
            function TimedOut() {
                try {
                    if ( strCreatedDateSQL != '' )
                    {
                       // var varDate = new Date();
					              var varDate = varCurrentDate;
                        return ( ( varDate.getTime() - varCreatedDate.getTime() ) > ( 1800000 ) );//1800000
                    }
                }
                catch ( e ) { ClientError( e.message ); }
                return false;
            }

            function doSave(){
              var sProc = "";
              var sRequest = "";
              var aRequests = new Array();
              var sMethod = "";
              var sRetVal = "";
              if (strDocumentID == "" && selNoteType.selectedIndex == -1){
                ClientWarning("Please select a Note Type.  If the Note Type selection list is empty, click cancel and then try to enter the note again.");
                return;
              }
              if (selPertainsTo.CCDisabled == false && selPertainsTo.selectedIndex == -1){
                ClientWarning("Please select a value for Pertains To.  If the Pertains To selection list is empty, click cancel and then try to enter the note again.");
                return;
              }
              if (selChannel.CCDisabled == false && selChannel.selectedIndex == -1){
                ClientWarning("Please select a Service Channel.  If the Channel selection list is empty, click cancel and then try to enter the note again.");
                return;
              }
              if (gsSupervisorFlag != 1) {
                if ( TimedOut() ){
                    ClientInfo("30 minutes have passed since this note was created.\n You no longer have permission to modify this note.");
                    return;
                }
              }

              if (txtNote.value.length == 0){
                  ClientWarning('Please enter a note body before saving.');
                  return;
              }
              if (txtNote.value.length > iNoteMaxLength){
                var sMsg = "Note body cannot be longer than " + iNoteMaxLength + " characters.  It is currently " + txtNote.value.length + " characters long.";
                ClientWarning(sMsg);
                return;
              }

              if (parseInt(strDocumentID, 10) > 0){
                sProc = "uspNoteUpdDetail";
                sRequest = "ClaimAspectID=" + (selPertainsTo.CCDisabled == false ? selPertainsTo.value : strClaimAspectID) +
                            "&ClaimAspectServiceChannelID=" + gsChannelID +
                            "&OldClaimAspectServiceChannelID=" + gsOldChannelID +
                            "&DocumentID=" + strDocumentID +
                            "&NoteTypeID=" + strNoteTypeID +
                            "&StatusID=" + gsStatusID +
                            "&Note=" + escape(txtNote.value) +
                            "&OldClaimAspectID=" + strClaimAspectID +
                            "&PrivateFlag=" + chkPrivate.value +
                            "&UserID=" + strUserID +
                            "&SysLastUpdatedDate=" + strSysLastUpdatedDate;
                sMethod = "ExecuteSpNp";
                var iChange;
                if ((strContext != strPertainsTo) && (strContext == strOrigPertainsTo))
                  iChange = -1;
                else if ((strContext == strPertainsTo) && (strContext != strOrigPertainsTo))
                  iChange = 1;
                else
                  iChange = 0;

                var sCurrentContext = (strContext != strPertainsTo) ? "true" : "false";
                sRetVal = sCurrentContext + "|" + iChange + "|" + strPertainsTo.substr(0,3);
              } else {
                sProc = "uspNoteInsDetail";
                sRequest = "ClaimAspectID=" + selPertainsTo.value +
                            "&ClaimAspectServiceChannelID=" + gsChannelID +
                            "&NoteTypeID=" + selNoteType.value +
                            "&StatusID=" + gsStatusID +
                            "&Note=" + escape(txtNote.value) +
                            "&UserID=" + strUserID +
                            "&PrivateFlag=" + chkPrivate.value;
                sMethod = "ExecuteSpNp";
                var sCurrentContext = strContext == strPertainsTo ? "true" : "false";
                sRetVal = sCurrentContext + "|1|" + strPertainsTo.substr(0,3);
              }

              //alert(sProc + "\n" + sRequest); return;

              aRequests.push( { procName : sProc,
                                method   : sMethod,
                                data     : sRequest }
                            );
              var sXMLRequest = makeXMLSaveString(aRequests);
              //alert(sXMLRequest);

              var objRet = XMLSave(sXMLRequest);

              if (objRet && objRet.code == 0) {
                dialogArguments.refreshNotesWindow(false)
                window.close();
              }
            }

            function doCancel(){
              window.returnValue = "false";
              window.close();
            }

            function getClaimInfo(){
               var sProc = strClaimMessagesSP;
               var sRequest = "LynxID=" + strLynxID + 
                              "&UserID=" + strUserID;
      
               var sMethod = "ExecuteSpNpAsXML";
               
               var aRequests = new Array();
               aRequests.push( { procName : sProc,
                                 method   : sMethod,                                     
                                 data     : sRequest }
                             );
               var sXMLRequest = makeXMLSaveString(aRequests);
               //alert(sXMLRequest); return;
               
               AsyncXMLSave(sXMLRequest, getDataSuccess, getDataFailure);
               
            }
            
            function getDataSuccess(objXML){
                  blnDataReady = true;
                  var strProcResultXML = objXML.xml.selectSingleNode("//Result/Root").xml
                  xmlClaimInfo.async = false;
                  xmlClaimInfo.loadXML(strProcResultXML);
                  var obj = xmlClaimInfo.selectSingleNode("/Root");
                  if (obj){
                     if (selPertainsTo.selectedIndex != -1) {
                        obj.setAttribute("ClaimAspectID", selPertainsTo.value);
                     }
                  }
                  blnClaimInfoReady = true;
            }
            
            function getDataFailure(){
            }
            
            function showMessage(){
               strMessageTemplateID = selTemplate.value;
               var objReference = xmlReference.selectSingleNode("/Root/Reference[@List='MessageTemplate' and @ReferenceID = '" + strMessageTemplateID + "']");
               if (objReference){
                  var strMessageDescription = objReference.getAttribute("Name");
                  var strMessageTemplatePath = strMessageDescription.split("|")[1];
                  xmlMessageTemplate.ondatasetcomplete = messageTemplateReady;
                  xmlMessageTemplate.src = strMessageTemplatePath;
               }
            }
            
            function messageTemplateReady(){
               if ((blnClaimInfoReady == false) || (xmlMessageTemplate.readyState != "complete")){
                  window.setTimeout("messageTemplateReady()", 100);
                  return;
               }

               if (selTemplate.selectedIndex != -1){
                  var strResult = xmlClaimInfo.transformNode(xmlMessageTemplate);
                  if (strResult != ""){
                     txtNote.value = strResult;
                  }
               }
            }
            
            ]]>
        </script>

      </head>
      <body style="margin:5px;">
        <div style="position1:absolute; z-index:1; top1:10px; left1:10px;">
          <xsl:attribute name="CRUD"><xsl:value-of select="AdjustedCrud"/></xsl:attribute>
            <!-- Data Island for hiding 'reference' xml -->
            <xml id="xmlReference">
              <xsl:element name="Root">
                <xsl:copy-of select="Root/Reference"/>
              </xsl:element>
            </xml>
            <xsl:apply-templates select="/Root/Note"/>
        </div>
		
        <!-- 04Jan2013 TVD Please Wait Fix -->
		<div id="pageLoading" style="position:absolute; z-index:1; top:70px; left:260px; color:Red">
			Loading, Please wait....
		</div>
		
        <xml id="xmlClaimInfo" name="xmlClaimInfo">
          <Root/>
        </xml>
        <xml id="xmlMessageTemplate" name="xmlMessageTemplate"></xml>

        <!-- 04Jan2013 TVD Please Wait Fix -->
		<script language="JavaScript">
			doAgain();
		</script>
	  </body>
    </html>
  </xsl:template>

  <xsl:template match="Note">

    <xsl:variable name="LynxId"/>

    <xsl:variable name="StatusID" select="@StatusID"/>
    <xsl:variable name="ClaimAspectCode" select="@ClaimAspectCode"/>
    <xsl:variable name="ClaimAspectID" select="@ClaimAspectID"/>

    <table width1='100%' border='0' cellspacing='0' cellpadding='2' style="table-layout:fixed">
      <colgroup>
        <col width="75"/>
        <col width="150"/>
        <col width="65"/>
        <col width="150"/>
      </colgroup>
      <xsl:if test="@CreatedUserID!=''">
        <tr>
          <td>Date Entered:</td>
          <td colspan="3">
            <IE:APDLabel id="txtCreatedDate" name="txtCreatedDate" width="250" >
              <xsl:attribute name="value"><xsl:value-of select="js:formatSQLDateTime(string(@CreatedDate))"/></xsl:attribute>
            </IE:APDLabel>
            <!-- <xsl:value-of disable-output-escaping="yes" select="js:InputBox('CreatedDate',25,'CreatedDate',.,'',4)"/> -->
          </td>
        </tr>
        <tr>
          <td>Created By:</td>
          <td>
            <IE:APDLabel id="txtCreatedBy" name="txtCreatedBy" width="145" >
              <xsl:attribute name="value"><xsl:value-of select="user:FormatPersonName(@CreatedUserNameFirst,@CreatedUserNameLast)"/></xsl:attribute>
            </IE:APDLabel>
          </td>
          <td>User Role:</td>
          <td>
            <IE:APDLabel id="txtCreatedRole" name="txtCreatedRole" width="145" >
              <xsl:attribute name="value"><xsl:value-of select="@CreatedUserRoleName"/></xsl:attribute>
            </IE:APDLabel>
            <!-- <xsl:value-of disable-output-escaping="yes" select="js:InputBox('CreatedUserRoleName',23,'CreatedUserRoleName',.,'',5)"/> -->
          </td>
        </tr>
      </xsl:if>
      <tr>
        <td>Note Type:</td>
        <td colspan="3">
          <xsl:choose>
            <xsl:when test="/Root/Note/@DocumentID != ''">
              <IE:APDLabel id="selNoteType" name="selNoteType" width="200" >
                <xsl:attribute name="value"><xsl:value-of select="/Root/Reference[@List='NoteType' and @ReferenceID=/Root/Note/@NoteTypeID]/@Name"/></xsl:attribute>
              </IE:APDLabel>
            </xsl:when>
            <xsl:otherwise>
              <IE:APDCustomSelect id="selNoteType" name="selNoteType" 
                  displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false"
                  CCTabIndex="1" required="true" width="200" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="@NoteTypeID"/></xsl:attribute>
                <xsl:for-each select="/Root/Reference[@List='NoteType' and @EnabledFlag = '1']">
              	<IE:dropDownItem imgSrc="" style="display:none">
                  <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                  <xsl:value-of select="@Name"/>
                </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <IE:APDCheckBox id="chkPrivate" name="chkPrivate" caption="Private Note"
            CCDisabled="false" required="false" CCTabIndex="2" canDirty="false" onBeforeChange=""
            onChange="" onAfterChange="" >
            <xsl:attribute name="value"><xsl:value-of select="@PrivateFlag"/></xsl:attribute>
          </IE:APDCheckBox>
          <!-- <xsl:value-of disable-output-escaping="yes"
              select="js:AddSelect( 'NoteTypeID', 2, '', '', 1, 10, /Root/Reference[@List='NoteType' and @EnabledFlag = '1'], 'Name', 'ReferenceID', 0, 0, 'NoteTypeID', . )"/> -->
        </td>
      </tr>
      <tr>
        <td>Pertains To:</td>
        <td colspan="3">
          <IE:APDCustomSelect id="selPertainsTo" name="selPertainsTo" 
            displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false"
            CCTabIndex="3" required="true" width="100" onChange="onPertainsToChanged()">

            <xsl:variable name="claimAspect"><xsl:value-of select="concat(@ClaimAspectCode, @ClaimAspectNumber)"/></xsl:variable>
            <xsl:attribute name="value"><xsl:value-of select="/Root/Reference[@List='PertainsTo' and @ClaimAspectCode=$claimAspect]/@ReferenceID"/></xsl:attribute>
            <xsl:for-each select="/Root/Reference[@List='PertainsTo' and @ClaimAspectCode!='clm']">
          	<IE:dropDownItem imgSrc="" style="display:none">
              <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
              <xsl:value-of select="@Name"/>
            </IE:dropDownItem>
            </xsl:for-each>
          </IE:APDCustomSelect>
        </td>
      </tr>
      <tr>
        <td>Channel:</td>
        <td colspan="2">
          <IE:APDCustomSelect id="selChannel" name="selChannel" 
              displayCount="5" blankFirst="false" canDirty="false" CCTabIndex="4"
              required="true" width="150" onChange="setCurrentStatus()">
          </IE:APDCustomSelect>
        </td>
      </tr>
      <tr>
        <td>Template:</td>
        <td colspan="2">
          <IE:APDCustomSelect id="selTemplate" name="selTemplate" 
              displayCount="5" blankFirst="false" canDirty="false" CCTabIndex="4"
              required="false" width="150" onChange="showMessage()">
          </IE:APDCustomSelect>
        </td>
      </tr>
      <xsl:if test="/Root/Note/@DocumentID != ''">
        <tr>
          <td>Status:</td>
          <td colspan="3">
            <IE:APDLabel id="selStatus" name="selStatus" width="150" >
              <xsl:attribute name="value"><xsl:value-of select="/Root/Reference[@List='Status' and @ReferenceID=/Root/Note/@StatusID]/@Name"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
      </xsl:if>
      <tr>
        <td colspan='4'>
          <IE:APDTextArea id="txtNote" name="txtNote" width="455" height="75"
            required="true" canDirty="false" CCDisabled="false" CCTabIndex="5" onChange="" >
            <xsl:attribute name="value"><xsl:value-of select="@Note"/></xsl:attribute>
            <xsl:attribute name="maxLength"><xsl:value-of select="/Root/Metadata[@Entity='Note']/Column[@Name='Note']/@MaxLength"/></xsl:attribute>
          </IE:APDTextArea>
        </td>
      </tr>
      <tr>
        <td colspan='4'>
          <table border="0" cellpadding="0" cellspacing="0" style="width:100%;table-layout:fixed">
            <colgroup>
              <col width="*"/>
              <col width="150px"/>
              <col width="*"/>
            </colgroup>
            <tr>
              <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
              <td>
					<IE:APDButton id="btnSave" name="btnSave" value="Save" width="" CCDisabled="false" CCTabIndex="6" onButtonClick="doSave()" />
					<IE:APDButton id="btnCancel" name="btnCancel" value="Cancel" width="" CCDisabled="false" CCTabIndex="7" onButtonClick="doCancel()"/>
              </td>
              <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
            </tr>
          </table>
          <!-- <input type='button' class='formbutton' name='butCancelNote' value='Cancel' onClick='window.close()'/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <input type='button' class='formbutton' name='butSaveNote' value='Save' onClick='SaveNote(true)'/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp; -->
          <!--<input type='button' class='formbutton' name='butSaveNote' value='Dev Save' onClick='SaveNote(false)'/>-->
        </td>
      </tr>
    </table>
  </xsl:template>

</xsl:stylesheet>

