<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="UserDetail">

<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:decimal-format NaN="0"/>


<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="InsertMode"/>
<xsl:param name="LoginUserID"/>
<xsl:param name="UserProfileCRUD" select="User Profile"/>
<xsl:param name="UserPermissionCRUD" select="User Permission"/>

<xsl:template match="/Root">
  
  <!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspAdmUserGetDetailXML,UserDetail.xsl, 33   -->

    <xsl:choose>
        <xsl:when test="(substring($UserProfileCRUD, 2, 1) = 'R') or ((/Root/@UserID = '-1') and (substring($UserProfileCRUD, 1, 1) = 'C'))">
            <xsl:call-template name="mainHTML">
              <xsl:with-param name="UserProfileCRUD" select="$UserProfileCRUD"/>
              <xsl:with-param name="UserPermissionCRUD" select="$UserPermissionCRUD"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <html>
          <head>
              <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
              <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
          </head>
          <body class="bodyAPDSub" unselectable="on" style="background-color:#FFFFFF">
            <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
              <tr>
                <td align="center">
                  <font color="#ff0000"><strong>You do not have sufficient permission to add/view User detail. 
                  <br/>Please contact your supervisor.</strong></font>
                </td>
              </tr>
            </table>
          </body>
          </html>
        </xsl:otherwise>
    </xsl:choose>
    
</xsl:template>

<xsl:template name="mainHTML">
  <xsl:param name="UserProfileCRUD"/>
  <xsl:param name="UserPermissionCRUD"/>
  <xsl:variable name="ProfileReadOnly" select="boolean(substring($UserProfileCRUD, 3, 1) != 'U')"/>
  <xsl:variable name="PermissionReadOnly" select="boolean(substring($UserPermissionCRUD, 3, 1) != 'U')"/>
  <xsl:variable name="APDAppID"><xsl:value-of select="/Root/Reference[@List='Application' and @Name='APD']/@ReferenceID"/></xsl:variable>
<!-- <xsl:variable name="ProfileReadOnly">
  <xsl:value-of select="boolean(substring($UserProfileCRUD, 3, 1) != 'U')"/>
</xsl:variable>
<xsl:variable name="PermissionReadOnly">
  <xsl:value-of select="boolean(substring($UserPermissionCRUD, 3, 1) != 'U')"/>
</xsl:variable> -->

<html>
<title>User Information</title>
<head>
<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/CoolSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<!-- Script modules -->
<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formutilities.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/utilities.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,32);
		  event.returnValue=false;
		  };	
</script>

<script language="JavaScript">
  var gsCRUD = "<xsl:value-of select="/Root/@CRUDUserProfile"/>";
  var gsCRUDUserProfile = "<xsl:value-of select="$UserProfileCRUD"/>";
  var gsCRUDUserPermission = "<xsl:value-of select="$UserPermissionCRUD"/>";

	var gsLoginUserID = "<xsl:value-of select="$LoginUserID"/>";
    var nRolesCount = <xsl:value-of select="count(/Root/Reference[@List='Role'])"/>;
    var nTotalRoles = <xsl:value-of select="count(Role[@MemberFlag=1])"/>;
	var nClaimRoles = <xsl:value-of select="count(/Root/Reference[@List='Role' and @BusinessFunctionCD='C'])"/>;
	var nProgramRoles = <xsl:value-of select="count(/Root/Reference[@List='Role' and @BusinessFunctionCD='S'])"/>;
    var gsUserLastUpdatedDate = "<xsl:value-of select="/Root/User/@SysLastUpdatedDate"/>";
    var gbInsertMode = <xsl:value-of select="$InsertMode"/>;
    var gbProfileCount = <xsl:value-of select="count(/Root/User/Profile)"/>;
    var gbPermissionCount = <xsl:value-of select="count(/Root/User/Permission)"/>;
    var gbLicenseStateCount = <xsl:value-of select="count(/Root/User/States)"/>;
    var bResetAllOverride = false;    
  	var nCurrentPrimaryIndex = "<xsl:value-of select="/Root/User/Role[@PrimaryRoleFlag='1']/@RoleID"/>";
<!--     var gsStateLicenseRequired = new Array(<xsl:for-each select="/Root/User/States"><xsl:value-of select="@LicenseRequired"/><xsl:if test="position() != last()">, </xsl:if></xsl:for-each>); -->
    var gsStateCodes = new Array(<xsl:for-each select="/Root/User/States">"<xsl:value-of select="@StateCode"/>"<xsl:if test="position() != last()">, </xsl:if></xsl:for-each>);
<!--     var gsNoLicReqStCount = <xsl:value-of select="count(/Root/User/States[@AssignWorkFlag=1 and @LicenseRequired=0])"/>; -->
<!--     var gsNoLicReqStCount2 = gsNoLicReqStCount; -->
    var sAppID = "<xsl:value-of select="Reference[@List='Application' and @Name='APD']/@ReferenceID"/>";
    var sAppSysLastUpdatedDate = "<xsl:value-of select="User/Application[@ApplicationID=$APDAppID]/@SysLastUpdatedDate"/>";
    var gsInsCoCount = "<xsl:value-of select='count(/Root/Reference[@List="InsuranceCompany"])'/>";
    var gsInsAssignWork = new Array(<xsl:for-each select="/Root/Reference[@List='InsuranceCompany']"><xsl:variable name="InsuranceCompanyID" select="@ReferenceID"></xsl:variable><xsl:choose><xsl:when test="/Root/User/Insurance[@InsuranceCompanyID=$InsuranceCompanyID]">"1"</xsl:when><xsl:otherwise>"0"</xsl:otherwise></xsl:choose><xsl:if test="position() != last()">, </xsl:if></xsl:for-each>);
    var gsInsAssignWorkCount = <xsl:value-of select="count(/Root/Reference[@List='InsuranceCompany'])"/>;
    var gsInsAssignWorkCount2 = <xsl:value-of select="count(/Root/User/Insurance)"/>;
    var gsInsCoIDs = new Array(<xsl:for-each select="/Root/Reference[@List='InsuranceCompany']"><xsl:value-of select="@ReferenceID"/><xsl:if test="position() != last()">, </xsl:if></xsl:for-each>);
    
    var gsLicReqStCount =  <xsl:value-of select="count(/Root/User/States[@AssignWorkFlag=1 or @LicenseRequired=1])"/>;
	<!-- var gsLicReqStCount =  0; -->
	var gsUserStates = new Array(<xsl:for-each select="/Root/User/States[@AssignWorkFlag=1 or @LicenseFlag=1]">"<xsl:value-of select="@StateCode"/>"<xsl:if test="position() != last()">, </xsl:if></xsl:for-each>);
	var gsUserStatesAssignWork = new Array(<xsl:for-each select="/Root/User/States[@AssignWorkFlag=1 or @LicenseFlag=1]">"<xsl:value-of select="@AssignWorkFlag"/>"<xsl:if test="position() != last()">, </xsl:if></xsl:for-each>);
	var gsUserStatesLicensed = new Array(<xsl:for-each select="/Root/User/States[@AssignWorkFlag=1 or @LicenseFlag=1]">"<xsl:value-of select="@LicenseFlag"/>"<xsl:if test="position() != last()">, </xsl:if></xsl:for-each>);
  var gsUserInsurance = new Array(<xsl:for-each select="/Root/User/Insurance">"<xsl:value-of select="@InsuranceCompanyID"/>"<xsl:if test="position() != last()">, </xsl:if></xsl:for-each>);

	var gsUserProfileCount = <xsl:value-of select="count(/Root/User/Profile[@OverriddenFlag=1 and @BusinessFunctionCD='C'])"/>;
	var gsUserProfileIDList = new Array(<xsl:for-each select="/Root/User/Profile[@OverriddenFlag=1 and @BusinessFunctionCD='C']">"<xsl:value-of select="@ProfileID"/>"<xsl:if test="position() != last()">, </xsl:if></xsl:for-each>);
	var gsUserProfileValueList = new Array(<xsl:for-each select="/Root/User/Profile[@OverriddenFlag=1 and @BusinessFunctionCD='C']">"<xsl:value-of select="@ProfileValue"/>"<xsl:if test="position() != last()">, </xsl:if></xsl:for-each>);
	
	var gsMondayStartTime = "<xsl:value-of select="/Root/User/@OperatingMondayStartTime"/>";
	var gsMondayEndTime = "<xsl:value-of select="/Root/User/@OperatingMondayEndTime"/>";
	var gsTuesdayStartTime = "<xsl:value-of select="/Root/User/@OperatingTuesdayStartTime"/>";
	var gsTuesdayEndTime = "<xsl:value-of select="/Root/User/@OperatingTuesdayEndTime"/>";
	var gsWednesdayStartTime = "<xsl:value-of select="/Root/User/@OperatingWednesdayStartTime"/>";
	var gsWednesdayEndTime = "<xsl:value-of select="/Root/User/@OperatingWednesdayEndTime"/>";
	var gsThursdayStartTime = "<xsl:value-of select="/Root/User/@OperatingThursdayStartTime"/>";
	var gsThursdayEndTime = "<xsl:value-of select="/Root/User/@OperatingThursdayEndTime"/>";
	var gsFridayStartTime = "<xsl:value-of select="/Root/User/@OperatingFridayStartTime"/>";
	var gsFridayEndTime = "<xsl:value-of select="/Root/User/@OperatingFridayEndTime"/>";
	var gsSaturdayStartTime = "<xsl:value-of select="/Root/User/@OperatingSaturdayStartTime"/>";
	var gsSaturdayEndTime = "<xsl:value-of select="/Root/User/@OperatingSaturdayEndTime"/>";
	var gsSundayStartTime = "<xsl:value-of select="/Root/User/@OperatingSundayStartTime"/>";
	var gsSundayEndTime = "<xsl:value-of select="/Root/User/@OperatingSundayEndTime"/>";
	var gsAssignmentStartDate = "<xsl:value-of select="/Root/User/@AssignmentBeginDate"/>";
	var gsAssignmentEndDate ="<xsl:value-of select="/Root/User/@AssignmentEndDate"/>";
	

    
<![CDATA[

	var bRolesChanged = false;
  var bAssignmentPoolChanged = false;
    
    var doc = document.all;

	// button images for ADD/DELETE/SAVE
	preload('buttonAdd','/images/but_ADD_norm.png');
	preload('buttonAddDown','/images/but_ADD_down.png');
	preload('buttonAddOver','/images/but_ADD_over.png');
	preload('buttonDel','/images/but_DEL_norm.png');
	preload('buttonDelDown','/images/but_DEL_down.png');
	preload('buttonDelOver','/images/but_DEL_over.png');
	preload('buttonSave','/images/but_SAVE_norm.png');
	preload('buttonSaveDown','/images/but_SAVE_down.png');
	preload('buttonSaveOver','/images/but_SAVE_over.png');

    var sTime, eTime, dNow;
    dNow = new Date();
    sTime = dNow.getSeconds() * 1000 + dNow.getMilliseconds();
    
    function stopTimer(){
        dNow = new Date();
        eTime = dNow.getSeconds() * 1000 + dNow.getMilliseconds();
        window.status = ((eTime - sTime)/1000 + " sec.");
    }

  var currentTabRow = 1;
  var curTabID = "";
  var tabArray = null;
  var tabsHIndend = 10;
  var tabsVIndend = 18;
  

	// Page Initialize
	function PageInit()
	{
        //tabInit(false, '#FFFFFF'); 
        ShowSB80();
        InitTabs();
        /*window.setTimeout("initSelectBoxes()", 1, "javascript");  
        window.setTimeout("InitFields()", 1, "javascript");  
        window.setTimeout("onpasteAttachEvents()", 1, "javascript");*/
        initSelectBoxes();  
        InitFields();  
        onpasteAttachEvents();
            
        changePage(tab11);
        //window.setTimeout("ShowSB100();stopTimer();", 1);
        ShowSB100();
        stopTimer();
	  }
	
    function ValidateAllPhones() {
        if (validatePhoneSections(doc.PhoneAreaCode, doc.PhoneExchangeNumber, doc.PhoneUnitNumber) == false) return false;
        if (validatePhoneSections(doc.FaxAreaCode, doc.FaxExchangeNumber, doc.FaxUnitNumber) == false) return false;
        if (isNumeric(doc.PhoneExtensionNumber.value) == false){
            ClientWarning("Invalid numeric value specified.");
            try {
              doc.PhoneExtensionNumber.focus();
              doc.PhoneExtensionNumber.select();
            } catch (e) {}
            return false;
        }
        if (isNumeric(doc.FaxExtensionNumber.value) == false){
            ClientWarning("Invalid numeric value specified.");
            try {
              doc.FaxExtensionNumber.focus();
              doc.FaxExtensionNumber.select();
            } catch (e) {}
            return false;
        }
        return true;
    }
    
  function tabBeforeChange(obj, tab)
  {
    var relatedTab = obj
    if (obj == tab) return;
    return ValidateAllPhones();
  }

  function handleJSError(sFunction, e)
  {
    var result = "An error has occurred on the page.\nFunction = " + sFunction + "\n";
    for (var i in e) {
      result += "e." + i + " = " + e[i] + "\n";
    }
    ClientError( result );
  }

	// Validate Role Member Flag
	function ValidateMemberFlag(obj)
	{
    try {
        var objID = obj.id;
        var obj1 = obj.firstChild;

  		// If Role checked is the first one, autopopulate Primary flag
  		if (obj1.value > 0 && nTotalRoles == 0 && nCurrentPrimaryIndex == 0) {
            var obj2 = document.getElementById("grpPrimaryRole_" + objID.substring(0, objID.length - 3).split("_")[1] + "Div");
            if (obj2 != undefined)
                myRadioFunctionality(obj2);
            
  		}

        // If Role is being unchecked make sure primary flag is not checked
  		if (obj1.value == 0) {
            if (parseInt(objID.substring(0, objID.length - 3).split("_")[1]) == nCurrentPrimaryIndex){
                ClientWarning( "You cannot remove the primary role from this user." );
                CheckBoxChange(obj1);
  				return;
  			}
  		}
  		// Adjust Role counts
  		bRolesChanged = true;
  		if (obj1.value > 0) {
  			nTotalRoles++;
  			if (obj.businessFunctionCd == "C") {
  				nClaimRoles++;
  			}
  			else {
  				if (obj.businessFunctionCd == "S") {
  					nProgramRoles++;
  				}
  			}
  		}
  		else {		//checked == false
  			nTotalRoles--;
  			if (obj.businessFunctionCd == "C") {
  				nClaimRoles--;
  			}
  			else {
  				if (obj.businessFunctionCd == "S") {
  					nProgramRoles--;
  				}
  			}
  		}

  		// Adjust visibility of Claim and Shop Program tabs based on role count
  		/*var oSpanClaim = document.getElementById("tab12");
  		var oSpanProgram = document.getElementById("tab13");

  		if (nClaimRoles > 0) {
  			oSpanClaim.style.visibility = "visible";
  		}
  		else {
  			oSpanClaim.style.visibility = "hidden";
  		}

  		if (nProgramRoles > 0) {
  			oSpanProgram.style.visibility = "visible";
  		}
  		else {
  			oSpanProgram.style.visibility = "hidden";
  		}*/
  	}
    catch(e) {
      handleJSError("validateRollMemberFlag", e)
    }
  }


	// Save Primary group value
	function SavePrimaryValue()
	{
    try {
    
        //nCurrentPrimaryIndex = getMyRadioValue();
  		/*var colPrimary = document.getElementsByName("grpPrimaryRole");
  		for (var index = 0; index < colPrimary.length; index++) {
  			if (colPrimary[index].checked == true) {
  				nCurrentPrimaryIndex = index;
  				return;
  			}
  		}*/
        //alert(nCurrentPrimaryIndex);
    }
    catch(e) {
      handleJSError("SavePrimaryValue", e)
    }
	}

	// Validate Primary Role Flag
	function ValidatePrimaryFlag(obj)
	{
    try {
        if (obj.disabled) return false;
        
        var objID = obj.id;
        objID = objID.substring(0, objID.length-3);

  		var colMemberCheck = document.getElementsByName("chkRoleMember_" + objID.split("_")[1]);
        if (colMemberCheck[0].value == 0) {
        var sSave = YesNoMessage("Role Membership check", "In order to set this role as Primary, APD must make the user a member of this role.  Do you wish APD to do this?");
        if (sSave == "Yes") {
  			//if (confirm("In order to set this role as Primary, APD must make the user a member of this role.  Do you wish APD to do this?") == true) {
                CheckBoxChange(colMemberCheck[0].parentElement.firstChild);
                colMemberCheck[0].parentElement.firstChild.onblur();

                var obj2 = colMemberCheck[0].parentElement;
                ValidateMemberFlag(obj2);
                try {
                  obj.focus();
                } catch (e) {}

                return true;
  			}
  			else {
  				var colPrimary = document.getElementsByName(objID.split("_")[0] + "_" + nCurrentPrimaryIndex + "Div");
                myRadioFunctionality(colPrimary[0]);
                return false;
  			}
  		}
        gbDirtyFlag = true;
        return true;
    }
    catch(e) {
      handleJSError("ValidatePrimaryFlag", e);
    }
	}

	// Handle User Activate/Deactivate Button Click
	function SetUserStatus()
	{
    try {
      if (document.readyState != "complete") return;
      
      var doc = document.all;
      var sRequest = "UserID=" +  doc.namedItem("UserID").value +
                     "&ApplicationID=" + sAppID +
                     "&SysLastUserID=" + gsLoginUserID;  // SysLastUserID

      var sLogComment = "User ID: "  + doc.namedItem("UserID").value + " [" + doc.namedItem("CsrNo").value + "]";
      if (doc.namedItem("txtActiveInactive").value.indexOf("Inactive") != -1)
        sLogComment += " was activated";
      else
        sLogComment += " was deactivated";
      var oReturn = RSExecute("/rs/RSADSAction.asp", "RadExecute", "uspAdmUserSetStatus", sRequest);
      
      if (oReturn.status == -1) {
        ServerEvent();
  	  }
  	  else {
          var aReturn = oReturn.return_value.split("||");
          if (aReturn[1] != 0)  {
            var sReturnXML = aReturn[0];
            var sRetUserID, sRetLastUpdatedDate, sActive, sAccessBeginEndDate, sText;
            if (sReturnXML.substr(0, 6) == "<Root>" ) {
              var iStartIdx = sReturnXML.indexOf("UserID=");
              sRetUserID = sReturnXML.substring( iStartIdx + 8, sReturnXML.indexOf("\"", iStartIdx + 8))
              
              iStartIdx = sReturnXML.indexOf("Active=", iStartIdx);
              sActive = sReturnXML.substr( iStartIdx + 8, 1)

              iStartIdx = sReturnXML.indexOf("AccessBeginEndDate=", iStartIdx);
              sAccessBeginEndDate = sReturnXML.substr( iStartIdx + 20, sReturnXML.indexOf("\"", iStartIdx + 20))

              iStartIdx = sReturnXML.indexOf("SysLastUpdatedDate=", iStartIdx);
              sRetLastUpdatedDate = sReturnXML.substring( iStartIdx + 20, sReturnXML.indexOf("\"", iStartIdx + 20))

              if (sActive == "1")
                sText = "Active ";
              else
                sText = "Inactive ";
                
              if (sAccessBeginEndDate.indexOf("T") > 0) {
                sText += " since " + UTCConvertDate(sAccessBeginEndDate) + " " + UTCConvertTime(sAccessBeginEndDate);
              }
              
              doc.namedItem("txtActiveInactive").value = sText;
              var objCurRow = window.parent.curRow;
              if (sActive == "1") {
                // User is now active
                doc.namedItem("cmdActivate").value = "Deactivate";
                if (objCurRow){
                  objCurRow.cells[2].innerText = "Yes";
                  objCurRow.cells[3].innerText = UTCConvertDate(sAccessBeginEndDate) + " " + UTCConvertTime(sAccessBeginEndDate);
                }
              }
              else {
                // User is now inactive
                doc.namedItem("cmdActivate").value = "Activate";
                if (objCurRow){
                  objCurRow.cells[2].innerText = "No";
                  objCurRow.cells[3].innerText = UTCConvertDate(sAccessBeginEndDate) + " " + UTCConvertTime(sAccessBeginEndDate);
                }
              }
    
              // Update SysLastUpdatedDate based on output
              document.getElementById("content11").setAttribute("LastUpdatedDate", sRetLastUpdatedDate);
              sAppSysLastUpdatedDate = sRetLastUpdatedDate;

              sRequest = "AuditTypeCD=S&KeyDescription=" + doc.namedItem("NameFirst").value + " " + doc.namedItem("NameLast").value + 
                         "&KeyID=" + doc.namedItem("UserID").value + "&LogEntry=" + sLogComment + "&UserID=" + gsLoginUserID
                         
              var oReturn = RSExecute("/rs/RSADSAction.asp", "JustExecute", "uspAuditLogInsDetail", sRequest);
            }
          }
      }
    }
    catch(e) {
      handleJSError("SetUserStatus", e);
    }
	}

	var inADS_Pressed = false;		// re-entry flagging for buttons pressed
  var objDivWithButtons = null;
	// Handle click of Add or Save button
	function ADS_Pressed(sAction, sName)
	{
      if (document.readyState != "complete") return false;
      if (ValidateAllPhones() == false) return false;
      // Field Edit Checks
      if (GetValueFromElement("CsrNo") == "") {
        ClientWarning( "CSR Number is Required." );
        return false;
      }

      if (GetValueFromElement("NameFirst") == "") {
        ClientWarning( "First Name is Required." );
        return false;
      }

      if (GetValueFromElement("NameLast") == "") {
        ClientWarning( "Last Name is Required.");
        return false;
      }

      if (GetValueFromElement("EmailAddress") == "") {
        ClientWarning( "Email address is Required.");
        return false;
      }

      // Phone number is required.
      if (GetValueFromElement("PhoneAreaCode") == "" || GetValueFromElement("PhoneExchangeNumber") == "" || GetValueFromElement("PhoneUnitNumber") == "" ){
          ClientWarning( "The phone number is required. Please enter a valid phone number.");
          return false;
      }

      var sPrimaryRoleIndex = nCurrentPrimaryIndex;
      if (sPrimaryRoleIndex == "") {
          ClientWarning( "The user must have at least one role selected.");
          return false;
      }
      


    try {
      if (sAction == "Update" && !gbDirtyFlag) return; //no change made to the page
  		if (inADS_Pressed == true) return;
  		inADS_Pressed = true;
      
      objDivWithButtons = window.parent.crudDivUsers;
      disableADS(objDivWithButtons, true);

  		var oDiv = document.getElementById(sName);
  		if (sAction == "Update") {
	        if (gbInsertMode == true) {
            sAction = "Insert"
          }

		  setTimeout(ShowSB40, 1);
          var aRetValue = SaveData(sAction);
          setTimeout(ShowSB80,2);
          
        //var sParentURL = window.parent.location.href;
        //if (sParentURL.indexOf("?") != -1)
        //  sParentURL = sParentURL.substring(0, sParentURL.indexOf("?"));
          
        //alert(sParentURL);
        // Process any return value.  If aRetValue[1] = 1, aRetValue[0] contains the recordset formatted as a comma-delimited
        // string, if aRetValue[1] = 0 , aRetValue[0] contains an error message.
        if (aRetValue != null) {
          if (aRetValue[1] != 0)  {
            var sReturnXML = aRetValue[0];
            var sRetUserID, sRetLastUpdatedDate;
            if (sReturnXML.substr(0, 6) == "<Root>" ) {
              var iStartIdx = sReturnXML.indexOf("UserID=");
              sRetUserID = sReturnXML.substring( iStartIdx + 8, sReturnXML.indexOf("\"", iStartIdx + 8))
              
              iStartIdx = sReturnXML.indexOf("SysLastUpdatedDate=", iStartIdx);
              sRetLastUpdatedDate = sReturnXML.substring( iStartIdx + 20, sReturnXML.indexOf("\"", iStartIdx + 20))

            }

            if (gbInsertMode == true) {
              // refresh the page with the new user id
              window.parent.navigate("/admin/Users.asp?sel=" + sRetUserID);
            }
            else {
              if (bRolesChanged || bAssignmentPoolChanged) {
                //defect #879 - refresh the page if the role membership changed.
                //if (doc.namedItem("UserID").value != "")
                  //parent.oIframe.frameElement.src = "/admin/UserDetail.asp?UserID=" + doc.namedItem("UserID").value;
                //else
                  //window.location.reload();
                  window.parent.navigate("/admin/Users.asp?sel=" + document.all["UserID"].value);
                return;
              }
              else {
                // Update SysLastUpdatedDate based on output
//                var oMainDiv = document.getElementById("content11");
//                oMainDiv.setAttribute("LastUpdatedDate", sRetLastUpdatedDate);
  
      			    gbDirtyFlag = false;
          			if (typeof(parent.gbDirtyFlag)=='boolean')
          				parent.gbDirtyFlag = false;
          
                resetControlBorders(document.getElementById("content11"));
                var objCurRow = window.parent.curRow;
                if (objCurRow) {
                  objCurRow.cells[0].innerText = document.all["CsrNo"].value;
                  objCurRow.cells[1].innerText = document.all["NameLast"].value + ", " + document.all["NameFirst"].value;
                  objCurRow.cells[6].innerText = (document.all["SupervisorFlag"].value == "1" ? "Yes" : "No");
                }
              }
            }
          }
          else {
            // If this code executes, most likely an error was raised in the middle tier or the database
            ServerEvent();
          }
        }
      }

  		else if (sAction == "Insert") {
  			if (oDiv.Many == "true") {
  				parent.oIframe.frameElement.src = "/admin/UserDetail.asp?UserID=New";
  			}
  		}
      else {
        var userError = new Object();
        userError.message = "Unhandled ADS action: Action = " + sAction;
        userError.name = "MiscellaneousException";
        throw userError;
			}

  		inADS_Pressed = false;
      disableADS(objDivWithButtons, false);
      objDivWithButtons = null;
        setTimeout(ShowSB100,300);
    }
    catch(e) {
      handleJSError("ADS_Pressed", e);
    }
	}

      
	function SaveData(sAction)
	{
    try {
      var sAssignmentBeginDate = "";
      var sAssignmentEndDate = "";
      // Begin Save
/*      if (!gbInsertMode){
        if (document.getElementById("AssignmentBeginDate").value == "01/01/1900 00:00:00 AM")
          document.getElementById("AssignmentBeginDate").value = "";
        if (document.getElementById("AssignmentEndDate").value == "01/01/1900 00:00:00 AM")
          document.getElementById("AssignmentEndDate").value = "";
          
        sAssignmentBeginDate = document.getElementById("AssignmentBeginDate").value;
        sAssignmentEndDate = document.getElementById("AssignmentEndDate").value;
      }
*/
      var sRequest = "";
      var sProcName = "";
      var bLicenseState = false;
      var bInsAssignWork = false;
      var sLicenseState = ""; 
	  var sInsAssignWork = "";
	  
	  var tempObj = new String(gsUserInsurance);   
      sInsAssignWork = tempObj.replace(/[,]/g, ";");
   
	  tempObj="";

    // Get list of states the user has either a LicensedFlag or AssignWorkFLag set
    var sLicenseAssignStList = document.getElementById("txtUserStates").value;

/*
      for(var i = 0; i<gsLicReqStCount; i++ )
	  tempObj = tempObj + gsUserStates[i] + ","  + gsUserStatesLicensed[i] + "," + gsUserStatesAssignWork[i] + ";" ;
	  sLicenseState = tempObj; 
*/
	  var strUserProfileList = new String();
	  for (var i = 0; i< gsUserProfileCount; i++)
	  {
	  strUserProfileList = strUserProfileList + gsUserProfileIDList[i] + "," + gsUserProfileValueList[i] ;
	  if (i != gsUserProfileCount-1) strUserProfileList = strUserProfileList + ",";
      }
	
	      
	/* var strUserLicStates = new String();   
	   strUserLicStates="";
       for(var i = 0; i<=gsLicReqStCount; i++ )
	   strUserLicStates = strUserLicStates + gsUserStates[i] + ","  + gsUserStatesLicensed[i] + "," + gsUserStatesAssignWork[i] + ";" ;
		
	   alert(strUserLicStates);

	   var objLicenseState = document.getElementById("chkStateLicensed_" + idx);
            var objAssignWork = document.getElementById("chkStateAssignWork_" + idx);
            if (objLicenseState && objAssignWork){
                if (objLicenseState.value != "0" || objAssignWork.value != "0"){
                    bLicenseState = true;
                    //sLicenseState += objLicenseState.value + ",";
                    sLicenseState += gsStateCodes[idx-1] + "," + 
                                    (objLicenseState.value != "0" ? "1" : "0") + "," +
                                    (objAssignWork.value != "0" ? "1" : "0") + ";";
                    
                }
            }
        }
        if (bLicenseState) {
            //Strip off semi-colon and close parameter
            sLicenseState = sLicenseState.substring(0, sLicenseState.length - 1);
        }
        
   */
   
   
        
        // Get list of Insurance Company IDs from which this Rep may be assigned work.
   /*     for (idx=1; idx <= gsInsCoCount; idx++){
          var objInsAssignWork = document.getElementById("chkInsAssignWork_" + idx);
          
          if (objInsAssignWork){
            if (objInsAssignWork.value != "0"){
              bInsAssignWork = true;
              sInsAssignWork += gsInsCoIDs[idx-1] + ";";
            }
          }
        }
        
        if (bInsAssignWork){
          //Strip off semi-colon and close parameter
          sInsAssignWork = sInsAssignWork.substring(0, sInsAssignWork.length - 1);
        }        
      }*/
     
        var sProfileList2 = "";
        var sPermissionList2 = "";
        
      // User Roles
      {
        var sRoleList = "";
        var objIdx;        
        var nPrimaryRoleID = nCurrentPrimaryIndex;
        //if (nCurrentPrimaryIndex > 0 && nCurrentPrimaryIndex < tblRole.rows.length){
          //nPrimaryRoleID = tblRole.rows[nCurrentPrimaryIndex-1].cells[3].innerText;
        //}
        
        // Primary role to list first
        if (nCurrentPrimaryIndex != "") {
          sRoleList = nCurrentPrimaryIndex + ",";
        }
        
        //Now the rest
    
        /*for (var idx=1; idx <= nRolesCount; idx++) {
            var colMemberRoles = document.getElementById("chkRoleMember_" + idx);
            if (colMemberRoles){
                if (colMemberRoles.value > 0 && colMemberRoles.value != nPrimaryRoleID) {
                    sRoleList += colMemberRoles.value + ",";
                }
            }
        }*/
        var nRoles = tblRole.rows.length;
        for (var idx=0; idx < nRoles; idx++) {
          var iRoleID = tblRole.rows[idx].cells[3].innerText;
            var colMemberRoles = document.getElementById("chkRoleMember_" + iRoleID);
            if (colMemberRoles){
                if (colMemberRoles.value > 0 && colMemberRoles.value != nPrimaryRoleID) {
                    sRoleList += colMemberRoles.value + ",";
                }
            }
        }

        //Strip off final comma and close parameter
        sRoleList = sRoleList.substring(0, sRoleList.length - 1);
      }
      
        var aNewProf = new Array();

      // User Profile Overrides
      var sProfileList = "";
      var sProfile = "";
      if (!gbInsertMode){
          // Step through Profile Fields
          var bProfileSelected = false;
          
          if (!gbInsertMode){ //no profile stuff in new user
              var objProfileRows = tblProfile.rows;
              var iProfileCount = objProfileRows.length;
              for (var i = 0; i < iProfileCount; i++){
                if (objProfileRows[i].cells.length > 1){
                  var iProfileID = objProfileRows[i].cells[objProfileRows[i].cells.length - 1].innerText;
                  sProfile = "";
                  var objProfileOverrides = document.getElementById("chkProfileOverridden_" + iProfileID);
                  if (objProfileOverrides){
                      if (objProfileOverrides.value > 0){
                          bProfileSelected = true;
                          var objTxt = document.getElementById("ProfileValue" + iProfileID);
                          var sValue = objTxt.value;
                          if (sValue == "Yes" || sValue == "No")
                            sValue = (sValue == "Yes" ? 1 : 0);
                          sProfile = objProfileOverrides.value + "," + sValue;
                          aNewProf.push(sProfile);
                          sProfileList += sProfile + ","; 
                          sProfileList2 += sProfile + ";";
                      }
                  }
                }
              }
              
              if (bProfileSelected) {
                    //Strip off final comma and close parameter
                    sProfileList = sProfileList.substring(0, sProfileList.length - 1);
              }
          }
      }

      // User Permission Overrides
      var sPermissionList = "";
      var aNewPerm = new Array();
      var sPermission = "";
      if (!gbInsertMode){
            // Step through Permission Fields
            var bPermissionSelected = false;
            if (!gbInsertMode){ //no permission stuff in new user
                var objPermissionRows = tblPermissions.rows;
                var iPermissionCount = objPermissionRows.length;
                for(var i=0; i < iPermissionCount; i++){
                  //alert(objPermissionRows[i].cells.length);
                    if (objPermissionRows[i].cells.length == 1) continue;
                    var iPermissionID = objPermissionRows[i].cells[objPermissionRows[i].cells.length - 1].innerText;
                    if (isNaN(iPermissionID)) continue;
                    sPermission = "";
                    var objPermissionOverrides = document.getElementById("chkPermissionOverridden_" + iPermissionID);
                    if (objPermissionOverrides){
                        if (objPermissionOverrides.value > 0) {
                            bPermissionSelected = true;

                            var objTxt = document.getElementById("PermissionValue" + iPermissionID);
                            var sValue = objTxt.value;
                           
                            if (sValue == "Yes" || sValue == "No")
                              sValue = (sValue=="Yes" ? "0,1,1,0" : "0,0,0,0");
                            else {
                              
                              switch(parseInt(sValue, 10)) {
                                case 0:
                                  sValue = "0,0,0,0";
                                  break;
                                case 8:
                                  sValue = "0,1,0,0";
                                  break;
                                case 12:
                                  sValue = "0,1,1,0";
                                  break;
                                case 24:
                                  sValue = "1,1,0,0";
                                  break;
                                case 28:
                                  sValue = "1,1,1,0";
                                  break;
                                case 30:
                                  sValue = "1,1,1,1";
                                  break;
                                default:
                                  sValue = "0,0,0,0";
                                  break;
                                
                              }
                            }

                            sPermission = objPermissionOverrides.value + "," + sValue + ",";
                            //if (sValue == "Yes" || sValue == "No")
                            //  sValue = (sValue == "Yes" ? 1 : 0);
                                
                            sPermissionList += sPermission;
                            sPermissionList2 += sPermission.substring(0,  sPermission.length - 1) + ";";
                            aNewPerm.push(sPermission);
                        }
                    }
                }    
                if (bPermissionSelected) {
                      //Strip   off final   comma   and close   parameter
                      sPermissionList = sPermissionList.substring(0, sPermissionList.length - 1);
                }
            }
      }
      

      // Work Assignment Pools
      var sAssignmentPoolList = "";
      if (!gbInsertMode){
          var objIdx;
          var nPools = tblAssignmentPool.rows.length;
          for (var idx=0; idx < nPools; idx++) {
            var iPoolID = tblAssignmentPool.rows[idx].cells[3].innerText;
              var colPools = document.getElementById("chkWorkPool_" + iPoolID);
              if (colPools){
                  if (colPools.value > 0) {
                      sAssignmentPoolList += colPools.value.Trim() + ",";
                  }
              }
          }
          //Strip off final comma and close parameter
          sAssignmentPoolList = sAssignmentPoolList.substring(0, sAssignmentPoolList.length - 1);
      }

      //Generate a log Comment if the security items changed.
      var logComment = "";
      var bSecurityItemsChanged = false;
      var sUserID = document.all["UserID"].value;
      if(!gbInsertMode){      
        if (document.all["CsrNo"].value != document.all["__CSRNo"].value){
          //logComment += "* CSRNo was changed from " + document.all["__CSRNo"].value + " to " + document.all["CsrNo"].value + "\n";
          logComment += "User id: " + sUserID + " - CSRNo was changed from " + document.all["__CSRNo"].value + " to " + document.all["CsrNo"].value + "\n";
          bSecurityItemsChanged = true;
        }
        else {
          sUserID += " [" + document.all["__CSRNo"].value + "]";
        }
          
        if (document.all["SupervisorFlag"].value != document.all["__SupervisorFlag"].value){
          //logComment += "* Supervisor Flag was changed from " + document.all["__SupervisorFlag"].value + " to " + document.all["SupervisorFlag"].value + "\n";
          logComment += "User id: " + sUserID + " - Supervisor Flag was changed from " + document.all["__SupervisorFlag"].value + " to " + document.all["SupervisorFlag"].value + "\n";
          bSecurityItemsChanged = true;
        }
              
        //if ((document.all["SupervisorUserID"].value != "") && (document.all["SupervisorUserID"].value != document.all["__SupervisorUserID"].value)) {
        document.all["__SupervisorUserID"].value = (document.all["__SupervisorUserID"].value == "0" ? "" : document.all["__SupervisorUserID"].value);
        if (document.all["SupervisorUserID"].value != document.all["__SupervisorUserID"].value && gsCRUDUserProfile.indexOf("U") != -1) {
          var sFrom="", sTo="", sID="";
          sFromID = document.all["__SupervisorUserID"].value;
          sToID = document.all["SupervisorUserID"].value;
          if (sFromID == 0) sFrom = "-- No Supervisor --";
          if (sToID == 0) sTo = "-- No Supervisor --";
          var iLength = selSupervisorUserID.options.length;
          for (var i = 0; i < iLength; i++){
            if (selSupervisorUserID.options[i].value == sToID){
              sTo = selSupervisorUserID.options[i].innerText;
              if (sFrom != "") break;
            }
            if (selSupervisorUserID.options[i].value == sFromID){
              sFrom = selSupervisorUserID.options[i].innerText;
              if (sTo != "") break;
            }
          }
          //logComment += "* Reports to was changed from " + sFrom + " to " + sTo + "\n";
          logComment += "User id: " + sUserID + " - Reports to was changed from " + sFrom + " to " + sTo + "\n";
          bSecurityItemsChanged = true;
        }
  
        if (document.all["txtActiveInactive"].value != document.all["__UserStatus"].value) {
          //logComment += "* User status was changed from " + document.all["__UserStatus"].value + " to " + document.all["txtActiveInactive"].value + "\n";
          logComment += "User id: " + sUserID + " - User status was changed from " + document.all["__UserStatus"].value + " to " + document.all["txtActiveInactive"].value + "\n";
          bSecurityItemsChanged = true;
        }
  
        if (sRoleList != document.all["__Role"].value) {
          bRolesChanged = true;
          //logComment += "* Role Membership changes:\n";
          var oldRole = document.all["__Role"].value.split(",");
          var newRole = sRoleList.split(",");
          
          var bFound = false;
          var iLength = newRole.length;
          //check for any new additions
          for (var i = 0; i < iLength; i++) {
            bFound = false;
            var jLength = oldRole.length;
            for (var j = 0; j < jLength; j++) {
              if (newRole[i] == oldRole[j]) {
                bFound = true;
                break;
              }
            }
            
            if (!bFound) {
              //logComment += "    * Added Role: " + getRoleMembershipText(newRole[i]) + "\n";
              logComment += "User id: " + sUserID + " - Added Role: " + getRoleMembershipText(newRole[i]) + "\n";
            }
          }
          
          var iLength = oldRole.length;
          //check for any deletions
          for (var i = 0; i < iLength; i++) {
            bFound = false;
            var jLength = newRole.length;
            for (var j = 0; j < jLength; j++) {
              if (oldRole[i] == newRole[j]) {
                bFound = true;
                break;
              }
            }
            
            if (!bFound) {
              //logComment += "    * Removed Role: " + getRoleMembershipText(oldRole[i]) + "\n";
              logComment += "User id: " + sUserID + " - Removed Role: " + getRoleMembershipText(oldRole[i]) + "\n";
            }
          }
          
          //check if primary role changed
          if (oldRole[0] != newRole[0]) {
            //logComment += "    * Primary Role membership changed from : " + getRoleMembershipText(oldRole[0]) + " to " + getRoleMembershipText(newRole[0]) + "\n";
            logComment += "User id: " + sUserID + " - Primary Role membership changed from : " + getRoleMembershipText(oldRole[0]) + " to " + getRoleMembershipText(newRole[0]) + "\n";
          }
          
          bSecurityItemsChanged = true;
        }
        
        //Profile changes
          var aOldProf = document.all["__Profile"].value.split(";");
          var bProfileChanged = false;
          var bFound = false;
          var oldProfID, newProfID;
          var aOld, aNew;
          var sProfComments = "";
          var iLength = aNewProf.length;
          //check for any new overrides
          for (var i = 0; i < iLength; i++){
            bFound = false;
            aNew = aNewProf[i].split(",");
            newProfID = aNew[0];
            var jLength = aOldProf.length;
            for (var j = 0; j < jLength; j++){
              aOld = aOldProf[j].split(",");
              oldProfID = aOld[0];
              if (newProfID == oldProfID) {
                //check if the permissions changed
                if (aNew[1] != aOld[1]) {
                  sProfComments += "User id: " + sUserID + " - Profile Setting for \"" + getProfileText(newProfID) + "\" was changed from: " + aOld[1] +
                                                                                       " to " +
                                                                                       aNew[1] + "\n";
                  bProfileChanged = true;
                }
                bFound = true;
                break;
              }
            }
            if (!bFound){
              sProfComments += "User id: " + sUserID + " - Added Profile Override for \"" + getProfileText(newProfID) + "\" : " + aNew[1] + "\n";
              bProfileChanged = true;
            }
          }
          var iLength = aOldProf.length;
          //check for any deletion of overrides
          for (var i = 0; i < iLength; i++){
            bFound = false;
            aOld = aOldProf[i].split(",");
            oldProfID = aOld[0];
            var jLength = aNewProf.length;
            for (var j = 0; j < jLength; j++){
              aNew = aNewProf[j].split(",");
              newProfID = aNew[0];
              if (oldProfID == newProfID) {
                bFound = true;
                break;
              }
            }
            if (!bFound && oldProfID != ""){
              if (!isNaN(oldProfID)){
                sProfComments += "User id: " + sUserID + " - Defaulted Profile Setting for \"" + getProfileText(oldProfID) + "\"\n";
                bPermissionChanged = true;
              }
            }
          }
        
          if (bProfileChanged) {
            logComment += sProfComments;
            bSecurityItemsChanged = true;
          }           

        //Permission changes
          var aOldPerm = document.all["__Permission"].value.split(";");
          var bPermissionChanged = false;
          var bFound = false;
          var oldPermID, newPermID;
          var aOld, aNew;
          var sPermComments = "";
          var iLength = aNewPerm.length;
          //check for any new overrides
          for (var i = 0; i < iLength; i++){
            bFound = false;
            aNew = aNewPerm[i].split(",");
            newPermID = aNew[0];
            var jLength = aOldPerm.length;
            for (var j = 0; j < jLength; j++){
              aOld = aOldPerm[j].split(",");
              oldPermID = aOld[0];
              if (newPermID == oldPermID) {
                //check if the permissions changed
                if (aNew[1] != aOld[1] ||
                    aNew[2] != aOld[2] ||
                    aNew[3] != aOld[3] ||
                    aNew[4] != aOld[4] ) {
                  sPermComments += "User id: " + sUserID + " - Permission for \"" + getPermissionText(newPermID) + "\" was changed from: " + ((aOld[1] == 1) ? " Create" : "" ) +
                                                                                       ((aOld[2] == 1) ? " Read" : "" ) +
                                                                                       ((aOld[3] == 1) ? " Update" : "" ) +
                                                                                       ((aOld[4] == 1) ? " Delete" : "" ) + " to " +
                                                                                       ((aNew[1] == 1) ? " Create" : "" ) +
                                                                                       ((aNew[2] == 1) ? " Read" : "" ) +
                                                                                       ((aNew[3] == 1) ? " Update" : "" ) +
                                                                                       ((aNew[4] == 1) ? " Delete" : "" ) + 
                                                                                       ((aNew[1] == 0 && aNew[2] == 0 && aNew[3] == 0 && aNew[4] == 0) ? "No Access" : "") + "\n";
                  bPermissionChanged = true;
                }
                bFound = true;
                break;
              }
            }
            if (!bFound){
              sPermComments += "User id: " + sUserID + " - Added Permission Override for \"" + getPermissionText(newPermID) + "\" : " + ((aNew[1] == 1) ? " Create" : "" ) +
                                                                                    ((aNew[2] == 1) ? " Read" : "" ) +
                                                                                    ((aNew[3] == 1) ? " Update" : "" ) +
                                                                                    ((aNew[4] == 1) ? " Delete" : "" ) +  
                                                                                    ((aNew[1] == 0 && aNew[2] == 0 && aNew[3] == 0 && aNew[4] == 0) ? "No Access" : "") + "\n";
              bPermissionChanged = true;
            }
          }
          
          var iLength = aOldPerm.length;
          //check for any deletion of overrides
          for (var i = 0; i < iLength; i++){
            bFound = false;
            aOld = aOldPerm[i].split(",");
            oldPermID = aOld[0];
            var jLength = aNewPerm.length;
            for (var j = 0; j < jLength; j++){
              aNew = aNewPerm[j].split(",");
              newPermID = aNew[0];
              if (oldPermID == newPermID) {
                bFound = true;
                break;
              }
            }
            if (!bFound && oldPermID != ""){
              if (!isNaN(oldPermID)){
                sPermComments += "User id: " + sUserID + " - Defaulted Permission Setting for \"" + getPermissionText(oldPermID) + "\"\n";
                bPermissionChanged = true;
              }
            }
          }
        

        //Work Assignment Pool changes
        if (sAssignmentPoolList != document.all["__Pool"].value) {
          bAssignmentPoolChanged = true;
          var oldPool = document.all["__Pool"].value.split(",");
          var newPool = sAssignmentPoolList.split(",");
          var bFound = false;
          var iLength = newPool.length;
          
          //check for any new additions
          for (var i = 0; i < iLength; i++) {
            bFound = false;
            var jLength = oldPool.length;
            for (var j = 0; j < jLength; j++) {
              if (newPool[i] == oldPool[j]) {
                bFound = true;
                break;
              }
            }
            if (!bFound) {
              logComment += "User id: " + sUserID + " - Added Assignment Pool: " + getWorkAssignmentText(newPool[i]) + "\n";
              bPermissionChanged = true;
            }
          }
          
          //check for any deletions
          var iLength = oldPool.length;
          for (var i = 0; i < iLength; i++) {
            bFound = false;
            var jLength = newPool.length;
            for (var j = 0; j < jLength; j++) {
              if (oldPool[i] == newPool[j]) {
                bFound = true;
                break;
              }
            }
            if (!bFound) {
              logComment += "User id: " + sUserID + " - Removed Assignment Pool: " + getWorkAssignmentText(oldPool[i]) + "\n";
              bPermissionChanged = true;
            }
          }
          
          bSecurityItemsChanged = true;
        }

          if (bPermissionChanged) {
            logComment += sPermComments;
            bSecurityItemsChanged = true;
          }
      }
      else {
        //Insert Mode
        logComment = "Created new user: " + document.all["CsrNo"].value;
        bSecurityItemsChanged = true;
      }

      if (!bSecurityItemsChanged) {
        logComment = "";
      }
	  
	  if(sProfileList == null || sProfileList =="")
	    sProfileList = strUserProfileList;
	  else
	  sProfileList = sProfileList + "," + strUserProfileList;
	  
      if (sAction == "Insert") {
	  sLicenseState="";
	  sInsAssignWork="";
	  sProfileList="";
	  sPermissionList="";
	  }
	  
      sRequest = "UserID=" + document.all["UserID"].value + "&" +
                  "OfficeID=" + "&" + // this should be empty for Lynx Employees
                  "SupervisorUserID=" + (document.all["SupervisorUserID"].value == "0" ? "" : document.all["SupervisorUserID"].value) + "&" +
                  "AssignmentBeginDate=" + gsAssignmentStartDate + "&" +
                  "AssignmentEndDate=" + gsAssignmentEndDate + "&" +
                  "CsrNo=" + escape(document.all["CsrNo"].value) + "&" +
                  "EmailAddress=" + escape(document.all["EmailAddress"].value) + "&" +
                  "FaxAreaCode=" + document.all["FaxAreaCode"].value + "&" +
                  "FaxExchangeNumber=" + document.all["FaxExchangeNumber"].value + "&" +
                  "FaxExtensionNumber=" + document.all["FaxExtensionNumber"].value + "&" +
                  "FaxUnitNumber=" + document.all["FaxUnitNumber"].value + "&" +
                  "NameFirst=" + escape(document.all["NameFirst"].value) + "&" +
                  "NameLast=" + escape(document.all["NameLast"].value) + "&" +
                  "NameTitle=" + escape(document.all["NameTitle"].value) + "&" +
                  "PhoneAreaCode=" + document.all["PhoneAreaCode"].value + "&" +
                  "PhoneExchangeNumber=" + document.all["PhoneExchangeNumber"].value + "&" +
                  "PhoneExtensionNumber=" + document.all["PhoneExtensionNumber"].value + "&" +
                  "PhoneUnitNumber=" + document.all["PhoneUnitNumber"].value + "&" +
                  "SupervisorFlag=" + document.all["SupervisorFlag"].value + "&" +
				  "OperatingMondayStartTime=" + gsMondayStartTime + "&" +
		          "OperatingMondayEndTime="  + gsMondayEndTime +  "&" +
		          "OperatingTuesdayStartTime=" + gsTuesdayStartTime + "&" +
		          "OperatingTuesdayEndTime=" + gsTuesdayEndTime + "&" +
		          "OperatingWednesdayStartTime="  + gsWednesdayStartTime + "&" +
		          "OperatingWednesdayEndTime="  + gsWednesdayEndTime + "&" +
		          "OperatingThursdayStartTime=" + gsThursdayStartTime + "&" +
		          "OperatingThursdayEndTime=" + gsThursdayEndTime + "&" +
		          "OperatingFridayStartTime=" + gsFridayStartTime + "&" +
		          "OperatingFridayEndTime=" + gsFridayEndTime  + "&" +
		          "OperatingSaturdayStartTime="  + gsSaturdayStartTime + "&" +
		          "OperatingSaturdayEndTime=" + gsSaturdayEndTime + "&" +
		          "OperatingSundayStartTime=" + gsSundayStartTime + "&" +
		          "OperatingSundayEndTime=" + gsSundayEndTime + "&" +
                  "LicenseAssignStList=" + sLicenseAssignStList + "&" +
                  "InsAssignWorkList=" + sInsAssignWork + "&" +
                  "RoleList=" + sRoleList + "&" +
                  "ProfileList=" + sProfileList + "&" +
                  "PermissionList=" + sPermissionList + "&" +
                  "AssignmentPoolList=" + sAssignmentPoolList + "&" +
                  "SysLastUserID=" + gsLoginUserID + "&" +
                  "SysLastUpdatedDate=" + content11.LastUpdatedDate;
		
      var sProcName;
      if (sAction == "Insert") {
        var sProcName = "uspAdmUserInsDetail";
      }
      else {
        var sProcName = "uspAdmUserUpdDetail";
      }
      
      //alert(sAction + "\n\n" + sRequest + "\n\n" + logComment);
//      return;
    	// Call database
       
      var oReturn = RSExecute("/rs/UserManagementRS.asp", "RadExecute", sProcName, sRequest, logComment, gsLoginUserID);

      if (oReturn.status == -1) {
        ServerEvent();
        return;
  	  }
  	  else {
        aReturn = oReturn.return_value.split("||");
      	if (aReturn[1] == null || aReturn[1] == 0) {
          //SP returned a message (not an error)
          ServerEvent();
          return;
		    } 
        
        //update the user's last updated date
        var sReturnXML = aReturn[0];
        var sRetUserID;
        if (sReturnXML.substr(0, 6) == "<Root>" ) {
          var iStartIdx = sReturnXML.indexOf("UserID=");
          sRetUserID = sReturnXML.substring( iStartIdx + 8, sReturnXML.indexOf("\"", iStartIdx + 8))

          var iStartIdx = sReturnXML.indexOf("SysLastUpdatedDate=", iStartIdx);
          sRetLastUpdatedDate = sReturnXML.substring( iStartIdx + 20, sReturnXML.indexOf("\"", iStartIdx + 20))

          var oMainDiv = document.getElementById("content11");
          if (oMainDiv)
            oMainDiv.setAttribute("LastUpdatedDate", sRetLastUpdatedDate);
            
          //alert(sRetLastUpdatedDate);
        }
        if (sAction == "Insert") {
          // insert the application record
          sRequest = "UserID=" + sRetUserID + "&" +
                     "ApplicationID=" + sAppID + "&" +
                     "LogonId=" + escape(document.all["CsrNo"].value) + "&" + 
                     "PasswordHint=&" +
                     "SysLastUserID=" + gsLoginUserID;
          var oReturn = RSExecute("/rs/RSADSAction.asp", "RadExecute", "uspAdmUserApplicationInsDetail", sRequest);
    
          if (oReturn.status == -1) {
            ServerEvent();
            return;
      	  }
        }
 
        if (sAction == "Update") {
          // update the application record
          sRequest = "UserID=" + sRetUserID + "&" +
                     "ApplicationID=" + sAppID + "&" +
                     "LogonId=" + escape(document.all["CsrNo"].value) + "&" + 
                     "PasswordHint=&" +
                     "SysLastUserID=" + gsLoginUserID + "&" + 
                     "SysLastUpdatedDate=" + sAppSysLastUpdatedDate;
          var oReturn = null;
          oReturn = RSExecute("/rs/RSADSAction.asp", "RadExecute", "uspAdmUserApplicationUpdDetail", sRequest);
    
          if (oReturn.status == -1) {
            ServerEvent();
            return;
      	  } else {
            aReturn = oReturn.return_value.split("||");
          	if (aReturn[1] == null || aReturn[1] == 0) {
              //SP returned a message (not an error)
              ServerEvent();
              return;
    		    }
          }


          var sReturnXML = aReturn[0];
          if (sReturnXML.substr(0, 6) == "<Root>" ) {
            //alert(1);
            var iStartIdx = sReturnXML.indexOf("SysLastUpdatedDate=");
            sAppSysLastUpdatedDate = sReturnXML.substring( iStartIdx + 20, sReturnXML.indexOf("\"", iStartIdx + 20))
          }
        }

        //reset the default place holders with the save data.
        document.all["__CSRNo"].value = document.all["CsrNo"].value;
        document.all["__SupervisorFlag"].value = document.all["SupervisorFlag"].value;
        document.all["__SupervisorUserID"].value = document.all["SupervisorUserID"].value;
        document.all["__UserStatus"].value = document.all["txtActiveInactive"].value;
        document.all["__Role"].value = sRoleList;
        document.all["__Profile"].value = sProfileList2;
        document.all["__Permission"].value = sPermissionList2;
        document.all["__Pool"].value = sAssignmentPoolList;
        return aReturn;
      }
 	  }
    catch(e) {
      handleJSError("SaveData", e);
    }
  }

  function SetTabVisibility()
  {
    try {
      if (nTotalRoles == 0) {
        var obj = document.getElementById("tabs2");
        obj.style.visibility = "hidden";
        obj = document.getElementById("Content21");
  		  obj.style.visibility = "hidden";
  		  obj = document.getElementById("Content22");
  		  obj.style.visibility = "hidden";
      }
    }
    catch(e) {
      handleJSError("SetTabVisibility", e);
    }
  }
  
  function getRoleMembershipText(sRoleID){
    if (tblRole) {
      var iLength = tblRole.rows.length;
      for (var i = 0; i < iLength; i++){
        if (tblRole.rows[i].cells[3].innerText == sRoleID)
          return tblRole.rows[i].cells[1].innerText;
      }
    }
    return "";
  }
  
  function getPermissionText(sPermissionID){
    if (tblPermissions) {
      var iLength = tblPermissions.rows.length;
      for (var i = 0; i < iLength; i++){
      if (tblPermissions.rows[i].cells.length < 2) continue;
        if (tblPermissions.rows[i].cells[4].innerText == sPermissionID)
          return tblPermissions.rows[i].cells[1].innerText;
      }
    }
    return "";
  }

  function getProfileText(sProfileID){
    if (tblProfile) {
      var iLength = tblProfile.rows.length;
      for (var i = 0; i < iLength; i++){
        if (tblProfile.rows[i].cells.length < 2) continue;
        if (tblProfile.rows[i].cells[4].innerText == sProfileID)
          return tblProfile.rows[i].cells[1].innerText;
      }
    }
    return "";
  }

  function getWorkAssignmentText(sPoolID){
    if (tblAssignmentPool) {
      var iLength = tblAssignmentPool.rows.length;
      for (var i = 0; i < iLength; i++){
        if (tblAssignmentPool.rows[i].cells[3].innerText == sPoolID)
          return tblAssignmentPool.rows[i].cells[0].innerText;
      }
    }
    return "";
  }


  function myDivOnFocusIn(obj){
    if (gbInsertMode == false)
        DivOnFocusIn(content12);
        //DivOnFocusIn(obj);
  }
  function myRadioFunctionality(obj){
  
    if (obj.disabled) return;
    
    var str = obj.id;
    var objIdx;
    
    str = str.substring(0, str.length-3);

    //Reset all Primary Role radios'
    /*for(var idx = 1; idx <= nRolesCount; idx++){
        eval("objIdx = RBgrpPrimaryRole_" + idx + ";");
        if (objIdx != undefined){
            changeImage(objIdx.layer,objIdx.imgNames+0,"radio0");
    		var imgObj1 = document.getElementById(objIdx.imgNames+0);
    		imgObj1.state = 0;
        }
    }*/
    var nRoles = tblRole.rows.length;
    
    for(var idx = 0; idx < nRoles; idx++){
      var iRoleID = tblRole.rows[idx].cells[3].innerText;
        eval("objIdx = RBgrpPrimaryRole_" + iRoleID + ";");
        if (objIdx != undefined){
            changeImage(objIdx.layer,objIdx.imgNames+0,"radio0");
    		var imgObj1 = document.getElementById(objIdx.imgNames+0);
    		imgObj1.state = 0;
        }
    }
    //return;
    if (obj != undefined){
        var objNewSelect;
        objNewSelect = document.getElementsByName(str);
        if (objNewSelect != undefined){
            var idx = str.split("_")[1];
            eval("objIdx = RBgrpPrimaryRole_" + idx + ";");
            if (objIdx != undefined){
                changeImage(objIdx.layer,objIdx.imgNames+0,"radio1");
        		var imgObj1 = document.getElementById(objIdx.imgNames+0);
        		imgObj1.state = 1;

                //save the current selected index
                nCurrentPrimaryIndex = idx;
            }
        }
    }
  }
  
  function focusMyTab(){
    try {
      divStateLicensed.focus();
    } catch (e) {}
  }

    var tabsInited = false;
    function InitTabs(){
        var tmpTabsArray = document.all["tabsInTabs"].childNodes;
        tabArray = new Array();
        var iLength = tmpTabsArray.length;
        for (var i = 0; i < iLength; i++)
            if (tmpTabsArray[i].tagName == "DIV")
                tabArray.push(tmpTabsArray[i]);
            
        for (i = tabArray.length - 1; i > -1; i--){
            tabArray[i].style.top = (2 + tabsVIndend * (tabArray.length - i - 1)) + "px";
            tabArray[i].style.left = (3 + tabsHIndend * (i)) + "px";
            tabArray[i].style.visibility = "visible";
        }
        tabsInited = true;
    }

    function hoverTab(obj){
        if (obj.id != curTabID)
            obj.className = "tabhover1";
    }
    
    function hoverOff(obj){
        if (obj.id != curTabID)
            obj.className = "tab1";
    }


    function resetTabs(){
        var tabs;
        var tabContent = null;
        var iLength = tabArray.length;
        for (var i = 0; i < iLength; i++) {
            tabs = tabArray[i].getElementsByTagName("SPAN");
            var jLength = tabs.length;
            for (var j = 1; j < jLength; j++) {
                tabs[j].className = "tab1";
                tabContent = document.all[tabs[j].frm];
                if (tabContent)
                  tabContent.style.display = "none";
            }
        }
        curTabID = null;
    }    

    function changePage(obj){
        if (!tabsInited) return;
        if (curTabID == obj.id) return;

        resetTabs();
        obj.className = "tabactive1";
        curTabID = obj.id;

        var frm = obj.getAttribute("frm");
        var objFrm = null;
        if (frm.substr(0, 6) == "frame:") {
            objFrm = document.all[frm.substr(6)];
        }
        else
            objFrm = document.all[frm];

        if (objFrm) {
            objFrm.style.display = "inline";
        }
        if (frm.substr(0, 6) == "frame:"){
            var pg = obj.getAttribute("tabPage");
            var frmLoaded = objFrm.getAttribute("frmLoaded");

            //if (curInsuranceID && !frmLoaded) {
                //objFrm.src = pg + "?InsuranceID=" + curInsuranceID;
                //objFrm.frmLoaded = true;
            //}
        }
    }      
    
    function isDirty(){
      return gbDirtyFlag;
    }

    function showHideProfileGroup(GrpCode, tbl){
      if (tbl){
        var bShow = true;
        var sImgSrc = event.srcElement.src;
        if (sImgSrc.indexOf("plus.gif") != -1){
          event.srcElement.src = "/images/minus.gif";
          bShow = true;
        } else {
          event.srcElement.src = "/images/plus.gif";
          bShow = false;
        }
        var tblRows = tbl.rows;
        var tblRowsLen = tblRows.length;
        var sType = "";
        for (var i = 0; i < tblRowsLen; i++){
          if (tblRows[i].businessCode == GrpCode){
            sType = tblRows[i].getAttribute("DataTypeCD");
            //alert(i + ": " + sType);
            if (bShow) {
              tblRows[i].cells[2].firstChild.style.display = "inline";
              tblRows[i].style.display = "inline";
            } else {
              tblRows[i].cells[2].firstChild.style.display = "none";
              tblRows[i].style.display = "none";
            }
          }
        }
      }
    }
    
    function checkProfileOverride(){
      var oTR = this.parentElement.parentElement;
      var sType = oTR.getAttribute("DataTypeCD");
      
      var iProfileID = oTR.lastChild.innerText;
      var sDefault = oTR.lastChild.previousSibling.innerText;
      var sValue = "";
      switch(sType) {
        case "B":
          eval("sValue=RBProfileValue" + iProfileID + ".value");
          break;
        case "S":
          var oSel = document.all["selProfileValue" + iProfileID];
          if (oSel){
            sValue = oSel.options[oSel.selectedIndex].innerText;
          }
          break;
        case "N":
          break;
      }
      if (this.lastChild.value > 0) {
          var objChkDiv = document.all["chkProfileOverriddenDiv"];
          if (objChkDiv && objChkDiv.lastChild.value == 0){
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
      }
      if (this.lastChild.value == 0 && sValue != sDefault){
        if (!bResetAllOverride)
          var sOverride = YesNoMessage("Reset to Default", "Do you want to reset profile settings for '" + this.parentElement.nextSibling.innerText + "' to default?");
        else
          var sOverride = "Yes";
          
  			if (sOverride == "No"){
          CheckBoxChange(this.firstChild);
          return;
        }
        switch(sType) {
          case "B":
            if (sDefault == "Yes")
              eval("RBProfileValue" + iProfileID + ".change(0,'Yes')");
            else
              eval("RBProfileValue" + iProfileID + ".change(1,'No')");
            break;
          case "S":
            var oSel = document.all["selProfileValue" + iProfileID];
            if (oSel)
              oSel.optionselect(sDefault, null);
            break;
          case "N":
            break;
        }
      }
    }
    
    function checkPermissionOverride(){
      var oTR = this.parentElement.parentElement;
      var sType = oTR.getAttribute("DataTypeCD");
      
      var iPermissionID = oTR.lastChild.innerText;
      var sDefault = oTR.lastChild.previousSibling.innerText;
      var sValue = "";
      switch(sType) {
        case "B":
          eval("sValue=RBPermissionValue" + iPermissionID + ".value");
          break;
        case "S":
          var oSel = document.all["selPermissionValue" + iPermissionID];
          if (oSel){
            sValue = oSel.options[oSel.selectedIndex].innerText;
          }
          break;
      }
      if (this.lastChild.value > 0) {
          var objChkDiv = document.all["chkPermissionOverriddenDiv"];
          if (objChkDiv && objChkDiv.lastChild.value == 0){
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
      }
      if (this.lastChild.value == 0 && sValue != sDefault){
        if (!bResetAllOverride)
          var sOverride = YesNoMessage("Reset to Default", "Do you want to reset permission settings for the Entity '" + this.parentElement.nextSibling.innerText + "' to default?");
        else
          var sOverride = "Yes";

  			if (sOverride == "No"){
          CheckBoxChange(this.firstChild);
          return;
        }
        switch(sType) {
          case "B":
            if (sDefault == "Yes")
              eval("RBPermissionValue" + iPermissionID + ".change(0,'Yes')");
            else
              eval("RBPermissionValue" + iPermissionID + ".change(1,'No')");
            break;
          case "S":
            var oSel = document.all["selPermissionValue" + iPermissionID];
            if (oSel)
              oSel.optionselect(sDefault, null);
            break;
        }
      }
    }
    
    function ProfileOverride(){
      try {
        if (this.disabled) return;
        var oTR = this.parentElement.parentElement;
      } catch(e) {
        if (event.srcElement.disabled) return;
        var oTR = event.srcElement.parentElement.parentElement;
      }
      var sType = oTR.getAttribute("DataTypeCD");
      
      var iProfileID = oTR.lastChild.innerText;

      var objChk = document.all["chkProfileOverridden_" + iProfileID];
      if (objChk){
        if(objChk.value == 0){
          var objChkDiv = document.all["chkProfileOverridden_" + iProfileID + "Div"];
          if (objChkDiv){
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
            gbDirtyFlag = true;
          }

          var objChkDiv = document.all["chkProfileOverriddenDiv"];
          if (objChkDiv && objChkDiv.lastChild.value == 0){
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
      }
    }
    
    function PermissionOverride(){
      try {
        if (this.disabled) return;
        var oTR = this.parentElement.parentElement;
      } catch(e) {
        if (event.srcElement.disabled) return;
        var oTR = event.srcElement.parentElement.parentElement;
      }
      var sType = oTR.getAttribute("DataTypeCD");
      
      var iPermissionID = oTR.lastChild.innerText;

      var objChk = document.all["chkPermissionOverridden_" + iPermissionID];
      if (objChk){
        if(objChk.value == 0){
          var objChkDiv = document.all["chkPermissionOverridden_" + iPermissionID + "Div"];
          if (objChkDiv){
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
            gbDirtyFlag = true;
          }
          var objChkDiv = document.all["chkPermissionOverriddenDiv"];
          if (objChkDiv && objChkDiv.lastChild.value == 0){
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
      }
    }
    
    function defaultPermissionOverride(){
      var objChk = document.all["chkPermissionOverridden"];
      if (objChk){
        if(objChk.value == 0){
          var sOverride = YesNoMessage("Reset to Default", "Do you want to reset all permission settings to default?");
    			if (sOverride == "No"){
            CheckBoxChange(this.firstChild);
            return;
          } else {
            bResetAllOverride = true;
            var TRs = tblPermissions.rows;
            var iTRsCount = TRs.length;
            for (var i = 0; i < iTRsCount; i++) {
              if (TRs[i].cells.length > 1) {
                var iPermissionID = TRs[i].lastChild.innerText;
                var objChk1 = document.all["chkPermissionOverridden_" + iPermissionID];
                if (objChk1) {
                  if (objChk1.value > 0) {
                    var obj = document.all["chkPermissionOverridden_" + iPermissionID + "Div"];
                    if (obj)
                      CheckBoxChange(obj.firstChild);
                      CheckBoxBlur(obj.firstChild);
                      obj.fireEvent("onclick");
                  }
                }
              }
            }
            bResetAllOverride = false;
          }
        }
      }
    }

    function defaultProfileOverride(){
      var objChk = document.all["chkProfileOverridden"];
      if (objChk){
        if(objChk.value == 0){
          var sOverride = YesNoMessage("Reset to Default", "Do you want to reset all profile settings to default?");
    			if (sOverride == "No"){
            CheckBoxChange(this.firstChild);
            return;
          } else {
            bResetAllOverride = true;
            var TRs = tblProfile.rows;
            var iTRsCount = TRs.length;
            for (var i = 0; i < iTRsCount; i++) {
              if (TRs[i].cells.length > 1) {
                var iProfileID = TRs[i].lastChild.innerText;
                var objChk1 = document.all["chkProfileOverridden_" + iProfileID];
                if (objChk1) {
                  if (objChk1.value > 0) {
                    var obj = document.all["chkProfileOverridden_" + iProfileID + "Div"];
                    if (obj)
                      CheckBoxChange(obj.firstChild);
                      CheckBoxBlur(obj.firstChild);
                      obj.fireEvent("onclick");
                  }
                }
              }
            }
            bResetAllOverride = false;
          }
        }
      }
    }    
    



	// Validate Work Pool Assignemnts Function
	function ValidatePoolFunction(obj)
	{
    try
    {
      var oDiv = document.getElementById("WorkAssignmentPoolDiv");
      var elms = oDiv.getElementsByTagName("DIV");
      var oFunction = obj.FunctionCD;
      var bDupChkBox = false;
      
      if (obj.lastChild.value == obj.firstChild.trueValue)
      {
        for(var i = 0; i < elms.length; i++)
        {
          if (elms[i] != obj && elms[i].FunctionCD == oFunction && elms[i].firstChild.trueValue == elms[i].lastChild.value)
          {
            CheckBoxChange(obj.firstChild);
            bDupChkBox = true;
          }
        }
      }
      
      if (bDupChkBox == true)
      {
        ClientWarning("Only one pool per function is allowed.");
        return false;
      }
    }
    catch(e)
    {
      handleJSError("ValidatePoolFunction", e)
    }
  }


 /*   function selectAllNoLicenseRequired(){
      var obj = document.all["chkStateNotLicensed"];
      var iNewValue = 0;
      if (obj) {
        iNewValue = obj.value;
        //for (var i = 1; i <= gsStateLicenseRequired.length; i++) { 
        for (var i = gsStateLicenseRequired.length; i > 0 ; i--) {
          var objChkDiv = document.all["chkStateAssignWork_" + i + "Div"];
          var objChk = document.all["chkStateAssignWork_" + i];
          if (objChkDiv && objChk.value != iNewValue && gsStateLicenseRequired[i-1] == 0) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
      }
    } */
        
  /*  function selectAllInsCos(){
      var obj = document.all["chkAllInsurance"];
      var iNewValue = 0;
      if (obj) {
        iNewValue = obj.value;
        for (var i = gsInsAssignWork.length; i > 0 ; i--) {
          var objChkDiv = document.all["chkInsAssignWork_" + i + "Div"];
          var objChk = document.all["chkInsAssignWork_" + i];
          if (objChkDiv && ((objChk.value != iNewValue && gsInsAssignWork[i-1] == 0) || (objChk.value == 0))) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
      }
    } */
    
  /*  function resetAssignedWork(){
      try {
        document.all["cmdResetAssign"].disabled = true;
        //for (var i = 1; i <= gsStateLicenseRequired.length; i++) {
        for (var i = gsStateLicenseRequired.length; i > 0 ; i--) {
          var objChkDiv = document.all["chkStateAssignWork_" + i + "Div"];
          var objChk = document.all["chkStateAssignWork_" + i];
          if (objChkDiv && objChk.value == 1) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
        var objChkDiv = document.all["chkStateNotLicensedDiv"];
        var objChk = document.all["chkStateNotLicensed"];
        if (objChkDiv && objChk.value == 1) {
          CheckBoxChange(objChkDiv.firstChild);
          CheckBoxBlur(objChkDiv.firstChild);
        }
        gsNoLicReqStCount2 = 0;
      } catch (e) {}
      finally {
        document.all["cmdResetAssign"].disabled = false;
      }
    } */

  /*  function resetLicensedState(){
      try {
        document.all["cmdResetLicensed"].disabled = true;
        //for (var i = 1; i <= gsStateLicenseRequired.length; i++) {
        for (var i = gsStateLicenseRequired.length; i > 0 ; i--) {
          var objChkDiv = document.all["chkStateLicensed_" + i + "Div"];
          var objChk = document.all["chkStateLicensed_" + i];
          if (objChkDiv && objChk.value == 1) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
      } catch (e) {}
      finally {
        document.all["cmdResetLicensed"].disabled = false;
      }
    }*/
    
  /*  function stateLicensedClicked(obj) {
      var idx = obj.index;
      if (idx > 0 && idx <= gsStateLicenseRequired.length) {
        if (gsStateLicenseRequired[idx-1] == 0) {
          var objChkDiv = document.all["chkStateLicensed_" + idx + "Div"];
          var objChk = document.all["chkStateLicensed_" + idx];
          if (objChkDiv && objChk.value == 1) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        } else {
          var objChkDiv = document.all["chkStateLicensed_" + idx + "Div"];
          var objChk = document.all["chkStateLicensed_" + idx];
          var objChkDiv2 = document.all["chkStateAssignWork_" + idx + "Div"];
          var objChk2 = document.all["chkStateAssignWork_" + idx];
          if (objChkDiv && objChk.value == 0 && objChk2.value == 1) {
            CheckBoxChange(objChkDiv2.firstChild);
            CheckBoxBlur(objChkDiv2.firstChild);
          }
          if (objChkDiv && objChk.value == 1 && objChk2.value == 0) {
            CheckBoxChange(objChkDiv2.firstChild);
            CheckBoxBlur(objChkDiv2.firstChild);
          }
        }
      }
    } */
    
    /*function resetInsAssignWork(){
      try {
        document.all["cmdResetInsAssignWork"].disabled = true;
                
        for (var i = gsInsCoCount; i > 0 ; i--) {
          var objChkDiv = document.all["chkInsAssignWork_" + i + "Div"];
          var objChk = document.all["chkInsAssignWork_" + i];
          if (objChkDiv && objChk.value == 1) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
      } catch (e) {}
      finally {
        document.all["cmdResetInsAssignWork"].disabled = false;
      }    
    }*/
    
    
   /* function InsAssignWorkClicked(obj){
      var idx = obj.index;
          
      var objChk = document.all["chkInsAssignWork_" + idx];
      if (objChk){ // && gsInsAssignWork[idx-1] == 0) {
        if (objChk.value == 0)
          gsInsAssignWorkCount2--;
        else
          gsInsAssignWorkCount2++;
                  
        if (gsInsAssignWorkCount2 != gsInsAssignWorkCount) {
          var objChkDiv = document.all["chkAllInsuranceDiv"];
          var objChk = document.all["chkAllInsurance"];
          if (objChkDiv && objChk.value == 1) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        } else {
          var objChkDiv = document.all["chkAllInsuranceDiv"];
          var objChk = document.all["chkAllInsurance"];
          if (objChkDiv && objChk.value == 0) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
      }    
    } */
    
 /*   function assignWorkClicked(obj) {
      var idx = obj.index;

      var objChkDiv = document.all["chkStateLicensed_" + idx + "Div"];
      var objChk = document.all["chkStateLicensed_" + idx];
      if (objChkDiv && objChk.value == 0 && gsStateLicenseRequired[idx-1] == 1) {
        ClientWarning("Cannot mark assign work when the user is not licensed for that state and the state requires licensing.");
        var objChk = document.all["chkStateAssignWork_" + idx + "Div"];
        if (objChk) {
          CheckBoxChange(objChk.firstChild);
          CheckBoxBlur(objChk.firstChild);
        }
        return;
      }

      var objChk = document.all["chkStateAssignWork_" + idx];
      if (objChk && gsStateLicenseRequired[idx-1] == 0) {
        if (objChk.value == 0)
          gsNoLicReqStCount2--;
        else
          gsNoLicReqStCount2++;
          
        if (gsNoLicReqStCount2 != gsNoLicReqStCount) {
          var objChkDiv = document.all["chkStateNotLicensedDiv"];
          var objChk = document.all["chkStateNotLicensed"];
          if (objChkDiv && objChk.value == 1) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        } else {
          var objChkDiv = document.all["chkStateNotLicensedDiv"];
          var objChk = document.all["chkStateNotLicensed"];
          if (objChkDiv && objChk.value == 0) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
      }
    } */
    
    ShowSB40();
	
	if (document.attachEvent)
      document.attachEvent("onclick", top.hideAllMenuScriptlets);
    else
      document.onclick = top.hideAllMenuScriptlets;
	  
]]>
</script>
</head>
<BODY unselectable="on" class="bodyAPDSub" onLoad="PageInit();" style="background-color:#FFFFFF;margin:0px;padding:0px;overflow:hidden;height:100%;width:100%" tabIndex="-1"> <!--SetTabVisibility(); -->
    <form name="frmUserDetail" method="post" action="">
        <SPAN id="content11" name="content11" style="position:relative; top: 0px; left: 0px;width:100%;height:230px;padding:0px;margin:0px;">
          <xsl:attribute name="LastUpdatedDate">
            <xsl:value-of select="/Root/User/@SysLastUpdatedDate"/>
          </xsl:attribute>
            <DIV unselectable="on" style="z-index:1;" name="tabsInTabs" id="tabsInTabs">
                <DIV id="tabs1" unselectable="on" class="apdtabs" style="position:relative;">
                    <SPAN id="tab10" class="" unselectable="on" tabPage="" style="width:0px;"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</SPAN>
                    <SPAN id="tab11" class="tab1" unselectable="on" frm="divUsrDetail" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Details</SPAN>
                <!--    <xsl:if test="$InsertMode = 'false'">
                    <SPAN id="tab12" class="tab1" unselectable="on" frm="divUsrDept" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Claim Dept Info</SPAN>
                    </xsl:if> -->
                    <SPAN id="tab13" class="tab1" unselectable="on" frm="divUsrRole" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Role</SPAN>
                    <xsl:if test="$InsertMode = 'false'">
                    <SPAN id="tab14" class="tab1" unselectable="on" frm="divUsrProfile" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Profile Override</SPAN>
                    <SPAN id="tab15" class="tab1" unselectable="on" frm="divUsrPermission" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Permission Override</SPAN>
                    </xsl:if>
                    <xsl:if test="$InsertMode = 'false' and /Root/User/@SupervisorFlag = '1'">
                    <SPAN id="tab16" class="tab1" unselectable="on" frm="divUsrSubordinates" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Subordinates</SPAN>
                    </xsl:if>
                    <xsl:if test="$InsertMode = 'false'">
                    <SPAN id="tab17" class="tab1" unselectable="on" frm="divUsrAssignmentPool" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Work Assignment Pools</SPAN>
                    </xsl:if>
                </DIV>
            </DIV>
            
          <DIV name="divUsrDetail" id="divUsrDetail" style="border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
              <xsl:call-template name="tabGeneral">
                <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
          </DIV>
          <!--<xsl:if test="$InsertMode = 'false'">
          <DIV name="divUsrDept" id="divUsrDept" style="display:none;border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
              <xsl:call-template name="claimDept"></xsl:call-template>
          </DIV>
          </xsl:if> -->
          <DIV name="divUsrRole" id="divUsrRole" style="display:none;border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
              <xsl:call-template name="roles">
                <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
          </DIV>
          <xsl:if test="$InsertMode = 'false'">
          <DIV name="divUsrProfile" id="divUsrProfile" style="display:none;border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
              <xsl:call-template name="profileOverrides">
                <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
          </DIV>
          <DIV name="divUsrPermission" id="divUsrPermission" SRC="/blank.asp" style="display:none;border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
              <xsl:call-template name="permissionOverrides">
                <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
          </DIV>
          <DIV name="divUsrSubordinates" id="divUsrSubordinates" SRC="/blank.asp" style="display:none;border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
              <xsl:call-template name="subordinates"/>
          </DIV>

          <DIV name="divUsrAssignmentPool" id="divUsrAssignmentPool" SRC="/blank.asp" style="display:none;border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
              <xsl:call-template name="AssignmentPool">
                <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
          </DIV>

          </xsl:if>
        </SPAN>
    </form>
    <xsl:call-template name="pageDefaults"></xsl:call-template>
</BODY>
</html>
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
</xsl:template>

<xsl:template name="tabGeneral">
  <xsl:param name="ProfileReadOnly"/>
  <xsl:param name="PermissionReadOnly"/>
  <xsl:variable name="APDAppID"><xsl:value-of select="/Root/Reference[@List='Application' and @Name='APD']/@ReferenceID"/></xsl:variable>

  <!-- Get list of states the user has either a LicensedFlag or AssignWorkFLag set -->
  <!-- and drop it in a hidden input control to be used by the saving routine -->
  <input type="hidden" name="txtUserStates" id="txtUserStates" >
    <xsl:attribute name="value">
      <xsl:for-each select="/Root/User/States[@AssignWorkFlag=1 or @LicenseFlag=1]">
        <xsl:value-of select="@StateCode"/>,<xsl:value-of select="@LicenseFlag"/>,<xsl:value-of select="@AssignWorkFlag"/><xsl:if test="position() != last()">;</xsl:if>
      </xsl:for-each>
    </xsl:attribute>
  </input>

    <table border="0" style="margin:10px;">
      <colgroup>
        <col width="75px;"/>
        <col width="*"/>
      </colgroup>
        <tr>
            <td  valign="bottom">CSR #</td>
            <td >
              <xsl:choose>
                <xsl:when test="string($ProfileReadOnly) = 'true'">
                  <xsl:value-of disable-output-escaping="yes" select="js:InputBox('CsrNo',8,'LogonId',/Root/User/Application[@ApplicationID=$APDAppID],'',5,1)"/>
                  <xsl:text disable-output-escaping="yes">></xsl:text>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:choose>
                    <xsl:when test="$InsertMode='true'">
                      <INPUT id="CsrNo" size="8"  type="text" class="InputField" name="LogonId" tabindex="1" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of disable-output-escaping="yes" select="js:InputBox('CsrNo',8,'LogonId',/Root/User/Application[@ApplicationID=$APDAppID],'',1,1)"/>
                      <xsl:text disable-output-escaping="yes">></xsl:text>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>
              </xsl:choose>
                <xsl:value-of disable-output-escaping="yes" select="js:InputBox('UserID',8,'UserID',/Root,'',6)"/>
            </td>
			
        </tr>
        <tr valign="bottom">
            <td>Name</td>
            <td>
              <table cellspacing="0" border="0" cellpadding="0">
                <tr>
                  <td>
                      <xsl:value-of disable-output-escaping="yes" select="js:InputBox('NameTitle',5,'NameTitle',/Root/User,'',1,2)"/>
                  </td>
                  <td>
                    <img src="/images/spacer.gif" alt="" width="10" height="1" border="0"/>
                    <xsl:value-of disable-output-escaping="yes" select="js:InputBox('NameFirst',20,'NameFirst',/Root/User,'',1,3)"/>
                    <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
                    <xsl:value-of disable-output-escaping="yes" select="js:InputBox('NameLast',20,'NameLast',/Root/User,'',1,4)"/>
                  </td>
                </tr>
              </table>
            </td>
			
			<!-- <TD align="center">Monday</TD>
             <TD> 
	           <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingMondayStartTime',4,'OperatingMondayStartTime',/Root/User,'',1,7)"/>
			    -
	           <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingMondayEndTime',4,'OperatingMondayEndTime',/Root/User,'',1,7)"/>
             </TD> -->
	
        
			
              <!-- role stuff was here -->
        </tr>
        <tr>
            <td  valign="bottom">Phone</td>
            <td>
                <xsl:value-of disable-output-escaping="yes" select="js:InputBox('PhoneAreaCode',2,'PhoneAreaCode',/Root/User,'',10,5)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, PhoneExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
                <xsl:value-of disable-output-escaping="yes" select="js:InputBox('PhoneExchangeNumber',2,'PhoneExchangeNumber',/Root/User,'',10,6)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, PhoneUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
                <xsl:value-of disable-output-escaping="yes" select="js:InputBox('PhoneUnitNumber',2,'PhoneUnitNumber',/Root/User,'',10,7)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, PhoneExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
                <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
                <xsl:value-of disable-output-escaping="yes" select="js:InputBox('PhoneExtensionNumber',4,'PhoneExtensionNumber',/Root/User,'',10,8)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
            </td>
		<!--	<TD align="center">Tuesday</TD>
            <TD>
	          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingTuesdayStartTime',4,'OperatingTuesdayStartTime',/Root/User,'',1,8)"/>
	          - 
	          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingTuesdayEndTime',4,'OperatingTuesdayEndTime',/Root/User,'',1,8)"/>
            </TD> -->
			
        </tr>
        <tr>
            <td valign="bottom">Fax</td>
            <td>
                <xsl:value-of disable-output-escaping="yes" select="js:InputBox('FaxAreaCode',2,'FaxAreaCode',/Root/User,'',10,9)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, FaxExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
                <xsl:value-of disable-output-escaping="yes" select="js:InputBox('FaxExchangeNumber',2,'FaxExchangeNumber',/Root/User,'',10,10)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, FaxUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
                <xsl:value-of disable-output-escaping="yes" select="js:InputBox('FaxUnitNumber',2,'FaxUnitNumber',/Root/User,'',10,11)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, FaxExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
                <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
                <xsl:value-of disable-output-escaping="yes" select="js:InputBox('FaxExtensionNumber',4,'FaxExtensionNumber',/Root/User,'',10,12)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
            </td>
		<!--	 <TD align="center">Wednesday</TD>
	        <TD>
	  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingWednesdayStartTime',4,'OperatingWednesdayStartTime',/Root/User,'',1,9)"/>
                - 
			    <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingWednesdayEndTime',4,'OperatingWednesdayEndTime',/Root/User,'',1,9)"/>
		    </TD> -->
        </tr>
        <tr>
            <td valign="bottom">Email</td>
            <td>
                <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EmailAddress',55,'EmailAddress',/Root/User,'',10,13)"/><xsl:text disable-output-escaping="yes">onbeforedeactivate="checkEMail(this);"></xsl:text>
            </td>
			<!--<TD align="center">Thusday</TD>
            <TD>
	         <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingThursdayStartTime',4,'OperatingThursdayStartTime',/Root/User,'',1,10)"/>
             -
	         <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingThursdayEndTime',4,'OperatingThursdayEndTime',/Root/User,'',1,10)"/>
            </TD> -->
        </tr>
		
        <tr>
          <td colspan="2">
            <table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="bottom">
                  Supervisor
                  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                </td>
                <td  unselectable="on">
                  <xsl:choose>
                    <xsl:when test="$ProfileReadOnly = 'true' or /Root/User/@HasReports = '1'">
                      <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('SupervisorFlag',string(/Root/User/@SupervisorFlag),'1','0','','true',14)"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('SupervisorFlag',string(/Root/User/@SupervisorFlag),'1','0','','',14)"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
                <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
                <td>
                    Reports To 
                    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                </td>
                <td>
                  <xsl:variable name="supervisorID"><xsl:value-of select="/Root/User/@SupervisorUserID"/></xsl:variable>
                      <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('SupervisorUserID',2,'onSelectChange',string($supervisorID),1,5,/Root/Reference[@List='Supervisor'],'Name','ReferenceID',0,90,'','',15)"/>
                      <xsl:if test="boolean($ProfileReadOnly) = true()">
                        <script>
                          selSupervisorUserID.disabled = true;
                        </script>
                      </xsl:if>
                </td>
              </tr>
            </table>
          </td>
		<!--  <TD align="center">Friday</TD>
    	  <TD>
	        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingFridayStartTime',4,'OperatingFridayStartTime',/Root/User,'',1,11)"/>
            - 
	        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingFridayEndTime',4,'OperatingFridayEndTime',/Root/User,'',1,11)"/>
          </TD> -->
        </tr>
        <tr>
            <td >User Status:</td>
            <td>
                <xsl:choose>
                    <xsl:when test="/Root/@UserID = '-1'">
                        <input type="text" name="txtActiveInactive" class="InputReadOnlyField" size="35" tabindex="-1" >
                            <xsl:attribute name="readonly"/>
                        </input>
                        <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
                        <input type="button" name="cmdActivate" value="Activate" class="formbutton" onClick="SetUserStatus()" tabindex="23" readOnly="" disabled=""/>
                    </xsl:when>
                    <xsl:when test="/Root/User/Application[@ApplicationID=$APDAppID]/@AccessEndDate = ''">
                        <input type="text" name="txtActiveInactive" class="InputReadOnlyField" size="35" tabindex="-1" >
                            <xsl:attribute name="value">Active since <xsl:call-template name="formatDateTime"><xsl:with-param name="sDateTime"><xsl:value-of select="/Root/User/Application[@ApplicationID=$APDAppID]/@AccessBeginDate"/></xsl:with-param></xsl:call-template></xsl:attribute>
                            <xsl:attribute name="readonly"/>
                        </input>
                        <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
                        <input type="button" name="cmdActivate" value="Deactivate" class="formbutton" onClick="SetUserStatus()" tabindex="23">
                            <!-- <xsl:if test="/Root/User/@HasReports = 1"> -->
                            <xsl:if test="string($ProfileReadOnly) = 'true' or /Root/User/@HasReports = '1'">
                                <xsl:attribute name="disabled"></xsl:attribute>
                            </xsl:if>
                        </input>
                    </xsl:when>
                    <xsl:otherwise>
                        <input type="text" name="txtActiveInactive" class="InputReadOnlyField" size="35" tabindex="-1" >
                            <xsl:attribute name="value">Inactive since <xsl:call-template name="formatDateTime"><xsl:with-param name="sDateTime"><xsl:value-of select="/Root/User/Application[@ApplicationID=$APDAppID]/@AccessEndDate"/></xsl:with-param></xsl:call-template></xsl:attribute>
                            <xsl:attribute name="readonly"/>
                        </input>
                        <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
                        <input type="button" name="cmdActivate" value="Activate" class="formbutton" onClick="SetUserStatus()" tabindex="23">
                            <!-- <xsl:if test="/Root/User/@HasReports = 1"> -->
                            <xsl:if test="string($ProfileReadOnly) = 'true' or /Root/User/@HasReports = '1'">
                                <xsl:attribute name="disabled"></xsl:attribute>
                            </xsl:if>
                        </input>
                    </xsl:otherwise>
                </xsl:choose>
            </td>
			<!--<TD align="center">Saturday</TD>
            <TD>
	    	  <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingSaturdayStartTime',4,'OperatingSaturdayStartTime',/Root/User,'',1,12)"/>
              -
 	          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingSaturdayEndTime',4,'OperatingSaturdayEndTime',/Root/User,'',1,12)"/>
          </TD> -->
        </tr>
		
		
		<tr>
		       <td>
			   </td>
			   <td>
			   </td> 
		 	<!--  <td align="center">Sunday</td>
			  <td>
                     <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingSundayStartTime',4,'OperatingSundayStartTime',/Root/User,'',1,13)"/>
		              - 
 	                 <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingSundayEndTime',4,'OperatingSundayEndTime',/Root/User,'',1,13)"/>
               </td> -->
         </tr>
        
		
    </table>
	
	
    <SCRIPT>
        <![CDATA[
          if (gsCRUDUserProfile.indexOf("U") == -1) 
            document.all["cmdActivate"].disabled = true;
        ]]>
    </SCRIPT>
</xsl:template>

<!--
<xsl:template name="claimDept">
  <xsl:variable name="APDAppID"><xsl:value-of select="/Root/Reference[@List='Application' and @Name='APD']/@ReferenceID"/></xsl:variable>
  <table border="0" cellspacing="0" cellpadding="0" style="margin:10px;">
    <tr>
      <td>
        <table border="0" cellspacing="3" cellpadding="3">
          <colgroup>
            <col width="125" align="right"/>
            <col width="150"/>
          </colgroup>          
          <tr>
            <td>Stop Assignment:</td>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AssignmentEndDate',35,'AssignmentEndDate',/Root/User,'',4,26)"/>
            </td>
          </tr>
          <tr>
            <td>Restart Assignment:</td>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AssignmentBeginDate',35,'AssignmentBeginDate',/Root/User,'',4,27)"/>
            </td>
          </tr>
          <tr>
            <td>Last Assignment:</td>
            <td>
              <INPUT id='LastAssignmentDate' size='32' type='text' class='InputReadonlyField' name='LastAssignmentDate' tabindex='-1' >
                  <xsl:attribute name="readonly"/>
                  <xsl:attribute name="systemfield"/>
                  <xsl:if test="string-length(/Root/User/@LastAssignmentDate) > 0">
                      <xsl:attribute name="value">
                          <xsl:call-template name="formatDateTime"><xsl:with-param name="sDateTime"><xsl:value-of select="/Root/User/@LastAssignmentDate"/></xsl:with-param></xsl:call-template>
                      </xsl:attribute>
                  </xsl:if>
              </INPUT>
            </td>
          </tr>  
          <tr>
            <td>
              Assign work from all states with no license required:
            </td>
            <td valign="bottom">
              <xsl:variable name="allNotLicensed">
                <xsl:choose>
                  <xsl:when test="count(/Root/User/States[@LicenseRequired=0]) = count(/Root/User/States[@AssignWorkFlag=1 and @LicenseRequired=0])">1</xsl:when>
                  <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
                      
              <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkStateNotLicensed',string($allNotLicensed),'1','0','','',31)"/>
              <script>
                var obj = document.getElementById("chkStateNotLicensedDiv");
                obj.onclick = new Function("", "selectAllNoLicenseRequired()");
              </script>
            </td>
          </tr>
          <tr>
            <td>
              Assign work from all Insurance Companies:
            </td>
            <td valign="bottom">
              
              <xsl:variable name="allInsurance">
                <xsl:choose>
                  <xsl:when test="count(/Root/User/Insurance) = count(/Root/Reference[@List='InsuranceCompany'])">1</xsl:when>
                  <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              
              <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkAllInsurance',string($allInsurance),'1','0','','',32)"/>
              <script>
                var obj = document.getElementById("chkAllINsuranceDiv");
                obj.onclick = new Function("", "selectAllInsCos()");
              </script>
            </td>
          </tr>
        </table>
      </td>
      <td style="width:20px">
      </td>
      <td align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr valign="bottom">
            <td align="left">
              <table border="0" cellspacing="1" cellpadding="2" style="border-collapse:collapse;table-layout:fixed">
                <colgroup>
                  <col width="125"/>
                  <col width="60"/>
                  <col width="60"/>
                  <col width="60"/>
                </colgroup>
                <tr class="QueueHeader" style="height:32px;">
                  <td class="TableSortHeader">State</td>
                  <td class="TableSortHeader">Licensed</td>
                  <td class="TableSortHeader">Assign Work</td>
                  <td class="TableSortHeader">License Required</td>
                </tr>
              </table>
              <DIV id="divStateLicensed" unselectable="on" style="z-index:1; width:327px; height:150px; overflow: auto;">
                <table border="0" cellspacing="1" cellpadding="2" style="border-collapse:collapse;table-layout:fixed;">
                  <colgroup>
                    <col width="125" style="padding:0px;padding-left:10px;"/>
                    <col width="60" style="text-align:center"/>
                    <col width="60" style="text-align:center"/>
                    <col width="60" style="text-align:center"/>
                  </colgroup>

                    <xsl:for-each select="/Root/User/States">
                   
                        <xsl:variable name="readOnly">
                          <xsl:choose>
                            <xsl:when test="@LicenseRequired = 0">true</xsl:when>
                            <xsl:otherwise>false</xsl:otherwise>
                          </xsl:choose>
                        </xsl:variable>
                    <tr style="height:18px;">
                      <td><xsl:value-of select="@StateValue"/></td>
                      <td onclick="focusMyTab()">
                   
                        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkStateLicensed_',string(@LicenseFlag),'1','0',position(), '', 28)"/>
                        <script>
                            var obj = document.getElementById("chkStateLicensed_<xsl:value-of select="position()"/>Div");
                            obj.onclick = new Function("", "stateLicensedClicked(this)");
                            obj.index = <xsl:value-of select="position()"/>;
                        </script>
                      </td>
                      <td onclick="focusMyTab()">
                        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkStateAssignWork_',string(@AssignWorkFlag),'1','0',position(),'',29)"/>
                        <script>
                            var obj = document.getElementById("chkStateAssignWork_<xsl:value-of select="position()"/>Div");
                            obj.onclick = new Function("", "assignWorkClicked(this)");
                            obj.index = <xsl:value-of select="position()"/>;
                        </script>
                      </td>
                      <td>
                        <xsl:choose>
                          <xsl:when test="@LicenseRequired = 1">Yes</xsl:when>
                          <xsl:otherwise>No</xsl:otherwise>
                        </xsl:choose>
                      </td>
                    </tr>
                    </xsl:for-each>
                  </table>
              </DIV>
            </td>
          </tr>
          <tr>
            <td>
              <table>
                <tr><td colspan="4"><img src="/images/spacer.gif" alt="" height="7" border="0"/></td></tr>
                <tr>
                  <td><img src="/images/spacer.gif" alt="" width="128"  height="10" border="0"/></td>
                  <td><Button type="button" name="cmdResetLicensed" class="formbutton" onClick="resetLicensedState()" tabindex="34">Clear</Button></td>
                  <td><img src="/images/spacer.gif" alt="" width="12"  height="10" border="0"/></td>
                  <td><Button type="button" name="cmdResetAssign" class="formbutton" onClick="resetAssignedWork()" tabindex="33">Clear</Button></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      
      <td style="width:30px">
      </td>
      
      <td align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr valign="                     bottom">
            <td align="left">
              <table border="0" cellspacing="1" cellpadding="2" width="215" style="border-collapse:collapse;table-layout:fixed">
                <colgroup>
                  <col/>
                  <col width="60"/>
                </colgroup>
                <tr class="QueueHeader" style="height:32px;">
                  <td class="TableSortHeader">Insurance Company</td>
                  <td class="TableSortHeader">Assign Work</td>
                </tr>
              </table>
              <DIV id="divInsuranceAssign" unselectable="on" style="z-index:1; width:232px; height:150px; overflow: auto;">
                <table border="0" cellspacing="1" cellpadding="2" width="215" style="border-collapse:collapse;table-layout:fixed;">
                  <colgroup>
                    <col style="padding:0px;padding-left:10px;"/>
                    <col width="60" style="text-align:center"/>
                  </colgroup>
                
                  <xsl:for-each select="/Root/Reference[@List = 'InsuranceCompany']">
                    
                    <xsl:variable name="InsuranceCompanyID">
                      <xsl:value-of select="@ReferenceID"/>
                    </xsl:variable>
                    
                    <xsl:variable name="InsAssignWork">
                      <xsl:choose>
                        <xsl:when test="/Root/User/Insurance[@InsuranceCompanyID = $InsuranceCompanyID]">1</xsl:when>
                        <xsl:otherwise>0</xsl:otherwise>
                      </xsl:choose>
                    </xsl:variable>
                      
                  <tr style="height:18px;">
                    <td nowrap="true"><xsl:value-of select="@Name"/></td>
                    <td onclick="focusMyTab()">
                      <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkInsAssignWork_',string($InsAssignWork),'1','0',position(), '', 30)"/>
                      <script>
                        var obj = document.getElementById("chkInsAssignWork_<xsl:value-of select="position()"/>Div");
                        obj.onclick = new Function("", "InsAssignWorkClicked(this)");
                        obj.index = <xsl:value-of select="position()"/>;
                      </script>
                    </td>                      
                  </tr>
                  </xsl:for-each>
                </table>
              </DIV>
            </td>
          </tr>
          <tr>
            <td>
              <table border="0">
                <tr><td colspan="4"><img src="/images/spacer.gif" alt="" height="7" border="0"/></td></tr>
                <tr>
                  <td><img src="/images/spacer.gif" alt="" width="155"  height="10" border="0"/></td>
                  <td>
                    <Button type="button" name="cmdResetInsAssignWork" class="formbutton" onClick="resetInsAssignWork()" tabindex="35">Clear</Button>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          
        </table>
      </td>
    </tr>
  </table>
</xsl:template>-->

<xsl:template name="roles">
  <xsl:param name="ProfileReadOnly"/>
  <xsl:param name="PermissionReadOnly"/>
      <table width="255px" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;margin-left:10px;">
        <tr>
          <td>Role Membership</td>
          <td align="right">Primary</td>
        </tr>
      </table>
  <DIV unselectable="on" style="z-index:1; width:270px; height:190  px; overflow: auto;margin:10px;">
          <!--<script>
              var sSelected = "<xsl:value-of select="/Root/User/Role[@PrimaryRoleFlag='1']/@RoleID"/>";
          </script>-->
      <xsl:variable name="PrimaryRole" select="/Root/User/Role[@PrimaryRoleFlag='1']/@RoleID"/>

      <table width="99%" border="0" height="84" name="tblRole" id="tblRole">
          <xsl:for-each select="/Root/Reference[@List='Role']">
              <xsl:variable name="RoleID"><xsl:value-of select="@ReferenceID"/></xsl:variable>
              <xsl:variable name="selected">
                <xsl:choose>
                  <xsl:when test="/Root/User/Role[@RoleID=$RoleID]">1</xsl:when>
                  <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
          <tr>
              <td align="center">
                <xsl:choose>
                  <xsl:when test="$PermissionReadOnly = 'true'">
                    <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkRoleMember_',string($selected),string(@ReferenceID),'0',string(@ReferenceID), string($ProfileReadOnly), 24)"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkRoleMember_',string($selected),string(@ReferenceID),'0',string(@ReferenceID), '', 24)"/>
                  </xsl:otherwise>
                </xsl:choose>
                  
                  <script>
                      var obj = document.getElementById("chkRoleMember_<xsl:value-of select="@ReferenceID"/>Div");
                      obj.index = <xsl:value-of select="position()-1"/>;
                      obj.businessFunctionCd = "<xsl:value-of select="@BusinessFunctionCD"/>";
                      obj.onclick = new Function("", "ValidateMemberFlag(this)");
                  </script>
              </td>
              <td><xsl:value-of select="@Name"/></td>
              <td>
                  <script>
                  
                    <xsl:choose>
                      <xsl:when test="@ReferenceID = $PrimaryRole">
                          nCurrentPrimaryIndex = "<xsl:value-of select="$PrimaryRole"/>";
                          AddRadio("grpPrimaryRole_<xsl:value-of select="@ReferenceID"/>", 1, " ", " ");
                      </xsl:when>
                      <xsl:otherwise>
                          AddRadio("grpPrimaryRole_<xsl:value-of select="@ReferenceID"/>", 1, " ", "");
                      </xsl:otherwise>
                    </xsl:choose>

                      var objRadio = document.getElementById("grpPrimaryRole_<xsl:value-of select="@ReferenceID"/>Div");
                      objRadio.RoleID = <xsl:value-of select="@ReferenceID"/>;
                      objRadio.onclick = new Function("", "if (ValidatePrimaryFlag(this)) myRadioFunctionality(this);");
                      if (gsCRUDUserPermission.indexOf("U") == -1) 
                        objRadio.disabled = true;
                  </script>
              </td>
              <td style="display:none"><xsl:value-of select="@ReferenceID"/></td>
          </tr>
          </xsl:for-each>
      </table>
  </DIV>
</xsl:template>

<xsl:template name="shopManagement">
      <table width="99%" border="0">
        <tr>
          <td></td>
        </tr>
        <tr>
          <td align="center">This tab has not been implemented yet.</td>
        </tr>
        <tr>
          <td></td>
        </tr>
      </table>
</xsl:template>

<xsl:template name="profileOverrides">
  <xsl:param name="ProfileReadOnly"/>
  <xsl:param name="PermissionReadOnly"/>
  <div style="margin:5px">  
    <table border="0">
          <colgroup>
            <col width="85px"/>
            <col width="200px"/>
            <col width="225px"/>
            <col width="185px"/>
          </colgroup>
        <tr style="font-weight:bold" valign="top">
            <td align="center">Overridden<br/>
              <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkProfileOverridden',boolean(/Root/User/Profile[@OverriddenFlag='1']),'1','0','', $PermissionReadOnly,1,-1)"/>
              <script>
                chkProfileOverriddenDiv.onclick=defaultProfileOverride;
              </script>
            </td>
            <td>Profile Name</td>
            <td>Value</td>
            <td>Default Value</td>
        </tr>
    </table>
    <DIV unselectable="on" style="z-index:1; width:720px; height:195px; overflow: auto;">
        <table border="0" nams="tblProfile" id="tblProfile" style="width:100%;">
          <colgroup>
            <col width="85px"/>
            <col width="200px"/>
            <col width="225px"/>
            <col width="175px"/>
          </colgroup>
            <tr style="height:24px;font-color:#FF0000;">
              <td colspan="4"><img src="/images/minus.gif" style="margin-right:15px;cursor:hand;" onclick="showHideProfileGroup('', tblProfile)"/><strong>Common</strong></td>
            </tr>
            <xsl:for-each select="/Root/User/Profile[@BusinessFunctionCD='']">
              <xsl:sort select="@ProfileName" data-type="text" order="ascending"/>
              <xsl:call-template name="profileRow">
                <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
            </xsl:for-each>

         <!--   <tr style="height:24px;font-color:#FF0000;">
              <td colspan="4"><img src="/images/minus.gif" style="margin-right:15px;cursor:hand;" onclick="showHideProfileGroup('C', tblProfile)"/><strong>Claims Management</strong></td>
            </tr>
            <xsl:for-each select="/Root/User/Profile[@BusinessFunctionCD='C']">
              <xsl:sort select="@ProfileName" data-type="text" order="ascending"/>
              <xsl:call-template name="profileRow">
                <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
            </xsl:for-each> -->

            <tr style="height:24px;font-color:#FF0000;">
              <td colspan="4"><img src="/images/minus.gif" style="margin-right:15px;cursor:hand;" onclick="showHideProfileGroup('S', tblProfile)"/><strong>Shop Management</strong></td>
            </tr>
            <xsl:for-each select="/Root/User/Profile[@BusinessFunctionCD='S']">
              <xsl:sort select="@ProfileName" data-type="text" order="ascending"/>
              <xsl:call-template name="profileRow">
                <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
            </xsl:for-each>
        </table>
    </DIV>
  </div>
</xsl:template>

<xsl:template name="profileRow">
  <xsl:param name="ProfileReadOnly"/>
  <xsl:param name="PermissionReadOnly"/>
            <tr style="height:24px;" valign="middle">
              <xsl:attribute name="businessCode"><xsl:value-of select="@BusinessFunctionCD"/></xsl:attribute>
              <xsl:attribute name="DataTypeCD"><xsl:value-of select="@DataTypeCD"/></xsl:attribute>
                <td style="border-bottom:1px dashed #FFE4B5;" align="center">
                  <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkProfileOverridden_', string(@OverriddenFlag),string(@ProfileID),'0',string(@ProfileID), $ProfileReadOnly,1, 29)"/>
                  <script>
                    chkProfileOverridden_<xsl:value-of select="@ProfileID"/>Div.onclick=checkProfileOverride;
                  </script>
                </td>
                <td style="border-bottom:1px dashed #FFE4B5;"><xsl:value-of select="@ProfileName"/></td>
                <td style="border-bottom:1px dashed #FFE4B5;">
                  <xsl:call-template name="profileItemValue">
                    <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
                    <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
                  </xsl:call-template>
                </td>
                <td style="border-bottom:1px dashed #FFE4B5;">
                  <xsl:choose>
                    <xsl:when test="@DataTypeCD = 'B'">
                     <xsl:choose>
                      <xsl:when test="@DefaultProfileValue=1">Yes</xsl:when>
                      <xsl:otherwise>No</xsl:otherwise>
                     </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@DataTypeCD = 'S'">
                      <xsl:variable name="ProfileID"><xsl:value-of select="@ProfileID"/></xsl:variable>
                      <xsl:variable name="DefaultProfileValue"><xsl:value-of select="@DefaultProfileValue"/></xsl:variable>
                      <xsl:value-of select="/Root/Reference[@List='ProfileSelection' and @ProfileID=$ProfileID and @ReferenceID=$DefaultProfileValue]/@Name"/>
                    </xsl:when>
                    <xsl:when test="@DataTypeCD = 'N'">
                      <xsl:choose>
                        <xsl:when test="@DefaultProfileValue != ''">
                          <xsl:value-of select="@DefaultProfileValue"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:when>
                  </xsl:choose>
                </td>
                <td style="display:none"><xsl:value-of select="@ProfileID"/></td>
            </tr>
</xsl:template>

<xsl:template name="profileItemValue">
  <xsl:param name="ProfileReadOnly"/>
  <xsl:param name="PermissionReadOnly"/>
  <xsl:choose>
    <xsl:when test="@DataTypeCD = 'B'">
      <xsl:variable name="defValue">
       <xsl:choose>
        <xsl:when test="@ProfileValue=1">Yes</xsl:when>
        <xsl:otherwise>No</xsl:otherwise>
       </xsl:choose>
      </xsl:variable>
      <xsl:value-of disable-output-escaping="yes" select="js:AddRadio(concat('ProfileValue', @ProfileID), 0, string($defValue), 'Yes', 'No')"/>
      <script>
        ProfileValue<xsl:value-of select="@ProfileID"/>Div.onclick=ProfileOverride;
        <xsl:if test="boolean($ProfileReadOnly) = true()">
          ProfileValue<xsl:value-of select="@ProfileID"/>Div.disabled=true;
        </xsl:if>
      </script>
    </xsl:when>
    <xsl:when test="@DataTypeCD = 'S'">
      <xsl:variable name="ProfileID" select="@ProfileID"/>
      <xsl:value-of disable-output-escaping="yes" select="js:AddSelect(concat('ProfileValue', @ProfileID),2,'onSelectChange',string(@ProfileValue),1,(last()-position()),/Root/Reference[@List='ProfileSelection' and @ProfileID=$ProfileID],'Name','ReferenceID',0,90,'','',15)"/>
      <script>
        selProfileValue<xsl:value-of select="@ProfileID"/>.onclick=ProfileOverride;
        <xsl:if test="boolean($ProfileReadOnly) = true()">
          selProfileValue<xsl:value-of select="@ProfileID"/>.disabled=true;
        </xsl:if>
      </script>
    </xsl:when>
    <xsl:when test="@DataTypeCD = 'N'">
      <INPUT size='8' type='text' maxlength='15' onchange="ProfileOverride()">
        <xsl:attribute name="name"><xsl:value-of select="concat('ProfileValue', @ProfileID)"/></xsl:attribute>
        <xsl:attribute name="id"><xsl:value-of select="concat('ProfileValue', @ProfileID)"/></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="string(@ProfileValue)"/></xsl:attribute>
        <xsl:choose>
          <xsl:when test="boolean($ProfileReadOnly) = true()">
            <xsl:attribute name="disabled"/>
            <xsl:attribute name="readonly"/>
            <xsl:attribute name="class">InputReadonlyField</xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="class">InputField</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>
      </INPUT>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="permissionOverrides">
  <xsl:param name="ProfileReadOnly"/>
  <xsl:param name="PermissionReadOnly"/>  
  
  <div style="margin:8px">
    <table border="0">
          <colgroup>
            <col width="85px"/>
            <col width="200px"/>
            <col width="225px"/>
            <col width="185px"/>
          </colgroup>
        <tr style="font-weight:bold" valign="top">
            <td align="center">
              Overridden<br/>
              <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkPermissionOverridden',boolean(/Root/User/Permission[@OverriddenFlag='1']),'1','0','', $PermissionReadOnly,1,-1)"/>
              <script>
                chkPermissionOverriddenDiv.onclick=defaultPermissionOverride;
              </script>
            </td>
            <td>Entity</td>
            <td>Value</td>
            <td >Default Value</td>
        </tr>
    </table>
    <DIV unselectable="on" style="z-index:1; width:720px; height:195px; overflow: auto;">
        <table border="0" cellspacing="0" cellpadding="0" name="tblPermissions" id="tblPermissions" style="width:100%;">
          <colgroup>
            <col width="85px"/>
            <col width="200px"/>
            <col width="225px"/>
            <col width="175px"/>
          </colgroup>
            <tr style="height:24px;">
              <td colspan="5">
                <img src="/images/minus.gif" style="margin-right:15px;cursor:hand;" onclick="showHideProfileGroup('', tblPermissions)"/><strong>Common</strong>
              </td>
            </tr>
            <xsl:for-each select="/Root/User/Permission[@BusinessFunctionCD='']">
              <xsl:sort select="@GroupCD" data-type="text" order="ascending"/>
              <xsl:sort select="@Name" data-type="text" order="ascending"/>
              <xsl:call-template name="permissionRow">
                <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
            </xsl:for-each>

            <tr style="height:24px;">
              <td colspan="5"><img src="/images/minus.gif" style="margin-right:15px;cursor:hand;" onclick="showHideProfileGroup('C', tblPermissions)"/><strong>Claims Management</strong></td>
            </tr>
            <xsl:for-each select="/Root/User/Permission[@BusinessFunctionCD='C']">
              <xsl:sort select="@GroupCD" data-type="text" order="ascending"/>
              <xsl:sort select="@Name" data-type="text" order="ascending"/>
              <xsl:call-template name="permissionRow">
                <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
            </xsl:for-each>

            <tr style="height:24px;">
              <td colspan="5"><img src="/images/minus.gif" style="margin-right:15px;cursor:hand;" onclick="showHideProfileGroup('S', tblPermissions)"/><strong>Shop Management</strong></td>
            </tr>
            <xsl:for-each select="/Root/User/Permission[@BusinessFunctionCD='S']">
              <xsl:sort select="@GroupCD" data-type="text" order="ascending"/>
              <xsl:sort select="@Name" data-type="text" order="ascending"/>
              <xsl:call-template name="permissionRow">
                <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
            </xsl:for-each>
        </table>
    </DIV>
  </div>
</xsl:template>

<xsl:template name="permissionRow">
  <xsl:param name="ProfileReadOnly"/>
  <xsl:param name="PermissionReadOnly"/>
                <tr style="height:24px;">
                  <xsl:attribute name="businessCode"><xsl:value-of select="@BusinessFunctionCD"/></xsl:attribute>
                  <xsl:choose>
                    <xsl:when test="@GroupCD = 'A' or @GroupCD = 'R'">
                      <xsl:attribute name="DataTypeCD">B</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="@GroupCD = 'D'">
                      <xsl:attribute name="DataTypeCD">S</xsl:attribute>
                    </xsl:when>
                  </xsl:choose>
                    <td style="border-bottom:1px dashed #FFE4B5;" align="center" >
                      <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkPermissionOverridden_',boolean(@OverriddenFlag=1),string(@PermissionID),'0',string(@PermissionID), $PermissionReadOnly,1, 31)"/>
                      <script>
                        chkPermissionOverridden_<xsl:value-of select="@PermissionID"/>Div.onclick=checkPermissionOverride;
                      </script>
                    </td>
                    <td style="border-bottom:1px dashed #FFE4B5;">
                      <xsl:value-of select="@Name"/>
                    </td>
                    <td style="border-bottom:1px dashed #FFE4B5;">
                      <xsl:call-template name="permissionItemValue">
                        <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
                        <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
                      </xsl:call-template>
                    </td>
                    <td style="border-bottom:1px dashed #FFE4B5;">
                      <xsl:choose>
                        <xsl:when test="@GroupCD = 'A' or @GroupCD = 'R'">
                         <xsl:choose>
                          <xsl:when test="@DefaultReadFlag=1">Yes</xsl:when>
                          <xsl:otherwise>No</xsl:otherwise>
                         </xsl:choose>
                        </xsl:when>
                        <xsl:when test="@GroupCD = 'D'">
                          <xsl:variable name="defaultValue">
                            <xsl:value-of select="number(@DefaultCreateFlag)*16 + number(@DefaultReadFlag)*8 + number(@DefaultUpdateFlag)*4 + number(@DefaultDeleteFlag)*2"/>
                          </xsl:variable>
                          <xsl:choose>
                            <xsl:when test="$defaultValue=0">No Access</xsl:when>
                            <xsl:when test="$defaultValue=8">Read</xsl:when>
                            <xsl:when test="$defaultValue=12">Read, Update</xsl:when>
                            <xsl:when test="$defaultValue=24">Create, Read</xsl:when>
                            <xsl:when test="$defaultValue=28">Create, Read, Update</xsl:when>
                            <xsl:when test="$defaultValue=30">Full Access</xsl:when>
                          </xsl:choose>
                        </xsl:when>
                      </xsl:choose>
                    </td>
                    <td style="display:none"><xsl:value-of select="@PermissionID"/></td>
                </tr>
</xsl:template>

<xsl:template name="permissionItemValue">
  <xsl:param name="ProfileReadOnly"/>
  <xsl:param name="PermissionReadOnly"/>
  <xsl:choose>
    <xsl:when test="@GroupCD = 'A' or @GroupCD = 'R'">
      <xsl:variable name="defValue">
       <xsl:choose>
        <xsl:when test="@ReadFlag=1">Yes</xsl:when>
        <xsl:otherwise>No</xsl:otherwise>
       </xsl:choose>
      </xsl:variable>
      <xsl:value-of disable-output-escaping="yes" select="js:AddRadio(concat('PermissionValue', string(@PermissionID)), 0, string($defValue), 'Yes', 'No')"/>
      <script>
        PermissionValue<xsl:value-of select="@PermissionID"/>Div.onclick=PermissionOverride;
        <xsl:if test="boolean($PermissionReadOnly) = true()">
          PermissionValue<xsl:value-of select="@PermissionID"/>Div.disabled=true;
        </xsl:if>
      </script>
    </xsl:when>
    <xsl:when test="@GroupCD = 'D'">
      <xsl:variable name="value" select="number(@CreateFlag)*16 + number(@ReadFlag)*8 + number(@UpdateFlag)*4 + number(@DeleteFlag)*2"/>
      <xsl:variable name="IDs">0|8|12|24|28|30</xsl:variable>
      <xsl:variable name="Names">No Access|Read|Read, Update|Create, Read|Create, Read, Update|Full Access</xsl:variable>
      <xsl:value-of disable-output-escaping="yes" select="js:AddSelect(concat('PermissionValue', string(@PermissionID)),2,'onSelectChange',string($value),1,number(count(.)-position()),string($Names),'',string($IDs))"/>
      <script>
        selPermissionValue<xsl:value-of select="@PermissionID"/>.onclick=PermissionOverride;
        <xsl:if test="boolean($PermissionReadOnly) = true()">
          selPermissionValue<xsl:value-of select="@PermissionID"/>.disabled=true;
        </xsl:if>
      </script>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="subordinates">
  <div style="margin:10px;">
    Users reporting to <xsl:value-of select="concat(/Root/User/@NameLast, ', ', /Root/User/@NameFirst)"/>:
    <div style="width:250px;height:175px;overflow:auto;margin:5px;padding:3px;">
      <table border="0" cellspacing="0" cellpadding="2">
        <xsl:for-each select="/Root/User/Subordinates">
          <xsl:sort select="@NameLast"/>
          <xsl:sort select="@NameFirst"/>
          <tr>
            <td>
              <xsl:value-of select="@NameLast"/>, <xsl:value-of select="@NameFirst"/>
            </td>
          </tr>
        </xsl:for-each>
      </table>
    </div>
  </div>
</xsl:template>


<xsl:template name="AssignmentPool">
  <xsl:param name="ProfileReadOnly"/>
  <xsl:param name="PermissionReadOnly"/>  
  
  <div style="margin:8px">
    <table border="0">
      <colgroup>
        <col width="190px"/>
        <col width="80px"/>
        <col width="80px"/>
      </colgroup>
      <tr style="font-weight:bold" valign="top">
        <td>Assignment Pool</td>
        <td>Function</td>
        <td></td>
      </tr>
    </table>
    <DIV id="WorkAssignmentPoolDiv" unselectable="on" style="z-index:1; width:300px; height:195px; overflow: auto;">
        <table border="0" cellspacing="0" cellpadding="0" name="tblAssignmentPool" id="tblAssignmentPool" style="width:100%;">
          <colgroup>
            <col width="200px"/>
            <col width="80px"/>
            <col width="20px"/>
          </colgroup>

            <xsl:for-each select="/Root/Reference[@List='AssignmentPool']">
              <tr style="height:24px;">
                  <td style="border-bottom:1px dashed #FFE4B5;">
                    <xsl:value-of select="@Name"/>
                  </td>
                  <td style="border-bottom:1px dashed #FFE4B5;">
                    <!-- <xsl:value-of select="@FunctionCD"/> -->
                    <xsl:choose>
                      <xsl:when test="@FunctionCD = 'OWN'">Owner</xsl:when>
                      <xsl:when test="@FunctionCD = 'ALST'">Analyst</xsl:when>
                      <xsl:when test="@FunctionCD = 'SPRT'">Support</xsl:when>
                    </xsl:choose>
                  </td>
                  <td style="border-bottom:1px dashed #FFE4B5;">
                    <xsl:variable name="AssignmentPoolID"><xsl:value-of select="@ReferenceID"/></xsl:variable>
                    <xsl:variable name="selected">
                      <xsl:choose>
                        <xsl:when test="/Root/User/AssignmentPool[@AssignmentPoolID=$AssignmentPoolID]">1</xsl:when>
                        <xsl:otherwise>0</xsl:otherwise>
                      </xsl:choose>
                    </xsl:variable>
                    <xsl:choose>
                      <xsl:when test="$PermissionReadOnly = 'true'">
                        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkWorkPool_',string($selected),string(@ReferenceID),'0',string(@ReferenceID), string($ProfileReadOnly), 24)"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkWorkPool_',string($selected),string(@ReferenceID),'0',string(@ReferenceID), '', 24)"/>
                      </xsl:otherwise>
                    </xsl:choose>
                    <script>
                      var obj = document.getElementById("chkWorkPool_<xsl:value-of select="@ReferenceID"/>Div");
                      obj.index = <xsl:value-of select="position()-1"/>;
                      obj.FunctionCD = "<xsl:value-of select="@FunctionCD"/>";
                      <!-- obj.onclick = new Function("", "ValidatePoolFunction(this)"); -->
                    </script>
                  </td>
              <td style="display:none"><xsl:value-of select="@ReferenceID"/></td>
              </tr>
            </xsl:for-each>

        </table>
    </DIV>
  </div>
</xsl:template>


<xsl:template name="pageDefaults">
  <xsl:variable name="APDAppID"><xsl:value-of select="/Root/Reference[@List='Application' and @Name='APD']/@ReferenceID"/></xsl:variable>
  <div style="display:none">
    <input type="hidden" name="__CSRNo" id="__CSRNo" >
      <xsl:attribute name="value"><xsl:value-of select="/Root/User/Application[@ApplicationID=$APDAppID]/@LogonId"/></xsl:attribute>
    </input>
    <input type="hidden" name="__SupervisorFlag" id="__SupervisorFlag" >
      <xsl:attribute name="value"><xsl:value-of select="/Root/User/@SupervisorFlag"/></xsl:attribute>
    </input>
    <input type="hidden" name="__SupervisorUserID" id="__SupervisorUserID" >
      <xsl:attribute name="value"><xsl:value-of select="/Root/User/@SupervisorUserID"/></xsl:attribute>
    </input>
    <input type="hidden" name="__UserStatus" id="__UserStatus" >
      <xsl:choose>
        <xsl:when test="/Root/User/Application[@ApplicationID=$APDAppID]/@AccessEndDate = ''">
          <xsl:attribute name="value">Active since <xsl:call-template name="formatDateTime"><xsl:with-param name="sDateTime"><xsl:value-of select="/Root/User/Application[@ApplicationID=$APDAppID]/@AccessBeginDate"/></xsl:with-param></xsl:call-template></xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="value">Inactive since <xsl:call-template name="formatDateTime"><xsl:with-param name="sDateTime"><xsl:value-of select="/Root/User/Application[@ApplicationID=$APDAppID]/@AccessEndDate"/></xsl:with-param></xsl:call-template></xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
    </input>
    <input type="hidden" name="__Role" id="__Role" >
      <xsl:attribute name="value">
        <xsl:value-of select="/Root/User/Role[@PrimaryRoleFlag=1]/@RoleID"/>
        <xsl:for-each select="/Root/User/Role[@PrimaryRoleFlag!=1]">
          <xsl:text>,</xsl:text><xsl:value-of select="@RoleID"/>
        </xsl:for-each>
      </xsl:attribute>
    </input>
    <input type="hidden" name="__Profile" id="__Profile" >
      <xsl:attribute name="value">
        <xsl:for-each select="/Root/User/Profile[@OverriddenFlag=1]"><xsl:value-of select="@ProfileID"/>,<xsl:value-of select="@ProfileValue"/>;</xsl:for-each>
      </xsl:attribute>
    </input>
    <input type="hidden" name="__Permission" id="__Permission" >
      <xsl:attribute name="value">
        <xsl:for-each select="/Root/User/Permission[@OverriddenFlag=1]"><xsl:value-of select="@PermissionID"/>,<xsl:value-of select="@CreateFlag"/>,<xsl:value-of select="@ReadFlag"/>,<xsl:value-of select="@UpdateFlag"/>,<xsl:value-of select="@DeleteFlag"/>;</xsl:for-each>
      </xsl:attribute>
    </input>
    <input type="hidden" name="__Pool" id="__Pool" >
      <xsl:attribute name="value">
        <xsl:for-each select="/Root/User/AssignmentPool">
          <xsl:value-of select="@AssignmentPoolID"/>
          <xsl:if test="position() != last()">,</xsl:if>
        </xsl:for-each>
      </xsl:attribute>
    </input>
  </div>
</xsl:template>



<xsl:template name="formatDateTime">
    <xsl:param name="sDateTime"/>

    <xsl:variable name="sYear" select="substring($sDateTime, 1,4)"/>
    <xsl:variable name="sMonth" select="substring($sDateTime, 6,2)"/>
    <xsl:variable name="sDay" select="substring($sDateTime, 9,2)"/>
    
    <xsl:value-of select="concat($sMonth, '/', $sDay, '/', $sYear)"/>
    
    <xsl:variable name="sHH1" select="substring($sDateTime, 12,2)"/>
    <xsl:if test="number($sHH1) &gt; 12">
        <xsl:variable name="sHH" select="format-number(number($sHH1) - 12, '00')"/>
        <xsl:variable name="sAMPM" select="PM"/>
        <xsl:variable name="sMM" select="substring($sDateTime, 15,2)"/>
        <xsl:variable name="sSS" select="substring($sDateTime, 18,2)"/>
        <xsl:value-of select="concat(' ', $sHH, ':', $sMM, ':', $sSS, ' PM')"/>
    </xsl:if>
    <xsl:if test="number($sHH1) &lt; 13">
        <xsl:variable name="sHH" select="$sHH1"/>
        <xsl:variable name="sMM" select="substring($sDateTime, 15,2)"/>
        <xsl:variable name="sSS" select="substring($sDateTime, 18,2)"/>
        <xsl:value-of select="concat(' ', $sHH, ':', $sMM, ':', $sSS, ' AM')"/>
    </xsl:if>
</xsl:template>

</xsl:stylesheet>