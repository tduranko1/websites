<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="RoleDetail">

<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:decimal-format NaN="0"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="UserPermissionCRUD" select="User Permission"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="InsertMode">false</xsl:param>
<xsl:param name="LoginUserID"/>

<xsl:template match="/Root">
    <xsl:choose>
        <xsl:when test="substring($UserPermissionCRUD, 3, 1) = 'U'">
            <xsl:call-template name="mainHTML">
              <xsl:with-param name="UserPermissionCRUD" select="$UserPermissionCRUD"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <html>
          <head>
              <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
              <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
          </head>
          <body class="bodyAPDSub" unselectable="on" style="background-color:#FFFFFF">
            <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
              <tr>
                <td align="center">
                  <font color="#ff0000"><strong>You do not have sufficient permission to Edit Role Detail.
                  <br/>Please contact your supervisor.</strong></font>
                </td>
              </tr>
            </table>
          </body>
          </html>
        </xsl:otherwise>
    </xsl:choose>

</xsl:template>

<xsl:template name="mainHTML">
  <xsl:param name="UserPermissionCRUD"/>
  <!-- <xsl:variable name="PermissionReadOnly" select="contains($UserPermissionCRUD, 'U')"/> -->
  <xsl:variable name="PermissionReadOnly" select="boolean(substring($UserPermissionCRUD, 3, 1) != 'U')"/>
<html>
<title>Role Information</title>
<head>
<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/CoolSelect.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<!-- Script modules -->
<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formutilities.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/utilities.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>


<script language="JavaScript">
	  var gsLoginUserID = <xsl:value-of select="$LoginUserID"/>;
    var gbProfileCount = <xsl:value-of select="count(/Root/Role/Profile)"/>;
    var gbPermissionCount = <xsl:value-of select="count(/Root/Role/Permission)"/>;
    var gbPermissionCRUD = '<xsl:value-of select="$UserPermissionCRUD"/>';
    var bResetAllOverride = false;

<![CDATA[

	// button images for ADD/DELETE/SAVE
	preload('buttonAdd','/images/but_ADD_norm.png');
	preload('buttonAddDown','/images/but_ADD_down.png');
	preload('buttonAddOver','/images/but_ADD_over.png');
	preload('buttonDel','/images/but_DEL_norm.png');
	preload('buttonDelDown','/images/but_DEL_down.png');
	preload('buttonDelOver','/images/but_DEL_over.png');
	preload('buttonSave','/images/but_SAVE_norm.png');
	preload('buttonSaveDown','/images/but_SAVE_down.png');
	preload('buttonSaveOver','/images/but_SAVE_over.png');

  var currentTabRow = 1;
  var curTabID = "";
  var tabArray = null;
  var tabsHIndend = 10;
  var tabsVIndend = 18;


	// Page Initialize
	function PageInit()
	{
    ShowSB80();
    InitTabs();
    initSelectBoxes();
    InitFields();
    changePage(tab11);
    ShowSB100();
	}

  function handleJSError(sFunction, e)
  {
    var result = "An error has occurred on the page.\nFunction = " + sFunction + "\n";
    for (var i in e) {
      result += "e." + i + " = " + e[i] + "\n";
    }
    ClientError( result );
  }

	var inADS_Pressed = false;		// re-entry flagging for buttons pressed
  var objDivWithButtons = null;
	// Handle click of Add or Save button
	function ADS_Pressed(sAction, sName)
	{
    try {
      if (sAction == "Update" && !gbDirtyFlag) return; //no change made to the page
  		if (inADS_Pressed == true) return;
  		inADS_Pressed = true;
      objDivWithButtons = content11.parentElement;
      disableADS(objDivWithButtons, true);
  		var oDiv = document.getElementById("content11");

  		if (sAction == "Update") {

				setTimeout(ShowSB40,1);
                var aRetValue = SaveData(sAction)
				setTimeout(ShowSB80,2);
        // Process any return value.  If aRetValue[1] = 1, aRetValue[0] contains the recordset formatted as a comma-delimited
        // string, if aRetValue[1] = 0 , aRetValue[0] contains an error message.
        if (aRetValue != null) {
          if (aRetValue[1] != 0)  {
            // Split the elements of the recordset. The 2nd value should be the SysLastUpdatedDate.  Get it and update the page.
            //var aReturnRS = aRetValue[0].split(",");
            var sReturnXML = aRetValue[0];
            var sRetRoleID, sRetLastUpdatedDate;
            if (sReturnXML.substr(0, 6) == "<Root>" ) {
              var iStartIdx = sReturnXML.indexOf("RoleID=");
              sRetRoleID = sReturnXML.substring( iStartIdx + 8, sReturnXML.indexOf("\"", iStartIdx + 8))
              
              iStartIdx = sReturnXML.indexOf("SysLastUpdatedDate=", iStartIdx);
              sRetLastUpdatedDate = sReturnXML.substring( iStartIdx + 20, sReturnXML.indexOf("\"", iStartIdx + 20))

            }

            //alert(sRetLastUpdatedDate);
            oDiv.setAttribute("LastUpdatedDate", sRetLastUpdatedDate);

    			gbDirtyFlag = false;
    			if (typeof(parent.gbDirtyFlag)=='boolean')
    				parent.gbDirtyFlag = false;

    			resetControlBorders(document.getElementById("content11"));
          }
          else {
            // If this code executes, most likely an error was raised in the middle tier or the database
            ServerEvent();
          }
        }
      }
      else {
        var userError = new Object();
        userError.message = "Unhandled ADS action: Action = " + sAction;
        userError.name = "MiscellaneousException";
        throw userError;
			}

  		inADS_Pressed = false;
      disableADS(objDivWithButtons, false);
      objDivWithButtons = null;
         setTimeout(ShowSB100,300);
    }
    catch(e) {
      handleJSError("ADS_Pressed", e);
    }
	}

    function SaveData(sAction)
    {
    try {
      // Field Edit Checks
      if (GetValueFromElement("RoleName") == "") {
        ClientWarning( "Role Name is Required." );
        return;
      }

      // Begin Save
       //var sRequest = GetRequestStringFromHTMLForm("frmRoleDetail", false);
       //sRequest += "&RoleName=" + document.getElementById("RoleName").value;

  		// User Profile Overrides
  		{
    		// Step through Profile Fields
  			var bProfileSelected = false;
        var sProfileList = "";
        var sPermissionList = "";
        var sProfile = "";
        var sProfileList2 = "";
        var sPermissionList2 = "";

        var aNewProf = new Array();

            if (!gbInsertMode){ //no profile stuff in new user
                var objProfileRows = tblProfile.rows;
                var iProfileCount = objProfileRows.length;
                for (var i = 0; i < iProfileCount; i++){
                  if (objProfileRows[i].cells.length > 1){
                    var iProfileID = objProfileRows[i].cells[objProfileRows[i].cells.length - 1].innerText;
                    sProfile = "";
                    var objProfileOverrides = document.getElementById("chkProfileOverridden_" + iProfileID);
                    if (objProfileOverrides){
                        if (objProfileOverrides.value > 0){
                            bProfileSelected = true;
                            var objTxt = document.getElementById("ProfileValue" + iProfileID);
                            var sValue = objTxt.value;
                            if (sValue == "Yes" || sValue == "No")
                              sValue = (sValue == "Yes" ? 1 : 0);
                            sProfile = objProfileOverrides.value + "," + sValue;
                            aNewProf.push(sProfile);
                            sProfileList += sProfile + ",";
                            sProfileList2 += sProfile + ";";
                        }
                    }
                  }
                }

                if (bProfileSelected) {
          			  //Strip off final comma and close parameter
          			  sProfileList = sProfileList.substring(0, sProfileList.length - 1);
          		}
            }
  		}

      // User Permission Overrides
  		{
    		// Step through Permission Fields
  			var bPermissionSelected = false;

            var aNewPerm = new Array();
            var sPermission = "";

            if (!gbInsertMode){ //no permission stuff in new user
                var objPermissionRows = tblPermissions.rows;
                var iPermissionCount = objPermissionRows.length;
                for(var i=0; i < iPermissionCount; i++){
                  //alert(objPermissionRows[i].cells.length);
                    if (objPermissionRows[i].cells.length == 1) continue;
                    var iPermissionID = objPermissionRows[i].cells[objPermissionRows[i].cells.length - 1].innerText;
                    if (isNaN(iPermissionID)) continue;
                    sPermission = "";
                    var objPermissionOverrides = document.getElementById("chkPermissionOverridden_" + iPermissionID);
                    if (objPermissionOverrides){
                        if (objPermissionOverrides.value > 0) {
                            bPermissionSelected = true;

                            var objTxt = document.getElementById("PermissionValue" + iPermissionID);
                            var sValue = objTxt.value;

                            if (sValue == "Yes" || sValue == "No")
                              sValue = (sValue=="Yes" ? "0,1,1,0" : "0,0,0,0");
                            else {

                              switch(parseInt(sValue, 10)) {
                                case 0:
                                  sValue = "0,0,0,0";
                                  break;
                                case 8:
                                  sValue = "0,1,0,0";
                                  break;
                                case 12:
                                  sValue = "0,1,1,0";
                                  break;
                                case 24:
                                  sValue = "1,1,0,0";
                                  break;
                                case 28:
                                  sValue = "1,1,1,0";
                                  break;
                                case 30:
                                  sValue = "1,1,1,1";
                                  break;
                                default:
                                  sValue = "0,0,0,0";
                                  break;

                              }
                            }

                            sPermission = objPermissionOverrides.value + "," + sValue + ",";
                            //if (sValue == "Yes" || sValue == "No")
                            //  sValue = (sValue == "Yes" ? 1 : 0);

                            sPermissionList += sPermission;
                            sPermissionList2 += sPermission.substring(0,  sPermission.length - 1) + ";";
                            aNewPerm.push(sPermission);
                        }
                    }
                }

                if (bPermissionSelected) {
          			  //Strip	off	final	comma	and	close	parameter
          			  sPermissionList = sPermissionList.substring(0,	sPermissionList.length	-	1);
                }
            }
      }

      var logComment = "";
      var bSecurityItemsChanged = false;
      var sRoleID = document.all["RoleID"].value;

        if (document.all["RoleName"].value != document.all["__RoleName"].value){
          logComment += "Role id: " + sRoleID + " - Role Name was changed from " + document.all["__RoleName"].value + " to " + document.all["RoleName"].value + "\n";
          bSecurityItemsChanged = true;
        }
        else
          sRoleID += " [" + document.all["__RoleName"].value + "] ";


        //Profile changes
          var aOldProf = document.all["__Profile"].value.split(";");
          var bProfileChanged = false;
          var bFound = false;
          var oldProfID, newProfID;
          var aOld, aNew;
          var sProfComments = "";
          var iLength = aNewProf.length;
          //check for any new overrides
          for (var i = 0; i < iLength; i++){
            bFound = false;
            aNew = aNewProf[i].split(",");
            newProfID = aNew[0];
            var jLength = aOldProf.length;
            for (var j = 0; j < jLength; j++){
              aOld = aOldProf[j].split(",");
              oldProfID = aOld[0];
              if (newProfID == oldProfID) {
                //check if the permissions changed
                if (aNew[1] != aOld[1]) {
                  sProfComments += "Role id: " + sRoleID + " - Profile Setting for \"" + getProfileText(newProfID) + "\" was changed from: " + aOld[1] +
                                                                                       " to " +
                                                                                       aNew[1] + "\n";
                  bProfileChanged = true;
                }
                bFound = true;
                break;
              }
            }
            if (!bFound){
              sProfComments += "Role id: " + sRoleID + " - Added Profile Override for \"" + getProfileText(newProfID) + "\" : " + aNew[1] + "\n";
              bProfileChanged = true;
            }
          }

          //check for any deletion of overrides
          var iLength = aOldProf.length;
          for (var i = 0; i < iLength; i++){
            bFound = false;
            aOld = aOldProf[i].split(",");
            oldProfID = aOld[0];
            var jLength = aNewProf.length;
            for (var j = 0; j < aNewProf.length; j++){
              aNew = aNewProf[j].split(",");
              newProfID = aNew[0];
              if (oldProfID == newProfID) {
                bFound = true;
                break;
              }
            }
            if (!bFound && oldProfID != ""){
              if (!isNaN(oldProfID)){
                sProfComments += "Role id: " + sRoleID + " - Defaulted Profile Setting for \"" + getProfileText(oldProfID) + "\"\n";
                bPermissionChanged = true;
              }
            }
          }

          if (bProfileChanged) {
            logComment += sProfComments;
            bSecurityItemsChanged = true;
          }

        //Permission changes
          var aOldPerm = document.all["__Permission"].value.split(";");
          var bPermissionChanged = false;
          var bFound = false;
          var oldPermID, newPermID;
          var aOld, aNew;
          var sPermComments = "";
          var iLength = aNewPerm.length;
          //check for any new overrides
          for (var i = 0; i < iLength; i++){
            bFound = false;
            aNew = aNewPerm[i].split(",");
            newPermID = aNew[0];
            var jLength = aOldPerm.length;
            for (var j = 0; j < jLength; j++){
              aOld = aOldPerm[j].split(",");
              oldPermID = aOld[0];
              if (newPermID == oldPermID) {
                //check if the permissions changed
                if (aNew[1] != aOld[1] ||
                    aNew[2] != aOld[2] ||
                    aNew[3] != aOld[3] ||
                    aNew[4] != aOld[4] ) {
                  sPermComments += "Role id: " + sRoleID + " - Permission for \"" + getPermissionText(newPermID) + "\" was changed from: " + ((aOld[1] == 1) ? " Create" : "" ) +
                                                                                       ((aOld[2] == 1) ? " Read" : "" ) +
                                                                                       ((aOld[3] == 1) ? " Update" : "" ) +
                                                                                       ((aOld[4] == 1) ? " Delete" : "" ) + " to " +
                                                                                       ((aNew[1] == 1) ? " Create" : "" ) +
                                                                                       ((aNew[2] == 1) ? " Read" : "" ) +
                                                                                       ((aNew[3] == 1) ? " Update" : "" ) +
                                                                                       ((aNew[4] == 1) ? " Delete" : "" ) +
                                                                                       ((aNew[1] == 0 && aNew[2] == 0 && aNew[3] == 0 && aNew[4] == 0) ? "No Access" : "") + "\n";
                  bPermissionChanged = true;
                }
                bFound = true;
                break;
              }
            }
            if (!bFound){
              sPermComments += "Role id: " + sRoleID + " - Added Permission Override for \"" + getPermissionText(newPermID) + "\" : " + ((aNew[1] == 1) ? " Create" : "" ) +
                                                                                    ((aNew[2] == 1) ? " Read" : "" ) +
                                                                                    ((aNew[3] == 1) ? " Update" : "" ) +
                                                                                    ((aNew[4] == 1) ? " Delete" : "" ) +
                                                                                    ((aNew[1] == 0 && aNew[2] == 0 && aNew[3] == 0 && aNew[4] == 0) ? "No Access" : "") + "\n";
              bPermissionChanged = true;
            }
          }

          var iLength = aOldPerm.length;
          //check for any deletion of overrides
          for (var i = 0; i < iLength; i++){
            bFound = false;
            aOld = aOldPerm[i].split(",");
            oldPermID = aOld[0];
            var jLength = aNewPerm.length;
            for (var j = 0; j < jLength; j++){
              aNew = aNewPerm[j].split(",");
              newPermID = aNew[0];
              if (oldPermID == newPermID) {
                bFound = true;
                break;
              }
            }
            if (!bFound && oldPermID != ""){
              if (!isNaN(oldPermID)){
                sPermComments += "Role id: " + sRoleID + " - Defaulted Permission Setting for \"" + getPermissionText(oldPermID) + "\"\n";
                bPermissionChanged = true;
              }
            }
          }

          if (bPermissionChanged) {
            logComment += sPermComments;
            bSecurityItemsChanged = true;
          }

      if (!bSecurityItemsChanged)
        logComment = "";

      var sProcName = "uspAdmRoleUpdDetail";


      sRequest = "RoleID=" + document.all["RoleID"].value + "&" +
                  "RoleName=" + escape(document.all["RoleName"].value) + "&" +
                  "ProfileList=" + sProfileList + "&" +
                  "PermissionList=" + sPermissionList + "&" +
                  "SysLastUserID=" + gsLoginUserID + "&" +
                  "SysLastUpdatedDate=" + content11.LastUpdatedDate;

      //alert(sProcName + "\n\n" + sRequest );//+ "\n\n" + logComment
      //return;

        // Call database
      var oReturn = RSExecute("/rs/UserManagementRS.asp", "RadExecute", sProcName, sRequest, logComment, gsLoginUserID);

      if (oReturn.status == -1) {
        ServerEvent();
      }
      else {
          aReturn = oReturn.return_value.split("||");
        if (aReturn[1] == null || aReturn[1] == 0) {
          // Set error to be returned to the caller
                aReturn[1] = 0;
        }

        //reset the default place holders with the saved info
        document.all["__RoleName"].value = document.all["RoleName"].value;
        document.all["__Profile"].value = sProfileList2;
        document.all["__Permission"].value = sPermissionList2;

        return aReturn;
      }
      }
    catch(e) {
      handleJSError("SaveData", e);
    }
  }

  function getPermissionText(sPermissionID){
    if (tblPermissions) {
      var iLength = tblPermissions.rows.length;
      for (var i = 0; i < iLength; i++){
        if (tblPermissions.rows[i].cells.length < 2) continue;
        if (tblPermissions.rows[i].cells[4].innerText == sPermissionID)
          return tblPermissions.rows[i].cells[1].innerText;
      }
    }
    return "";
  }

  function getProfileText(sProfileID){
    if (tblProfile) {
      var iLength = tblProfile.rows.length;
      for (var i = 0; i < iLength; i++){
        if (tblProfile.rows[i].cells.length < 2) continue;
        if (tblProfile.rows[i].cells[4].innerText == sProfileID)
          return tblProfile.rows[i].cells[1].innerText;
      }
    }
    return "";
  }

    var tabsInited = false;
    function InitTabs(){
        var tmpTabsArray = document.all["tabsInTabs"].childNodes;
        tabArray = new Array();
        var iLength = tmpTabsArray.length;
        for (var i = 0; i < iLength; i++)
            if (tmpTabsArray[i].tagName == "DIV")
                tabArray.push(tmpTabsArray[i]);

        for (i = tabArray.length - 1; i > -1; i--){
            tabArray[i].style.top = (2 + tabsVIndend * (tabArray.length - i - 1)) + "px";
            tabArray[i].style.left = (3 + tabsHIndend * (i)) + "px";
            tabArray[i].style.visibility = "visible";
        }
        tabsInited = true;
    }

    function hoverTab(obj){
        if (obj.id != curTabID)
            obj.className = "tabhover1";
    }

    function hoverOff(obj){
        if (obj.id != curTabID)
            obj.className = "tab1";
    }


    function resetTabs(){
        var tabs;
        var tabContent = null;
        var iLength = tabArray.length;
        for (var i = 0; i < iLength; i++) {
            tabs = tabArray[i].getElementsByTagName("SPAN");
            var jLength = tabs.length;
            for (var j = 1; j < jLength; j++) {
                tabs[j].className = "tab1";
                tabContent = document.all[tabs[j].frm];
                if (tabContent)
                  tabContent.style.display = "none";
            }
        }
        curTabID = null;
    }

    function changePage(obj){
        if (!tabsInited) return;
        if (curTabID == obj.id) return;

        resetTabs();
        obj.className = "tabactive1";
        curTabID = obj.id;

        var frm = obj.getAttribute("frm");
        var objFrm = null;
        if (frm.substr(0, 6) == "frame:") {
            objFrm = document.all[frm.substr(6)];
        }
        else
            objFrm = document.all[frm];

        if (objFrm) {
            objFrm.style.display = "inline";
        }
        if (frm.substr(0, 6) == "frame:"){
            var pg = obj.getAttribute("tabPage");
            var frmLoaded = objFrm.getAttribute("frmLoaded");
        }
    }

    function isDirty(){
      return gbDirtyFlag;
    }

    function checkProfileOverride(){
      var oTR = this.parentElement.parentElement;
      var sType = oTR.getAttribute("DataTypeCD");

      var iProfileID = oTR.lastChild.innerText;
      var sDefault = oTR.lastChild.previousSibling.innerText;
      var sValue = "";
      switch(sType) {
        case "B":
          eval("sValue=RBProfileValue" + iProfileID + ".value");
          break;
        case "S":
          var oSel = document.all["selProfileValue" + iProfileID];
          if (oSel){
            sValue = oSel.options[oSel.selectedIndex].innerText;
          }
          break;
        case "N":
          break;
      }
      if (this.lastChild.value > 0) {
          var objChkDiv = document.all["chkProfileOverriddenDiv"];
          if (objChkDiv && objChkDiv.lastChild.value == 0){
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
      }
      if (this.lastChild.value == 0 && sValue != sDefault){
        if (!bResetAllOverride)
          var sOverride = YesNoMessage("Reset to Default", "Do you want to reset profile settings for '" + this.parentElement.nextSibling.innerText + "' to default?");
        else
          var sOverride = "Yes";

  			if (sOverride == "No"){
          CheckBoxChange(this.firstChild);
          return;
        }
        switch(sType) {
          case "B":
            if (sDefault == "Yes")
              eval("RBProfileValue" + iProfileID + ".change(0,'Yes')");
            else
              eval("RBProfileValue" + iProfileID + ".change(1,'No')");
            break;
          case "S":
            var oSel = document.all["selProfileValue" + iProfileID];
            if (oSel)
              oSel.optionselect(sDefault, null);
            break;
          case "N":
            break;
        }
      }
    }

    function checkPermissionOverride(){
      var oTR = this.parentElement.parentElement;
      var sType = oTR.getAttribute("DataTypeCD");

      var iPermissionID = oTR.lastChild.innerText;
      var sDefault = oTR.lastChild.previousSibling.innerText;
      var sValue = "";
      switch(sType) {
        case "B":
          eval("sValue=RBPermissionValue" + iPermissionID + ".value");
          break;
        case "S":
          var oSel = document.all["selPermissionValue" + iPermissionID];
          if (oSel){
            sValue = oSel.options[oSel.selectedIndex].innerText;
          }
          break;
      }
      if (this.lastChild.value > 0) {
          var objChkDiv = document.all["chkPermissionOverriddenDiv"];
          if (objChkDiv && objChkDiv.lastChild.value == 0){
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
      }
      if (this.lastChild.value == 0 && sValue != sDefault){
        if (!bResetAllOverride)
          var sOverride = YesNoMessage("Reset to Default", "Do you want to reset permission settings for the Entity '" + this.parentElement.nextSibling.innerText + "' to default?");
        else
          var sOverride = "Yes";

  			if (sOverride == "No"){
          CheckBoxChange(this.firstChild);
          return;
        }
        switch(sType) {
          case "B":
            if (sDefault == "Yes")
              eval("RBPermissionValue" + iPermissionID + ".change(0,'Yes')");
            else
              eval("RBPermissionValue" + iPermissionID + ".change(1,'No')");
            break;
          case "S":
            var oSel = document.all["selPermissionValue" + iPermissionID];
            if (oSel)
              oSel.optionselect(sDefault, null);
            break;
        }
      }
    }

    function ProfileOverride(){
      try {
        if (this.disabled) return;
        var oTR = this.parentElement.parentElement;
      } catch(e) {
        if (event.srcElement.disabled) return;
        var oTR = event.srcElement.parentElement.parentElement;
      }
      var iProfileID = oTR.lastChild.innerText;

      var objChk = document.all["chkProfileOverridden_" + iProfileID];
      if (objChk){
        if(objChk.value == 0){
          var objChkDiv = document.all["chkProfileOverridden_" + iProfileID + "Div"];
          if (objChkDiv){
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
          var objChkDiv = document.all["chkProfileOverriddenDiv"];
          if (objChkDiv && objChkDiv.lastChild.value == 0){
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
        gbDirtyFlag = true;
      }
    }

    function PermissionOverride(){
      try {
        if (this.disabled) return;
        var oTR = this.parentElement.parentElement;
      } catch(e) {
        if (event.srcElement.disabled) return;
        var oTR = event.srcElement.parentElement.parentElement;
      }
      var iPermissionID = oTR.lastChild.innerText;

      var objChk = document.all["chkPermissionOverridden_" + iPermissionID];
      if (objChk){
        if(objChk.value == 0){
          var objChkDiv = document.all["chkPermissionOverridden_" + iPermissionID + "Div"];
          if (objChkDiv){
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
          var objChkDiv = document.all["chkPermissionOverriddenDiv"];
          if (objChkDiv && objChkDiv.lastChild.value == 0){
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
        gbDirtyFlag = true;
      }
    }

    function showHideProfileGroup(GrpCode, tbl){
      if (tbl){
        var bShow = true;
        var sImgSrc = event.srcElement.src;
        if (sImgSrc.indexOf("plus.gif") != -1){
          event.srcElement.src = "/images/minus.gif";
          bShow = true;
        } else {
          event.srcElement.src = "/images/plus.gif";
          bShow = false;
        }
        var tblRows = tbl.rows;
        var tblRowsLen = tblRows.length;
        var sType = "";
        for (var i = 0; i < tblRowsLen; i++){
          if (tblRows[i].businessCode == GrpCode){
            sType = tblRows[i].getAttribute("DataTypeCD");
            //alert(i + ": " + sType);
            if (bShow) {
              tblRows[i].cells[2].firstChild.style.display = "inline";
              tblRows[i].style.display = "inline";
            } else {
              tblRows[i].cells[2].firstChild.style.display = "none";
              tblRows[i].style.display = "none";
            }
          }
        }
      }
    }

    function defaultPermissionOverride(){
      var objChk = document.all["chkPermissionOverridden"];
      if (objChk){
        if(objChk.value == 0){
          var sOverride = YesNoMessage("Reset to Default", "Do you want to reset all permission settings to default?");
    			if (sOverride == "No"){
            CheckBoxChange(this.firstChild);
            return;
          } else {
            bResetAllOverride = true;
            var TRs = tblPermissions.rows;
            var iTRsCount = TRs.length;
            for (var i = 0; i < iTRsCount; i++) {
              if (TRs[i].cells.length > 1) {
                var iPermissionID = TRs[i].lastChild.innerText;
                var objChk1 = document.all["chkPermissionOverridden_" + iPermissionID];
                if (objChk1) {
                  if (objChk1.value > 0) {
                    var obj = document.all["chkPermissionOverridden_" + iPermissionID + "Div"];
                    if (obj)
                      CheckBoxChange(obj.firstChild);
                      CheckBoxBlur(obj.firstChild);
                      obj.fireEvent("onclick");
                  }
                }
              }
            }
            bResetAllOverride = false;
          }
        }
      }
    }

    function defaultProfileOverride(){
      var objChk = document.all["chkProfileOverridden"];
      if (objChk){
        if(objChk.value == 0){
          var sOverride = YesNoMessage("Reset to Default", "Do you want to reset all profile settings to default?");
    			if (sOverride == "No"){
            CheckBoxChange(this.firstChild);
            return;
          } else {
            bResetAllOverride = true;
            var TRs = tblProfile.rows;
            var iTRsCount = TRs.length;
            for (var i = 0; i < iTRsCount; i++) {
              if (TRs[i].cells.length > 1) {
                var iProfileID = TRs[i].lastChild.innerText;
                var objChk1 = document.all["chkProfileOverridden_" + iProfileID];
                if (objChk1) {
                  if (objChk1.value > 0) {
                    var obj = document.all["chkProfileOverridden_" + iProfileID + "Div"];
                    if (obj)
                      CheckBoxChange(obj.firstChild);
                      CheckBoxBlur(obj.firstChild);
                      obj.fireEvent("onclick");
                  }
                }
              }
            }
            bResetAllOverride = false;
          }
        }
      }
    }

    ShowSB40();

]]>
</script>
</head>
<BODY unselectable="on" class="bodyAPDSub" onLoad="PageInit()" style="background-color:#FFFFFF">
<form name="frmRoleDetail" method="post" action="">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
<!-- Start Tabs1 -->
<DIV unselectable="on" style="position:absolute; visibility: visible; top: 0px; left: 0px">
  <SPAN id="content11" name="content11" style="position:relative; top: 0px; left: 0px;width:100%;height:230px;padding:0px;margin:0px;">
    <xsl:attribute name="LastUpdatedDate">
      <xsl:value-of select="/Root/Role/@SysLastUpdatedDate"/>
    </xsl:attribute>
      <DIV unselectable="on" style="z-index:1;" name="tabsInTabs" id="tabsInTabs">
          <DIV id="tabs1" unselectable="on" class="apdtabs" style="position:relative;">
              <SPAN id="tab10" class="" unselectable="on" tabPage="" style="width:0px;"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</SPAN>
              <SPAN id="tab11" class="tab1" unselectable="on" frm="divRoleDetail" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Details</SPAN>
              <xsl:if test="$InsertMode = 'false'">
              <SPAN id="tab12" class="tab1" unselectable="on" frm="divRoleProfile" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Profile Override</SPAN>
              <SPAN id="tab13" class="tab1" unselectable="on" frm="divRolePermission" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Permission Override</SPAN>
              <SPAN id="tab14" class="tab1" unselectable="on" frm="divRoleUsers" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Users</SPAN>
              </xsl:if>
          </DIV>
      </DIV>

    <DIV name="divRoleDetail" id="divRoleDetail" style="border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
        <xsl:call-template name="tabGeneral"></xsl:call-template>
    </DIV>
    <xsl:if test="$InsertMode = 'false'">
    <DIV name="divRoleProfile" id="divRoleProfile" style="display:none;border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
        <xsl:call-template name="profileOverrides">
          <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
        </xsl:call-template>
    </DIV>
    <DIV name="divRolePermission" id="divRolePermission" SRC="/blank.asp" style="display:none;border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
        <xsl:call-template name="permissionOverrides"></xsl:call-template>
    </DIV>
    <DIV name="divRoleUsers" id="divRoleUsers" SRC="/blank.asp" style="display:none;border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
        <xsl:call-template name="RoleUsers"></xsl:call-template>
    </DIV>
    </xsl:if>
  </SPAN>
</DIV>
<!-- End Tabs1 -->

</form>
<xsl:call-template name="pageDefaults"></xsl:call-template>
</BODY>
</html>
</xsl:template>


<xsl:template name="tabGeneral">
  <div style="margin:10px">
    <table width="99%" border="0">
      <tr>
        <td width="16%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
        <td colspan="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
      </tr>
      <tr>
        <td width="16%">Role Name</td>
        <td width="67%">
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('RoleName',40,'Name',/Root/Role,'','',1)"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('RoleID',8,'RoleID',/Root/Role,'',6)"/>
        </td>
        <td>Enabled</td>
        <td><xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkRoleEnabled',boolean(/Root/Role/@EnabledFlag=1),'1','0','','1')"/>
        </td>
      </tr>
      <tr>
        <td width="16%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
      </tr>
      <tr>
        <td width="16%">Business Function:</td>
        <td colspan="3">

            <INPUT id="txtBusinessFunction" size="35"  type="text" class="InputReadonlyField"  name="BusinessCD" tabindex="-1">
                <xsl:choose>
                    <xsl:when test="/Root/Role/@BusinessFunctionCD = 'C'">
                        <xsl:attribute name="value">Claim Unit</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="/Root/Role/@BusinessFunctionCD = 'S'">
                        <xsl:attribute name="value">Shop Management Unit</xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="value"></xsl:attribute>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:attribute name="readonly"/>
                <xsl:attribute name="systemfield"/>
            </INPUT>
        </td>
      </tr>
      <tr>
        <td width="16%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
        <td colspan="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
      </tr>
      <tr>
        <td width="16%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
        <td colspan="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
      </tr>
    </table>
  </div>
</xsl:template>

<xsl:template name="profileOverrides">
  <xsl:param name="PermissionReadOnly"/>
  <div style="margin:5px">
    <table border="0">
          <colgroup>
            <col width="65px"/>
            <col width="200px"/>
            <col width="225px"/>
            <col width="185px"/>
          </colgroup>
        <tr style="font-weight:bold">
            <td align="center">
              Overridden<br/>
              <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkProfileOverridden',boolean(/Root/Role/Profile[@OverriddenFlag='1']),'1','0','', $PermissionReadOnly,1,-1)"/>
              <script>
                chkProfileOverriddenDiv.onclick=defaultProfileOverride;
              </script>
            </td>
            <td>Profile Name</td>
            <td >Value</td>
            <td >Default Value</td>
        </tr>
    </table>
    <DIV unselectable="on" style="z-index:1; width:720px; height:200px; overflow: auto;">
        <table border="0" nams="tblProfile" id="tblProfile" style="width:100%;">
          <colgroup>
            <col width="65px"/>
            <col width="200px"/>
            <col width="220px"/>
            <col width="175px"/>
          </colgroup>
            <tr style="height:24px;font-color:#FF0000;">
              <td colspan="4"><img src="/images/minus.gif" style="margin-right:15px;cursor:hand;" onclick="showHideProfileGroup('', tblProfile)"/><strong>Common</strong></td>
            </tr>
            <xsl:for-each select="/Root/Role/Profile[@BusinessFunctionCD='']">
              <xsl:sort select="@ProfileName" data-type="text" order="ascending"/>
              <xsl:call-template name="profileRow">
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
            </xsl:for-each>

            <tr style="height:24px;font-color:#FF0000;">
              <td colspan="4"><img src="/images/minus.gif" style="margin-right:15px;cursor:hand;" onclick="showHideProfileGroup('C', tblProfile)"/><strong>Claims Management</strong></td>
            </tr>
            <xsl:for-each select="/Root/Role/Profile[@BusinessFunctionCD='C']">
              <xsl:sort select="@ProfileName" data-type="text" order="ascending"/>
              <xsl:call-template name="profileRow">
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
            </xsl:for-each>

            <tr style="height:24px;font-color:#FF0000;">
              <td colspan="4"><img src="/images/minus.gif" style="margin-right:15px;cursor:hand;" onclick="showHideProfileGroup('S', tblProfile)"/><strong>Shop Management</strong></td>
            </tr>
            <xsl:for-each select="/Root/Role/Profile[@BusinessFunctionCD='S']">
              <xsl:sort select="@ProfileName" data-type="text" order="ascending"/>
              <xsl:call-template name="profileRow">
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
            </xsl:for-each>
        </table>
    </DIV>
  </div>
</xsl:template>

<xsl:template name="profileRow">
  <xsl:param name="PermissionReadOnly"/>
            <tr style="height:24px;" valign="middle">
              <xsl:attribute name="businessCode"><xsl:value-of select="@BusinessFunctionCD"/></xsl:attribute>
              <xsl:attribute name="DataTypeCD"><xsl:value-of select="@DataTypeCD"/></xsl:attribute>
                <td style="border-bottom:1px dashed #FFE4B5;" align="center">
                  <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkProfileOverridden_', string(@OverriddenFlag),string(@ProfileID),'0',string(@ProfileID), boolean($PermissionReadOnly), 1, 29)"/>

                  <script>
                    chkProfileOverridden_<xsl:value-of select="@ProfileID"/>Div.onclick=checkProfileOverride;
                  </script>
                </td>
                <td style="border-bottom:1px dashed #FFE4B5;"><xsl:value-of select="@ProfileName"/></td>
                <td style="border-bottom:1px dashed #FFE4B5;">
                  <xsl:call-template name="profileItemValue">
                    <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
                  </xsl:call-template>
                </td>
                <td style="border-bottom:1px dashed #FFE4B5;">
                  <xsl:choose>
                    <xsl:when test="@DataTypeCD = 'B'">
                     <xsl:choose>
                      <xsl:when test="@DefaultProfileValue=1">Yes</xsl:when>
                      <xsl:otherwise>No</xsl:otherwise>
                     </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@DataTypeCD = 'S'">
                      <xsl:variable name="ProfileID"><xsl:value-of select="@ProfileID"/></xsl:variable>
                      <xsl:variable name="DefaultProfileValue"><xsl:value-of select="@DefaultProfileValue"/></xsl:variable>
                      <xsl:value-of select="/Root/Reference[@List='ProfileSelection' and @ProfileID=$ProfileID and @ReferenceID=$DefaultProfileValue]/@Name"/>
                    </xsl:when>
                    <xsl:when test="@DataTypeCD = 'N'">
                      <xsl:choose>
                        <xsl:when test="@DefaultProfileValue != ''">
                          <xsl:value-of select="@DefaultProfileValue"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:when>
                  </xsl:choose>
                </td>
                <td style="display:none"><xsl:value-of select="@ProfileID"/></td>
            </tr>
</xsl:template>

<xsl:template name="profileItemValue">
  <xsl:param name="PermissionReadOnly"/>
  <xsl:choose>
    <xsl:when test="@DataTypeCD = 'B'">
      <xsl:variable name="defValue">
       <xsl:choose>
        <xsl:when test="@ProfileValue=1">Yes</xsl:when>
        <xsl:otherwise>No</xsl:otherwise>
       </xsl:choose>
      </xsl:variable>
      <xsl:value-of disable-output-escaping="yes" select="js:AddRadio(concat('ProfileValue', @ProfileID), 0, string($defValue), 'Yes', 'No')"/>
      <script>
        ProfileValue<xsl:value-of select="@ProfileID"/>Div.onclick=ProfileOverride;
        <xsl:if test="boolean($PermissionReadOnly) = true()">
          ProfileValue<xsl:value-of select="@ProfileID"/>Div.disabled=true;
        </xsl:if>
      </script>
    </xsl:when>
    <xsl:when test="@DataTypeCD = 'S'">
      <xsl:variable name="ProfileID" select="@ProfileID"/>
      <xsl:value-of disable-output-escaping="yes" select="js:AddSelect(concat('ProfileValue', @ProfileID),2,'onSelectChange',string(@ProfileValue),1,5,/Root/Reference[@List='ProfileSelection' and @ProfileID=$ProfileID],'Name','ReferenceID',0,90,'','',15)"/>
      <script>
        selProfileValue<xsl:value-of select="@ProfileID"/>.onclick=ProfileOverride;
        <xsl:if test="boolean($PermissionReadOnly) = true()">
          selProfileValue<xsl:value-of select="@ProfileID"/>.disabled=true;
        </xsl:if>
      </script>
    </xsl:when>
    <xsl:when test="@DataTypeCD = 'N'">
      <INPUT size='8' type='text' maxlength='15' onchange="ProfileOverride()">
        <xsl:attribute name="name"><xsl:value-of select="concat('ProfileValue', @ProfileID)"/></xsl:attribute>
        <xsl:attribute name="id"><xsl:value-of select="concat('ProfileValue', @ProfileID)"/></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="string(@ProfileValue)"/></xsl:attribute>
        <xsl:choose>
          <xsl:when test="boolean($PermissionReadOnly) = true()">
            <xsl:attribute name="disabled"/>
            <xsl:attribute name="readonly"/>
            <xsl:attribute name="class">InputReadonlyField</xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="class">InputField</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>
      </INPUT>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="permissionOverrides">
  <xsl:param name="PermissionReadOnly"/>

  <div style="margin:5px">
    <table border="0">
          <colgroup>
            <col width="65px"/>
            <col width="200px"/>
            <col width="225px"/>
            <col width="185px"/>
          </colgroup>
        <tr style="font-weight:bold">
            <td align="center">
              Overridden<br/>
              <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkPermissionOverridden',boolean(/Root/Role/Permission[@OverriddenFlag='1']),'1','0','', $PermissionReadOnly,1,-1)"/>
              <script>
                chkPermissionOverriddenDiv.onclick=defaultPermissionOverride;
              </script>
            </td>
            <td>Entity</td>
            <td>Value</td>
            <td >Default Value</td>
        </tr>
    </table>
    <DIV unselectable="on" style="z-index:1; width:720px; height:200px; overflow: auto;">
        <table border="0" cellspacing="0" cellpadding="0" name="tblPermissions" id="tblPermissions" style="width:100%;">
          <colgroup>
            <col width="65px"/>
            <col width="200px"/>
            <col width="225px"/>
            <col width="175px"/>
          </colgroup>
            <tr style="height:24px;">
              <td colspan="5"><img src="/images/minus.gif" style="margin-right:15px;cursor:hand;" onclick="showHideProfileGroup('', tblPermissions)"/><strong>Common</strong></td>
            </tr>
            <xsl:for-each select="/Root/Role/Permission[@BusinessFunctionCD='']">
              <xsl:sort select="@GroupCD" data-type="text" order="ascending"/>
              <xsl:sort select="@Name" data-type="text" order="ascending"/>
              <xsl:call-template name="permissionRow">
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
            </xsl:for-each>

            <tr style="height:24px;">
              <td colspan="5"><img src="/images/minus.gif" style="margin-right:15px;cursor:hand;" onclick="showHideProfileGroup('C', tblPermissions)"/><strong>Claims Management</strong></td>
            </tr>
            <xsl:for-each select="/Root/Role/Permission[@BusinessFunctionCD='C']">
              <xsl:sort select="@GroupCD" data-type="text" order="ascending"/>
              <xsl:sort select="@Name" data-type="text" order="ascending"/>
              <xsl:call-template name="permissionRow">
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
            </xsl:for-each>

            <tr style="height:24px;">
              <td colspan="5"><img src="/images/minus.gif" style="margin-right:15px;cursor:hand;" onclick="showHideProfileGroup('S', tblPermissions)"/><strong>Shop Management</strong></td>
            </tr>
            <xsl:for-each select="/Root/Role/Permission[@BusinessFunctionCD='S']">
              <xsl:sort select="@GroupCD" data-type="text" order="ascending"/>
              <xsl:sort select="@Name" data-type="text" order="ascending"/>
              <xsl:call-template name="permissionRow">
                <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
              </xsl:call-template>
            </xsl:for-each>
        </table>
    </DIV>
  </div>
</xsl:template>

<xsl:template name="permissionRow">
  <xsl:param name="ProfileReadOnly"/>
  <xsl:param name="PermissionReadOnly"/>
                <tr style="height:24px;">
                  <xsl:attribute name="businessCode"><xsl:value-of select="@BusinessFunctionCD"/></xsl:attribute>
                  <xsl:choose>
                    <xsl:when test="@GroupCD = 'A' or @GroupCD = 'R'">
                      <xsl:attribute name="DataTypeCD">B</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="@GroupCD = 'D'">
                      <xsl:attribute name="DataTypeCD">S</xsl:attribute>
                    </xsl:when>
                  </xsl:choose>
                    <td style="border-bottom:1px dashed #FFE4B5;" align="center">
                      <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkPermissionOverridden_',boolean(@OverriddenFlag=1),string(@PermissionID),'0',string(@PermissionID), string($PermissionReadOnly),1, 31)"/>
                      <script>
                        chkPermissionOverridden_<xsl:value-of select="@PermissionID"/>Div.onclick=checkPermissionOverride;
                      </script>
                    </td>
                    <td style="border-bottom:1px dashed #FFE4B5;">
                      <xsl:value-of select="@Name"/>
                    </td>
                    <td style="border-bottom:1px dashed #FFE4B5;">
                      <xsl:call-template name="permissionItemValue">
                        <xsl:with-param name="PermissionReadOnly" select="$PermissionReadOnly"/>
                      </xsl:call-template>
                    </td>
                    <td style="border-bottom:1px dashed #FFE4B5;">
                      <xsl:choose>
                        <xsl:when test="@GroupCD = 'A' or @GroupCD = 'R'">
                         <xsl:choose>
                          <xsl:when test="@DefaultReadFlag=1">Yes</xsl:when>
                          <xsl:otherwise>No</xsl:otherwise>
                         </xsl:choose>
                        </xsl:when>
                        <xsl:when test="@GroupCD = 'D'">
                          <xsl:variable name="defaultValue">
                            <xsl:value-of select="number(@DefaultCreateFlag)*16 + number(@DefaultReadFlag)*8 + number(@DefaultUpdateFlag)*4 + number(@DefaultDeleteFlag)*2"/>
                          </xsl:variable>
                          <xsl:choose>
                            <xsl:when test="$defaultValue=0">No Access</xsl:when>
                            <xsl:when test="$defaultValue=8">Read</xsl:when>
                            <xsl:when test="$defaultValue=12">Read, Update</xsl:when>
                            <xsl:when test="$defaultValue=24">Create, Read</xsl:when>
                            <xsl:when test="$defaultValue=28">Create, Read, Update</xsl:when>
                            <xsl:when test="$defaultValue=30">Full Access</xsl:when>
                          </xsl:choose>
                        </xsl:when>
                      </xsl:choose>
                    </td>
                    <td style="display:none"><xsl:value-of select="@PermissionID"/></td>
                </tr>
</xsl:template>

<xsl:template name="permissionItemValue">
  <xsl:param name="PermissionReadOnly"/>
  <xsl:choose>
    <xsl:when test="@GroupCD = 'A' or @GroupCD = 'R'">
      <xsl:variable name="defValue">
       <xsl:choose>
        <xsl:when test="@ReadFlag=1">Yes</xsl:when>
        <xsl:otherwise>No</xsl:otherwise>
       </xsl:choose>
      </xsl:variable>
      <xsl:value-of disable-output-escaping="yes" select="js:AddRadio(concat('PermissionValue', string(@PermissionID)), 0, string($defValue), 'Yes', 'No')"/>
      <script>
        PermissionValue<xsl:value-of select="@PermissionID"/>Div.onclick=PermissionOverride;
        <xsl:if test="boolean($PermissionReadOnly) = true()">
          PermissionValue<xsl:value-of select="@PermissionID"/>Div.disabled=true;
        </xsl:if>
      </script>
    </xsl:when>
    <xsl:when test="@GroupCD = 'D'">
      <xsl:variable name="value" select="number(@CreateFlag)*16 + number(@ReadFlag)*8 + number(@UpdateFlag)*4 + number(@DeleteFlag)*2"/>
      <xsl:variable name="IDs">0|8|12|24|28|30</xsl:variable>
      <xsl:variable name="Names">No Access|Read|Read, Update|Create, Read|Create, Read, Update|Full Access</xsl:variable>
      <xsl:value-of disable-output-escaping="yes" select="js:AddSelect(concat('PermissionValue', string(@PermissionID)),2,'onSelectChange',string($value),1,number(count(.)-position()),string($Names),'',string($IDs))"/>
      <script>
        selPermissionValue<xsl:value-of select="@PermissionID"/>.onclick=PermissionOverride;
        <xsl:if test="boolean($PermissionReadOnly) = true()">
          selPermissionValue<xsl:value-of select="@PermissionID"/>.disabled=true;
        </xsl:if>
      </script>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="RoleUsers">
  <div style="margin:10px;">
    <xsl:value-of select="/Root/Role/@Name"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Users:
    <div style="width:250px;height:175px;overflow:auto;margin:5px;padding:3px;">
      <table border="0" cellspacing="0" cellpadding="2">
        <xsl:for-each select="/Root/Role/Users[@UserLastName != 'System']">
          <xsl:sort select="@UserLastName"/>
          <xsl:sort select="@UserFirstName"/>
          <tr>
            <td>
              <xsl:if test="@Active != 1">
                <xsl:attribute name="style">color:<![CDATA[#]]>C0C0C0</xsl:attribute>
              </xsl:if>
              <xsl:value-of select="@UserLastName"/>, <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@UserFirstName"/>
            </td>
            <td style="display:none"><xsl:value-of select="@UserID"/></td>
          </tr>
        </xsl:for-each>
      </table>
    </div>
  </div>
</xsl:template>

<xsl:template name="pageDefaults">
  <div style="display:none">
    <input type="hidden" name="__RoleName" id="__RoleName" >
      <xsl:attribute name="value"><xsl:value-of select="/Root/Role/@Name"/></xsl:attribute>
    </input>
    <input type="hidden" name="__Profile" id="__Profile" >
      <xsl:attribute name="value">
        <xsl:for-each select="/Root/Role/Profile[@OverriddenFlag=1]"><xsl:value-of select="@ProfileID"/>,<xsl:value-of select="@ProfileValue"/>;</xsl:for-each>
      </xsl:attribute>
    </input>
    <input type="hidden" name="__Permission" id="__Permission" >
      <xsl:attribute name="value">
        <xsl:for-each select="/Root/Role/Permission[@OverriddenFlag=1]"><xsl:value-of select="@PermissionID"/>,<xsl:value-of select="@CreateFlag"/>,<xsl:value-of select="@ReadFlag"/>,<xsl:value-of select="@UpdateFlag"/>,<xsl:value-of select="@DeleteFlag"/>;</xsl:for-each>
      </xsl:attribute>
    </input>
  </div>
</xsl:template>
</xsl:stylesheet>
