<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="PMDCSI">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="PMDDateSearch.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="ShopCRUD" select="Shop"/>

<xsl:template match="/Root">

<xsl:param name="LocID"/>

<xsl:variable name="ShopLocationID" select="/Root/@ShopLocationID"/>
<xsl:variable name="BeginDate" select="/Root/@BeginDate"/>
<xsl:variable name="EndDate" select="/Root/@EndDate"/>
<xsl:variable name="AssignmentCode" select="/Root/@AssignmentCode"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDCSIGetDetailXML,PMDCSI.xsl,1391, '01/01/2000', '05/06/2003'   -->

<HEAD>
  <TITLE>Program Manager Desktop</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  
  <style>
  	A{
		  color:blue
	  }
	  A:hover{
		  color : #B56D00;
		  font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
		  font-weight : bold;
		  font-size : 10px;
		  cursor : hand;
	  }
  </style>
  
<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDNavigate.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">

parent.gsShopLocationID = "<xsl:value-of select="$ShopLocationID"/>";
parent.gsBeginDate = "<xsl:value-of select="user:UTCConvertDate(string($BeginDate))"/>";
parent.gsEndDate = "<xsl:value-of select="user:UTCConvertDate(string($EndDate))"/>";
parent.gsAssignmentCode = "<xsl:value-of select="$AssignmentCode"/>";

<![CDATA[

//init the table selections, must be last
function initPage()
{
  txtBeginDate.value = parent.gsBeginDate;
  txtEndDate.value = parent.gsEndDate;
  parent.resizeScrollTable(document.getElementById("CNScrollTable"));
  parent.document.all.tab15.className = "tabactive1";
}

function GridSelect(oRow){
  parent.gsLynxID = oRow.cells[0].innerText;
  parent.gsClaimAspectID = oRow.cells[0].getAttribute("ClaimAspectID");
  parent.gsAssignmentID = oRow.cells[0].getAttribute("AssignmentID");
  parent.gsReferrer = "PMDCSI.asp";
  window.navigate("PMDEstimateList.asp?AssignmentID=" + parent.gsAssignmentID);
}

]]>
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSub" unselectable="on" onLoad="initPage();" bgcolor="#FFFAEB">
  
  <DIV unselectable="on" class="SmallGroupingTab" style="position:absolute; z-index:2; top: 5px; left: 6px;">
    Shop<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Assignment<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;List</DIV>
  <DIV unselectable="on" class="SmallGrouping" style="position:absolute; left:3px; top:21px;width:965px">

  <xsl:call-template name="DateSearch"> <!-- search bar -->
    <xsl:with-param name="AssignmentCode" select="$AssignmentCode"/>
  </xsl:call-template>
  
    <IMG src="/images/spacer.gif" width="1" height="4" border="0" />

    <DIV id="CNScrollTable" style="width:100%;" >
      <TABLE unselectable="on" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSort1')" width="100%" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
        <TBODY>
          <TR unselectable="on" class="QueueHeader">
      			<TD unselectable="on" class="TableSortHeader" sIndex="0" width="60" rowspan="2">LYNX ID</TD>
      			<TD unselectable="on" class="TableSortHeader" sIndex="99" width="100%" rowspan="2" style="cursor:default;"> Vehicle YMM </TD>
      			<TD unselectable="on" class="TableSortHeader" sIndex="99" width="130" rowspan="2" style="cursor:default;"> Vehicle Owner </TD>
      			<TD unselectable="on" class="TableSortHeader" sIndex="99" width="219" colspan="3" style="cursor:default;"> Estimate Totals </TD>
      			<TD unselectable="on" class="TableSortHeader" sIndex="1" width="40" rowspan="2"> CSI % </TD>
      			<TD unselectable="on" class="TableSortHeader" sIndex="99" width="215" colspan="5" style="cursor:default;"> CSI Questions </TD>
      			<TD unselectable="on" class="TableSortHeader" sIndex="99" width="0" rowspan="2" style="cursor:default;"></TD> <!-- scroll bar spacer -->
	        </TR>
    		  <TR unselectable="on" class="QueueHeader">
      			<TD height="20" unselectable="on" class="TableSortHeader" sIndex="99" width="70" nowrap="nowrap" style="cursor:default;"> Original </TD>
      			<TD unselectable="on" class="TableSortHeader" sIndex="99" width="70" nowrap="nowrap" style="cursor:default;"> Supplement </TD>
      			<TD unselectable="on" class="TableSortHeader" sIndex="99" width="70" nowrap="nowrap" style="cursor:default;"> Final </TD>
      			<TD unselectable="on" class="TableSortHeader" sIndex="99" width="40" nowrap="nowrap" style="cursor:default;"> #1 </TD>
      			<TD unselectable="on" class="TableSortHeader" sIndex="99" width="40" nowrap="nowrap" style="cursor:default;"> #2 </TD>
      			<TD unselectable="on" class="TableSortHeader" sIndex="99" width="40" nowrap="nowrap" style="cursor:default;"> #3 </TD>
      			<TD unselectable="on" class="TableSortHeader" sIndex="99" width="40" nowrap="nowrap" style="cursor:default;"> #4 </TD>
      			<TD unselectable="on" class="TableSortHeader" sIndex="99" width="40" nowrap="nowrap" style="cursor:default;"> #5 </TD>
    		  </TR>
        </TBODY>
      </TABLE>

	  <DIV unselectable="on" class="autoflowTable" style="width:100%; height:260px;">
      <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" width="100%" border="0" cellspacing="1" cellpadding="0" style="table-layout:fixed">
        <TBODY bgColor1="ffffff" bgColor2="fff7e5">
          <xsl:for-each select="Shop/Assignment" ><xsl:call-template name="ShopAssignmentList"/></xsl:for-each>
        </TBODY>
      </TABLE>
    </DIV>
	</DIV>

  </DIV>

  <DIV unselectable="on" style="position:absolute; left:0px; top:310px">
    <xsl:for-each select="Shop" ><xsl:call-template name="ShopInfo"><xsl:with-param name="ShopCRUD"><xsl:value-of select="$ShopCRUD"/></xsl:with-param></xsl:call-template></xsl:for-each>
	  <xsl:for-each select="Stats" ><xsl:call-template name="CSIStuff"/></xsl:for-each>
  </DIV>
</BODY>
</HTML>
</xsl:template>

  <!-- Gets the list of Asssignments -->
  <xsl:template name="ShopAssignmentList">
    <TR unselectable="on" onMouseOut="parent.GridMouseOut(this)" onMouseOver="parent.GridMouseOver(this)" onclick="GridSelect(this)">
      <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
      <xsl:attribute name="title"><xsl:if test="@CancellationDate != ''">This Assignment has been cancelled but has Reinspection(s).  </xsl:if>Click a Vehicle to view its Estimate List.</xsl:attribute>
      <TD unselectable="on" width="58" class="GridTypeTD">
	      <xsl:attribute name="AssignmentID"><xsl:value-of select="@AssignmentID"/></xsl:attribute> 
        <xsl:attribute name="ClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:attribute>       
        <xsl:choose>
          <xsl:when test="/Root/@ViewCRD=1">
            <a>
              <xsl:if test="@CancellationDate != ''"><xsl:attribute name="style">color:red;</xsl:attribute></xsl:if>
              <xsl:attribute name="href">javascript:NavToClaim(<xsl:value-of select="@LynxID"/>)</xsl:attribute>
              <xsl:value-of select="@LynxID"/>
            </a>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test="@CancellationDate != ''"><xsl:attribute name="style">color:red;</xsl:attribute></xsl:if>
            <xsl:value-of select="@LynxID"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" width="100%" class="GridTypeTD" style="text-align:left;">
        <xsl:attribute name="style">text-align:left;<xsl:if test="@CancellationDate != ''">color:red;</xsl:if></xsl:attribute>
	  	  <xsl:value-of select="@VehicleYear"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		    <xsl:value-of select="@VehicleMake"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
		    <xsl:value-of select="@VehicleModel"/>
      </TD>
      <TD unselectable="on" width="128" class="GridTypeTD" style="text-align:left;">
        <xsl:choose>
          <xsl:when test="@OwnerBusinessName != ''">
            <xsl:value-of select="@OwnerBusinessName"/>
          </xsl:when>
          <xsl:when test="@OwnerLastName != ''">
            <xsl:value-of select="@OwnerLastName"/>,<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@OwnerFirstName"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" width="72" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@OriginalEstimateAmt=''">
           <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@OriginalEstimateAmt"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" width="72" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@FirstSupplementAmt=''" >
           <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@FirstSupplementAmt"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" width="71" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@FinalEstimateAmt=''" >
           <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@FinalEstimateAmt"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
	  
      <TD unselectable="on" width="41" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@CSIPct=''" >
           <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@CSIPct"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
	  <TD unselectable="on" width="42" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@CSI1=''" >
           <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@CSI1"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
	  <TD unselectable="on" width="42" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@CSI2=''" >
           <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@CSI2"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
	  <TD unselectable="on" width="42" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@CSI3=''" >
           <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@CSI3"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
	  <TD unselectable="on" width="42" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@CSI4=''" >
           <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@CSI4"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
	  <TD unselectable="on" width="41" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@CSI5=''" >
           <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@CSI5"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD style="display:none">
        <xsl:value-of select="@VehicleNumber"/>
      </TD>
    </TR>
  </xsl:template>

  <!-- Gets the shop details -->
  <xsl:template name="CSIStuff">
      
      <DIV unselectable="on" class="SmallGroupingTab" style="position:absolute; z-index:2; top:70px; left:443px;">
        Customer Service Audit</DIV>
      <DIV unselectable="on" class="SmallGrouping" style="position:absolute; left:440px; top:86px;">
        <TABLE unselectable="on" border="0" cellspacing="1" cellpadding="1" style="text-align:center; table-layout:fixed;">
          <TR unselectable="on">
		    <colgroup>
			  <col width="150" align="left"/>
			  <col width="80"/>
			  <col width="80"/>
			  <col width="80"/>
			</colgroup>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
            <TD unselectable="on" class="TableHeader2"><SPAN class="boldtext"># Assign</SPAN></TD>
            <TD unselectable="on" class="TableHeader2"><SPAN class="boldtext"># Est</SPAN></TD>
			<TD unselectable="on" class="TableHeader2"><SPAN class="boldtext">% Sat</SPAN></TD>
          </TR>
          <TR unselectable="on" bgColor="#FDF5E6">
            <TD unselectable="on" class="boldtext" nowrap="nowrap">Customer Contact</TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:value-of select="xxx"/>
			  xx
            </TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:value-of select="xxx"/>
			  xx
            </TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:value-of select="xxx"/>
			  xx
            </TD>
          </TR>
          <TR unselectable="on" bgColor="#FFFFEE">
            <TD unselectable="on" class="boldtext" nowrap="nowrap">Check-in Form</TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:value-of select="xxx"/>
			  xx
            </TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:value-of select="xxx"/>
			  xx
            </TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:value-of select="xxx"/>
			  xx
            </TD>
          </TR>
          <TR unselectable="on" bgColor="#FDF5E6">
            <TD unselectable="on" class="boldtext" nowrap="nowrap">Scheduled Start Dates</TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:value-of select="xxx"/>
			  xx
            </TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:value-of select="xxx"/>
			  xx
            </TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:value-of select="xxx"/>
			  xx
            </TD>
          </TR>
		  <TR unselectable="on" bgColor="#FFFFEE">
            <TD unselectable="on" class="boldtext" nowrap="nowrap">Final Check List</TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:value-of select="xxx"/>
			  xx
            </TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:value-of select="xxx"/>
			  xx
            </TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:value-of select="xxx"/>
			  xx
            </TD>
          </TR>
        </TABLE>
      </DIV>

  </xsl:template>

  
</xsl:stylesheet>

