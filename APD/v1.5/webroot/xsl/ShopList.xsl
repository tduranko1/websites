<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
	xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ShopList">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/msjs-client-library-htc.js"/>
<xsl:import href="msxsl/msxsl-function-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserId"/>
<xsl:param name="LynxId"/>
<xsl:param name="VehicleNumber"/>
<xsl:param name="ClaimAspectID"/>
<xsl:param name="ShopRemarks"/>
<xsl:param name="AssignmentTo"/>
<xsl:param name="MEChildWin"/>
<xsl:param name="ClaimAspectServiceChannelID"/>

<xsl:template match="/Root">

<xsl:variable name="AreaCode" select="/Root/@AreaCode" />
<xsl:variable name="ExchangeNumber" select="/Root/@ExchangeNumber" />
<xsl:variable name="InsuranceCompanyID" select="/Root/@InsuranceCompanyID" />
<xsl:variable name="AssignmentAtSelection" select="/Root/@AssignmentAtSelection" />
<xsl:variable name="ShopSearchLogID" select="/Root/@ShopSearchLogID"/>



<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.0.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspShopSearchByDistanceXML,ShopList.xsl,'33919',145,'A'  -->

<HEAD>
<TITLE>Shop List</TITLE>

<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdmain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdselect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/coolselect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<SCRIPT language="JavaScript">

  var gsUserID = '<xsl:value-of select="$UserId"/>';
  var gsLynxID = '<xsl:value-of select="$LynxId"/>';
  var gsVehNum = '<xsl:value-of select="$VehicleNumber"/>';
  var gsClaimAspectID = '<xsl:value-of select="$ClaimAspectID"/>';
  var gsClaimAspectServiceChannelID = '<xsl:value-of select="$ClaimAspectServiceChannelID"/>';
  var gsAssignmentAtSelectionFlag = '<xsl:value-of select="$AssignmentAtSelection"/>';
  var gsShopSearchLogID = '<xsl:value-of select="$ShopSearchLogID"/>';
  var gsMEChildWin = '<xsl:value-of select="$MEChildWin"/>';
  var gsShopId, gsSelectedShopRank, gsSelectedShopScore;

<![CDATA[

  // Tooltip Variables to set:
  messages= new Array()
  // Write your descriptions in here.
  messages[0]="Double Click to Expand"
  messages[1]="Single-Click Column to sort";

  var aryShopInfo = new Array();

  function ConvertTime(strTime)
  {
    return strTime;
    if (strTime == "")
      return "--:--";

    var Hours = strTime.substring(0,2);
    if (Hours.substring(0,1) == "0")
      Hours = Hours.substring(1,1);
    var Mins = strTime.substring(3,2);
    var sAMPM = " am";
    Hrs = parseInt(Hours);
    if (Hrs >= 12)
    {
      sAMPM = " pm";
      Hrs -= 12;
    }
    if (Hrs == 0)
      Hrs = 12;

    //if (Hrs < 10) Hours = "0" + Hrs;
    //else
      Hours = String(Hrs);

    return Hours + ":" + Mins + sAMPM;
  }

  // Double Click - does the same as single click for now
  function GridSelect(oRow)
  {
    GridClick(oRow)
  }

  // Single Click
  function GridClick(oRow)
  {
    if (!oRow) return;

    //get the table name of the row that was clicked
    var oTable = oRow.parentElement.parentElement;
    gsShopId=oRow.cells[7].innerText;
    gsSelectedShopRank=oRow.cells[7].getAttribute("SelectedShopRank");
    gsSelectedShopScore=oRow.cells[7].getAttribute("SelectedShopScore");
    txtShopName.value=oRow.cells[8].innerText;
    aryShopInfo[0] = txtShopName.value;
    txtShopAddress1.value=oRow.cells[9].innerText;
    aryShopInfo[1] = txtShopAddress1.value;
    txtShopAddress2.value=oRow.cells[10].innerText;
    aryShopInfo[2] = txtShopAddress2.value;
    txtShopCity.value=oRow.cells[11].innerText;
    aryShopInfo[3] = txtShopCity.value;
    txtShopState.value=oRow.cells[12].innerText;
    aryShopInfo[4] = txtShopState.value;
    txtShopZip.value=oRow.cells[13].innerText;
    aryShopInfo[5] = txtShopZip.value;
    txtShopPhone.value=oRow.cells[32].innerText;
    aryShopInfo[6] = txtShopPhone.value;
    txtShopFax.value=oRow.cells[33].innerText;
    aryShopInfo[7] = txtShopFax.value;
    ShopLocalTime=UTCConvertTime(oRow.cells[14].innerText);
    txtShopLocalTime.value=ConvertTime(ShopLocalTime);
    txtShopTimeZone.value=oRow.cells[15].innerText;
    txtShopOpen.value=oRow.cells[5].innerText;
    txtShopDistance.value=oRow.cells[6].innerText;
    txtShopHoursWeekOpen.value=ConvertTime(oRow.cells[17].innerText);
    txtShopHoursWeekClose.value=ConvertTime(oRow.cells[16].innerText);
    txtShopHoursSaturdayOpen.value=ConvertTime(oRow.cells[27].innerText);
    txtShopHoursSaturdayClose.value=ConvertTime(oRow.cells[26].innerText);
    txtShopHoursSundayOpen.value=ConvertTime(oRow.cells[29].innerText);
    txtShopHoursSundayClose.value=ConvertTime(oRow.cells[28].innerText);
    txtShopContactName.value=oRow.cells[30].innerText;
    txtShopContactPhone.value=oRow.cells[31].innerText;
    aryShopInfo[8] = gsShopId;
  }

  //init the shop selections
  function initPage()
  {
    var oTbl = document.getElementById("tblsort1");
    if (oTbl)
      GridClick(oTbl.rows[0]);
  	//if ( parent.location == top.location) {
    if (parent.inDialogView) {
      var obj = document.getElementById("ShopContactInfo");
      if (obj){
        obj.style.position = "absolute";
        obj.style.width = "710px";
        obj.style.left =  "2px";
        obj.style.top = "145px"; 
       } 
       
       obj = document.getElementById("tblSortDiv");
       if (obj)
        obj.style.height= "115px";
       
       obj = document.getElementById("ShopCommentsInfo");
       if (obj){
        obj.style.position = "absolute";
        obj.style.top = "255px";
       }
      
      if (document.getElementById("buttonClose"))
        buttonClose.style.visibility = "visible";
    }
    else	{
      if (document.getElementById("buttonClose")) 
        buttonClose.style.visibility = 'hidden';  //hide the close button in frame window
    }
  }

  var gbInAssign = false;
  function AssignShop(){
      if (parent.stat1) parent.stat1.Show("Please wait...");
      window.setTimeout("AssignShop2()", 100);
  }
  
  function AssignShop2()
  {
    var sRequest;
  
    if (gbInAssign == true)
      return;

    gbInAssign = true;
    
    btnSelectShop.disabled = true;
    
    sRemarks = escape(txtShopRemarks.value);

    sRequest = (gsClaimAspectID.length > 0) ? "ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID + "&ClaimAspectID=" + gsClaimAspectID : "LynxID=" + gsLynxID + "&VehicleNumber=" + gsVehNum;
    sRequest += "&ShopSearchLogID=" + gsShopSearchLogID +
                "&SelectOperationCD=S&UserId=" + gsUserID +
                "&ShopLocationID=" + gsShopId +
                "&SelectedShopRank=" + gsSelectedShopRank +
                "&SelectedShopScore=" + gsSelectedShopScore +
                "&AssignmentRemarks=" + sRemarks +
                "&NotifyEvent=1" +
                "&WarrantySelect=" + parent.strWarranty;
                         
     if (parent.strWarranty == "1")
      sProc = "uspWorkflowWarrantySelectShop";
     else
      sProc = "uspWorkFlowSelectShop";
     
     var aRequests = new Array();
     aRequests.push( { procName : sProc,
                       method   : "executespnp",
                       data     : sRequest }
                   );
     //alert(makeXMLSaveString(aRequests));
     //return;
     var objRet = XMLSave(makeXMLSaveString(aRequests));
     if (objRet.code == 0 && objRet.xml) {
         var oRetXML = objRet.xml;
         if (parent.strWarranty == "1") {
            if (window != top && typeof(parent.AssignedShop) == "function")
                 parent.AssignedShop();
            else
              window.close();
            return;
         }
         var oResult = oRetXML.selectSingleNode("//Result");
         if (oResult) {
            gsAssignmentID = oResult.text;
            //alert(gsAssignmentID);
            
            //now assignment LDAU
            sProc = "uspWorkflowAssignShop";
           
            sRequest = "AssignmentID=" + gsAssignmentID;
           
            var aRequests = new Array();
            aRequests.push( { procName : sProc,
                             method   : "executespnp",
                             data     : sRequest }
                         );
           //alert(sRequest);
           //alert(makeXMLSaveString(aRequests));
           //return;
            var objRet = XMLSave(makeXMLSaveString(aRequests));
            if (objRet.code == 0) {
               if ( gsAssignmentAtSelectionFlag == 1 ) {
                  //now send the assigment
                  sRequest = "<Root><Assignment assignmentID=\"" + gsAssignmentID + "\" userID=\"" + gsUserID + "\" staffAppraiser=\"false\" /></Root>"
                  //alert("calling com..\n" + sRequest);
                  var objRet = XMLServerExecute("AssignShop.asp", sRequest);
                  if (objRet.code == 0){
                     var strRet;
                     if (objRet.xml){
                        var oResult = objRet.xml.selectSingleNode("/Root/Success");
                        if (oResult){
                           var strStatus = oResult.text;
   
                           if (strStatus.substring(0, 1) == "F")  ClientInfo("Electronic assignment to shop failed.");
                           if (strStatus.substring(1, 1) == "F") ClientInfo("Fax assignment to shop failed.");
                           
                           document.location.reload();
                        }
                     } else {
                        ClientWarning("Assignment failed.");
                     }
                  }
               }
               if (window != top && typeof(parent.AssignedShop) == "function")
                 parent.AssignedShop();
               else
                 window.close();
            }
         }
     }
    /*var co = RSExecute("/rs/RSADSAction.asp", "RetJustExecute", "uspWorkFlowSelectShop", sRequest);
    var retArray = ValidateRS( co );

    if ( retArray[1] == 1 )
    {

      var sAssignmentID = retArray[0];
      if ( gsAssignmentAtSelectionFlag == 1 )
      {
        var coObj = RSExecute("/rs/RSADSAction.asp", "JustExecute", "uspWorkflowAssignShop", "AssignmentID=" + sAssignmentID );
  			ValidateRS(coObj);
        //need to uncomment this when the COM component is ready to take the assignment ID.
        RSExecute( "/rs/RSAssignShop.asp", "AssignShop", sAssignmentID, gsUserID, false, showFaxResults, showFaxErrors, null );
      }

      if (window != top && typeof(parent.AssignedShop) == "function")
        parent.AssignedShop();
      else
        window.close();
    }*/
    gbInAssign = false;
    if (parent.stat1) parent.stat1.Hide();
  }

  
  function SelectThisShop()
  {
    var sReturn = txtShopName.value +", "+ txtShopCity.value +", "+ txtShopState.value;
    parent.window.returnValue = sReturn;
    parent.window.close();
  }
  
  
  function showFaxResults( co )
  {
    var retArray = ValidateRS( co );
    if ( retArray[1] == 1 )
    {
      var sResult = retArray[0];
      var sVanResult = sResult.charAt(0);
      var sFaxResult = sResult.charAt(1);
      if (sVanResult == "F") ClientInfo("Electronic assignment to shop failed.");
      if (sFaxResult == "F") ClientInfo("Fax assignment to shop failed.");
    }
    btnSelectShop.disabled = false;
  }

  function showFaxErrors( co )
  {
      var msg = "The following error occurred during the '" + co.context + "' remote script call: '"
        + co.message + "'/n  The raw data returned by the remote method call is: " + co.data;
      ClientEvent( 51, msg, "ShopList.xsl showFaxErrors()" );
      btnSelectShop.disabled = false;
  }


//if (document.attachEvent)
//  document.attachEvent("onclick", top.hideAllMenuScriptlets);
//else
//  document.onclick = top.hideAllMenuScriptlets;

]]>
</SCRIPT>

</HEAD>
<BODY unselectable="on" class="bodyAPDSubSub" onLoad="initSelectBoxes(); InitFields(); initPage()" style="padding:0px;margin:0px;overflow:hidden;">
<!-- <SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT> -->

<xsl:if test="count(/Root/Shop) > 0">

<!-- <DIV unselectable="on" id="content11" style="width:700px; height:340px; left:0px; top:0px">
  <DIV unselectable="on" style="position:absolute;  width: 7px; left: 2px; top: 3px"> -->

    <xsl:choose>
      <xsl:when test="(/Root/@ShopTypeCode = 'P')" >
        <TABLE unselectable="on" class="ClaimMiscInputs" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center; table-layout:fixed">
          <TBODY>
            <SPAN unselectable="on" onMouseOver="popUp(1)" onMouseOut="popOut()">
              <TR unselectable="on" class="QueueHeader" style="height:20px;">
                <TD unselectable="on" class="TableSortHeader" style="cursor:default;" width="44"> Prog </TD>
                <TD unselectable="on" class="TableSortHeader" style="cursor:default;" width="213"> Name </TD>
                <TD unselectable="on" class="TableSortHeader" style="cursor:default;" width="205"> Address </TD>
                <TD unselectable="on" class="TableSortHeader" style="cursor:default;" width="111"> City </TD>
                <TD unselectable="on" class="TableSortHeader" style="cursor:default;" width="34"> St </TD>
                <TD unselectable="on" class="TableSortHeader" style="cursor:default;" width="55"> Open </TD>
                <TD unselectable="on" class="TableSortHeader" style="cursor:default;" width="30"> Dist </TD>
              </TR>
            </SPAN>
          </TBODY>
        </TABLE>
      </xsl:when>

      <xsl:otherwise>
        <TABLE unselectable="on" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSort1')" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center; table-layout:fixed">
          <TBODY>
            <SPAN unselectable="on" onMouseOver="popUp(1)" onMouseOut="popOut()">
              <TR unselectable="on" class="QueueHeader" style="height:20px">
                <TD unselectable="on" class="TableSortHeader" width="44" type="Image"> Prog </TD>
                <TD unselectable="on" class="TableSortHeader" width="213" type="String"> Name </TD>
                <TD unselectable="on" class="TableSortHeader" width="205" type="String"> Address </TD>
                <TD unselectable="on" class="TableSortHeader" width="111" type="String"> City </TD>
                <TD unselectable="on" class="TableSortHeader" width="34" type="String"> St </TD>
                <TD unselectable="on" class="TableSortHeader" width="55" type="String"> Open </TD>
                <TD unselectable="on" class="TableSortHeader" width="30"  type="Number"> Dist </TD>
              </TR>
            </SPAN>
          </TBODY>
        </TABLE>
      </xsl:otherwise>
    </xsl:choose>
<!--   </DIV> -->

<DIV unselectable="on" id="tblSortDiv" class="autoflowTable" style="width: 700px; height: 40px;">
    <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" cellspacing="1" border="0" cellpadding="2" style="table-layout:fixed">
     <TBODY bgColor1="ffffff" bgColor2="fff7e5">
        <colgroup>
          <col width="41"/>
          <col width="213"/>
          <col width="204"/>
          <col width="109"/>
          <col width="34"/>
          <col width="54"/>
          <col width="27"/>
        </colgroup>
        <xsl:for-each select="Shop">
          <xsl:call-template name="ShopInfo">
          </xsl:call-template>
        </xsl:for-each>
      </TBODY>
    </TABLE>
</DIV>

 <DIV unselectable="on" id="ShopContactInfo"> 
    <TABLE unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="0">
      <TR unselectable="on">
        <TD unselectable="on">Shop Name:</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtShopName" class="InputReadonlyField" size="29" >
            <xsl:attribute name="readonly"/>
          </INPUT>
        </TD>
        <TD unselectable="on">Phone:</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtShopPhone" class="InputReadonlyField" size="18" >
            <xsl:attribute name="readonly"/>
          </INPUT>
        </TD>
        <TD unselectable="on">Contact:</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtShopContactName" class="InputReadonlyField" size="24" >
            <xsl:attribute name="readonly"/>
          </INPUT>
        </TD>
      </TR>
      <TR unselectable="on">
        <TD unselectable="on">Address:</TD>
        <TD rowspan="3" unselectable="on">
          <INPUT type="text" id="txtShopAddress1" class="InputReadonlyField" size="29" >
            <xsl:attribute name="readonly"/>
          </INPUT>
          <br/>
          <INPUT type="text" id="txtShopAddress2" class="InputReadonlyField" size="29" >
            <xsl:attribute name="readonly"/>
          </INPUT>
          <br/>
          <INPUT type="text" id="txtShopCity" class="InputReadonlyField" size="17" >
            <xsl:attribute name="readonly"/>
          </INPUT>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <INPUT type="text" id="txtShopState" class="InputReadonlyField" size="2" >
            <xsl:attribute name="readonly"/>
          </INPUT>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <INPUT type="text" id="txtShopZip" class="InputReadonlyField" size="3" >
            <xsl:attribute name="readonly"/>
          </INPUT>
        </TD>
        <TD unselectable="on">Fax:</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtShopFax" class="InputReadonlyField" size="18" >
            <xsl:attribute name="readonly"/>
          </INPUT>
        </TD>
        <TD unselectable="on">Contact Phone:</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtShopContactPhone" class="InputReadonlyField" size="18" >
            <xsl:attribute name="readonly"/>
          </INPUT>
        </TD>
      </TR>
      <TR unselectable="on">
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on">Hours:</TD>
        <TD rowspan="3" unselectable="on">
          <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
            <TR unselectable="on">
              <TD unselectable="on">Mon<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;-<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Fri:</TD>
              <TD unselectable="on">
                <INPUT type="text" id="txtShopHoursWeekOpen" class="InputReadonlyField" size="4" >
                  <xsl:attribute name="readonly"/>
                </INPUT>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;-<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                <INPUT type="text" id="txtShopHoursWeekClose" class="InputReadonlyField" size="4" >
                  <xsl:attribute name="readonly"/>
                </INPUT>
              </TD>
            </TR>
            <TR unselectable="on">
              <TD unselectable="on">Saturday:</TD>
              <TD unselectable="on">
                <INPUT type="text" id="txtShopHoursSaturdayOpen" class="InputReadonlyField" size="4" >
                  <xsl:attribute name="readonly"/>
                </INPUT>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;-<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                <INPUT type="text" id="txtShopHoursSaturdayClose" class="InputReadonlyField" size="4" >
                  <xsl:attribute name="readonly"/>
                </INPUT>
              </TD>
            </TR>
            <TR unselectable="on">
              <TD unselectable="on">Sunday:</TD>
              <TD unselectable="on">
                <INPUT type="text" id="txtShopHoursSundayOpen" class="InputReadonlyField" size="4" >
                  <xsl:attribute name="readonly"/>
                </INPUT>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;-<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                <INPUT type="text" id="txtShopHoursSundayClose" class="InputReadonlyField" size="4" >
                  <xsl:attribute name="readonly"/>
                </INPUT>
              </TD>
            </TR>
          </TABLE>
        </TD>
        <TD unselectable="on">Distance:</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtShopDistance" class="InputReadonlyField" size="3" >
            <xsl:attribute name="readonly"/>
          </INPUT>
        </TD>
      </TR>
      <TR unselectable="on">
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on">Local Time:</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtShopLocalTime" class="InputReadonlyField" size="9" >
            <xsl:attribute name="readonly"/>
          </INPUT>
        </TD>
      </TR>
      <TR unselectable="on">
        <TD unselectable="on">Shop Open:</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtShopOpen" class="InputReadonlyField" size="10" >
            <xsl:attribute name="readonly"/>
          </INPUT>
        </TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on">Time Zone:</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtShopTimeZone" class="InputReadonlyField" size="20" >
            <xsl:attribute name="readonly"/>
          </INPUT>
        </TD>
      </TR>
    </TABLE>
  </DIV>

   <DIV unselectable="on" id="ShopCommentsInfo"> 
    <TABLE unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="0">
	 <colgroup>
        <col width="100"/>
        <col width="460"/>
        <col width="100"/>	
        <col width="100"/>
       </colgroup>
      <TR>
	      <TD unselectable="on" rowspan="2">
          <xsl:if test="$MEChildWin != 'true'">
            Comments to the Shop:
          </xsl:if>
         </TD>
    	   <TD unselectable="on" rowspan="2">
          <xsl:if test="$MEChildWin != 'true'">
              <TEXTAREA class="TextAreaField" cols="85" rows="2" name="ShopRemarks" id="txtShopRemarks" Maxlength="500"><xsl:value-of select="$ShopRemarks"/></TEXTAREA>
          </xsl:if>
         </TD>
    		 <TD unselectable="on">
          <xsl:choose>
            <xsl:when test="$MEChildWin = 'true'" >
    		      <IE:APDButton id="btnSelectShop" name="btnSelectShop" onButtonClick="SelectThisShop()" value="Select Shop"/>
            </xsl:when>
            <xsl:when test="$AssignmentTo != 'LDAU'" >
    		      <IE:APDButton id="btnSelectShop" name="btnSelectShop" onButtonClick="AssignShop()" value="Select Shop"/>
            </xsl:when>
            <xsl:otherwise>
            </xsl:otherwise>
          </xsl:choose>
    		 </TD>
    		 <TD unselectable="on">
    		   <IE:APDButton id="buttonClose" name="buttonClose" onButtonClick="parent.window.close()" value="Cancel"/>
         </TD>
      </TR>
    </TABLE>
  </DIV>

<!-- </DIV>  -->

</xsl:if>

<xsl:if test="count(/Root/Shop) = 0">
    <SPAN class="boldtext" style="position:absolute; top:10px; left:10px;">
  <xsl:text disable-output-escaping="yes">
      Sorry, no results returned. Please check your search values and try again.
  </xsl:text>
    </SPAN>
</xsl:if>

</BODY>
</HTML>

</xsl:template>

  <!-- Gets the New or Pending Claims -->
  <xsl:template name="ShopInfo">

    <TR unselectable="on" onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' RowType='Claim' onClick='GridClick(this)' onDblClick='GridSelect(this)'>
		<xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
      <TD unselectable="on" class="GridTypeTD" align="center">
        <xsl:choose>
          <xsl:when test="@ShopType = 'Program'">
           <img src="/images/cmark.gif" alt="" width="11" height="11" border="0" title="Program Shop" />
          </xsl:when>
          <xsl:when test="@ShopType = 'CEI'">CEI</xsl:when>
          <xsl:when test="@ShopType != 'Program'">
           <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
        </xsl:choose>
        <xsl:if test="@CerifiedFirstFlag"><img src="/images/CFLogo_small.jpg" alt="" width="11" height="11" border="0" title="CertifiedFirst Shop" /></xsl:if>
      </TD>
      <TD unselectable="on" class="GridTypeTD" style="text-align:left">
        <xsl:choose>
          <xsl:when test="@Name = ''" >
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:when test="string-length(@Name) > 32" >
            <xsl:value-of select="substring(@Name,1,32)"/><xsl:text>...</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@Name"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" style="text-align:left">
        <xsl:choose>
          <xsl:when test="@Address1 = ''" >
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:when test="string-length(@Address1) > 24" >
            <xsl:value-of select="substring(@Address1,1,24)"/><xsl:text>., </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@Address1"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" style="text-align:left">
        <xsl:choose>
          <xsl:when test="@AddressCity = ''" >
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:when test="string-length(@AddressCity) > 16" >
            <xsl:value-of select="substring(@AddressCity,1,16)"/><xsl:text>., </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@AddressCity"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" style="text-align:center">
        <xsl:choose>
          <xsl:when test="@AddressState = ''" >
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@AddressState"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@ShopOpen=''" >
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@ShopOpen"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@Distance=''" >
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@Distance"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>

      <TD style="display:none">
        <xsl:attribute name="SelectedShopRank"><xsl:value-of select="@SelectedShopRank"/></xsl:attribute>
        <xsl:attribute name="SelectedShopScore"><xsl:value-of select="@SelectedShopScore"/></xsl:attribute>
        <xsl:value-of select="@ShopLocationID"/>
      </TD>
      <TD style="display:none"><xsl:value-of select="@Name"/></TD>
      <TD style="display:none"><xsl:value-of select="@Address1"/></TD>
      <TD style="display:none"><xsl:value-of select="@Address2"/></TD>
      <TD style="display:none"><xsl:value-of select="@AddressCity"/></TD>
      <TD style="display:none"><xsl:value-of select="@AddressState"/></TD>
      <TD style="display:none"><xsl:value-of select="@AddressZip"/></TD>
      <TD style="display:none"><xsl:value-of select="@TimeAtShop"/></TD>
      <TD style="display:none"><xsl:value-of select="@TimeZone"/></TD>
      <TD style="display:none"><xsl:value-of select="@OperatingMondayEndTime"/></TD>
      <TD style="display:none"><xsl:value-of select="@OperatingMondayStartTime"/></TD>
      <TD style="display:none"><xsl:value-of select="@OperatingTuesdayEndTime"/></TD>
      <TD style="display:none"><xsl:value-of select="@OperatingTuesdayStartTime"/></TD>
      <TD style="display:none"><xsl:value-of select="@OperatingWednesdayEndTime"/></TD>
      <TD style="display:none"><xsl:value-of select="@OperatingWednesdayStartTime"/></TD>
      <TD style="display:none"><xsl:value-of select="@OperatingThursdayEndTime"/></TD>
      <TD style="display:none"><xsl:value-of select="@OperatingThursdayStartTime"/></TD>
      <TD style="display:none"><xsl:value-of select="@OperatingFridayEndTime"/></TD>
      <TD style="display:none"><xsl:value-of select="@OperatingFridayStartTime"/></TD>
      <TD style="display:none"><xsl:value-of select="@OperatingSaturdayEndTime"/></TD>
      <TD style="display:none"><xsl:value-of select="@OperatingSaturdayStartTime"/></TD>
      <TD style="display:none"><xsl:value-of select="@OperatingSundayEndTime"/></TD>
      <TD style="display:none"><xsl:value-of select="@OperatingSundayStartTime"/></TD>
      <TD style="display:none"><xsl:value-of select="ShopContact/@Name"/></TD>
      <TD style="display:none"><xsl:value-of select="ShopContact/@PhoneAreaCode"/>-<xsl:value-of select="ShopContact/@PhoneExchangeNumber"/>-<xsl:value-of select="ShopContact/@PhoneUnitNumber"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="ShopContact/@PhoneExtensionNumber"/></TD>
      <TD style="display:none"><xsl:value-of select="@PhoneAreaCode"/>-<xsl:value-of select="@PhoneExchangeNumber"/>-<xsl:value-of select="@PhoneUnitNumber"/>-<xsl:value-of select="@PhoneExtensionNumber"/></TD>
      <TD style="display:none"><xsl:value-of select="@FaxAreaCode"/>-<xsl:value-of select="@FaxExchangeNumber"/>-<xsl:value-of select="@FaxUnitNumber"/>-<xsl:value-of select="@FaxExtensionNumber"/></TD>
    </TR>
  </xsl:template>

</xsl:stylesheet>

