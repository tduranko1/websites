<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="PMDShopList">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">

	
<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDShopSearchGetListXML,PMDSearchResults.xsl, 33, testshop79, null, null, null   -->

<HEAD>
  <TITLE>Program Manager Desktop</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
  <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
  </script>

  
  <style>
  	A{
		color:blue
	}
	A:hover{
		color : #B56D00;
		font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
		font-weight : bold;
		font-size : 10px;
		cursor : hand;
	}	
  </style>
  
<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>

</HEAD>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
var saveBG;
<![CDATA[


function SearchSelect(nav, pmid, supFlag, dmid){
  var retVal = nav + "|" + pmid + "|";
	retVal += (supFlag == 1) ? pmid : dmid;
		
	if (nav == "shoplocation")
		retVal += "|" + arguments[4];
	
	parent.window.returnValue = retVal
	parent.window.close();
}

]]>
</SCRIPT>

<BODY class="bodyAPDSubSub" unselectable="on" style="background:transparent;">
<DIV id="CNScrollTable" style="width:100%;" >
<TABLE unselectable="on" id="tblHead" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSort1')" cellspacing="1" border="0" cellpadding="0" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
  <TBODY>
    <TR unselectable="on" class="QueueHeader">
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="0" width="101" nowrap="nowrap" height="20"> Pgm Mgr </TD>
	    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="1" width="220" nowrap="nowrap"> Shop Name </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="2" width="220" nowrap="nowrap"> Address </TD>
	    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="3" width="160" nowrap="nowrap"> City, State </TD>
	    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="4" width="50" nowrap="nowrap"> Zip </TD>
	    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="5" width="90" nowrap="nowrap"> Shop Phone </TD>
	  </TR>
  </TBODY>
</TABLE>

<DIV unselectable="on" class="autoflowTable" style="height:375px;">
  <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" border="0" cellspacing="1" cellpadding="0" style="table-layout:fixed;">
    <TBODY bgColor1="ffffff" bgColor2="fff7e5">
      <xsl:for-each select="Shop" ><xsl:call-template name="ShopListInfo"/></xsl:for-each>
    </TBODY>
  </TABLE>
</DIV>
</DIV>
</BODY>
</HTML>
</xsl:template>

  <!-- Gets the list of shops -->
  <xsl:template name="ShopListInfo">
  	
    <TR unselectable="on" onMouseOut="parent.GridMouseOut(this)" onMouseOver="parent.GridMouseOver(this)">
    <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
      <TD unselectable="on" width="100" class="GridTypeTD" style="text-align:left; font-color:blue;">
	  	  <xsl:attribute name="UserID"><xsl:value-of select="@ProgramManagerUserID"/></xsl:attribute>
		    <xsl:attribute name="UserID"><xsl:value-of select="@SupervisorUserID"/></xsl:attribute>
        <xsl:choose>
			    <xsl:when test="@ProgramManager = ''"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:when>
			    <xsl:otherwise>
    			  <a>					
    				  <xsl:attribute name="href">javascript:SearchSelect("market", <xsl:value-of select="@ProgramManagerUserID"/>, 
    				                                                               <xsl:value-of select="@SupervisorFlag"/>, 
    													                                             <xsl:value-of select="@SupervisorUserID"/>)
    				  </xsl:attribute>
    				  <xsl:value-of select="@ProgramManager"/>
			      </a>
			    </xsl:otherwise>
		    </xsl:choose>
      </TD>
	    <TD unselectable="on" width="220" class="GridTypeTD" style="text-align:left">
        <xsl:choose>
			    <xsl:when test="@Name = ''"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:when>
			    <xsl:otherwise>
			      <a>
				      <xsl:attribute name="href">javascript:SearchSelect("shoplocation", <xsl:value-of select="@ProgramManagerUserID"/>, 
				                                                                         <xsl:value-of select="@SupervisorFlag"/>, 
								  						                                                   <xsl:value-of select="@SupervisorUserID"/>, 
									  					                                                   <xsl:value-of select="@ShopLocationID"/>)
				      </xsl:attribute>
				      <xsl:value-of select="@Name"/>
			      </a>
			    </xsl:otherwise>
		    </xsl:choose>
      </TD>
	  <TD unselectable="on" width="220" class="GridTypeTD" style="text-align:left">
        <xsl:choose>
			<xsl:when test="@Address = ''">
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="@Address"/>
			</xsl:otherwise>
		</xsl:choose>
      </TD>
      <TD unselectable="on" width="160" class="GridTypeTD" style="text-align:left">
	    <xsl:choose>
			<xsl:when test="@AddressCity != '' or @AddressState != ''">
				<xsl:value-of select="@AddressCity"/>
				<xsl:if test="@AddressCity != '' and @AddressState != ''">,<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:if>
				<xsl:value-of select="@AddressState"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
			</xsl:otherwise>
		</xsl:choose>
      </TD>
	  <TD unselectable="on" width="50" class="GridTypeTD" style="text-align:left">
        <xsl:choose>
			<xsl:when test="@AddressZip = ''">
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="@AddressZip"/>
			</xsl:otherwise>
		</xsl:choose>
      </TD>
	  <TD unselectable="on" width="90" class="GridTypeTD" style="text-align:left">
        <xsl:choose>
			<xsl:when test="@Phone = ''">
				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="@Phone"/>
			</xsl:otherwise>
		</xsl:choose>
      </TD>
	</TR>
  </xsl:template>

</xsl:stylesheet>