<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="PMDReinspectCalc">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />
<xsl:param name="mode"/>

<xsl:template match="/Root">  
		
<HTML>
<HEAD>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDReinspectCalcGetDetailXML,PMDReinspectCalc.xsl,471  -->

<TITLE>Line Item Calculator</TITLE>

<LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- The compute function is dependent on the table layout.  It requires the following column inhabitants: -->
	<!-- pct: col1; hrs: col2; unit col4; line: col 6.  Additional rows conforming to this layout can be added maintenance free. -->
<script language="javascript">

<![CDATA[

function btnCompute_onclick(tbl){
	var pct; var hrs; var unit; var line; var obj;
	var total = 0;
	
	for(i=0; i < tbl.rows.length; i++){
		pct = 1; hrs = 1; unit = 0; line = 0;
		
		obj = tbl.rows[i].cells[3].children[0];
		if (obj){ 
			pct = obj.value != "" ? (parseFloat(obj.value)/100) : 1;
			if (obj.value != ""){
				switch(obj.getAttribute("discount")){
					case "0":
						pct = 1 + pct;
						break;
					case "1":
						pct = 1 - pct;
						break;						
				}
			}
		}
		
		obj = tbl.rows[i].cells[4].children[0];
		if (obj){ 
			if (obj.value != "")
				hrs = obj.value;
			else{ 
				if (obj.getAttribute("default"))
					hrs = obj.getAttribute("default");
				else 
					hrs = 0;
			}
		}		
		
		obj = tbl.rows[i].cells[6].children[0];
		if (obj) unit = obj.value != "" ? obj.value : 0;
		
		line = FormatMoney(pct * hrs * unit);
				
		if(obj) tbl.rows[i].cells[8].innerText = line;
		total += parseFloat(line);		
	}
	
	tdTotal.innerText = FormatMoney(total);	
}

function btnClear_onclick(tbl){
	var aInputs = document.all.tags("INPUT");
    for (i=0; i < aInputs.length; i++)
		if (aInputs[i].type == "text" && aInputs[i].getAttribute("preset") != 1) aInputs[i].value = "";
		
	for (i=0; i<tbl.rows.length; i++){
		var obj = tbl.rows[i].cells[8]
		if (obj.getAttribute("subtotal") == 1)
			obj.innerText = "";
	}	
	
	tdTotal.innerText = "";	
}	

function btnReturn_onclick(){
	var mode = rdoReI.checked ? 'rei' : 'est';
	window.returnValue =  tdTotal.innerText + "|" + mode;
	window.close();
}

function CheckMoney(event){
	var obj = eval("document.all." + event.srcElement.id);
	//return

	if (obj.value > 1000000){
		event.returnValue = false;
		return;
	}	
	
	var idx = obj.value.indexOf(".")
	
	if ((event.keyCode < 48  && event.keyCode != 46) || event.keyCode > 57){
		event.returnValue = false;
		return;
	}
		
	if (idx > -1){
		//if (obj.value.length - idx > 2){	// allow only two decimal places
		//	event.returnValue = false;
		//	return;
		//}
		
		if (event.keyCode == 46){			// allow only one decimal point
			event.returnValue = false;
			return;
		}
	}
	else{
		if (obj.value.length > 6){
			event.returnValue = false;
			return;
		}
	}	
}

function FormatMoney(num){
	var idx;
	
	num = ((Math.round(num * 100))/100).toString();	
	
	idx = num.indexOf(".")
	if (idx == -1) 	
		num += ".00";
	else{ 
		if (num.length - idx <= 2)
			num += "0";	
	}
	return num;
}

]]>


</script>


</HEAD>
<BODY class="bodyAPDSub" unselectable="on" bgcolor="#FFFAEB" style="margin-right:none">

<div id="divCalc" style="position:absolute; top:5; left:5;">
<table id="tblCalc" cellspacing="0" cellpadding="0" border="0" style="table-layout:fixed">
  <colgroup>
    <col width="120"/>
  	<col width="70"/>
  	<col width="35" align="left"/>
    <col width="45"/>
    <col width="40"/>
  	<col width="15"/>
  	<col width="70"/>
  	<col width="20" align="right"/>
  	<col width="70" align="right"/>
  </colgroup>
  <tr >
    <td  class="tdHeaderBlueTop" width="120">Materials</td>
  	<td  class="tdHeaderBlueTop" align="center">Database</td>
  	<td  class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    <td  class="tdHeaderBlueTop" align="center">%</td>
    <td  class="tdHeaderBlueTop" align="center">Hrs</td>
  	<td  class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td  class="tdHeaderBlueTop" align="center">Price/Rate</td>
  	<td  class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td  class="tdHeaderBlueTop" align="center">ReI</td>
  </tr>
  <tr>
    <td>Parts</td>
  	<td></td>
  	<td></td>
  	<td></td>
  	<td></td>
    <td></td>
    <td><input type="text" id="txtParts" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td>$</td>
  	<td subtotal="1"></td>
  </tr>
  <tr>
    <td>Parts w/ Markup</td>
  	<td>
  	  <input type="text" id="txtDBPartsMarkUpPct" class="inputFld" preset="1" readonly="readonly" style="text-align:right; width:100%; background-color:#dddddd;">
  	    <xsl:attribute name="value"><xsl:value-of select="Pricing/@RecycledPartsMarkUpPct * 1.00 "/></xsl:attribute>
  	  </input>		
  	</td>
  	<td>%</td>
      <td><input type="text" id="txtPartsMarkUpPct" discount="0" class="inputFld" onkeypress="CheckMoney(event)" onpaste="return false" style="text-align:right; width:40;"/></td>
  	<td></td>
  	<td></td>
      <td><input type="text" id="txtPartsWithMarkUp" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td></td>
  	<td subtotal="1"></td>
  </tr>
  <tr>
    <td>Parts w/ Discount</td>
  	<td>
  	  <input type="text" id="txtDBPartsDiscountPct" class="inputFld" preset="1"  readonly="readonly" style="text-align:right; width:100%; background-color:#dddddd;">
  	    <xsl:if test="Pricing/@PartsDiscount != ''">
  		  <xsl:attribute name="value"><xsl:value-of select="Pricing/@PartsDiscount * 1.00 "/>
  		</xsl:attribute></xsl:if>
  	  </input>	
  	</td>
  	<td>%</td>
      <td><input type="text" id="txtPartsDiscountPct" discount="1" class="inputFld" onkeypress="CheckMoney(event)" onpaste="return false" style="text-align:right; width:40;"/></td>
  	<td></td>
  	<td></td>
      <td><input type="text" id="txtPartsWithDiscount" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td></td>
  	<td subtotal="1"></td>
  </tr>
  <tr>
    <td>Body Supplies</td>
	  <td></td>
    <td></td>
	  <td></td>
    <td><input type="text" id="txtBodySuppliesHrs" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td>@</td>
  	<td><input type="text" id="txtBodySupplies" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td></td>
  	<td subtotal="1"></td>
  </tr>
  <tr>
    <td>Paint Supplies</td>
	  <td></td>
    <td></td>
	  <td></td>
    <td><input type="text" id="txtPaintSuppliesHrs" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td>@</td>
  	<td><input type="text" id="txtPaintSupplies" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td></td>
  	<td subtotal="1"></td>
  </tr>  
  <tr>
    <td><img src="/images/spacer.gif" border="0" width="3px" height="4px"/></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td class="tdHeaderBlueTop">Labor</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr>
    <td>Body</td>
  	<td>
  	  <input type="text" id="txtDBBodyRate" class="inputFld" readonly="readonly" preset="1" style="text-align:right; background-color:#dddddd; width:100%;">
  		<xsl:attribute name="value"><xsl:value-of select="Pricing/@Body"/></xsl:attribute>
  	  </input>
  	</td>
  	<td></td>
  	<td></td>
  	<td><input type="text" id="txtBodyHrs" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
    <td>@</td>
    <td><input type="text" id="txtBodyRate" class="inputFld" onkeypress="CheckMoney(event)" onpaste="return false" style="text-align:right; width:100%;"/></td>
  	<td>$</td>
  	<td subtotal="1"></td>
  </tr>
  <tr>
    <td>Mechanical</td>
  	<td>
  	  <input type="text" id="txtDBMechanicalRate" class="inputFld" readonly="readonly" preset="1" style="text-align:right; background-color:#dddddd; width:100%;">
  		<xsl:attribute name="value"><xsl:value-of select="Pricing/@Mechanical"/></xsl:attribute>
  	  </input>
  	</td>
  	<td></td>
    <td></td>
  	<td><input type="text" id="txtMechanicalHrs" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td>@</td>
    <td><input type="text" id="txtMechanicalRate" class="inputFld" onkeypress="CheckMoney(event)" onpaste="return false" style="text-align:right; width:100%;"/></td>
  	<td></td>
  	<td subtotal="1"></td>
  </tr>
  <tr>
    <td>Frame</td>
  	<td>
  	  <input type="text" id="txtDBFrameRate" class="inputFld" readonly="readonly" preset="1" style="text-align:right; background-color:#dddddd; width:100%;">
  		<xsl:attribute name="value"><xsl:value-of select="Pricing/@Frame"/></xsl:attribute>
  	  </input>
  	</td>
  	<td></td>
    <td></td>
  	<td><input type="text" id="txtFrameHrs" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td>@</td>
    <td><input type="text" id="txtFrameRate" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td></td>
  	<td subtotal="1"></td>
  </tr>
  <tr>
    <td>Refinish</td>
  	<td>
  	  <input type="text" id="txtDBRefinishRate" class="inputFld" readonly="readonly" preset="1" style="text-align:right; background-color:#dddddd; width:100%;">
  		<xsl:attribute name="value"><xsl:value-of select="Pricing/@Refinish"/></xsl:attribute>
  	  </input>
  	</td>
  	<td></td>
    <td></td>
    <td><input type="text" id="txtRefinishHrs" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td>@</td>
  	<td><input type="text" id="txtRefinishRate" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td></td>
  	<td subtotal="1"></td>
  </tr>
  <tr>
    <td>Paint</td>
  	<td></td>
  	<td></td>
    <td></td>
    <td><input type="text" id="txtPaintHrs" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td>@</td>
  	<td><input type="text" id="txtPaintRate" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td></td>
  	<td subtotal="1"></td>
  </tr>  
  <tr>
    <td><img src="/images/spacer.gif" border="0" width="3px" height="4px"/></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
  	<td class="tdHeaderBlueTop">Other</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop">
      <input type="text" id="txtOtherRate" class="inputFld" onkeypress="CheckMoney(event)" onpaste="return false" style="text-align:right; width:40;"/>
    </td>
  	<td class="tdHeaderBlueTop">
      <input type="text" id="txtOtherHrs" default="1" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/>
    </td>
  	<td style="border-top: .02cm solid darkblue;">@</td>
  	<td class="tdHeaderBlueTop">
      <input type="text" id="txtOther" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/>
    </td>
  	<td style="border-top: .02cm solid darkblue;">$</td>
  	<td style="border-top: .02cm solid darkblue;" subtotal="1"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" border="0" width="3px" height="4px"/></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td class="tdHeaderBlueTop">Taxes</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="tdHeaderBlueTop"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr>
  	<td>Parts Net Total</td>
  	<td></td>
  	<td></td>
  	<td><input type="text" id="txtPartsTaxRate" class="inputFld" onkeypress="CheckMoney(event)" onpaste="return false" style="text-align:right; width:40;"/></td>
  	<td></td>
  	<td></td>
  	<td><input type="text" id="txtTaxParts" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td>$</td>
  	<td subtotal="1"></td>
  </tr>
  <tr>
  	<td>Labor Net Total</td>
  	<td></td>
  	<td></td>
  	<td><input type="text" id="txtLaborTaxRate" class="inputFld" onkeypress="CheckMoney(event)" onpaste="return false" style="text-align:right; width:40;"/></td>
  	<td></td>
  	<td></td>
  	<td><input type="text" id="txtTaxLabor" class="inputFld" style="text-align:right; width:100%;" onkeypress="CheckMoney(event)" onpaste="return false"/></td>
  	<td></td>
  	<td subtotal="1"></td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" border="0" width="3px" height="4px"/></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>

<table cellspacing="0" cellpadding="0" border="0" style="table-layout:fixed;">
  <colgroup>
	  <col width="400"/>
	  <col width="15" align="right"/>
	  <col width="70" align="right"/>
  </colgroup>
  <tr>
    <td class="tdHeaderBlue">Total</td>
	  <td class="tdHeaderBlue">$</td>
    <td class="tdHeaderBlue" id="tdTotal"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr><td height="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td></tr>
  <tr>
    <td colspan="3" align="right" class="tdHeaderCenter">
  	  <input type="radio" id="rdoEstimate" name="rdoMode"><xsl:if test="$mode='est'"><xsl:attribute name="checked"/></xsl:if></input>Est. Amt.
  	  <input type="radio" id="rdoReI" name="rdoMode"><xsl:if test="$mode='rei'"><xsl:attribute name="checked"/></xsl:if></input>ReI Amt.
  	  <IMG src="/images/spacer.gif" width="50" height="6" border="0" />
  	  <input type="button" id="btnReturn" class="formButton" value="Return" onclick="btnReturn_onclick()" style="width:80"/>
  	  <input type="button" id="btnClear" class="formButton" value="Clear" onclick="btnClear_onclick(tblCalc)" style="width:80"/>
  	  <input type="button" id="btnCompute" class="formButton" value="Compute" onclick="btnCompute_onclick(tblCalc)" style="width:80"/>
  	  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp; 
	  </td>
  </tr>
  <tr>
    <td height="3" colspan="3" style="border-bottom: .02cm solid darkblue;"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
</table>
</div>
</BODY>
</HTML>

</xsl:template>
</xsl:stylesheet>