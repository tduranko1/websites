<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:user="http://mycompany.com/mynamespace"
		xmlns:session="http://lynx.apd/session-manager"
		id="PMDMain">

<xsl:import href="msxsl/generic-template-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSessionGetUserDetailXML,PMD.xsl, csr0901   -->

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="Environment"/>
<xsl:param name="WindowID"/>
<xsl:param name="SearchType"/>
<xsl:param name="EntityID"/>
<xsl:param name="ClaimView"/>
<xsl:param name="LynxID"/>


<xsl:include href="APDMenu.xsl"/>

<xsl:template match="/Root">

<xsl:variable name="Context" select="session:XslUpdateSession( 'Context', '' )"/>
<xsl:variable name="ViewCRD" select="session:XslUpdateSession( 'ViewCRD', string(User/DesktopPermission/@CRD) )"/>
<xsl:variable name="ViewPMD" select="session:XslUpdateSession( 'ViewPMD', string(User/DesktopPermission/@PMD) )"/>
<xsl:variable name="ViewUAD" select="session:XslUpdateSession( 'ViewUAD', string(User/DesktopPermission/@UAD) )"/>
<xsl:variable name="ViewMED" select="session:XslUpdateSession( 'ViewMED', string(User/DesktopPermission/@MED))"/>

<!-- Make sure we have a valid user -->
<xsl:if test="count(User)=1">

	<xsl:variable name="UserID" select="User/@UserID"/>
	<xsl:variable name="SupervisorUserID" select="User/@SupervisorUserID"/>
	<xsl:variable name="SuperUserFlag" select="User/@SuperUserFlag"/>
	<xsl:variable name="SupervisorFlag" select="User/@SupervisorFlag"/>
	<xsl:variable name="RoleID" select="User/Role[@PrimaryRoleFlag = '1']/@RoleID"/>
	<xsl:variable name="RoleName" select="User/Role[@PrimaryRoleFlag = '1']/@Name"/>
    
  <xsl:value-of select="session:XslUpdateSession( 'UserID', string($UserID))"/>
  <xsl:value-of select="session:XslUpdateSession( 'UserRole', string($RoleName) )"/>
  <xsl:value-of select="session:XslUpdateSession( 'UserRoleID', string($RoleID) )"/>
  <xsl:value-of select="session:XslUpdateSession( 'SupervisorUserID', string($SupervisorUserID) )"/>
  <xsl:value-of select="session:XslUpdateSession( 'SupervisorFlag', string($SupervisorFlag) )"/>

	<HTML>

    <HEAD>
      <TITLE id="tTitle"><xsl:value-of select="$EntityID"/> Shop Maintenance Desktop - <xsl:value-of select="$Environment"/></TITLE>
      <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
	    <LINK rel="stylesheet" href="/css/Menu.css" type="text/css"/>
	    <LINK rel="stylesheet" href="/css/officexp/Menu.css" type="text/css" id="menuStyleSheet"/>

	  <script type="text/javascript" src="/js/menu3.js"></script>
	  <script type="text/javascript" src="/js/PMD.js"></script>
    <script type="text/javascript" src="/js/windowUtils.js"></script>
    <script type="text/javascript" src="/js/PMDNavigate.js"></script>
    <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
    <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
    </script>
	
    <!-- Page Specific Scripting -->
    <SCRIPT language="JavaScript">
    
    var oCallingWin;
    var gsShopLoadID="";
    var gsID="";
	  var gsSSN="";
	  var gsName="";
	  var gsCity="";
	  var gsZip="";
    var gsState="";
	  var gsPhoneArea="";
	  var gsPhoneExchange="";
	  var gsPhoneUnit="";
	  var gsProgramShop="";
    var gsProgramShopIndexSMT="";
    var gsProgramShopIndexWeb="";
	  var gsSpecialty="";
	  var gsSearchType= "<xsl:value-of select='$SearchType'/>";
	  var gsCancelSearchType="";
	  var gsInfoName = "";
	  var gsInfoAddress = "";
	  var gsInfoCity = "";
	  var gsInfoPhone = "";
	  var gsScroll;
	  var gsBusinessParentID
	  var gsBusinessInfoID = "";
	  var gsShopID = "<xsl:value-of select="$EntityID"/>";
	  var gsBillingID = 0;
	  var gsDealerID = "";
	  var gsPageFile;
	  var gsFramePageFile;
	  var gsOrigin = "";
	  var gbInfo = false;
    var gsEIN="";
    var gsCertifiedFirstID="";
    var gsPageID;
    var gsEntityID="<xsl:value-of select='$EntityID'/>";
    var vWindowInitTimer = "uninitialized";
    var gbLoadingNotes = false;
    var gsWindowID = "<xsl:value-of select='string($WindowID)'/>";
    var gsUserID = "<xsl:value-of select='$UserID'/>";
    var gbShopOpen = false;
    var gbLoadingDocs = false;
        
	  function CancelURL(){
	  	var sURL = "/ProgramMgr/" + gsFramePageFile;
		  gsOrigin = "wiz";
      
      switch(gsFramePageFile){
			  case "SMTDetailLevel.asp":
				  sURL += "?SearchType=" + gsCancelSearchType + "&amp;BusinessInfoID=" + gsBusinessInfoID + "&amp;ShopID=" + gsShopID;
				  break;
			  case "SMTDealerDetailLevel.asp":
				  sURL += "?DealerID=" + gsDealerID + "&amp;PageID=" + gsPageID;
          break;
        case "SMTMain.asp":
          sURL += "?PageID=" + gsPageID;
          break;
          
		  }
		  return sURL;
	  }

    //init the table selections, must be last
    function initPage(){
      self.resizeTo("1022", "699");
      //alert(window.opener.parent.Search());
      
      //document.frames["ifrmContent"].
      // initial window will be named 'APD...".  Subsequent windows produced from clicking a serarch result will be named 'SMT...'.
      if (window.name.substring(0, 3) == "APD")    
        document.frames["oIframe"].frameElement.src = "SMTMain.asp?PageID=SMT";
      else{
        var sUrl;
        switch (gsSearchType){
          case "S":
            sUrl = "SMTDetailLevel.asp?SearchType=S&amp;BusinessInfoID=&amp;ShopID=" + gsEntityID;
            break;
          case "B":
            sUrl = "SMTDetailLevel.asp?SearchType=B&amp;ShopID=&amp;BusinessInfoID=" + gsEntityID;
            break;
          case "D":
            sUrl = "SMTDealerDetailLevel.asp?DealerID=" + gsEntityID;
            break;
          case "BusinessWiz":
            sUrl = "SMTWizard.asp?mode=businfo";
            break;
          case "ShopWiz":
            sUrl = "SMTWizard.asp?mode=shop";
            break;
          case "DealerWiz":
            sUrl = "SMTDealerDetailLevel.asp?mode=wizard";
            break;          
        }
              
        document.frames["oIframe"].frameElement.src = sUrl;
        
        vWindowInitTimer = window.setInterval( "initSubWindows()", 1000 );          
      }
      
      
        
      obj = document.getElementById("oIframe");
      obj.style.visibility = "visible";
      obj.style.display = "inline";
    }

   function SetMainTab(sTab){
   	 imgMainTab1.src = "/images/shop_des.gif";
  	 imgMainTab2.src = "/images/business_des.gif";
  	 imgMainTab3.src = "/images/par_des.gif";
  
  	 switch(sTab){
  		 case "shop":
  			 imgMainTab1.src = "/images/shop_sel.gif";
  			 break;
  		 case "businfo":
  			 imgMainTab2.src = "/images/business_sel.gif";
  			 break;
  		 case "parent":
  			 imgMainTab3.src = "/images/par_sel.gif";
  			 break;
     }
       
     ifrmPMDShopSearch.SetMainTab(sTab);
   }


  function SetInfoHeader(sEntityType, sEntityID, sName, sAddress, sCity, sPhone){
    document.getElementById("layerEntityData").style.visibility = "visible";
    document.getElementById("tdIDLabel").innerText = sEntityType;
    document.getElementById("tdEntityID").innerText = sEntityID;

    if (sName.length > 22) sName = sName.substr(0,22) + "...";
    document.getElementById("tdName").innerText = sName;

  	if (sAddress.length > 22) sAddress = sAddress.substr(0,22) + "...";
      document.getElementById("tdAddress").innerText = sAddress;
  
  	if (sCity.length > 22) sCity = sCity.substr(0,22) + "...";
      document.getElementById("tdCity").innerText = sCity;
  
    document.getElementById("tdPhone").innerText = sPhone;
  
  	layerEntityData.style.visibility = "visible";
  	gbInfo = true;
  }

  function AdminMenu(bEnable, nMenu){
    var oMenu = document.getElementById("adminMenu");
    if (bEnable == true)
      oMenu.rows[nMenu-1].className="";
	  else
      oMenu.rows[nMenu-1].className="disabled";
	  forceRebuild(oMenu);
  }

  function SwitchMenuSearchItem(sItem, nMenu){
    var oMenu = document.getElementById("adminMenu");
    var sState = "";
    
    if (sItem == "web"){
      oMenu.rows[nMenu-1].cells[1].innerText = "Main Search";
      oMenu.rows[nMenu-1].href = "/ProgramMgr/SMTMain.asp?PageID=SMT";
      sState = "disabled";
    }
    else{
      oMenu.rows[nMenu-1].cells[1].innerText = "Web Signups";
      oMenu.rows[nMenu-1].href = "/ProgramMgr/SMTMain.asp?PageID=WebSignups";
    }
    
    for (--nMenu; nMenu > 0; nMenu--){
      if (nMenu != 2)
        oMenu.rows[nMenu-1].className = sState;
   }
    
    forceRebuild(oMenu);
  }
  
  function StringPrep(s){
  	return s.replace(/\|/g, "\'");
  }

  //SMT doesn't have a save button, so its not appropriate to check for dirty flag and display the msg
  function chkBeforeUnloadWindow(){
 //   if (oIframe.gbDirtyFlag == true)
 //    event.returnValue = "The information on this page has changed.";
  }

  if (document.attachEvent)
  	document.attachEvent("onclick", top.hideAllMenuScriptlets);
  else
    document.onclick = top.hideAllMenuScriptlets;

  <![CDATA[
    
  function initSubWindows()
  {
    try
    {
      // The following statement will throw if undefined.
      // This allows us to hold off on filling the sub windows
      // until we know for sure that remote scripting is ready.
      if ( MSRS != null ){
        window.clearInterval( vWindowInitTimer );
        vWindowInitTimer = "";
        
        if (gsShopID != "") gbShopOpen = true;
        
        // Shop Window.
        //if ( bClaimRepDesktop == true ){
          refreshNotesWindow( gsShopID, gsUserID );
          refreshDocumentsWindow( gsShopID, gsUserID );
          window_columnEqual();
        //}
      }
    }
    catch ( e ) {}
  }  
  
  // Rebuilds the notes window.
  function refreshNotesWindow(sShopLocationID, sUserID)
  {
      // Wrapped to ensure following command always executes.
      try
      {
          // Logic to reduce multiple loads at initial page build.
          if ( vWindowInitTimer == "" && !gbLoadingNotes )
          {
              gbLoadingNotes = true;
              if ( winNotes )
                  winNotes.loadContent("/ProgramMgr/SMTNotes.asp?WindowID=" + gsWindowID + "&ShopLocationID=" + sShopLocationID + "&UserID=" + sUserID);
          }
      } catch ( e ) { }

      gbLoadingNotes = false;
  }

  // Rebuilds the documents window.
  function refreshDocumentsWindow(sShopLocationID, sUserID)
  {
      // Wrapped to ensure following command always executes.
      try
      {
          // Logic to reduce multiple loads at initial page build.
          if ( vWindowInitTimer == "" && !gbLoadingDocs )
          {
              gbLoadingDocs = true;
              if ( winDocs )
                  winDocs.loadContent("/ProgramMgr/SMTDocuments.asp?WindowID=" + gsWindowID + "&ShopLocationID=" + sShopLocationID + "&UserID=" + sUserID);
          }
      } catch ( e ) { }

      gbLoadingDocs = false;
  }
  
  ]]>
  
 </SCRIPT>

</HEAD>
<BODY unselectable="on" onLoad="initPage();" onbeforeunload="chkBeforeUnloadWindow()" style="background-color:#336699; color:#000000; font-family:Tahoma,Arial,Helvetica,sans-serif;	font-size:11px;	background-image:url(/images/fundo_default.gif); margin:0px; border:0px;">
  <SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
  <SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary");</SCRIPT>

  <TABLE unselectable="on" width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
    <TR>
  	  <TD unselectable="on" width="100%">
           
      
        <xsl:call-template name="APDMenu"><!-- Menu bar -->
          <xsl:with-param name="Desktop">
            <xsl:choose>
              <xsl:when test="contains($SearchType,'Wiz')">WIZ</xsl:when>
              <xsl:when test="$EntityID=''">SMT</xsl:when>
              <xsl:when test="$SearchType='D'">DEA</xsl:when>
              <xsl:otherwise>SHO</xsl:otherwise>
            </xsl:choose>
          </xsl:with-param>
		      <xsl:with-param name="Permission"><xsl:value-of select="User/DesktopPermission/@Territory"/></xsl:with-param>
		    </xsl:call-template>
	  </TD>
    </TR>
    <TR>
  	  <TD unselectable="on" width="100%"><IMG unselectable="on" src="/images/spacer.gif" width="1" height="4" border="0" alt=""/></TD>
    </TR>
    <TR>
  	  <TD unselectable="on" height="100%" width="100%">
        <IFRAME id="ifrmPMDShopSearch" name="oIframe" src="/blank.asp" style="width:765; height:550; top:25; border:0px; padding:2px; visibility:hidden; position:absolute; background:transparent; overflow:hidden" allowtransparency="true" ></IFRAME>
      </TD>
    </TR>
    <TR>
  	  <TD unselectable="on"><IMG unselectable="on" src="/images/spacer.gif" width="1" height="4" border="0" alt=""/></TD>
    </TR>
    <TR>
  	  <TD unselectable="on" width="100%"><img src="/images/apdlogo.gif" alt="" width="154" height="36" hspace="10" vspace="10" border="0"/></TD>
    </TR>
  </TABLE>

<!--include the Tab Folder Info DIV's-->
  <DIV unselectable="on" id="layerTabs" style="position:absolute; left: 4px; top: 573px; width: 730px; height: 36px; visibility: hidden" border="0" cellspacing="0" cellpadding="0">
	<TABLE unselectable="on" cellpadding="0" cellspacing="0" border="0">
	  <TR>
	    <TD unselectable="on">
		  <IMG unselectable="on" id="imgMainTab1" src="/images/shop_sel.gif" style="cursor:hand" width="93" height="19" border="0" onClick="SetMainTab('shop');" onmouseover="window.status='Shop Info'; return true" onmouseout="window.status=''; return true" onFocus="if(this.blur)this.blur()"/>
	    </TD>
	    <TD unselectable="on">
		  <IMG unselectable="on" id="imgMainTab2" src="/images/bus_info_des.gif" style="cursor:hand" width="90" height="19" border="0" onClick="SetMainTab('businfo');" onmouseover="window.status='Business Info'; return true" onmouseout="window.status=''; return true" onFocus="if(this.blur)this.blur()"/>
		</TD>
		<TD unselectable="on">
		  <IMG unselectable="on" id="imgMainTab3" src="/images/par_des.gif" style="cursor:hand" width="90" height="19" border="0" onClick="SetMainTab('parent');" onmouseover="window.status='Parent Info'; return true" onmouseout="window.status=''; return true" onFocus="if(this.blur)this.blur()"/>
		</TD>
	  </TR>
	</TABLE>
  </DIV>


  <!--include the Info Header Info DIV-->
    <DIV unselectable="on" id="layerEntityData" style="position:absolute; width:200px; height:93px; z-index:1; left: 557px; top: 575px; visibility: hidden;">
      <TABLE unselectable="on" id="tblHeader" width="100%" height="93" border="0" cellspacing="0" cellpadding="0" background="/images/SMTfolder.gif">
        <TR>
          <TD unselectable="on" valign="top" nowrap="nowrap">
            <IMG unselectable="on" src="/images/spacer.gif" width="160" height="14" border="0"/>
          </TD>
       </TR>
        <TR>
          <TD unselectable="on" valign="top" nowrap="nowrap">
            <SPAN unselectable="on" id="tdIDLabel" class="boldText"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</SPAN><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<SPAN unselectable="on" id="tdEntityID"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</SPAN>
          </TD>
        </TR>
		<TR>
          <TD unselectable="on" valign="top" nowrap="nowrap">
            <SPAN unselectable="on" class="boldText"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Name:</SPAN><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<SPAN unselectable="on" id="tdName"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</SPAN>
          </TD>
        </TR>
        <TR>
          <TD unselectable="on" valign="top" colspan="2">
            <SPAN unselectable="on" class="boldText"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Address:</SPAN><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<SPAN unselectable="on" id="tdAddress"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</SPAN>
          </TD>
        </TR>
        <TR>
          <TD unselectable="on" valign="top" colspan="2" nowrap="nowrap">
            <SPAN unselectable="on" class="boldText"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;City:</SPAN><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<SPAN unselectable="on" id="tdCity"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</SPAN>
          </TD>
        </TR>
        <TR>
          <TD unselectable="on" valign="top" colspan="2" nowrap="nowrap">
            <SPAN unselectable="on" class="boldText"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Phone:</SPAN><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<SPAN unselectable="on" id="tdPhone"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</SPAN>
          </TD>
        </TR>
      </TABLE>
    </DIV>
  
    <!-- create note window for business and shop windows only -->
    <script>
       if (gsEntityID != "" &amp;&amp; gsSearchType != "D") createSubWindow("winNotes", true, "Notes", "win_logo2.gif", 320000);  // ~ 5.3 min
       if (gsEntityID != "" &amp;&amp; gsSearchType != "D") createSubWindow("winDocs", true, "Documents", "win_logo2.gif", 630555);  // ~ 10.5 min
    </script>
  
</BODY>
</HTML>



</xsl:if>

<xsl:if test="count(User)=0">
	<xsl:text>Please check your login information.  If you continue to have problems, contact your supervisor.</xsl:text>
</xsl:if>
</xsl:template>
</xsl:stylesheet>
