<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTMoveShopSearch">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="Environment"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID             SP                          XSL             PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTShopSearchReferenceGetListXML,SMTMoveShopSearch.xsl   -->


<HEAD>
  <TITLE>Shop Maintenance</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
var gsPageFile = "SMTBusinessSearch.asp";

<![CDATA[
//init the table selections, must be last
function initPage(){
	top.gsPageFile = gsPageFile;
	parent.tab13.className = "tabactive1";
	parent.btnDelete.style.visibility = "hidden";
	parent.btnSave.style.visibility = "visible";
  parent.btnSave.disabled = true;
  txtExclusionEntityID.value = top.gsBusinessInfoID;

	txtName.value = parent.gsSearchName;
	txtCity.value = parent.gsSearchCity;
	selState.value = parent.gsSearchState;
	txtPhoneAreaCode.value = parent.gsSearchPhoneAreaCode;
	txtPhoneExchange.value = parent.gsSearchPhoneExchange;
	txtPhoneUnit.value = parent.gsSearchPhoneUnit;
	selShopType.value = parent.gsSearchShopType;
  selSpecialties.value = parent.gsSearchSpecialties;

  parent.btnSave.disabled = false;
}

function Search(){
	if (isNaN(txtBusinessInfoID.value)){
		parent.ClientWarning("Please enter a numeric value for 'ID'.");
		txtBusinessInfoID.select();
		return;
	}

	SearchIndicator();
	document.frames["ifrmShopList"].frameElement.style.visibility="hidden";

  parent.gsSearchID = txtBusinessInfoID.value;
	parent.gsSearchName = txtName.value;
	parent.gsSearchCity = txtCity.value;
	parent.gsSearchState = selState.value;
	parent.gsSearchPhoneAreaCode = txtPhoneAreaCode.value;
	parent.gsSearchPhoneExchange = txtPhoneExchange.value;
	parent.gsSearchPhoneUnit = txtPhoneUnit.value;
  parent.gsSearchShopType = selShopType.value;
  parent.gsSearchSpecialties = selSpecialties.value;

	var qString = "";
	var aInputs = document.all.tags("INPUT");
    for (var i=0; i < aInputs.length; i++) {
		var sType = aInputs[i].type;
    if (sType == "text" || sType == "hidden"){
			val = escape(aInputs[i].value.replace(/'/g, "''"));
			qString += aInputs[i].name + "=" + val.substr(0, aInputs[i].maxLength) + "&";
		}
  }

	aInputs = document.all.tags("SELECT");
  for (var i=0; i < aInputs.length; i++) {
    qString += aInputs[i].name + "=" + aInputs[i].value + "&";
  }

  qString += "page=businessshop";
  document.frames["ifrmShopList"].frameElement.src = "SMTBusinessSearchResults.asp?" + qString
}

function NumbersOnly(event){
	if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;
}

function Save(){
	ifrmShopList.Save();
}

function ShowCount(count){
	var sCountMsg = count + " Shop";
	if (count == '0' || count > '1') sCountMsg += "s";
	sCountMsg += " Found";
	lblCount.innerText = sCountMsg;
	lblSearching.style.display = "none";
	lblCount.style.display = "inline";
	btnClear.disabled = false;
}

function SearchIndicator(){
	parent.btnSave.disabled = true;
	btnSearch.disabled = true;
	btnClear.disabled = true;
	lblCount.style.display = "none";
	lblSearching.style.display = "inline";
	lblSearching.innerText = "Searching";
	gsSearchingIntervalID = window.setInterval("if (lblSearching.innerText == 'Searching.......') lblSearching.innerText = 'Searching'; lblSearching.innerText += '.'", 750);
}

function Clear(){
	txtBusinessInfoID.value = "";
	txtName.value = "";
	txtCity.value = "";
	selState.selectedIndex = 0;
	txtPhoneAreaCode.value = "";
	txtPhoneExchange.value = "";
	txtPhoneUnit.value = "";
  selShopType.selectedIndex = 0;
  selSpecialties.selectedIndex = 0;
	lblSearching.style.display = "none";
	lblCount.style.display = "none";
}

]]>

</SCRIPT>
</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" style="background:transparent;" onLoad="initPage();" onkeypress="if (event.keyCode == 13) Search();">

<TABLE unselectable="on" width="720" class="ShopAssignTableSrh" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td valign="top">
      <table border="0" cellspacing="0" cellpadding="1" width="100%">
        <colgroup>
          <col width="65"/>
          <col width="100%"/>
        </colgroup>
        <tr>
          <td nowrap="nowrap">ID:</td>
          <td nowrap="nowrap">
            <input type="hidden" name="SearchType" value="S"/>
            <input type="hidden" name="ExclusionEntityID" id="txtExclusionEntityID"/>
            <input type="hidden" name="ExclusionType" value="S"/>
            <input type="text" name="EntityID" id="txtBusinessInfoID" size="9" maxLength="8" class="inputFld" onkeypress="NumbersOnly(window.event)"/>
          </td>
        </tr>
        <tr>
          <td nowrap="nowrap">Name:</td>
          <td nowrap="nowrap">
            <input type="text" class="inputFld" name="Name" id="txtName" size="51" maxLength="50"/>
          </td>
        </tr>
        <tr>
          <td nowrap="nowrap">City:</td>
          <td nowrap="nowrap" colspan="2">
            <input type="text" class="inputFld" name="AddressCity" id="txtCity" size="51" maxLength="30"/>
          </td>
        </tr>
        <tr>
          <td nowrap="nowrap">State:</td>
          <td nowrap="nowrap" colspan="2">
            <select name="AddressState" id="selState" class="inputFld" style="width:146;">
              <option value=""></option>
			   <xsl:for-each select="Reference[@ListName='State']">
				 <xsl:call-template name="BuildSelectOptions"/>
			   </xsl:for-each>
            </select>
          </td>
        </tr>
		  </table>
	  </td>
	  <td>
	    <table>
        <tr>
          <td>Program Shop:</td>
          <td>
            <select name="ProgramFlag" id="selShopType" class="inputFld" style="width:146;">
              <xsl:for-each select="Reference[@ListName='ShopType']">
				        <xsl:call-template name="BuildSelectOptions"/>
			        </xsl:for-each>
            </select>
          </td>
        </tr>
        <tr>
          <td>Specialty:</td>
          <td>
            <select id="selSpecialties" name="SpecialtyID" class="inputFld" style="width:146;">
              <option value=""></option>
			        <xsl:for-each select="Reference[@ListName='Specialty']">
				        <xsl:call-template name="BuildSelectOptions"/>
			        </xsl:for-each>
            </select>
          </td>
        </tr>
        <tr>
          <td nowrap="nowrap">Phone:</td>
          <td nowrap="nowrap">
            <input type="text" class="inputFld" name="PhoneAreaCode" id="txtPhoneAreaCode" size="2" maxLength="3" onkeypress="NumbersOnly(window.event)"/> -
            <input type="text" class="inputFld" name="PhoneExchange" id="txtPhoneExchange" size="2" maxLength="3" onkeypress="NumbersOnly(window.event)"/> -
            <input type="text" class="inputFld" name="PhoneUnit" id="txtPhoneUnit" size="3" maxLength="4" onkeypress="NumbersOnly(window.event)"/>
          </td>
		    </tr>
        <tr>
          <td></td>
          <td align="right">
		  	    <xsl:if test="contains($ShopCRUD, 'U')">
              <input type="button" id="btnClear" value="Clear" class="formButton" onclick="Clear()" style="width:70"/>
              <input type="button" id="btnSearch" value="Search" class="formButton" onclick="Search()" style="width:70"/>
            </xsl:if>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</TABLE>

<div style="position:absolute; top:78px; left: 280px; height:12px: width; 80px;">
	<label id="lblCount" style="color:blue; font-weight:600; display:none"></label>
</div>

<div style="position:absolute; top:78px; left: 280px; height:12px: width; 80px;">
	<label id="lblSearching" style="color:blue; font-weight:600; display:none"></label>
</div>

<IFRAME id="ifrmShopList" src="../blank.asp" style="position:relative; top:4px; width:720; height:369; border:0px; visibility:hidden; background:transparent; overflow:hidden" allowtransparency="true" >
</IFRAME>

</BODY>
</HTML>

</xsl:template>

<xsl:template name="BuildSelectOptions">
	<xsl:param name="current"/>
	<option>
		<xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>

		<xsl:if test="@ReferenceID = $current">
			<xsl:attribute name="selected"/>
		</xsl:if>

		<xsl:value-of select="@Name"/>
	</option>
</xsl:template>

</xsl:stylesheet>
