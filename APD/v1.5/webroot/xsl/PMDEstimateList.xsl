<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="PMDEstimateList">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="PMDDateSearch.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />
<xsl:param name="ClaimCRUD" select="Claim"/>
<xsl:param name="ShopCRUD" select="Shop"/>

<xsl:param name="ImageRootDir"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP                    XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDEstimateGetListXML,PMDEstimateList.xsl,828   -->

<HEAD>
  <TITLE>Program Manager Desktop</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  
  <style>
  	A{
		  color:blue
	  }
	  A:hover{
		  color : #B56D00;
		  font-family1 : Verdana, Geneva, Arial, Helvetica, sans-serif;
		  font-weight1 : bold;
		  font-size1 : 10px;
		  cursor : hand;
	  }
  </style>

  <!-- CLIENT SCRIPTS -->
  <SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
  <SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>
  <SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
  <SCRIPT language="JavaScript" src="/js/showimage.js"></SCRIPT>
  <SCRIPT language="JavaScript" src="/js/PMDNavigate.js"></SCRIPT>
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
  <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
  </script>

  <!-- Page Specific Scripting -->
  <SCRIPT language="JavaScript">
   
  var gsImageRootDir = '<xsl:value-of select="$ImageRootDir"/>';
  var bMouseInToolTip = false;
  parent.gsInsuranceCompanyID = '<xsl:value-of select="Claim/@InsuranceCompanyID"/>';
  var gsClaimCRUD = "<xsl:value-of select="$ClaimCRUD"/>";
   
  
  <![CDATA[
  
  function initPage(){  
    //parent.resizeScrollTable(document.getElementById("CNScrollTable"));
    if (typeof(parent.hideProgress) == "function")
      parent.hideProgress();
  }
  
  function upLevel_onclick(){
    parent.hideEstimates();
  	//window.navigate(parent.gsReferrer + "?ShopLocationID=" + parent.gsShopLocationID + "&BeginDate=" + parent.gsBeginDate + "&EndDate=" + parent.gsEndDate + "&AssignmentCode=" + parent.gsAssignmentCode);
  }
  
  function GridSelect(oRow, event){
    var strDocumentID = oRow.cells[1].getAttribute("DocumentID");

  	if (strDocumentID > 0)
  		window.navigate("PMDReinspectForm.asp?SupForm=0&DocumentID=" + strDocumentID);
  	else
  		parent.ClientWarning("There is no valid Document ID associated with this Estimate.");
  }
  
  function ProcessLink(sArg, sDoc){
  	var sHREF;
  	switch (sArg){
  		case "est":
  			sHREF = "/EstimateDocView.asp?docPath=" + gsImageRootDir
  			if (arguments[2])
  				sHREF += arguments[2];
        window.showModelessDialog(sHREF, "", "dialogHeight:355px; dialogWidth:1038px; dialogTop:5px; dialogLeft:5px; resizable:yes; status:no; help:no; center:no;");
        break;
  		case "rei":
  			if (sDoc > 0)
  				window.navigate("PMDReinspectForm.asp?SupForm=0&DocumentID=" + sDoc);
  			else
  				parent.ClientWarning("There is no valid Document ID associated with this Estimate.");
  			break;
  		case "audit":
  			//sHREF = "../EstimateAudit.asp?Caller=PMD&DocumentID=" + sDoc
        sHREF = "../Estimates.asp?DocumentID=" + sDoc + "&ShowTotals=0";
        window.open(sHREF, "", "height=523px, width=728px, top=5px, left=5px, resizable=no, status=no, help=no, center=yes");
        break;
      case "photos":
        parent.ShowPhotoViewer();
  	}
  }

  function initData(){
    if (xmlEstimates){
      var recCount = xmlEstimates.XMLDocument.firstChild.childNodes.length;
      if (recCount < 1){
        divNoData.style.display = "inline";
        tblSort1.style.display = "none";
      }
    }
  }
  
  function initRec(){
    if (tblSort1.readyState == "complete"){
      var oRows = tblSort1.rows;
      for (var i = 0; i < oRows.length; i++){
        oRows[i].style.backgroundColor = (oRows[i].rowIndex % 2 != 0 ? "#fff7e5" : "#ffffff");
      }
    } else {
      window.setTimeout("initRec()", 100);
    }
  }
  
  function showEstimate(oTR){
    var iDocumentID = oTR.cells[10].innerText;
    var sImageLocation = oTR.cells[11].innerText;
    if (sImageLocation != ""){
      //replace the double \\ to \
      sImageLocation = sImageLocation.replace(/[\\]{2}/g, "\\");
      ProcessLink('est', iDocumentID , sImageLocation);
    }
  }
  
  function showAudit(oTR){
    var iDocumentID = oTR.cells[10].innerText;
    var iVANFlag = oTR.cells[18].innerText;
    //if (iVANFlag == 1)
      ProcessLink('audit', iDocumentID);
    //else
    //  parent.ClientWarning("This is a manual estimate. Estimate detail and audit information is not available.");
  }
  
  function showREI(oTR){
    var iDocumentID = oTR.cells[10].innerText;
    var sImageLocation = oTR.cells[11].innerText;
    if (typeof(parent.showProgress) == "function")
      parent.showProgress();
    window.setTimeout("ProcessLink('rei', " + iDocumentID + ")", 100);
  }
  
  function keepToolTip(){
    bMouseInToolTip = true;
  }

  function showToolTip(obj){
    if (obj){
      if (obj.cells[14].innerText == "1") {
        var sRequestedBy = obj.cells[15].innerText;
        var sRequestedOn = obj.cells[16].innerText;
        var sRequestComments = obj.cells[17].innerText;
        
        divRequestedBy.innerText = sRequestedBy;
        divRequestedOn.innerText = sRequestedOn;
        divRequestComments.innerText = sRequestComments;
        
        var iLeft = iTop = 0;
        iTop = event.y + 5;
        toolTip.style.top = iTop;
        toolTip.style.left = obj.offsetLeft + 20;
        toolTip.style.display = "inline";
        bMouseInToolTip = true;
      } else  
        hideToolTip();
    } else {
      bMouseInToolTip = false;
      hideToolTip();
    }
  }
  
  function hideToolTip(){
    if (bMouseInToolTip == false) {
      toolTip.style.display = "none";
      bMouseInToolTip = false;
    }
  }
  ]]>
  
  </SCRIPT>
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" onload="initPage();" bgcolor="#FFFAEB">
  <DIV unselectable="on" class="SmallGroupingTab" style="position:absolute; z-index:2; top: 5px; left: 6px;">
    Estimate<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;List
  </DIV>
  <DIV unselectable="on" class="SmallGrouping" style="position:absolute; left:3px; top:21px; width:1005px">
    <TABLE unselectable="on" width="100%" class="ShopAssignTableSrh" border="0" cellspacing="0" cellpadding="2">
      <TR unselectable="on">
        <TD unselectable="on" width="100%" height="25" style="color : #000066;"></TD>
  		  <TD><input type="button" id="btnPhoto" class="formButton" value="Photos" onclick="ProcessLink('photos')" style="width:80"/></TD>
  		  <TD><IMG src="/images/spacer.gif" width="188" height="4" border="0" /></TD>
      	<TD unselectable="on"><IMG src="/images/smrefresh.gif" onClick="upLevel_onclick()" border="0" width="16" height="16" alt="Return to Reinspection Assignments" style="cursor:hand;" /></TD>
      	<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      </TR>
    </TABLE>
    
    <IMG src="/images/spacer.gif" width="1" height="4" border="0"/>
  
    <DIV id="CNScrollTable"> 
  	  <TABLE id="tblHead" unselectable="on" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSort1')" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
        <colgroup>
          <col width="30px"/>
          <col width="150px"/>
          <col width="100px"/>
          <col width="100px"/>
          <col width="190px"/>
          <col width="60px"/>
          <col width="150px"/>
          <col width="63px"/>
          <col width="63px"/>
          <col width="63px"/>
        </colgroup>
        <TBODY>
          <TR unselectable="on" class="QueueHeader">
  			    <TD unselectable="on" class="TableSortHeader" sIndex="99" style="cursor:default;"> ReI Req </TD>
  			    <TD unselectable="on" class="TableSortHeader" sIndex="99" style="cursor:default;"> Estimate Seq. </TD>
  			    <TD unselectable="on" class="TableSortHeader" sIndex="99" style="cursor:default;"> Received Date </TD>
  			    <TD unselectable="on" class="TableSortHeader" sIndex="99" style="cursor:default;"> Source </TD>
  			    <TD unselectable="on" class="TableSortHeader" sIndex="99" style="cursor:default;"> Appraiser Name </TD>
            <TD unselectable="on" class="TableSortHeader" sIndex="99" style="cursor:default;"> Duplicate </TD>
  			    <TD unselectable="on" class="TableSortHeader" sIndex="99" style="cursor:default;"> Est./Supp. Amount </TD>
            <TD unselectable="on" class="TableSortHeader" sIndex="99" style="cursor:default;"> Estimate </TD>
            <TD unselectable="on" class="TableSortHeader" sIndex="99" style="cursor:default;"> Audit </TD>
            <TD unselectable="on" class="TableSortHeader" sIndex="99" style="cursor:default;"> ReI Form </TD>
          </TR>
  		  </TBODY>
      </TABLE>
      
	    <DIV unselectable="on" class="autoflowTable" style="width:100%; height:325px;">
        <DIV id="divNoData" style="margin:5px;font-weight:bold;color:#FF0000;display:none;width:100%;text-align:center" unselectable="on">No data available</DIV>
        <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" border="1" cellspacing="0" cellpadding="3" style="table-layout:fixed;border:1px solid #C0C0C0;border-collapse:collapse" dataSrc="#xmlEstimates" onreadystatechange="initRec()">
          <colgroup>
            <col width="28px" style="text-align:center"/>
            <col width="150px"/>
            <col width="100px" style="text-align:center"/>
            <col width="100px" style="text-align:center"/>
            <col width="190px"/>
            <col width="60px" style="text-align:center"/>
            <col width="150px" style="text-align:right"/>
            <col width="62px" style="text-align:center"/>
            <col width="63px" style="text-align:center"/>
            <col width="63px" style="text-align:center"/>
            <col style="display:none"/>
            <col style="display:none"/>
            <col style="display:none"/>
            <col style="display:none"/>
            <col style="display:none"/>
            <col style="display:none"/>
            <col style="display:none"/>
            <col style="display:none"/>
            <col style="display:none"/>
          </colgroup>
          <TBODY bgColor1="ffffff" bgColor2="fff7e5">
            <TR unselectable="on" style="height:21px" onmouseenter="GridMouseOver(this)" onmouseleave="GridMouseOut(this)">
              <TD unselectable="on" onmouseenter="showToolTip(this.parentElement)" onmouseleave="hideToolTip()"><img dataFld="ReIRequestedFlag"/></TD>
              <TD unselectable="on"><span class="NoWrap" dataFld="DocType"/></TD>
              <TD unselectable="on"><span class="NoWrap" dataFld="EstCreatedDate"/></TD>
              <TD unselectable="on"><span class="NoWrap" dataFld="DocumentSource"/></TD>
              <TD unselectable="on"><span class="NoWrap" dataFld="AppraiserName"/></TD>
              <TD unselectable="on"><span class="NoWrap" dataFld="Duplicate"/></TD>
              <TD unselectable="on"><span class="NoWrap" dataFld="EstAmount"/></TD>
              <TD unselectable="on"><img dataFld="EstImage" onclick="showEstimate(this.parentElement.parentElement)"/></TD>
              <TD unselectable="on"><img dataFld="AuditImage" onclick="showAudit(this.parentElement.parentElement)"/></TD>
              <TD unselectable="on"><img dataFld="ReIFormImage" onclick="showREI(this.parentElement.parentElement)"/></TD>
              <TD unselectable="on"><span class="NoWrap" dataFld="DocumentID"/></TD>
              <TD unselectable="on"><span class="NoWrap" dataFld="ImageLocation"/></TD>
              <TD unselectable="on"><span class="NoWrap" dataFld="AuditAvailableFlag"/></TD> 
              <TD unselectable="on"><span class="NoWrap" dataFld="ReinspectID"/></TD>
              <TD unselectable="on"><span class="NoWrap" dataFld="ReIReqFlag"/></TD>
              <TD unselectable="on"><span class="NoWrap" dataFld="ReIRequestedBy"/></TD>
              <TD unselectable="on"><span class="NoWrap" dataFld="ReIRequestedDate"/></TD>
              <TD unselectable="on"><span class="NoWrap" dataFld="ReIReqComments"/></TD>
              <TD unselectable="on"><span class="NoWrap" dataFld="VANFlag"/></TD>
           </TR>
          </TBODY>
        </TABLE>
      </DIV>	
	  </DIV>  
  </DIV>

  <div id="toolTip" name="toolTip" style="position:absolute;top:100px;left:100px;display:none;height:150px;width:300px;cursor:default;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)" onclick="bMouseInToolTip=false;hideToolTip()" onmouseenter="keepToolTip()" onmouseleave="bMouseInToolTip=false;hideToolTip()">
    <table id="tblToolTip" border="0" cellspacing="0" cellpadding="0" style="height1:100%;width:100%;border:1px solid #556B2F;background-color:#FFFFFF" onreadystatechange="checkToolTipSize()">
      <tr style="height:21px;text-align:center;font-weight:bold;background-color:#556B2F;">
        <td style="color:#FFFFFF">Reinspection Request Details</td>
      </tr>
      <tr valign="top">
        <td>
          <div id="divToolTipFlow" style="overflow:auto;height:100%;width:100%;padding:3px;">
            <table border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;width:100%">
              <colgroup>
                <col width="90px"/>
                <col width="*"/>
              </colgroup>
              <tr>
                <td style="font-weight:bold">Requested By:</td>
                <td><div id="divRequestedBy"/></td>
              </tr>
              <tr>
                <td style="font-weight:bold">Requested On:</td>
                <td><div id="divRequestedOn"/></td>
              </tr>
              <tr valign="top">
                <td style="font-weight:bold">Comments:</td>
                <td><div id="divRequestComments" style="height:90px;overflow:auto;"/></td>
              </tr>
            </table>
          </div>
        </td>
      </tr>
    </table>
  </div>
  
  <DIV unselectable="on" style="position:absolute; left:0px; top:360px">
    <xsl:for-each select="Shop" >
      <xsl:call-template name="ShopInfo">
        <xsl:with-param name="ShopCRUD"><xsl:value-of select="$ShopCRUD"/></xsl:with-param>
      </xsl:call-template>
    </xsl:for-each>
	  <xsl:for-each select="Claim" >
      <xsl:call-template name="ClaimInfo">
        <xsl:with-param name="ClaimCRUD"><xsl:value-of select="$ClaimCRUD"/></xsl:with-param>
      </xsl:call-template>
    </xsl:for-each>
  </DIV>
  
  <xml id="xmlEstimates" ondatasetcomplete="initData()">
    <Root>
      <xsl:for-each select="Estimate">
      <Estimate>
        <xsl:copy-of select="@*"/>
          <xsl:attribute name="ReIRequestedFlag">
            <xsl:choose>
              <xsl:when test="@ReIReqFlag='1'">/images/cbcheck.png</xsl:when>
              <xsl:otherwise>/images/spacer.gif</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="Duplicate">
            <xsl:choose>
              <xsl:when test="@DuplicateFlag='1'">Yes</xsl:when>
              <xsl:otherwise>No</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="DocType">
            <xsl:choose>
              <xsl:when test="@DocumentType = 'Supplement'"><xsl:value-of select="concat(@DocumentType, '-', @SequenceNumber)"/></xsl:when>
              <xsl:otherwise><xsl:value-of select="@DocumentType"/></xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="EstAmount">
            <xsl:choose>
              <xsl:when test="@Amount != ''"><xsl:value-of select="format-number(@Amount, '$###,###,##0.00')"/></xsl:when>
              <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="EstCreatedDate">
            <xsl:if test="@CreatedDate!='1900-01-01T00:00:00'">
              <xsl:value-of select="user:UTCConvertDate(string(@CreatedDate))"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="ReIRequestedDate">
            <xsl:if test="@ReIReqDate!='1900-01-01T00:00:00'">
              <xsl:value-of select="user:UTCConvertDate(string(@ReIReqDate))"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="EstImage">
      	    <xsl:choose>
              <xsl:when test="@ImageLocation = ''">/images/NotEdited.gif</xsl:when>
              <xsl:otherwise>/images/image.gif</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <!-- <xsl:attribute name="AuditImage">/images/ElectronicEst.gif</xsl:attribute> -->
          <xsl:attribute name="AuditImage">
    	      <xsl:choose>
    		      <xsl:when test="@VANFlag = 1">/images/ElectronicEst.gif</xsl:when>
    			    <xsl:otherwise>/images/NotEdited.gif</xsl:otherwise>
    		    </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="ReIFormImage">
    	      <xsl:choose>
    			    <xsl:when test="@ReinspectID &gt; 0">/images/edited.gif</xsl:when>
    			    <xsl:otherwise>/images/NotEdited.gif</xsl:otherwise>
    			  </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="ReIRequestedBy"><xsl:value-of select="concat(@ReIReqUserNameLast, ', ', @ReIReqUserNameFirst)"/></xsl:attribute>
          <xsl:attribute name="ReIRequestedDate">
            <xsl:if test="@ReIReqDate!='1900-01-01T00:00:00'">
              <xsl:value-of select="user:extractSQLDateTime(string(@ReIReqDate))"/>
            </xsl:if>
          </xsl:attribute>
      </Estimate>
      </xsl:for-each>
    </Root>
  </xml>
</BODY>
</HTML>

</xsl:template>

<!-- Gets the list of Estimates -->
<xsl:template name="EstimateList">
    
  <!--<TR unselectable="on" onMouseOut="parent.GridMouseOut(this)" onMouseOver="parent.GridMouseOver(this)" onclick="GridSelect(this, event)">-->
	<TR unselectable="on" onMouseOut="parent.GridMouseOut(this)" onMouseOver="parent.GridMouseOver(this); this.style.cursor='default'">
    <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
    <TD unselectable="on" width="100%" class="GridTypeTD" style="text-align:left">
      <xsl:choose>
		    <xsl:when test="@AppraiserName != ''"><xsl:value-of select="@AppraiserName"/></xsl:when>
		    <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
		  </xsl:choose>
    </TD>
	  <TD unselectable="on" width="90" class="GridTypeTD" style="text-align:left">
      <xsl:attribute name="DocumentID"><xsl:value-of select="@DocumentID"/></xsl:attribute>
		  <xsl:choose>
		    <xsl:when test="@DocumentType != ''"><xsl:value-of select="@DocumentType"/></xsl:when>
		    <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
		  </xsl:choose>  
    </TD>
    <TD unselectable="on" width="90" class="GridTypeTD">
      <xsl:choose>
		    <xsl:when test="@CreatedDate != ''"><xsl:value-of select="user:UTCConvertDateByNodeType(.,'CreatedDate', 'A')"/></xsl:when>
		    <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
		  </xsl:choose>       
    </TD>
	  <TD unselectable="on" width="90" class="GridTypeTD" style="text-align:right">
      <xsl:choose>
		    <xsl:when test="@Amount != ''"><xsl:value-of select="@Amount"/></xsl:when>
		    <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
		  </xsl:choose> 
	  </TD>
	  <TD unselectable="on" width="112" class="GridTypeTD" nowrap="nowrap">
	    <xsl:choose>
        <xsl:when test="@ImageLocation = ''">
          <IMG unselectable="on" src="/images/NotEdited.gif" width="15" height="15" title="Image not available"/>
        </xsl:when>
        <xsl:otherwise>
          <img unselectable="on" src="/images/image.gif" width="15" height="16" style="cursor:hand" title="Estimate Image Available">
			      <xsl:attribute name="onclick">ProcessLink('est', '<xsl:value-of select="@DocumentID"/>', '<xsl:value-of select="@ImageLocation"/>')</xsl:attribute>
			    </img>
        </xsl:otherwise>
      </xsl:choose>
	  </TD>
    <TD unselectable="on" width="89" class="GridTypeTD" nowrap="nowrap"> <!-- estimate audit -->
      <IMG unselectable="on" width="15" height="15">
	      <xsl:choose>
		      <xsl:when test="@AuditAvailableFlag = 1">
            <xsl:attribute name="style">cursor:hand</xsl:attribute>
            <xsl:attribute name="title">Estimate Audit Available</xsl:attribute>
            <xsl:attribute name="src">/images/edited.gif</xsl:attribute>
            <xsl:attribute name="onclick">ProcessLink('audit', '<xsl:value-of select="@DocumentID"/>')</xsl:attribute>
          </xsl:when>
			    <xsl:otherwise>
            <xsl:attribute name="style">cursor:default</xsl:attribute>
            <xsl:attribute name="title">No Estimate Audit Available</xsl:attribute>
            <xsl:attribute name="src">/images/NotEdited.gif</xsl:attribute>
          </xsl:otherwise>
		    </xsl:choose>
		  </IMG>
    </TD>
	  <TD unselectable="on" width="111" class="GridTypeTD" nowrap="nowrap">
	    <IMG unselectable="on" width="15" height="15" style="cursor:hand">
	      <xsl:choose>
			    <xsl:when test="@ReinspectID &gt; 0">
            <xsl:attribute name="title">Reinspection Form</xsl:attribute>
            <xsl:attribute name="src">/images/edited.gif</xsl:attribute>
          </xsl:when>
			    <xsl:otherwise>
            <xsl:attribute name="title">Blank Reinspection Form</xsl:attribute>
            <xsl:attribute name="src">/images/NotEdited.gif</xsl:attribute>
          </xsl:otherwise>
			  </xsl:choose>
       
        <xsl:attribute name="onclick">ProcessLink('rei', '<xsl:value-of select="@DocumentID"/>')</xsl:attribute>
      </IMG>
	  </TD>
  </TR> 
</xsl:template>

<!-- Gets the claim details -->
<xsl:template name="ClaimInfo">
  <xsl:param name="ClaimCRUD"/>
  <DIV unselectable="on" class="SmallGroupingTab" style="position:absolute; z-index:2; top: 70px; left: 443px;">Claim Info</DIV>
  <DIV unselectable="on" class="SmallGrouping" style="position:absolute; left:440px; top:86px; z-index:1;">
    <TABLE unselectable="on" id="tblClaimInfo" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
      <TR unselectable="on">
		    <colgroup>
		      <col width="285"/>
			    <col width="226"/>
		    </colgroup>
		    <TD>
		      <TABLE style="table-layout:fixed;">
			      <TR>
			        <colgroup>
  				      <col width="60" align="left"/>
  				      <col width="220" align="left"/>
  				    </colgroup>			
  		        <TD unselectable="on" nowrap="nowrap" class="TableHeader2">
                <SPAN class="boldtext">LYNX ID:</SPAN>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  	          </TD>
  		        <TD id="tdLynxID" unselectable="on" class="TableHeader2" style="text-align:left">
                <xsl:choose>
                  <xsl:when test="contains($ClaimCRUD, 'R') = true()">                
                    <a>
                      <xsl:if test="@CancellationDate != ''"><xsl:attribute name="style">color:red;</xsl:attribute></xsl:if>
                      <xsl:attribute name="href">javascript:NavToClaim(<xsl:value-of select="@LynxID"/>)</xsl:attribute>
                      <xsl:value-of select="@LynxID"/>
                    </a>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:if test="@CancellationDate != ''"><xsl:attribute name="style">color:red;</xsl:attribute></xsl:if>
                    <xsl:value-of select="@LynxID"/>
                  </xsl:otherwise>
                </xsl:choose>
              </TD>
  		      </TR>
  		      <TR unselectable="on" bgColor="#FDF5E6">
  		        <TD unselectable="on" nowrap="nowrap">
  		          <SPAN class="boldtext"> Veh #:</SPAN>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  		        </TD>
  		        <TD id="tdClaimAspectNumber" unselectable="on"><xsl:value-of select="@ClaimAspectNumber"/></TD>
            </TR>
  		      <TR unselectable="on" bgColor="#FFFFEE">
  		        <TD unselectable="on" nowrap="nowrap">
  		          <SPAN class="boldtext"> Vehicle:</SPAN>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  		        </TD>
  		        <TD unselectable="on">
                <xsl:value-of select="@VehicleYear"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  				      <xsl:value-of select="@VehicleMake"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  				      <xsl:value-of select="@VehicleModel"/>
  		        </TD>
            </TR>
  		      <TR unselectable="on" bgColor="#FDF5E6">
  		        <TD unselectable="on" nowrap="nowrap">
  		          <SPAN class="boldtext"> Owner:</SPAN>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  		        </TD>
  		        <TD unselectable="on">
                <xsl:value-of select="@OwnerBusinessName"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  				      <xsl:value-of select="@OwnerFirstName"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  				      <xsl:value-of select="@OwnerLastName"/>
  		        </TD>
            </TR>
  			  </TABLE>
  		  </TD>
  		  <TD unselectable="on">
  		    <TABLE style="table-layout:fixed;">
  			    <TR>
  			      <colgroup>
  				      <col width="70" align="left"/>
  				      <col width="150" align="left"/>
  				    </colgroup>			
  		        <TD unselectable="on" nowrap="nowrap"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		        <TD id="tdLynxID" unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		      </TR>
  			    <TR unselectable="on" bgColor="#FDF5E6">
  			      <TD unselectable="on" nowrap="nowrap"> 
  				      <SPAN class="boldtext"> Company:</SPAN>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  				    </TD>
  			      <TD unselectable="on"><xsl:value-of select="@CompanyName"/></TD>
  			    </TR>
  			    <TR unselectable="on" nowrap="nowrap" bgColor="#FFFFEE">
  			      <TD unselectable="on" nowrap="nowrap"> 
  				      <SPAN class="boldtext"> Claim:</SPAN>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  				    </TD>
  			      <TD unselectable="on"><xsl:value-of select="@ClaimNumber"/></TD>
  			    </TR>
  			    <TR unselectable="on" nowrap="nowrap"  bgColor="#FDF5E6">
  			  	  <TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  				    <TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  			    </TR>
  			  </TABLE>
  		  </TD>
  		</TR>  	
    </TABLE>
  </DIV>
</xsl:template>

</xsl:stylesheet>

