<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="ShopInfo">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<!-- This page is a child of SMTDetailLevel.  This page resides in that page's 'ifrmContent' iframe and
       is dependent on global variables in that page to maintain state -->

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="Saved"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID          SP                   XSL       PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTAllInfoGetDetailXML,SMTAllInfo.xsl,'80559574', 66214   -->

<HEAD>
  <TITLE>Web Shop Load</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>


<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/SMTAllInfo.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<!-- Page Specific Scripting -->


<script language="javascript">

<![CDATA[

function InitPage(){
	onpasteAttachEvents();
  selGlassReplChgType_onchange();
	selTowChgType_onchange();
}

function NoNegatives(event){
	if (event.keyCode == 45)
		event.returnValue = false;
}

function checkNumber() {
  if (isNaN(event.srcElement.value)) {
    alert("Invalid numeric or decimal value specified.");
    event.srcElement.focus();
    event.srcElement.select();
  }

  switch(event.srcElement.id){
		case "txtLatitude":
			if (Math.abs(frmShop.txtLatitude.value) > 90){
				alert("Latitude must be between -90 and 90 degrees.");
				event.srcElement.select();
			}
			break;
		case "txtLongitude":
			if (Math.abs(frmShop.txtLongitude.value) > 180){
				alert("Longitude must be between -180 and 180 degrees.");
				event.srcElement.select();
			}
			break;
	}
}

function ValidateAllPhones() {
  if (validatePhoneSections(frmShop.txtPhoneAreaCode, frmShop.txtPhoneExchangeNumber, frmShop.txtPhoneUnitNumber) == false) return false;
	if (validatePhoneSections(frmShop.txtFaxAreaCode, frmShop.txtFaxExchangeNumber, frmShop.txtFaxUnitNumber) == false) return false;
  if (isNumeric(frmShop.txtPhoneExtensionNumber.value) == false){
    alert("Invalid numeric value specified.");
    frmShop.txtPhoneExtensionNumber.focus();
    frmShop.txtPhoneExtensionNumber.select();
    return false;
  }

  if (isNumeric(frmShop.txtFaxExtensionNumber.value) == false){
    alert("Invalid numeric value specified.");
    frmShop.txtFaxExtensionNumber.focus();
    frmShop.txtFaxExtensionNumber.select();
    return false;
  }

  validateAreaCodeSplit(frmShop.txtPhoneAreaCode, frmShop.txtPhoneExchangeNumber);
  validateAreaCodeSplit(frmShop.txtFaxAreaCode, frmShop.txtFaxExchangeNumber);
  return true;
}

function ValidatePage(){

    if (frmShop.txtName.value == ""){
      alert("A value is required in the 'Name' field.");
		  frmShop.txtName.focus();
		  return false;
	  }

    if (frmShop.txtSPName.value == ""){
      alert("A Name must be entered for Business Owner")
      frmShop.txtSPName.focus();
      return false;
    }

    if (frmShop.txtSLPName.value == ""){
      alert("A Name must be entered for Shop Contact")
      frmShop.txtSLPName.focus();
      return false;
    }

    if (frmShop.selWorkmanshipWarranty.selectedIndex <= 0){
       alert("A warranty period must be selected for both 'Warranty' types for Program Shops.");
       frmShop.selWorkmanshipWarranty.focus();
       return false;
    }

    if (frmShop.selRefinishWarranty.selectedIndex <= 0){
      alert("A warranty period must be selected for both 'Warranty' types for Program Shops.");
      frmShop.selRefinishWarranty.focus();
      return false;
    }

    if (frmShop.selCommMethod.value != "" && frmShop.txtPreferredCommunicationAddress.value == ""){
      alert("Please specify a communication address.");
      frmShop.txtPreferredCommunicationAddress.focus();
      event.returnValue = false;
      return false;
    }

    if (frmShop.selCommMethod.value == "" && frmShop.txtPreferredCommunicationAddress.value != ""){
      alert("Please specify a communication method.");
      frmShop.selCommMethod.focus();
      return false;
    }

    if (frmShop.txtOEMDiscounts.value.length > 250){
      alert("Text entered into OEM Discounts must be limited to no more than 250 characters.");
      frmShop.txtOEMDiscounts.focus();
      return false;
    }

    //validate phone numbers
    if (ValidateAllPhones() == false)
		return false;



	  // validate location hours
	  var badHoursMsg = "The Hours of Operation Start time (left) must be less than the End time (right)."

	  if ((frmShop.txtMondayStartTime.value != "" || frmShop.txtMondayEndTime.value != "") &&
	    (frmShop.txtMondayEndTime.value <= frmShop.txtMondayStartTime.value || frmShop.txtMondayStartTime.value == "")){
		  alert(badHoursMsg);
		  frmShop.txtMondayStartTime.select();
		  return false;
	  }

	  if ((frmShop.txtTuesdayStartTime.value != "" || frmShop.txtTuesdayEndTime.value != "") &&
	    (frmShop.txtTuesdayEndTime.value <= frmShop.txtTuesdayStartTime.value || frmShop.txtTuesdayStartTime.value == "")){
		  alert(badHoursMsg);
		  frmShop.txtTuesdayStartTime.select();
		  return false;
	  }

	  if ((frmShop.txtWednesdayStartTime.value != "" || frmShop.txtWednesdayEndTime.value != "") &&
	    (frmShop.txtWednesdayEndTime.value <= frmShop.txtWednesdayStartTime.value || frmShop.txtWednesdayStartTime.value == "")){
		  alert(badHoursMsg);
		  frmShop.txtWednesdayStartTime.select();
		  return false;
	  }

	  if ((frmShop.txtThursdayStartTime.value != "" || frmShop.txtThursdayEndTime.value != "") &&
	    (frmShop.txtThursdayEndTime.value <= frmShop.txtThursdayStartTime.value || frmShop.txtThursdayStartTime.value == "")){
		  alert(badHoursMsg);
		  frmShop.txtThursdayStartTime.select();
		  return false;
	  }

	  if ((frmShop.txtFridayStartTime.value != "" || frmShop.txtFridayEndTime.value != "") &&
	    (frmShop.txtFridayEndTime.value <= frmShop.txtFridayStartTime.value || frmShop.txtFridayStartTime.value == "")){
		  alert(badHoursMsg);
		  frmShop.txtFridayStartTime.select();
		  return false;
	  }

	  if ((frmShop.txtSaturdayStartTime.value != "" || frmShop.txtSaturdayEndTime.value != "") &&
	    (frmShop.txtSaturdayEndTime.value <= frmShop.txtSaturdayStartTime.value || frmShop.txtSaturdayStartTime.value == "")){
		  alert(badHoursMsg);
		  frmShop.txtSaturdayStartTime.select();
		  return false;
	  }

	  if ((frmShop.txtSundayStartTime.value != "" || frmShop.txtSundayEndTime.value != "") &&
	    (frmShop.txtSundayEndTime.value <= frmShop.txtSundayStartTime.value || frmShop.txtSundayStartTime.value == "")){
		  alert(badHoursMsg);
		  frmShop.txtSundayStartTime.select();
		  return false;
	  }
    return true;
}

function CheckNum(event){
	var obj = eval("frmShop." + event.srcElement.id);
	var idx = obj.value.indexOf(".")
	var pct = false;
	var prec;
	var code = event.keyCode;

  	if (obj.getAttribute("precision") != null)
    	prec = obj.getAttribute("precision");
  	else
    	prec = 2;

	if ((code < 48 && code != 46) || code > 57){ // limit to numerals and decimal point
		event.returnValue = false;
		return;
	}

	if (arguments[1])
		pct = true;

	if (code == 45){				// no negatives
		event.returnValue = false;
		return;
	}

	if (idx > -1){
		//if (obj.value.length - idx > prec){		// allow only proper number decimal places -- 1 for percentages, 2 otherwise
		//	event.returnValue = false;
		//	return;
		//}

		if (code == 46){			// allow only one decimal point
			event.returnValue = false;
			return;
		}
	}

	if (pct){								// ensure percentages do not exceed 100
		if (obj.value == 100){
			event.returnValue = false
			return;
		}

		if (obj.value == 10 && code!= 46 && code != 48 && idx == -1){
			event.returnValue = false;
			return;
		}

		if (obj.value > 10 && code != 46 && idx == -1){
			event.returnValue = false;
			return;
		}
	}
}

function selGlassReplChgType_onchange(){
	lblWindshield.style.display = "none";
	lblGlassDollar.style.display = "none";
	lblWindshieldPercent.style.display = "none";
	frmShop.txtWindshieldDiscount.style.display = "none";
	lblSideBack.style.display = "none";
	frmShop.txtGlassOutsourceSvcFee.style.display = "none";
	lblGlassPercent.style.display = "none";

	frmShop.txtSideBackGlassDiscount.style.display = "none";
	lblSubletFee.style.display = "none";
	frmShop.txtGlassSubletSvcFee.style.display = "none";
	lblSubletPct.style.display = "none";
	frmShop.txtGlassSubletSvcPct.style.display = "none";

	switch (frmShop.selGlassReplChgType.value){
		case "N":
			lblWindshield.style.display = "inline";
			frmShop.txtWindshieldDiscount.style.display = "inline";
			lblWindshieldPercent.style.display = "inline";
			lblSideBack.style.display = "inline";
			frmShop.txtSideBackGlassDiscount.style.display = "inline";
			lblGlassPercent.style.display = "inline";
			break;
		case "S":
			lblSubletFee.style.display = "inline";
			lblGlassDollar.style.display = "inline";
			frmShop.txtGlassSubletSvcFee.style.display = "inline";
			lblSubletPct.style.display = "inline";
			frmShop.txtGlassSubletSvcPct.style.display = "inline";
			lblGlassPercent.style.display = "inline";
			break;
		case "O":
			lblGlassDollar.style.display = "inline";
			frmShop.txtGlassOutsourceSvcFee.style.display = "inline";
			break;
	}
}

function selTowChgType_onchange(){
	if (frmShop.selTowChargeType.value == "F"){
		lblFlatFee.style.display = "inline";
		lblFFDollar.style.display= "inline";
		frmShop.txtFlatFee.style.display = "inline";
	}
	else{
		lblFlatFee.style.display = "none";
		lblFFDollar.style.display= "none";
		frmShop.txtFlatFee.style.display = "none";
	}
}
]]>

</script>

</HEAD>

<body class="bodyAPDsub" onLoad="InitPage()">



<div style="background-color:#FFFAEB;">

<IMG src="/images/spacer.gif" width="6" height="3" border="0" />

<form id="frmShop" method="post" action="SMTAllInfo.asp?act=save" onsubmit="return(ValidatePage())">

  <!-- Get Shop Info -->
  <strong>Shop ID: </strong><xsl:value-of select="@ShopID"/>
  <hr/>
  <strong>Business Info:</strong>
  <xsl:apply-templates select="BusinessInfo"/>
  <hr/>
  <strong>Business Owner:</strong>
  <xsl:apply-templates select="BusinessPersonnel"/>
  <hr/>
  <strong>Shop Info:</strong>
  <xsl:apply-templates select="ShopInfo"/>
  <hr/>
  <strong>Shop Contact:</strong>
  <xsl:apply-templates select="ShopPersonnel"/>
  <hr/>
  <strong>Shop Pricing:</strong>
  <xsl:apply-templates select="Pricing"/>
  <hr/>
  <strong>Shop OEM Discounts:</strong>
  <br/>
  If your shop provides discounts on OEM parts for foreign autos, domestic autos, or any specific makes, please provide that<br/>
  information in the box below.  (Input must be limited to 250 characters.)
  <br/>
  <textarea id="txtOEMDiscounts" name="OEMDiscounts" rows="4" cols="100" onpaste="onTextAreaPaste()"/>
  <hr/>


  <input type="hidden" id="txtBusinessInfoID" name="BusinessInfoID">
    <xsl:attribute name="value"><xsl:value-of select="/Root/@BusinessInfoID"/></xsl:attribute>
  </input>

  <input type="hidden" id="txtShopID" name="ShopID" class="inputFld" tabindex="-1" readonly="readonly" width="60" style="background-color:#eeeeee;">
    <xsl:attribute name="value"><xsl:value-of select="@ShopID"/></xsl:attribute>
  </input>

  <input type="hidden" id="txtSysLastUpdatedDate" name="SysLastUpdatedDate">
    <xsl:attribute name="value"><xsl:value-of select="ShopInfo/@SysLastUpdatedDate"/></xsl:attribute>
  </input>

  <input type="submit" value="Send Data to LYNX"/>
</form>
</div>

</body>
</HTML>

</xsl:template>

<xsl:template match="BusinessInfo">
  <table>
    <tr>
      <td nowrap="on">SSN/EIN:</td>
      <td>
        <input type="text" id="txtFedTaxID" name="FedTaxID" class="inputFld" onkeypress="if (event.keyCode == 46) return false; NumbersOnly(window.event);" disabled="true">
				  <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'BusinessInfo']/Column[@Name='FedTaxId']/@MaxLength"/></xsl:attribute>
	  	    <xsl:attribute name="value"><xsl:value-of select="@FedTaxID"/></xsl:attribute>
			  </input>
      </td>
    </tr>
  </table>
</xsl:template>

<xsl:template match="ShopInfo">
  <TABLE cellSpacing="0" cellPadding="0" border="0" width="715" style="table-layout:fixed;">  <!-- master table -->
    <TR>
	    <colgroup>
	      <col width="275"/>
		    <col width="5"/>
		    <col width="360"/>
		    <col width="5"/>
		    <col width="135"/>
	    </colgroup>
    </TR>
	  <TR><TD><IMG src="/images/spacer.gif" width="6" height="6" border="0" /></TD></TR>
    <TR>
      <TD valign="top"><!-- column 1 : shop info -->
        <TABLE border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
	        <TR>
            <colgroup>
		          <col width="60"/>
			        <col width="215"/>
		        </colgroup>
		        <TD>Name:</TD>
            <TD nowrap="nowrap">
		          <input type="text" id="txtName" name="Name" autocomplete="off" size="37" class="inputFld">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='Name']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
			        </input>
		        </TD>
          </TR>
          <TR>
            <TD>Address1:</TD>
            <TD>
              <input type="text" id="txtAddress1" name="Address1" size="37" class="inputFld">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='Address1']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@Address1"/></xsl:attribute>
			        </input>
            </TD>
		      </TR>
          <TR>
            <TD>Address2:</TD>
            <TD>
              <input type="text" id="txtAddress2" name="Address2" size="37" class="inputFld">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='Address2']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@Address2"/></xsl:attribute>
			        </input>
            </TD>
          </TR>
          <TR>
		        <TD>Zip:</TD>
            <TD noWrap="nowrap">
              <input type="text" id="txtAddressZip" name="AddressZip" size="9" class="inputFld" maxlength="5" onkeypress="NumbersOnly(event)">
			          <xsl:attribute name="value"><xsl:value-of select="@AddressZip"/></xsl:attribute>
			        </input>
            </TD>
          </TR>
          <TR>
		        <TD>City:</TD>
            <TD>
              <input type="text" id="txtAddressCity" name="AddressCity" size="20" class="inputFld">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='AddressCity']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@AddressCity"/></xsl:attribute>
			        </input>
            </TD>
          </TR>
	        <TR>
	          <TD>State:</TD>
            <TD nowrap="nowrap">
              <select id="selState" name="AddressState" class="inputFld">
                <option></option>
			          <xsl:for-each select="/Root/Reference[@ListName='State']">
			          <xsl:call-template name="BuildSelectOptions">
			            <xsl:with-param name="current"><xsl:value-of select="/Root/ShopInfo/@AddressState"/></xsl:with-param>
			          </xsl:call-template>
			        </xsl:for-each>
            </select>
		      </TD>
        </TR>
	      <TR>
	        <TD>County:</TD>
            <TD>
              <input type="text" id="txtAddressCounty" name="AddressCounty" size="20" class="inputFld">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='AddressCounty']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@AddressCounty"/></xsl:attribute>
			        </input>
            </TD>
	        </TR>
          <TR>
	          <TD>Phone:</TD>
            <TD noWrap="nowrap">
		          <xsl:call-template name="ContactNumbers"><xsl:with-param name="ContactMethod">Phone</xsl:with-param></xsl:call-template>
		        </TD>
          </TR>
          <TR>
            <TD>Fax:</TD>
            <TD noWrap="nowrap">
              <xsl:call-template name="ContactNumbers"><xsl:with-param name="ContactMethod">Fax</xsl:with-param></xsl:call-template>
            </TD>
            <TD noWrap="nowrap"></TD>
          </TR>
          <TR>
            <TD>Email:</TD>
            <TD noWrap="nowrap">
              <input type="text" id="txtEmailAddress" name="EmailAddress" size="37" class="inputFld" onbeforedeactivate="checkEMail(this)">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='EmailAddress']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@EmailAddress"/></xsl:attribute>
			        </input>
            </TD>
            <TD noWrap="nowrap"></TD>
          </TR>
          <TR>
            <TD noWrap="nowrap">Web Site:</TD>
            <TD noWrap="nowrap">
              <input type="text" id="txtWebSiteAddress" name="WebSiteAddress" size="37" class="inputFld" onbeforedeactivate="checkURL(this)">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='WebSiteAddress']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@WebSiteAddress"/></xsl:attribute>
			        </input>
            </TD>
            <TD noWrap="nowrap"></TD>
          </TR>
          <TR>
            <TD>Latitude / Longitude:</TD>
            <TD valign="bottom">
              <input type="text" id="txtLatitude" name="Latitude" size="12" class="inputFld" onkeypress="NumbersOnly(event)" onblur="checkNumber()">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='Latitude']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@Latitude"/></xsl:attribute>
			        </input>
              /
              <input type="text" id="txtLongitude" name="Longitude" size="12" class="inputFld" onkeypress="NumbersOnly(event)" onblur="checkNumber()">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='Longitude']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@Longitude"/></xsl:attribute>
			        </input>
            </TD>
          </TR>
	      </TABLE>
      </TD><!-- end of column 1 -->
      <TD noWrap="nowrap"><!-- spacer column --> </TD>
      <TD valign="top"> <!-- column 2 Shop Program info -->
        <TABLE border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
	        <TR>
		        <colgroup>
		          <col width="140"/>
		          <col width="225"/>
		        </colgroup>
            <TD colspan="2"><IMG src="/images/spacer.gif" width="6" height="24" border="0" /></TD>
          </TR>
          <TR>
            <TD nowrap="nowrap">Certified First ID:</TD>
            <TD nowrap="nowrap">
              <input type="text" id="txtCertifiedFirstID" name="CertifiedFirstID" class="inputFld" size="15">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='CertifiedFirstID']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@CertifiedFirstID"/></xsl:attribute>
			        </input>
            </TD>
          </TR>
          <TR>
            <TD nowrap="nowrap">Workmanship Warranty:</TD>
            <TD>
              <select id="selWorkmanshipWarranty" name="WarrantyPeriodWorkmanshipCD" class="inputFld">
                <option></option>
                <xsl:for-each select="/Root/Reference[@ListName='WarrantyPeriod']">
			            <xsl:call-template name="BuildSelectOptions">
			              <xsl:with-param name="current" select="/Root/ShopInfo/@WarrantyPeriodWorkmanshipCD"/>
			            </xsl:call-template>
		            </xsl:for-each>
              </select>
            </TD>
          </TR>
		      <TR>
            <TD nowrap="nowrap">Refinish Warranty:</TD>
            <TD>
              <select id="selRefinishWarranty" name="WarrantyPeriodRefinishCD" class="inputFld">
                <option></option>
                <xsl:for-each select="/Root/Reference[@ListName='WarrantyPeriod']">
			            <xsl:call-template name="BuildSelectOptions">
			              <xsl:with-param name="current" select="/Root/ShopInfo/@WarrantyPeriodRefinishCD"/>
			            </xsl:call-template>
		            </xsl:for-each>
              </select>
            </TD>
          </TR>
          <TR>
            <TD>Pref Estimate Package:</TD>
            <TD>
              <select id="selPreferredEstimatePackageID" name="PreferredEstimatePackageID" class="inputFld">
			          <option></option>
			          <xsl:for-each select="/Root/Reference[@ListName='EstimatePackage']">
			            <xsl:call-template name="BuildSelectOptions">
			              <xsl:with-param name="current" select="/Root/ShopInfo/@PreferredEstimatePackageID"/>
			            </xsl:call-template>
		            </xsl:for-each>
              </select>
            </TD>
          </TR>
		      <TR>
            <TD>Communication Method:</TD>
            <TD>
              <select id="selCommMethod" name="PreferredCommunicationMethodID" class="inputFld">
                <option></option>
			          <xsl:for-each select="/Root/Reference[@ListName='CommunicationMethod']">
			            <xsl:call-template name="BuildSelectOptions">
			              <xsl:with-param name="current" select="/Root/ShopInfo/@PreferredCommunicationMethodID"/>
			            </xsl:call-template>
		            </xsl:for-each>
              </select>
            </TD>
          </TR>
          <TR>
            <TD nowrap="nowrap">Communication Address:</TD>
            <TD>
              <input type="text" id="txtPreferredCommunicationAddress" name="PreferredCommunicationAddress" size="16" class="inputFld">
			          <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopInfo']/Column[@Name='PreferredCommunicationAddress']/@MaxLength"/></xsl:attribute>
			          <xsl:attribute name="value"><xsl:value-of select="@PreferredCommunicationAddress"/></xsl:attribute>
			        </input>
            </TD>
          </TR>
		    </TABLE>
      </TD> <!-- end of column 2 -->
      <TD nowrap="nowrap" width="5px"><!-- spacer column --> </TD>
      <TD valign="top"> <!-- column 3 -->
	      <TABLE cellpadding="0" cellspacing="0" border="0">
	        <TR>
		        <TD>
	            <xsl:apply-templates select="/Root/ShopHours"/>
		        </TD>
		      </TR>
		    </TABLE>
	  </TD> <!-- end of column 3 -->
  </TR>
</TABLE>  <!-- end of master table -->
</xsl:template>

<xsl:template match="/Root/ShopHours">
<table>
  <TR>
    <TD noWrap="nowrap" align="middle" colSpan="2" class="boldtext">Hours of Operation:</TD>
  </TR>
  <TR>
    <TD>Mon:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtMondayStartTime" name="OperatingMondayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
        <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingMondayStartTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingMondayStartTime"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtMondayEndTime" name="OperatingMondayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingMondayEndTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingMondayEndTime"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD>Tue:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtTuesdayStartTime" name="OperatingTuesdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingTuesdayStartTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingTuesdayStartTime"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtTuesdayEndTime" name="OperatingTuesdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingTuesdayEndTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingTuesdayEndTime"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD>Wed:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtWednesdayStartTime" name="OperatingWednesdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingWednesdayStartTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingWednesdayStartTime"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtWednesdayEndTime" name="OperatingWednesdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingWednesdayEndTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingWednesdayEndTime"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD>Thu:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtThursdayStartTime" name="OperatingThursdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingThursdayStartTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingThursdayStartTime"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtThursdayEndTime" name="OperatingThursdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingThursdayEndTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingThursdayEndTime"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD>Fri:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtFridayStartTime" name="OperatingFridayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingFridayStartTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingFridayStartTime"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtFridayEndTime" name="OperatingFridayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingFridayEndTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingFridayEndTime"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD>Sat:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtSaturdayStartTime" name="OperatingSaturdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingSaturdayStartTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingSaturdayStartTime"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtSaturdayEndTime" name="OperatingSaturdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingSaturdayEndTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingSaturdayEndTime"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD>Sun:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtSundayStartTime" name="OperatingSundayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingSundayStartTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingSundayStartTime"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtSundayEndTime" name="OperatingSundayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)">
	      <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'ShopHours']/Column[@Name='OperatingSundayEndTime']/@MaxLength"/></xsl:attribute>
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingSundayEndTime"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
</table>


</xsl:template>

<xsl:template match="ShopPersonnel">
   <xsl:variable name="PersonnelID" select="@PersonnelID"/>

  <div style="height:95; width:675;">

	  <input type="hidden" id="txtSLPPersonnelID" name="SLPPersonnelID">
      <xsl:if test="@PersonnelID != ''">
        <xsl:attribute name="value"><xsl:value-of select="@PersonnelID"/></xsl:attribute>
      </xsl:if>
    </input>

	  <input type="hidden" id="txtSLPGenderCD" name="SLPGenderCD"></input>

	  <input type="hidden" id="txtSLPPersonnelTypeID" name="SLPPersonnelTypeID">
      <xsl:attribute name="value">
        <xsl:choose>
          <xsl:when test="@PersonnelTypeID != ''"><xsl:value-of select="@PersonnelTypeID"/></xsl:when>
          <xsl:otherwise>2</xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
	  </input>

  <table cellSpacing="0" width="675px" cellPadding="1" border="0">
    <tr>
      <td>Name:</td>
      <td>
        <input type="text" id="txtSLPName" name="SLPName" size="51"  class="inputFld" autocomplete="off">
		      <xsl:attribute name="maxlength"><xsl:value-of select="/Root/Metadata[@Entity='Personnel']/Column[@Name='Name']/@MaxLength"/></xsl:attribute>
		      <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
		    </input>
      </td>
      <td width="10px"></td>
      <td>Shop Manager:
        <input type="checkbox" id="cbSLPShopManagerFlag" onclick="txtSLPShopManagerFlag.value=this.checked?1:0;">
		      <xsl:if test="@ShopManagerFlag='1'"><xsl:attribute name="checked"/></xsl:if>
		    </input>
		    <input type="hidden" id="txtSLPShopManagerFlag" name="SLPShopManagerFlag"  class="inputFld">
		      <xsl:attribute name="value"><xsl:value-of select="@ShopManagerFlag"/></xsl:attribute>
		    </input>
      </td>
    </tr>
    <tr>
      <td>Email:</td>
      <td>
	      <input id="txtSLPEmailAddress" name="SLPEmailAddress" size="51" class="inputFld" onbeforedeactivate="checkEMail(this)">
		      <xsl:attribute name="maxlength"><xsl:value-of select="/Root/Metadata[@Entity='Personnel']/Column[@Name='EmailAddress']/@MaxLength"/></xsl:attribute>
		      <xsl:attribute name="value"><xsl:value-of select="@EmailAddress"/></xsl:attribute>
		    </input>
	    </td>
      <td></td>
      <td nowrap="nowrap">Preferred Contact Method:</td>
      <td>
        <select id="selSLPPreferredContactMethodID" name="SLPPreferredContactMethodID" class="inputFld">
		      <option></option>
		      <xsl:for-each select="/Root/Reference[@ListName='ContactMethod']">
            <xsl:call-template name="BuildSelectOptions">
			        <xsl:with-param name="current" select="/Root/Personnel[@PersonnelID=$PersonnelID]/@PreferredContactMethodID"/>
			      </xsl:call-template>
		      </xsl:for-each>
        </select>
      </td>
    </tr>
    <tr>
      <td>Phone:</td>
      <td nowrap="nowrap" align="left">
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Phone</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SLP</xsl:with-param>
        </xsl:call-template>
      </td>
      <td width="10px"></td>
      <td>Fax: </td>
      <td nowrap="nowrap">
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Fax</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SLP</xsl:with-param>
        </xsl:call-template>
      </td>
	  </tr>
    <tr>
      <td>Cell:</td>
      <td>
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Cell</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SLP</xsl:with-param>
        </xsl:call-template>
      </td>
      <td width="10px"></td>
      <td>Pager: </td>
      <td>
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Pager</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SLP</xsl:with-param>
        </xsl:call-template>
      </td>
    </tr>
  </table>
  </div>
</xsl:template>

<xsl:template match="BusinessPersonnel">
   <xsl:variable name="PersonnelID" select="@PersonnelID"/>

  <div style="height:95; width:675;">

	  <input type="hidden" id="txtSPPersonnelID" name="SPPersonnelID">
      <xsl:if test="@PersonnelID != ''">
        <xsl:attribute name="value"><xsl:value-of select="@PersonnelID"/></xsl:attribute>
      </xsl:if>
    </input>

	  <input type="hidden" id="txtSPGenderCD" name="SPGenderCD"></input>

	  <input type="hidden" id="txtSPPersonnelTypeID" name="SPPersonnelTypeID">
      <xsl:attribute name="value">
        <xsl:choose>
          <xsl:when test="@PersonnelTypeID != ''"><xsl:value-of select="@PersonnelTypeID"/></xsl:when>
          <xsl:otherwise>6</xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
	  </input>

  <table cellSpacing="0" width="675px" cellPadding="1" border="0">
    <tr>
      <td>Name:</td>
      <td>
        <input type="text" id="txtSPName" name="SPName" size="51"  class="inputFld" autocomplete="off">
		      <xsl:attribute name="maxlength"><xsl:value-of select="/Root/Metadata[@Entity='Personnel']/Column[@Name='Name']/@MaxLength"/></xsl:attribute>
		      <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
		    </input>
      </td>
      <td width="10px"></td>
      <td>Shop Manager:
        <input type="checkbox" id="cbSPShopManagerFlag" onclick="txtSPShopManagerFlag.value=this.checked?1:0;">
		      <xsl:if test="@ShopManagerFlag='1'"><xsl:attribute name="checked"/></xsl:if>
		    </input>
		    <input type="hidden" id="txtSPShopManagerFlag" name="SPShopManagerFlag"  class="inputFld">
		      <xsl:attribute name="value"><xsl:value-of select="@ShopManagerFlag"/></xsl:attribute>
		    </input>
      </td>
    </tr>
    <tr>
      <td>Email:</td>
      <td>
	      <input id="txtSPEmailAddress" name="SPEmailAddress" size="51" class="inputFld" onbeforedeactivate="checkEMail(this)">
		      <xsl:attribute name="maxlength"><xsl:value-of select="/Root/Metadata[@Entity='Personnel']/Column[@Name='EmailAddress']/@MaxLength"/></xsl:attribute>
		      <xsl:attribute name="value"><xsl:value-of select="@EmailAddress"/></xsl:attribute>
		    </input>
	    </td>
      <td></td>
      <td nowrap="nowrap">Preferred Contact Method:</td>
      <td>
        <select id="selSPPreferredContactMethodID" name="SPPreferredContactMethodID" class="inputFld">
		      <option></option>
		      <xsl:for-each select="/Root/Reference[@ListName='ContactMethod']">
            <xsl:call-template name="BuildSelectOptions">
			        <xsl:with-param name="current" select="/Root/Personnel[@PersonnelID=$PersonnelID]/@PreferredContactMethodID"/>
			      </xsl:call-template>
		      </xsl:for-each>
        </select>
      </td>
    </tr>
    <tr>
      <td>Phone:</td>
      <td nowrap="nowrap" align="left">
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Phone</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SP</xsl:with-param>
        </xsl:call-template>
      </td>
      <td width="10px"></td>
      <td>Fax: </td>
      <td nowrap="nowrap">
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Fax</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SP</xsl:with-param>
        </xsl:call-template>
      </td>
	  </tr>
    <tr>
      <td>Cell:</td>
      <td>
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Cell</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SP</xsl:with-param>
        </xsl:call-template>
      </td>
      <td width="10px"></td>
      <td>Pager: </td>
      <td>
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Pager</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SP</xsl:with-param>
        </xsl:call-template>
      </td>
    </tr>
  </table>
  </div>
</xsl:template>

<xsl:template match="Pricing">
<TABLE unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="0">
<TR>
<TD width="10px"></TD>
<TD width="30%" valign="top">
<table unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2"><b>Hourly Labor Rates:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Sheet Metal:</td>
          <td align="right">$
            <input type="text" id="txtSheetMetal" name="HourlyRateSheetMetal" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			         <xsl:attribute name="value"><xsl:value-of select="@HourlyRateSheetMetal"/></xsl:attribute>
			      </input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Refinishing:</td>
          <td align="right">$
            <input type="text" id="txtRefinish" name="HourlyRateRefinishing" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@HourlyRateRefinishing"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Unibody/Frame:</td>
          <td align="right">$
            <input type="text" id="txtUnibodyFrame" name="HourlyRateUnibodyFrame" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@HourlyRateUnibodyFrame"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Mechanical:</td>
          <td align="right">$
            <input type="text" id="txtMechanical" name="HourlyRateMechanical" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@HourlyRateMechanical"/></xsl:attribute>
			</input>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="7px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td colspan="2">
      <table unselectable="on" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="4"><b>Refinish Materials Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Single Stage:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
          <td>Charge Per Hour:</td>
          <td align="right">$
            <input type="text" id="txtHourly1" name="RefinishSingleStageHourly" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@RefinishSingleStageHourly"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Maximum Charge:</td>
          <td align="right">$
            <input type="text" id="txtMax1" name="RefinishSingleStageMax" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@RefinishSingleStageMax"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Two Stage:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Charge Per Hour:</td>
          <td align="right">$
            <input type="text" id="txtHourly2" name="RefinishTwoStageHourly" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageHourly"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Maximum Charge:</td>
          <td align="right">$
            <input type="text" id="txtMax2" name="RefinishTwoStageMax" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageMax"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Clear Coat Max Hours:</td>
          <td align="right">
            <input type="text" id="txtCCMax2" name="RefinishTwoStageCCMaxHrs" style="text-align:right;" wizard="yes" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageCCMaxHrs"/></xsl:attribute>
			</input>hrs
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Three Stage:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Charge Per Hour:</td>
          <td align="right">$
            <input type="text" id="txtHourly3" name="RefinishThreeStageHourly" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageHourly"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Maximum Charge:</td>
          <td align="right">$
            <input type="text" id="txtMax3" name="RefinishThreeStageMax" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageMax"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Clear Coat Max Hours:</td>
          <td align="right">
            <input type="text" id="txtCCMax3" name="RefinishThreeStageCCMaxHrs" style="text-align:right;" wizard="yes" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageCCMaxHrs"/></xsl:attribute>
			</input>hrs
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="7px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
	  <div style="height:55">
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Towing Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Charge Type:</td>
          <td align="right">
            <select id="selTowChargeType" name="TowInChargeTypeCD" style="text-align:right;" wizard="yes" onchange="selTowChgType_onchange()">
              <option value=""></option>
			  <xsl:for-each select="/Root/Reference[@ListName='TowInChargeType']">
			    <xsl:call-template name="BuildSelectOptions">
				  <xsl:with-param name="current" select="/Root/Pricing/@TowInChargeTypeCD"/>
				</xsl:call-template>
			  </xsl:for-each>
            </select>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td><label id="lblFlatFee">Flat Fee:</label></td>
          <td align="right"><label id="lblFFDollar">$</label>
            <input type="text" id="txtFlatFee" name="TowInFlatFee" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@TowInFlatFee"/></xsl:attribute>
			</input>
          </td>
        </tr>
      </table>
	  </div>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="7px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Frame Setup Charges (Hours):</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>No Measurement:</td>
          <td align="right">
            <input type="text" id="txtPullNoMeasure" name="PullSetup" style="text-align:right;" wizard="yes" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@PullSetup"/></xsl:attribute>
			</input>hrs
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>With Measurement</td>
          <td align="right">
            <input type="text" id="txtPullMeasure" name="PullSetupMeasure" style="text-align:right;" wizard="yes" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@PullSetupMeasure"/></xsl:attribute>
			</input>hrs
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</TD>
<TD width="30px"></TD> <!-- spacer column -->
<TD width="30%" valign="top">
<table width="100%" valign="top" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <table width="100%" valign="top" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="4"><b>Air Conditioning Service Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">R-12 Evaluate/Recharge:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>With Freon:</td>
          <td align="right">$
            <input type="text" id="txtR12Freon" name="R12EvacuateRechargeFreon" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@R12EvacuateRechargeFreon"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Without Freon:</td>
          <td align="right">$
            <input type="text" id="txtR12NoFreon" name="R12EvacuateRecharge" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@R12EvacuateRecharge"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">R-134 Evaluate/Recharge:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>With Freon:</td>
          <td align="right">$
            <input type="text" id="txtR134Freon" name="R134EvacuateRechargeFreon" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@R134EvacuateRechargeFreon"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Without Freon:</td>
          <td align="right">$
            <input type="text" id="txtR134NoFreon" name="R134EvacuateRecharge" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@R134EvacuateRecharge"/></xsl:attribute>
			</input>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="4"><b>Stripe Replacement Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Tape Stripes:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Per Panel:</td>
          <td align="right">$
            <input type="text" id="txtTapePerPanel" name="StripeTapePerPanel" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@StripeTapePerPanel"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Per Side:</td>
          <td align="right">$
            <input type="text" id="txtTapePerSide" name="StripeTapePerSide" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@StripeTapePerSide"/></xsl:attribute>
			</input>
          </td>
		</tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Paint Stripes:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Per Panel:</td>
          <td align="right">$
            <input type="text" id="txtPaintPerPanel" name="StripePaintPerPanel" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@StripePaintPerPanel"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Per Side:</td>
          <td align="right">$
            <input type="text" id="txtPaintPerSide" name="StripePaintPerSide" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@StripePaintPerSide"/></xsl:attribute>
			</input>
          </td>
		</tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Glass Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Replacement Type:</td>
          <td align="right">
            <select id="selGlassReplChgType" name="GlassReplacementChargeTypeCD" style="text-align:right;" wizard="yes" onchange="selGlassReplChgType_onchange()">
              <option value=""></option>
			        <xsl:for-each select="/Root/Reference[@ListName='GlassReplacementChargeType']">
			          <xsl:call-template name="BuildSelectOptions">
				          <xsl:with-param name="current" select="/Root/Pricing/@GlassReplacementChargeTypeCD"/>
				        </xsl:call-template>
			        </xsl:for-each>
            </select>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>
            <label id="lblWindshield">Windshield Discount:</label>
            <label id="lblSubletFee">Sublet Fee:</label>
          </td>
          <td align="right">
            <input type="text" id="txtWindshieldDiscount" name="DiscountPctWindshield" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1)">
			  <xsl:attribute name="value"><xsl:value-of select="@DiscountPctWindshield"/></xsl:attribute>
			</input>
			<label id="lblWindshieldPercent">%</label>
			<label id="lblGlassDollar">$</label>
            <input type="text" id="txtGlassSubletSvcFee" name="GlassSubletServiceFee" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event); frmShop.GlassSubletServicePct.value = '';" onchange="frmShop.GlassSubletServicePct.value = '';">
			  <xsl:attribute name="title">Please enter either a Sublet Service Fee OR a Sublet Discount Percentage</xsl:attribute>
			  <xsl:attribute name="value"><xsl:value-of select="@GlassSubletServiceFee"/></xsl:attribute>
			</input>
			<input type="text" id="txtGlassOutsourceSvcFee" name="GlassOutsourceServiceFee" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@GlassOutsourceServiceFee"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>
	        <label id="lblSideBack">Side/Back Discount:</label>
            <label id="lblSubletPct">Sublet Percent:</label>
          </td>
          <td align="right">
            <input type="text" id="txtSideBackGlassDiscount" name="DiscountPctSideBackGlass" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1)">
			  <xsl:attribute name="value"><xsl:value-of select="@DiscountPctSideBackGlass"/></xsl:attribute>
			</input>
			<input type="text" id="txtGlassSubletSvcPct" name="GlassSubletServicePct" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1); frmShop.GlassSubletServiceFee.value = '';" onchange="frmShop.GlassSubletServiceFee.value = '';">
			  <xsl:attribute name="title">Please enter either a Sublet Service Fee OR a Sublet Discount Percentage</xsl:attribute>
			  <xsl:attribute name="value"><xsl:value-of select="@GlassSubletServicePct"/></xsl:attribute>
			</input>
			<label id="lblGlassPercent">%</label>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</TD>
<TD width="30px"></TD> <!-- spacer column -->
<TD width="34%" valign="top">
<table border="0" width="235" cellpadding="0" cellspacing="0">
  <tr valign="top" width="100%">
    <td>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Recycled Parts:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Markup Percent:</td>
          <td align="right">
            <input type="text" id="txtRecycledPartsMarkup" name="PartsRecycledMarkupPct" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1)">
			  <xsl:attribute name="value"><xsl:value-of select="@PartsRecycledMarkupPct"/></xsl:attribute>
			</input>%
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="6px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="235" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Additional Repair Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>2-Wheel Alignment:</td>
          <td align="right">$
            <input type="text" id="txtAlign2Wheel" name="AlignmentTwoWheel" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@AlignmentTwoWheel"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>4-Wheel Alignment:</td>
          <td align="right">$
            <input type="text" id="txtAlign4Wheel" name="AlignmentFourWheel" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@AlignmentFourWheel"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Tire Mount and Balance:</td>
          <td align="right">$
            <input type="text" id="txtTireMountBalance" name="TireMountBalance" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@TireMountBalance"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Flex Additive:</td>
          <td align="right">$
            <input type="text" id="txtFlexAdditive" name="FlexAdditive" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@FlexAdditive"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Coolant (Green):</td>
          <td align="right">$
            <input type="text" id="txtCoolantGreen" name="CoolantGreen" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@CoolantGreen"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Coolant (Red):</td>
          <td align="right">$
            <input type="text" id="txtCoolantRed" name="CoolantRed" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@CoolantRed"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Undercoat:</td>
          <td align="right">$
            <input type="text" id="txtUndercoat" name="Undercoat" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@Undercoat"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Chip Guard:</td>
          <td align="right">$
            <input type="text" id="txtChipGuard" name="ChipGuard" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@ChipGuard"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Caulking/Seam Sealer:</td>
          <td align="right">$
            <input type="text" id="txtCaulkSeamSeal" name="CaulkingSeamSealer" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@CaulkingSeamSealer"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Corrosion Protection:</td>
          <td align="right">$
            <input type="text" id="txtCorrosionProt" name="CorrosionProtection" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@CorrosionProtection"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Cover Car:</td>
          <td align="right">$
            <input type="text" id="txtCoverCar" name="CoverCar" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@CoverCar"/></xsl:attribute>
			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Hazardous Waste:</td>
          <td align="right">$
            <input type="text" id="txtHazardousWaste" name="HazardousWaste" style="text-align:right;" wizard="yes" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
			  <xsl:attribute name="value"><xsl:value-of select="@HazardousWaste"/></xsl:attribute>
			</input>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="235" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Tax Rates:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Sales:</td>
          <td align="right">
            <input type="text" id="txtSalesTax" name="SalesTaxPct" style="text-align:right;" wizard="yes" size="8" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
			  <xsl:attribute name="value"><xsl:value-of select="@SalesTaxPct"/></xsl:attribute>
			</input>%
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>County:</td>
          <td align="right">
            <input type="text" id="txtCountyTax" name="CountyTaxPct" style="text-align:right;" wizard="yes" size="8" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
			  <xsl:attribute name="value"><xsl:value-of select="@CountyTaxPct"/></xsl:attribute>
			</input>%
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Municipal:</td>
          <td align="right">
            <input type="text" id="txtMunicipalTax" name="MunicipalTaxPct" style="text-align:right;" wizard="yes" size="8" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
			  <xsl:attribute name="value"><xsl:value-of select="@MunicipalTaxPct"/></xsl:attribute>
			</input>%
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Other:</td>
          <td align="right">
            <input type="text" id="txtOtherTax" name="OtherTaxPct" style="text-align:right;" wizard="yes" size="8" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
			  <xsl:attribute name="value"><xsl:value-of select="@OtherTaxPct"/></xsl:attribute>
			</input>%
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</TD>
</TR>
</TABLE>
</xsl:template>

<xsl:template name="ContactNumbers">
  <xsl:param name="ContactMethod"/>
  <xsl:param name="EntityAbbrev"/>

  <input maxlength="3" size="2" onbeforedeactivate="checkAreaCode(this);" class="inputFld" wizard="yes">
	<xsl:attribute name="id">txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>AreaCode</xsl:attribute>
	<xsl:attribute name="name"><xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>AreaCode</xsl:attribute>
	<xsl:attribute name="onkeypress">NumbersOnly(event);phoneAutoTab(this,txt<xsl:value-of select="$ContactMethod"/>ExchangeNumber,'A');</xsl:attribute>
	<xsl:attribute name="value">
	  <xsl:choose>
	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="@PhoneAreaCode"/></xsl:when>
		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="@FaxAreaCode"/></xsl:when>
		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="@CellAreaCode"/></xsl:when>
		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="@PagerAreaCode"/></xsl:when>
	  </xsl:choose>
	</xsl:attribute>
  </input> -

  <input maxlength="3" size="2" onbeforedeactivate="checkPhoneExchange(this);" class="inputFld" wizard="yes">
	<xsl:attribute name="id">txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>ExchangeNumber</xsl:attribute>
	<xsl:attribute name="name"><xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>ExchangeNumber</xsl:attribute>
	<xsl:attribute name="onkeypress">NumbersOnly(event);phoneAutoTab(this,txt<xsl:value-of select="$ContactMethod"/>UnitNumber,'E');</xsl:attribute>
	<xsl:attribute name="value">
	  <xsl:choose>
	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="@PhoneExchangeNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="@FaxExchangeNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="@CellExchangeNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="@PagerExchangeNumber"/></xsl:when>
	  </xsl:choose>
	</xsl:attribute>
  </input> -

  <input maxlength="4" size="3" onbeforedeactivate="checkPhoneNumber(this);" class="inputFld" wizard="yes">
    <xsl:attribute name="id">txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>UnitNumber</xsl:attribute>
	<xsl:attribute name="name"><xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>UnitNumber</xsl:attribute>
	<xsl:attribute name="onkeypress">NumbersOnly(event);phoneAutoTab(this,txt<xsl:value-of select="$ContactMethod"/>ExtensionNumber,'U');</xsl:attribute>
	<xsl:attribute name="value">
	  <xsl:choose>
	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="@PhoneUnitNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="@FaxUnitNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="@CellUnitNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="@PagerUnitNumber"/></xsl:when>
	  </xsl:choose>
	</xsl:attribute>
  </input>

  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;

  <input maxlength="5" size="5" onkeypress="NumbersOnly(window.event)" class="inputFld" wizard="yes">
    <xsl:attribute name="id">txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>ExtensionNumber</xsl:attribute>
	<xsl:attribute name="name"><xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>ExtensionNumber</xsl:attribute>
	<xsl:attribute name="value">
	  <xsl:choose>
	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="@PhoneExtensionNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="@FaxExtensionNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="@CellExtensionNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="@PagerExtensionNumber"/></xsl:when>
	  </xsl:choose>
	</xsl:attribute>
  </input>
</xsl:template>

</xsl:stylesheet>
