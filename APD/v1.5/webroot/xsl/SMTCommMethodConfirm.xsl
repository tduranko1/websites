<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="Insurance">

<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/generic-template-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">
<HTML>

<HEAD>
<TITLE>Confirm Communication Address</TITLE>
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<script language="javascript">
<![CDATA[
  var retVal = "";
  function OkDialog(){
    retVal = "OK";
    window.close();
  }
  
  function CancelDialog(){
    retVal = "Cancel";
    window.close();
  }
  
  function unLoadPage(){
    window.returnValue = retVal;
  }
]]>
</script>
</HEAD>
<BODY class="bodyAPDSub" unselectable="on" style="overflow:hidden;" tabIndex="-1" onunload="unLoadPage()">
	<DIV style="border-bottom: 1px black solid;border-top: 1px white solid; background:#CCCCCC;width:100%;height:18px;padding:5px;">
    <center>
	  <span id="title" style="background:#CCCCCC;" ><strong>Confirm Communication Address</strong></span>
    </center>
	</DIV>
  <DIV style="padding:10px;">
  <div style="height:24px;margin-top:24px;margin-bottom:18px;">The following Shops have the same Communication Method and Communication Address.</div>
  <xsl:call-template name="ShopList"/>
  <div style="height:24px;margin-top:24px;">Do you want to accept the communication address as entered?</div>
  <div align="right">
    <input type="button" class="formButton" value="  OK  " onclick="OkDialog()"/>
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
    <input type="button" class="formButton" value="Cancel" onclick="CancelDialog()"/>
  </div>
  </DIV>
</BODY>
</HTML>
</xsl:template>

<xsl:template name="ShopList">
    <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed;">
        <colgroup>
            <col width="50px"/>
            <col width="250px"/>
            <col width="155px"/>
        </colgroup>
        <tr style="height:21px;cursor:hand;">
            <td unselectable="on" class="TableSortHeader">ID</td>
            <td unselectable="on" class="TableSortHeader">Name</td>
            <td unselectable="on" class="TableSortHeader">City, State</td>
        </tr>
    </table>
    <DIV unselectable="on" class="autoflowTable" style="height:84px;width:455px">
        <table border="0" cellspacing="1" cellpadding="2" style="table-layout:fixed;">
            <colgroup>
                <col width="48px"/>
                <col width="250px"/>
                <col width="131px"/>
            </colgroup>
        <xsl:for-each select="/Root/Shop">
            <tr>
                <td class="GridTypeTD" nowrap="" unselectable="on">
                  <xsl:value-of select="@ShopID"/>
                </td>
                <td class="GridTypeTD" nowrap="" unselectable="on" style="text-align:left;">
                    <xsl:value-of select="@Name"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                </td>
                <td class="GridTypeTD" nowrap="" unselectable="on" style="text-align:left;">
                    <xsl:value-of select="concat(@AddressCity, ', ', @AddressState)"/>
                </td>
            </tr>
        </xsl:for-each>
        </table>
    </DIV>
</xsl:template>

</xsl:stylesheet>
