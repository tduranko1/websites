<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:user="http://mycompany.com/mynamespace"
		xmlns:session="http://lynx.apd/session-manager"
		id="PMDMain">

<xsl:import href="msxsl/generic-template-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="Environment"/>
<xsl:param name="WindowID"/>
<xsl:param name="ClaimView"/>
<xsl:param name="LynxID"/>


<xsl:include href="APDMenu.xsl"/>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSessionGetUserDetailXML,PMD.xsl, csr0901   -->

<xsl:template match="/Root">

<xsl:variable name="Context" select="session:XslUpdateSession( 'Context', '' )"/>
<xsl:variable name="ViewCRD" select="session:XslUpdateSession( 'ViewCRD', string(User/DesktopPermission/@CRD) )"/>
<xsl:variable name="ViewPMD" select="session:XslUpdateSession( 'ViewPMD', string(User/DesktopPermission/@PMD) )"/>
<xsl:variable name="ViewUAD" select="session:XslUpdateSession( 'ViewUAD', string(User/DesktopPermission/@UAD) )"/>
<xsl:variable name="ViewMED" select="session:XslUpdateSession('ViewMED', string(User/DesktopPermission/@MED))"/>

<!-- Make sure we have a valid user -->
<xsl:if test="count(User)=1">

	<xsl:variable name="UserID" select="User/@UserID"/>
	<xsl:variable name="SupervisorUserID" select="User/@SupervisorUserID"/>
	<xsl:variable name="SuperUserFlag" select="User/@SuperUserFlag"/>
	<xsl:variable name="SupervisorFlag" select="User/@SupervisorFlag"/>
	<xsl:variable name="RoleID" select="User/Role[@PrimaryRoleFlag = '1']/@RoleID"/>
	<xsl:variable name="RoleName" select="User/Role[@PrimaryRoleFlag = '1']/@Name"/>

  <xsl:value-of select="session:XslUpdateSession( 'UserID', string($UserID))"/>
  <xsl:value-of select="session:XslUpdateSession( 'UserRole', string($RoleName) )"/>
  <xsl:value-of select="session:XslUpdateSession( 'UserRoleID', string($RoleID) )"/>
  <xsl:value-of select="session:XslUpdateSession( 'SupervisorUserID', string($SupervisorUserID) )"/>
  <xsl:value-of select="session:XslUpdateSession( 'SupervisorFlag', string($SupervisorFlag) )"/>

	<HTML>

    <HEAD>
      <TITLE>Mobile Electronics Desktop - <xsl:value-of select="$Environment"/></TITLE>
      <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
	  <LINK rel="stylesheet" href="/css/Menu.css" type="text/css"/>
	  <LINK rel="stylesheet" href="/css/officexp/Menu.css" type="text/css" id="menuStyleSheet"/>

	  <SCRIPT language="JavaScript" src="js/browser.js"></SCRIPT>
	  <script type="text/javascript" src="/js/menu3.js"></script>
	  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>
      <script type="text/javascript">
          document.onhelp=function(){
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };
      </script>

      <!-- Page Specific Scripting -->
      <SCRIPT language="JavaScript">
	  var gbPMSearch = false;

  <![CDATA[
      //init the table selections, must be last
      function initPage(){
        self.resizeTo("682", "800");
        document.frames["ifrmMain"].frameElement.src = "/MEAssignment.asp";
        //document.frames["ifrmMain"].frameElement.src = "/blank.asp";
        obj = document.getElementById("ifrmMain");
        obj.style.visibility = "visible";
        obj.style.display = "inline";
        setSB(100, sb);
      }

    	var SBstarted = false;
    	function setSB(v, el)
    	{
    		if ((document.all && document.getElementsByTagName) || document.readyState == "complete")
    		{
    			filterEl = el.children[0];
    			valueEl  = el.children[1];

    			el.style.display="inline";

    			if (filterEl.style.pixelWidth > 0)
    			{
    				var filterBackup = filterEl.style.filter;
    				filterEl.style.filter = "";
    				filterEl.style.filter = filterBackup;
    			}

    			filterEl.style.width = v + "%";
    			valueEl.innerText = v + "%";

    			//if (v < 10 && SBstarted == false)
    			//{
    			//	SBstarted = true;
    			//	window.setTimeout("fakeProgress(" + (v + 1) + ", document.all['" + el.id + "'])", 100);
    			//}

    			if (v == 100)
    			{
    				SBstarted = false;
    				window.setTimeout("HideSB(document.all['" + el.id + "'])", 300);
    			}
    		}
    	}

    	function HideSB(el) {
    		el.style.display="none";
    	}

      if (document.attachEvent)
        document.attachEvent("onclick", top.hideAllMenuScriptlets);
      else
        document.onclick = top.hideAllMenuScriptlets;

      var sCommentLynxID = "";
      function SendComments()
      {
          var sDimensions = "dialogHeight:200px; dialogWidth:400px; ";
          var sSettings = "center:Yes; help:No; resizable:No; status:No; unadorned:Yes;";
          var strRet = window.showModalDialog("MESendComments.asp?initlynxid=" + sCommentLynxID, "", sDimensions + sSettings);
      }

  ]]>
      </SCRIPT>

      </HEAD>

      <BODY unselectable="on" onLoad="initPage();" onbeforeunload1="chkBeforeUnloadWindow()" style="background-color:#336699;	color:#000000; font-family:Tahoma,Arial,Helvetica,sans-serif;	font-size:11px;	background-image:url(/images/fundo_default.gif); margin:0px; border:0px;overflow:hidden" tabIndex="-1">


  <TABLE unselectable="on" width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
    <TR>
  	  <TD unselectable="on" width="100%">
	    <xsl:call-template name="APDMenu"><xsl:with-param name="Desktop">MED</xsl:with-param></xsl:call-template>  <!-- Menu bar -->
      </TD>
   </TR>
    <TR height="100%" >
  	  <TD unselectable="on" width="100%" style="padding:5px;">
    	<DIV style="position:absolute; left: 810px; top: 2px;"  border="0" cellspacing="0" cellpadding="0">
    		<!-- Status Bar Starts -->
    		<div id="sb" style="border: 1 solid black; width: 200px; height: 16px; background: #F3ECE5; text-align: left;">
    			<div id="sbChild1" style="position: absolute; width: 0%; height: 14px; filter: Alpha(Opacity=0, FinishOpacity=100, Style=1, StartX=0, StartY=0, FinishX=100, FinishY=0);">
    				<div style="width: 100%; height: 100%; background: #ffcf9C; font-size: 1;"></div>
    			</div>
    			<div style="position: absolute; width: 100%; text-align: center; font-family:  Verdana,arial; font-size: 11px; color: black;">0%</div>
    		</div>
    		<!-- Status Bar Ends -->
    	</DIV>
      <table border="0" cellspacing="0" cellpadding="0" style="height:100%; width:100%;">
        <colgroup>
          <col width="5px"/>
          <col width="*"/>
          <col width="5px"/>
        </colgroup>
        <tr>
          <td>
            <img src="/images/folder_TLC.gif"/>
          </td>
          <td width="100%">
            <img src="/images/folder_top.gif" style="width:100%;height:6px;"/>
          </td>
          <td>
            <img src="/images/folder_TRC.gif"/>
          </td>
        </tr>
        <tr height="100%">
          <td>
            <img src="/images/folder_left.gif" style="height:100%;width:5px;"/>
          </td>
          <td width="100%">
            <IFRAME id="ifrmMain" name="ifrmMain" src="blank.asp" style="width:100%; height:100%; border:0px; padding:0px; visibility:hidden; position:absolute; background:transparent; overflow:hidden" allowtransparency="true" >
            </IFRAME>
          </td>
          <td>
            <img src="/images/folder_right.gif" style="height:100%;width:5px;"/>
          </td>
        </tr>
        <tr>
          <td>
            <img src="/images/folder_BLC.gif"/>
          </td>
          <td width="100%">
            <img src="/images/folder_bottom.gif" style="width:100%;height:6px;"/>
          </td>
          <td>
            <img src="/images/folder_BRC.gif"/>
          </td>
        </tr>
      </table>
    </TD>
  </TR>
  <TR>
  	<TD unselectable="on">
     	<IMG unselectable="on" src="/images/spacer.gif" width="1" height="1" border="0" alt=""/>
    </TD>
  </TR>
  <TR>
  	<TD unselectable="on" width="100%">
			<img src="/images/apdlogo.gif" alt="" width="154" height="36" hspace="10" vspace="10" border="0"/>
    </TD>
  </TR>
</TABLE>

</BODY>
</HTML>



</xsl:if>
<xsl:if test="count(User)=0">
	<xsl:text>Please check your login information.  If you continue to have problems, contact your supervisor.</xsl:text>
</xsl:if>
</xsl:template>
</xsl:stylesheet>
