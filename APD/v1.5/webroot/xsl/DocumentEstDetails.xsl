<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="http://mycompany.com/mynamespace" xmlns:IE="http://mycompany.com/mynamespace" xmlns:js="urn:the-xml-files:xslt" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:fo="http://www.w3.org/1999/XSL/Format" id="DocumentEstDetails">
	<xsl:import href="msxsl/msjs-client-library-htc.js"/>
	<xsl:import href="msxsl/msjs-client-library.js"/>
	<xsl:import href="msxsl/generic-template-library.xsl"/>
	<xsl:output method="html" indent="yes" encoding="UTF-8"/>
	<!-- XslParams - stuffed from COM before transform -->
	<xsl:param name="LynxID"/>
	<xsl:param name="WindowID"/>
	<xsl:param name="ImageRootDir"/>
	<xsl:param name="UserId"/>
	<xsl:param name="DocumentID"/>
	<xsl:param name="ClaimAspectID"/>
	<xsl:param name="PageTitle"/>
	<xsl:param name="EstimateUpd"/>
	<xsl:param name="ImageLocation"/>
	<xsl:param name="FileUploadFlag"/>
	<xsl:param name="InsuranceCompanyID"/>
	<xsl:param name="DisplayPricingFlag"/>
	<xsl:param name="DisDocTypeSelect"/>
	<xsl:param name="isSaved"/>
	<xsl:template match="/Root">
		<xsl:variable name="EstimateUpd" select="$EstimateUpd"/>
		<html>
			<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
			<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
			<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
			<STYLE type="text/css">
				Input {
				font-family: Arial, sans-serif;
				font-weight: normal;
				font-size: 8pt;
				}
			</STYLE>
			<!-- <SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formutilities.js"></SCRIPT> -->
			<SCRIPT language="JavaScript" src="/js/APDControls.js"/>
			<SCRIPT language="JavaScript" src="/js/formats.js"/>
			<script type="text/javascript" language="JavaScript1.2" src="webhelp/RoboHelp_CSH.js"/>
			<script type="text/javascript">
				document.onhelp=function(){
				RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,31);
				event.returnValue=false;
				};
			</script>
			<SCRIPT language="JavaScript">
				var gsDocumentID = "<xsl:value-of select="$DocumentID"/>";
				var gsClaimAspectID = "<xsl:value-of select="$ClaimAspectID"/>";
				var gsUserID = "<xsl:value-of select="$UserId"/>";
				var gsLynxID = "<xsl:value-of select="$LynxID"/>";
				var gsDocumentVANSourceFlag = "<xsl:value-of select="Estimate/@DocumentVANSourceFlag"/>";
				var gsFile = "";
				var gsEstimateUpd = "<xsl:value-of select="$EstimateUpd"/>";
				var gsHighestNetTotal;
				var gsFileUploadFlag = "<xsl:value-of select="$FileUploadFlag"/>";
				var gsInsuranceCompanyID = "<xsl:value-of select="$InsuranceCompanyID"/>";
				var gsImageRootDir = "<xsl:value-of select="$ImageRootDir"/>";
				var gsWindowID = "<xsl:value-of select="$WindowID"/>";
				var gsImageLocation = "<xsl:value-of select="$ImageLocation"/>";
				var gsEstimateTypeAuditedID = "<xsl:value-of select="/Root/Reference[@List='EstimateTypeCD' and @Name='Audited']/@ReferenceID"/>";
				var bReinspectionRequested = <xsl:value-of select="/Root/Estimate/@ReinspectionRequestFlag='1'"/>;
				var sReturnValue = "";
				var gsVehCount = "<xsl:value-of select="count(/Root/Reference[@List='PertainsTo'])"/>";
				var sDisDocTypeSelect = "<xsl:value-of select="$DisDocTypeSelect"/>";

				var strDocLocation = "<xsl:value-of select="$ImageLocation"/>";
				var strEstimateLockedFlag = "<xsl:value-of select="/Root/Estimate/@EstimateLockedFlag"/>";
				var strUserIsSupervisor = "<xsl:value-of select="/Root/@UserIsSupervisor"/>";
				//var gsServiceChannel = "<xsl:value-of select="/Root/Reference[@List='ServiceChannel' and @ClaimAspectID=$ClaimAspectID]/@Name"/>";
				var strServiceChannelCD = "<xsl:value-of select="/Root/@ServiceChannelCD"/>";
				var gsServiceChannelValue = "<xsl:value-of select="/Root/Reference[@List='ServiceChannel']/@ServiceChannelCD"/>";
				var strEarlyBillFlag = "<xsl:value-of select="/Root/@EarlyBillFlag"/>";
				var strSupervisorFlag = "<xsl:value-of select="/Root/@SupervisorFlag"/>";
				var strApprovedDocumentCount = "<xsl:value-of select="/Root/@ApprovedDocumentCount"/>";
				var strWarrantyExistsFlag = "<xsl:value-of select="/Root/@WarrantyExistsFlag"/>";
				var strApprovedFlag = "<xsl:value-of select="/Root/Estimate/@ApprovedFlag"/>";
				var strApprovedDate = "<xsl:value-of select="/Root/Estimate/@ApprovedDate"/>";
				var strBilledFlag = "<xsl:value-of select="/Root/Estimate/@BilledFlag"/>";
				var strApprovedExpired = false;
				var strAgreedPriceMetCD = "<xsl:value-of select="/Root/Estimate/@AgreedPriceMetCD"/>";
				var blnPriceAgreedChanging = false;

				var strDocEstURL = "";
				var strDocViewURL = "";
				var objWndOriginalDoc = null;
				var intOrigNetTotalAmt = 0;
				var blnCreatePayment = false;

				var gIsSaved = "<xsl:value-of select="$isSaved"/>";

				<![CDATA[

  //function PageInit(){
  function __pageInit()
  {
	  
    strDocViewURL = "EstimateDocView.asp?docPath=" + strDocLocation;
     
	if (gIsSaved == "Saved") {
		document.getElementById("lblSaved").style.display = "inline";
	}
	else {
		document.getElementById("lblSaved").style.display = "none";
	}
	 
	if (gsEstimateUpd == "1") {
		updDocumentType();
		document.getElementById("txtDocumentTypeID").setFocus();
		window.setTimeout("ifrmViewEstimate.frameElement.src = strDocViewURL", 150);
		getEstimates();
		tdDataRetrieve.style.visibility = "hidden";
		tdDataRetrieveSel.style.visibility = "hidden";

        chkApproveDocument.CCDisabled = (strAgreedPriceMetCD != "Y")//false;

		if (strServiceChannelCD == "RRP") {
			chkApproveDocument.CCDisabled = false;		// approved estimate should always be enabled if Repair Referral
        }
		
        if (strServiceChannelCD == "DA") {
			chkApproveDocument.CCDisabled = true;		// approved estimate should always be disabled if Desk Audit

        } else {
        
			if (strApprovedDocumentCount != "0"){
				if (strApprovedFlag == "0"){
				   chkApproveDocument.CCDisabled = true;
				   strApprovedExpired = true;
				} else {
					var dtApproved = null;
					if (strApprovedDate != ""){
						dtExpired = new Date(formatSQLDateTime(strApprovedDate));
						dtExpired.setMinutes(dtExpired.getMinutes() + 30);
						dtNow = new Date();
	
						if (dtNow >= dtExpired){
							chkApproveDocument.CCDisabled = true;
							strApprovedExpired = true;
						}
	
						if (strSupervisorFlag == "1"){
							chkApproveDocument.CCDisabled = false;
						}
					}
				}
			}
			if (strBilledFlag == "1"){
				chkApproveDocument.CCDisabled = true;
			}
		}

    } else {
		document.getElementById("trClaimAspectID").style.display = "inline";
		document.getElementById("trServiceChannelID").style.display = "inline";
		document.getElementById("txtClaimAspectID").setFocus();

		if (document.getElementById("btnLockEstimate"))
			document.getElementById("btnLockEstimate").style.display = "none";
		if (document.getElementById("divLockEstimate"))
			document.getElementById("divLockEstimate").style.display = "none";
    }
    
    if (document.getElementById("txtTaxTotalAmt"))
       document.getElementById("txtTaxTotalAmt").style.display = "none";
    
    if (gsEstimateUpd == "1" && strServiceChannelCD == "PS"){
		if (document.getElementById("txtTaxTotalAmt"))
			document.getElementById("txtTaxTotalAmt").style.display = "inline";
		trTaxTotal.style.display = "inline";
    }
    if (strEstimateLockedFlag == "1"){
		if (gsEstimateUpd == "1"){
			lockEstimateData(true);
		}
    }

/*
	if (gsServiceChannelValue == "RRP") {		
			chkApproveDocument.CCDisabled = true;			
     
	}
*/

  }
  
  function lockEstimateData(blnLocked){
		document.getElementById("txtDocumentTypeID").CCDisabled = blnLocked;
		document.getElementById("txtSupplementSeqNumber").CCDisabled = blnLocked;
		document.getElementById("txtDuplicateFlag").CCDisabled = blnLocked;
		document.getElementById("txtEstimateTypeCD").CCDisabled = blnLocked;
		document.getElementById("txtAgreedPriceMetCD").CCDisabled = blnLocked;
		document.getElementById("txtRepairTotalAmt").CCDisabled = blnLocked;
		document.getElementById("txtBettermentAmt").CCDisabled = blnLocked;
		document.getElementById("txtDeductibleAmt").CCDisabled = blnLocked;
		document.getElementById("txtOtherAdjustmentAmt").CCDisabled = blnLocked;
		document.getElementById("chkApproveDocument").CCDisabled = blnLocked;
		document.getElementById("chkWarrantyDocument").CCDisabled = blnLocked;
		document.getElementById("btnSave").CCDisabled = blnLocked;
         
        if (strUserIsSupervisor != "1"){
            if (gsEstimateUpd == "1"){
				document.getElementById("btnLockEstimate").CCDisabled = true;
            }
        }
  }

  function updDocumentType() {
	if (document.getElementById("txtDocumentTypeID").value == "3" || document.getElementById("txtDocumentTypeID").value == "10") {
		document.getElementById("trDuplicateFlag").style.display = "inline"
		document.getElementById("trEstimateTypeCD").style.display = "inline"
		document.getElementById("trAgreedPriceMetCD").style.display = "inline"
		document.getElementById("tblTotals").style.display = "inline"
		
		if (gsFileUploadFlag == 1) 
			document.getElementById("trDataRetrieve").style.display = "inline"


		if (document.getElementById("divLockEstimate"))
			document.getElementById("divLockEstimate").style.display = "inline";
         
		if (gsEstimateUpd == "1") updDuplicateFlag();
    } else {
		document.getElementById("trDuplicateFlag").style.display = "none";
		document.getElementById("trEstimateTypeCD").style.display = "none";
		document.getElementById("trAgreedPriceMetCD").style.display = "none";
		document.getElementById("tblTotals").style.display = "none";
		
		try {
			document.getElementById("trDataRetrieve").style.display = "inline";
			document.getElementById("trDataRetrieve").cols[0].style.visible = "hidden";
			document.getElementById("trDataRetrieve").cols[1].style.visible = "hidden";
			document.getElementById("trDataRetrieve").cols[2].style.visible = "visible";
		} catch (e) {}
		
		if (document.getElementById("divLockEstimate"))
			document.getElementById("divLockEstimate").style.display = "none";
      
    }

	if (document.getElementById("txtDocumentTypeID").value == "10")
		document.getElementById("txtSupplementSeqNumber").style.display = "inline";
    else {
		document.getElementById("txtSupplementSeqNumber").style.display = "none";
		document.getElementById("txtSupplementSeqNumber").value = "0";
    }
  }
  
  function getEstimates(){
      var strPertainsTo = "";
      var strClaimAspectID = "";
      if (gsEstimateUpd == "1"){
         strClaimAspectID = gsClaimAspectID;
         var objPertainsTo = xmlReference.selectSingleNode("//Reference[@List='PertainsTo' and @ClaimAspectID='" + strClaimAspectID + "']");
         if (objPertainsTo){
            strApprovedDocumentCount = objPertainsTo.getAttribute("ApprovedDocumentCount");
         }
      } else {
         strPertainsTo = document.getElementById("txtClaimAspectID").value;
         var objPertainsTo = xmlReference.selectSingleNode("//Reference[@List='PertainsTo' and @ReferenceID='" + strPertainsTo + "']");
         if (objPertainsTo){
            var strClaimAspectID = objPertainsTo.getAttribute("ClaimAspectID");
            strApprovedDocumentCount = objPertainsTo.getAttribute("ApprovedDocumentCount");
         }
      }
      
      if (strClaimAspectID != ""){
         var sProc = "uspClaimVehicleDocumentsGetDetailXML";
         var sRequest = "ClaimAspectID=" + strClaimAspectID + "&InsuranceCompanyID=" + gsInsuranceCompanyID + "&DocumentCD=E";
         var aRequests = new Array();
         aRequests.push( { procName : sProc,
                           method   : "executespnpasxml",
                           data     : sRequest }
                      );
                      
         AsyncXMLSave(makeXMLSaveString(aRequests), initPertainsToEstimates, handleErrorPertainsToEstimate);
      } else {
         alert("ClaimAspectID could not be determined.");
      }
  }
  
  function initPertainsToEstimates(objXML){
      var xmlData = objXML.xml.selectSingleNode("//StoredProc[@name='uspClaimVehicleDocumentsGetDetailXML']/Result/Root");
      if (xmlData){
         xmlPertainsToEstimates.loadXML(xmlData.xml);
         if (document.getElementById("txtEstimateTypeCD").value == "A"){
            showOriginalData();
            updTotals();
         }
      }
  }
  
  function handleErrorPertainsToEstimate(objXML){
  }
  
  function updDeductibleAmt()
  {
    var loSelVal = document.getElementById("txtClaimAspectID").value;
    var sDeductibleAmt;
    if (loSelVal != "")
    {
      xmlReference.setProperty("SelectionLanguage", "XPath");
      var strXPath;
      strXPath = '//Reference[@List="PertainsTo"][@ReferenceID="' + loSelVal + '"]';
      sDeductibleAmt = xmlReference.documentElement.selectSingleNode(strXPath).getAttribute("DeductibleAmt");
      document.getElementById("txtDeductibleAmt").value = sDeductibleAmt;
      document.getElementById("txtDeductibleAmt").contentSaved();
      document.getElementById("DedAmtOriginal").innerText = sDeductibleAmt;
      updTotals();

      //show/hide audited estimate type
      var bShowAudited = xmlReference.documentElement.selectSingleNode(strXPath).getAttribute("ShowAudited");
      if (bShowAudited == 1){
        //check if audited item exist in the dropdown
        if (gsEstimateTypeAuditedID != "") {
          if (txtEstimateTypeCD) {
            var bFound = false;
            for (var i=0; i<txtEstimateTypeCD.Options.length; i++) {
              if (txtEstimateTypeCD.Options[i].value == gsEstimateTypeAuditedID) {
                bFound = true;
                break;
              }
            }
            if (!bFound) {
              //Audited item not found. so add it.
              txtEstimateTypeCD.AddItem(gsEstimateTypeAuditedID, "Audited");
            }
          }
        }
      } else {
        if (gsEstimateTypeAuditedID != "") {
          if (txtEstimateTypeCD) {
            var bFound = false;
            for (var i=0; i<txtEstimateTypeCD.Options.length; i++) {
              if (txtEstimateTypeCD.Options[i].value == gsEstimateTypeAuditedID) {
                txtEstimateTypeCD.RemoveItem(i);
                break;
              }
            }
          }
        }
      }
    }
    else
    {
      document.getElementById("txtDeductibleAmt").value = "";
      document.getElementById("DedAmtOriginal").innerText = "";
    }
    refreshChannelList();
  }

  function updTotals()
  {
	var loNetTotalAmt = document.getElementById("txtNetTotalAmt");
   //alert(Number(document.getElementById("txtDeductibleAmt").value));
   //if (event.srcElement.id == "txtDeductibleAmt")
   //   document.getElementById("txtDeductibleAmt").contentSaved();
	var Total = Number(document.getElementById("txtRepairTotalAmt").value) - Number(document.getElementById("txtBettermentAmt").value) - Number(document.getElementById("txtDeductibleAmt").value) - Number(document.getElementById("txtOtherAdjustmentAmt").value);
   var loNetTotalAmt = document.getElementById("txtNetTotalAmt");
   var NetTotal = Number(document.getElementById("txtRepairTotalAmt").value) - Number(document.getElementById("txtBettermentAmt").value) - Number(document.getElementById("txtOtherAdjustmentAmt").value);
   var OrigTotal = Number(document.getElementById("divOrigRepairTotal").innerText) - Number(document.getElementById("divOrigBetterment").innerText) - Number(document.getElementById("divOrigOthAdj").innerText);
    //if (intOrigNetTotalAmt > 0 && parseFloat(Total) > 0) {
    if (OrigTotal > 0) {
      if (NetTotal < OrigTotal){
         //divSavings.innerText = formatCurrency(((intOrigNetTotalAmt - Total) * 1.00 / intOrigNetTotalAmt) * 100.00) + "%";
         divSavings.innerText = formatCurrency(((OrigTotal - NetTotal) * 1.00 / OrigTotal) * 100.00) + "%";
      } else {
         divSavings.innerText = "--";
      }
    } else {
      divSavings.innerText = "--";
    }

    if (Total == "") Total = "0.00";
    
    if ((gsEstimateUpd == "1") && (Total < 0)) Total = "0.00";
    
    loNetTotalAmt.CCDisabled = false;
    loNetTotalAmt.value = Total;
    loNetTotalAmt.CCDisabled = true;
    
  }

	function chkNetTotalDiff()
	{
    var sNetValue = parseFloat(document.getElementById("txtNetTotalAmt").value);
    var sHighestNetValue = parseFloat(gsHighestNetTotal);
    if (sHighestNetValue != NaN)
    {
      var relPercent = sHighestNetValue * 0.3;
      if (sNetValue > sHighestNetValue + relPercent || sNetValue < sHighestNetValue - relPercent)
        return true;
      else
        return false;
    }
  }

  var inUpload_Pressed = false;
  function verifySubmit()
  {
	  var lobtnSave = document.getElementById("btnSave");
    lobtnSave.CCDisabled = true;
    inUpload_Pressed = true;

    var sWarnUser = "";

    // In case the user changed Duplicate back to NO
    /*if (document.getElementById("txtEstimateTypeCD").value == "A" && gsVehCount > 1)
      document.getElementById("txtDuplicateFlag").value = "1";*/

	  if (gsEstimateUpd != "1")
    {
      var liFileSizeLimit = 2000;
      var ifrmDoc = parent.document.getElementById("ifrmDocUploadBrowse").contentWindow;
      var loFiles = ifrmDoc.document.getElementById("filUpload");
      var lsFiles = loFiles.value;
      var aFiles = lsFiles.split(";");
      var lsFile, lsFileExt;
      if (document.getElementById("txtDocumentTypeID").value == "3" || document.getElementById("txtDocumentTypeID").value == "10") {
        if (aFiles.length > 1) {
          sWarnUser = "<li> Cannot upload multiple documents for Estimate/Supplement</li><br>";
        }
      }

      var loFSO = new ActiveXObject("Scripting.FileSystemObject");
      for (var i = 0; i < aFiles.length; i++) {
        lsFile = aFiles[i];
        lsFile = lsFile.replace(/\\/g, "\\\\"); //escape the back-slash
        lsFile = lsFile.replace(/\"/g, ""); // remove the double quote "

        lsFileExt = loFSO.GetExtensionName(lsFile);
        lsFileExt = lsFileExt.toLowerCase();

        if (lsFile == "")
        {
          sWarnUser +=  "<li> Specify a file to upload by clicking on the Browse button.</li><br>";
        }
        else if (lsFileExt != "pdf" && lsFileExt != "tif" && lsFileExt != "jpg" && lsFileExt != "gif" && lsFileExt != "bmp" && lsFileExt != "doc")
        {
          sWarnUser +=  "<li> The file type you are uploading " + lsFile + " is not allowable.</li><br>";
          sWarnUser +=  "   Please upload files with extensions of PDF TIF JPG GIF BMP DOC.<br>";
        }
        else
        {
          var liFileSize = chkFileSize(lsFile);

          if (liFileSize > liFileSizeLimit)
            sWarnUser +=  "<li> The file " + lsFile + " exceeds the 2 MB size limit.</li><br>";
          else if (liFileSize == 0)
            sWarnUser +=  "<li> The file specified is zero bytes. Please check the file you are uploading.</li><br>";
        }
      }
      loFSO = null;
    }

	  if (gsEstimateUpd != "1")
    {
      if (document.getElementById("txtDocumentTypeID").value != "10")
        document.getElementById("txtSupplementSeqNumber").value = "0";

      if (document.getElementById("txtClaimAspectID").hasItemSelected(false) == false)
        sWarnUser +=  "<li> A Pertains To must be specified.</li><br>";

      if (document.getElementById("txtChannelID").hasItemSelected(false) == false)
        sWarnUser +=  "<li> A Service Channel must be specified.</li><br>";
    }

    if (document.getElementById("txtDocumentTypeID").hasItemSelected(false) == false)
      sWarnUser +=  "<li> A Document Type must be specified.</li><br>";

	  if (document.getElementById("txtDocumentTypeID").value == "3" || document.getElementById("txtDocumentTypeID").value == "10")
    {
      if (gsDocumentVANSourceFlag != 1)
      {
        if (document.getElementById("txtDocumentTypeID").value == "10" && (document.getElementById("txtSupplementSeqNumber").hasItemSelected(false) == false || document.getElementById("txtSupplementSeqNumber").value == "0"))
          sWarnUser +=  "<li> Select a Supplement number from the drop-down.</li><br>";
      }

      if (document.getElementById("txtDuplicateFlag").hasItemSelected(false) == false)
        sWarnUser += "<li> Duplicate selection is required.</li><br>";

      if (document.getElementById("txtEstimateTypeCD").hasItemSelected(false) == false)
        sWarnUser += "<li> Original/Audit selection is required.</li><br>";

      if (document.getElementById("txtAgreedPriceMetCD").hasItemSelected(false) == false && document.getElementById("txtEstimateTypeCD").value == "A" && document.getElementById("txtDuplicateFlag").value == "0")
        sWarnUser += "<li> Price Agreed selection is required for non-duplicate audited documents.</li><br>";

      if (gsDocumentVANSourceFlag != 1 && document.getElementById("txtDuplicateFlag").value == "0")
      {
        if (document.getElementById("txtRepairTotalAmt").value == "")
          sWarnUser += "<li> Repair Total amount is required.</li><br>";

        if (document.getElementById("txtDeductibleAmt").value == "")
          sWarnUser += "<li> Deductible amount is required.</li><br>";

        if (document.getElementById("txtOtherAdjustmentAmt").value == "")
          sWarnUser += "<li> Other Adjustment amount is required.</li><br>";
      }
    }
    else
    {
      document.getElementById("txtDuplicateFlag").value = "";
      document.getElementById("txtEstimateTypeCD").value = "";
    }
    
    /*if ((strServiceChannelCD == "PS") && (txtDocumentTypeID.value == 3 || txtDocumentTypeID.value == 10) ){
      if (txtTaxTotalAmt.value == ""){
         sWarnUser += "<li>Total Tax amount is required. Please enter the total tax amount from the Estimate/Supplement.</li><br>";
      }
    }*/

    if (sWarnUser != '')
    {
      ClientWarning(sWarnUser);
      lobtnSave.CCDisabled = false;
      inUpload_Pressed = false;
      gsFile = "";
      return false;
    }
    else
    {
      if (gsDocumentVANSourceFlag != 1 && (document.getElementById("txtDocumentTypeID").value == "3" || document.getElementById("txtDocumentTypeID").value == "10"))
      {
        if (doQCCheck() == false) {
            lobtnSave.CCDisabled = false;
            inUpload_Pressed = false;
            return false;
        }
      }
      
  	  if ((document.getElementById("txtDocumentTypeID").value == "3" || document.getElementById("txtDocumentTypeID").value == "10") && (gsDocumentVANSourceFlag != 1))
      {
        if (document.getElementById("txtNetTotalAmt").value < 0)
        {
          var sRet = YesNoMessage("Negative Net Total Warning", "The Net Total value is negative and will be changed to $0.00. Press YES to continue, or NO to cancel and correct the values entered.");
          if (sRet != "Yes")
          {
            document.getElementById("txtRepairTotalAmt").setFocus();
            lobtnSave.CCDisabled = false;
            inUpload_Pressed = false;
            gsFile = "";
            return false;
          }
          else
          {
        	  var loNetTotalAmt = document.getElementById("txtNetTotalAmt");
            loNetTotalAmt.CCDisabled = false;
            loNetTotalAmt.value = "0.00";
            loNetTotalAmt.CCDisabled = true;
          }
        }
      }

     lobtnSave.CCDisabled = false;
     //return;
     //alert("After return");

  	  if (gsEstimateUpd != "1")
        showProgressGif('inline');

  	  if (gsEstimateUpd == "1")
      {
        contSaveDoc(gsDocumentID);
      }
      else
      {
        var lsDocPertainsTo = document.getElementById("txtClaimAspectID").value;
        var lsClaimAspectServiceChannelID = document.getElementById("txtChannelID").value;
        var lsFileType = document.getElementById("txtDocumentTypeID").text;
        var lsSupSeqNumber = document.getElementById("txtSupplementSeqNumber").value;

        xmlReference.setProperty("SelectionLanguage", "XPath");
        var strXPath;
        strXPath = '//Reference[@List="PertainsTo"][@ReferenceID="' + lsDocPertainsTo + '"]';
        gsClaimAspectID = xmlReference.documentElement.selectSingleNode(strXPath).getAttribute("ClaimAspectID");

        //var lsFileUploadParams = gsLynxID +"|"+ escape(lsDocPertainsTo) +"|"+ escape(lsFileType) +"|"+ lsSupSeqNumber +"|"+ gsUserID;
        //document.cookie = "APDFileUploadParams=" + lsFileUploadParams;
        window.setTimeout("callUpload('" + gsLynxID + "', '" + lsDocPertainsTo +
            "', '" + lsClaimAspectServiceChannelID + "', '" + lsFileType + "', '" +
            lsSupSeqNumber + "', '" + gsUserID + "')", 150);
      }
    }
  }
  
  function doQCCheck(){
      var strDocumentType = document.getElementById("txtDocumentTypeID").value;
      var strDocumentTypeText = document.getElementById("txtDocumentTypeID").text;
      var strSuppSeqNo = document.getElementById("txtSupplementSeqNumber").value;
      var strDuplicate = document.getElementById("txtDuplicateFlag").value;
      var strEstimateTypeCD = document.getElementById("txtEstimateTypeCD").value;
      var objOriginalEstimate = null;
      var objDupDocument = null;
      
      //adding a non-duplicate. check to see if this document already exists
      if (strDuplicate == "0"){
         if (parseInt(gsDocumentID, 10) > 0){
            objDupDocument = xmlPertainsToEstimates.selectSingleNode("//Document[@DocumentID != '" + gsDocumentID + "' and @ServiceChannelCD='" + strServiceChannelCD + "' and @DocumentTypeID = '" + strDocumentType + "' and @SupplementSeqNumber = '" + strSuppSeqNo + "' and @EstimateTypeCD = '" + strEstimateTypeCD + "' and @DuplicateFlag='0']");
         } else {
            objDupDocument = xmlPertainsToEstimates.selectSingleNode("//Document[@ServiceChannelCD='" + strServiceChannelCD + "' and @DocumentTypeID = '" + strDocumentType + "' and @SupplementSeqNumber = '" + strSuppSeqNo + "' and @EstimateTypeCD = '" + strEstimateTypeCD + "' and @DuplicateFlag='0']");
         }
         
         if (objDupDocument){
            // duplicate document exists. Cannot add/update
            ClientWarning("Another " + (strEstimateTypeCD == "A" ? "Audited " : "Original ") + strDocumentTypeText + (strDocumentType == "10" ? " " + strSuppSeqNo : "") + " exists. Cannot have a duplicate.");
            return false;
         }

         if (strEstimateTypeCD == "A"){
            //adding/updating an audited estimate/supplement
            if (parseInt(gsDocumentID, 10) > 0){
               //updating an existing estimate/supplement
               objOriginalEstimate = xmlPertainsToEstimates.selectSingleNode("//Document[@ServiceChannelCD='" + strServiceChannelCD + "' and @DocumentID != '" + gsDocumentID + "' and @DocumentTypeID = '" + strDocumentType + "' and @EstimateTypeCD = 'O'" + (strSuppSeqNo == null ? "" : " and @SupplementSeqNumber='" + strSuppSeqNo + "'") + "]");
            } else {
               //uploading a new estimate/supplement
               objOriginalEstimate = xmlPertainsToEstimates.selectSingleNode("//Document[@ServiceChannelCD='" + strServiceChannelCD + "' and @DocumentTypeID = '" + strDocumentType + "' and @EstimateTypeCD = 'O'" + (strSuppSeqNo == null ? "" : " and @SupplementSeqNumber='" + strSuppSeqNo + "'") + "]");
            }
            
            if (objOriginalEstimate == null){
               // missing original
               ClientWarning("A Original " + strDocumentTypeText + (strDocumentType == "10" ? " " + strSuppSeqNo : "") + " is missing.\n" + 
                              "Please add this missing document and try again.");
               return false;
            } else {
               // check if the audited net amount is greater than original
               var auditedNetAmount = parseFloat(document.getElementById("txtNetTotalAmt").value);
               var originalNetAmount = objOriginalEstimate.getAttribute("NetEstimateAmt");
               
               //alert("Audited: " + auditedNetAmount + "\nOriginal: " + originalNetAmount);
               
               if (isNaN(originalNetAmount)) originalNetAmount = 0;
               if (auditedNetAmount > originalNetAmount){
                  ClientWarning("Audited amount cannot be greater than the original amount of $" + originalNetAmount);
                  return false;
               }
               
               if (auditedNetAmount < originalNetAmount){
                  if (originalNetAmount <= 2500){
                     if (auditedNetAmount < (originalNetAmount * 0.7)){
                        var sPromptRet = YesNoMessage("Is the Total Net Amount Correct?", "The Net Total value differs from the Original " + strDocumentTypeText + (strDocumentType == "10" ? " " +  strSuppSeqNo : "")  + " by over 30%.\nIs this correct?.\n\nPress YES to continue, or NO to cancel and correct.");
                        if (sPromptRet != "Yes"){
                           return false;
                        }
                     }
                  } else {
                     if (auditedNetAmount < (originalNetAmount * 0.8)){
                        var sPromptRet = YesNoMessage("Is the Total Net Amount Correct?", "The Net Total value differs from the Original " + strDocumentTypeText + (strDocumentType == "10" ? " " + strSuppSeqNo : "")  + " by over 20%.\nIs this correct?.\n\nPress YES to continue, or NO to cancel and correct.");
                        if (sPromptRet != "Yes"){
                           return false;
                        }
                     }
                  }
               }
            }
         }         
      }
      return true;
  }

  function callUpload(gsLynxID, lsDocPertainsTo, lsClaimAspectServiceChannelID, lsFileType, lsSupSeqNumber, gsUserID) {
    var strWarrantyFlag = chkWarrantyDocument.value;
    var ifrmDoc = parent.document.getElementById("ifrmDocUploadBrowse").contentWindow;
    ifrmDoc.uploadFile(gsLynxID, lsDocPertainsTo, lsClaimAspectServiceChannelID, lsFileType, lsSupSeqNumber, gsUserID, strWarrantyFlag);
  }

	function contSaveDoc(docID)
	{
	  var lobtnSave = document.getElementById("btnSave");
    gsDocumentID = docID;

    if (gsDocumentID == 0)
    {
      showProgressGif('none');
      ClientWarning("Unable to submit document to the server. Please try again. <br>If the problem persists, please contact your supervisor.");
      lobtnSave.CCDisabled = false;
      inADS_Pressed = false;
      return false;
    }
    else
    {
      if (document.getElementById("txtDocumentTypeID").value != "3" && document.getElementById("txtDocumentTypeID").value != "10") //no need to prcess further for non-estimate/supplement
      {
        return;
      }

      var sProcName = "uspEstimateQuickUpdDetail";

      var lsEstimateTypeCD = document.getElementById("txtEstimateTypeCD").value;
      var sAdjTotal = Number(document.getElementById("txtDeductibleAmt").value) + Number(document.getElementById("txtOtherAdjustmentAmt").value);
      var lsAgreedPriceMetCD;
      if (document.getElementById("txtAgreedPriceMetCD").value == null)
        lsAgreedPriceMetCD = "";
      else
        lsAgreedPriceMetCD = document.getElementById("txtAgreedPriceMetCD").value;

      sAdjTotal = Math.round(sAdjTotal*100)/100; //round to 2 decimal places
      var sRequest =  "DocumentID=" + gsDocumentID +
                      "&ClaimAspectID=" + gsClaimAspectID +
                      "&AdjustmentTotalAmt=" + sAdjTotal +
                      "&DeductibleAmt=" + document.getElementById("txtDeductibleAmt").value +
                      "&DocumentTypeID=" + document.getElementById("txtDocumentTypeID").value +
                      "&DuplicateFlag=" + document.getElementById("txtDuplicateFlag").value +
                      "&EstimateTypeCD=" + document.getElementById("txtEstimateTypeCD").value +
                      "&NetTotalAmt=" + document.getElementById("txtNetTotalAmt").value +
                      "&OtherAdjustmentAmt=" + document.getElementById("txtOtherAdjustmentAmt").value +
                      "&TaxTotalAmt=" + document.getElementById("txtTaxTotalAmt").value +
                      "&RepairTotalAmt=" + document.getElementById("txtRepairTotalAmt").value +
                      "&BettermentTotalAmt=" + document.getElementById("txtBettermentAmt").value +
                      "&SupplementSeqNumber=" + document.getElementById("txtSupplementSeqNumber").value +
                      "&AgreedPriceMetCD=" + lsAgreedPriceMetCD +
                      "&ApprovedFlag=" + chkApproveDocument.value +
                      "&WarrantyFlag=" + chkWarrantyDocument.value + 
                      "&DocumentEditFlag=" + gsEstimateUpd + 
                      "&UserID=" + gsUserID;

      //alert(sRequest); return;
      var aRequests = new Array();
      aRequests.push( { procName : sProcName,
                        method   : "ExecuteSpNpAsXML",
                        data     : sRequest }
                    );
      var sXMLRequest = makeXMLSaveString(aRequests);
      var objRet = XMLSave(sXMLRequest);
      if (objRet && objRet.code == 0 && objRet.xml) {
         //update success
         if (gsEstimateUpd == "1")
         {
            var lsDocType, lsDocTypeName = document.getElementById("txtDocumentTypeID").text;
            var lsDocTypeID = document.getElementById("txtDocumentTypeID").value;

            if ( lsDocTypeID == 3 )
            {
               lsDocType = "E0";
            }
            else if ( lsDocTypeID == 10 )
            {
               var suppSeqNum = document.getElementById("txtSupplementSeqNumber").value;
               lsDocType = "S" + ( suppSeqNum > 9 ? suppSeqNum : "0" + suppSeqNum );
               lsDocTypeName += " " + suppSeqNum;
            }

            var lsRepairTotalAmt = document.getElementById("txtRepairTotalAmt").value;
            var lsNetTotalAmt = document.getElementById("txtNetTotalAmt").value;
            var lsEstimateTypeCD = document.getElementById("txtEstimateTypeCD").value;
            var lsDuplicate = document.getElementById("txtDuplicateFlag").value;

            var oRetXML = objRet.xml;
            if (oRetXML){
               var DocNode = oRetXML.selectSingleNode("//Root/@SysLastUpdatedDate");
               if (DocNode) {
                  sSysLastUpdatedDate = DocNode.nodeValue;
               }
            }

            //approve the document
            if (strApprovedFlag != chkApproveDocument.value){
               sProcName = "uspApproveDocument";
               sRequest =  "DocumentID=" + gsDocumentID +
                            "&ApprovedFlag=" + chkApproveDocument.value +
                            "&NetAmount=" + document.getElementById("txtNetTotalAmt").value +
                            "&DeductibleAmount=" + document.getElementById("txtDeductibleAmt").value +
                            "&TaxTotalAmount=" + document.getElementById("txtTaxTotalAmt").value +
                            "&CreatePaymentFlag=" + (blnCreatePayment == true ? "1" : "0") +
                            "&UserID=" + gsUserID;
   
               var aRequests = new Array();
               aRequests.push( { procName : sProcName,
                                 method   : "ExecuteSpNp",
                                 data     : sRequest }
                             );
               var sXMLRequest = makeXMLSaveString(aRequests);
               var objRet = XMLSave(sXMLRequest);
            }

            sReturnValue = lsDocType + "|" + lsRepairTotalAmt + "|" + lsNetTotalAmt + "|" +
                lsEstimateTypeCD + "|" + lsDuplicate + "|" + lsAgreedPriceMetCD + "|" +
                gsDocumentVANSourceFlag + "|" + sSysLastUpdatedDate + "|" + bReinspectionRequested +
                "|" + lsDocTypeName + "|" + lsDocTypeID + "|" + chkApproveDocument.value;

            //alert(sReturnValue);
            window.close();
         } else {
            //approve the document
            if (strApprovedFlag != chkApproveDocument.value){
               sProcName = "uspApproveDocument";
               sRequest =  "DocumentID=" + gsDocumentID +
                            "&ApprovedFlag=" + chkApproveDocument.value +
                            "&NetAmount=" + document.getElementById("txtNetTotalAmt").value +
                            "&DeductibleAmount=" + document.getElementById("txtDeductibleAmt").value +
                            "&TaxTotalAmount=" + document.getElementById("txtTaxTotalAmt").value +
                            "&CreatePaymentFlag=" + (blnCreatePayment == true ? "1" : "0") +
                            "&UserID=" + gsUserID;
   
               var aRequests = new Array();
               aRequests.push( { procName : sProcName,
                                 method   : "ExecuteSpNp",
                                 data     : sRequest }
                             );
               var sXMLRequest = makeXMLSaveString(aRequests);
               var objRet = XMLSave(sXMLRequest);
            }

            if (parent.document.getElementById("cbDelFile").value == "1")
               delTheFile(gsFile);

            gsFile = "";
            parent.document.getElementById("cbDelFile").value = "0";

            parent.reloadDocDetailsIfrm();
            if (parent.document.getElementById("ifrmDocDisplay").style.display == "inline") {
               parent.document.getElementById("ifrmDocDisplay").style.display = "none";
               parent.document.getElementById("ifrmDocDisplay").src = "blank.asp";
            }
            
            if (document.getElementById("chkLockEstimate")) {
               if (document.getElementById("chkLockEstimate").value == "1"){
                  lockUnlockEstimate();
               }
            }
         }
      } else {
         return;
      }
    }
  }

  function closeMe()
  {
    sReturnValue = "0|" + bReinspectionRequested;
    window.close();
  }

  function showProgressGif(state)
  {
    if (document.getElementById('progressGif'))
      document.getElementById('progressGif').style.display = state;
  }


  function chkFileSize(sFile)
  {
    var sFileSize;
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    if (fso.FileExists(sFile))
    {
      var f = fso.GetFile(sFile);
      if (f)
      {
        sFileSize = Math.round(f.size / 1024);
        return sFileSize;
      }
    }
    else
    {
      ClientWarning("Unable to find file.<br>Please check the file name and path.");
      return false;
    }
  }

  function delTheFile(sFile)
  {
    var sFileSize;
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    if (fso.FileExists(sFile))
    {
      var f = fso.GetFile(sFile);
      f.Delete();
      if (fso.FileExists(sFile))
      {
        ClientWarning("Unable to delete file.<br>Please delete the file manually.");
        return false;
      }
      else
        return true;
    }
    else
    {
      //File does not exist. It must have been deleted by the upload process.
      //ClientWarning("Unable to find file when deleting.<br>Please delete the file manually.");
      return true;
    }
  }


  function submitRetrieveData()
  {
    document.all("SupplementNo").value = txtSupplementSeqNumber.value;
    document.all("EstimatingPackage").value = txtDataRetrieve.value;
    document.frmRetrieveData.submit();
  }


  function dsplRetrieveDataMsg(sMsg)
  {
    ClientWarning(sMsg);
  }

  // Calls the image viewer and makes it visible in the page
  function showPricing()
  {
    var sHREF = "/ShopPricingHistory.asp?WindowID=" + gsWindowID + "&DocumentID=" + gsDocumentID + "&docPath=" + gsImageRootDir + gsImageLocation;
    window.showModelessDialog(sHREF, "", "dialogHeight:630px; dialogWidth:840px; dialogTop:120px; dialogLeft:474px; resizable:no; status:no; help:no; center:no;");
  }

  function requestReinspection(){
    frmReIRequest.DocumentID.value = gsDocumentID;
    frmReIRequest.UserID.value = gsUserID;
    if (btnReqReinspection.value == "Request Reinspection"){
      disableControls(true);
      bReIRequested = true;
      var sURL = "/frmComments.htm";
      var sArgs = "dialogWidth:350px;dialogHeight:165px;scrollbars:no;center:yes;location:no;directories:no;status:no;menubar:no;toolbar:no;resizable:no;help:no";
      var sRet = window.showModalDialog(sURL, "", sArgs);
      frmReIRequest.Comments.value = sRet;
      frmReIRequest.fn.value = "request";
    } else {
      var sRet = YesNoMessage("Confirm Cancellation", "Do you want to cancel the reinspection request?");
      if (sRet != "Yes") return;
      disableControls(true);
      frmReIRequest.fn.value = "cancel";
    }
    frmReIRequest.submit();
  }

  function disableControls(bDisable){
    if (btnReqReinspection)
      btnReqReinspection.CCDisabled = bDisable;
    btnClose.CCDisabled = bDisable;
    btnSave.CCDisabled = bDisable;
  }

  function showToolTip(obj){
    if (toolTip)
      toolTip.style.display = "inline";
  }

  function hideToolTip(obj){
    if (toolTip)
      toolTip.style.display = "none";
  }

  function doUnload(){
    if (sReturnValue == "") sReturnValue = "0|" + bReinspectionRequested;
    window.returnValue = sReturnValue;
  }

  function updDuplicateFlag()
  {
    /*if (document.getElementById("txtEstimateTypeCD").value == "A" && gsVehCount > 1)
      document.getElementById("txtDuplicateFlag").value = "1";*/
      
    colOrigData.style.display = (txtEstimateTypeCD.value == "A" ? "inline" : "none");
    if (document.getElementById("txtEstimateTypeCD").value == "A"){
      showOriginalData();
      trSavings.style.visibility = "visible";
    } else {
      trSavings.style.visibility = "hidden";
    }
    
    if (gsEstimateUpd == "1")
      updTotals();
  }

  function refreshChannelList()
  {
      txtChannelID.Clear();
      var sPertainsTo = txtClaimAspectID.value;
      if ( sPertainsTo.length > 0 )
      {
          var oAspectNode = xmlReference.selectSingleNode(
              '/Root/Reference[@List="PertainsTo"][@ReferenceID="' + sPertainsTo + '"]' );
          if ( oAspectNode != null )
          {
              gsClaimAspectID = oAspectNode.getAttribute("ClaimAspectID");
              if ( gsClaimAspectID != null )
              {
                  var oChannels = xmlReference.selectNodes(
                      "/Root/Reference[@List='ServiceChannel' and @ClaimAspectID='" + gsClaimAspectID + "']");
                  for (var i = 0; i < oChannels.length; i++){
                      txtChannelID.AddItem(oChannels[i].getAttribute("ReferenceID"), oChannels[i].getAttribute("Name"), "", i);
                  }
              }
          }
      }
  }
  
  function lockUnlockEstimate(){
      var sRet = "";
      if (strEstimateLockedFlag == "0"){
         var strDocumentName = txtDocumentTypeID.text + (txtDocumentTypeID.value == 10 ? txtSupplementSeqNumber.value : "");
         var sRet = YesNoMessage("Lock " + strDocumentName, "Do you want to lock this " + strDocumentName + "?. You may not be able to change the data once the data is locked.");
      } else {
         sRet = "Yes";
      }
      
      if (sRet == "Yes") {
         var sProcName = "uspEstimateLockUpdDetail";
         var sRequest = "DocumentID=" + gsDocumentID +
                        "&ClaimAspectID=" + gsClaimAspectID +
                        "&LockedFlag=" + (strEstimateLockedFlag == "1" ? "0" : "1") +
                        "&UserID=" + gsUserID;
         var aRequests = new Array();
         aRequests.push( { procName : sProcName,
                           method   : "ExecuteSpNpAsXML",
                           data     : sRequest }
                       );
                       
         var sXMLRequest = makeXMLSaveString(aRequests);
         //alert(sXMLRequest); //return;
         var objRet = XMLSave(sXMLRequest);
         if (objRet && objRet.code == 0 && objRet.xml) {
            //alert(objRet.xml.xml);
            if (gsEstimateUpd == "1"){
               strEstimateLockedFlag = (strEstimateLockedFlag == "1" ? "0" : "1");
               if (strEstimateLockedFlag == "1"){
                  document.getElementById("btnLockEstimate").value = "Unlock " + document.getElementById("txtDocumentTypeID").text;
                  lockEstimateData(true);
               } else {
                  document.getElementById("btnLockEstimate").value = "Lock " + document.getElementById("txtDocumentTypeID").text;
                  lockEstimateData(false);
               }
            }
         }
      }
  }
  
  function enableLockEstimate(){
      if (document.getElementById("btnLockEstimate")){
         if (gsEstimateUpd == "1"){
            btnLockEstimate.CCDisabled = (txtDuplicateFlag.value == "1");
            updDuplicateFlag();
         } else {
            chkLockEstimate.CCDisabled = (txtDuplicateFlag.value == "1");
         }
      }
  }
  
  function showOriginalData(){
      var strDocumentType = document.getElementById("txtDocumentTypeID").value;
      var strSuppSeqNo = document.getElementById("txtSupplementSeqNumber").value;
      var strEstimateTypeCD = document.getElementById("txtEstimateTypeCD").value;
      var objOriginalEstimate = null;
      var strXPath = "";
      
      divOrigCaption.innerText = (strDocumentType == 3 ? "E01" : "S" + (strSuppSeqNo == null ? "" : strSuppSeqNo)) + " Original";

      if (parseInt(gsDocumentID, 10) > 0){
         //updating an existing estimate/supplement
         strXPath = "//Document[@ServiceChannelCD='" + strServiceChannelCD + "' and @DocumentID != '" + gsDocumentID + "' and @DocumentTypeID = '" + strDocumentType + "' and @EstimateTypeCD = 'O'" + (strSuppSeqNo == null ? "" : " and @SupplementSeqNumber='" + strSuppSeqNo + "'") + "]";
      } else {
         //uploading a new estimate/supplement
         strXPath = "//Document[@ServiceChannelCD = '" + strServiceChannelCD + "' and @DocumentTypeID = '" + strDocumentType + "' and @EstimateTypeCD = 'O'" + (strSuppSeqNo == null ? "" : " and @SupplementSeqNumber='" + strSuppSeqNo + "'") + "]";
      }
      objOriginalEstimate = xmlPertainsToEstimates.selectSingleNode(strXPath);
      
      if (objOriginalEstimate){
         divOrigRepairTotal.innerText = objOriginalEstimate.getAttribute("GrossEstimateAmt");
         divOrigBetterment.innerText = objOriginalEstimate.getAttribute("BettermentAmt");
         divOrigDeductible.innerText = objOriginalEstimate.getAttribute("DeductibleAmt");
         divOrigOthAdj.innerText = objOriginalEstimate.getAttribute("OtherAdjustmentsAmt");
         divOrigNetTotalAmt.innerText = objOriginalEstimate.getAttribute("NetEstimateAmt");
         intOrigNetTotalAmt = parseFloat(objOriginalEstimate.getAttribute("NetEstimateAmt"));
      } else {
         divOrigRepairTotal.innerText = "";
         divOrigBetterment.innerText = "";
         divOrigDeductible.innerText = "";
         divOrigOthAdj.innerText = "";
         divOrigNetTotalAmt.innerText = "";
         intOrigNetTotalAmt = 0;
      }
      
  }
  
  function showTaxTotal(){
      strServiceChannelCD = "";
      var oChannel = xmlReference.selectSingleNode(
          "/Root/Reference[@List='ServiceChannel' and @ReferenceID='" + txtChannelID.value + "']");
      if (oChannel){
         strServiceChannelCD = oChannel.getAttribute("ServiceChannelCD");
      }
      if (strServiceChannelCD == "PS"){
         trTaxTotal.style.display = "inline";
          if (document.getElementById("txtTaxTotalAmt"))
             document.getElementById("txtTaxTotalAmt").style.display = "inline";
             
          if(strWarrantyExistsFlag == "1"){
             chkWarrantyDocument.CCDisabled = false;
             chkWarrantyDocument.value = 0;
          }
      } else {
         trTaxTotal.style.display = "none";
          if (document.getElementById("txtTaxTotalAmt"))
             document.getElementById("txtTaxTotalAmt").style.display = "none";
          chkWarrantyDocument.CCDisabled = false;
          chkWarrantyDocument.value = 0;
          chkWarrantyDocument.CCDisabled = true;
      }
      
  }
  
  function updateEarlyBill(){
    
    if (blnPriceAgreedChanging == true) return;

	if (strServiceChannelCD == "DA") return;
    
    if (strBilledFlag == false) {
    
		if (strServiceChannelCD == "DA") {
			chkApproveDocument.value = 0;
			chkApproveDocument.CCDisabled = true;	//disable mark as approved if Desk Audit
			
		} else if (txtAgreedPriceMetCD.value == "Y") {
			chkApproveDocument.title = "";
			if (gsEstimateUpd != "1" && strApprovedDocumentCount == "1") {
				chkApproveDocument.CCDisabled = true;
				chkApproveDocument.title = "Another Estimate/Supplement has been Approved for Early Billing.";
				return;
			}
			if (strApprovedExpired == false) {
				chkApproveDocument.CCDisabled = false;
			} else {
				if (strApprovedFlag == "1") {
				   chkApproveDocument.CCDisabled = false;
				   chkApproveDocument.value = 1;
				   chkApproveDocument.CCDisabled = true;
				}
			}
		} else {
			chkApproveDocument.value = 0;
			chkApproveDocument.CCDisabled = true;
		}
    } else if (strBilledFlag == true) {
		blnPriceAgreedChanging = true;
		ClientWarning("Cannot change the Price Agreed for an Estimate/Supplement that has been Approved for Early Billing and a Invoice already generated.");
		
		if (strAgreedPriceMetCD != "")
			txtAgreedPriceMetCD.value = strAgreedPriceMetCD;
		else
			txtAgreedPriceMetCD.selectedIndex = -1;
			
		blnPriceAgreedChanging = false;
    }
    
    if (gsServiceChannelValue == "RRP") {		
			chkApproveDocument.CCDisabled = true;			
     
	}	
	
	if (strServiceChannelCD == "RRP") {
			
			chkApproveDocument.CCDisabled = false;	//enable mark as approved if Repair Referral
			
		}
		
		
  }
  
  function updateWarranty(){
      blnCreatePayment = false;
      if (chkApproveDocument.value == 1 && strServiceChannelCD != "RRP"){
         chkWarrantyDocument.value = 0;
         chkWarrantyDocument.CCDisabled = true;
         if (strApprovedExpired == false && strEarlyBillFlag == "1") {
            var sRet = YesNoMessage("Add Pay", "Would you like to automatically add a Payment (Net Amount) to the active program shop? Click 'Yes' to add a Payment.");
            if (sRet == "Yes"){
               blnCreatePayment = true;
            }
         }
      } else {
         if (strWarrantyExistsFlag == "1" ) chkWarrantyDocument.CCDisabled = false;
      }
  }
  
    // Show AuditEstimateValues window - WJC - 8/10/2011
  function showAuditEstimateValues()
  {
  
		var sHREF = "AuditEstimateValues.asp?WindowID=" + gsWindowID + "&DocumentID=" + gsDocumentID;

		var winName = "AuditEstimateValues_" + gsLynxID;
		
		// 09Jul2012 - TVD - Increased Height for new fields added
		var sWinAuditEstValues = window.open(sHREF, winName, "Height=300px, Width=450px, Top=300px, Left=300px, resizable=No, status=No, menubar=No");
		sWinAuditEstValues.focus();
  }

]]>
			</SCRIPT>
			<head>
				<title>
					<xsl:value-of select="$PageTitle"/>
				</title>
			</head>
			<body bgcolor="#FFFFCC" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" onLoad1="PageInit();" onbeforeunload="doUnload()" tabindex="-1" style="overflow:hidden;">
				<xsl:if test="$EstimateUpd = '1'">
					<br/>
					<br/>
				</xsl:if>
				<xsl:if test="Estimate/@ReinspectionCompleted = '1'">
					<div id="toolTip" name="toolTip" style="z-index:999;position:absolute;top:90px;left:475px;display:none;height1:150px;width:250px;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)">
						<table id="tblToolTip" border="0" cellspacing="0" cellpadding="0" style="height1:100%;width:100%;border:1px solid #556B2F;background-color:#FFFFFF" onreadystatechange="checkToolTipSize()">
							<tr style="height:21px;text-align:center;font-weight:bold;background-color:#556B2F;">
								<td style="color:#FFFFFF">Reinspection Details</td>
								<td>
									<span id="spnShopLocationID"/>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<table border="0" cellpadding="2" cellspacing="0" style="margin:5px;">
										<colgroup>
											<col width="100px"/>
											<col width="150px"/>
										</colgroup>
										<tr valign="top">
											<td>
												<b>Reinspector:</b>
											</td>
											<td>
												<xsl:value-of select="Estimate/@ReinspectionPerformedBy"/>
											</td>
										</tr>
										<tr valign="top">
											<td>
												<b>Completed on:</b>
											</td>
											<td>
												<xsl:value-of select="js:formatSQLDateTime(string(Estimate/@ReinspectionDate))"/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
				</xsl:if>
				<TABLE unselectable="on" width="580" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed;border-collapse:collapse">
					<colgroup>
						<col width="280"/>
						<col width="300"/>
					</colgroup>
					<TR style="height:175px">
						<!-- original value:  260 -->
						<TD unselectable="on" valign="top" nowrap="">
							<TABLE unselectable="on" width="300" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
								<colgroup>
									<col width="98px"/>
									<col width="200px"/>
								</colgroup>
								<TR id="trClaimAspectID" style="display:none">
									<TD unselectable="on" nowrap="">
										<strong>Pertains To:</strong>
									</TD>
									<TD unselectable="on" nowrap="">
										<IE:APDCustomSelect id="txtClaimAspectID" name="ClaimAspectID" blankFirst="false" canDirty="true" required="true" onChange="getEstimates();updDeductibleAmt()" CCTabIndex="6">
											<xsl:for-each select="/Root/Reference[@List='PertainsTo']">
												<IE:dropDownItem style="display:none">
													<xsl:attribute name="value">
														<xsl:value-of select="@ReferenceID"/>
													</xsl:attribute>
													<xsl:value-of select="@Name"/>
												</IE:dropDownItem>
											</xsl:for-each>
										</IE:APDCustomSelect>
									</TD>
								</TR>
								<TR id="trServiceChannelID" style="display:none">
									<TD unselectable="on" nowrap="">
										<strong>Service Channel:</strong>
									</TD>
									<TD unselectable="on" nowrap="">
										<IE:APDCustomSelect id="txtChannelID" name="ClaimAspectServiceChannelID" width="140" blankFirst="false" canDirty="true" required="true" onChange="showTaxTotal()" CCTabIndex="7">
										</IE:APDCustomSelect>
									</TD>
								</TR>
								<TR>
									<TD unselectable="on" nowrap="">
										<strong>Document Type:</strong>
										<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
									</TD>
									<TD unselectable="on" nowrap="">
										<IE:APDCustomSelect id="txtDocumentTypeID" name="DocumentTypeID" width="130" blankFirst="false" canDirty="true" displayCount="8" required="true" onChange="updDocumentType()" CCTabIndex="8">
											<xsl:if test="$EstimateUpd = '1'">
												<xsl:attribute name="value">
													<xsl:value-of select="Estimate/@DocumentTypeID"/>
												</xsl:attribute>
											</xsl:if>
											<xsl:if test="Estimate/@DocumentVANSourceFlag = '1' or $DisDocTypeSelect = '1'">
												<xsl:attribute name="CCDisabled">true</xsl:attribute>
											</xsl:if>
											<xsl:for-each select="/Root/Reference[@List='DocumentType']">
												<xsl:if test="@ReferenceID != '4' and @ReferenceID != '15'">
													<xsl:choose>
														<xsl:when test="$EstimateUpd = '1'">
															<xsl:if test="@ReferenceID = '3' or @ReferenceID = '10'">
																<IE:dropDownItem style="display:none">
																	<xsl:attribute name="value">
																		<xsl:value-of select="@ReferenceID"/>
																	</xsl:attribute>
																	<xsl:value-of select="@Name"/>
																</IE:dropDownItem>
															</xsl:if>
														</xsl:when>
														<xsl:otherwise>
															<IE:dropDownItem style="display:none">
																<xsl:attribute name="value">
																	<xsl:value-of select="@ReferenceID"/>
																</xsl:attribute>
																<xsl:value-of select="@Name"/>
															</IE:dropDownItem>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:if>
											</xsl:for-each>
										</IE:APDCustomSelect>
										<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;

										<IE:APDCustomSelect id="txtSupplementSeqNumber" name="SupplementSeqNumber" width="34" blankFirst="false" canDirty="true" displayCount="8" required="true" style="display:none" CCTabIndex="9" onChange="showOriginalData()">
											<xsl:if test="$EstimateUpd = '1'">
												<xsl:attribute name="value">
													<xsl:value-of select="Estimate/@SupplementSeqNumber"/>
												</xsl:attribute>
											</xsl:if>
											<xsl:if test="Estimate/@DocumentVANSourceFlag = '1' and Estimate/@FullSummaryExistsFlag = '1'">
												<xsl:attribute name="CCDisabled">true</xsl:attribute>
											</xsl:if>
											<IE:dropDownItem value="0" style="display:none"></IE:dropDownItem>
											<IE:dropDownItem value="1" style="display:none">1</IE:dropDownItem>
											<IE:dropDownItem value="2" style="display:none">2</IE:dropDownItem>
											<IE:dropDownItem value="3" style="display:none">3</IE:dropDownItem>
											<IE:dropDownItem value="4" style="display:none">4</IE:dropDownItem>
											<IE:dropDownItem value="5" style="display:none">5</IE:dropDownItem>
											<IE:dropDownItem value="6" style="display:none">6</IE:dropDownItem>
											<IE:dropDownItem value="7" style="display:none">7</IE:dropDownItem>
											<IE:dropDownItem value="8" style="display:none">8</IE:dropDownItem>
											<IE:dropDownItem value="9" style="display:none">9</IE:dropDownItem>
											<IE:dropDownItem value="10" style="display:none">10</IE:dropDownItem>
										</IE:APDCustomSelect>
									</TD>
								</TR>
								<TR id="trDuplicateFlag" style="display:none">
									<TD unselectable="on" nowrap="">
										<strong>Duplicate:</strong>
									</TD>
									<TD unselectable="on" nowrap="">
										<IE:APDCustomSelect id="txtDuplicateFlag" name="DuplicateFlag" blankFirst="false" canDirty="true" displayCount="8" required="true" CCTabIndex="10" onchange="enableLockEstimate()">
											<xsl:if test="$EstimateUpd = '1'">
												<xsl:attribute name="value">
													<xsl:value-of select="Estimate/@DuplicateFlag"/>
												</xsl:attribute>
											</xsl:if>
											<IE:dropDownItem value="0" style="display:none">No</IE:dropDownItem>
											<IE:dropDownItem value="1" style="display:none">Yes</IE:dropDownItem>
										</IE:APDCustomSelect>
									</TD>
								</TR>
								<TR id="trEstimateTypeCD" style="display:none">
									<TD unselectable="on" nowrap="">
										<strong>Original/Audited:</strong>
									</TD>
									<TD unselectable="on" nowrap="">
										<IE:APDCustomSelect id="txtEstimateTypeCD" name="EstimateTypeCD" width="60" blankFirst="false" canDirty="true" displayCount="8" required="true" onChange="updDuplicateFlag()" CCTabIndex="11">
											<xsl:if test="$EstimateUpd = '1'">
												<xsl:attribute name="value">
													<xsl:value-of select="Estimate/@EstimateTypeCD"/>
												</xsl:attribute>
											</xsl:if>
											<xsl:for-each select="/Root/Reference[@List='EstimateTypeCD']">
												<IE:dropDownItem style="display:none">
													<xsl:attribute name="value">
														<xsl:value-of select="@ReferenceID"/>
													</xsl:attribute>
													<xsl:value-of select="@Name"/>
												</IE:dropDownItem>
											</xsl:for-each>
										</IE:APDCustomSelect>
									</TD>
								</TR>
								<TR id="trAgreedPriceMetCD" style="display:none">
									<TD unselectable="on" nowrap="">
										<strong>Price Agreed:</strong>
									</TD>
									<TD unselectable="on" nowrap="">
										<IE:APDCustomSelect id="txtAgreedPriceMetCD" name="AgreedPriceMetCD" blankFirst="false" canDirty="true" displayCount="8" required="true" CCTabIndex="12" onchange="updateEarlyBill()">
											<xsl:if test="$EstimateUpd = '1'">
												<xsl:attribute name="value">
													<xsl:value-of select="Estimate/@AgreedPriceMetCD"/>
												</xsl:attribute>
											</xsl:if>
											<xsl:for-each select="/Root/Reference[@List='AgreedPriceMetCD']">
												<IE:dropDownItem style="display:none">
													<xsl:attribute name="value">
														<xsl:value-of select="@ReferenceID"/>
													</xsl:attribute>
													<xsl:value-of select="@Name"/>
												</IE:dropDownItem>
											</xsl:for-each>
										</IE:APDCustomSelect>
									</TD>
								</TR>
								<TR id="trApproveDocument" style="">
									<TD unselectable="on" nowrap="" colspan="2">
										<IE:APDCheckBox id="chkApproveDocument" name="chkApproveDocument"  value="0" width="" CCDisabled="true" required="false" CCTabIndex="13" alignment="" canDirty="false" onBeforeChange="" onChange="updateWarranty()" onAfterChange="">
											<xsl:attribute name="caption">
												<xsl:choose>
													<xsl:when test="@ServiceChannelCD = 'RRP'">Mark as Approved </xsl:when>
													<xsl:otherwise>Mark as Initial Bill Estimate</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:attribute name="value">
												<xsl:value-of select="/Root/Estimate/@ApprovedFlag"/>
											</xsl:attribute>

										</IE:APDCheckBox>
									</TD>
								</TR>
								<TR id="trWarranty" style="">
									<TD unselectable="on" nowrap="" colspan="2">
										<IE:APDCheckBox id="chkWarrantyDocument" name="chkWarrantyDocument" caption="Applies to Warranty" value="0" width="" CCDisabled="false" required="false" CCTabIndex="13" alignment="" canDirty="false" onBeforeChange="" onChange="" onAfterChange="">
											<xsl:attribute name="value">
												<xsl:value-of select="/Root/Estimate/@WarrantyFlag"/>
											</xsl:attribute>
											<xsl:attribute name="CCDisabled">
												<xsl:choose>
													<xsl:when test="/Root/@WarrantyExistsFlag = '1'">false</xsl:when>
													<xsl:otherwise>true</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
										</IE:APDCheckBox>
									</TD>
								</TR>
							</TABLE>
						</TD>
						<TD unselectable="on" valign="top" nowrap="">
							<TABLE unselectable="on" id="tblTotals" border="0" cellspacing="0" cellpadding="2" style="display:none;table-layout:fixed;border-collapse:collapse">
								<colgroup>
									<col width="125px"/>
									<col width="95px"/>
									<col id="colOrigData" width="75px" style="background-color:#FFCC99;text-align:right;padding:0px;padding-right:3px;display:none"/>
								</colgroup>
								<TR id="trDataRetrieve" style="display1:none">
									<TD id="tdDataRetrieve" unselectable="on" nowrap="">
										<strong>Retrieve Data:</strong>
									</TD>
									<TD id="tdDataRetrieveSel" unselectable="on" valign="center" nowrap="">
										<TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
											<TR>
												<TD unselectable="on">
													<IE:APDCustomSelect id="txtDataRetrieve" name="DataRetrieve" value="Mitchell" CCDisabled="false" blankFirst="true" canDirty="true" displayCount="8" required="true" CCTabIndex="13">
														<IE:dropDownItem value="Mitchell" style="display:none">Mitchell</IE:dropDownItem>
														<IE:dropDownItem value="Pathways" style="display:none">Pathways</IE:dropDownItem>
													</IE:APDCustomSelect>
												</TD>
												<TD>
													<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
												</TD>
												<TD>
													<img src="/images/next_button.gif" onClick="submitRetrieveData()" width="19" height="19" border="0" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Click to Retrieve data"/>
												</TD>
											</TR>
										</TABLE>
									</TD>
									<TD style="background-color:#CC6600">
										<div id="divOrigCaption" style="font-weight:bold;color:#FFFFFF;text-align:center"/>
									</TD>
								</TR>
								<TR>
									<TD unselectable="on" nowrap="">
										<strong>Repair Total:</strong>
									</TD>
									<TD unselectable="on" nowrap="">
										<IE:APDInputCurrency id="txtRepairTotalAmt" name="RepairTotalAmt" neg="false" precision="9" scale="2" required="true" canDirty="true" onChange="updTotals()" CCTabIndex="14">
											<xsl:if test="$EstimateUpd = '1'">
												<xsl:attribute name="value">
													<xsl:value-of select="Estimate/@RepairTotalAmt"/>
												</xsl:attribute>
											</xsl:if>
											<xsl:if test="Estimate/@DocumentVANSourceFlag = '1' and Estimate/@FullSummaryExistsFlag = '1'">
												<xsl:attribute name="CCDisabled">true</xsl:attribute>
											</xsl:if>
										</IE:APDInputCurrency>
									</TD>
									<TD>
										<div id="divOrigRepairTotal"/>
									</TD>
								</TR>
								<TR>
									<TD unselectable="on" nowrap="">
										<strong>Betterment:</strong>
									</TD>
									<TD unselectable="on" nowrap="" style="overflow-x: hidden; overflow-y: hidden;">
										<IE:APDInputCurrency id="txtBettermentAmt" name="BettermentAmt" neg="true" precision="8" scale="2" required="true" canDirty="true" onChange="updTotals()" CCTabIndex="15">
											<xsl:attribute name="value">
												<xsl:choose>
													<xsl:when test="Estimate/@BettermentAmt = '' and Estimate/@DocumentVANSourceFlag != '1'">
														<xsl:text disable-output-escaping="yes">0</xsl:text>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="Estimate/@BettermentAmt"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:if test="Estimate/@DocumentVANSourceFlag = '1' and Estimate/@FullSummaryExistsFlag = '1'">
												<xsl:attribute name="CCDisabled">true</xsl:attribute>
											</xsl:if>
										</IE:APDInputCurrency>
									</TD>
									<TD>
										<div id="divOrigBetterment"/>
									</TD>
								</TR>
								<TR>
									<TD unselectable="on" nowrap="">
										<strong>Deductible:</strong>
										<xsl:variable name="ClaimAspectID" select="/Root/@ClaimAspectID"/>
										($<SPAN id="DedAmtOriginal">
											<xsl:value-of select="/Root/Reference[@List='PertainsTo' and @ClaimAspectID=$ClaimAspectID]/@DeductibleAmt"/>
										</SPAN>)
									</TD>
									<TD unselectable="on" nowrap="" style="overflow-x: hidden; overflow-y: hidden;">
										<IE:APDInputCurrency id="txtDeductibleAmt" name="DeductibleAmt" neg="false" precision="9" scale="2" required="true" canDirty="true" onChange="updTotals();txtDeductibleAmt.contentSaved()" CCTabIndex="16">
											<xsl:attribute name="value">
												<xsl:choose>
													<xsl:when test="Estimate/@DocumentVANSourceFlag = '1' or Estimate/@FullSummaryExistsFlag = '1'">
														<xsl:value-of select="Estimate/@DeductibleAmt"/>
													</xsl:when>
													<xsl:when test="Estimate/@FullSummaryExistsFlag = '0'">
														<xsl:choose>
															<xsl:when test="Estimate/@DeductibleAmt != ''">
																<xsl:value-of select="Estimate/@DeductibleAmt"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="/Root/Reference[@List='PertainsTo' and @ClaimAspectID=$ClaimAspectID]/@DeductibleAmt"/>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:when>
													<xsl:otherwise>
														<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:if test="Estimate/@DocumentVANSourceFlag = '1' and Estimate/@FullSummaryExistsFlag = '1'">
												<xsl:attribute name="CCDisabled">true</xsl:attribute>
											</xsl:if>
										</IE:APDInputCurrency>
									</TD>
									<TD>
										<div id="divOrigDeductible"/>
									</TD>
								</TR>
								<TR>
									<TD unselectable="on" nowrap="">
										<strong>Other Adjustments:</strong>
										<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
										<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
									</TD>
									<TD unselectable="on" nowrap="" style="overflow-x: hidden; overflow-y: hidden;">
										<IE:APDInputCurrency id="txtOtherAdjustmentAmt" name="OtherAdjustmentAmt" neg="true" precision="8" scale="2" required="true" canDirty="true" onChange="updTotals()" CCTabIndex="17">
											<xsl:attribute name="value">
												<xsl:choose>
													<xsl:when test="Estimate/@OtherAdjustmentAmt = '' and Estimate/@DocumentVANSourceFlag != '1'">
														<xsl:text disable-output-escaping="yes">0</xsl:text>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="Estimate/@OtherAdjustmentAmt"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:if test="Estimate/@DocumentVANSourceFlag = '1' and Estimate/@FullSummaryExistsFlag = '1'">
												<xsl:attribute name="CCDisabled">true</xsl:attribute>
											</xsl:if>
										</IE:APDInputCurrency>
									</TD>
									<TD>
										<div id="divOrigOthAdj"/>
									</TD>
								</TR>
								<TR id="trTaxTotal" style="visibility1: hidden; display:none">
									<TD unselectable="on" nowrap="">
										<strong>Tax Total:</strong>
									</TD>
									<TD unselectable="on" nowrap="">
										<IE:APDInputCurrency id="txtTaxTotalAmt" name="TaxTotalAmt" precision="9" scale="2" CCDisabled="false" CCTabIndex="18">
											<xsl:if test="$EstimateUpd = '1'">
												<xsl:attribute name="value">
													<xsl:value-of select="Estimate/@TaxTotalAmt"/>
												</xsl:attribute>
											</xsl:if>
										</IE:APDInputCurrency>
									</TD>
									<TD>
										<div id="divOrigTaxTotalAmt"/>
									</TD>
								</TR>
								<TR>
									<TD unselectable="on" nowrap="">
										<strong>Net Total:</strong>
									</TD>
									<TD unselectable="on" nowrap="">
										<IE:APDInputCurrency id="txtNetTotalAmt" name="NetTotalAmt" precision="9" scale="2" CCDisabled="true" CCTabIndex="-1">
											<xsl:if test="$EstimateUpd = '1'">
												<xsl:attribute name="value">
													<xsl:value-of select="Estimate/@NetTotalAmt"/>
												</xsl:attribute>
											</xsl:if>
											<xsl:attribute name="value">
												<xsl:value-of select="Estimate/@NetTotalAmt"/>
											</xsl:attribute>
										</IE:APDInputCurrency>
									</TD>
									<TD>
										<div id="divOrigNetTotalAmt"/>
									</TD>
								</TR>
								<tr id="trSavings" style="visibility: hidden">
									<td>
										<strong>% Savings:</strong>
									</td>
									<td>
										<div id="divSavings" style="text-align:right;padding:0px;padding-right:10px;"/>
									</td>
									<td/>
								</tr>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
				<TABLE>
					<colgroup>
						<col width="150px"/>
						<col width="300px"/>
					</colgroup>
					<TR style="height:30px">
						<TD unselectable="on" align="center" nowrap="">
							<div id="progressGif" style="display:none;font-weight:bold">
								Uploading... Please wait.
								<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
								<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
								<img src="images/progress.gif" alt="" width="82" height="10" border="0"/>
							</div>
						</TD>
						<TD unselectable="on" nowrap="">
							<TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;padding:0px;margin:0px">
								<colgroup>
									<col width="125"/>
									<col width="45"/>
									<col width="45"/>
									<col width="130">
										<xsl:if test="/Root/@DocumentID = 0">
											<xsl:attribute name="style">display:none</xsl:attribute>
										</xsl:if>
									</col>
								</colgroup>
								<TR>
									<TD unselectable="on" nowrap="">
										<xsl:choose>
											<xsl:when test="/Root/@DocumentID != 0 and Estimate/@EstimateLockedFlag = '1'">
												<IE:APDButton id="btnLockEstimate" name="btnLockEstimate" CCDisabled="false" CCTabIndex="18" width="125" onButtonClick="lockUnlockEstimate()">
													<xsl:attribute name="value">
														<xsl:choose>
															<xsl:when test="$EstimateUpd = '1' and Estimate/@EstimateLockedFlag = '1'">Unlock </xsl:when>
															<xsl:otherwise>Lock </xsl:otherwise>
														</xsl:choose>
														<xsl:value-of select="/Root/Reference[@List='DocumentType' and @ReferenceID=/Root/Estimate/@DocumentTypeID]/@Name"/>
													</xsl:attribute>
												</IE:APDButton>
											</xsl:when>
											<xsl:otherwise>
												<!--
												 <div id="divLockEstimate" style="display:none">
													<IE:APDCheckBox id="chkLockEstimate" name="chkLockEstimate" caption="Lock Estimate" value="0" CCDisabled="false" required="false" CCTabIndex="18" canDirty="false"/>
												 </div>
												 -->
											</xsl:otherwise>
										</xsl:choose>
									</TD>
									<TD unselectable="on" nowrap="">
										<IE:APDButton id="btnClose" name="btnClose" value="Close" CCDisabled="false" width="42" onButtonClick="closeMe()" title="Close" CCTabIndex="26"/>
									</TD>
									<TD unselectable="on" nowrap="">
										<IE:APDButton id="btnSave" name="btnSave" value="Save" CCDisabled="false" width="40" onButtonClick="verifySubmit();" title="Save" CCTabIndex="25"/>
									</TD>
									<TD unselectable="on" nowrap="">
										<xsl:choose>
											<xsl:when test="Estimate/@ActiveAssignment = '1' and Estimate/@ProgramShopAssignment = '1' and Estimate/@ReinspectionCompleted = '0'">
												<IE:APDButton id="btnReqReinspection" name="btnReqReinspection" width="120" onButtonClick="requestReinspection();" CCTabIndex="18" CCDisabled="false">
													<xsl:attribute name="value">
														<xsl:choose>
															<xsl:when test="Estimate/@ReinspectionRequestFlag='1'">Cancel Reinspection</xsl:when>
															<xsl:otherwise>Req. Reinspection</xsl:otherwise>
														</xsl:choose>
													</xsl:attribute>
												</IE:APDButton>
											</xsl:when>
											<xsl:when test="Estimate/@ReinspectionCompleted = '1'">
												<div style="font-weight:bold;color:#0000FF" onmouseenter="showToolTip(this)" onmouseleave="hideToolTip(this)">Reinspection Completed.</div>
											</xsl:when>
										</xsl:choose>
									</TD>
									<TD>
										<xsl:choose>
											<xsl:when test="$FileUploadFlag != '1' or @ServiceChannelCD = 'PS' or  @ServiceChannelCD = 'RRP'">
												<IE:APDButton id="btn_AuditEstValues" name="btn_AuditEstValues" value="Audit Estimate Values" onButtonClick="showAuditEstimateValues()" width="160" CCDisabled="false" CCTabIndex="19"/>
											</xsl:when>
										</xsl:choose>
									</TD>
									<TD>
										<div id="lblSaved" name="lblSaved" style="position:absolute;top:212px;left:670px;color:blue;white-space: nowrap;">
											(Audit Estimate Values Saved)
										</div>
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<form name="frmRetrieveData" id="frmRetrieveData" method="POST" action="RetrieveEstData.asp" target="ifrmRetrieveData">
						<input type="hidden" name="LynxID" id="txtLynxID">
							<xsl:attribute name="value">
								<xsl:value-of select="$LynxID"/>
							</xsl:attribute>
						</input>
						<input type="hidden" name="EstimatingPackage" id="EstimatingPackage"/>
						<input type="hidden" name="SupplementNo" id="SupplementNo"/>
					</form>
					<form name="frmReIRequest" id="frmReIRequest" method="POST" action="NotifyReinspection.asp" target="ifrmRetrieveData">
						<input type="hidden" name="DocumentID" id="DocumentID"/>
						<input type="hidden" name="Comments" id="Comments"/>
						<input type="hidden" name="UserID" id="UserID"/>
						<input type="hidden" name="fn" id="fn"/>
					</form>
				</TABLE>
				<xsl:if test="$DisplayPricingFlag != 0">
					<div style="position:absolute; left:600; top:19;">
						<xsl:choose>
							<xsl:when test="/Root/@PricingAvailable='0'">There is no pricing data available for the shop to which this vehicle is assigned.</xsl:when>
							<xsl:otherwise>
								<table>
									<tr>
										<th colspan="2">Shop Pricing as of Assignment</th>
									</tr>
									<xsl:for-each select="/Root/ShopPricing">
										<tr>
											<td>
												<b>Sheet Metal:</b>
											</td>
											<td>
												<IE:APDInputCurrency id="txtSheetMetal" name="SheetMetal" neg="false" precision="9" scale="2" canDirty="false" CCDisabled="true">
													<xsl:attribute name="value">
														<xsl:value-of select="@SheetMetal"/>
													</xsl:attribute>
												</IE:APDInputCurrency>
											</td>
										</tr>
										<tr>
											<td>
												<b>Refinishing:</b>
											</td>
											<td>
												<IE:APDInputCurrency id="txtRefinishing" name="Refinishing" neg="false" precision="9" scale="2" canDirty="false" CCDisabled="true">
													<xsl:attribute name="value">
														<xsl:value-of select="@Refinishing"/>
													</xsl:attribute>
												</IE:APDInputCurrency>
											</td>
										</tr>
										<tr>
											<td>
												<b>Unibody/Frame:</b>
											</td>
											<td>
												<IE:APDInputCurrency id="txtUnibodyFrame" name="UnibodyFrame" neg="false" precision="9" scale="2" canDirty="false" CCDisabled="true">
													<xsl:attribute name="value">
														<xsl:value-of select="@UnibodyFrame"/>
													</xsl:attribute>
												</IE:APDInputCurrency>
											</td>
										</tr>
										<tr>
											<td>
												<b>Mechanical:</b>
											</td>
											<td>
												<IE:APDInputCurrency id="txtMechanical" name="Mechanical" neg="false" precision="9" scale="2" canDirty="false" CCDisabled="true">
													<xsl:attribute name="value">
														<xsl:value-of select="@Mechanical"/>
													</xsl:attribute>
												</IE:APDInputCurrency>
											</td>
										</tr>
									</xsl:for-each>
									<tr>
										<td>
											<a href="#" onClick="showPricing()">view all</a>
										</td>
									</tr>
								</table>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</xsl:if>
				<xsl:if test="$EstimateUpd = '1'">
					<IFRAME id="ifrmViewEstimate" name="ifrmViewEstimate" frameborder="0" style="width:854px; height:376px; border:0px" tabindex="-1">
						<!-- <xsl:attribute name="src">EstimateDocView.asp?docPath=<xsl:value-of select='$ImageLocation'/></xsl:attribute> -->
					</IFRAME>
				</xsl:if>
				<IFRAME id="ifrmDocumentEstList" name="ifrmDocumentEstList" frameborder="0" style="display:none; border:0px;" tabindex="-1">
					<!-- <xsl:attribute name="src">
      DocumentEstList.asp?LynxID=<xsl:value-of select="$LynxID"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>InsuranceCompanyID=<xsl:value-of select="$InsuranceCompanyID"/>
      <xsl:if test="$EstimateUpd = '1'">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>ClaimAspectID=<xsl:value-of select="$ClaimAspectID"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>EstimateUpd=1
      </xsl:if>
    </xsl:attribute> -->
				</IFRAME>
				<IFRAME id="ifrmRetrieveData" name="ifrmRetrieveData" src="blank.asp" frameborder="0" style="display:none; border:0px;" tabindex="-1"/>
				<xml id="xmlReference">
					<Root>
						<xsl:copy-of select="/Root/Reference[@List='PertainsTo']"/>
						<xsl:copy-of select="/Root/Reference[@List='ServiceChannel']"/>
					</Root>
				</xml>
				<xml id="xmlPertainsToEstimates">
				</xml>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
