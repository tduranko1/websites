<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:ms="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="VehicleInvolved">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="InvolvedCRUD" select="Involved"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="WindowID"/>
<xsl:param name="UserId"/>
<xsl:param name="LynxId"/>
<xsl:param name="VehicleNumber"/>
<xsl:param name="pageReadOnly"/>

<xsl:template match="/Root">

<xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>
<!-- <xsl:variable name="pageReadOnly" select="string(boolean($ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'))"/> -->

<xsl:variable name="InvolvedID" select="/Root/@InvolvedID" />
<xsl:variable name="InvolvedLastUpdatedDate" select="/Root/Involved/@SysLastUpdatedDate"/>
<xsl:value-of select="js:SetCRUD( string($InvolvedCRUD) )"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},10,uspVehicleInvolvedGetDetailXML,VehicleInvolved.xsl,711,145   -->

<HEAD>
<TITLE>Vehicle Involved</TITLE>

<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/CoolSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>

<STYLE type="text/css">
    A {color:navy; text-decoration:none;}
    A:Hover {color:navy; text-decoration:underline;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,33);
		  event.returnValue=false;
		  };	
</script>

<SCRIPT language="JavaScript">

  var gsCRUD = '<xsl:value-of select="$InvolvedCRUD"/>';
  var gsLynxID = '<xsl:value-of select="$LynxId"/>';
  var gsUserID = '<xsl:value-of select="$UserId"/>';
  var gsVehNum = '<xsl:value-of select="$VehicleNumber"/>';
  var gsInvolvedID = '<xsl:value-of select="$InvolvedID"/>';
  var gsInvolvedLastUpdatedDate = "<xsl:value-of select="$InvolvedLastUpdatedDate"/>";
  var gsInvolvedTypesCount = '<xsl:value-of select="count(/Root/Reference[@List='InvolvedType'])"/>';
  var gsInsured = '<xsl:value-of select="Involved/InvolvedType[@InvolvedTypeName='Insured']/@InvolvedTypeID"/>';
  var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";
  var bPageReadonly = <xsl:value-of select="$pageReadOnly"/>;
  var posInsured = null;
  var posClaimant = null;


<![CDATA[

  messages= new Array()
  // Write your descriptions in here.
  messages[0]="Double Click to Expand"
  messages[1]="Double Click to Close"

  var inADS_Pressed = false;    // re-entry flagging for buttons pressed
  var objDivWithButtons = null;
  var gbDirtyFlag = false;

  // ADS Button pressed
  function ADS_Pressed(sAction, sName)
  {
	  if ((ValidatePhones() == false) || (checkDOB() == false)) return new Array("", "0");
    if (sAction == "Update" && !gbDirtyFlag) return new Array("", "0"); //nothing changed.
    if (inADS_Pressed == true) return new Array("", "0");

    try {
      var gsClaimAspectID = parent.gsClaimAspectID;
    } catch (e) {
      ClientError("Invalid ClaimAspectID found in claim Vehicle.")
    }

    GetCBvalues('InvolvedType');
    if ((txtInvolvedTypeList.value == "") && (sAction != "Delete"))
    {
      var e = "Please specify an involved type before saving.";
      var retArray = [e, 0];
      return retArray;
    }
    else
    {
			if (sAction == "Delete" && gsInsured == "3")
			{
				var e = "Can not delete the Insured Involved!";
  	    var retArray = [e, 0];
    	  return retArray;
			}

      inADS_Pressed = true;
      objDivWithButtons = window.parent.divWithButtons;
      disableADS(objDivWithButtons, true);

      var sRequest = GetRequestString(sName);

      sRequest += "LynxID=" + gsLynxID + "&";
      sRequest += "UserID=" + gsUserID + "&";
      sRequest += "VehicleNumber=" + gsVehNum + "&";
      sRequest += "ClaimAspectID=" + gsClaimAspectID + "&";

      var oDiv = document.getElementById(sName);
      //alert(oDiv.name);
      //alert(sAction);

      var retArray = DoCrudAction(oDiv, sAction, sRequest);

      //alert("retArray[0] = " + retArray[0]);
      //alert("retArray[1] = " + retArray[1]);

      if (sAction != "Delete" && retArray[1] == "1")
      {
        //update the last Updated date...
        if (retArray[0] != "")
        {
          var objXML = new ActiveXObject("Microsoft.XMLDOM");
          objXML.loadXML(retArray[0]);

          var contextDate = objXML.documentElement.selectSingleNode("/Root/NewVehicleInvolved/@SysLastUpdatedDate")
          oDiv.LastUpdatedDate = contextDate.nodeValue;
          gsInvolvedLastUpdatedDate = oDiv.LastUpdatedDate;
          var InvolvedID = objXML.documentElement.selectSingleNode("/Root/NewVehicleInvolved/@InvolvedID")
          oDiv.ContextID = InvolvedID.nodeValue;
          gsInvolvedID = oDiv.ContextID;

          try {
            //if "Owner was part of the Involved Type list then update the current tab.
            if (txtInvolvedTypeList.value.indexOf("4") != -1)
                top.document.frames["oIframe"].updateTabContent(txtInvolvedFirstName.value, txtInvolvedLastName.value, txtInvolvedBusiness.value);
            else
                top.document.frames["oIframe"].updateTabContent("", "", "");
          }
          catch(e) {}

          objXML = null;
        }
        else
          ClientError("No Update information returned!");
      }

      inADS_Pressed = false;
      disableADS(objDivWithButtons, false);
      objDivWithButtons = null;

      return retArray;
    }
  }

  // Page Initialize
  function PageInit()
  {
    try
    {
      aspObject = RSGetASPObject("rs/RSADSAction.asp");

      document.parentWindow.frameElement.style.visibility='inherit';

      ShowHideInjuryFlds(InjuredCD);

      parent.document.all.imgNADA.style.display = 'inline';
      parent.document.all.imgNADA.style.cursor = 'hand';

      var chkClaimant = chkInsured = null;
      chkClaimant = document.all["InvolvedType" + posClaimant + "Div"];
      chkInsured = document.all["InvolvedType" + posInsured + "Div"];

      if (parent.gsParty == "1st Party") {
        if (chkClaimant)
          chkClaimant.parentElement.parentElement.style.display = "none";

        if (chkInsured && parseInt(top.giInvInsuredCount, 10) > 0)
          chkInsured.parentElement.parentElement.style.display = "none";
      } else if (parent.gsParty == "3rd Party") {
        if (chkInsured)
          chkInsured.parentElement.parentElement.style.display = "none";

        //alert(top.gaVehClaimantCount[parent.gsVehNum]);
        if ((typeof(top.gaVehClaimantCount) == "object") && (top.gaVehClaimantCount.length > parent.gsVehNum) && (gsInvolvedID == "0")){
          if (chkClaimant && parseInt(top.gaVehClaimantCount[parent.gsVehNum], 10) > 0) {
            chkClaimant.parentElement.parentElement.style.display = "none";
          }
        }

        if (chkInsured)
          chkInsured.parentElement.parentElement.style.display = "none";

        if ((typeof(top.gaVehClaimantCount) == "object") && (top.gaVehClaimantCount.length > parent.gsVehNum) && (gsInvolvedID == "0")){
          if (chkClaimant && parseInt(top.gaVehClaimantCount[parent.gsVehNum], 10) > 0)
            chkClaimant.parentElement.parentElement.style.display = "none";
        }

        if (gsInvolvedID != "0"){
          var objClaimantInp = document.all["InvolvedType" + posClaimant];
          //cannot uncheck the claimant.
          if(objClaimantInp && chkClaimant && objClaimantInp.value == "1") {
            var obj = chkClaimant.firstChild;
            obj.readOnly = "1";
            obj.style.cursor = "";
            obj.onclick = obj.onmouseover = obj.onmouseout = "";
            obj = obj.firstChild;
            obj.src = "/images/cbcheckro.png";
            obj.onclick = obj.onmouseover = obj.onmouseout = "";

          }
        }

        //need to remove the above section and uncomment the below. This allows changing the claimant. We will do this later
        // when we have some mechanism to check a claimant will exist for a 3rd party vehicle.
        /*if (gsInvolvedID != "0" && parseInt(top.gaVehClaimantCount[parent.gsVehNum], 10) > 0){
          var objClaimantInp = document.all["InvolvedType" + posClaimant];
          if(objClaimantInp && chkClaimant) {
            if (objClaimantInp.value == "0"){
              var obj = chkClaimant.firstChild;
              obj.readOnly = "1";
              obj.style.cursor = "";
              obj.onclick = obj.onmouseover = obj.onmouseout = "";
              obj = obj.firstChild;
              obj.src = "/images/cbreadonly.png";
              obj.onclick = obj.onmouseover = obj.onmouseout = "";
            }
          }
        }*/
      } else if (parent.gsParty == "Not an Exposure") {
        if (chkInsured)
          chkInsured.parentElement.parentElement.style.display = "none";
        if (chkClaimant)
          chkClaimant.parentElement.parentElement.style.display = "none";
      }

      if (txtInvolvedDOB.value != '//')
        CalculateAge(txtInvolvedDOB, txtInvolvedAge);
      else
        txtInvolvedDOB.value = '';

      txtInvolvedDOB.futureDate = false;

    }catch(e) {}
    onpasteAttachEvents();
  }

  // show the contact window
  function ShowContactLayer(sLayer, sVis, sRoId, sTxtId)
  {
    if (gsCRUD.indexOf("U") == -1 || gsCRUD.indexOf("R") == -1) return;

    var objRet = null;
    var objArgument = new Object;
    switch (sLayer) {
        case "ThirdPartyInfo":
            objArgument.thirdParty = txtThirdPartyInsName.value;
            objArgument.PolicyNumber = txtThirdPartyInsPolicyNum.value;
            objArgument.PhoneAC = txtThirdPartyInsPhoneAC.value;
            objArgument.PhoneEX = txtThirdPartyInsPhoneEX.value;
            objArgument.PhoneUN = txtThirdPartyInsPhoneUN.value;
            objArgument.PhoneXT = txtThirdPartyInsPhoneXT.value;
            objArgument.ClaimNumber = txtThirdPartyInsClaimNum.value;
            objArgument.readOnly = bPageReadonly;

            objRet = window.showModalDialog("popThirdParty.htm", objArgument, "dialogHeight:155px;dialogWidth:560px;center:1;help:0;resizable:0;scroll:0;status:0")
            if (objRet && bPageReadonly == false){
                txtThirdPartyInsName.value = objRet.thirdParty;
                txtThirdPartyInsPolicyNum.value = objRet.PolicyNumber;
                txtThirdPartyInsPhoneAC.value = objRet.PhoneAC;
                txtThirdPartyInsPhoneEX.value = objRet.PhoneEX;
                txtThirdPartyInsPhoneUN.value = objRet.PhoneUN;
                txtThirdPartyInsPhoneXT.value = objRet.PhoneXT;
                txtThirdPartyInsClaimNum.value = objRet.ClaimNumber;
                document.all[sRoId].value = document.all[sTxtId].value;
                setControlDirty(document.getElementById(sRoId));
            }
            break;
        case "DocHospitalInfo":
            objArgument.BusinessName = txtDoctorBusinessName.value;
            objArgument.Address1 = txtDoctorAddress1.value;
            objArgument.Address2 = txtDoctorAddress2.value;
            objArgument.City = txtDoctorCity.value;
            objArgument.State = txtDoctorState.value;
            objArgument.Zip = txtDoctorZip.value;
            objArgument.PhoneAC = txtDoctorDayPhoneAC.value;
            objArgument.PhoneEX = txtDoctorDayPhoneEX.value;
            objArgument.PhoneUN = txtDoctorDayPhoneUN.value;
            objArgument.PhoneXT = txtDoctorDayPhoneXT.value;
            objArgument.readOnly = bPageReadonly;

            objRet = window.showModalDialog("popDocHospital.htm", objArgument, "dialogHeight:210px;dialogWidth:660px;center:1;help:0;resizable:0;scroll:0;status:0")
            if (objRet && bPageReadonly == false){
                txtDoctorBusinessName.value = objRet.BusinessName;
                txtDoctorAddress1.value = objRet.Address1;
                txtDoctorAddress2.value = objRet.Address2;
                txtDoctorCity.value = objRet.City;
                txtDoctorState.value = objRet.State;
                txtDoctorZip.value = objRet.Zip;
                txtDoctorDayPhoneAC.value = objRet.PhoneAC;
                txtDoctorDayPhoneEX.value = objRet.PhoneEX;
                txtDoctorDayPhoneUN.value = objRet.PhoneUN;
                txtDoctorDayPhoneXT.value = objRet.PhoneXT;

                document.all[sRoId].value = document.all[sTxtId].value;
                setControlDirty(document.getElementById(sRoId));
            }
            break;
        case "EmployerInfo":
            objArgument.BusinessName = txtEmployerBusinessName.value;
            objArgument.Address1 = txtEmployerAddress1.value;
            objArgument.Address2 = txtEmployerAddress2.value;
            objArgument.City = txtEmployerCity.value;
            objArgument.State = txtEmployerState.value;
            objArgument.Zip = txtEmployerZip.value;
            objArgument.PhoneAC = txtEmployerDayPhoneAC.value;
            objArgument.PhoneEX = txtEmployerDayPhoneEX.value;
            objArgument.PhoneUN = txtEmployerDayPhoneUN.value;
            objArgument.PhoneXT = txtEmployerDayPhoneXT.value;
            objArgument.readOnly = bPageReadonly;

            objRet = window.showModalDialog("popEmployerInfo.htm", objArgument, "dialogHeight:210px;dialogWidth:660px;center:1;help:0;resizable:0;scroll:0;status:0")
            if (objRet && bPageReadonly == false){
                txtEmployerBusinessName.value = objRet.BusinessName;
                txtEmployerAddress1.value = objRet.Address1;
                txtEmployerAddress2.value = objRet.Address2;
                txtEmployerCity.value = objRet.City;
                txtEmployerState.value = objRet.State;
                txtEmployerZip.value = objRet.Zip;
                txtEmployerDayPhoneAC.value = objRet.PhoneAC;
                txtEmployerDayPhoneEX.value = objRet.PhoneEX;
                txtEmployerDayPhoneUN.value = objRet.PhoneUN;
                txtEmployerDayPhoneXT.value = objRet.PhoneXT;

                document.all[sRoId].value = document.all[sTxtId].value;
                setControlDirty(document.getElementById(sRoId));
            }
            break;
        case "AttorneyInfo":
            objArgument.BusinessName = txtAttorneyBusinessName.value;
            objArgument.Address1 = txtAttorneyAddress1.value;
            objArgument.Address2 = txtAttorneyAddress2.value;
            objArgument.City = txtAttorneyCity.value;
            objArgument.State = txtAttorneyState.value;
            objArgument.Zip = txtAttorneyZip.value;
            objArgument.PhoneAC = txtAttorneyDayPhoneAC.value;
            objArgument.PhoneEX = txtAttorneyDayPhoneEX.value;
            objArgument.PhoneUN = txtAttorneyDayPhoneUN.value;
            objArgument.PhoneXT = txtAttorneyDayPhoneXT.value;
            objArgument.busiSelect = selAttorneyBusinessTypeCD.outerHTML;
            objArgument.readOnly = bPageReadonly;

            objRet = window.showModalDialog("popAttorneyInfo.htm", objArgument, "dialogHeight:210px;dialogWidth:700px;center:1;help:0;resizable:0;scroll:0;status:0")
            if (objRet && bPageReadonly == false){
                txtAttorneyBusinessName.value = objRet.BusinessName;
                txtAttorneyAddress1.value = objRet.Address1;
                txtAttorneyAddress2.value = objRet.Address2;
                txtAttorneyCity.value = objRet.City;
                txtAttorneyState.value = objRet.State;
                txtAttorneyZip.value = objRet.Zip;
                txtAttorneyDayPhoneAC.value = objRet.PhoneAC;
                txtAttorneyDayPhoneEX.value = objRet.PhoneEX;
                txtAttorneyDayPhoneUN.value = objRet.PhoneUN;
                txtAttorneyDayPhoneXT.value = objRet.PhoneXT;
                if (objRet.BusinessCode > 0)
                    selAttorneyBusinessTypeCD.optionselect(null, objRet.BusinessCode);

                document.all[sRoId].value = document.all[sTxtId].value;
                setControlDirty(document.getElementById(sRoId));
            }
            break;
    }
  }

  // builds the multi-checkboxes list for Involved Type
  function GetCBvalues(tag)
  {
    var sName = tag;
    var sId = "";
    var node = "";
     var otb = document.getElementById("tblInvolvedTypes");

    for (var i = 1; i <= gsInvolvedTypesCount; i++)
    {
      node = i;
      sInputName = sName + node;
			var objInput = document.getElementById(sInputName);
			if (objInput)
			{
	      getValues = objInput.value;

	      if (getValues == '1')
	      {
	        if (sId == "") sId += otb.rows[i-1].cells[1].innerText;
	        else sId += "," + otb.rows[i-1].cells[1].innerText;
	      }
			}
    }

    txtInvolvedTypeList.value = sId;
  }

  function ValidatePhones() {
      if (validatePhoneSections(txtInvolvedDayPhoneAC, txtInvolvedDayPhoneEX, txtInvolvedDayPhoneNM) == false) return false;
      if (validatePhoneSections(txtInvolvedNightPhoneAC, txtInvolvedNightPhoneEX, txtInvolvedNightPhoneNM) == false) return false;
      if (validatePhoneSections(txtInvolvedAltPhoneAC, txtInvolvedAltPhoneEX, txtInvolvedAltPhoneNM) == false) return false;
      return true;
  }

  function ShowHideInjuryFlds()
  {
    if (InjuredCD.value == 'Y')
    {
      obj = document.getElementById("InjuryQuestFieldDiv");
      obj.style.visibility = "visible";
      obj.style.display = "inline";
      obj = document.getElementById("InjuryDescFieldDiv");
      obj.style.visibility = "visible";
      obj.style.display = "inline";
    }
    else
    {
      obj = document.getElementById("InjuryQuestFieldDiv");
      obj.style.visibility = "hidden";
      obj.style.display = "none";
      obj = document.getElementById("InjuryDescFieldDiv");
      obj.style.visibility = "hidden";
      obj.style.display = "none";
    }
  }

  function checkDOB(){
    if (txtInvolvedDOB.value.length > 0) {
      var dt = new Date(txtInvolvedDOB.value);
      var dtNow = new Date();
      if (dt > dtNow) {
        ClientWarning("Date of Birth cannot be in the future. Please try again");
        txtInvolvedDOB.select();
        txtInvolvedDOB.focus();
        return false;
      }
      else
        return true;
    }
  }

  if (document.attachEvent)
    document.attachEvent("onclick", top.hideAllMenuScriptlets);
  else
    document.onclick = top.hideAllMenuScriptlets;

]]>
</SCRIPT>

</HEAD>
<BODY unselectable="on" class="bodyAPDSubSub" onLoad="initSelectBoxes(); InitFields(); PageInit();" tabindex="-1">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
<!-- <xsl:if test="$ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'"> -->
<xsl:if test="$pageReadOnly = 'true'">
  <xsl:value-of select="js:SetCRUD( '_R__' )"/>
  <xsl:value-of select="js:setPageDisabled()"/>
  <!-- <xsl:value-of select="js:SetCRUD( '_R_' )"/> -->
</xsl:if>

<DIV id="content22" name="VehicleInvolved" Update="uspVehicleInvolvedUpdDetail" Insert="uspVehicleInvolvedInsDetail" Delete="uspVehicleInvolvedDel" style="visibility:inherit">
  <xsl:attribute name="CRUD">
    <xsl:choose>
      <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
      <xsl:otherwise><xsl:value-of select="$InvolvedCRUD"/></xsl:otherwise>
    </xsl:choose>
  </xsl:attribute>
  <xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Involved/@SysLastUpdatedDate"/></xsl:attribute>
  <xsl:attribute name="ContextID"><xsl:value-of select="@InvolvedID"/></xsl:attribute>
  <xsl:attribute name="ContextName">Involved</xsl:attribute>

  <!-- Start Involved Info -->
  <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
    <TR>
      <TD unselectable="on">Name:</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedTitle',3,'NameTitle',Involved,'','',1)"/>
      </TD>
      <TD unselectable="on" colspan="2"><xsl:attribute name="nowrap"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedFirstName',10,'NameFirst',Involved,'','',2)"/>
        <img unselectable="on" src="/images/spacer.gif" alt="" width="2" height="1" border="0"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedLastName',14,'NameLast',Involved,'','',3)"/>
      </TD>
      <TD unselectable="on">
        Gender:
      </TD>
      <TD unselectable="on">
        <xsl:variable name="rtGCD" select="Involved/@GenderCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('GenderCD',2,'onSelectChange',string($rtGCD),1,2,/Root/Reference[@List='GenderCD'],'Name','ReferenceID','','','','',17,1)"/>
      </TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        Loc. in Veh.:
      </TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:variable name="plRtype" select="Involved/@PersonLocationID"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('PersonLocationID',2,'onSelectChange',number($plRtype),1,6,/Root/Reference[@List='PersonLocation'],'Name','ReferenceID',120,'','','',34)"/>
      </TD>
    </TR>
    <TR>
      <TD unselectable="on">Business:</TD>
      <TD unselectable="on" colspan="3">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedBusiness',35,'BusinessName',Involved,'','',4)"/>
      </TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        SSN/EIN:
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedDriverSSN',20,'FedTaxId',Involved,'','',17)"/>
      </TD>
      <TD unselectable="on">Seatbelt:</TD>
      <TD unselectable="on">
        <xsl:variable name="rtSB" select="Involved/@SeatBeltCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('SeatBeltCD',2,'onSelectChange',string($rtSB),1,4,/Root/Reference[@List='SeatBeltCD'],'Name','ReferenceID','','','','',35,1)"/>
      </TD>
    </TR>
    <TR>
      <TD unselectable="on" colspan="2"><xsl:attribute name="nowrap"/>
        Business Type:
      </TD>
      <TD unselectable="on" colspan="2"><xsl:attribute name="nowrap"/>
        <xsl:variable name="rtBT" select="Involved/@BusinessTypeCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('BusinessTypeCD',2,'onSelectChange',string($rtBT),1,2,/Root/Reference[@List='BusinessType'],'Name','ReferenceID','','','','',5,1)"/>
      </TD>
      <TD unselectable="on">DOB:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedDOB',12,'BirthDate',Involved,'onBlur=CalculateAge(this,txtInvolvedAge)',2,18)"/>
      </TD>
      <TD unselectable="on">Injured?</TD>
      <TD unselectable="on">
        <xsl:variable name="rtICD" select="Involved/@InjuredCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('InjuredCD',2,'onInjC',string($rtICD),1,3,/Root/Reference[@List='InjuredCD'],'Name','ReferenceID','','','','',36,1)"/>
      </TD>
    </TR>
    <TR>
      <TD unselectable="on">Address:</TD>
      <TD unselectable="on" colspan="2">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedAddress1',21,'Address1',Involved,'','',7)"/>
      </TD>
      <TD unselectable="on" align="center" class="autoFlowSpanTitle"><xsl:attribute name="nowrap"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Involved Type<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on">Age:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedAge',2,'Age',Involved,'onkeypress=NumbersOnly(window.event)','',19)"/>
        <!-- <INPUT type="text" name="Age" id="txtInvolvedAge" readonly="" value="" size="2" class="InputReadonlyField" tagindex="19"/> -->
      </TD>
      <TD unselectable="on">Injury:</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <DIV id="InjuryQuestFieldDiv" style="visibility:hidden; display:none;">
          <xsl:variable name="ijRtype" select="Involved/Injury/@InjuryTypeID"/>
          <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('InjuryTypeID',2,'onSelectChange',number($ijRtype),1,2,/Root/Reference[@List='InjuryType'],'Name','ReferenceID',0,'','','',37)"/>
        </DIV>
      </TD>
    </TR>
    <TR>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" colspan="2">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedAddress2',21,'Address2',Involved,'','',8)"/>
      </TD>
      <TD unselectable="on" rowspan="6" valign="top" align="center"><xsl:attribute name="nowrap"/>
        <DIV unselectable="on" id="InvolvedTypeCBs" style="z-index:1; width:84px; height:100px; overflow: auto;">
          <TABLE id='tblInvolvedTypes' unselectable="on" border="0" cellspacing="0" cellpadding="1">
            <xsl:for-each select="Reference[@List='InvolvedType']">
              <TR>
                <TD unselectable="on">
                  <xsl:choose>
                    <xsl:when test="@Name='Insured' and /Root/Involved/InvolvedType/@InvolvedTypeName='Insured'">
                      <xsl:value-of select="@Name"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:if test="boolean(@Name='Insured')= false"><xsl:value-of select="@Name"/></xsl:if>
                    </xsl:otherwise>
                  </xsl:choose>
                </TD>
                <TD unselectable="on" style="display:none"><xsl:value-of select="@ReferenceID"/></TD>
                <TD unselectable="on">
                <xsl:choose>
                  <xsl:when test="boolean(@Name='Insured' and /Root/Involved/InvolvedType/@InvolvedTypeName='Insured')">
                       <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('InvolvedType',boolean(/Root/Involved/InvolvedType[@InvolvedTypeID=current()/@ReferenceID]),'1','0',position(),1,16)"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:if test="boolean(@Name='Insured') = false">
                      <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('InvolvedType',boolean(/Root/Involved/InvolvedType[@InvolvedTypeID=current()/@ReferenceID]),'1','0',position(),'',16)"/>
                    </xsl:if>
                  </xsl:otherwise>
                 </xsl:choose>
                </TD>
              </TR>
              <xsl:if test="@Name='Insured'">
                <script language="javascript">
                  posInsured = <xsl:value-of select="position()"/>;
                </script>
              </xsl:if>
              <xsl:if test="@Name='Claimant'">
                <script language="javascript">
                  posClaimant = <xsl:value-of select="position()"/>;
                </script>
              </xsl:if>
            </xsl:for-each>
          </TABLE>
          <INPUT type="hidden" id="txtInvolvedTypeList" name="InvolvedTypeList" value="" />
        </DIV>
      </TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        Best Time to Call:
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedBTC',15,'BestContactTime',Involved,'','',20)"/>
      </TD>
      <TD unselectable="on" rowspan="2" valign="top">Injury Desc.:</TD>
      <TD unselectable="on" rowspan="5" valign="top">
        <DIV id="InjuryDescFieldDiv" style="Visibility:hidden; display:none;">
          <xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtInvolvedInjuryDesc',24,6,'InjuryDescription',Involved/Injury,'','',38)"/>
        </DIV>
      </TD>
    </TR>
    <TR>
      <TD unselectable="on">City:</TD>
      <TD unselectable="on" colspan="2">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedCity',15,'AddressCity',Involved,'','',9)"/>
      </TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        Best Num to Call:
      </TD>
      <TD unselectable="on">
        <xsl:variable name="rtBCP" select="Involved/@BestContactPhoneCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('BestContactPhoneCD',2,'onSelectChange',string($rtBCP),1,2,/Root/Reference[@List='BestContactPhoneCD'],'Name','ReferenceID','','','','',21,1)"/>
      </TD>
    </TR>
    <TR>
      <TD unselectable="on">State:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedState',2,'AddressState',Involved,'',12,10)"/>
      </TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        Zip:
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedZip',6,'AddressZip',Involved,'',10,11)"/>
        <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="InvolvedValidateZip(this,'txtInvolvedState', 'txtInvolvedCity');"></xsl:text>
      </TD>
      <TD unselectable="on">Day:</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedDayPhoneAC',1,'DayAreaCode',Involved,'',10,22)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedDayPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedDayPhoneEX',1,'DayExchangeNumber',Involved,'',10,23)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedDayPhoneNM, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedDayPhoneNM',2,'DayUnitNumber',Involved,'',10,24)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedDayPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
        <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedDayPhoneXT',3,'DayExtensionNumber',Involved,'',10,25)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    </TR>
    <TR>
      <TD unselectable="on" colspan="3"><xsl:attribute name="nowrap"/>
        Driver Lic. #:
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedDL',16,'DriverLicenseNumber',Involved,'','',12)"/>
      </TD>
      <TD unselectable="on">Night:</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedNightPhoneAC',1,'NightAreaCode',Involved,'',10,26)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedNightPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedNightPhoneEX',1,'NightExchangeNumber',Involved,'',10,27)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedNightPhoneNM, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedNightPhoneNM',2,'NightUnitNumber',Involved,'',10,28)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedNightPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
        <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedNightPhoneXT',3,'NightExtensionNumber',Involved,'',10,29)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    </TR>
    <TR>
      <TD unselectable="on" colspan="3"><xsl:attribute name="nowrap"/>
        License State:
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedLicState',2,'DriverLicenseState',Involved,'',12,13)"/>
      </TD>
      <TD unselectable="on">Alt.:</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedAltPhoneAC',1,'AlternateAreaCode',Involved,'',10,30)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedAltPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedAltPhoneEX',1,'AlternateExchangeNumber',Involved,'',10,31)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedAltPhoneNM, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedAltPhoneNM',2,'AlternateUnitNumber',Involved,'',10,32)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedAltPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
        <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedAltPhoneXT',3,'AlternateExtensionNumber',Involved,'',10,33)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    </TR>
    <TR>
      <TD unselectable="on">Violation?</TD>
      <TD unselectable="on" colspan="2"><xsl:attribute name="nowrap"/>
        <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
          <TR>
            <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
            <TD unselectable="on" colspan="2"><xsl:attribute name="nowrap"/>
              <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('ViolationFlag',boolean(Involved[@ViolationFlag='1']),'1','0','','' ,14)"/>
            </TD>
          </TR>
        </TABLE>
      </TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <a href="#" onClick="ShowContactLayer('ThirdPartyInfo', 'visible', 'roThirdPartyInsName', 'txtThirdPartyInsName')" tabindex="38">Third Party Ins</a>:
      </TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('roThirdPartyInsName',24,'ThirdPartyInsuranceName',Involved/ThirdPartyInsurance,'',7)"/>
      </TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <a href="#" onClick="ShowContactLayer('DocHospitalInfo', 'visible', 'roDoctorBusinessName', 'txtDoctorBusinessName')" tabindex="40">Doc/Hospital</a>:
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('roDoctorBusinessName',24,'DoctorBusinessName',Involved/Doctor,'',7)"/>
      </TD>
    </TR>
    <TR>
      <TD unselectable="on" colspan="4"><xsl:attribute name="nowrap"/>
        Violation Desc:
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtViolationDesc',32,'ViolationDescription',Involved,'','',15)"/>
      </TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <a href="#" onClick="ShowContactLayer('EmployerInfo', 'visible', 'roEmployerBusinessName', 'txtEmployerBusinessName')" tabindex="39">Employer</a>:
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('roEmployerBusinessName',24,'EmployerBusinessName',Involved/Employer,'',7)"/>
      </TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <a href="#" onClick="ShowContactLayer('AttorneyInfo', 'visible', 'roAttorneyBusinessName', 'txtAttorneyBusinessName')" tabindex="41">Attorney</a>:
      </TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('roAttorneyBusinessName',24,'AttorneyBusinessName',Involved/Attorney,'',7)"/>
      </TD>
    </TR>
  </TABLE>
  <!-- End Involved Info -->

  <!-- Third Party Insurance Info -->
  <DIV id="ThirdPartyInfo" class="popupWorkarea" style="position:absolute; z-index:60000; top: 20px; left: 20px;">
    <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
      <TR>
        <TD unselectable="on" colspan="4" height="20" valign="top" class="boldtext" bgcolor="#e0e0e0" style="vertical-align:middle; text-align:center;" nowrap="">
          Third Party Insurance Contact Info
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4" >
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          Name:
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtThirdPartyInsName',50,'ThirdPartyInsuranceName',Involved/ThirdPartyInsurance,'','',42)"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          Policy #:
        </TD>
        <TD unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtThirdPartyInsPolicyNum',20,'ThirdPartyInsurancePolicyNumber',Involved/ThirdPartyInsurance,'','',43)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on">Phone:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtThirdPartyInsPhoneAC',1,'ThirdPartyInsuranceAreaCode',Involved/ThirdPartyInsurance,'',10,45)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtThirdPartyInsPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtThirdPartyInsPhoneEX',1,'ThirdPartyInsuranceExchangeNumber',Involved/ThirdPartyInsurance,'',10,46)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtThirdPartyInsPhoneUN, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtThirdPartyInsPhoneUN',2,'ThirdPartyInsuranceUnitNumber',Involved/ThirdPartyInsurance,'',10,47)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtThirdPartyInsPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
          <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtThirdPartyInsPhoneXT',3,'ThirdPartyInsuranceExtensionNumber',Involved/ThirdPartyInsurance,'',10,48)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          Claim #:
        </TD>
        <TD unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtThirdPartyInsClaimNum',30,'ThirdPartyInsuranceClaimNumber',Involved/ThirdPartyInsurance,'','',44)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4" >
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4"  bgcolor="#e0e0e0" nowrap="">
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4" >
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on" align="right"><xsl:attribute name="nowrap"/>
          <INPUT type="button" name="CloseThirdPartyInsInfo" value="Close" onClick="ShowContactLayer('ThirdPartyInfo', 'hidden', 'roThirdPartyInsName', 'txtThirdPartyInsName')" class="formbutton" tabindex="49"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
      </TR>
    </TABLE>
  </DIV>

  <!-- Doctor/Hospital Info -->
  <DIV id="DocHospitalInfo" class="popupWorkarea" style="position:absolute; z-index:60000; top: 20px; left: 20px;">
    <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
      <TR>
        <TD unselectable="on" colspan="4" height="20" valign="top" class="boldtext" bgcolor="#e0e0e0" style="vertical-align:middle; text-align:center;" nowrap="">
         Doctor / Hospital Contact Info
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4" >
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          Name:
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorBusinessName',50,'DoctorBusinessName',Involved/Doctor,'','',50)"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          Address:
        </TD>
        <TD unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorAddress1',50,'DoctorAddress1',Involved/Doctor,'','',51)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on">Phone:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorDayPhoneAC',1,'DoctorDayAreaCode',Involved/Doctor,'',10,56)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtDoctorDayPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorDayPhoneEX',1,'DoctorDayExchangeNumber',Involved/Doctor,'',10,57)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtDoctorDayPhoneUN, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorDayPhoneUN',2,'DoctorDayUnitNumber',Involved/Doctor,'',10,58)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtDoctorDayPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
          <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorDayPhoneXT',3,'DoctorDayExtensionNumber',Involved/Doctor,'',10,59)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
        </TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorAddress2',50,'DoctorAddress2',Involved/Doctor,'','',52)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on">City:</TD>
        <TD unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorCity',30,'DoctorAddressCity',Involved/Doctor,'','',53)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on">State:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorState',2,'DoctorAddressState',Involved/Doctor,'',12,54)"/>
          <xsl:text disable-output-escaping="yes">&gt;&amp;</xsl:text>nbsp;Zip:
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorZip',7,'DoctorAddressZip',Involved/Doctor,'','',55)"/>
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4" >
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4"  bgcolor="#e0e0e0" nowrap="">
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4" >
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on" align="right"><xsl:attribute name="nowrap"/>
          <INPUT type="button" name="CloseDoctorInfo" value="Close" onClick="ShowContactLayer('DocHospitalInfo', 'hidden', 'roDoctorBusinessName', 'txtDoctorBusinessName')" class="formbutton" tabindex="60"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
      </TR>
    </TABLE>
  </DIV>

  <!-- Employer Info -->
  <DIV id="EmployerInfo" class="popupWorkarea" style="position:absolute; z-index:60000; top: 20px; left: 20px;">
    <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
      <TR>
        <TD unselectable="on" colspan="4" height="20" valign="top" class="boldtext" bgcolor="#e0e0e0" style="vertical-align:middle; text-align:center;" nowrap="">
          Employer Contact Info
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4" >
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          Name:
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerBusinessName',50,'EmployerBusinessName',Involved/Employer,'','',61)"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          Address:
        </TD>
        <TD unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerAddress1',50,'EmployerAddress1',Involved/Employer,'','',62)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on">Phone:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerDayPhoneAC',1,'EmployerDayAreaCode',Involved/Employer,'',10,67)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtEmployerDayPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerDayPhoneEX',1,'EmployerDayExchangeNumber',Involved/Employer,'',10,68)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtEmployerDayPhoneUN, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerDayPhoneUN',2,'EmployerDayUnitNumber',Involved/Employer,'',10,69)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtEmployerDayPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
          <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerDayPhoneXT',3,'EmployerDayExtensionNumber',Involved/Employer,'',10,70)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
        </TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerAddress2',50,'EmployerAddress2',Involved/Employer,'','',63)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on">City:</TD>
        <TD unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerCity',30,'EmployerAddressCity',Involved/Employer,'','',64)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on">State:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerState',2,'EmployerAddressState',Involved/Employer,'',12,65)"/>
          <xsl:text disable-output-escaping="yes">&gt;&amp;</xsl:text>nbsp;Zip:
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerZip',7,'EmployerAddressZip',Involved/Employer,'','',66)"/>
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4" >
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4"  bgcolor="#e0e0e0" nowrap="">
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4" >
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on" align="right"><xsl:attribute name="nowrap"/>
          <INPUT type="button" name="CloseEmployerInfo" value="Close" onClick="ShowContactLayer('EmployerInfo', 'hidden', 'roEmployerBusinessName', 'txtEmployerBusinessName')" class="formbutton" tabindex="71"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
      </TR>
    </TABLE>
  </DIV>

  <!-- Attorney Info -->
  <DIV id="AttorneyInfo" class="popupWorkarea" style="position:absolute; z-index:60000; top: 20px; left: 20px;">
    <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
      <TR>
        <TD unselectable="on" colspan="4" height="20" valign="top" class="boldtext" bgcolor="#e0e0e0" style="vertical-align:middle; text-align:center;" nowrap="">
          Attorney Contact Info
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4" >
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          Name:
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyBusinessName',50,'AttorneyBusinessName',Involved/Attorney,'','',72)"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          Address:
        </TD>
        <TD>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyAddress1',50,'AttorneyAddress1',Involved/Attorney,'','',73)"/>
        </TD>
      </TR>
      <TR>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        Bus. Type:
      </TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:variable name="rtABT" select="Involved/Attorney/@AttorneyBusinessTypeCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('AttorneyBusinessTypeCD',2,'onSelectChange',string($rtABT),1,2,/Root/Reference[@List='BusinessType'],'Name','ReferenceID','','','','',78,1)"/>
      </TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyAddress2',50,'AttorneyAddress2',Involved/Attorney,'','',74)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on">Phone:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyDayPhoneAC',1,'AttorneyDayAreaCode',Involved/Attorney,'',10,79)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtAttorneyDayPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyDayPhoneEX',1,'AttorneyDayExchangeNumber',Involved/Attorney,'',10,80)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtAttorneyDayPhoneUN, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyDayPhoneUN',2,'AttorneyDayUnitNumber',Involved/Attorney,'',10,81)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtAttorneyDayPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
          <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyDayPhoneXT',3,'AttorneyDayExtensionNumber',Involved/Attorney,'',10,82)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
        </TD>
        <TD unselectable="on">City:</TD>
        <TD unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyCity',30,'AttorneyAddressCity',Involved/Attorney,'','',75)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on">State:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyState',2,'AttorneyAddressState',Involved/Attorney,'',12,76)"/>
          <xsl:text disable-output-escaping="yes">&gt;&amp;</xsl:text>nbsp;Zip:
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyZip',7,'AttorneyAddressZip',Involved/Attorney,'','',77)"/>
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4" >
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4"  bgcolor="#e0e0e0" nowrap="">
        </TD>
      </TR>
      <TR style="height:4px">
        <TD unselectable="on" colspan="4" height="4" >
        </TD>
      </TR>
      <TR>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on" align="right"><xsl:attribute name="nowrap"/>
          <INPUT type="button" name="CloseAttorneyInfo" value="Close" onClick="ShowContactLayer('AttorneyInfo', 'hidden', 'roAttorneyBusinessName', 'txtAttorneyBusinessName')" class="formbutton" tabindex="83"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
      </TR>
    </TABLE>
  </DIV>

</DIV>

</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>
