<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    id="ClaimSearch">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msxsl-function-library.xsl"/>

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function myNameConcat(firstPart, secondPart, delim){
        if (delim == "") delim = ", ";
        if (firstPart == "" && secondPart == "")
            return "&nbsp;";
        else if (firstPart != "" && secondPart != "")
            return firstPart + delim + secondPart;
        else
            return firstPart + secondPart;
    }
  ]]>
</msxsl:script>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="WindowID"/>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ReOpenCRUD" select="Action:Reopen Exposure"/>

<xsl:template match="/Root">

<HTML>
<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspCRDClosedClaimGetDetailXML,ClosedClaimSearch.xsl,19,3000   -->
<HEAD>

<TITLE></TITLE>
<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="STYLESHEET" href="/css/CoolSelect.css" type="text/css"/>

<!-- for radio buttons -->
<STYLE type="text/css">A {color:black; text-decoration:none;}</STYLE>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/ClaimNavigate.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,7);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">

    var sReOpenCRUD = '<xsl:value-of select="$ReOpenCRUD"/>';
    var sClaimAspectID = "";
<![CDATA[

//update the selected elements from the table row
function GridClick(oRow)
{
  if (!oRow) return;

  //get the table name of the row that was clicked
  var oTable = oRow.parentElement.parentElement;
  //based on the table name update the form elements to the selectd row
  if (oTable.id == "tblSort2")
  {
    txtDateofLossP.value = oRow.cells[4].innerText;
    txtLynxIDP.value = oRow.cells[1].innerText;
    txtPHNameP.value = oRow.cells[3].innerText;
    txtTimeOpenP.value = oRow.cells[10].innerText + " days " + oRow.cells[11].innerText + " hrs. " + oRow.cells[12].innerText + " mins.";
    txtClaimNumberP.value = oRow.cells[6].innerText;
    txtStateP.value = oRow.cells[7].innerText;
    txtInsuranceCoP.value = oRow.cells[5].innerText;
    //txtCoverageP.value = oRow.cells[1].innerText;
    spVehNumP.innerText = oRow.cells[8].innerText;
    spPropNumP.innerText = oRow.cells[9].innerText;
    sClaimAspectID = oRow.getAttribute("ClaimAspectID");

    if (sReOpenCRUD.indexOf("U") != -1)
    {
        btnReOpen.disabled = false;
        btnReOpen.style.cursor = 'hand';
    }
    else
    {
        btnReOpen.disabled = true;
        btnReOpen.style.cursor = 'default';
    }

    var numVeh = parseInt(oRow.cells[8].innerText);
    var oSelect = document.getElementById("selPCVeh");
    oSelect.removeall();

    var oOption = new Object();

    for(var indx=0;indx < numVeh; indx++)
    {
      var str = oRow.cells[17+(indx*4)].innerText;
      var strStatus = oRow.cells[18+(indx*4)].innerText;
      if (str.substr(0,1) == ",") str = str.substr(2);
      if (str.length > 28) str = str.substring(0,28);
      if (strStatus.indexOf("Closed") != -1)
        oOption.text = "<img src='/images/closedExposure.gif' title='Closed Vehicle'/><img src='/images/spacer.gif' style='height:1px;width:5px'/>" + str;
      else
      {
        if (str == "")
          oOption.text = "<img src='/images/spacer.gif' style='height:1px;width:15px'/> Veh " + oRow.cells[16+(indx*4)].innerText;
        else
          oOption.text = "<img src='/images/spacer.gif' style='height:1px;width:15px'/>" + str;
      }
      oOption.value = String(oRow.cells[16+(indx*4)].innerText +"||"+ oRow.cells[19+(indx*4)].innerText);
      oSelect.add(oOption);
    }

    if (oSelect.firstChild.nextSibling && oSelect.firstChild.nextSibling.className=="dropDown")
        oSelect.firstChild.nextSibling.style.height = Math.min(75, (numVeh * 15) + 1);

    if (numVeh > 0)
    {
     	oSelect.remove(0);
      oSelect.options[0].selected = true;
    }

    var numProp = parseInt(oRow.cells[9].innerText);
    oSelect = document.getElementById("selPCProp");
    oSelect.removeall();

    for(var indx=0;indx < numProp; indx++)
    {
      var str = oRow.cells[17+(indx*4)+(numVeh*4)].innerText;
      var strStatus = oRow.cells[18+(indx*4)+(numVeh*4)].innerText;
      if (str.substr(0,1) == ",") str = str.substr(2);
      if (str.length > 28) str = str.substring(0,28);
      if (strStatus.indexOf("Closed") != -1)
        oOption.text = "<img src='/images/closedExposure.gif' title='Closed Property'/><img src='/images/spacer.gif' style='height:1px;width:5px'/>" + str;
      else
      {
        if (str == "")
          oOption.text = "<img src='/images/spacer.gif' style='height:1px;width:15px'/> Prop " + oRow.cells[16+(indx*4)+(numVeh*4)].innerText;
        else
          oOption.text = "<img src='/images/spacer.gif' style='height:1px;width:15px'/>" + str;
      }
      oOption.value=String(oRow.cells[16+(indx*4)+(numVeh*4)].innerText +"||"+ oRow.cells[19+(indx*4)+(numVeh*4)].innerText);
      oSelect.add(oOption);
    }

    if (oSelect.firstChild.nextSibling && oSelect.firstChild.nextSibling.className=="dropDown")
        oSelect.firstChild.nextSibling.style.height = Math.min(75, (numProp * 15) + 1);

    if (numProp > 0)
    {
      oSelect.remove(0);
      oSelect.options[0].selected = true;
      return;
    }
  }

}

function onSelectChange(selobj)
{
  if ((selobj.options[selobj.selectedIndex].innerText == "----") || (selobj.options[selobj.selectedIndex].innerText == "")) return;
  var strNum = selobj.options[selobj.selectedIndex].value;
  var aryNum = strNum.split("||");

	var sURL = "";
	var lynxid = txtLynxIDP.value;

	if (selobj.id == "selPCVeh")
    NavOpenWindow( "vehicle", lynxid, aryNum[1] );
	else
    NavOpenWindow( "property", lynxid, aryNum[1] );
}


function resizeScrollTable(oElement)
{
	var head = oElement.firstChild;
	var headTable = head.firstChild;
	var body = oElement.lastChild;
	var bodyTable = body.firstChild;

	body.style.height = Math.max(0, oElement.clientHeight - head.offsetHeight);

	var scrollBarWidth = body.offsetWidth - body.clientWidth;

	// go through each cell in the head and resize
	var headCells = headTable.rows[0].cells;
	if (bodyTable.rows.length > 0)
	{
		var bodyCells = bodyTable.rows[0].cells;
		var iLength = bodyCells.length;
		for (var i = 0; i < iLength; i++)
		{
			if (bodyCells[i].style.display != "none")
			{
				if (i == 0)
          headCells[i].style.width = bodyCells[i].offsetWidth + 2;
				else if (i == (headCells.length-1))
          headCells[i].style.width = bodyCells[i].offsetWidth + scrollBarWidth;
        else
          headCells[i].style.width = bodyCells[i].offsetWidth;
      }
		}
	}
}

function ReAssignClaim()
{
	var strLynxID = document.getElementById("txtLynxIDP").value;

//  var width="300", height="200";
//  var left = (screen.width/2) - width/2;
//  var top = (screen.height/2) - height/2;
//  var styleStr = 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbar=no,resizable=no,copyhistory=yes,width='+width+',height='+height+',left='+left+',top='+top+',screenX='+left+',screenY='+top;
//  var msgWindow = window.open("/ClaimReAssign.asp?LynxID="+strLynxID,"ClaimReAssign", styleStr);
	var strReturn = ""

	strReturn = window.showModalDialog("/ClaimReAssign.asp?LynxID="+strLynxID,strReturn,"dialogHeight: 200px; dialogWidth: 300px; center: Yes; help: No; resizable: No; status: No; unadorned: Yes");
}

//init the table selections, must be last
function initPage()
{
	var oTbl = document.getElementById("tblSort2");
	GridClick(oTbl.rows[0]);

	resizeScrollTable(document.getElementById("CPScrollTable"));

	top.setSB(100,top.sb);
}

// Function that will prevent page to navigate one level back in history when backspace is pressed in a
// read-only field. This function must be called in body's onkeyDown event.
function docNoBkSpace()  {
	var srcTagName;
	srcTagName = event.srcElement.tagName;
	if ((srcTagName == 'INPUT') || (srcTagName == 'TEXTAREA')) {
		if (event.srcElement.readOnly == true) {
			if (event.keyCode == 8) //backspace
				event.returnValue = false;
			if ((event.altKey) && ((event.keyCode == 37) || (event.keyCode == 39))) //alt+left arrow, alt+ right arrow
				event.returnValue = false;
		}
	}
}

function reOpenClaim(){
    var sDimensions = "dialogHeight:260px; dialogWidth:445px; "
    var sSettings = "resizable:no; status:no; help:no; center:yes;"
    var strReturn = window.showModalDialog("ClaimExposure.asp?LYNXID=" + txtLynxIDP.value + "&ClaimAspectID=" + sClaimAspectID + "&mode=open", "", sDimensions + sSettings);
    if (strReturn == "OK") {
      document.location.reload(true);
    }
}

function GridSelect(oRow)
{
    var strLynxID = oRow.cells[1].innerText;
    NavOpenWindow( "claim", strLynxID, "1" );
}

function navigateTo(strEntity)
{
    var srcEle = event.srcElement;
    var curRow = srcEle.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
    var strLynxID = curRow.cells[1].innerText;

    switch(strEntity)
    {
        case "claim":
            NavOpenWindow( strEntity, strLynxID, "" );
            break;

        case "vehicle":
            var numVeh = parseInt(curRow.cells[8].innerText);
            if (numVeh > 0){
                var firstVehNum = curRow.cells[16].innerText;
                if (strLynxID != "" && firstVehNum != "")
                    NavOpenWindow( strEntity, strLynxID, firstVehNum );
            }
            break;

        case "property":
            var numVeh = parseInt(curRow.cells[8].innerText);
            var numProp = curRow.cells[9].innerText;
            if (numProp > 0){
                var firstPropNum = curRow.cells[16+(numVeh*4)].innerText;
                if (strLynxID != "" && firstPropNum != "")
                    NavOpenWindow( strEntity, strLynxID, firstPropNum );
            }
            break;
    }
    window.event.cancelBubble = true;
}

//this will attach the docNoBkSpace function to the document which will alter all documents that include this script file.
document.onkeydown = docNoBkSpace;

// Variables to set for tooltips:

messages= new Array()
// Write your descriptions in here.
messages[0]="Answer Wait Time";
messages[1]="Single-Click Column to sort";

if (document.attachEvent)
	document.attachEvent("onclick", top.hideAllMenuScriptlets);
else
	document.onclick = top.hideAllMenuScriptlets;

]]>
</SCRIPT>
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" style="background:transparent;border:0px" onLoad="popupInit();initSelectBoxes();initPage();" LEFTMARGIN="0" TOPMARGIN="0" RIGHTMARGIN="0" BOTTOMMARGIN="0" tabIndex="-1">

  <!-- begin content 1 -->
    <DIV id="CPScrollTable" style="position:relative; left:6px; width:960px;">
			<SPAN>
        <TABLE unselectable="on" class="ClaimMiscInputs" width="100%" onClick="sortColumn(event, 'tblSort2')" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
          <TR unselectable="on" class="QueueHeader" style="height:28px">
          <TD unselectable="on" class="TableSortHeader" type="TimeD"> Days Closed </TD>
          <TD unselectable="on" class="TableSortHeader" type="Number"> LYNX ID </TD>
          <TD unselectable="on" class="TableSortHeader" type="Number"> Status </TD>
          <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Policy Holder </TD>
          <TD unselectable="on" class="TableSortHeader" type="Date"> Date of Loss </TD>
          <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Insurance Co. </TD>
          </TR>
        </TABLE>
			</SPAN>
	    <DIV unselectable="on" class="autoflowTable" style="width:100%; height:282px;">
	      <TABLE unselectable="on" id="tblSort2" class="GridTypeTable" width="100%" cellspacing="1" border="0" cellpadding="0">
		      <TBODY bgColor1="ffffff" bgColor2="fff7e5">
						<xsl:for-each select="Claim"	>
              <xsl:sort data-type="number" select="@LynxID" order="ascending" />
							<xsl:call-template name="Claims"></xsl:call-template>
						</xsl:for-each>
	  	    </TBODY>
	     </TABLE>
	    </DIV>
    </DIV>
    <DIV unselectable="on" id="Layer1" style="position:absolute; left:0px; top:314px; width:100%">
      <DIV unselectable="on" id="CallerInfoTab" class="SmallGroupingTab" style="position:absolute; width:110px; z-index:2; top:7px; left:8px">
        Claim Preview </DIV>
      <DIV unselectable="on" id="InsuredInfo" class="SmallGrouping" style="position:absolute; left:6px; top:23px; z-index:1; width:726px; height:108px">
        <TABLE unselectable="on" class="paddedTable" border="0" cellspacing="0" cellpadding="0" width="100%">
          <TR unselectable="on">
            <TD unselectable="on" ><xsl:attribute name="nowrap"/>Vehicles in Claim (<span id="spVehNumP">0</span>): </TD>
            <TD unselectable="on" >
               <script>addSelectCustomValues('selPCVeh',2,'onSelectChange',1,1,0,1,'CRUD',0,'----',200);</script>
            </TD>
            <TD unselectable="on" ><xsl:attribute name="nowrap"/>Properties in Claim (<span id="spPropNumP">0</span>): </TD>
            <TD unselectable="on" >
              <script>addSelectCustomValues('selPCProp',2,'onSelectChange',1,1,0,1,'CRUD',0,'----',200);</script>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on" width="116"> LYNX ID </TD>
            <TD  unselectable="on" width="251">
              <INPUT type="text" id="txtLynxIDP" class="InputReadonlyField" size="15"  >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
            <TD unselectable="on" width="97"> Date of Loss: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtDateofLossP" class="InputReadonlyField" size="18" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on" width="116"> Policy Holder: </TD>
            <TD unselectable="on" width="251">
              <INPUT type="text" id="txtPHNameP" class="InputReadonlyField" size="30" >
                <xsl:attribute name="readonly"/>
              </INPUT>

            </TD>
            <TD unselectable="on" width="97"> Days Closed: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtTimeOpenP" class="InputReadonlyField" size="25" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on" width="116"> Claim Number: </TD>
            <TD unselectable="on" width="251">
              <INPUT type="text" id="txtClaimNumberP" class="InputReadonlyField" size="20" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
            <TD unselectable="on"  width="97"> State: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtStateP" class="InputReadonlyField" size="4" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on" width="116"> Insurance Co.: </TD>
            <TD unselectable="on"  width="251">
              <INPUT type="text" id="txtInsuranceCoP" class="InputReadonlyField" size="30" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
            <TD unselectable="on" width="97"> </TD>
            <TD unselectable="on" width="148" align="right">
                <input type="button" id="btnReOpen" name="btnReOpen" class="formButton" style="cursor:default;" value="Reopen" readonly="" disabled="" onclick="reOpenClaim()"/>
            </TD>
          </TR>
        </TABLE>
      </DIV>
    </DIV>


<DIV unselectable="on" id="divTooltip"> </DIV>

</BODY>
</HTML>
</xsl:template>

	<!-- Gets the New or Pending Claims -->
	<xsl:template name="Claims">
    <TR unselectable="on" onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' RowType='Claim' onClick='GridClick(this)' onDblClick='GridSelect(this)' style="height:21px;">
    <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
    <xsl:attribute name="ClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
      <TD unselectable="on" width="104" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@ClaimClosedTimeDays!=0"><xsl:value-of select="@ClaimClosedTimeDays"/><xsl:text> days</xsl:text></xsl:when>
          <xsl:when test="@ClaimClosedTimeHours!=0"><xsl:value-of select="@ClaimClosedTimeHours"/><xsl:text> hrs</xsl:text></xsl:when>
          <xsl:otherwise><xsl:value-of select="@ClaimClosedTimeMinutes"/><xsl:text> mins</xsl:text></xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" width="81" class="GridTypeTD">
        <xsl:value-of select="@LynxID"/>
      </TD>
        <TD unselectable="on" class="GridTypeTD" width="90px" style="text-align:left">
            <table cellspacing="0" cellpadding="0" border="0">
            <tr>
            <td width="26px" style="text-align:center">
            <xsl:choose>
                <xsl:when test="contains(@ClaimStatus, 'Closed')">
                    <img src="/images/icon_claim_closed.gif" title="Claim Closed" onclick="event.returnValue=false;" ondblclick="navigateTo('claim')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                </xsl:when>
                <xsl:otherwise>
                    <img src="/images/icon_claim_open.gif" title="Claim Open" onclick="event.returnValue=false;" ondblclick="navigateTo('claim')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                </xsl:otherwise>
            </xsl:choose>
            </td>
            <td width="30px" style="text-align:center">
            <xsl:variable name="VehicleCount"><xsl:value-of select="count(Vehicle)"/></xsl:variable>
            <xsl:variable name="VehiclesOpen"><xsl:value-of select="count(Vehicle[not(contains(@Status, 'Closed') or contains(@Status, 'Cancelled') or contains(@Status, 'Voided'))])"/></xsl:variable>
            <xsl:if test="$VehicleCount > 0">
                <xsl:choose>
                    <xsl:when test="$VehicleCount &gt; 1">
                        <xsl:choose>
                            <xsl:when test="$VehiclesOpen = $VehicleCount">
                                <img src="/images/icon_vehicle_multi_open.gif" title="All Vehicles Open" onclick="event.returnValue=false;" ondblclick="navigateTo('vehicle')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                            </xsl:when>
                            <xsl:when test="$VehiclesOpen = 0">
                                <img src="/images/icon_vehicle_multi_closed.gif" title="All Vehicles Closed" onclick="event.returnValue=false;" ondblclick="navigateTo('vehicle')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                            </xsl:when>
                            <xsl:when test="$VehicleCount &gt; 1 and $VehiclesOpen &lt; $VehicleCount">
                                <img src="/images/icon_vehicle_multi_open_closed.gif" title="Some Vehicles Closed" onclick="event.returnValue=false;" ondblclick="navigateTo('vehicle')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="$VehicleCount = 1">
                        <xsl:choose>
                            <xsl:when test="$VehiclesOpen = $VehicleCount">
                                <img src="/images/icon_vehicle_open.gif" title="Vehicle Open" onclick="event.returnValue=false;" ondblclick="navigateTo('vehicle')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                            </xsl:when>
                            <xsl:when test="$VehiclesOpen = 0">
                                <img src="/images/icon_vehicle_closed.gif" onclick="event.returnValue=false;" ondblclick="navigateTo('vehicle')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)">
                                  <xsl:attribute name="title"><xsl:value-of select="Vehicle/@Status"/></xsl:attribute>
                                </img>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:when>
                </xsl:choose>
            </xsl:if>
            </td>
            <td width="30px" style="text-align:center">
            <xsl:variable name="PropertyCount"><xsl:value-of select="count(Property)"/></xsl:variable>
            <xsl:variable name="PropertiesOpen"><xsl:value-of select="count(Property[not(contains(@Status, 'Closed'))])"/></xsl:variable>
            <xsl:if test="$PropertyCount > 0">
                <xsl:choose>
                    <xsl:when test="$PropertyCount &gt; 1">
                        <xsl:choose>
                            <xsl:when test="$PropertiesOpen = $PropertyCount">
                                <img src="/images/icon_property_multi_open.gif" title="All Properties Open" onclick="event.returnValue=false;" ondblclick="navigateTo('property')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                            </xsl:when>
                            <xsl:when test="$PropertiesOpen = 0">
                                <img src="/images/icon_property_multi_closed.gif" title="All Properties Closed" onclick="event.returnValue=false;" ondblclick="navigateTo('property')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                            </xsl:when>
                            <xsl:when test="$PropertyCount &gt; 1 and $PropertiesOpen &lt; $PropertyCount">
                                <img src="/images/icon_property_multi_open_closed.gif" title="Some Properties Closed" onclick="event.returnValue=false;" ondblclick="navigateTo('property')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="$PropertyCount = 1">
                        <xsl:choose>
                            <xsl:when test="$PropertiesOpen = $PropertyCount">
                                <img src="/images/icon_property_open.gif" title="Property Open" onclick="event.returnValue=false;" ondblclick="navigateTo('property')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                            </xsl:when>
                            <xsl:when test="$PropertiesOpen = 0">
                                <img src="/images/icon_property_closed.gif" title="Property Closed" onclick="event.returnValue=false;" ondblclick="navigateTo('property')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:when>
                </xsl:choose>
            </xsl:if>
            </td>
            </tr>
            </table>
        </TD>
      <TD unselectable="on" width="150" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="string-length(@PolicyHolderBusinessName) > 0" >
            <xsl:value-of select="substring(@PolicyHolderBusinessName,1,25)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
                <xsl:when test="@PolicyHolderLastName != '' and @PolicyHolderFirstName != ''">
                    <xsl:value-of select="substring(@PolicyHolderLastName,0,12)"/>, <xsl:value-of select="substring(@PolicyHolderFirstName,0,12)"/>
                </xsl:when>
                <xsl:when test="@PolicyHolderLastName != '' or @PolicyHolderFirstName != ''">
                    <xsl:value-of select="substring(@PolicyHolderLastName,0,12)"/><xsl:value-of select="substring(@PolicyHolderFirstName,0,12)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" width='100' class="GridTypeTD">
        <xsl:value-of select="user:UTCConvertDateByNodeType(.,'LossDate','A')"/>
      </TD>
      <TD unselectable="on"  class="GridTypeTD">
        <xsl:value-of select="@InsuranceCompanyName"/>
      </TD>
      <TD style="display:none"><xsl:value-of select="@ClientClaimNumber"/></TD>
      <TD style="display:none"><xsl:value-of select="@LossState"/></TD>
      <TD style="display:none"><xsl:value-of select="count(Vehicle)"/></TD>
      <TD style="display:none"><xsl:value-of select="count(Property)"/></TD>
      <TD style="display:none"><xsl:value-of select="@ClaimClosedTimeDays"/></TD>
      <TD style="display:none"><xsl:value-of select="@ClaimClosedTimeHours"/></TD>
      <TD style="display:none"><xsl:value-of select="@ClaimClosedTimeMinutes"/></TD>
      <TD style="display:none"></TD><!--need to remove and update indexs-->
      <TD style="display:none"><xsl:value-of select="@PolicyHolderFirstName"/></TD>
      <TD style="display:none"><xsl:value-of select="@PolicyHolderLastName"/></TD>
      <xsl:for-each select="Vehicle">
        <TD style="display:none"><xsl:value-of select="@VehicleNumber"/></TD>
        <TD style="display:none">
            <xsl:choose>
                <xsl:when test= "string-length(@OwnerBusinessName) &gt; 0">
                    <xsl:value-of select="@OwnerBusinessName"/>
                </xsl:when>
                <xsl:when test= "string-length(@OwnerLastName) &gt; 0 and string-length(@OwnerFirstName) &gt; 0">
                  <xsl:value-of select="user:myNameConcat(string(@OwnerLastName), string(@OwnerFirstName), ', ')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat(@OwnerLastName, @OwnerFirstName)"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="string(@VehicleYear) = '0'">
                    <xsl:variable name="MM"><xsl:value-of select="user:myNameConcat(normalize-space(string(@Make)), normalize-space(string(@Model)), ' ')"/></xsl:variable>
                    <xsl:if test="$MM != '&amp;nbsp;'">
                        (<xsl:value-of select="$MM"/><xsl:value-of select="string-length(normalize-space($MM))"/>)
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                    (<xsl:value-of select="user:myNameConcat(string(user:myNameConcat(string(@VehicleYear), string(@Make), ' ')), string(@Model), ' ')"/>)
                </xsl:otherwise>
            </xsl:choose>
            <!-- <xsl:value-of select="@OwnerLastName"/>, <xsl:value-of select="@OwnerFirstName"/> (<xsl:value-of select="@VehicleYear"/>-<xsl:value-of select="@Make"/><xsl:text> </xsl:text><xsl:value-of select="@Model"/>) -->
        </TD>
        <!-- <TD style="display:none"><xsl:value-of select="@OwnerLastName"/>, <xsl:value-of select="@OwnerFirstName"/> (<xsl:value-of select="@VehicleYear"/>-<xsl:value-of select="@Make"/><xsl:text> </xsl:text><xsl:value-of select="@Model"/>)</TD> -->
        <!-- <TD style="display:none"><xsl:value-of select="@VehicleYear"/>-<xsl:value-of select="@Make"/><xsl:text> </xsl:text><xsl:value-of select="@Model"/></TD> -->
        <TD style="display:none"><xsl:value-of select="@Status"/></TD>
        <TD style="display:none"><xsl:value-of select="@ClaimAspectID"/></TD>
      </xsl:for-each>
      <xsl:for-each select="Property">
        <TD style="display:none"><xsl:value-of select="@PropertyNumber"/></TD>
        <TD style="display:none">
            <xsl:choose>
                <xsl:when test= "string-length(@OwnerBusinessName) &gt; 0">
                    <xsl:value-of select="@OwnerBusinessName"/>
                </xsl:when>
                <xsl:when test= "string-length(@OwnerLastName) &gt; 0 and string-length(@OwnerFirstName) &gt; 0">
                  <xsl:value-of select="user:myNameConcat(string(@OwnerLastName), string(@OwnerFirstName), ', ')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat(string(@OwnerLastName), string(@OwnerFirstName))"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="string-length(@PropertyName) &gt; 0">
            (<xsl:value-of select="@PropertyName"/>)
            </xsl:if>
            <!-- <xsl:value-of select="@OwnerLastName"/>, <xsl:value-of select="@OwnerFirstName"/> (<xsl:value-of select="@PropertyName"/>) -->
        </TD>
        <!-- <TD style="display:none"><xsl:value-of select="@OwnerLastName"/>, <xsl:value-of select="@OwnerFirstName"/> (<xsl:value-of select="@PropertyName"/>)</TD> -->
        <!-- <TD style="display:none"><xsl:value-of select="@PropertyName"/></TD> -->
        <TD style="display:none"><xsl:value-of select="@Status"/></TD>
        <TD style="display:none"><xsl:value-of select="@ClaimAspectID"/></TD>
      </xsl:for-each>
    </TR>
	</xsl:template>


</xsl:stylesheet>
