<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml"
              omit-xml-declaration="yes"
              cdata-section-elements="Document EmailSubject EmailBody Subject Body Message MessageEscaped MessageHTML AttachmentsHTML NotesMsg"
               />
  <xsl:template match="/">
    <Root>
      <Send>
        <xsl:choose>
          <!-- CSR bundles -->
          <xsl:when test="/Root/@bundlingProfileName='RRP - Acknowledgement of Claim'">
            <xsl:attribute name="via">EML</xsl:attribute>
            <xsl:attribute name="value">
                <xsl:choose>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='General Casualty'">ClaimMailGC@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='Unigard Office'">ClaimMailGC@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='National Farmers Union'">ClaimMailGC@us.qbe.com</xsl:when>
                </xsl:choose>
            </xsl:attribute>
          </xsl:when>
          <xsl:when test="/Root/@bundlingProfileName='RRP - Missing ProcessClaims Assignment'">
            <xsl:attribute name="via">EML</xsl:attribute>
            <xsl:attribute name="value">
                <xsl:choose>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='General Casualty'">ClaimMailGC@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='Unigard Office'">ClaimMailGC@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='National Farmers Union'">ClaimMailGC@us.qbe.com</xsl:when>
                </xsl:choose>
            </xsl:attribute>
          </xsl:when>
          <xsl:when test="/Root/@bundlingProfileName='RRP - Assignment Cancellation'">
            <xsl:attribute name="via">EML</xsl:attribute>
            <xsl:attribute name="value">
                <xsl:choose>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='General Casualty'">ClaimMailGC@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='Unigard Office'">ClaimMailGC@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='National Farmers Union'">ClaimMailGC@us.qbe.com</xsl:when>
                </xsl:choose>
            </xsl:attribute>
          </xsl:when>
          <xsl:when test="/Root/@bundlingProfileName='RRP - Status Update - Claim Rep'">
            <xsl:attribute name="via">EML</xsl:attribute>
            <xsl:attribute name="value">
                <xsl:choose>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='General Casualty'">ClaimMailGC@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='Unigard Office'">ClaimMailGC@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='National Farmers Union'">ClaimMailGC@us.qbe.com</xsl:when>
                </xsl:choose>
            </xsl:attribute>
          </xsl:when>
          <xsl:when test="/Root/@bundlingProfileName='RRP - Status Update - MDS'">
            <xsl:attribute name="via">EML</xsl:attribute>
            <xsl:attribute name="value">
                <xsl:choose>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='General Casualty'">ClaimMailGC@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='Unigard Office'">ClaimMailGC@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='National Farmers Union'">ClaimMailGC@us.qbe.com</xsl:when>
                </xsl:choose>
            </xsl:attribute>
          </xsl:when>
          <xsl:when test="/Root/@bundlingProfileName='RRP - Status Update - Action Required'">
            <xsl:attribute name="via">EML</xsl:attribute>
            <xsl:attribute name="value">
                <xsl:choose>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='General Casualty'">ClaimMailGC@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='Unigard Office'">ClaimMailGC@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='National Farmers Union'">ClaimMailGC@us.qbe.com</xsl:when>
                </xsl:choose>
            </xsl:attribute>
          </xsl:when>
          <xsl:when test="/Root/@bundlingProfileName='RRP - No Inspection Alert &amp; Close'">
            <xsl:attribute name="via">EML</xsl:attribute>
            <xsl:attribute name="value">qbeapd@lynxservices.com</xsl:attribute>
          </xsl:when>

          <!-- MDS Bundles -->
          <xsl:when test="/Root/@bundlingProfileName='RRP - Total Loss Alert (Adjuster)'">
            <xsl:attribute name="via">EML</xsl:attribute>
            <xsl:attribute name="value">
                <xsl:choose>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='General Casualty'">ClaimMailGC@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='Unigard Office'">ClaimMailGC@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='National Farmers Union'">ClaimMailGC@us.qbe.com</xsl:when>
                </xsl:choose>
            </xsl:attribute>
          </xsl:when>
          <xsl:when test="/Root/@bundlingProfileName='RRP - Total Loss Valuation (Adjuster &amp; Appraisers)'">
            <xsl:attribute name="via">EML</xsl:attribute>
            <xsl:attribute name="value">
                <xsl:choose>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='General Casualty'">ClaimMailGC@us.qbe.com;jeff.parks@us.qbe.com; kenneth.wiederien@us.qbe.com;rusty.howell@us.qbe.com; mike.garrett@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='Unigard Office'">ClaimMailGC@us.qbe.com;jeff.parks@us.qbe.com; kenneth.wiederien@us.qbe.com;rusty.howell@us.qbe.com; mike.garrett@us.qbe.com</xsl:when>
                  <xsl:when test="//Root/Claim/@AdjusterOfficeName='National Farmers Union'">ClaimMailGC@us.qbe.com;jeff.parks@us.qbe.com; kenneth.wiederien@us.qbe.com;rusty.howell@us.qbe.com; mike.garrett@us.qbe.com</xsl:when>
                </xsl:choose>
            </xsl:attribute>
          </xsl:when>
          <!--<xsl:when test="/Root/@bundlingProfileName='SHOP: RRP - Approved Estimate Shop Notification'">
            <xsl:value-of select="."/>
          </xsl:when>-->
          <xsl:when test="/Root/@bundlingProfileName='RRP - Closing Inspection Complete'">
            <xsl:attribute name="via">EML</xsl:attribute>
            <xsl:attribute name="value">qbeapd@lynxservices.com</xsl:attribute>
          </xsl:when>
          <xsl:when test="/Root/@bundlingProfileName='RRP - Closing Supplement'">
            <xsl:attribute name="via">EML</xsl:attribute>
            <xsl:attribute name="value">qbeapd@lynxservices.com</xsl:attribute>
          </xsl:when>
          <xsl:when test="/Root/@bundlingProfileName='RRP - Closing Total Loss'">
            <xsl:attribute name="via">EML</xsl:attribute>
            <xsl:attribute name="value">qbeapd@lynxservices.com</xsl:attribute>
          </xsl:when>
        </xsl:choose>
      </Send>
    </Root>
  </xsl:template>

</xsl:stylesheet>








