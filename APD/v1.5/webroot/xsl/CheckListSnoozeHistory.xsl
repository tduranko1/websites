<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:local="http://local.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt">

	<xsl:import href="msxsl/msjs-client-library.js"/>
	<xsl:import href="msxsl/generic-template-library.xsl"/>

  <xsl:template match="/">

    <html>
      <head>
        <title>Task Snooze History</title>

        <!-- <link rel="stylesheet" href="/css/apdMain.css" type="text/css"/> -->
				<LINK rel="stylesheet" href="/css/apdWindow.css" type="text/css"/>

        <style type="text/css">
            A {color:black; text-decoration:none;}
        </style>

        <script type="text/javascript" language="JavaScript1.2" src="/js/apdcontrols.js"></script>

        <script language="JavaScript">
            <![CDATA[
              function doCancel()
							{
                	window.returnValue = false;
                	window.close();
              }
            ]]>
        </script>

      </head>
      <body class="bodyAPDSub" scroll="yes">
    		<table class="ClaimMiscInputs" border='0' cellspacing='0' cellpadding='2' >
      		<tr height="34px" class="QueueHeader" >
      		  <td width="11%" class="clWinGridTypeHeaderStatic">When Delayed</td>
      		  <td width="15%" class="clWinGridTypeHeaderStatic">Delayed By</td>
      		  <td width="30%" class="clWinGridTypeHeaderStatic">Reason</td>
      		  <td width="*" class="clWinGridTypeHeaderStatic">Comment </td>
      		</tr>

          <xsl:choose>
            <xsl:when test="count(/Root/TaskDelay)=0">
							<tr><td>
          			<img src="/images/spacer.gif" width="8" height="60" border="0"/>
							</td></tr>
							<tr>
								<td align="right" colspan="2">
									<img src="images/event_4.bmp"/>
								</td>
								<td align="left" colspan="2">
									<strong> No snooze history is available for this task.</strong>
								</td>
							</tr>
            </xsl:when>
            <xsl:otherwise>
        			<xsl:apply-templates select="/Root/TaskDelay" />
            </xsl:otherwise>
          </xsl:choose>

    		</table>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="TaskDelay">
		<tr>

      <xsl:attribute name="bgColor">
        <xsl:value-of select="user:chooseBackgroundColor(position(), 'fff7e5', 'ffffff' ) "/>
      </xsl:attribute>

      <td id="DelayDate" class="clWinGridTypeTDStatic" align="center">
        <xsl:value-of select="user:UTCConvertDateAndTimeByNodeTypeShort(.,'DelayDate','a')"/>
      </td>
      <td id="DelayUser" class="clWinGridTypeTDStatic" align="center">
        <xsl:value-of select="user:FormatPersonName(@UserNameFirst,@UserNameLast)"/>
      </td>
      <td id="Reason" class="clWinGridTypeTDStatic" align="center">
        <xsl:value-of select="@Reason"/>
      </td>
      <td id="Comment" class="clWinGridTypeTDStatic" align="center">
        <xsl:value-of select="@Comment"/>
      </td>
		</tr>
  </xsl:template>

</xsl:stylesheet>

