<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:local="http://local.com/mynamespace"
    id="Notes">

<xsl:import href="msxsl/msjs-client-library_apdnet.js"/>
  <xsl:import href="msxsl/generic-template-library_apdnet.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<msxsl:script language="JScript" implements-prefix="local">

    // Returns an integer based upon date and role logic.
    function getDateRoleState( date, complete_role )
    {
        var state = 0;
        var cur_date = new Date();
        var due_date = new Date( date );

        if ( ( due_date.valueOf() - ( 3600000 * 24 ) ) &gt; cur_date.valueOf() )
            state = 4;  // overdue
        else if ( due_date.valueOf() &gt; cur_date.valueOf() )
            state = 2;  // due within 24 hours

        if ( complete_role == '0' )
            state += 1;

        return state;
    }

</msxsl:script>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="LynxID"/>
<xsl:param name="UserID"/>
<xsl:param name="WindowID"/>
<xsl:param name="windowState"/>
<xsl:param name="ShowAllNotes"/>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="NoteCRUD" select="NoteCRUD"/>

<xsl:template match="/Root">

<!-- New -->
<xsl:variable name="LynxID" select="LynxID"/>
<xsl:variable name="UserID" select="UserID"/>  <!-- @vars are from Attributes -->
<xsl:variable name="SessionKey" select="SessionKey"/>
<xsl:variable name="WindowID" select="WindowID"/>
<xsl:variable name="windowState" select="windowState"/>
<!-- xsl:variable name="ShowAllNotes" select="ShowAllNotes"/ -->
<!-- xsl:variable name="ShowAllNotes" select="'true'"/ -->

  <xsl:variable name="VehClaimAspectID" select="VehClaimAspectID"/>
<xsl:variable name="VehCount" select="VehCount"/>
<xsl:variable name="NoteCRUD" select="NoteCRUD"/>  <!-- Note from Nodes -->
<xsl:variable name="RunTime" select="RunTime"/>

  <!-- TVD - Moved the session saving to dot net -->
<!--xsl:variable name="PertainsTo"-->
<!--xsl:copy-of select="/Root/Reference[@List='PertainsTo']"/-->
<!--xsl:copy-of select="/Root/Reference[@List='ServiceChannel']"/-->
<!--xsl:copy-of select="/Root/Reference[@List='Status']"/-->
<!--/xsl:variable-->

<!-- TVD - Moved the session saving to dot net -->
<!--xsl:variable name="Metadata"-->
<!--xsl:copy-of select="/Root/Metadata"/-->
<!--/xsl:variable-->

<!-- TVD - Moved the session saving to dot net -->
<!-- xsl:value-of select="session:XslUpdateSessionNodeList( 'NotePertainsTo', $PertainsTo )"/ -->
<!-- xsl:value-of select="session:XslUpdateSessionNodeList( 'NoteMetadata', $Metadata )"/ -->

  <!-- APDXSL:C:\websites\apd\v1.2\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspNoteGetDetailXML,APDNotes.xsl, 147468   -->

<!-- Update Session Variables using the WebService -->
  <!--xsl:variable name="SessVariable" select="concat('NotePertainsTo-', $WindowID)"/-->
  <!--xsl:variable name="SessVariable" select="concat('Metadata-', $WindowID)"/-->
  <!--xsl:variable name="Sess" select="user:UpdateSessionVar(string($SessionKey), string($SessVariable), $PertainsTo)"/-->
  <!--xsl:variable name="Sess" select="user:UpdateSessionVar(string($SessionKey), string($SessVariable), $Metadata)"/-->
  <!--xsl:variable name="FOOBAR" select="document($Sess)"/-->
  <HTML>

<HEAD>
<TITLE>Diary ToDo List</TITLE>

<LINK rel="stylesheet" href="/css/apdwindow.css" type="text/css"/>
<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>
<SCRIPT language="JavaScript" src="js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="js/APDWindow.js"></SCRIPT>
<SCRIPT language="JavaScript" src="js/tablesort.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>
<script type="text/javascript">
          document.onhelp=function(){
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,19);
		  event.returnValue=false;
		  };
</script>

<script language="javascript">
  <!-- Check Veh IFrame exists -->
  //var iframe = document.frames[''];
  //alert("Check Parent");
  
  var gsWindowID = "<xsl:value-of select="$WindowID"/>";
  var sWindowName = "winNotes";
  var vNoteCRUD = "<xsl:value-of select="$NoteCRUD"/>";
  var vNoteLynxID = "<xsl:value-of select="$LynxID"/>";
  var vNoteClaimAspectID = "<xsl:value-of select="$VehClaimAspectID"/>";
  var vNoteUserID = "<xsl:value-of select="$UserID"/>";
  var sWindowState = "<xsl:value-of select="$windowState"/>";
  var saveNotesBG;
  var iNotes = 0;
  var iTotalNotes;
  var gbShowAllNotes = false;

  if ('<xsl:value-of select="$ShowAllNotes"/>' != "")
    gbShowAllNotes = "<xsl:value-of select="$ShowAllNotes"/>";

  <!-- Check if VehClaimAspectID exits.  If not, wait and then reload the page. -->
  //if (vNoteClaimAspectID == "") {
  //  <!-- Wait 10 sec -->
  //  setTimeout(
  //}

  <![CDATA[  

  function pageInit(){
    if (sWindowState == "max")
      setWindowState(sWindowState);

    if (document.getElementById("ShowAllFilter") && gbShowAllNotes == "true")
    {
      document.getElementById("ShowAllFilter").src = "/images/docfilter_single.gif";
      document.getElementById("ShowAllFilter").title = "Show notes for selected vehicle only";
      document.getElementById("ShowAllFilter").onclick = new Function("toggleCheckListShowAll(false); return false;");
    }
  }

  // Override from the standard version to handle note body popups.
  function noteGridMouseOver( oObject )
  {
      try {
          saveNotesBG = oObject.style.backgroundColor;
          oObject.style.backgroundColor="#FFEEBB";
          oObject.style.color = ( oObject.supervisor == "true" ) ? "#3333ff" : "#000066";
          oObject.style.cursor='hand';

          if ( sWindowState != 'max' )
              displayNoteBottomBody( oObject.idx, '' );
      } catch ( e ) { ClientError( e.message ); }
  }

  // Overrid from the standard version to handle note body popups.
  function noteGridMouseOut( oObject )
  {
      try {
          oObject.style.backgroundColor = saveNotesBG;
          oObject.style.color = ( oObject.supervisor == "true" ) ? "#0000ff" : "#000000";

          if ( sWindowState != 'max' )
              displayNoteBottomBody( oObject.idx, 'none' );
      } catch ( e ) { ClientError( e.message ); }
  }

  // Sets/Clears the note body display for the indexed note.
  function displayNoteBottomBody( idx, state )
  {
      try {
          var noteBodyBottom = document.getElementById( 'noteBodyBottom' + idx );
          if ( noteBodyBottom != null )
          {
              //window.clipboardData.setData('text',noteBodyBottom.innerHTML);
              noteBodyBottom.style.display = state;
          }
      } catch ( e ) { ClientError( e.message ); }
  }

  // Overridden window function for display/hiding of 'extra' table columns.
  function setWindowState( newstate )
  {
      try {
          if ( this.state != newstate ) {
              setNotesExtraColDisplay( ( newstate == 'max' ) ? '' : 'none' );
              this.state = newstate;
              sWindowState = newstate;
          }
      } catch ( e ) { ClientError( e.message ); }
  }

  // Sets/Clears the display state for the 'extra' table columns.
  function setNotesExtraColDisplay( state )
  {
      try {
              document.all.noteType.style.display = state;
              document.all.noteBodyRight.style.display = state;
              document.all.noteTypeHeader.style.display = state;
              document.all.noteBodyRightHeader.style.display = state;

              if (state == "none"){
                document.all.noteUserID.style.width = "100px"
                document.all.noteUserIDHeader.style.width = "150px";
              } else {
                document.all.noteUserID.style.width = "100px"
                document.all.noteUserIDHeader.style.width = "100px";
              }
      } catch ( e ) { ClientError( e.message ); }
  }

  function addNote()
  {
      try {
          if ( vNoteCRUD.substr( 0, 1 ) != "C" )
          {
              ClientInfo( 'You do not have permission to add notes.  Please contact your supervisor.' );
          }
          else if ( vNoteLynxID != null && vNoteLynxID != "" && vNoteLynxID != "0" )
          {
              var arrNotes = window.showModalDialog( "NoteDetails.asp?WindowID=" + gsWindowID, window,
                "dialogHeight:226px; dialogWidth:480px; resizable:no; status:no; help:no; center:yes; ");
              SetEntityMenu(arrNotes);
          }
          else
              ClientInfo( "You must open a claim before creating notes.");
      } catch ( e ) { ClientError( e.message ); }
  }

  function editNote(row)
  {
      try {
          var strReq = "NoteDetails.asp?WindowID=" + gsWindowID + "&DocumentID=" + row.children.DocumentID.innerText;
          var arrNotes = window.showModalDialog( strReq, window,
            "dialogHeight:291px; dialogWidth:490px; resizable:no; status:no; help:no; center:yes;" );

          SetEntityMenu(arrNotes);

      } catch ( e ) { ClientError( e.message ); }
  }

  function SetEntityMenu(arr){
    var currentContext
    var entity;
    var delta;

    if (arr){
      arr = arr.split("|");
      currentContext = arr[0];
      if (currentContext == "false") return;
      entity = arr[2];
      delta = arr[1];
      if (delta == 1) top.gsCountNotes++;
      if (delta == -1) top.gsCountNotes--;

      var bVal = (top.gsCountNotes <= 0 && top.gsCountTasks <= 0 && top.gsCountBilling <= 0 && top.gsEntityCRUD.indexOf("D") != -1) ? true : false;

      switch(entity){
        case "veh":
          top.VehiclesMenu(bVal,3);
        case "prp":
          top.PropertiesMenu(bVal,3);
      }
    }
  }

  function refreshNotesWindow(bFromTimer)
  {
      try {
          if ( ( String( bFromTimer ) == "undefined" ) || ( bFromTimer == null ) )
              bFromTimer = true;

          if (bFromTimer) {
            if (sWindowState == "max"){
              parent.loadContent("/APDNotes.asp?WindowID=" + gsWindowID + "&LynxID=" + vNoteLynxID + "&UserID=" + vNoteUserID + "&winState=" + sWindowState + "&ShowAllNotes=" + gbShowAllNotes);
              return;
            }
            window.location = "/APDNotes.asp?WindowID=" + gsWindowID + "&LynxID=" + vNoteLynxID + "&UserID=" + vNoteUserID + "&winState=" + sWindowState + "&ShowAllNotes=" + gbShowAllNotes;
            window.location.reload();
          } else {
            parent.loadContent("/APDNotes.asp?WindowID=" + gsWindowID + "&LynxID=" + vNoteLynxID + "&UserID=" + vNoteUserID + "&winState=" + sWindowState + "&ShowAllNotes=" + gbShowAllNotes);
          }
      } catch ( e ) { alert( "APDNotes.xsl::refreshNotesWindow() " + e.message ); }
  }

  function toggleCheckListShowAll(bNotesToggle)
  {
    gbShowAllNotes = bNotesToggle;
    refreshNotesWindow(false);
  }
]]>
</script>
</HEAD>
<BODY unselectable="on" style="background-color:#FFFFFF;overflow:hidden;border:0px;padding:1px;" onload="pageInit()" tabIndex="-1" topmargin="0"  leftmargin="0">
  <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;background-color:#067BAF;width:100%;height:100%;" >
    <tr style="padding:1px;height:19px;" oncontextmenu="return false">
      <td>
        <div id="divWinToolBar" class="clWinToolBar" >
          <img border="0" src="/images/smnew2.gif" onClick="addNote(); return false" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Add Note" />
          <img src="/images/spacer.gif" width="8" height="2" border="0"/>
          <img border="0" src="/images/smrefresh.gif" onClick="refreshNotesWindow(false, vNoteLynxID, vNoteUserID); return false" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Refresh" />
          <xsl:if test="$VehCount &gt; 1">
            <img src="/images/spacer.gif" width="8" height="2" border="0"/>
            <img id="ShowAllFilter" border="0" src="/images/docfilter_many.gif" onClick="toggleCheckListShowAll(true); return false" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" title="Show all notes for claim" />
          </xsl:if>
          &#160;&#160;&#160;<xsl:value-of select="$RunTime"/>
        </div>
      </td>
    </tr>
    <tr style="padding:1px;height:21px;">
      <td>
        <xml id="Reference"><xsl:copy-of select="//Reference[@List='NoteType']"/></xml>
          <table width1="100%" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSortNotes')" cellspacing="0" border="0" cellpadding="1"
            style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size:10px; text-align:center table-layout:fixed" oncontextmenu="return false">
            <tbody>
              <tr class="QueueHeader" style="height:20px">
                <td class="clWinGridTypeHeader" width="30" type="String">Veh</td>
                <td class="clWinGridTypeHeader" width="18" type="String">Ch</td>
                <td class="clWinGridTypeHeader" width="60" type="Date">Date</td>
                <xsl:choose>
                  <xsl:when test="$windowState='max'">
                    <td class="clWinGridTypeHeader" width="100" type="String" id="noteUserIDHeader">User</td>
                    <td class="clWinGridTypeHeader" width="110" type="String" id="noteTypeHeader">Note Type</td>
                    <td class="clWinGridTypeHeader" width="660" type="String" id="noteBodyRightHeader">Note</td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td class="clWinGridTypeHeader" width="150" type="String" id="noteUserIDHeader">User</td>
                    <td class="clWinGridTypeHeader" width="110" type="String" style="display:none" id="noteTypeHeader"> Note Type </td>
                    <td class="clWinGridTypeHeader" width="660" type="String" style="display:none" id="noteBodyRightHeader"> Note </td>
                  </xsl:otherwise>
                </xsl:choose>
              </tr>
            </tbody>
          </table>
      </td>
    </tr>
    <tr style="padding:1px;height:*;">
      <td>
        <div style="width:100%;height:100%;overflow:hidden;overflow:auto;background-color:#FFFFFF" class="clScrollTable">
          <table id="tblSortNotes" class1="clWinGridTypeTable" width1="100%" cellspacing="0" border="1" cellpadding="2" style="table-layout:fixed;border:1px; border-collapse:collapse">
            <colgroup>
            <col width="34"/>
            <col width="20"/>
            <col width="62"/>
            <xsl:choose>
              <xsl:when test="$windowState='max'">
                <col width="100" id="noteUserID" />
                <col width="130" id="noteType"/>
                <col width="634" id="noteBodyRight"/>
              </xsl:when>
              <xsl:otherwise>
                <col width="100" id="noteUserID" />
                <col width="130" style="display:none" id="noteType"/>
                <col width="634" style="display:none" id="noteBodyRight"/>
              </xsl:otherwise>
            </xsl:choose>
            <col width="0"   style="display:none"/>
            </colgroup>
            <tbody dataRows="2" bgColor1="fff7e5" bgColor2="ffffff">

              <!--xsl:copy-of select="$PertainsTo"/-->
              <xsl:if test="contains($NoteCRUD,'R')">
                <xsl:choose>
                  <xsl:when test="$ShowAllNotes = 'true'">
                    <xsl:for-each select="Note[@ClaimAspectCode = 'veh']" >
                      <xsl:sort select="@ClaimAspectID" order="ascending"/>
                      <xsl:sort select="@CreatedDate" order="descending"/>
                      <xsl:call-template name="Note"/>
                    </xsl:for-each>
                    <xsl:for-each select="Note[@ClaimAspectCode = 'clm']" >
                      <xsl:sort select="@CreatedDate" order="descending"/>
                      <xsl:call-template name="Note"/>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:when test="$VehClaimAspectID = ''">
                    <xsl:for-each select="Note[@ClaimAspectCode = 'veh']" >
                      <xsl:sort select="@ClaimAspectID" order="ascending"/>
                      <xsl:sort select="@CreatedDate" order="descending"/>
                      <xsl:call-template name="Note"/>
                    </xsl:for-each>
                    <xsl:for-each select="Note[@ClaimAspectCode = 'clm']" >
                      <xsl:sort select="@CreatedDate" order="descending"/>
                      <xsl:call-template name="Note"/>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:for-each select="Note[@ClaimAspectID = $VehClaimAspectID]" >
                      <xsl:sort select="@CreatedDate" order="descending"/>
                      <xsl:call-template name="Note"/>
                    </xsl:for-each>
                    <xsl:for-each select="Note[@ClaimAspectCode = 'clm']" >
                      <xsl:sort select="@CreatedDate" order="descending"/>
                      <xsl:call-template name="Note"/>
                    </xsl:for-each>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:if>
            </tbody>
          </table>
        </div>
      </td>
    </tr>
  </table>
</BODY>
</HTML>
</xsl:template>

  <!-- 06Jun2013 - TVD - New Code added to handle date formatting -->
  <xsl:template name="formatDate">
    <xsl:param name="dateTime" />
    <xsl:variable name="date" select="substring-before($dateTime, 'T')" />
    <xsl:variable name="year" select="substring-before($date, '-')" />
    <xsl:variable name="month" select="substring-before(substring-after($date, '-'), '-')" />
    <xsl:variable name="day" select="substring-after(substring-after($date, '-'), '-')" />
    <xsl:value-of select="concat($month, '/', $day, '/', $year)" />
  </xsl:template>

  <!-- 06Jun2013 - TVD - New Code added to handle time formatting -->
  <xsl:template name="formatTime">
    <xsl:param name="dateTime" />
    <xsl:variable name="time" select="substring-after($dateTime, 'T')" />
    <xsl:variable name="hh" select="substring-before($time, ':')" />
    <xsl:variable name="mm" select="substring-before(substring-after($time, ':'), ':')" />
    <xsl:variable name="ss" select="substring-after(substring-after($time, ':'), ':')" />
    <xsl:choose>
      <xsl:when test="$hh > 12">
        <xsl:variable name="nhh" select="substring-before($time, ':') - 12" />
        <xsl:value-of select="concat(concat($nhh, ':', $mm), ' PM')"/>
      </xsl:when>
      <xsl:when test="$hh = 12">
        <xsl:variable name="nhh" select="substring-before($time, ':')" />
        <xsl:value-of select="concat(concat($nhh, ':', $mm), ' PM')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="nhh" select="substring-before($time, ':')" />
        <xsl:value-of select="concat(concat($nhh, ':', $mm), ' AM')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- 06Jun2013 - TVD - New Code added to handle date/time formatting -->
  <xsl:template name="formatDateTime">
    <xsl:param name="dateTime" />
    <xsl:variable name="date" select="substring-before($dateTime, 'T')" />
    <xsl:variable name="year" select="substring(substring-before($date, '-'),3,2)" />
    <xsl:variable name="month" select="substring-before(substring-after($date, '-'), '-')" />
    <xsl:variable name="day" select="substring-after(substring-after($date, '-'), '-')" />

    <xsl:variable name="time" select="substring-after($dateTime, 'T')" />
    <xsl:variable name="hh" select="substring-before($time, ':')" />
    <xsl:variable name="mm" select="substring-before(substring-after($time, ':'), ':')" />
    <xsl:variable name="ss" select="substring-after(substring-after($time, ':'), ':')" />
    <xsl:choose>
      <xsl:when test="$hh > 12">
        <xsl:variable name="nhh" select="substring-before($time, ':') - 12" />
        <xsl:variable name="TimeFormatted" select="concat(concat($nhh, ':', $mm), ' PM')"/>
        <xsl:variable name="DateFormatted" select="concat($month, '/', $day, '/', $year)"/>
        <xsl:value-of select="concat($DateFormatted, ' ', $TimeFormatted)"/>
      </xsl:when>
      <xsl:when test="$hh = 12">
        <xsl:variable name="nhh" select="substring-before($time, ':')" />
        <xsl:variable name="TimeFormatted" select="concat(concat($nhh, ':', $mm), ' PM')"/>
        <xsl:variable name="DateFormatted" select="concat($month, '/', $day, '/', $year)"/>
        <xsl:value-of select="concat($DateFormatted, ' ', $TimeFormatted)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="nhh" select="substring-before($time, ':')" />
        <xsl:variable name="TimeFormatted" select="concat(concat($nhh, ':', $mm), ' AM')"/>
        <xsl:variable name="DateFormatted" select="concat($month, '/', $day, '/', $year)"/>
        <xsl:value-of select="concat($DateFormatted, ' ', $TimeFormatted)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Each note gets its own table row -->
<xsl:template name="Note">

  <xsl:if test="boolean(string(@CreatedUserID))">

    <!-- Displayed table row -->
    <tr onClick="editNote(this)"
        onMouseOut="noteGridMouseOut(this)"
        onMouseOver="noteGridMouseOver(this)" valign="top">

      <xsl:attribute name="idx">
        <xsl:value-of select="position()"/>
      </xsl:attribute>
      <xsl:attribute name="bgColor">
        <xsl:value-of select="user:chooseBackgroundColor(position(), 'fff7e5', 'ffffff' ) "/>
      </xsl:attribute>

      <!-- Supervisors get special text highlighting -->
      <xsl:choose>
        <xsl:when test="@CreatedUserSupervisorFlag='0'">
          <xsl:attribute name="CreatedUserSupervisorFlag">0</xsl:attribute>
          <xsl:attribute name="supervisor">false</xsl:attribute>
          <xsl:attribute name="style">color:#000000; font-weight:normal</xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="CreatedUserSupervisorFlag">1</xsl:attribute>
          <xsl:attribute name="supervisor">true</xsl:attribute>
          <xsl:attribute name="style">color:#0000FF; font-weight:bold</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

        <xsl:if test="@PrivateFlag = '1'">
          <xsl:attribute name="style">font-style:italic; font-color:#ff0000;</xsl:attribute>
        </xsl:if>

      <td id="PertainsTo" class="clWinGridTypeTD" align="center" valign="top">
        <xsl:if test="not(@ClaimAspectNumber=0)">
          <xsl:value-of select="@ClaimAspectNumber"/>
        </xsl:if>
        <xsl:if test="@ClaimAspectNumber=0">
          <xsl:value-of select="@ClaimAspectCode"/>
        </xsl:if>
      </td>

      <td id="ServiceChannelCD" class="clWinGridTypeTD" align="center" valign="top">
        <xsl:value-of select="@ServiceChannelCD"/>
      </td>

      <td id="CreatedDate" class="clWinGridTypeTD" align="center">
        <xsl:call-template name="formatDateTime">
          <xsl:with-param name="dateTime" select="@CreatedDate" />
        </xsl:call-template>
      </td>
      <td id="CreatedUser" class="clWinGridTypeTD" align="center">
        <xsl:value-of select="@CreatedUserNameLast"/>
        <xsl:if test="@CreatedUserNameFirst != ''">
          ,
          <xsl:value-of select="substring(@CreatedUserNameFirst,1,1)"/>.
        </xsl:if>
      </td>
      <td id="NoteTypeID" class="clWinGridTypeTD" align="center">
        <xsl:variable name="NoteTypeID"><xsl:value-of select="@NoteTypeID"/></xsl:variable>
        <xsl:value-of select="/Root/Reference[@List='NoteType'][@ReferenceID=$NoteTypeID]/@Name"/>
      </td>
      <td id="Note" class="clWinGridTypeTD" align="left" width="360">
        <xsl:call-template name="break"><xsl:with-param name="text" select="@Note"/></xsl:call-template>
      </td>
      <td id="DocumentID" class="clWinGridTypeTD" align="left" style="display:none"><xsl:value-of select="@DocumentID"/></td>
    </tr>

    <!-- Hidden note body popup table row -->
    <tr bgcolor="FFEEBB">
      <xsl:attribute name="id">noteBodyBottom<xsl:value-of select="position()"/></xsl:attribute>
      <xsl:attribute name="idx"><xsl:value-of select="position()"/></xsl:attribute>

      <!-- Supervisors get special text highlighting -->
      <xsl:choose>
        <xsl:when test="@CreatedUserSupervisorFlag='0'">
          <xsl:attribute name="style">color:#000000; font-weight:normal; display:none</xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="style">color:#3333ff; font-weight:bold; display:none</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <td class="clWinGridTypeTD" align="left" colspan="4">
        <xsl:call-template name="break"><xsl:with-param name="text" select="@Note"/></xsl:call-template>
      </td>
    </tr>
  </xsl:if>
</xsl:template>

  <xsl:template name="break">
    <!-- replaces 'carriage returns' in the 'text' parameter with hard-coded '<br/>'s -->
    <xsl:param name="text"/>
    <xsl:choose>
      <xsl:when test="contains($text, '||')">
        <xsl:value-of select="substring-before($text, '||')"/>
        <br/>
        <xsl:call-template name="break">
          <xsl:with-param name="text" select="substring-after($text,'||')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>
