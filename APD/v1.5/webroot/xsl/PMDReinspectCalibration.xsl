<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    id="PMDReinspectCalibration">

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">
	<xsl:param name="UserID"/>
	
<HTML>

<!--            APP _PATH                            SessionKey                   USERID                   SP                        XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDReinspectCalibrationGetDetailXML,PMDReinspectCalibration.xsl,777  -->

<HEAD>
  <TITLE>Reinspection Calibration</TITLE>
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
  <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
  </script>
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" bgcolor="#FFFAEB" style="margin-right:none">
 
<table border="0">
  <tr>
    <td valign="top">
	  <table unselectable="on" class="GridTypeTable" border="0" cellspacing="1" cellpadding="1" style="table-layout:fixed;">
	    <colgroup>
		  <col width="40"></col>
		  <col width="40"></col>
		  <col width="160"></col>
		  <col width="55"></col>
		  <col width="55"></col>
		  <col width="20"></col>
		  <col width="40"></col>
		  <col width="40"></col>
		  <col width="160"></col>
		  <col width="60"></col>
		  <col width="60"></col>
		</colgroup>
		<tr>
		  <td colspan="5" unselectable="on" class="TableSortHeader">Program Manager ReI</td>
		  <td unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		  <td colspan="5" unselectable="on" class="TableSortHeader">District Manager ReI</td>
		</tr>
		<tr>
	      <td unselectable="on" class="TableSortHeader">Est Item</td>
	      <td unselectable="on" class="TableSortHeader">Exc Code</td>
	      <td unselectable="on" class="TableSortHeader">Description</td>
	      <td unselectable="on" class="TableSortHeader">Est Amt</td>
	      <td unselectable="on" class="TableSortHeader">ReI Amt</td>
		  <td unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		  <td unselectable="on" class="TableSortHeader">Est Item</td>
	      <td unselectable="on" class="TableSortHeader">Exc Code</td>
	      <td unselectable="on" class="TableSortHeader">Description</td>
	      <td unselectable="on" class="TableSortHeader">Est Amt</td>
	      <td unselectable="on" class="TableSortHeader">ReI Amt</td>
		</tr>
	  </table>
	</td>
  </tr>
</table>

<div class="autoflowdiv" style="width:765px; height:300px;">
<table border="0">
  <tr>
    <td valign="top">
	  <table unselectable="on" class="GridTypeTable" border="0" cellspacing="1" cellpadding="1" style="table-layout:fixed;">
		<colgroup>
		  <col width="40"></col>
		  <col width="40"></col>
		  <col width="160"></col>
		  <col width="55"></col>
		  <col width="55"></col>
		  <col width="20"></col>
		  <col width="40"></col>
		  <col width="40"></col>
		  <col width="160"></col>
		  <col width="60"></col>
		  <col width="60"></col>
		</colgroup>
			    
		<xsl:for-each select="EstimateDetail">
		  <tr>
		    <xsl:call-template name="EstimateDetail">
		      <xsl:with-param name="SupFlag">0</xsl:with-param>
			</xsl:call-template>
			<td class="gridSpacerTD"></td>
			<xsl:call-template name="EstimateDetail">
		      <xsl:with-param name="SupFlag">1</xsl:with-param>
		    </xsl:call-template>
		  </tr>
		</xsl:for-each>
	  </table>
	</td>
  </tr>
  <tr>
 	<td valign="top">
	  <table cellpadding="0" cellspacing="0" border="0">
	    <tr>
		  <td>
	  <table unselectable="on" class="GridTypeTable" border="0" cellspacing="1" cellpadding="1" style="table-layout:fixed;">
	    <colgroup>
		  <col width="40"></col>
		  <col width="40"></col>
		  <col width="160"></col>
		  <col width="55"></col>
		  <col width="55"></col>
		</colgroup>
		
		<xsl:for-each select="//Reinspect[@ReinspectTypeCD != 'SUP']/ReinspectDetail[@ReinspectExceptionID = 'MD']"> 
		  <tr> 
	        <xsl:call-template name="Comparison"/>
		  </tr>
	    </xsl:for-each>
	  </table>
	</td>
	<td width="20"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	<td valign="top" cellpadding="0" cellspacing="0">
	  <table unselectable="on" class="GridTypeTable" border="0" cellspacing="1" cellpadding="1" style="table-layout:fixed;">
	    <colgroup>
		  <col width="40"></col>
		  <col width="40"></col>
		  <col width="160"></col>
		  <col width="60"></col>
		  <col width="60"></col>
		</colgroup>
	  	<xsl:for-each select="//Reinspect[@ReinspectTypeCD = 'SUP']/ReinspectDetail[@ReinspectExceptionID = 'MD']"> 
		  <tr> 
		    <xsl:call-template name="Comparison"/>
	      </tr>
        </xsl:for-each>
      </table>
	      </td>
	    </tr>
	  </table>
    </td>
  </tr>
  </table>
</div>
<hr/>
<IMG src="/images/spacer.gif" width="6" height="18" border="0" />

<table cellpadding="0" cellspacing="0" width="720px">
  <tr>
	<td>
	  <table border="0" align="right">
	    <tr>
		  <td class="labelColumn" nowrap="nowrap">Program Manager Percent Satisfactory:</td>
		  <td width="40" align="right"><xsl:value-of select="Reinspect[@ReinspectTypeCD != 'SUP']/@PercentSatisfactory"/></td>
		</tr>
		<tr>
		  <td class="labelColumn" nowrap="nowrap">District Manager Percent Satisfactory:</td>
		  <td width="40" align="right"><xsl:value-of select="Reinspect[@ReinspectTypeCD = 'SUP']/@PercentSatisfactory"/></td>
		</tr>
		<tr>
		  <td class="labelColumn" nowrap="nowrap">Variance:</td>
		  <td width="40" align="right">
		    <xsl:variable name="variance" select="Reinspect[@ReinspectTypeCD != 'SUP']/@PercentSatisfactory - Reinspect[@ReinspectTypeCD = 'SUP']/@PercentSatisfactory"/>
		    <xsl:choose>
			  <xsl:when test="$variance > 0">
			    <xsl:value-of select="round($variance * 10) div 10"/>
			  </xsl:when>
			  <xsl:otherwise>
			    <xsl:value-of select="round($variance * -10) div 10"/>
			  </xsl:otherwise>
			</xsl:choose>			
		  </td>
		</tr>
	  </table>
	</td>
  </tr>
</table>

</BODY>
</HTML>
</xsl:template>

<xsl:template name="EstimateDetail">  
  <xsl:param name="SupFlag"/>

  <xsl:variable name="Detail" select="@DetailNumber"/>
  <xsl:variable name="RDID" select="//Reinspect[@ReinspectTypeCD != 'SUP']/ReinspectDetail[@EstimateDetailNumber = $Detail]/@DetailNumber"/>
  <xsl:variable name="RDIDsup" select="//Reinspect[@ReinspectTypeCD = 'SUP']/ReinspectDetail[@EstimateDetailNumber = $Detail]/@DetailNumber"/>
	
  <xsl:if test="$RDID != '' or $RDIDsup != ''">
	<xsl:choose>
	  <xsl:when test="$SupFlag='0' and $RDID != ''">
	    <xsl:for-each select="//Reinspect[@ReinspectTypeCD != 'SUP']/ReinspectDetail[@EstimateDetailNumber = $Detail]">  
		  <xsl:call-template name="Comparison"/>
	    </xsl:for-each>
	  </xsl:when>
	  <xsl:when test="$SupFlag = '1' and $RDIDsup != ''">
	    <xsl:for-each select="//Reinspect[@ReinspectTypeCD = 'SUP']/ReinspectDetail[@EstimateDetailNumber = $Detail]">  
		  <xsl:call-template name="Comparison"/>
		</xsl:for-each>
	  </xsl:when>
	  <xsl:otherwise>
	    <td unselectable="on" class="GridTypeTD2"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	    <td unselectable="on" class="GridTypeTD2"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	    <td unselectable="on" class="GridTypeTD2"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	    <td unselectable="on" class="GridTypeTD2"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td unselectable="on" class="GridTypeTD2"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>	    
	  </xsl:otherwise>
	</xsl:choose>
  </xsl:if>  
</xsl:template>

<xsl:template name="Comparison">
    <td unselectable="on" class="GridTypeTD2">
	  <xsl:choose>
		<xsl:when test="@EstimateDetailNumber != '' and @EstimateDetailNumber != '0'">
		  <xsl:value-of select="@EstimateDetailNumber"/>
		</xsl:when>
		<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
	  </xsl:choose>
	</td>
	<td unselectable="on" class="GridTypeTD2">
	  <xsl:choose>
		<xsl:when test="@ReinspectExceptionID != ''">
		  <xsl:value-of select="@ReinspectExceptionID"/>
		</xsl:when>
		<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
	  </xsl:choose>
	</td>
	<td unselectable="on" class="GridTypeTD2" style="text-align:left;">
	  <xsl:choose>
		<xsl:when test="@Comment != ''">
		  <xsl:value-of select="@Comment"/>
		</xsl:when>
		<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
	  </xsl:choose>
	</td>
	<td unselectable="on" class="GridTypeTD2" style="text-align:right">
	  <xsl:choose>
		<xsl:when test="@EstimateAmt != ''">
		  <xsl:value-of select="@EstimateAmt"/>
		</xsl:when>
		<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
	  </xsl:choose>
	</td>
	<td unselectable="on" class="GridTypeTD2" style="text-align:right">
	  <xsl:choose>
		<xsl:when test="@ReinspectAmt != ''">
		  <xsl:value-of select="@ReinspectAmt"/>
		</xsl:when>
		<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
	  </xsl:choose>
	</td>
</xsl:template>

</xsl:stylesheet>