<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    id="APDMenu">

<!--
Creating Menus
The menus are built with tables. The table should have the class set to menu
and you should also set the cellspacing to 0.Each row in the table is treated as a menu item.
The attribute called href is used for the location that should be loaded when the item is clicked and
target is for the name of the designated window to load the location in.

To add an image to the menu item, Notice the class icon added to the TD containing the icon image.

Creating Sub Menus
Sub menus look exactly like normal subs. The difference is the menu item that associates the sub menu. The menu
item should have the class name set to 'sub' and you could either include a nested table or set an attribute
called menu to the id of the sub menu.

Notice below the TD with the class 'more' to create a right arrow that indicates that there is a sub menu.

Disabled and separators
To disable a menu item just set the class name to disabled.
A separator is a little bit trickier but once you have made one you can just copy the code.
-->

<xsl:template name="APDMenu">
  <xsl:param name="Desktop"/>
  <xsl:param name="Permission"/>
  
    
<!-- Menu bar starts here -->
<div id="menuBar" >
<table class="menuBar" onmouseover="menuBarOver()" onmouseout="menuBarOut()" onclick="menuBarClick()" cellspacing="1" cellpadding="0" unselectable="on">
<tr height="20">
  <xsl:if test="$Desktop!='CLM' and $Desktop != 'SHO' and $Desktop != 'DEA' and $Desktop != 'PGM'">
  <td unselectable="on">
    <table cellspacing="0" cellpadding="0" class="root" menu="desktopMenu">
    <tr unselectable="on">
      <td class="left" unselectable="on"></td>
      <td class="middle" unselectable="on">DeskTops</td>
      <td class="right" unselectable="on"></td>
    </tr>
    </table>
  </td>
  </xsl:if>
  <xsl:if test="$Desktop='CLM'">
  <td>
    <table cellspacing="0" cellpadding="0" class="root" menu="claimsMenu" unselectable="on">
    <tr unselectable="on">
      <td class="left" unselectable="on"></td>
      <td class="middle" unselectable="on">Claims</td>
      <td class="right" unselectable="on"></td>
    </tr>
    </table>
  </td>
  <td unselectable="on">
    <table cellspacing="0" cellpadding="0" class="root" menu="vehiclesMenu" unselectable="on">
    <tr unselectable="on">
      <td class="left" unselectable="on"></td>
      <td class="middle" unselectable="on">Vehicles</td>
      <td class="right" unselectable="on"></td>
    </tr>
    </table>
  </td>
  <xsl:if test="$ClaimView = 'exp'">
  <td unselectable="on">
    <table cellspacing="0" cellpadding="0" class="root" menu="propertiesMenu" unselectable="on">
    <tr>
      <td class="left" unselectable="on"></td>
      <td class="middle" unselectable="on">Properties</td>
      <td class="right" unselectable="on"></td>
    </tr>
    </table>
  </td>
  </xsl:if>
  <td unselectable="on">
    <table cellspacing="0" cellpadding="0" class="root" menu="documentsMenu" unselectable="on">
    <tr>
      <td class="left" unselectable="on"></td>
      <td class="middle" unselectable="on">Documents</td>
      <td class="right" unselectable="on"></td>
    </tr>
    </table>
  </td>
  <td unselectable="on">
    <table cellspacing="0" cellpadding="0" class="root" menu="financialMenu" unselectable="on">
    <tr unselectable="on">
      <td class="left" unselectable="on"></td>
      <td class="middle" nowrap="" unselectable="on">Financial</td>
      <td class="right" unselectable="on"></td>
    </tr>
    </table>
  </td>
  </xsl:if>  <!-- end of CLM exclusive menu items -->
  <xsl:if test="$Desktop='CRD' or $Desktop='UAD' or $Desktop='PMD'">
  <td unselectable="on">
    <table cellspacing="0" cellpadding="0" class="root" menu="reportsMenu" unselectable="on">
    <tr>
      <td class="left" unselectable="on"></td>
      <td class="middle" nowrap="" unselectable="on">Reports</td>
      <td class="right" unselectable="on"></td>
    </tr>
    </table>
  </td>
  </xsl:if>
  <!-- The 'and $Permission=1' that is grouped with $Desktop='PMD' below is included because there is currently only 1
       item on the admin menu in the PMD.  If more items are added that condition should be removed.  Note: Permission is already
       applied explicitly to the proper menu item so no further adjustments will be needed -->
  <xsl:if test="$Desktop='CRD' or $Desktop='SMT' or $Desktop='SHO' or ($Desktop='PMD' and $Permission=1)">
  <td unselectable="on">
    <table cellspacing="0" cellpadding="0" class="root" menu="adminMenu" unselectable="on">
    <tr unselectable="on">
      <td class="left" unselectable="on"></td>
      <td class="middle" nowrap="" unselectable="on">Admin</td>
      <td class="right" unselectable="on"></td>
    </tr>
    </table>
  </td>
  </xsl:if>
  <xsl:if test="$Desktop='CRD'">
  <!-- <td unselectable="on">
    <table id="preferencesRoot" cellspacing="0" cellpadding="0" class="root" menu="preferencesMenu" unselectable="on">
    <tr unselectable="on">
      <td class="left" unselectable="on"></td>
      <td class="middle" nowrap="" unselectable="on">Preferences</td>
      <td class="right" unselectable="on"></td>
    </tr>
    </table>
  </td> -->
  </xsl:if>
  <xsl:if test="$Desktop='MED'">
  <td unselectable="on">
    <table cellspacing="0" cellpadding="0" class="root" menu="MEClaimsMenu" unselectable="on">
    <tr unselectable="on">
      <td class="left" unselectable="on"></td>
      <td class="middle" nowrap="" unselectable="on">Claim</td>
      <td class="right" unselectable="on"></td>
    </tr>
    </table>
  </td>
  </xsl:if>
   <xsl:if test="$Desktop='TLQ'">
      <td unselectable="on">
         <table cellspacing="0" cellpadding="0" class="root" menu="TLClaimsMenu" unselectable="on">
            <tr unselectable="on">
               <td class="left" unselectable="on"></td>
               <td class="middle" nowrap="" unselectable="on">Claim</td>
               <td class="right" unselectable="on"></td>
            </tr>
         </table>
      </td>
   </xsl:if>
   <td unselectable="on">
    <table cellspacing="0" cellpadding="0" class="root" menu="HelpMenu" unselectable="on">
    <tr unselectable="on">
      <td class="left" unselectable="on"></td>
      <td class="middle" nowrap="" unselectable="on">Help</td>
      <td class="right" unselectable="on"></td>
    </tr>
    </table>
  </td>
  <td width="250" align="center" unselectable="on">
    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  </td>
<xsl:if test="$Desktop='CRD'">
  <td width="230" align="center" unselectable="on">
    <table border="0" cellspacing="0" cellpadding="0" unselectable="on">
    <tr unselectable="on">
      <td valign="middle" nowrap="" unselectable="on">
        <img src="/images/tb_search.gif" hspace="2" align="middle" title="Advanced Search" ondblclick="event.returnValue=false;" onclick="navigateTo('advSearch')" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"/>
      </td>
      <td valign="middle" nowrap="" unselectable="on">
        <xsl:text disable-output-escaping="yes">LYNX ID:</xsl:text>
      </td>
      <td valign="middle" nowrap="" unselectable="on">
        <INPUT type="text" name="txtSearchLynxID" class="InputField" size="10" onKeypress="if (event.keyCode==13) navigateTo('claim'); else NumbersOnly(event);" onkeydown="if (InvIdMsg.style.visibility == 'visible') fade(InvIdMsg,0);" allowDecimal="false" allowMinus="false" maxlength="15" default="true"/>
      </td>
      <td valign="middle" nowrap="" unselectable="on">
        <img src="/images/tb_claim.gif" hspace="2" align="middle" title="Go to Claim" ondblclick="event.returnValue=false;" onclick="navigateTo('claim');" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"/>
        <img src="/images/tb_vehicle.gif" hspace="2" align="middle" title="Go to Vehicle" ondblclick="event.returnValue=false;" onclick="navigateTo('vehicle');" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"/>
        <img src="/images/tb_property.gif" hspace="2" align="middle" title="Go to Property" ondblclick="event.returnValue=false;" onclick="navigateTo('property')" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"/>
        <img src="/images/tb_document.gif" hspace="2" align="middle" title="Go to Documents" ondblclick="event.returnValue=false;" onclick="navigateTo('document')" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"/>
	<img src="/images/tb_DotNetClaim.gif" hspace="0" align="middle" id="APDNET_Image"  title="Go to Claim New Page" ondblclick="event.returnValue=false;" onclick="navigateTo('claimNew');" style="FILTER:alpha(opacity=60); cursor:hand; visibility:hidden;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"/>
      </td>
    </tr>
    </table>
  </td>
</xsl:if>
</tr>
</table>
</div>


<!-- menus start here -->
<table id="desktopMenu" class="menu" cellspacing="0" cellpadding="0">
  <xsl:if test="User/DesktopPermission/@CRD = 1">
	<tr title="Claim Rep Desktop">
	  <xsl:attribute name="href">
	    <xsl:choose>
      <xsl:when test="$Desktop='CRD'">/ClaimRepDesk.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:when>
      <xsl:otherwise>/ApdSelect.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:otherwise>
		</xsl:choose>
	  </xsl:attribute>
	  <xsl:attribute name="target">
	    <xsl:choose>
		  <xsl:when test="$Desktop='CRD'">oIframe</xsl:when>
		  <xsl:otherwise>_top</xsl:otherwise>
		</xsl:choose>
	  </xsl:attribute>

		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Claim Representative</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  </xsl:if>

  <xsl:if test="User/DesktopPermission/@PMD = 1">
	<tr title="Program Manager Desktop">
	  <xsl:attribute name="href">
	    <xsl:choose>
		  <xsl:when test="$Desktop='PMD'">/ProgramMgr/PMDMain.asp</xsl:when>
		  <xsl:otherwise>/ProgramMgr/PMD.asp</xsl:otherwise>
		</xsl:choose>
	  </xsl:attribute>
	  <xsl:attribute name="target">
	    <xsl:choose>
		  <xsl:when test="$Desktop='PMD'">oIframe</xsl:when>
		  <xsl:otherwise>_top</xsl:otherwise>
		</xsl:choose>
	  </xsl:attribute>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Program Manager</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  </xsl:if>
  
  <xsl:if test="User/DesktopPermission/@SMT = 1">
	<tr title="Shop Maint. Desktop">
	  <xsl:attribute name="href">
	    <xsl:choose>
		  <xsl:when test="$Desktop='SMT'">/ProgramMgr/SMTMain.asp?PageID=SMT</xsl:when>
		  <xsl:otherwise>/ProgramMgr/SMT.asp</xsl:otherwise>
		</xsl:choose>
	  </xsl:attribute>
	  <xsl:attribute name="target">
	    <xsl:choose>
		  <xsl:when test="$Desktop='SMT'">oIframe</xsl:when>
		  <xsl:otherwise>_top</xsl:otherwise>
		</xsl:choose>
	  </xsl:attribute>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Shop Maintenance</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  </xsl:if>

  <xsl:if test="User/DesktopPermission/@UAD = 1">
	<tr href="/UAD.asp" target="" title="User Administration Desktop">
	  <xsl:attribute name="href">
	    <xsl:choose>
		  <xsl:when test="$Desktop='UAD'">/admin/UADMain.asp</xsl:when>
		  <xsl:otherwise>/UAD.asp</xsl:otherwise>
		</xsl:choose>
	  </xsl:attribute>
	  <xsl:attribute name="target">
	    <xsl:choose>
		  <xsl:when test="$Desktop='UAD'">ifrmUADMain</xsl:when>
		  <xsl:otherwise>_top</xsl:otherwise>
		</xsl:choose>
	  </xsl:attribute>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">User Administration</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  </xsl:if>

  <xsl:if test="User/DesktopPermission/@MED = 1">
	<tr title="Mobile Electronics Desktop" >
	  <xsl:attribute name="href">
    <xsl:choose>
		  <xsl:when test="$Desktop='MED'">/MED.asp</xsl:when>
		  <xsl:otherwise>/MED.asp</xsl:otherwise>
		</xsl:choose> 
	  </xsl:attribute>
	  <xsl:attribute name="target">_top</xsl:attribute>
	    <!-- <xsl:choose>
		  <xsl:when test="$Desktop='MED'">oIframe</xsl:when>
		  <xsl:otherwise>_top</xsl:otherwise>
		</xsl:choose>
	  </xsl:attribute> -->
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Mobile Electronics</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  </xsl:if>
   <xsl:if test="User/DesktopPermission/@TLQ = 1">
      <tr title="Total Loss Queue" >
         <xsl:attribute name="href">
            <xsl:choose>
               <xsl:when test="$Desktop='TLQ'">/TLQ.asp</xsl:when>
               <xsl:otherwise>/TLQ.asp</xsl:otherwise>
            </xsl:choose>
         </xsl:attribute>
         <xsl:attribute name="target">_top</xsl:attribute>
         <td class="left">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
         </td>
         <td class="middle" nowrap="">Total Loss Queue</td>
         <td class="right">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
         </td>
      </tr>
   </xsl:if>
</table>

<xsl:if test="$Desktop='CRD' or $Desktop='CLM'">
<table cellspacing="0" cellpadding="0"  class="menu" id="claimsMenu">
  <!-- <tr class="disabled" >
    <xsl:choose>
      <xsl:when test="$ClaimView = 'exp'">
        <xsl:attribute name="href">/ApdSelect.asp?WindowID=<xsl:value-of select="$WindowID"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>amp;LynxID=<xsl:value-of select="$LynxID"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>amp;Entity=claim<xsl:text disable-output-escaping="yes">&amp;</xsl:text>amp;ClaimView=cond</xsl:attribute>
        <xsl:attribute name="title">Standard Claim View</xsl:attribute>
        <xsl:attribute name="target">_top</xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="href">/ApdSelect.asp?WindowID=<xsl:value-of select="$WindowID"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>amp;LynxID=<xsl:value-of select="$LynxID"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>amp;Entity=claim<xsl:text disable-output-escaping="yes">&amp;</xsl:text>amp;ClaimView=exp</xsl:attribute>
        <xsl:attribute name="title">Expanded Claim View</xsl:attribute>
        <xsl:attribute name="target">_top</xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">
      <xsl:choose>
        <xsl:when test="$ClaimView = 'exp'">Standard View</xsl:when>
        <xsl:otherwise>Expanded View</xsl:otherwise>
      </xsl:choose>
    </td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr> -->
  <tr class="disabled" target="oIframe" title="Claim History">
    <xsl:attribute name="href">/ClaimHistory.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Claim History</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
	<!-- <tr class="disabled" href="javascript:ShowClaimReAssign('Claim');" title="Transfer claim ownership">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Transfer claim ownership</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr> -->
    <!--<tr class="disabled">
		<td colspan="3"><hr noshade="" size="1" color="#D3D3D3" width="100%"/></td>
	</tr>
    <tr class="disabled" href="javascript:ShowCloseExposureDialog();" title="Close Exposure">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Close Exposure</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
    <tr class="disabled" href="javascript:ShowReopenExposureDialog();" title="Reopen Exposure">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Reopen Exposure</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr class="disabled">
		<td colspan="3"><hr noshade="" size="1" color="#D3D3D3" width="100%"/></td>
	</tr>
    <tr class="disabled" href="javascript:ShowClaimCancelDialog();" title="Claim Cancel">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Claim Cancel</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
    <tr class="disabled" href="javascript:ShowClaimVoidDialog();" title="Claim Void">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Claim Void</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr> -->
</table>

<table cellspacing="0" cellpadding="0"  class="menu" id="vehiclesMenu">
  <!-- <tr class="disabled" target="oIframe" title="Vehicles in Claim List">
    <xsl:attribute name="href">/ClaimVehicleList.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
    <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="middle" nowrap="">Vehicle List</td>
  	<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr class="disabled" target="oIframe" title="Add a Vehicle to Claim">
    <xsl:attribute name="href">/ClaimVehicleInsert.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
    <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="middle" nowrap="">Add</td>
  	<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr class="disabled" href="javascript:VehicleDelete();" title="Remove the current Vehicle from Claim">
    <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="middle" nowrap="">Remove</td>
  	<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr class="disabled">
		<td colspan="3"><hr noshade="" size="1" color="#D3D3D3" width="100%"/></td>
	</tr>
	<tr class="disabled" href="javascript:ShowClaimReAssign('Vehicle');" title="Reassign the current vehicle">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Reassign</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr class="disabled">
		<td colspan="3"><hr noshade="" size="1" color="#D3D3D3" width="100%"/></td>
	</tr>
  <tr class="disabled" href="javascript:ShowCloseExposureDialog('Vehicle');" title="Close the current vehicle">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Close</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr class="disabled" href="javascript:ShowReopenExposureDialog();" title="Reopen the current vehicle">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Reopen</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr class="disabled">
		<td colspan="3"><hr noshade="" size="1" color="#D3D3D3" width="100%"/></td>
	</tr>
  <tr class="disabled" href="javascript:ShowEntityCancelDialog('vehicle');" title="Cancel the current vehicle">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Cancel</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr class="disabled" href="javascript:ShowEntityVoidDialog('vehicle');" title="Void the current vehicle">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Void</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr class="disabled">
		<td colspan="3"><hr noshade="" size="1" color="#D3D3D3" width="100%"/></td>
	</tr>
  <tr class="disabled" href="javascript:CloseServiceChannel();" title="Close Service Channel">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Close Service Channel</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr class="disabled" href="javascript:CancelServiceChannel();" title="Cancel Service Channel">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Cancel Service Channel</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr> -->
  <tr class="disabled" href="javascript:CompleteServiceChannel();" title="Complete Current Service Channel">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Complete Service Channel</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr class="disabled" href="javascript:CancelServiceChannel();" title="Cancel Current Service Channel">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Cancel Service Channel</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>   
  <tr class="disabled" href="javascript:ReactivateServiceChannel();" title="Reactivate Service Channel">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Reactivate Service Channel</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr class="disabled" href="javascript:ActivateWarranty();" title="Activate Warranty">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Activate Warranty</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr class="disabled">
		<td colspan="3"><hr noshade="" size="1" color="#D3D3D3" width="100%"/></td>
	</tr>
	<tr class="disabled" href="javascript:ShowClaimReAssign('Vehicle');" title="Reassign the current vehicle">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Reassign Vehicle</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr class="disabled" href="javascript:ShowEntityCancelDialog('vehicle');" title="Cancel the current vehicle">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Cancel Vehicle</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr class="disabled" href="javascript:ShowEntityVoidDialog('vehicle');" title="Void the current vehicle">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Void Vehicle</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>   
</table>

  <xsl:if test="$ClaimView = 'exp'">
<table cellspacing="0" cellpadding="0"  class="menu" id="propertiesMenu">
<tr class="disabled" target="oIframe" title="Property in Claim List">
  <xsl:attribute name="href">/ClaimPropertyList.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
	<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	<td class="middle" nowrap="">Property List</td>
	<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
</tr>
<tr class="disabled" target="oIframe" title="Add a Propterty to Claim">
  <xsl:attribute name="href">/ClaimPropertyInsert.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
  <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	<td class="middle" nowrap="">Add Property</td>
	<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
</tr>
<tr class="disabled" href="javascript:PropertyDelete();" title="Remove the current Property from Claim">
  <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	<td class="middle" nowrap="">Remove Property</td>
	<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
</tr>
</table>
  </xsl:if>

<table cellspacing="0" cellpadding="0" class="menu" id="documentsMenu">
  <tr class="disabled" href="javascript:ShowDocumentUploadDialog();" title="Attach a New Document">
    <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    <td class="middle" nowrap="">Attach Document</td>
    <td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr class="disabled" target="oIframe" title="Documents in Claim List">
    <xsl:attribute name="href">/ClaimDocument.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
    <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    <td class="middle" nowrap="">Document List</td>
    <td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr class="disabled" href="javascript:ShowDocumentGeneratorDialog();" title="Generate a New Document">
    <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    <td class="middle" nowrap="">Document Generator</td>
    <td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr class="disabled" href="javascript:ShowDocument2ClientDialog();" title="Send Documents to Client">
    <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    <td class="middle" nowrap="">Send Documents to Client</td>
    <td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr class="disabled" href="javascript:ShowCustomFormsDialog();" title="Create Custom Forms">
    <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    <td class="middle" nowrap="">Custom Forms</td>
    <td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
</table>

<table cellspacing="0" cellpadding="0"  class="menu" id="financialMenu">
  <tr class="disabled" target="oIframe" title="View and Edit Invoice details">
    <xsl:attribute name="href">/InvoiceDetail.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Invoice Details</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>  
  <tr class="disabled" target="oIframe" title="View Journal Transactions">
    <xsl:attribute name="href">/JournalDetail.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Journal</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>  
</table>
</xsl:if> <!-- end of CRD exclusive menu items -->

<xsl:if test="$Desktop='CRD' or $Desktop='UAD' or $Desktop='PMD'">
<table cellspacing="0" cellpadding="0"  class="menu" id="reportsMenu">
 <xsl:if test="$Desktop='CRD'">
  <!-- <tr target="oIframe" title="Claim Register Report">
    <xsl:attribute name="href">/reports/ClaimRegister.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Claim Register</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr> -->
	<!--<tr href="/reports/shopAssignment.asp" target="oIframe" title="Shop Assignment Report">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Shop Assignment</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>-->
  <!-- <tr target="oIframe" title="Open Billing Report">
    <xsl:attribute name="href">/reports/openBilling.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Open Billing</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr target="oIframe" title="Non-Billed Claim Report">
    <xsl:attribute name="href">/reports/nonBilledClaim.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Non-Billed Claim</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr target="oIframe" title="Imaging Summary Report">
    <xsl:attribute name="href">/reports/imagingSummary.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Imaging Summary</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr target="oIframe" title="Program Shop Report">
    <xsl:attribute name="href">/reports/programShop.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Program Shop Report</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr class="disabled">
		<td colspan="3"><hr noshade="" size="1" color="#D3D3D3" width="100%"/></td>
	</tr> -->
  <tr target="oIframe" title="Client Reports">
    <xsl:attribute name="href">/reports/clientReports.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Client Reports</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr target="oIframe" title="Operational Reports">
    <xsl:attribute name="href">/reports/OperationalReports.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Operational Reports</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr target="oIframe" title="Management Reports">
    <xsl:attribute name="href">/reports/ManagementReports.asp?WindowID=<xsl:value-of select="$WindowID"/></xsl:attribute>
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Management Reports</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
 </xsl:if>
	
 <xsl:if test="$Desktop='PMD'">
    <tr href="/reports/ProgramMgrReports.asp" target="oIframe" title="Program Manager Reports">
		  <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		  <td class="middle" nowrap="">Program Manager Reports</td>
		  <td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	  </tr>
 </xsl:if>
    
 <xsl:if test="$Desktop='UAD'">
	<tr href="/reports/SecurityAudit.asp" target="ifrmUADMain" title="Security Audit Report">
      <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
      <td class="middle" nowrap="">Security Audit Report</td>
      <td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    </tr>
	<tr href="/reports/ClmRepProfileVerify.asp" target="ifrmUADMain" title="Claim Rep. Profile Verification">
      <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
      <td class="middle" nowrap="">Claim Rep. Profile Verification</td>
      <td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
 </xsl:if>
</table>
</xsl:if>

<xsl:if test="$Desktop='CRD' or $Desktop='PMD' or $Desktop = 'SMT' or $Desktop = 'SHO'">
<table cellspacing="0" cellpadding="0"  class="menu" id="adminMenu">
 <xsl:if test="$Desktop='CRD'">
	<tr href="/admin/APDAdmin.asp" target="oIframe">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Data Administration</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
    <tr href="javascript:OpenWorkAssignment()">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Work Assignment</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr href="/admin/UnprocessedPartnerData.asp" target="oIframe">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Unprocessed Partner Data</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
 </xsl:if>
 
 <xsl:if test="$Desktop='SMT'">
	  <tr href="javascript:NavToShop('BusinessWiz')">
  	<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Add Business</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
	<tr href="javascript:NavToShop('DealerWiz')">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Add Dealer</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
  <tr href="/ProgramMgr/SMTWebSignupSrch.asp" target="oIframe">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Web Signups</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
 </xsl:if>
 
 <xsl:if test="($Desktop='SMT' or $Desktop='PMD') and $Permission=1">
   <tr href="javascript:window.showModalDialog('SMTTerritory.asp', '', 'dialogHeight:395px; dialogWidth:490px; resizable:no; status:no; help:no; center:yes;');">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td class="middle" nowrap="">Manage Territories</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
 </xsl:if>
 
 <xsl:if test="$Desktop='SHO'">
   <tr href="javascript:NavToShop('ShopWiz')" class="disabled">
  		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  		<td class="middle" nowrap="">Add Shop</td>
  		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	</tr>
 </xsl:if>
 
	<!-- I left these in the source so it would be easy to add back for dev debug
  <tr class="disabled">
  	<td nowrap="" colspan="3" style="height: 10px; padding: 0;">
  		<div style="border: 1px inset ; height: 2; overflow: hidden;"></div>
  	</td>
  </tr>
  <tr href="/admin/RefEstimatingPackages.asp" target="oIframe">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="middle" nowrap="">Shop Est. Packages</td>
  	<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr href="/admin/RefSpecialties.asp" target="oIframe">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="middle" nowrap="">Shop Specialties</td>
  	<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr class="disabled">
  	<td nowrap="" colspan="3" style="height: 10px; padding: 0;">
  		<div style="border: 1px inset ; height: 2; overflow: hidden;"></div>
  	</td>
  </tr>
  <tr href="/admin/RefNoteType.asp" target="oIframe">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="middle" nowrap="">Note Types</td>
  	<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr href="/admin/RefDocumentType.asp" target="oIframe">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="middle" nowrap="">Document Types</td>
  	<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr href="/admin/RefDocumentSource.asp" target="oIframe">
		<td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  	<td class="middle" nowrap="">Document Sources</td>
  	<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
-->
</table>
</xsl:if>

<xsl:if test="$Desktop='CRD' or $Desktop='CLM'">
<!-- <table cellspacing="0" cellpadding="0"  class="menu" id="preferencesMenu">
	<tr id="StdClaimView" href="javascript:setClaimViewPref(StdClaimView, 'ClaimView', 'cond')" >
		<td class="left">
      <xsl:choose>
        <xsl:when test="$ClaimView = 'exp'">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </xsl:when>
        <xsl:otherwise>
          <img src="/images/menuCheck.gif"/>
        </xsl:otherwise>
      </xsl:choose>
    </td>
		<td class="middle" nowrap="">Standard Claim View</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
	<tr id="ExpClaimView" href="javascript:setClaimViewPref(ExpClaimView, 'ClaimView', 'exp')" >
		<td class="left">
      <xsl:choose>
        <xsl:when test="$ClaimView = 'exp'">
          <img src="/images/menuCheck.gif"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </xsl:otherwise>
      </xsl:choose>
    </td>
		<td class="middle" nowrap="">Expanded Claim View</td>
		<td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
	</tr>
</table> -->
 </xsl:if>
 
<xsl:if test="$Desktop='MED'">
 <table cellspacing="0" cellpadding="0" class="menu" id="MEClaimsMenu">
 <tr href="JavaScript:SendComments()" title="Send Comments to Claim Rep.">
    <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    <td class="middle" nowrap="">Send Comments</td>
    <td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
</table>
</xsl:if>

<table cellspacing="0" cellpadding="0" class="menu" id="HelpMenu">
 <tr href="JavaScript:DisplayHelp()" title="Help">
    <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    <td class="middle" nowrap="">Help</td>
    <td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
  <tr href="JavaScript:AboutPopup()" title="About">
    <td class="left"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    <td class="middle" nowrap="">About</td>
    <td class="right"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
  </tr>
</table>

<!-- This is the right-click popup menu for the Diary/CheckList.
      It is used only as a template for the menu, as the menu items
      get modified when the popup is created but before they are shown. -->
<div id="checkListPopupMenu" style="display:none;">
  <table unselectable="on" cellspacing="1" cellpadding="0" width="180" height="66" style="border:1px solid #666666;table-layout:fixed;cursor:hand;font-family:verdana;font-size:11px;">
    <tr unselectable="on">
      <td unselectable="on" height="20" style="border:1pt solid #F9F8F7"
          onmouseover="this.style.background='#B6BDD2';this.style.borderColor='#0A246A';"
          onmouseout="this.style.background='#F9F8F7';this.style.borderColor='#F9F8F7';"
          onclick="CompleteOnClick">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <img src="/images/tasklist_action_grayscale.gif" border="0" hspace="0" vspace="0" align="absmiddle"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        Complete Task
      </td>
    </tr>
    <tr unselectable="on">
      <td unselectable="on" height="20" style="border:1pt solid #F9F8F7"
          onmouseover="this.style.background='#B6BDD2';this.style.borderColor='#0A246A';"
          onmouseout="this.style.background='#F9F8F7';this.style.borderColor='#F9F8F7';"
          onclick="EditOnClick">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <img src="/images/notepad_grayscale.gif" border="0" hspace="0" vspace="0" align="absmiddle"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        Edit Task
      </td>
    </tr>
    <tr unselectable="on">
      <td unselectable="on" height="20" style="border:1pt solid #F9F8F7"
          onmouseover="this.style.background='#B6BDD2';this.style.borderColor='#0A246A';"
          onmouseout="this.style.background='#F9F8F7';this.style.borderColor='#F9F8F7';"
          onclick="NavigateOnClick">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <img src="/images/navigate_image.gif" border="0" hspace="0" vspace="0" align="absmiddle"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        Navigate To
      </td>
    </tr>
  </table>
</div>
</xsl:template>

</xsl:stylesheet>
