<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="DocumentType">

<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/generic-template-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="InfoCRUD" select="Reference"/>

<xsl:template match="/Root">

<xsl:choose>
    <xsl:when test="contains($InfoCRUD, 'R')">
        <xsl:call-template name="mainPage">
            <xsl:with-param name="InfoCRUD"><xsl:value-of select="$InfoCRUD"/></xsl:with-param>
        </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
        <html>
        <head>
            <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
            <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
        </head>
        <body class="bodyAPDSub" unselectable="on" bgcolor="#FFFFFF">
        <center>
            <font color="#ff0000"><strong>You do not have sufficient permission to view this page.
            <br/>Please contact administrator for permissions.</strong></font>
        </center>
        </body>
        </html>
    </xsl:otherwise>
</xsl:choose>

</xsl:template>

<xsl:template name="mainPage">
<xsl:param name="InfoCRUD"/>
<HTML>

<HEAD>
<TITLE>Data Administration: Document Type</TITLE>
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="javascript" src="/js/apdcontrols.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<SCRIPT language="Javascript">
<![CDATA[

    var curRow = null;
    var gbDirty = false;
    var sCRUD = "____";
    var bRowSelecting = false;

    function pageInit(){
        initCRUD();
    }

    function initCRUD(){
        sCRUD = CPScrollTable.getAttribute("CRUD");
        if (sCRUD.indexOf("C") == -1){
            btnAdd.CCDisabled = true;
        }
        if (sCRUD.indexOf("U") == -1){
            btnAdd.CCDisabled = true;
            btnSave.CCDisabled = true;
        }
    }

    function curEnabledClick(){
        if (curRow){
            if (curRow.readOnly) return;
            if (curEnabledFlagDiv.disabled) return;

            if (curEnabledFlag.value == 1){
                curRow.cells[1].firstChild.firstChild.firstChild.src = "/images/cbcheckro.png";
                curRow.cells[1].firstChild.lastChild.value = "1";
            }
            else{
                curRow.cells[1].firstChild.firstChild.firstChild.src = "/images/cbreadonly.png";
                curRow.cells[1].firstChild.lastChild.value = "0";
            }
            var curAttr = curRow.getAttribute("rowAttr");
            if (curAttr != "new")
                curRow.rowAttr = "upd";
            gbDirty = true;
        }
    }

    function curEstimateClick(){
        if (curRow){
            if (curRow.readOnly) return;
            if (curEstimateFlagDiv.disabled) return;

            if (curEstimateFlag.value == 1){
                curRow.cells[2].firstChild.firstChild.firstChild.src = "/images/cbcheckro.png";
                curRow.cells[2].firstChild.lastChild.value = "1";
            }
            else{
                curRow.cells[2].firstChild.firstChild.firstChild.src = "/images/cbreadonly.png";
                curRow.cells[2].firstChild.lastChild.value = "0";
            }
            var curAttr = curRow.getAttribute("rowAttr");
            if (curAttr != "new")
                curRow.rowAttr = "upd";
            gbDirty = true;
        }
    }

    function txtCurNameChange(){
        if (curRow) {
            if (curRow.readOnly) return;

            //remove any trailing and leading spaces
            remove_XS_whitespace(txtCurName);
            if (txtCurName.value == "")
                curRow.cells[3].innerText = " "; //this will make sure that the cell has a border.
            else
                curRow.cells[3].innerText = txtCurName.value;

            var curAttr = curRow.getAttribute("rowAttr");
            if (curAttr != "new")
                curRow.rowAttr = "upd";
            gbDirty = true;
        }
    }

    function GridClick(oRow)
    {
    	try {
      if (!oRow) return;
      
      //if (curRow && curRow.rowAttr == "new") return;
      
    	//get the table name of the row that was clicked
    	var oTable = oRow.parentElement.parentElement;
    	//based on the table name update the form elements to the selectd row
    	if(oTable)
    	{
            bRowSelecting = true;
            resetRowCursor(oTable);

            oRow.cells[0].innerHTML = "<img src='/images/arrowright.gif'>"
            oRow.style.backgroundColor = "#FFD700";

            setMoveUpDown(oTable, oRow);
            curRow = oRow;

            disableCurData(false);
            clearCurData();

            txtName.value = oRow.cells[4].innerText.Trim();
            selClassCode.value = oRow.cells[9].innerText;
            chkEnabled.value = (oRow.cells[10].innerText == 1 ? 1 : 0);
            chkEstimate.value = (oRow.cells[11].innerText == 1 ? 1 : 0);
            txtName.contentSaved();
            selClassCode.contentSaved();
            chkEnabled.contentSaved();
            chkEstimate.contentSaved();
            
            if ((sCRUD.indexOf("U") == -1) || (oRow.readOnly)){
                disableCurData(true);
                return;
            }
            disableCurData(false);
            bRowSelecting = false;
    	}
      } catch (e) {alert(e.description)}
    }

    function clearCurData(){
        chkEnabled.value = 0;
        chkEstimate.value = 0;
        selClassCode.selectedIndex = -1;
        txtName.value = "";
    }

    function disableCurData(val){
        if (sCRUD.indexOf("U") == -1){
            btnSave.CCDisabled = true;
            val = true;
        }

        //curData.disabled = val;
        chkEnabled.CCDisabled = val;
        chkEstimate.CCDisabled = val;
        selClassCode.CCDisabled = val;
        txtName.CCDisabled = val;
    }

    function resetRowCursor(oTbl){
        var iRows = oTbl.rows.length;
        for (var i = 0; i < iRows; i++) {
            oTbl.rows[i].cells[0].innerHTML = "&nbsp;";
            oTbl.rows[i].style.backgroundColor = "#FFFFFF";
        }
    }

    function setMoveUpDown(oTbl, oRow){

        if (sCRUD.indexOf("U") == -1) return;

        var iRows = oTbl.rows.length;
        var idx = -1;
        for (var i = 0; i < iRows; i++) {
            if (oTbl.rows[i] == oRow){
                idx = i + 1;
                break;
            }
        }

        if (idx == 1 && idx < iRows){
            imgMoveUp.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity = 25)";
            imgMoveUp.disabled = true;
            imgMoveDown.style.filter = "FlipV progid:DXImageTransform.Microsoft.Alpha(opacity = 100)";
            imgMoveDown.disabled = false;
        }

        if (idx == iRows && idx > 0){
            imgMoveUp.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity = 100)";
            imgMoveUp.disabled = false;
            imgMoveDown.style.filter = "FlipV progid:DXImageTransform.Microsoft.Alpha(opacity = 25)";
            imgMoveDown.disabled = true;
        }

        if (idx > 1 && idx < iRows) {
            imgMoveUp.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity = 100)";
            imgMoveUp.disabled = false;
            imgMoveDown.style.filter = "FlipV progid:DXImageTransform.Microsoft.Alpha(opacity = 100)";
            imgMoveDown.disabled = false;
        }
    }

    function moveRow(dir){
    	//get the table name of the row that was clicked
        var oRow = curRow;
        var retRow = null;
    	  var oTable = oRow.parentElement.parentElement;
        var iRows = oTable.rows.length;
        var idx = -1;
        for (var i = 0; i < iRows; i++) {
            if (oTable.rows[i] == oRow){
                idx = i;
                break;
            }
        }

        switch (dir){
            case -1: // move up
                if (oTable.rows[idx].rowAttr != "new")
                    oTable.rows[idx].rowAttr = "upd";
                if (oTable.rows[idx - 1].rowAttr != "new")
                    oTable.rows[idx - 1].rowAttr = "upd";
                retRow = oTable.moveRow(idx, idx-1);
                break;
            case 1: // move down
                if (oTable.rows[idx].rowAttr != "new")
                    oTable.rows[idx].rowAttr = "upd";
                if (oTable.rows[idx + 1].rowAttr != "new")
                    oTable.rows[idx + 1].rowAttr = "upd";
                retRow = oTable.moveRow(idx, idx+1);
                break;
        }
        oRow.scrollIntoView(false);
        setMoveUpDown(oTable, oRow);
        btnSave.CCDisabled = false;
        gbDirty = true;
    }

    function btnAdd_onclick(){
        divButtons.disabled = true;
        var oNewRow = tblSort2.insertRow(-1);
        var oNewCell = null;
        for (i = 0; i < 12; i++){
            oNewCell = oNewRow.insertCell();
            oNewCell.innerHTML = "&nbsp;";
            oNewCell.className="GridTypeTD";
            if (i > 4){
                oNewCell.style.display = "none";
            }
        }
        oNewRow.unselectable="on";
        oNewRow.RowType = "DocumentSource";
        oNewRow.rowNo = oNewRow.rowIndex + 1;
        oNewRow.cells[5].innerText = oNewRow.rowIndex + 1;
        oNewRow.rowAttr = "new";
        oNewRow.onclick = new Function("", "GridClick(this)");
        oNewRow.style.height = "21px";
        oNewRow.style.cursor = "hand";
        oNewRow.cells[2].style.textAlign = "center";
        oNewRow.cells[3].style.textAlign = "center";
        oNewRow.cells[4].style.textAlign = "left";

        GridClick(oNewRow);
        oNewRow.scrollIntoView(false);
        divButtons.disabled = false;
    }

    function btnSave_onclick(){
    	var retArray = new Array;
      var bRefresh = false;

      //validate the rows
      var oRows = tblSort2.rows;
      for (var i = 0; i < oRows.length; i++){
        objRow = tblSort2.rows[i];
        if (!ValidateRow(objRow)) {
          GridClick(objRow);
          return;
        }
      }
      
      var aRequests = new Array();
      var sProc, sRequest;
      for (var i = 0; i < oRows.length; i++){
        if (oRows[i].rowAttr != "new" && oRows[i].rowAttr != "upd") continue;
        if (oRows[i].rowAttr == "new" || oRows[i].cells[6].innerText.Trim() == ""){
          sProc = "uspRefDocumentTypeInsDetail";
          sRequest = "DisplayOrder=" + (oRows[i].rowIndex + 1) + "&" +
                     "EnabledFlag=" + oRows[i].cells[10].innerText + "&" +
                     "EstimateTypeFlag=" + oRows[i].cells[11].innerText + "&" +
                     "DocumentClassCD=" + oRows[i].cells[9].innerText + "&" +
                     "Name=" + escape(oRows[i].cells[4].innerText) + "&" +
                     "EstimateType=" + oRows[i].cells[3].innerText + "&" +
                     "SysLastUserID=" + curUser;
        } else {
          sProc = "uspRefDocumentTypeUpdDetail";
          sRequest = "DocumentTypeID=" + oRows[i].cells[6].innerText + "&" +
                     "DisplayOrder=" + (oRows[i].rowIndex + 1) + "&" +
                     "Name=" + escape(oRows[i].cells[4].innerText) + "&" +
                     "DocumentClassCD=" + oRows[i].cells[9].innerText + "&" +
                     "EnabledFlag=" + oRows[i].cells[10].innerText + "&" +
                     "EstimateTypeFlag=" + oRows[i].cells[11].innerText + "&" +
                     "SysLastUserID=" + curUser + "&" +
                     "SysLastUpdatedDate=" + oRows[i].cells[8].innerText;
        }
        aRequests.push( { procName : sProc,
                          method   : "executespnpasxml",
                          data     : sRequest }
                      );
      }
      var objRet = XMLSave(makeXMLSaveString(aRequests));
      if (objRet.code == 0) {
          document.location.reload();
      } else 
        ServerEvent();
      
    }

    function ValidateRow(oRow) {
        if (oRow.cells[3].innerText.Trim() == "") {
            ClientWarning("Please select a Applies to from the list.");
            return false;
        }

        if (oRow.cells[4].innerText.Trim() == "") {
            ClientWarning("Name is required. Please enter a valid Name");
            return false;
        }
        return true;
    }
    
    function updateData(){
      if (curRow){
        if (bRowSelecting) return;
        curRow.cells[1].innerHTML = "<img src='" + (chkEnabled.value == 1 ? "/images/cbcheckro.png" : "/images/cbreadonly.png") + "'/>";
        curRow.cells[2].innerHTML = "<img src='" + (chkEstimate.value == 1 ? "/images/cbcheckro.png" : "/images/cbreadonly.png") + "'/>";
        curRow.cells[3].innerHTML = (selClassCode.selectedIndex != -1 ? selClassCode.text : "");
        curRow.cells[4].innerHTML = txtName.value;

        curRow.cells[9].innerText = (selClassCode.selectedIndex != -1 ? selClassCode.value : "");
        curRow.cells[10].innerText = chkEnabled.value;
        curRow.cells[11].innerText = chkEstimate.value;
        btnSave.CCDisabled = false;
        curRow.rowAttr = "upd";
        gbDirty = true;
      }
    }
	
	if (document.attachEvent)
      document.attachEvent("onclick", top.hideAllMenuScriptlets);
    else
      document.onclick = top.hideAllMenuScriptlets;
	
]]>

</SCRIPT>
</HEAD>
<BODY class="bodyAPDSub" unselectable="on" style="background-color:#FFFFFF;overflow:hidden" onload="pageInit();" topmargin="3px" leftmargin="8px" tabIndex="-1">
    <SPAN align="right" unselectable="on" id="divButtons" name="divButtons" style="position:absolute;top:3px;left:590px;">
      <IE:APDButton id="btnAdd" name="btnAdd" value="Add" CCDisabled="false" CCTabIndex="1" onButtonClick="btnAdd_onclick()"/>
      <IE:APDButton id="btnSave" name="btnSave" value="Save" CCDisabled="true" CCTabIndex="2" onButtonClick="btnSave_onclick()"/>
    </SPAN>
    <DIV align="right" style="height:18px;width:720px" unselectable="on">
        <img id="imgMoveUp" src="/images/arrowup.gif" disabled="" title="move up the display order" style="cursor:hand;filter:Gray progid:DXImageTransform.Microsoft.Alpha(opacity = 50)" onclick="moveRow(-1)"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<img id="imgMoveDown" src="/images/arrowup.gif" disabled="" title="move down the display order" style="cursor:hand;filter:FlipV Gray progid:DXImageTransform.Microsoft.Alpha(opacity = 50)" onclick="moveRow(1)"/>
    </DIV>
    <DIV id="CPScrollTable" style="width:100%;">
        <xsl:attribute name="CRUD"><xsl:value-of select="$InfoCRUD"/></xsl:attribute>
        <span >
            <TABLE unselectable="on" class="ClaimMiscInputs" width="720px" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
                <colgroup>
                    <col width="24px"/>
                    <col width="73px"/>
                    <col width="100px"/>
                    <col width="100px"/>
                    <col width="420px"/>
                </colgroup>
                <TR unselectable="on" class="QueueHeader" style="height:28px">
                    <TD unselectable="on" class="TableSortHeader" type="Number"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
                    <TD unselectable="on" class="TableSortHeader" type="Number"> Enabled </TD>
                    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Estimate </TD>
                    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Applies to </TD>
                    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Name </TD>
                </TR>
            </TABLE>
        </span>
        <DIV unselectable="on" class="autoflowTable" style="width: 720px; height: 314px;overflow:auto">
            <TABLE unselectable1="on" id="tblSort2" class="GridTypeTable" width="699px" cellspacing="1" border="0" cellpadding="3" style="table-layout:fixed;">
                <colgroup>
                    <col width="21px"/>
                    <col width="72px"/>
                    <col width="99px"/>
                    <col width="99px"/>
                    <col width="395px"/>
                    <col width="0px"/>
                    <col width="0px"/>
                    <col width="0px"/>
                    <col width="0px"/>
                </colgroup>
                <TBODY bgColor1="ffffff" bgColor2="fff7e5">
                    <xsl:for-each select="DocType"	>
                        <xsl:call-template name="DocTypeRows"></xsl:call-template>
                    </xsl:for-each>
                </TBODY>
            </TABLE>
        </DIV>
    </DIV>
    <br/>
    <DIV unselectable="on" style="border:1px solid gray;padding:5px;width=720px" id="curData" name="curData">
        <span unselectable="on" style="position:relative;top:-12px;left:10px; background-color:white;padding:5px;font:11px Tahoma,Arial,Verdana;font-weight:bold">Current Selection</span>
        <TABLE border="0" cellspacing="0" cellpadding="3" style="margin-left:15px;" unselectable="on">
          <colgroup>
            <col width="100px"/>
            <col width="250px"/>
          </colgroup>
            <TR>
                <TD unselectable="on" >
                  <IE:APDCheckBox id="chkEnabled" name="chkEnabled" caption="Enabled" value="0" CCDisabled="true" required="false" CCTabIndex="3" canDirty="false" onchange="updateData()"/>
                </TD>
                <TD unselectable="on" >
                  <IE:APDCheckBox id="chkEstimate" name="chkEstimate" caption="Document is an Estimate" value="0" CCDisabled="true" required="false" CCTabIndex="4" canDirty="false" onchange="updateData()"/>
                </TD>
            </TR>
            <TR>
                <TD unselectable="on" >Applies to:</TD>
                <TD unselectable="on" colspan="3" >
                  <IE:APDCustomSelect id="selClassCode" name="selClassCode" blankFirst="false" canDirty="false" CCDisabled="true" CCTabIndex="5" required="true" onchange="updateData()">
                  	<IE:dropDownItem value="C" style="display:none">Claim</IE:dropDownItem>
                    <IE:dropDownItem value="S" style="display:none">Shop</IE:dropDownItem>
                  </IE:APDCustomSelect>
                </TD>
            </TR>
            <TR>
                <TD unselectable="on" >Name</TD>
                <TD unselectable="on" colspan="3" >
                  <IE:APDInput id="txtName" name="txtName" value="" size="50" required="true" canDirty="false" CCDisabled="true" CCTabIndex="6" onchange="updateData()">
                    <xsl:attribute name="maxLength"><xsl:value-of select="/Root/Metadata/Column[@Name='Name']/@MaxLength"/></xsl:attribute>
                  </IE:APDInput>
                </TD>
            </TR>
        </TABLE>
    </DIV>
    <br/>
</BODY>
</HTML>
<!-- <SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT> -->
</xsl:template>
<xsl:template name="DocTypeRows">
    <TR unselectable1="on" RowType="DocumentType" onClick="GridClick(this)" style="height:21px;cursor:hand;"><!-- onMouseOut="GridMouseOut(this)" onMouseOver="GridMouseOver(this)" -->
        <xsl:attribute name="rowNo"><xsl:value-of select="position()"/></xsl:attribute>
        <xsl:if test="@SysMaintainedFlag = 1">
            <xsl:attribute name="readOnly">true</xsl:attribute>
        </xsl:if>
        <TD unselectable1="on" class="GridTypeTD" nowrap=""><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable1="on" class="GridTypeTD" nowrap="">
            <xsl:if test="@SysMaintainedFlag = 1">
                <xsl:attribute name="style">color:#A9A9A9;</xsl:attribute>
            </xsl:if>
            <xsl:choose>
              <xsl:when test="@EnabledFlag = '1'"><img src="/images/cbcheckro.png"/></xsl:when>
              <xsl:otherwise><img src="/images/cbreadonly.png"/></xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD unselectable1="on" class="GridTypeTD" style="text-align:center;">
            <xsl:choose>
              <xsl:when test="@EstimateTypeFlag = '1'"><img src="/images/cbcheckro.png"/></xsl:when>
              <xsl:otherwise><img src="/images/cbreadonly.png"/></xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD unselectable1="on" class="GridTypeTD" style="text-align:center;">
          <xsl:value-of select="/Root/Reference[@ListName='DocumentClassCD' and @ReferenceID = current()/@DocumentClassCD]/@Name"/>
        </TD>
        <TD unselectable1="on" class="GridTypeTD" style="text-align:left;">
            <xsl:if test="@SysMaintainedFlag = 1">
                <xsl:attribute name="style">color:#A9A9A9;text-align:left</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="@Name"/>
        </TD>
        <TD style="display:none;"><xsl:value-of select="@DisplayOrder"/></TD>
        <TD style="display:none;"><xsl:value-of select="@DocumentTypeID"/></TD>
        <TD style="display:none;"><xsl:value-of select="@SysLastUserID"/></TD>
        <TD style="display:none;"><xsl:value-of select="@SysLastUpdateDate"/></TD>
        <TD style="display:none;"><xsl:value-of select="@DocumentClassCD"/></TD>
        <TD style="display:none;"><xsl:value-of select="@EnabledFlag"/></TD>
        <TD style="display:none;"><xsl:value-of select="@EstimateTypeFlag"/></TD>
    </TR>
</xsl:template>


</xsl:stylesheet>
