<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="InvoiceDetail">

<xsl:import href="msxsl/msxsl-function-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="InfoCRUD" select="InfoCRUD"/>

  <!-- XslParams - stuffed from COM before transform -->
<xsl:param name="LynxId"/>
<xsl:param name="UserId"/>
<xsl:param name="VehicleNum"/>
<xsl:param name="WindowID"/>

  <!-- New -->
  <xsl:param name="ClaimStatus"/>
  <xsl:param name="InsuranceCompanyID"/>  
  
  <xsl:template match="/Root">

  <!-- xsl:variable name="Context" select="session:XslUpdateSession('Context',string(/Root/@Context))"/ -->
  <!-- xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/ -->
  <!-- xsl:variable name="InsuranceCompanyID" select="session:XslGetSession('InsuranceCompanyID')"/ -->

  <xsl:choose>
    <xsl:when test="contains($InfoCRUD, 'R')">
      <xsl:call-template name="mainPage">
        <xsl:with-param name="LynxId"><xsl:value-of select="$LynxId"/></xsl:with-param>
        <xsl:with-param name="UserId"><xsl:value-of select="$UserId"/></xsl:with-param>
		  <xsl:with-param name="VehicleNum"><xsl:value-of select="$VehicleNum"/></xsl:with-param>
        <xsl:with-param name="ClaimStatus"><xsl:value-of select="$ClaimStatus"/></xsl:with-param>
        <xsl:with-param name="InfoCRUD"><xsl:value-of select="$InfoCRUD"/></xsl:with-param>
        <xsl:with-param name="InsuranceCompanyID"><xsl:value-of select="$InsuranceCompanyID"/></xsl:with-param>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <html>
        <head>
          <LINK rel="stylesheet" href="/css/apdControls.css" type="text/css"/>
        </head>
        <body unselectable="on" bgcolor="#FFFFFF">
          <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
            <tr>
              <td align="center">
                <font color="#ff0000"><strong>You do not have sufficient permission to view the billing page.
                <br/>Please contact administrator for permissions.</strong></font>
              </td>
            </tr>
          </table>
        </body>
      </html>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="mainPage">
  <xsl:param name="LynxId"/>
  <xsl:param name="UserId"/>
  <xsl:param name="ClaimStatus"/>
 <xsl:param name="VehicleNum"/>
  <xsl:param name="InfoCRUD"/>
  <xsl:param name="InsuranceCompanyID"/>

<HTML>

<HEAD>

<TITLE></TITLE>
<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>


<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,36);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">

  var gsWindowID = "<xsl:value-of select="$WindowID"/>";
	var gsLynxID = "<xsl:value-of select="$LynxId"/>";
  var gsUserID = "<xsl:value-of select="$UserId"/>";
	var gsVehicleNum = "<xsl:value-of select="$VehicleNum"/>";
	var gsCSRNo = "<xsl:value-of select="/Root/@CSRNo"/>";
  var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";
  var oCurInvoiceRow = null;
  var sCRUD = "<xsl:value-of select="$InfoCRUD"/>";
  var gsDispatchNumber = "<xsl:value-of select="Invoice/@DispatchNumber"/>";
  var gsInsuranceCompanyID = "<xsl:value-of select="$InsuranceCompanyID"/>";
  var gsNoPaymentsWarningFlag = "<xsl:value-of select="@NoPaymentWarningFlag"/>";

    
  
  <![CDATA[
    function showHideGroup(objImg){
      if (tblInvoice && objImg && objImg.tagName == "IMG") {
        var sEntityGrp = objImg.getAttribute("EntityGrp");
        var sDisplay = "";
        if (objImg.src.indexOf("minus.gif") == -1){
          objImg.src = "/images/minus.gif";
          sDisplay = "inline";
        } else {
          objImg.src = "/images/plus.gif";
          sDisplay = "none";
        }
        if (sEntityGrp && sEntityGrp != "") {
          var iStartRow = objImg.parentElement.parentElement.rowIndex;
          var oRows = tblInvoice.rows;
          var iRowCount = oRows.length;
          for (var i = iStartRow + 1; i < iRowCount; i++){
            var sRowEntityGrp = oRows[i].getAttribute("EntityGrp");
            if (sRowEntityGrp && sRowEntityGrp != "" && sRowEntityGrp == sEntityGrp)
              oRows[i].style.display = sDisplay;
            else
              break;
          }
        }
      }
    }

    function showHideServices(objImg) {
      if (objImg) {
        var sDisplay = "";
        if (objImg.src.indexOf("minus.gif") == -1){
          objImg.src = "/images/minus.gif";
          sDisplay = "block";
        } else {
          objImg.src = "/images/plus.gif";
          sDisplay = "none";
        }

        var oDivServices = objImg.parentElement.lastChild;
        if (oDivServices)
          oDivServices.style.display = sDisplay;
      }
      event.cancelBubble = true;
    }

    function showInvoiceDetails(objRow){
      try {
        if (objRow) {
          var iInvoiceID = objRow.lastChild.innerText;
                    
          if (oCurInvoiceRow) {
            oCurInvoiceRow.style.backgroundColor = "#FFFFFF";            
          }
                    
          oCurInvoiceRow = objRow;
          oCurInvoiceRow.style.backgroundColor = "#FFD700";
          
          if (xmlInvoice) {
            var oInvoiceNode = xmlInvoice.selectSingleNode("/Root/Invoice[@InvoiceID=" + iInvoiceID + "]");
            if (oInvoiceNode) {
              var sItemTypeCD = oInvoiceNode.getAttribute("ItemTypeCD");
              var sRecordingUserName = oInvoiceNode.getAttribute("RecordingUserName");
              var sEntryDate = oInvoiceNode.getAttribute("EntryDate");
              var sStatusDesc = oInvoiceNode.getAttribute("StatusDesc");
              var sStatusDate = oInvoiceNode.getAttribute("StatusDate");
              var sDescription = oInvoiceNode.getAttribute("Description");
              var sAppearsOnInvoice = oInvoiceNode.getAttribute("ItemizeFlag");   
              var sClaimAspectID = oInvoiceNode.getAttribute("ClaimAspectID");

// 04Jun2019 TVD - Debugging
alert("sDescription: " + sDescription);


              var sAttributeValue="";
              var sInvoicingModelPayment = "";
              var sInvoiceMethod = "";
              var oNode;
              var sXPath;
              
              sEntryDate = formatSQLDateTimeAMPM(sEntryDate);
              sStatusDate = (sStatusDate == "1900-01-01T00:00:00" ? "" : formatSQLDateTimeAMPM(sStatusDate));               
              
              oNode = xmlInvoice.selectSingleNode("/Root");
              if (oNode){
                sAttributeValue = oNode.getAttribute("InvoiceMethodCD");    
                sInvoicingModelPayment = oNode.getAttribute("InvoicingModelPaymentCD");
              }
                                
              sXPath = "/Root/Reference[@List='InvoiceMethod' and @ReferenceID='" + sAttributeValue + "']";                                     
              oNode = xmlInvoice.selectSingleNode(sXPath);
              if (oNode)
                  sInvoiceMethod = oNode.getAttribute("Name");   
              
              
              if (sItemTypeCD == "F"){
                document.all.tabFeeDetail.style.display = "inline";
                document.all.tabPaymentDetail.style.display = "none";
                                
                var sFeeCategoryCD = oInvoiceNode.getAttribute("FeeCategoryCD");                
                switch (sFeeCategoryCD){
                  case "H":
                    txtCategory.value = "Handling Fee";
                    break;
                  case "A":
                    txtCategory.value = "Additional Fee";
                    break;
                  default:
                    txtCategory.value = "";
                    break;      
                }    
                           
                txtBillingUser.value = sRecordingUserName;
                txtItemize.value = (sAppearsOnInvoice == "0" ? "No" : "Yes");
                txtBillEntryDate.value = sEntryDate;
                txtPickupDate.value = sStatusDate;
                txtFeeStatus.value = sStatusDesc;
                
                txtInvoiceMethod.value = sInvoiceMethod;   
                  
                oNode = xmlInvoice.selectSingleNode("/Root/ClaimAspect[@ClaimAspectID = '" + sClaimAspectID + "']");                
                if (oNode)
                  sAttributeValue = oNode.getAttribute("InvoicingModelBillingCD");
                else
                  sAttributeValue = "";
                
                sXPath = "/Root/Reference[@List='InvoicingModel' and @ReferenceID='" + sAttributeValue + "']";          
                oNode = xmlInvoice.selectSingleNode(sXPath);
                if (oNode)
                  txtInvoiceModel.value = oNode.getAttribute("Name");
                  
                txtGenerate.value = txtInvoiceMethod.value == "Paper" && txtInvoiceModel.value == "Per Claim" ? "Yes" : "No"; 
              } 
              else{
                document.all.tabFeeDetail.style.display = "none";
                document.all.tabPaymentDetail.style.display = "inline";
                
                var sPayeeAddress1 = oInvoiceNode.getAttribute("PayeeAddress1");
                var sPayeeAddress2 = oInvoiceNode.getAttribute("PayeeAddress2");
                var sPayeeCity = oInvoiceNode.getAttribute("PayeeAddressCity");
                var sPayeeState = oInvoiceNode.getAttribute("PayeeAddressState");
                var sPayeeZip = oInvoiceNode.getAttribute("PayeeAddressZip");
                
                txtPayeeAddress1.value = sPayeeAddress1;
                txtPayeeAddress2.value = sPayeeAddress2;
                txtPayeeCity.value = sPayeeCity;
                txtPayeeState.value = sPayeeState;
                txtPayeeZip.value = sPayeeZip;
                txtPaymentStatus.value = sStatusDesc;
                txtPaymentUser.value = sRecordingUserName;
                txtDescription.value = sDescription;
                
                txtInvoiceMethodPayment.value = sInvoiceMethod;   
                               
                sXPath = "/Root/Reference[@List='InvoicingModel' and @ReferenceID='" + sInvoicingModelPayment + "']";          
                oNode = xmlInvoice.selectSingleNode(sXPath);
                if (oNode)
                  txtInvoiceModelPayment.value = oNode.getAttribute("Name");
                  
                txtGeneratePayment.value = txtInvoiceMethodPayment.value == "Paper" && txtInvoiceModelPayment.value == "Per Claim" ? "Yes" : "No";
              }             
            }
          }
        }
      } catch (e) {
        ClientError("An error occured while executing client side function showInvoiceDetails(): \nError description:" + e.description);
      } finally {
      }
    }

    function OnInvoiceMouseOver(objRow){
      /*if (objRow !=== curInvoiceRow)
        objRow.style.backgroundColor = "#FFF8DC"*/
    }

    function OnInvoiceMouseOut(objRow){
      /*if (objRow !=== curInvoiceRow)
        objRow.style.backgroundColor = "#FFFFFF"*/
    }

        
     function AddFee(sEntity){
      try {
        objRet = window.showModalDialog("/claimBillingAdd.asp?WindowID=" + gsWindowID + "&Entity=" + sEntity , null, "dialogHeight:320px;dialogWidth:705px;center:1;help:0;resizable:0;scroll:0;status:0")
        if (objRet == "OK"){
          document.location.reload();
        }
      } catch(e) {
        ClientError("An error occured while executing client side function AddBilling(" + sEntity + "): \nError description:" + e.description);
      } finally {
      }
    }
    
    function AddPayment(sEntity, sClaimAspectID){
      try {
        objRet = window.showModalDialog("/PaymentAdd.asp?WindowID=" + gsWindowID + "&Entity=" + sEntity + "&ClaimAspectID=" + sClaimAspectID , null,
                                        "dialogHeight:330px;dialogWidth:580px;center:1;help:0;resizable:0;scroll:0;status:0");
        if (objRet == "OK"){
          document.location.reload();
        }
      } 
      catch(e) {
        ClientError("An error occured while executing client side function AddPayment(" + sEntity + "): \nError description:" + e.description);
      } 
      finally {
      }
    }

 function ShowCustomFormsDialog(){
 
      var sDimensions = "height=470, width=800, top=54, left=108, "
      var sSettings = "resizable=no, status=no, help=no, center=yes"
      var sReturn = window.open("CustomForms.asp?WindowID=" + gsWindowID + "&LynxId=" + gsLynxID + "&UserId=" + gsUserID + "&VehNum=" + gsVehicleNum + "&InsuranceCompanyID=" + gsInsuranceCompanyID , "", sDimensions + sSettings);         
  }

    function InvoiceEdit(sEntity, iInvoiceID, sItemTypeCD, sClaimAspectID){
      event.cancelBubble = true;
      try {
        
        var sURL;
        var sStyle;
        
        if (sItemTypeCD == "F"){
          sURL = "/claimBillingEdit.asp?WindowID=" + gsWindowID + "&Entity=" + sEntity + "&InvoiceID=" + iInvoiceID;
          sStyle = "dialogHeight:330px;dialogWidth:705px;center:1;help:0;resizable:0;scroll:0;status:0";
        }
        else{
          sURL = "/PaymentAdd.asp?WindowID=" + gsWindowID + "&Entity=" + sEntity + "&InvoiceID=" + iInvoiceID + "&ClaimAspectID=" + sClaimAspectID;
          sStyle = "dialogHeight:350px;dialogWidth:580px;center:1;help:0;resizable:0;scroll:0;status:0";
        }
                                                      
        objRet = window.showModalDialog(sURL, null, sStyle);
        if (objRet == "OK"){
          document.location.reload();
        }
      } catch(e) {
        ClientError("An error occured while executing client side function InvoiceEdit(" + sEntity + ", " + iInvoiceID + "): \nError description:" + e.description);
      } finally {
      }
    }

    function InvoiceDelete(iInvoiceID, sDispatchNumber, iDispatchCount) {
      try {
        
        event.cancelBubble = true;
                      
                                       
        var sMsg;
        
        if (iDispatchCount > 1)
          //----------------------------------------------------//
		  // 29Jan2013 - TVD - Modifying the code to reflect    //
		  // that there are multiple invoices with this         //
		  // dispatch number, but only this one will be deleted //
          //----------------------------------------------------//
          //sMsg = "There are " + iDispatchCount + " invoice items on this Dispatch (" + sDispatchNumber + ").  These must be deleted as a group.  Do you want to delete these " + iDispatchCount + " invoice entries?";
		  sMsg = "There are " + iDispatchCount + " invoice items on this Dispatch (" + sDispatchNumber + ").  Do you still want to delete ONLY this invoice entry?";
        else
          sMsg = "Do you want to delete the invoice entry?";
          
        if (sDispatchNumber != '')
          sMsg += "\n\n The item(s) to be deleted have been sent to Financial Services.  Contact Financial Services to ensure these items are voided there as well."
        
        var sRet= YesNoMessage("Confirmation", sMsg);
          if (sRet != "Yes")
              return;
                       
        
        var sRequest = "";
        var sProc = "uspInvoiceDel";
        var sSysLastUpdatedDate = "";
        if (iInvoiceID > 0 && xmlInvoice) {
          var oInvoiceNode = xmlInvoice.selectSingleNode("/Root/Invoice[@InvoiceID='" + iInvoiceID + "']");
          if (oInvoiceNode)
            sSysLastUpdatedDate = oInvoiceNode.getAttribute("SysLastUpdatedDate");
        }


        sRequest = "InvoiceID=" + iInvoiceID +
                   "&DispatchNumber=" + sDispatchNumber +
                   "&UserID=" + gsUserID +
                   "&SysLastUpdatedDate=" + sSysLastUpdatedDate +
                   "&NotifyEvent=1";
        //alert(sRequest);
        var aRequests = new Array();;
        aRequests.push( { procName : sProc,
                          method   : "ExecuteSpNp",
                          data     : sRequest }
                      );
        var sXMLRequest = makeXMLSaveString(aRequests);
        var objRet = XMLSave(sXMLRequest);
        if (objRet && objRet.code == 0 && objRet.xml) {
          document.location.reload();
        }
      } catch (e) {
        ClientError("An error occured while executing client side function InvoiceDelete(" + iInvoiceID + ", " + iDispatchNumber + ", " + sDispatchCount + "): \nError description:" + e.description);
      }
    }
    
    function ReqestApproval(sClaimAspectID) {
      if (gsNoPaymentsWarningFlag == "1") {
        var sRet = YesNoMessage("Billing", "No payments have been made on a program shop estimate associated with this claim.  Do you wish to submit this for approval anyway?");
        if (sRet == "No")
          return false;
      }
     
          
      var i=1;
      
      try{        
        while (true)       
          eval("btnReqestApproval" + i++).disabled = true;                    
      }
      catch (e){}   
      
      var coObj = RSExecute("/rs/RSClaimBill.asp", "RequestApproval", sClaimAspectID, gsUserID);
    
    	if (coObj.status == -1){
    			ServerEvent();
    	}
    	else{
    		var retArray = coObj.return_value.split("||");
    		if (retArray[1] != "0")	{
          ClientWarning("Request Successful.");
          document.location.reload();
    		}
    		else {           
          ServerEvent();          
          try{        
            i=1;
            while (true)       
              eval("btnReqestApproval" + i++).disabled = false;                    
          }
          catch (e){}         
        }
    	}
    }
    
    
    function GenInvoice(sClaimAspectID) {
    
      if (gsNoPaymentsWarningFlag == "1") {
        var sRet = YesNoMessage("Billing", "No payments have been made on a program shop estimate associated with this claim.  Do you wish to generate the invoice anyway?");
        if (sRet == "No")
          return false;
      } else {
      	var sRet = YesNoMessage("Billing", "Do you want to generate Invoice?.\nNote: This may make take several minutes.");
      	if (sRet == "No")
      		return false;
      }
      
      if (sRet != "Yes")
        return;
          
     	var i=1;
      
      try{        
        while (true)       
          eval("btnGenInvoice" + i++).disabled = true;                    
      }
      catch (e){}   
      
      //var coObj = RSExecute("/rs/RSClaimBill.asp", "GenerateInvoice", gsDispatchNumber, gsInsuranceCompanyID, gsLynxID, sClaimAspectID, gsUserID);
      var coObj = RSExecute("/rs/RSClaimBill.asp", "GenerateInvoice", gsInsuranceCompanyID, sClaimAspectID, gsUserID);
      
    	if (coObj.status == -1){
    			ServerEvent();
    	}
    	else{
    		var retArray = coObj.return_value.split("||");
    		if (retArray[1] > "0"){
          ClientInfo("Invoice was generated successfully.");
          document.location.reload();
        }
        else if (retArray[1] < "0"){
          ClientInfo("No invoice is required due to no payment and a $0 fee for the billed service.");          
          //document.location.reload();
        }    		
    		else {
          try{        
            i=1;
            while (true)       
              eval("btnGenInvoice" + i++).disabled = false;                    
          }
          catch (e){}   
    			ServerEvent();
        }
    	}
     try{        
        i=1;
        while (true)       
          eval("btnGenInvoice" + i++).disabled = false;                    
      }
      catch (e){}   
    }
    
    function showDispatchDetails(str){
      if (document.getElementById("xmlDispatch" + str) === null){
         var oXML = document.createElement("XML");
         oXML.id = "xmlDispatch" + str;
         divDispatchXML.appendChild(oXML);
         getDispatchData(str);
         tblDispatchDetail.style.display = "none";
         tblDispatchDetail.dataSrc = "";
         divPleaseWait.style.display = "inline";
      } else {
         tblDispatchDetail.style.display = "inline";
         tblDispatchDetail.dataSrc = "#xmlDispatch" + str;
      }
      divDispatchDetails.style.display = "inline";
      spnDispatchNumber.innerText = str;
      centerDiv(divDispatchDetails);
    }
    
    function hideDispatchDetails(){
      divDispatchDetails.style.display = "none";
    }

   function getDispatchData(strDispatch){
      var sProc = "uspDispatchNumberGetDetailXML";
      var sRequest = "DispatchNumber=" + strDispatch;

      var sMethod = "ExecuteSpNpAsXML";
      
      var aRequests = new Array();
      aRequests.push( { procName : sProc,
                        method   : sMethod,
                        data     : sRequest }
                    );
      
      var sXMLRequest = makeXMLSaveString(aRequests);
      //alert(sXMLRequest); return;

      AsyncXMLSave(sXMLRequest, getDispatchDataSuccess, getDispatchDataFailure);
   }
   
   function getDispatchDataSuccess(objXML){
      var strProcResultXML = objXML.xml.selectSingleNode("//Result/Root").xml;
      var strDispatchNumber = spnDispatchNumber.innerText;
      if (strDispatchNumber != ""){
         tblDispatchDetail.style.display = "inline";
         document.getElementById("xmlDispatch" + strDispatchNumber).loadXML(strProcResultXML);
         tblDispatchDetail.dataSrc = "#xmlDispatch" + strDispatchNumber;
      } else {
      }
      divPleaseWait.style.display = "none";
   }
   
   function getDispatchDataFailure(objXML){
   }
    
   function centerDiv(obj){
         with (obj){
            style.top = (document.body.offsetHeight - obj.offsetHeight) / 2;
            style.left = (document.body.offsetWidth - obj.offsetWidth) / 2;
            //style.visibility = "visible";
            //style.display = "none";
            firstChild.style.filter = "";
         }
   }
   
   function showReleasePayment(sEntity, sClaimAspectID){
      try {
         /*var xmlData = new ActiveXObject("MSXML2.DOMDocument");
         xmlData.async = false;
         xmlData.loadXML("<Root/>");
         var objRoot = xmlData.selectSingleNode("/Root");
         if (objRoot){
            var objInvoices = xmlInvoice.selectNodes("/Root/Invoice[@ItemTypeCD='I' and @ClaimAspectID='" + sClaimAspectID + "']");
            for (var i = 0; i < objInvoices.length; i++){
               objRoot.appendChild(objInvoices[i].cloneNode(false));
            }
            var objPayee = xmlInvoice.selectNodes("/Root/Payee");
            for (var i = 0; i < objPayee.length; i++){
               objRoot.appendChild(objPayee[i].cloneNode(false));
            }
         }*/
         obj = {
            entity: sEntity,
            userID: gsUserID,
            claimAspectID: sClaimAspectID,
            xmlData: xmlInvoice
         }
        objRet = window.showModalDialog("/releasepayment.htm", obj, "dialogHeight:700px;dialogWidth:520px;center:1;help:0;resizable:0;scroll:0;status:0")
        if (objRet == "OK"){
          document.location.reload();
        }
      } catch(e) {
        ClientError("An error occured while executing client side function showReleasePayment(" + sEntity + "): \nError description:" + e.description);
      } finally {
      }
   }
    
	if (document.attachEvent)
      document.attachEvent("onclick", top.hideAllMenuScriptlets);
    else
      document.onclick = top.hideAllMenuScriptlets;
	
  ]]>
</SCRIPT>
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" style="background:transparent; border:0px; overflow:hidden;margin:0px;padding:5px;" onLoad="" tabIndex="-1">
    
  <xsl:call-template name="ExposureBilling">
    <xsl:with-param name="InfoCRUD"><xsl:value-of select="$InfoCRUD"/></xsl:with-param>
    <xsl:with-param name="ClaimStatus"><xsl:value-of select="$ClaimStatus"/></xsl:with-param>
    <xsl:with-param name="InsuranceCompanyID"><xsl:value-of select="$InsuranceCompanyID"/></xsl:with-param>
  </xsl:call-template>
    
  <xsl:call-template name="InvoiceDetail"/>
  <xml id="xmlInvoice" name="xmlInvoice"><xsl:copy-of select="/"/></xml>
  
  <div id="divDispatchDetails" style="position:absolute;top:0px;left:0px;height:160px;width:645px;overflow:auto;padding:3px;border:2px solid #FFA500;background-color:#F5F5DC;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#2F4F4F,strength=5);display:none" onclick="hideDispatchDetails()">
      <div class="TDLabel">Transaction Details for: <span id="spnDispatchNumber"></span></div>
      <table border="1" cellpadding="2" cellspacing="0" style="background-color:#FFFFFF;border-collapse:collapse;table-layout:fixed">
         <colgroup>
            <col width="80px"/>
            <col width="80px"/>
            <col width="200px"/>
            <col width="80px"/>
            <col width="80px"/>
            <col width="95px"/>
         </colgroup>
         <tr>
            <td class="TableSortHeader">Tx. Date</td>
            <td class="TableSortHeader">Code</td>
            <td class="TableSortHeader">Description</td>
            <td class="TableSortHeader">Disb. Date</td>
            <td class="TableSortHeader">Mode</td>
            <td class="TableSortHeader">Amount</td>
         </tr>
      </table>
      <div style="height:119px;overflow:auto;border:0px solid #FF0000">
         <div class="TDLabel" id="divPleaseWait" style="display:none">Please wait...</div>
         <table id="tblDispatchDetail" border="1" cellpadding="2" cellspacing="0" style="background-color:#FFFFFF;border-collapse:collapse;table-layout:fixed;" datasrc="">
            <colgroup>
               <col width="80px" style="text-align:center"/>
               <col width="80px" style="text-align:center"/>
               <col width="200px"/>
               <col width="80px" style="text-align:center"/>
               <col width="80px" style="text-align:center"/>
               <col width="95px" style="text-align:right"/>
            </colgroup>
            <tr>
               <td><div datafld="TransactionDate"/></td>
               <td><div datafld="TransactionDesc"/></td>
               <td><div datafld="Description"/></td>
               <td><div datafld="DisbursementDate"/></td>
               <td><div datafld="DisbursementMethodDesc"/></td>
               <td><div datafld="Amount"/></td>
            </tr>
         </table>
      </div>
   
  </div>
  
  <div id="divDispatchXML"/>
  <!-- <xml id="xmlData"/> -->
  
  <SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
  <SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
</BODY>
</HTML>
</xsl:template>

  
<xsl:template name="ExposureBilling">
  <xsl:param name="InfoCRUD"/>
  <xsl:param name="ClaimStatus"/>
  <xsl:param name="InsuranceCompanyID"/>
  
  <table border="0" cellspacing="1" cellpadding="2" style="border-collapse:collapse;table-layout:fixed">
    <colgroup>
      <col width="65px"/>
      <col width="180px"/>
      <col width="60px"/>
      <col width="60px"/>
      <col width="75px"/>
      <col width="75px"/>
      <col width="50px"/>
      <col width="75px"/>
      <col width="15px" style="display:none"/>
    </colgroup>
    <tr class="QueueHeader">
      <td class="TableSortHeader">
        <font color="white">
          <xsl:value-of select="RunTime"/>
        </font>  
      </td>
      <td class="TableSortHeader">Entity</td>
      <td class="TableSortHeader">Dispatch</td>
      <td class="TableSortHeader">Type</td>
      <td class="TableSortHeader">Amount</td>
      <td class="TableSortHeader">Entered</td>
      <td class="TableSortHeader">Stat</td>
      <td class="TableSortHeader">Stat Date</td>
      <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    </tr>
  </table>
 
  <div style="height:270px;overflow-x:hidden;overflow-y:auto;border:1px solid #C0C0C0;background-color:#FFFFFF">
    <table name="tblInvoice" id="tblInvoice" border="0" cellspacing="0" cellpadding="2" style="border-collapse:collapse;table-layout:fixed;">
      <colgroup>
        <col width="30px"/>
        <col width="15px"/>
        <col width="15px"/>
        <col width="250px"/>
        <col width="60px" style="text-align:center"/>
        <col width="60px" style="text-align:center"/>
        <col width="75px" style="text-align:right"/>
        <col width="75px" style="text-align:center"/>
        <col width="60px" style="text-align:center"/>
        <col width="65px" style="text-align:center"/>
      </colgroup>
      <tr style="height:21px">
        <td colspan="3"></td>
        <td colspan="3" style="font-weight:bold" unselectable="on">LYNX ID: <xsl:value-of select="@LynxID"/></td> 
        <td colspan="2"></td>         
        <td colspan="2"></td>
      </tr>
      
    <xsl:for-each select="ClaimAspect[@EntityCode != 'clm']">
      <xsl:variable name="EntityGrp"><xsl:value-of select="@EntityCode"/></xsl:variable>
      <xsl:variable name="ClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
      <xsl:variable name="InvoicingModelBillingCD"><xsl:value-of select="@InvoicingModelBillingCD"/></xsl:variable>
      <xsl:variable name="AssignmentTypeServiceChannelCD"><xsl:value-of select="@AssignmentTypeServiceChannelCD"/></xsl:variable>
      
      <xsl:variable name="ExpClosed">
        <xsl:choose>
          <!-- <xsl:when test="$ClaimStatus = 'Claim Closed' or $ClaimStatus = 'Claim Cancelled' or $ClaimStatus = 'Claim Voided' or @Status='Vehicle Closed'">true</xsl:when> -->
          <xsl:when test="@Status='Vehicle Closed'">true</xsl:when>
          <xsl:otherwise>false</xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      
      <tr style="height:28px;cursor:default">
        
        <td>
          <img src="/images/minus.gif" align="absmiddle" onclick="showHideGroup(this)" style="cursor:hand">
            <xsl:attribute name="EntityGrp"><xsl:value-of select="$EntityGrp"/></xsl:attribute>
          </img>
        </td>
        <td colspan="4"  style="font-weight:bold" unselectable="on">
          <xsl:value-of select="@Name"/>
          <xsl:variable name="ServiceChannelCD"><xsl:value-of select="@ServiceChannelCD"/></xsl:variable>
          <xsl:if test="@ServiceChannelCD != ''">
            - <xsl:value-of select="/Root/Reference[@List='ServiceChannelCD' and @ReferenceID = $ServiceChannelCD]/@Name"/>
          </xsl:if>
          <xsl:variable name="DispositionTypeCD"><xsl:value-of select="@DispositionTypeCD"/></xsl:variable>
          <xsl:if test="@DispositionTypeCD != ''">
            - <xsl:value-of select="/Root/Reference[@List='DispositionTypeCD' and @ReferenceID = $DispositionTypeCD]/@Name"/>
          </xsl:if>
          <xsl:if test="$ExpClosed='true'">
            <span style="color:red">
              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              VEH CLOSED
            </span>
          </xsl:if>
        </td>
        <td colspan="5" align="right">
        
          <xsl:variable name="DisplayAddPay">
            <xsl:choose>
              <!-- This is a Desk Audit (or Desk Review). -->
              <xsl:when test="@ServiceChannelCD='DA' or @ServiceChannelCD='DR' or @ServiceChannelCD='RRP'">false</xsl:when>
              
              <!-- Hide the addpay for choice shop claims -->
              <xsl:when test="/Root/ClaimAspect[@ClaimAspectID=$ClaimAspectID and @ServiceChannelCD='CS' and @AssignmentTypeServiceChannelCD='CS']">false</xsl:when>

              <!-- No Handling fee has been added. 
              <xsl:when test="count(//Invoice[@ClaimAspectID=$ClaimAspectID and @ItemTypeCD='F' and @FeeCategoryCD='H'])=0">false</xsl:when>-->
              
              <!-- Default -->
              <xsl:otherwise>true</xsl:otherwise>              
            </xsl:choose>
          </xsl:variable>
          
          <xsl:variable name="DisplayAddFee">
            <xsl:choose>
              <!-- No Handling fee has been added. -->
              <xsl:when test="count(//Invoice[@ClaimAspectID=$ClaimAspectID and @ItemTypeCD='F' and @FeeCategoryCD='H'])=0">true</xsl:when>
              
              <!-- There are applicable additional fees that have not been added. -->
              <xsl:when test="count(/Root/Reference[@List='ClientFee' and @CategoryCD='A' and (@ReferenceID = /Root/Reference[@List='ClientFeeDefinition' and (@ServiceID = /Root/Reference[@List='Service' and @ServiceChannelCD = $AssignmentTypeServiceChannelCD and (@ReferenceID = /Root/Invoice[@ClaimAspectID=$ClaimAspectID]/InvoiceService/@ServiceID)]/@ReferenceID)]/@ReferenceID)])">true</xsl:when>   
              
                                                                                               
              <!-- There are applicable additional fees that allow multiple billing. -->
              <xsl:when test="1=1">true</xsl:when>
              
              <!-- Default -->
              <xsl:otherwise>false</xsl:otherwise>
            </xsl:choose>
          </xsl:variable>

          <xsl:variable name="DisplayAddFee1">
                <xsl:choose>
                  <xsl:when test="/Root/ClaimAspect[@ClaimAspectID=$ClaimAspectID and @ServiceChannelCD='CS' and @AssignmentTypeServiceChannelCD='CS']">false</xsl:when>
                  <xsl:otherwise>true</xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
      
          
          <xsl:if test="contains($InfoCRUD, 'C')">
            <xsl:if test="$DisplayAddFee1='true'">
            <IE:APDButton value="Add Fee" width="60">
              <xsl:attribute name="id"><xsl:value-of select="concat('btnFeeAdd_', string(@EntityCode))"/></xsl:attribute>
              <xsl:attribute name="name"><xsl:value-of select="concat('btnFeeAdd_', string(@EntityCode))"/></xsl:attribute>
              <xsl:variable name="sQuote">"</xsl:variable>
              <xsl:attribute name="onButtonClick">AddFee("<xsl:value-of select="@EntityCode"/>", <xsl:value-of select="@ClaimAspectID"/>)</xsl:attribute>
              <xsl:if test="$ClaimStatus = 'Claim Closed' or $ClaimStatus = 'Claim Cancelled' or $ClaimStatus = 'Claim Voided' or @Status='Vehicle Closed'">
                <xsl:attribute name="CCDisabled">true</xsl:attribute>
                <xsl:attribute name="title">
                  <xsl:choose>
                     <xsl:when test="$ClaimStatus = 'Claim Closed' or $ClaimStatus = 'Claim Cancelled' or $ClaimStatus = 'Claim Voided'">
                        <xsl:value-of select="concat($ClaimStatus, '. Cannot add fee')"/>
                     </xsl:when>
                     <xsl:when test="@Status='Vehicle Closed'">
                        <xsl:value-of select="concat(@Status, '. Cannot add fee')"/>
                     </xsl:when>
                  </xsl:choose>
                </xsl:attribute>
              </xsl:if>  
            </IE:APDButton>  
           </xsl:if>
            
            <xsl:if test="$DisplayAddPay='true'">
              <IE:APDButton value="Add Pay"  width="60">
                <xsl:attribute name="id"><xsl:value-of select="concat('btnPaymentAdd_', string(@EntityCode))"/></xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="concat('btnPaymentAdd_', string(@EntityCode))"/></xsl:attribute>
                <xsl:variable name="sQuote">"</xsl:variable>
                <xsl:attribute name="onButtonClick">AddPayment("<xsl:value-of select="@EntityCode"/>", <xsl:value-of select="@ClaimAspectID"/>)</xsl:attribute>
                <xsl:if test="$ClaimStatus = 'Claim Closed' or $ClaimStatus = 'Claim Cancelled' or $ClaimStatus = 'Claim Voided' or @Status='Vehicle Closed'">
                  <xsl:attribute name="CCDisabled">true</xsl:attribute>
                <xsl:attribute name="title">
                  <xsl:choose>
                     <xsl:when test="$ClaimStatus = 'Claim Closed' or $ClaimStatus = 'Claim Cancelled' or $ClaimStatus = 'Claim Voided'">
                        <xsl:value-of select="concat($ClaimStatus, '. Cannot add payment')"/>
                     </xsl:when>
                     <xsl:when test="@Status='Vehicle Closed'">
                        <xsl:value-of select="concat(@Status, '. Cannot add payment')"/>
                     </xsl:when>
                  </xsl:choose>
                </xsl:attribute>
                </xsl:if>
              </IE:APDButton>
            </xsl:if>
          </xsl:if>
        <!-- </td>
        
        <td colspan="2">                         -->
           
          <!-- This variable encapsulates all business rules pertaining to whether or not the 'Generate Invoice' button should be displayed -->                     
          <xsl:variable name="curDisposition"><xsl:value-of select="/Root/ClaimAspect[@ClaimAspectID=$ClaimAspectID]/ClaimAspectServiceChannel[@ServiceChannelCD='PS']/@DispositionCD"/></xsl:variable>
         
          <xsl:variable name="DisplayButton">
            <xsl:choose>              
              <!-- We have received estimates for this PS vehicle but have not made any payments. 
              <xsl:when test="@NoPaymentWarningFlag = '1'">false</xsl:when>-->
                                          
              <!-- All items have been processed (i.e. there are no items for this vehicle with a status of 'APD'). -->
              <!-- <xsl:when test="count(/Root/Invoice[@ClaimAspectID=$ClaimAspectID and @StatusCD='APD'])=0">false</xsl:when> -->
              <xsl:when test="count(/Root/Invoice[@ClaimAspectID=$ClaimAspectID and ((@ItemTypeCD='I' and @StatusCD='APD') or (@ItemTypeCD='F' and @StatusCD='APD' and $InvoicingModelBillingCD != 'B'))])=0">false</xsl:when>
              
              <!-- The service channel of at least 1 invoiced service does not match the service channel of the current assignment type -->
              <xsl:when test="count(/Root/Invoice[@ClaimAspectID=$ClaimAspectID]/InvoiceService[@ServiceChannelCD != '1P' and @ServiceChannelCD != '3P' and @ServiceChannelCD != '' and @ServiceChannelCD != $AssignmentTypeServiceChannelCD]) &gt; 0">false</xsl:when>
                             
              <!-- Early Billing -->
              <xsl:when test="/Root/@EarlyBillFlag = '1' and @ServiceChannelCD = 'PS' and ($curDisposition = 'TL' or $curDisposition = 'CO' or $curDisposition = 'ST')">true</xsl:when>
              <xsl:when test="/Root/@EarlyBillFlag = '1' and @ServiceChannelCD = 'PS' and (@AgreedEstimateCount = '0' and @ApprovedDocumentCount = '0')">false</xsl:when>
              <xsl:when test="/Root/@EarlyBillFlag = '1' and @ServiceChannelCD = 'PS' and @WorkStartConfirmFlag != '1'">false</xsl:when>

              <!-- At least one fee or indemnity has been billed for this vehicle. -->
              <xsl:when test="count(/Root/Invoice[@ClaimAspectID = $ClaimAspectID and (@ItemTypeCD='F' or @ItemTypeCD='I' or @ItemTypeCD='E')]) &gt; 0">
                  
                  <xsl:choose>
                    
                    <!-- This vehicle is subject to claim invoicing -->
                    <xsl:when test="@InvoicingModelBillingCD='C'">true</xsl:when>
                    
                    <!-- Payments have been made for this vehicle and we bill payments on a per claim basis for this client.  -->
                    <xsl:when test="(count(/Root/Invoice[@ClaimAspectID = $ClaimAspectID and @ItemTypeCD != 'F']) &gt; 0) and /Root/@InvoicingModelPaymentCD='C'">true</xsl:when>
                    
                    <!-- Default -->                      
                    <xsl:otherwise>false</xsl:otherwise>
                  </xsl:choose>  
              </xsl:when>
              <!-- Default -->
              <xsl:otherwise>false</xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <xsl:variable name="DisplayButtonReason">
            <xsl:choose>
              <!-- We have received estimates for this PS vehicle but have not made any payments.
              <xsl:when test="@NoPaymentWarningFlag = '1'">false</xsl:when>-->

              <!-- All items have been processed (i.e. there are no items for this vehicle with a status of 'APD'). -->
              <xsl:when test="count(/Root/Invoice[@ClaimAspectID=$ClaimAspectID and @StatusCD='APD'])=0">No items in APD status</xsl:when>

              <!-- The service channel of at least 1 invoiced service does not match the service channel of the current assignment type -->
              <xsl:when test="count(/Root/Invoice[@ClaimAspectID=$ClaimAspectID]/InvoiceService[@ServiceChannelCD != '1P' and @ServiceChannelCD != '3P' and @ServiceChannelCD != '' and @ServiceChannelCD != $AssignmentTypeServiceChannelCD]) &gt; 0">
              		The service channel following invoiced service(s) does not match the service channel of the current assignment type (<xsl:value-of select="$AssignmentTypeServiceChannelCD"/>):
              		<xsl:for-each select="/Root/Invoice[@ClaimAspectID=$ClaimAspectID]/InvoiceService[@ServiceChannelCD != '1P' and @ServiceChannelCD != '3P' and @ServiceChannelCD != '' and @ServiceChannelCD != $AssignmentTypeServiceChannelCD]">
              			<xsl:variable name="ServiceID"><xsl:value-of select="@ServiceID"/></xsl:variable>
              			<xsl:value-of select="concat(/Root/Reference[@List='Service' and @ReferenceID=$ServiceID]/@Name, '[', /Root/Reference[@List='Service' and @ReferenceID=$ServiceID]/@ServiceChannelCD, ']')"/>
              			<xsl:if test="position() != last()">,</xsl:if>
              		</xsl:for-each>
              		
              </xsl:when>

              <!-- Early Billing -->
              <xsl:when test="/Root/@EarlyBillFlag = '1' and @ServiceChannelCD = 'PS' and (@AgreedEstimateCount = '0' and @ApprovedDocumentCount = '0')">
   No estimate with Price Agreed or No estimate has been approved for Early billing.
              </xsl:when>
              <xsl:when test="/Root/@EarlyBillFlag = '1' and @ServiceChannelCD = 'PS' and @WorkStartConfirmFlag != '1'">
   Repair Start Date has not been confirmed.
              </xsl:when>

              <!-- At least one fee or indemnity has been billed for this vehicle. -->
              <xsl:when test="count(/Root/Invoice[@ClaimAspectID = $ClaimAspectID and (@ItemTypeCD='F' or @ItemTypeCD='I' or @ItemTypeCD='E')]) &gt; 0">

                  <xsl:choose>

                    <!-- This vehicle is subject to claim invoicing -->
                    <xsl:when test="@InvoicingModelBillingCD='C'"></xsl:when>

                    <!-- Payments have been made for this vehicle and we bill payments on a per claim basis for this client.  -->
                    <xsl:when test="(count(/Root/Invoice[@ClaimAspectID = $ClaimAspectID and @ItemTypeCD != 'F']) &gt; 0) and /Root/@InvoicingModelPaymentCD='C'"></xsl:when>

                    <!-- Default -->
                    <xsl:otherwise>Unknown reason 1</xsl:otherwise>
                  </xsl:choose>
              </xsl:when>

              <!-- Default -->
              <xsl:otherwise>Unknown reason 2</xsl:otherwise>
            </xsl:choose>
          </xsl:variable>                             
          <xsl:variable name="InvoiceMethodCD"><xsl:value-of select="/Root/@InvoiceMethodCD"/></xsl:variable>
          <xsl:variable name="ServiceChannelCDLocal"><xsl:value-of select="/Root/ClaimAspect[@ClaimAspectID=$ClaimAspectID]/ClaimAspectServiceChannel/@ServiceChannelCD"/></xsl:variable>
          <!--<IE:APDLabel id="txtPayeeZip3" name="txtPayeeZip3" width="285" >
            <xsl:attribute name='titles'>
              <xsl:value-of  select='$ClaimAspectID'/>              
              
            </xsl:attribute>            
          </IE:APDLabel>-->
          <!--<xsl:variable name="InvoiceCount">
            count(/Root/Invoice[@ClaimAspectID=$ClaimAspectID and ((@ItemTypeCD='I' and @StatusCD='APD') or (@ItemTypeCD='F' and @StatusCD='APD' and $InvoicingModelBillingCD != 'B'))])=0
            <xsl:value-of select="count(/Root/Invoice[@ClaimAspectID=$ClaimAspectID/ClaimAspectServiceChannel/@ServiceChannelCD]"/>
          </xsl:variable> -->
 	
			
          <xsl:if test="$ServiceChannelCDLocal='RRP' and $InsuranceCompanyID='304' " >
            <xsl:if test="count(/Root/Invoice[@ClaimAspectID=$ClaimAspectID ]) &gt; 0" >
            <IE:APDButton value="Gen. Memo Bill"  width="100"  CCTabIndex="">
				<xsl:attribute name="id">
					<xsl:value-of select="concat('btnMemoBill_', string(@EntityCode))"/>
				</xsl:attribute>
				<xsl:attribute name="name">
					<xsl:value-of select="concat('btnMemoBill_', string(@EntityCode))"/>
				</xsl:attribute>
              <xsl:attribute name="onButtonClick">
                ShowCustomFormsDialog()
              </xsl:attribute>
            </IE:APDButton>
            </xsl:if>
          </xsl:if>
          
          <!-- DisplayButton encapsulates rules that apply to both the 'Generate Invoice' and 'Request Approval' buttons -->           
          <xsl:if test="$DisplayButton='true'">
            <xsl:choose>
              <!-- Generate Invoice displays when Client does not authorize payments and prefers Paper invoicing. -->
              <xsl:when test="@ClientAuthorizesPaymentFlag=0 and $InvoiceMethodCD='P'">     
                <IE:APDButton name="btnGenInvoice" value="Gen. Invoice" width="85">
                  <xsl:attribute name="id">btnGenInvoice<xsl:value-of select="position()"/></xsl:attribute>
                  <xsl:attribute name="onButtonClick">GenInvoice(<xsl:value-of select="@ClaimAspectID"/>)</xsl:attribute>              
                  <xsl:if test="$ClaimStatus = 'Claim Closed' or $ClaimStatus = 'Claim Cancelled' or $ClaimStatus = 'Claim Voided'">
                    <xsl:attribute name="CCDisabled">true</xsl:attribute>                    
                      <xsl:attribute name="title">
                        <xsl:value-of select="concat($ClaimStatus, '. Cannot Generate Invoice.')"/>
                      </xsl:attribute>
                  </xsl:if>
                </IE:APDButton>                
              </xsl:when>
                            
              <!-- Request Approval displays when Client authorizes payments. -->
              <xsl:when test="@ClientAuthorizesPaymentFlag=1">
                
                <!-- TODO: Fix this.  Temporary hack for OCG until we sort out the (possibly) evolving file format for electronic billing. -->
                <xsl:if test="@DispositionTypeCD='RC' and count(/Root/Invoice[@ItemTypeCD != 'F']) &gt; 0">
                
                  <IE:APDButton name="btnRequestApproval" value="Req. Approval" width="85">
                    <xsl:attribute name="id">btnReqestApproval<xsl:value-of select="position()"/></xsl:attribute>
                    <xsl:attribute name="onButtonClick">ReqestApproval(<xsl:value-of select="@ClaimAspectID"/>)</xsl:attribute>              
                    <xsl:if test="$ClaimStatus = 'Claim Closed' or $ClaimStatus = 'Claim Cancelled' or $ClaimStatus = 'Claim Voided'">
                      <xsl:attribute name="CCDisabled">true</xsl:attribute>
                      <xsl:attribute name="title">
                        <xsl:value-of select="concat($ClaimStatus, '. Cannot Request approval.')"/>
                      </xsl:attribute>
                    </xsl:if>
                  </IE:APDButton>
                
                </xsl:if>
                
              </xsl:when>
            </xsl:choose>              
          </xsl:if>
          <xsl:if test="(/Root/@EarlyBillFlag = '1' or //ClaimAspect[@ClaimAspectID=$ClaimAspectID]/ClaimAspectServiceChannel/@ServiceChannelCD='TL') and count(/Root/Invoice[@ClaimAspectID = $ClaimAspectID and @ItemTypeCD='I' and @StatusCD = 'FS']) &gt; 0">
             <IE:APDButton id="btnReleasePayment" name="btnReleasePayment" value="Rel. Payment" width="90" CCTabIndex="">
               <xsl:attribute name="id"><xsl:value-of select="concat('btnReleasePayment_', $ClaimAspectID)"/></xsl:attribute>
               <xsl:attribute name="name"><xsl:value-of select="concat('btnReleasePayment_', $ClaimAspectID)"/></xsl:attribute>
               <xsl:choose>
                  <xsl:when test="$ClaimStatus = 'Claim Closed' or $ClaimStatus = 'Claim Cancelled' or $ClaimStatus = 'Claim Voided'">
                   <xsl:attribute name="CCDisabled">true</xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                   <xsl:attribute name="CCDisabled">false</xsl:attribute>
                  </xsl:otherwise>
               </xsl:choose>
               <xsl:attribute name="onButtonClick">showReleasePayment("<xsl:value-of select="@Name"/>", <xsl:value-of select="@ClaimAspectID"/>)</xsl:attribute>
             </IE:APDButton>
          </xsl:if>
          <xsl:if test="$DisplayButton='false' and $DisplayButtonReason != ''">
	          <img src="/images/info.png">
	               <xsl:attribute name="title"><xsl:value-of select="concat('Generate Invoice not available because ', $DisplayButtonReason)"/></xsl:attribute>
	          </img>
          </xsl:if>            
        </td>             
      </tr>
        
        <xsl:for-each select="/Root/Invoice[@ClaimAspectID=$ClaimAspectID]">
          <xsl:variable name="ItemTypeCD" select="@ItemTypeCD"/>
          
          <tr style="cursor:hand;" valign="top" onmouseover="OnInvoiceMouseOver(this)" onmouseout="OnInvoiceMouseOut(this)" onclick="showInvoiceDetails(this)">
            <xsl:attribute name="EntityGrp"><xsl:value-of select="$EntityGrp"/></xsl:attribute>
            <td colspan="2" unselectable="on" align="left">
                            
              <xsl:if test="$ExpClosed != 'true'">
                <xsl:if test="contains($InfoCRUD, 'D') or (contains($InfoCRUD, 'U') and @StatusCD != 'FS' and @StatusCD != 'AUTH')">
                  <xsl:variable name="DispatchNumber"><xsl:value-of select="@DispatchNumber"/></xsl:variable>
                  <xsl:variable name="DispatchCount"><xsl:value-of select="count(/Root/Invoice[@DispatchNumber=$DispatchNumber and @DispatchNumber != '' and @DispatchNumber != '0'])"/></xsl:variable>
                                  
                  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                  <button style="border:0px; height:14px;width:14px;padding:0px;margin:0px;background-color:transparent;cursor:hand;">
                    <xsl:attribute name="onclick">
                      InvoiceDelete(
                      <xsl:value-of select="@InvoiceID"/>, 
                      '<xsl:value-of select="$DispatchNumber"/>',
                      <xsl:value-of select="$DispatchCount"/>)
                    </xsl:attribute>
                    <img src="/images/delete.gif" title="Delete" valign="absmiddle"/>
                  </button>
                </xsl:if>  
                                
                <xsl:if test="$ExpClosed != 'true' and contains($InfoCRUD, 'U') and @StatusCD != 'FS' and @StatusCD != 'AUTH'">
                  <button style="border:0px; height:14px;width:14px;padding:0px;margin:0px;background-color:transparent;cursor:hand;">
                    <xsl:attribute name="onclick">InvoiceEdit('<xsl:value-of select="$EntityGrp"/>', <xsl:value-of select="@InvoiceID"/>, '<xsl:value-of select="@ItemTypeCD"/>', <xsl:value-of select="$ClaimAspectID"/>)</xsl:attribute>                                                  
                    <img src="/images/edit.gif" title="Edit" valign="absmiddle"/>
                  </button>
                </xsl:if>
              </xsl:if>
                
                            
            </td>
            <td unselectable="on">
              <span>
                <xsl:if test="@FeeCategoryCD='H'"><xsl:attribute name="title">Handling Fee</xsl:attribute></xsl:if>
                <xsl:if test="@FeeCategoryCD='A'"><xsl:attribute name="title">Additional Fee</xsl:attribute></xsl:if>
                <xsl:value-of select="@FeeCategoryCD"/>
              </span>
            </td>
            <td unselectable="on">
              <xsl:choose>
                <xsl:when test="$ItemTypeCD='F'">
                  <img src="/images/plus.gif" align="absmiddle" style="cursor:hand" onclick="showHideServices(this)"/>
                  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                  <xsl:value-of select="@Description"/>
                  <xsl:if test="count(InvoiceService) &gt; 0">
                    <div style="display:none;margin-left:15px;padding:3px;">
                      <xsl:for-each select="InvoiceService">
                        <xsl:variable name="ServiceID"><xsl:value-of select="@ServiceID"/></xsl:variable>
                        <xsl:value-of select="/Root/Reference[@List='Service' and @ReferenceID=$ServiceID]/@Name"/>
                        (<xsl:value-of select="@PertainsToDescription"/>/<xsl:value-of select="@ServiceChannelCD"/>)
                        <br/>
                      </xsl:for-each>
                    </div>
                  </xsl:if>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="@PayeeName"/></xsl:otherwise>
              </xsl:choose>
            </td>
            <td unselectable="on">
              <xsl:choose>
                <xsl:when test="@DispatchNumber != ''">
                  <a href="javascript:showDispatchDetails(this)">
                     <xsl:attribute name="href"><xsl:value-of select="concat('javascript:showDispatchDetails(', @DispatchNumber, ')')"/></xsl:attribute>
                     <xsl:value-of select="@DispatchNumber"/>
                  </a>
                </xsl:when>
                <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
              </xsl:choose>
            </td>
            <td unselectable="on">
               <xsl:value-of select="/Root/Reference[@List='ItemTypeCD' and @ReferenceID=$ItemTypeCD]/@Name"/>
               <xsl:if test="@Model = 'B'"> (Bulk)</xsl:if>
            </td>
            <td unselectable="on">
              <xsl:choose>
                <xsl:when test="@ItemTypeCD != 'F'"><xsl:value-of select="@Amount"/></xsl:when>
                <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
              </xsl:choose>
            </td>              
            <td unselectable="on"><xsl:value-of select="user:UTCConvertDate(string(@EntryDate))"/></td>
            <td unselectable="on"><xsl:value-of select="@StatusCD"/></td>
            <td unselectable="on">
              <xsl:choose>
                <xsl:when test="@ItemizeFlag = '0'">N/A</xsl:when>
                <xsl:otherwise>
                  <xsl:if test="@StatusDate != '1900-01-01T00:00:00'"><xsl:value-of select="user:UTCConvertDate(string(@StatusDate))"/></xsl:if>
                </xsl:otherwise>
              </xsl:choose>
            </td>
            <td style="display:none"><xsl:value-of select="@InvoiceID"/></td>
          </tr>
        </xsl:for-each>
    </xsl:for-each>
    </table>
  </div>
</xsl:template>

  
  <xsl:template name="InvoiceDetail">
    <IE:APDTabGroup id="tabInvoiceDetail" name="tabInvoiceDetail" width="735" height="200" preselectTab="0" showADS="false" >
      <IE:APDTab id="tabDetail" name="tabDetail" caption="Details" tabPage="InvoiceDetail"  />

      <IE:APDTabPage name="InvoiceDetail" id="InvoiceDetail" style="display:none">
        <div id="tabFeeDetail" style="display:inline;">
          <table border="0" cellspacing="2" cellpadding="0" style="table-layout:fixed;">
            <colgroup>
              <col width="400"/>
              <col width="10"/>
              <col width="290"/>
            </colgroup>
            
            <tr>
              <td valign="top">
                <table border="0" cellspacing="2" cellpadding="1" style="table-layout:fixed;">
                  <colgroup>
                    <col width="115"/>
                    <col width="285"/>
                  </colgroup>
                  
                  <tr>
                    <td>Category:</td>
                    <td>
                      <IE:APDLabel id="txtCategory" name="txtCategory" width="285" />
                    </td>                  
                  </tr>
                  <tr>
                    <td>Billing User:</td>
                    <td>
                      <IE:APDLabel id="txtBillingUser" name="txtBillingUser" width="285" />
                    </td>                 
                  </tr>            
                  <tr>
                    <td>Bill Entry Date:</td>
                    <td>
                      <IE:APDLabel id="txtBillEntryDate" name="txtBillEntryDate" width="285"/>
                    </td>                 
                  </tr>
                  <tr>
                    <td>Pickup Date:</td>
                    <td>
                      <IE:APDLabel id="txtPickupDate" name="txtPickupDate" width="285"/>
                    </td>
                  </tr>                
                </table>
              </td>
              <td></td>
              <td valign="top">
                <table border="0" cellspacing="2" cellpadding="1" style="table-layout:fixed;">
                  <colgroup>
                    <col width="115"/>
                    <col width="165"/>
                  </colgroup>  
                  <tr>
                    <td>Status:</td>
                    <td>
                      <IE:APDLabel id="txtFeeStatus" name="txtFeeStatus" width="165"/>
                    </td>
                  </tr>
                  <tr>
                    <td>Appears on Invoice:</td>
                    <td>
                      <IE:APDLabel id="txtItemize" name="txtItemize" width="165" />
                    </td>
                  </tr>
                  <tr>
                     <td>Invoice Method:</td>
                    <td>
                      <IE:APDLabel id="txtInvoiceMethod" name="txtInvoiceMethod" width="165" />
                    </td>
                  </tr>
                  <tr>
                    <td>Invoice Model:</td>
                    <td>
                      <IE:APDLabel id="txtInvoiceModel" name="txtInvoiceModel" width="165" />
                    </td>
                  </tr>
                  <tr>
                    <td>Generate Invoice:</td>
                    <td><IE:APDLabel id="txtGenerate" name="txtGenerate" width="165" /></td>
                  </tr>
                </table> 
              </td>
            </tr>
          </table>
        </div>
        
        <div id="tabPaymentDetail" style="display:none;">
          <table border="0" cellspacing="2" cellpadding="0" style="table-layout:fixed; overflow:hidden">
          <colgroup>
            <col width="400"/>
            <col width="10"/>
            <col width="290"/>
          </colgroup>
          <tr>
            <td valign="top">
              <table border="0" cellspacing="2" cellpadding="1" style="table-layout:fixed;">
                <colgroup>
                  <col width="115"/>
                  <col width="285"/>
                </colgroup>
                <tr>
                  <td>Payee Address 1:</td>
                  <td>
                    <IE:APDLabel id="txtPayeeAddress1" name="txtPayeeAddress1" width="285" />
                  </td>
                </tr>
                <tr>
                  <td>Payee Address 2:</td>
                  <td>
                    <IE:APDLabel id="txtPayeeAddress2" name="txtPayeeAddress2" width="285" />
                  </td>
                </tr>
                <tr>
                  <td>City:</td>
                  <td>
                    <IE:APDLabel id="txtPayeeCity" name="txtPayeeCity" width="285" />
                  </td>
                </tr>
                <tr>
                  <td>State:</td>
                  <td>
                    <IE:APDLabel id="txtPayeeState" name="txtPayeeState" width="285" />
                  </td>
                </tr>
                <tr>
                  <td>Zip Code:</td>
                  <td>
                    <IE:APDLabel id="txtPayeeZip" name="txtPayeeZip" width="285" />
                  </td>
                </tr>
                <tr>
                  <td valign="top">Payment Desc:</td>
                  <td valign="top"><IE:APDLabel id="txtDescription" name="txtDescription" width="285" height="48"/></td>
                </tr>
              </table>
            </td>
            <td></td>
            <td valign="top">
              <table border="0" cellspacing="2" cellpadding="1" style="table-layout:fixed;">
                <colgroup>
                  <col width="115"/>
                  <col width="165"/>
                </colgroup>                
                <tr>
                  <td>Status:</td>
                  <td><IE:APDLabel id="txtPaymentStatus" name="txtPaymentStatus" width="165"/></td>
                </tr>                
                <tr>
                  <td>Payment User:</td>
                  <td><IE:APDLabel id="txtPaymentUser" name="txtPaymentUser" width="165"/></td>
                </tr>
                <tr>
                  <td>Invoice Method:</td>
                  <td><IE:APDLabel id="txtInvoiceMethodPayment" name="txtInvoiceMethodPayment" width="165"/></td>
                </tr>
                <tr>
                  <td>Invoice Model:</td>
                  <td><IE:APDLabel id="txtInvoiceModelPayment" name="txtInvoiceModelPayment" width="165"/></td>
                </tr>
                <tr>
                  <td>Generate Invoice:</td>
                  <td><IE:APDLabel id="txtGeneratePayment" name="txtGeneratePayment" width="165"/></td>
                </tr>                
              </table>
            </td>
          </tr>
        </table>
        </div>        
      </IE:APDTabPage>
    </IE:APDTabGroup>
  </xsl:template>

</xsl:stylesheet>
