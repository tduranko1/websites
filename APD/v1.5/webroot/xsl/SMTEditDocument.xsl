<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:local="http://local.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    id="Notes">

<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/generic-template-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="ShopLocationID"/>
<xsl:param name="DocumentID"/>
<xsl:param name="UserID"/>
<xsl:param name="WindowID"/>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<xsl:template match="/Root">

<xsl:variable name="Metadata"><xsl:copy-of select="/Root/Metadata"/></xsl:variable>

<HTML>

<HEAD>
<TITLE>Edit Shop Document</TITLE>

<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/datepicker2.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>

<script language="javascript">

  var gsWindowID = "<xsl:value-of select='$WindowID'/>";
  var gsShopLocationID = "<xsl:value-of select="$ShopLocationID"/>";
  var gsDocumentID = "<xsl:value-of select="$DocumentID"/>";
  var gsUserID = "<xsl:value-of select="$UserID"/>";
  var sRet = "";
  var oDocument = window.dialogArguments;

<![CDATA[

  function pageInit(){
    if (oStat){
      oStat.style.left = (document.body.offsetWidth - 150) / 2 - 50;
      oStat.style.top = (document.body.offsetHeight - 75) / 2 - 25;
    }
    if(oDocument){
        txtEffectiveDate.value = oDocument.EffectiveDate;
        txtExpirationDate.value = oDocument.ExpirationDate;
        txtComments.value = (oDocument.Comments ? oDocument.Comments : "");
        selDocumentType.SelectItem(null, oDocument.DocumentType);
        gsShopLocationID = oDocument.ShopLocationID;
        gsDocumentID = oDocument.DocumentID;
        if (gsShopLocationID != "" && gsDocumentID != "") btnSave.CCDisabled = false;
        selDocumentType.setFocus();
    }
  }

  function doClose(){
    sRet = "Close";
    window.close();
  }
  
  function doSave(){
    if (selDocumentType.selectedIndex == -1){
      ClientWarning("Please select a Document Type.");
      return;
    }
    txtComments.value = txtComments.value.replace(/<>/g, "");
    var sProc = "uspShopDocumentUpdDetail";
    var sRequest = "ShopLocationID=" + gsShopLocationID +
                   "&DocumentID=" + gsDocumentID + 
                   "&EffectiveDate=" + txtEffectiveDate.value + 
                   "&ExpirationDate=" + txtExpirationDate.value + 
                   "&DocumentType=" + escape(selDocumentType.text) + 
                   "&Comments=" + escape(txtComments.value) + 
                   "&UserID=" + gsUserID;

    if (sProc != "" && sRequest != ""){
        var aRequests = new Array();
        aRequests.push( { procName : sProc,
                          method   : "ExecuteSpNp",
                          data     : sRequest }
                      );
        var objRet = XMLSave(makeXMLSaveString(aRequests));
    }
    sRet = "Save";
    window.close();
  }
  
  function onDlgClose(){
    window.returnValue = sRet;
  }


]]>

</script>

</HEAD>
<BODY unselectable="on" style="background-color:#FFFFFF;overflow:hidden;border:0px;padding:1px;" onload="pageInit()" tabIndex="-1" topmargin="0"  leftmargin="0" onbeforeunload="onDlgClose()">
  <IE:APDStatus id="oStat" name="oStat" class="APDStatus" width="250" height="75" />
  <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed;" >
    <colgroup>
      <col width="100px" style="font-weight:bold"/>
      <col width="*"/>
    </colgroup>
    <tr>
      <td>Type:</td>
      <td>
        <IE:APDCustomSelect id="selDocumentType" name="selDocumentType" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="2" required="true" width="250" onChange="">
          <xsl:for-each select="/Root/Reference[@ListName='DocumentType']">
        	<IE:dropDownItem style="display:none">
            <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
            <xsl:value-of select="@Value"/>
          </IE:dropDownItem>
          </xsl:for-each>
        </IE:APDCustomSelect>
      </td>
    </tr>
    <tr>
      <td>Effective Date:</td>
      <td>
        <IE:APDInputDate id="txtEffectiveDate" name="txtEffectiveDate" type="date" futureDate="true" required="false" canDirty="false" CCDisabled="false" CCTabIndex="3" onChange="" />
      </td>
    </tr>
    <tr>
      <td>Expiration Date:</td>
      <td>
        <IE:APDInputDate id="txtExpirationDate" name="txtExpirationDate" type="date" futureDate="true" required="false" canDirty="false" CCDisabled="false" CCTabIndex="4" onChange="" />
      </td>
    </tr>
    <tr valign="top">
      <td>Comments:</td>
      <td>
        <IE:APDTextArea id="txtComments" name="txtComments" maxLength="500" width="250" height="50" required="false" canDirty="false" CCDisabled="false" CCTabIndex="5" onChange="" />
      </td>
    </tr>
    <tr>
      <td colspan="2" style="padding-left:125px">
        <IE:APDButton id="btnSave" name="btnSave" value="Save" width="75" CCDisabled="true" CCTabIndex="6" onButtonClick="doSave()"/>
        <IE:APDButton id="btnClose" name="btnClose" value="Close" width="75" CCDisabled="false" CCTabIndex="7" onButtonClick="doClose()"/>
      </td>
    </tr>
  </table>
</BODY>
</HTML>
</xsl:template>

</xsl:stylesheet>
