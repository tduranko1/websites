<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:js="urn:the-xml-files:xslt"
	id="SMTShopBilling">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- This page is a child of SMTDetailLevel.  This page resides in that page's 'ifrmContent' iframe and
       is dependent on global variables in that page to maintain state -->

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserID"/>
<xsl:param name="ShopID"/>
<xsl:param name="mode"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTShopBillingGetDetailXML,SMTBilling.xsl,394   -->

<HEAD>
  <TITLE>Shop Maintenance</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  <LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>


<!-- CLIENT SCRIPTS -->
<xsl:if test="$mode != 'wizard'"><SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT></xsl:if>
<SCRIPT language="JavaScript" src="/js/SMTValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<script language="javascript">

var gsUserID = '<xsl:value-of select="$UserID"/>';
var gsBillingID = '<xsl:value-of select="@BillingID"/>';
var gsMode = '<xsl:value-of select="$mode"/>';
var gsPageFile = "SMTShopBilling.asp";
var gsShopCRUD = '<xsl:value-of select="$ShopCRUD"/>';

<![CDATA[

function InitPage(){
	if (gsMode != "wizard")
		InitEditPage();
	else{
		SetWizNames();
		parent.frameLoaded(5);
	}

	if (gsShopCRUD.indexOf("U") != -1) frmBilling.txtName.focus();

	onpasteAttachEvents();

	//set dirty flags associated with all inputs and selects.
  var aInputs = null;
  var obj = null;
  aInputs = document.all.tags("INPUT");
  for (var i=0; i < aInputs.length; i++) {
    if (aInputs[i].type=="text"){
      aInputs[i].attachEvent("onkeypress", SetDirty);
      aInputs[i].attachEvent("onchange", SetDirty);
      aInputs[i].attachEvent("onpropertychange", SetDirty);
    }
    if (aInputs[i].type=="checkbox" || aInputs[i].type == "radio") aInputs[i].attachEvent("onclick", SetDirty);
    
    if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
  }

  aInputs = document.all.tags("SELECT");
  for (var i=0; i < aInputs.length; i++) {
      aInputs[i].attachEvent("onchange", SetDirty);
      if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
  }
}

function InitEditPage(){
	top.gsPageFile = gsPageFile;
	parent.gsPageName = "Shop Billing";
	parent.btnDelete.style.visibility = "hidden";

  if (gsShopCRUD.indexOf("U") > -1){
    parent.btnSave.style.visibility = "visible";
	  parent.btnSave.disabled = false;
  }
  else
    parent.btnSave.style.visibility = "hidden";

	parent.tab23.className = "tabactive1";
}

function txtAddressZip_onBlur(){

    IsValidZip(frmBilling.txtAddressZip);

	ValidateZip(frmBilling.txtAddressZip, 'selState', 'txtAddressCity');
	ValidateZipToCounty(frmBilling.txtAddressZip, 'txtAddressCounty');
}

function txtCheckExtension(){
    if (isNumeric(event.srcElement.value) == false) {
        ClientWarning("Invalid numeric value specified. Please try again.");
        event.srcElement.focus();
        event.srcElement.select();
        return false;
    }
}

function txtCheckDate() {
    CheckDate(event.srcElement);
}

function ValidateAllPhones() {
	if (validatePhoneSections(frmBilling.txtPhoneAreaCode, frmBilling.txtPhoneExchangeNumber, frmBilling.txtPhoneUnitNumber) == false) return false;
	if (validatePhoneSections(frmBilling.txtFaxAreaCode, frmBilling.txtFaxExchangeNumber, frmBilling.txtFaxUnitNumber) == false) return false;
    if (isNumeric(frmBilling.txtPhoneExtensionNumber.value) == false){
        Parent.ClientWarning("Invalid numeric value specified. Please try again.");
        frmBilling.txtPhoneExtensionNumber.focus();
        frmBilling.txtPhoneExtensionNumber.select();
        return false;
    }
    if (isNumeric(frmBilling.txtFaxExtensionNumber.value) == false){
        Parent.ClientWarning("Invalid numeric value specified. Please try again.");
        frmBilling.txtFaxExtensionNumber.focus();
        frmBilling.txtFaxExtensionNumber.select();
        return false;
    }
    validateAreaCodeSplit(frmBilling.txtPhoneAreaCode, frmBilling.txtPhoneExchangeNumber);
    validateAreaCodeSplit(frmBilling.txtFaxAreaCode, frmBilling.txtFaxExchangeNumber);

    return true;
}

function IsDirty(){
	Save();
}

function ValidatePage(){
  if (frmBilling.txtName.value == ""){
    	parent.ClientWarning("A value is required in the \" Name\" field.");
    	frmBilling.txtName.focus();
    	return false;
	}
  if (ValidateAllPhones() == false) return false;

  return true;
}

function Save(){
  if (!ValidatePage()) return false;

	parent.btnSave.disabled = true;
	frmBilling.action += "?mode=update&BillingID=" + gsBillingID;
	frmBilling.submit();
	parent.gbDirtyFlag = false;
}

function SetDirty(){
	parent.gbDirtyFlag = true;
}

]]>

<xsl:if test="$mode='wizard'">
	function SetWizNames(){
		frmBilling.txtPhoneAreaCode.setAttribute("wizname", "BillingPhoneAreaCode");
		frmBilling.txtPhoneExchangeNumber.setAttribute("wizname", "BillingPhoneExchangeNumber");
		frmBilling.txtPhoneUnitNumber.setAttribute("wizname", "BillingPhoneUnitNumber");
		frmBilling.txtPhoneExtensionNumber.setAttribute("wizname", "BillingPhoneExtensionNumber");
		frmBilling.txtFaxAreaCode.setAttribute("wizname", "BillingFaxAreaCode");
		frmBilling.txtFaxExchangeNumber.setAttribute("wizname", "BillingFaxExchangeNumber");
		frmBilling.txtFaxUnitNumber.setAttribute("wizname", "BillingFaxUnitNumber");
		frmBilling.txtFaxExtensionNumber.setAttribute("wizname", "BillingFaxExtensionNumber");
	}
</xsl:if>
</script>

</HEAD>

<body class="bodyAPDsub" onload="InitPage();" style="overflow:hidden; margin-left:0; margin-right:0; margin-top:0;">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>

<div style="background-color:#FFFAEB; height:450px;">

<IMG src="/images/spacer.gif" width="6" height="12" border="0" />

<form id="frmBilling" method="post" style="overflow:hidden">
  <xsl:apply-templates select="Billing"/> <!-- Get Billing Info -->

  <input type="hidden" id="txtBillingID" name="BillingID">
    <xsl:attribute name="value"><xsl:value-of select="@BillingID"/></xsl:attribute>
  </input>
  <input type="hidden" id="txtSysLastUserID" name="SysLastUserID">
    <xsl:attribute name="value"><xsl:value-of select="$UserID"/></xsl:attribute>
  </input>
  <input type="hidden" id="txtSysLastUpdatedDate" name="SysLastUpdatedDate">
    <xsl:attribute name="value"><xsl:value-of select="Billing/@SysLastUpdatedDate"/></xsl:attribute>
  </input>
  <input type="hidden" id="txtCallBack" name="CallBack"></input>
  </form>
</div>

<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

</body>
</HTML>
</xsl:template>

<xsl:template match="Billing">
  <table cellSpacing="0" cellPadding="0" border="0" width="715" style="table-layout:fixed;">
    <tr>
      <td>
        <table cellSpacing="0" cellPadding="1" border="0">
          <tr>
            <td colspan="3"><strong>Remittance Address:</strong></td>
          </tr>
          <tr>
            <td><IMG src="/images/spacer.gif" width="6" height="18" border="0" /></td> <!-- Spacer Row -->
          </tr>
          <tr>
            <td width="10px"><span style="font-weight:bold;">*</span></td>
            <td>Name:</td>
            <td>
              <input type="text" id="txtName" name="Name" wizname="BillingName" autocomplete="off" size="51" class="inputFld">
      			    <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Billing']/Column[@Name='Name']/@MaxLength"/></xsl:attribute>
      			    <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
      			  </input>
            </td>
          </tr>
          <tr>
            <td></td>
            <td>Address1:</td>
            <td>
              <input type="text" id="txtAddress1" name="Address1" wizname="BillingAddress1" size="51" class="inputFld">
      			    <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Billing']/Column[@Name='Address1']/@MaxLength"/></xsl:attribute>
      			    <xsl:attribute name="value"><xsl:value-of select="@Address1"/></xsl:attribute>
      			  </input>
            </td>
          </tr>
          <tr>
            <td></td>
            <td noWrap="nowrap">Address2:</td>
            <td>
              <input type="text" id="txtAddress2" name="Address2" wizname="BillingAddress2" size="51" class="inputFld">
      			    <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Billing']/Column[@Name='Address2']/@MaxLength"/></xsl:attribute>
      			    <xsl:attribute name="value"><xsl:value-of select="@Address2"/></xsl:attribute>
      			  </input>
            </td>
          </tr>
          <tr>
            <td></td>
            <td>Zip:</td>
            <td>
              <input type="text" id="txtAddressZip" name="AddressZip" wizname="BillingAddressZip" size="9" maxlength="5" onblur="txtAddressZip_onBlur()" onkeypress="NumbersOnly(event)" class="inputFld">
        			  <xsl:attribute name="value"><xsl:value-of select="@AddressZip"/></xsl:attribute>
        			</input>
            </td>
          </tr>
          <tr>
            <td></td>
            <td>City:</td>
            <td>
              <input type="text" id="txtAddressCity" name="AddressCity" wizname="BillingAddressCity" size="31" class="inputFld">
        			  <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Billing']/Column[@Name='AddressCity']/@MaxLength"/></xsl:attribute>
        			  <xsl:attribute name="value"><xsl:value-of select="@AddressCity"/></xsl:attribute>
        			</input>
            </td>
          </tr>
          <tr>
            <td></td>
            <td>State:</td>
            <td>
              <select id="selState" name="AddressState" wizname="BillingAddressState" class="inputFld">
  			        <option></option>
                <xsl:for-each select="/Root/Reference[@ListName='State']">
        			    <xsl:call-template name="BuildSelectOptions"><xsl:with-param name="current" select="/Root/Billing/@AddressState"/></xsl:call-template>
        			  </xsl:for-each>
              </select>
            </td>
          </tr>
          <tr>
            <td></td>
            <td>County:</td>
            <td>
              <input type="text" id="txtAddressCounty" name="AddressCounty" wizname="BillingAddressCounty" size="31" class="inputFld">
        			  <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Billing']/Column[@Name='AddressCounty']/@MaxLength"/></xsl:attribute>
        			  <xsl:attribute name="value"><xsl:value-of select="@AddressCounty"/></xsl:attribute>
        			</input>
            </td>
          </tr>
          <tr>
            <td></td>
            <td>Phone:</td>
            <td noWrap="nowrap">
              <xsl:call-template name="ContactNumbers"><xsl:with-param name="ContactMethod">Phone</xsl:with-param></xsl:call-template>
            </td>
          </tr>
          <tr>
            <td></td>
            <td>Fax:</td>
            <td noWrap="nowrap">
              <xsl:call-template name="ContactNumbers"><xsl:with-param name="ContactMethod">Fax</xsl:with-param></xsl:call-template>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>
        <table cellSpacing="0" cellPadding="1" border="0">
          <tr>
            <td colspan="2"><IMG src="/images/spacer.gif" width="6" height="18" border="0" /></td>
          </tr>
          <tr>
            <td colspan="3"><strong>Electronic Fund Transfer Information:</strong></td>
          </tr>
        <tr>
          <td><IMG src="/images/spacer.gif" width="6" height="18" border="0" /></td> <!-- Spacer Row -->
        </tr>
        <tr>
          <td width="10"></td>
          <td>EFT Contract Signed:</td>
          <td nowrap="nowrap">
            <input type="checkbox" id="cbEFTContractSignedFlag" onclick="txtEFTContractSignedFlag.value=this.checked?1:0" class="inputFld">
      			  <xsl:if test="@EFTContractSignedFlag=1"><xsl:attribute name="checked"/></xsl:if>
      			</input>
            <input type="hidden" id="txtEFTContractSignedFlag" name="EFTContractSignedFlag" wizname="BillingEFTContractSignedFlag">
      			  <xsl:attribute name="value"><xsl:value-of select="@EFTContractSignedFlag"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td></td>
          <td noWrap="nowrap">EFT Account Number:</td>
          <td>
            <input type="text" id="txtEFTAccountNumber" name="EFTAccountNumber" wizname="BillingEFTAccountNumber" size="51" class="inputFld">
      			  <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Billing']/Column[@Name='EFTAccountNumber']/@MaxLength"/></xsl:attribute>
      			  <xsl:attribute name="value"><xsl:value-of select="@EFTAccountNumber"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td></td>
          <td>EFT Account Type:</td>
          <td>
    		    <input type="radio" id="rbSavings" name="EFTAccountType" value="S" onclick="txtEFTAccountTypeCD.value=this.value">
      			  <xsl:if test="@EFTAccountTypeCD='S'"><xsl:attribute name="checked"/></xsl:if>Savings
      			</input>
            <input type="radio" id="rbChecking" name="EFTAccountType" value="C" onclick="txtEFTAccountTypeCD.value=this.value">
      			  <xsl:if test="@EFTAccountTypeCD='C'"><xsl:attribute name="checked"/></xsl:if>Checking
      			</input>
            <input type="radio" id="rbNone" name="EFTAccountType" value="" onclick="txtEFTAccountTypeCD.value=this.value">
    			    <xsl:if test="@EFTAccountTypeCD=''"><xsl:attribute name="checked"/></xsl:if>None
			      </input>
            <input type="hidden" id="txtEFTAccountTypeCD" name="EFTAccountTypeCD" wizname="BillingEFTAccountTypeCD">
      			  <xsl:attribute name="value"><xsl:value-of select="@EFTAccountTypeCD"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td></td>
          <td noWrap="nowrap">EFT Routing Number:</td>
          <td>
            <input type="text" id="txtEFtroutingNumber" name="EFTRoutingNumber" wizname="BillingEFTRoutingNumber" size="51" class="inputFld">
      			  <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Billing']/Column[@Name='EFTRoutingNumber']/@MaxLength"/></xsl:attribute>
      			  <xsl:attribute name="value"><xsl:value-of select="@EFTRoutingNumber"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td></td>
          <td noWrap="nowrap">EFT Effective Date:</td>
          <td>
            <input type="text" id="txtEFTEffectiveDate" name="EFTEffectiveDate" wizname="BillingEFTEffectiveDate" size="20" onblur="txtCheckDate()" class="inputFld">
      			  <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Billing']/Column[@Name='EFTEffectiveDate']/@MaxLength"/></xsl:attribute>
      			  <xsl:attribute name="value">
      			    <xsl:if test="@EFTEffectiveDate != ''">
      			      <xsl:value-of select="concat(substring(@EFTEffectiveDate, 6, 2),'/',substring(@EFTEffectiveDate, 9, 2),'/',substring(@EFTEffectiveDate, 1, 4))"/>
      				  </xsl:if>
			        </xsl:attribute>
      			</input>
      			<A  href='#' onclick='ShowCalendar(dimgEFTEffectiveDate,txtEFTEffectiveDate)' >
      			  <IMG  align='absmiddle' border='0' width='24' height='17' id='dimgEFTEffectiveDate' src='/images/calendar.gif'/>
      			</A>
      		  <DIV  id='divCalendar' class='PopupDiv' onMouseOut='CalMouseOut(this)' onMouseOver='CalMouseOver(this)' onDeactivate='CalBlur(this)' style='position:absolute;width:180px;height:127px;z-index:20000;display:none;visibility:hidden'></DIV>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</xsl:template>

</xsl:stylesheet>
