<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt">

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspRptShopActivityGetXML,ShopActivity.xsl, '07/01/2003', ''   -->


<xsl:template match="/Root">
  <div align="left" style="width:725px;margin-bottom:4px;">
    <input type="button" class="formbutton" onclick="printPage()" value="Print" id="btnPrint"/>
  </div>
  <div id="rptText" style="width:725px;height:390px;background-color:#FFFFFF;border:1px solid #c0c0c0;border-bottom:3px solid #000000;border-right:3px solid #000000;padding:5px;overflow:auto;">
    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-bottom:4px double #000000;margin-bottom:8px;">
    <tr>
      <td><div style="font:bolder 12pt Tahoma;">Shop Activity Report</div></td>
      <td><div style="font:bolder 12pt Tahoma;" align="right">LYNX Services</div></td>
    </tr>
    </table>
    
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
      <colgroup>
        <col width="65px"/>
        <col width="100px"/>
        <col width="100px"/>
        <col width="55px"/>
        <col width="100px"/>
        <col/>
      </colgroup>
      <tr>
        <td><strong>From Date:</strong></td>
        <td><xsl:value-of select="@FromDate"/></td>
        <td> </td>
        <td><strong>To Date:</strong></td>
        <td><xsl:value-of select="@ToDate"/></td>
        <td> </td>
        <td align="right"><strong>Report Date:  </strong><xsl:value-of select="@ReportDate"/></td>
      </tr>
    </table>
    <br/>
    
    <xsl:for-each select="State">
      <xsl:variable name="StateAbbrev" select="@AddressState"/>
      <div style="font-size:14; font-weight:bold">
        <xsl:value-of select="/Root/Reference[@ListName='State' and @ReferenceID=$StateAbbrev]/@Name"/>
      </div>
      <xsl:for-each select="Shop"><xsl:call-template name="Shop"/></xsl:for-each>
    </xsl:for-each>
  </div>
</xsl:template>

<xsl:template name="Shop">
  <table cellspacing="0" cellpadding="3" border="1" width="100%" style="border:1px solid gray; border-collapse:collapse;margin-bottom:12px;">
    <colgroup>
      <col width="80px"/>
      <col width="220px"/>
      <col width="210px"/>
      <col width="120px"/>
    </colgroup>
    <tr style="background-color:silver">
      <td><strong>Shop ID:</strong><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@ShopLocationID"/></td>
      <td><strong><xsl:value-of select="@Name"/></strong></td>
      <td colspan="2"><strong>Contact:</strong><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@ContactName"/></td>
      
    </tr>
    <tr style="background-color:silver">
      <td><strong>Type:</strong><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@Type"/></td>
      <td>
        <xsl:value-of select="@Address1"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of select="@AddressCity"/>,<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@AddressState"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of select="@AddressZip"/>
      </td>
      <td><xsl:value-of select="concat('(', @PhoneAreaCode, ') ', @PhoneExchangeNumber, '-', @PhoneUnitNumber)"/></td>
      <td><strong>Last ReI:</strong><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@LastReinspectionDate"/></td>
    </tr>
  </table>
  
  <table cellspacing="0" cellpadding="3" border="1" width="100%" style="border:1px solid gray; border-collapse:collapse;margin-bottom:12px;">
    <colgroup>
      <col width="80px"/>
      <col width="220px"/>
      <col width="90px"/>
      <col width="90px"/>
      <col width="50px"/>
      <col width="100px"/>
    </colgroup>
    <tr style="background-color:silver">
      <td><strong>Lynx ID</strong></td>
      <td><strong>Vehicle</strong></td>
      <td><strong>Assignment</strong></td>
      <td><strong>Estimate</strong></td>
      <td><strong>Type</strong></td>
      <td><strong>Amount</strong></td>
    </tr>
    <xsl:variable name="ShopLocationID" select="@ShopLocationID"/>
    <xsl:for-each select="/Root/Assignment[@ShopLocationID=$ShopLocationID]"><xsl:call-template name="Assignment"/></xsl:for-each>
  </table>
  
</xsl:template>


<xsl:template name="Assignment">
  <tr>
    <td><xsl:value-of select="@LynxID"/></td>
    <td>
      <xsl:value-of select="@VehicleYMM"/><br/>
      License State:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@LicensePlateState"/>
      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      Number:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@LicensePlateNumber"/><br/>
      Owner:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      <xsl:choose>
        <xsl:when test="@OwnerBusinessName != ''"><xsl:value-of select="@OwnerBusinessName"/></xsl:when>
        <xsl:otherwise><xsl:value-of select="@OwnerName"/></xsl:otherwise>
      </xsl:choose>    
    </td>
    <td><xsl:value-of select="@AssignmentDate"/></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <xsl:variable name="AssignmentID" select="@AssignmentID"/>
  <xsl:for-each select="/Root/Estimate[@AssignmentID=$AssignmentID]"><xsl:call-template name="Estimate"/></xsl:for-each>
</xsl:template>
  
<xsl:template name="Estimate">
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td><xsl:value-of select="@EstimateDate"/></td>
    <td>
      <xsl:choose>
        <xsl:when test="@EstimateType='Estimate'">E</xsl:when>
        <xsl:otherwise>S</xsl:otherwise>
      </xsl:choose>      
    </td>
    <td><xsl:value-of select="@Amount"/></td>
  </tr>
</xsl:template>

</xsl:stylesheet>