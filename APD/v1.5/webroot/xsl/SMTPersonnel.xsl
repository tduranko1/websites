<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTPersonnel">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- This page is a child of SMTDetailLevel.  This page resides in that page's 'ifrmContent' iframe and
       is dependent on global variables in that page to maintain state -->

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserID"/>
<xsl:param name="mode"/>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<xsl:template match="/Root">

<xsl:variable name="MaxPersonnel">
	<xsl:choose>
	  <xsl:when test="@EntityType='S'">4</xsl:when>
	  <xsl:when test="@EntityType='B'">2</xsl:when>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="ShopAfterHoursContact" select="Reference[@List='PersonnelType' and @Name='Shop After Hours Contact']/@ReferenceID"/>
<xsl:variable name="EntityPrimaryContactID">
  <xsl:choose>
    <xsl:when test="@EntityType='S'"><xsl:value-of select="Reference[@List='PersonnelType' and @Name='Shop Manager']/@ReferenceID"/></xsl:when>
    <xsl:otherwise><xsl:value-of select="Reference[@List='PersonnelType' and @Name='Owner/Officer #1']/@ReferenceID"/></xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<xsl:variable name="PersonnelCount"  select="count(Personnel)"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID             SP                 XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTPersonnelGetDetailXML,SMTPersonnel.xsl,2025, 'S'   -->

<HEAD>
  <TITLE>Business Personnel</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>


<!-- CLIENT SCRIPTS -->
<xsl:if test="$mode != 'wizard'"><SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT></xsl:if>
<SCRIPT language="JavaScript" src="/js/SMTValid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">

var gsEntityID = '<xsl:value-of select="@EntityID"/>';
var gsEntityType = '<xsl:value-of select="@EntityType"/>';
var gsPersonnelCount = '<xsl:value-of select="$PersonnelCount"/>';
var gsMaxPersonnel = '<xsl:value-of select="$MaxPersonnel"/>';
var gsMode = '<xsl:value-of select="$mode"/>';
var gsPageFile = "SMTPersonnel.asp";
var gsShopCRUD = '<xsl:value-of select="$ShopCRUD"/>';
var gsEntityPrimaryContactID = '<xsl:value-of select="$EntityPrimaryContactID"/>';
var gsPrimaries =  '<xsl:value-of select="count(/Root/Personnel[@PersonnelTypeID=$EntityPrimaryContactID])"/>';

<![CDATA[

function InitPage(){

  	if (gsMode != "wizard")
  		InitEditPage();
  	else{
		  SetWizNames();
    	if (gsEntityType == "B")
  			parent.frameLoaded(2);
		  else
			  parent.frameLoaded(4);
    }

    if (gsMode == "add")
      frmPersonnel.txtName.select();
    else if (gsShopCRUD.indexOf("U") != -1 && gsPersonnelCount > 0)
            frmPersonnel.txtName1.select();

  onpasteAttachEvents()
  //set dirty flags associated with all inputs and selects and checkboxes.
  var aInputs = null;
  var obj = null;
  aInputs = document.all.tags("INPUT");
  for (var i=0; i < aInputs.length; i++) {
    if (aInputs[i].type=="text"){
      aInputs[i].attachEvent("onchange", SetDirty);
	    aInputs[i].attachEvent("onkeypress", SetDirty);
      aInputs[i].attachEvent("onpropertychange", SetDirty);
	  }
        
	  if (aInputs[i].type=="checkbox") aInputs[i].attachEvent("onclick", SetDirty);
	  if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
  }

  aInputs = document.all.tags("SELECT");
  for (var i=0; i < aInputs.length; i++){
    aInputs[i].attachEvent("onchange", SetDirty);
	  if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
  }
}

function InitEditPage(){

	top.gsPageFile = gsPageFile;
	parent.btnDelete.style.display = "none";
  parent.btnCancel.style.display = "none";
  parent.btnNew.style.visibility = "hidden";
  parent.btnSave.style.visibility = "visible";

  if (gsEntityType == "B"){
  	parent.gsPageName = "Business Personnel";
	  tab = parent.tab12;
  }
  else{
  	parent.gsPageName = "Shop Personnel";
	  tab = parent.tab22;
  }

  tab.className = "tabactive1";
  if (gsShopCRUD.indexOf("U") > -1) parent.btnSave.disabled = false;

  if (gsPersonnelCount < gsMaxPersonnel){
    if (gsShopCRUD.indexOf("C") > -1){
      if (gsMode != "add"){
        parent.btnNew.style.visibility = "visible";
        parent.btnNew.style.display = "inline";
      }
      else{
        parent.btnCancel.style.display = "inline";
        parent.btnCancel.style.visibility = "visible";
        parent.btnCancel.disabled = false;
      }
    }
  }
}

function ValidatePhones(){
  var arrDevice = new Array("Phone", "Fax", "Cell", "Pager");
  var sDevice, oAreaCode, oExchange, oUnit;
  var sXPath, oNode;
  var iLimit;

  switch(gsMode){
    case "wizard":
      iLimit = gsMaxPersonnel;
      break;
    case "add":
      iLimit = 1;
      break;
    default:
      iLimit = gsPersonnelCount;
      break;
  }

  for (i=1; i<=iLimit; i++){
    var iPersonnelNum = gsMode == "add" ? "" : i;
    var objDelete = eval("frmPersonnel.txtDeletePersonnel" + iPersonnelNum)

    if (objDelete && objDelete.value == 0){
      for (j=0; j<arrDevice.length; j++){

        sDevice = "frmPersonnel.txt" + arrDevice[j];
        oAreaCode = eval(sDevice + "AreaCode" + iPersonnelNum);
        oExchange = eval(sDevice + "ExchangeNumber" + iPersonnelNum);
        oUnit = eval(sDevice + "UnitNumber" + iPersonnelNum);

        if (validatePhoneSections(oAreaCode, oExchange, oUnit) == false) return false;

        try{
          sXPath = '//Reference[@List="AreaCode"][@Name="' + oAreaCode.value + '"]';
          oNode = xmlReference.documentElement.selectSingleNode(sXPath);
          if (oNode) validateAreaCodeSplit(oAreaCode, oExchange);
        }
        catch(e){
          parent.ClientWarning("There was a problem validating the contact numbers.  Plese try again.")
          return false;
        }
      }
    }
  }
  return true;
}

function SetDirty(){
	parent.gbDirtyFlag = true;
}

function IsDirty(){
	Save();
}

function ValidatePage(){
  // only call this function from the wizards
  if (frmPersonnel.txtName1.value == ""){
	  parent.ClientWarning("A name is required for 'Contact #1'.");
		frmPersonnel.txtName1.focus();
		return false;
	}
  var oText;
  var oSel;

  for (i=1; i<=gsMaxPersonnel; i++){
    oText = eval("frmPersonnel.txtName" + i)
    if (oText.value.replace(/\s/g, "") == "")
      oText.value = "";

    oSel = eval("frmPersonnel.selPersonnelType" + i);
    if (oText.value != "" && oSel.selectedIndex < 1){
      parent.ClientWarning("You must select a contact type for  'Contact #" + i + "'.");
		  oSel.focus();
		  return false;
    }
  }

  if (!CheckPersonnelType()){
    return false;
  }

  if (ValidatePhones() == false) return false;

  return true;
}

function LocalValidatePage(){
  var iNum;
  var iCheckCount = gsMode == "add" ? 1 : gsPersonnelCount;
    
  if (!ValidatePhones()) return false;
  if (!CheckPersonnelType()) return false;
  if (gsMode != "add" && gsShopCRUD.indexOf("D") > -1) if (!CheckDeletes()) return false;
  
  for (i=1; i<=iCheckCount; i++){
    iNum = gsMode == "add" ? "" : i;
    obj = eval("frmPersonnel.txtName" + iNum);
    if (obj.value.replace(/\s/g, "") == ""){
      obj.value = "";
      parent.ClientWarning("Please enter a Name for this contact.");
		  obj.focus();
      return false;
    }

    obj = eval("frmPersonnel.selPersonnelType" + iNum);
    if (obj.selectedIndex < 1){
      parent.ClientWarning("Please select a Contact Type for this contact.");
		  obj.focus();
      return false;
    }
    
    var objDelete = eval("frmPersonnel.txtDeletePersonnel" + iNum)

    if (objDelete && objDelete.value == 0){
      var oSel = eval("frmPersonnel.selPreferredContactMethodID" + iNum);
          
      switch (oSel.options[oSel.selectedIndex].text){
        case "Email":
          oText = eval("frmPersonnel.txtEmailAddress" + iNum);
          if (oText.value == ""){  
            parent.ClientWarning("You have selected a Contact Type of Email.  Please provide an Email Address.");
            oText.focus();
            return false;
          }
          break;
        case "Phone":
          oText = eval("frmPersonnel.txtPhoneAreaCode" + iNum);
          if (oText.value == ""){  
            parent.ClientWarning("You have selected a Contact Type of Phone.  Please provide a Phone Number.");
            oText.focus();
            return false;
          }
          break;
        case "Fax":
          oText = eval("frmPersonnel.txtFaxAreaCode" + iNum);
          if (oText.value == ""){  
            parent.ClientWarning("You have selected a Contact Type of Fax.  Please provide a Fax Number.");
            oText.focus();
            return false;
          }
          break;
        case "Cell Phone":
          oText = eval("frmPersonnel.txtCellAreaCode" + iNum);
          if (oText.value == ""){  
            parent.ClientWarning("You have selected a Contact Type of Cell.  Please provide a Cell Number.");
            oText.focus();
            return false;
          }
          break;
        case "Pager":
          oText = eval("frmPersonnel.txtPagerAreaCode" + iNum);
          if (oText.value == ""){  
            parent.ClientWarning("You have selected a Contact Type of Pager.  Please provide a Pager Number.");
            oText.focus();
            return false;
          }
          break;
      }
    }
  }
  return true;
}

function Save(){
  if (!LocalValidatePage()) return false;
  parent.btnSave.disabled = true;
	if (gsMode == "add") gsMode = "insert";

  frmPersonnel.action += "?mode=" + gsMode + "&EntityID=" + gsEntityID + "&EntityType=" + gsEntityType;
  frmPersonnel.submit();
	parent.gbDirtyFlag = false;	
}

function CheckPersonnelType(){
  var obj;
  var count = 0;
  var sMsg;

  var iPersonnel = gsMode == "wizard" ? gsMaxPersonnel : gsPersonnelCount;

  if (gsMode != "add"){
    for (i=1; i<=iPersonnel; i++){
      obj = eval("frmPersonnel.selPersonnelType" + i);
      if (obj.value == gsEntityPrimaryContactID)
        count++;
    }
  }
  else
    count = frmPersonnel.selPersonnelType.value == gsEntityPrimaryContactID ? parseInt(gsPrimaries) + 1 : gsPrimaries;

  if (count != 1){
    var sContactType = gsEntityType == "S" ? "Shop Manager" : "Owner/Officer #1";
    sMsg = gsMode == "add" ? "There is already a person selected to be '" + sContactType + "'.  Please select a different contact type." :
                             "Exactly one person must be specified as '" + sContactType + "'.  Please correct this and try again."
    parent.ClientWarning(sMsg);
    if (gsMode == "add") frmPersonnel.selPersonnelType.focus();
    if (gsMode != "wizard") parent.btnSave.disabled = false;
    return false;
  }
  return true;
}

function CheckDeletes(){
  var obj;
  var sMsg;
  var count = 0;
  var bDeletePrimary = false;

  if (gsPersonnelCount > 1){
    for (i=1; i<=gsPersonnelCount; i++){
      obj = eval("frmPersonnel.cbDeletePersonnel" + i)
      if (obj.checked == true){
        count++;
        obj = eval("frmPersonnel.selPersonnelType" + i);
        if (obj.value == gsEntityPrimaryContactID)
          bDeletePrimary = true;
      }
    }
  }

  if (count > 0){
    if (gsEntityType == "S" && bDeletePrimary == true){
      sMsg = "You cannot remove the Primary Contact.  Please be sure the person marked 'Primary Contact' is not also checked for removal."
      parent.ClientWarning(sMsg);
      return false;
    }

    if (count == gsPersonnelCount){
      sMsg = "You cannot delete all personnel.  Please uncheck 'Remove Contact' for at least one personnel and try again.";
      parent.ClientWarning(sMsg);
      return false;
    }

    sMsg = "Are you sure you want to delete " + count + " personnel?  Deletes cannot be undone.  Click 'Yes' to delete.";
    sMsg += "  If you click 'No', no changes will be saved.";
    var sDelete = YesNoMessage("Personnel", sMsg)
    if (sDelete != "Yes")
      return false;
  }
  return true;
}

function selContactMethod_onchange(obj){
  try{
    var iPosition = gsMode != "add" ? obj.id.slice(-1) : "";
    var sMethod = obj.options[obj.selectedIndex].text;
    if (sMethod == "Cell Phone") sMethod = "Cell";
    
    eval("document.all.spEmail" + iPosition).style.visibility = "hidden";
    eval("document.all.spPhone" + iPosition).style.visibility = "hidden";
    eval("document.all.spFax" + iPosition).style.visibility = "hidden";
    eval("document.all.spPager" + iPosition).style.visibility = "hidden";
    eval("document.all.spCell" + iPosition).style.visibility = "hidden";
    if (sMethod == "Email" || sMethod == "Phone" || sMethod == "Fax" || sMethod == "Cell" || sMethod == "Pager")
      eval("document.all.sp" + sMethod + iPosition).style.visibility = "visible";
  }
  catch(e){}
}

function Add(){
  if (parent.CheckDirty()){
    parent.btnSave.disabled = true;
    parent.btnNew.style.visibility = "hidden";
    frmPersonnel.action += "?mode=add";
    frmPersonnel.submit();
  }
}

function Cancel(){
  parent.gbDirtyFlag = false;
  parent.btnCancel.disabled = true;
  parent.btnSave.disabled = true;
  window.navigate("SMTPersonnel.asp?EntityID=" + gsEntityID + "&EntityType=" + gsEntityType);
}

]]>

<xsl:if test="$mode='wizard'">
	function SetWizNames(){
		for (i=1; i &lt;= gsMaxPersonnel; i++){
			eval("frmPersonnel.txtPhoneAreaCode" + i).setAttribute("wizname", gsEntityType + "PPhoneAreaCode" + i);
			eval("frmPersonnel.txtPhoneExchangeNumber" + i).setAttribute("wizname", gsEntityType + "PPhoneExchangeNumber" + i);
			eval("frmPersonnel.txtPhoneUnitNumber" + i).setAttribute("wizname", gsEntityType + "PPhoneUnitNumber" + i);
			eval("frmPersonnel.txtPhoneExtensionNumber" + i).setAttribute("wizname", gsEntityType + "PPhoneExtensionNumber" + i);
			eval("frmPersonnel.txtFaxAreaCode" + i).setAttribute("wizname", gsEntityType + "PFaxAreaCode" + i);
			eval("frmPersonnel.txtFaxExchangeNumber" + i).setAttribute("wizname", gsEntityType + "PFaxExchangeNumber" + i);
			eval("frmPersonnel.txtFaxUnitNumber" + i).setAttribute("wizname", gsEntityType + "PFaxUnitNumber" + i);
			eval("frmPersonnel.txtFaxExtensionNumber" + i).setAttribute("wizname", gsEntityType + "PFaxExtensionNumber" + i);
			eval("frmPersonnel.txtCellAreaCode" + i).setAttribute("wizname", gsEntityType + "PCellAreaCode" + i);
			eval("frmPersonnel.txtCellExchangeNumber" + i).setAttribute("wizname", gsEntityType + "PCellExchangeNumber" + i);
			eval("frmPersonnel.txtCellUnitNumber" + i).setAttribute("wizname", gsEntityType + "PCellUnitNumber" + i);
			eval("frmPersonnel.txtCellExtensionNumber" + i).setAttribute("wizname", gsEntityType + "PCellExtensionNumber" + i);
			eval("frmPersonnel.txtPagerAreaCode" + i).setAttribute("wizname", gsEntityType + "PPagerAreaCode" + i);
			eval("frmPersonnel.txtPagerExchangeNumber" + i).setAttribute("wizname", gsEntityType + "PPagerExchangeNumber" + i);
			eval("frmPersonnel.txtPagerUnitNumber" + i).setAttribute("wizname", gsEntityType + "PPagerUnitNumber" + i);
			eval("frmPersonnel.txtPagerExtensionNumber" + i).setAttribute("wizname", gsEntityType + "PPagerExtensionNumber" + i);
		}
  }
</xsl:if>

</SCRIPT>
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" onLoad="InitPage();" bgcolor="#FFFAEB" style="overflow:hidden; margin-left:0; margin-right:0; margin-top:0;">

  <!-- Impact Type XML Data -->
  <xml id="xmlReference"><Root><xsl:copy-of select="/Root/Reference[@List='AreaCode']"/></Root></xml>

  <div style="background-color:#FFFAEB; height:475px;">
    <xsl:variable name="max">
      <xsl:choose>
	      <xsl:when test="@EntityType = 'B'">2</xsl:when>
	      <xsl:otherwise>4</xsl:otherwise>
	    </xsl:choose>
    </xsl:variable>

    <FORM id="frmPersonnel" method="post" style="position:absolute; overflow:hidden;">
      <input type="hidden" id="txtCallBack" name="CallBack"/>
      <input type="hidden" name="EntityID" id="txtEntityID">
	      <xsl:attribute name="value"><xsl:value-of select="@EntityID"/></xsl:attribute>
      </input>

	    <input type="hidden" name="EntityType" id="txtEntityType">
	      <xsl:attribute name="value"><xsl:value-of select="@EntityType"/></xsl:attribute>
      </input>

      <input type="hidden" name="SysLastUserID" id="SysLastUserID">
	      <xsl:attribute name="value"><xsl:value-of select="$UserID"/></xsl:attribute>
      </input>
      
      <xsl:choose>
        <xsl:when test="$PersonnelCount=0 and $mode='update'">
          <div style="position:relative; top:0px; left:0px; height:20; width:300; color:red;">
            There are currently no personnel for this<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:choose>
              <xsl:when test="@EntityType='S'">Shop</xsl:when>
              <xsl:otherwise>Business</xsl:otherwise>
            </xsl:choose>.  
          </div>       
        </xsl:when>
        <xsl:when test="$mode='add'">
          <xsl:call-template name="Personnel">
            <xsl:with-param name="EntityType" select="@EntityType"/>
            <xsl:with-param name="mode" select="$mode"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="$mode='wizard'">
          <xsl:call-template name="Personnel">
            <xsl:with-param name="EntityType" select="@EntityType"/>
            <xsl:with-param name="mode" select="$mode"/>
            <xsl:with-param name="max" select="$max"/>
            <xsl:with-param name="iterator">1</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:for-each select="Personnel">
            <xsl:call-template name="Personnel">
              <xsl:with-param name="EntityType" select="@EntityType"/>
            </xsl:call-template>
	        </xsl:for-each>
        </xsl:otherwise>
      </xsl:choose>
    </FORM>

  </div>

  <SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

</BODY>
</HTML>
</xsl:template>


<xsl:template name="Personnel">
  <xsl:param name="EntityType"/>
  <xsl:param name="mode"/>
  <xsl:param name="max"/>
  <xsl:param name="iterator"/>

  <xsl:variable name="PersonnelID" select="@PersonnelID"/>
  <xsl:variable name="ContactMethodID" select="@PreferredContactMethodID"/>
  
  <xsl:variable name="Position">
    <xsl:choose>
      <xsl:when test="$mode = 'wizard'"><xsl:value-of select="$iterator"/></xsl:when>
      <xsl:when test="$mode != 'add'"><xsl:value-of select="position()"/></xsl:when>
    </xsl:choose>
  </xsl:variable>

  <IMG src="/images/spacer.gif" width="30" height="7" border="0" />
  <br/>
  <IMG src="/images/background_top_flip.gif" width="675" height="2" border="0" />
  <br/>
	<IMG src="/images/spacer.gif" width="30" height="7" border="0" />

	<input type="hidden">
    <xsl:attribute name="id">txtSysLastUpdatedDate<xsl:value-of select="$Position"/></xsl:attribute>
    <xsl:attribute name="name">SysLastUpdatedDate<xsl:value-of select="$Position"/></xsl:attribute>
    <xsl:attribute name="value"><xsl:value-of select="@SysLastUpdatedDate"/></xsl:attribute>
	</input>

	<input type="hidden">
    <xsl:attribute name="id">txtrelationSysLastUpdatedDate<xsl:value-of select="$Position"/></xsl:attribute>
    <xsl:attribute name="name">RelationSysLastUpdatedDate<xsl:value-of select="$Position"/></xsl:attribute>
    <xsl:attribute name="value"><xsl:value-of select="@RelationSysLastUpdatedDate"/></xsl:attribute>
	</input>

	<input type="hidden">
    <xsl:attribute name="id">txtPersonnelID<xsl:value-of select="$Position"/></xsl:attribute>
    <xsl:attribute name="name">PersonnelID<xsl:value-of select="$Position"/></xsl:attribute>
    <xsl:attribute name="value"><xsl:value-of select="@PersonnelID"/></xsl:attribute>
  </input>

	<input type="hidden">
    <xsl:attribute name="id">txtGenderCD<xsl:value-of select="$Position"/></xsl:attribute>
    <xsl:attribute name="name">GenderCD<xsl:value-of select="$Position"/></xsl:attribute>
    <xsl:attribute name="wizname"><xsl:value-of select="$EntityType"/>PGenderCD<xsl:value-of select="$Position"/></xsl:attribute>
	  <xsl:attribute name="value"><xsl:value-of select="@GenderCD"/></xsl:attribute>
	</input>
      
	<input type="hidden">
	  <xsl:attribute name="id">txtMinorityFlag<xsl:value-of select="$Position"/></xsl:attribute>
    <xsl:attribute name="name">MinorityFlag<xsl:value-of select="$Position"/></xsl:attribute>
    <xsl:attribute name="wizname"><xsl:value-of select="$EntityType"/>PMinorityFlag<xsl:value-of select="$Position"/></xsl:attribute>
	  <xsl:attribute name="value"><xsl:value-of select="@MinorityFlag"/></xsl:attribute>
	</input>
	  
	
  <table cellSpacing="0" width="775px" cellPadding="1" border="0">
    <colgroup>
      <col width="10px"/>
      <col width="60px"/>
      <col width="290px"/>
      <col width="10px"/>
      <col width="10px"/>
      <col width="85px"/>
      <col width="310px"/>
    </colgroup>
    <tr>
      <td><span style="font-weight:bold;">*</span></td>
      <td>Name:</td>
      <td>
        <input type="text" size="51" class="inputFld" autocomplete="off"> 
          <xsl:attribute name="id">txtName<xsl:value-of select="$Position"/></xsl:attribute>
          <xsl:attribute name="name">Name<xsl:value-of select="$Position"/></xsl:attribute>
		      <xsl:attribute name="wizname"><xsl:value-of select="$EntityType"/>PName<xsl:value-of select="$Position"/></xsl:attribute>
		      <xsl:attribute name="maxlength"><xsl:value-of select="/Root/Metadata[@Entity='Personnel']/Column[@Name='Name']/@MaxLength"/></xsl:attribute>
		      <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
		    </input>
      </td>
      <td></td>
      <td><span style="font-weight:bold;">*</span></td>
      <td>Contact Type:</td>
      <td>
        <table border="0">
          <tr>
            <td>
              <select style="width:150">
                <xsl:attribute name="id">selPersonnelType<xsl:value-of select="$Position"/></xsl:attribute>
                <xsl:attribute name="name">PersonnelTypeID<xsl:value-of select="$Position"/></xsl:attribute>
		            <xsl:attribute name="wizname"><xsl:value-of select="$EntityType"/>PPersonnelTypeID<xsl:value-of select="$Position"/></xsl:attribute>          
                <option></option>
                <xsl:for-each select="/Root/Reference[@List='PersonnelType' and @AppliesTo=/Root/@EntityType]">
                  <xsl:call-template name="BuildSelectOptions">
                    <xsl:with-param name="current">
                      <xsl:choose>
                        <xsl:when test="$mode='wizard' and $iterator=1">
                          <xsl:choose>
                            <xsl:when test="$EntityType='B'">5</xsl:when>
                            <xsl:when test="$EntityType='S'">2</xsl:when>
                          </xsl:choose>  
                        </xsl:when>
                        <xsl:otherwise><xsl:value-of select="/Root/Personnel[@PersonnelID=$PersonnelID]/@PersonnelTypeID"/></xsl:otherwise>
                      </xsl:choose>
                    </xsl:with-param>
                  </xsl:call-template>
                </xsl:for-each>  
              </select>
            </td>
            <td width="15"></td>
            <!--
            <td width="45"><xsl:if test="/Root/@EntityType = 'S'">Manager:</xsl:if></td>
            <td>
              <xsl:if test="/Root/@EntityType = 'S'">
                <input type="checkbox" tabindex="-1">
                  <xsl:attribute name="id">cbShopManagerFlag<xsl:value-of select="$Position"/></xsl:attribute>
                  <xsl:attribute name="onclick">txtShopManagerFlag<xsl:value-of select="$Position"/>.value=this.checked?1:0</xsl:attribute>
		              <xsl:if test="@ShopManagerFlag='1'"><xsl:attribute name="checked"/></xsl:if>  
		            </input>
              </xsl:if>
              <input type="hidden" class="inputFld">
                <xsl:attribute name="id">txtShopManagerFlag<xsl:value-of select="$Position"/></xsl:attribute>
                <xsl:attribute name="name">ShopManagerFlag<xsl:value-of select="$Position"/></xsl:attribute>
		            <xsl:attribute name="wizname"><xsl:value-of select="$EntityType"/>PShopManagerFlag<xsl:value-of select="$Position"/></xsl:attribute>
		            <xsl:attribute name="value">
                  <xsl:choose>
                    <xsl:when test="@ShopManagerFlag != ''"><xsl:value-of select="@ShopManagerFlag"/></xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
		          </input>  
            </td>
            -->
            <td></td>
            <td>
              <input type="hidden" class="inputFld">
                <xsl:attribute name="id">txtShopManagerFlag<xsl:value-of select="$Position"/></xsl:attribute>
                <xsl:attribute name="name">ShopManagerFlag<xsl:value-of select="$Position"/></xsl:attribute>
		            <xsl:attribute name="wizname"><xsl:value-of select="$EntityType"/>PShopManagerFlag<xsl:value-of select="$Position"/></xsl:attribute>
		            <xsl:attribute name="value">
                  <xsl:choose>
                    <xsl:when test="@ShopManagerFlag != ''"><xsl:value-of select="@ShopManagerFlag"/></xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
		          </input>  
            </td>
          </tr>
        </table>       
      </td>
    </tr>
    <tr>
      <td>
        <span>
          <xsl:attribute name="id">spEmail<xsl:value-of select="$Position"/></xsl:attribute>
          <xsl:attribute name="style">
            font-weight:bold;visibility:
            <xsl:choose>
              <xsl:when test="/Root/Reference[@List='ContactMethod' and @ReferenceID=$ContactMethodID]/@Name='Email'">visible</xsl:when>
              <xsl:otherwise>hidden</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          *        
        </span>
      </td>
      <td>Email:</td>
      <td>
	      <input size="51" class="inputFld" onbeforedeactivate="checkEMail(this)">
          <xsl:attribute name="id">txtEmailAddress<xsl:value-of select="$Position"/></xsl:attribute>
          <xsl:attribute name="name">EmailAddress<xsl:value-of select="$Position"/></xsl:attribute>
          <xsl:attribute name="wizname"><xsl:value-of select="$EntityType"/>PEmailAddress<xsl:value-of select="$Position"/></xsl:attribute>
          <xsl:attribute name="maxlength"><xsl:value-of select="/Root/Metadata[@Entity='Personnel']/Column[@Name='EmailAddress']/@MaxLength"/></xsl:attribute>
		      <xsl:attribute name="value"><xsl:value-of select="@EmailAddress"/></xsl:attribute>		      
		    </input>
	    </td>
      <td></td>
      <td></td>
      <td nowrap="nowrap">Contact Method:</td>
      <td>
        <table>
          <tr>
            <td>
              <select style="width:150" onchange="selContactMethod_onchange(this)">
		            <xsl:attribute name="id">selPreferredContactMethodID<xsl:value-of select="$Position"/></xsl:attribute>
                <xsl:attribute name="name">PreferredContactMethodID<xsl:value-of select="$Position"/></xsl:attribute>
                <xsl:attribute name="wizname"><xsl:value-of select="$EntityType"/>PPreferredContactMethodID<xsl:value-of select="$Position"/></xsl:attribute>
                <option></option>
		            <xsl:for-each select="/Root/Reference[@List='ContactMethod']">
                  <xsl:call-template name="BuildSelectOptions">
			              <xsl:with-param name="current" select="/Root/Personnel[@PersonnelID=$PersonnelID]/@PreferredContactMethodID"/>
			            </xsl:call-template>
		            </xsl:for-each>
              </select>
            </td>
            <td width="15"></td>
            <td width="45"><xsl:if test="count(/Root/Personnel) > 1 and $mode != 'add' and contains($ShopCRUD, 'D')">Remove:</xsl:if></td>
            <td>
              <xsl:if test="count(/Root/Personnel) > 1 and $mode != 'add' and contains($ShopCRUD, 'D')">
                <input type="checkbox" tabindex="-1">
                  <xsl:attribute name="id">cbDeletePersonnel<xsl:value-of select="$Position"/></xsl:attribute>
                  <xsl:attribute name="onclick">txtDeletePersonnel<xsl:value-of select="$Position"/>.value=this.checked?1:0</xsl:attribute>
                </input>
                
                <input type="hidden" class="inputFld" value="0">
                  <xsl:attribute name="id">txtDeletePersonnel<xsl:value-of select="$Position"/></xsl:attribute>
                  <xsl:attribute name="name">DeletePersonnel<xsl:value-of select="$Position"/></xsl:attribute>
		            </input>
              </xsl:if>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>
        <span>
          <xsl:attribute name="id">spPhone<xsl:value-of select="$Position"/></xsl:attribute>
          <xsl:attribute name="style">
            font-weight:bold;visibility:
            <xsl:choose>
              <xsl:when test="/Root/Reference[@List='ContactMethod' and @ReferenceID=$ContactMethodID]/@Name='Phone'">visible</xsl:when>
              <xsl:otherwise>hidden</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          *        
        </span>      
      </td>
      <td>Phone:</td>
      <td nowrap="nowrap" align="left">
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Phone</xsl:with-param>
          <xsl:with-param name="ContactNumber"><xsl:value-of select="$Position"/></xsl:with-param>
        </xsl:call-template>
      </td>
      <td></td>
      <td>
        <span>
          <xsl:attribute name="id">spFax<xsl:value-of select="$Position"/></xsl:attribute>
          <xsl:attribute name="style">
            font-weight:bold;visibility:
            <xsl:choose>
              <xsl:when test="/Root/Reference[@List='ContactMethod' and @ReferenceID=$ContactMethodID]/@Name='Fax'">visible</xsl:when>
              <xsl:otherwise>hidden</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          *        
        </span>    
      </td>
      <td>Fax: </td>
      <td nowrap="nowrap">
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Fax</xsl:with-param>
          <xsl:with-param name="ContactNumber"><xsl:value-of select="$Position"/></xsl:with-param>
        </xsl:call-template>
      </td>
	  </tr>
    <tr>
      <td>
        <span>
          <xsl:attribute name="id">spCell<xsl:value-of select="$Position"/></xsl:attribute>
          <xsl:attribute name="style">
            font-weight:bold;visibility:
            <xsl:choose>
              <xsl:when test="/Root/Reference[@List='ContactMethod' and @ReferenceID=$ContactMethodID]/@Name='Cell Phone'">visible</xsl:when>
              <xsl:otherwise>hidden</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          *        
        </span>   
      </td>
      <td>Cell:</td>
      <td>
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Cell</xsl:with-param>
          <xsl:with-param name="ContactNumber"><xsl:value-of select="$Position"/></xsl:with-param>
        </xsl:call-template>
      </td>
      <td></td>
      <td>
        <span>
          <xsl:attribute name="id">spPager<xsl:value-of select="$Position"/></xsl:attribute>
          <xsl:attribute name="style">
            font-weight:bold;visibility:
            <xsl:choose>
              <xsl:when test="/Root/Reference[@List='ContactMethod' and @ReferenceID=$ContactMethodID]/@Name='Pager'">visible</xsl:when>
              <xsl:otherwise>hidden</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          *        
        </span>
      </td>
      <td>Pager: </td>
      <td>
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Pager</xsl:with-param>
          <xsl:with-param name="ContactNumber"><xsl:value-of select="$Position"/></xsl:with-param>
        </xsl:call-template>
      </td>
    </tr>
  </table>
  
  <xsl:if test="$mode='wizard' and $iterator &lt; $max">
    <xsl:call-template name="Personnel">
      <xsl:with-param name="EntityType" select="$EntityType"/>
      <xsl:with-param name="mode" select="$mode"/>
      <xsl:with-param name="max" select="$max"/>
      <xsl:with-param name="iterator" select="$iterator + 1"/>  
    </xsl:call-template>
  </xsl:if>
  
</xsl:template>


</xsl:stylesheet>

