<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ms="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimVehicleList">

<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<ms:script language="JScript" implements-prefix="js">
<![CDATA[

  function MainTabs(ctx, pos, VehNum, ClaimAspectID)
  {
    var strLastName = ctx[0].selectSingleNode("@NameLast").nodeValue;
    var strFirstName = ctx[0].selectSingleNode("@NameFirst").nodeValue;
    var strBusiness = ctx[0].selectSingleNode("@BusinessName").nodeValue;
    var strClosed = ctx[0].selectSingleNode("@ClosedStatus").nodeValue;
    var strStatus = ctx[0].selectSingleNode("@Status").nodeValue;

    var strRet = "<SPAN unselectable='on' id='tab1"+pos+"' class='tab1' onClick='TabChange("+pos+","+VehNum+","+ClaimAspectID+")' vehClosed='" + strClosed + "' >";
    //if (strClosed == "1")
    //    strRet += "<img src='/images/closedExposure.gif' title='Vehicle Closed'/><img src='/images/spacer.gif' style='width:3px;height:1px'/>";
    switch (strStatus){
      case "Vehicle Closed" :
        strRet += "<img src='/images/closedExposure.gif' title='Vehicle Closed'/><img src='/images/spacer.gif' style='width:3px;height:1px'/>";
        break;
      case "Vehicle Cancelled" :
        strRet += "<img src='/images/cancelled.gif' title='Vehicle Cancelled'/><img src='/images/spacer.gif' style='width:3px;height:1px'/>";
        break;
      case "Vehicle Voided" :
        strRet += "<img src='/images/voided.gif' title='Vehicle voided'/><img src='/images/spacer.gif' style='width:3px;height:1px'/>";
        break;
    }
    if (strLastName != "" || strFirstName != "" || strBusiness != "")
        strRet += VehNum;
    else
        strRet += "Veh" + VehNum;

    strFirstName = strFirstName.substr(0,1);
		if (strLastName.length > 15)
			strLastName = strLastName.substr(0,15) + "..";

    if (strBusiness != "")
        if (strBusiness.length > 15)
            strBusiness = strBusiness.substr(0, 15) + "..";
    if (strBusiness != "")
        strRet = strRet + ": " + strBusiness + "</SPAN>";
    else if (strFirstName != "" && strLastName != "")
        strRet = strRet + ": " + strFirstName + ". " + strLastName +"</SPAN>";
    else if (strFirstName != "" || strLastName != "")
        strRet = strRet + ": " + strFirstName + strLastName +"</SPAN>";
    else
        strRet = strRet + "</SPAN>";
    return strRet;
  }

  function ContentTabs(ctx, pos, VehNum)
  {
    var lynxid = ctx[0].nodeValue;
     var sVisible = "hidden";

    var strRet = "<DIV unselectable='on' class='content1' id='content1"+pos+"' style='background-color:#FFFAEB; width:742px; height:492px;'>";
    strRet += "<IFRAME src='blank.asp' name='oIframe"+pos+"' id='oIframe"+pos+"' onreadystatechange='top.IFStateChange(this);'";
    var sVisible = "hidden";
    strRet += "style='border : 0px; visibility:"+sVisible+" ;position:absolute; width:730px; height:480px; background : transparent;overflow:hidden' allowtransparency='true'></IFRAME></DIV>";
    return strRet;
  }

]]>
</ms:script>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="VehicleCRUD" select="Vehicle"/>
<xsl:param name="DocumentCRUD" select="Document"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserId"/>
<xsl:param name="WindowID"/>
<xsl:param name="VehContext"/>
<xsl:param name="ClaimAspectID"/>

<xsl:template match="/">

<xsl:variable name="LynxID" select="/Root/@LynxID" />

<xsl:variable name="InsuranceCompanyName" select="session:XslGetSession('InsuranceCompanyName')"/>
<xsl:variable name="ClientClaimNumber" select="session:XslGetSession('ClientClaimNumber')"/>
<xsl:variable name="InsuredNameFirst" select="session:XslGetSession('InsuredNameFirst')"/>
<xsl:variable name="InsuredNameLast" select="session:XslGetSession('InsuredNameLast')"/>
<xsl:variable name="LossDate" select="session:XslGetSession('LossDate')"/>
<xsl:variable name="CoverageLimitWarningInd" select="session:XslGetSession('CoverageLimitWarningInd')"/>

<xsl:variable name="ClaimRestrictedFlag" select="session:XslGetSession('RestrictedFlag')"/>
<xsl:variable name="ClaimOpen" select="session:XslGetSession('ClaimOpen')"/>
<xsl:variable name="CSRNameFirst" select="session:XslGetSession('OwnerUserNameFirst')"/>
<xsl:variable name="CSRNameLast" select="session:XslGetSession('OwnerUserNameLast')"/>
<xsl:variable name="InsuredBusinessName" select="session:XslGetSession('InsuredBusinessName')"/>
<xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>
<xsl:variable name="DemoFlag" select="session:XslGetSession('DemoFlag')"/>
<xsl:variable name="InsuranceCompanyID" select="session:XslGetSession('InsuranceCompanyID')"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspClaimVehicleGetListXML,ClaimVehicleList.xsl,6366, 145   -->

<HEAD>
<TITLE>Vehicle Info</TITLE>
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<style>
    .toolbarButton {
      width: 15px;
      height: 15px;
      cursor:arrow;
      background-color: transparent;
      border:0px;
    }
    .toolbarButtonOver {
      width: 15px;
      height: 15px;
      cursor:arrow;
      background-color: transparent;
      border-top:1px solid #D3D3D3;
      border-left:1px solid #D3D3D3;
      border-bottom:1px solid #696969;
      border-right:1px solid #696969;
    }

    .toolbarButtonOut {
      width: 15px;
      height: 15px;
      cursor:arrow;
      background-color: transparent;
      border:0px;
    }
</style>

<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<SCRIPT language="JavaScript">

var gsWindowID = "<xsl:value-of select="$WindowID"/>";
var gsLynxID = "<xsl:value-of select="$LynxID"/>";
var gsInsuranceCompanyID = '<xsl:value-of select="$InsuranceCompanyID"/>';
var gsVehCount = "<xsl:value-of select="count(/Root/Vehicle)"/>";
var gsVehContext = "<xsl:value-of select="$VehContext"/>";
var gsClaimAspectID = "<xsl:value-of select="$ClaimAspectID"/>";
var gsClaimID = "<xsl:value-of select="$ClientClaimNumber"/>";
var gsIFname = "<xsl:value-of select="js:cleanString(string($InsuredNameFirst))"/>";
var gsILname = "<xsl:value-of select="js:cleanString(string($InsuredNameLast))"/>";
var gsLossDate = "<xsl:value-of select="js:UTCConvertDate(string($LossDate))"/>";
var gsInsco = "<xsl:value-of select="js:cleanString(string($InsuranceCompanyName))"/>";
var gsClaimRestrictedFlag = "<xsl:value-of select="$ClaimRestrictedFlag"/>";
var gsClaimOpen = "<xsl:value-of select="$ClaimOpen"/>";
var gsCSRNameFirst = '<xsl:value-of select="js:cleanString(string($CSRNameFirst))"/>';
var gsCSRNameLast = '<xsl:value-of select="js:cleanString(string($CSRNameLast))"/>';
var gsIBusiness = "<xsl:value-of select="js:cleanString(string($InsuredBusinessName))"/>";
var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";
var gsVehicleCRUD = "<xsl:value-of select="$VehicleCRUD"/>";
var giVeh1stPartyCount = "<xsl:value-of select="count(/Root/Vehicle[@ExposureCD='1'])"/>";
var giVeh3rdPartyCount = "<xsl:value-of select="count(/Root/Vehicle[@ExposureCD='3'])"/>";
var giVehNAPartyCount = "<xsl:value-of select="count(/Root/Vehicle[@ExposureCD='N'])"/>";
var gsDemoFlag = "<xsl:value-of select="$DemoFlag"/>";
var gsDocumentCRUD = "<xsl:value-of select="$DocumentCRUD"/>";
var gsCoverageLimitWarningInd = "<xsl:value-of select="$CoverageLimitWarningInd"/>";

if (parent.gsCoverageLimitWarningInd)
  parent.gsCoverageLimitWarningInd = "<xsl:value-of select="$CoverageLimitWarningInd"/>";

<!-- Get the claimAspectID for the vehContext -->
<xsl:if test="$VehContext!=''">
gsClaimAspectID = "<xsl:value-of select="/Root/Vehicle[@VehicleNumber=$VehContext]/@ClaimAspectID"/>";
</xsl:if>

<![CDATA[

var gsVehNumIndex = 1;
var gsVehNum = 0;
var gbVehClosed = false;

function TabChange(index, VehNum, ClaimAspectID)
{
  gsVehNumIndex = index;
  gsVehNum = VehNum;

  //scroll the tab list automatically
  var curTab = document.all["tab1" + index];
  tabs1.scrollLeft = 0;
  if (curTab && tabScroll.style.display == "inline")
    if (((curTab.offsetLeft + curTab.offsetWidth) >= tabs1.offsetWidth))
        curTab.scrollIntoView(true);
  
  top.sVehicleName = curTab.innerText;
  
  if (curTab && curTab.getAttribute("vehClosed") == "1")
    gbVehClosed = true;
  else
    gbVehClosed = false;


  document.frames[index-1].frameElement.style.visibility="visible";
  for (var x=1; x <= gsVehCount; x++)
  {
    if (x != index)
    {
      document.frames[x-1].frameElement.style.visibility="hidden";
      document.frames[x-1].frameElement.src = "blank.asp";
    }
    else
    {
      document.frames[index-1].frameElement.src = "ClaimVehicle.asp?WindowID=" + gsWindowID + "&LynxID="+gsLynxID+"&ClaimAspectID="+ClaimAspectID;
      top.setSB(0,top.sb);
    }
  }
}

function VehicleDelete()
{
  try
  { document.frames[gsVehNumIndex-1].ADS_Pressed("Delete", "content11");
  }
  catch(e)
  { //ClientError( "ClaimVehicleList.xsl::VehicleDelete() " + e.message ); 
  }
}

//Notification from APDSelect.asp that the main tabs have changed
function ParentMainTabChange()
{
	document.frames[gsVehNumIndex-1].MainTabChange();
}


function PageInit()
{
  try
  {
    top.setClaimHeader(gsCSRNameFirst, gsCSRNameLast, gsLynxID, gsClaimID, gsIFname, gsILname, gsLossDate, gsInsco, gsClaimRestrictedFlag, gsClaimOpen, gsClaimStatus, gsIBusiness, gsDemoFlag, gsInsuranceCompanyID);
    top.SetMainTab(2);
    if (gsClaimStatus == "Claim Closed" ||
        gsClaimStatus == "Claim Cancelled" ||
        gsClaimStatus == "Claim Voided"){
      top.VehiclesMenu(false,2);
      top.VehiclesMenu(false,3);
    } else {
      top.VehiclesMenu(true,2);
      top.VehiclesMenu(true,3);
    }

    if (gsVehicleCRUD.indexOf("C") == -1) top.VehiclesMenu(false,2);
    if (gsVehicleCRUD.indexOf("D") == -1) top.VehiclesMenu(false,3);

    if (gsDocumentCRUD.indexOf("C") == -1)
      top.DocumentsMenu(false, 1);
    
    top.giVeh1stPartyCount = giVeh1stPartyCount;
    top.gaVehClaimantCount = new Array();

    var objTabLast = tabs1.lastChild;
    if ((objTabLast.offsetLeft + objTabLast.offsetWidth) > tabs1.offsetWidth) {
      tabScroll.style.display = "inline";
    }

    var index = 1;
    var oTbl = document.getElementById("tblVehCross");
    if (gsClaimAspectID != "")
    {
      var x
      for(x=0; x < gsVehCount; x++)
      {
        if (oTbl.rows[x].cells[1].innerText == gsVehContext)
          break;
      }
      index = oTbl.rows[x].cells[0].innerText;
      TabChange(index, gsVehContext, gsClaimAspectID);
    }
    else
      TabChange(index, oTbl.rows[0].cells[1].innerText, oTbl.rows[0].cells[2].innerText);

    var objTab = document.getElementById("tab1"+index);
    objTab.depressTab();

    top.setSB(100,top.sb);
  }
  catch ( e ) { ClientError( "ClaimVehicleList.xsl::PageInit() " + e.message ); }
}

function PageUnload()
{
  top.VehiclesMenu(false,2);
  top.VehiclesMenu(false,3);
}

function updateTabContent(strFirstName, strLastName, strBusiness){
    var objCurTab = document.all["tab1" + gsVehNumIndex];
    if (objCurTab) {
        strFirstName = strFirstName.substr(0,1);
		if (strLastName.length > 15)
			strLastName = strLastName.substr(0,15) + "..";

        if (strBusiness != "")
            if (strBusiness.length > 15)
                strBusiness = strBusiness.substr(0, 15) + "..";
        if (strBusiness != "")
            objCurTab.innerText = gsVehNum + ": " + strBusiness;
        else if (strFirstName != "" && strLastName != "")
            objCurTab.innerText = gsVehNum + ": " + strFirstName + ". " + strLastName;
        else if (strFirstName != "" || strLastName != "")
            objCurTab.innerText = gsVehNum + ": " + strFirstName + strLastName;
        else
            objCurTab.innerText = "Veh " + gsVehNum;
    }
}

if (document.attachEvent)
  document.attachEvent("onclick", top.hideAllMenuScriptlets);
else
  document.onclick = top.hideAllMenuScriptlets;

]]>
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSub" unselectable="on" onLoad="tabInit(false);PageInit();" onUnload="PageUnload();" tabIndex="-1">
    <xsl:if test="not(contains($VehicleCRUD, 'C'))">
        <script language="javascript">
            setTimeout("top.VehiclesMenu(false, 2)", 1); //disable the add vehicle menu.
        </script>
    </xsl:if>
    <xsl:if test="not(contains($VehicleCRUD, 'D'))">
        <script language="javascript">
            setTimeout("top.VehiclesMenu(false, 3)", 1); //disable the remove vehicle menu.
        </script>
    </xsl:if>
    <!-- Start Tabs -->
      <DIV unselectable="on" style="position:absolute; visibility: visible; top: 0px; left: 0px;">
        <xsl:if test="boolean(/Root/Vehicle)">
          <div id="tabScroll" style="position:absolute;top:3px; left:710px;width:35px;z-index:200;display:none">
            <button class="toolbarButton" tabIndex="-1" onmouseover="this.className='toolbarButtonOver'" onmouseout="this.className='toolbarButtonOut'" onmousedown="tabs1.doScroll('pageleft');this.fireEvent('onblur');"><span style="font:10pt Marlett">3</span></button>
            <button class="toolbarButton" tabIndex="-1" onmouseover="this.className='toolbarButtonOver'" onmouseout="this.className='toolbarButtonOut'" onmousedown="tabs1.doScroll('pageright');this.fireEvent('onblur');"><span style="font:10pt Marlett">4</span></button>
          </div>
          <DIV unselectable="on" id="tabs1" class="apdtabs" style="width:700px;overflow:hidden;top:-1px;">
            <SPAN id="tab10" unselectable="on" style="width:0px;height:0px;"></SPAN>
            <xsl:for-each select="/Root/Vehicle">
              <xsl:sort data-type="number" select="@VehicleNumber" order="ascending" />
              <xsl:value-of disable-output-escaping="yes" select="js:MainTabs(., position(), string(@VehicleNumber), string(@ClaimAspectID))"/>
            </xsl:for-each>
          </DIV>  <!-- Start Veh1 Info -->

          <xsl:for-each select="/Root/Vehicle"  >
            <xsl:sort data-type="number" select="@VehicleNumber" order="ascending" />
            <xsl:value-of disable-output-escaping="yes" select="js:ContentTabs(/Root/@LynxID, position(), string(@VehicleNumber))"/>
          </xsl:for-each>

        </xsl:if>
      </DIV>
    <!-- End Tabs -->

    <TABLE id="tblVehCross" style="display:none">
      <xsl:for-each select="/Root/Vehicle"  >
      <xsl:sort data-type="number" select="@VehicleNumber" order="ascending" />
      <TR>
        <TD style="display:none"><xsl:value-of select="position()"/></TD>
        <TD style="display:none"><xsl:value-of select="@VehicleNumber"/></TD>
        <TD style="display:none"><xsl:value-of select="@ClaimAspectID"/></TD>
      </TR>
      </xsl:for-each>
    </TABLE>

</BODY>
</HTML>

</xsl:template>

</xsl:stylesheet>
