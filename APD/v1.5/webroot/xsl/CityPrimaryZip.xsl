<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="CityPrimaryZip">

<xsl:import href="msxsl/msxsl-function-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />


<xsl:template match="/Root">
  
<HTML>
<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspShopZipByCityStateXML,CityPrimaryZip.xsl, 'fort', 'FL'   -->
<HEAD>

<TITLE>Please select a city</TITLE>

<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
 
  
<SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>        
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<SCRIPT language="JavaScript">
  
	  
  <![CDATA[
     
  function OK(){
    window.returnValue = selCity.value;
    window.close();
  }
  
  function Cancel(){
    window.returnValue = "cancel";
    window.close();    
  }

  ]]>
  </SCRIPT>

</HEAD>

<BODY class="bodyAPDSub" unselectable="on" style="background:transparent; border:0px; overflow:hidden;background-color:#FFFFFF;padding:0px;margin:5px;">
  
<DIV unselectable="on" id="ShopLayout" style="position:absolute; left:0px; top:0px">
  
  <TABLE unselectable="on" width="400" class="ShopAssignTableSrh" border="1" cellspacing="0" cellpadding="1">
    <TR unselectable="on">
      <TD unselectable="on" nowrap="nowrap" align="center">
         <select id="selCity" size="10" style="width:350">
           <xsl:for-each select="City">
             <option>
               <xsl:attribute name="value"><xsl:value-of select="@PrimaryZip"/></xsl:attribute>
               <xsl:value-of select="@City"/>,<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@State"/>
             </option>
           </xsl:for-each>
         </select>
      </TD>
    </TR>
    <TR>
      <TD unselectable="on" align="center">
        <IE:APDButton id="btnCancel" name="btnCancel" value="Cancel" width="60" onButtonClick="Cancel()"/>
        <IE:APDButton id="btnOK" name="btnOK" value="OK" width="60" onButtonClick="OK()"/>
      </TD>
    </TR>
  </TABLE>

  
<img src="/images/spacer.gif" width="1" height="3" border="0"/><br />

<SPAN class="boldtext" id="errormsg" style="position:absolute; left:10px; width:680px; height:30px; padding-top:20px;"></SPAN>


</DIV>




</BODY>
</HTML>

</xsl:template>
 
</xsl:stylesheet>
