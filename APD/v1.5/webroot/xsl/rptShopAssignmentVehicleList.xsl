<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt">

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">
    <xsl:choose>
        <xsl:when test="@ErrorDesc[.!='']">
            <error/><xsl:value-of select="@ErrorDesc"/>
        </xsl:when>
        <xsl:otherwise>
            <select id="selVehicleNumber" name="selVehicleNumber">
            <xsl:for-each select="/Root/Vehicle" >
                <option><xsl:value-of select="@VehicleNumber"/></option>
            </xsl:for-each>
            </select>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>
</xsl:stylesheet>