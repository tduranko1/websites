<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="PMDMarketAnalysis">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="ClaimCRUD" select="Claim"/>
<xsl:param name="ShopCRUD" select="Shop"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDMarketAnalysisGetListXML,PMDMarketAnalysis.xsl,35   -->

<HEAD>
  <TITLE>Program Manager Desktop</TITLE>
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  
  <style>
  	A{
		  color:blue
	  }
	  A:hover{
		  color : #B56D00;
		  font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
		  font-weight : bold;
		  font-size : 10px;
		  cursor : hand;
	  }
  </style>
  
  <!-- CLIENT SCRIPTS -->
<!--   <SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT> -->
  <SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>
  
  <SCRIPT language="JavaScript" src="/js/PMDNavigate.js"></SCRIPT>
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
  <script type="text/javascript"> 
            document.onhelp=function(){  
  		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,23);
  		  event.returnValue=false;
  		  };	
  </script>

  <xsl:variable name="nonZeroAssignmentUsers">
  <xsl:for-each select="ProgramManager">
    <xsl:variable name="curUserID"><xsl:value-of select="@UserID"/></xsl:variable>
    <xsl:variable name="assignmentCount"><xsl:value-of select="count(/Root/ActiveQueue[@ProgramManagerUserID=$curUserID])"/></xsl:variable>
    <xsl:if test="$assignmentCount &gt; 0"><xsl:value-of select="$curUserID"/>,</xsl:if>
  </xsl:for-each>
  </xsl:variable>
  
  <!-- Page Specific Scripting -->
  <SCRIPT language="JavaScript">
  var gsProgramManagerUserID = '<xsl:value-of select="@ProgramManagerUserID"/>';
  var bDataAvailable = false;
  var recCount = 0;
  var iCurPage = 0;
  var iPgCount = 0;
  var iPageSize = 25;
  var iTblCountReady = 0;
  var sMgrIDs = "<xsl:value-of select="$nonZeroAssignmentUsers"/>";
  var aMgrIDs = sMgrIDs.split(",");
  var oCurRow = null;
  var oShopPopup = window.createPopup();
  var gsShopCRUD = "<xsl:value-of select="$ShopCRUD"/>"; 
  var gsClaimCRUD = "<xsl:value-of select="$ClaimCRUD"/>"; 
  
  <![CDATA[

  //init the table selections, must be last
  function initPage(){
    if ((aMgrIDs.length < 2) && (typeof(parent.hideProgress) == "function"))
      parent.hideProgress();
    //setHeight();
    divScroll.focus();
  }
  
  function sortTable(oCol, sSortFldName, sDataType){
    if (typeof(parent.showProgress) == "function")
      parent.showProgress("Sorting data. Please wait...");
    window.setTimeout("sortTable2(" + oCol.uniqueID + ", '" + sSortFldName + "', '" + sDataType + "')", 100);
  }
  //update the selected elements from the table row
    function sortTable2(oCol, sSortFldName, sDataType) {
      var oTbls = document.getElementsByTagName("table");
      var oDataTbls = new Array();
      iTblCountReady = 0;
      for (var i = 0; i < oTbls.length; i++){
        if (oTbls[i].dataSrc != "")
          oDataTbls.push(oTbls[i]);
      }
      if (oDataTbls && oDataTbls.length > 0){
        var oTD = oCol;
        var sSortOrder = (oTD ? oTD.getAttribute("sortOrder") : "ascending");
        if (sSortOrder == "ascending")
           sSortOrder = "descending";
        else
           sSortOrder = "ascending";
        for (var i = 0; i < oDataTbls.length; i++){
          var oXML = null;
          var oTbl = oDataTbls[i];
          if (oTbl.rows.length == 0) {continue};
          if (oTbl) {
            sXMLSrc = oTbl.dataSrc;
            sXMLSrc = sXMLSrc.substr(1);
          }
          if (sXMLSrc != "")
            oXML = document.getElementById(sXMLSrc);
          
            if (sSortFldName != "" && oXML && oTbl) {
      
                bSorting = true;
                
                var sFldName = sSortFldName;
                   
                if (sSortFldName == "DefaultOrder")
                  sSortOrder = "ascending"
              
                var sColumnType = (sDataType == "number" ? "number" : "text");
              
                var iCount = 0;
                var objTmp = null;
                if (sFldName) {
                      var sDataSrc = sXMLSrc;
                      var objXML = oXML; 
                      if (objXML) {
              
                         var sFirstNode = objXML.XMLDocument.documentElement.firstChild.nodeName;
              
                         oTbl.dataSrc = "";
                         oTbl.rows[0].style.backgroundColor = "#FFFFFF";
                         var objXSL = new ActiveXObject("Msxml2.DOMDocument");
                         if (objXSL) {
                            objXSL.async = false;
              
                            var sXSL = '<?xml version="1.0" encoding="UTF-8"?>' +
                                        '<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' +
              
                                        '<xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>' +
              
                                        '<xsl:template match="/Root">' +
                                           '<xsl:element name="Root">' +
                                           '<xsl:apply-templates select="' + sFirstNode + '">' +
                                              '<xsl:sort select="@' + sFldName + '" data-type="' + sColumnType + '" ' +
                                              (sColumnType == 'text' ? 'case-order="lower-first"' : '') +
                                              ' order="' + sSortOrder + '" />' +
                                           '</xsl:apply-templates>' +
                                           '</xsl:element>' +
                                        '</xsl:template>' +
              
                                        '<xsl:template match="' + sFirstNode + '">' +
                                           '<xsl:copy-of select="."/>' +
                                        '</xsl:template>' +
                                        '</xsl:stylesheet>';
                            //window.clipboardData.setData("Text", sXSL);
                            objXSL.loadXML(sXSL);
              
                            //oXML.loadXML(oXML.transformNode(objXSL));
                            oXML.transformNodeToObject(objXSL, oXML);
                            oTbl.dataSrc = "#" + sDataSrc;
              
                         }
                      }
                }
            }            
            //display the sort icon
            divSort.style.display = "inline";
            
            if (sSortOrder == "ascending")
              divSort.firstChild.src = "/images/ascending.gif";
            else
              divSort.firstChild.src = "/images/descending.gif";
              
            if (oTD) {
              var x = oTD.offsetLeft + oTD.offsetWidth - divSort.offsetWidth;
              var y = ((oTD.offsetTop + oTD.offsetHeight) - divSort.offsetHeight) / 2 + oTD.offsetTop + 3;
              divSort.style.left = x - 3;
              divSort.style.top = y;
              
              oTD.setAttribute("sortOrder", sSortOrder);
            }
            bSorting = false;
        }
      }
      if (typeof(parent.hideProgress) == "function")
        parent.hideProgress();
    }

    function showHide(obj){
      if (obj.tagName == "IMG"){
        var oTR = obj.parentElement.parentElement;
        if (oTR.tagName == "TR"){
          var oTRData = oTR.nextSibling;
          if (obj.src.indexOf("minus") != -1){
            obj.src = "/images/plus.gif";
            oTRData.style.display = "none";
          } else {
            obj.src = "/images/minus.gif";
            oTRData.style.display = "inline";
          }
        }
      }
    }
    
    var bDone = false;
    function hideProgress(obj){
      if (obj.readyState == "complete"){
        iTblCountReady++;
        if (iTblCountReady == (aMgrIDs.length-1)){
          if (typeof(parent.hideProgress) == "function")
            parent.hideProgress();
        }
        window.setTimeout("setRowColors(" + obj.uniqueID + ")", 100);
      }
    }
    
    function setRowColors(obj){
      if (obj){
        var oRows = obj.rows;
        for (var i = 1; i < oRows.length; i++){
          oRows[i].bgColor = oRows[i].style.backgroundColor = (oRows[i].rowIndex % 2 != 0 ? "#fff7e5" : "#ffffff");
        }
      }
    }
    
    function openClaim(sClaimAspect){
      var sLynxID, sClaimAspectNum;
      event.returnValue = false;
      event.cancelBubble = true;
      if (gsClaimCRUD.indexOf('R') == -1){
        ClientWarning("You do not have permission to view Claim details. Please contact your supervisor.");
        return;
      }
      if (sClaimAspect.indexOf("-") > 0){
        sLynxID = sClaimAspect.split("-")[0];
        sClaimAspectNum = sClaimAspect.split("-")[1];
      } else {
        sLynxID = sClaimAspect;
        sClaimAspectNum = 1;
      }
      if (isNaN(sLynxID) == false){
        NavToClaim2(sLynxID, sClaimAspectNum);
      }
    }
    
    function showShopContext(obj){
      oCurRow = obj.parentElement.parentElement;
      var lefter = event.offsetX;
      var topper = event.offsetY;
      event.returnValue = false;
      event.cancelBubble = true;
      oShopPopup.document.body.innerHTML = divContextMenu.innerHTML;
      oShopPopup.show(lefter, topper, 153, 64, obj);
    }
    
    function openAllAssignments(obj){
      var sShopLocationID;
      var sShopID;
      if (obj == null) obj = oCurRow;
      if (obj){
        if (oShopPopup)
          oShopPopup.hide();
        sShopLocationID = obj.cells[10].innerText;
        sShopID = obj.cells[11].innerText;
        NavToPMDDetail(sShopLocationID);
      }
    }

    function openShopProfile(oTR){
      var sShopLocationID;
      var sShopID;
      if (gsShopCRUD.indexOf('R') == -1){
        ClientWarning("You do not have permission to view Shop Profile. Please contact your supervisor.");
        return;
      }
      if (oCurRow == null) oCurRow = oTR;
      if (oCurRow){
        if (oShopPopup)
          oShopPopup.hide();
        sShopLocationID = oCurRow.cells[10].innerText;
        sShopID = oCurRow.cells[11].innerText;
        NavToShop("S", sShopLocationID, sShopID);
        oCurRow = null;
      }
    }
    
    function openAssignment(oTR){
      if (oCurRow == null) oCurRow = oTR;
      if (oCurRow){
        if (oShopPopup)
          oShopPopup.hide();
        var sShopLocationID = oCurRow.cells[10].innerText;
        var sAssignmentID = oCurRow.cells[12].innerText;
        var sAssignmentDate = oCurRow.cells[13].innerText;
        NavToAssignment(sShopLocationID, sAssignmentID, sAssignmentDate);
        oCurRow = null;
      }
    }
    
    function setHeight(){
      divScroll.style.height = document.body.offsetHeight - tblHead.offsetHeight;
    }
    
    function sortDefault(){
      tdDefaultCol.setAttribute("sortOrder", "");
      sortTable(tdDefaultCol, 'RowIndex', 'number');
    }

  ]]>
  </SCRIPT>

  <SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
  
</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" style="backgro1und:transparent;overflow:hidden" onLoad="initPage();">

<DIV id="CNScrollTable" style="height:100%">
  <TABLE unselectable="on" id="tblHead" class="ClaimMiscInputs" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
    <colgroup>
      <col width="200px" style="font-weight:bold;text-align:center"/>
      <col width="111px" style="font-weight:bold;text-align:center"/>
      <col width="50px" style="font-weight:bold;text-align:center"/>
      <col width="75px" style="font-weight:bold;text-align:center"/>
      <col width="150px" style="font-weight:bold;text-align:center"/>
      <col width="150px" style="font-weight:bold;text-align:center"/>
      <col width="75px" style="font-weight:bold;text-align:center"/>
      <col width="73px" style="font-weight:bold;text-align:center"/>
      <col width="30px" style="font-weight:bold;text-align:center"/>
      <col width="75px" style="font-weight:bold;text-align:center"/>
      <col style="display:none"/>
    </colgroup>
    <tr unselectable="on" class="QueueHeader">
      <td unselectable="on" class="TableSortHeader" onclick="sortTable(this, 'Name', 'text')" >Shop Name</td>
      <td unselectable="on" class="TableSortHeader" onclick="sortTable(this, 'AddressCity', 'text')" >City</td>
      <td unselectable="on" class="TableSortHeader" onclick="sortTable(this, 'AddressState', 'text')" >State</td>
      <td unselectable="on" class="TableSortHeader" onclick="sortTable(this, 'ClaimAspect', 'text')" >LYNX ID</td>
      <td unselectable="on" class="TableSortHeader" onclick="sortTable(this, 'InsuranceCoName', 'text')" >Insurance Company</td>
      <td unselectable="on" class="TableSortHeader" onclick="sortTable(this, 'Vehicle', 'text')" >Vehicle YMM</td>
      <td unselectable="on" class="TableSortHeader" onclick="sortTable(this, 'RepairStatus', 'text')" >Repair Status</td>
      <td unselectable="on" class="TableSortHeader" onclick="sortTable(this, 'ReIDate', 'text')" >ReI Date</td>
      <td unselectable="on" class="TableSortHeader" onclick="sortTable(this, 'ReIRequestedFlag', 'number')" >ReI Req</td>
      <td unselectable="on" class="TableSortHeader" onclick="sortTable(this, 'ReIRequestedDate', 'text')" >Requested Date</td>      
      <td id="tdDefaultCol" unselectable="on" class="TableSortHeader" onclick="sortTable(this, 'RowIndex', 'number')" >Default Row Index</td>
    </tr>
  </TABLE>
  <div id="divSort" name="divSort" style="position:absolute;top:0px;left:0px;display:none"><img src="/images/ascending.gif"/></div>
  <div id="divNoData" name="divNoData" class="TableData" style="display:none;height:210px;border:0px;width:100%;padding:10px;text-align:center;color:#FF0000;font-weight:bold">No data to display.</div>
  <div id="divContextMenu" style="position:absolute;top:15;left:15;display:none">
    <div style="width:150px;border:1px solid #000000;background-color:#FFFFFF;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)">
      <table border="0" cellpadding="3" cellspacing="0" style="font:11px Tahoma, Arial;width:100%;cursor:hand;background-color:#FFFFFF">
        <tr>
          <td onmouseenter="this.style.backgroundColor='#FFF8DC'" onmouseleave="this.style.backgroundColor='#FFFFFF'" onclick="parent.openAssignment()">Open this Assignment</td>
        </tr>
        <tr>
          <td onmouseenter="this.style.backgroundColor='#FFF8DC'" onmouseleave="this.style.backgroundColor='#FFFFFF'" onclick="parent.openAllAssignments()">Display all Assignments</td>
        </tr>
        <tr>
          <td onmouseenter="this.style.backgroundColor='#FFF8DC'" onmouseleave="this.style.backgroundColor='#FFFFFF'" onclick="parent.openShopProfile()">Open Shop Profile</td>
        </tr>
      </table>
    </div>
  </div>
  <div id="divScroll" style="overflow:auto;border:0px solid #FF0000"><!-- height:expression(eval(document.body.offsetHeight) - eval(tblHead.offsetHeight)); -->
  <xsl:for-each select="ProgramManager">
    <xsl:variable name="curUserID"><xsl:value-of select="@UserID"/></xsl:variable>
    <xsl:variable name="xmlIslandName"><xsl:value-of select="concat('xml_', string($curUserID))"/></xsl:variable>
    <xsl:variable name="assignmentCount"><xsl:value-of select="count(/Root/ActiveQueue[@ProgramManagerUserID=$curUserID])"/></xsl:variable>
    <table border="0" cellpadding="2" cellspacing="0" style="table-layout:fixed;">
      <colgroup>
        <col width="15px"/>
        <col width="*" style="font-weight:bold"/>
      </colgroup>
      <tr style="height:24px;background-color:#DEB887;">
        <td><img src="/images/minus.gif" style="cursor:hand" onclick="showHide(this)"/></td>
        <td>
          <xsl:value-of select="concat(@NameLast, ', ', @NameFirst)"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          (<xsl:value-of select="$assignmentCount"/> Assignments)
        </td>
      </tr>
      <tr>
        <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
        <td>
          <xsl:if test="$assignmentCount &gt; 0">
          <table name="tblData1" border="1" cellspacing="0" cellpadding="3" style="table-layout:fixed;border-collapse:collapse" onreadystatechange="hideProgress(this)" oncontextmenu="event.returnValue=false">
            <xsl:attribute name="dataSrc"><xsl:value-of select="concat('#', $xmlIslandName)"/></xsl:attribute>
            <colgroup>
              <col width="182px"/>
              <col width="111px"/>
              <col width="50px"/>
              <col width="75px" style="text-align:center"/>
              <col width="150px"/>
              <col width="150px"/>
              <col width="75px" style="text-align:center"/>
              <col width="73px" style="text-align:center"/>
              <col width="30px" style="text-align:center"/>
              <col width="74px" style="text-align:center"/>
              <col style="display:none"/>
              <col style="display:none"/>
              <col style="display:none"/>
              <col style="display:none"/>
            </colgroup>
            <tr style="cursor:hand;" ondblClick="openAssignment(this)" onMouseOut="GridMouseOut(this)" onMouseOver="GridMouseOver(this)">
              <td>
                <span class="NoWrap" dataFld="Name" unselectable="on" oncontextmenu="showShopContext(this)">
                  <xsl:if test="contains($ShopCRUD, 'R') = true()">
                    <xsl:attribute name="style">color:#0000FF</xsl:attribute>
                    <xsl:attribute name="onClick">openShopProfile(this.parentElement.parentElement)</xsl:attribute>
                  </xsl:if>
                </span>
              </td>
              <td><span class="NoWrap" dataFld="AddressCity" unselectable="on"/></td>
              <td><span class="NoWrap" dataFld="AddressState" unselectable="on"/></td>
              <td>
                <span class="NoWrap" dataFld="ClaimAspect" unselectable="on">
                  <xsl:if test="contains($ClaimCRUD, 'R') = true()">
                    <xsl:attribute name="style">color:#0000FF</xsl:attribute>
                    <xsl:attribute name="onClick">openClaim(this.innerText)</xsl:attribute>
                  </xsl:if>
                </span>
              </td>
              <td><span class="NoWrap" dataFld="InsuranceCoName" unselectable="on"/></td>
              <td><span class="NoWrap" dataFld="Vehicle" unselectable="on"/></td>
              <td><span class="NoWrap" dataFld="RepairStatus" unselectable="on"/></td>
              <td><span class="NoWrap" dataFld="ReIDate" unselectable="on"/></td>
              <td unselectable="on"><img dataFld="ReIRequested" style="height:13px;width:13px;"/></td>
              <td><span class="NoWrap" dataFld="ReIRequestedDate" unselectable="on"/></td>
              <td><span dataFld="ShopLocationID"/></td>
              <td><span dataFld="ShopID"/></td>
              <td><span dataFld="AssignmentID"/></td>
              <td><span dataFld="AssignmentDate"/></td>
            </tr>
          </table>
          </xsl:if>
        </td>
      </tr>
    </table>
    <xml>
      <xsl:attribute name="id"><xsl:value-of select="$xmlIslandName"/></xsl:attribute>
      <Root>
        <xsl:for-each select="/Root/ActiveQueue[@ProgramManagerUserID=$curUserID]">
        <xsl:sort select="@ReIRequested" data-type="number" order="descending"/>
        <xsl:sort select="@ReIRequestedDate" order="descending"/>
        <xsl:sort select="@ReILockedFlag" order="ascending"/>
        <xsl:sort select="@AssignmentID" order="descending"/>
        <ActiveQueue>
          <xsl:attribute name="RowIndex"><xsl:value-of select="position()"/></xsl:attribute>
          <xsl:attribute name="ShopLocationID"><xsl:value-of select="@ShopLocationID"/></xsl:attribute>
          <xsl:attribute name="ShopID"><xsl:value-of select="@ShopID"/></xsl:attribute>
          <xsl:attribute name="AssignmentID"><xsl:value-of select="@AssignmentID"/></xsl:attribute>
          <xsl:attribute name="Name"><xsl:value-of select="@Name"/></xsl:attribute>
          <xsl:attribute name="AddressCity"><xsl:value-of select="@AddressCity"/></xsl:attribute>
          <xsl:attribute name="AddressState"><xsl:value-of select="@AddressState"/></xsl:attribute>
          <xsl:attribute name="ProgramManagerUserID"><xsl:value-of select="@ProgramManagerUserID"/></xsl:attribute>
          <xsl:attribute name="LynxID"><xsl:value-of select="@LynxID"/></xsl:attribute>
          <xsl:attribute name="ClaimAspectNumber"><xsl:value-of select="@ClaimAspectNumber"/></xsl:attribute>
          <xsl:attribute name="ClaimAspect"><xsl:value-of select="concat(string(@LynxID), '-', string(@ClaimAspectNumber))"/></xsl:attribute>
          <xsl:attribute name="ClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
          <xsl:attribute name="InsuranceCoName"><xsl:value-of select="@InsuranceCoName"/></xsl:attribute>
          <xsl:variable name="VehYear"><xsl:if test="@VehicleYear != '0'"><xsl:value-of select="@VehicleYear"/></xsl:if></xsl:variable>
          <xsl:attribute name="Vehicle"><xsl:value-of select="concat(string($VehYear), ' ', string(@VehicleMake), ' ', string(@VehicleModel))"/></xsl:attribute>
          <xsl:attribute name="RepairStatus"><xsl:value-of select="@RepairStatus"/></xsl:attribute>
          <xsl:attribute name="ReIDate">
            <xsl:choose>
              <xsl:when test="@ReIDate != ''"><xsl:value-of select="@ReIDate"/></xsl:when>
              <xsl:otherwise>No</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="ReIRequestedFlag"><xsl:value-of select="@ReIRequested"/></xsl:attribute>
          <xsl:attribute name="ReIRequested">
            <xsl:choose>
              <xsl:when test="@ReIRequested = '1'">/images/cbcheck.png</xsl:when>
              <xsl:otherwise>/images/spacer.gif</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="ReIRequestedDate">
            <xsl:choose>
              <xsl:when test="@ReIRequestedDate != '1900-01-01T00:00:00'"><xsl:value-of select="@ReIRequestedDate"/></xsl:when>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="AssignmentDate">
            <xsl:choose>
              <xsl:when test="@AssignmentDate != '1900-01-01T00:00:00'"><xsl:value-of select="@AssignmentDate"/></xsl:when>
            </xsl:choose>
          </xsl:attribute>
        </ActiveQueue>
        </xsl:for-each>
      </Root>
    </xml>
  </xsl:for-each>
  </div>
</DIV>
<script language="javaScript">
  setHeight();
</script>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>