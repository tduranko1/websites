<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:fo="http://www.w3.org/1999/XSL/Format"
		xmlns:ms="urn:schemas-microsoft-com:xslt"
		xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
		xmlns:js="urn:the-xml-files:xslt"
		id="ClaimWitness">

<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="WitnessCRUD" select="Witness"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="WindowID"/>
<xsl:param name="UserId"/>
<xsl:param name="LynxId"/>

<xsl:template match="/Root">

<xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>
<xsl:variable name="pageReadOnly" select="string(boolean($ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'))"/>

<xsl:variable name="InvolvedID" select="/Root/@InvolvedID" />
<xsl:variable name="WitnessLastUpdatedDate" select="/Root/Witness/@SysLastUpdatedDate"/>

<xsl:if test="$ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'">
  <xsl:value-of select="js:setPageDisabled()"/>
</xsl:if>
<xsl:value-of select="js:SetCRUD( string($WitnessCRUD) )"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP                     XSL    PARAMETERS TO SP -->
<!-- APDXSL:C:\websites\apd\v1.0.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},10,uspClaimWitnessGetDetailXML,ClaimWitness.xsl,762,145   -->

<HEAD>
<TITLE>Vehicle Details</TITLE>

<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="STYLESHEET" type="text/css" href="/css/CoolSelect.css"/>
<STYLE type="text/css">
		A {color:black; text-decoration:none;}
</STYLE>
<SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT LANGUAGE="javascript" SRC="/js/calendar.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>


<SCRIPT language="JavaScript">
	var gsInvolvedID = '<xsl:value-of select="$InvolvedID"/>';
  var gsWitnessLastUpdatedDate = '<xsl:value-of select="$WitnessLastUpdatedDate"/>';
  var gsUserID = '<xsl:value-of select="$UserId"/>';
	var gsWitnessCRUD = '<xsl:value-of select="$WitnessCRUD"/>';
  var gsLynxID = '<xsl:value-of select="$LynxId"/>';
  var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";

<![CDATA[

	messages= new Array()
	// Write your descriptions in here.
	messages[0]="Double Click to Expand"
	messages[1]="Double Click to Close"

	preload('checkbox0','/images/cbuncheck.png')
	preload('checkbox1','/images/cbcheck.png')
	preload('checkboxhover0','/images/cbhover.png')
	preload('checkboxhover1','/images/cbhoverck.png')

	preload('radio0','/images/radiounsel.png')
	preload('radio1','/images/radiosel.png')
	preload('radiohover0','/images/radiohover.png')
	preload('radiohover1','/images/radioselhv.png')


  var inADS_Pressed = false;    // re-entry flagging for buttons pressed
  var objDivWithButtons = null;
  var gbDirtyFlag = false;

  function PageInit(){
    initSelectBoxes();
    InitFields();
    onpasteAttachEvents();
    top.setSB(100,top.sb);
  }

	function ADS_Pressed(sAction, sName)
	{
    if (sAction != "Delete")
    {
      if (ValidatePhones() == false) return new Array("", "0");
      if (!gbDirtyFlag) return new Array("", "0"); //nothing changed.
    }
    if (inADS_Pressed == true) return new Array("", "0");
    inADS_Pressed = true;
    objDivWithButtons = window.parent.divWithButtons;
    disableADS(objDivWithButtons, true);

    var sRequest = GetRequestString(sName);
		sRequest += "LynxID=" + gsLynxID + "&";
		sRequest += "UserID=" + gsUserID + "&";

		var oDiv = document.getElementById(sName);
		var retArray = DoCrudAction(oDiv, sAction, sRequest);

		if (sAction != "Delete" && retArray[1] == "1")
		{
			//update the last Updated date...
			var objXML = new ActiveXObject("Microsoft.XMLDOM");
			objXML.loadXML(retArray[0]);

			var contextDate = objXML.documentElement.selectSingleNode("/Root/NewWitness/@SysLastUpdatedDate");
			oDiv.LastUpdatedDate = contextDate.nodeValue;
			gsWitnessLastUpdatedDate = oDiv.LastUpdatedDate;
			var InvolvedID = objXML.documentElement.selectSingleNode("/Root/NewWitness/@InvolvedID");
			oDiv.ContextID = InvolvedID.nodeValue;
			gsInvolvedID = oDiv.ContextID;

			objXML = null;
		}

    inADS_Pressed = false;
    disableADS(objDivWithButtons, false);
    objDivWithButtons = null;

		return retArray;
	}

	function ShowHideTxtArea(id, visibility)
	{
	    divs = document.getElementsByTagName("div");
	    divs[id].style.visibility = visibility;
	}

    function ValidatePhones() {
        if (validatePhoneSections(txtWitnessDayAreaCode, txtWitnessDayExchangeNumber, txtWitnessDayUnitNumber) == false) return false;
        if (validatePhoneSections(txtWitnessNightAreaCode, txtWitnessNightExchangeNumber, txtWitnessNightUnitNumber) == false) return false;
        if (validatePhoneSections(txtWitnessAlternateAreaCode, txtWitnessAlternateExchangeNumber, txtWitnessAlternateUnitNumber) == false) return false;
        return true;
    }

	if (document.attachEvent)
		document.attachEvent("onclick", top.hideAllMenuScriptlets);
	else
		document.onclick = top.hideAllMenuScriptlets;

]]>
</SCRIPT>

</HEAD>
<BODY class="bodyAPDSubSub" onLoad="PageInit();">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
  <DIV id="content16" name="Witness" Update="uspClaimWitnessUpdDetail" Insert="uspClaimWitnessInsDetail" Delete="uspClaimWitnessDel">
		<xsl:attribute name="CRUD"><xsl:value-of select="$WitnessCRUD"/></xsl:attribute>
		<xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Witness/@SysLastUpdatedDate"/></xsl:attribute>
		<xsl:attribute name="ContextID"><xsl:value-of select="@InvolvedID"/></xsl:attribute>
		<xsl:attribute name="ContextName">Involved</xsl:attribute>

    <!-- Start Witness Info -->
    <xsl:for-each select="Witness" >
			<xsl:call-template name="WitnessInfo"/>
		</xsl:for-each>
	</DIV>
</BODY>
</HTML>
</xsl:template>

	<!-- Gets the Witness Info -->
  <xsl:template name="WitnessInfo" >
        <TABLE class="paddedTable" border="0" cellspacing="0" cellpadding="0" width="710">
          <TR>
            <TD unselectable="on">Name:</TD>
            <TD  unselectable="on" nowrap="">
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessNameTitle',2,'NameTitle',.,'','',1)"/>
              <img src="/images/spacer.gif" alt="" width="2" height="1" border="0"/>
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessNameFirst',15,'NameFirst',.,'','',2)"/>
              <img src="/images/spacer.gif" alt="" width="3" height="1" border="0"/>
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessNameLast',15,'NameLast',.,'','',3)"/>
            </TD>
            <TD unselectable="on" colspan="5" nowrap="">Location of Witness During Accident:
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessLocation',44,'Location',.,'','',9)"/>
            </TD>
          </TR>
          <TR>
            <TD unselectable="on">Address:</TD>
            <TD unselectable="on">
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessAddress1',30,'Address1',.,'','',4)"/>
            </TD>
            <TD  unselectable="on" nowrap="">SSN/EIN:</TD>
					  <TD  unselectable="on" nowrap="">
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTaxID',15,'FedTaxId',.,'','',10)"/>
            </TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          </TR>
          <TR>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
            <TD unselectable="on">
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessAddress2',30,'Address2',.,'','',5)"/>
            </TD>
            <TD unselectable="on" nowrap="">Best Number to Call:</TD>
            <TD unselectable="on" nowrap="">
              <xsl:variable name="rtBCP" select="@BestContactPhoneCD"/>
              <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('BestContactPhoneCD',2,'onSelectChange',string($rtBCP),1,2,/Root/Reference[@List='BestContactPhone'],'Name','ReferenceID','','43','','',11,1)"/>
            </TD>
            <TD unselectable="on">Day:</TD>
            <TD  unselectable="on" nowrap="">
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessDayAreaCode',2,'DayAreaCode',.,'',10,13)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtWitnessDayExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessDayExchangeNumber',2,'DayExchangeNumber',.,'',10,14)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtWitnessDayUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessDayUnitNumber',2,'DayUnitNumber',.,'',10,15)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtWitnessDayExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
							<img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
	            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessDayExtensionNumber',3,'DayExtensionNumber',.,'',10,16)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
            </TD>
          </TR>
          <TR>
            <TD unselectable="on">City:</TD>
            <TD unselectable="on">
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessCity',15,'AddressCity',.,'','',6)"/>
            </TD>
            <TD  unselectable="on" nowrap="">Best Time to Call:</TD>
            <TD unselectable="on">
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessBestTimeToCall',20,'BestContactTime',.,'','',12)"/>
            </TD>
            <TD unselectable="on">Night:</TD>
            <TD unselectable="on" nowrap="">
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessNightAreaCode',2,'NightAreaCode',.,'',10,17)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtWitnessNightExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessNightExchangeNumber',2,'NightExchangeNumber',.,'',10,18)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtWitnessNightUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessNightUnitNumber',2,'NightUnitNumber',.,'',10,19)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtWitnessNightExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
							<img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
	            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessNightExtensionNumber',3,'NightExtensionNumber',.,'',10,20)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
            </TD>
          </TR>
          <TR>
            <TD unselectable="on">State:</TD>
            <TD unselectable="on" nowrap="">
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessState',2,'AddressState',.,'',12,7)"/>
              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Zip:
  						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessZip',10,'AddressZip',.,'',10,8)"/>
	  					<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtWitnessState', 'txtWitnessCity');"></xsl:text>
            </TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
            <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
            <TD unselectable="on">Alt.:</TD>
            <TD unselectable="on" nowrap="">
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessAlternateAreaCode',2,'AlternateAreaCode',.,'',10,21)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtWitnessAlternateExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessAlternateExchangeNumber',2,'AlternateExchangeNumber',.,'',10,22)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtWitnessAlternateUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessAlternateUnitNumber',2,'AlternateUnitNumber',.,'',10,23)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtWitnessAlternateExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
							<img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
	            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWitnessAlternateExtensionNumber',3,'AlternateExtensionNumber',.,'',10,24)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
            </TD>
          </TR>
        </TABLE>
  </xsl:template>

</xsl:stylesheet>
