<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:ms="urn:schemas-microsoft-com:xslt"
		xmlns:js="urn:the-xml-files:xslt" >

<ms:script language="JScript" implements-prefix="js">
<![CDATA[

	//String.prototype.Trim = trim_string;
	//String.prototype.HTMLescape = htmlescape_string;

	/*
		Global CRUD use : "CRUD"
	*/
	var privateReadOnly = false;
	var privateHidden = false;
  var bPageDisabled = false;
	function SetCRUD(sCRUD)
	{
		privateReadOnly = false;
		privateHidden = false;

		if (sCRUD.indexOf("R") == -1) privateHidden = true;
		if (sCRUD.indexOf("U") == -1) privateReadOnly = true;
    if (bPageDisabled) privateReadOnly = true;
    
    return "";
	}
  
  function setPageDisabled(){
      bPageDisabled = true;
    return "";
  }

  function setPageEnabled(){
      bPageDisabled = false;
    return "";
  }
	/*
		Generate an InputBox

		Parameters:
			sID      = ID of Inputbox ex. 'txtVehicleYear'
			size     = initial size of the box
			sAttr    = Attribut name ex. 'VehicleYear'
			ctx      = COntext node object  ex. '.'
			sEvents  = (Optional) custom events to add to the box ex. onBlur='FormatPhone(this)'
			MValueFlage = (Optional) to place value info after this call, anything other than null
                    1 = no formating, 2 = format for Date, 3 = Time. 4 = date and time, 5 = readonly, 6= hidden,
                    7=diff name attribute and readonly, 8=right-align, 9=right-align and readonly, 10 = ??,
                    11 = readonly w/o background, 12 = State field
			tabindex = index of tabbing

		Usage:
			InputBox('txtDescYear',3,'VehicleYear',/Root/Vehicle)
			or
			InputBox('txtDescYear',3,'VehicleYear',/Root/Vehicle),'',1,12)
	*/
	function InputBox(sID, size, sAttr, ctx, sEvents, MValueFlag, tabindex)
	{
		try
		{
			var sType = "text";
			if (privateHidden == true) sType = "password";
			var bUpdateFlag = true;
			if (privateReadOnly == true) bUpdateFlag = false;

			var sDTElement = "";
			var sDate = "";
			var sTime = "";

			// get Root Node
			var ctxRoot = ctx[0].selectSingleNode("/Root");
			var sElement = ctx[0].nodeName;
      var ctxAttr = ctxRoot.selectSingleNode("Metadata[@Entity='"+sElement+"']/Column[@Name='"+sAttr+"']");

			// get initial value for box
      var sName, sValue = "";
      if ( sAttr.length > 0 )
      {
	       var ctxVal = ctx[0].selectSingleNode("@" + sAttr);
	       sName = sAttr;
	       if ( ctxVal != null )
	       {
	           switch ( MValueFlag )
	           {
                 case 2:  if (ctxVal.nodeValue != '' && ctxVal.nodeValue != '1900-01-01T00:00:00' )
	                          sValue = UTCConvertDate(ctxVal.nodeValue)
                          else
                            sValue = "";
                          break;
                 case 3:  if (ctxVal.nodeValue != '' && ctxVal.nodeValue != '1900-01-01T00:00:00')
	                          sValue = UTCConvertTime(ctxVal.nodeValue)
                          else
                            sValue = "";
                          break;
                 case 4:  if (ctxVal.nodeValue != '' && ctxVal.nodeValue != '1900-01-01T00:00:00')
                          {
                            sDate = UTCConvertDate(ctxVal.nodeValue);
                            sTime = UTCConvertTime(ctxVal.nodeValue);
                            sValue = sDate + " " + sTime;
                          }
                          else
                            sValue = "";
                          //if (ctxAttr != null )
                          if (ctxAttr != null && privateHidden == false && privateReadOnly == false)
                            sType = "hidden";

                          break;
								 case 5:  sValue = ctxVal.nodeValue; bUpdateFlag = false; break;
								 case 6:  sValue = ctxVal.nodeValue; sType = "hidden"; break;
								 case 7:  sValue = ctxVal.nodeValue; sName = "odd" + sName; bUpdateFlag = false; break;
								 case 9:  sValue = ctxVal.nodeValue; bUpdateFlag = false; break;
								 case 11: sValue = ctxVal.nodeValue; bUpdateFlag = false; break;
	               default: sValue = ctxVal.nodeValue;
	           }
	       }
      }

      		// build input box html tag
            var strRet;
			strRet = "<INPUT id='"+sID+"' size='"+size+"' ";

			sValue = sValue.Trim();
			sValue = sValue.HTMLescape();

			if (privateHidden == true)
			{
				var lenV = sValue.length;
				sValue = "";
				for (var vx=0; vx < lenV; vx++) sValue = sValue + "x";
			}
			
			if (sValue.length > 0)
				strRet += "value='" + sValue + "' ";

			if (sEvents != null)
				strRet += sEvents + " ";

			// get node for the metadata, if null then readonly
			// get Update flag for readonly fields

			strRet += "type='"+sType+"' ";

			if (bUpdateFlag == true)
			{
				if (ctxAttr != null)
				{
					// set the maxlength based on the DataType in the metadata
					var	sType = ctxAttr.selectSingleNode("@DataType").nodeValue;

					var intMaxLen = 0;
					var PrecisionNode = ctxAttr.selectSingleNode("@Precision");
					var iPrecision = 0;
					var ScaleNode = ctxAttr.selectSingleNode("@Scale");
					if (PrecisionNode && ScaleNode)
					{
						try
						{
							intMaxLen = parseInt(PrecisionNode.nodeValue);
						 	iPrecision = parseInt(PrecisionNode.nodeValue);
							var intScale = parseInt(ScaleNode.nodeValue);
							if (intScale > 0)
								intMaxLen += (intScale + 1);
							// need a work around for the rounding of 99999999999999.999999 to 100000000000000000.0
							if (sType == "money"){ intMaxLen--; iPrecision--; }
						}
						catch(e) { intMaxLen = 0; }
					}

					if (sType == "char" || sType == "varchar")
						strRet += "maxlength='" + ctxAttr.selectSingleNode("@MaxLength").nodeValue + "' ";
					else
						strRet += "maxlength='"+intMaxLen+"' ";

					strRet += "dbtype = '" + sType + "' ";

					if (PrecisionNode)
						strRet += "Precision = '"+ iPrecision + "' ";

					if (ScaleNode)
						strRet += "Scale = '"+ ScaleNode.nodeValue + "' ";

					// set the required tag attr based on the 'nullable'
					if (ctxAttr.selectSingleNode("@Nullable").nodeValue == "No")
						strRet += "class='InputRequiredField' requiredFlag ";
					else
						strRet += "class='InputField' ";
				}
				else
				{
          if (MValueFlag == 11)
          {
            bUpdateFlag = true;
            strRet += "class='inputNoBkgrd' readonly systemfield ";
          }
          else
            strRet += "class='InputReadonlyField' readonly systemfield ";
					
          tabindex = -1;
				}
			}
      else
      {
        if (ctxAttr)
        {
    			var	sType = ctxAttr.selectSingleNode("@DataType").nodeValue;
    			var PrecisionNode = ctxAttr.selectSingleNode("@Precision");
    			var ScaleNode = ctxAttr.selectSingleNode("@Scale");
  
          strRet += "dbtype = '" + sType + "' ";
    			if (PrecisionNode)
    			    strRet += "Precision = '"+ PrecisionNode.nodeValue + "' ";
    			if (ScaleNode)
    		        strRet += "Scale = '"+ ScaleNode.nodeValue + "' ";
        }

        if (MValueFlag == 11)
        {
          tabindex = -1;
          bUpdateFlag = true;
          strRet += "class='inputNoBkgrd' readonly systemfield ";
        }
        else
        {
          strRet += "class='InputReadonlyField' readonly systemfield ";
        }
      }

      strRet += "name='" + sName + "' ";

			if (tabindex != null)
				strRet += "tabindex='" + tabindex + "' ";

			if (ctxAttr == null && MValueFlag == 8 || MValueFlag == 9)
        strRet += "style='text-align:right;' >";

			if (ctxAttr == null && MValueFlag == 11)
        strRet += "style='cursor:default;' >";

			if ((sEvents == null && MValueFlag == null) || MValueFlag == 7 )
				strRet += " >";
			else if (sEvents != null && MValueFlag == null)
				strRet += " >";
			else if (MValueFlag == 6)
				strRet += " >";

      // Dont put up the calendar if read-only or hidden.
      if ( ctxAttr != null && MValueFlag != null && privateReadOnly == false && privateHidden == false )
      {
          switch ( MValueFlag )
          {
              case 2:
                  var sImgname = "dimg"+ctx[0].nodeName+sAttr;
                  strRet +="><A  href='#' onclick='ShowCalendar("+sImgname+","+sID+")' >";
                  strRet +="<IMG  align='absmiddle' border='0' width='24' height='17' id='"+sImgname+"' src='/images/calendar.gif'/>";
                  strRet +="</A>";
                  strRet +="<DIV  id='divCalendar' class='PopupDiv' onMouseOut='CalMouseOut(this)' onMouseOver='CalMouseOver(this)' onDeactivate='CalBlur(this)' style='position:absolute;width:180px;height:127px;z-index:20000;display:none;visibility:hidden'></DIV>";
                  break;
							case 4:
                  var sImgname = "dimg"+ctx[0].nodeName+sAttr;
                  strRet +=">";
									strRet +="<INPUT type='text' dbtype='varchar' size='10' maxlength='10' class='InputField' id='ctrDate"+sID+"' ";
									strRet +="value='"+sDate+"' onBlur='CheckDate(this)' onPropertyChange='DTUpdate(this)' DTParent='"+sID+"' ";
									if (bUpdateFlag == false) strRet += "readonly ";
                  strRet += "tabindex='" + tabindex + "' ";
									strRet +="/>";
									strRet +="<INPUT type='text' dbtype='varchar' size='12' maxlength='12' class='InputField' id='ctrTime"+sID+"' ";
									strRet +="value='"+sTime+"' onBlur='CheckTime(this)' onPropertyChange='DTUpdate(this)' DTParent='"+sID+"' ";
									if (bUpdateFlag == false) strRet += "readonly ";
                  strRet += "tabindex='" + tabindex + "' ";
									strRet +="/>";
									strRet +="<A  href='#' onclick='ShowCalendar("+sImgname+",ctrDate"+sID+")' >";
                  strRet +="<IMG  align='absmiddle' border='0' width='24' height='17' id='"+sImgname+"' src='/images/calendar.gif' " + "tabindex='" + tabindex + "' " +"/>";
                  strRet +="</A>";
                  strRet +="<DIV  id='divCalendar' class='PopupDiv' onMouseOut='CalMouseOut(this)' onMouseOver='CalMouseOver(this)' onDeactivate='CalBlur(this)' style='position:absolute;width:180px;height:127px;z-index:20000;display:none;visibility:hidden'></DIV>";
                  break;

							case 6:   strRet += ""; break;
							case 7:   break;
							case 8:   strRet += "style='text-align:right;' >"; break;
							case 9:   strRet += ""; break;
							case 10:  break;
							case 11:  strRet += "style='cursor:default;' >"; break;
							case 12:  strRet += "onBlur='this.value=this.value.toUpperCase()' >"; break;
              default: strRet += " >"; break;
          }
      }
		}
		catch(e){ strRet = e.message; }
        
        if (strRet.indexOf("type='password'") != -1 && MValueFlag != 10)
            if (strRet.substr(strRet.length - 1, 1) != ">")
                strRet += ">";
                
		return strRet;
	}

		/*
		Generate an TextArea

		Parameters:
			sID      = ID of Inputbox ex. 'txtVehicleYear'
			cols     = initial cols size of the box, set to 100% width if 0
			rows     = initial rows size of the box
			sAttr    = Attribut name ex. 'VehicleYear'
			ctx      = COntext node object  ex. '.'
			sEvents  = (Optional) custom events to add to the box ex. onBlur='FormatPhone(this)'
			sPopup   = show a large popup of the text, for small textareas
			tabindex = index of tabbing

		Usage:
			TextArea('txtInvolvedInjuryDesc',24,5,'Description',Involved/Injury)
			or
			TextArea('txtInvolvedInjuryDesc',24,5,'Description',Involved/Injury,'onblur..')
	*/

	function TextArea(sID,cols,rows,sAttr,ctx, sEvents, sPopup, tabindex)
	{
		try
		{
            var bUpdateFlag = true;
            if (privateReadOnly == true) bUpdateFlag = false;

			// get Root Node
			var ctxRoot = ctx[0].selectSingleNode("/Root");
			// get initial value for box
      var sValue = "";
      var ctxAtt = ctx[0].selectSingleNode("@" + sAttr);
      if ( ctxAtt != null )
        if (ctxAtt.nodeValue != null && ctxAtt.nodeValue != "[NULL]")
          sValue = ctxAtt.nodeValue;

      var sName = sAttr; // GetNodeNames(ctx[0], "_") + "_" + sAttr;
			var sElement = ctx[0].nodeName;

			// build input box html tag
            var strRet;
			if (cols == 0)
               strRet = "<TEXTAREA style='width:100%' id='"+sID+"' rows='"+rows+"' name='"+sName+"' ";
      else
			  strRet = "<TEXTAREA id='"+sID+"' cols='"+cols+"' rows='"+rows+"' name='"+sName+"' ";

			if (tabindex != null)
				strRet += "tabindex='" + tabindex + "' ";

			if (sEvents != null && sEvents != "")
				strRet += sEvents + " ";

			if (sPopup != null && sPopup != "")
				strRet += "onDblClick=\"ShowHideTxtArea('"+sPopup+"','visible',this)\" ";

			// get node for the metadata, if null then readonly

/*
			var ctxUpdate = ctxRoot.selectSingleNode("Metadata[@Entity='"+sElement+"']");
			if (ctxUpdate != null) //should not have to do this
			{
				var sFlag = ctxUpdate.selectSingleNode("@Update").nodeValue;
				if (sFlag == "0")
					bUpdateFlag = false;
			}
*/
			if (bUpdateFlag == true)
			{
				var ctxAttr = ctxRoot.selectSingleNode("Metadata[@Entity='"+sElement+"']/Column[@Name='"+sAttr+"']");
				if (ctxAttr != null)
				{
					// set the required tag attr based on the 'nullable'
					if (ctxAttr.selectSingleNode("@Nullable").nodeValue == "No")
						strRet += "class='TextAreaRequiredField' requiredFlag ";
					else
						strRet += "class='TextAreaField' ";

					strRet += "maxlength='" + ctxAttr.selectSingleNode("@MaxLength").nodeValue + "' ";

				}
				else
				{
					strRet += "class='readonlyField' readonly ";
				}
			}
			else
			{
				strRet += "class='readonlyField' readonly ";
			}

			strRet += ">"
			if (privateHidden == false)
			{
				sValue = sValue.Trim();
				sValue = sValue.HTMLescape();
				strRet += sValue;
			}
			strRet += "</TEXTAREA>";
			return strRet;
		}
		catch(e)
		{
			return e.message;
		}
	}

	/* to use the tabindex must pass 'readonly' as false or true not null */
	function AddCheckBox(cbname, defaultToTrue, truevalue, falsevalue,idnumber, readonly, tabindex)
	{
        try
        {
            var str = "";
            var imgname = "/images/cbuncheck.png";

            var selectval = falsevalue;
            if (defaultToTrue == true)
            {
                imgname = "/images/cbcheck.png";
                selectval = truevalue;
            }

            if (idnumber)
                cbname += idnumber;

            str = "<div unselectable='on' id='"+cbname+"Div'>";
            
            str +="<a unselectable='on' ";
            
            
            if (privateReadOnly == false && (readonly == null || readonly == false)) {
                if (tabindex != null)
                    str +="tabindex='" + tabindex + "' ";
            }
            else
                str +="tabindex='-1' ";
            
            str += "trueValue='" + truevalue + "' falseValue='" + falsevalue + "' readOnly='" + readonly + "' value='" + selectval + "' ";
            
            if (privateReadOnly == false && (readonly == null || readonly == false)) {
                str += " style='cursor:hand' onClick='CheckBoxChange(this)' ";
                str += "onfocus='CheckBoxFocus(this)' onblur='CheckBoxBlur(this)' onkeypress='CheckBoxKeyPress(this);' ";
            }
            else {
                if (selectval == truevalue)
                    imgname = "/images/cbcheckro.png";
                else
                    imgname = "/images/cbreadonly.png";
            }
            
            str += "><img unselectable='on' id='"+cbname+"Img' src='"+imgname+"' width=13 height=13 border='0' ";
            if (privateReadOnly == false && (readonly == null || readonly == false))
                str += "onmouseover='CheckBoxHover(this)' onmouseout='CheckBoxHoverOut(this)' ";
            str += "></a>";
            str +="<input type='hidden' name='" + cbname + "' id='" + cbname  + "' value='" + selectval + "' />";
            
            str +="</div>";
            
            return str;
        }
        catch(e){ return e.message; }

	}

  function UTCConvertDate(vDate)
  {
    var vYear = vDate.substr(0,4);
    var vMonth = vDate.substr(5,2);
    var vDay = vDate.substr(8,2);
    vDate = vMonth + '/' + vDay  + '/' + vYear
    return vDate;
  }

  function UTCConvertTime(vDate)
  {
      var ampm = 'AM'
      var hh = vDate.substr(11,2)
      var mm = vDate.substr(14,2)
      var ss = vDate.substr(17,2)
      if (hh >= 12)
      {
        if (hh > 12) hh = hh-12;
        ampm = 'PM';
      }
      vDate = hh + ':' + mm  + ':' + ss  + ' ' + ampm;
      return vDate;
  }

	function ConvertToArray(obj, text)
	{
		var aryRet = new Array();
    if ( obj != null)
    {
      if (typeof(obj) == "string")
      {
          aryRet = obj.split("|");
      }
      else  //node object
      {
        var count = obj.length;
				var str = "";
        for(var idx=0; idx < count; idx++)
        {
            str = obj[idx].selectSingleNode("@"+text).nodeValue;
						aryRet[idx] = str.Trim();
        }
      }
    }
		return aryRet;
	}

    /*
        Generate a select (combo) box

        Parameters:
            selectname  = ID of SelectBox ex. 'selVehicleYear'.
            selectdir   = Arrow direction.
            fnOnChange  = OnChange Callback function (can be '').
            selectval   = Value to select (can be '' if resultAttr and ctx passed).
            selectsize  = The number of items being added to the list box.
            zindex      = Drawing order - items down page should have lower zindex.
            OptNode     = Node list of reference data to extract from.
            valueText   = Attribute to get display string from in OptNode.
            valueAttr   = Attribute to get value string from in OptNode.
        Optional Parameters:
            customWidth = Custom select box width.
            optionHeight= Custom select box height (will auto scroll).
            resultAttr  = Attribute to stuff result back into.
            ctx         = Current context.
						tabindex 		= index of tabbing
            blankFlag   = Add a blank entry to the select options
            idnumber    = Add an optional ID to the selectname for multiple selects on the same page

        Usage:
            AddSelect( 'selPertainsTo', 2, '', string($PertainsTo), 1, 7, /Root/Reference[@List='PertainsTo'], 'Name', 'ReferenceID', 0, 0 )
        Or:
            AddSelect( 'selPertainsTo', 2, '', '', 1, 7, /Root/Reference[@List='PertainsTo'], 'Name', 'ReferenceID', 0, 0, 'PertainsTo', . )
    */
	// select id , direction, function_name_onChange, SelectedIndex, size, zindex, options, option_value1, option_value2, ..... , width (optional)
    function AddSelect( selectname, selectdir, fnOnChange, selectval, selectsize, zindex, OptNode, valueText, valueAttr, customWidth, optionHeight, resultAttr, ctx, tabindex, blankFlag, idnumber)
	{
		try
		{
			var optionscount = 0;
			var aryOption = ConvertToArray(OptNode, valueText)
			var aryOptionValues
      if (valueAttr != null)
			{
				if (typeof(OptNode)=="object")
                    aryOptionValues = ConvertToArray(OptNode, valueAttr);
				else
                    aryOptionValues = ConvertToArray(valueAttr);
			}

			optionscount = aryOption.length;
			var str = "";
			var arrowtype = "6";
			if (selectdir == 8) arrowtype = "5";
			if (privateReadOnly == true) arrowtype = " ";

			var sZindex = "z-index:" + String(10000+zindex) + ";";

			var sHeight = "";
			if (optionHeight != null)
			    sHeight = "height:"+optionHeight+"px;";

			//get the width from the options
			var selWidth = 90;
			if (customWidth != null && customWidth != 0)
			{
			    selWidth = customWidth;
			}
			else
			{
                var i;
			    for(i=0; i<optionscount; i++)
			    {
			        var str = aryOption[i];
			        if (((str.length * 6)+25) > selWidth) selWidth = (str.length * 6)+25;
			    }
			}

			var sName = selectname;
			// this is a work around for having selects with the same name
			// screws up the dirty flag code
			// example selectname =  XXXX_1, removes the _1
			if (sName.indexOf("_") == sName.length-2)
			sName = sName.substring(0,sName.length-2);

			if ( resultAttr != null && resultAttr != "" )
			{
			    //sName = GetNodeNames(ctx[0], "_") + "_" + resultAttr;
			    if ( selectval == "" )
			    {
			        var resultNode = ctx[0].selectSingleNode("@" + resultAttr);
			        if ( resultNode != null )
			            selectval = resultNode.nodeValue;
			    }
			}

			// change readonly to remove change event
			var sOnChangeEvent = fnOnChange+"(this)";
			var sOnOptionClick = "optionClick()";
			if (privateReadOnly == true)
			{
				sOnChangeEvent = "";
				sOnOptionClick = "";
			}

			// check tab index
			var strTabIndex = "";
			if (tabindex != null)
				strTabIndex = " tabindex='"+tabindex+"' ";

       if (idnumber)
  			str = "<SPAN  class='select' style='width:"+selWidth+"px;' id='sel"+selectname+"_"+idnumber+"' onchange='"+sOnChangeEvent+"' selectedIndex='0' size='"+selectsize+"' style='position: relative; top: 0;"+sZindex+"'>";
      else
  			str = "<SPAN  class='select' style='width:"+selWidth+"px;' id='sel"+selectname+"' onchange='"+sOnChangeEvent+"' selectedIndex='0' size='"+selectsize+"' style='position: relative; top: 0;"+sZindex+"'>";
			str +="<TABLE class='selectTable' cellspacing='0' cellpadding='0' onactivate='FocusStyle(this)' ondeactivate='BlurStyle(this);' ";
			if (privateReadOnly == false)
				str += " onClick='toggleDropDown(this.parentElement, 2)' onkeydown='SelKeyDown(this.parentElement,event)'"

			str +=" ><TR style='height:8px;'>";
			str +="<TD class='selected'  >  &nbsp;</TD>";
			str +="<TD  align='CENTER' valign='MIDDLE' class='selButton' ";
			if (privateReadOnly == false)
				str += "onmouseover='selHover(this,1)' onmouseout='selHover(this,0)' onmousedown='selMouseDown(this)' onmouseup='selHover(this,0)'";

			str += " ><SPAN style='position: relative; left: 1; top: -2; width: 100%;'><A HREF='#'"+strTabIndex+">"+arrowtype+"</a></SPAN>";
			str +="</TD></TR></TABLE>";
			str +="<DIV   class='dropDown' style='" + sZindex + sHeight+"' onClick='"+sOnOptionClick+"' onMouseOver='optionOver()' onMouseOut='optionOut()' id='selectOptionDiv'>";

      // adds blank entry for custom drop drown
      if (blankFlag == 1)
      {
      if (selectval == "")
			  str +="<DIV class='option' value='' selected nowrap> </DIV>";
			else
        str +="<DIV class='option' value='' nowrap> </DIV>";
      }

			var strSel = ""
			for(i=0; i<optionscount; i++)
			{
			    strSel = "";
			    var valuestr = "";
			    if (valueAttr != null)
			        valuestr = aryOptionValues[i];
			    else
			        valuestr = String(i);

			    //be sure it's an ID not a CD type param
          if ( ( ( String(selectval) == "" ) && i == 0 && valuestr == 'Unknown' ) || ( valuestr == String(selectval) ) )
			        strSel = "selected";

			    str +="<DIV class='option' value='"+valuestr+"' "+strSel+" nowrap>"+aryOption[i]+"</DIV>";
			}
      
			str +="</DIV>"

			if ( sName != "" )
        if (idnumber)
			    str +="<input type='hidden' name='" + sName + "_" + idnumber + "' value='" + selectval + "' id='hiddenInput'/>";
        else
			    str +="<input type='hidden' name='" + sName + "' value='" + selectval + "' id='hiddenInput'/>";

			str +="</SPAN>";

			if (privateHidden == true) 
			{
				str ="<input type='password' value='xxxx' readonly class='InputReadonlyField' size='6'/>";
			}
			return str;
		}
		catch(e)
		{
			return e.message;
		}
	}

	// parameters : rbname, vertOrhorz,defaultvalue, value1, value2,.....valueN
	function AddRadio()
	{
		try
		{
			//var args=AddRadio.arguments;
			var args 
			var rbname = args[0];
			var dir = args[1];  //0 = horz , 1 = vert
			var defaultvalue = args[2];
            var iLength;

			var sdir = "&nbsp;&nbsp;";
			if (dir == 1) sdir = "<br>";

			var simage = "";
			var defstate = "";
			var docstr = "<div unselectable='on' id='"+rbname+"Div'>";
            iLength = args.length;
            var i;
			for(i=0; i < (iLength - 3); i++)
			{
				simage = "/images/radiounsel.png";
				defstate = "0";
				if (args[i+3] == defaultvalue)
				{
					simage = "/images/radiosel.png";
					defstate = "1";
				}
				docstr += "<a unselectable='on' ";

				if (privateReadOnly == false)
					docstr +="href='javascript:RB"+rbname+".change("+i+",\""+args[i+3]+"\")' ";
				else
					docstr +="href='' ";

				docstr +=" ><img unselectable='on' name='"+rbname+"Img"+i+"' src='"+simage+"' width=13 height=13 border='0' state='"+defstate+"'> "+args[i+3]+"</a>";
				if (i < (args.length-4)) docstr += sdir;
			}

			docstr +="<input type='hidden' name='" + rbname + "' value='" + defaultvalue + "' />";
			docstr += "</div>";
			docstr +="<SCRIPT>RB"+rbname+" = new Radio('"+rbname+"',"+(args.length-3)+",'"+defaultvalue+"');</SCRIPT>"

			return docstr;
		}
		catch(e){ return e.message; }

	}

	function ConvertBTC(sSel)
	{
		switch(sSel.substr(0,1))
		{
			case 'D':
				return 1;
			case 'N':
				return 2;
			case 'A':
				return 3;
		}
		return 0;
	}

	function ConvertTP(sSel)
	{
		switch(sSel.substr(0,1))
		{
			case 'B':
				return 1;
			case 'P':
				return 2;
		}
		return 0;
	}
/*
function trim_string()
{
         var ichar, icount;
         var strValue = this;

    if (this !== null && typeof this === 'object')
    {
         ichar = strValue.length - 1;
         icount = -1;
         while (strValue.charAt(ichar)==' ' && ichar > icount)
         {
             --ichar;
             if (ichar!=(strValue.length-1)) 
             {
                 strValue = strValue.slice(0,ichar+1);
                 ichar = 0;
                 icount = strValue.length - 1;
             }
         }
         while (strValue.charAt(ichar)==' ' && ichar < icount)
         {
             ++ichar;
             if (ichar!=0) 
             {
                 strValue = strValue.slice(ichar,strValue.length);
             }
         }
         return strValue;
    }
 }
 
 function htmlescape_string()
 {
    var ichar, icount;
    var strValue = this;
    ichar = strValue.length - 1;

		var idx;
    idxLength = strValue.length;
		for (idx=0; idx < idxLength; idx++)
		{
			if (strValue.charAt(idx) == '"')
				strValue = spliceString(idx, strValue, "&quot;");
			else if (strValue.charAt(idx) == '&')
				strValue = spliceString(idx, strValue, "&amp;");
			else if (strValue.charAt(idx) == ">")
				strValue = spliceString(idx, strValue, "&gt;");
			else if (strValue.charAt(idx) == '<')
				strValue = spliceString(idx, strValue, "&lt;");
			else if (strValue.charAt(idx) == "'")
				strValue = spliceString(idx, strValue, "&#039;");
		}

		return strValue; 
 }
 */
 function escape_string(str){
    if (str && str.length > 0)
      return escape(str);
    else
      return "";
 }
 
 function spliceString(idx, str, sEsc)
 {
		var sl1 = "";
		var sl2 = "";
		var strValue = str;
		
		sl1 = strValue.substr(0, idx);
		sl2 = strValue.substr(idx+1);
		strValue = sl1 + sEsc + sl2;
		
		return strValue;
 }
 
//cleans new line char, carriage return, form feed, vertical tab, forward slash
function cleanString(str) {
  var strRet = "";
  if (str) {
    if (str.length > 0) {
      strRet = str;
      strRet = str.replace(/[\f\n\r\v]/g,""); //removes any form-feed, new line, carriage return, vertical tab chars
      strRet = strRet.replace(/[\\]/g,"\\\\"); // escapes any backslash
      strRet = strRet.replace(/[\"]/g,"\\\""); // escape any double quotes
      strRet = strRet.replace(/[\']/g,"\\\'"); // escape any single quotes
    }
  }
  return strRet;
}

function TrimInsignificantZeroes(str){
	var len,i;
    for (i=0; i<3; i++){
		len = str.length;
		if (str.charAt(len - 1) == "0")
			str = str.substr(0, len - 1);
	}
	return str;
}

function formatSQLDateTime(str){
  if (str) {
     var re = /(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(.*)/;

     var a = str.match(re);
     if (a) {
        if (a.length == 8){
           return a[2] + "/" + a[3] + "/" + a[1] + " " + a[4] + ":" + a[5] + ":" + a[6];
        }
     }
  }

  return "";
}

]]>
</ms:script>
</xsl:stylesheet>
