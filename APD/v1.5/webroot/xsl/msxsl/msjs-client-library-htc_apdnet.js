<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:ms="urn:schemas-microsoft-com:xslt"
      xmlns:js="urn:the-xml-files:xslt" >

<ms:script language="JScript" implements-prefix="js">
<![CDATA[

   //String.prototype.Trim = trim_string;

   var sAPDNS = "IE"; //Application defined Namespace for the behavior controls. This will appear as <IE:
   var sCaptionList, sValues, sValue, sCaptions, aCaptions, sDBType, iPrecision, iScale, iMaxLength, bInt, arguments; // 19Jun2013 - TVD - New Added

   /*
      Global CRUD use : "CRUD"
   */
   var privateReadOnly = false;
   var privateHidden = false;
   function SetCRUD(sCRUD)
   {
      privateReadOnly = false;
      privateHidden = false;

      if (sCRUD.indexOf("R") == -1) privateHidden = true;
      if (sCRUD.indexOf("U") == -1) privateReadOnly = true;
        return "";
   }


   //XSL version of APD Controls. These use the value as specfied by the xsl document
   function XSL_APDCheckBox(sID, sCaption, ctx, attr, iWidth, bDisabled,
                  iTabIndex, bEndTag, iAlignment, bCanDirty, sOnChange,
                  sOnBeforeChange, sOnAfterChange) {

      bDisabled = str2Boolean(bDisabled);
      var objInfo = getValueFromXML(ctx, attr);

      var sValue = "";
      var bRequired = false;
      if (objInfo) {
         sValue = objInfo.value;

         //if (objInfo.required == true)
         //   bRequired = true;

         if (objInfo.disabled == true)
            bDisabled = (bDisabled || true);
      }

      return APDCheckBox(sID, sCaption, sValue, iWidth, bDisabled, bRequired,
                  iTabIndex, bEndTag, iAlignment, bCanDirty, sOnChange,
                  sOnBeforeChange, sOnAfterChange);
   }

   function XSL_APDCustomSelect(sID, ctx, attrValue, sReferenceList,
                                bBlankFirst, iDisplayCount, bDisabled,
                                iTabIndex, bCanDirty, iDirection, iWidth, iDropWidth,
                                bSorted, sOnChange) {

      bDisabled = str2Boolean(bDisabled);
      var objInfo = getSelectFromXML(ctx, attrValue, sReferenceList);
      var sValue = sValues = sCaptions = "";
      var bRequired = false;
      if (objInfo) {
         sValue = objInfo.value;
         sValues = objInfo.valueList;
         sCaptions = objInfo.captionList;
         bRequired = (objInfo.required == true ? true : false);

         if (objInfo.disabled == true)
            bDisabled = (bDisabled || true);
      }
      return APDCustomSelect(sID, sValue, sValues, sCaptions, bBlankFirst, iDisplayCount,
                  bDisabled, bRequired, iTabIndex, bCanDirty,
                  iDirection, iWidth, iDropWidth, bSorted,
                  sOnChange);
   }

   function XSL_APDInput(sID, ctx, attr, iWidth, bDisabled,
                  iTabIndex, bCanDirty,
                  bEndTag, sOnChange) {

      bDisabled = str2Boolean(bDisabled);
      var objInfo = getValueFromXML(ctx, attr);

      var sRet = sValue = sDBType = "";
      var bRequired = false;
      var iMaxLength = iPrecision = iScale = 0;

      if (objInfo) {
         sValue = objInfo.value;

         if (objInfo.required == true)
            bRequired = true;

         if (objInfo.disabled == true)
            bDisabled = (bDisabled || true);
         iMaxLength = objInfo.maxLength;

         sDBType = objInfo.dataType;
         iPrecision = objInfo.precision;
         iScale = objInfo.scale;
      }
      
      if (sDBType == "money") { // add more datatype when needed.
        if (iScale > 2) {
          // this is to fix the database schema inconsistency like vehicle book value defined as money and the control
          // display a 4 decimal scale. This inconsistency appears in all other places and this is to fix the money datatype
          iPrecision -= (iScale - 2)
          iScale = 2;
        }
      }
      
      switch (sDBType) {
         case "bigint":
            sRet = APDInputNumeric(sID, sValue, iPrecision, iScale, true, true,
                            iWidth, bDisabled, bRequired, iTabIndex, bCanDirty,
                            "", "", bEndTag, sOnChange);
            break;
         case "bit":
            sRet = APDCheckBox(sID, "", sValue, "", bDisabled, bRequired,
                        iTabIndex, bEndTag, 1, bCanDirty, sOnChange,
                        "", "");
            break;
         case "char":
            sRet = APDInput(sID, escape(sValue), iMaxLength, "", iWidth,
                        bDisabled, bRequired, iTabIndex, bCanDirty,
                        bEndTag, sOnChange);
            break;
         case "datetime":
            sValue = formatSQLDateTime(sValue);
            sRet = APDInputDate(sID, sValue, "datetime", true, bDisabled, bRequired,
                         iTabIndex, bCanDirty, bEndTag, sOnChange);
            break;
         case "decimal":
            sRet = APDInputNumeric(sID, sValue, iPrecision, iScale, false, true,
                            iWidth, bDisabled, bRequired, iTabIndex, bCanDirty,
                            "", "", bEndTag, sOnChange);
            break;
         case "float":
            sRet = APDInputNumeric(sID, sValue, iPrecision, iScale, false, true,
                            iWidth, bDisabled, bRequired, iTabIndex, bCanDirty,
                            "", "", bEndTag, sOnChange);
            break;
         case "int":
            sRet = APDInputNumeric(sID, sValue, iPrecision, iScale, true, true,
                            iWidth, bDisabled, bRequired, iTabIndex, bCanDirty,
                            "-2147483648", "+2147483648", bEndTag, sOnChange);
            break;
         case "money":
            sRet = APDInputCurrency(sID, sValue, iPrecision, iScale, iWidth,
                             bDisabled, bRequired, iTabIndex, bCanDirty,
                             "", "", "$", bEndTag, sOnChange);
            break;
         case "nchar":
            sRet = APDInput(sID, escape(sValue), iMaxLength, "", iWidth,
                        bDisabled, bRequired, iTabIndex, bCanDirty,
                        bEndTag, sOnChange);
            break;
         /*case "ntext":
            sRet = APDTextArea(sID, sValue, iMaxLength, iHeight, iWidth, bDisabled,
                        bRequired, iTabIndex, bCanDirty, bEndTag, sOnChange);*/
            break;
         case "nvarchar":
            sRet = APDInput(sID, escape(sValue), iMaxLength, "", iWidth,
                        bDisabled, bRequired, iTabIndex, bCanDirty,
                        bEndTag, sOnChange);
            break;
         case "numeric":
            sRet = APDInputNumeric(sID, sValue, iPrecision, iScale, false, true,
                            iWidth, bDisabled, bRequired, iTabIndex, bCanDirty,
                            "", "", bEndTag, sOnChange);
            break;
         case "real":
            sRet = APDInputNumeric(sID, sValue, iPrecision, iScale, false, true,
                            iWidth, bDisabled, bRequired, iTabIndex, bCanDirty,
                            "", "", bEndTag, sOnChange);
            break;
         case "smalldatetime":
            sValue = formatSQLDateTime(sValue);
            sRet = APDInputDate(sID, sValue, "datetime", true, bDisabled, bRequired,
                         iTabIndex, bCanDirty, bEndTag, sOnChange);
            break;
         case "smallint":
            sRet = APDInputNumeric(sID, sValue, iPrecision, iScale, true, true,
                            iWidth, bDisabled, bRequired, iTabIndex, bCanDirty,
                            "-32768", "32768", bEndTag, sOnChange);
            break;
         case "smallmoney":
            sRet = APDInputCurrency(sID, sValue, iPrecision, iScale, iWidth,
                             bDisabled, bRequired, iTabIndex, bCanDirty,
                             "-214748.3648", "+214748.3648", "$", bEndTag, sOnChange);
            break;
         /*case "text":
            break;*/
         case "timestamp":
            sValue = formatSQLDateTime(sValue);
            sRet = APDInputDate(sID, sValue, "datetime", true, bDisabled, bRequired,
                         iTabIndex, bCanDirty, bEndTag, sOnChange);
            break;
         case "tinyint":
            sRet = APDInputNumeric(sID, sValue, iPrecision, iScale, true, false,
                            iWidth, bDisabled, bRequired, iTabIndex, bCanDirty,
                            "0", "255", bEndTag, sOnChange);
            break;
         case "varchar":
            sRet = APDInput(sID, escape(sValue), iMaxLength, "", iWidth,
                        bDisabled, bRequired, iTabIndex, bCanDirty,
                        bEndTag, sOnChange);
            break;
         default:
            sRet = APDInput(sID, escape(sValue), iMaxLength, "", iWidth,
                        bDisabled, bRequired, iTabIndex, bCanDirty,
                        bEndTag, sOnChange);
            break;
      }

      return sRet;
   }

   function XSL_APDInputCurrency(sID, ctx, attr, iWidth, bDisabled, iTabIndex, bCanDirty,
                                 iMin, iMax, sSymbol, bEndTag, sOnChange){
      bDisabled = str2Boolean(bDisabled);
      var objInfo = getValueFromXML(ctx, attr);

      var sValue = "";
      var bRequired = false;
      var iPrecision = iScale = 0;
      if (objInfo) {
         sValue = objInfo.value;

         if (objInfo.required == true)
            bRequired = true;

         if (objInfo.disabled == true)
            bDisabled = (bDisabled || true);
            
         iMaxLength = objInfo.maxLength;

         iPrecision = objInfo.precision;
         iScale = objInfo.scale;
      }
      
      if (iScale > 2) {
        // this is to fix the database schema inconsistency like vehicle book value defined as money and the control
        // display a 4 decimal scale. This inconsistency appears in all other places and this is to fix the money datatype
        iPrecision -= (iScale - 2)
        iScale = 2;
      }
      
      return APDInputCurrency(sID, sValue, iPrecision, iScale, iWidth,
                       bDisabled, bRequired, iTabIndex, bCanDirty,
                       iMin, iMax, sSymbol, bEndTag, sOnChange);
   }

   function XSL_APDInputDate(sID, ctx, attr, sType, bFutureDate, bDisabled,
                             iTabIndex, bCanDirty, bEndTag, sOnChange) {
      bDisabled = str2Boolean(bDisabled);
      var objInfo = getValueFromXML(ctx, attr);

      var sValue = "";
      var bRequired = false;
      if (objInfo) {
         sValue = formatSQLDateTime(objInfo.value);

         if (objInfo.required == true)
            bRequired = true;

         if (objInfo.disabled == true)
            bDisabled = (bDisabled || true);
      }

      //alert(formatSQLDateTime(objInfo.value) + "\n" + formatSQLDate(objInfo.value) + "\n" + formatSQLTime(objInfo.value));

      return APDInputDate(sID, sValue, sType, bFutureDate, bDisabled, bRequired,
                          iTabIndex, bCanDirty, bEndTag, sOnChange);
   }

   function XSL_APDInputNumeric(sID, ctx, attr, bNegative, iWidth, bDisabled, iTabIndex, bCanDirty,
                                iMin, iMax, bEndTag, sOnChange) {
      bDisabled = str2Boolean(bDisabled);
      var objInfo = getValueFromXML(ctx, attr);

      var sValue = "";
      var bRequired = bInt = false;
      var iPrecision = iScale = 0;
      if (objInfo) {
         sValue = objInfo.value;

         if (objInfo.required == true)
            bRequired = true;

         if (objInfo.disabled == true)
            bDisabled = (bDisabled || true);
         iMaxLength = objInfo.maxLength;

         iPrecision = objInfo.precision;
         iScale = objInfo.scale;
         if (iScale == 0)
            bInt = true;
      }
      return APDInputNumeric(sID, sValue, iPrecision, iScale, bInt, bNegative,
                             iWidth, bDisabled, bRequired, iTabIndex, bCanDirty,
                             iMin, iMax, bEndTag, sOnChange);
   }

   function XSL_APDInputPhone(sID, ctx, attrArea, attrExch, attrNum, attrExt, attrAll, bShowExtension,
                              bDisabled, iTabIndex, bCanDirty, bIncludeMask, bEndTag, sOnChange) {
      bDisabled = str2Boolean(bDisabled);
      var objInfo = getPhoneFromXML(ctx, attrArea, attrExch, attrNum, attrExt, attrAll);

      var sValue = "";
      var bRequired = false;
      if (objInfo) {
         sValue = objInfo.value;

         if (objInfo.required == true)
            bRequired = true;

         if (objInfo.disabled == true)
            bDisabled = (bDisabled || true);
      }
      return APDInputPhone(sID, sValue, "", "", "", "", bShowExtension,
                           bDisabled, bRequired, iTabIndex, bCanDirty,
                           bIncludeMask, bEndTag, sOnChange)
   }

   function XSL_APDLabel(sID, ctx, attr, iHeight, iWidth, bDisabled) {
      bDisabled = str2Boolean(bDisabled);
      var objInfo = getValueFromXML(ctx, attr);

      var sValue = "";
      var bRequired = false;
      if (objInfo) {
         sValue = objInfo.value;

         if (objInfo.disabled == true)
            bDisabled = (bDisabled || true);
      }
      return APDLabel(sID, sValue, iHeight, iWidth, bDisabled);
   }

   /*function XSL_APDRadio(sID, sCaption, sGroupName, ctx, attr, sCheckedValue, sUncheckedValue,
                         iAlignment, iWidth, iTabIndex, bCanDirty, bEndTag, sOnChange) {


      return APDRadio(sID, sCaption, sGroupName, sValue, sCheckedValue, sUncheckedValue,
                      iAlignment, iWidth, bDisabled, bRequired, iTabIndex, bCanDirty,
                      bEndTag, sOnChange);
   }

   function XSL_APDRadioGroup() {
      APDRadioGroup(sID, bDisabled, bRequired, bCanDirty, bEndTag, sOnChange, sOnBeforeChange, sOnAfterChange)
   }*/

   function XSL_APDTextArea(sID, ctx, attr, iHeight, iWidth, bDisabled, iTabIndex, bCanDirty, bEndTag, sOnChange) {
      bDisabled = str2Boolean(bDisabled);
      var objInfo = getValueFromXML(ctx, attr);

      var sValue = "";
      var bRequired = false;
      var iMaxLength = 0;
      if (objInfo) {
         sValue = objInfo.value;

         if (objInfo.required == true)
            bRequired = true;

         if (objInfo.disabled == true)
            bDisabled = (bDisabled || true);

         iMaxLength = objInfo.maxLength;
      }

      return APDTextArea(sID, escape(sValue), iMaxLength, iHeight, iWidth, bDisabled,
                        bRequired, iTabIndex, bCanDirty, bEndTag, sOnChange);
   }

   //Regular version of APD Controls. These use the value as specfied by the user
   function APDButton(sID, sValue, iWidth, bDisabled, iTabIndex, bEndTag, sOnButtonClick) {
      var bTagEnd = true;
      bTagEnd = ((bEndTag) ? str2Boolean(bEndTag) : true);

      if (privateReadOnly == true)
         bDisabled = true;

      var sAPDElement = '<' + sAPDNS + ':APDButton ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        'value="' + sValue + '" ' +
                        (iWidth && (str2Int(iWidth) > 0) ? 'width="' + iWidth + '" ' : '') +
                        (bDisabled && (str2Boolean(bDisabled) == true) ? 'CCDisabled="true"' : '') +
                        (iTabIndex && (str2Int(iTabIndex) != 0) ? 'CCTabIndex="' + iTabIndex + '" ' : '') +
                        (sOnButtonClick && (sOnButtonClick.length > 0) ? 'onButtonClick="' + sOnButtonClick + (sOnButtonClick.indexOf('(') == -1 ? '()' : '') + '" ' : '');

      if (privateHidden == true)
         sAPDElement += "style='display:none' ";
      if (bTagEnd == true)
         sAPDElement += '/>\n';

      return sAPDElement;
   }

   function APDCheckBox(sID, sCaption, sValue, iWidth, bDisabled, bRequired,
                        iTabIndex, bEndTag, iAlignment, bCanDirty, sOnChange,
                        sOnBeforeChange, sOnAfterChange) {
      var bTagEnd = true;
      bTagEnd = ((bEndTag) ? str2Boolean(bEndTag) : true);

      if (privateReadOnly == true)
         bDisabled = true;

      var sAPDElement = '<' + sAPDNS + ':APDCheckBox ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        'caption="' + sCaption + '" ' +
                        'value="' + sValue + '" ' +
                        (iWidth && (str2Int(iWidth) > 0) ? 'width="' + iWidth + '" ' : '') +
                        (bDisabled && (str2Boolean(bDisabled) == true) ? 'CCDisabled="true" ' : '') +
                        (bRequired && (str2Boolean(bRequired) == true) ? 'required="true" ' : '') +
                        (iTabIndex && (str2Int(iTabIndex) != 0) ? 'CCTabIndex="' + iTabIndex + '" ' : '') +
                        (iAlignment && (str2Int(iAlignment) > 0) ? 'alignment="' + iAlignment + '" ' : '') +
                        (bCanDirty && (str2Boolean(bCanDirty) == true) ? 'canDirty="true" ' : 'canDirty="false" ') +
                        (sOnChange && (sOnChange.length > 0) ?
                           'onChange="' + sOnChange + (sOnChange.indexOf('(') == -1 ? '()' : '') + '" '
                           : '') +
                        (sOnBeforeChange && (sOnBeforeChange.length > 0) ?
                           'onBeforeChange="' + sOnBeforeChange + (sOnBeforeChange.indexOf('(') == -1 ? '()' : '') + '" '
                           : '') +
                        (sOnAfterChange && (sOnAfterChange.length > 0) ?
                           'onAfterChange="' + sOnAfterChange + (sOnAfterChange.indexOf('(') == -1 ? '()' : '') + '" '
                           : '');

      if (privateHidden == true)
         sAPDElement += "style='display:none' ";
      if (bTagEnd == true)
         sAPDElement += '/>\n';

      return sAPDElement;
   }

   function APDCustomSelect(sID, sValue, sValueList, sCaptionList, bBlankFirst, iDisplayCount,
                            bDisabled, bRequired, iTabIndex, bCanDirty,
                            iDirection, iWidth, iDropWidth, bSorted,
                            sOnChange) {
      var aValues = aCaptions = new Array();

      if (sValueList){
         sValueList = unescape(sValueList);
         aValues = sValueList.split("|");
      }

      if (sCaptionList){
         sCaptionList = unescape(sCaptionList);
         aCaptions = sCaptionList.split("|");
      }

      if (privateReadOnly == true)
         bDisabled = true;

      var sAPDElement = '<' + sAPDNS + ':APDCustomSelect ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        'value="' + sValue + '" ' +
                        (bBlankFirst && (str2Boolean(bBlankFirst) == true) ? 'blankFirst="true" ' : '') +
                        (iDisplayCount && (str2Int(iDisplayCount) > 0) ? 'displayCount="' + iDisplayCount + '" ' : '') +
                        (bDisabled && (str2Boolean(bDisabled) == true) ? 'CCDisabled="true" ' : '') +
                        (bRequired && (str2Boolean(bRequired) == true) ? 'required="true" ' : '') +
                        (iTabIndex && (str2Int(iTabIndex) != 0) ? 'CCTabIndex="' + iTabIndex + '" ' : '') +
                        (bCanDirty && (str2Boolean(bCanDirty) == true) ? 'canDirty="true" ' : 'canDirty="false" ') +
                        (iDirection && (str2Int(iDirection) >= -1) ? 'direction="' + iDirection + '" ' : '') +
                        (iWidth && (str2Int(iWidth) > 0) ? 'width="' + iWidth + '" ' : '') +
                        (iDropWidth && (str2Int(iDropWidth) > 0) ? 'dropDropWidth="' + iDropWidth + '" ' : '') +
                        (bSorted && (str2Boolean(bSorted) == true) ? 'sorted ' : '') +
                        (sOnChange && (sOnChange.length > 0) ? 'onChange="' + sOnChange + (sOnChange.indexOf('(') == -1 ? '()' : '') + '" ' : '') +
                        (privateHidden == true ? 'style="display:none" ' : '') +
                        ' >\n';
      var sDropCount = Math.min(aValues.length, aCaptions.length);
      for (var i = 0; i < sDropCount; i++) {
         sAPDElement += '<' + sAPDNS + ':dropDownItem ' +
                           'value="' + escape(aValues[i]) + '" ' +
                           'style="display:none" >' +
                           //'style="visibility:hidden" >' +
                           escape(aCaptions[i]) +
                           '</' + sAPDNS + ':dropDownItem>\n';
      }

      sAPDElement += '</' + sAPDNS + ':APDCustomSelect>';

      return sAPDElement;
   }

   function APDInput(sID, sValue, iMaxLength, iSize, iWidth,
                     bDisabled, bRequired, iTabIndex, bCanDirty,
                     bEndTag, sOnChange) {
      var bTagEnd = true;
      bTagEnd = ((bEndTag) ? str2Boolean(bEndTag) : true);

      if (privateReadOnly == true)
         bDisabled = true;

      var sAPDElement = '<' + sAPDNS + ':APDInput ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        'value="' + sValue + '" ' +
                        (iMaxLength && (str2Int(iMaxLength) > 0) ? 'maxLength="' + iMaxLength + '" ' : '') +
                        (iSize && (str2Int(iSize) > 0) ? 'size="' + iSize + '" ' : '') +
                        (iWidth && (str2Int(iWidth) > 0) ? 'width="' + iWidth + '" ': '') +
                        (bDisabled && (str2Boolean(bDisabled) == true) ? 'CCDisabled="true" ' : '') +
                        (bRequired && (str2Boolean(bRequired) == true) ? 'required="true" ' : '') +
                        (iTabIndex && (str2Int(iTabIndex) != 0) ? 'CCTabIndex="' + iTabIndex + '" ' : '') +
                        (bCanDirty && (str2Boolean(bCanDirty) == false) ? 'canDirty="false" ' : 'canDirty="true" ') +
                        (sOnChange && (sOnChange.length > 0) ? 'onChange="' + sOnChange + (sOnChange.indexOf('(') == -1 ? '()' : '') + '" ' : '');

      if (privateHidden == true)
         sAPDElement += "style='display:none' ";
      if (bTagEnd == true)
         sAPDElement += '/>\n';

      return sAPDElement;
   }

   function APDInputCurrency(sID, sValue, iPrecision, iScale, iWidth,
                             bDisabled, bRequired, iTabIndex, bCanDirty,
                             iMin, iMax, sSymbol, bEndTag, sOnChange) {
      var bTagEnd = true;
      bTagEnd = ((bEndTag) ? str2Boolean(bEndTag) : true);

      if (privateReadOnly == true)
         bDisabled = true;

      var sAPDElement = '<' + sAPDNS + ':APDInputCurrency ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        'value="' + sValue + '" ' +
                        (iPrecision && (str2Int(iPrecision) > 0) ? 'precision="' + iPrecision + '" ' : '') +
                        (iScale && (str2Int(iScale) > 0) ? 'scale="' + iScale + '" ' : '') +
                        (iWidth && (str2Int(iWidth) > 0) ? 'width="' + iWidth + '" ': '') +
                        (bDisabled && (str2Boolean(bDisabled) == true) ? 'CCDisabled="true" ' : '') +
                        (bRequired && (str2Boolean(bRequired) == true) ? 'required="true" ' : '') +
                        (iTabIndex && (str2Int(iTabIndex) != 0) ? 'CCTabIndex="' + iTabIndex + '" ' : '') +
                        (bCanDirty && (str2Boolean(bCanDirty) == true) ? 'canDirty="true" ' : 'canDirty="false" ') +
                        (iMin && (str2Int(iMin) > 0) ? 'min="' + iMin + '" ': '') +
                        (iMax && (str2Int(iMax) > 0) ? 'max="' + iMax + '" ': '') +
                        (sSymbol && (sSymbol.length > 0) ? 'symbol="' + sSymbol + '" ': '') +
                        (sOnChange && (sOnChange.length > 0) ? 'onChange="' + sOnChange + (sOnChange.indexOf('(') == -1 ? '()' : '') + '" ' : '');

      if (privateHidden == true)
         sAPDElement += "style='display:none' ";
      if (bTagEnd == true)
         sAPDElement += '/>\n';

      return sAPDElement;
   }

   function APDInputDate(sID, sValue, sType, bFutureDate, bDisabled, bRequired,
                         iTabIndex, bCanDirty, bEndTag, sOnChange) {
      var bTagEnd = true;
      bTagEnd = ((bEndTag) ? str2Boolean(bEndTag) : true);

      sType = (sType ? sType.toLowerCase() : "");

      var sControlType = "";
      if (sType == "datetime" || sType == "date" || sType == "time")
         sControlType = sType;
      else
         sControlType = "date";

      if (privateReadOnly == true)
         bDisabled = true;

      var sAPDElement = '<' + sAPDNS + ':APDInputDate ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        'value="' + sValue + '" ' +
                        'type="' + sControlType + '" ' +
                        (bFutureDate && (str2Boolean(bFutureDate) == true) ? 'futureDate="true" ' : 'futureDate="false" ') +
                        (bDisabled && (str2Boolean(bDisabled) == true) ? 'CCDisabled="true" ' : '') +
                        (bRequired && (str2Boolean(bRequired) == true) ? 'required="true" ' : '') +
                        (iTabIndex && (str2Int(iTabIndex) != 0) ? 'CCTabIndex="' + iTabIndex + '" ' : '') +
                        (bCanDirty && (str2Boolean(bCanDirty) == true) ? 'canDirty="true" ' : 'canDirty="false" ') +
                        (sOnChange && (sOnChange.length > 0) ? 'onChange="' + sOnChange + (sOnChange.indexOf('(') == -1 ? '()' : '') + '" ' : '');

      if (privateHidden == true)
         sAPDElement += "style='display:none' ";
      if (bTagEnd == true)
         sAPDElement += '/>\n';

      return sAPDElement;
   }

   function APDInputNumeric(sID, sValue, iPrecision, iScale, bInt, bNegative,
                            iWidth, bDisabled, bRequired, iTabIndex, bCanDirty,
                            iMin, iMax, bEndTag, sOnChange) {
      var bTagEnd = true;
      bTagEnd = ((bEndTag) ? str2Boolean(bEndTag) : true);

      if (privateReadOnly == true)
         bDisabled = true;


      var sAPDElement = '<' + sAPDNS + ':APDInputNumeric ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        'value="' + sValue + '" ' +
                        (iPrecision && (str2Int(iPrecision) > 0) ? 'precision="' + iPrecision + '" ' : '') +
                        (str2Int(iScale) >= 0 ? 'scale="' + iScale + '" ' : '') +
                        (bInt && (str2Boolean(bInt) == true) ? 'int="true" ' : 'int="false" ') +
                        (bNegative && (str2Boolean(bNegative) == true) ? 'neg="true" ' : 'neg="false" ') +
                        (iWidth && (str2Int(iWidth) > 0) ? 'width="' + iWidth + '" ': '') +
                        (bDisabled && (str2Boolean(bDisabled) == true) ? 'CCDisabled="true" ' : '') +
                        (bRequired && (str2Boolean(bRequired) == true) ? 'required="true" ' : '') +
                        (iTabIndex && (str2Int(iTabIndex) != 0) ? 'CCTabIndex="' + iTabIndex + '" ' : '') +
                        (bCanDirty && (str2Boolean(bCanDirty) == true) ? 'canDirty="true" ' : 'canDirty="false" ') +
                        (iMin && (str2Int(iMin) > 0) ? 'min="' + iMin + '" ': '') +
                        (iMax && (str2Int(iMax) > 0) ? 'max="' + iMax + '" ': '') +
                        (sOnChange && (sOnChange.length > 0) ? 'onChange="' + sOnChange + (sOnChange.indexOf('(') == -1 ? '()' : '') + '" ' : '');

      if (privateHidden == true)
         sAPDElement += "style='display:none' ";
      if (bTagEnd == true)
         sAPDElement += '/>\n';

      return sAPDElement;
   }

   function APDInputPhone(sID, sValue, sArea, sExch, sNum, sExt, bShowExtension,
                          bDisabled, bRequired, iTabIndex, bCanDirty,
                          bIncludeMask, bEndTag, sOnChange) {
      var bTagEnd = true;
      bTagEnd = ((bEndTag) ? str2Boolean(bEndTag) : true);

      if (privateReadOnly == true)
         bDisabled = true;

      var sAPDElement = '<' + sAPDNS + ':APDInputPhone ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        'value="' + sValue + '" ' +
                        (sArea && sArea.length > 0 ? 'areaCode="' + sArea + '" ' : '') +
                        (sExch && sExch.length > 0 ? 'phoneExchange="' + sExch + '" ' : '') +
                        (sNum && sNum.length > 0 ? 'phoneNumber="' + sNum + '" ' : '') +
                        (sExt && sExt.length > 0 ? 'phoneExtension="' + sExt + '" ' : '') +
                        (bShowExtension && (str2Boolean(bShowExtension) == true) ? 'showExtension="true" ' : '') +
                        (bDisabled && (str2Boolean(bDisabled) == true) ? 'CCDisabled="true" ' : '') +
                        (bRequired && (str2Boolean(bRequired) == true) ? 'required="true" ' : '') +
                        (iTabIndex && (str2Int(iTabIndex) != 0) ? 'CCTabIndex="' + iTabIndex + '" ' : '') +
                        (bCanDirty && (str2Boolean(bCanDirty) == true) ? 'canDirty="true" ' : 'canDirty="false" ') +
                        (bIncludeMask && (str2Boolean(bIncludeMask) == true) ? 'IncludeMask="true" ' : 'IncludeMask="false" ') +
                        (sOnChange && (sOnChange.length > 0) ? 'onChange="' + sOnChange + (sOnChange.indexOf('(') == -1 ? '()' : '') + '" ' : '');

      if (privateHidden == true)
         sAPDElement += "style='display:none' ";
      if (bTagEnd == true)
         sAPDElement += '/>\n';

      return sAPDElement;
   }

   function APDLabel(sID, sValue, iHeight, iWidth, bDisabled) {
      if (privateReadOnly == true)
         bDisabled = true;

      var sAPDElement = '<' + sAPDNS + ':APDLabel ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        'value="' + sValue + '" ' +
                        (iWidth && (str2Int(iWidth) > 0) ? 'width="' + iWidth + '" ': '') +
                        (iHeight && (str2Int(iHeight) > 0) ? 'height="' + iHeight + '" ': '') +
                        (bDisabled && (str2Boolean(bDisabled) == true) ? 'CCDisabled="true" ' : '') +
                        (privateHidden == true ? 'style="display:none" ' : '') +
                        '/>\n';

      return sAPDElement;
   }

   function APDRadio(sID, sCaption, sGroupName, sValue, sCheckedValue, sUncheckedValue,
                     iAlignment, iWidth, bDisabled, bRequired, iTabIndex, bCanDirty,
                     bEndTag, sOnChange) {
      var bTagEnd = true;
      bTagEnd = ((bEndTag) ? str2Boolean(bEndTag) : true);

      if (privateReadOnly == true)
         bDisabled = true;

      var sAPDElement = '<' + sAPDNS + ':APDRadio ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        'caption="' + sCaption + '" ' +
                        'groupName="' + sGroupName + '" ' +
                        'value="' + sValue + '" ' +
                        (sCheckedValue && sCheckedValue.length > 0 ? 'checkedValue="' + sCheckedValue + '" ' : '') +
                        (sUncheckedValue && sUncheckedValue.length > 0 ? 'uncheckedValue="' + sUncheckedValue + '" ' : '') +
                        (iAlignment && str2Int(iAlignment) > 0 ? 'alignment="' + iAlignment + '" ' : '') +
                        (iWidth && str2Int(iWidth) > 0 ? 'width="' + iWidth + '" ' : '') +
                        (bDisabled && (str2Boolean(bDisabled) == true) ? 'CCDisabled="true" ' : '') +
                        (bRequired && (str2Boolean(bRequired) == true) ? 'required="true" ' : '') +
                        (iTabIndex && (str2Int(iTabIndex) != 0) ? 'CCTabIndex="' + iTabIndex + '" ' : '') +
                        (bCanDirty && (str2Boolean(bCanDirty) == true) ? 'canDirty="true" ' : 'canDirty="false" ') +
                        (sOnChange && (sOnChange.length > 0) ? 'onChange="' + sOnChange + (sOnChange.indexOf('(') == -1 ? '()' : '') + '" ' : '');

      if (privateHidden == true)
         sAPDElement += "style='display:none' ";
      if (bTagEnd == true)
         sAPDElement += '/>\n';

      return sAPDElement;
   }

   function APDRadioGroup(sID, bDisabled, bRequired, bCanDirty, bEndTag, sOnChange, sOnBeforeChange, sOnAfterChange) {
      var bTagEnd = true;
      bTagEnd = ((bEndTag) ? str2Boolean(bEndTag) : true);

      if (privateReadOnly == true)
         bDisabled = true;

      var sAPDElement = '<' + sAPDNS + ':APDRadioGroup ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        (bDisabled && (str2Boolean(bDisabled) == true) ? 'CCDisabled="true" ' : '') +
                        (bRequired && (str2Boolean(bRequired) == true) ? 'required="true" ' : '') +
                        (bCanDirty && (str2Boolean(bCanDirty) == true) ? 'canDirty="true" ' : 'canDirty="false" ') +
                        (sOnChange && (sOnChange.length > 0) ? 'onChange="' + sOnChange + (sOnChange.indexOf('(') == -1 ? '()' : '') + '" ' : '') +
                        (sOnBeforeChange && (sOnBeforeChange.length > 0) ? 'onBeforeChange="' + sOnBeforeChange + (sOnBeforeChange.indexOf('(') == -1 ? '()' : '') + '" ' : '') +
                        (sOnAfterChange && (sOnAfterChange.length > 0) ? 'onAfterChange="' + sOnAfterChange + (sOnAfterChange.indexOf('(') == -1 ? '()' : '') + '" ' : '');

      if (privateHidden == true)
         sAPDElement += "style='display:none' ";
      if (bTagEnd == true)
         sAPDElement += '/>\n';

      return sAPDElement;
   }

   function APDStatus(sID, iHeight, iWidth, sContext) {
      var sAPDElement = '<' + sAPDNS + ':APDStatus ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        (iHeight && (str2Int(iHeight) > 0) ? 'height="' + iHeight + '" ': '') +
                        (iWidth && (str2Int(iWidth) > 0) ? 'width="' + iWidth + '" ': '') +
                        (sContext && sContext.length > 0 ? 'context="' + sContext + '" ' : 'context="window"') +
                        '/>\n';

      return sAPDElement;
   }

   function APDTab(sID, sCaption, sGroupName, sTabPage, iWidth, sCRUD, sAddProc,
                   sDelProc, sUpdProc, bDisabled, bHidden, iTabIndex, bEndTag,
                   sOnTabActivate, sOnTabBeforeDeactivate, sOnTabSave, sOnTabAdd,
                   sOnTabDel, sOnTabValidate) {
      var bTagEnd = true;
      bTagEnd = ((bEndTag) ? str2Boolean(bEndTag) : true);

      if (privateHidden == true)
         bDisabled = true;

      var sAPDElement = '<' + sAPDNS + ':APDTab ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        'caption="' + sCaption + '" ' +
                        'groupName="' + sGroupName + '" ' +
                        'tabPage="' + sTabPage + '" ' +
                        (iWidth && str2Int(iWidth) > 0 ? 'width="' + iWidth + '" ' : '') +
                        (sCRUD && sCRUD.length > 0 ? 'CRUD="' + sCRUD + '" ' : '') +
                        (sAddProc && sAddProc.length > 0 ? 'addStoredProc="' + sAddProc + '" ' : '') +
                        (sDelProc && sDelProc.length > 0 ? 'delStoredProc="' + sDelProc + '" ' : '') +
                        (sUpdProc && sUpdProc.length > 0 ? 'updStoredProc="' + sUpdProc + '" ' : '') +
                        (bDisabled && (str2Boolean(bDisabled) == true) ? 'CCDisabled="true" ' : '') +
                        (bHidden && (str2Boolean(bHidden) == true) ? 'hidden="true" ' : '') +
                        (iTabIndex && (str2Int(iTabIndex) != 0) ? 'CCTabIndex="' + iTabIndex + '" ' : '') +
                        (sOnTabActivate && (sOnTabActivate.length > 0) ? 'onTabActivate="' + sOnTabActivate + (sOnTabActivate.indexOf('(') == -1 ? '()' : '') + '" ' : '') +
                        (sOnTabBeforeDeactivate && (sOnTabBeforeDeactivate.length > 0) ? 'onTabBeforeDeactivate="' + sOnTabBeforeDeactivate + (sOnTabBeforeDeactivate.indexOf('(') == -1 ? '()' : '') + '" ' : '') +
                        (sOnTabSave && (sOnTabSave.length > 0) ? 'onTabSave="' + sOnTabSave + (sOnTabSave.indexOf('(') == -1 ? '()' : '') + '" ' : '') +
                        (sOnTabAdd && (sOnTabAdd.length > 0) ? 'onTabAdd="' + sOnTabAdd + (sOnTabAdd.indexOf('(') == -1 ? '()' : '') + '" ' : '') +
                        (sOnTabDel && (sOnTabDel.length > 0) ? 'onTabDel="' + sOnTabDel + (sOnTabDel.indexOf('(') == -1 ? '()' : '') + '" ' : '') +
                        (sOnTabValidate && (sOnTabValidate.length > 0) ? 'onTabValidate="' + sOnTabValidate + (sOnTabValidate.indexOf('(') == -1 ? '()' : '') + '" ' : '');

      if (bTagEnd == true)
         sAPDElement += '/>\n';

      return sAPDElement;
   }

   function APDTabGroupStart(sID, iHeight, iWidth, bStack, iPreselectTab, bShowADS, sCRUD,
                        bMany, sAddProc, sDelProc, sUpdProc, bDisabled, iTabIndex,
                        bEndTag, sOnAfterChange, sOnTabSave, sOnTabAdd, sOnTabDel, sOnTabValidate) {
      var bTagEnd = true;
      bTagEnd = ((bEndTag) ? str2Boolean(bEndTag) : true);

      var sAPDElement = '<' + sAPDNS + ':APDTabGroup ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        (iHeight && str2Int(iHeight) > 0 ? 'height="' + iHeight + '" ' : '') +
                        (iWidth && str2Int(iWidth) > 0 ? 'width="' + iWidth + '" ' : '') +
                        (bStack && (str2Boolean(bStack) == true) ? 'stackable="true" ' : '') +
                        (arguments.length > 4 && str2Int(iPreselectTab) >= 0 ? 'preselectTab="' + iPreselectTab + '" ' : '') +
                        (bShowADS && (str2Boolean(bShowADS) == true) ? 'showADS="true" ' : 'showADS="false" ') +
                        (sCRUD && sCRUD.length > 0 ? 'CRUD="' + sCRUD + '" ' : '') +
                        (bMany && (str2Boolean(bMany) == true) ? 'many="true" ' : '') +
                        (sAddProc && sAddProc.length > 0 ? 'addStoredProc="' + sAddProc + '" ' : '') +
                        (sDelProc && sDelProc.length > 0 ? 'delStoredProc="' + sDelProc + '" ' : '') +
                        (sUpdProc && sUpdProc.length > 0 ? 'updStoredProc="' + sUpdProc + '" ' : '') +
                        (bDisabled && (str2Boolean(bDisabled) == true) ? 'CCDisabled="true" ' : '') +
                        (iTabIndex && (str2Int(iTabIndex) != 0) ? 'CCTabIndex="' + iTabIndex + '" ' : '') +
                        (sOnAfterChange && (sOnAfterChange.length > 0) ? 'onAfterChange="' + sOnAfterChange + (sOnAfterChange.indexOf('(') == -1 ? '()' : '') + '" ' : '') +
                        (sOnTabSave && (sOnTabSave.length > 0) ? 'onTabSave="' + sOnTabSave + (sOnTabSave.indexOf('(') == -1 ? '()' : '') + '" ' : '') +
                        (sOnTabAdd && (sOnTabAdd.length > 0) ? 'onTabAdd="' + sOnTabAdd + (sOnTabAdd.indexOf('(') == -1 ? '()' : '') + '" ' : '') +
                        (sOnTabDel && (sOnTabDel.length > 0) ? 'onTabDel="' + sOnTabDel + (sOnTabDel.indexOf('(') == -1 ? '()' : '') + '" ' : '') +
                        (sOnTabValidate && (sOnTabValidate.length > 0) ? 'onTabValidate="' + sOnTabValidate + (sOnTabValidate.indexOf('(') == -1 ? '()' : '') + '" ' : '');

      if (bTagEnd == true)
         sAPDElement += '>\n';

      return sAPDElement;
   }

   function APDTabGroupEnd(){
      return '</' + sAPDNS + ':APDTabGroup>\n';
   }

   function APDTextArea(sID, sValue, iMaxLength, iHeight, iWidth, bDisabled,
                        bRequired, iTabIndex, bCanDirty, bEndTag, sOnChange) {
      var bTagEnd = true;
      bTagEnd = ((bEndTag) ? str2Boolean(bEndTag) : true);

      var sAPDElement = '<' + sAPDNS + ':APDTextarea ' +
                        'id="' + sID + '" name="' + sID + '" ' +
                        'value="' + sValue + '" ' +
                        (iMaxLength && (str2Int(iMaxLength) > 0) ? 'maxLength="' + iMaxLength + '" ' : '') +
                        (iHeight && (str2Int(iHeight) > 0) ? 'height="' + iHeight + '" ': '') +
                        (iWidth && (str2Int(iWidth) > 0) ? 'width="' + iWidth + '" ': '') +
                        (bDisabled && (str2Boolean(bDisabled) == true) ? 'CCDisabled="true" ' : '') +
                        (bRequired && (str2Boolean(bRequired) == true) ? 'required="true" ' : '') +
                        (iTabIndex && (str2Int(iTabIndex) != 0) ? 'CCTabIndex="' + iTabIndex + '" ' : '') +
                        (bCanDirty && (str2Boolean(bCanDirty) == true) ? 'canDirty="true" ' : 'canDirty="false" ') +
                        (sOnChange && (sOnChange.length > 0) ? 'onChange="' + sOnChange + (sOnChange.indexOf('(') == -1 ? '()' : '') + '" ' : '');

      if (bTagEnd == true)
         sAPDElement += '/>\n';

      return sAPDElement;
   }

   //Utility functions
   function str2Boolean(str) {
      var bRetVal = false;
      if (typeof(str) == "string") {
         if (str != "") {
            str = str.toLowerCase();
            bRetVal = ((str == "true" ||
                        str == "y" ||
                        str == "1") ? true : false);
         } else
            bRetVal = true;
      }
      else if (typeof(str) == "boolean")
         bRetVal = str;
      else
         bRetVal = Boolean(str);

      return bRetVal;
   }

   function str2Int(str) {
      if (str != "")
         return parseInt(str, 10);
      else
         return 0;
   }
/*
   function trim_string() {
      var ichar, icount;
      var strValue = this;
      ichar = strValue.length - 1;
      icount = -1;
      while (strValue.charAt(ichar)==' ' && ichar > icount)
         --ichar;
      if (ichar!=(strValue.length-1))
         strValue = strValue.slice(0,ichar+1);
      ichar = 0;
      icount = strValue.length - 1;
      while (strValue.charAt(ichar)==' ' && ichar < icount)
         ++ichar;
      if (ichar!=0)
         strValue = strValue.slice(ichar,strValue.length);
      return strValue;
    // Use a regular expression to replace leading and trailing
    // spaces with the empty string
    //return this.replace(/(^\s*)|(\s*$)/g, "");
   }
*/
   function encode(str) {
      if (str != "")
         return escape(str);
      else
         return str;
   }

   function unencode(str) {
      if (str != "")
         return unescape(str);
      else
         return str;
   }

   function formatSQLDateTime(str){
      if (str) {
         var re = /(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(.*)/;

         var a = str.match(re);
         if (a) {
            if (a.length == 8){
               return a[2] + "/" + a[3] + "/" + a[1] + " " + a[4] + ":" + a[5] + ":" + a[6];
            }
         }
      }

      return "";
   }

   function formatSQLDate(str){
      if (str) {
         var re = /(\d{4})-(\d{2})-(\d{2})T(.*)/;

         var a = str.match(re);
         if (a) {
            if (a.length == 5){
               return a[2] + "/" + a[3] + "/" + a[1];
            }
         }
      }

      return "";
   }

   function formatSQLTime(str){
      if (str) {
         var re = /(.*)T(\d{2}):(\d{2}):(\d{2})(.*)/;

         var a = str.match(re);
         if (a) {
            if (a.length == 6){
               return a[2] + ":" + a[3] + ":" + a[4];
            }
         }
      }

      return "";
   }

   function getValueFromXML(ctx, attr){
      var retObj = {
                     value     : "",
                     dataType  : null,
                     precision : null,
                     scale     : null,
                     maxLength : null,
                     required  : false,
                     disabled  : false
                   }
      if (typeof(ctx) == "object" && attr != "" && ctx[0]) {

         var objVal = ctx[0].selectSingleNode("@" + attr);
         if (objVal)
            retObj.value = objVal.value;

         var ctxRoot = ctx[0].selectSingleNode("/Root");
         var sElement = ctx[0].nodeName;

         var ctxAttr = ctxRoot.selectSingleNode("Metadata[@Entity='"+ sElement + "']/Column[@Name='" + attr + "']");

         if (ctxAttr) {
            retObj.dataType = ctxAttr.selectSingleNode("@DataType").value;
            retObj.precision = (ctxAttr.selectSingleNode("@Precision") ? ctxAttr.selectSingleNode("@Precision").value : "");
            retObj.scale = (ctxAttr.selectSingleNode("@Scale") ? ctxAttr.selectSingleNode("@Scale").value : "");
            retObj.maxLength = (ctxAttr.selectSingleNode("@MaxLength") ? ctxAttr.selectSingleNode("@MaxLength").value : "");
            if ((ctxAttr.selectSingleNode("@Nullable") ? ctxAttr.selectSingleNode("@Nullable").value : "").toLowerCase().Trim() == "no")
               retObj.required = true;
            else
               retObj.required = false;
         } else
            retObj.disabled = true;

      }

      return retObj;
   }

   function getSelectFromXML(ctx, attr, sReferenceList){
      var retObj = {
                     value       : "",
                     valueList   : null,
                     captionList : null,
                     required    : false,
                     disabled    : false,
                     error       : null
                   }
      if (typeof(ctx) == "object" && attr != "" && ctx[0]) {


         var objVal = ctx[0].selectSingleNode("@" + attr);
         if (objVal)
            retObj.value = objVal.value;

         var ctxRoot = ctx[0].selectSingleNode("/Root");
         var sElement = ctx[0].nodeName;

         var sElement = ctx[0].nodeName;

         var ctxAttr = ctxRoot.selectSingleNode("Metadata[@Entity='"+ sElement + "']/Column[@Name='" + attr + "']");
         var iPrecision = null;

         if (ctxAttr) {
            if ((ctxAttr.selectSingleNode("@Nullable") ? ctxAttr.selectSingleNode("@Nullable").value : "").toLowerCase().Trim() == "no")
               retObj.required = true;
            else
               retObj.required = false;

            iPrecision = (ctxAttr.selectSingleNode("@Precision") ? ctxAttr.selectSingleNode("@Precision").value : null);
         } else
            retObj.disabled = true;

         var ctxRef = ctxRoot.selectNodes("Reference[@List='" + sReferenceList + "']");
         var sValueList = sCaptionList = "";

         if (ctxRef) {
            for (var i = 0; i < ctxRef.length; i++){
               sValueList += ctxRef[i].selectSingleNode("@ReferenceID").value + "|";
               sCaptionList += ctxRef[i].selectSingleNode("@Name").value + "|";
            }
         }
         //remove the last "|"
         if (sValueList.substr(sValueList.length - 1, 1) == "|")
            sValueList = sValueList.substr(0, sValueList.length - 1);
         if (sCaptionList.substr(sCaptionList.length - 1, 1) == "|")
            sCaptionList = sCaptionList.substr(0, sCaptionList.length - 1);


         retObj.valueList = sValueList;
         retObj.captionList = sCaptionList;

      }

      return retObj;
   }

   function getPhoneFromXML(ctx, attrArea, attrExch, attrNum, attrExt, attrAll){
      var retObj = {
                     value       : "",
                     required    : false,
                     disabled    : false
                   }
      if (typeof(ctx) == "object" && ctx[0]) {


         var sPhone = "";
         var objVal = ctx[0].selectSingleNode("@" + attrArea);
         if (objVal)
            sPhone = objVal.value;
         else
            sPhone = "   ";

         var objVal = ctx[0].selectSingleNode("@" + attrExch);
         if (objVal)
            sPhone += objVal.value;
         else
            sPhone += "   ";

         var objVal = ctx[0].selectSingleNode("@" + attrNum);
         if (objVal)
            sPhone += objVal.value;
         else
            sPhone += "    ";

         var objVal = ctx[0].selectSingleNode("@" + attrExt);
         if (objVal)
            sPhone += objVal.value;

         if (attrAll && attrAll.length > 0) {
            var objVal = ctx[0].selectSingleNode("@" + attrAll);
            if (objVal)
               sPhone = objVal.value;
            else
               sPhone = "";
         }


         retObj.value = sPhone;
         var ctxRoot = ctx[0].selectSingleNode("/Root");
         var sElement = ctx[0].nodeName;

         var sElement = ctx[0].nodeName;

         var ctxAttr = ctxRoot.selectSingleNode("Metadata[@Entity='"+ sElement + "']/Column[@Name='" + attrArea + "']");

         if (ctxAttr) {
            if ((ctxAttr.selectSingleNode("@Nullable") ? ctxAttr.selectSingleNode("@Nullable").value : "").toLowerCase().Trim() == "no")
               retObj.required = true;
            else
               retObj.required = false;
         } else
            retObj.disabled = true;
      }

      return retObj;
   }

]]>
</ms:script>
</xsl:stylesheet>
