<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:local="http://local.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt">

  <xsl:import href="msxsl/msjs-client-library-htc.js"/>

  <msxsl:script language="JScript" implements-prefix="local">

      function ClaimAspectToPertainsTo( strTypeName, strNumber )
      {
          return ( strNumber == "0" ) ? strTypeName : strTypeName + " " + strNumber;
      }

      function GetDefaultContext( strContext )
      {
          return ( strContext == "" ) ? "clm" : strContext;
      }

  </msxsl:script>

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="RawContext"/>
  <xsl:param name="UserId"/>
  <xsl:param name="TaskCompleted"/>
  <xsl:param name="LynxId"/>
  <xsl:param name="WindowID"/>

  <!-- APDXSL:C:\websites\apd\v1.2\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspDiaryGetDetailXML,CheckListComplete.xsl, 1547, 6790, 33   -->

  <xsl:template match="/">

    <xsl:variable name="PertainsTo">
      <xsl:value-of select="/Root/DiaryTask/@ClaimAspectCode"/>
      <xsl:if test="/Root/DiaryTask/@ClaimAspectNumber != '0'"><xsl:value-of select="/Root/DiaryTask/@ClaimAspectNumber"/></xsl:if>
    </xsl:variable>

    <html>
      <head>
        <title>Complete A Task</title>
        <link rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>

        <style type="text/css">
            A {color:black; text-decoration:none;}
        </style>

        <script type="text/javascript" language="JavaScript1.2" src="/js/apdcontrols.js"></script>
       	<script type="text/javascript" language="JavaScript1.2" src="/webhelp/RoboHelp_CSH.js"></script>
        <script type="text/javascript">
          document.onhelp=function(){
      		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,8);
      		  event.returnValue=false;
      		}
		    </script>

        <script language="JavaScript">

            window.name = "thisWin";
            window.returnValue = "false";

            var strNotApplicable  = "0";

            var sCommentCompleteRequired = '<xsl:value-of select="/Root/DiaryTask/@CommentCompleteRequiredFlag"/>';
            var sCommentNARequired = '<xsl:value-of select="/Root/DiaryTask/@CommentNARequiredFlag"/>';
            var strPertainsTo = '<xsl:value-of select="$PertainsTo"/>';
            var strContext = '<xsl:value-of select="local:GetDefaultContext(string($RawContext))"/>';
            var strCheckListID = "<xsl:value-of select="/Root/DiaryTask/@CheckListID"/>";
            var strSysLastUpdatedDate  = "<xsl:value-of select="/Root/DiaryTask/@SysLastUpdatedDate"/>";
            var strNoteRequiredFlag = "<xsl:value-of select="/Root/DiaryTask/@CommentNARequiredFlag"/>";
            var strUserID = "<xsl:value-of select="string($UserId)"/>";
            var iNotesMaxLength = parseInt("<xsl:value-of select="/Root/Metadata[@Entity='DiaryTask']/Column[@Name='Note']/@MaxLength"/>", 10);

            <![CDATA[
              //function initPage(){
              function __pageInit(){
                  var strReason = txtReasonPrefix.value;
                  var iNotesNewLen = iNotesMaxLength - strReason.length - 1;
                  txtReason.maxLength = iNotesNewLen;
                  setPrefix();
              }

              function setPrefix(){
                if (rbTaskNotApplicable.checked == true){
                  var strReason = "I marked the task '" + txtTaskName.value + "' as 'Not Applicable' because";
                  txtReasonPrefix.value = strReason;
                  txtReason.required = (sCommentNARequired == "1");
                  var iNotesNewLen = iNotesMaxLength - strReason.length - 1;
                  txtReason.maxLength = iNotesNewLen;
                  txtReason.value = "";
                  txtReasonPrefix.style.display = "inline";
                }

                if (rbTaskComplete.checked == true){
                  var strReason = "";
                  txtReasonPrefix.value = strReason;
                  txtReason.required = (sCommentCompleteRequired == "1");
                  var iNotesNewLen = iNotesMaxLength - strReason.length - 1;
                  txtReason.maxLength = iNotesNewLen;
                  txtReason.value = "'" + txtTaskName.value + "' task completed.";
                  txtReasonPrefix.style.display = "none";
                }
              }

              function doSave(){
                var sProc = "";
                var sRequest = "";
                var aRequests = new Array();
                var sMethod = "";
                var iNotesLen = txtReasonPrefix.value.length + " " + txtReason.value.length;
                var sNotes = "";

                if (txtReason.required == true && txtReason.value == ""){
                  ClientWarning( "Please enter a comment to be associated with this task completion." );
                  return;
                }

                if (iNotesLen > iNotesMaxLength){
                  ClientWarning( "Please restrict your reason why this task is no longer applicable to " + iNotesMaxLength + " characters.  The current reason is " + iNotesLen + " characters long." );
                  return;
                }

                if (txtReason.value.length > 0)
                  sNotes = txtReasonPrefix.value + " " + txtReason.value;

                sProc = "uspWorkflowCompleteTask";
                sRequest = "ChecklistID=" + strCheckListID +
                            "&NotApplicableFlag=" + (rbTaskNotApplicable.checked ? "1" : "0")+
                            "&NoteRequiredFlag=" + (txtReason.required ? "1" : "0") +
                            "&Note=" + escape(sNotes) +
                            "&UserID=" + strUserID +
														"&NoteTypeID=" + selNoteType.value +
                            "&SysLastUpdatedDate=" + strSysLastUpdatedDate;
                sMethod = "ExecuteSpNp";

                //alert(sProc + "\n" + sRequest); return;

                aRequests.push( { procName : sProc,
                                  method   : sMethod,
                                  data     : sRequest }
                              );
                var sXMLRequest = makeXMLSaveString(aRequests);
                //alert(sXMLRequest);

                var objRet = XMLSave(sXMLRequest);

                if (objRet && objRet.code == 0) {
                  dialogArguments.refreshCheckListWindow(false);
                  window.close();
                }
              }

              function doCancel(){
                window.returnValue = false;
                window.close();
              }
            ]]>
        </script>

      </head>
      <body onload1="initPage()">
        <div style="position:absolute; z-index:1; top:10px; left:10px;">
            <xsl:apply-templates select="/Root/DiaryTask">
              <xsl:with-param name="UserId"><xsl:value-of select="$UserId"/></xsl:with-param>
              <xsl:with-param name="TaskCompleted"><xsl:value-of select="$TaskCompleted"/></xsl:with-param>
            </xsl:apply-templates>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="DiaryTask">

    <xsl:param name="UserId"/>
    <xsl:param name="TaskCompleted"/>

    <table width='100%' border='0' cellspacing='0' cellpadding='2' style="table-layout:fixed">
      <colgroup>
        <col width="70px"/>
        <col width="*"/>
      </colgroup>
      <tr>
        <td>Task Name</td>
        <td>
          <IE:APDLabel id="txtTaskName" name="txtTaskName" width="250" CCDisabled="false" >
            <xsl:attribute name="value"><xsl:value-of select="@TaskName"/></xsl:attribute>
          </IE:APDLabel>
        </td>
      </tr>
      <tr>
        <td>Comment</td>
        <td>
          <IE:APDLabel id="txtComment" name="txtComment" width="250" CCDisabled="false" >
            <xsl:attribute name="value"><xsl:value-of select="@UserTaskDescription"/></xsl:attribute>
          </IE:APDLabel>
        </td>
      </tr>
      <tr valign="top">
        <td>Mark Task as</td>
        <td>
          <IE:APDRadioGroup id="rbgMarkTask" name="rbgMarkTask" required="false" canDirty="false" CCDisabled="false">
            <IE:APDRadio id="rbTaskComplete" name="rbTaskComplete" caption="Complete" value="1" groupName="rbgMarkTask" checkedValue="1" uncheckedValue="0" required="false" CCDisabled="false" CCTabIndex="1" onChange="setPrefix()" />
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <IE:APDRadio id="rbTaskNotApplicable" name="rbTaskNotApplicable" caption="Not Applicable" value="0" groupName="rbgMarkTask" checkedValue="2" uncheckedValue="0" required="false" CCDisabled="false" CCTabIndex="1" onChange="setPrefix()" />
          </IE:APDRadioGroup>
        </td>
      </tr>
      <tr>
        <td>Note Type</td>
        <td>
          <xsl:choose>
            <xsl:when test="count(/Root/Reference[@List='NoteType'])=1">
              <input type="hidden" id="selNoteType" name="selNoteType" >
                <xsl:attribute name="value"><xsl:value-of select="/Root/Reference[@List='NoteType']/@ReferenceID"/></xsl:attribute>
							</input>
              <IE:APDLabel id="selNoteTypeLabel" name="selNoteTypeLabel" width="250" >
                <xsl:attribute name="value"><xsl:value-of select="/Root/Reference[@List='NoteType']/@Name"/></xsl:attribute>
              </IE:APDLabel>
            </xsl:when>
            <xsl:otherwise>
              <IE:APDCustomSelect id="selNoteType" name="selNoteType" displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="1" required="false" width="246" onChange="">
                <xsl:attribute name="value"><xsl:value-of select="/Root/Reference[@List='NoteType']/@ReferenceID"/></xsl:attribute>
                <xsl:for-each select="/Root/Reference[@List='NoteType']">
              	<IE:dropDownItem imgSrc="" style="display:none">
                  <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                  <xsl:value-of select="@Name"/>
                </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </tr>
      <tr>
        <td valign="top">Note</td>
        <td style="height:150px" valign="top">
          <IE:APDLabel id="txtReasonPrefix" name="txtReasonPrefix" height="32" width="250" CCDisabled="false" style="display:none" >
            <xsl:attribute name="value">I marked the task '<xsl:value-of select="@TaskName"/>' as 'Complete' because</xsl:attribute>
          </IE:APDLabel>
          <IE:APDTextArea id="txtReason" name="txtReason" width="242" height="110" canDirty="false" CCDisabled="false" CCTabIndex="2" onChange="" >
            <xsl:attribute name="maxLength"><xsl:value-of select="/Root/Metadata[@Entity='DiaryTask']/Column[@Name='Note']/@MaxLength"/></xsl:attribute>
            <xsl:attribute name="required"><xsl:value-of select="boolean(/Root/DiaryTask/@CommentCompleteRequiredFlag = '1')"/></xsl:attribute>
          </IE:APDTextArea>
        </td>
      </tr>
      <tr>
        <td colspan='2'>
          <table border="0" cellpadding="0" cellspacing="0" style="width:100%;table-layout:fixed">
            <colgroup>
              <col width="*"/>
              <col width="150px"/>
              <col width="*"/>
            </colgroup>
            <tr>
              <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
              <td>
                <IE:APDButton id="btnSave" name="btnSave" value="Commit" width="" CCDisabled="false" CCTabIndex="3" onButtonClick="doSave()"/>
                <IE:APDButton id="btnCancel" name="btnCancel" value="Cancel" width="" CCDisabled="false" CCTabIndex="4" onButtonClick="doCancel()"/>
              </td>
              <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </xsl:template>

</xsl:stylesheet>

