<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:local="http://local.com/mynamespace"
    id="Notes">

<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/generic-template-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function GetFileSize(strFile) {
      var fso, sImgInfo;
      sImgInfo = "";
      fso = new ActiveXObject("Scripting.FileSystemObject");
      strFile = strFile.replace(/\\\\/g, "\\");
      if (fso.FileExists(strFile)){
        var f = fso.GetFile(strFile);
        if (f){
          sImgInfo += "; " + Math.round(f.size / 1024) + " kb";
        }
      }
      else {
        sImgInfo = "Not Found";
      }
      
      return sImgInfo;
    }    

    function UTCConvertDate(vDate) {
      var vYear = vDate.substr(0,4);
      var vMonth = vDate.substr(5,2);
      var vDay = vDate.substr(8,2);
      vDate = vMonth + '/' + vDay  + '/' + vYear
      return vDate;
    }
    
    function FixDoubleSlash(str){
      var strRet = "";
      if (str != ""){
        strRet = str.replace(/\\\\/g, "\\");
      }
      return strRet;
    }
    
  ]]>
</msxsl:script>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="ShopLocationID"/>
<xsl:param name="UserID"/>
<xsl:param name="WindowID"/>
<xsl:param name="windowState"/>
<xsl:param name="ImageRootDir"/>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<xsl:template match="/Root">

<xsl:variable name="Metadata"><xsl:copy-of select="/Root/Metadata"/></xsl:variable>

<xsl:value-of select="session:XslUpdateSessionNodeList( 'NoteMetadata', $Metadata )"/>

<HTML>

<HEAD>
<TITLE>Shop Notes</TITLE>

  <LINK rel="stylesheet" href="/css/apdwindow.css" type="text/css"/>
  <LINK rel="stylesheet" href="/css/Documents.css" type="text/css"/>
<STYLE type="text/css">
    A {color:black; text-decoration:none;}
    .thumbNail {
      border-left: 1px solid #C0C0C0;
      border-top: 1px solid #C0C0C0;
      border-right: 1px solid #808080;
      border-bottom: 1px solid #808080;
      background-color: #F5F5F5;
      width:98px;
      height:88px;
      margin:5px;
      cursor: hand;
    }
</STYLE>
<SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>

<script language="javascript">

  var gsWindowID = "<xsl:value-of select='$WindowID'/>";
  var sWindowName = "winDocs";
  var vShopCRUD = "<xsl:value-of select='$ShopCRUD'/>";
  var sWindowState = "<xsl:value-of select='$windowState'/>";
  var gsShopLocationID = "<xsl:value-of select="$ShopLocationID"/>";
  var gsUserID = "<xsl:value-of select="$UserID"/>";
  var gsImageRootDir = "<xsl:value-of select="$ImageRootDir"/>";
  var bMouseInToolTip = false;
  var iToolTipWidth = iToolTipHeight = 0;
  var iTimeout = -1;
  var oCurImg = null;
  var oDocPopup  = window.createPopup();

<![CDATA[

  function pageInit(){
    if (sWindowState == "max")
      setWindowState(sWindowState);
  }

  // Overridden window function for display/hiding of 'extra' table columns.
  function setWindowState( newstate )
  {
    try {
      if ( this.state != newstate ) {
        //setNotesExtraColDisplay( ( newstate == 'max' ) ? '' : 'none' );
        this.state = newstate;
        sWindowState = newstate;
      }
    } catch ( e ) { ClientError( e.message ); }
  }

  function refreshDocumentssWindow( bFromTimer, vShopID, vUserID)
  {
    try {
      if ( ( String( bFromTimer ) == "undefined" ) || ( bFromTimer == null ) )
        bFromTimer = true;

      if (bFromTimer) {
        if (sWindowState == "max"){
          parent.loadContent("/ProgramMgr/SMTDocuments.asp?WindowID=" + gsWindowID + "&ShopLocationID=" + gsShopLocationID + "&UserID=" + gsUserID + "&winState=" + sWindowState);
          return;
        }
        window.location.reload();
      } else {
        parent.loadContent("/ProgramMgr/SMTDocuments.asp?WindowID=" + gsWindowID + "&ShopLocationID=" + gsShopLocationID + "&UserID=" + gsUserID);
      }
    } 
    catch ( e ) { alert( "SMTDocuments.xsl::refreshDocumentssWindow() " + e.message ); }
  }
  
  function addDocument(){
    var strRet = window.showModalDialog("SMTAddDocument.asp?WindowID=" + gsWindowID + "&ShopLocationID=" + gsShopLocationID,"",
                                        "dialogHeight:200px;dialogWidth:400px;center:1;help:0;resizable:0;scroll:0;status:0");
    if (strRet == "Save"){
      window.location.reload();
    }
  }

  function keepToolTip(){
    bMouseInToolTip = true;
    if (iTimeout > 0) window.clearTimeout(iTimeout);
  }

  function showToolTip(obj){
    if (obj && xmlDocument){
      if (obj.DocumentID != "") {
        var oNode = xmlDocument.selectSingleNode("/Root/Document[@DocumentID='" + obj.DocumentID + "']");
        if (oNode){
          var sComments = oNode.getAttribute("Comments");
          if (sComments == null) return;
          divComments.innerText = sComments;
          var iLeft = iTop = 0;
          
          if (iToolTipWidth == 0 || iToolTipHeight == 0){
            toolTip.style.display = "inline";
            iToolTipWidth = toolTip.offsetWidth;
            iToolTipHeight = toolTip.offsetHeight;
          }
            
          iTop = obj.offsetTop + iToolTipHeight  + 5;
          iLeft = obj.offsetLeft + 5;
          
          if ((iLeft + iToolTipWidth) > document.body.offsetWidth)
            iLeft = document.body.offsetWidth - iToolTipWidth - 10;
            
          if ((iTop + iToolTipHeight) > document.body.offsetHeight){
            iTop = document.body.offsetHeight - iToolTipHeight - 10;
          }
          
          toolTip.style.top = iTop;
          toolTip.style.left = iLeft;
          toolTip.style.display = "inline";
          bMouseInToolTip = true;
          if (iTimeout > 0) window.clearTimeout(iTimeout);
          iTimeout = window.setTimeout("bMouseInToolTip=false;hideToolTip()", 2000);
        }
        
      } else  
        hideToolTip();
    } else {
      bMouseInToolTip = false;
      hideToolTip();
    }
  }
  
  function hideToolTip(){
    if (bMouseInToolTip == false) {
      if (iTimeout > 0) window.clearTimeout(iTimeout);
      toolTip.style.display = "none";
      bMouseInToolTip = false;
    }
  }
  
  function displayImage(obj){
    if (obj && xmlDocument){
      if (obj.DocumentID != "") {
        var oNode = xmlDocument.selectSingleNode("/Root/Document[@DocumentID='" + obj.DocumentID + "']");
        if (oNode){
          var sImageLocation = oNode.getAttribute("ImageLocation");
          sImageLocation = sImageLocation.replace(/\\\\/g, "\\");
          var sDocPath = gsImageRootDir + sImageLocation;
          window.showModelessDialog("/EstimateDocView.asp?docPath=" + sDocPath, "",
                                              "dialogHeight:640px;dialogWidth:750px;center:1;help:0;resizable:0;scroll:0;status:0");
        }
      }
    }
  }
  
  function showContext(obj){
    oCurImg = obj;
    bMouseInToolTip = false;
    hideToolTip();
    var lefter = event.offsetX;
    var topper = event.offsetY;
    event.returnValue = false;
    event.cancelBubble = true;
    oDocPopup.document.body.innerHTML = divContextMenu.innerHTML;
    oDocPopup.show(lefter, topper, 153, 64, obj);
  }
  
  function viewImage(){
    if (oCurImg){
      displayImage(oCurImg);
    }
  }
  
  function editDocument(){
    if (oCurImg){
      var iDocumentID = oCurImg.DocumentID;
      var oNode = xmlDocument.selectSingleNode("/Root/Document[@DocumentID='" + iDocumentID + "']");
      if (oNode){
        var oDocument = new Object();

        oDocument.ShopLocationID = gsShopLocationID;
        oDocument.DocumentID = iDocumentID;
        oDocument.DocumentType = oNode.getAttribute("DocumentType");
        oDocument.EffectiveDate = oNode.getAttribute("EffectiveDate");
        oDocument.ExpirationDate = oNode.getAttribute("ExpirationDate");
        oDocument.Comments = oNode.getAttribute("Comments");
        
        var strRet = window.showModalDialog("SMTEditDocument.asp?WindowID=" + gsWindowID + "&ShopLocationID=" + gsShopLocationID + "&DocumentID=" + iDocumentID, oDocument,
                                            "dialogHeight:180px;dialogWidth:400px;center:1;help:0;resizable:0;scroll:0;status:0");
        if (strRet == "Save"){
          window.location.reload();
        }      
      }
    }
  }

  function deleteDocument(){
    if (oCurImg){
			if (YesNoMessage("Confirm Delete", "Are you sure you wish to delete this document?") == "Yes"){
        var iDocumentID = oCurImg.DocumentID;
        var sProc = "uspShopDocumentDel";
        var sRequest = "ShopLocationID=" + gsShopLocationID +
                       "&DocumentID=" + iDocumentID + 
                       "&UserID=" + gsUserID;
    
        if (sProc != "" && sRequest != ""){
            var aRequests = new Array();
            aRequests.push( { procName : sProc,
                              method   : "ExecuteSpNp",
                              data     : sRequest }
                          );
            var objRet = XMLSave(makeXMLSaveString(aRequests));
            window.location.reload();
        }
      }
    }
  }
]]>

</script>

</HEAD>
<BODY unselectable="on" style="background-color:#FFFFFF;overflow:hidden;border:0px;padding:1px;" onload="pageInit()" tabIndex="-1" topmargin="0"  leftmargin="0">
  <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;background-color:#067BAF;width:100%;height:100%;" >
    <tr style="padding:1px;height:21px;" oncontextmenu="return false">
      <td>
        <div id="divWinToolBar" class="clWinToolBar" >
          <img border="0" src="/images/new.gif" onClick="addDocument(); return false" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Add Document" />
          <img src="/images/spacer.gif" width="4" height="2" border="0"/>
          <img border="0" src="/images/smrefresh.gif" onClick="refreshDocumentssWindow(false, gsShopLocationID, gsUserID); return false" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Refresh" />
        </div>
      </td>
    </tr>
    <tr style="padding:1px;">
      <td>
        <div style="width:100%;height:100%;overflow:hidden;overflow:auto;background-color:#FFFFFF" class="clScrollTable">
            <xsl:for-each select="Document">
              <xsl:sort select="@ExpirationDate"/>
              <xsl:call-template name="ShopDocument">
                <xsl:with-param name="RootDir"><xsl:value-of select="$ImageRootDir"/></xsl:with-param>
              </xsl:call-template>
            </xsl:for-each>
        </div>
      </td>
    </tr>
  </table>
  <div id="toolTip" name="toolTip" style="position:absolute;top:10px;left:10px;display:none;height:75px;width:200px;cursor:default;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)" onclick="bMouseInToolTip=false;hideToolTip()" onmouseenter="keepToolTip()" onmouseleave="bMouseInToolTip=false;hideToolTip()">
    <table id="tblToolTip" border="0" cellspacing="0" cellpadding="0" style="height1:100%;width:100%;border:1px solid #556B2F;background-color:#FFFFFF;font:11px Tahoma" onreadystatechange="checkToolTipSize()">
      <tr style="height:21px;text-align:center;font-weight:bold;background-color:#556B2F;">
        <td style="color:#FFFFFF">Comments</td>
      </tr>
      <tr valign="top">
        <td>
          <div id="divToolTipFlow" style="overflow:auto;height:100%;width:100%;padding:3px;">
            <table border="0" cellpadding="2" cellspacing="0" style="border-collapse:collapse;width:100%">
              <colgroup>
                <col width="*"/>
              </colgroup>
              <tr valign="top">
                <td><div id="divComments" style="height:50px;overflow:auto;font:11px Tahoma">Test</div></td>
              </tr>
            </table>
          </div>
        </td>
      </tr>
    </table>
  </div>
  <div id="divContextMenu" style="position:absolute;top:15;left:15;display:none">
    <div style="width:150px;border:1px solid #000000;background-color:#FFFFFF;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)">
      <table border="0" cellpadding="3" cellspacing="0" style="font:11px Tahoma, Arial;width:100%;cursor:hand;background-color:#FFFFFF">
        <tr>
          <td onmouseenter="this.style.backgroundColor='#FFF8DC'" onmouseleave="this.style.backgroundColor='#FFFFFF'" onclick="parent.viewImage()">View Image</td>
        </tr>
        <tr>
          <td onmouseenter="this.style.backgroundColor='#FFF8DC'" onmouseleave="this.style.backgroundColor='#FFFFFF'" onclick="parent.editDocument()">View Document Details</td>
        </tr>
        <tr>
          <td onmouseenter="this.style.backgroundColor='#FFF8DC'" onmouseleave="this.style.backgroundColor='#FFFFFF'" onclick="parent.deleteDocument()">Delete Document</td>
        </tr>
      </table>
    </div>
  </div>  
  <xml id="xmlDocument" name="xmlDocument">
    <xsl:copy-of select="/"/>
    <!-- <Root>
      <xsl:for-each select="/Root/Document">
      <Document>
        <xsl:copy>
          <xsl:apply-templates select="@*"/>
        </xsl:copy> -->

        <!-- <xsl:attribute name="DocumentID"><xsl:value-of select="@DocumentID"/></xsl:attribute>
        <xsl:attribute name="DocumentType"><xsl:value-of select="@DocumentType"/></xsl:attribute>
        <xsl:attribute name="CreatedDate"><xsl:value-of select="@CreatedDate"/></xsl:attribute>
        <xsl:attribute name="EffectiveDate"><xsl:value-of select="@EffectiveDate"/></xsl:attribute>
        <xsl:attribute name="ExpirationDate"><xsl:value-of select="@ExpirationDate"/></xsl:attribute>
        <xsl:attribute name="ImageLocation"><xsl:value-of select="@ImageLocation"/></xsl:attribute>
        <Comments>
          <![CDATA[
          <xsl:value-of disable-output-escaping="yes" select="@Comments"/>
          ]]>
        </Comments> -->
      <!-- </Document>
      </xsl:for-each>
    </Root> -->
  </xml>
</BODY>
</HTML>
</xsl:template>

<xsl:template name="ShopDocument">
  <xsl:param name="RootDir"/>
  <xsl:variable name="FixedImageRootDir"><xsl:value-of select="user:FixDoubleSlash(string($ImageRootDir))"/></xsl:variable>
  <xsl:variable name="FixedImageLocation"><xsl:value-of select="user:FixDoubleSlash(string(@ImageLocation))"/></xsl:variable>
  <span class="thumbNail" unselectable="on" ondblclick="displayImage(this)" onmouseenter="showToolTip(this)" onmouseleave="hideToolTip()" oncontextmenu="showContext(this)">
    <xsl:attribute name="name">tn<xsl:value-of select="position()"/></xsl:attribute>
    <xsl:attribute name="id">tn<xsl:value-of select="position()"/></xsl:attribute>
    <xsl:attribute name="DocumentID"><xsl:value-of select="@DocumentID"/></xsl:attribute>
    <center unselectable="on">
      <span class="thumbNailImage" style="height:80px;width:80px;">
      <table border="0" cellpadding="0" cellspacing="0" style="height:100%;width:100%;font:7pt Tahoma;">
        <tr valign="middle">
          <td style="text-align:center">
            <xsl:choose>
              <xsl:when test="contains('jpg gif png bmp emf wmf', @ImageType) = true()">
                <img  unselectable="on">
                  <xsl:attribute name="name">img<xsl:value-of select="position()"/></xsl:attribute>
                  <xsl:attribute name="id">img<xsl:value-of select="position()"/></xsl:attribute>
                  <xsl:attribute name="src">\Thumbnail.asp?height=90&amp;width=90&amp;Doc=<xsl:value-of select="$FixedImageRootDir"/><xsl:value-of select="$FixedImageLocation"/></xsl:attribute>
                </img>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="@DocumentType"/>
              </xsl:otherwise>
            </xsl:choose>
          </td>
        </tr>
      </table>
      </span>
    </center>
    <span class="thumbNailInfo" title="Expiration Date; File Size">
      <xsl:variable name="FileInfo" select="user:GetFileSize(concat($RootDir, @ImageLocation))"/>
      <xsl:choose>
        <xsl:when test="$FileInfo = 'Not Found'">
          <xsl:attribute name="style">color:#FF0000</xsl:attribute>
          Not Found
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@ExpirationDate"/> <xsl:value-of select="$FileInfo"/>
        </xsl:otherwise>
      </xsl:choose>
    </span>
  </span>
</xsl:template>

</xsl:stylesheet>
