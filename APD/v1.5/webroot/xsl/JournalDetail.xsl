<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="JournalDetail">

<xsl:import href="msxsl/msxsl-function-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="LynxId"/>
<xsl:param name="UserId"/>
<xsl:param name="WindowID"/>

<xsl:template match="/Root">
<HTML>
<HEAD>
<TITLE></TITLE>
<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>
</HEAD>
  <BODY style="margin:0px;padding:0px">
    <table border="0" cellspacing="0" cellpadding="2">
      <colgroup>
        <col width="645px"/>
        <col width="80px"/>
      </colgroup>
      <tr>
        <td>
          <b>
            <div class="tdlabel" style="font-size:15px;font-weight:bold">Journal Details</div>
          </b>
        </td>
        <td>
          <b>
            <font color="#D8D8D8">
              <xsl:value-of select="RunTime"/>
            </font>
          </b>
        </td>
      </tr>
    </table>

    <xsl:for-each select="ClaimAspect">
      <xsl:sort select="@ClaimAspectNumber" data-type="number"/>
      <xsl:variable name="ClaimAspectID" select="@ClaimAspectID"/>

      <div class="tdlabel" style="padding:10px 0px 10px 0px">
        <b>
          <xsl:value-of select="concat('Vehicle ', @ClaimAspectNumber, ' - ', @ServiceChannel, ' - ', @Disposition)"/>
        </b>
      </div>
      <table border="1" cellspacing="0" cellpadding="2" style="border-collapse:collapse;table-layout:fixed;border-color:#C0C0C0">
        <colgroup>
          <col width="80px"/>
          <col width="60px"/>
          <col width="245px"/>
          <col width="80px"/>
          <col width="90px"/>
          <col width="90px"/>
          <col width="80px" style="text-align:right"/>
        </colgroup>
        <tr>
          <td class="TableSortHeader">Transaction Dt.</td>
          <td class="TableSortHeader">Code</td>
          <td class="TableSortHeader">Description</td>
          <td class="TableSortHeader">Dispatch #</td>
          <td class="TableSortHeader">Disbursement Dt.</td>
          <td class="TableSortHeader">Disbursement Method</td>
          <td class="TableSortHeader">Amount</td>
        </tr>
        <xsl:for-each select="//Dispatch[@ClaimAspectID=$ClaimAspectID]">
          <xsl:sort select="@TransactionDate" data-type="text" order="descending"/>
          <tr>
            <td>
              <xsl:value-of select="@TransactionDateFormatted"/>
            </td>
            <td>
              <xsl:value-of select="@TransactionDesc"/>
            </td>
            <td>
              <xsl:value-of select="@Description"/>
            </td>
            <td>
              <xsl:value-of select="@DispatchNumber"/>
            </td>
            <td>
              <xsl:value-of select="@DisbursementDate"/>
            </td>
            <td>
              <xsl:value-of select="@DisbursementMethodDesc"/>
            </td>
            <td>
              <xsl:choose>
                <xsl:when test="@DisbursementMethodCD != ''">
                  <xsl:value-of select="@DisbursementAmount"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="@Amount"/>
                </xsl:otherwise>
              </xsl:choose>

            </td>
          </tr>
        </xsl:for-each>
      </table>
      <br/>
      <br/>
    </xsl:for-each>
  </BODY>
</HTML>

</xsl:template>

</xsl:stylesheet>
