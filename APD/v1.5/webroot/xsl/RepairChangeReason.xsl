<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimVehicle">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/msjs-client-library-htc.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->

<!-- XslParams - stuffed from COM before transform -->

<xsl:template match="/Root">


<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspClaimVehicleGetDetailXML,ClaimVehicle.xsl,4253,145   -->

<HEAD>
<TITLE>Repair Schedule Change Reason</TITLE>

<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>

<SCRIPT language="JavaScript">

<![CDATA[
  function checkOther(){
    txtOther.CCDisabled = !(selReason.text == "Other");
  }
  
  function doOk(){
    var sRet;
    if (selReason.selectedIndex == -1){
      ClientWarning("Please select a reason from the list.");
      return;
    }
    if (selReason.text == "Other") {
      if (txtOther.value.Trim() == "") {
        ClientWarning("Please describe the Other reason and try again.");
        if (txtOther.CCDisabled == false)
          txtOther.setFocus();
        return;
      }
      sRet = selReason.value + String.fromCharCode(0) + txtOther.value;
    } else 
      sRet = selReason.value + String.fromCharCode(0) + selReason.text;
    window.returnValue = sRet;
    
    window.close();
  }
]]>

</SCRIPT>

</HEAD>
<BODY unselectable="on" style="margin:0px;padding:5px;overflow:hidden;" tabIndex="-1" onload="">
  <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed;border-collapse:collapse">
    <colgroup>
      <col width="50px"/>
      <col width="*"/>
    </colgroup>
    <tr>
      <td>Reason:</td>
      <td>
        <IE:APDCustomSelect id="selReason" name="selReason" displayCount="6" blankFirst="false" canDirty="false" CCTabIndex="1" width="260" onChange="checkOther()">
          <xsl:for-each select="/Root/Reason">
        	  <IE:dropDownItem style="display:none">
              <xsl:attribute name="value"><xsl:value-of select="@ReasonID"/></xsl:attribute>
              <xsl:value-of select="@Name"/>
            </IE:dropDownItem>
          </xsl:for-each>
        </IE:APDCustomSelect>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <IE:APDTextArea id="txtOther" name="txtOther" maxLength="150" width="307" height="75" canDirty="false" CCTabIndex="2" CCDisabled="true"/>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <IE:APDButton id="btnOk" name="btnOk" value="Ok" CCTabIndex="3" onButtonClick="doOk()"/>
      </td>
    </tr>
  </table>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>
