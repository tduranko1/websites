<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:local="http://local.com/mynamespace"
    id="Notes">

<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/generic-template-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<msxsl:script language="JScript" implements-prefix="local">

    // Returns an integer based upon date and role logic.
    function getDateRoleState( date, complete_role )
    {
        var state = 0;
        var cur_date = new Date();
        var due_date = new Date( date );

        if ( ( due_date.valueOf() - ( 3600000 * 24 ) ) &gt; cur_date.valueOf() )
            state = 4;  // overdue
        else if ( due_date.valueOf() &gt; cur_date.valueOf() )
            state = 2;  // due within 24 hours

        if ( complete_role == '0' )
            state += 1;

        return state;
    }
   
</msxsl:script>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="ShopLocationID"/>
<xsl:param name="UserID"/>
<xsl:param name="WindowID"/>
<xsl:param name="windowState"/>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<xsl:template match="/Root">

<xsl:variable name="Metadata"><xsl:copy-of select="/Root/Metadata"/></xsl:variable>

<xsl:value-of select="session:XslUpdateSessionNodeList( 'NoteMetadata', $Metadata )"/>

<HTML>

<HEAD>
<TITLE>Shop Notes</TITLE>

<LINK rel="stylesheet" href="/css/apdwindow.css" type="text/css"/>
<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/APDWindow.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		

<script type="text/javascript"> 
      /*    document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,19);
		  event.returnValue=false;
		  };	*/
</script>

<script language="javascript">

  var gsWindowID = "<xsl:value-of select='$WindowID'/>";
  var sWindowName = "winNotes";
  var vShopCRUD = "<xsl:value-of select='$ShopCRUD'/>";
  var vNoteShopLocationID = "<xsl:value-of select='$ShopLocationID'/>";
  var vNoteUserID = "<xsl:value-of select='$UserID'/>";
  var sWindowState = "<xsl:value-of select='$windowState'/>";
  var saveNotesBG;
  var iNotes = 0;
  var iTotalNotes;

<![CDATA[

  function pageInit(){
    if (sWindowState == "max")
      setWindowState(sWindowState);
  }

  // Override from the standard version to handle note body popups.
  function noteGridMouseOver( oObject )
  {
    try {
      saveNotesBG = oObject.style.backgroundColor;
      oObject.style.backgroundColor="#FFEEBB";
      oObject.style.color = ( oObject.supervisor == "true" ) ? "#3333ff" : "#000066";
      oObject.style.cursor='hand';

      if ( sWindowState != 'max' )
        displayNoteBottomBody( oObject.idx, '' );
    } catch ( e ) { ClientError( e.message ); }
  }

  // Overrid from the standard version to handle note body popups.
  function noteGridMouseOut( oObject )
  {
    try {
      oObject.style.backgroundColor = saveNotesBG;
      oObject.style.color = ( oObject.supervisor == "true" ) ? "#0000ff" : "#000000";

      if ( sWindowState != 'max' )
        displayNoteBottomBody( oObject.idx, 'none' );
    } catch ( e ) { ClientError( e.message ); }
  }

  // Sets/Clears the note body display for the indexed note.
  function displayNoteBottomBody( idx, state )
  {
    try {
      var noteBodyBottom = document.getElementById( 'noteBodyBottom' + idx );
      if ( noteBodyBottom != null )
        noteBodyBottom.style.display = state;
    } catch ( e ) { ClientError( e.message ); }
  }

  // Overridden window function for display/hiding of 'extra' table columns.
  function setWindowState( newstate )
  {
    try {
      if ( this.state != newstate ) {
        setNotesExtraColDisplay( ( newstate == 'max' ) ? '' : 'none' );
        this.state = newstate;
        sWindowState = newstate;
      }
    } catch ( e ) { ClientError( e.message ); }
  }

  // Sets/Clears the display state for the 'extra' table columns.
  function setNotesExtraColDisplay( state )
  {
    try {
      if ( document.all.noteUserRole != null )
      {
        document.all.noteUserRole.style.display = state;
        document.all.noteBodyRight.style.display = state;
        document.all.noteUserRoleHeader.style.display = state;
        document.all.noteBodyRightHeader.style.display = state;
      }
    } catch ( e ) { ClientError( e.message ); }
  }

  function addNote()
  {
    try {
      
      if ( vShopCRUD.substr( 0, 1 ) != "C" )
      {
        ClientInfo( 'You do not have permission to add notes.  Please contact your supervisor.' );
      }
      else if ( vNoteShopLocationID != null && vNoteShopLocationID != "" && vNoteShopLocationID != "0" )
      {
        var strReq = "/ProgramMgr/SMTNoteDetails.asp?WindowID=" + gsWindowID + "&ShopLocationID=" + vNoteShopLocationID;
        var arrNotes = window.showModalDialog( strReq, window, "dialogHeight:170px; dialogWidth:480px; resizable:no; status:no; help:no; center:yes; ");
      }
      else
        ClientInfo( "You must open a shop before creating notes.");
    } 
    catch ( e ) { ClientError( e.message ); }
  }

  function editNote(row)
  {
    try {
      var strReq = "/ProgramMgr/SMTNoteDetails.asp?WindowID=" + gsWindowID + "&ShopLocationID=" + vNoteShopLocationID + "&DocumentID=" + row.children.DocumentID.innerText;
      var arrNotes = window.showModalDialog( strReq, window, "dialogHeight:210px; dialogWidth:480px; resizable:no; status:no; help:no; center:yes;" );
                            
    } 
    catch ( e ) { ClientError( e.message ); }
  }
    

  function refreshNotesWindow( bFromTimer, vShopID, vUserID)
  {
    try {
      if ( ( String( bFromTimer ) == "undefined" ) || ( bFromTimer == null ) )
        bFromTimer = true;

      if (bFromTimer) {
        if (sWindowState == "max"){
          parent.loadContent("/ProgramMgr/SMTNotes.asp?WindowID=" + gsWindowID + "&ShopLocationID=" + vNoteShopLocationID + "&UserID=" + vNoteUserID + "&winState=" + sWindowState);
          return;
        }
        window.location.reload();
      } else {
        parent.loadContent("/ProgramMgr/SMTNotes.asp?WindowID=" + gsWindowID + "&ShopLocationID=" + vNoteShopLocationID + "&UserID=" + vNoteUserID);
      }
    } 
    catch ( e ) { alert( "SMTNotes.xsl::refreshNotesWindow() " + e.message ); }
  }
]]>

</script>

</HEAD>
<BODY unselectable="on" style="background-color:#FFFFFF;overflow:hidden;border:0px;padding:1px;" onload="pageInit()" tabIndex="-1" topmargin="0"  leftmargin="0">
  <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;background-color:#067BAF;width:100%;height:100%;" >
    <tr style="padding:1px;height:19px;" oncontextmenu="return false">
      <td>
        <div id="divWinToolBar" class="clWinToolBar" >
          <img border="0" src="/images/smnew2.gif" onClick="addNote(); return false" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Add Note" />
          <img src="/images/spacer.gif" width="4" height="2" border="0"/>
          <img border="0" src="/images/smrefresh.gif" onClick="refreshNotesWindow(false, vNoteShopLocationID, vNoteUserID); return false" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Refresh" />
        </div>
      </td>
    </tr>
    <tr style="padding:1px;height:21px;">
      <td>
          <table width="100%" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSortNotes')" cellspacing="0" border="0" cellpadding="1"
            style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size:10px; text-align:center table-layout:fixed" oncontextmenu="return false">
            <tbody>
              <tr class="QueueHeader" style="height:20px">
                <td class="clWinGridTypeHeader" width="130" type="Date"  > Date Entered </td>
                <td class="clWinGridTypeHeader" width="80"  type="String"> User </td>
                <xsl:choose>
                  <xsl:when test="$windowState='max'">
                    <td class="clWinGridTypeHeader" width="100" type="String" id="noteUserRoleHeader"> Role </td>
                    <td class="clWinGridTypeHeader" width="260" type="String" id="noteBodyRightHeader"> Note </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td class="clWinGridTypeHeader" width="100" type="String" style="display:none" id="noteUserRoleHeader"> Role </td>
                    <td class="clWinGridTypeHeader" width="260" type="String" style="display:none" id="noteBodyRightHeader"> Note </td>
                  </xsl:otherwise>
                </xsl:choose>
              </tr>
            </tbody>
          </table>
      </td>
    </tr>
    <tr style="padding:1px;height:*;">
      <td>
        <div style="width:100%;height:100%;overflow:hidden;overflow:auto;background-color:#FFFFFF" class="clScrollTable">
          <table id="tblSortNotes" class="clWinGridTypeTable" width="100%" cellspacing="0" border="0" cellpadding="1">
            <col width="130" />
            <col width="80"  />
            <xsl:choose>
              <xsl:when test="$windowState='max'">
                <col width="100" id="noteUserRole"/>
                <col width="260" id="noteBodyRight"/>
              </xsl:when>
              <xsl:otherwise>
                <col width="100" style="display:none" id="noteUserRole"/>
                <col width="260" style="display:none" id="noteBodyRight"/>
              </xsl:otherwise>
            </xsl:choose>
            <col width="0"   style="display:none"/>
            <tbody dataRows="2" bgColor1="fff7e5" bgColor2="ffffff">
              <xsl:if test="contains($ShopCRUD,'R')">
                <xsl:apply-templates select="Note">
                  <xsl:sort select="@CreatedDate" order="descending"/>
                </xsl:apply-templates>
              </xsl:if>
            </tbody>
          </table>
        </div>
      </td>
    </tr>
  </table>
</BODY>
</HTML>
</xsl:template>

<!-- Each note gets its own table row -->
<xsl:template match="Note">

  <xsl:if test="boolean(string(@CreatedUserID))">

    <!-- Displayed table row -->
    <tr onClick="editNote(this)"
        onMouseOut="noteGridMouseOut(this)"
        onMouseOver="noteGridMouseOver(this)">

      <xsl:attribute name="idx">
        <xsl:value-of select="position()"/>
      </xsl:attribute>
      <xsl:attribute name="bgColor">
        <xsl:value-of select="user:chooseBackgroundColor(position(), 'fff7e5', 'ffffff' ) "/>
      </xsl:attribute>

      <!-- Supervisors get special text highlighting -->
      <xsl:choose>
        <xsl:when test="@CreatedUserSupervisorFlag='0'">
          <xsl:attribute name="CreatedUserSupervisorFlag">0</xsl:attribute>
          <xsl:attribute name="supervisor">false</xsl:attribute>
          <xsl:attribute name="style">color:#000000; font-weight:normal</xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="CreatedUserSupervisorFlag">1</xsl:attribute>
          <xsl:attribute name="supervisor">true</xsl:attribute>
          <xsl:attribute name="style">color:#0000FF; font-weight:bold</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <td id="CreatedDate" class="clWinGridTypeTD" align="center">
        <xsl:value-of select="user:UTCConvertDateAndTimeByNodeType(.,'CreatedDate','a')"/>
      </td>
      <td id="CreatedUser" class="clWinGridTypeTD" align="center">
        <xsl:value-of select="user:FormatPersonName(@CreatedUserNameFirst,@CreatedUserNameLast)"/>
      </td>
      <td id="CreatedUserRoleName" class="clWinGridTypeTD" align="center">
        <xsl:value-of select="@CreatedUserRoleName"/>
      </td>
      <td id="Note" class="clWinGridTypeTD" align="left" width="260">
        <xsl:call-template name="break"><xsl:with-param name="text" select="@Note"/></xsl:call-template>
      </td>
      <td id="DocumentID" class="clWinGridTypeTD" align="left" style="display:none"><xsl:value-of select="@DocumentID"/></td>
    </tr>

    <!-- Hidden note body popup table row -->
    <tr bgcolor="FFEEBB">
      <xsl:attribute name="id">noteBodyBottom<xsl:value-of select="position()"/></xsl:attribute>
      <xsl:attribute name="idx"><xsl:value-of select="position()"/></xsl:attribute>

      <!-- Supervisors get special text highlighting -->
      <xsl:choose>
        <xsl:when test="@CreatedUserSupervisorFlag='0'">
          <xsl:attribute name="style">color:#000000; font-weight:normal; display:none</xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="style">color:#3333ff; font-weight:bold; display:none</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <td class="clWinGridTypeTD" align="left" colspan="2">
        <xsl:call-template name="break"><xsl:with-param name="text" select="@Note"/></xsl:call-template>
      </td>
    </tr>
  </xsl:if>
</xsl:template>

<xsl:template name="break">  <!-- replaces 'carriage returns' in the 'text' parameter with hard-coded '<br/>'s -->
 <xsl:param name="text"/>
 <xsl:choose>
   <xsl:when test="contains($text, '&#xA;')">
     <xsl:value-of select="substring-before($text, '&#xA;')"/>
     <br/>
     <xsl:call-template name="break"><xsl:with-param name="text" select="substring-after($text,'&#xA;')"/></xsl:call-template>
   </xsl:when>
   <xsl:otherwise>
     <xsl:value-of select="$text"/>
   </xsl:otherwise>
 </xsl:choose>
</xsl:template>


</xsl:stylesheet>
