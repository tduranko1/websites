<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="PMDPhotoviewer">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="PMDDateSearch.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">

<xsl:param name="ImageRootDir"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP                    XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDPhotosGetListXML,PMDPhotoViewer.xsl,6790,1   -->

<HEAD>
  <TITLE>Photo Viewer</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>

  <style>
  	Span{
		color:blue
	}
	Span:hover{
		color : #B56D00;
		font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
		font-weight : bold;
		font-size : 10px;
		cursor : hand;
	}	
  </style>
  
<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
 
var gsImageRootDir = '<xsl:value-of select="$ImageRootDir"/>';
  
<![CDATA[

function ProcessLink(obj){
	var imgLoc = gsImageRootDir + obj.getAttribute("ImageLocation");
	if (imgLoc){
		ifrmContent.frameElement.src = imgLoc;	
	}			
	obj.style.color = "orange";	
}

function MouseOver(obj){
	if (obj.style.color != "orange")
		obj.style.color = "darkblue";
	obj.style.fontWeight = "bold";
	obj.style.cursor = "hand";
}

function MouseOut(obj){
	if (obj.style.color != "orange")
		obj.style.color = "blue";
	obj.style.fontWeight = "normal";
	obj.style.cursor = "default";
}


]]>

</SCRIPT>

</HEAD>

<BODY class="bodyAPDSub" unselectable="on" onload="initPage();" bgcolor="#FFFAEB">

<DIV unselectable="on" style="position:absolute; left:15; top:30;">
  <TABLE unselectable="on" width="100px" border="0" cellspacing="2" cellpadding="2" style="table-layout:fixed">
    <xsl:apply-templates select="Photo"/>
  </TABLE>
</DIV>

<DIV unselectable="on" id="content11" class="content1" style="background-color:#FFFAEB; position:absolute; width:730px; height:710px; left:150px; top:10px;">
  <IFRAME id="ifrmContent" src="blank.asp" style="width:720px; height:700px; border:0px; padding:0px; position:absolute; background:transparent; overflow:hidden" allowtransparency="true" ></IFRAME>
</DIV>

</BODY>
</HTML>	
	
</xsl:template>

<xsl:template match="Photo">
  <tr unselectable="on">
    <td width="100px" height="18px">
	  <span onclick="ProcessLink(this)" style="color:blue" onmouseover="MouseOver(this)" onmouseout="MouseOut(this)">
	    <xsl:attribute name="ImageLocation"><xsl:value-of select="@ImageLocation"/></xsl:attribute>
	    Photo<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="position()"/>
	  </span>
    </td>
  </tr>
</xsl:template>

</xsl:stylesheet>

