<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTBusinessInfo">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<xsl:param name="mode"/>

<xsl:template match="/Root">
<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTDupFedTaxIDGetListXML,SMTFedTaxID.xsl, '593514252', 3152   -->

<HEAD>
  <TITLE>Shop Maintenance</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  <style>
    .link{
      color:blue
    }
    .linkhover{
      color : #B56D00;
	    font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
	    font-weight : bold;
	    font-size : 10px;
	    cursor : hand;
    }
  </style>

<!-- CLIENT SCRIPTS -->

<!-- Page Specific Scripting -->
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<SCRIPT language="javascript">


<![CDATA[

  function GoLink(id){
    window.returnValue = id;
    window.close();
  }
  
  function IdMouseOver(){
    event.srcElement.className = "linkhover";
  }
  
  function IdMouseOut(){
    event.srcElement.className = "link";
  }
]]>
  
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" style="overflow:hidden; margin-left:0; margin-right:0; margin-top:0;">
  <DIV id="CNScrollTable" style="width:100%">
    
    <IMG src="/images/spacer.gif" width="6" height="9" border="0" />
    
    <TABLE width="100%">
      <TR unselectable="on" >
        <TD align="center">
          <b>
          The following Business(s) already exist with the EIN you are attempting to save.
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          EINs must be unique<br/>within the system.  Please click ok and change the EIN. 
          </b>  
        </TD>
      </TR>
      <tr>
        <td><IMG src="/images/spacer.gif" width="6" height="6" border="0" /></td>
      </tr>
      <tr>
        <td align="center">
          <b>Alternatively,
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          you may click the ID and link directly to the existing Business.
          <xsl:if test="$mode='wizard'">
            <br/>From there you may choose 'Add Shop' from the 'Admin' menu to add a Shop to the existing Business if you wish.
          </xsl:if>
          </b>
        </td>
      </tr>
    </TABLE>
    
    <IMG src="/images/spacer.gif" width="6" height="9" border="0" />
    
	  <TABLE unselectable="on" class="ClaimMiscInputs" width="100%" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
      <TBODY>
        <TR unselectable="on" class="QueueHeader">
          <TD unselectable="on" class="TableSortHeader" sIndex="99" width="60" style="cursor:default"> ID </TD>
          <TD unselectable="on" class="TableSortHeader" sIndex="99" width="100%" style="cursor:default"> Business Name </TD>
          <TD unselectable="on" class="TableSortHeader" sIndex="99" width="200" style="cursor:default"> Business Address </TD>
          <TD unselectable="on" class="TableSortHeader" sIndex="99" width="150" style="cursor:default"> City, State </TD>
			  </TR>
      </TBODY>
		</TABLE>

	  <DIV unselectable="on" class="autoflowTable" style="width:100%; height:130px;">
      <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" width="100%" border="0" cellspacing="1" cellpadding="0" style="table-layout:fixed">
        <TBODY bgColor1="ffffff" bgColor2="fff7e5">
          <xsl:apply-templates select="Business"/>
        </TBODY>
      </TABLE>
    </DIV>
  </DIV>
  <div style="position:absolute; left=350;">
    <input type="button" id="btnOk" value="OK" class="formButton" style="width:80;" onclick="window.returnValue=0; window.close();"/>
  </div>
</BODY>
</HTML>
</xsl:template>


<xsl:template match="Business">
  <TR unselectable="on">
    <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
    <TD unselectable="on" width="58" class="GridTypeTD">
      <span class="link" onMouseOut="IdMouseOut(this)" onMouseOver="IdMouseOver(this)" onclick="GoLink(this.innerText)">
        <xsl:value-of select="@BusinessInfoID"/>
      </span>
    </TD>
    <TD unselectable="on" width="100%" class="GridTypeTD" style="text-align:left;"><xsl:value-of select="@Name"/></TD>
    <TD unselectable="on" width="200" class="GridTypeTD" style="text-align:left;"><xsl:value-of select="@Address1"/></TD>
    <TD unselectable="on" width="150" class="GridTypeTD" style="text-align:left;">
      <xsl:value-of select="@AddressCity"/>
      <xsl:if test="@AddressCity != '' and @AddressState != ''"><xsl:text>, </xsl:text></xsl:if>
      <xsl:value-of select="@AddressState"/>
    </TD>
  </TR>
</xsl:template>

</xsl:stylesheet>

