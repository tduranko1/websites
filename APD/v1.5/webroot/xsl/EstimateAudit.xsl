<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="EstimateAudit">

<xsl:import href="msxsl/msxsl-function-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="Caller"/>

<xsl:template match="/Root">

<HTML>
<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.0.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspEstimateAuditGetDetailXML, EstimateAudit.xsl, 777   -->

<HEAD>
  <TITLE>
    Estimate Audit - <xsl:value-of select="concat(Header/@LynxID, '-', Header/@VehicleNum)"/> - 
    <xsl:choose>
      <xsl:when test="Header/@EstimateType = 'Supplement'">
        <xsl:value-of select="concat(Header/@EstimateType, ' ', Header/@SupplementSequenceNumber)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="Header/@EstimateType"/>
      </xsl:otherwise>
    </xsl:choose>
    
  </TITLE>
  <!-- STYLES -->
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  
  <SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
  <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
  </script>

  <script language="javascript">
   
  var gsDocumentID = '<xsl:value-of select="@DocumentID"/>';
  var gsShopID = '<xsl:value-of select="Header/@ShopID"/>';
  
  <![CDATA[
  
    function doPrint(){
    	  var oObj = new Object;
        oObj.strTitle = document.title;
        oObj.strReport = rptText.innerHTML;
    	  window.showModalDialog('/Reports/printReport.asp', oObj, 'dialogHeight:0px;dialogWidth:0px;dialogTop:0px;dialogLeft:0px;center:no;status:no;resizable:yes;dialogHide:yes');
    }
  
  ]]>
  </script>
  
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" style="background:transparent; border:0px; overflow:hidden;" tabIndex="-1">
  <div >
    <xsl:attribute name="style">
      z-index:99;position:absolute; top:2; left:
      <xsl:choose>
        <xsl:when test="$Caller='PMD'">800</xsl:when>
        <xsl:otherwise>620</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    
    <xsl:if test="@DocumentID &gt; 0">
      <input type="button" id="btnPrint" value="Print" class="formButton" onclick="doPrint()" style="width:80;"/>
    </xsl:if>
  </div>
  
  <div style="position:absolute; left:5; top:2;"><xsl:apply-templates select="Header"/></div>
  
  <DIV id="CNScrollTable" style="position:absolute; top:83;"> 
  <xsl:choose>
    <xsl:when test="count(Discrepancy) &gt; 0">
  	  <TABLE id="tblHead" unselectable="on" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSort1')" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
        <TBODY>
          <TR unselectable="on" class="QueueHeader">
  			    <td unselectable="on" class="TableSortHeader" sIndex="99" style="cursor:default;">
              <xsl:attribute name="width">
                <xsl:choose>
                  <xsl:when test="$Caller='PMD'">200</xsl:when>
                  <xsl:otherwise>175</xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
              Audit Rule
            </td>
            <td unselectable="on" class="TableSortHeader" width="60" sIndex="99" style="cursor:default;">Estimate Detail Line</td>
            <td unselectable="on" class="TableSortHeader" width="100%" sIndex="99" style="cursor:default;">Description</td>
            <td unselectable="on" class="TableSortHeader" sIndex="99" width="0" rowspan="2" style="cursor:default"></td> <!-- scroll bar spacer -->
          </TR>
  		  </TBODY>
      </TABLE>
    
      <DIV unselectable="on" class="autoflowTable">
        <xsl:attribute name="style">
          width:100%;height:<xsl:choose>
                              <xsl:when test="$Caller='PMD'">267</xsl:when>
                              <xsl:otherwise>325</xsl:otherwise>
                            </xsl:choose>px;          
        </xsl:attribute>
        <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" border="0" cellspacing="1" cellpadding="3" style="table-layout:fixed">
          <TBODY bgColor1="ffffff" bgColor2="fff7e5">
            <colgroup>
              <col unselectable="on" class="GridTypeTD" align="left">
                <xsl:attribute name="width">
                  <xsl:choose>
                    <xsl:when test="$Caller='PMD'">197</xsl:when>
                    <xsl:otherwise>172</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
              </col>
              <col unselectable="on" width="59" style="text-align:right;"/>
              <col unselectable="on" width="100%" style="text-align:left;"/>
              <col unselectable="on" width="0"/>
            </colgroup>
            <xsl:apply-templates select="Discrepancy"><xsl:with-param name="Caller" select="$Caller"/></xsl:apply-templates>
          </TBODY>
        </TABLE>
      </DIV>	
    </xsl:when>
    <xsl:when test="@EstimateAuditID = 0"><strong>This estimate was not audited.</strong></xsl:when>
    <xsl:otherwise><strong>This estimate passed all audits.  There are no discrepancies.</strong></xsl:otherwise>
  </xsl:choose>
	</DIV> 
    
    <br/>
  <div id="rptText" style="position:absolute; top:500px;display:none;">
	
	<IMG src="/images/spacer.gif" width="1" height="4" border="0"/>
	
	<xsl:for-each select="Header">
    <xsl:call-template name="rptHeader"/>
  </xsl:for-each>
	
  <IMG src="/images/spacer.gif" width="1" height="30" border="0" />
  
  <TABLE unselectable="on" border="0" cellspacing="1" cellpadding="3" style="table-layout:fixed;border-collapse:collapse">
    <colgroup>
      <col unselectable="on" width="193"/>
      <col unselectable="on" width="55"/>
      <col unselectable="on" width="100%"/>
    </colgroup>
    
    <TR unselectable="on">
	    <td unselectable="on" style="text-align:center;background-color:silver;font-weight:bold;">Audit Rule</td>
      <td unselectable="on" style="c;text-align:center;background-color:silver;font-weight:bold;">Estimate Detail Line</td>
      <td unselectable="on" style="text-align:center;background-color:silver;font-weight:bold;">Description</td>
    </TR>
  	<xsl:for-each select="Discrepancy">
      <xsl:call-template name="rptDiscrepancy"/>
    </xsl:for-each>
 </TABLE>
   
	
  <IMG src="/images/spacer.gif" width="1" height="30" border="0" />
	
	<table width="670" style="table-layout:fixed;">
	  <tr>
	    <td nowrap="nowrap" width="120px">Program Manager:</td>
  		<td width="230px"></td>
  		<td width="60px"></td>
  		<td width="50px">Date:</td>
  		<td width="200px"></td>
	  </tr>
	  <tr>
	    <td></td>
  		<td><hr noshade="" size="1"/></td>
  		<td></td>
  		<td></td>
  		<td><hr noshade="" size="1"/></td>
	  </tr>
	  <tr>
	    <td nowrap="nowrap">Shop Representative:</td>
		  <td></td>
	  </tr>
	  <tr>
	    <td></td>
  		<td><hr noshade="" size="1"/></td>
  		<td></td>
  		<td></td>
  		<td></td>
	  </tr>
	</table> 
 </div> <!-- rptText -->
</BODY>
</HTML>
</xsl:template>

<xsl:template match="Header">
  <TABLE>
  <TR>
  <TD valign="top">
    <table border="0">
      <colgroup>
        <col width="130"/>
        <col width="80"/>
      </colgroup>
      <tr>
        <td unselectable="on"><strong>Assignment Date:</strong></td>
        <td unselectable="on"><xsl:value-of select="@AssignmentDate"/></td>
      </tr>
      <tr>
        <td unselectable="on"><strong>Estimate Received:</strong></td>
        <td unselectable="on"><xsl:value-of select="@EstimateReceivedDate"/></td>
      </tr>
      <tr>
        <td unselectable="on"><strong>Estimate Type:</strong></td>
        <td unselectable="on"><xsl:value-of select="@EstimateType"/></td>
      </tr>
      <tr>
        <td unselectable="on"><strong>Suppl. Seq.</strong></td>
        <td unselectable="on">
          <xsl:choose>
            <xsl:when test="@SupplementSequenceNumber = '0'"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:when>
            <xsl:otherwise><xsl:value-of select="@SupplementSequenceNumber"/></xsl:otherwise>
          </xsl:choose>
       </td>
     </tr>
   </table>
   </TD>
   <TD width="10"></TD> <!-- spacer column -->
   <TD valign="top">
    <table border="0">
      <colgroup>
        <col width="100"/>
        <col width="250"/>
      </colgroup>
      <tr>
        <td unselectable="on"><strong>Vehicle:</strong></td>
        <td unselectable="on"><xsl:value-of select="@VehicleYMM"/></td>
      </tr>
      <tr>
        <td unselectable="on"><strong>Shop:</strong></td>
        <td unselectable="on"><xsl:value-of select="@ShopName"/></td>
      </tr>
      <tr>
        <td unselectable="on"><strong>Shop Contact:</strong></td>
        <td unselectable="on"><xsl:value-of select="@ShopContact"/></td>
      </tr>
      <tr>
        <td unselectable="on"><strong>Shop Phone:</strong></td>
        <td unselectable="on"><xsl:value-of select="@ShopPhone"/></td>
      </tr>
    </table>
    </TD>
    <TD valign="bottom">
    <table border="0">
      <colgroup>
        <col width="130"/>
        <col width="25"/>
      </colgroup>
      <tr>
        <!-- <td unselectable="on" style="font-size:18px"><strong>Audit Score:</strong></td>
        <td unselectable="on" style="font-size:18px"><xsl:value-of select="/Root/@AuditWeight"/></td> -->
      </tr>
    </table>
    </TD>
    </TR>
    </TABLE>
</xsl:template>

<xsl:template match="Discrepancy">
  <xsl:param name="Caller"/>
  <xsl:variable name="EstimateAuditRuleID" select="@EstimateAuditRuleID"/>
  
  <tr unselectable="on" bgcolor="white" valign="top">
    <!--<xsl:attribute name="height">
      <xsl:choose>
        <xsl:when test="$Caller='PMD'">55</xsl:when>
        <xsl:otherwise>85</xsl:otherwise>
      </xsl:choose>px
    </xsl:attribute>-->
    
    <td class="GridTypeTD" style="text-align:left;"><xsl:value-of select="@AuditRuleName"/></td>
    <td class="GridTypeTD">
      <xsl:choose>
        <xsl:when test="@EstimateDetailNumber != '0'"><xsl:value-of select="@EstimateDetailNumber"/></xsl:when>
        <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
      </xsl:choose>
    </td>
    <td class="GridTypeTD" style="text-align:left;">
      <xsl:choose>
        <xsl:when test="@AuditDescription != ''"><xsl:value-of select="@AuditDescription"/></xsl:when>
        <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
      </xsl:choose>
     </td>
    <td class="GridTypeTD"></td>
  </tr>
</xsl:template>

<xsl:template name="rptHeader">
  <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-bottom:4px double #000000;margin-bottom:8px;">
    <tr>
      <td><div style="font:bolder 18pt Tahoma;">Estimate Audit Report</div></td>
      <td><div style="font:bolder 18pt Tahoma;" align="right">LYNX Services</div></td>
    </tr>
  </table>
	
	<IMG src="/images/spacer.gif" width="1" height="10" border="0"/>
	
	<TABLE cellspacing="0" cellpadding="0" border="0">
    <colgroup>
      <col width="190"/>
      <col width="275"/>
      <col width="100%"/>
    </colgroup>
    <TR>
      <TD colspan="3" style="text-align:right">
        <table border="0">
          <colgroup>
            <col width="125"/>
            <col width="50"/>
          </colgroup>
          <tr>
            <!-- <td unselectable="on" style="font-size:18px"><strong>Audit Score:</strong></td>
            <td unselectable="on" style="font-size:18px"><xsl:value-of select="/Root/@AuditWeight"/></td> -->
          </tr>
        </table>      
        <br/>
      </TD>
    </TR>
	  <TR>
      <TD valign="top">
        <table cellspacing="0" cellpadding="2" border="0">
          <colgroup>
            <col width="115"/>
            <col width="75"/>
          </colgroup>
          <tr>
      		  <td nowrap="nowrap"><strong>Assignment Date:</strong></td>
            <td><xsl:value-of select="@AssignmentDate"/></td>
          </tr>
          <tr>
            <td nowrap="nowrap"><strong>Estimate Received:</strong></td>
            <td><xsl:value-of select="@EstimateReceivedDate"/></td>
          </tr>
          <tr>
            <td nowrap="nowrap"><strong>Estimate Type:</strong></td>
            <td><xsl:value-of select="@EstimateType"/></td>
          </tr>
          <tr>
            <td nowrap="nowrap"><strong>Suppl. Seq.:</strong></td>
            <td>
              <xsl:choose>
                <xsl:when test="@SupplementSequenceNumber = '0'"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:when>
                <xsl:otherwise><xsl:value-of select="@SupplementSequenceNumber"/></xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </table>
      </TD>
      <TD valign="top">
        <table cellspacing="0" cellpadding="2" border="0">
          <colgroup>
            <col width="90"/>
            <col width="185"/>
          </colgroup>
          <tr>
            <td nowrap="nowrap"><strong>Shop:</strong></td>
            <td><xsl:value-of select="@ShopName"/></td>
          </tr>
          <tr>
            <td nowrap="nowrap"><strong>Shop Contact:</strong></td>
            <td><xsl:value-of select="@ShopContact"/></td>
          </tr>
          <tr>
            <td nowrap="nowrap"><strong>Shop Phone:</strong></td>
            <td><xsl:value-of select="@ShopPhone"/></td>
          </tr>
          <tr>
      		  <td nowrap="nowrap"><strong>Vehicle:</strong></td>
            <td><xsl:value-of select="normalize-space(@VehicleYMM)"/></td>
          </tr>
        </table>
      </TD>
      <TD valign="top">
        <table cellspacing="0" cellpadding="2" border="0">
          <colgroup>
            <col width="80"/>
            <col width="125"/>
            <tr>
              <td><strong>Ins. Co.:</strong></td>
              <td><xsl:value-of select="@InsuranceCompanyName"/></td>
            </tr>
            <tr>
              <td><strong>Claim No:</strong></td>
              <td><xsl:value-of select="@ClaimNumber"/></td>
            </tr>
            <tr>
              <td><strong>Claim Rep.:</strong></td>
              <td><xsl:value-of select="@ClaimRepName"/></td>
            </tr>
            <tr>
              <td><strong>Insured:</strong></td>
              <td><xsl:value-of select="@InsuredName"/></td>
            </tr>
            <tr>
              <td><strong>LynxID:</strong></td>
              <td><xsl:value-of select="@LynxID"/></td>
            </tr>
          </colgroup>
        </table>     
      </TD>
    </TR>
  </TABLE>
</xsl:template>


<xsl:template name="rptDiscrepancy">
  <xsl:variable name="EstimateAuditRuleID" select="@EstimateAuditRuleID"/>
  
  <tr unselectable="on" bgcolor="white" valign="top" height1="80">
    <td class="GridTypeTD" style="text-align:left;"><xsl:value-of select="@AuditRuleName"/></td>
    <td class="GridTypeTD">
      <xsl:choose>
        <xsl:when test="@EstimateDetailNumber != '0'"><xsl:value-of select="@EstimateDetailNumber"/></xsl:when>
        <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
      </xsl:choose>
    </td>
    <td class="GridTypeTD" style="text-align:left;">
      <xsl:choose>
        <xsl:when test="@AuditDescription != ''"><xsl:value-of select="@AuditDescription"/></xsl:when>
        <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
      </xsl:choose>
     </td>
    <td class="GridTypeTD"></td>
  </tr>
</xsl:template>  
</xsl:stylesheet>
