<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    id="ClaimSearchResults">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="TransferClaimCRUD" select="Transfer Claim Ownership"/>

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function myNameConcat(firstPart, secondPart, delim){
        var sRet = "";
        if (delim == "") delim = ", ";
        if (firstPart == "" && secondPart == "")
            return " ";
        else if (firstPart != "" && secondPart != "") {
            sRet = firstPart + delim + secondPart;
        }
        else {
            sRet = firstPart + secondPart;
        }
        
        return sRet;
    }
    
    function inStr(strSearch, strNameFirst, strNameLast, strNameBusiness) {
      var iNameFirst = iNameLast = iNameBusiness = -1;
      if (strSearch) strSearch = strSearch.toLowerCase();
      if (strNameFirst) strNameFirst = strNameFirst.toLowerCase();
      if (strNameLast) strNameLast = strNameLast.toLowerCase();
      if (strNameBusiness) strNameBusiness = strNameBusiness.toLowerCase();
      
      if (strSearch && strNameFirst) iNameFirst = strNameFirst.indexOf(strSearch);
      if (strSearch && strNameLast) iNameLast = strNameLast.indexOf(strSearch);
      if (strSearch && strNameBusiness) iNameBusiness = strNameBusiness.indexOf(strSearch);

      return ((iNameFirst != -1) || (iNameLast != -1) || (iNameBusiness != -1));
    }

  ]]>
</msxsl:script>

<xsl:template match="/Root">

<HTML>
<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspClaimSearchXML,ClaimSearchResults.xsl,'','','','','','',145   -->
<HEAD>

<TITLE>Claim Search</TITLE>
<!-- STYLES -->
<LINK rel="stylesheet" href="css/apdcontrols.css" type="text/css"/>


<!-- CLIENT SCRIPTS -->
<SCRIPT language="javascript" src="/js/apdcontrols.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/claimnavigate.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 	
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,27);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
var sTransferClaimCRUD = "<xsl:value-of select="$TransferClaimCRUD"/>";
<![CDATA[


//init the table selections, must be last
function __pageInit()
{
  if (typeof(parent.hideMessage) == "function")
    parent.hideMessage();
}

function showSelectedRow(){
  var oSelRow = event.object;
  if (oSelRow){
    //var iLynxID = oSelRow.objXML.getAttribute("LynxID");
    var oRowXML = oSelRow.objXML;
    if (oRowXML){
      var iLynxID = oRowXML.selectSingleNode("LynxID").text;
  
      if(iLynxID > 0){
        var oRawXML = xmlRawData.selectSingleNode("/Root/Claim[LynxId='" + iLynxID + "']");
        if (oRawXML){
          //clear the vehicle and property drop downs
          selVehicles.Clear();
          selProperties.Clear();
          spnVehCount.innerText = "(0)";
          spnPrpCount.innerText = "(0)";
          try {
          divClaimStatusText.innerText = "";
          } catch (e) {}
          divClaimVoided.style.display = "none";
          
          //now populate the claim data in the preview
          txtLynxID.value = oRawXML.selectSingleNode("LynxId").text;
          txtClaimNumber.value = oRawXML.selectSingleNode("ClaimNumber").text;
          txtInsCo.value = oRawXML.selectSingleNode("InsuranceCompany").text;
          
          if (sTransferClaimCRUD.indexOf("U") != -1)
            btnReassign.CCDisabled = false;
          
          //load the vehicles
          var oVehicles = oRawXML.selectNodes("Vehicle");
          if (oVehicles && oVehicles.length > 0){
            spnVehCount.innerText = "(" + oVehicles.length + ")";
            for (var i = 0; i < oVehicles.length; i++){
              var sVehText = "";
              var sVehYrMkModel = "";
              var sVehStatImg = "";
              //var sClaimAspectID = oVehicles[i].getAttribute("ClaimAspectID");
              
              if (oVehicles[i].selectSingleNode("ClosedStatus").text == "1") {
                sVehStatImg = "/images/closedExposure.gif";
              }
              
              if (oVehicles[i].selectSingleNode("OwnerBusinessName").text != ""){
                sVehText = oVehicles[i].selectSingleNode("OwnerBusinessName").text;
              } else if (oVehicles[i].selectSingleNode("OwnerNameLast").text != "") {
                sVehText = oVehicles[i].selectSingleNode("OwnerNameLast").text + ", " + oVehicles[i].selectSingleNode("OwnerNameFirst").text;
              } else {
                sVehText = "Veh " + oVehicles[i].selectSingleNode("VehicleNumber").text;
              }
              
              if (parseInt(oVehicles[i].selectSingleNode("VehicleYear").text, 10) > 0)
                sVehYrMkModel += oVehicles[i].selectSingleNode("VehicleYear").text + " ";
                
              if (oVehicles[i].selectSingleNode("Make").text != "")
                sVehYrMkModel += oVehicles[i].selectSingleNode("Make").text + " ";
              
              if (oVehicles[i].selectSingleNode("Model").text != "")
                sVehYrMkModel += oVehicles[i].selectSingleNode("Model").text;
                
              if (sVehYrMkModel != "")
                sVehText += " (" + sVehYrMkModel + ")";
                
              selVehicles.AddItem(oVehicles[i].selectSingleNode("VehicleNumber").text, sVehText, sVehStatImg);
            }
            /*if (oVehicles.length == oRawXML.selectNodes("Vehicle[@Status='Cancelled']").length){
              divClaimStatusText.innerText = "CLAIM CANCELLED";
              divClaimVoided.style.display = "inline";
            }*/
            if (oVehicles.length == oRawXML.selectNodes("Vehicle[@Status='Voided']").length){
              divClaimStatusText.innerText = "CLAIM VOIDED";
              divClaimVoided.style.display = "inline";
            }
          }
  
  
          /*var oProperties = oRawXML.selectNodes("Property");
          if (oProperties && oProperties.length > 0){
            spnPrpCount.innerText = "(" + oProperties.length + ")";
            for (var i = 0; i < oProperties.length; i++){
              var sPrpText = "";
              var sPrpStatImg = "";
              var sClaimAspectID = oVehicles[i].getAttribute("ClaimAspectID");
              
              if (oProperties[i].getAttribute("ClosedStatus") == "1") {
                sPrpStatImg = "/images/closedExposure.gif";
              }
              
              if (oProperties[i].getAttribute("OwnerBusinessName") != ""){
                sPrpText = oProperties[i].getAttribute("OwnerBusinessName");
              } else if (oProperties[i].getAttribute("OwnerNameLast") != "") {
                sPrpText = oProperties[i].getAttribute("OwnerNameLast") + ", " + oProperties[i].getAttribute("OwnerNameFirst");
              } else {
                sPrpText = "Veh " + oProperties[i].getAttribute("PropertyNumber");
              }
              selProperties.AddItem(oProperties[i].getAttribute("PropertyNumber"), sPrpText, sPrpStatImg);
            }
          }*/
        }
      }
    }
  }
}

function openVehicle(){
  top.sClaimView = "cond";
  var sURL = NavOpenWindowUrl("vehicle", txtLynxID.value, selVehicles.value); //NavOpenWindowUrl( "vehicle", txtLynxID.value, selVehicles.value);
  //alert(sURL);
  top.closeMe( sURL );
}

function openProperty(){
  var sURL = NavOpenWindowUrl( "property", txtLynxID.value, selProperties.value);
  top.closeMe( sURL );
}


function navigateTo(strEntity){
  var oSelRow = Grid1.selectedRow;
  if(oSelRow && oSelRow.objXML){
    //var sLynxID = oSelRow.objXML.getAttribute("LynxID");
    var sLynxID = oSelRow.objXML.selectSingleNode("LynxID").text;

    var iVehCount = iPrpCount = 0;
    var oRawXML = xmlRawData.selectSingleNode("/Root/Claim[@LynxId='" + sLynxID + "']");
    if (oRawXML){
      var oVehicles = oRawXML.selectNodes("Vehicle");
      if (oVehicles && oVehicles.length > 0){
        iVehCount = oVehicles.length
      }
      var oProperties = oRawXML.selectNodes("Property");
      if (oProperties && oProperties.length > 0){
        iPrpCount = oProperties.length;
      }
    }
    switch(strEntity)
    {
      case "claim":
        top.closeMe( NavOpenWindowUrl( "claim", sLynxID, "1" ) );
        break;
      case "vehicle":
        if (sLynxID != "" && iVehCount > 0)
          top.closeMe( NavOpenWindowUrl( 'vehicle', sLynxID, "1" ) );
        break;
      case "property":
        if (sLynxID != "" && iPrpCount > 0)
          top.closeMe( NavOpenWindowUrl( 'property', sLynxID, "1" ) );
        break;
    }
    window.event.cancelBubble = true;
  }
}

function reassignClaim(){
  var oSelRow = Grid1.selectedRow;
  if(oSelRow){
    var oRowXML = oSelRow.objXML;
    if (oRowXML){
      var sLynxID = oRowXML.selectSingleNode("LynxID").text;
    	var strReturn = ""
      var strClaimAspectIDClaim = oRowXML.selectSingleNode("ClaimAspectIdClaim").text;
      //alert(strClaimAspectIDClaim);
    	strReturn = window.showModalDialog("/ClaimReAssign.asp?LynxID="+sLynxID+"&ClaimAspectID=" + strClaimAspectIDClaim + "&Entity=Claim",strReturn,"dialogHeight: 200px; dialogWidth: 300px; center: Yes; help: No; resizable: No; status: No; unadorned: Yes");
    }
  }
}

]]>
</SCRIPT>
</HEAD>

<BODY LEFTMARGIN="0" TOPMARGIN="0" RIGHTMARGIN="0" BOTTOMMARGIN="0" CLASS="bodyAPDSub" STYLE="background:transparent;border:0px" onLoad1="initPage();" unselectable="on">
  <xml id="xmlSearchResult" name="xmlSearchResult">
    <Root>
      <xsl:for-each select="/Root/Claim">
        <xsl:sort select="@LynxId" data-type="number"/>
        <xsl:variable name="ClaimStatus">
          <xsl:choose>
            <xsl:when test="count(Vehicle[@ClosedStatus='1']) = count(Vehicle)">Closed</xsl:when>
            <xsl:otherwise>Open</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:variable name="VehicleCount"><xsl:value-of select="count(Vehicle)"/></xsl:variable>
        <xsl:variable name="VehiclesOpen"><xsl:value-of select="count(Vehicle[@ClosedStatus=0])"/></xsl:variable>
        <xsl:variable name="PropertyCount"><xsl:value-of select="count(Property)"/></xsl:variable>
        <xsl:variable name="PropertiesOpen"><xsl:value-of select="count(Property[@ClosedStatus=0])"/></xsl:variable>
        <xsl:variable name="InvolvedType">
          <xsl:choose>
            <xsl:when test="string-length(/Root/@InvolvedNameLast) &gt; 0">
              <xsl:choose>
                <xsl:when test="user:inStr(string(/Root/@InvolvedNameLast), string(@InsuredFirstName), string(@InsuredLastName), string(@InsuredBusinessName)) = true()">Insured</xsl:when>
                <xsl:otherwise>Owner</xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>Insured</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:variable name="InvolvedName">
          <xsl:choose>
            <xsl:when test="$InvolvedType='Owner'">
              <xsl:for-each select="Vehicle">
                <xsl:if test="user:inStr(string(/Root/@InvolvedNameLast), string(@OwnerNameFirst), string(@OwnerNameLast), string(@OwnerBusinessName)) = true()">
          			  <xsl:choose>
                    <xsl:when test="string-length(@OwnerBusinessName) > 0" >
                			<xsl:value-of select="@OwnerBusinessName"/>
                    </xsl:when>
                    <xsl:otherwise>
          				    <xsl:value-of select="user:myNameConcat(string(@OwnerNameLast), string(@OwnerNameFirst), ', ')"/>
                    </xsl:otherwise>
                  </xsl:choose><xsl:if test="position() != last()">;</xsl:if>
                </xsl:if>
              </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
      			  <xsl:choose>
                <xsl:when test="string-length(@InsuredBusinessName) > 0" >
            			<xsl:value-of select="@InsuredBusinessName"/>
                </xsl:when>
                <xsl:otherwise>
      				    <xsl:value-of select="user:myNameConcat(string(@InsuredLastName), string(@InsuredFirstName), ', ')"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>


        <Result>
          <xsl:element name="LynxID"><xsl:value-of select="@LynxId"/></xsl:element>
          <xsl:element name="ClaimAspectIdClaim"><xsl:value-of select="@ClaimAspectIdClaim"/></xsl:element>
          <xsl:element name="Status"><xsl:value-of select="@ClosedStatus"/><xsl:value-of select="sum(Vehicle/@ClosedStatus)"/><xsl:value-of select="sum(Property/@ClosedStatus)"/></xsl:element>
          <xsl:element name="ClaimStatus">
            <xsl:choose>
              <xsl:when test="$ClaimStatus='Closed'">/images/icon_claim_closed.gif</xsl:when>
              <xsl:otherwise>/images/icon_claim_open.gif</xsl:otherwise>
            </xsl:choose>
          </xsl:element>
          <xsl:element name="VehicleStatus">
            <xsl:choose>
                <xsl:when test="$VehicleCount &gt; 1">
                    <xsl:choose>
                        <xsl:when test="$VehiclesOpen = $VehicleCount">/images/icon_vehicle_multi_open.gif</xsl:when>
                        <xsl:when test="$VehiclesOpen = 0">/images/icon_vehicle_multi_closed.gif</xsl:when>
                        <xsl:when test="$VehicleCount &gt; 1 and $VehiclesOpen &lt; $VehicleCount">/images/icon_vehicle_multi_open_closed.gif</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$VehicleCount = 1">
                    <xsl:choose>
                        <xsl:when test="$VehiclesOpen = $VehicleCount">/images/icon_vehicle_open.gif</xsl:when>
                        <xsl:when test="$VehiclesOpen = 0">/images/icon_vehicle_closed.gif</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>/images/spacer.gif</xsl:otherwise>
            </xsl:choose>
          </xsl:element>
          <xsl:element name="PropertyStatus">
            <xsl:choose>
                <xsl:when test="$PropertyCount &gt; 1">
                    <xsl:choose>
                        <xsl:when test="$PropertiesOpen = $PropertyCount">/images/icon_property_multi_open.gif</xsl:when>
                        <xsl:when test="$PropertiesOpen = 0">/images/icon_property_multi_closed.gif</xsl:when>
                        <xsl:when test="$PropertyCount &gt; 1 and $PropertiesOpen &lt; $PropertyCount">/images/icon_property_multi_open_closed.gif</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$PropertyCount = 1">
                    <xsl:choose>
                        <xsl:when test="$PropertiesOpen = $PropertyCount">/images/icon_property_open.gif</xsl:when>
                        <xsl:when test="$PropertiesOpen = 0">/images/icon_property_closed.gif</xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>/images/spacer.gif</xsl:otherwise>
            </xsl:choose>
          </xsl:element>
          <xsl:element name="InvolvedType"><xsl:value-of select="$InvolvedType"/></xsl:element>
          <xsl:element name="InvolvedName"><xsl:value-of select="$InvolvedName"/></xsl:element>
          <xsl:element name="ClaimNumber"><xsl:value-of select="@ClaimNumber"/></xsl:element>
          <xsl:element name="InsuranceCompany"><xsl:value-of select="@InsuranceCompany"/></xsl:element>
        </Result>
      </xsl:for-each>
    </Root>
  </xml>
  <xml id="xmlRawData" name="xmlRawData">
    <Root>
      <xsl:for-each select="/Root/Claim">
        <Claim>
          <xsl:for-each select="@*">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', name(.), '&gt;')"/>
            <xsl:value-of select="."/>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', name(.), '&gt;')"/>
          </xsl:for-each>
          <xsl:for-each select="Vehicle">
            <Vehicle>
            <xsl:for-each select="@*">
              <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', name(.), '&gt;')"/>
              <xsl:value-of select="."/>
              <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', name(.), '&gt;')"/>
            </xsl:for-each>
            </Vehicle>
          </xsl:for-each>

          <xsl:for-each select="Property">
            <Vehicle>
            <xsl:for-each select="@*">
              <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', name(.), '&gt;')"/>
              <xsl:value-of select="."/>
              <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', name(.), '&gt;')"/>
            </xsl:for-each>
            </Vehicle>
          </xsl:for-each>
        </Claim>
      </xsl:for-each>
    </Root>
  </xml>
  
  <IE:APDDataGrid id="Grid1" CCDataSrc="xmlSearchResult" CCDataPageSize="10" showAlternateColor="false" altColor="#E6E6FA" CCWidth="680px" CCHeight="235" showHeader="true" gridLines="horizontal" keyField="LynxID" oncontext1="showDetails()" onrowselect="showSelectedRow()" onBeforeRowSelect1="cancelSelect()">
      <IE:APDDataGridHeader>
          <IE:APDDataGridTR>
              <IE:APDDataGridTD width="50px" caption="LYNX ID" fldName="LynxID" DataType="number" sortable="true" CCStyle="text-align:center"/>
              <IE:APDDataGridTD width="60px" caption="Status" fldName="Status" DataType="" sortable="true" CCStyle="text-align1:center"/>
              <IE:APDDataGridTD width="85px" caption="Involved Type" fldName="InvolvedType" DataType="" sortable="true" CCStyle="text-align:center"/>
              <IE:APDDataGridTD width="160px" caption="Involved/Business Name" fldName="InvolvedName" sortable="true"/>
              <IE:APDDataGridTD width="110px" caption="ClaimNumber" fldName="ClaimNumber" DataType="" sortable="true"/>
              <IE:APDDataGridTD width="150px" caption="Insurance Co." fldName="InsuranceCompany" DataType="" sortable="true"/>
          </IE:APDDataGridTR>
      </IE:APDDataGridHeader>
      <IE:APDDataGridBody>
			  <IE:APDDataGridTR ondblclick="navigateTo('claim')">
					<IE:APDDataGridTD>
					  <IE:APDDataGridFld fldName="LynxID" />
					</IE:APDDataGridTD>
					<IE:APDDataGridTD>
					  <IE:APDDataGridFld fldName="ClaimStatus" displayType="image" ondblclick="navigateTo('claim')"/>
            <IE:APDDataGridFld displayType="text" value=" " />
            <IE:APDDataGridFld displayType="text" value=" " />
            <IE:APDDataGridFld fldName="VehicleStatus" displayType="image" ondblclick="navigateTo('vehicle')"/>
            <IE:APDDataGridFld displayType="text" value=" " />
            <IE:APDDataGridFld displayType="text" value=" " />
            <IE:APDDataGridFld fldName="PropertyStatus" displayType="image" ondblclick="navigateTo('property')"/>
					</IE:APDDataGridTD>
					<IE:APDDataGridTD>
					  <IE:APDDataGridFld fldName="InvolvedType" />
					</IE:APDDataGridTD>
					<IE:APDDataGridTD>
					  <IE:APDDataGridFld fldName="InvolvedName" />
					</IE:APDDataGridTD>
					<IE:APDDataGridTD>
					  <IE:APDDataGridFld fldName="ClaimNumber" />
					</IE:APDDataGridTD>
					<IE:APDDataGridTD>
					  <IE:APDDataGridFld fldName="InsuranceCompany" />
					</IE:APDDataGridTD>
			  </IE:APDDataGridTR>
      </IE:APDDataGridBody>
  </IE:APDDataGrid>
  <img src="/images/spacer.gif" style="height:5px;width:100%"/>
  <IE:APDTabGroup id="tab1" name="tab1" many="false" width="680" height="120" preselectTab="0" stackable="false" CCDisabled="false" CCTabIndex="2" showADS="false">
    <IE:APDTab id="tabPreview" name="tabPreview" caption="Claim Preview" tabPage="divClaimPreview" CCDisabled="false" CCTabIndex="3" hidden="false" />
    
    <IE:APDTabPage name="divClaimPreview" id="divClaimPreview" style="display:none">
      <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
        <colgroup>
          <col width="110px"/>
          <col width="222px"/>
          <col width="115px"/>
          <col width="217px"/>
        </colgroup>
        <tr>
          <td>Vehicles in Claim <span id="spnVehCount"/>:</td>
          <td>
            <IE:APDCustomSelect id="selVehicles" name="selVehicles" displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="3" required="false" width="200" onChange="openVehicle()">
            </IE:APDCustomSelect>
          </td>
          <td>Properties in Claim <span id="spnPrpCount"/>:</td>
          <td>
            <IE:APDCustomSelect id="selProperties" name="selProperties" displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="3" required="false" width="200" onChange="openProperty()">
            </IE:APDCustomSelect>
          </td>
        </tr>
        <tr>
          <td>LYNX ID:</td>
          <td colspan="3">
            <IE:APDLabel id="txtLynxID" name="txtLynxID" width="200" value="" CCDisabled="false" />
          </td>
        </tr>
        <tr>
          <td>Claim Number:</td>
          <td colspan="2">
            <IE:APDLabel id="txtClaimNumber" name="txtClaimNumber" width="200" value="" CCDisabled="false" />
          </td>
          <td>
            <IE:APDButton id="btnReassign" name="btnReassign" value="Reassign" CCDisabled="true" CCTabIndex="3" onButtonClick="reassignClaim()"/>
          </td>
        </tr>
        <tr>
          <td>Insurance Co.:</td>
          <td colspan="3">
            <IE:APDLabel id="txtInsCo" name="txtInsCo" width="200" value="" CCDisabled="false" />
          </td>
        </tr>
      </table>
    </IE:APDTabPage>
  </IE:APDTabGroup>
  <DIV id="divClaimVoided" name="divClaimVoided" style="z-index:2;position:absolute; top:280px;left:5px;width:650px;height:85px;FILTER:alpha(opacity=75);display:none">
    <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%;background-color:#FFFFFF;FILTER:alpha(opacity=100)">
      <tr>
        <td style="text-align:center;font-size:24px;font-weight:bold;color:#FF0000;letter-spacing:0.5mm ">
          <div id="divClaimStatusText">CLAIM VOIDED</div>
        </td>
      </tr>
    </table>
  </DIV>      
</BODY>
</HTML>
</xsl:template>

</xsl:stylesheet>
