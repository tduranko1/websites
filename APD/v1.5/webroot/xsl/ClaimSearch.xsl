<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:IE="http://mycompany.com/mynamespace"
  xmlns:user="http://mycompany.com/mynamespace"
  xmlns:js="urn:the-xml-files:xslt"
  id="ClaimSearch">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library-htc.js"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/">

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},19,uspClaimSearchParmGetDetailXML,ClaimSearch.xsl   -->

<HTML>
<HEAD>
<TITLE>Search Claim</TITLE>
<!-- STYLES -->
<LINK rel="STYLESHEET" href="/css/apdcontrols.css" type="text/css"/>

<!-- for radio buttons -->
<STYLE type="text/css">A {color:black; text-decoration:none;}</STYLE>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="javascript" src="/js/apdcontrols.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,27);
		  event.returnValue=false;
		  };	
</script>


<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">

<![CDATA[

var sSearchCriteria = "";

function pageInit(){
  if (oStat){
    oStat.style.top = (document.body.offsetHeight - 75) / 2;
    oStat.style.left = (document.body.offsetWidth - 250) / 2;
  }
}

function doSearch(){
  sSearchCriteria = "";
  if (txtSearchLynxID.value != "")
    sSearchCriteria = "LynxID=" + txtSearchLynxID.value;
  if (selInsCo.selectedIndex != -1)
    sSearchCriteria += (sSearchCriteria != "" ? "&" : "") + "InsuranceCompanyID=" + selInsCo.value;
  if (txtClaimNumber.value != "")
    sSearchCriteria += (sSearchCriteria != "" ? "&" : "") + "Claim=" + txtClaimNumber.value;
  if (txtInvolvedName.value != "")
    sSearchCriteria += (sSearchCriteria != "" ? "&" : "") + "Lastname=" + txtInvolvedName.value;
    
    
  if (sSearchCriteria == ""){
    ClientWarning("Please enter a search criteria.");
    return;
  }
  
  tabClaimSearch.CCDisabled = true;
  oStat.Show("Please wait...");
  document.frames["ifrmSearch"].frameElement.src = "/blank.asp";
  window.setTimeout("doSearch2()", 100);
}

function doSearch2(){
  SearchResultsInfo.style.visibility = "visible";
  document.frames["ifrmSearch"].frameElement.src = "/ClaimSearchResults.asp?" + sSearchCriteria;
}

function hideMessage(){
  if (oStat)
    oStat.Hide();
    
  tabClaimSearch.CCDisabled = false;
}

function closeMe( src )
{
  window.returnValue = src;
  window.close();
}

]]>
</SCRIPT>
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" style="border:0px;padding:5px" onload="pageInit()">
<IE:APDTabGroup id="tabSearch" name="tabSearch" many="false" width="680" height="78" preselectTab="0" stackable="false" CCDisabled="false" CCTabIndex1="1" showADS="false"   >
  <IE:APDTab id="tabClaimSearch" name="tabClaimSearch" caption="Search Claims" tabPage="divClaimSearch" CCDisabled="false" CCTabIndex1="2" hidden="false"  />

  <IE:APDTabPage name="divClaimSearch" id="divClaimSearch" style="display:none">
    <table border="0" cellpadding="2" cellspacing="0" style="width:100%">
      <colgroup>
        <col width="90"/>
        <col width="200"/>
        <col width="100"/>
        <col width="*"/>
        <col width="50"/>
      </colgroup>
      <tr>
        <td>LYNX ID:</td>
        <td>
          <IE:APDInputNumeric id="txtSearchLynxID" name="txtSearchLynxID" precision="8" scale="0" neg="false" int="true" required="false" canDirty="false" CCDisabled="false" CCTabIndex="3" onChange="" />
        </td>
        <td>Insurance Company:</td>
        <td colspan="2">
          <IE:APDCustomSelect id="selInsCo" name="selInsCo" displayCount="5" blankFirst="true" canDirty="false" CCDisabled="false" CCTabIndex="4" sorted="true">
            <xsl:for-each select="/Root/Reference[@List='InsuranceCompany']">
          	  <IE:dropDownItem style="display:none">
                <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                <xsl:value-of select="@Name"/>
              </IE:dropDownItem>
            </xsl:for-each>
          </IE:APDCustomSelect>
        </td>
      </tr>
      <tr>
        <td>Involved Name:</td>
        <td>
          <IE:APDInput id="txtInvolvedName" name="txtInvolvedName" value="" size="30" maxLength="45" required="false" canDirty="false" CCDisabled="false" CCTabIndex="5" />
        </td>
        <td>Claim Number:</td>
        <td>
          <IE:APDInput id="txtClaimNumber" name="txtClaimNumber" value="" size="30" maxLength="45" required="false" canDirty="false" CCDisabled="false" CCTabIndex="6" />
        </td>
        <td>
          <IE:APDButton id="btnSearch" name="btnSearch" value="Search" width="50" CCDisabled="false" CCTabIndex="7" onButtonClick="doSearch()"/>
        </td>
      </tr>
    </table>
  </IE:APDTabPage>
  
</IE:APDTabGroup>

<DIV unselectable="on" id="SearchResultsInfo"  style="position:absolute; visibility:hidden; left:6px; top:90px; z-index:1; width: 682px; height: 375px;border:0px solid #FF0000">
  <IFRAME id="ifrmSearch" name="ifrmSearch" src="blank.asp" style="width:100%; height:100%; border:0px; visibility:inherit; position:absolute; background:transparent; overflow:hidden;border:0px solid #FF0000" allowtransparency="true" >
  </IFRAME>
</DIV>

<IE:APDStatus id="oStat" name="oStat" width="250" height="75" />

</BODY>
</HTML>
</xsl:template>

</xsl:stylesheet>
