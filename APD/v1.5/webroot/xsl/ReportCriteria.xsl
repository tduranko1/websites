<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ms="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace" 
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ReportCriteria" >
<xsl:import href="msxsl/msjs-client-library-htc.js"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
    
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="ReportCategory"/>

<ms:script language="JScript" implements-prefix="user">
  <![CDATA[
    function makeOptions(strValues, strCaptions) {
      if (strValues != "" && strCaptions != "") {
        var strRet = "";
        var aValues, aCaptions;
        aValues = strValues.split("|");
        aCaptions = strCaptions.split("|");
        iLen = Math.min(aValues.length, aCaptions.length);
        for (var i = 0; i < iLen; i++){
          strRet += "<option value='" + aValues[i] + "'>" + aCaptions[i] + "</option>";
        }
        
        return strRet;
      } else
        return "";
    }
    function escapedDoubleQuotes(strValue) {
      if (strValue != "") {
        var strRet = "";
        strRet = strValue.replace(/\"/g, "\\\"");
        return strRet;
      } else
        return "";
    }
  ]]>
</ms:script>

<xsl:template match="/Root">
<html>
<head>
  <LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
  <LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>

  <SCRIPT language="JavaScript" src="/js/datepicker2.js"></SCRIPT>
  <SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>
  <script type="text/javascript"> 
	var sReportCategory="<xsl:value-of select='string($ReportCategory)'/>";
    document.onhelp=function(){  
		   if (sReportCategory =='ClientReports') 
		      RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,6);
			 else if (sReportCategory == 'ManagementReports') 
			   RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,17);
			 else if (sReportCategory == 'OperationalReports') 
		       RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,21);
			 else
  	 		   RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
           event.returnValue=false;
   	       };	
  </script>
  <script language="javascript">
    aCriteriaObjs = new Array(<xsl:for-each select="Criteria">"<xsl:value-of select="@ParamName"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    aCriteriaObjsReq = new Array(<xsl:for-each select="Criteria">"<xsl:value-of select="@RequiredField"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    aCriteriaObjsName = new Array(<xsl:for-each select="Criteria">"<xsl:value-of select="user:escapedDoubleQuotes(string(@DisplayText))"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    aCriteriaObjsDataType = new Array(<xsl:for-each select="Criteria">"<xsl:value-of select="@DataType"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    aCriteriaObjsMaxValue = new Array(<xsl:for-each select="Criteria">"<xsl:value-of select="@MaxValue"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    aCriteriaObjsDefaultValueScript = new Array(<xsl:for-each select="Criteria">"<xsl:value-of select="@DefaultValueScript"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    sReportTitle = "<xsl:value-of select="js:cleanString(string(@ReportName))"/>";
    sProcName = "<xsl:value-of select="js:cleanString(string(@ProcName))"/>";
    sRptName = "<xsl:value-of select="js:cleanString(string(@RptName))"/>";
    sStaticParam = "<xsl:value-of select="js:cleanString(string(@StaticParameters))"/>";
    iExtBrowser = null;
    
<![CDATA[
    function pageInit(){
      if (__blnPageInitialized == false){
         window.setTimeout("pageInit()", 100);
         return;
      }
      if (aCriteriaObjs != null) {
        var sDefaultValue = null;
        for (var i = 0; i < aCriteriaObjs.length; i++) {
          if (aCriteriaObjsDefaultValueScript[i] != "") {
            eval("sDefaultValue = " + aCriteriaObjsDefaultValueScript[i] + ";");
            if (sDefaultValue != null) {
              document.getElementById(aCriteriaObjs[i]).value = sDefaultValue;
              sDefaultValue = null;
            }
          }
        }
        
        if (csExportFormat.selectedIndex != -1 && csExportFormat.Options.length == 1) {
          document.getElementById("divExportFormat").style.display = "none";
          //set the first and only item in the export format as the default.
          csExportFormat.selectedIndex = 0;
        }
      }
      
      document.getElementById("ifrmReport").style.height = document.body.offsetHeight - tblCriteria.offsetHeight;
    }
    
    function isDataValid() {
      if (aCriteriaObjs !== null) {
        for (var i = 0; i < aCriteriaObjs.length; i++) {
          if (aCriteriaObjsReq[i] == "1") {
            var obj = document.getElementById(aCriteriaObjs[i]);
            switch (aCriteriaObjsDataType[i]) {
              case "R":
                if (obj) {
                  if (obj.selectedIndex == -1) {
                    ClientWarning(aCriteriaObjsName[i] + " is a required field. Please select a value from the list and try again.");
                    obj.setFocus();
                    return false;
                  }
                }
                break;
              case "N":
                if (obj) {
                  if (isNaN(obj.value)){
                    ClientWarning(aCriteriaObjsName[i] + " is a numeric field. Please enter a numeric value and try again.");
                    obj.setFocus();
                    return false;
                  }
                  if (obj.value == "") {
                    ClientWarning(aCriteriaObjsName[i] + " is a required field. Please enter a value and try again.");
                    obj.setFocus();
                    return false;
                  }
                  if (parseFloat(obj.value) > parseFloat(aCriteriaObjsMaxValue[i])) {
                    ClientWarning(aCriteriaObjsName[i] + " value cannot exceed " + aCriteriaObjsMaxValue[i] + ". Please try again.");
                    obj.setFocus();
                    return false;
                  }
                }
                break;
            }
          }
        }
      }
      if (csExportFormat.selectedIndex == -1) {
        ClientWarning("Please select the report format.")
        csExportFormat.setFocus();
        return false;
      }
      return true;
    }

    function getReport() {
      if (isDataValid()) {
        var sCriteria = "";
        var sStateList = "";
        var bAllStates = false;

        var objStateCDList = document.getElementById("StateCDList");
        if (objStateCDList) {
          if (objStateCDList.selectedIndex == -1) {
            sStateList = "";
            bAllStates = true;
          } else {
            sStateList = objStateCDList.text;
          }
        }
        
        if (aCriteriaObjs.join(",").indexOf("InsuranceCompanyID") == -1)
          sCriteria = "&InsuranceCompanyID=" + (parent.csInsurance && parent.csInsurance.selectedIndex != -1 ? parent.csInsurance.value : "");
          
        var sFriendlyName = sReportTitle;
        for (var i = 0; i < aCriteriaObjs.length; i++) {
          var obj = document.getElementById(aCriteriaObjs[i]);
          if (obj) {
            switch (aCriteriaObjs[i]) {
              case "StateCDList":
                sCriteria += "&" + aCriteriaObjs[i] + "=" + sStateList;
                if (bAllStates)
                  sFriendlyName += "-All States";
                else {
                  if (sStateList != "")
                    sFriendlyName += "-" + sStateList;
                }
                sFriendlyName = sFriendlyName.replace(/by State/g, "");
                break;
              default:
                sCriteria += "&" + aCriteriaObjs[i] + "=" + obj.value;
                if (obj.tagName.toLowerCase() == "apdcustomselect") {
                  if (obj.text.Trim() != "")
                    sFriendlyName += "-" + obj.text;
                }
                else {
                  if (obj.value != "")
                    sFriendlyName += "-" + obj.value;
                }
                break;
            }
          }
        }      
        
        //All file name illegal chars should be corrected. Or else we will get a file download dialog rather than inline pdf.
        sFriendlyName = sFriendlyName.replace(/\//g, "-"); // replace / char with -
        sFriendlyName = sFriendlyName.replace(/[\\\:\*\?\"\<\>\|]/g, ""); // invalid file name chars are \/:*?<>|. so remove these
        
        var objForm = document.getElementById("frmGetReport");
        if (document.getElementById("chkOpenExternal").value == 1) {
          if ((iExtBrowser != null) && (iExtBrowser.closed == false)) iExtBrowser.close();
          //if ((iExtBrowser == null) || (iExtBrowser.closed == true)) {
            var sFeatures = "directories=no, location=no, menubar=no, resizable=yes, scrollbars=no, status=no, titlebar=no, toolbar=no "
            iExtBrowser = window.open("/blank.asp", "sExtBrowser", sFeatures);
          //}
          objForm.target = "sExtBrowser";
          try {
            ifrmReport.frameElement.style.display = "none";
          } catch (e) {}
        } else {
          objForm.target = "ifrmReport";
          try {
            ifrmReport.frameElement.style.display = "inline";
            ifrmReport.frameElement.style.border = "1px solid #C0C0C0";
          } catch (e) {}
        }
        //alert("sRptName : " + sRptName + 
        //      "\nsProcName : " + sProcName +
        //      "\nsCriteria : " + sCriteria +
        //      "\nsStaticParam : " + sStaticParam +
        //      "\nsFriendlyName : " + sFriendlyName);
        //return;
        //sFormat = "PDF";
        var sFormat = csExportFormat.value;
        
        if (sFormat == "" || sFormat == null)
          sFormat = "PDF";

        objForm.reportFileName.value = sRptName;
        objForm.storedProc.value = sProcName;
        objForm.reportParams.value = sCriteria;
        objForm.staticParams.value = sStaticParam;
        objForm.friendlyName.value = sFriendlyName + "." + sFormat;
        objForm.reportFormat.value = sFormat;
        objForm.submit();

        if (document.getElementById("chkOpenExternal").checked == true) {
          if (iExtBrowser != null)
            iExtBrowser.focus();
        }
      }
    }

	if (document.attachEvent)
      document.attachEvent("onclick", top.hideAllMenuScriptlets);
    else
      document.onclick = top.hideAllMenuScriptlets;
	
]]>
  </script>
  </head>
<body style="margin:0px;padding:0px;border:0px;width:100%;height:100%;overflow:hidden" tabindex="-1" onload="pageInit()">
<table name="tblCriteria" id="tblCriteria" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;width:100%">
  <tr valign="bottom">
    <td>
      <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
      <colgroup>
        <col width="110px"/>
        <col width="*"/>
      </colgroup>
    <xsl:for-each select="Criteria">
      <xsl:sort data-type="number" select="@DisplayOrder" order="ascending" />
      <xsl:variable name="CriteriaID"><xsl:value-of select="@CriteriaID"/></xsl:variable>
      <tr>
        <td class="bodyBlue">
          <xsl:choose>
            <xsl:when test="@DataType='B'">
              <xsl:attribute name="colspan">2</xsl:attribute>
              <IE:APDCheckBox canDirty="false">
                <xsl:attribute name="id"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <xsl:attribute name="caption"><xsl:value-of select="@DisplayText"/></xsl:attribute>
                <xsl:attribute name="CCTabIndex"><xsl:value-of select="position()"/></xsl:attribute>
                <xsl:attribute name="sorted">true</xsl:attribute>
                <xsl:if test="@RequiredField='1'">
                  <xsl:attribute name="required">true</xsl:attribute>
                </xsl:if>
              </IE:APDCheckBox>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="@DisplayText"/>:
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td>
          <xsl:choose>
            <xsl:when test="@DataType='R' and @ReferenceType='S'">
              <IE:APDCustomSelect displayCount="5" canDirty="false">
                <xsl:attribute name="id"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <xsl:attribute name="CCTabIndex"><xsl:value-of select="position()"/></xsl:attribute>
                <xsl:attribute name="sorted">true</xsl:attribute>
                <xsl:choose>
                  <xsl:when test="@RequiredField='1'">
                    <xsl:attribute name="required">true</xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:attribute name="blankFirst">true</xsl:attribute>
                    <xsl:attribute name="value"></xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:for-each select="/Root/Reference[@List=$CriteriaID]">
              	  <IE:dropDownItem style="display:none">
                  <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                  <xsl:value-of select="@Name"/>
                  </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </xsl:when>
            <xsl:when test="@DataType='R' and @ReferenceType='L'">
              <xsl:variable name="valueAndCaption"><xsl:value-of select="/Root/Reference[@List=$CriteriaID]/@Name"/></xsl:variable>
              <xsl:variable name="valueList">
                <xsl:value-of select="substring-before($valueAndCaption, ',')"/>
              </xsl:variable>
              <xsl:variable name="CaptionList">
                <xsl:value-of select="substring-after($valueAndCaption, ',')"/>
              </xsl:variable>
              <xsl:variable name="required">
                <xsl:choose>
                  <xsl:when test="@RequiredField='1'">true</xsl:when>
                  <xsl:otherwise>false</xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:variable name="blankFirst">
                <xsl:choose>
                  <xsl:when test="@RequiredField='1'">false</xsl:when>
                  <xsl:otherwise>true</xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:value-of disable-output-escaping="yes" select="js:APDCustomSelect(string(@ParamName), '', string($valueList), string($CaptionList), string($blankFirst), 5, 'false', string($required), position(), 'false')"/>
            </xsl:when>
            <xsl:when test="@DataType='N'">
              <IE:APDInputNumeric scale="0" neg="false" int="true" canDirty="false">
                <xsl:attribute name="id"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <xsl:attribute name="precision"><xsl:value-of select="@MaxLength"/></xsl:attribute>
                <xsl:attribute name="CCTabIndex"><xsl:value-of select="position()"/></xsl:attribute>
                <xsl:if test="@RequiredField='1'">
                  <xsl:attribute name="required">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputNumeric>
            </xsl:when>
            <xsl:when test="@DataType='D'">
              <IE:APDInputDate id="" name="" value="" type="" futureDate="false" canDirty="false" >
                <xsl:attribute name="id"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <xsl:attribute name="CCTabIndex"><xsl:value-of select="position()"/></xsl:attribute>
                <xsl:if test="@RequiredField='1'">
                  <xsl:attribute name="required">true</xsl:attribute>
                </xsl:if>
              </IE:APDInputDate>
            </xsl:when>
          </xsl:choose>
        </td>
      </tr>
    </xsl:for-each>
    </table>
    </td>
    <td>
      <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
      <tr>
        <td colspan="2">    
          <div id="divExportFormat" name="divExportFormat">Format:
            <IE:APDCustomSelect id="csExportFormat" name="csExportFormat" displayCount="5" canDirty="false" CCTabIndex="-1" required="true">
              <xsl:attribute name="value"><xsl:value-of select="/Root/Reference[@List='ExportFormat' and @DefaultFlag='1']/@ReferenceID"/></xsl:attribute>
              <xsl:for-each select="/Root/Reference[@List='ExportFormat']">
                <xsl:sort select="@Name" data-type="text"/>
                <IE:dropDownItem style="display:none">
                  <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                  <xsl:value-of select="@Name"/>
                </IE:dropDownItem>
              </xsl:for-each>
            </IE:APDCustomSelect>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div id="divViewExternal" name="divViewExternal">
            <IE:APDCheckBox id="chkOpenExternal" name="chkOpenExternal" caption="View report in separate window" CCTabIndex="-1" canDirty="false" />
          </div>
        </td>
        <td>
          <IE:APDButton id="viewReport" name="viewReport" value="View Report" CCTabIndex="" onButtonClick="getReport()"/>
        </td>        
      </tr>
      </table>
    </td>
    </tr>
    </table>
  	<IFRAME id="ifrmReport" name="ifrmReport" style="width:100%;border:0px solid #000000;overflow:hidden;" frameBorder="0" >
    <xsl:attribute name="src">/blank.asp?ReportCategory=<xsl:value-of select="$ReportCategory"/></xsl:attribute>
    </IFRAME>
	  
  <form name="frmGetReport" id="frmGetReport" method="post" target="ifrmReport" action="getReport.asp">
    <input type="hidden" name="reportFileName" id="reportFileName"/>
    <input type="hidden" name="storedProc" id="storedProc"/>
    <input type="hidden" name="reportParams" id="reportParams"/>
    <input type="hidden" name="staticParams" id="staticParams"/>
    <input type="hidden" name="friendlyName" id="friendlyName"/>
    <input type="hidden" name="reportFormat" id="reportFormat"/>
  </form>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
