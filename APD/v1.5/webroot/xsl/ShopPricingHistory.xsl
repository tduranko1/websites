<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:IE="http://mycompany.com/mynamespace"
	id="ShopPricingHistory">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />





<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserId"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTShopPricingHistoryGetDetailXML,ShopPricingHistory.xsl,2471   -->

<HEAD>
  <TITLE><xsl:value-of select="/Root/@ShopID"/> -- Shop Pricing</TITLE>

  <LINK rel="stylesheet" href="/css/pmd.css" type="text/css"/>
  <LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>

  <STYLE type="text/css">
      A {color:black; text-decoration:none;}
      .nowrap {text-overflow:ellipsis;overflow:hidden;white-space:nowrap;width:100%;cursor:default;}
  </STYLE>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
  
  
  
<!-- CLIENT SCRIPTS -->
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

</HEAD>

<body style="overflow:hidden; margin-left:0; margin-right:0;">
  
  <xsl:choose>
    <xsl:when test="/Root/@PricingAvailable!='0'"><xsl:apply-templates select="Pricing"/></xsl:when>
    <xsl:otherwise>There is no pricing data available in the APD system for the shop to which this vehicle is assigned.</xsl:otherwise>
  </xsl:choose>

</body>
</HTML>
</xsl:template>

<xsl:template match="Pricing">

<TABLE unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="0">
<colgroup>
  <col width="10px"/>
  <col width="256px"/>
  <col width="30px"/>
  <col widht="256px"/>
  <col width="30px"/>
  <col widht="258px"/>
</colgroup>
<TR>
  <TH colspan="6" style="font-size:large"><xsl:value-of select="@Name"/></TH>
</TR>
<TR>
  <TH colspan="6"><xsl:value-of select="@Address"/></TH>
</TR>
<TR>
  <TH colspan="6"><xsl:value-of select="@City"/></TH>
</TR>
<TR>
  <TH colspan="6"><xsl:value-of select="@Phone"/></TH>
</TR>
<TR>
  <TD><img source="/images/spacer.gif" border="0" height="30px" width="10px" style="visibility:hidden"/></TD>
</TR>
<TR>
<TD></TD>
<TD valign="top">
<table unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">
        <table width="100%" border="0" cellspacing="1" cellpadding="1">
        <tr>
          <th colspan="2" align="left">Hourly Labor Rates:</th>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Sheet Metal:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@HourlyRateSheetMetal"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Refinishing:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@HourlyRateRefinishing"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Unibody/Frame:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@HourlyRateUnibodyFrame"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Mechanical:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@HourlyRateMechanical"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="7px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td colspan="2">
      <table unselectable="on" width="100%" border="0" cellpadding="1" cellspacing="1">
        <tr>
          <th colspan="4" align="left">Refinish Materials Charges:</th>
        </tr>
        <!--<tr>
          <td width="5px"></td>
          <td colspan="3">Single Stage:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
          <td>Charge Per Hour:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@RefinishSingleStageHourly"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Maximum Charge:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@RefinishSingleStageMax"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Two Stage:</td>
        </tr>-->
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Charge Per Hour:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageHourly"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Maximum Charge:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageMax"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Clear Coat Max Hours:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageCCMaxHrs"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <!--<tr>
          <td width="5px"></td>
          <td colspan="3">Three Stage:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Charge Per Hour:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageHourly"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Maximum Charge:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageMax"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Clear Coat Max Hours:</td>
          <td align="right">
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageCCMaxHrs"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>-->
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="7px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
	  <div style="height:55">
      <table width="100%" cellpadding="1" cellspacing="1">
        <tr>
          <th colspan="3" align="left">Towing Charges:</th>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Charge Type:</td>
          <td align="right">
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@TowInChargeTypeCD"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <xsl:if test="@TowInChargeTypeCD='F'">
          <tr>
            <td width="5px"></td>
            <td>Flat Fee:</td>
            <td align="right">
              <IE:APDLabel width="60px" style="align:right">
                <xsl:attribute name="value"><xsl:value-of select="@TowInFlatFee"/></xsl:attribute>
              </IE:APDLabel>
            </td>
          </tr>
        </xsl:if>        
      </table>
	  </div>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="7px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="1" cellspacing="1">
        <tr>
          <th colspan="3" align="left">Frame Setup Charges (Hours):</th>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>No Measurement:</td>
          <td align="right">
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@PullSetup"/></xsl:attribute>
            </IE:APDLabel>hrs
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>With Measurement</td>
          <td align="right">
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@PullSetupMeasure"/></xsl:attribute>
            </IE:APDLabel>hrs
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</TD>
<TD></TD> <!-- spacer column -->
<TD valign="top">
<table width="100%" valign="top" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <table width="100%" valign="top" cellpadding="1" cellspacing="1">
        <tr>
          <th colspan="4" align="left">Air Conditioning Service Charges:</th>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">R-12 Evaluate/Recharge:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>With Freon:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@R12EvacuateRechargeFreon"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Without Freon:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@R12EvacuateRecharge"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">R-134 Evaluate/Recharge:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>With Freon:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@R134EvacuateRechargeFreon"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Without Freon:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@R134EvacuateRecharge"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="1" cellspacing="1">
        <tr>
          <th colspan="4" align="left">Stripe Replacement Charges:</th>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Tape Stripes:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Per Panel:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@StripeTapePerPanel"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Per Side:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@StripeTapePerSide"/></xsl:attribute>
            </IE:APDLabel>
          </td>
		    </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Paint Stripes:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Per Panel:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@StripePaintPerPanel"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Per Side:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@StripePaintPerSide"/></xsl:attribute>
            </IE:APDLabel>
          </td>
		    </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="1" cellspacing="1">
        <tr>
          <th colspan="3" align="left">Glass Charges:</th>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Replacement Type:</td>
          <td align="right">
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@GlassReplacementChargeTypeCD"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <xsl:choose>
          <xsl:when test="@GlassReplacementChargeTypeCD='NAGS'">
            <tr>
              <td width="5px"></td>
              <td>Windshield Discount:</td>
              <td align="right">
                <IE:APDLabel width="60px" style="align:right">
                  <xsl:attribute name="value"><xsl:value-of select="@DiscountPctWindshield"/></xsl:attribute>
                </IE:APDLabel>%
              </td>
            </tr>
            <tr>
              <td width="5px"></td>
              <td>Side/Back Discount:</td>
              <td align="right">
                <IE:APDLabel width="60px" style="align:right">
                  <xsl:attribute name="value"><xsl:value-of select="@DiscountPctSideBackGlass"/></xsl:attribute>
                </IE:APDLabel>%
              </td>
            </tr>
          </xsl:when>
          <xsl:when test="@GlassReplacementChargeTypeCD='Sublet'">
            <tr>
              <td width="5px"></td>
              <td>Sublet Fee:</td>
              <td align="right">$
                <IE:APDLabel width="60px" style="align:right">
                  <xsl:attribute name="value"><xsl:value-of select="@GlassSubletServiceFee"/></xsl:attribute>
                </IE:APDLabel>
              </td>
            </tr>
            <tr>
              <td width="5px"></td>
              <td>Sublet Percent:</td>
              <td align="right">
                <IE:APDLabel width="60px" style="align:right">
                  <xsl:attribute name="value"><xsl:value-of select="@GlassSubletServicePct"/></xsl:attribute>
                </IE:APDLabel>%
              </td>
            </tr>
          </xsl:when>
          <xsl:when test="@GlassReplacementChargeTypeCD='Outsource'">
            <tr>
              <td width="5px"></td>
              <td></td>
              <td align="right">
                <IE:APDLabel width="60px" style="align:right">
                  <xsl:attribute name="value"><xsl:value-of select="@GlassOutsourceServiceFee"/></xsl:attribute>
                </IE:APDLabel>%
              </td>
            </tr>
          </xsl:when>
          <xsl:otherwise><tr><td colspan="3"></td></tr></xsl:otherwise>
        </xsl:choose>
      </table>
    </td>
  </tr>
</table>
</TD>
<TD></TD> <!-- spacer column -->
<TD valign="top">
<table border="0" width="235" cellpadding="1" cellspacing="1">
  <tr valign="top" width="100%">
    <td>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <th align="left" colspan="3">Recycled Parts:</th>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Markup Percent:</td>
          <td align="right">
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@PartsRecycledMarkupPct"/></xsl:attribute>
            </IE:APDLabel>%
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="6px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="235" cellpadding="1" cellspacing="1">
        <tr>
          <th align="left" colspan="3">Additional Repair Charges:</th>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Daily Storage:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@DailyStorage"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>2-Wheel Alignment:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@AlignmentTwoWheel"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>4-Wheel Alignment:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@AlignmentFourWheel"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Tire Mount and Balance:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@TireMountBalance"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Flex Additive:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@FlexAdditive"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Coolant (Green):</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@CoolantGreen"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Coolant (Red):</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@CoolantRed"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Undercoat:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@Undercoat"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Chip Guard:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@ChipGuard"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Caulking/Seam Sealer:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@CaulkingSeamSealer"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Corrosion Protection:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@CorrosionProtection"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Cover Car:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@CoverCar"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Hazardous Waste:</td>
          <td align="right">$
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@HazardousWaste"/></xsl:attribute>
            </IE:APDLabel>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="235" cellpadding="1" cellspacing="1">
        <tr>
          <td colspan="3"><b>Tax Rates:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Sales:</td>
          <td align="right">
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@SalesTaxPct"/></xsl:attribute>
            </IE:APDLabel>%
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>County:</td>
          <td align="right">
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@CountyTaxPct"/></xsl:attribute>
            </IE:APDLabel>%
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Municipal:</td>
          <td align="right">
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@MunicipalTaxPct"/></xsl:attribute>
            </IE:APDLabel>%
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Other:</td>
          <td align="right">
            <IE:APDLabel width="60px" style="align:right">
              <xsl:attribute name="value"><xsl:value-of select="@OtherTaxPct"/></xsl:attribute>
            </IE:APDLabel>%
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</TD>
</TR>
</TABLE>
</xsl:template>

</xsl:stylesheet>
