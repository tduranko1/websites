<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    id="PMDDateSearch">


<xsl:template name="DateSearch">
  
  <xsl:param name="AssignmentCode"/>
  
  <link rel="stylesheet" type="text/css" src="/css/datepicker.css"/>
  <script language="javascript" src="/js/datepicker.js"></script>
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
  <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
  </script>
  
  <script language="javascript">
   
  <![CDATA[
  document.attachEvent("onkeypress", SearchOnEnter);
   
  function SearchOnEnter(){
  	if (event.keyCode == 13) btnSearch_onclick();
  }
  
  
  function btnSearch_onclick(){
  	var sURL = window.location.toString(); 
	var pos = sURL.indexOf("&");
	var sQString;
	if (pos != -1)
		sURL = sURL.substr(0, pos);
	
	if (!CheckDate(txtBeginDate)) return;		
	if (!CheckDate(txtEndDate)) return;
	
		
	sURL += "&BeginDate=" + txtBeginDate.value + "&EndDate=" + txtEndDate.value + "&AssignmentCode=" + txtAssignmentCode.value;
	window.navigate(sURL);
   }
  
  function CheckDate(obj){
  	if (obj.value == "") return true;
    if (obj.value.length < 6 || parent.chkdate(obj) == false )    {
		parent.ClientWarning("The entered date is invalid. Please enter dates in 'mm/dd/yyyy' format");
        obj.select();
        return false;
    }
    return true;
  }
  
    
  ]]>
  </script>
  
<TABLE unselectable="on" width="100%" class="ShopAssignTableSrh" border="0" cellspacing="0" cellpadding="2">
      <TR unselectable="on">
        <TD unselectable="on" nowrap="nowrap">Date Ranges:</TD>
        <TD unselectable="on"><IMG src="/images/spacer.gif" width="18" height="18" border="0" /></TD>
        <TD unselectable="on" nowrap="nowrap">From</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtBeginDate" name="txtBeginDate" class="inputFld" size="9" maxlength="10" value="" onBlur="parent.remove_XS_whitespace(this)" />
		  <A  href='#' onclick='ShowCalendar(dimgBeginDate,txtBeginDate)' >
		    <IMG  align='absmiddle' border='0' width='24' height='17' id='dimgBeginDate' src='/images/calendar.gif'/>
		  </A>
		  <DIV  id='divCalendar' class='PopupDiv' onMouseOut='CalMouseOut(this)' onMouseOver='CalMouseOver(this)' onDeactivate='CalBlur(this)' style='position:absolute;width:180px;height:127px;z-index:20000;display:none;visibility:hidden'></DIV>
        </TD>
		<TD unselectable="on"><IMG src="/images/spacer.gif" width="18" height="18" border="0" /></TD>
        <TD unselectable="on" nowrap="nowrap">To</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtEndDate" name="txtEndDate" class="inputFld" size="9" maxlength="10" value="" onBlur="parent.remove_XS_whitespace(this)" />
          <A  href='#' onclick='ShowCalendar(dimgEndDate,txtEndDate)' >
		    <IMG  align='absmiddle' border='0' width='24' height='17' id='dimgEndDate' src='/images/calendar.gif'/>
		  </A>
		  <DIV  id='divCalendar' class='PopupDiv' onMouseOut='CalMouseOut(this)' onMouseOver='CalMouseOver(this)' onDeactivate='CalBlur(this)' style='position:absolute;width:180px;height:127px;z-index:20000;display:none;visibility:hidden'></DIV>
        </TD>
        <TD unselectable="on"><IMG src="/images/spacer.gif" width="18" height="18" border="0" /></TD>
        <TD unselectable="on">
          <SELECT id="txtAssignmentCode" size="1" class="inputFld">
  			    <OPTION value="B">
              <xsl:if test="boolean($AssignmentCode='B')">
                <xsl:attribute name="SELECTED">
                  <xsl:text>on</xsl:text>
                </xsl:attribute>
              </xsl:if>
              <xsl:text>Open and Closed Assignments</xsl:text>
		        </OPTION>
  			    <OPTION value="O">
              <xsl:if test="boolean($AssignmentCode='O')">
                <xsl:attribute name="SELECTED">
                  <xsl:text>on</xsl:text>
                </xsl:attribute>
              </xsl:if>
              <xsl:text>Open Assignments</xsl:text>
			      </OPTION>
  			    <OPTION value="C">
              <xsl:if test="boolean($AssignmentCode='C')">
                <xsl:attribute name="SELECTED">
                  <xsl:text>on</xsl:text>
                </xsl:attribute>
              </xsl:if>
              <xsl:text>Closed Assignments</xsl:text>
			      </OPTION>
          </SELECT>
          <!--<xsl:value-of select="/Root/@AssignmentCode"/>-->
        </TD>
        <TD unselectable="on"><IMG src="/images/spacer.gif" width="18" height="18" border="0" /></TD>
        <TD unselectable="on" align="center" nowrap="nowrap">
          <INPUT unselectable="on" type="button" id="btnSearch" name="btnSearch" value="Search" class="searchBtn" onClick="btnSearch_onclick();" />
        </TD>
        <TD unselectable="on" width="100%" align="center"></TD>
		<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    	<TD unselectable="on"><!--<IMG src="/images/smrefresh.gif" onClick="parent.upLevel()" border="0" width="16" height="16" alt="Return to Shop Search" style="cursor:hand;" />--></TD>
    	<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      </TR>
    </TABLE>
</xsl:template>


<!-- Gets the shop details -->
<xsl:template name="ShopInfo">
  <xsl:param name="ShopCRUD"/>
  <DIV unselectable="on" class="SmallGroupingTab" style="position:absolute; z-index:2; top: 70px; left: 6px;">Shop Info</DIV>
  <DIV unselectable="on" class="SmallGrouping" style="position:absolute; left:3px; top:86px; z-index:1;">
  <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="3" style="table-layout:fixed; overflow:hidden;">
    <colgroup>
    	<col width="55" nowrap="nowrap"/>
    	<col width="40" nowrap="nowrap"/>
    	<col width="35" nowrap="nowrap"/>
    	<col width="90" nowrap="nowrap"/>
    	<col width="60" nowrap="nowrap"/>
    	<col width="130" nowrap="nowrap"/>
    </colgroup>

    <TR unselectable="on">
      <TD unselectable="on" align="right" nowrap="nowrap" colspan="4" class="TableHeader2">
        <SPAN class="boldtext">Shop ID:</SPAN>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of select="@ShopLocationID"/>
      </TD>
      <TD unselectable="on" class="TableHeader2"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" align="right" nowrap="nowrap" class="TableHeader2">
        <SPAN class="boldtext">Program Score:</SPAN>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of select="@ProgramScore"/>
      </TD>
    </TR>
    <TR unselectable="on" bgcolor="#FDF5E6">
      <TD unselectable="on" class="boldtext">Name:</TD>
      <TD unselectable="on" colspan="3" nowrap="nowrap" style="overflow:hidden; height1:34">
        <span class="NoWrap" >
        <xsl:choose>
          <xsl:when test="contains($ShopCRUD, 'R') = true()">
            <a>
              <xsl:attribute name="href">javascript:NavToShop("S", <xsl:value-of select="@ShopLocationID"/>, <xsl:value-of select="@ShopBusinessID"/>)</xsl:attribute>
              <xsl:value-of select="@Name"/>
            </a>
          </xsl:when>
          <xsl:otherwise><xsl:value-of select="@Name"/></xsl:otherwise>
        </xsl:choose>
        </span>
      </TD>
      <TD unselectable="on" class="boldtext">Pgm Mgr:</TD>
      <TD unselectable="on" nowrap="nowrap">
        <span class="NoWrap" >
       <xsl:attribute name="ProgramManagerUserID"><xsl:value-of select="@ProgramManagerUserID"/></xsl:attribute>
        <xsl:value-of select="@ProgramManagerName"/>
        </span>
      </TD>
    </TR>
    <TR unselectable="on" bgcolor="#FFFFEE">
      <TD unselectable="on" class="boldtext">Address:</TD>
      <TD unselectable="on" colspan="3" nowrap="nowrap">
        <span class="NoWrap" >
        <xsl:value-of select="@Address1"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </span>
      </TD>
      <TD unselectable="on" class="boldtext">Contact:</TD>
      <TD unselectable="on" nowrap="nowrap"><span class="NoWrap" ><xsl:value-of select="@ContactName"/></span></TD>
    </TR>
    <TR unselectable="on" bgcolor="#FDF5E6">
      <TD unselectable="on" class="boldtext">City:</TD>
      <TD unselectable="on" colspan="3"><span class="NoWrap" ><xsl:value-of select="@AddressCity"/></span></TD>
      <TD unselectable="on" class="boldtext">Phone:</TD>
      <TD unselectable="on">
        <xsl:value-of select="@PhoneAreaCode"/>
  			<xsl:text>-</xsl:text>
  			<xsl:value-of select="@PhoneExchangeNumber"/>
  			<xsl:text>-</xsl:text>
  			<xsl:value-of select="@PhoneUnitNumber"/>
  			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  			<xsl:value-of select="@PhoneExtensionNumber"/>
      </TD>
    </TR>
    <TR unselectable="on" bgcolor="#FFFFEE">
      <TD unselectable="on" class="boldtext">State:</TD>
      <TD unselectable="on"><xsl:value-of select="@AddressState"/></TD>
      <TD unselectable="on" class="boldtext" nowrap="nowrap">ZIP:</TD>
      <TD unselectable="on"><xsl:value-of select="@AddressZip"/></TD>
      <TD unselectable="on" class="boldtext">Fax:</TD>
      <TD unselectable="on">
        <xsl:value-of select="@FaxAreaCode"/>
      	<xsl:text>-</xsl:text>
      	<xsl:value-of select="@FaxExchangeNumber"/>
      	<xsl:text>-</xsl:text>
      	<xsl:value-of select="@FaxUnitNumber"/>
      	<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      	<xsl:value-of select="@FaxExtensionNumber"/>
      </TD>
    </TR>
    <TR unselectable="on" bgcolor="#FDF5E6">
      <TD unselectable="on" class="boldtext">County:</TD>
      <TD unselectable="on" colspan="3">
        <xsl:choose>
          <xsl:when test="@AddressCounty=''" ><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:when>
          <xsl:otherwise><xsl:value-of select="@AddressCounty"/></xsl:otherwise>
        </xsl:choose> 
      </TD>
      <TD unselectable="on" class="boldtext">E-mail:</TD>
      <TD unselectable="on"><span class="NoWrap" ><a><xsl:attribute name="href">mailto:<xsl:value-of select="@EmailAddress"/></xsl:attribute><xsl:value-of select="@EmailAddress"/></a></span></TD>
    </TR>
  </TABLE>
</DIV>
	
</xsl:template>

</xsl:stylesheet>