<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTMain">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="Environment"/>
<xsl:param name="PageID"/>
<xsl:param name="EntityID"/>

<xsl:template match="/Root">

<HTML>
<!--            APP _PATH                            SessionKey                   USERID             SP                          XSL             PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMShopSearchReferenceGetListXML,SMTMain.xsl   -->

<HEAD>
  <TITLE>Shop Maintenance</TITLE>

  <!-- STYLES -->
<LINK rel="stylesheet" href="../css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="../css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  

<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,29);
		  event.returnValue=false;
		  };	
 </script>
 
<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
var gsPageID = '<xsl:value-of select="$PageID"/>';
var gsFramePageFile = "SMTMain.asp"
var gsPageFile = "SMTMain.asp";
var gsSearchingIntervalID;
if (gsPageID == "SMT" || gsPageID == "WebSignups") top.layerEntityData.style.visibility = "hidden";

<![CDATA[
//init the table selections, must be last
function initPage(){
  top.gsLoc = window.location.toString().slice(16);
  if (gsPageID == "SMT" || gsPageID == "WebSignups") top.gsFramePageFile = gsFramePageFile;
  else top.gsPageFile= gsPageFile;
  top.gsPageID = gsPageID;
  
  switch(gsPageID){
    case "SMT":
      top.SwitchMenuSearchItem("main",3);
	  top.gsPageID ="SMT";
	  tabsIntabs[0].tabs.onchange=tabChange;
      tabsIntabs[0].tabs.onBeforechange=tabBeforeChange;
      break;
    case "WebSignups":
      top.SwitchMenuSearchItem("web", 3);
	  top.gsPageID ="WebSignups";
	  tabsIntabs[0].tabs.onchange=tabChange;
      tabsIntabs[0].tabs.onBeforechange=tabBeforeChange;
      break;
    case "DealerAddBusiness":
    case "BusinessIncludeShop":
      parent.tab13.className = "tabActive1";
      parent.btnDelete.style.visibility = "hidden"; 
      break;
  } 
   
    txtEntityID.value = top.gsID;
	txtFEIN.value = top.gsSSN;
	txtName.value = top.gsName;
	txtCity.value = top.gsCity;
	selState.value = top.gsState;
    txtZip.value = top.gsZip;
	txtPhoneAreaCode.value = top.gsPhoneArea;
	txtPhoneExchange.value = top.gsPhoneExchange;
	txtPhoneUnit.value = top.gsPhoneUnit;
  
  if (gsPageID == "WebSignups")
    selShopType.selectedIndex = top.gsProgramShopIndexWeb;
  else if (gsPageID != "DealerAddBusiness")
    selShopType.selectedIndex = top.gsProgramShopIndexSMT;
  
	if (gsPageID != "DealerAddBusiness") 
	selSpecialties.value = top.gsSpecialty;
  
	if (gsPageID == "SMT"){
    switch (top.gsSearchType){
  		case "S":
  			rdoShop.checked = true;
  			txtSearchType.value = "S";
  			break;
  		case "B":
  			rdoBusiness.checked = true;
  			txtSearchType.value = "B";
  			break;
  		case "D":
  			rdoDealer.checked = true;
  			txtSearchType.value = "D";
        SetRelevantControls(top.gsSearchType)
  			break;
    }
	}
}


function Search(){
	if (isNaN(txtEntityID.value)){
		top.ClientWarning("Please enter a numeric value for 'ID'.");
		txtEntityID.select();
		return;
	}
 
  btnSearch.disabled = true;
  
	if (gsPageID == "SMT"){
  	rdoShop.disabled = true;
  	rdoBusiness.disabled = true;
  	rdoDealer.disabled = true;
  }

	document.frames["ifrmShopList"].frameElement.style.visibility = "hidden";
	SearchIndicator();

	// save search parameters
	top.gsID = txtEntityID.value;
	top.gsSSN = txtFEIN.value;
	top.gsName = txtName.value;
	top.gsCity = txtCity.value;
	top.gsState = selState.value;
    top.gsZip = txtZip.value;
	top.gsPhoneArea = txtPhoneAreaCode.value;
	top.gsPhoneExchange = txtPhoneExchange.value;
	top.gsPhoneUnit = txtPhoneUnit.value;
	if (gsPageID != "DealerAddBusiness"){
    top.gsProgramShop = selShopType.value;
    if (gsPageID == "WebSignups")
      top.gsProgramShopIndexWeb = selShopType.selectedIndex;
    else
      top.gsProgramShopIndexSMT = selShopType.selectedIndex;
    }
	top.gsSpecialty = selSpecialties.value;
	top.gsSearchType = txtSearchType.value;

	var val;
	var qString = "";
	var aInputs = document.all.tags("INPUT");
  for (var i=0; i < aInputs.length; i++) {
		var sType = aInputs[i].type;
    if (sType == "text" || sType == "hidden"){
		  val = escape(aInputs[i].value.replace(/'/g, "''"));
      qString += aInputs[i].name + "=" + val.substr(0, aInputs[i].maxLength) + "&";
		}
  }
	aInputs = document.all.tags("SELECT");
  for (var i=0; i < aInputs.length; i++) {
		qString += aInputs[i].name + "=" + aInputs[i].value + "&";
	}
	qString = qString.slice(0, qString.length - 1);
	btnClear.disabled = true;
   
  switch(gsPageID){
    case "SMT":
      document.frames["ifrmShopList"].frameElement.src = "SMTEntitySearchResults.asp?" + qString;
      break;
    case "DealerAddBusiness":
    case "BusinessIncludeShop":
      document.frames["ifrmShopList"].frameElement.src = "SMTBusinessSearchResults.asp?" + qString + "&PageID=" + gsPageID; 
      break;
    case "WebSignups":
      document.frames["ifrmShopList"].frameElement.src = "SMTWebSignupSearchResults.asp?" + qString + "&PageID=" + gsPageID; 
      break;
  }  
}

function IsDirty(){
  Save();
}

function Save(){
	ifrmShopList.Save();
}

function SearchIndicator(){
	lblCount.style.display = "none";
	lblSearching.style.display = "inline";
	lblSearching.innerText = "Searching";
	gsSearchingIntervalID = window.setInterval("if (lblSearching.innerText == 'Searching.......') lblSearching.innerText = 'Searching'; lblSearching.innerText += '.'", 750);
}

function NumbersOnly(event){
	if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;
}

function ShowCount(count){
	var sCountMsg = count;

	switch (txtSearchType.value){
		case "S":
			sCountMsg += " Shop";
			break;
		case "B":
			sCountMsg += " Business";
			break;
		case "D":
			sCountMsg += " Dealer";
			break;
	}

	if (count == '0' || count > '1') sCountMsg += txtSearchType.value == "B" ? "es" : "s";
	sCountMsg += " Found";
	lblCount.innerText = sCountMsg;
	lblSearching.style.display = "none";
	lblCount.style.display = "inline";
	window.clearInterval(top.gsSearchingIntervalID);

	if (count > 0) document.frames["ifrmShopList"].frameElement.style.visibility = "visible";

	btnClear.disabled = false;
	btnSearch.disabled = false;
  
  if (gsPageID == "SMT"){
  	rdoShop.disabled = false;
  	rdoBusiness.disabled = false;
  	rdoDealer.disabled = false;
  }
}

function Clear(){
	txtEntityID.value = "";
	txtFEIN.value = "";
	txtName.value = "";
	txtCity.value = "";
	selState.selectedIndex = 0;
	txtPhoneAreaCode.value = "";
	txtPhoneExchange.value = "";
	txtPhoneUnit.value = "";
    txtZip.value = "";
	selShopType.selectedIndex = 0;
	selSpecialties.selectedIndex = 0;
	rdoShop.click();
	lblSearching.style.display = "none";
	lblCount.style.display = "none";
	lblSorting.style.display = "none";
}

function SearchTypeChange(sType){
  txtSearchType.value = sType;
  if (sType == "D"){
    SetRelevantControls(sType);
  }
  else{
    SetRelevantControls(sType);
  }    
}

  function tabBeforeChange(obj, tab)
    {
        if (tab.id == "tab12"){
            var obj;
            obj = document.getElementById("ifrmDistanceSearch");
            obj.style.visibility = "visible";
            obj.style.display = "inline";
            obj.src = "SMTDistanceSearch.asp";
			// to clear the previous search list 
			obj = document.getElementById("ifrmShopList");
 		    obj.style.visibility = "visible";
			obj.src="blank.asp"
			obj.style.display="inline";
			lblSearching.style.display = "none";
   	        lblCount.style.display = "none";
	        lblSorting.style.display = "none";
			
        }
        else {
            var obj;
            obj = document.getElementById("ifrmDistanceSearch");
            obj.style.visibility = "hidden";
            obj.style.display = "inline";
			// to clear the previous search list
         	obj = document.getElementById("ifrmShopList");
			obj.src="blank.asp"
			obj.style.visibility = "hidden";
			obj.style.display="inline";
			
        }
    }
    


function SetRelevantControls(sType){
  var bDisabled = sType == "D" ? true : false;
  var sColor = sType == "D" ? "gray" : "black";

  selSpecialties.disabled = bDisabled;
  selShopType.disabled = bDisabled;
  txtFEIN.disabled = bDisabled;
  tdSpecialty.style.color = sColor;
  tdProgramShop.style.color = sColor;
  tdSSN.style.color = sColor;
}

if (document.attachEvent)
    document.attachEvent("onclick", top.hideAllMenuScriptlets);
  else
    document.onclick = top.hideAllMenuScriptlets;


]]>

</SCRIPT>
</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" style="margin:0px;padding:0px;overflow:hidden;background:transparent;"  onLoad="tabInit(false,'#FFFFFF'); initPage();"  tabIndex="-1"   onkeypress="if (event.keyCode == 13) Search();"> 

<!-- Start Tabs -->
<DIV unselectable="on" style="position:absolute; visibility: visible; top: 0px; left: 0px;width:100%;height:100%;">

  <DIV unselectable="on" id="tabs1" class="apdtabs">
  	<SPAN unselectable="on" id="tab11" class="tab1">SMT Main Search</SPAN>
  	<SPAN unselectable="on" id="tab12" class="tab1">Shop Distance Search</SPAN>
  </DIV>
  
  <DIV unselectable="on" class="content1" id="content11" name="SMTMainSearch" Many="true" style="visibility:visible; width:100%; height:100%">
   <xsl:call-template name="SMTMainSearch">
   </xsl:call-template>
 </DIV>  
   
  <DIV unselectable="on" class="content1" id="content12" name="ShopDistanceSearch" Many="true" CRUD="" style="visibility:hidden; width:100%; height:100%;">
     <IFRAME id="ifrmDistanceSearch" src="/blank.asp" style='width:100%; height:100%; border:0px; visibility:inherit; position:absolute; background:transparent; overflow:hidden' allowtransparency='true' ></IFRAME>
   </DIV> 
   
</DIV>

<IFRAME id="ifrmShopList" src="../blank.asp" allowtransparency="true">
  <xsl:attribute name="style">
    position:relative; top:170px; border:1px; left:5px; visibility:hidden; background:transparent; overflow:hidden;
    <xsl:choose>
      <xsl:when test="$PageID='SMT' or $PageID='WebSignups'">width:770; height:390;</xsl:when>
      <xsl:when test="$PageID='DealerAddBusiness' or $PageID='BusinessIncludeShop'">width:720; height:350;</xsl:when>
    </xsl:choose>
  </xsl:attribute>
</IFRAME>

</BODY>
</HTML>

</xsl:template>


<!-- Gets the Main SMT Search Screen -->
  <xsl:template name="SMTMainSearch" >
  
  <xsl:if test="$PageID='WebSignups'">
     <div style="position:absolute; top:5; left:330; color:darkblue;">Web Signups
	 </div>
  </xsl:if>

  <input type="hidden" id="txtCallBack" name="CallBack"/>

  <TABLE unselectable="on" class="ShopAssignTableSrh" border="0" cellspacing="0" cellpadding="2">
  <xsl:attribute name="width">
    <xsl:choose>
      <xsl:when test="$PageID='SMT' or $PageID='WebSignups'">750</xsl:when>
      <xsl:when test="$PageID='DealerAddBusiness' or $PageID='BusinessIncludeShop'">718</xsl:when>
    </xsl:choose>
  </xsl:attribute>
  <tr>
    <td valign="top">
      <table border="0" cellspacing="0" cellpadding="1" width="100%">
        <colgroup>
          <col width="100"/>
          <col width="100%"/>
        </colgroup>
        <xsl:choose>
          <xsl:when test="$PageID='SMT'">
            <tr>
              <td style="font-weight:bold;">Search Type:</td>
              <td align="left">
      		   	  <input type="radio" id="rdoShop" name="type" checked="checked" onclick="SearchTypeChange('S')"/>Shop
                <img src="/images/spacer.gif" border="0" height="2" width="5"/>
      			    <input type="radio" id="rdoBusiness" name="type" onclick="SearchTypeChange('B')"/>Business
                <img src="/images/spacer.gif" border="0" height="2" width="5"/>
                <input type="radio" id="rdoDealer" name="type" onclick="SearchTypeChange('D')"/>Dealer
              </td>
            </tr>
          </xsl:when>
          <xsl:when test="$PageID='DealerAddBusiness'">
            <input type="hidden" name="ExclusionEntityID"><xsl:attribute name="value"><xsl:value-of select="$EntityID"/></xsl:attribute></input>
            <input type="hidden" name="ExclusionType" value="D"/>
          </xsl:when>  
          <xsl:when test="$PageID='BusinessIncludeShop'">
            <input type="hidden" name="ExclusionEntityID"><xsl:attribute name="value"><xsl:value-of select="$EntityID"/></xsl:attribute></input>
            <input type="hidden" name="ExclusionType" value="S"/>
          </xsl:when>          
        </xsl:choose>    
        <tr>
          <td nowrap="nowrap"><xsl:if test="$PageID='WebSignups'">Shop Load </xsl:if>ID:</td>
          <td nowrap="nowrap">
            <input type="hidden" id="txtSearchType" name="SearchType" maxLength="1">
              <xsl:attribute name="value">
                <xsl:choose>
                  <xsl:when test="$PageID='SMT'">S</xsl:when>
                  <xsl:when test="$PageID='DealerAddBusiness'">B</xsl:when>
                  <xsl:when test="$PageID='BusinessIncludeShop'">S</xsl:when>
                  <xsl:when test="$PageID='WebSignups'">S</xsl:when>
                </xsl:choose>
              </xsl:attribute>
            </input>
            <input type="text" id="txtEntityID" size="9" maxLength="8" class="inputFld" onkeypress="NumbersOnly(window.event)">
              <xsl:attribute name="name">
                <xsl:choose>
                  <xsl:when test="$PageID='WebSignups'">ShopLoadID</xsl:when>
                  <xsl:otherwise>EntityID</xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
            </input>
          </td>
        </tr>
        <tr>
          <td nowrap="nowrap">Name:</td>
          <td nowrap="nowrap">
            <input type="text" class="inputFld" name="Name" id="txtName" size="51" maxLength="50"/>
          </td>
        </tr>
        <tr>
          <td nowrap="nowrap">City:</td>
          <td nowrap="nowrap">
            <input type="text" class="inputFld" name="AddressCity" id="txtCity" size="51" maxLength="30"/>
          </td>
        </tr>
        <tr>
          <td nowrap="nowrap">State:</td>
          <td nowrap="nowrap">
            <select name="AddressState" id="selState" class="inputFld" style="width:146;">
              <option value=""></option>
      			  <xsl:for-each select="Reference[@ListName='State']">
      				  <xsl:call-template name="BuildSelectOptions"/>
      			  </xsl:for-each>
            </select>
            <img src="/images/spacer.gif" border="0" height="2" width="45"/>
            Zip:
            <img src="/images/spacer.gif" border="0" height="2" width="10"/>
            <input type="text" class="inputFld" name="AddressZip" id="txtZip" size="6" maxLength="5" onkeypress="NumbersOnly(window.event)"/>
          </td>
        </tr>
        <tr>
          <td nowrap="nowrap">Phone:</td>
          <td nowrap="nowrap">
            <input type="text" class="inputFld" name="PhoneAreaCode" id="txtPhoneAreaCode" size="2" maxLength="3" onkeypress="NumbersOnly(window.event)"/> -
            <input type="text" class="inputFld" name="PhoneExchange" id="txtPhoneExchange" size="2" maxLength="3" onkeypress="NumbersOnly(window.event)"/> -
            <input type="text" class="inputFld" name="PhoneUnit" id="txtPhoneUnit" size="3" maxLength="4" onkeypress="NumbersOnly(window.event)"/>
          </td>
        </tr>
      </table>
    </td>
    <td width="10px"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    <td valign="top">
      <table border="0" width="100%">
        <tr>
          <td id="tdSSN">SSN/EIN:</td>
          <td nowrap="nowrap">
            <input type="text" class="inputFld" name="FedTaxID" id="txtFEIN" size="16" maxLength="9" onkeypress="NumbersOnly(window.event)"/>
          </td>
        </tr>
        <tr>
          <td id="tdProgramShop">
            <xsl:if test="$PageID='DealerAddBusiness'"><xsl:attribute name="style">color:gray</xsl:attribute></xsl:if>Shop Type:
          </td>
          <td>
            <select name="ProgramFlag" id="selShopType" class="inputFld" style="width:210;">
              <xsl:if test="$PageID='DealerAddBusiness'"><xsl:attribute name="disabled"/></xsl:if>
              <xsl:for-each select="Reference[@ListName='ShopType']">
        				<xsl:call-template name="BuildSelectOptions"><xsl:with-param name="current">1</xsl:with-param></xsl:call-template>
      			  </xsl:for-each>
            </select>
          </td>
        </tr>
        <tr>
          <td id="tdSpecialty">
            <xsl:if test="$PageID='WebSignups'"><xsl:attribute name="style">color:gray</xsl:attribute></xsl:if>Specialty:
          </td>
          <td>
            <select id="selSpecialties" name="SpecialtyID" class="inputFld" style="width:210;">
              <xsl:if test="$PageID='WebSignups'"><xsl:attribute name="disabled"/></xsl:if>
              <option value=""></option>
    			    <xsl:for-each select="Reference[@ListName='Specialty']">
      				  <xsl:call-template name="BuildSelectOptions"/>
    			    </xsl:for-each>
            </select>
          </td>
        </tr>

		<!--<xsl:attribute name="value">-->
			<xsl:choose>
				<xsl:when test="$PageID='WebSignups'">
					<tr>
						<td nowrap="nowrap">Certified First ID:</td>
						<td nowrap="nowrap">
						  <input type="text" class="inputFld" name="CertifiedFirstID" id="txtCertifiedFirstID" size="25" maxLength="50"/>
						</td>
					  </tr>
					  <tr>
							<td><img src="/images/spacer.gif" border="0" height="20px" width="1px"/></td>
					  </tr>
				</xsl:when>
				<xsl:otherwise>
					<tr>
					  <td nowrap="nowrap">Program Key:</td>
					  <td nowrap="nowrap">
							D = DRP; R = RRP; C = CEI; N/A = Non-Program
					  </td>
					</tr>
					<tr>
						<td><img src="/images/spacer.gif" border="0" height="20px" width="1px"/></td>
				  </tr>
				</xsl:otherwise>
			</xsl:choose>
		<!--</xsl:attribute>        -->
   
	    </table>
	  </td>
  </tr>
 </TABLE>

 <div>
  <xsl:attribute name="style">
    <xsl:choose>
      <xsl:when test="$PageID='SMT'">position:absolute; top:120px; left:555px;</xsl:when>
      <xsl:when test="$PageID='WebSignups'">position:absolute; top:105px; left:555px;</xsl:when>
      <xsl:when test="$PageID='DealerAddBusiness' or $PageID='BusinessIncludeShop'">position:absolute; top:98px; left:525px;</xsl:when>
    </xsl:choose>
  </xsl:attribute>
  <input type="button" id="btnClear" value="Clear" class="formButton" onclick="Clear()" style="width:80"/>
  <input type="button" id="btnSearch" value="Search" class="formButton" onclick="Search()" style="width:80"/>
 </div>

 <div>
  <xsl:attribute name="style">
    <xsl:choose>
      <xsl:when test="$PageID='SMT'">position:absolute; top:124px; left: 333px; height:12px: width; 80px;</xsl:when>
      <xsl:when test="$PageID='WebSignups'">position:absolute; top:112px; left: 333px; height:12px: width; 80px;</xsl:when>
      <xsl:when test="$PageID='DealerAddBusiness' or $PageID='BusinessIncludeShop'">position:absolute; top:104px; left: 333px; height:12px: width; 80px;</xsl:when>
    </xsl:choose>
  </xsl:attribute>
  <label id="lblCount" style="color:blue; font-weight:600; display:none"></label>
  <label id="lblSearching" style="color:blue; font-weight:600; display:none"></label>
  <label id="lblSorting" style="color:blue; font-weight:600; display:none"></label>
</div>
</xsl:template>


<xsl:template name="BuildSelectOptions">
	<xsl:param name="current"/>
	<option>
		<xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>

		<xsl:if test="@ReferenceID = $current">
			<xsl:attribute name="selected"/>
		</xsl:if>

		<xsl:value-of select="@Name"/>
	</option>
</xsl:template>

</xsl:stylesheet>



