<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimVehicle">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="VehicleCRUD" select="Vehicle"/>
<xsl:param name="InvolvedCRUD" select="Involved"/>
<xsl:param name="EstimateCRUD" select="Estimate"/>


<xsl:param name="ReopenExpCRUD" select="Action:Reopen Exposure"/>
<xsl:param name="CloseExpCRUD" select="Action:Close Exposure"/>
<xsl:param name="TransferClaimCRUD" select="Transfer Claim Exposure"/>
<xsl:param name="ReassignExpCRUD" select="Reassign Exposure"/>
<xsl:param name="CancelExpCRUD" select="Cancel Exposure"/>
<xsl:param name="VoidExpCRUD" select="Void Exposure"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserId"/>
<xsl:param name="WindowID"/>
<xsl:param name="ImageRootDir"/>

<xsl:template match="/Root">

<xsl:variable name="Context" select="session:XslUpdateSession('Context',string(/Root/@Context))"/>
<xsl:variable name="InsuranceCompanyID" select="session:XslGetSession('InsuranceCompanyID')"/>
<xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>
<xsl:variable name="LynxID" select="session:XslGetSession('LynxID')"/>
<xsl:variable name="ClaimAspectID" select="session:XslGetSession('ClaimAspectID')"/>
<xsl:variable name="ClaimAspectIDClaim" select="session:XslGetSession('Claim_ClaimAspectID')"/>

<xsl:variable name="VehicleNumber" select="/Root/@VehicleNumber" />
<xsl:variable name="PertainsTo" select="/Root/@Context" />
<xsl:variable name="ContextSupported" select="/Root/@ContextSupportedFlag" />
<xsl:variable name="VehicleLastUpdatedDate" select="/Root/Vehicle/@SysLastUpdatedDate"/>
<xsl:variable name="ClaimAspectLastUpdatedDate" select="/Root/Vehicle/@ClaimAspectSysLastUpdatedDate"/>
<!-- <xsl:variable name="pageReadOnly" select="string(boolean($ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'))"/> -->
<xsl:variable name="pageReadOnly" select="string(boolean(/Root/Vehicle/@Status='Vehicle Cancelled' or /Root/Vehicle/@Status='Vehicle Voided'))"/>
<xsl:variable name="ExposureCD" select="Vehicle/@ExposureCD"/>
<xsl:variable name="gsParty" select="/Root/Reference[@List='Exposure' and @ReferenceID=$ExposureCD]/@Name"/>
<xsl:variable name="rtCo" select="/Root/Vehicle/@ClientCoverageTypeID"/>
<!-- <xsl:if test="$ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'"> -->
<xsl:if test="/Root/Vehicle/@Status='Vehicle Cancelled' or /Root/Vehicle/@Status='Vehicle Voided'">
  <xsl:value-of select="js:setPageDisabled()"/>
</xsl:if>

<xsl:value-of select="js:SetCRUD( string($VehicleCRUD) )"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspClaimVehicleGetDetailXML,ClaimVehicle.xsl,4253,145   -->

<HEAD>
<TITLE>Vehicle Details</TITLE>

<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/CoolSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>

<STYLE type="text/css">
  A {
  	color: #000000;
  	text-decoration: none;
  }
  .setImgBorder {
  	border: 1px solid #008080;
  }
</STYLE>

<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/showimage.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,33);
		  event.returnValue=false;
		  };	
</script>
<SCRIPT language="JavaScript">

	var gsVehicleCRUD = '<xsl:value-of select="$VehicleCRUD"/>';
	var gsInvolvedCRUD = '<xsl:value-of select="$InvolvedCRUD"/>';
	var gsEstimateCRUD = '<xsl:value-of select="$EstimateCRUD"/>';

  //CRUDs
  var sReopenExpCRUD = "<xsl:value-of select="$ReopenExpCRUD"/>";
  var sCloseExpCRUD = "<xsl:value-of select="$CloseExpCRUD"/>";
  var sReassignExpCRUD = "<xsl:value-of select="$ReassignExpCRUD"/>";
  var sCancelExpCRUD = "<xsl:value-of select="$CancelExpCRUD"/>";
  var sVoidExpCRUD = "<xsl:value-of select="$VoidExpCRUD"/>";

  var gsLynxID = '<xsl:value-of select="$LynxID"/>';
  var gsWindowID = '<xsl:value-of select="$WindowID"/>';

  //var gsVehNum = '<xsl:value-of select="$VehicleNumber"/>';
  var gsVehNum = parent.gsVehNum;

  var gsUserID = '<xsl:value-of select="$UserId"/>';
  var gsInsuranceCompanyID = '<xsl:value-of select="$InsuranceCompanyID"/>';
  var gsVehicleLastUpdatedDate = '<xsl:value-of select="$VehicleLastUpdatedDate"/>';
  var gsClaimAspectLastUpdatedDate = '<xsl:value-of select="$ClaimAspectLastUpdatedDate"/>';
  var gsSaftyDeviceCount = '<xsl:value-of select="count(/Root/Reference[@List='SafetyDevice'])"/>';
	var gsImageRootDir = '<xsl:value-of select="$ImageRootDir"/>';
  var gsDocPath;
  var gsClaimAspectID = '<xsl:value-of select="@ClaimAspectID"/>';
  var gsClaimAspectIDClaim = '<xsl:value-of select="$ClaimAspectIDClaim"/>';
  var gsCountNotes = '<xsl:value-of select="@CountNotes"/>';
  var gsCountTasks = '<xsl:value-of select="@CountTasks"/>';
  var gsCountBilling = '<xsl:value-of select="@CountBilling"/>';
  var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";
  var bPageReadOnly = "<xsl:value-of select="$pageReadOnly"/>";
  var gsClaimAspectID = "<xsl:value-of select="/Root/Vehicle/@ClaimAspectID"/>";
  var gsClientCoverageTypeID ="<xsl:value-of select ="/Root/Vehicle/@ClientCoverageTypeID"/>";
  var gsParty = "<xsl:value-of select="/Root/Reference[@List='Exposure' and @ReferenceID=$ExposureCD]/@Name"/>";
  var giInvInsuredCount = "<xsl:value-of select="count(/Root/Vehicle/Involved/InvolvedType[@InvolvedTypeName = 'Insured'])"/>";
  var giInvVehicleClaimantCount = "<xsl:value-of select="count(/Root/Vehicle/Involved/InvolvedType[@InvolvedTypeName = 'Claimant'])"/>";
  var gsInitialCoverageValue ="<xsl:value-of  select="concat(string(/Root/Vehicle/CoverageType[@ClientCoverageTypeID=$rtCo]/@Name),'(',string(/Root/Vehicle/CoverageType[@ClientCoverageTypeID=$rtCo]/@CoverageProfileCD),')')"/>";
  var profileCDValue;
  var sStatus = "<xsl:value-of select="/Root/Vehicle/@Status"/>";
  var sReadOnly = "<xsl:value-of select="$pageReadOnly"/>";
  var bRepairStartConfirmFlag = "<xsl:value-of select="/Root/Vehicle/@RepairStartConfirmFlag"/>";
  var bRepairEndConfirmFlag = "<xsl:value-of select="/Root/Vehicle/@RepairEndConfirmFlag"/>";

  var sRepairStartDate = "";
  var sRepairEndDate = "";
  
<![CDATA[

  // Tooltip Variables to set:
  messages= new Array()
  // Write your descriptions in here.
  messages[0]="Double-click to Expand"
  messages[1]="Double-click to Close"

  // button images for ADD/DELETE/SAVE
  preload('buttonAdd','/images/but_ADD_norm.png')
  preload('buttonAddDown','/images/but_ADD_down.png')
  preload('buttonAddOver','/images/but_ADD_over.png')
  preload('buttonDel','/images/but_DEL_norm.png')
  preload('buttonDelDown','/images/but_DEL_down.png')
  preload('buttonDelOver','/images/but_DEL_over.png')
  preload('buttonSave','/images/but_SAVE_norm.png')
  preload('buttonSaveDown','/images/but_SAVE_down.png')
  preload('buttonSaveOver','/images/but_SAVE_over.png')

  // Select changes to update dirty flag and changed border
  function onSelectChange(selobj)
  {
    gbDirtyFlag = true;
    // check to make sure if in a collection
    if (selobj[0]) selobj = selobj[0];
    selobj.children[0].style.borderColor='#805050';
  }
  
  function InitializeCoverageType()
  {
   var ClientCoverageSelectElement = document.getElementById('selClientCoverageTypeID');
   int_optionselect(ClientCoverageSelectElement,gsInitialCoverageValue);
   copySelected(ClientCoverageSelectElement); 
   var c11 = document.getElementById("content11");
   resetSelectBoxBorder(c11);
  // Initialize CoverageProfileCD 
   var ClientCoverage = document.getElementById("ClientCoverageTypeID");
   var CoverageIDSelectedText = findSelectedStateText(ClientCoverageSelectElement,ClientCoverage.value);
   var pIndex = CoverageIDSelectedText.indexOf("(")+1;
   var plIndex = CoverageIDSelectedText.indexOf(")");
   
   profileCDValue = CoverageIDSelectedText.substring(pIndex,plIndex); 
  }
  
  
  function OnSelectCoverageType()
  {
  var ClientCoverage = document.getElementById("ClientCoverageTypeID");
  var ClientCoverageSelectElement = document.getElementById('selClientCoverageTypeID');
  var CoverageIDSelectedText = findSelectedStateText(ClientCoverageSelectElement,ClientCoverage.value);
  
  onSelectChange(ClientCoverageSelectElement);
  int_optionselect(ClientCoverageSelectElement,CoverageIDSelectedText);
  copySelected(ClientCoverageSelectElement); 
  
  var pIndex = CoverageIDSelectedText.indexOf("(")+1;
  var plIndex = CoverageIDSelectedText.indexOf(")");
  profileCDValue = CoverageIDSelectedText.substring(pIndex,plIndex);
  //move this to under ADS_Pressed, selected value should not be reflected into the database unless it is SAVED
  //document.getElementById("CoverageProfileCD").value = profileCDValue; 
  }
  
   
   // Involved selection box change
  var inInvolvedChange = false;
  function onInvolvedChange(selobj)
  {
    if (inInvolvedChange == true) return;
    inInvolvedChange = true;
		if (document.frames["ifrmVehicleInvolved"].gbDirtyFlag == true && gbDirtyFlag == true) {
  		var sSave = YesNoMessage("Need To Save", "The information in VehicleInvolved has changed!  Do you want to save?");
  		if (sSave == "Yes")
        {
			document.frames["ifrmVehicleInvolved"].ADS_Pressed("Update", "content22");
  		}
    }
    
    //check if in insert mode and if so ignore change
    if (gbInsertMode == true)
		{
			setTimeout(selFixUp,200);
			inInvolvedChange = false;
		 	return;
		}

    if (selobj == null)
    {
      selobj = document.getElementById("selVehicleInvolved");
      var idxLength = selobj.options.length;
      for (var idx=0; idx < idxLength; idx++)
      {
        var str = selobj.options[idx].innerText;
        if (str.indexOf("Insured") != -1)
          break;
      }
      if (idx < idxLength)
        selobj.optionselect(null, idx);
    }

    var sInvolved = selobj.options[selobj.selectedIndex].value;
    var aryInvolved = sInvolved.split(",");
    var involvedID = aryInvolved[0];

    if (involvedID > 0)
      document.frames["ifrmVehicleInvolved"].frameElement.src = "VehicleInvolved.asp?WindowID=" + gsWindowID + "&InvolvedID=" + involvedID  + "&pageReadOnly=" + bPageReadOnly;
    else
      document.frames["ifrmVehicleInvolved"].frameElement.src = "blank.asp";

    inInvolvedChange = false;
  }
  

  function selFixUp()
  {
	selobj = document.getElementById("selVehicleInvolved");
	selobj.children[0].rows[0].cells[0].innerHTML = "<B>** New **</B>";
  }


  function onShopAssignment()
  {
     document.frames["ifrmShopAssignment"].frameElement.src = "VehicleShopAssignment.asp?WindowID=" + gsWindowID + "&InsuranceCompanyID=" + gsInsuranceCompanyID + "&ClaimAspectID=" + gsClaimAspectID;
  }

  // Page Initialize
  function PageInit()
  {
    tabsIntabs[0].tabs.onchange=tabChange;
    tabsIntabs[1].tabs.onchange=tabChange;
    tabsIntabs[0].tabs.onBeforechange=tabBeforeChange;
    tabsIntabs[1].tabs.onBeforechange=tabBeforeChange;
    
    //set the current vehicles claim aspect id to the top window. Used by the 
    // Vehicle Reassign.
    top.sVehicleClaimAspectID = gsClaimAspectID;
    top.sClaim_ClaimAspectID = gsClaimAspectIDClaim;
 
    InitializeCoverageType();
	if(document.all.selClientCoverageTypeID)
	    document.all.selClientCoverageTypeID.attachEvent("onblur",OnSelectCoverageType);

    /*if (gsCountNotes == "0" && gsCountTasks == "0" && gsCountBilling == "0" && gsVehicleCRUD.indexOf("D") != -1)
      top.VehiclesMenu(true,3);
    else
      top.VehiclesMenu(false,3);*/

    if (sStatus == "Open") {
      /*//Add vehicle
      if (gsVehicleCRUD.indexOf("C") != -1)
        top.VehiclesMenu(true, 2);
      //remove vehicle
      if (gsCountNotes == "0" && gsCountTasks == "0" && gsCountBilling == "0" && gsVehicleCRUD.indexOf("D") != -1)
        top.VehiclesMenu(true,3);
      else
        top.VehiclesMenu(false,3);
      
      //Reassign vehicle
      if (sReassignExpCRUD.indexOf("U") != -1)
        top.VehiclesMenu(true, 5);
        
      //Close vehicle
      if (sCloseExpCRUD.indexOf("U") != -1)
        top.VehiclesMenu(true, 7);

      //disable reopen because the vehicle is already open
      top.VehiclesMenu(false, 8);

      //Cancel vehicle
      if (sCancelExpCRUD.indexOf("U") != -1)
        top.VehiclesMenu(true, 10);

      //Void vehicle
      if (sVoidExpCRUD.indexOf("U") != -1)
        top.VehiclesMenu(true, 11);
        
      */
      //Add
        //top.VehiclesMenu((gsVehicleCRUD.indexOf("C") != -1), 2);
        top.VehiclesMenu(false, 2);
      //remove vehicle
        top.VehiclesMenu((gsCountNotes == "0" && gsCountTasks == "0" && gsCountBilling == "0" && gsVehicleCRUD.indexOf("D") != -1),3);
      
      //Reassign vehicle
        top.VehiclesMenu((sReassignExpCRUD.indexOf("U") != -1), 5);
        
      //Close vehicle
        top.VehiclesMenu((sCloseExpCRUD.indexOf("U") != -1), 7);

      //disable reopen because the vehicle is already open
      top.VehiclesMenu(false, 8);

      //Cancel vehicle
        top.VehiclesMenu((sCancelExpCRUD.indexOf("U") != -1), 10);

      //Void vehicle
        top.VehiclesMenu((sVoidExpCRUD.indexOf("U") != -1), 11);      
    } else if (sStatus.indexOf("Closed") != -1) {
      top.VehiclesMenu(false, 7); // Close
      top.VehiclesMenu(true, 8); // Reopen
      top.VehiclesMenu(false, 10); // Cancel
      top.VehiclesMenu(false, 11); // Void
    } else if (sStatus.indexOf("Cancelled") != -1) {
      top.VehiclesMenu(false, 7); // Close
      top.VehiclesMenu(true, 8); // Reopen
      top.VehiclesMenu(false, 10); // Cancel
      top.VehiclesMenu(false, 11); // Void
    } else if (sStatus.indexOf("Voided") != -1) {
      top.VehiclesMenu(false, 5); // Reassign
      top.VehiclesMenu(false, 7); // Close
      top.VehiclesMenu(false, 8); // Reopen
      top.VehiclesMenu(false, 10); // Cancel
      top.VehiclesMenu(false, 11); // Void
    }      
    top.gsCountNotes = gsCountNotes;
    top.gsCountTasks = gsCountTasks;
    top.gsCountBilling = gsCountBilling;
    top.gsEntityCRUD = gsVehicleCRUD;

    top.gsInvInsuredCount = giInvInsuredCount;
    //top.giInvVehicleClaimantCount = giInvVehicleClaimantCount;
    if (top.gaVehClaimantCount){
      top.gaVehClaimantCount[gsVehNum] = giInvVehicleClaimantCount;
    }
    //this is to trap error messages when running stand-alone
    try
    {
		  window.setTimeout("onShopAssignment()", 500);
  	  window.setTimeout("onInvolvedChange()", 1000);
    }
    catch(e) {}

    if (txtDescYear)
      txtDescYear.onbeforedeactivate = CheckVehicleYear;

    //try {selExposureCD.disabled = true;} catch(e) {}
    top.setSB(100,top.sb);
    onpasteAttachEvents();
    attachPasteEvents(document.all.txtDescYear);
    attachPasteEvents(document.all.txtOdometer);
    attachPasteEvents(document.all.txtBookValue);
    attachPasteEvents(document.all.txtEstimatedSpeed);
    attachPasteEvents(document.all.txtPostedSpeed);
	  resizeScrollTable(document.getElementById("ESTScrollTable"));
    if (document.getElementById("btnRepairEndConfirm")) {
      if (txtRepairEndDate.value != "" && btnRepairEndConfirm){
        var dtRepairEnd = new Date(txtRepairEndDate.value);
        var dtToday = new Date();
        if (dtRepairEnd > dtToday)
          btnRepairEndConfirm.style.display = "none";
      } else if (txtRepairEndDate.value == "" ||txtRepairStartDate.value == "" || txtRepairStartDate.Disabled == true)
        btnRepairEndConfirm.style.display = "none";
    }

    if (document.getElementById("btnRepairStartConfirm")) {
      if (txtRepairStartDate.value != ""){
        var dtRepairStart = new Date(txtRepairStartDate.value);
        var dtToday = new Date();
        
        if (dtRepairStart > dtToday)
          btnRepairStartConfirm.style.display = "none";
      } else
        btnRepairStartConfirm.style.display = "none";
    }

    if (bRepairStartConfirmFlag == "1") {
      txtRepairStartDate.disabled = true;
      txtRepairStartDate.className = "InputReadonlyField";
    }

    if (bRepairEndConfirmFlag == "1") {
      txtRepairEndDate.disabled = true;
      txtRepairEndDate.className = "InputReadonlyField";
    }
    
    gbDirtyFlag = false;
  }

	
  function CheckVehicleYear(){
    if (txtDescYear.value.length > 0 && txtDescYear.value.length != 4) {
      ClientWarning("Vehicle year must be a 4 digit year. Please try again.");
      txtDescYear.focus();
      event.returnValue = false;
      event.cancelBubble = true;
    }
    
    var dtNow = new Date();
    var iNextYear = dtNow.getFullYear() + 1;
    
    if (txtDescYear.value < 1900 || txtDescYear.value > iNextYear){
      ClientWarning("Vehicle year must be between 1900 and " + iNextYear + ". Please try again.");
      txtDescYear.focus();
      event.returnValue = false;
      event.cancelBubble = true;
    }
  }

  var inADS_Pressed = false;    // re-entry flagging for buttons pressed
  var objDivWithButtons = null;

  // ADS Button pressed
  function ADS_Pressed(sAction, sName)
  {
    if (divWithButtons && divWithButtons.id == "content24") return; // Documents tab.  User could not have changed the information here.

    if (ValidatePhones(sName) == false) return;

    if (sName=="content11") 
	{
	   var RepairDateObj = document.getElementById("txtRepairEndDate"); 
       if( ValidateEndDate(RepairDateObj,txtRepairStartDate,txtRepairEndDate)== false) return;

      if (txtRepairStartDate.value != "" || txtRepairStartDate.value != "") {
        var dtOldStart = new Date(sRepairStartDate);
        var dtOldEnd = new Date(sRepairEndDate);
        var dtNewStart = new Date(txtRepairStartDate.value);
        var dtNewEnd = new Date(txtRepairEndDate.value);
        
        if ((sRepairStartDate != "" && dtNewStart.toString() != dtOldStart.toString()) ||
            (sRepairEndDate != "" && dtNewEnd.toString() != dtOldEnd.toString())) {
          if (txtRepairScheduleChangeReason.value == "") {
            var strReturn = window.showModalDialog("/RepairChangeReason.asp?ClaimAspectID=" + gsClaimAspectID, "","dialogHeight: 161px; dialogWidth: 332px; center: Yes; help: No; resizable: No; status: No; unadorned: Yes");
            txtRepairScheduleChangeReason.value = strReturn;
            if (ldTrim(strReturn) == "")
              return;
          }
        }
      }
	}
	
    if (sAction == "Update" && divWithButtons.id == "content21" && (typeof(ifrmShopAssignment.isDirty) == "function" && ifrmShopAssignment.isDirty() == true)) 
      //set the dirty flag so that user can save the assignment remarks. This is due to the mix of old control page 
      //interacting with the new control page.
      gbDirtyFlag = true; 
      
    if (sAction == "Update" && !gbDirtyFlag) return; //no change made to the page
    if (inADS_Pressed == true) return;
    inADS_Pressed = true;
    objDivWithButtons = divWithButtons;
    disableADS(objDivWithButtons, true);

    var oDiv = document.getElementById(sName)

    if (sAction == "Update")
    {
      if (oDiv.Many == "true")
      {
        var sType = oDiv.name;
        if (gbInsertMode == true) sAction = "Insert"
        var retArray = document.frames["ifrm"+sType].ADS_Pressed(sAction, sName);
        if (retArray[1] != "0")
        {
          if (sType != 'ShopAssignment')
          {
						//need to set mode to non insert so ** NEw does not get displayed again
						var org_mode = gbInsertMode;
						gbInsertMode = false;
                        UpdateSelects(sType, retArray[0]);
						gbInsertMode = org_mode;

            if (gbInsertMode == true)
            {
              oDiv.Count++;
              UpdateTabTextCount(oDiv);
            }
       
            selObj = document.getElementById(oDiv.id + "Sel");	
            selObj.style.borderColor = "#cccccc";
            gbInsertMode = false;
            if (oDiv.getAttribute("CRUD").indexOf("C") != -1)
              eval(sName+"_ADD.style.visibility='inherit'");
            if (oDiv.getAttribute("CRUD").indexOf("D") != -1)
              eval(sName+"_DEL.style.visibility='inherit'");
          }
        }
        else if (retArray[0] != "")
        {
          ClientInfo(retArray[0]);
        }
      }
      
      else if (sName == "content23")
      {
        var oDiv = document.getElementById(sName)
        var tblObj = document.getElementById("tbl"+oDiv.name);
        var idxLength = tblObj.rows.length;

        for (var idx=0; idx < idxLength; idx++)
        {
          if (tblObj.rows[idx].dirty && tblObj.rows[idx].dirty == true)
          {
            var sRequest = "";
            var oCells = tblObj.rows[idx].cells;
            var cellsLength = oCells.length;
            for (var x=0; x < cellsLength; x++)
            {
              if (oCells[x].xmlAttribute)
                if (x == 7)
                  sRequest += oCells[x].xmlAttribute +"=" + escape(ldTrim(oCells[x].innerText).charAt(0)) + "&";
                else
                  sRequest += oCells[x].xmlAttribute +"=" + escape(ldTrim(oCells[x].innerText)) + "&";
            }
            sRequest += "UserID=" + escape(gsUserID) + "&";
            sRequest += "DocumentID=" + escape(ldTrim(tblObj.rows[idx].DocumentID));
            var retArray = DoCrudAction(oDiv, sAction, sRequest);
            if (retArray[1] != "0")
            {
              //update the last Updated date...
              var objXML = new ActiveXObject("Microsoft.XMLDOM");
              objXML.loadXML(retArray[0]);
              var docDateNode = objXML.documentElement.selectSingleNode("/Root/@SysLastUpdatedDate");
              oCells[9].innerText = docDateNode.nodeValue;
              oCells[10].innerText = docDateNode.nodeValue;     
              tblObj.rows[idx].dirty = false;
              for (var z=0; z < cellsLength; z++)
              {
                if (oCells[z].dirty)
                {
                  oCells[z].dirty = false;
                  oCells[z].style.borderBottom = "1px solid #BBBBBB";
                  oCells[z].style.borderRight = "1px solid #BBBBBB";
                  oCells[z].style.borderLeft = "1px solid #FFF0F5";
                  oCells[z].style.borderTop = "1px solid #FFF0F5";
                } 
              }
            }
          }
        }
      }
      else if (sName == "content21")
      {
        var sRequest = "AssignmentID=" + ifrmShopAssignment.gsAssignmentID +
                      "&AssignmentRemarks=" + escape(ifrmShopAssignment.document.getElementById("AssignmentRemarks").value) + 
                      "&UserID=" + ifrmShopAssignment.gsUserID +
                      "&SysLastUpdatedDate=" + ifrmShopAssignment.gsAssignmentLastUpdatedDate;
        var retArray = DoCrudAction(oDiv, sAction, sRequest);
        ifrmShopAssignment.document.location.reload();
      }
      
      else
      {
        if (txtDescYear.value.length > 0 && txtDescYear.value.length != 4) {
          ClientWarning("Vehicle year must be a 4 digit year. Please try again.");
          return;
        }
		
		document.getElementById("CoverageProfileCD").value = profileCDValue;
		
        GetCBvalues('AirBag');
        var sRequest = GetRequestString(sName);

        sRequest += "VehicleSysLastUpdatedDate=" + gsVehicleLastUpdatedDate + "&";
        sRequest += "VehicleNumber=" + gsVehNum + "&";

        sRequest += "ImpactPoints="+ strImpactPoints+"&";
        sRequest += "PriorImpactPoints="+strPrevImpactPoints +"&";

        sRequest += "LynxID=" + gsLynxID + "&";
        sRequest += "UserID=" + gsUserID + "&";
        sRequest += "ClaimAspectID=" + gsClaimAspectID + "&";
        
        var retArray = DoCrudAction(oDiv, sAction, sRequest);

        if (retArray[1] != "0")
        {
          if (sName == "content11") {
            reloadPage();
            return;
          }
          //update the last Updated date...
          var objXML = new ActiveXObject("Microsoft.XMLDOM");
          objXML.loadXML(retArray[0]);
          var rootNode = objXML.documentElement.selectSingleNode("/Root");

          var VehicleNode = rootNode.selectSingleNode("Vehicle/@SysLastUpdatedDate");
          if (VehicleNode)
          {
            gsVehicleLastUpdatedDate = VehicleNode.nodeValue;
            oDiv.LastUpdatedDate = gsVehicleLastUpdatedDate
            //keep Description, Vehicle Location, Damage, Speed and Remarks tabs in sync
            var oDesc = document.getElementById("content11");
            if (oDesc) oDesc.LastUpdatedDate = gsVehicleLastUpdatedDate;
  		    var oVehLoc = document.getElementById("content13");
            if (oVehLoc) oVehLoc.LastUpdatedDate = gsVehicleLastUpdatedDate;
            var oRental = document.getElementById("content14");
            if (oRental) oRental.LastUpdatedDate = gsVehicleLastUpdatedDate;

       }
          var contextDate = rootNode.selectSingleNode(oDiv.name+"/@SysLastUpdatedDate")
          if (contextDate)
            oDiv.LastUpdatedDate = contextDate.nodeValue;

          var contextID = rootNode.selectSingleNode(oDiv.name+"/@InvolvedID")
          if (contextID)
            oDiv.ContextID = contextID.nodeValue;
        }
      }
    }
    else if (sAction == "Insert")
    {
      if (oDiv.Many == "true")
      {
        var sType = oDiv.name;

        var selObj = document.getElementById("sel"+sType)
        selObj.children[0].rows[0].cells[0].innerHTML = "<B>** New **</B>";
        document.frames["ifrm"+sType].frameElement.src = sType+".asp?WindowID=" + gsWindowID + "&InvolvedID=0";

        selObj = document.getElementById(oDiv.id + "Sel");
        selObj.style.borderColor = "#805050";

        eval(sName+"_ADD.style.visibility='hidden'");
        eval(sName+"_DEL.style.visibility='hidden'");

        if (oDiv.getAttribute("CRUD").indexOf("U") != -1)
          eval(sName+"_SAV.style.visibility='inherit'");

        gbInsertMode = true;
        gbDirtyFlag = true;
      }
    }
    else if (sAction == "Delete")
    {
      if (oDiv.Many == "true")
      {
        var sType = oDiv.name;
				//if (gsInvolvedCount == 0) return;

        var objSel = document.getElementById("sel"+sType);
        if (objSel)
          if(objSel.options.length == 0) return;

        var sInvolvedName = objSel.firstChild.innerText;
        if (sInvolvedName.indexOf("[") != -1)
          var sInvolved = sInvolvedName.substr(0, sInvolvedName.indexOf("["))
        else
          var sInvolved = sInvolvedName.substr(0, sInvolvedName.length-3);

        var sInvolvedConfirm = YesNoMessage("Are you sure that you want to delete Involved " +sInvolved+ "?",
                                            "Press YES to delete the selected Involved, or NO to cancel.");
        if (sInvolvedConfirm == "Yes")
        {
          var retArray = document.frames["ifrm"+sType].ADS_Pressed(sAction, sName);
          if (retArray[1] != "0")
          {
            UpdateSelects(sType,retArray[0]);

            if (parseInt(oDiv.Count) > 0) oDiv.Count--;
            UpdateTabTextCount(oDiv);
						if (oDiv.Count == 0)
						{
				      eval(sName+"_SAV.style.visibility='hidden'");
				      eval(sName+"_DEL.style.visibility='hidden'");
						}
            //document.frames["ifrm"+sType].frameElement.src = "/blank.asp";
          }
					else
					{
          	ClientInfo(retArray[0]);
	        }
        }
      }
      else
      {
        if (gsVehNum != "1")
        {
          if (gsClaimStatus == "Claim Cancelled" ||
              gsClaimStatus == "Claim Voided") {
              ClientWarning("Cannot delete when the claim has been cancelled or voided.");

              inADS_Pressed = false;
              disableADS(objDivWithButtons, false);
              objDivWithButtons = null;

              return;
          }
          var sRequest = "SysLastUpdatedDate=" + gsClaimAspectLastUpdatedDate + "&";
          sRequest += "VehicleNumber=" + gsVehNum + "&";
          sRequest += "LynxID=" + gsLynxID + "&";
          sRequest += "UserID=" + gsUserID + "&";
          sRequest += "ClaimAspectID=" + gsClaimAspectID + "&=";
          sRequest += "NotifyEvent=1&";

          var sIdVehicle = "";
          if (txtDescYear.value != "" && txtDescMake.value != "")
            sIdVehicle += " (" +txtDescYear.value+ " " +txtDescMake.value+ ")";

          var sVehicleDelConfirm = YesNoMessage("Are you sure that you want to delete Vehicle " +gsVehNum + sIdVehicle+ "?",
                                                "Press YES to delete this Vehicle, or NO to cancel.");
          if (sVehicleDelConfirm == "Yes")
          {
            var retArray = DoCrudAction(oDiv, sAction, sRequest);
            if (retArray[1] != "0")
              parent.document.parentWindow.frameElement.src="ClaimVehicleList.asp?WindowID=" + gsWindowID + "&LynxID=" + gsLynxID + "&VehContext=1";
          }
        }
        else
        {
          ClientWarning("You can not remove Vehicle 1 in a Claim!");
        }
      }
	  
    }

    inADS_Pressed = false;
    disableADS(objDivWithButtons, false);
    objDivWithButtons = null;
  }

  // Re-load the Involved select box from DXML
  function UpdateSelects(sType, sXML)
  {
    if (sXML == "") return;  //ERROR!!!

    var objXML = new ActiveXObject("Microsoft.XMLDOM");
    objXML.loadXML(sXML);
    var objNodeList = objXML.documentElement.selectNodes("/Root/"+sType);
    var objSel = document.getElementById("sel"+sType);
    // var selIndex = objNodeList.length - 1;
    var selInvId = objXML.documentElement.selectSingleNode("/Root/NewVehicleInvolved/@InvolvedID")
    var sInvolvedID = selInvId.nodeValue;
    var selIndex = 0;
    var strOptions = "";
    var nLength = objNodeList.length;
    var iClaimantCount = 0;
    for (var n=0; n < nLength; n++)
    {
      var objNode = objNodeList.nextNode();
      var strSel = ( n == selIndex ) ? " selected" : "";
      strOptions += "<div class='option' value='"  + objNode.selectSingleNode( "@InvolvedID" ).text + ",";
      strOptions += objNode.selectSingleNode( "@DayAreaCode" ).text + ",";
      strOptions += objNode.selectSingleNode( "@DayExchangeNumber" ).text + ",";
      strOptions += objNode.selectSingleNode( "@AddressCity" ).text + ",";
      strOptions += objNode.selectSingleNode( "@AddressState" ).text + ",";
      strOptions += objNode.selectSingleNode( "@AddressZip" ).text + "'";
      strOptions += strSel + " nowrap>";

        if (objNode.selectSingleNode( "@BusinessName" ).text != "")
        {
          strOptions += objNode.selectSingleNode( "@BusinessName" ).text + " ";
        }
        else
        {
          strOptions += objNode.selectSingleNode( "@NameFirst" ).text + " ";
          strOptions += objNode.selectSingleNode( "@NameLast" ).text + " " ;
        }

      if (objNode.selectSingleNode( "@InvolvedID" ).text != "0") {
        strOptions += "[";
        var objNodeTypes = objNode.selectNodes("VehicleInvolvedType");
        var xLength = objNodeTypes.length;
        for(var x = 0; x < xLength; x++)
        {
          var objNodeT = objNodeTypes.nextNode();
          var InvType = objNodeT.selectSingleNode( "@InvolvedTypeName" ).text;
          strOptions += InvType;
          if (x < (objNodeTypes.length - 1))
            strOptions += ", ";
          if (InvType == "Claimant")
            iClaimantCount++;
        }
        strOptions += "]";
      }
      strOptions += "</div>";

      if (sInvolvedID == (objNode.selectSingleNode("@InvolvedID").text))
        selIndex = n;
    }
    if (top.gaVehClaimantCount)
      top.gaVehClaimantCount[gsVehNum] = iClaimantCount;

    objSel.all.selectOptionDiv.innerHTML = strOptions;
    objSel.optionselect( null, selIndex );
  }

  // builds the multi-checkboxes list for Safety Devices Deployed
  function GetCBvalues(tag)
  {
    var sName = tag;
    var sId = "";
    var node = "";
    var otb = document.getElementById("tblAirBags");

    for (var i = 1; i <= gsSaftyDeviceCount; i++)
    {
      node = i;
      sInputName = sName + node;
      getValues = document.getElementById(sInputName).value;

      if (getValues == '1')
      {
        if (sId == "") sId += otb.rows[i-1].cells[1].innerText;
        else sId += "," + otb.rows[i-1].cells[1].innerText;
      }
    }
    txtSafetyDevicesDeployed.value = sId;
  }

  // NADA module popup code
  function callNada()
  {
    if (gsVehicleCRUD.indexOf("U") == -1 || gsVehicleCRUD.indexOf("R") == -1) return;
    var myreq = "";
    var strNada = "";
//    var doc = document.all;
    var strRetVal;
    var arrRetVal;
    var arrNameValue;
    var control;

    myreq = "&strVIN=" + txtDescVIN.value;
     //myreq += "&strRegion=" + txtLicensePlateState.value;
    //var strNada = txtNadaId.value;
    if (strNada != "null")
      myreq += "&strVehId=" + strNada;
    myreq += "&strMake=" + txtDescMake.value;
    myreq += "&strModel=" + txtDescModel.value;
    myreq += "&strYear=" + txtDescYear.value;
    myreq += "&strBody=" + txtDescBodyStyle.value;
    myreq += "&txtMileage=" + Mileage.value;
    myreq += "&strZip=" + document.frames("ifrmVehicleInvolved").document.all.txtInvolvedZip.value;
    myreq += "&strReadOnly=" + bPageReadOnly
    
    // The object "myObject" is sent to the modal window.
    strRetVal = window.showModalDialog("VehicleEvaluator.asp?WindowID=" + gsWindowID + myreq, "", "dialogHeight:480px; dialogWidth:750px; resizable:no; status:no; help:no; center:yes;");
    if (strRetVal == null || strRetVal == 'cancel')
      return false;

    arrRetVal = strRetVal.split('||');
    var iLength = arrRetVal.length;
    for(i=0; i < iLength; i++){
      arrNameValue = arrRetVal[i].split('--');

      switch(arrNameValue[0]){
        case 'year':
          control = eval('VehicleYear');
          break;
        case 'make':
          control = eval('Make');
          arrNameValue[1] = arrNameValue[1].substr(0, 15);  // 15 character limit imposed by database
          break;
        case 'model':
          control = eval('Model');
          arrNameValue[1] = arrNameValue[1].substr(0, 15);
          break;
        case 'body':
          control = eval('BodyStyle');
          arrNameValue[1] = arrNameValue[1].substr(0, 15);
          break;
        case 'vin':
          control = eval('VIN');
          break;
        case 'value':
          control = eval('BookValueAmt');
          break;
        case 'miles':
          control = eval('Mileage');
          break;
      }

      if (control.value != arrNameValue[1]){
        gbDirtyFlag = true;
        control.value = arrNameValue[1];
        control.select();
      }
    }
  }

  // IMAGE module popup code
  function ShowImage(strPath)
  {
    if (strPath)
      gsDocPath = gsImageRootDir + strPath;

    var sHREF = "/EstimateDocView.asp?WindowID=" + gsWindowID + "&docPath=" + gsDocPath;
  	window.showModalDialog(sHREF, "", "dialogHeight:500px; dialogWidth:700px; dialogTop:100px; dialogLeft:100px; resizable:yes; status:no; help:no; center:no;");
    gsDocPath = "";
  }


  // Kodak Imaging module popup code
  function EditImage(strPath)
  {
    if (strPath)
      gsDocPath = gsImageRootDir + strPath;

    var wsh = new ActiveXObject( "WSCript.shell" );
    wsh.Run( "Kodakimg " + gsDocPath );
    wsh.AppActivate( "Imaging" )
    gsDocPath = "";
  }


  function callEstImgContextMenu(x,y,strPath)
  {
    if (gsEstimateCRUD.indexOf("U") == -1) return;
    gsDocPath = gsImageRootDir + strPath;

    var contextHtml="";
    contextHtml+='<TABLE unselectable="on" STYLE="border:1pt solid #666666" BGCOLOR="#F9F8F7" WIDTH="130" HEIGHT="44" CELLPADDING="0" CELLSPACING="1">';
    contextHtml+='<ST'+'YLE TYPE="text/css">\n';
    contextHtml+='td {font-family:verdana; font-size:11px;}\n';
    contextHtml+='</ST'+'YLE>\n';
    contextHtml+='<SC'+'RIPT LANGUAGE="JavaScript">\n';
    contextHtml+='\n<'+'!--\n';
    contextHtml+='window.onerror=null;\n';
    contextHtml+='/'+' -'+'->\n';
    contextHtml+='</'+'SCRIPT>\n';
    contextHtml+='<TR unselectable="on"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ID="i0" ONMOUSEOVER="document.all.i0.style.background=\'#B6BDD2\';document.all.i0.style.border=\'1pt solid #0A246A\';document.all.i0.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i0.style.background=\'#F9F8F7\';document.all.i0.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.EditImage();">&nbsp;<IMG SRC="/images/notepad.gif" WIDTH="12" HEIGHT="12" BORDER="0" HSPACE="0" VSPACE="0" ALIGN="absmiddle">&nbsp;&nbsp;Edit Image</TD></TR>';
    contextHtml+='<TR unselectable="on"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ID="i1" ONMOUSEOVER="document.all.i1.style.background=\'#B6BDD2\';document.all.i1.style.border=\'1pt solid #0A246A\';document.all.i1.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i1.style.background=\'#F9F8F7\';document.all.i1.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.ShowImage();">&nbsp;<IMG SRC="/images/EstNewItem.gif" WIDTH="12" HEIGHT="12" BORDER="0" HSPACE="0" VSPACE="0" ALIGN="absmiddle">&nbsp;&nbsp;View Image</TD></TR>';
    contextHtml+='</TABLE>';

    var oPopupBody = oPopup.document.body;
    oPopupBody.innerHTML = contextHtml;
    oPopup.show(x, y, 130, 44, window.event.srcElement);
  }

  // Opens the expanded Assigned Shop tab when the Assign Shop button is clicked
  function AssignShop(ShopRemarks, sAssignmentTo)
  {
    try {
      var sShopRemarks = ShopRemarks;
      obj = document.getElementById("Content21");
    } catch (e) {}

  
      // Get params from Involved
      var selobj = document.getElementById("selVehicleInvolved");
      var sInvolved = selobj.options[selobj.selectedIndex].value;
      var aryInvolved = sInvolved.split(",");
      var sAC = aryInvolved[1];
      var sEX = aryInvolved[2];
      var sCTY = aryInvolved[3];
      var sST = aryInvolved[4];
      var sZIP = aryInvolved[5];
      //alert(sInvolved); return;
      
	  var gsLocationName  = document.getElementById("txtVehLocName").value;
	  var gsLocationCity  = document.getElementById("txtVehLocCity").value;
	//  var gsLocationState = document.getElementById("txtVehLocState").value;
	  var gsLocationState = document.getElementById("LocationState").value;
	  var gsLocationZip   = document.getElementById("txtVehLocZip").value;

      if ( ldTrim(gsLocationCity) != "" ) { sZIP = gsLocationZip; sCTY = gsLocationCity; sST = gsLocationState; }
	  else if ( ldTrim(gsLocationZip) != "") { sZIP = gsLocationZip; sCTY = gsLocationCity; sST = gsLocationState; }  		
	
      var sHREF = "ShopLookup.asp?WindowID=" + gsWindowID + "&AreaCode=" + sAC + 
        "&ExchangeNumber=" + sEX + "&ZipCode=" + sZIP + "&City=" + sCTY + "&State=" + sST +
        "&UserID=" + gsUserID + "&LynxID=" + gsLynxID + "&VehicleNumber=" + gsVehNum +
        "&ClaimAspectID=" + gsClaimAspectID + "&InsuranceCompanyID=" + gsInsuranceCompanyID + 
        "&ShopRemarks=" + sShopRemarks + "&LocationName=" + gsLocationName + "&AssignmentTo=" + sAssignmentTo;
       
	  window.showModalDialog(sHREF, "", "dialogHeight:440px; dialogWidth:725px; resizable:no; status:no; help:no; center:yes;");     
      onShopAssignment();
	    
  }

  // Resizes Assigned Shop DIV to original size
  function ResizeAssignShop()
  {
  }

  // Controls expanding of Shop Assignment tab if a related top tab is clicked
  function onTabChangeBubble(obj)
  {
  }

  // Fills the shop data in the Assigned Shop tab once a shop is assigned
  function AssignedShop()
  {
   // ResizeAssignShop();
  }
  
  
    
  function CancelAssignedShop()
  {
    gsShopId = "";
    var co = RSExecute("/rs/RSADSAction.asp", "JustExecute", "uspWorkFlowSelectShop", "LynxId="+gsLynxID+"&VehicleNumber="+gsVehNum+"ClaimAspectID="+gsClaimAspectID+"&UserId="+gsUserID+"&ShopLocationId="+gsShopId+"&ShopRemarks="+txtShopRemarks.value+"&NotifyEvent=1");

    if (co.status != -1)
    {
      onShopAssignment();
    }
    else
      ServerEvent();
  }

  
  function showFaxResults( co )
  {
    var SendFaxBtn = document.getElementById("SendFaxBtnDiv");
    SendFaxBtn.disabled = false;
    SendFaxBtn.style.cursor = 'hand';

    var retArray = ValidateRS( co );
    if ( retArray[1] == 1 )
      ClientInfo("Fax transmitted successfully to RightFAX server.");
  }

  function showFaxErrors( co )
  {
    var SendFaxBtn = document.getElementById("SendFaxBtnDiv");
    SendFaxBtn.disabled = false;
    SendFaxBtn.style.cursor = 'hand';

    var msg = "The following error occurred during the '" + co.context + "' remote script call: '"
      + co.message + "'/n  The raw data returned by the remote method call is: " + co.data;
    ClientEvent( 51, msg, "ClaimVehicle.xsl showFaxErrors()" );
  }

  // brings up the Estimate Summary page
  function ShowEstimate(docId)
  {
  	if (gbDirtyFlag == true)
  	{
  		var sSave = YesNoMessage("Need To Save", "The information in " + divWithButtons.name + " has changed!  Do you want to save?");
  		if (sSave == "Yes")
      {
  			ADS_Pressed("Update", divWithButtons.id);
  		}
      else
  		{
  			gbDirtyFlag = false;
  			if (typeof(parent.gbDirtyFlag)=='boolean')
  				parent.gbDirtyFlag = false;
      }
    }

    if (docId != "")
    {
      //var sURL = "Estimates.asp?WindowID=" + gsWindowID + "&DocumentID=" + docId;
      //window.showModalDialog(sURL,"","dialogHeight:540px; dialogWidth:738px; dialogTop:76px; dialogLeft:18px; center:No; help:No; resizable:Yes; status:No; unadorned:Yes");
      //window.open(sURL,"","dialogHeight:540px; dialogWidth:738px; dialogTop:76px; dialogLeft:18px; center:No; help:No; resizable:No; status:Yes; unadorned:Yes");

      var sURL = "Estimates.asp?WindowID=" + gsWindowID + "&DocumentID=" + docId + "&ShowTotals=1";
      var winName = "EstimateFull_" + gsLynxID;
      var sWinEstimates = window.open(sURL, winName,"Height=523px, Width=728px, Top=90px, Left=40px, resizable=Yes, status=No, menubar=No, titlebar=No");
      parent.parent.gAryOpenedWindows.push(sWinEstimates);
      sWinEstimates.focus();
    }
  }

  function ShowImpact(defaultSetting)
  {
    var strReturn = document.getElementById("txtImpactArea").innerText + "|" + document.getElementById("txtPriorImpactArea").innerText + "|" + defaultSetting;

    strReturn = window.showModalDialog("/DamagePick2.asp",strReturn,"dialogHeight: 410px; dialogWidth: 520px; center: Yes; help: No; resizable: No; status: No; unadorned: Yes");

    if (strReturn && strReturn != "")
    {
      var sArray = strReturn.split("|");
      SaveImageImpact(sArray[0], sArray[1]);
    }
  }

  var strImpactPoints = "";
  var strPrevImpactPoints = "";

  function SaveImageImpact(sAreaImpact,sPrevAreaImpact)
  {
    document.getElementById("txtImpactArea").innerText = sAreaImpact;
    document.getElementById("txtPriorImpactArea").innerText = sPrevAreaImpact;

    document.getElementById("txtImpactArea").parentElement.style.border = '1px solid #805050';
    document.getElementById("txtPriorImpactArea").parentElement.style.border = '1px solid #805050';
    gbDirtyFlag = true;

    var sArray = sAreaImpact.split(",");

    xmlReference.setProperty("SelectionLanguage", "XPath");
    strImpactPoints = ImpactNodes(sArray);
    sArray = sPrevAreaImpact.split(",");
    strPrevImpactPoints = ImpactNodes(sArray);

  }

  
  function Trim(str)
  {
	while (str.charAt(0) == ' ') str = str.substr(1);
	while (str.charAt(str.length-1) == ' ') str = str.substr(0,str.length-2);
	return str;
  }

  
  function ImpactNodes(sArray)
  {
    var strPrimaryID = "";
    var strXPath = "";
    var objNode;
    var strIDs = "";
	var strNode="";

    try
    {
      var iLength = sArray.length;
      for (var i=0; i < iLength; i++)
      {
        if (Trim(sArray[i]) != "")
        {
          if (sArray[i].indexOf("(Primary)") != -1)
          {
            strNode = Trim(sArray[i]);
						var strTmp = strNode.substring(0, strNode.indexOf("(Primary)")-1);
            strXPath = '//Reference[@List="Impact"][@Name="' + strTmp + '"]/@ReferenceID';
            objNode = xmlReference.documentElement.selectSingleNode( strXPath );
            strPrimaryID = objNode.value;
          }
          else
          {
            strXPath = '//Reference[@List="Impact"][@Name="' + Trim(sArray[i]) + '"]/@ReferenceID';
            objNode = xmlReference.documentElement.selectSingleNode( strXPath );
            strIDs += String(objNode.value) + ",";
          }
        }
      }

      if (strPrimaryID != "")
        strIDs = strPrimaryID + "," + strIDs;
      return strIDs;
    }
    catch(e)
    {
      ClientError( e.message + ": XPath = " + strXPath );
    }
  }

  
  function tabBeforeChange(obj, tab)
  {
    var relatedTab = obj
		// if still on the same tab just ignore
  	if (obj == tab) return;
    if (relatedTab.content.id == "content11" || window.document.activeElement.name == "VehicleYear") {
      if (txtDescYear.value.length > 0 && txtDescYear.value.length != 4) {
        ClientWarning("Vehicle year must be a 4 digit year. Please try again.");
        return false;
      }
    }
    if (relatedTab.content.id == "content21" && (typeof(ifrmShopAssignment.isDirty) == "function" && ifrmShopAssignment.isDirty() == true)) {
  		var sSave = YesNoMessage("Need To Save", "The information in Vehicle Assignment has changed!  To save the changes, click Yes and then click Save.?");
  		if (sSave == "Yes")
        return false; // will prevent tab change.
    }
	
    if (relatedTab.content.id == "content22"  && document.frames["ifrmVehicleInvolved"].gbDirtyFlag == true && gbDirtyFlag == true){
    	var sSave = YesNoMessage("Need To Save", "The information in Vehicle Involved has changed!  To save the changes, click Yes and then click Save.?");
  		if (sSave == "Yes")
        return false; // will prevent tab change.
		else if (sSave == "No")  
		{
		 document.frames["ifrmVehicleInvolved"].gbDirtyFlag = false;
		 gbDirtyFlag = false;
		} 
     } 	
	
    return ValidatePhones(relatedTab.content.id);
  }


  function ValidatePhones(sName) {
       switch(sName) {
            case "content13": //Vehicle Location Info
                if (validatePhoneSections(txtVHPhoneAC, txtVHPhoneEX, txtVHPhoneUN) == false) return false;
                break;
            case "content22": //Involved Info
                //After the page loads the current tab is content31. Now if the user types directly into the content21
                //sections and navigates out, this will check for valid phone and then bring up the dirty flag dialog.
                var activeEle = document.activeElement;
                if ((activeEle == txtVHPhoneAC) || (activeEle == txtVHPhoneEX) || (activeEle == txtVHPhoneUN))
                    if (validatePhoneSections(txtVHPhoneAC, txtVHPhoneEX, txtVHPhoneUN) == false) return false;

                var oDiv = document.getElementById("content22")
                var sType = oDiv.name;
                try {
                    return document.frames["ifrm"+sType].ValidatePhones();
                }
                catch (e) {return true;}
                break;
            case "content12": //Vehicle Contact
                if (validatePhoneSections(txtContactDayAreaCode, txtContactDayExchangeNumber, txtContactDayUnitNumber) == false) return false;
                if (validatePhoneSections(txtContactNightAreaCode, txtContactNightExchangeNumber, txtContactNightUnitNumber) == false) return false;
                if (validatePhoneSections(txtContactAlternateAreaCode, txtContactAlternateExchangeNumber, txtContactAlternateUnitNumber) == false) return false;
                break;
        }
        return true;
    }


   function resizeScrollTable(oElement)
	{
	  var head = oElement.firstChild;
	  var headTable = head.firstChild;
	  var body = oElement.lastChild;
	  var bodyTable = body.firstChild;

	  body.style.height = Math.max(0, oElement.clientHeight - head.offsetHeight);

	  var scrollBarWidth = body.offsetWidth - body.clientWidth; 

		  // set width of the table in the head
		  //headTable.style.width = Math.max(0, Math.max(bodyTable.offsetWidth + scrollBarWidth, oElement.clientWidth));

		  // go through each cell in the head and resize
		  var headCells = headTable.rows[0].cells;
		  if (bodyTable.rows.length > 0)
		  {
		    var bodyCells = bodyTable.rows[0].cells;
       		 var iLength = bodyCells.length;
		    for (var i = 0; i < iLength; i++)
		    {
		      if (bodyCells[i].style.display != "none")
		      {
		        headCells[i].style.width = bodyCells[i].offsetWidth;
		        if (i == (headCells.length-1)) bodyCells[i].style.width = bodyCells[i].offsetWidth - (scrollBarWidth-2 );
		      }
		    }
		}
	  }


	function SanityCheck(obj)
	{
    obj.value = ipeFormatCurrencyStr(obj.value);
    var prevValue = parseFloat(obj.prevValue);
    var newValue = parseFloat(obj.value);
    if (prevValue != NaN)
    {
      var relPercent = prevValue * 0.3;
      if(newValue > prevValue+relPercent || newValue < prevValue-relPercent)
        ClientWarning("Please verify the value entered. It differs from the original value by &plusmn; 30%.");
    }
	}

  //currency formating
  function ipeFormatCurrencyStr(X)
  {
    if (isNaN(X) || X == '.' || X == '')
      return X = ' ';
    else
      return X = formatcurrency(X);
  }

  
  function formatcurrency(X)
  {
    var roundNum = Math.round(X*100)/100; //round to 2 decimal places
    var strValue = new String(roundNum);

    if (strValue.indexOf('.') == -1)
      strValue = strValue + '.00';
    else if (strValue.indexOf('.') == strValue.length - 2)
      strValue = strValue + '0';
    else
      strValue = strValue.substring(0, strValue.indexOf('.') + 3);
    
    return strValue;
  }

   
  function GetCityState(objZip,State,City)
  {
    InvolvedValidateZip(objZip,State,City);
    var StateObj = document.getElementById(State);
    var StateElement = document.getElementById('sel' + State);
    var stateText = findSelectedStateText(StateElement,StateObj.value);
    onSelectChange(StateElement);
    int_optionselect(StateElement,stateText);
    copySelected(StateElement);
  }
  
  
  function ldTrim(strText)
  { 
    while (strText.substring(0,1) == ' ') // this will get rid of leading spaces
      strText = strText.substring(1, strText.length);
    
    while (strText.substring(strText.length-1,strText.length) == ' ') // this will get rid of trailing spaces
      strText = strText.substring(0, strText.length-1);
    
    return strText;
  } 


  function ShowQuickEstUpdWindow(oRow, sDocumentID, ImageLocation)
  {
    var sImageLocation = gsImageRootDir + ImageLocation;

    if (sDocumentID != "")
    {
      var sURL = "DocumentEstDetails.asp?EstimateUpd=1&ClaimAspectID=" + gsClaimAspectID + "&DocumentID=" + sDocumentID + "&UserID=" + gsUserID + "&ImageLocation=" + sImageLocation + "&LynxId=" + gsLynxID + "&InsuranceCompanyID=" + gsInsuranceCompanyID;
      var strReturn = window.showModalDialog(sURL,"","dialogHeight:580px; dialogWidth:860px; center:Yes; help:No; resizable:No; status:No; unadorned:Yes");
    }

    if (strReturn && strReturn != "0")
    {
      var sArray = strReturn.split("|");
      // returnValue = lsDocType | lsRepairTotalAmt | lsNetTotalAmt | lsEstimateTypeCD | lsDuplicate | lsAgreedPriceMetCD | gsDocumentVANSourceFlag
      
      if (sArray[6] != "1")
      {
        if (sArray[0] == "")
          oRow.cells[2].innerHTML = "&nbsp;";
        else
          oRow.cells[2].innerHTML = sArray[0];

        if (sArray[1] == "")
          oRow.cells[4].innerHTML = "&nbsp;";
        else
          oRow.cells[4].innerHTML = sArray[1];

        if (sArray[2] == "")
          oRow.cells[5].innerHTML = "&nbsp;";
        else
          oRow.cells[5].innerHTML = sArray[2];

        if (sArray[3] == "O")
          oRow.cells[6].innerHTML = "Original";
        else if (sArray[3] == "A")
          oRow.cells[6].innerHTML = "Audited";
        else 
          oRow.cells[6].innerHTML = "&nbsp;";

        if (sArray[4] == "1")
          oRow.cells[7].innerHTML = "Yes";
        else if (sArray[4] == "0")
          oRow.cells[7].innerHTML = "No";
        else 
          oRow.cells[7].innerHTML = "&nbsp;";

        if (sArray[5] == "Y")
          oRow.cells[8].innerHTML = "Yes";
        else if (sArray[5] == "N")
          oRow.cells[8].innerHTML = "No";
        else 
          oRow.cells[8].innerHTML = "&nbsp;";
      }
    }
  }
  

  function inShopAssignment(){
    if (ifrmShopAssignment.gsAssignmentID == 0) {
      var obj = document.getElementById("content21_SAV");
      if(obj) {
        obj.style.visibility = "hidden";
      }
    }
    else {
      var obj = document.getElementById("content21_SAV");
      if(obj) {
        obj.style.visibility = (gsVehicleCRUD.indexOf("U") != -1 ? "inherit" : "hidden");;
      }
    }
  }
  
  function confirmRepairStart() {
    if (gbDirtyFlag == true){
      ClientWarning("Please save the changes and try again.");
      return;
    }

    if (RepairEndDate.value == ""){
      ClientWarning("Please enter a Repair End Date.");
      return;
    }
    
    var strRet = YesNoMessage("Confirm Repair Start", "Are you sure the repair on this vehicle has started. Once confirmed the repair start date cannot be changed.");
    switch (strRet) {
      case "Yes":
        var sProc;
        var sRequest;
        sRequest = "ClaimAspectID=" + gsClaimAspectID + "&";
        sRequest += "RepairDateCD=S&";
        sRequest += "UserID=" + gsUserID;
        
        sProc = "uspRepairConfirmUpdDetail";
        
        var co = RSExecute("/rs/RSADSAction.asp", "JustExecute", sProc, sRequest);
    
        if (co.status != -1)
        {
          //document.location.reload();
          reloadPage();
        }
        else
          ServerEvent();
        
        break;
    }
    
  }

  function confirmRepairEnd() {
    if (gbDirtyFlag == true){
      ClientWarning("Please save the changes and try again.");
      return;
    }
    var strRet = YesNoMessage("Confirm Repair Completion", "Are you sure the repair on this vehicle has completed. Once confirmed the repair end date cannot be changed.");
    switch (strRet) {
      case "Yes":
        var sProc;
        var sRequest;
        sRequest = "ClaimAspectID=" + gsClaimAspectID + "&";
        sRequest += "RepairDateCD=E&";
        sRequest += "UserID=" + gsUserID;
        
        sProc = "uspRepairConfirmUpdDetail";
        
        var co = RSExecute("/rs/RSADSAction.asp", "JustExecute", sProc, sRequest);
    
        if (co.status != -1)
        {
          reloadPage();
        }
        else
          ServerEvent();

        break;
    }
  }

  function reloadPage(){
    if (typeof(top.refreshNotesWindow) == "function")
      top.refreshNotesWindow( top.vLynxID, top.vUserID );
    if (typeof(top.refreshCheckListWindow) == "function")
      top.refreshCheckListWindow( top.vLynxID, top.vUserID );
    document.location.reload();
  }
  
  if (document.attachEvent)
    document.attachEvent("onclick", top.hideAllMenuScriptlets);
  else
    document.onclick = top.hideAllMenuScriptlets;

  function updateVIN()
  {
    if (document.getElementById("txtDescEstimateVIN").value == "")
      return;
    else
    {
      document.getElementById("txtDescVIN").value = document.getElementById("txtDescEstimateVIN").value;
      document.getElementById("txtDescVIN").dirty = "true";
      document.getElementById("txtDescVIN").style.border = "1px solid #805050";
      document.getElementById("txtDescVIN").style.background = "#ffffff;";
      gbDirtyFlag = true;
    }
  }

]]>
</SCRIPT>


</HEAD>
<BODY unselectable="on" class="bodyAPDSub" style="overflow:hidden" onLoad="tabInit(false,'#FFFFFF'); initSelectBoxes(); InitFields(); PageInit();">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

<!-- Impact Type XML Data -->
<xml id="xmlReference"><Root><xsl:copy-of select="/Root/Reference[@List='Impact']"/></Root></xml>

<!-- Start Tabs1 -->
<DIV unselectable="on" id="TabSection1" style="position:absolute; visibility: visible; top: 0px; left: 0px">

  <DIV unselectable="on" id="tabs1" class="apdtabs">
    <SPAN unselectable="on" id="tab11" class="tab1">Description</SPAN>
    <SPAN unselectable="on" id="tab12" class="tab1">Vehicle Contact</SPAN>
    <SPAN unselectable="on" id="tab13" class="tab1">Vehicle Location Info</SPAN>
	<SPAN unselectable="on" id="tab14" class="tab1">Rental</SPAN>
  </DIV>

  <!-- Start Description Info -->
  <DIV unselectable="on" class="content1" id="content11" name="VehicleDescription" Update="uspVehicleDescriptionUpdDetail" Delete="uspClaimVehicleDel" style="width:726px; height:180px; visibility:visible" onfocusin="DivOnFocusIn(this)">
    <xsl:attribute name="CRUD">
      <xsl:choose>
        <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
        <xsl:otherwise><xsl:value-of select="$VehicleCRUD"/></xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Vehicle/@SysLastUpdatedDate"/></xsl:attribute>
    <xsl:for-each select="Vehicle" >
      <xsl:call-template name="DescriptionInfo">
        <xsl:with-param name="myCRUD"><xsl:value-of select="$VehicleCRUD"/></xsl:with-param>
        <xsl:with-param name="myPageReadOnly"><xsl:value-of select="$pageReadOnly"/></xsl:with-param>
      </xsl:call-template>
    </xsl:for-each>
    <SCRIPT>ADS_Buttons('content11');</SCRIPT>
	<DIV id="partyDiv"  style='position:absolute; visibility:visible; top:-22px; left:450px; width:150px;'>
	  <TABLE><TR>
	     <TD unselectable="on" style="font-weight:bold" nowrap="">Party:</TD>
	     <TD unselectable="on"  nowrap=""><xsl:value-of select="$gsParty"/></TD>
	  </TR></TABLE>
	</DIV>
  </DIV>

    
  <xsl:if test="$pageReadOnly = 'true'">
  <xsl:value-of select="js:setPageDisabled()"/>
  </xsl:if>
  <xsl:value-of select="js:SetCRUD(string($VehicleCRUD))"/>

  <!-- Start Contact Info -->
  <DIV unselectable="on" class="content1" id="content12" name="VehicleContact" Update="uspvehicleContactUpdDetail" onfocusin="DivOnFocusIn(this)" style="width:726px;height:180px;">
  <xsl:attribute name="CRUD">
      <xsl:choose>
        <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
        <xsl:otherwise><xsl:value-of select="$VehicleCRUD"/></xsl:otherwise>
      </xsl:choose>
  </xsl:attribute>
  <xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Vehicle/Contact/@SysLastUpdatedDate"/></xsl:attribute>
  <xsl:attribute name="ContextID"><xsl:value-of select="Vehicle/Contact/@InvolvedID"/></xsl:attribute>
  <xsl:attribute name="ContextName">Involved</xsl:attribute>
    <xsl:for-each select="Vehicle/Contact" >
		<xsl:call-template name="VehicleContactInfo"/>
	</xsl:for-each>
  <SCRIPT>ADS_Buttons('content12');</SCRIPT>
  </DIV>
  
  

    <!-- Start Vehicle Location Info -->
  <DIV unselectable="on" class="content1" id="content13" name="VehicleLocation" Update="uspVehicleLocationUpdDetail" style="width:726px; height:180px;"  onfocusin="DivOnFocusIn(this)">
    <xsl:attribute name="CRUD">
      <xsl:choose>
        <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
        <xsl:otherwise><xsl:value-of select="$VehicleCRUD"/></xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Vehicle/@SysLastUpdatedDate"/></xsl:attribute>
    <xsl:for-each select="Vehicle" >
      <xsl:call-template name="VehicleLocationInfo"/>
    </xsl:for-each>
  <SCRIPT>ADS_Buttons('content13');</SCRIPT> 
  </DIV>
  
  
   <!-- Start Rental Info -->
  <DIV unselectable="on" class="content1" id="content14" name="VehicleRental" Update="uspVehicleRentalUpdDetail" style="width:726px; height:180px" onfocusin="DivOnFocusIn(this)">
    <xsl:attribute name="CRUD">
      <xsl:choose>
        <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
        <xsl:otherwise><xsl:value-of select="$VehicleCRUD"/></xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Vehicle/@SysLastUpdatedDate"/></xsl:attribute>
    <xsl:for-each select="Vehicle" >
      <xsl:call-template name="VehicleRental"/>
    </xsl:for-each>
    <SCRIPT>ADS_Buttons('content14');</SCRIPT>
  </DIV>
</DIV><!-- End Tabs1 -->


<!-- Start Tabs3 -->
<DIV unselectable="on" id="TabSection2" style="position:absolute; visibility: visible; top: 206px; left: 0px">
  <DIV unselectable="on" id="tabs2" class="apdtabs">
     <SPAN unselectable="on" id="tab21" class="tab2">Assignment</SPAN>
      <SPAN unselectable="on" id="tab22" class="tab2">Involved (<xsl:value-of select="count(Vehicle/Involved[@InvolvedID &gt; 0])"/>)</SPAN>
      <SPAN unselectable="on" id="tab23" class="tab2">Estimates</SPAN>
      <xsl:if test="$ContextSupported=1">
      <SPAN unselectable="on" id="tab24" class="tab2">Documents</SPAN>
    </xsl:if>
  </DIV>

  
  <!-- Start Shop Assignment Info -->
  <DIV unselectable="on" class="content2" id="content21" name="ShopAssignment" Update="uspAssignmentUpdRemarks" style="width:726px; height:250px;" onfocusin="DivOnFocusIn(this)">
    <xsl:attribute name="CRUD">
      <xsl:choose>
        <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
        <xsl:otherwise>_RU_</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
      <xsl:call-template name="ShopAssignInfo">
        <xsl:with-param name="InsuranceCompanyID" select="$InsuranceCompanyID"/>
      </xsl:call-template>
    <SCRIPT>ADS_Buttons('content21');</SCRIPT>
  </DIV>
  
  <xsl:choose>
        <xsl:when test="contains($InvolvedCRUD, 'R')">
            <!-- This will allow the user to atleast browse thru the Involved List and the Involved page will be readonly. -->
            <xsl:value-of select="js:setPageEnabled()"/>
            <xsl:value-of select="js:SetCRUD( '_RU_' )"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="js:SetCRUD( $InvolvedCRUD )"/>
        </xsl:otherwise>
   </xsl:choose>
	
	
	
  <!-- Start Involved Info -->
  <DIV unselectable="on" class="content2" id="content22" name="VehicleInvolved" Update="uspVehicleInvolvedUpdDetail" Insert="uspVehicleInvolvedInsDetail" Delete="uspVehicleInvolvedDel" Many="true" onfocusin="DivOnFocusIn(this)" style="width:714px; height:234px;">
    <xsl:attribute name="CRUD">
      <xsl:choose>
        <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
        <xsl:otherwise><xsl:value-of select="$InvolvedCRUD"/></xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="Count"><xsl:value-of select="count(Vehicle/Involved[@InvolvedID &gt; 0])"/></xsl:attribute>
    <xsl:call-template name="InvolvedInfo"/>
    <SCRIPT>ADS_Buttons('content22');</SCRIPT>
  </DIV>
  
  <xsl:if test="$pageReadOnly = 'true'">
    <xsl:value-of select="js:setPageDisabled()"/>
  </xsl:if>
  <xsl:value-of select="js:SetCRUD( string($VehicleCRUD) )"/> 
  


  <!-- Start Estimates Info -->
  <DIV unselectable="on" class="content2" id="content23" name="Estimates" Update="uspVehicleEstimateUpdDetail" onfocusin="DivOnFocusIn(this)" style="width:726px; height:254px; padding:6px;">
     <xsl:attribute name="CRUD">
      <xsl:choose>
         <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
         <xsl:otherwise>____</xsl:otherwise>
      </xsl:choose>
     </xsl:attribute>
      <xsl:call-template name="EstimatesInfo">
            <xsl:with-param name="myCRUD">
              <xsl:choose>
                <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
                <xsl:otherwise><xsl:value-of select="$EstimateCRUD"/></xsl:otherwise>
              </xsl:choose>
            </xsl:with-param>
      </xsl:call-template>
      <SCRIPT>ADS_Buttons('content23');</SCRIPT>
  </DIV>

  <!-- Start Documents -->
  <!-- Only if the vehicle documents are supported by the insurance company  display them -->
  <xsl:if test="$ContextSupported=1">
  <DIV unselectable="on" class="content2" name="Documents" id="content24" CRUD="____" style="width:726px; height:254px;">
    <IFRAME id="ifrmDocuments" onreadystatechange='top.IFStateChange(this);' style='width:722px; height:250px; top:2px; left:2px; border:0px; margin:0px; position:absolute; background:transparent; overflow:hidden;' allowtransparency='true' >
      <xsl:attribute name="src">Documents.asp?WindowID=<xsl:value-of select="$WindowID"/>&amp;LynxID=<xsl:value-of select="$LynxID"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>ClaimAspectID=<xsl:value-of select="$ClaimAspectID"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>InsuranceID=<xsl:value-of select="$InsuranceCompanyID"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>EntityName=Vehicle<xsl:text disable-output-escaping="yes">&amp;</xsl:text>EntityNumber=<xsl:value-of select="$VehicleNumber"/></xsl:attribute>
    </IFRAME>
      <SCRIPT>ADS_Buttons('content24');</SCRIPT>
  </DIV>
  </xsl:if>

</DIV> <!-- End Tabs3 -->


<!-- Estimate Summary Frame -->
<IFRAME id="ifrmEstimateSummary" src="blank.asp"  onreadystatechange='top.IFStateChange(this);' style='width:728px; height:480px; top:0px; left:0px; border:1px; visibility:hidden; display:none; position:absolute; background:transparent; overflow:hidden;' allowtransparency='true' >
</IFRAME>

</BODY>
</HTML>
</xsl:template>

  <!-- Gets the Description Info -->
  <xsl:template name="DescriptionInfo" >
  <xsl:param name="myCRUD"/>
  <xsl:param name="myPageReadOnly"/>
  <xsl:variable name="AssignmentDisabled">
    <xsl:value-of select="contains($myCRUD, 'U') = false() or /Root/Vehicle/@ActiveReinspection = '1'"/>
  </xsl:variable>
   <TABLE class="paddedTable" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;margin:0px;margin-top:10px;" >
       <colgroup>
        <col width="59"/>
        <col width="111"/>
<!--        <col width="10"/>-->
        <col width="98"/>
        <col width="111"/>
<!--        <col width="10"/>-->
        <col width="142"/>
        <col width="196"/>
<!--         <col width="55"/>   -->
<!--        <col width="10"/> -->		
<!--         <col width="50"/> -->
<!-- 		<col width="45"/> -->
<!-- 		<col width="15"/> -->
       </colgroup>
       <TR>  
       <TD unselectable="on">Year:</TD>
       <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDescYear',3,'VehicleYear',.,'','',1)"/>
       </TD>
	   <TD unselectable="on">Inspection Date:</TD>
       <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInspectionDate',8,'InspectionDate',.,'',2,10)"/>
       </TD>			  
       <TD unselectable="on">Assignment Type: </TD>
	   <TD unselectable="on" nowrap="">	
        <xsl:if test="$AssignmentDisabled = true()">
          <xsl:value-of select="js:setPageDisabled()"/>
          <xsl:value-of select="js:SetCRUD('_R__')"/>
        </xsl:if>
         <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('AssignmentTypeID',2,'onSelectChange',string(@CurrentAssignmentTypeID),1,13,/Root/Reference[@List='AssignmentType'],'Name','ReferenceID','180','50','','',17,1)"/> 
        <xsl:if test="$AssignmentDisabled = true()">
          <xsl:value-of select="js:setPageEnabled()"/>
          <xsl:value-of select="js:SetCRUD(string($myCRUD))"/>
        </xsl:if>
	   </TD>     
       </TR>
	
       <TR>
       <TD unselectable="on">Make:</TD>
       <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDescMake',15,'Make',.,'','',2)"/>
       </TD>
	   <TD unselectable="on">Repair Start:</TD>
       <TD unselectable="on">
        <xsl:variable name="repairStartDate">
            <xsl:if test="/Root/Vehicle/@RepairStartDate!='1900-01-01T00:00:00'"><xsl:value-of select="js:formatSQLDateTime(string(/Root/Vehicle/@RepairStartDate))"/></xsl:if>
        </xsl:variable>
	        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtRepairStartDate',8,'RepairStartDate',.,'',2,10)"/>
          <xsl:if test="/Root/Vehicle/@RepairStartConfirmFlag = '0'">
          <button name="btnRepairStartConfirm" id="btnRepairStartConfirm" style="height:12px; width:19px; padding:0px; margin:0px; border:0px; background-color:#FFFFFF; cursor:hand" tabIndex="121" onclick="confirmRepairStart()" title="Click to confirm repair start date">
            <img src="/images/tick.gif"/>
          </button>
          </xsl:if>
        <script language="javascript">
          sRepairStartDate = "<xsl:value-of select="$repairStartDate"/>";
        </script>
       </TD>

       <TD unselectable="on" nowrap="">
        Coverage:
       </TD>
       <TD unselectable="on" nowrap="">
     <xsl:choose>  
      <xsl:when test="/Root/Vehicle/@ExposureCD='1'">
       <xsl:variable name="AllClientCoverageID">
	     <xsl:for-each select="/Root/Vehicle/CoverageType[@CoverageProfileCD='UIM' or @CoverageProfileCD='COLL' or @CoverageProfileCD='COMP' or @CoverageProfileCD='UM']">
		   <xsl:sort select="@DisplayOrder" data-type="number"/>
		   <xsl:variable name="CCTID"><xsl:value-of select="@ClientCoverageTypeID"/></xsl:variable>
           <xsl:value-of select="/Root/Vehicle/CoverageType[@ClientCoverageTypeID=$CCTID]/@ClientCoverageTypeID"/>|
	     </xsl:for-each>
	   </xsl:variable> 
       <xsl:variable name="AllCoverageTypes">
        <xsl:for-each select="/Root/Vehicle/CoverageType[@CoverageProfileCD='UIM' or @CoverageProfileCD='COLL' or @CoverageProfileCD='COMP' or @CoverageProfileCD='UM']">
		  <xsl:sort select="@DisplayOrder" data-type="number"/>
	      <xsl:variable name="CCTID"><xsl:value-of select="@ClientCoverageTypeID"/></xsl:variable>
          <xsl:value-of  select="concat(string(/Root/Vehicle/CoverageType[@ClientCoverageTypeID=$CCTID]/@Name),'(',string(/Root/Vehicle/CoverageType[@ClientCoverageTypeID=$CCTID]/@CoverageProfileCD),')')"/>|
	    </xsl:for-each>
	   </xsl:variable> 
	   <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('ClientCoverageTypeID',2,'onSelectChange','',1,12,string($AllCoverageTypes),string($AllCoverageTypes),string($AllClientCoverageID),'190','','','',18)"/>
	  </xsl:when>
	
      <xsl:when test="/Root/Vehicle/@ExposureCD='3'">
       <xsl:variable name="AllClientCoverageID">
	     <xsl:for-each select="/Root/Vehicle/CoverageType[@CoverageProfileCD='LIAB']">
		   <xsl:sort select="@DisplayOrder" data-type="number"/>
		   <xsl:variable name="CCTID"><xsl:value-of select="@ClientCoverageTypeID"/></xsl:variable>
           <xsl:value-of select="/Root/Vehicle/CoverageType[@ClientCoverageTypeID=$CCTID]/@ClientCoverageTypeID"/>|
	     </xsl:for-each>
 	   </xsl:variable> 
       <xsl:variable name="AllCoverageTypes">
        <xsl:for-each select="/Root/Vehicle/CoverageType[@CoverageProfileCD='LIAB']">
		  <xsl:sort select="@DisplayOrder" data-type="number"/>
	       <xsl:variable name="CCTID"><xsl:value-of select="@ClientCoverageTypeID"/></xsl:variable>
           <xsl:value-of  select="concat(string(/Root/Vehicle/CoverageType[@ClientCoverageTypeID=$CCTID]/@Name),'(',string(/Root/Vehicle/CoverageType[@ClientCoverageTypeID=$CCTID]/@CoverageProfileCD),')')"/>|
	    </xsl:for-each>
	    </xsl:variable> 
	     <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('ClientCoverageTypeID',2,'onSelectChange','',1,12,string($AllCoverageTypes),string($AllCoverageTypes),string($AllClientCoverageID),'190','','','',18)"/>
	   </xsl:when>
      </xsl:choose>
   
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('CoverageProfileCD',8,'CoverageProfileCD',/Root/Vehicle,'',6)"/>  
       </TD>
       </TR>  
	   
       <TR>
       <TD unselectable="on" >Model:</TD>
       <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDescModel',15,'Model',.,'','',3)"/>
       </TD>  
	   <TD unselectable="on" nowrap="">Repair End:</TD>
       <TD unselectable="on" ><xsl:attribute name="nowrap"/>
        <xsl:variable name="repairEndDate">
            <xsl:if test="/Root/Vehicle/@RepairEndDate!='1900-01-01T00:00:00'"><xsl:value-of select="js:formatSQLDateTime(string(/Root/Vehicle/@RepairEndDate))"/></xsl:if>
        </xsl:variable>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtRepairEndDate',8,'RepairEndDate',.,'',2,11)"/>
        <xsl:if test="/Root/Vehicle/@RepairEndConfirmFlag = '0' and /Root/Vehicle/@RepairStartConfirmFlag = '1'">
        <button name="btnRepairEndConfirm" id="btnRepairEndConfirm" style="height:12px; width:19px; padding:0px; margin:0px; border:0px; background-color:#FFFFFF; cursor:hand" tabIndex="123" onclick="confirmRepairEnd()" title="Click to confirm repair end date">
          <img src="/images/tick.gif"/>
        </button>
        
        </xsl:if>
        <script language="javascript">
          sRepairEndDate = "<xsl:value-of select="$repairEndDate"/>";
        </script>
        <input type="hidden" id="txtRepairScheduleChangeReason" name="RepairScheduleChangeReason"/>
       </TD>
	   <TD unselectable="on">
        Impact Area:
            <IMG src="/images/next_button.gif" onClick="ShowImpact('New Damage')" width="19" height="19" border="0" align="absmiddle" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)" tabindex="20">
              <xsl:if test="$myPageReadOnly = 'true'">
                <xsl:attribute name="disabled"/>
              </xsl:if>
            </IMG>
	   </TD>  
	   <TD unselectable="on" nowrap="" >
            Prior Damage Area:
			<IMG src="/images/next_button.gif" onClick="ShowImpact('Previous Damage')" width="19" height="19" border="0" align="absmiddle" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)" tabindex="21">
              <xsl:if test="$myPageReadOnly = 'true'">
                <xsl:attribute name="disabled"/>
              </xsl:if>
            </IMG>
       </TD>   
       </TR>
	
       <TR>
       <TD unselectable="on">Body Style:</TD>
       <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDescBodyStyle',15,'BodyStyle',.,'','',4)"/>
       </TD>  
	   <TD unselectable="on"  nowrap="">Driven w/ Perm.:</TD>
       <TD unselectable="on" >
        <xsl:variable name="rtPTD" select="@PermissionToDriveCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('PermissionToDriveCD',2,'onSelectChange',string($rtPTD),1,2,/Root/Reference[@List='PermissionToDrive'],'Name','ReferenceID','70','','','',12,1)"/>
       </TD>  	
	   <TD rowspan="2" valign="top">
      <xsl:variable name="PrimaryImpactID"><xsl:value-of select="/Root/Vehicle/Impact[@ImpactID != '0' and @CurrentImpactFlag='1' and @PrimaryImpactFlag='1']/@ImpactID"/></xsl:variable>
      <xsl:variable name="ImpactArea">
        <xsl:if test="$PrimaryImpactID != ''">
          <xsl:value-of select="concat(string(/Root/Reference[@List='Impact' and @ReferenceID=$PrimaryImpactID]/@Name), ' (Primary), ')"/>
        </xsl:if>
        <xsl:for-each select="/Root/Vehicle/Impact[@ImpactID != '0' and @CurrentImpactFlag='1' and @PrimaryImpactFlag='0']">
		<xsl:variable name="CurrentImpactID">
		      <xsl:value-of select="@ImpactID"/></xsl:variable>
		<xsl:value-of select="/Root/Reference[@List='Impact' and @ReferenceID=$CurrentImpactID]/@Name"/>, 
	</xsl:for-each>
      </xsl:variable>
	    <DIV class="autoflowDiv" style="width:135px; height:40px;overflow-x:hidden;overflow-y:auto">
              <SPAN id="txtImpactArea" class="InputReadonlyField" style="width:100%; height:100%">
                <!-- <xsl:for-each select="Reference[@List='Impact']">
                  <xsl:variable name="ImpactNode" select="/Root/Vehicle/Impact[@ImpactID=current()/@ReferenceID and @CurrentImpactFlag='1']"/>
                  <xsl:if test="boolean($ImpactNode)">
                    <xsl:value-of select="@Name"/>
                    <xsl:if test="$ImpactNode/@PrimaryImpactFlag='1'">
                      <xsl:text> (Primary)</xsl:text>
                    </xsl:if>
                    <xsl:text>, </xsl:text>
                  </xsl:if>
                </xsl:for-each> -->
                <xsl:value-of select="$ImpactArea"/>
              </SPAN>
            </DIV>  
	   </TD>	  
	   <TD rowspan="2" valign="top">
	        <DIV class="autoflowDiv" style="width:140px; height:40px;overflow-x:hidden;overflow-y:auto">
            <xsl:variable name="PriorImpactArea">
              <xsl:for-each select="/Root/Vehicle/Impact[@ImpactID != '0' and @PriorImpactFlag='1']"><xsl:variable name="PriorImpactID"><xsl:value-of select="@ImpactID"/></xsl:variable><xsl:value-of select="/Root/Reference[@List='Impact' and @ReferenceID=$PriorImpactID]/@Name"/>, </xsl:for-each>
              <!-- <xsl:for-each select="/Root/Vehicle/Impact[@CurrentImpactFlag='0']">
                <xsl:value-of select="/Root/Reference[@List='Impact' and @ReferenceID=./@ImpactID]/@Name"/>
              </xsl:for-each> -->
            </xsl:variable>
              <SPAN id="txtPriorImpactArea" class="InputReadonlyField" style="width:100%; height:100%">
                <!-- <xsl:for-each select="Reference[@List='Impact']">
                  <xsl:if test="boolean(/Root/Vehicle/Impact[@ImpactID=current()/@ReferenceID and @PriorImpactFlag='1'])">
                    <xsl:value-of select="@Name"/>
                      <xsl:text>, </xsl:text>
                  </xsl:if>
                </xsl:for-each> -->
                <xsl:value-of select="$PriorImpactArea"/>
              </SPAN>
            </DIV>
	   </TD>  
       </TR>
	
      <TR>
	  <TD unselectable="on">VIN:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDescVIN',17,'VIN',.,'','',5)"/>
      </TD> 

      <TD unselectable="on" nowrap="">
        VIN (Est):
        <xsl:if test="@EstimateVIN != '' and @VIN != @EstimateVIN">				
          <img align="absbottom" src="/images/edit.gif" alt="Update VIN" width="14" height="14" border="0" onClick="updateVIN()" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)" >
            <xsl:if test="not(contains($myCRUD, 'U')) or ($myPageReadOnly = 'true')">
              <xsl:attribute name="disabled"/>
              <xsl:attribute name="readonly"/>
            </xsl:if>
          </img>
        </xsl:if>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDescEstimateVIN',17,'EstimateVIN',.,'',5,5)"/>
      </TD>  
     </TR>
	  <TR>
	  <TD unselectable="on">Color:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDescColor',10,'Color',.,'','',6)"/>
      </TD>  

      <TD unselectable="on" nowrap="">Est. Speed/Posted:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEstimatedSpeed',3,'ImpactSpeed',.,'','',13)"/> / 
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPostedSpeed',3,'PostedSpeed',.,'','',14)"/>            
      </TD>  
	  

       <TD unselectable="on" nowrap="">
	  <table border="0" cellspacing="0" cellpadding="0">
	  <tr>
       <td>Drivable</td>
       <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
       <td><xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('DriveableFlag',boolean(/Root/Vehicle[@DriveableFlag='1']),'1','0','','',19)"/></td>
	  </tr>
	  </table>
       </TD>

      <TD unselectable="on" align="left" class="autoFlowSpanTitle">Air Bags Deployed</TD>   

	  </TR>
	  
	  <TR>  
	  <TD unselectable="on" nowrap="">Odometer:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOdometer',10,'Mileage',.,'','',7)"/>
      </TD> 

	   <TD unselectable="on">
        License Plate:
       </TD>
       <TD unselectable="on" colspan="2" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtLicensePlate',7,'LicensePlateNumber',.,'','',15)"/>		
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
	       License State:
        <SPAN style="vertical-align:middle"> <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('LicensePlateState',2,'onSelectChange',string(@LicensePlateState),1,11,/Root/Reference[@List='State'],'ReferenceID','ReferenceID','60','75','','',24,1)"/> </SPAN>
	   </TD>
	    <TD unselectable="on" rowspan="2"  valign="top"><xsl:attribute name="nowrap"/> 
	     <DIV unselectable="on" style="z-index:1; width:160px; height:45px; overflow: auto;"> 
          <TABLE id='tblAirBags' unselectable="on" border="0" cellspacing="0" cellpadding="1">
            <xsl:for-each select="/Root/Reference[@List='SafetyDevice']">
              <xsl:sort data-type="number" select="@DisplayOrder" order="ascending" />
              <TR>
                <TD unselectable="on"><xsl:value-of select="@Name"/></TD>
                <TD unselectable="on" style="display:none"><xsl:value-of select="@ReferenceID"/></TD>
                <TD unselectable="on">
                  <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AirBag',boolean(/Root/Vehicle/SafetyDevice[@SafetyDeviceID=current()/@ReferenceID]),'1','0',position(),'',25)"/>
                </TD>
              </TR>
            </xsl:for-each>
          </TABLE>
          <INPUT type="hidden" id="txtSafetyDevicesDeployed" name="SafetyDevicesDeployed" value="" />
        </DIV>
       </TD>   

	 </TR>
	
	 <TR>
	  <TD unselectable="on">Book Value:</TD>
      <TD unselectable="on">
        <table border="0" cellspacing="0" cellpadding="0" >
          <tr>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtBookValue',8,'BookValueAmt',.,'','',8)"/>
              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            </td>
            <td>
              <IMG id="imgNADA" unselectable="on" src="/images/NADA1.gif" onClick="callNada()" border="0" style="display:none; FILTER:alpha(opacity=70); cursor:hand;" onmouseout="makeHighlight(this,1,70)" onmouseover="makeHighlight(this,0,100)" tabindex="9" valign="absbottom">
                  <xsl:if test="not(contains($myCRUD, 'U')) or ($myPageReadOnly = 'true')">
                      <xsl:attribute name="disabled"/>
                      <xsl:attribute name="readonly"/>
                  </xsl:if>
              </IMG>
            </td>		
          </tr>
        </table>
      </TD> 

	  <TD unselectable="on">Remarks to Rep: </TD>
	  <TD unselectable="on" colspan="2" valign="top">
<!-- 	    <DIV class="autoflowDiv" style="width:260px; height:24px;overflow-x:hidden;overflow-y:auto"> -->
          <!-- <SPAN id="txtPriorImpactArea" class="InputReadonlyField" style="width:100%; height:100%">	 -->
            <xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtRemarksToRep','0',2,'Remarks',.,'','',16)"/>
		  <!-- </SPAN> -->
<!-- 		</DIV> -->
       </TD>   

	</TR>
  </TABLE>
  </xsl:template>
  
  
  <!-- Gets the Shop Assign Info -->
  <xsl:template name="ShopAssignInfo" >
    <xsl:param name="InsuranceCompanyID"/>
    <IFRAME id="ifrmShopAssignment" src="blank.asp" onreadystatechange='top.IFStateChange(this);' style='width:720px; height:243px; border:0px; visibility:inherit; position:absolute; background:transparent; overflow:hidden' allowtransparency='true' >
      <!-- <xsl:attribute name="src">VehicleShopAssignment.asp?WindowID=<xsl:value-of select="$WindowID"/>&amp;InsuranceCompanyID=<xsl:value-of select="$InsuranceCompanyID"/>&amp;ClaimAspectID=<xsl:value-of select="/Root/Vehicle/@ClaimAspectID"/></xsl:attribute> -->
      
    </IFRAME>
  </xsl:template>

  
  <!-- Gets the Appraiser Info -->
  <xsl:template name="AppraiserInfo" >
  <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
    <TR>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        Appraiser Name:
      </TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAppName',25,'Name',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on">Address:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAppAddress1',20,'Address1',.)"/>
      </TD>
      <TD unselectable="on">E-mail:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAppEmail',20,'EmailAdsress',.)"/>
      </TD>
    </TR>
    <TR>
      <TD unselectable="on">Phone:</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAppPhoneAC',1,'PhoneAreaCode',.)"/> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAppPhoneEX',1,'PhoneExchangeNumber',.)"/> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAppPhoneUN',2,'PhoneUnitNumber',.)"/>
        <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAppPhoneXT',3,'PhoneExtensionNumber',.)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAppAddress2',20,'Address2',.)"/>
      </TD>
      <TD unselectable="on">Website:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAppWebsite',20,'WebSiteAddress',.)"/>
      </TD>
    </TR>
    <TR>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">City:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAppCity',15,'AddressCity',.)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    </TR>
    <TR>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">State:</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAppState',2,'AddressState',.,'',12)"/>
        Zip:
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAppZip',10,'AddressZip',.)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    </TR>
    <TR>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    </TR>
  </TABLE>
  </xsl:template>

  
  
  <!-- Gets the Vehicle Location Info -->
  <xsl:template name="VehicleLocationInfo" >
    <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0" width="690">
      <colgroup>
        <col width="80"/>
        <col width="200"/>
     </colgroup>  
	  <TR></TR>
	  <TR></TR>
	  <TR></TR>	    
      <TR>
      <TD unselectable="on">Location:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehLocName',25,'LocationName',.,'','',75)"/>
      </TD>
	  </TR>  
	  <TR></TR>
	  <TR></TR>  
	   
	  <TR>
      <TD unselectable="on">Address:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehLocAddress1',25,'LocationAddress1',.,'','',76)"/>	
      </TD>
	  </TR>		  
  	  <TR></TR>
	  <TR></TR>  
	  
	  <TR>
	  <TD unselectable="on">
	    <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
	  </TD> 
	  <TD unselectable="on">
	    <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehLocAddress2',25,'LocationAddress2',.,'','',77)"/>
  	  </TD>
	  </TR>  
  	  <TR></TR>
	  <TR></TR>  
	  
	  <TR>
      <TD unselectable="on">City:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehLocCity',19,'LocationCity',.,'','',78)"/>
      </TD>
	  </TR>  
  	  <TR></TR>
	  <TR></TR>  
	  
	  <TR>
      <TD unselectable="on">State:</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
	    <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('LocationState',2,'onSelectChange',string(@LocationState),1,11,/Root/Reference[@List='State'],'Name','ReferenceID','130','75','','',79,1)"/>	  
	  </TD>
	  </TR>
      <TR></TR>
	  <TR></TR>	   
	   
	  <TR>
      <TD unselectable="on"> Zip: </TD>
	  <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehLocZip',6,'LocationZip',.,'',10,80)"/>
       <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="GetCityState(this,'LocationState','txtVehLocCity');"></xsl:text>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      </TR>  
	  <TR></TR>
	  <TR></TR>	 
	     
	  <TR>
      <TD unselectable="on">Phone:</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVHPhoneAC',1,'LocationAreaCode',.,'',10,81)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtVHPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVHPhoneEX',1,'LocationExchangeNumber',.,'',10,82)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtVHPhoneUN, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVHPhoneUN',2,'LocationUnitNumber',.,'',10,83)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtVHPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
        <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVHPhoneXT',3,'LocationExtensionNumber',.,'',10,84)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
      </TD>
      </TR>
    </TABLE>
  </xsl:template>

 
  <!-- Gets the Involved Info -->
  <xsl:template name="InvolvedInfo" > 
   <!-- id content31Sel changed to content32Sel changed to 22 -->
    <DIV unselectable="on" id="content22Sel" class="selBox" style="z-index:1; width:716px; height:234px;">
      <DIV unselectable="on" style="position:relative; left:10px; top:-10px; z-index:100000">
        <xsl:variable name="InvolvedName">
          <xsl:if test="count(Vehicle/Involved[@InvolvedID &gt; 0]) > 0">
            <xsl:for-each select="Vehicle/Involved">
              <xsl:choose>
                <xsl:when test="@BusinessName!=''">
                  <xsl:value-of select="@BusinessName"/>
                </xsl:when>
                <xsl:when test="@BusinessName=''">
                  <xsl:value-of select="@NameFirst"/>
                  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                  <xsl:value-of select="@NameLast"/>
                </xsl:when>
              </xsl:choose>
              <xsl:text> [</xsl:text>
                <xsl:for-each select="InvolvedType">
                  <xsl:value-of select="@InvolvedTypeName"/>
                  <xsl:if test="not(position()=last())"><xsl:text>, </xsl:text></xsl:if>
                </xsl:for-each>
              <xsl:text>]</xsl:text>
              <xsl:if test="boolean(InvolvedType[@InvolvedTypeName = 'Insured'])">
                <xsl:text> **</xsl:text>
              </xsl:if>
              <xsl:if test="not(position()=last())"><xsl:text>|</xsl:text></xsl:if>
            </xsl:for-each>
          </xsl:if>
        </xsl:variable>
        <xsl:variable name="InvolvedIDs">
          <xsl:for-each select="Vehicle/Involved">
            <xsl:value-of select="@InvolvedID"/><xsl:text>,</xsl:text>
            <xsl:value-of select="@DayAreaCode"/><xsl:text>,</xsl:text>
            <xsl:value-of select="@DayExchangeNumber"/><xsl:text>,</xsl:text>
            <xsl:value-of select="js:escape_string(string(@AddressCity))"/><xsl:text>,</xsl:text>
            <xsl:value-of select="js:escape_string(string(@AddressState))"/><xsl:text>,</xsl:text>
            <xsl:value-of select="@AddressZip"/>
            <xsl:if test="not(position()=last())"><xsl:text>|</xsl:text></xsl:if>
          </xsl:for-each>
        </xsl:variable>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('VehicleInvolved',2,'onInvolvedChange',0,1,2,string($InvolvedName),'',string($InvolvedIDs),'','100')"/>
      </DIV>
      <IFRAME id="ifrmVehicleInvolved" src="blank.asp" onreadystatechange='top.IFStateChange(this);' style='width:704px; height:214px; border : 0px; visibility: hidden;position:absolute; background : transparent;overflow:hidden;' allowtransparency='true' >
        <!-- <xsl:choose>
          <xsl:when test="count(/Root/Vehicle/Involved[@InvolvedID != 0]) != 0">
            <xsl:attribute name="src">VehicleInvolved.asp?WindowID=<xsl:value-of select="$WindowID"/>&amp;InvolvedID=<xsl:value-of select="/Root/Vehicle/Involved[@InvolvedID != 0]/@InvolvedID"/></xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="src">blank.asp</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose> -->
      </IFRAME>
    </DIV>
  </xsl:template>

  
  <!-- Gets the Contact Info -->
  <xsl:template name="VehicleContactInfo" >
      <TABLE class="paddedTable" border="0" cellspacing="0" cellpadding="0" width="710">
        <TR>
          <TD unselectable="on">Name:</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNameTitle',2,'NameTitle',.,'',1,90)"/>
            <img src="/images/spacer.gif" alt="" width="2" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNameFirst',15,'NameFirst',.,'',1,91)"/>
            <img src="/images/spacer.gif" alt="" width="3" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNameLast',15,'NameLast',.,'',1,92)"/>
          </TD>
          <TD  unselectable="on"><xsl:attribute name="nowrap"/>Relation to Ins.:</TD>
          <TD colspan="2"><xsl:attribute name="nowrap"/>
            <xsl:variable name="rtContactRelToIns" select="@InsuredRelationID"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('InsuredRelationID_2',2,'onSelectChange',number($rtContactRelToIns),1,3,/Root/Reference[@List='ContactRelationToInsured'],'Name','ReferenceID','','','','',99)"/>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
         </TR>
		 
         <TR>
          <TD unselectable="on">Address:</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAddress1',30,'Address1',.,'',1,93)"/>
          </TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        </TR>
		
        <TR>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD><xsl:attribute name="nowrap"/>
          <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAddress2',30,'Address2',.,'',1,94)"/>
          </TD>
          <TD colspan="4" unselectable="on"><img src="images\background_top_flip.gif" width="204" /><img src="images\background_top.gif" width="204" /></TD>  
	   </TR>
	   
        <TR>
          <TD unselectable="on">City:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactCity',19,'AddressCity',.,'',1,95)"/>
          </TD>
          <TD><xsl:attribute name="nowrap"/>Best Number to Call:</TD>
          <TD><xsl:attribute name="nowrap"/>
            <xsl:variable name="rtBCP" select="@BestContactPhoneCD"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('BestContactPhoneCD',2,'onSelectChange',string($rtBCP),1,2,/Root/Reference[@List='BestContactPhone'],'Name','ReferenceID','','','','',100,1)"/>
         </TD>
          <TD unselectable="on">Day:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactDayAreaCode',2,'DayAreaCode',.,'',10,102)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactDayExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactDayExchangeNumber',2,'DayExchangeNumber',.,'',10,103)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactDayUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactDayUnitNumber',2,'DayUnitNumber',.,'',10,104)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactDayExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactDayExtensionNumber',3,'DayExtensionNumber',.,'',10,105)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
		
        <TR>
          <TD unselectable="on">State:</TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>
              <!--<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactState',2,'AddressState',.,'',12,96)"/> -->
  		       <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('AddressState',2,'onSelectChange',string(@AddressState),1,11,/Root/Reference[@List='State'],'Name','ReferenceID','130','75','','',96,1)"/>	  
          </TD>
          <TD><xsl:attribute name="nowrap"/>Best Time to Call:</TD>
          <TD>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactBestContactTime',20,'BestContactTime',.,'',1,101)"/>
          </TD>
          <TD unselectable="on">Night:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNightAreaCode',2,'NightAreaCode',.,'',10,106)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactNightExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNightExchangeNumber',2,'NightExchangeNumber',.,'',10,107)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactNightUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNightUnitNumber',2,'NightUnitNumber',.,'',10,108)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactNightExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactNightExtensionNumber',3,'NightExtensionNumber',.,'',10,109)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
		
        <TR>
		<TD unselectable="on">Zip:</TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactZip',10,'AddressZip',.,'',10,97)"/>
            <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="GetCityState(this,'AddressState','txtContactCity');"></xsl:text> 
          </TD>	     
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on">Alt.:</TD>
          <TD nowrap="">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAlternateAreaCode',2,'AlternateAreaCode',.,'',10,110)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactAlternateExchangeNumber, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAlternateExchangeNumber',2,'AlternateExchangeNumber',.,'',10,111)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactAlternateUnitNumber, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAlternateUnitNumber',2,'AlternateUnitNumber',.,'',10,112)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtContactAlternateExtensionNumber, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtContactAlternateExtensionNumber',3,'AlternateExtensionNumber',.,'',10,113)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
        </TR>
		
		<TR>
		<TD unselectable="on">E-mail:</TD>
          <TD unselectable="on">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmailAddress',30,'EmailAddress',.,'',10,98)"/><xsl:text disable-output-escaping="yes">onbeforedeactivate="checkEMail(this);"></xsl:text>
          </TD>
		</TR>
      </TABLE>
  </xsl:template>

  <!-- Gets the Estimates Info -->
  <xsl:template name="EstimatesInfo" >
    <xsl:param name="myCRUD"/>
  <DIV id="ESTScrollTable" style="width:100%;" >
    <TABLE unselectable="on" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblEstimates')" width="100%" border="1" cellspacing="0">
      <TBODY>
        <TR class="QueueHeader" style="height:22px">
          <TD unselectable="on" class="TableSortHeader" type="String" title="Sort">Rec/Attach Date</TD>
          <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="99" style="cursor:default;">Summary</TD>
          <TD unselectable="on" class="TableSortHeader" type="String" title="Sort">Type</TD>
          <TD unselectable="on" class="TableSortHeader" type="String" title="Sort">Source</TD>
          <TD unselectable="on" class="TableSortHeader" type="String" sIndex="99" style="cursor:default;">Gross Total</TD>
          <TD unselectable="on" class="TableSortHeader" type="String" sIndex="99" style="cursor:default;">Net Total</TD>
          <TD unselectable="on" class="TableSortHeader" type="String" sIndex="99" style="cursor:default;">Orig/Audit</TD>
          <TD unselectable="on" class="TableSortHeader" type="String" sIndex="99" style="cursor:default;">Duplicate</TD>
          <TD unselectable="on" class="TableSortHeader" type="String" sIndex="99" style="cursor:default;">Price Agreed</TD>
        </TR>
      </TBODY>
    </TABLE>
    <DIV unselectable="on" class="autoflowTable" style="height:216px;">
     <TABLE unselectable="on" id="tblEstimates" class="GridTypeTable" width="100%" cellspacing="1" border="0" cellpadding="0">
       <TBODY bgColor1="ffffff" bgColor2="fff7e5">
     <xsl:choose>
        <xsl:when test="contains($myCRUD, 'R')">
            <xsl:for-each select="Vehicle/Document[@DocumentID &gt; 0 and @EstimateTypeFlag = '1']"  >
              <xsl:sort select="@DocumentID" data-type="number" order="descending"/>
              <xsl:call-template name="EstimatesList"/>
            </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
            <div style="width:100%;text-align:center">
            <br/>
            <font color="#FF0000">You do not have permissions to view this tab.</font>
            <br/>
            Please contact your supervisor.
            </div>
        </xsl:otherwise>            
     </xsl:choose>
       </TBODY>
     </TABLE>
    </DIV>
	</DIV>
  </xsl:template>

  
  <xsl:template name="EstimatesList">
    <TR dirty="false" onMouseOut="GridMouseOut(this)" onMouseOver="GridMouseOver(this, 0)">
    <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
    <xsl:attribute name="DocumentID"><xsl:value-of select="@DocumentID"/></xsl:attribute>
      <TD height="23" unselectable="on" class="GridTypeTD" width="20%" style="text-align:left; padding-left:2px;" nowrap="">
        <xsl:variable name="chkCreatedDate" select="@CreatedDate"/>
        <xsl:choose>
          <xsl:when test="contains($chkCreatedDate, '1900')">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="js:UTCConvertDate(string(current()/@CreatedDate))" />
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:value-of select="js:UTCConvertTime(string(current()/@CreatedDate))"/>
          </xsl:otherwise>            
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="10%" nowrap="">
        <xsl:choose>
          <xsl:when test="@FullSummaryExistsFlag = 1">
            <img unselectable="on" src="/images/ElectronicEst.gif" border="0" width1="24" height1="14"
              title="Electronic Summary. Click to open full summary screen." style="FILTER1:alpha(opacity=60); cursor:hand;"
              onmouseout1="makeHighlight(this,1,60)" onmouseover1="makeHighlight(this,0,100)">
              <xsl:attribute name="onClick">ShowEstimate('<xsl:value-of select="@DocumentID"/>')</xsl:attribute>
            </img>
          </xsl:when>
          <xsl:otherwise>
            <img unselectable="on" src="/images/ManualEst.gif" border="0" width1="15" height1="15"
              title="Manual Summary. Click to open quick summary screen." style="FILTER1:alpha(opacity=60); cursor:hand;"
              onmouseout1="makeHighlight(this,1,60)" onmouseover1="makeHighlight(this,0,100)">
              <xsl:attribute name="onClick">ShowQuickEstUpdWindow(this.parentElement.parentElement, '<xsl:value-of select="@DocumentID"/>', '<xsl:value-of select="@ImageLocation"/>')</xsl:attribute>
            </img>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="12%" nowrap="">
        <xsl:choose>
          <xsl:when test="@DocumentTypeName = 'Supplement'">
            <xsl:value-of select="@DocumentTypeName"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="@SupplementSeqNumber"/>
          </xsl:when>
          <xsl:otherwise>
        		<xsl:value-of select="@DocumentTypeName"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="8%" nowrap="">
        <xsl:value-of select="@DocumentSourceName"/>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="12%" style="text-align:right; padding-right:2px;" nowrap="">
        <xsl:if test="@GrossEstimateAmt = ''">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </xsl:if>
        <xsl:value-of select="@GrossEstimateAmt"/>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="12%" style="text-align:right; padding-right:2px;" nowrap="">
        <xsl:if test="@NetEstimateAmt = ''">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </xsl:if>
        <xsl:value-of select="@NetEstimateAmt"/>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="10%" nowrap="">
        <xsl:if test="@EstimateTypeCD = 'A'">Audited</xsl:if>
        <xsl:if test="@EstimateTypeCD = 'O'">Original</xsl:if>
        <xsl:if test="@EstimateTypeCD = ''">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </xsl:if>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="8%" nowrap="">
        <xsl:if test="@DuplicateFlag = '1'">Yes</xsl:if>
        <xsl:if test="@DuplicateFlag = '0'">No</xsl:if>
        <xsl:if test="@DuplicateFlag = ''">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </xsl:if>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="8%" nowrap="">
        <xsl:if test="@AgreedPriceMetCD = 'Y'">Yes</xsl:if>
        <xsl:if test="@AgreedPriceMetCD = 'N'">No</xsl:if>
        <xsl:if test="@AgreedPriceMetCD = ''">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </xsl:if>
      </TD>
    	<TD unselectable="on" style="display:none;">
        <xsl:value-of select="@FullSummaryAvailableFlag"/>
      </TD>
    	<TD unselectable="on" style="display:none;">
        <xsl:value-of select="@SysLastUpdatedDateDocument"/>
      </TD>
    	<TD unselectable="on" style="display:none;">
        <xsl:value-of select="@SysLastUpdatedDateEstimate"/>
      </TD>
    </TR>
  </xsl:template>

  <xsl:template name="VehicleRental">
      <TABLE unselectable="on" border="0" cellspacing="2" cellpadding="0" width="700">
        <TR>
          <TD unselectable="on" unwrap="">Rental Days Authorized:</TD>
          <TD unselectable="on">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtRental',4,'RentalDaysAuthorized',.,'','',120)"/>
          </TD>
		 </TR>
		 <TR></TR>		
		 <TR>
          <TD unselectable="on">Instructions:</TD>
          <TD unselectable="on">
            <xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtRentalInstructions',100,2,'RentalInstructions',.,'','',121)"/>
          </TD>
        </TR>
      </TABLE>
  </xsl:template>

</xsl:stylesheet>
