<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimLossType">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ClaimCRUD" select="Claim"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="SelectionText"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL    PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2.0\webroot\,{6D65EFC5-52B6-4302-84C4-080E14C8900B},19,uspClaimLossTypeGetDetailXML,ClaimLossType.xsl   -->

<HEAD>
<TITLE>Claim Loss Type</TITLE>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/CoolSelect.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/utilities.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>


<SCRIPT language="JavaScript">
	var gsClaimCRUD = '<xsl:value-of select="$ClaimCRUD"/>';
    var gsSelect = '<xsl:value-of select="js:cleanString(string($SelectionText))"/>';

<![CDATA[

  function pageInit(){
    var sLossTypeText = gsSelect;
    var selIdx;
    var arrLossType;
        if (sLossTypeText.indexOf(String.fromCharCode(160)) != -1)
          arrLossType = sLossTypeText.split(String.fromCharCode(160) + "-" + String.fromCharCode(160))
        else
          arrLossType = sLossTypeText.split(" - ");

        switch (arrLossType.length) {
          case 1 :
            selIdx = getOptionIndex(selLossType_1, arrLossType[0].Trim());
            if (selIdx != -1) selLossType_1.optionselect(null, selIdx);
            break;
          case 2 :
            selIdx = getOptionIndex(selLossType_1, arrLossType[0].Trim());
            if (selIdx != -1) selLossType_1.optionselect(null, selIdx);

            selIdx = getOptionIndex(LossType_2, arrLossType[1].Trim());
            if (selIdx != -1) LossType_2.optionselect(null, selIdx);
            break;
          case 3 :
            selIdx = getOptionIndex(selLossType_1, arrLossType[0].Trim());
            if (selIdx != -1) selLossType_1.optionselect(null, selIdx);

            selIdx = getOptionIndex(LossType_2, arrLossType[1].Trim());
            if (selIdx != -1) LossType_2.optionselect(null, selIdx);

            selIdx = getOptionIndex(LossType_3, arrLossType[2].Trim());
            if (selIdx != -1) LossType_3.optionselect(null, selIdx);
            break;
        }
  }

  function getOptionIndex(selObj, selText) {
    if ((selObj != null) && (selText != '')) {
      var iLength = selObj.options.length;
      for (var i = 0; i < iLength; i++)
        if (selObj.options[i].innerText == selText)
          return i;
    }
    return -1;
  }

  function OkLossType() {
    window.returnValue = document.getElementById('LTDisplay').innerText+"||"+LossTypeID.value;
    close();
  }
  // Loss Type level 1 change
  function onLTChange1(selobj)
  {
    var RefID = selobj.options[selobj.selectedIndex].value;
    document.getElementById('LTDisplay').innerText = selobj.options[selobj.selectedIndex].innerText;
    var strXPath = '//Reference[@List="LossType"][@ParentID="' + RefID + '"]';
    xmlReference.setProperty("SelectionLanguage", "XPath");
    var objList = xmlReference.documentElement.selectNodes( strXPath );

    var objNextSel = document.getElementById( "LossType_2" );
    replaceSelectListContents(objNextSel , objList, 0, "@Name", "@ReferenceID" );
    LossTypeID.value = RefID;

    onLTChange2(objNextSel);
    if (RefID == 0)  //If Unknown, blank out the third drop-down
    {
      var objNextNextSel = document.getElementById( "LossType_3" );
      replaceSelectListContents(objNextNextSel , objList, 0, "@Name", "@ReferenceID" );
    }
  }

  // Loss Type level 2 change
  function onLTChange2(selobj)
  {
    // need to make sure there is something the select
    if (selobj.options.length > 0)
    {
      var RefID = selobj.options[selobj.selectedIndex].value;
      var strValue = selobj.options[selobj.selectedIndex].innerText;
      if (strValue != "")
      {
        var selLT1 = document.getElementById( "selLossType_1" );
        document.getElementById('LTDisplay').innerText = selLT1.options[selLT1.selectedIndex].innerText +
          String.fromCharCode(160) + "-" + String.fromCharCode(160) + strValue;
        var strXPath = '//Reference[@List="LossType"][@ParentID="' + RefID + '"]';
        xmlReference.setProperty("SelectionLanguage", "XPath");
        var objList = xmlReference.documentElement.selectNodes( strXPath );

        var objNextSel = document.getElementById( "LossType_3" );
        replaceSelectListContents(objNextSel, objList, 0, "@Name", "@ReferenceID" );
        LossTypeID.value = RefID;

        onLTChange3(objNextSel);
      }
    }
  }

  // Loss Type level 3 change
  function onLTChange3(selobj)
  {
    if (selobj.options.length > 0)
    {
      var RefID = selobj.options[selobj.selectedIndex].value;
      var strValue = selobj.options[selobj.selectedIndex].innerText;
      if (strValue != "")
      {
        var selLT1 = document.getElementById( "selLossType_1" );
        var selLT2 = document.getElementById( "LossType_2" );
        document.getElementById('LTDisplay').innerText =selLT1.options[selLT1.selectedIndex].innerText +
          String.fromCharCode(160) + "-" + String.fromCharCode(160) +
          selLT2.options[selLT2.selectedIndex].innerText +
          String.fromCharCode(160) + "-" + String.fromCharCode(160) +
          strValue;
        LossTypeID.value = RefID;
      }
    }
  }
]]>
</SCRIPT>
</HEAD>
<BODY unselectable="on" class="bodyAPDSub" style="margin:0px 0px 0px 0px; padding:10px" onLoad="initSelectBoxes();InitFields();pageInit()">

  <!-- Loss Type XML Data -->
  <DIV unselectable="on" style="background-color:#FFFFFF; border:1px solid #333377; padding:10px;">
    <xml id="xmlReference"><Root><xsl:copy-of select="/Root/Reference[@List='LossType']"/></Root></xml>
    <xsl:call-template name="LossType"/>
  </DIV>

</BODY>
</HTML>
</xsl:template>

<xsl:template name="LossType">
      <DIV id="LossType" style="width:350px; height:80px;">
        <TABLE border="0" cellspacing="0" cellpadding="3">
        <TR valign="middle">
          <TD nowrap="">1.<img src="/images/spacer.gif" alt="" width="5" height="1" border="0"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('LossType_1',2,'onLTChange1',0,1,5,/Root/Reference[@List='LossType' and not(@ParentID)],'Name','ReferenceID',0,95)"/>
          </TD>
        </TR>
        <TR>
          <TD>2.<img src="/images/spacer.gif" alt="" width="15" height="1" border="0"/>
            <SCRIPT>
              addSelectCustomValues('LossType_2',2,'onLTChange2',' ',1,2,1,gsClaimCRUD,' ',' ',250,80);
            </SCRIPT>
          </TD>
        </TR>
        <TR>
          <TD>3.<img src="/images/spacer.gif" alt="" width="25" height="1" border="0"/>
            <SCRIPT>
              addSelectCustomValues('LossType_3',2,'onLTChange3',' ',1,0,1,gsClaimCRUD,' ',' ',250,70);
            </SCRIPT>
          </TD>
        </TR>
        <TR>
          <TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </TD>
        </TR>
        <TR>
          <TD>Loss Type Selection:
          </TD>
        </TR>
        <TR style="height:36px">
          <TD class="InputField"><SPAN id="LTDisplay"></SPAN></TD>
        </TR>
        <TR>
          <TD><xsl:attribute name="nowrap"/>
            <img src="/images/spacer.gif" alt="" width="1" height="4" border="0"/>
          </TD>
        </TR>
        <TR align="right">
          <TD><xsl:attribute name="nowrap"/>
            <INPUT type="button" name="OkThis" value="OK"  class="formbutton" onClick="OkLossType();"/>
            <img src="/images/spacer.gif" alt="" width="15" height="1" border="0"/>
            <INPUT type="button" name="CancelThis" value="Cancel"  class="formbutton" onClick="window.returnValue='';window.close()"/>
          </TD>
        </TR>
      </TABLE>
      <INPUT id="txtLossType" size="10" value=""  type="hidden" name="LossTypeID" />
      </DIV>
</xsl:template>
</xsl:stylesheet>
