<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTDealerOwnedShops">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserID"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTDealerBusinessGetListXML,SMTDealerBusinessList.xsl, 887   -->

<HEAD>
  <TITLE>Search Results</TITLE>
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>

  <style>
  	A{
		color:blue
	}
	A:hover{
		color : #B56D00;
		font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
		font-weight : bold;
		font-size : 10px;
		cursor : hand;
	}
  </style>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
	var gsPageFile = "SMTDealerBusinessList.asp";
	var gsShopCRUD = '<xsl:value-of select="$ShopCRUD"/>';

<![CDATA[

function initPage(){
	top.gsPageFile = gsPageFile;
	parent.tab12.className = "tabactive1";
	parent.btnSave.style.visibility = gsShopCRUD.indexOf("U") > -1 ? "visible" : "hidden";
  
  var aInputs = null;
  aInputs = document.all.tags("INPUT");
  for (var i=0; i < aInputs.length; i++){    
    if (aInputs[i].type=="checkbox")
	  aInputs[i].attachEvent("onclick", Setdirty);    
  }
}

function Setdirty(){
	parent.gbDirtyFlag = true;
}

function GridMouseOver(oObject){
	saveBG = oObject.style.backgroundColor;
	oObject.style.backgroundColor="#FFEEBB";
	oObject.style.color = "#000066";
}

function GridMouseOut(oObject){
	oObject.style.backgroundColor=saveBG;
	oObject.style.color = "#000000";
}

function Save(){
	parent.btnSave.disabled = true;
	frmOwnedBus.action += "?mode=save&CallBack=" + txtCallBack.value;
  frmOwnedBus.submit();
  parent.gbDirtyFlag = false;
}

var giCheckCount = 0;
function CountChecks(obj){
  if (obj.checked == true) 
    giCheckCount++;
  else 
    giCheckCount--;
    
  parent.btnSave.disabled = giCheckCount > 0 ? false : true;
}

]]>
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" style="background:transparent;" onLoad="initPage();">

<DIV id="CNScrollTable">
<TABLE unselectable="on" id="tblHead" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSort1')" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
  <TBODY>
    <TR unselectable="on" class="QueueHeader">
	  <TD unselectable="on" class="TableSortHeader" sIndex="99" width="52" nowrap="nowrap" style="cursor:default; height:20;"> Remove </TD>
	  <TD unselectable="on" class="TableSortHeader" sIndex="99" width="52" nowrap="nowrap" style="cursor:default;"> ID </TD>
      <TD unselectable="on" class="TableSortHeader" sIndex="99" width="248" nowrap="nowrap" style="cursor:default;"> Name </TD>
      <TD unselectable="on" class="TableSortHeader" sIndex="99" width="226" nowrap="nowrap" style="cursor:default;">Address</TD>
      <TD unselectable="on" class="TableSortHeader" sIndex="99" width="124" nowrap="nowrap" style="cursor:default;"> City, State </TD>
    </TR>
  </TBODY>
</TABLE>

<DIV unselectable="on" id="divResultList" class="autoflowTable" style="position:relative; top:0; height:455px;">
  <input type="hidden" id="txtCallBack" name="CallBack"/>
  <form id="frmOwnedBus" method="post">
  <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" border="0" cellspacing="1" cellpadding="0" style="table-layout:fixed;">
    <TBODY bgColor1="ffffff" bgColor2="fff7e5">
      <xsl:for-each select="Shop" >
	    <xsl:call-template name="Shop">
        <xsl:with-param name="UserID" select="$UserID"/>
        <xsl:with-param name="ShopCRUD" select="$ShopCRUD"/>
      </xsl:call-template>
      </xsl:for-each>
    </TBODY>
  </TABLE>
  </form>
</DIV>

</DIV>
</BODY>
</HTML>
</xsl:template>

<!-- Gets the search results -->
<xsl:template name="Shop">
  <xsl:param name="UserID"/>
  <xsl:param name="ShopCRUD"/>

  <TR unselectable="on" onMouseOut="GridMouseOut(this)" onMouseOver="GridMouseOver(this)">
    <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
	<td unselectable="on" class="GridTypeTD" width="50">
	  <input type="checkbox" name="chkBusinessInfo" onclick="CountChecks(this)">
      <xsl:attribute name="value"><xsl:value-of select="position()"/></xsl:attribute>
      <xsl:if test="contains($ShopCRUD, 'U') = false"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
    </input>
	  <input type="hidden" name="BusinessInfoID"><xsl:attribute name="value"><xsl:value-of select="@BusinessInfoID"/></xsl:attribute></input>
	  <input type="hidden" name="DealerID"><xsl:attribute name="value"><xsl:value-of select="@DealerID"/></xsl:attribute></input>
	  <input type="hidden" name="SysLastUserID"><xsl:attribute name="value"><xsl:value-of select="$UserID"/></xsl:attribute></input>
	  <input type="hidden" name="SysLastUpdatedDate"><xsl:attribute name="value"><xsl:value-of select="@SysLastUpdatedDate"/></xsl:attribute></input>
  </td>
    <TD unselectable="on" class="GridTypeTD" width="50" style="text-align:left">
      <xsl:choose>
	    <xsl:when test = "@BusinessInfoID != ''">
		  <xsl:value-of select="@BusinessInfoID"/>
		</xsl:when>
	    <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
	  </xsl:choose>
	</TD>
    <TD unselectable="on" width="246" class="GridTypeTD" style="text-align:left">
      <xsl:choose>
	    <xsl:when test = "@Name != ''">
		  <a>
		    <xsl:attribute name="href">
			  javascript:parent.window.navigate("SMTDetailLevel.asp?SearchType=B&amp;BusinessInfoID=<xsl:value-of select='@BusinessInfoID'/>")
			</xsl:attribute>
		    <xsl:value-of select="@Name"/>
		  </a>
		</xsl:when>
		<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
	  </xsl:choose>
    </TD>
    <TD unselectable="on" width="225" class="GridTypeTD" style="text-align:left">
	  <xsl:choose>
		<xsl:when test = "@Address1 != ''"><xsl:value-of select="@Address1"/></xsl:when>
		<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
	  </xsl:choose>
    </TD>
    <TD unselectable="on" width="125" class="GridTypeTD" style="text-align:left">
	  <xsl:value-of select="@AddressCity"/>
	  <xsl:if test="@AddressCity != '' and @AddressState != ''">,<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:if>
	  <xsl:value-of select="@AddressState"/>
	  <xsl:if test="normalize-space(@AddressCity) = '' and normalize-space(@AddressState) = ''"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:if>
    </TD>
  </TR>
</xsl:template>

</xsl:stylesheet>
