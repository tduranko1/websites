<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="UserList">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function formatDateTime(strDate)
     {
        var strRet = "";
        var strTmp = "";
        var aTmp;
        if (strDate != "") {
            strTmp = strDate.split("T")[0];
            aTmp = strTmp.split("-");
            strRet = aTmp[1] + "/" + aTmp[2] + "/" + aTmp[0];
            strTmp = strDate.split("T")[1];
            aTmp = strTmp.split(":");
            strRet += " " + aTmp[0] + "/" + aTmp[1] + "/" + aTmp[2];
        }
        return strRet;
     }
  ]]>
</msxsl:script>

<xsl:template match="/Root">
<html>
<head>
<!-- STYLES -->
<LINK rel="stylesheet" href="../css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="../css/tabs.css" type="text/css"/>

<!-- Script modules -->
<SCRIPT language="JavaScript" src="/js/tabs.js"/>
<SCRIPT language="JavaScript" src="/js/formvalid.js"/>
<SCRIPT language="JavaScript" src="/js/tablesort.js"/>
<SCRIPT language="JavaScript" src="/js/grid.js"/>
<SCRIPT language="JavaScript" src="/js/images.js"/>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
<![CDATA[
	// button images for ADD/DELETE/SAVE
	preload('buttonAdd','/images/but_ADD_norm.png');
	preload('buttonAddDown','/images/but_ADD_down.png');
	preload('buttonAddOver','/images/but_ADD_over.png');
	preload('buttonDel','/images/but_DEL_norm.png');
	preload('buttonDelDown','/images/but_DEL_down.png');
	preload('buttonDelOver','/images/but_DEL_over.png');
	preload('buttonSave','/images/but_SAVE_norm.png');
	preload('buttonSaveDown','/images/but_SAVE_down.png');
	preload('buttonSaveOver','/images/but_SAVE_over.png');


  function PageInit() {
    var sCRUD = parent.sCRUDInfo;
    if ((sCRUD == undefined) || (sCRUD.indexOf("C") == -1) || (sCRUD.indexOf("R") == -1)){
        ClientWarning("Access Denied to view Role Information. Please contact your supervisor");
        window.navigate("../blank.asp");
    }
  }
  
  function handleJSError(sFunction, e)
  {
    var result = "An error has occurred on the page.\nFunction = " + sFunction + "\n";
    for (var i in e) {
      result += "e." + i + " = " + e[i] + "\n";
    }
    ClientError(result);
  }

  function RoleGridSelect(oRow)
  {
    try {
  	  var strRoleID = oRow.cells[3].innerText;
  	  parent.document.location="RoleDetail.asp?RoleID=" + strRoleID;
    }
    catch(e) {
      handleJSError("RoleGridSelect", e)
    }
  }

	var inADS_Pressed = false;		// re-entry flagging for buttons pressed
	// Handle click of Add button
	function ADS_Pressed(sAction, sName)
	{
    try {
  		if (inADS_Pressed == true) return;
  		inADS_Pressed = true;

  		var oDiv = document.getElementById(sName);

  		if (sAction == "Insert") {
  			if (oDiv.Many == "true") {
  				parent.oIframe.frameElement.src = "/admin/UserDetail.asp?UserID=New";
  			}
  		}
      else {
        var userError = new Object();
        userError.message = "Unhandled ADS action: Action = " + sAction;
        userError.name = "MiscellaneousException";
        throw userError;
			}

  		inADS_Pressed = false;
    }
    catch(e) {
      handleJSError("ADS_Pressed", e);
    }
	}
    
    
]]>
</SCRIPT>

<title>APD User and Role Management</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
</head>

<BODY unselectable="on" class="bodyAPDSub" onLoad="InitFields();PageInit();" style="overflow:hidden;background:transparent;">
<!-- Start Tabs1 -->
<DIV unselectable="on" style="position:absolute; visibility: visible; top: 0px; left: 0px">
  
  <!-- Start Role List -->
        <TABLE class="UserMiscInputs" onClick="sortColumn(event, 'tblRoles')" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center" width="720px">
            <colgroup>
                <col width="81px"/>
                <col width="330px"/>
                <col width="270px"/>
            </colgroup>
            <TBODY>
        		<tr class="QueueHeader" style="height:21px">
         			<td class="TableSortHeader" type="String">Enabled</td>
         			<td class="TableSortHeader" type="String">Role Name</td>
         			<td class="TableSortHeader" type="String">Business Unit</td>
        		</tr>
            </TBODY>
        </TABLE>
        <DIV unselectable="on" style="width:720px; height:460px; overflow: auto;">
        	<TABLE id="tblRoles" class="GridTypeTable" cellspacing="1" border="0" cellpadding="3">
            <colgroup>
                <col width="81px"/>
                <col width="335px"/>
                <col width="257px"/>
            </colgroup>
        	<TBODY bgColor1="ffffff" bgColor2="fff7e5">
                <xsl:for-each select="/Root/Role">
                    <TR onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' onClick='RoleGridSelect(this)' style='height:18px;'>
                        <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
                        <!-- <xsl:choose>
                            <xsl:when test="position() mod 2 = 0">
                                <xsl:attribute name="bgcolor">#fff7e5</xsl:attribute>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="bgcolor">#ffffff</xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose> -->
                        <td align="center" style="border:1px ridge;">
                            <xsl:choose>
                                <xsl:when test="@EnabledFlag = 1">
                                    <xsl:text>Yes</xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>No</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td style="border:1px ridge;"><xsl:value-of select="@Name"/></td>
                        <td align="center" style="border:1px ridge;">
                            <xsl:choose>
                                <xsl:when test="@BusinessCD = 'C'">
                                    <xsl:text>Claim Unit</xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>Shop Management Unit</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
     			        <td style="display:none"><xsl:value-of select="@RoleID"/> </td>
                    </TR>
                </xsl:for-each>
             </TBODY>
             </TABLE>
        </DIV>
</DIV>
</BODY>
</html>
</xsl:template>

</xsl:stylesheet>
