<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace">

<msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
        /*********************************************************
        *  This function will process the next node and return the
        *  value of the node in a suitable 
        **********************************************************/
        
        function getPageInfo( nodeList )
        {
            //var ndeType = nde.dataType;
            var nde = nodeList.nextNode().firstChild;
            var ndeType = nde.nodeType;
            if (ndeType == 4) { //CDATA
                var retVal = "\n" + stripEmptyLines(nde.nodeTypedValue);
                return retVal;
            }
            //else
            //    return nde.nodeTypedValue;
        }
        
        /*********************************************************
        *  This function will chop the top and bottom empty lines
        *  so that the page break does not extend to the next page.
        **********************************************************/
        function stripEmptyLines(str){
            var tmpArr;
            var tmpLine = "                                                                                ";

            str = str.replace(/\t/g, "");
            tmpArr = str.split("\n");
            
            var arrLen = tmpArr.length;
            
            //chop the top empty lines
            for (var i = 0; i < arrLen; i++) {
                if (tmpLine.indexOf(tmpArr[0]) != -1)
                    tmpArr.splice(0, 1);
                else 
                    break;
            }

            //chop the bottom empty lines
            for (var i = tmpArr.length - 1; i >= 0; i--) {
                if (tmpLine.indexOf(tmpArr[tmpArr.length-1]) != -1)
                    tmpArr.splice(tmpArr.length-1, 1);
                else
                    break;
            }
            
            return tmpArr.join("\n");
        }
        
  ]]>
</msxsl:script>
    
    <xsl:template match="/PrintImage">
        <html>
            <head>
                <title>APD XML Viewer</title>
                <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
                <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
                <style>
                    .myPage {
                        border: 1px solid #000000;
                        border-bottom:3px solid #000000;
                        border-right:3px solid #000000;
                        background-color: #FFF8DC;
                        padding:15px;
                    }
                    .pageContent {
                        background-color: #FFFFFF;
                         height:100%;
                   }
                    .pagePrint {
                        width:100%;
                        background-color: #FFFFFF;
                        border:0px;
                    }
                    pre {
                        font-family: Courier New;
                        font-size: 10pt;
                        cursor:default;
                        width: 100%;
                        background: #FFFFFF;
                    }
                    .toolbarButton {
                      width: 24px;
                      height: 24px;
                      cursor:arrow;
                      background-color: #FFFFFF;
                      border:0px;
                    }
                    .toolbarButtonOver {
                      width: 24px;
                      height: 24px;
                      cursor:arrow;
                      background-color: #FFFFFF;
                      border-top:1px solid #D3D3D3;
                      border-left:1px solid #D3D3D3;
                      border-bottom:1px solid #696969;
                      border-right:1px solid #696969;
                    }
                
                    .toolbarButtonOut {
                      width: 24px;
                      height: 24px;
                      cursor:arrow;
                      background-color: #FFFFFF;
                      border:0px;
                    }

                </style>

    <SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
    <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
    <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
    </script>

          
                <script language="javascript">
                    var numPages = <xsl:value-of select="count(Page)"/>;
                    var zoomVal = 100;
                    var curPage = 0;

                    <![CDATA[
                    function zoomIn(){
                        if (zoomVal < 100) {
                            zoomVal += 10;
                            doc.style.zoom = zoomVal + "%";
                        }
                    }
            
                    function zoomOut(){
                        if (zoomVal > 40) { //limit this to a 40% zoomout.
                            zoomVal -= 10;
                            doc.style.zoom = zoomVal + "%";
                        }
                    }
            
                    function goToPage(val){
                        switch(val){
                            case 1:
                                curPage = 1;
                                break;
                            case 2:
                                if (curPage > 1)
                                    curPage--;
                                break;
                            case 3:
                                if (curPage < numPages)
                                    curPage++;
                                break;
                            case 4:
                                curPage = numPages;
                                break;
                        }
                        var pg = document.getElementById("pg" + curPage);
                        if (pg){
                            //toolbar.style.visibility = "hidden";
                            //pg.style.visibility = "hidden";
                            pg.scrollIntoView(true);
                            //window.scroll(0, 0);
                            //toolbar.style.visibility = "visible";
                            //pg.style.visibility = "visible";
                            displayPageInfo();
                        }
                    }

                    function initPage4Printing() {
                        var pg;
                        for (i = 1; i <= numPages; i++){
                            pg = document.getElementById("pg" + i);
                            if (pg) {
                                pg.className = "pagePrint";
                                if (i < numPages)
                                    pg.style.pageBreakAfter = "always";

                                pg.firstChild.className = "pagePrint";
                            }
                        }
                        document.title = "";
                        //btnPrint.style.display = "none";
                        //btnClose.style.display = "none";
                        toolbar.style.display = "none";
                        divDoc.style.overflow = "";
                        divDoc.style.height = "";
                        divDoc.style.width = "";
                        //window.scroll(0,0);
                    }
                    
                    function initPage4Screen(){
                        var pg;
                        for (i = 1; i <= numPages; i++){
                            pg = document.getElementById("pg" + i);
                            if (pg) {
                                pg.className = "myPage";
                                pg.style.pageBreakAfter = "auto";
                                pg.firstChild.className = "";
                            }
                        }
                        document.title = "APD XML Viewer";
                        toolbar.style.display = "inline";
                        divDoc.style.overflowY = "auto";
                        divDoc.style.overflowX = "hidden";
                        //divDoc.style.height = document.body.clientHeight - toolbar.clientHeight - 30;
                        divDoc.style.height = "100%";
                        divDoc.style.width = document.body.clientWidth;
                        toolbar.style.margin = "2px";
                        //divDoc.style.width = "100%";
                        //window.scroll(0,0);
                    }
                    
                    function printDoc(){
                        btnPrint.disabled = true;
                        window.print();
                        tb_onmouseout(btnPrint);
                        window.setTimeout("btnPrint.disabled = false", 1500);
                    }
                    
                    function processPageText(str){
                        var strRet = str;
                        var strLine = "";
                        var arrLines = str.split("\n");
                        var iCount = 2;
                        for (var i = arrLines.length - 1; i >= 0; i--) {
                            if (arrLines[i].replace(/\s/g, "") == "") {
                                arrLines.splice(i, 1);
                                iCount--;
                                if (iCount == 0)
                                    break;
                            }
                        }
                        strRet = arrLines.join("\n");
                        return strRet;
                    }
                    
                    var bSavingFile = false;
                    var xmlDoc = null;
                    var sFileName = "";
                    function saveFile(){
                      try {
                        var docURL = document.URLUnencoded;
                        if (docURL.substr(docURL.length-4, 4).toLowerCase() == ".xml") {
                          var doc = docURL.substr(docURL.lastIndexOf("\\\\"), docURL.length);
                          var fso = new ActiveXObject("Scripting.FileSystemObject");
                          if (fso) {
                            if (!fso.FileExists(doc)){
                              ClientWarning("File does not exist.<br>" + doc);
                              return;
                            }
                            if (fso.GetExtensionName(doc) != "xml") {
                              ClientWarning("File is not a valid XML file.");
                              return
                            }
                            sFileName = fso.GetBaseName(doc) + ".doc";
                          }
                          xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
                          xmlDoc.async = true;
                          xmlDoc.onreadystatechange = xmlDocReady;
                          xmlDoc.load(doc);
                          tb_onmouseout(btnSave1);
                          toolbar.disabled = true;
                          btnSave1.disabled = true;
                          divSave.style.display = "inline";
                          goToPage(1);
                          window.scroll(0, 0);
                        }
                      } catch(e) {
                        ClientError("An error occured while converting XML to MS Word document. [" + e.description + "]");
                      }
                    }
                    
                    function xmlDocReady(){
                      if (xmlDoc && xmlDoc.readyState == 4){ //completely loaded
                        window.setTimeout("xml2Doc()", 10);
                      }
                    }
                    
                    function xml2Doc(){
                      try {
                        var objNodeList = xmlDoc.documentElement.selectSingleNode("/PrintImage");
                        var pgContent = "";
                        if (objNodeList){
                            pgCount = 1;
                            for (var i = 0; i < objNodeList.childNodes.length; i++) {
                              var objChildNode = objNodeList.childNodes[i];
                                if (objChildNode.childNodes(0).nodeType == 4){ //CDATA
                                    pgContent += stripEmptyLines(objChildNode.childNodes(0).nodeTypedValue);
                                    if (pgCount < objNodeList.childNodes.length)
                                      pgContent += String.fromCharCode(12); //form-feed
                                    pgCount = pgCount + 1;
                                }
                            }
                        }
                        
                        var objWordApp = new ActiveXObject("Word.Application");
                        if (objWordApp) {
                          objWordApp.DisplayAlerts = 0; //wdAlertsNone
                          objWordApp.Visible = false;
                          var objWordDoc = objWordApp.Documents.Add();
                          if (objWordDoc) {
                              objWordApp.ActiveDocument.Content.InsertAfter(pgContent);
                              var myRange = objWordApp.ActiveDocument.Range(0, objWordApp.ActiveDocument.Paragraphs(objWordApp.ActiveDocument.Paragraphs.Count).Range.End);
                              myRange.Font.Name = "Courier New";
                              myRange.Font.Size = "10";
                              var obj = objWordApp.ActiveDocument.PageSetup;
                              obj.LeftMargin = objWordApp.InchesToPoints(1);
                              obj.RightMargin = objWordApp.InchesToPoints(0.5);
                              obj.TopMargin = objWordApp.InchesToPoints(1);
                              obj.BottomMargin = objWordApp.InchesToPoints(0.5);

                              /*var objSaveDlg = objWordApp.Dialogs(84);
                              if (objSaveDlg) {
                                objSaveDlg.Name = sFileName;
                                objSaveDlg.Show();
                              }*/
                            /*var sFeatures = "center:yes;help:no;resizable:yes;scroll:no;status:no;unadorned=yes;dialogHeight:245px;dialogWidth:400px;"
                            var fso = new ActiveXObject("Scripting.FileSystemObject");
                            var sFilePath = window.showModalDialog("/SaveAsDlg.htm", sFileName, sFeatures);
                            if (sFilePath) {
                              if (fso.FileExists(sFilePath)) {
                                var sReturn = YesNoMessage("Destination File Exists", "A file already exists at the destination folder with the same name. Do you want to overwrite?");
                                if (sReturn == "Yes"){
                                  var f = fso.GetFile(sFilePath);
                                  if (f && (f.attributes && 1)) { // if the file is read-only
                                    f.attributes = 0; // set attribute to normal.
                                  }
                                } else {
                                  sFilePath = null;
                                }
                              }
                            }*/

                            try {
                              var oXL = new ActiveXObject("Excel.Application");
                              if (oXL) {
                                sFileName2 = oXL.GetSaveAsFilename(sFileName, "Doc Files (*.doc),*.doc", 0);
                              } else {
                                ClientWarning("One of the features in MS Office is incorrectly installed on your machine. Please contact administrator.");
                                return;
                              }
                              if (sFileName2 == false) {
                                return;
                              }
                                
                             sFilePath = sFileName2;
                            } catch (e) {
                              ClientWarning("One of the features in MS Office is incorrectly installed on your machine. Please contact administrator.")
                              return;
                            } finally {
                              if (oXL)
                                oXL.Quit();
                            }
                            
                            if (sFilePath) {
                              try {
                                objWordApp.ActiveDocument.SaveAs(sFilePath);
                              } catch (e) {
                                ClientError("Could not save document to " + sFilePath);
                              }
                            }
                          }
                          objWordApp.Quit(0);
                        } else
                          ClientError("Could not create word application object on the client machine.");

                      } catch (e) {
                        ClientError("An error occured while converting XML to MS Word document. [" + e.description + "]");
                      }
                      finally {
                        toolbar.disabled = false;
                        btnSave1.disabled = false;
                        tb_onmouseout(btnSave1);
                        divSave.style.display = "none";
                      }
                    }
                    
                    function stripEmptyLines(str){
                        var tmpArr;
                        var tmpLine = "                                                                                ";
            
                        str = str.replace(/\t/g, "");
                        tmpArr = str.split("\n");
                        
                        var arrLen = tmpArr.length;
                        
                        //chop the top empty lines
                        for (var i = 0; i < arrLen; i++) {
                            if (tmpLine.indexOf(tmpArr[0]) != -1)
                                tmpArr.splice(0, 1);
                            else 
                                break;
                        }
            
                        //chop the bottom empty lines
                        for (var i = tmpArr.length - 1; i >= 0; i--) {
                            if (tmpLine.indexOf(tmpArr[tmpArr.length-1]) != -1)
                                tmpArr.splice(tmpArr.length-1, 1);
                            else
                                break;
                        }
                        
                        return tmpArr.join("\n");
                    }
                    
                    function closeMe(){
                        //window.close();
                        if (window.self == window.top) {
                            window.returnValue = true;
                            close();
                        }
                        else {
                            window.self.frameElement.style.display = "none";
                        }
                    }
                    
                    function PageInit(){
                        divDoc.style.width = document.body.clientWidth;
                        if (window.self == window.top) {
                            
                        }
                        else {
                            //hosted in a iframe
                            btnPrint.style.display = "none";
                            btnSave1.style.display = "none";
                            //btnClose.style.display = "none";
                            //divDoc.style.width = document.body.offsetWidth;
                            //divDoc.style.height = document.body.offsetHeight - 20; //minus the padding top and bottom
                            document.body.style.border = "1px solid #000000";
                        }
                        if (numPages > 0){
                            curPage = 1;
                            displayPageInfo();
                        }
                    }
                    
                    function displayPageInfo(){
                        if (numPages > 0) {
                            pgNo.innerText = "Page " + curPage + " / " + numPages;
                        }
                    }

                    function tb_onmouseover(obj){
                        if (obj.disabled || toolbar.disabled) return;
                        obj.className = "toolbarButtonOver";
                    }
                    
                    function tb_onmouseout(obj){
                        obj.className = "toolbarButtonOut";
                    }
                    
                    function windowActivate(){
                        if (bSavingFile) {
                            bSavingFile = false;
                            toolbar.disabled = false;
                            btnSave1.disabled = false;
                            tb_onmouseout(btnSave1);
                            divSave.style.display = "none";
                        }
                    }
                    
                    function window_resize(){
                        divDoc.style.width = document.body.clientWidth;
                    }
                    
                    ]]>
                </script>
            </head>    
            <body onbeforeprint="initPage4Printing()" onafterprint="initPage4Screen()" onload="PageInit()" topmargin="0" bottommargin="0" rightmargin="0" leftmargin="0" margintop="0" marginleft="0" style="overflow:hidden;" onfocus="windowActivate()" onresize="window_resize()">
              <table border="0" cellspacing="0" cellpadding="0" style="height:100%">
                <tr style="height:24px">
                  <td>
                        <div id="toolbar" name="toolbar" style="margin:2px;background-color:#FFFFFF">
                            <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
                            <tr>
                            <td width="*">
                            <button id="btnSave1" hidefocus="true" tabIndex="-1" tabStop="false" onclick="saveFile()" title="Save Document" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/save.gif"/></button>
                            <button id="btnPrint" hidefocus="true" tabIndex="-1" tabStop="false" onclick="printDoc()" title="Print Document" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/print.gif"/></button>
                            <img src="/images/spacer.gif" width="4px"/>
                            <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="zoomIn()" title="Zoom In" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/zoomin.gif"/></button>
                            <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="zoomOut()" title="Zoom Out" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/zoomout.gif"/></button>
                            <img src="/images/spacer.gif" width="4px"/>
                            <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(1)" title="First Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><strong>&lt;&lt;</strong></button>
                            <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(2)" title="Previous Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><strong>&lt;</strong></button>
                            <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(3)" title="Next Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><strong>&gt;</strong></button>
                            <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(4)" title="Last Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><strong>&gt;&gt;</strong></button>
                            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                            <span id="pgNo" name="pgNo" style="height:24px;vertical-align:middle;"></span>
                            </td>
                            <td align="right" valign="middle" style="width:24px;padding-right:5px;" >
                            <button hidefocus="true" id="btnClose" tabIndex="-1" tabStop="false" onclick="closeMe()" class="toolbarButton" title="Close Window"><img src="/images/close.gif" width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"/></button>
                            </td>
                            </tr>
                            </table>
                            <img src="/images/spacer.gif" style="width:100%;height:0px;border-top:1px solid #696969;border-bottom:1px solid #D3D3D3;"/>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div id="divDoc" class="autoflowDiv" style="height:100%;overflow:auto;padding:5px;">
                        <div id="doc" name="doc">
                        <xsl:for-each select="Page">
                            <table border="0" cellspacing="0" cellpadding="0" class="myPage">
                                <xsl:attribute name="id"><xsl:value-of select="concat('pg', string(position()))"/></xsl:attribute>
                                <tr><td nowrap="">
                            <!-- <div class="myPage">
                                <xsl:attribute name="id"><xsl:value-of select="concat('pg', string(position()))"/></xsl:attribute> -->
                                <span class="pageContent">
                                    <xsl:attribute name="id"><xsl:value-of select="concat('pgContent', string(position()))"/></xsl:attribute>
                                    <pre>
                                        <!-- <xsl:call-template name="myPageContent"/> -->
                                        <xsl:value-of select="user:getPageInfo(.)"/>
                                    </pre>
                                </span>
                                </td></tr></table>
                            <!-- </div> -->
                            <br/>
                        </xsl:for-each>
                        </div>
                        </div>
                      </td>
                    </tr>
                  </table>
                  <form id="frmGetDoc" name="frmGetDoc" method="POST" onreadystatechange="frmGetDocReadyChange()" style="display:none">
                  </form>
                  <div id="divSave" name="divSave" style="position:absolute;display:none;height:65px;width:300px;top:100px;left:180px;background-color:#FFF8DC;border:1px solid #000000;color:#4169E1;text-align:center;padding:25px;filter : progid:DXImageTransform.Microsoft.Shadow(color='#666666', Direction=135, Strength=4);">
                    <strong>Preparing Word document. Please wait... </strong>
                  </div>
            </body>
        </html>
    </xsl:template>
    
</xsl:stylesheet>
