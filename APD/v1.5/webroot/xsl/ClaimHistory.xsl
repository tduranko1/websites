<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimHistory">

<xsl:import href="msxsl/generic-template-library_apdnet.xsl"/>
<xsl:import href="msxsl/msjs-client-library_apdnet.js"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="LynxID"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.0.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},10,uspClaimHistoryGetDetailXML,ClaimHistory.xsl,6049   -->

<HEAD>
<TITLE>Vehicle Details</TITLE>

<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/CoolSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>
<script type="text/javascript">
          document.onhelp=function(){
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };
</script>


<SCRIPT language="JavaScript">

  var gsLynxID = '<xsl:value-of select="$LynxID"/>';

<![CDATA[

  // Tooltip Variables to set:
  messages= new Array()
  // Write your descriptions in here.
  messages[0]="Double Click to Expand"
  messages[1]="Single-Click Column to sort";

  // button images for ADD/DELETE/SAVE
  preload('buttonAdd','/images/but_ADD_norm.png')
  preload('buttonAddDown','/images/but_ADD_down.png')
  preload('buttonAddOver','/images/but_ADD_over.png')
  preload('buttonDel','/images/but_DEL_norm.png')
  preload('buttonDelDown','/images/but_DEL_down.png')
  preload('buttonDelOver','/images/but_DEL_over.png')
  preload('buttonSave','/images/but_SAVE_norm.png')
  preload('buttonSaveDown','/images/but_SAVE_down.png')
  preload('buttonSaveOver','/images/but_SAVE_over.png')

  //init the table selections, must be last
  function initPage()
  {
    top.SetMainTab(1)
  	var oTbl = document.getElementById("tblClaimHist");
	  GridClick(oTbl.rows[0]);
  	resizeScrollTable(document.getElementById("CHScrollTable"));

  	top.setSB(100,top.sb)
  }

  function resizeScrollTable(oElement)
  {
  	var head = oElement.firstChild;
  	var headTable = head.firstChild;
  	var body = oElement.lastChild;
  	var bodyTable = body.firstChild;

  	body.style.height = Math.max(0, oElement.clientHeight - head.offsetHeight);

  	var scrollBarWidth = body.offsetWidth - body.clientWidth;

  	// set width of the table in the head
  	//headTable.style.width = Math.max(0, Math.max(bodyTable.offsetWidth + scrollBarWidth, oElement.clientWidth));

  	// go through each cell in the head and resize
  	var headCells = headTable.rows[0].cells;
  	if (bodyTable.rows.length > 0)
  	{
  		var bodyCells = bodyTable.rows[0].cells;
  		var iLength = bodyCells.length;
  		for (var i = 0; i < iLength; i++)
  		{
  			if (bodyCells[i].style.display != "none")
  			{
  				headCells[i].style.width = bodyCells[i].offsetWidth + 2;
  				if (i == (headCells.length-1))
  				{
  					bodyCells[i].style.width = bodyCells[i].offsetWidth - (scrollBarWidth-2 ) - 4;
  				}
  			}
  		}
  	}
  }

  //update the selected elements from the table row
  function GridClick(oRow)
  {
  	if (!oRow) return;
  	//get the table name of the row that was clicked
  	var oTable = oRow.parentElement.parentElement;
  	//based on the table name update the form elements to the selectd row
  	if (oTable.id == "tblClaimHist")
  	{
  		txtLynxID.value = gsLynxID;
      txtStatus.value = oRow.cells[4].innerText;
      txtPertainsTo.value = oRow.cells[1].innerText;
      txtChannel.value = oRow.cells[2].innerText;
  		txtCompletedDate.value = oRow.cells[0].innerText;
      txtCompletedCSR.value = oRow.cells[8].innerText;
      txtCompletedUserName.value = oRow.cells[9].innerText +" "+ oRow.cells[10].innerText;
      txtCompletedUserRole.innerText = oRow.cells[11].innerText;

      txtCreatedDate.value = oRow.cells[12].innerText;

      txtCreatedCSR.value = oRow.cells[13].innerText;
      txtCreatedUserName.value = oRow.cells[14].innerText +" "+ oRow.cells[15].innerText;
      txtCreatedUserRole.innerText = oRow.cells[16].innerText;
      txtTaskType.value = oRow.cells[5].innerText;
      txtTaskName.innerText = oRow.cells[6].innerText;
      txtDesc.innerText = oRow.cells[17].innerText;
  	}
  }

  //if (document.attachEvent)
  //  document.attachEvent("onclick", top.hideAllMenuScriptlets);
  //else
  //  document.onclick = top.hideAllMenuScriptlets;

]]>
</SCRIPT>

</HEAD>
<BODY unselectable="on" class="bodyAPDSub" onLoad="initSelectBoxes(); InitFields(); initPage()">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

<font color="#D8D8D8">
  <xsl:value-of select="RunTime"/>
</font>

  <DIV id="DescriptionInfoTab" class="SmallGroupingTab" style="position:absolute; z-index:2; top:6px; left:6px;">
    Claim History <font color="#D8D8D8">(<xsl:value-of select="RunTime"/>)</font>
  </DIV>
<DIV id="ClaimHistory" class="SmallGrouping" name="ClaimHistory" style="position:absolute; left:3px; top:22px; width:740px; height:488px; z-index:1;" >
  <DIV id="CHScrollTable" style="width:100%;">
  	<SPAN>
		  <TABLE unselectable="on" class="ClaimMiscInputs" width="100%" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
		    <TR unselectable="on" class="QueueHeader" style="height:22px">
		      <TD unselectable="on" class="TableSortHeader" style="cursor:default;"> Date Completed </TD>
          <TD unselectable="on" class="TableSortHeader" style="cursor:default;"> Pertains To </TD>
          <TD unselectable="on" class="TableSortHeader" style="cursor:default;"> Ch </TD>
		      <TD unselectable="on" class="TableSortHeader" style="cursor:default;"> User </TD>
		      <TD unselectable="on" class="TableSortHeader" style="cursor:default;"> Status </TD>
		      <TD unselectable="on" class="TableSortHeader" style="cursor:default;"> Type </TD>
		      <TD unselectable="on" class="TableSortHeader" style="cursor:default;"> Event/Task Name </TD>
		      <TD unselectable="on" class="TableSortHeader" style="cursor:default;"> Description </TD>
		    </TR>
		  </TABLE>
  	</SPAN>
	  <DIV unselectable="on" class="autoflowTable" style="width:100%; height:294px;">
	    <TABLE unselectable="on" id="tblClaimHist" class="GridTypeTable" width="100%" cellspacing="1" border="0" cellpadding="0">
	      <TBODY bgColor1="ffffff" bgColor2="fff7e5">
					<xsl:for-each select="History"	>
						<xsl:call-template name="ClaimHistoryInfo">
						</xsl:call-template>
					</xsl:for-each>
	      </TBODY>
	    </TABLE>
	  </DIV>
	</DIV>

  <DIV unselectable="on" id="ClaimHistInfoTab" class="SmallGroupingTab" style="position:absolute; width:146px; z-index:2; top:336px; left:8px">
    Claim History Details
  </DIV>
  <DIV unselectable="on" id="InsuredInfo" class="SmallGrouping" style="position:absolute; left:6px; top:352px; z-index:1; width:726px; height:77px">
    <TABLE unselectable="on" class="paddedTable" border="0" cellspacing="0" cellpadding="0" width="100%">
      <TR unselectable="on">
        <TD unselectable="on" nowrap="nowrap">LYNX ID:</TD>
        <TD unselectable="on" >
          <INPUT type="text" id="txtLynxID" class="InputReadonlyField" size="8" >
					<xsl:attribute name="readonly"/>
				</INPUT>
        </TD>
        <TD unselectable="on">Status:</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtStatus" class="InputReadonlyField" size="8" >
					<xsl:attribute name="readonly"/>
				</INPUT>
        </TD>
        <TD unselectable="on" nowrap="nowrap">Pertains to:</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtPertainsTo" class="InputReadonlyField" size="12">
					<xsl:attribute name="readonly"/>
				</INPUT>
        </TD>
        <TD unselectable="on" nowrap="nowrap">Service Channel:
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <INPUT type="text" id="txtChannel" class="InputReadonlyField" size="20">
          <xsl:attribute name="readonly"/>
          </INPUT>
        </TD>
      </TR>
      <TR unselectable="on">
        <TD unselectable="on" nowrap="nowrap">Completed on:</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtCompletedDate" class="InputReadonlyField" size="22" >
					<xsl:attribute name="readonly"/>
				</INPUT>
          <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
        </TD>
        <TD unselectable="on">by:</TD>
        <TD unselectable="on" colspan="3" nowrap="nowrap">
          <INPUT type="text" id="txtCompletedCSR" class="InputReadonlyField" size="8">
					<xsl:attribute name="readonly"/>
				</INPUT>
          <img unselectable="on" src="/images/spacer.gif" alt="" width="10" height="1" border="0"/>
          <INPUT type="text" id="txtCompletedUserName" class="InputReadonlyField" size="25">
					<xsl:attribute name="readonly"/>
				</INPUT>
          <img unselectable="on" src="/images/spacer.gif" alt="" width="6" height="1" border="0"/>
        </TD>
        <TD unselectable="on" nowrap="nowrap">
          Role:
          <SPAN id="txtCompletedUserRole" class="InputReadonlyField" style="width:180px; height:15px"></SPAN>
        </TD>
      </TR>
      <TR unselectable="on">
        <TD unselectable="on" nowrap="nowrap">Created on:</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtCreatedDate" class="InputReadonlyField" size="22" >
					<xsl:attribute name="readonly"/>
				</INPUT>
          <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
        </TD>
        <TD unselectable="on">by:</TD>
        <TD unselectable="on" colspan="3" nowrap="nowrap">
          <INPUT type="text" id="txtCreatedCSR" class="InputReadonlyField" size="8">
					<xsl:attribute name="readonly"/>
				</INPUT>
          <img unselectable="on" src="/images/spacer.gif" alt="" width="10" height="1" border="0"/>
          <INPUT type="text" id="txtCreatedUserName" class="InputReadonlyField" size="25">
					<xsl:attribute name="readonly"/>
				</INPUT>
          <img unselectable="on" src="/images/spacer.gif" alt="" width="6" height="1" border="0"/>
        </TD>
        <TD unselectable="on" nowrap="nowrap">
          Role:
          <SPAN id="txtCreatedUserRole" class="InputReadonlyField" style="width:180px; height:15px"></SPAN>
        </TD>
      </TR>
      <TR unselectable="on">
        <TD unselectable="on" nowrap="nowrap">Task Type:</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtTaskType" class="InputReadonlyField" size="15">
					<xsl:attribute name="readonly"/>
				</INPUT>
        </TD>
        <TD unselectable="on" nowrap="nowrap">Name:</TD>
        <TD unselectable="on" colspan="4" nowrap="nowrap">
          <SPAN id="txtTaskName" class="InputReadonlyField" style="width:400px; height:15px"></SPAN>
        </TD>
      </TR>
      <TR unselectable="on">
        <TD unselectable="on" valign="top">Description:</TD>
        <TD unselectable="on" colspan="6" width="100%">
          <DIV class="autoflowDiv" style="overflow-x:hidden;overflow-y:auto">
            <SPAN id="txtDesc" class="InputReadonlyField" style="width:630px; height:38px"></SPAN>
          </DIV>
        </TD>
      </TR>
    </TABLE>
  </DIV>
</DIV>

<DIV unselectable="on" id="divTooltip"> </DIV>

</BODY>
</HTML>
</xsl:template>

  <!-- 06Jun2013 - TVD - New Code added to handle date formatting -->
  <xsl:template name="formatDate">
    <xsl:param name="dateTime" />
    <xsl:variable name="date" select="substring-before($dateTime, 'T')" />
    <xsl:variable name="year" select="substring-before($date, '-')" />
    <xsl:variable name="month" select="substring-before(substring-after($date, '-'), '-')" />
    <xsl:variable name="day" select="substring-after(substring-after($date, '-'), '-')" />
    <xsl:value-of select="concat($month, '/', $day, '/', $year)" />
  </xsl:template>

  <!-- 06Jun2013 - TVD - New Code added to handle time formatting -->
  <xsl:template name="formatTime">
    <xsl:param name="dateTime" />
    <xsl:variable name="time" select="substring-after($dateTime, 'T')" />
    <xsl:variable name="hh" select="substring-before($time, ':')" />
    <xsl:variable name="mm" select="substring-before(substring-after($time, ':'), ':')" />
    <xsl:variable name="ss" select="substring-after(substring-after($time, ':'), ':')" />
    <xsl:choose>
      <xsl:when test="$hh > 12">
        <xsl:variable name="nhh" select="substring-before($time, ':') - 12" />
        <xsl:value-of select="concat(concat($nhh, ':', $mm), ' PM')"/>
      </xsl:when>
      <xsl:when test="$hh = 12">
        <xsl:variable name="nhh" select="substring-before($time, ':')" />
        <xsl:value-of select="concat(concat($nhh, ':', $mm), ' PM')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="nhh" select="substring-before($time, ':')" />
        <xsl:value-of select="concat(concat($nhh, ':', $mm), ' AM')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- 06Jun2013 - TVD - New Code added to handle date/time formatting -->
  <xsl:template name="formatDateTime">
    <xsl:param name="dateTime" />
    <xsl:variable name="date" select="substring-before($dateTime, 'T')" />
    <xsl:variable name="year" select="substring(substring-before($date, '-'),1,4)" />
    <xsl:variable name="month" select="substring-before(substring-after($date, '-'), '-')" />
    <xsl:variable name="day" select="substring-after(substring-after($date, '-'), '-')" />

    <xsl:variable name="time" select="substring-after($dateTime, 'T')" />
    <xsl:variable name="hh" select="substring-before($time, ':')" />
    <xsl:variable name="mm" select="substring-before(substring-after($time, ':'), ':')" />
    <xsl:variable name="ss" select="substring-after(substring-after($time, ':'), ':')" />
    <xsl:choose>
      <xsl:when test="$hh > 12">
        <xsl:variable name="nhh" select="substring-before($time, ':') - 12" />

        <xsl:variable name="nhh1">
          <xsl:choose>
            <xsl:when test="string-length($nhh) &lt; 2">
              <xsl:value-of select="concat(0,$nhh)"/>
            </xsl:when>
            <xsl:when test="string-length($nhh) &gt; 1">
              <xsl:value-of select="$nhh"/>
            </xsl:when>
          </xsl:choose>
        </xsl:variable>  
        
        <xsl:variable name="TimeFormatted" select="concat(concat($nhh1, ':', $mm), ' PM')"/>

        <xsl:variable name="DateFormatted" select="concat($month, '/', $day, '/', $year)"/>
        <xsl:value-of select="concat($DateFormatted, ' ', $TimeFormatted)"/>
      </xsl:when>
      <xsl:when test="$hh = 12">
        <xsl:variable name="nhh" select="substring-before($time, ':')" />
        <xsl:variable name="TimeFormatted" select="concat(concat($nhh, ':', $mm), ' PM')"/>
        <xsl:variable name="DateFormatted" select="concat($month, '/', $day, '/', $year)"/>
        <xsl:value-of select="concat($DateFormatted, ' ', $TimeFormatted)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="nhh" select="substring-before($time, ':')" />
        <xsl:variable name="TimeFormatted" select="concat(concat($nhh, ':', $mm), ' AM')"/>
        <xsl:variable name="DateFormatted" select="concat($month, '/', $day, '/', $year)"/>
        <xsl:value-of select="concat($DateFormatted, ' ', $TimeFormatted)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <!-- Gets the Claim History details -->
	<xsl:template name="ClaimHistoryInfo">
		<TR unselectable="on" onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' onClick='GridClick(this)'>
		<xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
			<TD unselectable="on" class="GridTypeTD" width="100" height="22px">
        <!--<xsl:value-of select="js:UTCConvertDate(string(current()/@CompletedDate))" />
        <br />
        <xsl:value-of select="js:UTCConvertTime(string(current()/ @CompletedDate))"/>-->
        <!-- xsl:value-of select="user:UTCConvertDateAndTimeByNodeType(.,'CompletedDate','A')"/ -->
        <xsl:call-template name="formatDateTime">
          <xsl:with-param name="dateTime" select="@CompletedDate" />
        </xsl:call-template>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="70" valign="top">
				<xsl:value-of select="@PertainsTo"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
			</TD>
      <TD unselectable="on" class="GridTypeTD" width="20" valign="top">
        <xsl:value-of select="@ServiceChannelCDName"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
	    <TD unselectable="on" class="GridTypeTD" width="87" valign="top">
        <xsl:choose>
          <xsl:when test="@CompletedUserNameFirst != '' and @CompletedUserNameLast != ''">
    				<xsl:value-of select="substring(@CompletedUserNameFirst,1,1)"/><xsl:text>. </xsl:text>
    				<xsl:value-of select="@CompletedUserNameLast"/>
          </xsl:when>
          <xsl:when test="@CompletedUserNameFirst != '' and @CompletedUserNameLast = ''">
    				<xsl:value-of select="@CompletedUserNameFirst"/>
          </xsl:when>
          <xsl:otherwise>
    				<xsl:value-of select="substring(@CompletedUserNameFirst,1,1)"/><xsl:value-of select="@CompletedUserNameLast"/>
          </xsl:otherwise>
        </xsl:choose>
			</TD>
	    <TD unselectable="on" class="GridTypeTD" width="47" valign="top">
				<xsl:value-of select="@StatusName"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
			</TD>
	    <TD unselectable="on" class="GridTypeTD" width="37" valign="top">
				<xsl:value-of select="@EntryType"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
			</TD>
	    <TD unselectable="on" class="GridTypeTD" width="118" valign="top">
				<xsl:value-of select="@EventName"/>
				<xsl:value-of select="@TaskName"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;

			</TD>
	    <TD unselectable="on" class="GridTypeTD" style="text-align:left; vertical-align:top">
			<DIV style="width:184px">
        <xsl:choose>
          <xsl:when test="string-length(@Description) > 90" >
            <xsl:value-of select="substring(@Description,1,90)"/><xsl:text>...</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@Description"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:otherwise>
        </xsl:choose>
			</DIV>
			</TD>
			<TD style="display:none"><xsl:value-of select="@CompletedCSRNumber"/></TD>
			<TD style="display:none"><xsl:value-of select="@CompletedUserNameFirst"/></TD>
			<TD style="display:none"><xsl:value-of select="@CompletedUserNameLast"/></TD>
			<TD style="display:none"><xsl:value-of select="@CompletedRoleName"/></TD>
			<TD style="display:none">
        <!-- xsl:value-of select="user:UTCConvertDateAndTimeByNodeType(.,'CreatedDate','A')"/ -->
        <xsl:call-template name="formatDateTime">
          <xsl:with-param name="dateTime" select="@CreatedDate" />
        </xsl:call-template>
      </TD>
      <TD style="display:none"><xsl:value-of select="@CreatedCSRNumber"/></TD>
			<TD style="display:none"><xsl:value-of select="@CreatedUserNameFirst"/></TD>
			<TD style="display:none"><xsl:value-of select="@CreatedUserNameLast"/></TD>
			<TD style="display:none"><xsl:value-of select="@CreatedRoleName"/></TD>
			<TD style="display:none"><xsl:value-of select="@Description"/></TD>
		</TR>
	</xsl:template>

</xsl:stylesheet>
