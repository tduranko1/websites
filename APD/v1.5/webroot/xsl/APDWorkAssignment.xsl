<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="UserList">
<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="UserProfileCRUD" select="User Profile"/>
<xsl:param name="UserPermissionCRUD" select="User Permission"/>


<xsl:template match="/Root">
    <xsl:choose>
        <xsl:when test="(substring($UserProfileCRUD, 2, 1) = 'R') or ((/Root/@UserID = '-1') and (substring($UserProfileCRUD, 1, 1) = 'C'))">
            <xsl:call-template name="mainHTML">
              <xsl:with-param name="UserProfileCRUD" select="$UserProfileCRUD"/>
              <xsl:with-param name="UserPermissionCRUD" select="$UserPermissionCRUD"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <html>
          <head>
              <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
              <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
          </head>
          <body class="bodyAPDSub" unselectable="on" style="background-color:#FFFFFF">
            <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
              <tr>
                <td align="center">
                  <font color="#ff0000"><strong>You do not have sufficient permission to view user information. 
                  <br/>Please contact your supervisor.</strong></font>
                </td>
              </tr>
            </table>
          </body>
          </html>
        </xsl:otherwise>
    </xsl:choose>
    
</xsl:template>

<xsl:template name="mainHTML">
  <xsl:param name="UserProfileCRUD"/>
  <xsl:param name="UserPermissionCRUD"/>
  <xsl:variable name="APDAppID"><xsl:value-of select="/Root/Reference[@List='Application' and @Name='APD']/@ReferenceID"/></xsl:variable>
<html>
<head>
<!-- STYLES -->
<LINK rel="stylesheet" href="../css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="../css/tabs.css" type="text/css"/>

<!-- Script modules -->
<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,32);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
<![CDATA[
		
	// button images for ADD/DELETE/SAVE
  preload('buttonSave','/images/but_SAVE_norm.png');
  preload('buttonSaveDown','/images/but_SAVE_down.png');
  preload('buttonSaveOver','/images/but_SAVE_over.png');
  
	
  var curRow = null;
  // Page Initialize
  function PageInit()
   { 
    var preSelUserID = "";
    var sURL = window.location.href;
    setTimeout("tdUserName.fireEvent('onclick')", 100);
    if (sURL.indexOf("?") != -1) {
      var sQueryString = sURL.substring(sURL.indexOf("?") + 1, sURL.length);
      var aQueryString = sQueryString.split("&");
      var iLength = aQueryString.length;
      for (var i = 0; i < iLength; i++) {
        if (aQueryString[i].split("=")[0].toLowerCase() == "sel") {
          preSelUserID = aQueryString[i].split("=")[1];
          break;
        }
      }
      if(preSelUserID != "" && !isNaN(parseInt(preSelUserID)))
        window.setTimeout("preselect(" + preSelUserID + ")", 100);
    }     
   }

  function preselect(preSelUserID){
    if (parseInt(preSelUserID) > 0) {
      if (tblSort2) {
        var iLength = tblSort2.rows.length;
        for (var i = 0; i < iLength; i++) {
          if (tblSort2.rows[i].cells[4].innerText == preSelUserID) {
            tblSort2.rows[i].scrollIntoView(false);
            UserGridSelect(tblSort2.rows[i]);
            break;
          }
        }
      }
    }
  }
   
  function UserGridSelect(oRow)
  {
    try {
      if (curRow === oRow) return;  
	   
	   if (curRow) {
        if (typeof(ifrmUsrDetail.isDirty) == "function") {
          if (ifrmUsrDetail.isDirty()) {
              var sSave = YesNoMessage("Need to Save", "Some Information on the current selected row has changed. \nDo you want to save the changes?.");
              if (sSave == "Yes"){
                  if (!btnSave()) return true;
              }
          }
        }
      }
     	  
      if (curRow)
        curRow.style.backgroundColor = "#FFFFFF";
        
  	  var strUserID = oRow.cells[4].innerText;
      ifrmUsrDetail.frameElement.src = "/APDUserDetails.asp?UserID=" + strUserID;
      
      curRow = oRow;
      curRow.style.backgroundColor = "#FFD700";
      saveBG = curRow.style.backgroundColor;
    	
    }
    catch(e) {
      handleJSError("UserGridSelect", e)
    }
  }

  function handleJSError(sFunction, e)
  {
    var result = "An error has occurred on the page.\nFunction = " + sFunction + "\n";
    for (var i in e) {
      result += "e." + i + " = " + e[i] + "\n";
    }
    ClientError(result);
  }
  
  function btnSave(){
  
    if (typeof(ifrmUsrDetail.ADS_Pressed) == "function")
      ifrmUsrDetail.ADS_Pressed("Update", "Users");
    return true;
  }
  
  
]]>
</SCRIPT>

    <style type="text/css">
        td.tdHead {
          border-color : #000000 #999999 #999999 #000000;
          border-style : solid;
          border-top-width : 0px;
          border-right-width : 1px;
          border-bottom-width : 1px;
          border-left-width : 0px;
          color : #000066;
          background-color : #CCCCCC;
          font-family : Verdana, Arial, Helvetica, sans-serif;
          font-size : 10px;
          font-weight : bold;
          text-align : center;
          vertical-align : middle;
          white-space : nowrap;
         }
      </style>


<title>APD Work Assignment</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
</head>

<BODY unselectable="on" class="bodyAPDSub" onLoad="PageInit()" style="background-color:#FFFFFF;margin:0px;padding:0px;width:100%;height:100%;overflow:hidden" tabIndex="-1">
<table cellspacing="0" cellpadding="0" border="0" style="height:155px;width:100%">
<tr>  
<td>
 <DIV id="Users" name="Users" style="width:100%;">
  <span>
  <TABLE unselectable="on" onClick="sortColumn(event, 'tblSort2')" class="ClaimMiscInputs" width="100%" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
    <colgroup>
       <col width="90px"/>
       <col width="182px"/>
       <col width="322px"/>
       <col width="100px"/>
    </colgroup>
    <TBODY>
          <tr style="height:21px">
            <td class="tdHead">Csr Number</td>
            <td name="tdUserName" id="tdUserName" class="tdHead" >User Name</td>
            <td class="tdHead">Assignment Pool</td>
            <td class="tdHead">Assign Work</td>
         </tr>
    </TBODY>
   </TABLE>
  </span>

  <DIV unselectable="on" class="autoflowTable" style="width:100%; height:130px;" id="grid1" name="grid1">
  <TABLE unselectable="on" id="tblSort2" class="GridTypeTable" width="100%" cellspacing="1" border="0" cellpadding="3" style="overflow-y:scroll;height:140px;table-layout:fixed;" > 
      <colgroup>
          <col width="86px"/>
          <col width="182px"/>
          <col width="320px"/>
          <col width="82px"/>
      </colgroup>
  <TBODY>
       <xsl:for-each select="/Root/User">
        <xsl:if test="count(Application[@ApplicationID=$APDAppID]) &gt; 0">	
	     <xsl:choose>
          <xsl:when test="Application[@ApplicationID=$APDAppID]/@AccessEndDate = ''">
            <TR onClick='UserGridSelect(this)' onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' style='height:21px;'> 
            <td style="border:1px ridge;" nowrap="true" align="center"><xsl:value-of select="Application[@ApplicationID=$APDAppID]/@LogonId"/></td>
            <td style="border:1px ridge;" nowrap="true"><xsl:value-of select="concat(@NameLast, ', ', @NameFirst)"/></td>
            <td style="border:1px ridge;">
              <xsl:variable name="AssignmentPoolList">
                <xsl:for-each select="AssignmentPool">
                  <xsl:value-of select="@Name"/>
                  <xsl:if test="position() != last()">, </xsl:if>
                </xsl:for-each>
              </xsl:variable>
              <xsl:if test="string-length($AssignmentPoolList) &gt; 55">
                <xsl:attribute name="Title">
                  <xsl:value-of select="$AssignmentPoolList"/>
                </xsl:attribute>
                <xsl:attribute name="Style">
                  <xsl:text>cursor:help</xsl:text>
                </xsl:attribute>
              </xsl:if>
              <xsl:choose>
                <xsl:when test="$AssignmentPoolList = ''">
                  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                </xsl:when>
                <xsl:when test="string-length($AssignmentPoolList) &gt; 55">
                  <xsl:value-of select="substring($AssignmentPoolList, 1, 55)"/><xsl:text>...</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$AssignmentPoolList"/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
            <td style="border:1px ridge;" nowrap="true" align="center">
     	    <xsl:choose>
              <!-- <xsl:when test="@DispatchDeskReviewFlag = '1' or @InjuryInvolvementFlag='1' or @ReceiveClaimAssignmentsFlag='1' or @ReceiveDeskReviewsFlag='1'" > -->
              <xsl:when test="@AssignWork = '1'" >
                <xsl:text>Yes</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>No</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
            </td>
            <td style="display:none"><xsl:value-of select="@UserID"/></td>
            </TR>
          </xsl:when>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
 </TBODY>
 </TABLE>
</DIV>
</DIV>
 <br/>
 </td>
 </tr>
    <tr style="height:270px;top:160px" valign="top">
      <td>
	  <div align="right" unselectable="on" id="crudDivUsers" name="crudDivUsers" style="position:absolute;top:170px;left:0;width:98%;margin-right:20px;margin-top:2px;display:none">
	      <xsl:if test="(substring($UserProfileCRUD, 2, 1) = 'R') or (substring($UserPermissionCRUD, 3, 1) = 'U')">
           <img name="btnSave1" id="btnSave1" src="/images/but_SAVE_norm.png" onmouseover="if (!this.disabled) this.src='/images/but_SAVE_over.png'" onmouseout="if (!this.disabled) this.src='/images/but_SAVE_norm.png'" onmousedown="if (!this.disabled) this.src='/images/but_SAVE_down.png'" onclick="btnSave()" style="cursor:hand"/>
          </xsl:if>
		</div>
       <iframe name="ifrmUsrDetail" id="ifrmUsrDetail" src="/blank.asp" style="width:100%;height:100%;background:transparent;" allowtransparency="true"/> 
      </td>
    </tr>
  </table>
</BODY>
</html>
</xsl:template>

</xsl:stylesheet>
