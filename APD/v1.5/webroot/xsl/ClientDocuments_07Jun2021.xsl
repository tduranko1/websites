﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:js="urn:the-xml-files:xslt"
  xmlns:IE="http://mycompany.com/mynamespace"
  xmlns:user="http://mycompany.com/mynamespace">

  <xsl:import href="msxsl/msjs-client-library-htc.js"/>
  <xsl:import href="msxsl/msxsl-function-library.xsl"/>
  <xsl:output method="html" indent="yes" encoding="UTF-8"/>

  <!-- Permissions params - names must end in 'CRUD' -->
  <!-- <xsl:param name="DocumentCRUD" select="Document"/> -->

  <msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
    function resolveMacro(strValue, strDocumentTypeName, strSupplementNumber, strImageLocation, strClaimNumber, strLossDate, strLynxID, strInsured, strVehNum, strAdjuster, strPolicyNumber, strDocumentID) {
      var strRet = strValue;
      if (strRet != "" && strRet.indexOf("$") != -1) {
        var dt = new Date();
        strRet = strRet.replace(/\$ClientClaimNumber\$/g, strClaimNumber);
        strRet = strRet.replace(/\$DocumentTypeName\$/g, strDocumentTypeName);
        strRet = strRet.replace(/\$SupplementNumber\$/g, strSupplementNumber);        
        strRet = strRet.replace(/\$LossDate\$/g, strLossDate.replace(/\//g, "-"));
        strRet = strRet.replace(/\$LynxID\$/g, strLynxID);
        strRet = strRet.replace(/\$InsuredName\$/g, strInsured);
        strRet = strRet.replace(/\$VehicleNum\$/g, strVehNum);
        strRet = strRet.replace(/\$Adjuster\$/g, strAdjuster);
        strRet = strRet.replace(/\$DocumentID\$/g, strDocumentID);
        strRet = strRet.replace(/\$TimeStamp\$/g, dt.valueOf());
        strRet = strRet.replace(/\$PolicyNumber\$/g, strPolicyNumber);

        strRet = trim_string(strRet);
        
        strRet += strImageLocation.substr(strImageLocation.length - 4).toLowerCase();
      }
      return strRet;
    }
    
   function trim_string(strValue) {
      var ichar, icount;
      ichar = strValue.length - 1;
      icount = -1;
      while (strValue.charAt(ichar)==' ' && ichar > icount)
         --ichar;
      if (ichar!=(strValue.length-1))
         strValue = strValue.slice(0,ichar+1);
      ichar = 0;
      icount = strValue.length - 1;
      while (strValue.charAt(ichar)==' ' && ichar < icount)
         ++ichar;
      if (ichar!=0)
         strValue = strValue.slice(ichar,strValue.length);
      return strValue;
    // Use a regular expression to replace leading and trailing
    // spaces with the empty string
    //return this.replace(/(^\s*)|(\s*$)/g, "");
   }
  ]]>
  </msxsl:script>

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="InsCoId"/>
  <xsl:param name="LynxId"/>
  <xsl:param name="UserId"/>
  <xsl:param name="WindowID"/>
  <xsl:param name="SrcAppCd"/>
  <xsl:param name="ImageRootDir"/>
  <xsl:param name="EmailSubject"/>
  <xsl:param name="EmailBody"/>
  <xsl:param name="DocumentNameFormat"/>
  <xsl:param name="PackageTypeValuePDF"/>
  <xsl:param name="MergeFileFormat"/>
  <xsl:param name="NotifySuccessSubject"/>
  <xsl:param name="NotifySuccessBody"/>
  <xsl:param name="NotifyFailureSubject"/>
  <xsl:param name="NotifyFailureBody"/>
  <xsl:param name="IsElectronicBundle"/>
  <xsl:param name="VehNum"/>
  <xsl:param name="VehClaimAspectID"/>
  <xsl:param name="jobTransformationFile"/>

  <xsl:template match="/Root">

    <xsl:variable name="ClientClaimNumber">
      <xsl:value-of select="/Root/@ClientClaimNumber"/>
    </xsl:variable>
    <!--<xsl:variable name="LossDate"><xsl:value-of select="js:formatSQLDate(string(/Root/@LossDate))"/></xsl:variable>-->
    <xsl:variable name="LossDate">
      <xsl:value-of select="/Root/@LossDate"/>
    </xsl:variable>
    <xsl:variable name="InsuredName">
      <xsl:value-of select="/Root/@InsuredName"/>
    </xsl:variable>

    <html>
      <head>
        <title>
          <xsl:value-of select="$LynxId"/> - Send Documents to Client/Repair Facility
        </title>

        <link rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
        <link rel="stylesheet" href="/css/apdgrid3.css" type="text/css"/>

        <style>
          .apdDefault {font: 10pt Arial}
        </style>

        <script type="text/javascript" src="/tiny_mce/tiny_mce.js"/>

        <SCRIPT language="JavaScript" src="/js/apdcontrols.js"/>
        <SCRIPT language="JavaScript" src="/js/formats.js"/>
        <script type="text/javascript">
          tinyMCE.init({
          mode : "exact",
          theme : "advanced",
          skin : "o2k7",
          plugins : "fullscreen",
          theme_advanced_buttons1 : "fullscreen,|,bold,italic,underline,|,outdent,indent,bullist,numlist,|,fontselect,fontsizeselect,forecolor,|,backcolor",
          theme_advanced_buttons2 : "",
          theme_advanced_buttons3 : "",
          theme_advanced_toolbar_location : "top",
          theme_advanced_toolbar_align : "left",
          elements : "txtMessage",
          width : "504px",
          height : "147px",
          force_p_newlines : false,
          body_class : "apdDefault"
          });
        </script>

        <script language="JavaScript">

          var strDefaultSendVia = "<xsl:value-of select="/Root/@ReturnDocRoutingCD"/>";
          var strDefaultSendViaValue = "<xsl:value-of select="js:encode(string(/Root/@ReturnDocDestinationValue))"/>";
          var strWindowID = "<xsl:value-of select="$WindowID"/>";
          var strImageRootDir = "<xsl:value-of select="$ImageRootDir"/>";
          var strLynxID = "<xsl:value-of select="$LynxId"/>";
          var strUserID = "<xsl:value-of select="$UserId"/>";
          var strInsCoId = "<xsl:value-of select="$InsCoId"/>";
          var strInsCoName = "<xsl:value-of select="js:encode(string(/Root/@InsuranceCompanyName))"/>";
          var strVehNum = "<xsl:value-of select="$VehNum"/>";
          var strVehClaimAspectID = "<xsl:value-of select="$VehClaimAspectID"/>";
          var strEmailSubject = unescape("<xsl:value-of select="js:encode(user:resolveMacro(string($EmailSubject), '', '', '', string($ClientClaimNumber), string($LossDate), string($LynxId), string($InsuredName), string($VehNum), string(/Root/@AdjusterName), string(/Root/@PolicyNumber)))"/>");
          var strEmailBody = unescape("<xsl:value-of select="js:encode(user:resolveMacro(string($EmailBody), '', '', '', string($ClientClaimNumber), string($LossDate), string($LynxId), string($InsuredName), string($VehNum), string(/Root/@AdjusterName), string(/Root/@PolicyNumber)))"/>");
          var strDocumentNameFormat = "<xsl:value-of select="$DocumentNameFormat"/>";
          var strUserName = unescape("<xsl:value-of select="js:encode(string(/Root/@UserName))"/>");
          var strUserEmailAddress = unescape("<xsl:value-of select="js:encode(string(/Root/@UserEmailAddress))"/>");
          var strDefaultPackageType = "<xsl:value-of select="/Root/@ReturnDocPackageTypeCD"/>";
          var strPackageType = strDefaultPackageType;
          var strMergedFileName = unescape("<xsl:value-of select="js:encode(user:resolveMacro(string($MergeFileFormat), '', '', '', string($ClientClaimNumber), string($LossDate), string($LynxId), string($InsuredName), string($VehNum), string(/Root/@AdjusterName), string(/Root/@PolicyNumber)))"/>");
          var strSuccessSubject = unescape("<xsl:value-of select="user:resolveMacro(string($NotifySuccessSubject), '', '', '', string($ClientClaimNumber), string($LossDate), string($LynxId), string($InsuredName), string($VehNum), string(/Root/@AdjusterName), string(/Root/@PolicyNumber))"/>");
          var strSuccessBody = unescape("<xsl:value-of select="js:encode(string($NotifySuccessBody))"/>");
          var strFailureSubject = unescape("<xsl:value-of select="js:encode(user:resolveMacro(string($NotifyFailureSubject), '', '', '', string($ClientClaimNumber), string($LossDate), string($LynxId), string($InsuredName), string($VehNum), string(/Root/@AdjusterName), string(/Root/@PolicyNumber)))"/>");
          var strFailureBody = unescape("<xsl:value-of select="js:encode(string($NotifyFailureBody))"/>");
          var strClientClaimNumber = unescape("<xsl:value-of select="js:encode(string(/Root/@ClientClaimNumber))"/>");
          var strVehStatusID = "<xsl:value-of select="/Root/Reference[@List='PertainsTo' and @ReferenceID=$VehClaimAspectID]/@StatusID"/>";
          var bElectronicBundle = "<xsl:value-of select="$IsElectronicBundle"/>";

          var strVehicleNum = "<xsl:value-of select="/Root/@VehicleNumber"/>";
          var strVehicleYear = "<xsl:value-of select="/Root/@VehicleYear"/>";
          var strVehicleMake = unescape("<xsl:value-of select="js:encode(string(/Root/@VehicleMake))"/>");
          var strVehicleModel = unescape("<xsl:value-of select="js:encode(string(/Root/@VehicleModel))"/>");

          var strAdjusterName = unescape("<xsl:value-of select="js:encode(string(/Root/@AdjusterName))"/>");
          var strAdjusterFax = "<xsl:value-of select="/Root/@AdjusterFax"/>";
          var strAdjusterEmail = unescape("<xsl:value-of select="js:encode(string(/Root/@AdjusterEmail))"/>");
          var strAdjusterActive = "<xsl:value-of select="/Root/@AdjusterActiveFlag"/>";

          var strLossDate = "<xsl:value-of select="/Root/@LossDate"/>";

          //------ 19Mar2014 - TVD - Workflow Event Stuff --------//
          var strAdjusterID = unescape("<xsl:value-of select="js:encode(string(/Root/@AdjusterUserID))"/>");
          var strTotalLossAlertBundleID = "<xsl:value-of select="/Root/Bundling[@Name='Total Loss Alert - (Adjuster)']/@BundlingID"/>";
          var gsClaimAspectServiceChannelID;
          var gsUserID = strUserID;
          //alert(strAdjusterID);
          //alert(strTotalLossAlertBundleID);

          var bTemplateClicked = false;

          var strInsuredName = unescape("<xsl:value-of select="js:encode(string(/Root/@InsuredName))"/>");
          var blnSettingProfileValues = false;
          var oBundlingProfile = { sendPreference: "",
          sendVia: "",
          sendValue: "",
          sendFaxName: "",
          outputFormat: "",
          saveAs : ""
          }

          var iCCAVIdx = -1;
          var iFTPIdx = -1;

          var aDocuments = new Array();
          var aDocumentNames = new Array();
          var aDocCounter = new Array();
          var TempArrDocName = new Array();
          var strToday = "<xsl:value-of select="/Root/@Today"/>";
          var strClaimMessagesSP = "uspClaimMessageGetDetailXML";
          var blnMessageChanging = false;
          var blnClaimInfoReady = false;
          var blnPrefChanging = false;
          var strDocumentList = "";
          var blnMessageTemplateReady = false;
          var strPertainsTo = "";
          var strClaimAspectServiceChannelID = "";
          var strLossState = "<xsl:value-of select="/Root/@LossState"/>";
          var strShopState = "";
          var blnSystemOveridding = false;
          var blnAutoverse = false;
          var strJobTransformationFile = "<xsl:value-of select="$jobTransformationFile"/>";
          var strFTPRegEx = "(\\w{1,}):(\\w{1,})@(\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}|\\w{1,})(:\\d{1,5}){0,}((/\\w{1,}){0,})";
          var strFTPLogonID, strFTPPassword, strFTPServer, strFTPServerPort, strFTPPath;
          var strPolicyNumber = "<xsl:value-of select="/Root/@PolicyNumber"/>";
          var strAutoNotifyAdjusterFlag = 0;
          var blnProfileOverride = false;
          var blnProfileError = false;
          var strProfileErrorMsg = "";
          var strProfileDestValue = "";
          var strProfileDest = "";
          var blnShopBundling = false;
          var strAnalystName = "";
          var strAnalystPhone = "";
          var strEarlyBillFlag = "<xsl:value-of select="/Root/@EarlyBillFlag"/>";
          var strSalvageVendorEmail = "";
          var blnLienHolder = false;
          var strServiceChannelCD = "";
          var strIsNugen = 0;
          var strIsWebservice = 0;
          var strSplit;
          //var arrSelectedDocuments = new Array();
          var strPursuitAuditShopFax, strPursuitAuditShopEmail, strPursuitAuditShopName, strPursuitAuditContactName;
          var blnPreferredDestinationLoaded = false;
          var wsIndex = -1;
          var strSourceApplicationPassThruData = "";

          <![CDATA[
          function __pageInit(){
            txtFaxRecipient.style.visibility = "hidden";
            //selSendVia.CCDisabled = true;
            //txtFax.CCDisabled = true;
            //txtEmail.CCDisabled = true;
            //txtFaxRecipient.CCDisabled = true;
            
            if (strDefaultSendViaValue != ""){
               strDefaultSendViaValue = unescape(strDefaultSendViaValue);
            }
            
            //default the save as to Status report
            var iDocumentID = selSaveAs.GetValueFromText("Status Report");
            if (iDocumentID) selSaveAs.value = iDocumentID;
            selSaveAs.CCDisabled = true;
            
            txtFaxRecipient.style.visibility = trFaxRecipient.style.visibility = "hidden";
            try {
            if (strDefaultSendVia != ""){
              if (strDefaultSendVia == "CCAV"){
                //txtSendVia.value = "Autoverse";
                //selSendVia.style.display = "none";
                if (selSendVia.GetValueFromText("Autoverse") == null){
                   selSendVia.AddItem("CCAV", "Autoverse");
                   //iCCAVIdx = selSendVia.Options.length - 1;
                   selSendVia.value = "CCAV";
                   iCCAVIdx = selSendVia.selectedIndex;
                }
                //selSendVia.value = "FTP";
                //iFTPIdx = selSendVia.selectedIndex;
              } else {
                //txtSendVia.style.display = "none";                
              }
              for (var i = 0; i < selSendVia.Options.length; i++){
                if (selSendVia.Options[i].value == "FTP"){
                   iFTPIdx = selSendVia.selectedIndex;
                   break;
                }
              }
              selSendVia.value = strDefaultSendVia;
              selPreference.value = "DEF";
              selSendVia.CCDisabled = true;

              txtFax.CCDisabled = txtEmail.CCDisabled = (strDefaultSendViaValue != "");
            } else {
               blnSystemOveridding = true;
               selPreference.value = "OVR";
               blnSystemOveridding = false;
              //selPreference.CCDisabled = true;
            }
            } catch (e) {alert(e.description)}
            showOverride();

            try {
              stat1.style.top = (document.body.offsetHeight - 75) / 2;
              stat1.style.left = (document.body.offsetWidth - 240) / 2;
              divMissingDocs.style.top = (document.body.offsetHeight - 200) / 2;
              divMissingDocs.style.left = (document.body.offsetWidth - 400) / 2;
            } catch (e) {}
            
            //if (selClaimAspect.Options.length == 1){
            //select the first claim aspect in the list
            //selClaimAspect.selectedIndex = 0;
            selClaimAspect.value = strVehClaimAspectID;
            //}
            
            getClaimInfo();
            
            tinyMCE.get("txtMessage").setContent("<div style='font-weight:normal;line-height:normal;font-style:normal;font-family:Arial;font-variant:normal;'><span style='font-size:x-small;'><span style='font-family:arial,helvetica,sans-serif'></span></span></div>", {format:'raw'});
            
            if (strDefaultSendVia == "FTP" && strDefaultSendViaValue != ""){
               setFTPVariables(strDefaultSendViaValue);
            }
          }
          
          function setFTPVariables(strValue){
		
/*               if (strValue.indexOf("$CONFIG$") == -1){
                  var re = new RegExp(strFTPRegEx);
                  var m = re.exec(strValue);
                  if (m == null){
                     ClientError("Unable to decode FTP settings.");
                     selSendVia.RemoveItem(iFTPIdx);
                  } else {
                     strFTPLogonID = m[1];
                     strFTPPassword = m[2];
                     strFTPServer = m[3];
                     strFTPServerPort = m[4].replace(/[:]/g, "");
                     if (isNaN(strFTPServerPort) == true)
                        strFTPServerPort = "21";
                     strFTPPath = m[5];
                  }
               } else {
*/

				  var aFTPParams = strValue.split("|");
				  strFTPServer = aFTPParams[0];
                                  strFTPLogonID = aFTPParams[1]; 
				  strFTPPassword = aFTPParams[2]; 
				  strFTPServerPort = ""; //aFTPParams[3] 
				  strFTPPath = "/";


                  //strFTPServer = strValue;
                  //strFTPLogonID = strFTPPassword = strFTPServerPort = strFTPPath = "$EMPTY$";
//               }
          }

          function showOverride(){
            if (blnPrefChanging == true) return;
        
            switch (selPreference.value){
              case "DEF":
                if (iCCAVIdx == -1 && strDefaultSendVia == "CCAV"){
                  if (selSendVia.GetValueFromText("Autoverse") == null){
                     selSendVia.AddItem("CCAV", "Autoverse");
                     //iCCAVIdx = selSendVia.Options.length - 1;
                  }
                  blnPrefChanging = true;
                  selSendVia.value = "CCAV";
                  blnPrefChanging = false;
                  iCCAVIdx = selSendVia.selectedIndex;
                }
                if (oBundlingProfile.sendVia == "WS"){
                  if (selSendVia.GetValueFromText("Web Services") == null){
                     selSendVia.AddItem("WS", "Web Services");
                  }
                  blnPrefChanging = true;
                  selSendVia.value = "WS";
                  blnPrefChanging = false;
                  wsIndex = selSendVia.selectedIndex;
                }
                if ((strDefaultSendVia == "CCAV") || (strDefaultSendVia != "CCAV" && strDefaultSendVia != "" && strDefaultSendViaValue != "")){
                   selSendVia.CCDisabled = true;
                   tdOutputFormat.style.visibility = "hidden";
                   rbOptionTif.style.visibility = "hidden";
                   rbOptionPdf.style.visibility = "hidden";
                   trFaxRecipient.style.visibility = "hidden";
                   showRouteValue();
                   txtEmail.CCDisabled = txtFaxRecipient.CCDisabled = txtFax.CCDisabled = true;
                   //selMsgTemplate.CCDisabled = true;
                   if (oBundlingProfile && oBundlingProfile.saveAs != ""){
                     selSaveAs.CCDisabled = false;
                     selSaveAs.value = oBundlingProfile.saveAs;
                     selSaveAs.CCDisabled = true;
                   }
                } else {
                  blnPrefChanging = true;
                  blnSystemOveridding = true;
                  selPreference.selectedIndex = -1;
                  selPreference.value = "OVR";
                  //selMsgTemplate.CCDisabled = false;
                  selSaveAs.CCDisabled = false;
                  blnSystemOveridding = false;
                  blnPrefChanging = false;
                }
                break;
              case "OVR":
                if (blnSystemOveridding == false && strDefaultSendVia != ""){
                  //manual override. need to confirm action

				//---------------------------------------------------------//
				//-- 11Dec2012 - TVD - Warning Message                   --//
				//-- Notify client that they must change values manually --//
				//---------------------------------------------------------//
				var strWarning = 'WARNING: You have selected to use a template that has not been configured for this client. Before you send this bundle it is your responsibility to make sure all of the following settings are correctly made, and all data fields are populated with the correct info to insure the transmittal is in compliance with the client approved process.';
				strWarning = strWarning + '\n\nSend Preference – Need to change from “Client Preferred” to “Override” if the default settings/data shown need to be changed.' ;
				strWarning = strWarning + '\n\nSend via – Select either “Email” or “Fax” and edit related fields. FTP is unavailable for non-configured bundles.' ;
				strWarning = strWarning + '\n\t\t\tEmail Address';
				strWarning = strWarning + '\n\t\t\tor';
				strWarning = strWarning + '\n\t\t\tFax Number and Fax Recipient';
				strWarning = strWarning + '\n\nOutput format – Select either “TIF” or “PDF”';
				strWarning = strWarning + '\n\nSave as – Select the document type the bundle will be “saved as” in the claim file.';
				strWarning = strWarning + '\n\nMessage – This may default to generic text or a client specific text depending which template you select. Edit as needed to meet this bundle’s requirements.';

                  sRet = YesNoMessage("Confirm Override ", strWarning + "\n\nYou are about to override the settings. Do you want to continue?");
                  if (sRet != "Yes")
                     return;
                }
                /*if (iCCAVIdx > -1) {
                  selSendVia.RemoveItem(iCCAVIdx);
                  iCCAVIdx = -1;
                }*/
                showRouteValue();
                txtFax.CCDisabled = txtFaxRecipient.CCDisabled = txtEmail.CCDisabled = false;
                selSendVia.CCDisabled = false;
                /*if (strAdjusterActive == "1" && blnShopBundling == false){
                  if (strAdjusterEmail != ""){
                     selSendVia.value = "EML";
                  } else if (strAdjusterFax != ""){
                     selSendVia.value = "FAX";
                  }
                }*/
                tdOutputFormat.style.visibility = "visible";
                rbOptionTif.style.visibility = "visible";
                rbOptionPdf.style.visibility = "visible";
                txtFaxRecipient.style.visibility = trFaxRecipient.style.visibility = "hidden";
                
                /*if (strDefaultPackageType == "TIF"){
                   rbOptionPdf.value = 0;
                   rbOptionTif.value = 1;
                   rbGrpOption.value = 1;
                } else {
                   rbOptionTif.value = 0;
                   rbOptionPdf.value = 2;
                   rbGrpOption.value = 2;
                }*/
                enableDocuments(true);
                btnSend.CCDisabled = false;
                selMsgTemplate.CCDisabled = false;
                selSaveAs.CCDisabled = false;
                break;
            }
            showRouteValue();
          }
          
          function showRouteValue(){
            blnAutoverse = false;
			//alert(selSendVia.value);
            switch (selSendVia.value){
              case "EML":
                showEmail();break;
              case "FAX":
                showFax();break;
              case "FTP":
                  if (strDefaultSendVia == "FTP"){
                     setFTPVariables(strDefaultSendViaValue);
                  } else {
                     if (blnProfileOverride == false){
                        ClientWarning("FTP destination is not defined for generic document bundling. " + 
                                       "You have to choose a bundling profile that has the Send Via as FTP, then select the Send Preference to Override and then select Send Via to FTP.");
						selSendVia.value = strDefaultSendVia;
                        return;
                     }
                  }
                showNone();break;
              case "SG":
                showNone();break;
              case "CCAV":
                blnAutoverse = true;
                showNone();break;
              default:
                showNone();break;
            }
          }
          
          function showEmail(){
            divSendVia.innerText = "Email Address:";
            divFax.style.display = "none";
            divEmail.style.display = "inline";

           txtFaxRecipient.style.visibility = trFaxRecipient.style.visibility = "hidden";
            if (strProfileDest == "EML" && strProfileDestValue != ""){
               //-------------------------------//
               // 30Oct2013 - TVD - Added code  //
               // to allow both and adjuster    //
               // and default email             //
               //-------------------------------//
               if (strProfileDestValue.substring(0,1) == ';') {
                  txtEmail.value = strAdjusterEmail + strProfileDestValue;
               } else {
                  txtEmail.value = strProfileDestValue;
               }
            } else {
               if (strDefaultSendVia == "EML" && strDefaultSendViaValue != "") {
                  txtEmail.value = strDefaultSendViaValue;
               } else {
                  txtEmail.value = strAdjusterEmail;
               }
            }
             //For Emails - set the default output format to pdf 
             rbOptionTif.value = 0;
             rbOptionPdf.value = 2;
             rbGrpOption.value = 2;
          }
          
          function showFax(){
            divSendVia.innerText = "Fax Number:";
            divEmail.style.display = "none";
            divFax.style.display = "inline";

            if (blnShopBundling == false){
               if (strDefaultSendVia == "FAX" && strDefaultSendViaValue != ""){
                 txtFax.value = strDefaultSendViaValue;
                 txtFaxRecipient.style.visibility = trFaxRecipient.style.visibility = "visible";
                 txtFaxRecipient.value = strInsCoName;
               } else {
                  txtFax.value = strAdjusterFax;
                  txtFaxRecipient.style.visibility = trFaxRecipient.style.visibility = "visible";
                  txtFaxRecipient.value = strAdjusterName;
               }
            } else {
               if (strInsCoId == 176 || strInsCoId == 304){
                  //remove any fax formatting
                  var strFaxValue = "";
                  try {
                      strFaxValue = strPursuitAuditShopFax.replace(/[\(\) -]/g, '');
                  } catch (e){}
                  
                  txtFax.value = strFaxValue;
                  txtFaxRecipient.style.visibility = trFaxRecipient.style.visibility = "visible";
                  txtFaxRecipient.value = strPursuitAuditShopName;
               } else {
                  txtFax.value = "";
                  txtFaxRecipient.style.visibility = trFaxRecipient.style.visibility = "visible";
                  txtFaxRecipient.value = "";
               }
            }
             //For Fax - set the default output format to tif
             rbOptionPdf.value = 0;
             rbOptionTif.value = 1;
             rbGrpOption.value = 1;
          }
          
          function showNone(){
            divSendVia.innerText = "";
            divEmail.style.display = "none";
            divFax.style.display = "none";
            txtFaxRecipient.style.visibility = trFaxRecipient.style.visibility = "hidden";
            //tdOutputFormat.style.visibility = "hidden";
          }
          
          function showDoc(strDocument){
            if (strDocument != ""){
              var strDocPath = strImageRootDir + strDocument;
              var sHREF = "/EstimateDocView.asp?WindowID=" + strWindowID + "&docPath=" + strDocPath;
              var winName = "EstimateDocView_" + strLynxID;
               var sWinEstimateDoc = window.open(sHREF, winName, "Height=490px, Width=800px, Top=150px, Left=90px, resizable=Yes, status=No, menubar=No");
              sWinEstimateDoc.focus();
            }
          }
          
          function doSend(){
            var strDoc2Send = "";
            var strSendValue = "";
            var strSendVia = "";
            var strMerge = "";
            var oXML = new ActiveXObject("MSXML2.DOMDocument");
            var oRoot, oNode, oNode2;
            var blnMerge;
            var strExceptionOutputFormat = "";
            var strJobMessage = getBasicMessage();
            var strJobMessageHTML = tinyMCE.get("txtMessage").getContent({format: 'raw'});

            if (xmlDocuments){
              if (selSendVia.selectedIndex == -1){
                ClientWarning("Please select a value for \"Send via\" from the list.");
                return;                
              }
              
              //alert(tinyMCE.get("txtMessage").getContent({format: 'text'})); return;
              
              if ((aDocuments.length == 0) && (strJobMessage == "" || strJobMessage == null)){
                ClientWarning("No message was entered or no documents were selected to perform the operation.");
                return;
              }
              
              if ((strJobMessage != "" && strJobMessage.length < 10)){
                ClientWarning("Message must be atleast 10 characters long.");
                return;
              }
              
              if (strAdjusterEmail == txtEmail.value && strAdjusterActive == "0"){
               ClientWarning("Client Adjuster is inactive in APD. Cannot send email.");
               return;
              }
              
              //make sure the user has selected a vehicle.
              if (selClaimAspect.selectedIndex == -1){
               ClientWarning("Please select the vehicle from the list.");
               selClaimAspect.setFocus();
               selBundling.selectedIndex = -1;
               return;
              }
              
              if (blnProfileError){
                  ClientWarning("Issue with the bundling profile.<br/><br/>" + strProfileErrorMsg);
                  return;
              }
              
              //CinFin double billing fixes
              var strBundlingProfile = selBundling.text;               
              if ((strInsCoId == "194" && strIsWebservice == "1") && (strBundlingProfile == "Cash Out Alert & Close" || strBundlingProfile == "Closing Repair Complete" || strBundlingProfile == "Closing Supplement" || strBundlingProfile == "Closing - Total Loss" || strBundlingProfile == "Rental Invoice" || strBundlingProfile.trim() == "Initial Estimate and Photos (Adjuster)" || strBundlingProfile == "Acknowledgement of Claim" || strBundlingProfile == "No Inspection (Stale) Alert & Close" || strBundlingProfile == "Assignment Cancellation")) {
            
              var sProc;
               var sRequest = "ClaimAspectID=" + strVehClaimAspectID +
               "&UserID=" + strUserID;            
               sProc = "uspElecOutboundInvoiceGetDetailsWSXML";
               var aRequests = new Array();
               aRequests.push( { procName : sProc,
                                 method   : "executespnpasxml",
                                 data     : sRequest }
                             );
               
               var objRet = XMLSave(makeXMLSaveString(aRequests));
            
               if (objRet && objRet.code == 0 && objRet.xml)
               {
                
                  var oRetXML = objRet.xml;
                  var InvoiceSentFlag;
                  InvoiceSent = oRetXML.selectSingleNode("/Root/StoredProc[@name='" + sProc + "']/Result/Root");
                  InvoiceSentFlag = InvoiceSent.getAttribute("IsWebPage");
                  if(InvoiceSentFlag == 'Alert')
                  {
              
                     var sRet = YesNoMessage("Confirm Resend Invoice" ,"\n\nThe Invoice XML already sent to the client. \n Do you want to resend the Invoice xml with this bundle?");
                    if (sRet == "Yes") {
                        sProc = "uspElecOutboundInvoiceInsUpd";
                           sRequest = sRequest = "LynxID=" + strLynxID +
                                                  "&VehicleNumber=" + strVehicleNum +
                                                  "&SentFlag=" + "0" +
                                                 "&SysLastUserID=" + strUserID; 
                              var sMethod = "ExecuteSpNp";
                             aRequests = new Array();
                             aRequests.push( { procName : sProc,
                             method   : sMethod,
                           data     : sRequest }
                       );

                     objRet = XMLSave(makeXMLSaveString(aRequests));
                    } else if(sRet == "No") {
                    }
                    else 
                      return;
                  }
                  else if(InvoiceSentFlag == 'Yes')
                  {
                         var sHREF = "InvoiceStatusOption.asp?WindowID=" + strWindowID + "&ClaimAspectID=" + strVehClaimAspectID;                         
                         var sSettings = "dialogHeight:215px; dialogWidth:515px;  resizable:yes; status:no; help:no; center:yes;";
                         var InvoiceStatusOptionReVal = window.showModalDialog( sHREF, "", sSettings );
                         
                       

                         
                         if( InvoiceStatusOptionReVal.UserSeleted == 'Resend' || InvoiceStatusOptionReVal.UserSeleted == 'Correction')
                         {
                          sProc = "uspElecOutboundInvoiceInsUpd";
                           sRequest = sRequest = "LynxID=" + strLynxID +
                                                  "&VehicleNumber=" + strVehicleNum +
                                                  "&SentFlag=" + "0" +
                                                  "&UserOption=" + InvoiceStatusOptionReVal.UserSeleted +
                                                 "&SysLastUserID=" + strUserID; 
                              var sMethod = "ExecuteSpNp";
                             aRequests = new Array();
                             aRequests.push( { procName : sProc,
                             method   : sMethod,
                           data     : sRequest }
                       );

                     objRet = XMLSave(makeXMLSaveString(aRequests));
                         
                         }
                         else
                           return; 

                         
                  }
               }
            }
  
              

              oXML.async = false;
              oXML.loadXML("<ClientDocument/>");
              
              oRoot = oXML.selectSingleNode("/ClientDocument");

			  //alert(oRoot);
              
              if (oRoot) {
                if (strJobTransformationFile != "")
                  oRoot.setAttribute("jobTransformationFile", strJobTransformationFile);
                var oSendNode;
                oSendNode = oXML.createElement("Send");
                
                oSendNode.setAttribute("bundlingProfileName", selBundling.text);
                oSendNode.setAttribute("via", selSendVia.value);
                switch (selSendVia.value){
                  case "EML":
                    if (validateEmail() == false){
                      return;
                    } else {
                      strSendValue = txtEmail.value;
                      strSendVia = "Email";
                    }
                    break;
                  case "FAX":
                    if (txtFax.value == "" || txtFax.areaCode == "" || txtFax.phoneExchange == "" || txtFax.phoneNumber == ""){
                      ClientWarning("Invalid or missing fax value. Please enter the fax number.");
                      return;
                    } if (txtFaxRecipient.value == ""){
                      if (txtFax.value == strAdjusterFax) {
                         txtFaxRecipient.value = strAdjusterName;
                      } else {
                         ClientWarning("Missing fax recipient name. Please enter the fax recipient name.");
                         return;
                      }
                    } else {
                      strSendValue = txtFax.value;
                      strSendVia = "Fax";
                    }
                    break;
                  case "FTP":
                     strSendValue = strFTPServer;
                     strSendVia = "FTP";
                     break;
                  case "CCAV":
                    strSendValue = "";
                    strSendVia = "Autoverse";
                    break;
                  case "WS":
                     strSendValue = oBundlingProfile.sendValue;
                     strSendVia = oBundlingProfile.originalSendVia;
                     oSendNode.setAttribute("via", strSendVia);
                     break;
                }
                
                oSendNode.setAttribute("value", strSendValue);
                oSendNode.setAttribute("description", selSendVia.text);
                oSendNode.setAttribute("recipient", txtFaxRecipient.value);
                oSendNode.setAttribute("from", strUserName);
                oSendNode.setAttribute("date", strToday);
                oSendNode.setAttribute("jobName", "APD - " + selSendVia.text + " - LYNX ID: " + strLynxID + " - Document Processing");
                oSendNode.setAttribute("PackageType", (strPackageType != "" ? strPackageType : "PDF"));
                oSendNode.setAttribute("SaveAs", (selSaveAs.selectedIndex == -1 ? "Other" : selSaveAs.text));
                oSendNode.setAttribute("SaveAsID", (selSaveAs.selectedIndex == -1 ? "" : selSaveAs.value));
                oSendNode.setAttribute("ClaimAspectServiceChannelID", strClaimAspectServiceChannelID);
                oSendNode.setAttribute("PertainsTo", strPertainsTo);
                oSendNode.setAttribute("SettingsOverride", (selPreference.value == "OVR"));
                oSendNode.setAttribute("FTPLogonID", strFTPLogonID);
                oSendNode.setAttribute("FTPPassword", strFTPPassword);
                oSendNode.setAttribute("FTPServer", strFTPServer);
                oSendNode.setAttribute("FTPServerPort", strFTPServerPort);
                oSendNode.setAttribute("FTPPath", strFTPPath);
                oSendNode.setAttribute("autoNotifyAdjuster", strAutoNotifyAdjusterFlag);
                
                oNode = oXML.createElement("User");
                oNode.setAttribute("id", strUserID);
                oNode.setAttribute("Name", strUserName);
                oNode.setAttribute("email", strUserEmailAddress);
                oSendNode.appendChild(oNode);

                oNode = oXML.createElement("EmailSubject");
                oNode.appendChild(oXML.createCDATASection(strEmailSubject));
                oSendNode.appendChild(oNode);
                
                strEmailBody = strEmailBody.replace(/\$SentVia\$/g, strSendVia);
                strEmailBody = strEmailBody.replace(/\$SentTo\$/g, strSendValue);
                
                oNode = oXML.createElement("EmailBody");
                oNode.appendChild(oXML.createCDATASection(strEmailBody));
                oSendNode.appendChild(oNode);

                oNode = oXML.createElement("PackageType");
                oNode.text = strPackageType;
                oSendNode.appendChild(oNode);
                
                oNode = oXML.createElement("MergedFileName");
		if(selBundling.text == "Acknowledgement of Claim" || selBundling.text == "No Inspection (Stale) Alert & Close" || selBundling.text == "Assignment Cancellation"){
			if(selBundling.text == "Acknowledgement of Claim")
				strSplit = strMergedFileName.replace("Claim Documents", "Lynx Acknowledgement of Claim");
      else if(selBundling.text == "Assignment Cancellation")
        strSplit = strMergedFileName.replace("Claim Documents", "Assignment Cancellation");
			else
				strSplit = strMergedFileName.replace("Claim Documents", "Notification of a Stale Claim");

			strSplit = strSplit.split("-");
			if(strSplit.length > 0)
				strSplit[(strSplit.length)-1] = strSourceApplicationPassThruData + "-" + strSplit[(strSplit.length)-1];
			
			oNode.text = strSplit.join("-");}
		else
	                oNode.text = strMergedFileName;
                oSendNode.appendChild(oNode);
                
                oNode2 = oXML.createElement("Success");
                oNode = oXML.createElement("Subject");
                oNode.appendChild(oXML.createCDATASection(strSuccessSubject));
                oNode2.appendChild(oNode);

                oNode = oXML.createElement("Body");
                oNode.appendChild(oXML.createCDATASection(strSuccessBody));
                oNode2.appendChild(oNode);

                oSendNode.appendChild(oNode2);
                
                oNode2 = oXML.createElement("Failure");
                oNode = oXML.createElement("Subject");
                oNode.appendChild(oXML.createCDATASection(strFailureSubject));
                oNode2.appendChild(oNode);

                oNode = oXML.createElement("Body");
                oNode.appendChild(oXML.createCDATASection(strFailureBody));
                oNode2.appendChild(oNode);

                oSendNode.appendChild(oNode2);

                oRoot.appendChild(oSendNode);
                oNode = oXML.createElement("Message");
                oNode.setAttribute("id", "");
                oNode.setAttribute("format", "text");
                oNode.appendChild(oXML.createCDATASection(getBasicMessage()));
                oRoot.appendChild(oNode);
                
                oNode = oXML.createElement("MessageEscaped");
                oNode.setAttribute("format", "text");
                oNode.appendChild(oXML.createCDATASection(escape(getBasicMessage())));
                oRoot.appendChild(oNode);

                oNode = oXML.createElement("MessageHTML");
                oNode.setAttribute("format", "html");
                oNode.appendChild(oXML.createCDATASection(strJobMessageHTML));
                oRoot.appendChild(oNode);

                oNode = oXML.createElement("Claim");
                oNode.setAttribute("InsuranceCompanyID", strInsCoId);
                oNode.setAttribute("ClientClaimNumber", strClientClaimNumber);
                oNode.setAttribute("LynxID", strLynxID);
                oNode.setAttribute("LossDate", strLossDate);
                oNode.setAttribute("InsuredName", strInsuredName);
                oNode.setAttribute("Adjuster", strAdjusterName);
                oNode.setAttribute("AdjusterEmail", strAdjusterEmail);
                oNode.setAttribute("PolicyNumber", strPolicyNumber);
                oNode.setAttribute("AnalystName", strAnalystName);
                oNode.setAttribute("AnalystPhone", strAnalystPhone);
                
                oRoot.appendChild(oNode);

                oNode = oXML.createElement("Vehicle");
                oNode.setAttribute("claimAspectID", strVehClaimAspectID);
                oNode.setAttribute("statusID", strVehStatusID);
                oNode.setAttribute("number", strVehicleNum);
                oNode.setAttribute("SourceApplicationPassThruData", strSourceApplicationPassThruData);
                oNode.setAttribute("year", strVehicleYear);
                oNode.setAttribute("make", strVehicleMake);
                oNode.setAttribute("model", strVehicleModel);
                oNode.setAttribute("isNugen", strIsNugen);
                oRoot.appendChild(oNode);
                
                oNode = oXML.createElement("ClaimInfo");
                oNode.appendChild(xmlClaimInfo.selectSingleNode("//Claim").cloneNode(true));
                oRoot.appendChild(oNode);
                
                aDocCounter = new Array();
                var strAttachments, iExceptionCounter;
                arrExceptionCounter = new Array();
                arrTIFFileCounter = new Array();
                strAttachments = "*  " + strMergedFileName + "." + strPackageType.toLowerCase() + "<br/>";

                for (var i = 0; i < aDocuments.length; i++){
                    var oChkDoc = document.getElementById(aDocuments[i]);
                  if (oChkDoc) {
                    if (oChkDoc.value == 1){
                      blnMerge = true;
                      strExceptionOutputFormat = "";
                      var strDocumentName = oChkDoc.parentElement.parentElement.cells[9].innerText;
		      //alert("strDocumentName : " + strDocumentName);
                      var strAttachmentType = oChkDoc.parentElement.parentElement.cells[10].innerText;
                      var strDocumentID = aDocuments[i].substr(3);
                      var oDocNode = xmlDocuments.selectSingleNode("/Root/Document[@DocumentID='" + strDocumentID + "']");
                      var sDocumentTypeID = oDocNode.getAttribute("DocumentTypeID");
                      var oException = xmlExceptions.selectSingleNode("/Root/Reference[@List='ReturnDocExceptions' and @ReferenceID='" + sDocumentTypeID + "']");
                      var blnDTP = (oChkDoc.parentElement.parentElement.cells[6].innerText == "Yes");
                      var blnFinalEstimate = (oChkDoc.parentElement.parentElement.cells[5].innerText == "Yes");
                      var imgLoc=oDocNode.getAttribute("ImageLocation");
                      var docType = imgLoc.substr(imgLoc.lastIndexOf(".")+1, imgLoc.length-imgLoc.lastIndexOf(".")+1);
                     
                      
                      if(strInsCoId == "387" && docType.toLowerCase()=="tif")
                      {
                          TempArrDocName = new Array();
                          TempArrDocName = strDocumentName.split(",");
                          TempArrDocName = TempArrDocName[1].split("-");
                            arrTIFFileCounter += TempArrDocName[0].Trim() + " - " + imgLoc.replace(imgLoc.substring(0,14),"") + "\n";
                       }
                      
                      if (blnDTP == true && blnFinalEstimate == true){
                        strAttachmentType = "Direction To Pay and Final Estimate";
                      } else if (blnDTP == true) {
                        strAttachmentType = "Direction To Pay";
                      } else if (blnFinalEstimate == true){
                        strAttachmentType = "Final Estimate";
                      }
                      
                                            
                      /*if ((oChkDoc.parentElement.parentElement.cells[10].innerText.indexOf("Photo") != -1) && (selSendVia.value == "FAX")){
                        ClientWarning("Cannot send photos (selection item #" + (i + 1) + ") via fax due to file size limitations.");
                        return;
                      }*/
                      
                      //iExceptionCounter = updateCounter(oChkDoc.parentElement.parentElement.cells[10].innerText, oChkDoc.parentElement.parentElement.cells[11].innerText);
                      iExceptionCounter = updateCounter(strAttachmentType, oChkDoc.parentElement.parentElement.cells[11].innerText);

                      if (oException){
                        blnMerge = false;
                        var strOutputFile = oChkDoc.parentElement.parentElement.cells[9].innerText;

						//--- 18Jun2013 - TVD - Removing special characters from the outputfile name.
						strOutputFile = strOutputFile.replace(/[&\/\\#,+()$~%'":*?<>{}]/g,'');

                        strExceptionOutputFormat = oException.getAttribute("OutputTypeCD");
                        //alert(oException.xml);
                        strAttachments += "*  " + strOutputFile.substr(0, strOutputFile.length - 4) + " (" + iExceptionCounter + ")" + strOutputFile.substr(strOutputFile.length - 4) + "<br/>";
                      } else
                        blnMerge = true;
                      
                      oNode = oXML.createElement("Document");
                      oNode.setAttribute("id", strDocumentID);
                      oNode.setAttribute("createDate", oChkDoc.parentElement.parentElement.cells[4].innerText);
                      oNode.setAttribute("sequenceNumber", oChkDoc.parentElement.parentElement.cells[11].innerText);
                      oNode.setAttribute("documentTypeID", sDocumentTypeID);
                      oNode.setAttribute("documentTypeName", strAttachmentType);
                      oNode.setAttribute("documentType", docType);
                      oNode.setAttribute("imageLocation", oDocNode.getAttribute("ImageLocation"));
                      oNode.setAttribute("outputFormat", strExceptionOutputFormat);
                      
                      //--- 12Jun2012 - TVD - Renaming photos to include the lynxid
                      var sAttachmentName = "";
                      sAttachmentName = oChkDoc.parentElement.parentElement.cells[9].innerText;
                      //update the original LynxId-VehNum with the current selected Vehicle Number
                      var oldLynxidVehNum = strLynxID + "-" + strVehNum;
                      sAttachmentName = sAttachmentName.replace(oldLynxidVehNum, strLynxID + "-" + strVehicleNum);
                      sAttachmentName = sAttachmentName.replace(/.jpg/g,"-" + strLynxID + "-" + strVehicleNum + ".jpg");

					  //--- 18Jun2013 - TVD - Removing special characters from the attachement name.
					  sAttachmentName = sAttachmentName.replace(/[&\/\\#,+()$~%'":*?<>{}]/g,'');

                      if (iExceptionCounter > 1){
                         sAttachmentName = sAttachmentName.replace(strAttachmentType, strAttachmentType + " (" + iExceptionCounter + ")")
                      }
                      
                      oNode.setAttribute("outputFileName", sAttachmentName);
                      
                      oNode.setAttribute("merge", (blnMerge ? "true" : "false"));
                      oNode.setAttribute("exception", (oException == null ? "false" : "true"));
                      oRoot.appendChild(oNode);
                    }
                  }
                }
               
               if (arrTIFFileCounter.length > 0){
                     alert("The electronic document transmittal you are attempting, include files which use a restricted TIFF file type. These TIFF files need to be manually converted to PDF files, re-uploaded to replace the TIFF files, and then selected in the bundle for transmittal. A systematic solution for the need to convert TIFF to PDF file types is under development.\n\n" + "The following restricted TIF files needing PDF conversion are included in the bundle:\n\n" + arrTIFFileCounter);
                     retrun;
                     }
                  
                var aData, iCount, strDocName, strNotesMsg;
                
                if (aDocCounter.length > 0) {
                   strNotesMsg = "";
                  for (var i = 0; i < aDocCounter.length; i++){
                    aData = aDocCounter[i].split("|");
                    strDocName = aData[0];
                    iCount = aData[1];
                    
                    strNotesMsg += "  * " + (iCount > 1 ? iCount : "") + " " + strDocName + (iCount > 1 && (strDocName.substr(strDocName.length - 1, 1) != "s")? "s" : "") + "\n";
                  }

                  oNode = oXML.createElement("AttachmentsHTML");
                  oNode.appendChild(oXML.createCDATASection(strAttachments));
                  oRoot.appendChild(oNode);

                  strNotesMsg = "The following documents were sent via " + strSendVia +
                                ((strSendValue != "") ? " to " + strSendValue + ":\n" : "") +
                                strNotesMsg;
                  oNode = oXML.createElement("NotesMsg");
                  oNode.appendChild(oXML.createCDATASection(escape(strNotesMsg)));
                  oRoot.appendChild(oNode);
                }

                //window.clipboardData.setData("Text", oXML.xml);
                //alert(oXML.xml);return;
                //alert(aDocCounter.join("\n")); return;
                
                with (formGen){
                  theAction.value = "Generate";
                  txtDocumentsXML.innerText = oXML.xml;
                }
              }
              
              btnSend.CCDisabled = true;
              btnCancel.CCDisabled = true;
              
              //------ 19Mar2014 - TVD - Fire Workflow Salvage Bid Event --------//
              if (strTotalLossAlertBundleID == selBundling.value) {
                //--- Fire the Salvage Bid Workflow event ---//
                var sProc = "uspWorkflowNotifyEvent";
  
                // EventID 88 = Salvage Bid Required event
                var sRequest = "EventID=88" +
                  "&ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID +
                  "&Description=" + "Total Loss Alert - Salvage Bid Required" +
                  "&ConditionValue=PS" +
                  "&UserID=" + gsUserID;
        
                  //alert(sRequest);
                
                  var aRequests = new Array();
                  aRequests.push({ procName: sProc,
                      method: "executespnp",
                      data: sRequest
                  });
                  var objRet = XMLSave(makeXMLSaveString(aRequests));
              }

              //------ 24Mar2014 - TVD - Fire Workflow Vehicle Condition Event --------//
              if (strTotalLossAlertBundleID == selBundling.value) {
                //--- Fire the Vehicle Condition Workflow event ---//
                var sProc = "uspWorkflowNotifyEvent";
  
                // EventID 89 = Vehicle Condition Report Required event
                var sRequest = "EventID=89" +
                  "&ClaimAspectServiceChannelID=" + gsClaimAspectServiceChannelID +
                  "&Description=" + "Total Loss Alert - Vehicle Condition Report Required" +
                  "&ConditionValue=PS" +
                  "&UserID=" + gsUserID;
        
                  //alert(sRequest);
                
                  var aRequests = new Array();
                  aRequests.push({ procName: sProc,
                      method: "executespnp",
                      data: sRequest
                  });
                  var objRet = XMLSave(makeXMLSaveString(aRequests));
              }

              stat1.Show("Please wait...");
              window.setTimeout("submit2()", 100);
            }
          }
          
          function updateCounter(strDocumentName, strSupplementNum){
            //try {
            var i, bFound;
            var iCount = 1;
            if (strDocumentName.Trim() == "") return;
            bFound = false;
            if (strSupplementNum != "")
               strDocumentName += " " + strSupplementNum;

            for (i = 0; i < aDocCounter.length; i++){
              if (aDocCounter[i].indexOf(strDocumentName + "|") != -1){
                bFound = true;
                break;
              }
            }
            
            if (bFound){
              aData = aDocCounter[i].split("|");
              aData[1] = iCount = parseInt(aData[1], 10) + 1;
              aDocCounter[i] = aData.join("|");
            } else {
              aDocCounter.push(strDocumentName + "|1");
              iCount = 1;
            }
            //} catch (e) {alert(e.description)}
            
            return iCount;
          }
          
          function submit2(){
              formGen.submit();
          }
          
          function doCancel(){
            window.close();
          }
          
          function validateEmail(){
            var bValid = false;
            var strEmailAddress = txtEmail.value;
            if (strEmailAddress.indexOf(";") == -1){
              //single email address
              if (strEmailAddress == "" || isEmail(strEmailAddress) == false){
                ClientWarning("Invalid or missing email address. Please enter the email address. For multiple email addresses, delimit them with semi-colon.");
                bValid = false;
              } else 
                bValid = true;
            } else {
              //multiple email addresses. Need to validate every one of them
              var aEmailAddress = strEmailAddress.split(";");
              var strValidationMsg = "";
              for (var i = 0; i < aEmailAddress.length; i++){
                if (aEmailAddress[i] != "") {
                  if (isEmail(aEmailAddress[i]) == false){
                    strValidationMsg += "Invalid email address \"" + aEmailAddress[i] + "\"<br/>";
                  } else {
                    strEmailAddress += aEmailAddress[i] + ";";
                  }
                }
              }
              
              if (strValidationMsg != ""){
                ClientWarning(strValidationMsg);
              } else
                bValid = true;
            }
            
            return bValid;
          }
          
          function selectDocument(obj){
            if (obj.value == 1){
              //add Document to the array
              aDocuments.push(obj.id);
            } else {
              var id = obj.id;
              for (var i = 0; i < aDocuments.length; i++){
                if (aDocuments[i] == id){
                  var aNewArray = aDocuments.slice(0, i);
                  aDocuments = aNewArray.concat(aDocuments.slice(i+1));
                  obj.parentElement.parentElement.cells[1].innerText = "";
                }
              }
            }
            updateOrder();
            /*var strDocName = obj.caption;
            var blnFound = false;
            var strTemp = "";
            var intDocCount = 0;
            var blnDTP = (obj.parentElement.parentElement.cells[6].innerText == "Yes");
            var blnFinalEst = (obj.parentElement.parentElement.cells[5].innerText == "Yes");
            if (blnDTP && blnFinalEst){
               strDocName = "Direction To Pay/Final Estimate"
            } else if(blnDTP) {
               strDocName = "Direction To Pay"
            } else if (blnFinalEst){
               strDocName = "Final Estimate"
            }
            
            for (var i = 0; i < aDocumentNames.length; i++){
               strTemp = aDocumentNames[i];
               intDocCount = parseInt(strTemp.split("|")[0], 10);
               strTemp = strTemp.split("|")[1];
               if (strTemp == strDocName){
                  if (obj.value == 1) 
                     intDocCount++;
                  else
                     intDocCount--;
                  strTemp = intDocCount + "|" + strDocName;
                  aDocumentNames[i] = strTemp;
                  blnFound = true;
               }
            }
            if (blnFound == false){
               aDocumentNames.push("1|" + strDocName);
            }*/
            
            if (blnMessageTemplateReady == true){
               updateDocumentList();
            }
          }
          
          function checkPhoto(obj){
             /*if ((obj.parentElement.parentElement.cells[10].innerText.indexOf("Photo") != -1) && (strPackageType == "TIF")){
                event.returnValue = false;
                ClientWarning("The output format is set to tif and system cannot send photos as tif due to file size limitations.");
             }*/
          }
          
          function updateOrder(){
            //clear the names. we will build the names based on the selection order
            aDocumentNames = new Array();
            var strCaption = "";
            var blnDTP = false;
            var blnFinalEstimate = false;
            for (var i = 0; i < aDocuments.length; i++){
               strCaption = "";
               blnDTP = false;
               blnFinalEstimate = false;
               var oChk = document.getElementById(aDocuments[i]);
               if (oChk) {
                  oChk.parentElement.parentElement.cells[1].innerText = (i + 1);
                  strCaption = oChk.caption;
                  blnDTP = (oChk.parentElement.parentElement.cells[6].innerText == "Yes");
                  blnFinalEstimate = (oChk.parentElement.parentElement.cells[5].innerText == "Yes");

                  if (blnDTP == true && blnFinalEstimate == true){
                     strCaption = "Direction To Pay/Final Estimate";
                  } else if(blnDTP == true) {
                     strCaption = "Direction To Pay";
                  } else if (blnFinalEstimate == true){
                     strCaption = "Final Estimate";
                  }
                  
                  addDocumentName(strCaption);
               }
            }
          }

          function addDocumentName(strDocumentName){
            var blnFound = false;
            for (var i = 0; i < aDocumentNames.length; i++){
               strTemp = aDocumentNames[i];
               intDocCount = parseInt(strTemp.split("|")[0], 10);
               strTemp = strTemp.split("|")[1];
               if (strTemp == strDocumentName){
                  intDocCount++;
                  strTemp = intDocCount + "|" + strDocumentName;
                  aDocumentNames[i] = strTemp;
                  blnFound = true;
               }
            }
            if (blnFound == false){
               aDocumentNames.push("1|" + strDocumentName);
            }
          }
          
          function setOutputFormat(){
             strPackageType = (rbGrpOption.value == "1" ? "TIF" : "PDF");
          }
          
          function showDocuments(){
            var strClaimAspectID = selClaimAspect.value;
            var objDocuments = xmlDocuments.selectNodes("/Root/Document");
            var iDocCount = objDocuments.length;
            var blnChkDisabled = false;
            blnPreferredDestinationLoaded = false;
            //arrSelectedDocuments = new Array();
            for (var i = 0; i < iDocCount; i++){
               var strID = "chk" + objDocuments[i].getAttribute("DocumentID");
               var objChk = document.getElementById(strID);
               if (objChk){
                  //uncheck all checked documents
                  if (objChk.value == 1){
                     blnChkDisabled = false;
                     if (objChk.CCDisabled) blnChkDisabled = true;
                     objChk.value = 0;
                     if (blnChkDisabled) objChk.CCDisabled = true;
                  }
                  
                  //if the document pertains to the vehicle selected then show it. else hide it
                  if (objDocuments[i].getAttribute("ClaimAspectID") == strClaimAspectID){
                     objChk.parentElement.parentElement.style.display = "inline";
                  } else {
                     objChk.parentElement.parentElement.style.display = "none";
                  }
               }
            }
            if (xmlClaimAspect.selectSingleNode("/Root/ClaimAspect[@ClaimAspectID='" + strClaimAspectID + "']").getAttribute("SourceCCAV") == "1"){
               if (selSendVia.GetValueFromText("Autoverse") == null && iCCAVIdx == -1){
                  if (selSendVia.GetValueFromText("Autoverse") == null){
                     selSendVia.AddItem("CCAV", "Autoverse");
                  }
               }
            } else {
               if (selSendVia.GetValueFromText("Autoverse") != null){
                  var iLen = selSendVia.Options.length;
                  for (var i = 0; i < iLen; i++) {
                     if (selSendVia.Options[i].text == txt) {
                        selSendVia.RemoveItem(i);
                     }
                  }
               }
            }
            //load message templates for this vehicle
            selMsgTemplate.Clear();
            var objClaimAspect = xmlClaimAspect.selectSingleNode("/Root/ClaimAspect[@ClaimAspectID='" + strClaimAspectID + "']");
            if (objClaimAspect) {
               //alert(objClaimAspect.xml);
               strServiceChannelCD = objClaimAspect.getAttribute("ServiceChannelCD");
               strShopState = objClaimAspect.getAttribute("ShopState");
               strClaimAspectServiceChannelID = objClaimAspect.getAttribute("ClaimAspectServiceChannelID");
               strPertainsTo = objClaimAspect.getAttribute("PertainsTo");
               strVehicleNum = objClaimAspect.getAttribute("ClaimAspectNumber");
	       strSourceApplicationPassThruData = objClaimAspect.getAttribute("SourceApplicationPassThruData");
               strVehicleYear = (objClaimAspect.getAttribute("VehicleYear") ? objClaimAspect.getAttribute("VehicleYear") : "");
               strVehicleMake = (objClaimAspect.getAttribute("VehicleMake") ? objClaimAspect.getAttribute("VehicleMake") : "");
               strVehicleModel = (objClaimAspect.getAttribute("VehicleModel") ? objClaimAspect.getAttribute("VehicleModel") : "");
               strSalvageVendorEmail = (objClaimAspect.getAttribute("SalvageVendorEmailAddress") ? objClaimAspect.getAttribute("SalvageVendorEmailAddress") : "");
               blnLienHolder = (objClaimAspect.getAttribute("LienHolderID") != "");
               var oldLynxidVehNum = strLynxID + "-" + strVehNum;
               strEmailSubject = strEmailSubject.replace(oldLynxidVehNum, strLynxID + "-" + strVehicleNum);

               //------ 19Mar2014 - TVD - Fire Workflow Event --------//
               gsClaimAspectServiceChannelID = strClaimAspectServiceChannelID;

               if (strServiceChannelCD && strServiceChannelCD != ''){
                  var objMessages = xmlReference.selectNodes("/Root/Reference[@List='Messages' and (@Name = '" + strServiceChannelCD + "' or @Name = '')]");
                  //var objMessages = xmlReference.selectNodes("/Root/Reference[@List='Messages']");
                  var strMessageDescription = "";
                  if (objMessages){
                     for (var i = 0; i < objMessages.length; i++){
                        strMessageDescription = objMessages[i].getAttribute("MessageDescription");
                        selMsgTemplate.AddItem(objMessages[i].getAttribute("ReferenceID"), strMessageDescription.split("|")[0]);
                     }
                  }
                  //alert("xsl/BundlingPreferred_" + strServiceChannelCD + "_" + strInsCoId);
                  xmlPreferredDestination.src = "xsl/BundlingPreferred_" + strServiceChannelCD + "_" + strInsCoId + ".xsl";
               }
            }
            if (blnClaimInfoReady) {
               var obj = xmlClaimInfo.selectSingleNode("/Root")
               if (obj){
                  obj.setAttribute("ClaimAspectID", selClaimAspect.value)
               }
            }
            
            loadBundlingProfiles();
          }
          
          function loadBundlingProfiles(){
            strServiceChannelCD = "";
            var strBundlingName = "";
            //clear all the selections in the bundling
            selBundling.Clear();
            
            var strClaimAspectID = selClaimAspect.value;
            var objClaimAspect = xmlClaimAspect.selectSingleNode("/Root/ClaimAspect[@ClaimAspectID='" + strClaimAspectID + "']");
            if (objClaimAspect) {
               strServiceChannelCD = objClaimAspect.getAttribute("ServiceChannelCD");
               strIsNugen = objClaimAspect.getAttribute("IsNugen");
               strIsWebservice = objClaimAspect.getAttribute("IsWebservice");
            }
            
            var objBundlingProfile = xmlBundling.selectNodes("/Root/Bundling[@ServiceChannelCD = '' or @ServiceChannelCD = '" + strServiceChannelCD + "']");
            for (var i = 0; i < objBundlingProfile.length; i++){
               strBundlingName = objBundlingProfile[i].getAttribute("Name");
               if (strBundlingName.substr(0, 5) == "SHOP:") strBundlingName = strBundlingName.substr(5);
               selBundling.AddItem(objBundlingProfile[i].getAttribute("BundlingID"), strBundlingName);
            }
          }
      
          function selectBundleDocuments(){
            var iBundlingID = selBundling.value;
            //var blnShopBundling = false;
            var strMessageTemplateID = "";
            //reset values
            txtEmail.value = "";
            txtFax.value = "";
            txtFaxRecipient.value = "";
            selSaveAs.selectedIndex = -1;
            selSendVia.selectedIndex = -1;
            selPreference.selectedIndex = -1;
            blnProfileError = false;
            strProfileErrorMsg = "";
            strProfileDestValue = "";
            strProfileDest = "";
            blnShopBundling = false;
            strPackageType = strDefaultPackageType;
            
            if (strPackageType == "TIF"){
               rbOptionTif.value = 1;
               rbOptionPdf.value = 0;
               rbGrpOption.value = 1;
            } else {
               rbOptionTif.value = 0;
               rbOptionPdf.value = 2;
               rbGrpOption.value = 2;
            }
            
            divMissingDocs.style.display = "none";
            
            //set the bundling profile
            var objBundlingProfile = xmlBundling.selectSingleNode("/Root/Bundling[@BundlingID='" + iBundlingID + "']");
            if (objBundlingProfile){
               oBundlingProfile.outputFormat = objBundlingProfile.getAttribute("ReturnDocPackageTypeCD");
               oBundlingProfile.sendVia = objBundlingProfile.getAttribute("ReturnDocRoutingCD");
			   //alert(objBundlingProfile.getAttribute("ReturnDocRoutingCD") & "<br/>");
			   //alert(oBundlingProfile.sendVia & "<br/>");
               oBundlingProfile.sendValue = objBundlingProfile.getAttribute("ReturnDocRoutingValue");
               oBundlingProfile.saveAs = objBundlingProfile.getAttribute("DocumentTypeID");
               oBundlingProfile.AutoNotifyAdjusterFlag = objBundlingProfile.getAttribute("AutoNotifyAdjusterFlag");
               strMessageTemplateID = objBundlingProfile.getAttribute("MessageTemplateID");
               //***** strAutoNotifyAdjusterFlag = oBundlingProfile.AutoNotifyAdjusterFlag;

               if (blnPreferredDestinationLoaded){
                  if (blnClaimInfoReady){
				  
                     var obj = xmlClaimInfo.selectSingleNode("/Root")
                     if (obj){
                        obj.setAttribute("bundlingProfileName", objBundlingProfile.getAttribute("Name"))
                        //obj.setAttribute("bundlingProfileName", "RRP - Stale Alert & Close")
                     }
                     var strResult = xmlClaimInfo.transformNode(xmlPreferredDestination);
                     xmlPreferredProfileDestination.async = false;
                     xmlPreferredProfileDestination.loadXML(strResult);
                     
                     obj = xmlPreferredProfileDestination.selectSingleNode("//Send");
                     if (obj){
					 //alert(obj.getAttribute("via"));
                       oBundlingProfile.sendVia = obj.getAttribute("via");
                       oBundlingProfile.sendValue = obj.getAttribute("value");
                     }
                  }
               }
               
               if (objBundlingProfile.getAttribute("Name").substr(0, 5) == "SHOP:"){
                  blnShopBundling = true;
                  oBundlingProfile.sendPreference = "OVR";
                  oBundlingProfile.sendVia = "FAX";
                  oBundlingProfile.sendValue = "";
                  oBundlingProfile.sendFaxName = "";
                  
				  //-------------------------------//
				  // 17Apr2013 - TVD              -//
				  // Changing this to trigger off -//
				  // RRP instead of an InscCo     -//
				  //-------------------------------//
                  //if (strInsCoId == 176 || strInsCoId == 304){
                  if (strInsCoId == 176 || strServiceChannelCD == "RRP"){
                     if (strPursuitAuditShopEmail != ""){
                        oBundlingProfile.sendVia = "EML";
                        oBundlingProfile.sendValue = strPursuitAuditShopEmail;
                        oBundlingProfile.sendFaxName = "";
                     } else {
                        oBundlingProfile.sendVia = "FAX";
                        oBundlingProfile.sendValue = strPursuitAuditShopFax;
                        oBundlingProfile.sendFaxName = strPursuitAuditShopName;
                     }
                  }
               }


               //oBundlingProfile.sendFaxName = strInsCoName;
               if (typeof(oBundlingProfile.sendValue) == "string"){
                  strProfileDest = oBundlingProfile.sendVia;
                  var strValue = oBundlingProfile.sendValue;
                  if (strValue.indexOf("$") != -1){
                     if (strValue.indexOf("$ADJUSTER$") != -1) {
                        if (strAdjusterActive == "1"){
                           strValue = strValue.replace(/\$ADJUSTER\$/g,  (strAdjusterActive == "1" ? strAdjusterEmail : ""));
                        } else {
                           blnProfileError = true;
                           strProfileErrorMsg = "Adjuster on the claim is inactive. Cannot send documents to adjuster.";
                        }
                     }
                     if (strValue.indexOf("$SALVAGEVENDOR$") != -1) {
                        strValue = strValue.replace(/\$SALVAGEVENDOR\$/g, strSalvageVendorEmail);
                     }
                     //if (strValue.indexOf("$SHOPFAXOREMAIL$") != -1) {
                     //   if (strPursuitAuditShopEmail != ""){
                    //       strValue = strPursuitAuditShopEmail;
                    //       oBundlingProfile.sendVia = "EML";
                    //    } else if (strPursuitAuditShopFax != ""){
                    //       strValue = strPursuitAuditShopFax;
                    //       oBundlingProfile.sendVia = "FAX";
                    //    }
                    //    strProfileDest = oBundlingProfile.sendVia;
                    // }
                  }
                  oBundlingProfile.sendValue = strValue;
                  strProfileDestValue = strValue;
               }
               
               
               if (oBundlingProfile.sendVia == ""){
                  if (strDefaultSendVia != ""){
                     oBundlingProfile.sendVia = strDefaultSendVia;
                     oBundlingProfile.sendValue = strDefaultSendViaValue;
                  } else if (strAdjusterEmail != "")
                     oBundlingProfile.sendVia = "EML";
                  else if (strAdjusterFax != "")
                     oBundlingProfile.sendVia = "FAX";
               }
               
               switch (oBundlingProfile.sendVia){
                  case "EML":
                     if (oBundlingProfile.sendValue == "") {
                        oBundlingProfile.sendValue = (strDefaultSendViaValue != "" ? strDefaultSendViaValue : (strAdjusterActive == "1" ? strAdjusterEmail : "" ));
                        oBundlingProfile.sendFaxName = "";
                     }
                     break;
                  case "FAX":
                     if (oBundlingProfile.sendValue == "") {
                        oBundlingProfile.sendValue = (strDefaultSendViaValue != "" ? strDefaultSendViaValue : (strAdjusterActive == "1" ? strAdjusterFax : "" ));
                        oBundlingProfile.sendFaxName = (strDefaultSendViaValue != "" ? strInsCoName : (strAdjusterActive == "1" ? strAdjusterName : strInsCoName ));
                     }
                     break;
                  case "FTP":
                     if (oBundlingProfile.sendValue == "") {
                        oBundlingProfile.sendValue = strDefaultSendViaValue;
                        oBundlingProfile.sendFaxName = "";
                     }
               }
               
               if ((oBundlingProfile.sendVia == "CCAV") || 
                   (oBundlingProfile.sendVia != "CCAV" &&
                   oBundlingProfile.sendValue != "" && 
                   oBundlingProfile.saveAs != "")){
                  oBundlingProfile.sendPreference = "DEF";
               } else
                  oBundlingProfile.sendPreference = "OVR";
                  
                  //Change for Webservice configure from database
                  if ((oBundlingProfile.sendVia == "WS") || (oBundlingProfile.sendVia != "WS" &&
                   oBundlingProfile.sendValue != "" && 
                   oBundlingProfile.saveAs != "")){
                  oBundlingProfile.sendPreference = "DEF";
               } else
                  oBundlingProfile.sendPreference = "OVR";

               selMsgTemplate.selectedIndex = -1;
               setMessageTemplate("<div/>");
               if (strMessageTemplateID != "" && parseInt(strMessageTemplateID, 10) > 0){
                  blnMessageTemplateReady = false;
                  window.setTimeout("showMessage(" + strMessageTemplateID + ")", 100);
               }
               
               //now set the values
               setBundlingProfileValues();
               
               //unselect documents that where previously selected.
               var strClaimAspectID = selClaimAspect.value;
               var objDocs = xmlDocuments.selectNodes("/Root/Document[@ClaimAspectID='" + strClaimAspectID + "']");
               var objChk = null;
               if (objDocs){
                  for (var i = 0; i < objDocs.length; i++){
                     DocumentID = objDocs[i].getAttribute("DocumentID");
                     objChk = document.getElementById("chk" + DocumentID);
                     if (objChk){
                        objChk.CCDisabled = false;
                        if (objChk.value == 1) objChk.value = 0;
                     }
                  }
               }
               
               btnSend.CCDisabled = false;
              
               //now auto select the documents
               var objDocuments = objBundlingProfile.selectNodes("BundlingDocument[(@LossState='' or @LossState='" + strLossState + "') and (@ShopState='' or @ShopState='" + strShopState + "')]");
               var iDocCount = objDocuments.length;
               var DocumentTypeID, DirectionalCD, DirectionToPayFlag, DuplicateFlag, EstimateDuplicateFlag, ApprovedFlag;
               var EstimateTypeCD, FinalEstimateFlag, MandatoryFlag, SelectionOrder, VANFlag, DocumentID;
               var DocumentName = EstimateTypeFlag = "";
               var strXPath = "";
               var strMissing = "";
               var strXPath = "";
               var blnFinalEstimateSelected = blnDTPSelected = false;
               strDocumentList = "";
               var blnDocumentSelected = false;
               var blnDocMissing = false;
               
               clearDocumentChecklist();
               
               for (var i = 0; i < iDocCount; i++){
                  blnDocumentSelected = false;
                  strMissing = "";
                  DocumentTypeID = objDocuments[i].getAttribute("DocumentTypeID");
                  DirectionalCD = objDocuments[i].getAttribute("DirectionalCD");
                  DirectionToPayFlag = objDocuments[i].getAttribute("DirectionToPayFlag");
                  DuplicateFlag = objDocuments[i].getAttribute("DuplicateFlag");
                  EstimateDuplicateFlag = objDocuments[i].getAttribute("EstimateDuplicateFlag");
                  EstimateTypeCD = objDocuments[i].getAttribute("EstimateTypeCD");
                  FinalEstimateFlag = objDocuments[i].getAttribute("FinalEstimateFlag");
                  MandatoryFlag = objDocuments[i].getAttribute("MandatoryFlag");
                  SelectionOrder = objDocuments[i].getAttribute("SelectionOrder");
                  VANFlag = objDocuments[i].getAttribute("VANFlag");
                  ApprovedFlag = objDocuments[i].getAttribute("ApprovedFlag");
                  var objRef = xmlReference.selectSingleNode("/Root/Reference[@List='DocumentType' and @ReferenceID='" + DocumentTypeID + "']");
                  if (objRef){
                     DocumentName = objRef.getAttribute("Name");
                     EstimateTypeFlag = objRef.getAttribute("EstimateTypeFlag");
                  }
                  
                  if (strServiceChannelCD == "TL"){
                     if (DocumentName.indexOf("Lien") != -1){
                        //alert(DocumentName + ", " + );
                        if (blnLienHolder == false) MandatoryFlag = "0";
                     }
                  }
                  
                  if (blnAutoverse == true) DuplicateFlag = "1";
                  
                  if (EstimateTypeFlag != "1" && FinalEstimateFlag != "1" && DirectionToPayFlag != "1"){
                     //regular document
                     strXPath = "/Root/Document[@ClaimAspectID = '" + strClaimAspectID + "' and @DocumentTypeID = '" + DocumentTypeID + "'";
                     if (DuplicateFlag == "0") strXPath += " and @SendToCarrierStatusCD != 'Sent'";
                     strXPath += "]";
                     var objDoc = xmlDocuments.selectNodes(strXPath);
                     if (objDoc) {
                        if (objDoc.length > 0){
                           for (var j = 0; j < objDoc.length ; j++){
                              if ((DuplicateFlag == "1") || (DuplicateFlag == "0" && blnDocumentSelected == false)){
                                 DocumentID = objDoc[j].getAttribute("DocumentID");
                                 var objChk = document.getElementById("chk" + DocumentID);
                                 if (objChk){
                                    objChk.value = 1;
                                    blnDocumentSelected = true;
                                 }
                              }
                           }
                           if (blnDocumentSelected)
                              strDocumentList += "<li>" + DocumentName + "</li>";
                        } else {
                           if (MandatoryFlag == "1"){
                              //missing document
                              if (DocumentName != "")
                                 strMissing = DocumentName + (DuplicateFlag == "0" ? " [not already sent]" : "");
                              else
                                 strMissing = "DocumentTypeID: " + DocumentTypeID;
                           }
                        }
                     }
                  } else {
                     if (EstimateTypeFlag == "1" && FinalEstimateFlag == "0"){
                        //estimate
                        if (EstimateDuplicateFlag == "0"){
                           //get the latest estimate/supplement sequence number
                           var strSeqNo = "";
                           var strDocumentTypeID = "";
                           
                           strXPath = "//Root/Document[@ClaimAspectID = '" + strClaimAspectID + "' and //Root/Reference[@List='DocumentType' and @EstimateTypeFlag='1']/@ReferenceID = @DocumentTypeID";
                           if (strEarlyBillFlag == "1" && EstimateTypeCD == "A" && strServiceChannelCD != "DA"){
                              strXPath += " and @ApprovedFlag='1'";
                           } else if (EstimateTypeCD != "") {
                              strXPath += " and @EstimateTypeCD='" + EstimateTypeCD + "'";
                           }
                           strXPath += "]";
                           //alert(strXPath);
                           //window.clipboardData.setData("Text", strXPath);
                           var objDoc = xmlData.selectSingleNode(strXPath);
                           if (objDoc) {
                              strSeqNo = objDoc.getAttribute("SupplementSeqNumber");
                              strDocumentTypeID = objDoc.getAttribute("DocumentTypeID");
                              
                              //check if an estimate/supplement with the latest sequence number from A VAN Source exist
                              strXPath = "//Root/Document[@ClaimAspectID = '" + strClaimAspectID + "' and @DocumentTypeID = '" + strDocumentTypeID + "' and SupplementSeqNumber='" + strSeqNo + "' and //Root/Reference[@List='DocumentSource' and @VANFlag='1']/@ReferenceID = @DocumentSourceID";
                              if (EstimateTypeCD != "")
                                 strXPath += " and @EstimateTypeCD='" + EstimateTypeCD + "'";
                              strXPath += "]";
                              
                              var objDocEst = xmlData.selectSingleNode(strXPath);
                              if (objDocEst == null){
                                 objDocEst = objDoc; //select the manually uploaded latest estimate/supplement
                              }
                              
                              DocumentID = objDocEst.getAttribute("DocumentID");
                              var objChk = document.getElementById("chk" + DocumentID);
                              if (objChk){
                                 objChk.value = 1;
                                 blnDocumentSelected = true;
                              }
							  //-------------------------------//
							  // 17Apr2013 - TVD              -//
							  // Changing this to trigger off -//
							  // RRP instead of an InscCo     -//
							  //-------------------------------//
							  //manual hack. Bundling profile will not allow to setup E-O and E-A. So, E-O was added as S-O.
							//if (strInsCoId == "304") {
							  if (strServiceChannelCD == "RRP") {
	                            //if ((FinalEstimateFlag == "1" && blnFinalEstimateSelected == false) || (FinalBillEstimateFlag == 1)) {
	                            if (FinalEstimateFlag == "1") {
									DocumentName = (EstimateTypeCD != "" ? (EstimateTypeCD == "O" ? "Approved " : "Audited ") : "") + " Estimate/Supplement";
								}
								else {
									DocumentName = (EstimateTypeCD != "" ? (EstimateTypeCD == "O" ? "Initial " : "Audited ") : "") + " Estimate/Supplement";
								}
							}
								 if (ApprovedFlag == "1")
                                 DocumentName += " (Approved)";
                              if (blnDocumentSelected)
                                 strDocumentList += "<li>" + DocumentName + "</li>";
                           } else {
                              //no estimate/supplement exist.
                              if (MandatoryFlag == "1"){
                                 //missing document
                                 if (DocumentName != "")
                                    strMissing = DocumentName + (EstimateTypeCD != "" ? (EstimateTypeCD == "O" ? " (Original)" : " (Audited)") : "");
                                 else
                                    strMissing = "DocumentTypeID: " + DocumentTypeID;
                              }
                           }
                        } else {
                           //select multiple estimate/supplement
                           var strSeqNo = "";
                           var strDocumentTypeID = "";
                           strXPath = "//Root/Document[@ClaimAspectID = '" + strClaimAspectID + "' and //Root/Reference[@List='DocumentType' and @EstimateTypeFlag='1']/@ReferenceID = @DocumentTypeID";
                           if (EstimateTypeCD != "")
                              strXPath += " and @EstimateTypeCD='" + EstimateTypeCD + "'";
                           strXPath += "]";
                           
                           var objDoc = xmlData.selectSingleNode(strXPath);
                           if (objDoc) {
                              strSeqNo = parseInt(objDoc.getAttribute("SupplementSeqNumber"), 10);
                              strDocumentTypeID = objDoc.getAttribute("DocumentTypeID");
                              
                              if (isNaN(strSeqNo) == false){
                                 for (var j = strSeqNo; j >= 0; j--){
                                    //check if an estimate/supplement with the latest sequence number from A VAN Source exist
                                    strXPath = "//Root/Document[@ClaimAspectID = '" + strClaimAspectID + "' and @DocumentTypeID = '" + strDocumentTypeID + "' and SupplementSeqNumber='" + strSeqNo + "' and //Root/Reference[@List='DocumentSource' and @VANFlag='1']/@ReferenceID = @DocumentSourceID";
                                    if (EstimateTypeCD != "")
                                       strXPath += " and @EstimateTypeCD='" + EstimateTypeCD + "'";
                                    strXPath += "]";
                                    
                                    var objDocEst = xmlData.selectSingleNode(strXPath);
                                    if (objDocEst == null){
                                       strXPath = "//Root/Document[@ClaimAspectID = '" + strClaimAspectID + "' and //Root/Reference[@List='DocumentType' and @EstimateTypeFlag='1']/@ReferenceID = @DocumentTypeID";
                                       if (EstimateTypeCD != "")
                                          strXPath += " and @EstimateTypeCD='" + EstimateTypeCD + "'";
                                       strXPath += " and @SupplementSeqNumber='" + strSeqNo + "']";
                                       objDocEst = xmlData.selectSingleNode(strXPath); //select the manually uploaded latest estimate/supplement
                                    }
                                    
                                    if (objDocEst){
                                       DocumentID = objDocEst.getAttribute("DocumentID");
                                       var objChk = document.getElementById("chk" + DocumentID);
                                       if (objChk){
                                          objChk.value = 1;
                                          blnDocumentSelected = true;
                                       }
                                       if (blnDocumentSelected)
                                          strDocumentList += "<li>" + DocumentName + "</li>";
                                    }
                                    strSeqNo--;
                                 }
                              }
                           }
                        }
                     } else if (FinalEstimateFlag == "1" && blnFinalEstimateSelected == false){
                        //final estimate
                        //alert("Final Estimate");
                        strXPath = "/Root/Document[@ClaimAspectID = '" + strClaimAspectID + "' and (@FinalEstimateFlag='1' or @FinalBillEstimateFlag = '1')]";
                        /*if (strEarlyBillFlag == "1" && FinalEstimateFlag == "A"){
                           strXPath += " and @ApprovedFlag='1'";
                        } else if (EstimateTypeCD != "") {
                           strXPath += " and @EstimateTypeCD='" + EstimateTypeCD + "'";
                        }
                        strXPath += "]";*/
                        var objDoc = xmlDocuments.selectSingleNode(strXPath);
                        if (objDoc) {
                           DocumentID = objDoc.getAttribute("DocumentID");
                           var objChk = document.getElementById("chk" + DocumentID);
                           if (objChk){
                              objChk.value = 1;
                              blnFinalEstimateSelected = true;
                              blnDocumentSelected = true;
                           }
                           if (blnDocumentSelected)
                              strDocumentList += "<li>" + DocumentName + " [Final Estimate]</li>";
                        } else {
                           if (MandatoryFlag == "1"){
                              //missing document
                              if (DocumentName != "")
                                //if (strInsCoId == "304") {
								if (strServiceChannelCD == "RRP") {
                                 strMissing = "Approved " + DocumentName ;
								}
								else
								{
                                 strMissing = DocumentName + " [Final Estimate]";
								}
                              else
                                 strMissing = "DocumentTypeID: " + DocumentTypeID + " [Final Estimate]";
                           }
                        }
                     } else if (DirectionToPayFlag == "1" && blnDTPSelected == false) {
                        //direction to pay
                        //alert("DTP");
                        //strXPath = "/Root/Document[@ClaimAspectID = '" + strClaimAspectID + "' and @DocumentTypeID = '" + DocumentTypeID + "' and @DirectionToPayFlag='1']";
                        strXPath = "/Root/Document[@ClaimAspectID = '" + strClaimAspectID + "' and @DirectionToPayFlag='1']";
                        var objDoc = xmlDocuments.selectSingleNode(strXPath);
                        if (objDoc) {
                           DocumentID = objDoc.getAttribute("DocumentID");
                           var objChk = document.getElementById("chk" + DocumentID);
                           if (objChk){
                              objChk.value = 1;
                              blnDTPSelected = true;
                              blnDocumentSelected = true;
                           }
                           if (blnDocumentSelected)
                              strDocumentList += "<li>" + DocumentName + "</li>";
                        } else {
                           if (MandatoryFlag == "1"){
                              //missing document
                              if (DocumentName != "")
                                 strMissing = "Direction To Pay";
                              else
                                 strMissing = "DocumentTypeID: " + DocumentTypeID; 
                           }
                        }
                     }
                  }
                  if (strMissing != "") blnDocMissing = true;
                  if (strInsCoId == 304){
					addDocument2Checklist((strMissing == "" ? (FinalEstimateFlag == "1" ? "Approved Estimate/Supplement" : (DirectionToPayFlag == "1" ? "Direction To Pay" : DocumentName)) : strMissing), (strMissing != ""));
				  }
				  else {
					addDocument2Checklist((strMissing == "" ? (FinalEstimateFlag == "1" ? "Final Estimate" : (DirectionToPayFlag == "1" ? "Direction To Pay" : DocumentName)) : strMissing), (strMissing != ""));
				  }
				}
               

               if (blnDocMissing){
                  //ClientWarning("The following required documents for this bundling are missing:<br/>" + strMissing.replace(/\n/g, "<br/>"));
                  divMaskAll.style.display = "inline";
                  divMissingDocs.style.display = "inline";
                  if (selSendVia.value != "CCAV") {
                     btnSend.CCDisabled = (selPreference.value == "DEF");
                     divMissingDocAction.innerHTML = "Add the missing documents marked as <img src='/images/content_noaction_red.gif' align='absmiddle'/> to the vehicle and retry. You may choose to overrride to send a partial bundle.";
                  } else {
                     divMissingDocAction.innerHTML = "No action required. You may send this partial bundle.";
                  }
               } else {
                  btnSend.CCDisabled = false;
               }
               
               //lock the auto selection. user can override if they want to by selecting Override for Send Preference.
               if (selPreference.value == "DEF") enableDocuments(false);
            }
          }
          
          function setBundlingProfileValues(){
               blnProfileOverride = false;
               blnSettingProfileValues = true;
               selSendVia.CCDisabled = false;
               txtFax.CCDisabled = false;
               txtEmail.CCDisabled = false;
               txtFaxRecipient.CCDisabled = false;
               selPreference.CCDisabled = false;
               selSaveAs.CCDisabled = false;
			   //alert(oBundlingProfile.sendVia);
               //remove web services
               if (wsIndex > -1){
                  selSendVia.RemoveItem(wsIndex);
                  wsIndex = -1;
               }
               var strBundlingProfile = selBundling.text; 
               oBundlingProfile.originalSendVia = "";
               
               switch (oBundlingProfile.sendVia){
                  case "EML":
                     if (((strInsCoId == "304" && strIsNugen == "1") && (strBundlingProfile == "RRP - Closing Supplement" || strBundlingProfile == "RRP - Closing Total Loss" || strBundlingProfile == "RRP - Closing Inspection Complete" || strBundlingProfile == "RRP - No Inspection Alert & Close"))) {
                        selSendVia.AddItem("WS", "Web Services");
                        for (var i = 0; i < selSendVia.Options.length; i++){
                           if (selSendVia.Options[i].value == "WS"){
                              wsIndex = i;
                              break;
                           }
                        }
                        oBundlingProfile.originalSendVia = "EML";
                        oBundlingProfile.sendVia = "WS";
                     } 

		else if ((strInsCoId == "387" && strIsWebservice == "1") && (strBundlingProfile == "Cash Out Alert & Close" || strBundlingProfile == "Closing Repair Complete" || strBundlingProfile == "Closing Supplement" || strBundlingProfile == "Closing - Total Loss" || strBundlingProfile == "Rental Invoice" || strBundlingProfile == "Initial Estimate" || strBundlingProfile == "Acknowledgement of Claim" || strBundlingProfile == "No Inspection (Stale) Alert & Close")) {
                        selSendVia.AddItem("WS", "Web Services");
                        for (var i = 0; i < selSendVia.Options.length; i++){
                           if (selSendVia.Options[i].value == "WS"){
                              wsIndex = i;
                              break;
                           }
                        }
			//oBundlingProfile.originalSendVia = "EML";
                        oBundlingProfile.sendVia = "WS";
                        oBundlingProfile.sendValue = "";
			}
    else if ((strInsCoId == "194" && strIsWebservice == "1") && (strBundlingProfile == "Cash Out Alert & Close" || strBundlingProfile == "Closing Repair Complete" || strBundlingProfile == "Closing Supplement" || strBundlingProfile == "Closing - Total Loss" || strBundlingProfile == "Rental Invoice" || strBundlingProfile.trim() == "Initial Estimate and Photos (Adjuster)" || strBundlingProfile == "Acknowledgement of Claim" || strBundlingProfile == "No Inspection (Stale) Alert & Close" || strBundlingProfile == "Assignment Cancellation")) {
                        selSendVia.AddItem("WS", "Web Services");
                        for (var i = 0; i < selSendVia.Options.length; i++){
                           if (selSendVia.Options[i].value == "WS"){
                              wsIndex = i;
                              break;
                           }
                        }
			//oBundlingProfile.originalSendVia = "EML";
                        oBundlingProfile.sendVia = "WS";
                        oBundlingProfile.sendValue = "";
			}
else if ((bElectronicBundle && strInsCoId == "588") && (strBundlingProfile.Trim() == "Closing Repair Complete" || strBundlingProfile.Trim() == "Acknowledgement of Claim" || strBundlingProfile.Trim() == "Closing Supplement" || strBundlingProfile.Trim() == "Initial Estimate and Photos (Adjuster)" || strBundlingProfile.Trim() == "Assignment Cancellation" || strBundlingProfile.Trim() == "Rental Invoice" || strBundlingProfile.Trim() == "Closing - Total Loss"|| strBundlingProfile.Trim() == "Cash Out Alert & Close" || strBundlingProfile == "No Inspection (Stale) Alert & Close" || strBundlingProfile.Trim() == "Status Update - Claim Rep" || strBundlingProfile.Trim() == "Status Update - MDS"|| strBundlingProfile.Trim() == "Total Loss Alert - (Adjuster)")) {
   //alert("Test sccess..");
                        selSendVia.AddItem("WS", "Web Services");
                        for (var i = 0; i < selSendVia.Options.length; i++){
                           if (selSendVia.Options[i].value == "WS"){
                              wsIndex = i;
                              break;
                           }
                        }
			//oBundlingProfile.originalSendVia = "EML";
                        oBundlingProfile.sendVia = "WS";
                        oBundlingProfile.sendValue = "";
			}
		else {
                        txtEmail.value = oBundlingProfile.sendValue;
                     }
                     break;
                  case "FAX":
                     txtFax.value = oBundlingProfile.sendValue;
                     txtFaxRecipient.value = oBundlingProfile.sendFaxName;
                     break;
                  case "FTP":
                     blnProfileOverride = true;
                     setFTPVariables(oBundlingProfile.sendValue);
                     break;
               }
			   //alert(oBundlingProfile.sendVia);
               selSendVia.value = oBundlingProfile.sendVia + "";
               blnSystemOveridding = true;
               selPreference.value = (selPreference.value == "DEF" ? "OVR" : "DEF");
               blnSystemOveridding = false;
               
               /*var oldDisabledValue = selSaveAs.CCDisabled;
               selSaveAs.CCDisabled = false;*/
			   selSaveAs.value = oBundlingProfile.saveAs;
               //window.setTimeout("selSaveAs.CCDisabled = " + oldDisabledValue, 100);
               selPreference.value = oBundlingProfile.sendPreference;
              
               blnSettingProfileValues = false;
          }
          
          function enableDocuments(blnEnable){
            var strClaimAspectID = selClaimAspect.value;
            var objDocs = xmlDocuments.selectNodes("/Root/Document[@ClaimAspectID='" + strClaimAspectID + "']");
            var objChk = null;
            if (objDocs){
               for (var i = 0; i < objDocs.length; i++){
                  DocumentID = objDocs[i].getAttribute("DocumentID");
                  objChk = document.getElementById("chk" + DocumentID);
                  if (objChk){
                     objChk.CCDisabled = !(blnEnable);
                  }
               }
            }
          }
          
          function toggleMCE(id){
            if (!tinyMCE.get(id))
               tinyMCE.execCommand('mceAddControl', false, id);
            else
               tinyMCE.execCommand('mceRemoveControl', false, id);
          }
          
          function setMessageTemplate(strHTML){
            /*//reset the control to basic
            toggleMCE("txtMessage");
            txtMessage.innerText = strHTML;
            //set the control to enhanced
            toggleMCE("txtMessage");*/
            tinyMCE.get("txtMessage").setContent(strHTML, {format:'raw'});
          }

         function getClaimInfo(){
            var sProc = strClaimMessagesSP;
            var sRequest = "LynxID=" + strLynxID + 
                           "&UserID=" + strUserID;
   
            var sMethod = "ExecuteSpNpAsXML";
            
            var aRequests = new Array();
            aRequests.push( { procName : sProc,
                              method   : sMethod,
                              data     : sRequest }
                          );
            var sXMLRequest = makeXMLSaveString(aRequests);
            //alert(sXMLRequest); return;
            var objRet = XMLSave(sXMLRequest);
          
            //alert(objRet.code);
            if (objRet && objRet.code == 0){
               blnDataReady = true;
               var strProcResultXML = objRet.xml.selectSingleNode("//Result/Root").xml
               xmlClaimInfo.async = false;
               xmlClaimInfo.loadXML(strProcResultXML);
               blnClaimInfoReady = true;
               selBundling.CCDisabled = false;
               //window.clipboardData.setData("Text", strProcResultXML);
               //alert(strProcResultXML);
            }
         }
         
         function claimInfoReady(){
            var obj = xmlClaimInfo.selectSingleNode("/Root");
            if (obj){
               if (selClaimAspect.value != null)
                  obj.setAttribute("ClaimAspectID", selClaimAspect.value);
               var objVehicle = xmlClaimInfo.selectSingleNode("/Root/Claim/Vehicle");
               if (objVehicle){
                  strAnalystName = objVehicle.getAttribute("ClaimAnalystName");
                  strAnalystPhone = objVehicle.getAttribute("ClaimAnalystPhone");
                  strAnalystPhone = "(" + strAnalystPhone.substr(0, 3) + ") " + strAnalystPhone.substr(3, 3) + "-" + strAnalystPhone.substr(6, 4)
                  strEmailBody = strEmailBody.replace(/\$AnalystName\$/g, strAnalystName);
                  strEmailBody = strEmailBody.replace(/\$AnalystPhone\$/g, strAnalystPhone);
                  strPursuitAuditShopFax = objVehicle.getAttribute("ShopFax");
                  strPursuitAuditShopEmail = objVehicle.getAttribute("ShopEmail");
                  strPursuitAuditShopName = objVehicle.getAttribute("ShopName");
                  strPursuitAuditContactName = objVehicle.getAttribute("ShopContact");
               }
            }
         }

		//---------------------------------------------------------//
		//-- 11Dec2012 - TVD - Warning Message                   --//
		//-- Notify client that they must change values manually --//
		//---------------------------------------------------------//
        function showOverrideMessage(){
			var strWarning = 'WARNING: You have selected to use a template that has not been configured for this client. Before you send this bundle it is your responsibility to make sure all of the following settings are correctly made, and all data fields are populated with the correct info to insure the transmittal is in compliance with the client approved process.'
			strWarning = strWarning + '\n\nSend Preference – Need to change from “Client Preferred” to “Override” if the default settings/data shown need to be changed.' 
			strWarning = strWarning + '\n\nSend via – Select either “Email” or “Fax” and edit related fields. FTP is unavailable for non-configured bundles.' 
			strWarning = strWarning + '\n\tEmail Address'
			strWarning = strWarning + '\n\tor'
			strWarning = strWarning + '\n\tFax Number and Fax Recipient'
			strWarning = strWarning + '\n\nOutput format – Select either “TIF” or “PDF”'
			strWarning = strWarning + '\n\nSave as – Select the document type the bundle will be “saved as” in the claim file.'
			strWarning = strWarning + '\n\nMessage – This may default to generic text or a client specific text depending which template you select. Edit as needed to meet this bundle’s requirements.'

			sRet = YesNoMessage("Confirm Override ", strWarning + "\n\nYou are about to override the settings. Do you want to continue?");
			if (sRet != "Yes")
				return;
		}
         
		//---------------------------------------------------------//
		//-- 11Dec2012 - TVD - Warning Message                   --//
		//-- Notify client that they must change values manually --//
		//---------------------------------------------------------//
        function templateClicked(){
			bTemplateClicked = true;
		}
    
       function WestfieldEmailTeamplate(strMessageTemplateID){
            var strClaimAspectID = selClaimAspect.value;
            var objClaimAspect = xmlClaimAspect.selectSingleNode("/Root/ClaimAspect[@ClaimAspectID='" + strClaimAspectID + "']");
                strIsWebservice = objClaimAspect.getAttribute("IsWebservice");
            var strselMsgTemplate = selMsgTemplate.text;
            var WARNINGMSG = "WARNING: Using the Template option vs the Bundling Profile option will cause documents to NOT be sent electronically to the client.  Please use the Bundling Profile options to avoid this issue."; 
              
            if ((strInsCoId == "387" && strIsWebservice == "1") && (strselMsgTemplate == "Cash Out Alert & Close" || strselMsgTemplate == "Closing Repair Complete" || strselMsgTemplate == "Closing Supplement" || strselMsgTemplate == "Closing - Total Loss" || strselMsgTemplate == "Rental Invoice" || strselMsgTemplate == "Initial Estimate" || strselMsgTemplate == "Acknowledgement of Claim" || strselMsgTemplate == "No Inspection (Stale) Alert & Close")){
                if(strselMsgTemplate!=undefined && strselMsgTemplate!="" &&(strMessageTemplateID=="" || strMessageTemplateID==undefined))
                    alert(WARNINGMSG);
             }
             else if ((strInsCoId == "194" && strIsWebservice == "1") && (strselMsgTemplate == "Cash Out Alert & Close" || strselMsgTemplate == "Closing Repair Complete" || strselMsgTemplate == "Closing Supplement" || strselMsgTemplate == "Closing - Total Loss" || strselMsgTemplate == "Rental Invoice" || strselMsgTemplate.trim() == "Initial Estimate and Photos (Adjuster)" || strselMsgTemplate == "Acknowledgement of Claim" || strselMsgTemplate == "No Inspection (Stale) Alert & Close" || strselMsgTemplate == "Assignment Cancellation")){
                if(strselMsgTemplate!=undefined && strselMsgTemplate!="" &&(strMessageTemplateID=="" || strMessageTemplateID==undefined))
                    alert(WARNINGMSG);
                    btnSend.CCDisabled = true;
             }
             else if ((bElectronicBundle && strInsCoId == "588") && (strselMsgTemplate.Trim() == "Closing Repair Complete" || strselMsgTemplate.Trim() == "Acknowledgement of Claim" || strselMsgTemplate.Trim() == "Closing Supplement" || strselMsgTemplate.Trim() == "Initial Estimate and Photos (Adjuster)" || strselMsgTemplate.Trim() == "Assignment Cancellation" || strselMsgTemplate.Trim() == "Rental Invoice" || strselMsgTemplate.Trim() == "Closing - Total Loss"|| strselMsgTemplate.Trim() == "Cash Out Alert & Close"|| strselMsgTemplate.Trim() == "Status Update - Claim Rep" || strselMsgTemplate.Trim() == "Status Update - MDS"|| strselMsgTemplate.Trim() == "Total Loss Alert - (Adjuster)")){
                if(strselMsgTemplate!=undefined && strselMsgTemplate!="" &&(strMessageTemplateID=="" || strMessageTemplateID==undefined))
                    alert(WARNINGMSG);
             }
       }
        
         function showMessage(strMessageTemplateID){
            var blnDisabled = false;
            if (blnMessageChanging == true) return;
                   
            WestfieldEmailTeamplate(strMessageTemplateID);
              
            blnDisabled = selMsgTemplate.CCDisabled;

            if (strMessageTemplateID != "" && parseInt(strMessageTemplateID, 10) > 0){
               blnMessageChanging = true;
               selMsgTemplate.CCDisabled = false;
               selMsgTemplate.value = strMessageTemplateID;
               selMsgTemplate.CCDisabled = (selBundling.selectedIndex != -1);
               blnMessageChanging = false;
			      }
            strMessageTemplateID = selMsgTemplate.value;
            
            var objReference = xmlReference.selectSingleNode("/Root/Reference[@List='Messages' and @ReferenceID = '" + strMessageTemplateID + "']");
            if (objReference){
               var strMessageDescription = objReference.getAttribute("MessageDescription");
               var strMessageTemplatePath = strMessageDescription.split("|")[1];
               setMessageTemplate("<div></div>");
               //alert(blnClaimInfoReady);
               xmlMessageTemplate.src = strMessageTemplatePath;
            }
			
			//---------------------------------------------------------//
			//-- 11Dec2012 - TVD - Warning Message                   --//
			//-- Notify client that they must change values manually --//
			//---------------------------------------------------------//
			if (bTemplateClicked==true || selPreference.value == "OVR"){
				showOverrideMessage();
				bTemplateClicked = false;
			}
		}
         
         function messageTemplateReady(){
            if ((blnClaimInfoReady == false) || (xmlMessageTemplate.readyState != "complete")){
               window.setTimeout("messageTemplateReady()", 100);
               return;
            }
            
            if (selMsgTemplate.selectedIndex != -1){
               //alert(xmlMessageTemplate.parseError.reason);return;
               var strResult = xmlClaimInfo.transformNode(xmlMessageTemplate);
               //alert(strResult);
               if (strResult != ""){
                  strResult = "<div style='font-weight:normal;line-height:normal;font-style:normal;font-family:Arial;font-variant:normal;'><span style='font-size:x-small;'><span style='font-family:arial,helvetica,sans-serif'>" + strResult + "</span></span></div>"
                  setMessageTemplate(strResult);
                  //tinyMCE.get("txtMessage").setContent("<div style='font-weight:normal;line-height:normal;font-style:normal;font-family:Arial;font-variant:normal;'><span style='font-size:x-small;'><span style='font-family:arial,helvetica,sans-serif'>" + strResult + "</span></span></div>", {format:'raw'});
                  //divMessageTemplateHolder.innerHTML = strResult;
                  updateDocumentList();
                  blnMessageTemplateReady = true;
               }
            }
         }
         
         function getSelectedDocuments(){
            var strDocumentList = "";
            var intDocCount = 0;
            var strDocName = "";
            for (var i = 0; i < aDocumentNames.length; i++){
               strDocName = aDocumentNames[i].split("|")[1];
               intDocCount = parseInt(aDocumentNames[i].split("|")[0], 10);
               
               if (intDocCount > 0){
                  strDocumentList += "<li>" + (intDocCount > 1 ? intDocCount : "") + " " + strDocName + (intDocCount > 1  && (strDocName.substr(strDocName.length - 1, 1) != "s") ? "s" : "") + "</li>";
               }
            }
            
            if (strDocumentList == "") strDocumentList = "<li><font style='color:#FF0000'>None Selected</font></li>";
            return strDocumentList;
         }
         
         function updateDocumentList(){
            /*strMessageHTML = tinyMCE.get("txtMessage").getContent({format: 'raw'});
            divMessageTemplateHolder.innerHTML = strMessageHTML;
            if (document.getElementById("documentList")){
               documentList.innerHTML = getSelectedDocuments();
            }
            setMessageTemplate(divMessageTemplateHolder.innerHTML);*/
            updateMessageTemplate("documentList", getSelectedDocuments());
         }
         
         function getBasicMessage(){
            var strMessageHTML = tinyMCE.get("txtMessage").getContent({format: 'raw'});
            divMessageTemplateNonHTML.innerHTML = strMessageHTML;
            return divMessageTemplateNonHTML.innerText.replace(/(\n\r){2}/g, "\n");
         }
         
         function clearDocumentChecklist(){
            var rowCount = tblMissingDocHolder.rows.length;
            for (i = (rowCount - 1); i >= 0; i--){
               tblMissingDocHolder.deleteRow(i);
            }
         }
         
         function addDocument2Checklist(strDocName, blnMissing){
            var newRow = tblMissingDocHolder.insertRow();
            var newCell = newRow.insertCell();
            newCell.innerHTML = "<img src='/images/" + (blnMissing == true ? "content_noaction_red.gif" : "tick_circle_green.gif") + "'/>";
            
            newCell = newRow.insertCell();
            newCell.innerHTML = strDocName;
         }
         
         function updateMessageTemplate(strObjectID, strObjectText){
            if (strObjectID != ""){
               strMessageHTML = tinyMCE.get("txtMessage").getContent({format: 'raw'});
               divMessageTemplateHolder.innerHTML = strMessageHTML;
               if (document.getElementById(strObjectID)){
                  document.getElementById(strObjectID).innerHTML = strObjectText;
                  setMessageTemplate(divMessageTemplateHolder.innerHTML);
               }
            }
         }
         
         function updateFaxNumberInMessage(){
            try {
               var strFaxNumber = formatPhone(txtFax.value);
               updateMessageTemplate("_msgFaxNumber", strFaxNumber);
            } catch (e) {
            }
         }
         
         function updateEmailInMessage(){
           try {
             updateMessageTemplate("_msgFaxNumber", txtEmail.value);
             updateMessageTemplate("_msgFaxContact", strPursuitAuditContactName + ",");
           } catch (e) {}
         }
         
         function updateFaxRecipientInMessage(){
            try {
            if (blnSettingProfileValues == true || blnMessageTemplateReady == false) return;
            var strName = "" ;
            var strContact = txtFaxRecipient.value;
            if (typeof(strContact) == "string" && strContact.indexOf(";") != -1){
               strName = strContact.split(";")[0];
               
               strContact = strContact.split(";")[1];
            }
            if (strInsCoId == 304)
              strContact = strPursuitAuditContactName + ",";
            if (strContact != "") updateMessageTemplate("_msgFaxContact", strContact);
            if (strName != "") updateMessageTemplate("_msgFaxName", strName);
            } catch (e) {alert(e.description)}
         }
         
         function PreferredDesinationReady(){
           blnPreferredDestinationLoaded = true;
         }

        ]]>
        </script>

      </head>
      <body unselectable="on" scroll1="no" leftmargin="5" topmargin="5" bottommargin="5" rightmargin="5">
        <IE:APDStatus class="APDStatus" id="stat1" name="stat1" width="240" height="75"/>
        <xml id="xmlDocuments" name="xmlDocuments">
          <Root>
            <xsl:for-each select="/Root/Document">
              <xsl:sort select="@DocumentID" data-type="number" order="descending"/>
              <xsl:copy-of select="."/>
            </xsl:for-each>
          </Root>
        </xml>
        <xml id="xmlExceptions" name="xmlExceptions">
          <Root>
            <xsl:copy-of select="/Root/Reference[@List='ReturnDocExceptions']"/>
          </Root>
        </xml>
        <xml id="xmlBundling" name="xmlBundling">
          <Root>
            <xsl:for-each select="/Root/Bundling">
              <Bundling>
                <xsl:for-each select="@*">
                  <xsl:copy-of select="."/>
                </xsl:for-each>
                <xsl:for-each select="BundlingDocument">
                  <xsl:sort select="@SelectionOrder" data-type="number" order="ascending"/>
                  <xsl:copy-of select="."/>
                </xsl:for-each>
              </Bundling>
            </xsl:for-each>
          </Root>
        </xml>
        <xml id="xmlClaimAspect" name="xmlClaimAspect">
          <Root>
            <xsl:copy-of select="/Root/ClaimAspect"/>
          </Root>
        </xml>
        <xml id="xmlReference" name="xmlReference">
          <Root>
            <xsl:copy-of select="/Root/Reference"/>
          </Root>
        </xml>
        <xml id="xmlData" name="xmlData">
          <Root>
            <xsl:copy-of select="/Root"/>
          </Root>
        </xml>
        <xml id="xmlClaimInfo" name="xmlClaimInfo" ondatasetcomplete="claimInfoReady()">
          <Root/>
        </xml>
        <xml id="xmlMessageTemplate" name="xmlMessageTemplate" ondatasetcomplete="messageTemplateReady()">
        </xml>
        <xml id="xmlPreferredDestination" name="xmlPreferredDestination" ondatasetcomplete="PreferredDesinationReady()">
        </xml>
        <xml id="xmlPreferredProfileDestination" name="xmlPreferredProfileDestination">
        </xml>

        <div id="divMaskAll" name="divMaskAll" style="display:none;position:absolute;top:3px;left:3px;height:100%;width:100%;background-color:#000000;FILTER: progid:DXImageTransform.Microsoft.Alpha( style=0,opacity=25);z-index:9998;">
        </div>
        <div id="divMissingDocs" style="position:absolute;background:#FFFFFF;border:2px solid #556B2F;display:none;z-index:9999;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#C0C0C0,strength=3);padding:2px;">
          <table border="0" cellpadding="2" cellspacing="0" style="margin:10px;width:400px;height:200px;border-collapse:collapse">
            <tr valign="top" style="height:30px;">
              <td>
                <div style="font:12pt Arial; font-weight:bold">Document Checklist:</div>
              </td>
              <td style="text-align:right">
                <img src="/images/close.gif" onclick="divMissingDocs.style.display = 'none'; divMaskAll.style.display = 'none'"/>
              </td>
            </tr>
            <tr style="height:30px;">
              <td colspan="2">The following documents comprise this bundle:</td>
            </tr>
            <tr style="height:135px;">
              <td colspan="2">
                <div id="divMissingDocHolder" style="height:100%;width:100%;overflow:auto;">
                  <table id="tblMissingDocHolder" border="0" cellpadding="2" cellspacing="0">
                    <colgroup>
                      <col width="30px"/>
                      <col width="300px"/>
                    </colgroup>
                    <tr>
                      <td/>
                      <td/>
                    </tr>
                  </table>
                </div>
              </td>
            </tr>
            <tr valign="top">
              <td colspan="2">
                <b>Action:</b>
                <br/>
                <br/>
                <div id="divMissingDocAction"/>
              </td>
            </tr>
          </table>
        </div>
        <table border="0" cellpadding="2" cellspacing="0" style="width:100%;table-layout:fixed;border-collapse:collapse">
          <colgroup>
            <col width="500px"/>
            <col width="*"/>
          </colgroup>
          <tr>
            <td style="font-size:18px;font-weight:bold">Send Documents to Client/Repair Facility</td>
            <td style="text-align:right;font-weight:bold">
              LYNX ID: <xsl:value-of select="$LynxId"/>-<xsl:value-of select="$VehNum"/>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <img src="images/background_top.gif" style="height:2px"/>
            </td>
          </tr>
        </table>
        <table id="tblPage" border="0" cellpadding="2" cellspacing="0" style="width:100%;table-layout:fixed;border-collapse:collapse">
          <colgroup>
            <col width="90px"/>
            <col width="180px"/>
            <col width="50px"/>
            <col width="*"/>
          </colgroup>
          <tr>
            <td colspan="4">
              <div>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              </div>
            </td>
          </tr>
          <tr valign="top">
            <td>Vehicle:</td>
            <td>
              <IE:APDCustomSelect class="APDCustomSelect" id="selClaimAspect" name="selClaimAspect" displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="1" required="true" width="165" onChange="showDocuments()">
                <xsl:for-each select="/Root/ClaimAspect">
                  <xsl:sort select="@ClaimAspectNumber" data-type="number" order="ascending"/>
                  <IE:dropDownItem style="display:none">
                    <xsl:attribute name="value">
                      <xsl:value-of select="@ClaimAspectID"/>
                    </xsl:attribute>
                    <xsl:choose>
                      <xsl:when test="@OwnerBusinessName != ''">
                        <xsl:value-of select="concat(@ClaimAspectNumber, ': ', @OwnerBusinessName)"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="concat(@ClaimAspectNumber, ': ', @OwnerNameFirst, ' ', @OwnerNameLast)"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </td>
            <td colspan="2">
              <table border="0" cellpadding="0" cellspacing="0" style="table-layout:fixed;border-collapse:collapse">
                <colgroup>
                  <col width="50px"/>
                  <col width="174px"/>
                  <col width="55px"/>
                  <col width="*"/>
                </colgroup>
                <tr>
                  <td>Message:</td>
                  <td>
                    <!-- <button style="margin:0px;padding:0px;border:0px;background-color:transparent" onclick="showExpandedMessage()">
                     <img src="images/showmore.gif" title="Show expanded message"/>
                     </button> -->
                  </td>
                  <td>Template:</td>
                  <td>
                    <IE:APDCustomSelect class="APDCustomSelect" id="selMsgTemplate" name="selMsgTemplate" displayCount="5" sorted="true" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="1" required="true" width="225" onChange="showMessage()" onClick="templateClicked()">
                      <IE:dropDownItem style="display:none" value=""/>
                    </IE:APDCustomSelect>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top">
            <td>Bundling Profile:</td>
            <td>
              <IE:APDCustomSelect class="APDCustomSelect" id="selBundling" name="selBundling" displayCount="5" sorted="true" blankFirst="false" canDirty="false" CCDisabled="true" CCTabIndex="1" required="true" width="165" dropDownWidth="225" onChange="selectBundleDocuments()">
                <IE:dropDownItem style="display:none" value=""/>
                <!--<xsl:for-each select="/Root/Bundling">
                  <xsl:sort select="@ClaimAspectNumber" data-type="text" order="ascending"/>
                  <IE:dropDownItem style="display:none">
                     <xsl:attribute name="value"><xsl:value-of select="@BundlingID"/></xsl:attribute>
                     <xsl:value-of select="@Name"/>
                  </IE:dropDownItem>
               </xsl:for-each>-->
              </IE:APDCustomSelect>
            </td>
            <td colspan="2" rowspan="7">
              <!--<IE:APDTextArea class="APDTextArea" id="txtMessage" name="txtMessage" maxLength="1000" width="500" height="153" CCDisabled="false" CCTabIndex="2" required="false" onChange=""/>-->
              <!--<div id="divMessage" style="position:absolute;top:100px;left:100px;z-index:9999">-->
              <textarea id="txtMessage" name="txtMessage" class="mceSimple" maxLength="1000" style="font:10pt Arial;">
              </textarea>
              <!--</div>-->
            </td>
          </tr>
          <tr valign="top">
            <td>Send Preference:</td>
            <td>
              <IE:APDCustomSelect class="APDCustomSelect" id="selPreference" name="selPreference" displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="1" required="true" width="165" onChange="showOverride()">
                <IE:dropDownItem value="DEF" style="display:none">Client Preferred</IE:dropDownItem>
                <IE:dropDownItem value="OVR" style="display:none">Override</IE:dropDownItem>
              </IE:APDCustomSelect>
            </td>
          </tr>
          <tr valign="top">
            <td>Send via:</td>
            <td>
              <IE:APDCustomSelect class="APDCustomSelect" id="selSendVia" name="selSendVia" displayCount="5" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="1" required="true" width="165" onChange="showRouteValue()">
                <IE:dropDownItem value="EML" style="display:none">Email</IE:dropDownItem>
                <IE:dropDownItem value="FAX" style="display:none">Fax</IE:dropDownItem>
                <IE:dropDownItem value="FTP" style="display:none">FTP</IE:dropDownItem>
              </IE:APDCustomSelect>
            </td>
          </tr>
          <tr valign="top">
            <td>
              <div id="divSendVia" name="divSendVia"/>
            </td>
            <td style="height:24px">
              <div id="divEmail" name="divEmail" style="display:none">
                <IE:APDInput class="APDInput" id="txtEmail" name="txtEmail" size="100" maxLength="400" width="159"  required="true" canDirty="false" CCDisabled="false" CCTabIndex="2" onChange="if (txtEmail.value != strAdjusterEmail) updateEmailInMessage()"/>
              </div>
              <div id="divFax" name="divFax" style="display:none">
                <IE:APDInputPhone class="APDInputPhone" id="txtFax" name="txtFax" required="true" canDirty="false" showExtension="false" includeMask="false" CCDisabled="false" CCTabIndex="2" onChange="if (txtFax.value != strAdjusterFax) txtFaxRecipient.value = ''; updateFaxNumberInMessage()"/>
              </div>
              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            </td>
          </tr>
          <tr id="trFaxRecipient" valign="top" style="visibility:hidden">
            <td>Fax Recipient:</td>
            <td style="height:24px">
              <IE:APDInput class="APDInput" id="txtFaxRecipient" name="txtFaxRecipient" size="100" maxLength="400" width="159"  required="true" canDirty="false" CCDisabled="false" CCTabIndex="3" onchange="updateFaxRecipientInMessage()"/>
            </td>
          </tr>
          <tr id="tdOutputFormat" valign="top" style="visibility:hidden">
            <td>Output format:</td>
            <td style="height:24px">
              <IE:APDRadioGroup class="APDRadioGroup" id="rbGrpOption" name="rbGrpOption" required="false" canDirty="false" CCDisabled="false" onChange="setOutputFormat()">
                <IE:APDRadio class="APDRadio" id="rbOptionTif" name="rbOptionTif" caption="TIF" value="" groupName="rbGrpOption" checkedValue="1" uncheckedValue="0" required="false" CCDisabled="false" CCTabIndex="4" onChange1="" style="visibility:hidden"/>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                <IE:APDRadio class="APDRadio" id="rbOptionPdf" name="rbOptionPdf" caption="PDF" value="2" groupName="rbGrpOption" checkedValue="2" uncheckedValue="0" required="false" CCDisabled="false" CCTabIndex="5" onChange1="" style="visibility:hidden"/>
              </IE:APDRadioGroup>
            </td>
          </tr>
          <!-- <tr>
          <td colspan="4">
            <div><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</div>
          </td>
        </tr> -->
          <tr>
            <td>Save as:</td>
            <td>
              <IE:APDCustomSelect class="APDCustomSelect" id="selSaveAs" name="selSaveAs" displayCount="5" sorted="true" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="1" required="true" width="165" onChange="showRouteValue()">
                <xsl:for-each select="/Root/Reference[@List='DocumentType' and @EnabledFlag = '1' and @ReferenceID != '2' and @ReferenceID != '3' and @ReferenceID != '4' and @ReferenceID != '10']">
                  <xsl:sort select="@Name" data-type="text" order="ascending"/>
                  <IE:dropDownItem style="display:none">
                    <xsl:attribute name="value">
                      <xsl:value-of select="@ReferenceID"/>
                    </xsl:attribute>
                    <xsl:value-of select="@Name"/>
                  </IE:dropDownItem>
                </xsl:for-each>
              </IE:APDCustomSelect>
            </td>
          </tr>
          <tr>
            <td colspan="4">
              <div style="height:266px;width:100%;border:0px solid #FF0000;border-bottom:1px solid #C0C0C0">
                <table border="0" cellpadding="2" cellspacing="0" style="table-layout:fixed;">
                  <colgroup>
                    <col width="225px"/>
                    <col width="8px"/>
                    <col width="60px"/>
                    <col width="8px"/>
                    <col width="67px"/>
                    <col width="8px"/>
                    <col width="70px"/>
                    <col width="8px"/>
                    <col width="67px"/>
                    <col width="8px"/>
                    <col width="67px"/>
                    <col width="8px"/>
                    <col width="42px"/>
                    <col width="8px"/>
                    <col width="45px"/>
                    <col width="8px"/>
                    <col width="67px"/>
                  </colgroup>
                  <tr>
                    <td class="Header">Document Name</td>
                    <td class="Header">
                      <img src="images/resizer.gif"/>
                    </td>
                    <td class="Header">Selection Order</td>
                    <td class="Header">
                      <img src="images/resizer.gif"/>
                    </td>
                    <td class="Header">Source</td>
                    <td class="Header">
                      <img src="images/resizer.gif"/>
                    </td>
                    <td class="Header">Pertains To</td>
                    <td class="Header">
                      <img src="images/resizer.gif"/>
                    </td>
                    <td class="Header">Rcvd. Dt.</td>
                    <td class="Header">
                      <img src="images/resizer.gif"/>
                    </td>
                    <td class="Header">Final Est.</td>
                    <td class="Header">
                      <img src="images/resizer.gif"/>
                    </td>
                    <td class="Header">DTP</td>
                    <td class="Header">
                      <img src="images/resizer.gif"/>
                    </td>
                    <td class="Header">View</td>
                    <td class="Header">
                      <img src="images/resizer.gif"/>
                    </td>
                    <td class="Header">Sent to Carrier</td>
                  </tr>
                </table>
                <div style="height:242px;width:100%;overflow:auto">
                  <table border="1" cellpadding="2" cellspacing="0" style="table-layout:fixed;border-collapse:collapse;">
                    <colgroup>
                      <col width="229px" style="padding:0px;padding-left:5px;"/>
                      <col width="68px" style="text-align:center"/>
                      <col width="75px" style="padding:0px;padding-left:5px;text-align:center"/>
                      <col width="78px" style="padding:0px;padding-left:5px;"/>
                      <col width="75px" style="text-align:center"/>
                      <col width="75px" style="text-align:center"/>
                      <col width="50px" style="text-align:center"/>
                      <col width="53px" style="text-align:center"/>
                      <col width="68px" style="text-align:center"/>
                      <col style="display:none"/>
                      <col style="display:none"/>
                      <col style="display:none"/>
                    </colgroup>
                    `
                    <xsl:for-each select="Document[@DocumentID &gt; 0]">
                      <xsl:sort select="@DocumentID" data-type="number" order="descending"/>

                      <xsl:variable name="DocumentTypeID">
                        <xsl:value-of select="@DocumentTypeID"/>
                      </xsl:variable>
                      <xsl:variable name="SuppNum">
                        <xsl:if test="/Root/Reference[@List='DocumentType'and @ReferenceID=$DocumentTypeID]/@Name = 'Supplement'">
                          <xsl:value-of select="concat(' - ', @SupplementSeqNumber)"/>
                        </xsl:if>
                      </xsl:variable>


                      <xsl:variable name="EstimateType">
                        <xsl:if test="/Root/@InsuranceCompanyID='304' and (@DocumentTypeID = '3' or @DocumentTypeID = '10') and @FinalBillEstimateFlag = '1' ">
                          <xsl:text>Approved </xsl:text>
                        </xsl:if>
                        <xsl:if test="/Root/@InsuranceCompanyID='304' and (@DocumentTypeID = '3' or @DocumentTypeID = '10') and @FinalBillEstimateFlag = '0' ">
                          <xsl:choose>
                            <xsl:when test="@EstimateTypeCD='A'">Approved </xsl:when>
                            <xsl:when test="@EstimateTypeCD='O'">Initial </xsl:when>
                          </xsl:choose>
                        </xsl:if>
                        <xsl:if test="/Root/@EarlyBillFlag='1' and (@DocumentTypeID = '3' or @DocumentTypeID = '10') and @ApprovedFlag = '1'">Approved </xsl:if>
                      </xsl:variable>


                      <xsl:variable name="DocumentName">
                        <xsl:value-of select="concat($EstimateType, /Root/Reference[@List='DocumentType'and @ReferenceID=$DocumentTypeID]/@Name, $SuppNum)"/>
                      </xsl:variable>
                      <tr>
                        <xsl:attribute name="style">height:30px;</xsl:attribute>
                        <xsl:attribute name="name">
                          <xsl:value-of select="concat('tr_', @ClaimAspectID)"/>
                        </xsl:attribute>
                        <td>
                          <IE:APDCheckBox class="APDCheckBox" width="" CCDisabled="false" required="false" CCTabIndex="" canDirty="false" onBeforeChange="checkPhoto(this)" onChange="selectDocument(this)" onAfterChange="">
                            <xsl:attribute name="id">
                              <xsl:value-of select="concat('chk', @DocumentID)"/>
                            </xsl:attribute>
                            <xsl:attribute name="name">
                              <xsl:value-of select="concat('chk', @DocumentID)"/>
                            </xsl:attribute>
                            <xsl:attribute name="caption">
                              <xsl:value-of select="$DocumentName"/>
                            </xsl:attribute>
                          </IE:APDCheckBox>
                        </td>
                        <td/>
                        <td>
                          <xsl:variable name="DocumentSourceID">
                            <xsl:value-of select="@DocumentSourceID"/>
                          </xsl:variable>
                          <xsl:variable name="DocumentSourceName">
                            <xsl:value-of select="/Root/Reference[@List='DocumentSource'and @ReferenceID=$DocumentSourceID]/@Name"/>
                          </xsl:variable>
                          <xsl:value-of select="$DocumentSourceName"/>
                        </td>
                        <td>
                          <xsl:variable name="ClaimAspectID">
                            <xsl:value-of select="@ClaimAspectID"/>
                          </xsl:variable>
                          <xsl:variable name="PertainsTo">
                            <xsl:value-of select="/Root/Reference[@List='PertainsTo'and @ReferenceID=$ClaimAspectID]/@Name"/>
                          </xsl:variable>
                          <xsl:value-of select="$PertainsTo"/>
                        </td>
                        <td>
                          <xsl:value-of select="js:formatSQLDateTime(string(@ReceivedDate))"/>
                        </td>
                        <td>
                          <xsl:choose>
                            <xsl:when test="@FinalEstimateFlag = '1' or @FinalBillEstimateFlag = '1'">Yes</xsl:when>
                            <xsl:otherwise>
                              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                            </xsl:otherwise>
                          </xsl:choose>
                        </td>
                        <td>
                          <xsl:choose>
                            <xsl:when test="@DirectionToPayFlag = '1'">Yes</xsl:when>
                            <xsl:otherwise>
                              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                            </xsl:otherwise>
                          </xsl:choose>
                        </td>
                        <td>
                          <img src="/images/imageview.gif" style="cursor:hand">
                            <xsl:attribute name="onclick">
                              showDoc("<xsl:value-of select="@ImageLocation"/>")
                            </xsl:attribute>
                          </img>
                        </td>
                        <td>
                          <xsl:value-of select="@SendToCarrierStatusCD"/>
                        </td>
                        <td>
                          <xsl:value-of select="user:resolveMacro(string($DocumentNameFormat), string($DocumentName), string($SuppNum), string(@ImageLocation), string($ClientClaimNumber), string($LossDate), string($LynxId), string($InsuredName), string($VehNum), string(/Root/@AdjusterName), string(/Root/@PolicyNumber), string(@DocumentID))"/>
                        </td>
                        <td>
                          <xsl:value-of select="/Root/Reference[@List='DocumentType'and @ReferenceID=$DocumentTypeID]/@Name"/>
                        </td>
                        <td>
                          <xsl:if test="/Root/Reference[@List='DocumentType'and @ReferenceID=$DocumentTypeID]/@Name = 'Supplement'">
                            <xsl:value-of select="@SupplementSeqNumber"/>
                          </xsl:if>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td colspan="4">
              <span>
                <IE:APDButton class="APDButton" id="btnSend" name="btnSend" value="Send" width="75" CCDisabled="false" CCTabIndex="5" onButtonClick="doSend()"/>
                <IE:APDButton class="APDButton" id="btnCancel" name="btnCancel" value="Cancel" width="75" CCDisabled="false" CCTabIndex="6" onButtonClick="doCancel()"/>
                <!-- <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <IE:APDCheckBox id="chkNotify" name="chkNotify" class="APDCheckBox" caption="Notify me on success" CCDisabled="false" required="false" CCTabIndex="3" canDirty="false" /> -->
              </span>
            </td>
          </tr>
        </table>
        <form id="formGen" name="formGen" method="POST" action="ClientDocuments.asp" target1="_self" target="ifrmGen">
          <input type="hidden" name="theAction" id="theAction"/>
          <input type="hidden" name="txtDocumentsXML" id="txtDocumentsXML"/>
        </form>
        <iframe name="ifrmGen" id="ifrmGen" style="height:100px;width:300px;visibility1:hidden">
        </iframe>
        <div id="divMessageTemplateHolder" style="display:none"/>
        <div id="divMessageTemplateNonHTML" style="display:none"/>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>