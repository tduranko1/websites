<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="Insurance">

  <xsl:import href="msxsl/msjs-client-library-htc.js"/>

  <msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
    function myNameConcat(firstPart, secondPart, delim){
        if (delim == "") delim = ", ";
        if (firstPart != "" && secondPart != "")
            return firstPart + delim + secondPart;
        else if (firstPart != "" || secondPart != "")
            return firstPart + secondPart + "&nbsp;";
        else if (firstPart == "" && secondPart == "")
            return "&nbsp;";

    }
  ]]>
  </msxsl:script>

  <xsl:output method="html" indent="yes" encoding="UTF-8" />

  <!-- Permissions params - names must end in 'CRUD' -->
  <xsl:param name="InfoCRUD" select="Client Configuration"/>

  <xsl:template match="/Root">

    <xsl:choose>
      <xsl:when test="contains($InfoCRUD, 'R')">
        <xsl:call-template name="mainPage">
          <xsl:with-param name="InfoCRUD">
            <xsl:value-of select="$InfoCRUD"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <html>
          <head>
            <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
            <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
          </head>
          <body class="bodyAPDSub" unselectable="on" bgcolor="#FFFFFF">
            <table cellspacing="0" cellpadding="0" border="0" style="height:100%;width:100%">
              <tr>
                <td align="center">
                  <font color="#ff0000">
                    <strong>
                      You do not have sufficient permission to view this page.
                      <br/>Please contact administrator for permissions.
                    </strong>
                  </font>
                </td>
              </tr>
            </table>
          </body>
        </html>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="mainPage">
    <xsl:param name="InfoCRUD"/>
    <HTML>


      <HEAD>
        <TITLE>Data Administration: Insurance Companies</TITLE>

        <LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>

        <STYLE type="text/css">
          A {color:black; text-decoration:none;}
        </STYLE>

        <SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/zipcodeutils.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/datepicker2.js"></SCRIPT>
        <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>
        <script type="text/javascript">
          document.onhelp=function(){
          RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
          event.returnValue=false;
          };
        </script>
        <SCRIPT language="Javascript">
          var bWorkmanship = (<xsl:value-of select="count(/Root/Reference[@ListName='WarrantyWorkmanship'])"/> &gt; 0 ? true : false);
          var bParts = (<xsl:value-of select="count(/Root/Reference[@ListName='WarrantyRefinish'])"/> &gt; 0 ? true : false);
          var iInsID = null;
          var sCRUD = "<xsl:value-of select="$InfoCRUD"/>";
          var curRow = null;
          var sBillingModel = "";
          var iGoToTabIdx = null;
          var bPageReady = false;
          var bAddNew = false;
          var gbDirty = false;
          var oClickedRow = null;
          var bFeeValid = true;
          var bRatesLoaded = false;

          <![CDATA[


	function pageInit(){
	
		initCRUD();
		tabInsurance.CCDisabled = true;
		setTimeout("if (!curRow) {tabDetail.disablePage(true);bPageReady = true;}", 500);
		
		if (InsStat) {
			InsStat.style.top = (document.body.offsetHeight - 75) / 2;
			InsStat.style.left = (document.body.offsetWidth - 240) / 2;
		}
	}
  
  
	function initList(){
	
		if (!tabDetail.isReady) {
		  setTimeout("initList()", 500);
		  return;
		}
		
		tblSort2.disabled = false;
	}


	function initCRUD(){
		  //sCRUD = CPScrollTable.getAttribute("CRUD");
		  if (sCRUD.indexOf("C") != -1){
			  btnAdd1.CCDisabled = false;
			  btnSave1.CCDisabled = false;
			  setTimeout("initList()", 250);
		  }
		  if (sCRUD.indexOf("U") != -1){
			  btnSave1.CCDisabled = false;
			  setTimeout("initList()", 250);
			  //tblSort2.disabled = false;
		  }
		  if (sCRUD.indexOf("R") != -1){
			  setTimeout("initList()", 250);
		  }
	}


	function GridClick(oRow){
		if (!bPageReady) return;
		if (tblSort2.disabled) return;
	
		if (tabInsurance.getAttribute("CCDisabled") == true)
		  tabInsurance.setAttribute("CCDisabled", false, 0);
	
		if (tabDetail.CCDisabled == true)
			tabDetail.disablePage(false);
	
		if (oRow == curRow) return;
		
		oClickedRow = oRow;
	
		if (curRow) {
		  tabInsurance.SelectTab(0);
		  if (bFeeValid == false) {
			tabInsurance.SelectTab(4);
			return;
		  }
	
		  if (tabInsurance.isDirty == true || gbDirty == true) {
		  
			// change the tab to the first so that if any fee info was changed, the Need to save will be triggered.
			var sSave = YesNoMessage("Need to Save", "Some Information on the current selected row has changed. \nDo you want to save the changes?.");
			
			if (sSave == "Yes"){
				if (btnSave() == false) return;
			}
			else if (sSave == "No") {
				if (curRow.rowAttr == "new"){
					tblSort2.deleteRow(curRow.rowIndex);
					tabDetail.resetTab();
					curRow = null;
					bAddNew = false;
				}
				else {
					//tabDetail.resetTab();
					curRow.rowAttr = "";
				}
				gbDirty = false;
			}
			else if (sSave == "")
			  return;
		  } else if (curRow.rowAttr == "new") {
			tblSort2.deleteRow(curRow.rowIndex);
			tabDetail.resetTab();
			curRow = null;
			bAddNew = false;
		  }
		  
		  if (curRow)
			curRow.style.backgroundColor = "#FFFFFF";
		}
	
		oRow.style.backgroundColor = "#FFD700";
		loadData(oRow);
		
		if (sCRUD.indexOf("U") == -1) 
		  tabDetail.disablePage(true);
	
		curRow = oRow;
		oClickedRow = null;
	}


	function loadData(oRow) {
		if (oRow) {
			if (tabDetail.selected == false)
				tabInsurance.SelectTab(0);
		
			tabDetail.CCDisabled = false;
		  
			setCanDirty(false);
			txtID.CCDisabled = false;
			iInsID = oRow.cells[5].innerText;
			txtID.value = iInsID;
	
	
			if (oRow.cells[1].getAttribute("EnabledFlag") == 1)
				EnabledFlag.value = 1;
			else
				EnabledFlag.value = 0;
			
			txtName.value = (oRow.cells[2].innerHTML != "&nbsp;" ? oRow.cells[2].innerText : "");
			txtInsID.value = oRow.cells[5].innerText;
			txtAddress1.value = oRow.cells[6].innerText;
			txtAddress2.value = oRow.cells[7].innerText;
			txtCity.value = oRow.cells[8].innerText;
			State.value = oRow.cells[9].innerText;
			txtZip.value = oRow.cells[10].innerText;
			AssignmentAtSelectionFlag.value = oRow.cells[11].innerText;
			InsuranceBusType.value = oRow.cells[12].innerText;
			txtPhone.value = oRow.cells[19].innerText + oRow.cells[20].innerText + oRow.cells[22].innerText + oRow.cells[21].innerText;
			txtFax.value = oRow.cells[13].innerText + oRow.cells[14].innerText + oRow.cells[16].innerText + oRow.cells[15].innerText;
			txtSSN.value = oRow.cells[17].innerText;
			LicenseDetCode.value = oRow.cells[18].innerText;
			txtTotalLossValuation.value = oRow.cells[23].innerText;
			txtTotalLossWarning.value = oRow.cells[24].innerText;
			txtLastUpdatedDate.value = oRow.cells[25].innerText;
			WorkmanshipWarranty.value = oRow.cells[26].innerText;
			PartsWarranty.value = oRow.cells[27].innerText;
			sBillingModel = oRow.cells[28].innerText;
			BillingModelCD.value = oRow.cells[28].innerText;
			ClientAccessFlag.value = oRow.cells[29].innerText;
			InvoicingModelPaymentCD.value = oRow.cells[30].innerText;
			txtIngresAccountingID.value = oRow.cells[32].innerText;

      // 10Jul2019 TVD - Add FeeCreditFlag
      chkAllowCredits.value = oRow.cells[31].innerText;
      
      EarlyBillFlag.value = oRow.cells[34].innerText;
	
			if (EarlyBillFlag.value == "1") {
				EarlyBillStartDate.setAttribute("CCDisabled", false, 0);
				EarlyBillStartDate.value = oRow.cells[35].innerText;
			} else {
				EarlyBillStartDate.setAttribute("CCDisabled", true, 0);
			}

			bClaimAspectLoaded = bPaymentTypeLoaded = bShopExclLoaded = bFeesLoaded = false;
		  
			//reset the other iframes so that when user clicks it, the insurance specific will be loaded.
			frmInsClaimAspectTab.frameElement.src = "/blank.asp";
			frmInsPaymentTypeTab.frameElement.src = "/blank.asp";
			frmInsShopExclTab.frameElement.src = "/blank.asp";
			frmInsFeeBillTab.frameElement.src = "/blank.asp";
			setCanDirty(true);
	
			if (oRow.rowAttr == "new")
				hideNewInsMisc(true);
			else
				hideNewInsMisc(false);
			  
			if (iInsID != "" && oRow.getAttribute("rowAttr") != "new") {
				txtID.CCDisabled = true;
				txtIngresAccountingID.CCDisabled = true;
			}
		}
	}


	function setCanDirty(bCanDirty) {

		txtID.canDirty = EnabledFlag.canDirty = ClientAccessFlag.canDirty = 
		txtName.canDirty = txtAddress1.canDirty =
		txtAddress2.canDirty = txtCity.canDirty = State.canDirty =
		txtZip.canDirty = AssignmentAtSelectionFlag.canDirty =
		InsuranceBusType.canDirty = txtPhone.canDirty =
		txtFax.canDirty = txtSSN.canDirty = LicenseDetCode.canDirty =
		txtTotalLossValuation.canDirty = txtTotalLossWarning.canDirty =
		WorkmanshipWarranty.canDirty = PartsWarranty.canDirty = 
		BillingModelCD.canDirty = InvoicingModelPaymentCD.canDirty = //InvoicingModelBillingCD.canDirty = 
		txtIngresAccountingID.canDirty = EarlyBillFlag.canDirty = bCanDirty = chkAllowCredits.canDirty = bCanDirty;
	
		if (bCanDirty){
			txtID.contentSaved();
			EnabledFlag.contentSaved();
			ClientAccessFlag.contentSaved();
			txtName.contentSaved();
			txtAddress1.contentSaved();
			txtAddress2.contentSaved();
			txtCity.contentSaved();
			State.contentSaved();
			txtZip.contentSaved();
			AssignmentAtSelectionFlag.contentSaved();
			InsuranceBusType.contentSaved();
			txtPhone.contentSaved();
			txtFax.contentSaved();
			txtSSN.contentSaved();
			LicenseDetCode.contentSaved();
			txtTotalLossValuation.contentSaved();
			txtTotalLossWarning.contentSaved();
			WorkmanshipWarranty.contentSaved();
			PartsWarranty.contentSaved();
			BillingModelCD.contentSaved();
			InvoicingModelPaymentCD.contentSaved();
			txtIngresAccountingID.contentSaved();
      EarlyBillFlag.contentSaved();
			EarlyBillStartDate.contentSaved();
      
      // 10Jul2019 TVD - Adding FeeCreditFlag
      chkAllowCredits.contentSaved();
		}
	}


	function refreshDetails(){
		if (iInsID != null) {
		  if (sCRUD.indexOf("U") == -1)
			tabDetail.disablePage(true);
		  else
			tabDetail.disablePage(false);
		}
	}
  
  
	function refreshClaimAspect(){
		if (sCRUD.indexOf("R") != -1)
		  tabDetail.disablePage(false);
		if (iInsID != null) {
		  if (!bClaimAspectLoaded) {
			frmInsClaimAspectTab.frameElement.src = "/admin/refInsClaimAspect.asp?InsuranceID=" + iInsID;
			bClaimAspectLoaded = true;
		  }
		} else {
		  if (bClaimAspectLoaded) {
			frmInsClaimAspectTab.frameElement.src = "/blank.asp";
			bClaimAspectLoaded = false;
		  }
		}
	}


	function refreshPaymentType(){
		if (sCRUD.indexOf("R") != -1)
		  tabDetail.disablePage(false);
		if (iInsID != null) {
		  if (!bPaymentTypeLoaded) {
			frmInsPaymentTypeTab.frameElement.src = "/admin/refInsPaymentType.asp?InsuranceID=" + iInsID;
			bPaymentTypeLoaded = true;
		  }
		} else {
		  if (bPaymentTypeLoaded) {
			frmInsPaymentTypeTab.frameElement.src = "/blank.asp";
			bPaymentTypeLoaded = false;
		  }
		}
	}
  
  
	function refreshShopExcl() {
		if (sCRUD.indexOf("R") != -1)
		  tabDetail.disablePage(false);
		if (iInsID != null) {
		  if (!bShopExclLoaded) {
			frmInsShopExclTab.frameElement.src = "/admin/refInsShopExcl.asp?InsuranceID=" + iInsID; //+ "&CEI=" + UseCEIShopsFlag.value;
			bShopExclLoaded = true;
		  }
		} else {
		  if (bShopExclLoaded) {
			frmInsShopExclTab.frameElement.src = "/blank.asp";
			bShopExclLoaded = false;
		  }
		}
	}


	function refreshFees() {
		if (sCRUD.indexOf("R") != -1)
		  tabDetail.disablePage(false);
		if (iInsID != null) {
		  if (!bFeesLoaded) {
			frmInsFeeBillTab.frameElement.src = "/admin/refInsFeeBilling.asp?InsuranceID=" + iInsID;
			bFeesLoaded = true;
		  }
		} else {
		  if (bFeesLoaded) {
			frmInsFeeBillTab.frameElement.src = "/blank.asp";
			bFeesLoaded = false;
		  }
		}
	}
  
  
	function refreshRates(){
		if (sCRUD.indexOf("R") != -1)
		  tabDetail.disablePage(false);
		if (iInsID != null) {
			frmInsLaborRatesTab.frameElement.src = "/admin/refInsLaborRates.htm";
		} else {
		  if (bRatesLoaded) {
			frmInsLaborRatesTab.frameElement.src = "/blank.asp";
			bRatesLoaded = false;
		  }
		}
	}

  
	function getInsID(){
		return iInsID;
	}


	function resetRowCursor(oTbl){
		  var iRows = oTbl.rows.length;
		  for (var i = 0; i < iRows; i++) {
			  oTbl.rows[i].cells[0].innerHTML = "&nbsp;";
			  oTbl.rows[i].style.backgroundColor = "#FFFFFF";
		  }
	}


	function btnAdd(){
		if (bAddNew) return;
		  var oNewRow = tblSort2.insertRow(-1);
		  var oNewCell = null;
		  for (i = 0; i < 34; i++){
			  oNewCell = oNewRow.insertCell();
			  if (i < 4)
				  oNewCell.innerHTML = "&nbsp;";
			  oNewCell.className="GridTypeTD";
			  if (i > 3){
				  oNewCell.style.display = "none";
			  }
		}
		
		oNewRow.unselectable="on";
		oNewRow.rowAttr = "new";
		oNewRow.onclick = new Function("", "GridClick(this)");
		oNewRow.style.height = "21px";
		oNewRow.style.cursor = "hand";
		oNewRow.cells[2].style.textAlign = "left";
		oNewRow.cells[3].style.textAlign = "left";
		
		GridClick(oNewRow);
		oNewRow.scrollIntoView(false);
		hideNewInsMisc(true);
		txtID.setFocus();
		bAddNew = true;
	}


	function hideNewInsMisc(bVal){
		  if (bVal){
			  tabClaimAspect.style.display = "none";
			  tabPaymentType.style.display = "none";
			  tabShopExcl.style.display = "none";
			  tabFees.style.display = "none";
			  tabWorkflow.style.display = "none";
		  }
		  else {
			  tabClaimAspect.style.display = "inline";
			  tabPaymentType.style.display = "inline";
			  tabShopExcl.style.display = "inline";
			  tabFees.style.display = "inline";
			  tabWorkflow.style.display = "inline";
		  }
	}


	function btnSave() {

		if (txtID.value == "") {
			ClientWarning("Insurance Company ID cannot be empty. Please try again.");
			return false;
		}
		  
		if (txtName.value == "") {
			ClientWarning("Insurance Company Name cannot be empty. Please try again.");
			return false;
		}
		  
		if (BillingModelCD.selectedIndex <= 0) {
			ClientWarning("Billing Model cannot be empty. Please select one from the options.");
			return false;
		}
			
		if (InvoicingModelPaymentCD.selectedIndex <= 0) {
			ClientWarning("Invoicing Model for Payment cannot be empty. Please select one from the options.");
			return false;
		}
		  
		  InsStat.Show("Saving changes ...");
		  window.setTimeout("btnSave2()", 150);
		  return false;
	}


	function btnSave2(){
  
		if (curRow == null) return;
    
		btnSave2.onErrorCallback = btnSaveCallback;
    
		var retArray = new Array;
		var bRefresh = false;

        var sProc, sRequest, i, iRowNo;
        var objRow, sRowAttrib;
        var sEnabled;

        setTimeout(ShowSB40,1);

        sProc = ""; sRequest = "";
        objRow = curRow;
        sRowAttrib = objRow.getAttribute("rowAttr");
        iRowNo = objRow.getAttribute("rowNo");
        sEnabled = EnabledFlag.value;

        sRequest =  "InsuranceCompanyID=" + txtID.value + "&" +
                    "Name=" + escape(txtName.value) + "&" +
                    "Address1=" + escape(txtAddress1.value) + "&" +
                    "Address2=" + escape(txtAddress2.value) + "&" +
                    "AddressCity=" + escape(txtCity.value) + "&" +
                    "AddressState=" + State.value + "&" +
                    "AddressZip=" + txtZip.value + "&" +
                    "AssignmentAtSelectionFlag=" + AssignmentAtSelectionFlag.value + "&" +
                    "BusinessTypeCD=" + InsuranceBusType.value + "&" +
                    "EnabledFlag=" + sEnabled + "&" +
                    "ClientAccessFlag=" + ClientAccessFlag.value + "&" +
                    "FaxAreaCode=" + txtFax.areaCode + "&" +
                    "FaxExchangeNumber=" + txtFax.phoneExchange + "&" +
                    "FaxExtensionNumber=" + txtFax.phoneExtension + "&" +
                    "FaxUnitNumber=" + txtFax.phoneNumber + "&" +
                    "FedTaxId=" + txtSSN.value + "&" +
                    "LicenseDeterminationCD=" + LicenseDetCode.value + "&" +
                    "PhoneAreaCode=" + txtPhone.areaCode + "&" +
                    "PhoneExchangeNumber=" + txtPhone.phoneExchange + "&" +
                    "PhoneExtensionNumber=" + txtPhone.phoneExtension + "&" +
                    "PhoneUnitNumber=" + txtPhone.phoneNumber + "&" +
                    "TotalLossValuationWarningPercentage=" + txtTotalLossValuation.value + "&" +
                    "TotalLossWarningPercentage=" + txtTotalLossWarning.value + "&" +
                    "WorkmanshipWarrantyPeriod=" + WorkmanshipWarranty.value + "&" +
                    "PartsWarrantyPeriod=" + PartsWarranty.value + "&" +
                    "BillingModelCD=" + BillingModelCD.value + "&" +
                    "InvoicingModelPaymentCD=" + InvoicingModelPaymentCD.value + "&" +
                    "IngresAccountingID=" + txtIngresAccountingID.value + "&" +
                    "EarlyBillFlag=" + EarlyBillFlag.value + "&" +
                    "EarlyBillStartDate=" + EarlyBillStartDate.value + "&" +

                    // 10Jul2019 TVD - Adding FeeCreditFlag
                     "FeeCreditFlag=" + chkAllowCredits.value + "&" +

                    "SysLastUserID=" + curUser;

        switch (sRowAttrib){
            case "new":
                sProc = "uspAdmInsuranceInsDetail";
                break;
            default:
                sProc = "uspAdmInsuranceUpdDetail";

                if (bClaimAspectLoaded)
                    sRequest += "&CATSelectedFlags=" + frmInsClaimAspectTab.getSelectedCATFlags();
                else
                    sRequest += "&CATSelectedFlags=NC";

                if (bPaymentTypeLoaded)
                    sRequest += "&PTSelectedFlags=" + frmInsPaymentTypeTab.getSelectedPTFlags();
                else
                    sRequest += "&PTSelectedFlags=NC";

                sRequest += "&SysLastUpdatedDate=" + txtLastUpdatedDate.value;
                break;
        }
        
        //alert(txtLastUpdatedDate.value);
        //alert(sProc + "\n" + sRequest);
        //return;
        
        if (sProc != "" && sRequest != ""){
            var aRequests = new Array();
            aRequests.push( { procName : sProc,
                              method   : "ExecuteSpNpAsXML",
                              data     : sRequest }
                          );
            var objRet = XMLSave(makeXMLSaveString(aRequests));

            if (objRet && objRet.xml) {
				var sLastUpdateDate = "";
				var objRetXML = objRet.xml;

				if (sRowAttrib != "new") {
					oResultNode = objRetXML.documentElement.selectSingleNode("/Root/StoredProc[@name='" + sProc + "']/Result/Root/InsuranceCompany[@InsuranceID='" + txtInsID.value + "']/@SysLastUpdatedDate");
					if (oResultNode)
						sLastUpdateDate = oResultNode.text;
				} else {
					oResultNode = objRetXML.documentElement.selectSingleNode("/Root/StoredProc[@name='" + sProc + "']/Result/Root/InsuranceCompany");
					if (oResultNode) {
						iInsID = txtInsID.value = curRow.cells[5].innerText = oResultNode.getAttribute("InsuranceID");
						sLastUpdateDate = curRow.cells[25].innerText = oResultNode.getAttribute("SysLastUpdatedDate");
					}
				}

				txtLastUpdatedDate.value = sLastUpdateDate;

				tabDetail.contentSaved();
				if (bClaimAspectLoaded)
					resetControlBorders(frmInsClaimAspectTab.document.body);
				
				if (bPaymentTypeLoaded)
					resetControlBorders(frmInsPaymentTypeTab.document.body);
            }

            if (objRet && objRet.code == 0) {
				curRow.setAttribute("rowAttr", "", 0);
				
				if (sEnabled == "1")
					curRow.cells[1].innerHTML = "Yes";
				else
					curRow.cells[1].innerHTML = "No";

				curRow.cells[2].innerHTML = txtName.value;
				var sCityState = "";

				if (txtCity.value != "")
					sCityState = txtCity.value + (State.selectedIndex != -1 ? ", " : "");
				
				if (State.text != "")
					sCityState += State.text;

				curRow.cells[3].innerHTML = (sCityState != "" ? sCityState : "&nbsp;");
				
				curRow.cells[5].innerText = txtID.value;
				curRow.cells[2].innerText = txtName.value;
				curRow.cells[6].innerText = txtAddress1.value;
				curRow.cells[7].innerText = txtAddress2.value;
				curRow.cells[8].innerText = txtCity.value;
				curRow.cells[9].innerText = State.value;
				curRow.cells[10].innerText = txtZip.value;
				curRow.cells[11].innerText = AssignmentAtSelectionFlag.value;
				curRow.cells[12].innerText = InsuranceBusType.value;
				curRow.cells[13].innerText = txtFax.areaCode;
				curRow.cells[15].innerText = txtFax.phoneExtension;
				curRow.cells[14].innerText = txtFax.phoneExchange;
				curRow.cells[16].innerText = txtFax.phoneExchange;
				curRow.cells[17].innerText = txtSSN.value;
				curRow.cells[18].innerText = LicenseDetCode.value;
				curRow.cells[19].innerText = txtPhone.areaCode;
				curRow.cells[20].innerText = txtPhone.phoneExchange;
				curRow.cells[21].innerText = txtPhone.phoneExtension;
				curRow.cells[22].innerText = txtPhone.phoneExchange;
				curRow.cells[23].innerText = txtTotalLossValuation.value;
				curRow.cells[24].innerText = txtTotalLossWarning.value;
				curRow.cells[25].innerText = txtLastUpdatedDate.value;
				curRow.cells[26].innerText = WorkmanshipWarranty.value;
				curRow.cells[27].innerText = PartsWarranty.value;
				curRow.cells[28].innerText = BillingModelCD.value;
				curRow.cells[29].innerText = ClientAccessFlag.value;
				curRow.cells[30].innerText = InvoicingModelPaymentCD.value;
				//curRow.cells[31].innerText = UseCEIShopsFlag.value;
				curRow.cells[31].innerText = chkAllowCredits.value;
				curRow.cells[32].innerText = txtIngresAccountingID.value;
				//curRow.cells[33].innerText = InvoicingModelBillingCD.value;
				curRow.cells[34].innerText = EarlyBillFlag.value;
				curRow.cells[35].innerText = EarlyBillStartDate.value;

				hideNewInsMisc(false);
				txtID.CCDisabled = true;
				txtIngresAccountingID.CCDisabled = true;
				bAddNew = false;
			}
        }
        
        setTimeout(ShowSB100,300);
        
        if (iGoToTabIdx != null) {
			tabInsurance.SelectTab(iGoToTabIdx);
			iGoToTabIdx = null;
        }
        
        gbDirty = false;
        btnSaveCallback();

        if (oClickedRow) 
			GridClick(oClickedRow);

        return true;
	}
  
  
	function btnSaveCallback(){
		InsStat.Hide();
	}


	function checkFeeDirty(){
  
		var oFromTab = event.fromTab;
		var oToTab = event.toTab;
    
		if (oFromTab && oFromTab.id == "tabFees") {

		if (oFromTab.isReady && frmInsFeeBillTab.frameElement.readyState == "complete") {
		
			if (typeof(frmInsFeeBillTab.isDirty) != "function") return;
			
			if (frmInsFeeBillTab.isDirty() == true) {
				var sSave = YesNoMessage("Need to Save", "Fee Information for the current selected insurance company has changed. \nDo you want to save the changes?.");
				
				if (sSave == "Yes"){
					if (frmInsFeeBillTab.btnSaveFeeClick2() == false){
						event.returnValue = false;
						bFeeValid = false;
					} else {
						event.returnValue = true;
						bFeeValid = true;
					}
				} else if (sSave == "No") {
					bFeesLoaded = false;
					bFeeValid = false;
					frmInsFeeBillTab.frameElement.src = "/blank.asp";
				} else {
					event.returnValue = false;
					bFeeValid = false;
				}
			} else
				bFeeValid = true;
			}
		}
		
		if (oToTab.id == "tabFees" && tabDetail.isDirty == true) {
			var sSave = YesNoMessage("Need to Save", "Insurance Information for the current selected insurance company has changed. \nDo you want to save the changes?.");
			
			if (sSave == "Yes") {
				iGoToTabIdx = tabFees.TabIdx;
				//alert(iGoToTabIdx);
				event.returnValue = false;
				btnSave();
			} else if (sSave == "No") {
				tabDetail.resetTab();
				
				switch (sBillingModel) {
					case "C":
						rbExposureBilling.setAttribute("CCDisabled", "true", 0);
						rbClaimBilling.value = 1;
						break;
					case "E":
						rbClaimBilling.setAttribute("CCDisabled", "true", 0);
						rbExposureBilling.value = 2;
						break;
					case "B":
						rbClaimBilling.setAttribute("CCDisabled", true, 0);
						rbExposureBilling.setAttribute("CCDisabled", true, 0);
						rbCombinationBilling.value = 3;
						break;
				}
			} else {
				event.returnValue = false;
			}
		}
		
		if (oFromTab && oFromTab.id == "tabShopExcl") {
		  if (frmInsShopExclTab.gbDirty == true) {
			  var sSave = YesNoMessage("Need to Save", "Shop include/exclude list has changed. \nDo you want to save the changes?.");
			  if (sSave == "Yes"){
				frmInsShopExclTab.doSave2();
				event.returnValue = false;
			  } else {
				frmInsShopExclTab.reloadShops();
			  }
		  }
		}
	}


    function checkCombinationSelect() {
		var oRadio = event.radio;

		if (oRadio && oRadio.checkedValue == 3) {
			var sRet = YesNoMessage("Confirmation", "Once you upgrade to Combination Billing, the Claim level and Exposure level billing models will not be available.\nDo you want to continue with the upgrade?.");

			if (sRet != "Yes")
				event.returnValue = false;
		  }
    }
    
    
	function EarlyBillFlagChange() {
		
		if (EarlyBillFlag.value == "1") {
			EarlyBillStartDate.setAttribute("CCDisabled", false, 0);
			EarlyBillStartDate.setAttribute("Value", "", 0);
		} else {
			EarlyBillStartDate.setAttribute("CCDisabled", true, 0);
			EarlyBillStartDate.setAttribute("Value", "", 0);
		}
	}
	
    
	if (document.attachEvent)
      document.attachEvent("onclick", top.hideAllMenuScriptlets);
    else
      document.onclick = top.hideAllMenuScriptlets;
	
	
]]>
        </SCRIPT>
      </HEAD>
      <BODY class="bodyAPDSub" unselectable="on" style="background-color:#FFFFFF;overflow:hidden" onload="" topmargin="3px" leftmargin="8px" tabIndex="-1">
        <IE:APDStatus id="InsStat" name="InsStat" width="240" height="75"/>
        <SPAN align="right" unselectable="on" id="divButtons" name="divButtons" style="position:absolute;top:1px;left:870px;">
          <IE:APDButton name="btnAdd1" id="btnAdd1" value="Add" CCTabIndex="1" CCDisabled="true" onbuttonClick="btnAdd()" style="behavior: url(/behaviors/APDButton.htc)"/>
          <IE:APDButton name="btnSave1" id="btnSave1" value="Save" CCTabIndex="2" CCDisabled="true" onbuttonClick="btnSave()" style="behavior: url(/behaviors/APDButton.htc)"/>
        </SPAN>
        <DIV align="right" style="height:18px;">
        </DIV>
        <DIV id="CPScrollTable" style="width:100%;">
          <xsl:attribute name="CRUD">
            <xsl:value-of select="$InfoCRUD"/>
          </xsl:attribute>
          <span >
            <TABLE unselectable="on" class="ClaimMiscInputs" width="972px" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
              <colgroup>
                <col width="24px" style="display:none"/>
                <col width="73px"/>
                <col width="400px"/>
                <col width="224px"/>
              </colgroup>
              <TR unselectable="on" class="QueueHeader" style="height:28px">
                <TD unselectable="on" class="TableSortHeader" type="Number">
                  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                </TD>
                <TD unselectable="on" class="TableSortHeader" type="Number"> Enabled </TD>
                <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Name </TD>
                <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> City, State </TD>
              </TR>
            </TABLE>
          </span>
          <DIV unselectable="on" class="autoflowTable" style="width: 972px; height: 178px;" id="grid1" name="grid1">
            <TABLE unselectable="on" id="tblSort2" class="GridTypeTable" width="925px" cellspacing="1" border="0" cellpadding="3" style="table-layout:fixed;" disabled="true">
              <colgroup>
                <col width="21px" style="display:none"/>
                <col width="100px"/>
                <col width="557px"/>
                <col width="274px"/>
                <col width="0px"/>
                <col width="0px"/>
                <col width="0px"/>
                <col width="0px"/>
              </colgroup>
              <TBODY bgColor1="ffffff" bgColor2="fff7e5">
                <xsl:for-each select="Insurance">
                  <xsl:call-template name="InsuranceRows"></xsl:call-template>
                </xsl:for-each>
              </TBODY>
            </TABLE>
          </DIV>
        </DIV>

        <IE:APDTabGroup id="tabInsurance" name="tabInsurance" width="972px" height="250px" preselectTab="0" many="true" showADS="false" style="padding:0px;padding-top:5px;" onTabLoadComplete="pageInit()" onTabBeforeDeselect="checkFeeDirty()">
          <IE:APDTab id="tabDetail" name="tabDetail" caption="Details" tabPage="divInsDetailTab" onTabSelect="refreshDetails()" />
          <IE:APDTab id="tabClaimAspect" name="tabClaimAspect" caption="Claim Aspect" tabPage="divInsClaimAspect" onTabSelect="refreshClaimAspect()" />
          <IE:APDTab id="tabPaymentType" name="tabPaymentType" caption="Payment Type" tabPage="divInsPaymentType" onTabSelect="refreshPaymentType()" />
          <IE:APDTab id="tabShopExcl" name="tabShopExcl" caption="Shops" tabPage="divInsShopExcl" onTabSelect="refreshShopExcl()" />
          <IE:APDTab id="tabFees" name="tabFees" caption="Fees" tabPage="divInsFees" onTabSelect="refreshFees()" />
          <IE:APDTab id="tabRates" name="tabRates" caption="Labor Rates" tabPage="divLaborRates" onTabSelect="refreshRates()" />
          <IE:APDTab id="tabWorkflow" name="tabWorkflow" caption="Workflow" tabPage="divInsWorkflow" onTabSelect="" />
          <IE:APDTabPage name="divInsDetailTab" id="divInsDetailTab" style="display:none">
            <xsl:call-template name="InsuranceDetail"></xsl:call-template>
          </IE:APDTabPage>
          <IE:APDTabPage name="divInsClaimAspect" id="divInsClaimAspect" style="display:none">
            <IFRAME name="frmInsClaimAspectTab" id="frmInsClaimAspectTab" SRC="/blank.asp" style="border:0px solid gray;position:relative;height:100%;width:100%;top:0px;left:0px;background:transparent;" allowtransparency='true'>
            </IFRAME>
          </IE:APDTabPage>
          <IE:APDTabPage name="divInsPaymentType" id="divInsPaymentType" style="display:none">
            <IFRAME name="frmInsPaymentTypeTab" id="frmInsPaymentTypeTab" SRC="/blank.asp" style="border:0px solid gray;position:relative;height:100%;width:100%;top:0px;left:0px;background:transparent;" allowtransparency='true'>
            </IFRAME>
          </IE:APDTabPage>
          <IE:APDTabPage name="divInsShopExcl" id="divInsShopExcl" style="display:none">
            <IFRAME name="frmInsShopExclTab" id="frmInsShopExclTab" SRC="/blank.asp" style="border:0px solid gray;position:relative;height:100%;width:100%;top:0px;left:0px;background:transparent;" allowtransparency='true'>
            </IFRAME>
          </IE:APDTabPage>
          <IE:APDTabPage name="divInsFees" id="divInsFees" style="display:none">
            <IFRAME name="frmInsFeeBillTab" id="frmInsFeeBillTab" SRC="/blank.asp" style="border:0px solid gray;position:relative;height:100%;width:100%;top:0px;left:0px;background:transparent;" allowtransparency='true'>
            </IFRAME>
          </IE:APDTabPage>
          <IE:APDTabPage name="divLaborRates" id="divLaborRates" style="display:none">
            <IFRAME name="frmInsLaborRatesTab" id="frmInsLaborRatesTab" SRC="/blank.asp" style="border:0px solid gray;position:relative;height:100%;width:100%;top:0px;left:0px;background:transparent;" allowtransparency='true'>
            </IFRAME>
          </IE:APDTabPage>
          <IE:APDTabPage name="divInsWorkflow" id="divInsWorkflow" style="display:none">
            <IFRAME name="frmInsWorkflowTab" id="frmInsWorkflowTab" SRC="/blank.asp" style="border:0px solid gray;position:relative;height:100%;width:100%;top:0px;left:0px;background:transparent;" allowtransparency='true'>
            </IFRAME>
          </IE:APDTabPage>
        </IE:APDTabGroup>

        <br/>
        <br/>
      </BODY>
    </HTML>
  </xsl:template>

  <xsl:template name="InsuranceRows">
    <TR unselectable="on" RowType="Insurance" onClick="GridClick(this)" style="height:21px;cursor:hand;">
      <xsl:attribute name="rowNo">
        <xsl:value-of select="position()"/>
      </xsl:attribute>
      <TD class="GridTypeTD" nowrap="" name="rowInd">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD class="GridTypeTD" nowrap="" name="InsEnabledFlag">
        <xsl:attribute name="EnabledFlag">
          <xsl:value-of select="@EnabledFlag"/>
        </xsl:attribute>
        <!-- <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('EnabledFlag',string(@EnabledFlag),'1','0',position(),1)"/> -->
        <!-- <xsl:value-of disable-output-escaping="yes" select="js:APDCheckBox('EnabledFlag', '', string(@EnabledFlag), '', 'true', 'false', '', 'true', '', 'false')"/> -->
        <xsl:choose>
          <xsl:when test="@EnabledFlag='1'">Yes</xsl:when>
          <xsl:otherwise>No</xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD class="GridTypeTD" style="text-align:left;" name="InsName">
        <xsl:value-of select="@Name"/>
      </TD>
      <TD class="GridTypeTD" style="text-align:left;" name="AddressCityState">
        <xsl:value-of disable-output-escaping="yes" select="user:myNameConcat(string(@AddressCity), string(@AddressState), ', ')"/>
      </TD>
      <TD style="display:none;" name="DisplayOrder">
        <xsl:value-of select="@DisplayOrder"/>
      </TD>
      <TD style="display:none;" name="InsuranceCompanyID">
        <xsl:value-of select="@InsuranceCompanyID"/>
      </TD>
      <TD style="display:none;" name="Address1">
        <xsl:value-of select="@Address1"/>
      </TD>
      <TD style="display:none;" name="Address2">
        <xsl:value-of select="@Address2"/>
      </TD>
      <TD style="display:none;" name="AddressCity">
        <xsl:value-of select="@AddressCity"/>
      </TD>
      <TD style="display:none;" name="AddressState">
        <xsl:value-of select="@AddressState"/>
      </TD>
      <TD style="display:none;" name="AddressZip">
        <xsl:value-of select="@AddressZip"/>
      </TD>
      <TD style="display:none;" name="AssignmentAtSelectionFlag">
        <xsl:value-of select="@AssignmentAtSelectionFlag"/>
      </TD>
      <TD style="display:none;" name="BusinessTypeCD">
        <xsl:value-of select="@BusinessTypeCD"/>
      </TD>
      <TD style="display:none;" name="FaxAreaCode">
        <xsl:value-of select="@FaxAreaCode"/>
      </TD>
      <TD style="display:none;" name="FaxExchangeNumber">
        <xsl:value-of select="@FaxExchangeNumber"/>
      </TD>
      <TD style="display:none;" name="FaxExtensionNumber">
        <xsl:value-of select="@FaxExtensionNumber"/>
      </TD>
      <TD style="display:none;" name="FaxUnitNumber">
        <xsl:value-of select="@FaxUnitNumber"/>
      </TD>
      <TD style="display:none;" name="FedTaxId">
        <xsl:value-of select="@FedTaxId"/>
      </TD>
      <TD style="display:none;" name="LicenseDeterminationCD">
        <xsl:value-of select="@LicenseDeterminationCD"/>
      </TD>
      <TD style="display:none;" name="PhoneAreaCode">
        <xsl:value-of select="@PhoneAreaCode"/>
      </TD>
      <TD style="display:none;" name="PhoneExchangeNumber">
        <xsl:value-of select="@PhoneExchangeNumber"/>
      </TD>
      <TD style="display:none;" name="PhoneExtensionNumber">
        <xsl:value-of select="@PhoneExtensionNumber"/>
      </TD>
      <TD style="display:none;" name="PhoneUnitNumber">
        <xsl:value-of select="@PhoneUnitNumber"/>
      </TD>
      <TD style="display:none;" name="TotalLossValuationWarningPercentage">
        <xsl:value-of select="@TotalLossValuationWarningPercentage"/>
      </TD>
      <TD style="display:none;" name="TotalLossWarningPercentage">
        <xsl:value-of select="@TotalLossWarningPercentage"/>
      </TD>
      <TD style="display:none;" name="SysLastUpdatedDate">
        <xsl:value-of select="@SysLastUpdatedDate"/>
      </TD>
      <TD style="display:none;" name="WorkmanshipPeriodID">
        <xsl:value-of select="@WorkmanshipPeriodID"/>
      </TD>
      <TD style="display:none;" name="PartsPeriodID">
        <xsl:value-of select="@PartsPeriodID"/>
      </TD>
      <TD style="display:none;" name="BillingModelCD">
        <xsl:value-of select="@BillingModelCD"/>
      </TD>
      <TD style="display:none;" name="ClientAccessFlag">
        <xsl:value-of select="@ClientAccessFlag"/>
      </TD>
      <TD style="display:none;" name="InvoicingModelPaymentCD">
        <xsl:value-of select="@InvoicingModelPaymentCD"/>
      </TD>
      <!--<TD style="display:none;" name="UseCEIShopsFlag">
        <xsl:value-of select="@UseCEIShopsFlag"/>
      </TD>-->
      <TD style="display:none;" name="FeeCreditFlag">
        <xsl:value-of select="@FeeCreditFlag"/>
      </TD>
      <TD style="display:none;" name="IngresAccountingID">
        <xsl:value-of select="@IngresAccountingID"/>
      </TD>
      <TD style="display:none;" name="InvoicingModelBillingCD">
        <xsl:value-of select="@InvoicingModelBillingCD"/>
      </TD>
      <TD style="display:none;" name="EarlyBillFlag">
        <xsl:value-of select="@EarlyBillFlag"/>
      </TD>
      <TD style="display:none;" name="EarlyBillStartDate">
        <xsl:value-of select="js:formatSQLDateTime(string(@EarlyBillStartDate))"/>
      </TD>
    </TR>
  </xsl:template>

  <xsl:template name="InsuranceDetail">

    <xsl:variable name="sCaptionList">
      <xsl:for-each select="/Root/Reference[@ListName='BusinessTypeCD']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListValue"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sValueList">
      <xsl:for-each select="/Root/Reference[@ListName='BusinessTypeCD']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListCode"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sCaptionList2">
      <xsl:for-each select="/Root/Reference[@ListName='LicenseDeterminationCD']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListValue"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sValueList2">
      <xsl:for-each select="/Root/Reference[@ListName='LicenseDeterminationCD']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListCode"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sCaptionList3">
      <xsl:for-each select="/Root/Reference[@ListName='State']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListValue"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sValueList3">
      <xsl:for-each select="/Root/Reference[@ListName='State']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListCode"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sCaptionList4">
      <xsl:for-each select="/Root/Reference[@ListName='WarrantyWorkmanship']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListValue"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sValueList4">
      <xsl:for-each select="/Root/Reference[@ListName='WarrantyWorkmanship']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListCode"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sCaptionList5">
      <xsl:for-each select="/Root/Reference[@ListName='WarrantyRefinish']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListValue"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sValueList5">
      <xsl:for-each select="/Root/Reference[@ListName='WarrantyRefinish']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListCode"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sBillingModelCaptions">
      <xsl:for-each select="/Root/Reference[@ListName='BillingModel']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListValue"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sBillingModelValues">
      <xsl:for-each select="/Root/Reference[@ListName='BillingModel']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListCode"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sInvoicingModelPaymentCaptions">
      <xsl:for-each select="/Root/Reference[@ListName='InvoicingPaymentModel']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListValue"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sInvoicingModelPaymentValues">
      <xsl:for-each select="/Root/Reference[@ListName='InvoicingPaymentModel']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListCode"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sInvoicingModelBillingCaptions">
      <xsl:for-each select="/Root/Reference[@ListName='InvoicingBillingModel']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListValue"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sInvoicingModelBillingValues">
      <xsl:for-each select="/Root/Reference[@ListName='InvoicingBillingModel']">
        <xsl:if test="@ListValue != ''">
          <xsl:value-of select="@ListCode"/>
          <xsl:if test="not(position()=last())">
            <xsl:text>|</xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>


    <table cellspacing="0" cellpadding="1" border="0" style="margin:0px;table-layout:fixed;">
      <colgroup>
        <col width="85px"/>
        <col width="100px"/>
        <col width="100px"/>
        <col width="100px"/>
        <col width="15px"/>
        <col width="160px"/>
        <col width="*"/>
      </colgroup>
      <tr>
        <td unselectable="on" class="TDLabel">ID</td>
        <td>
          <IE:APDInputNumeric id="txtID" name="txtID" precision="5" scale="0" neg="false" int="true" max="32768" required="true" canDirty="true" CCTabIndex="3" />
        </td>
        <td unselectable="on" class="TDLabel" colspan="2">
          Enabled
          <IE:APDCheckBox id="EnabledFlag" name="EnabledFlag" caption="" value="0" CCTabIndex="3"/>
        </td>
        <td unselectable="on"></td>
        <td unselectable="on" class="TDLabel">SSN/EIN</td>
        <td unselectable1="on">
          <IE:APDInputNumeric id="txtSSN" name="txtSSN" value="" precision="15" scale="0" CCTabIndex="17"/>
        </td>
      </tr>

      <tr>
        <td unselectable="on" class="TDLabel">Name</td>
        <td unselectable="on" colspan="3">
          <IE:APDInput id="txtName" name="txtName" value="" size="50" maxLength="50" CCTabIndex="4" />
        </td>
        <td unselectable="on">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </td>
        <td unselectable="on" class="TDLabel">Business Type</td>
        <td unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:APDCustomSelect('InsuranceBusType', '', string($sValueList), string($sCaptionList), 'true', '', 'false', 'false', 18, 'true', '', '', '', 'true', '')"/>
        </td>
      </tr>
      <tr>
        <td unselectable="on" class="TDLabel">Address</td>
        <td unselectable="on" colspan="3">
          <IE:APDInput id="txtAddress1" name="txtAddress1" value="" size="50" maxLength="49" CCTabIndex="5" />
        </td>
        <td unselectable="on">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </td>
        <td unselectable="on" class="TDLabel">Assignment at Selection</td>
        <td unselectable="on">
          <IE:APDCheckBox id="AssignmentAtSelectionFlag" name="AssignmentAtSelectionFlag" caption="" value="0" CCTabIndex="19" />
        </td>
      </tr>
      <tr>
        <td unselectable="on">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </td>
        <td unselectable="on" colspan="3">
          <IE:APDInput id="txtAddress2" name="txtAddress2" value="" size="50" maxLength="49" CCTabIndex="5" />
        </td>
        <td unselectable="on">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </td>
        <td unselectable="on" class="TDLabel">License Determination</td>
        <td unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:APDCustomSelect('LicenseDetCode', '', string($sValueList2), string($sCaptionList2), 'true', '', 'false', 'false', 20, 'true', '', '', '', 'true', '')"/>
        </td>
      </tr>
      <tr>
        <td unselectable="on" class="TDLabel">Zip</td>
        <td unselectable="on" colspan="3">
          <IE:APDInput id="txtZip" name="txtZip" value="" size="9" maxLength="8" CCTabIndex="6" onfocusout="ValidateZip(txtZip, txtCity, State)" />
        </td>
        <td unselectable="on">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </td>
        <td unselectable="on" class="TDLabel">Total Loss Valuation</td>
        <td unselectable="on">
          <IE:APDInputNumeric id="txtTotalLossValuation" name="txtTotalLossValuation" precision="9" scale="5" neg="false" int="false" CCTabIndex="21" />
        </td>
      </tr>
      <tr>
        <td unselectable="on" class="TDLabel">City</td>
        <td unselectable="on" colspan="3">
          <IE:APDInput id="txtCity" name="txtCity" value="" size="30" maxLength="29" CCTabIndex="7" />
        </td>
        <td unselectable="on">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </td>
        <td unselectable="on" class="TDLabel">Total Loss Warning</td>
        <td unselectable="on">
          <IE:APDInputNumeric id="txtTotalLossWarning" name="txtTotalLossWarning" precision="9" scale="5" neg="false" int="false" CCTabIndex="22" />
        </td>
      </tr>
      <tr>
        <td unselectable="on" class="TDLabel">State</td>
        <td unselectable="on" colspan="3">
          <xsl:value-of disable-output-escaping="yes" select="js:APDCustomSelect('State', '', string($sValueList3), string($sCaptionList3), 'true', '9', 'false', 'false', 8, 'true', '', '', '', 'true', '')"/>
        </td>
        <td unselectable="on">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </td>
        <td unselectable="on" class="TDLabel">Minimum Workmanship Warranty</td>
        <td unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:APDCustomSelect('WorkmanshipWarranty', '', string($sValueList4), string($sCaptionList4), 'true', '', 'false', 'false', 23, 'true', '', '', '', 'true', '')"/>
        </td>
      </tr>
      <tr>
        <td unselectable="on" class="TDLabel">Phone</td>
        <td unselectable="on" colspan="3">
          <IE:APDInputPhone id="txtPhone" name="txtPhone" value="" areaCode="" phoneExchange="" phoneNumber="" phoneExtension="" showExtension="true" CCTabIndex="9"/>
        </td>
        <td unselectable="on">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </td>
        <td unselectable="on" class="TDLabel">Minimum Refinish Warranty</td>
        <td unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:APDCustomSelect('PartsWarranty', '', string($sValueList5), string($sCaptionList5), 'true', '', 'false', 'false', 24, 'true', '', '', '', 'true', '')"/>
        </td>
      </tr>
      <tr>
        <td unselectable="on" class="TDLabel">Fax</td>
        <td unselectable="on" colspan="3">
          <IE:APDInputPhone id="txtFax" name="txtFax" value="" areaCode="" phoneExchange="" phoneNumber="" phoneExtension="" showExtension="true" CCTabIndex="13"/>
        </td>
        <td unselectable="on">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </td>
        <td unselectable="on">Client Access</td>
        <td unselectable="on">
          <IE:APDCheckBox id="ClientAccessFlag" name="ClientAccessFlag" caption="" value="0" CCTabIndex="25"/>
        </td>
      </tr>
      
      
      <tr>
        <td unselectable="on">Billing Model:</td>
        <td unselectable="on" class="TDLabel" colspan="3">
          <xsl:value-of disable-output-escaping="yes" select="js:APDCustomSelect('BillingModelCD', '', string($sBillingModelValues), string($sBillingModelCaptions), 'true', '', 'false', 'false', 14, 'true', '', '', '', 'true', '')"/>
          <!--
              <IE:APDRadioGroup id="rbBillingModeGrp" name="rbBillingModeGrp" canDirty="true" >
                <IE:APDRadio id="rbClaimBilling" name="rbClaimBilling" caption="Claim Level" checkedValue="1" uncheckedValue="0" groupName="rbBillingModeGrp" canDirty="true" CCTabIndex="26"/>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                <IE:APDRadio id="rbExposureBilling" name="rbExposureBilling" caption="Exposure Level" checkedValue="2" uncheckedValue="0" groupName="rbBillingModeGrp" canDirty="true" CCTabIndex="27"/>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                <IE:APDRadio id="rbCombinationBilling" name="rbCombinationBilling" caption="Combination Level" checkedValue="3" uncheckedValue="0" groupName="rbBillingModeGrp" canDirty="true" CCTabIndex="28"/>
              </IE:APDRadioGroup>
              -->
        </td>
        <td></td>
        <td unselectable="on">Ingress Accounting ID:</td>

        <td unselectable="on">
          <table>
            <tbody>
              <tr>
                <td unselectable="on" class="TDLabel">
                  <IE:APDInputNumeric id="txtIngresAccountingID" name="txtIngresAccountingID" precision="9" scale="0" neg="false" int="true" CCTabIndex="26" />
                </td>
                <td class="TDLabel" colspan="2" >
                  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                  Fee Credit Processing:
                </td>
                <td>
                  <IE:APDCheckBox id="chkAllowCredits" name="chkAllowCredits" caption="" value="0" CCTabIndex="27"/>
                </td>
              </tr>
            </tbody>
          </table>
        </td>




      </tr>
     
      
      
      
      <tr>
        <td unselectable="on">Invoicing Model:</td>
        <td unselectable="on" class="TDLabel" colspan="3">
          <!-- Billing:
            <xsl:value-of disable-output-escaping="yes" select="js:APDCustomSelect('InvoicingModelBillingCD', '', string($sInvoicingModelPaymentValues), string($sInvoicingModelPaymentCaptions), 'true', '', 'false', 'false', 15, 'true', '', '', '', 'true', '')"/>            
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp; -->
          Payment:
          <xsl:value-of disable-output-escaping="yes" select="js:APDCustomSelect('InvoicingModelPaymentCD', '', string($sInvoicingModelPaymentValues), string($sInvoicingModelPaymentCaptions), 'true', '', 'false', 'false', 16, 'true', '', '', '', 'true', '')"/>
        </td>
        <td></td>
        <td unselectable="on">Early Bill:</td>
        <td unselectable="on">
          <table>
            <tbody>
              <tr>
                <td width="100">
                  <IE:APDCheckBox id="EarlyBillFlag" name="EarlyBillFlag" caption="" value="0" CCTabIndex="27" canDirty="true" onchange="EarlyBillFlagChange()"/>
                </td>
                <td>Early Bill Start Date:</td>
                <td>
                  <IE:APDInputDate id="EarlyBillStartDate" name="EarlyBillStartDate" size="20" maxLength="20" CCTabIndex="28" CCDisabled="false" canDirty="true" />
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr style="display:none">
        <td unselectable="on">ID</td>
        <td unselectable="on">
          <input type="hidden" name="txtInsID"  id="txtInsID" />
        </td>
        <td unselectable="on">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </td>
        <td unselectable="on">Updated Date:</td>
        <td unselectable="on">
          <input type="hidden" name="txtLastUpdatedDate"  id="txtLastUpdatedDate" />
        </td>
      </tr>
    </table>
    <script language="javascript">
      //used by the shop search iframe...
      var sStateCodes = "<xsl:value-of select="$sValueList3"/>";
      var sStates = "<xsl:value-of select="$sCaptionList3"/>";
    </script>
  </xsl:template>
</xsl:stylesheet>
