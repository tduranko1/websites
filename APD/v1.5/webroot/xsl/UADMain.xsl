<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="UserList">
<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- <xsl:param name="CRUDInfo"/> -->
<xsl:param name="InfoCRUD" select="Action:View User Administration Desktop"/>

<xsl:template match="/Root">
    <xsl:choose>
        <xsl:when test="substring($InfoCRUD, 3, 1) = 'U'">
            <xsl:call-template name="mainHTML">
              <xsl:with-param name="InfoCRUD" select="$InfoCRUD"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <html>
          <head>
              <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
              <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
          </head>
          <body class="bodyAPDSub" unselectable="on" style="background-color:#FFFFFF">
            <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
              <tr>
                <td align="center">
                  <font color="#ff0000"><strong>You do not have sufficient permission to view users.
                  <br/>Please contact your supervisor.</strong></font>
                </td>
              </tr>
            </table>
          </body>
          </html>
        </xsl:otherwise>
    </xsl:choose>

</xsl:template>

<xsl:template name="mainHTML">
  <xsl:param name="InfoCRUD"/>
<html>
<head>
<!-- STYLES -->
<LINK rel="stylesheet" href="../css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="../css/tabs.css" type="text/css"/>

<!-- Script modules -->
<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
	var sCRUDInfo = "<xsl:value-of select="$InfoCRUD"/>";
<![CDATA[
	// button images for ADD/DELETE/SAVE
	preload('buttonAdd','/images/but_ADD_norm.png');
	preload('buttonAddDown','/images/but_ADD_down.png');
	preload('buttonAddOver','/images/but_ADD_over.png');
	preload('buttonDel','/images/but_DEL_norm.png');
	preload('buttonDelDown','/images/but_DEL_down.png');
	preload('buttonDelOver','/images/but_DEL_over.png');
	preload('buttonSave','/images/but_SAVE_norm.png');
	preload('buttonSaveDown','/images/but_SAVE_down.png');
	preload('buttonSaveOver','/images/but_SAVE_over.png');

  // Page Initialize
  function PageInit() 
  { 
    tabsIntabs[0].tabs.onchange=tabChange;
    tabsIntabs[0].tabs.onBeforechange=tabBeforeChange;
  }

  function handleJSError(sFunction, e)
  {
    var result = "An error has occurred on the page.\nFunction = " + sFunction + "\n";
    for (var i in e) {
      result += "e." + i + " = " + e[i] + "\n";
    }
    ClientError(result);
  }

  function UserGridSelect(oRow)
  {
    try {
  	  var strUserID = oRow.cells[5].innerText;
    	document.location="UserDetail.asp?UserID=" + strUserID;
    }
    catch(e) {
      handleJSError("UserGridSelect", e)
    }
  }

	var inADS_Pressed = false;		// re-entry flagging for buttons pressed
	// Handle click of Add button
	function ADS_Pressed(sAction, sName)
	{
    try {
  		if (inADS_Pressed == true) return;
  		inADS_Pressed = true;

  		var oDiv = document.getElementById(sName);

  		if (sAction == "Insert") {
  			if (oDiv.Many == "true") {
  				parent.oIframe.frameElement.src = "/admin/UserDetail.asp?UserID=New";
  			}
  		}
      else {
        var userError = new Object();
        userError.message = "Unhandled ADS action: Action = " + sAction;
        userError.name = "MiscellaneousException";
        throw userError;
			}

  		inADS_Pressed = false;
    }
    catch(e) {
      handleJSError("ADS_Pressed", e);
    }
	}
    
    function tabBeforeChange(obj, tab)
    {
        if (tab.id == "tab12"){
            var obj;
            obj = document.getElementById("ifrmRoles");
            obj.style.visibility = "visible";
            obj.style.display = "inline";
            obj.src = "roles.asp";
        }
        else {
            var obj;
            obj = document.getElementById("ifrmRoles");
            obj.style.visibility = "hidden";
            obj.style.display = "inline";
        }
    }
    
	if (document.attachEvent)
      document.attachEvent("onclick", top.hideAllMenuScriptlets);
    else
      document.onclick = top.hideAllMenuScriptlets;
	
]]>
</SCRIPT>

<title>APD User and Role Management</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
</head>

<BODY unselectable="on" class="bodyAPDSub" onLoad="tabInit(false,'#FFFFFF'); InitFields(); PageInit()" style="margin:0px;padding:0px;overflow:hidden" tabIndex="-1">
<!-- Start Tabs1 -->
<DIV unselectable="on" style="position:absolute; visibility: visible; top: 0px; left: 0px;width:100%;height:100%;">

  <DIV unselectable="on" id="tabs1" class="apdtabs">
  	<SPAN unselectable="on" id="tab11" class="tab1">User List</SPAN>
  	<SPAN unselectable="on" id="tab12" class="tab1">Role List</SPAN>
  </DIV>
  
  <!-- Start User List -->
    <DIV unselectable="on" class="content1" id="content11" name="UserList" Many="true" style="visibility:visible; width:99%; height:96%">
         <IFRAME id="ifrmUsers" src="/admin/users.asp" style='width:976px; height:97%; border:0px; visibility:inherit; position:absolute; background:transparent; overflow:hidden' allowtransparency='true' >
         </IFRAME>
    </DIV>
    <DIV unselectable="on" class="content1" id="content12" name="RoleList" Many="true" CRUD="" style="visibility:hidden; width:99%; height:96%;">
         <IFRAME id="ifrmRoles" src="/blank.asp" style='width:976px; height:97%; border:0px; visibility:inherit; position:absolute; background:transparent; overflow:hidden' allowtransparency='true' >
         </IFRAME>
    </DIV>
</DIV>
</BODY>
</html>
</xsl:template>

<xsl:template name="formatDateTime">
    <xsl:param name="sDateTime"/>

    <xsl:variable name="sYear" select="substring($sDateTime, 1,4)"/>
    <xsl:variable name="sMonth" select="substring($sDateTime, 6,2)"/>
    <xsl:variable name="sDay" select="substring($sDateTime, 9,2)"/>
    
    <xsl:value-of select="concat($sMonth, '/', $sDay, '/', $sYear)"/>
    
    <xsl:variable name="sHH1" select="substring($sDateTime, 12,2)"/>
    <xsl:if test="number($sHH1) &gt; 12">
        <xsl:variable name="sHH" select="format-number(number($sHH1) - 12, '00')"/>
        <xsl:variable name="sAMPM" select="PM"/>
        <xsl:variable name="sMM" select="substring($sDateTime, 15,2)"/>
        <xsl:variable name="sSS" select="substring($sDateTime, 18,2)"/>
        <xsl:value-of select="concat(' ', $sHH, ':', $sMM, ':', $sSS, ' PM')"/>
    </xsl:if>
    <xsl:if test="number($sHH1) &lt; 13">
        <xsl:variable name="sHH" select="$sHH1"/>
        <xsl:variable name="sMM" select="substring($sDateTime, 15,2)"/>
        <xsl:variable name="sSS" select="substring($sDateTime, 18,2)"/>
        <xsl:value-of select="concat(' ', $sHH, ':', $sMM, ':', $sSS, ' AM')"/>
    </xsl:if>
</xsl:template>
</xsl:stylesheet>
