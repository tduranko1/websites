<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTMain">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="Environment"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID             SP                          XSL             PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTShopSearchReferenceGetListXML,SMTWebSignupSrch.xsl -->


<HEAD>
  <TITLE>Shop Maintenance</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
var gsFramePageFile = "SMTWebSignupSrch.asp"
var gsSearchingIntervalID;

top.layerEntityData.style.visibility = "hidden";
top.layerTabs.style.visibility="hidden";

<![CDATA[
//init the table selections, must be last
function initPage(){
  top.gsLoc = window.location.toString().slice(16);
	top.gsFramePageFile = gsFramePageFile;

  top.SwitchMenuSearchItem("web", 3);
  
	txtShopLoadID.value = parent.gsShopLoadID;
	txtFEIN.value = parent.gsSSN;
	txtName.value = parent.gsName;
	txtCity.value = parent.gsCity;
	selState.value = parent.gsState;
	txtPhoneAreaCode.value = parent.gsPhoneArea;
	txtPhoneExchange.value = parent.gsPhoneExchange;
	txtPhoneUnit.value = parent.gsPhoneUnit;
}

function Search(){
	if (isNaN(txtShopLoadID.value)){
		top.ClientWarning("Please enter a numeric value for 'Shop Load ID'.");
		txtShopLoadID.select();
		return;
	}

	btnSearch.disabled = true;
	
	document.frames["ifrmShopList"].frameElement.style.visibility = "hidden";
	SearchIndicator();

	// save search parameters
	parent.gsShopLoadID = txtShopLoadID.value;
	parent.gsSSN = txtFEIN.value;
	parent.gsName = txtName.value;
	parent.gsCity = txtCity.value;
	parent.gsState = selState.value;
  parent.gsZip = txtZip.value;
  parent.gsCertifiedFirstID = txtCertifiedFirstID.value;
	parent.gsPhoneArea = txtPhoneAreaCode.value;
	parent.gsPhoneExchange = txtPhoneExchange.value;
	parent.gsPhoneUnit = txtPhoneUnit.value;
	
	var val;
	var qString = "";
	var aInputs = document.all.tags("INPUT");
  for (var i=0; i < aInputs.length; i++) {
		var sType = aInputs[i].type;
    if (sType == "text" || sType == "hidden"){
		  val = escape(aInputs[i].value.replace(/'/g, "''"));
      qString += aInputs[i].name + "=" + val.substr(0, aInputs[i].maxLength) + "&";
		}
  }
	aInputs = document.all.tags("SELECT");
  for (var i=0; i < aInputs.length; i++) {
		qString += aInputs[i].name + "=" + aInputs[i].value + "&";
	}
	qString = qString.slice(0, qString.length - 1);
	btnClear.disabled = true;
  document.frames["ifrmShopList"].frameElement.src = "SMTWebSignupSearchResults.asp?" + qString;
}

function SearchIndicator(){
	lblCount.style.display = "none";
	lblSearching.style.display = "inline";
	lblSearching.innerText = "Searching";
	gsSearchingIntervalID = window.setInterval("if (lblSearching.innerText == 'Searching.......') lblSearching.innerText = 'Searching'; lblSearching.innerText += '.'", 750);
}

function NumbersOnly(event){
	if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;
}

function ShowCount(count){
	var sCountMsg = (count == '1') ? "1 Shop Found" : count + " Shops Found";
	
  lblCount.innerText = sCountMsg;
	lblSearching.style.display = "none";
	lblCount.style.display = "inline";
	window.clearInterval(parent.gsSearchingIntervalID);

	if (count > 0) document.frames["ifrmShopList"].frameElement.style.visibility = "visible";

	btnClear.disabled = false;
	btnSearch.disabled = false;
}

function Clear(){
	txtShopLoadID.value = "";
	txtFEIN.value = "";
	txtName.value = "";
	txtCity.value = "";
	selState.selectedIndex = 0;
	txtPhoneAreaCode.value = "";
	txtPhoneExchange.value = "";
	txtPhoneUnit.value = "";
  txtCertifiedFirstID.value = "";
	lblSearching.style.display = "none";
	lblCount.style.display = "none";
	lblSorting.style.display = "none";
}


]]>

</SCRIPT>
</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" style="background:transparent;" onLoad="initPage();" onkeypress="if (event.keyCode == 13) Search();">

<div style="position:absolute; top:5; left:330; color:darkblue;">Web Signups</div>

<TABLE unselectable="on" width="738" class="ShopAssignTableSrh" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td valign="top">
      <table border="0" cellspacing="0" cellpadding="1" width="100%">
        <colgroup>
          <col width="115"/>
          <col width="100%"/>
        </colgroup>
        <tr>
          <td nowrap="nowrap">Shop Load ID:</td>
          <td nowrap="nowrap">
            <input type="text" name="ShopLoadID" id="txtShopLoadID" size="9" maxLength="8" class="inputFld" onkeypress="NumbersOnly(window.event)"/>
          </td>
        </tr>
        <tr>
          <td>SSN/EIN:</td>
          <td nowrap="nowrap">
            <input type="text" class="inputFld" name="FedTaxID" id="txtFEIN" size="16" maxLength="9" onkeypress="NumbersOnly(window.event)"/>
          </td>
        </tr>
        <tr>
          <td nowrap="nowrap">Name:</td>
          <td nowrap="nowrap">
            <input type="text" class="inputFld" name="Name" id="txtName" size="51" maxLength="50"/>
          </td>
        </tr>
        <tr>
          <td nowrap="nowrap">Certified First ID:</td>
          <td nowrap="nowrap">
            <input type="text" class="inputFld" name="CertifiedFirstID" id="txtCertifiedFirstID" size="51" maxLength="50"/>
          </td>
        </tr>
      </table>
    </td>
    <td width="10px"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    <td valign="top">
      <table> 
        <tr>
          <td nowrap="nowrap">City:</td>
          <td nowrap="nowrap"><input type="text" class="inputFld" name="AddressCity" id="txtCity" size="41" maxLength="30"/></td>
        </tr>
        <tr>
          <td nowrap="nowrap">State:</td>
          <td nowrap="nowrap">
            <select name="AddressState" id="selState" class="inputFld" style="width:146;">
              <option value=""></option>
			        <xsl:for-each select="Reference[@ListName='State']"><xsl:call-template name="BuildSelectOptions"/></xsl:for-each>
            </select>
          </td>
        </tr>
        <tr>
          <td nowrap="nowrap">Zip Code:</td>
          <td nowrap="nowrap">
            <input type="text" name="AddressZip" id="txtZip" class="inputFld" size="6" maxlength="5" onkeypress="NumbersOnly(window.event)"/>
          </td>
        </tr>
        <tr>
          <td nowrap="nowrap">Phone:</td>
          <td nowrap="nowrap">
            <input type="text" class="inputFld" name="PhoneAreaCode" id="txtPhoneAreaCode" size="2" maxLength="3" onkeypress="NumbersOnly(window.event)"/> -
            <input type="text" class="inputFld" name="PhoneExchange" id="txtPhoneExchange" size="2" maxLength="3" onkeypress="NumbersOnly(window.event)"/> -
            <input type="text" class="inputFld" name="PhoneUnit" id="txtPhoneUnit" size="3" maxLength="4" onkeypress="NumbersOnly(window.event)"/>
          </td>
        </tr>
      </table>
     
	    <table width="100%" border="0" cellpadding="2" cellspacing="0">
	      <tr>
		      <td valign="bottom" align="right">
		        <input type="button" id="btnClear" value="Clear" class="formButton" onclick="Clear()" style="width:80"/>
		        <input type="button" id="btnSearch" value="Search" class="formButton" onclick="Search()" style="width:80"/>
		      </td>
		    </tr>
	    </table>
	    <IMG src="/images/spacer.gif" width="6" height="6" border="0" />
	  </td>
  </tr>
</TABLE>

<div style="position:absolute; top:110px; left: 353px; height:12px: width; 80px;">
	<label id="lblCount" style="color:blue; font-weight:600; display:none"></label>
</div>

<div style="position:absolute; top:110px; left: 353px; height:12px: width; 80px;">
	<label id="lblSearching" style="color:blue; font-weight:600; display:none"></label>
</div>

<div style="position:absolute; top:130px; left: 353px; height:12px: width; 80px;">
	<label id="lblSorting" style="color:blue; font-weight:600; display:none"></label>
</div>

<IFRAME id="ifrmShopList" src="../blank.asp" style="position:relative; top:4px; width:758; height:385; border:0px; visibility:hidden; background:transparent; overflow:hidden" allowtransparency="true" >
</IFRAME>

</BODY>
</HTML>

</xsl:template>

<xsl:template name="BuildSelectOptions">
	<xsl:param name="current"/>
	<option>
		<xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>

		<xsl:if test="@ReferenceID = $current">
			<xsl:attribute name="selected"/>
		</xsl:if>

		<xsl:value-of select="@Name"/>
	</option>
</xsl:template>

</xsl:stylesheet>
