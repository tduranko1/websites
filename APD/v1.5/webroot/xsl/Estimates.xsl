<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="Estimates">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function AdjustCRUD( strCrud)
     {
      var strRet = "_" + strCrud.substr(1,2) + "_";
       return strRet;
     }
  ]]>
</msxsl:script>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="EstimateCRUD" select="Estimate"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserId"/>
<xsl:param name="LynxId"/>
<xsl:param name="VehicleNumber"/>
<xsl:param name="WindowID"/>
<xsl:param name="ImageRootDir"/>
<xsl:param name="showTotals"/>
<xsl:param name="isSaved"/>

<xsl:template match="/Root">

<xsl:variable name="DocumentID" select="/Root/@DocumentID" />

<xsl:value-of select="js:SetCRUD( string($EstimateCRUD) )"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:s:\websites\apd\v1.2\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},19,uspEstimateGetDetailXML,Estimates.xsl,934  -->

<HEAD>
<TITLE>
  <xsl:choose>
    <xsl:when test="Document/@DocumentType = 'Supplement'"><xsl:value-of select="concat(Document/@DocumentType, '-', Document/@SupplementSeqNumber)"/></xsl:when>
    <xsl:otherwise><xsl:value-of select="Document/@DocumentType"/></xsl:otherwise>
  </xsl:choose>
  <xsl:value-of select="concat(' / ', $LynxId, '-', $VehicleNumber)"/>
</TITLE>

<LINK rel="stylesheet" href="/css/apdmain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdselect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/coolselect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/coolselect.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/utilities.js"></SCRIPT>
<script type="text/javascript" src="/js/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/js/formats.js"></script>

<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<SCRIPT language="JavaScript">

  var gsCRUD = '<xsl:value-of select="$EstimateCRUD"/>';
  var gsWindowID = '<xsl:value-of select="$WindowID"/>';
  var gsLynxID = '<xsl:value-of select="$LynxId"/>';
  var gsUserID = '<xsl:value-of select="$UserId"/>';
  var gsDocumentID = '<xsl:value-of select="$DocumentID"/>';
  var gsImageRootDir = '<xsl:value-of select="$ImageRootDir"/>';
  var imgLocation = '<xsl:value-of select="/Root/Document/@ImageLocation"/>';
  var gsshowTotals = '<xsl:value-of select="$showTotals"/>';//Flag to go to totals tab onLoad
  var gsVANFlag = '<xsl:value-of select="/Root/Document/@VANFlag"/>';
  var gsDetailEditableFlag = '<xsl:value-of select="/Root/Document/@DetailEditableFlag"/>';
  var strShopLocationID = '<xsl:value-of select="js:cleanString(string(/Root/@ShopLocationID))"/>';
  var strShopLocationCity = '<xsl:value-of select="js:cleanString(string(/Root/@ShopCity))"/>';
  var strShopLocationCounty = '<xsl:value-of select="js:cleanString(string(/Root/@ShopCounty))"/>';
  var strShopLocationState = '<xsl:value-of select="js:cleanString(string(/Root/@ShopState))"/>';
  var strInsuranceCompanyID = '<xsl:value-of select="js:cleanString(string(/Root/@InsuranceCompanyID))"/>';
  var strInsuranceCompanyName = '<xsl:value-of select="js:cleanString(string(/Root/@InsuranceCompanyName))"/>';
  var strServiceChannelCD = '<xsl:value-of select="js:cleanString(string(/Root/@ServiceChannelCD))"/>';
  var strApprovedDocumentCount = '<xsl:value-of select="/Root/@ApprovedDocumentCount"/>';
  var strApprovedDate = '<xsl:value-of select="/Root/Document/Estimate/@ApprovedDate"/>';
  var strSupervisorFlag = '<xsl:value-of select="/Root/@SupervisorFlag"/>';
  var strEarlyBillFlag = '<xsl:value-of select="/Root/@EarlyBillFlag"/>';
  var strUnapproveExpired = '<xsl:value-of select="/Root/Document/Estimate/@UnapproveExpired"/>';

  var gsOriginalMode = false;
  var gsUpdOrigAndAgrd = false;
  var gsPageInitFlag = false;
  var gsUpdSummFromDetails = false;
  var gsSummaryDirtyFlag = false;
  var gsDetailNumber = 0;
  var saveBG;
  var objLaborRateXML, objShopPricingXML;
  var blnExceptionsShown = false;

  // Array of opened windows for this claim.
  // Need to track opended non-modal windows in order to close them when this window is closed.
  var gAryOpenedEstWindows = new Array();
  var oOpener = null;

  var gIsSaved = "<xsl:value-of select="$isSaved"/>";

<![CDATA[

  // button images for ADD/DELETE/SAVE
  preload('buttonAdd','/images/but_ADD_norm.png')
  preload('buttonAddDown','/images/but_ADD_down.png')
  preload('buttonAddOver','/images/but_ADD_over.png')
  preload('buttonDel','/images/but_DEL_norm.png')
  preload('buttonDelDown','/images/but_DEL_down.png')
  preload('buttonDelOver','/images/but_DEL_over.png')
  preload('buttonSave','/images/but_SAVE_norm.png')
  preload('buttonSaveDown','/images/but_SAVE_down.png')
  preload('buttonSaveOver','/images/but_SAVE_over.png')

  // Variables to set for tooltips:
    messages= new Array()
    // Write your descriptions in here.
    messages[0]="";
    messages[1]="";

//  var oPopup = window.createPopup();
	var editablecell = null;

  // Page Initialize
  function PageInit()
  {
    $("#divExceptions").hide();
    $("#divBtnExceptions").hide();
    $("#divExceptions").bind("click", function(){
      $("#divExceptions").hide();blnExceptionsShown=false;
    });
    tabsIntabs[0].tabs.onchange=tabChange;
    tabsIntabs[0].tabs.onBeforechange=tabBeforeChange;

    EstDetailsPageInit();
    EstSummPageInit();

	if (gIsSaved == "Saved") {
		document.getElementById("lblSaved").style.display = "inline";
	}
	else {
		document.getElementById("lblSaved").style.display = "none";
	}
	
    if (gsshowTotals == 1)
      tabsIntabs[0].tabArray[2].depressTab();

    showEstimateDoc();

    divLoading.style.visibility = "hidden";
    oOpener = window.opener;
    
    getData();
    
    if (strEarlyBillFlag == "1" && strServiceChannelCD == "PS"){
       btn_ApproveDocument.CCDisabled = false;
       if (strApprovedDocumentCount != "0"){
         btn_ApproveDocument.CCDisabled = true;
         btn_ApproveDocument.title = "Another Estimate/Supplement has already been approved.";
       } else {
         var dtApproved = new Date();
         if (strApprovedDate != ""){
            dtExpired = new Date(formatSQLDateTime(strApprovedDate));
            dtExpired.setMinutes(dtExpired.getMinutes() + 30);
            dtNow = new Date();
            if (dtNow >= dtExpired){
               btn_ApproveDocument.CCDisabled = true;
            }
            if (strSupervisorFlag == "1"){
               btn_ApproveDocument.CCDisabled = false;
            }
         }
       }
    }
  }


  // Est Details Page Initialize
  function EstDetailsPageInit()
  {
    if (gsVANFlag == 1)
    {
      detailsFormatFields();
      resizeScrollTable(document.getElementById("EstScrollTable"));
    }
  }

  // Est Summ Page Initialize
  function EstSummPageInit()
  {
    gsPageInitFlag = true;
    formatFields();
    
    if (gsVANFlag == 1)
    {
      setAgreedMode();
    }

    //if (gsVANFlag != 1)
    //{
    //  initCalcRows();
    //  calcTaxableTotal();
    //  getOriginalTotals();
    //  getAgreedTotals();
    //}

    //if (gsVANFlag != 1 && txtOriginalExtendedAmt_36.value == 0)
    //{
    //  gsOriginalMode = true;
    //  setOriginalMode();
    //}
    //else if next line

    if (gsVANFlag == 1 && gsDetailEditableFlag == 0)
    {
      setAgreedModeDetailNonEdit();
    }

    //else if (gsVANFlag == 0 && txtOriginalExtendedAmt_36.value != 0)
    //{
    //  setAgreedModeDetailNonEdit();
    //  document.all["SetOriginalModeIcon"].style.display = "inline";
    //}
    //else
    //{
    //  setAgreedMode();
    //  if (gsVANFlag != 1)
    //    document.all["SetOriginalModeIcon"].style.display = "inline";
    //}
    gsPageInitFlag = false;

    //if (gsOriginalMode != true && txtAgreedExtendedAmt_36.value == 0)
    //  document.all["SetDupFieldsIcon"].style.display = "inline";
  }


  var inADS_Pressed = false;    // re-entry flagging for buttons pressed
  // ADS Button pressed
  //function ADS_Pressed(sAction, sName)
  function SaveAll()
  {
    // here we ignore the sAction and sName
    if (inADS_Pressed == true) return;

    inADS_Pressed = true;
    btn_Save.CCDisabled = true;
    //objDivWithButtons = divWithButtons;
    //disableADS(objDivWithButtons, true);

    if (gsUpdSummFromDetails == true) //check for changes made in Estimate Detail before saving
      getDetailsTotals();

    if (gsVANFlag == 1) //if a VAN estimate, save changes to the Estimate Details
    {
      details_ADS_Pressed();
    }
    summary_ADS_Pressed(); //save changes to the Estimate Summary

    inADS_Pressed = false;
    //disableADS(objDivWithButtons, false);
    //objDivWithButtons = null;
    btn_Save.CCDisabled = false;
  }


  // when changing tabs...
  function tabBeforeChange(obj, tab)
  {
    var relatedTab = obj;
	  if (obj == tab) return;

    // check for any cells in editable mode and close it before clicing away from the Estimate Details tab
    if (obj.content.id == "content12" && editablecell != null)
			EditableCellClose(editablecell.firstChild);

    // check for changes made in Detail if going to the Summary tab
    if ((obj.content.id == "content11" || obj.content.id == "content12" && tab.content.id == "content13") && gsUpdSummFromDetails == true)
    {
      getDetailsTotals();
      //SetDirtyFlag(false);
      //gbDirtyFlag = false;
    }
    // check for changes made in Summary, reset the global dirty flag and set the local Summary dirty flag
    if ((obj.content.id == "content13" && tab.content.id == "content11" || tab.content.id == "content12") && gbDirtyFlag == true)
    {
      gsSummaryDirtyFlag = true;
      //SetDirtyFlag(false);
      //gbDirtyFlag = false;
    }
    return true;
  }


  // Controls tab content when clicked
  function onTabChangeBubble(obj)
  {
    if (obj.activeTab.content.id == "content13")
    {
      if (tblEstimateSummary.style.display == "none")
        tblEstimateSummary.style.display = "inline";
    }
    
    if (obj.activeTab.content.id == "content14")
    {
      if (ifrmContent.frameElement.src == "")
        ifrmContent.frameElement.src= "EstimateAudit.asp?WindowID=" + gsWindowID + "&DocumentID=" + gsDocumentID;
    }
  }


  // Calls the image viewer and makes it visible in the page
  function showEstimateDoc()
  {
    var sHREF = "/EstimateDocView.asp?WindowID=" + gsWindowID + "&docPath=" + gsImageRootDir + imgLocation;
  	//window.showModelessDialog(sHREF, "", "dialogHeight:490px; dialogWidth:540px; dialogTop:120px; dialogLeft:474px; resizable:yes; status:no; help:no; center:no;");
    var winName = "EstimateDocView_" + gsLynxID;
  	var sWinEstimateDoc = window.open(sHREF, winName, "Height=490px, Width=690px, Top=150px, Left=90px, resizable=Yes, status=No, menubar=No");
    gAryOpenedEstWindows.push(sWinEstimateDoc);
    sWinEstimateDoc.focus();
  }
  

  // Calls the image viewer and makes it visible in the page
  function showPricing()
  {
    /*var sHREF = "/ShopPricingHistory.asp?WindowID=" + gsWindowID + "&docPath=" + gsImageRootDir + imgLocation + "&DocumentID=" + gsDocumentID;
    //window.showModelessDialog(sHREF, "", "dialogHeight:630px; dialogWidth:840px; dialogTop:120px; dialogLeft:474px; resizable:yes; status:no; help:no; center:no;");
    var winName = "ShopPricingHistory_" + gsLynxID;
    var sWinShopPricing = window.open(sHREF, winName, "Height=596px, Width=764px, Top=80px, Left=200px, resizable=Yes, status=No, menubar=No");
    gAryOpenedEstWindows.push(sWinShopPricing);
    sWinShopPricing.focus();*/
      var args = {InsuranceCompanyID: strInsuranceCompanyID, 
      InsuranceCompanyName: strInsuranceCompanyName, 
      State: strShopLocationState, 
      City: strShopLocationCity, 
      County: strShopLocationCounty, 
      ShopLocationID: strShopLocationID, 
      ServiceChannelCD: strServiceChannelCD};
      sWinShopPricing = window.showModelessDialog("exceptionreference.htm", args, "dialogHeight:740px;dialogWidth:265px;dialogLeft;status:no;scroll:no;help:no");
      gAryOpenedEstWindows.push(sWinShopPricing);
  }


  //See if dirty flags are set and save accordingly
  function chkNeedToSave()
  {
    if (gsUpdSummFromDetails == true || gsSummaryDirtyFlag == true || gbDirtyFlag == true )
    {
      var sSave = YesNoMessage("Need To Save", "The information in Estimates has changed!  Do you want to save?");
      if (sSave == "Yes")
      	SaveAll();
      else
      {
        SetDirtyFlag(false);
        //gbDirtyFlag = false;
      	//if (typeof(parent.gbDirtyFlag)=='boolean')
      	//	parent.gbDirtyFlag = false;
      }
    }
  }

  //Close Estimate Div and return to Vehicle
  function divClose()
  {
    if (typeof (parent.CancelShowEstimate) == "function")
      parent.CancelShowEstimate();
    else
      window.close();
  }


  //Close opened windows
  function chkOpenedWindow()
  {
    if (gAryOpenedEstWindows.length > 0)
    {
      for (var i=0; i<gAryOpenedEstWindows.length; i++)
      {
        if (typeof(gAryOpenedEstWindows[i]) != "undefined")  // test to see if window exists
        {
          if (!(gAryOpenedEstWindows[i].closed))             // test to see if a window is still open
            gAryOpenedEstWindows[i].close();
        }
      }
    }
  }
  
  function requestReinspection(){
    frmReIRequest.DocumentID.value = gsDocumentID;
    frmReIRequest.UserID.value = gsUserID;
    if (btnReqReinspection.value == "Request Reinspection"){     
      disableControls(true);
      bReIRequested = true;
      var sURL = "/frmComments.htm";
      var sArgs = "dialogWidth:350px;dialogHeight:165px;scrollbars:no;center:yes;location:no;directories:no;status:no;menubar:no;toolbar:no;resizable:no;help:no";
      var sRet = window.showModalDialog(sURL, "", sArgs);
      frmReIRequest.Comments.value = sRet;
      frmReIRequest.fn.value = "request";
    } else {
      var sRet = YesNoMessage("Confirm Cancellation", "Do you want to cancel the reinspection request?");
      if (sRet != "Yes") return;
      disableControls(true);
      frmReIRequest.fn.value = "cancel";
    }
    frmReIRequest.submit();
  }
  
  function disableVehCurrentAssignment(bDisabled){
    if (oOpener){
      oOpener.disableCurrentAssignment(bDisabled);
    }
  }
  
  function disableControls(bDisable){
    if (btnReqReinspection)
      btnReqReinspection.CCDisabled = bDisable;
  }
  
  function showToolTip(obj){
    if (toolTip)
      toolTip.style.display = "inline";
  }

  function hideToolTip(obj){
    if (toolTip)
      toolTip.style.display = "none";
  }

  function SetDirtyFlag(state)
  {
    gbDirtyFlag = state;
    if (state == true)
      btn_Save.CCDisabled = false;
    else
      btn_Save.CCDisabled = true;
  }

  // Calls the image viewer and makes it visible in the page
  function showAuditDoc()
  {
    var sHREF = "EstimateAudit.asp?WindowID=" + gsWindowID + "&DocumentID=" + gsDocumentID;
  	//window.showModelessDialog(sHREF, "", "dialogHeight:490px; dialogWidth:540px; dialogTop:120px; dialogLeft:474px; resizable:yes; status:no; help:no; center:no;");
    var winName = "AuditDocView_" + gsLynxID;
  	var sWinAuditDoc = window.open(sHREF, winName, "Height=490px, Width=700px, Top=150px, Left=90px, resizable=Yes, status=No, menubar=No");
    gAryOpenedEstWindows.push(sWinAuditDoc);
    sWinAuditDoc.focus();
  }
  
  // Show AuditEstimateValues window - WJC - 8/10/2011
  function showAuditEstimateValues()
  {
		var sHREF = "AuditEstimateValues.asp?WindowID=" + gsWindowID + "&DocumentID=" + gsDocumentID;
		var winName = "AuditEstimateValues_" + gsLynxID;

		// 09Jul2012 - TVD - Increased Height for new fields added
		var sWinAuditEstValues = window.open(sHREF, winName, "Height=300px, Width=450px, Top=300px, Left=300px, resizable=No, status=No, menubar=No");
		gAryOpenedEstWindows.push(sWinAuditEstValues);
		sWinAuditEstValues.focus();
  }
  
   function getData(){
      var sProc;
      var sRequest = "InsuranceCompanyID=" + strInsuranceCompanyID + 
                     "&StateCode=" + strShopLocationState +
                     "&City=" + strShopLocationCity +
                     "&County=" + strShopLocationCounty;
                     
      sProc = "uspLaborRatesGetXML";
      var aRequests = new Array();
      aRequests.push( { procName : sProc,
                        method   : "executespnpasxml",
                        data     : sRequest }
                    );
      if (strServiceChannelCD == "PS" || strServiceChannelCD == "RRP"){
         aRequests.push( { procName : "uspSMTShopPricingHistoryGetDetailXML",
                           method   : "executespnpasxml",
                           data     : "ShopLocationID=" + strShopLocationID }
                       );
      }
      var sXMLRequest = makeXMLSaveString(aRequests);
      
      AsyncXMLSave(sXMLRequest, getDataSuccess, getDataFailure);
   }
   
   function getDataSuccess(objXML){
      //alert(objXML.xml.xml);
      objLaborRateXML = objXML.xml.selectSingleNode("//StoredProc[@name='uspLaborRatesGetXML']/Result/Root");
      if (strServiceChannelCD == "PS" || strServiceChannelCD == "RRP"){
         objShopPricingXML = objXML.xml.selectSingleNode("//StoredProc[@name='uspSMTShopPricingHistoryGetDetailXML']");
      }
      checkExceptions();
      showPricing();
   }
   
   function getDataFailure(){
      //alert("Failure");
   }
   
   function checkExceptions(){
      var systemData, estData, objNode, strMin, strMax, strExceptions, strCriteria;
      strExceptions = "";
      
      //check body labor
      strExceptions += checkException("Body Labor", parseFloat(txtOriginalUnitAmt_12.value), 
                                       "BodyRateMin", "BodyRateMax", "HourlyRateSheetMetal");

      //check Frame labor
      strExceptions += checkException("Frame Labor", parseFloat(txtOriginalUnitAmt_14.value), 
                                       "FrameRateMin", "FrameRateMax", "HourlyRateUnibodyFrame");

      //check Material labor
      strExceptions += checkException("Material Labor", parseFloat(txtOriginalUnitAmt_9.value), 
                                       "MaterialRateMin", "MaterialRateMax", "RefinishTwoStageHourly");

      //check Mechanical labor
      strExceptions += checkException("Mechanical Labor", parseFloat(txtOriginalUnitAmt_13.value), 
                                       "MechRateMin", "MechRateMax", "HourlyRateMechanical");

      //check Refinish labor
      strExceptions += checkException("Refinish Labor", parseFloat(txtOriginalUnitAmt_16.value), 
                                       "RefinishRateMin", "RefinishRateMax", "HourlyRateRefinishing");

      if (strExceptions != ""){
         //$("#EstimateExceptions").text("Rate Exception(s) found.").show();
         btn_Exception.value = "Rate Exception(s) found.";
         $("#divBtnExceptions").show();
         $("#divExceptions").html(strExceptions);
      }
      
   }
   
   function checkException(strType, estData, strLaborRateMinAttr, strLaborRateMaxAttr, strShopPricingAttr){
      var strCriteria = "";
      var strExceptions = "";
      var strMin, strMax, systemData, objNode;
      systemData = estData;
      if (strServiceChannelCD == "PS" || strServiceChannelCD == "RRP"){
         if (objShopPricingXML) {
            var objNode = objShopPricingXML.selectSingleNode("//Pricing");
            if (objNode){
               strMin = objNode.getAttribute(strShopPricingAttr);
               strMin = (strMin != null ? parseFloat(strMin) : -1);
               if (strMin > 0 && strMin < systemData){
                  systemData = strMin;
                  strCriteria = "Shop Pricing";
               }
            }
         }
      } else {
         var objNode = objLaborRateXML.selectSingleNode("//LaborRate");
         if (objNode){
            strCriteria = "Client";
            strMin = objNode.getAttribute(strLaborRateMinAttr);
            strMax = objNode.getAttribute(strLaborRateMaxAttr);
            strMin = (strMin != null ? parseFloat(strMin) : -1);
            strMax = (strMax != null ? parseFloat(strMax) : strMin);
            
            if (estData > strMax){
               systemData = strMax;
            }
         }
      }

      
      if (estData > systemData) 
         return "<li>" + strType + " Rate: " + "Estimate = $" + formatCurrencyString(estData) + "; " +
                                                 strCriteria + " = $" + formatCurrencyString(systemData) + "</li>";
      else 
         return "";
   }
   
  function showExceptions(){
      if (blnExceptionsShown){
         $("#divExceptions").hide();
         blnExceptionsShown = false;
      } else {
         $("#divExceptions").slideDown();
         blnExceptionsShown = true;
      }
  }
  
  function doApproveDocument(){
      var strRet = YesNoMessage("Confirm Approve Document", "Do you want to Approve this Document for Early Bill?");
      if (strRet == "Yes"){
         //confirm the indemnity amount
         var param = {
                        payeeName: txtAppraiserName.value,
                        payeeAddress: txtCompanyAddress.value,
                        payeeAddressCity: txtCompanyCity.value,
                        payeeAddressState: txtCompanyState.value,
                        payeeAddressZip: txtCompanyZip.value,
                        netAmount: txtAgreedExtendedAmt_36.value,
                        deductibleAmount: txtAgreedExtendedAmt_32.value,
                        taxTotalAmount: txtAgreedExtendedAmt_29.value
                     }
         if (strEarlyBillFlag == "1"  ){ if  (strServiceChannelCD != "RRP") {
            var ret = window.showModalDialog("confirmAmount.htm", param, "dialogHeight:200px;dialogWidth:400px;center:yes;resizable:no;scroll:no;status:no;");
            if(ret){
               if (ret.button == "save"){
                  //update the database
                  var sProc = "uspApproveDocument";
                  var sRequest = "DocumentID=" + gsDocumentID + 
                                 "&ApprovedFlag=1" +
                                 "&NetAmount=" + ret.netAmount + 
                                 "&DeductibleAmount=" + ret.deductibleAmount +
                                 "&TaxTotalAmount=" + ret.taxTotalAmount +
                                 "&UserID=" + gsUserID;
         
                  var sMethod = "ExecuteSpNp";
                  
                  var aRequests = new Array();
                  aRequests.push( { procName : sProc,
                                    method   : sMethod,
                                    data     : sRequest }
                                );
                  
                  var sXMLRequest = makeXMLSaveString(aRequests);
                  //alert(sXMLRequest); return;
                  
                  var objRet = XMLSave(sXMLRequest);
                  if (objRet && objRet.code == 0) {
                     if (ret.netAmount > 0){
                        ClientWarning("Document was marked as Approved for Early Billing. A Payment to " + txtAppraiserName.value + " for $" + ret.netAmount + " was automatically added.");
                        btn_ApproveDocument.value = "Unapprove";
                        btn_ApproveDocument.onButtonClick = doUnapproveDocument;
                        btn_ApproveDocument.title = "Undo Approve Document for Early Bill";
						                     }
                  }
                  
               }
            }
			}
			else 
			{
	
			         var sProc = "uspApproveDocument";
                  var sRequest = "DocumentID=" + gsDocumentID + 
                                 "&ApprovedFlag=1" +
                                 "&NetAmount=0"  + 
                                 "&DeductibleAmount=0" + 
                                 "&TaxTotalAmount=0" +
                                 "&UserID=" + gsUserID;
								
         
                  var sMethod = "ExecuteSpNp";
                  
                  var aRequests = new Array();
                  aRequests.push( { procName : sProc,
                                    method   : sMethod,
                                    data     : sRequest }
                                );
                  
                  var sXMLRequest = makeXMLSaveString(aRequests);
                  //alert(sXMLRequest); return;
                  
                  var objRet = XMLSave(sXMLRequest);
                  if (objRet && objRet.code == 0) {
                   // alert(objRet.code);
                        <!--ClientWarning("Document was marked as Approved for Early Billing. A Payment to " + txtAppraiserName.value + " for $0 was automatically added.");-->
                        btn_ApproveDocument.value = "Unapprove";
                        btn_ApproveDocument.onButtonClick = doUnapproveDocument;
                        btn_ApproveDocument.title = "Undo Approve Document for Early Bill";
						                     
                  }
						}
         }
      }

  }
  
  function doUnapproveDocument(){
      /*var dtApproved = new Date();
      if (strApprovedDate != ""){
         dtApproved = new Date(formatSQLDateTime(strApprovedDate));
         dtExpired = new Date(dtApproved);
         dtExpired.setMinutes(dtExpired.getMinutes() + 30);
         dtNow = new Date();
         if (dtExpired > dtNow || strSupervisorFlag == "1"){*/
            var sProc = "uspApproveDocument";
            var sRequest = "DocumentID=" + gsDocumentID + 
                           "&ApprovedFlag=0" +
                           "&NetAmount=0" + 
                           "&DeductibleAmount=0" + 
                           "&TaxTotalAmount=0" + 
                           "&UserID=" + gsUserID;
   
            var sMethod = "ExecuteSpNp";
            
            var aRequests = new Array();
            aRequests.push( { procName : sProc,
                              method   : sMethod,
                              data     : sRequest }
                          );
            
            var sXMLRequest = makeXMLSaveString(aRequests);
            //alert(sXMLRequest); return;
            
            var objRet = XMLSave(sXMLRequest);
            if (objRet && objRet.code == 0) {
               ClientWarning("Approved status on this document was undone.");
               btn_ApproveDocument.value = "Approve";
               btn_ApproveDocument.onButtonClick = doApproveDocument;
               btn_ApproveDocument.title = "Approve Document for Early Bill";
            }
         /*} else {
            ClientWarning("Cannot undo Approve document. Timeout expired. Please contact your supervisor.");
         }
      }*/
  }
  
//  if (document.attachEvent)
//    document.attachEvent("onclick", top.hideAllMenuScriptlets);
//  else
//    document.onclick = top.hideAllMenuScriptlets;


]]>
</SCRIPT>

<SCRIPT language="JavaScript" src="/js/EstimateDetails.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/EstimateSummary.js"></SCRIPT>

</HEAD>
<BODY class="bodyAPDSub" onLoad="tabInit(false,'#FFFFFF'); initSelectBoxes(); InitFields(); PageInit(); popupInit();" onbeforeunload="chkNeedToSave(); chkOpenedWindow()">

<div id="divLoading" name="divLoading" style="position:absolute; left:200px; top:200px; visibility:hidden; height:65px; width:300px; background-color:#FFF8DC; border:1px solid #000000; color:#4169E1; text-align:center; padding:25px; font-family:Tahoma; font-size:11px; font-weight:bold; z-index:1000; filter:progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#808080,strength=5);">
</div>
<SCRIPT>
  divLoading.innerText = "Retrieving document. Please wait...";
  divLoading.style.visibility = "visible";
</SCRIPT>

<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

  <xsl:if test="Document/Estimate/@ReinspectionCompleted = '1'">
    <div id="toolTip" name="toolTip" style="z-index:999;position:absolute;top:25px;left:450px;display:none;height1:150px;width:250px;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)">
      <table id="tblToolTip" border="0" cellspacing="0" cellpadding="0" style="height1:100%;width:100%;border:1px solid #556B2F;background-color:#FFFFFF" onreadystatechange="checkToolTipSize()">
        <tr style="height:21px;text-align:center;font-weight:bold;background-color:#556B2F;">
          <td style="color:#FFFFFF">Reinspection Details</td>
          <td><span id="spnShopLocationID"/></td>
        </tr>
        <tr>
          <td colspan="2">
            <table border="0" cellpadding="2" cellspacing="0" style="margin:5px;">
              <colgroup>
                <col width="100px"/>
                <col width="150px"/>
              </colgroup>
              <tr valign="top">
                <td><b>Reinspector:</b></td>
                <td><xsl:value-of select="Document/Estimate/@ReinspectionPerformedBy"/></td>
              </tr>
              <tr valign="top">
                <td><b>Completed on:</b> </td>
                <td><xsl:value-of select="js:formatSQLDateTime(string(Document/Estimate/@ReinspectionDate))"/></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
  </xsl:if>

  <form name="frmReIRequest" id="frmReIRequest" method="POST" action="NotifyReinspection.asp" target="ifrmRetrieveData">
    <input type="hidden" name="DocumentID" id="DocumentID"/>
    <input type="hidden" name="Comments" id="Comments"/>
    <input type="hidden" name="UserID" id="UserID"/>
    <input type="hidden" name="fn" id="fn"/>
  </form>

  <IFRAME id="ifrmRetrieveData" name="ifrmRetrieveData" src="blank.asp" frameborder="0" style="display:none; border:0px;" tabindex="-1"></IFRAME>


  <form name="frmFaxAuditChanges" id="frmFaxAuditChanges" method="POST" action="FaxEstimateChanges.asp" target="ifrmFaxAuditChanges">
    <input type="hidden" name="DocumentID" id="DocumentID"/>
    <input type="hidden" name="UserID" id="UserID"/>
    <input type="hidden" name="EstimateChanges" id="EstimateChanges"/>
  </form>

  <IFRAME id="ifrmFaxAuditChanges" name="ifrmFaxAuditChanges" src="blank.asp" frameborder="0" style="display:none; border:0px;" tabindex="-1"></IFRAME>



<!-- Approve -->
  <!-- <xsl:if test="/Root/@EarlyBillFlag = '1'"> -->
  <DIV unselectable="on" id="ApproveDocument" style="position:absolute; left:5px; top:22px;">
    <IE:APDButton id="btn_ApproveDocument" name="btn_ApproveDocument" width="80" CCTabIndex="13">
      <xsl:choose>
         <xsl:when test="/Root/Document/Estimate/@ApprovedFlag='1'">
            <xsl:attribute name="value">Unapprove</xsl:attribute>
            <xsl:attribute name="onButtonClick">doUnapproveDocument()</xsl:attribute>
            <xsl:attribute name="title">Undo Approve Document for Early Bill</xsl:attribute>
         </xsl:when>
         <xsl:otherwise>
            <xsl:attribute name="value">Approve</xsl:attribute>
            <xsl:attribute name="onButtonClick">doApproveDocument()</xsl:attribute>
            <xsl:attribute name="title">Approve Document for Early Bill</xsl:attribute>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:choose>
         <xsl:when test="/Root/Document/Estimate/@ApprovedFlag!='1'">
            <xsl:attribute name="CCDisabled">false</xsl:attribute>
         </xsl:when>
         <xsl:when test="/Root/@SupervisorFlag = '1' or /Root/Document/Estimate/@UnapproveExpired != '1'">
            <xsl:attribute name="CCDisabled">false</xsl:attribute>
         </xsl:when>
         <xsl:when test="/Root/@ServiceChannelCD != 'PS'">
            <xsl:attribute name="CCDisabled">true</xsl:attribute>
         </xsl:when>
         <xsl:otherwise>
            <xsl:attribute name="CCDisabled">true</xsl:attribute>
         </xsl:otherwise>
      </xsl:choose>
    </IE:APDButton>
  </DIV>
  <!-- </xsl:if> -->

  <!-- Audit Estimate Values - WJC -->
  <DIV unselectable="on" id="Audit Estimate Values" style="position:absolute; left:90px; top:2px;">
    <IE:APDButton id="btn_AuditEstValues" name="btn_AuditEstValues" value="Audit Estimate Values" onButtonClick="showAuditEstimateValues()" width="150" CCDisabled="false" CCTabIndex="14"/>
  </DIV>
  
  <!-- Saved -->
  <DIV id="lblSaved" name="lblSaved" style="position:absolute;top:5px;left:250px;color:blue;">
	(Audit Estimate Values Saved)
  </DIV>

<!-- View Audit Doc -->
-  <DIV unselectable="on" id="View Audit" style="position:absolute; left:5px; top:2px;">
    <IE:APDButton id="btn_ViewAudit" name="btn_ViewAudit" value="View Audit" onButtonClick="showAuditDoc()" width="80" CCDisabled="false" CCTabIndex="14"/>
  </DIV>


<!-- Send Fax Icon -->
  <DIV unselectable="on" id="SendAuditFax" style="position:absolute; left:536px; top:2px;">
    <xsl:if test="Document/Estimate/@ProgramShopAssignment = '1'">
      <IE:APDButton id="btn_SendAuditFax" name="btn_SendAuditFax" value="Send Fax" onButtonClick="SendAuditFax()" width="64" CCTabIndex="17">
        <xsl:attribute name="CCDisabled">
          <xsl:choose>
            <xsl:when test="/Root/Document/@DetailEditableFlag = 0">true</xsl:when>
            <xsl:otherwise>false</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </IE:APDButton>
    </xsl:if>
  </DIV>

  <!-- View Request Estimate -->
  <DIV unselectable="on" id="ViewReI" style="position:absolute; left:90px; top:22px;">
    <xsl:choose>
      <xsl:when test="Document/Estimate/@ActiveAssignment = '1' and Document/Estimate/@ProgramShopAssignment = '1' and Document/Estimate/@ReinspectionCompleted = '0'">
        <IE:APDButton id="btnReqReinspection" name="btnReqReinspection" width="135" onButtonClick="requestReinspection();" CCTabIndex="18" CCDisabled="false">
          <xsl:attribute name="value">
            <xsl:choose>
              <xsl:when test="Document/Estimate/@ReinspectionRequestFlag='1'">Cancel Reinspection</xsl:when>
              <xsl:otherwise>Request Reinspection</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
        </IE:APDButton>
      </xsl:when>
      <xsl:when test="Document/Estimate/@ReinspectionCompleted = '1'">
        <div style="font-weight:bold;color:#0000FF;position:relative;top:4px;" onmouseenter="showToolTip(this)" onmouseleave="hideToolTip(this)">Reinspection Completed.</div>
      </xsl:when>
    </xsl:choose>
  </DIV>

<!-- View Summary Doc -->
  <DIV unselectable="on" id="View Estimate" style="position:absolute; left:240px; top:22px;">
    <IE:APDButton id="btn_ViewEstimate" name="btn_ViewEstimate" value="Estimate" onButtonClick="showEstimateDoc()" width="60" CCDisabled="false" CCTabIndex="15"/>
  </DIV>

  <!-- View Shop Pricing -->
  <DIV unselectable="on" id="ViewPricing" style="position:absolute; left:605px; top:2px;">
    <IE:APDButton id="btn_Pricing" name="btn_Pricing" value="Profile" onButtonClick="showPricing()" width="50" CCDisabled="false" CCTabIndex="16"/>
  </DIV>

  <!-- Save Button -->
  <DIV unselectable="on" id="ViewSave" style="position:absolute; left:660px; top:2px;">
    <IE:APDButton id="btn_Save" name="btn_Save" value="Save" onButtonClick="SaveAll()" width="40" CCDisabled="true" CCTabIndex="19"/>
  </DIV>
  
<!-- Return to Claim Icon -->
  <DIV unselectable="on" id="Close" style="position:absolute; left:710px; top:4px; vetical-align:text-top; cursor:hand;">
      <IMG src="/images/close.gif" width="18" height="18" border="0" align="absmiddle" onClick="divClose()" title="Close (Return to Vehicle)" style="cursor:hand;"/>
  </DIV>

<DIV id="divBtnExceptions" style="position:absolute; visibility: visible; top: 20px; left: 540px">
   <!-- <a id="EstimateExceptions" href="javascript:showExceptions()">Checking Rates ...</a> -->
   <IE:APDButton id="btn_Exception" name="btn_Exception" value="Checking Rates..." width="160" onButtonClick="showExceptions()" CCDisabled="false" CCTabIndex="19"/>
</DIV>
<DIV id="divExceptions" unselectable="on" style="position:absolute; border:1px solid #000000; background-color: #483D8B; color:#FFF; padding:5px; top: 42px; left: 381px; width:345px; z-index:99999">
</DIV>
<!-- Start Tabs1 -->
<DIV unselectable="on" id="TabSection1" style="position:absolute; visibility: visible; top: 51px; left: 0px">

  <DIV unselectable="on" id="tabs1" class="apdtabs">
    <SPAN unselectable="on" id="tab11" class="tab1">Estimate Header</SPAN>
    <SPAN unselectable="on" id="tab12" class="tab1">Details</SPAN>
    <SPAN unselectable="on" id="tab13" class="tab1">Totals</SPAN>
    <!--<SPAN unselectable="on" id="tab14" class="tab1">Audit</SPAN>-->
  </DIV>

  <!-- Start Estimate Header Info -->
  <DIV unselectable="on" class="content1" id="content11" name="EstimateHeader" onfocusin="DivOnFocusIn(this)" style="width:725px; height:483px;">
    <xsl:attribute name="CRUD"><xsl:value-of select="user:AdjustCRUD(string($EstimateCRUD))"/></xsl:attribute>

    <xsl:for-each select="Document/Estimate">
      <xsl:call-template name="EstimateHeaderInfo"/>
    </xsl:for-each>

    <!-- <SCRIPT>ADS_Buttons('content11');</SCRIPT> -->
  </DIV>

  <!-- Start Estimate Details Info -->
  <DIV unselectable="on" class="content1" id="content12" name="EstimateDetails" onfocusin="DivOnFocusIn(this, false)" style="width:725px; height:483px;">
    <xsl:attribute name="CRUD"><xsl:value-of select="user:AdjustCRUD(string($EstimateCRUD))"/></xsl:attribute>

    <xsl:for-each select="Document" >
      <xsl:call-template name="EstimateDetailsInfo"/>
    </xsl:for-each>

<!--     <IE:APDButton id="content12_SAV" name="content12_SAV" value="Save" class="APDButton" width="60" CCDisabled="false" CCTabIndex="19" onButtonClick="ADS_Pressed("Update","content12")"/> -->
    <!-- <SCRIPT>ADS_Buttons('content12');</SCRIPT> -->
  </DIV>

  <!-- Start Estimate Summary Info -->
  <DIV unselectable="on" class="content1" id="content13" name="EstimateSummary" onfocusin="DivOnFocusIn(this, false)" style="width:725px; height:483px;">
    <xsl:attribute name="CRUD"><xsl:value-of select="user:AdjustCRUD(string($EstimateCRUD))"/></xsl:attribute>

    <xsl:for-each select="Document" >
      <xsl:call-template name="EstimateSummaryInfo"/>
    </xsl:for-each>

  <!-- Set Original Mode -->
<!-- 
  <DIV unselectable="on" id="SetOriginalModeIcon" style="position:absolute; left:10px; top:4px; vetical-align:text-top; z-index:5; font-size:10px; display:none; FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)">
    <A  href="#" onClick="forceOriginalMode()" title="Set estimate to enter original values" onFocus="if(this.blur)this.blur()">
      <IMG src="/images/new.gif" width="14" height="14" border="0" align="absmiddle"/>Orig<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Mode
    </A>
  </DIV>
 -->
  <!-- Duplicate Fields -->
<!-- 
  <DIV unselectable="on" id="SetDupFieldsIcon" style="position:absolute; left:80px; top:4px; vetical-align:text-top; z-index:5; font-size:10px; display:none; FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)">
    <A  href="#" onClick="dupFields()" title="Copy Original values to Agreed" onFocus="if(this.blur)this.blur()">
      <IMG src="/images/duplicate.gif" width="13" height="14" border="0" align="absmiddle" /><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Dup<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Fields
    </A>
  </DIV>
 -->
    <!-- <SCRIPT>ADS_Buttons('content13');</SCRIPT> -->
  </DIV>
  
  <!-- Estimate Audit -->
  <DIV unselectable="on" class="content1" id="content14" name="EstimateAudit" onfocusin="DivOnFocusIn(this)" style="width:725px; height:492px;">
    <IFRAME id="ifrmContent" src="" style="position:relative; width:715px; height:464px; border:0px; background:transparent; overflow:hidden" allowtransparency="true" >
    </IFRAME>
  </DIV>
  <!-- End Tabs1 -->

</DIV>

<DIV unselectable="on" id="divTooltip"> </DIV>

<xml id="xmlAPDPartCategory"><Root><xsl:copy-of select="/Root/Reference[@List='APDPartCategory']"/></Root></xml>
<xml id="xmlLaborType"><Root><xsl:copy-of select="/Root/Reference[@List='LaborType']"/></Root></xml>
<xml id="xmlOperationType"><Root><xsl:copy-of select="/Root/Reference[@List='OperationType']"/></Root></xml>
<xml id="xmlPartType"><Root><xsl:copy-of select="/Root/Reference[@List='PartType']"/></Root></xml>

</BODY>
</HTML>
</xsl:template>




<!-- ************************************************************************** -->
<!-- **        Estimate Header                                                  -->
<!-- ************************************************************************** -->
<!-- Gets the Estimate Header Info -->
<xsl:template name="EstimateHeaderInfo" >
  <TABLE unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="0">
    <TR unselectable="on">
      <TD unselectable="on" nowrap="nowrap">Estimate Type:</TD>
      <TD unselectable="on">
        <INPUT class="InputReadonlyField" id="txtEstimateType" size="15" readonly="true">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Document/@DocumentType"/>
            <xsl:if test="/Root/Document/@DocumentType = 'Supplement'">
              <xsl:text> </xsl:text>
              <xsl:value-of select="/Root/Document/@SupplementSeqNumber"/>
            </xsl:if>
          </xsl:attribute>
        </INPUT>
      </TD>
      <TD unselectable="on" width=""><img src="/images/spacer.gif" width="40" height="1" border="0" /></TD>
      <TD unselectable="on" nowrap="nowrap">Source of Estimate:</TD>
      <TD unselectable="on" nowrap="nowrap" width="100%">
        <INPUT class="InputReadonlyField" id="txtDocumentSourceID" size="20" readonly="true">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Document/@DocumentSource"/>
          </xsl:attribute>
        </INPUT>
      </TD>
    </TR>
  </TABLE>

  <xsl:if test="/Root/Document/@VANFlag = 1">

  <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
    <TR unselectable="on">
      <TD unselectable="on" colspan="6"><img src="/images/spacer.gif" width="100%" height="10" border="0" /></TD>
    </TR>
    <TR unselectable="on">
      <TH unselectable="on" colspan="6" align="left">Estimating Company</TH>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" colspan="6"><img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /></TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" nowrap="nowrap">Name:</TD>
      <TD unselectable="on" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCompanyName',40,'CompanyName',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on">Address:</TD>
      <TD unselectable="on" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCompanyAddress',30,'CompanyAddress',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on">Phone:</TD>
      <TD unselectable="on" width="100%" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCompanyAreaCode',2,'CompanyAreaCode',.)"/> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCompanyExchangeNumber',2,'CompanyExchangeNumber',.)"/> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCompanyUnitNumber',3,'CompanyUnitNumber',.)"/>
      </TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCompanyCity',15,'CompanyCity',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCompanyState',2,'CompanyState',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtCompanyZip',4,'CompanyZip',.)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" width="100%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    </TR>
  </TABLE>

  <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
    <TR unselectable="on">
      <TD unselectable="on" colspan="5"><img src="/images/spacer.gif" width="100%" height="1" border="0" /></TD>
    </TR>
    <TR unselectable="on">
      <TH unselectable="on" colspan="5" align="left">Appraiser Information</TH>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" colspan="5"><img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /></TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on">Name:</TD>
      <TD unselectable="on" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAppraiserName',40,'AppraiserName',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on" nowrap="nowrap">License Number:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAppraiserLicenseNumber',25,'AppraiserLicenseNumber',.)"/>
      </TD>
      <TD unselectable="on" width="100%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" colspan="3" nowrap="nowrap">Date &amp; Time of Estimate of Record:
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtWrittenDate',25,'WrittenDate',.,'',4)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" width="100%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    </TR>
  </TABLE>

  <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
    <TR unselectable="on">
      <TD unselectable="on" colspan="7"><img src="/images/spacer.gif" width="100%" height="10" border="0" /></TD>
    </TR>
    <TR unselectable="on">
      <TH unselectable="on" colspan="7" align="left">Claim Information</TH>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" colspan="7"><img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /></TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" nowrap="nowrap">Claim Number:</TD>
      <TD unselectable="on" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtClaimNumber',20,'ClaimNumber',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on" nowrap="nowrap">Insurance Company:</TD>
      <TD unselectable="on" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInsuranceCompanyName',40,'InsuranceCompanyName',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" width="100%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" nowrap="nowrap">Policy Number:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPolicyNumber',20,'PolicyNumber',.)"/>
      </TD>
      <TD unselectable="on" nowrap="nowrap">Date of Loss:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtLossDate',25,'LossDate',.,'',4)"/>
      </TD>
      <TD unselectable="on" nowrap="nowrap">Type of Loss:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtLossType',15,'LossType',.)"/>
      </TD>
      <TD unselectable="on" width="100%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" nowrap="nowrap">Point of Impact:</TD>
      <TD unselectable="on" colspan="5">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPolicyNumber',120,'VehicleImpactPoint',.)"/>
      </TD>
      <TD unselectable="on" width="100%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    </TR>
  </TABLE>

  <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
    <TR unselectable="on">
      <TD unselectable="on" colspan="6"><img src="/images/spacer.gif" width="100%" height="10" border="0" /></TD>
    </TR>
    <TR unselectable="on">
      <TH unselectable="on" colspan="6" align="left">Inspection Site Information</TH>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" colspan="6"><img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /></TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on">Location:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInspectionLocation',40,'InspectionLocation',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on" nowrap="nowrap">Address:</TD>
      <TD unselectable="on" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInspectionAddress',30,'InspectionAddress',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on">Phone:</TD>
      <TD unselectable="on" width="100%" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInspectionAreaCode',2,'InspectionAreaCode',.)"/> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInspectionExchangeNumber',2,'InspectionExchangeNumber',.)"/> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInspectionUnitNumber',3,'InspectionUnitNumber',.)"/>
      </TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" nowrap="nowrap">Inspection Type:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInspectionType',15,'InspectionType',.)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInspectionCity',15,'InspectionCity',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInspectionState',2,'InspectionState',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInspectionZip',4,'InspectionZip',.)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" width="100%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    </TR>
  </TABLE>

  <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
    <TR unselectable="on">
      <TD unselectable="on" colspan="6"><img src="/images/spacer.gif" width="100%" height="10" border="0" /></TD>
    </TR>
    <TR unselectable="on">
      <TH unselectable="on" colspan="6" align="left">Repair Facilty Information</TH>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" colspan="6"><img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /></TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on">Name:</TD>
      <TD unselectable="on" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtShopName',40,'ShopName',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on" nowrap="nowrap">Address:</TD>
      <TD unselectable="on" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtShopAddress',30,'ShopAddress',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on">Phone:</TD>
      <TD unselectable="on" width="100%">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtShopAreaCode',2,'ShopAreaCode',.)"/> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtShopExchangeNumber',2,'ShopExchangeNumber',.)"/> -
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtShopUnitNumber',3,'ShopUnitNumber',.)"/>
      </TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" nowrap="nowrap">License Number:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtShopRegistrationNumber',15,'ShopRegistrationNumber',.)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtShopCity',15,'ShopCity',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtShopState',2,'ShopState',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtShopZip',4,'ShopZip',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on" width="100%" colspan="2" nowrap="nowrap">Days to Repair:
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtRepairDays',2,'RepairDays',.)"/>
      </TD>
    </TR>
  </TABLE>

  <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
    <TR unselectable="on">
      <TD unselectable="on" colspan="8"><img src="/images/spacer.gif" width="100%" height="10" border="0" /></TD>
    </TR>
    <TR unselectable="on">
      <TH unselectable="on" colspan="8" align="left">Vehicle Information</TH>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" colspan="8"><img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /></TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on">YMM:</TD>
      <TD unselectable="on" colspan="3" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehicleYear',3,'VehicleYear',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehicleMake',18,'VehicleMake',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehicleModel',18,'VehicleModel',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on" nowrap="nowrap">Body Style:</TD>
      <TD unselectable="on" nowrap="nowrap">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehicleBodyStyle',20,'VehicleBodyStyle',.)"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on">VIN:</TD>
      <TD unselectable="on" width="100%">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVIN',25,'VIN',.)"/>
      </TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on">Color:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehicleColor',10,'VehicleColor',.)"/>
      </TD>
      <TD unselectable="on">Mileage:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehicleMileage',10,'VehicleMileage',.)"/>
      </TD>
      <TD unselectable="on" nowrap="nowrap">Engine Type:</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtVehicleEngineType',10,'VehicleEngineType',.)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" width="100%"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" valign="top">Vehicle<br />Options:</TD>
      <TD unselectable="on" colspan="7" width="100%" nowrap="nowrap" height="110px;" valign="top">
        <xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtVehicleOptions',0,8,'VehicleOptions',.)"/>
      </TD>
    </TR>
  </TABLE>

  </xsl:if>
</xsl:template>



<!-- ************************************************************************** -->
<!-- **        Estimate Details                                                 -->
<!-- ************************************************************************** -->

<!-- Gets the Estimate Details Info -->
<xsl:template name="EstimateDetailsInfo" >

  <xsl:if test="/Root/Document/@VANFlag = 0">

  	<TABLE unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="0">
      <TR>
        <TD unselectable="on" height="100" width="100%" align="center" valign="center">
          <xsl:text disable-output-escaping="yes">There are no details for manual estimates.</xsl:text>
        </TD>
      </TR>
    </TABLE>

  </xsl:if>

  <xsl:if test="/Root/Document/@VANFlag = 1">

  	<TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
      <TR bgcolor="#EFEFEF">
    		<TD unselectable="on" width="224" colspan="4"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
    		<TD unselectable="on" width="218" colspan="4" class="TableSortHeader" style="cursor:default">Original</TD>
    		<TD unselectable="on" width="10"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <xsl:if test="/Root/Document/@DetailEditableFlag = 0">
    		<TD unselectable="on" width="218" colspan="4" class="TableSortHeader" style="cursor:default; background-color:#FF9966;" title="Data received from VAN does not match the Summary totals">Agreed</TD>
      </xsl:if>
      <xsl:if test="/Root/Document/@DetailEditableFlag = 1">
    		<TD unselectable="on" width="218" colspan="4" class="TableSortHeader" style="cursor:default">Agreed</TD>
      </xsl:if>
        <TD unselectable="on" width="40"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      </TR>
    </TABLE>

    <DIV id="EstScrollTable" style="width:100%;" >
    	<TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
        <TBODY>
    	    <TR>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Line<br/>No.</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Op<br/>Code</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Part<br/>Type</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Description</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Part<br/>Cat</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Qty</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Ext<br/>Price</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Lbr<br/>Hrs</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Paint<br/>Hrs</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Lbr<br/>Cat</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default"><IMG unselectable="on" src="/images/spacer.gif" width="0" height="0" border="0"/></TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Part<br/>Cat</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Qty</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Ext<br/>Price</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Lbr<br/>Hrs</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Paint<br/>Hrs</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Lbr<br/>Cat</TD>
    				<TD unselectable="on" class="TableSortHeader" style="cursor:default">Notes</TD>
    			</TR>
    			<TR>
    			</TR>
    		</TBODY>
      </TABLE>

      <DIV id="divEstimateDetail" class="autoflowTable" style="height: 378px">
    	  <TABLE unselectable="on" id="tblEstimateDetails" class="GridTypeTable" oncontextmenu="return false" width="100%" border="0" cellspacing="1" cellpadding="0">
    	    <TBODY bgColor1="ffffff" bgColor2="fff7e5">

    			  <xsl:for-each select="Detail"	>
    				  <xsl:call-template name="DetailsInfo"/>
    			  </xsl:for-each>


    	    </TBODY>
    	  </TABLE>
  	  </DIV>
  	</DIV>

  	<DIV unselectable="on" style="position:relative; height:40px; left:1px; top:8px; z-index:5">
      <xsl:call-template name="RemarksInfo"/>
  	</DIV>

  </xsl:if>

</xsl:template>

  <!-- Gets All Details Info -->
  <xsl:template name="DetailsInfo">

    <TR unselectable="on" height="23" valign="top" style="padding:2px" oncontextmenu="callContextMenu(event.x-1,event.y-1,this)" onmouseover="rowMouseOver(this)" onmouseout="rowMouseOut(this)" dirty="false">
      <xsl:attribute name="bgColor">
        <xsl:choose>
          <xsl:when test="@EnabledFlag = '0'">
            <xsl:text disable-output-escaping="yes">#FFCC99</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
  		<xsl:attribute name="LastUpdatedDate"><xsl:value-of select="@SysLastUpdatedDate"/></xsl:attribute>
  		<xsl:attribute name="DetailNumber"><xsl:value-of select="@DetailNumber"/></xsl:attribute>
      <xsl:choose>
        <xsl:when test="@EnabledFlag = '0'">
          <xsl:attribute name="deleteDoc">true</xsl:attribute>
         <xsl:attribute name="title">This line item has been delete.</xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="deleteDoc">false</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
    	<TD unselectable="on" class="GridTypeTD" width="5%" xmlAttribute="DetailNumber" style="text-align:center;" onclick="this.focus()" nowrap="nowrap">
    		<xsl:value-of select="@DetailNumber"/>
        <xsl:if test="@ManualLineFlag = 1">
          <SPAN style="vertical-align:super; cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
            <xsl:text disable-output-escaping="yes">#</xsl:text>
            <SPAN style="display:none">Manual Line Indicator</SPAN>
          </SPAN>
        </xsl:if>
        <xsl:choose>
          <xsl:when test="@Comment = ''">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:when test="@Comment = 'Ins'">
            <SPAN style="vertical-align:super; font-weight:bold; cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
              <xsl:text disable-output-escaping="yes">+</xsl:text>
              <SPAN style="display:none">Detail Line Added by Claim Rep</SPAN>
            </SPAN>
          </xsl:when>
          <xsl:otherwise>
            <SPAN style="vertical-align:super; font-weight:bold; cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
              <xsl:text disable-output-escaping="yes">c</xsl:text>
              <SPAN style="display:none"><xsl:value-of select="@Comment"/></SPAN>
            </SPAN>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>

  <xsl:choose>
    <xsl:when test="@Comment = 'Ins'">
    	<TD unselectable="on" class="GridTypeTD" width="6%" IDIndex="19" xmlAttribute="OperationCd" ondblclick="EditableCellDblClick(this,5,1,'Dropdown','','OperationType')" onmouseover="EditableCellOver(this)" onmouseout="EditableCellOut(this)" style="text-align:center" onclick="this.focus()" nowrap="nowrap">
        <xsl:choose>
          <xsl:when test="@OperationCd = '' or @OperationCd = 'COM'">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <SPAN style="cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
          		<xsl:value-of select="@OperationCd"/>
              <SPAN style="display:none">-</SPAN>
              <SPAN style="display:none"><xsl:value-of select="/Root/Reference[@List='OperationType' and @ReferenceID=current()/@OperationCd]/@Name"/></SPAN>
            </SPAN>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>
    </xsl:when>
    <xsl:otherwise>
    	<TD unselectable="on" class="GridTypeTD" width="5%" xmlAttribute="OperationCd" style="text-align:center" onclick="this.focus()" nowrap="nowrap">
        <xsl:choose>
          <xsl:when test="@OperationCd = ''">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <SPAN style="cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
          		<xsl:value-of select="@OperationCd"/>
              <SPAN style="display:none">-</SPAN>
              <SPAN style="display:none"><xsl:value-of select="/Root/Reference[@List='OperationType' and @ReferenceID=current()/@OperationCd]/@Name"/></SPAN>
            </SPAN>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>
    </xsl:otherwise>
  </xsl:choose>

  <xsl:choose>
    <xsl:when test="@Comment = 'Ins'">
    	<TD unselectable="on" class="GridTypeTD" width="6%" IDIndex="43" xmlAttribute="PartTypeAgreedCD" ondblclick="EditableCellDblClick(this,5,1,'Dropdown','','PartType')" onmouseover="EditableCellOver(this)" onmouseout="EditableCellOut(this)" style="text-align:center" onclick="this.focus()" nowrap="nowrap">
        <xsl:choose>
          <xsl:when test="@PartTypeAgreedCD = ''">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <SPAN style="cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
          		<xsl:value-of select="@PartTypeAgreedCD"/>
              <SPAN style="display:none">-</SPAN>
              <SPAN style="display:none"><xsl:value-of select="/Root/Reference[@List='PartType' and @ReferenceID=current()/@PartTypeAgreedCD]/@Name"/></SPAN>
            </SPAN>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>
    </xsl:when>
    <xsl:otherwise>
    	<TD unselectable="on" class="GridTypeTD" width="5%" xmlAttribute="PartTypeOriginalCD" style="text-align:center" onclick="this.focus()" nowrap="nowrap">
        <xsl:choose>
          <xsl:when test="@PartTypeOriginalCD = ''">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <SPAN style="cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
          		<xsl:value-of select="@PartTypeOriginalCD"/>
              <SPAN style="display:none">-</SPAN>
              <SPAN style="display:none"><xsl:value-of select="/Root/Reference[@List='PartType' and @ReferenceID=current()/@PartTypeOriginalCD]/@Name"/></SPAN>
            </SPAN>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>
    </xsl:otherwise>
  </xsl:choose>

  <xsl:choose>
    <xsl:when test="@Comment = 'Ins'">
    	<TD unselectable="on" class="GridTypeTD" IDIndex="21" xmlAttribute="Description" ondblclick="EditableCellDblClick(this,26,3)" onmouseover="EditableCellOver(this)" onmouseout="EditableCellOut(this)" onclick="this.focus()">
        <xsl:attribute name="style">
          <xsl:choose>
            <xsl:when test="@EnabledFlag = '0'">
              <xsl:text disable-output-escaping="yes">text-align:left</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text disable-output-escaping="yes">text-align:left; cursor:text</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
         <xsl:choose>
          <xsl:when test="string-length(@Description) &lt; 30">
            <DIV unselectable="on" class="autoflowDiv" style="height:26px;">
        		  <xsl:value-of select="@Description"/>
            </DIV>
          </xsl:when>
          <xsl:otherwise>
            <DIV unselectable="on" class="autoflowDiv" style="height:26px;" onMouseOver="popUpDynMsg(this.innerText)" onMouseOut="popOut()">
        		  <xsl:value-of select="@Description"/>
            </DIV>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>
    </xsl:when>
    <xsl:otherwise>
    	<TD unselectable="on" class="GridTypeTD" width="20%" style="text-align:left" xmlAttribute="Description" onclick="this.focus()">
        <xsl:choose>
          <xsl:when test="string-length(@Description) &lt; 30">
            <DIV unselectable="on" class="autoflowDiv" style="height:26px;">
        		  <xsl:value-of select="@Description"/>
            </DIV>
          </xsl:when>
          <xsl:otherwise>
            <DIV unselectable="on" class="autoflowDiv" style="height:26px;" onMouseOver="popUpDynMsg(this.innerText)" onMouseOut="popOut()">
        		  <xsl:value-of select="@Description"/>
            </DIV>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>
    </xsl:otherwise>
  </xsl:choose>

    	<TD unselectable="on" class="GridTypeTD" width="5%" style="text-align:center;" xmlAttribute="APDPartCategoryOriginalCD" onclick="this.focus()" nowrap="nowrap">
        <xsl:choose>
          <xsl:when test="@APDPartCategoryOriginalCD = ''">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <SPAN style="cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
          		<xsl:value-of select="@APDPartCategoryOriginalCD"/>
              <SPAN style="display:none"><xsl:value-of select="/Root/Reference[@List='APDPartCategory' and @ReferenceID=current()/@APDPartCategoryOriginalCD]/@Name"/></SPAN>
            </SPAN>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>

    	<TD unselectable="on" class="GridTypeTD" width="4%" style="text-align:center" xmlAttribute="QuantityOriginal" onclick="this.focus()" nowrap="nowrap">
        <xsl:value-of select="@QuantityOriginal"/>
    	</TD>

    	<TD unselectable="on" class="GridTypeTD" width="8%" style="text-align:right" xmlAttribute="PriceOriginal" onclick="this.focus()" nowrap="nowrap">
        <xsl:choose>
          <xsl:when test="@PriceIncludedOriginalFlag = 1">
            <SPAN style="font-style:italic; cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
              <xsl:text disable-output-escaping="yes">Incl.</xsl:text>
              <SPAN style="display:none"><xsl:value-of select="@PriceOriginal"/></SPAN>
            </SPAN>
          </xsl:when>
          <xsl:otherwise>
        		<xsl:value-of select="@PriceOriginal"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="@PriceChangedFlag = 1">
          <SPAN style="font-weight:bold; cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
            <xsl:text disable-output-escaping="yes">*</xsl:text>
            <SPAN style="display:none"><xsl:value-of select="@DBPriceOriginal"/></SPAN>
          </SPAN>
        </xsl:if>
    	</TD>

    	<TD unselectable="on" class="GridTypeTD" width="5%" style="text-align:right" xmlAttribute="LaborHoursOriginal" onclick="this.focus()" nowrap="nowrap">
        <xsl:choose>
          <xsl:when test="@LaborHoursIncludedOriginalFlag = 1">
            <SPAN style="font-style:italic; cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
              <xsl:text disable-output-escaping="yes">Incl.</xsl:text>
              <SPAN style="display:none"><xsl:value-of select="@LaborHoursOriginal"/></SPAN>
            </SPAN>
          </xsl:when>
          <xsl:otherwise>
        		<xsl:value-of select="@LaborHoursOriginal"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="@LaborHoursChangedFlag = 1">
          <SPAN style="font-weight:bold; cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
            <xsl:text disable-output-escaping="yes">*</xsl:text>
            <SPAN style="display:none"><xsl:value-of select="@DBLaborHoursOriginal"/></SPAN>
          </SPAN>
        </xsl:if>
    	</TD>

    	<TD unselectable="on" class="GridTypeTD" width="5%" style="text-align:right" xmlAttribute="RefinishLaborHoursOriginal" onclick="this.focus()" nowrap="nowrap">
        <xsl:choose>
          <xsl:when test="@RefinishLaborIncludedOriginalFlag = 1">
            <SPAN style="font-style:italic; cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
              <xsl:text disable-output-escaping="yes">Incl.</xsl:text>
              <SPAN style="display:none"><xsl:value-of select="@RefinishLaborHoursOriginal"/></SPAN>
            </SPAN>
          </xsl:when>
          <xsl:otherwise>
        		<xsl:value-of select="@RefinishLaborHoursOriginal"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="@RefinishLaborHoursChangedFlag = 1">
          <SPAN style="font-weight:bold; cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
            <xsl:text disable-output-escaping="yes">*</xsl:text>
            <SPAN style="display:none"><xsl:value-of select="@DBRefinishLaborHoursOriginal"/></SPAN>
          </SPAN>
        </xsl:if>
    	</TD>

    	<TD unselectable="on" class="GridTypeTD" width="4%" xmlAttribute="LaborTypeOriginalCD" style="text-align:center" onclick="this.focus()" nowrap="nowrap">
        <xsl:choose>
          <xsl:when test="@LaborTypeOriginalCD = ''">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <SPAN style="cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
          		<xsl:value-of select="@LaborTypeOriginalCD"/>
              <SPAN style="display:none"><xsl:value-of select="/Root/Reference[@List='LaborType' and @ReferenceID=current()/@LaborTypeOriginalCD]/@Name"/></SPAN>
            </SPAN>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>

    	<TD unselectable="on" class="GridTypeTD" nowrap="nowrap" style="background:#EFEFEF;" onclick="this.focus()">
        <IMG unselectable="on" src="/images/spacer.gif" width="0" height="0" border="0"/>
    	</TD>

    	<TD unselectable="on" class="GridTypeTD" width="5%" IDIndex="37" xmlAttribute="APDPartCategoryAgreedCD" ondblclick="EditableCellDblClick(this,5,1,'Dropdown','','APDPartCategory')" onmouseover="EditableCellOver(this)" onmouseout="EditableCellOut(this)" onclick="this.focus()" nowrap="nowrap">
        <xsl:attribute name="style">
          <xsl:choose>
            <xsl:when test="@EnabledFlag = '0'">
              <xsl:text disable-output-escaping="yes">text-align:cnter</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text disable-output-escaping="yes">text-align:center; cursor:text</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      
        <xsl:choose>
          <xsl:when test="@APDPartCategoryAgreedCD = ''">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <SPAN style="cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
          		<xsl:value-of select="@APDPartCategoryAgreedCD"/>
              <SPAN style="display:none">-</SPAN>
              <SPAN style="display:none"><xsl:value-of select="/Root/Reference[@List='APDPartCategory' and @ReferenceID=current()/@APDPartCategoryAgreedCD]/@Name"/></SPAN>
            </SPAN>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>

    	<TD unselectable="on" class="GridTypeTD" width="4%" IDIndex="38" xmlAttribute="QuantityAgreed" dataType="Qty" ondblclick="EditableCellDblClick(this,5,1,'Qty')" onmouseover="EditableCellOver(this)" onmouseout="EditableCellOut(this)" onclick="this.focus()" nowrap="nowrap">
        <xsl:attribute name="style">
          <xsl:choose>
            <xsl:when test="@EnabledFlag = '0'">
              <xsl:text disable-output-escaping="yes">text-align:center</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text disable-output-escaping="yes">text-align:center; cursor:text</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>

        <xsl:value-of select="@QuantityAgreed"/>
    	</TD>

    	<TD unselectable="on" class="GridTypeTD" width="8%" IDIndex="39" xmlAttribute="PriceAgreed" dataType="Money" ondblclick="EditableCellDblClick(this,10,1,'Money')" onmouseover="EditableCellOver(this)" onmouseout="EditableCellOut(this)" onclick="this.focus()" nowrap="nowrap">
        <xsl:attribute name="style">
          <xsl:choose>
            <xsl:when test="@EnabledFlag = '0'">
              <xsl:text disable-output-escaping="yes">text-align:right</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text disable-output-escaping="yes">text-align:right; cursor:text</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:choose>
          <xsl:when test="@PriceIncludedAgreedFlag = 1">
            <xsl:text disable-output-escaping="yes">Incl.</xsl:text>
          </xsl:when>
          <xsl:otherwise>
        		<xsl:value-of select="@PriceAgreed"/>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>

    	<TD unselectable="on" class="GridTypeTD" width="5%" IDIndex="40" xmlAttribute="LaborHoursAgreed" dataType="Hours" ondblclick="EditableCellDblClick(this,5,1,'Hours')" onmouseover="EditableCellOver(this)" onmouseout="EditableCellOut(this)" onclick="this.focus()" nowrap="nowrap">
        <xsl:attribute name="style">
          <xsl:choose>
            <xsl:when test="@EnabledFlag = '0'">
              <xsl:text disable-output-escaping="yes">text-align:right</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text disable-output-escaping="yes">text-align:right; cursor:text</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:choose>
          <xsl:when test="@LaborHoursIncludedAgreedFlag = 1">
              <xsl:text disable-output-escaping="yes">Incl.</xsl:text>
          </xsl:when>
          <xsl:otherwise>
        		<xsl:value-of select="@LaborHoursAgreed"/>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>

    	<TD unselectable="on" class="GridTypeTD" width="5%" IDIndex="41" xmlAttribute="RefinishLaborHoursAgreed" dataType="Hours" ondblclick="EditableCellDblClick(this,5,1,'Hours')" onmouseover="EditableCellOver(this)" onmouseout="EditableCellOut(this)" onclick="this.focus()" nowrap="nowrap">
        <xsl:attribute name="style">
          <xsl:choose>
            <xsl:when test="@EnabledFlag = '0'">
              <xsl:text disable-output-escaping="yes">text-align:right</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text disable-output-escaping="yes">text-align:right; cursor:text</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:choose>
          <xsl:when test="@RefinishLaborIncludedAgreedFlag = 1">
            <xsl:text disable-output-escaping="yes">Incl.</xsl:text>
          </xsl:when>
          <xsl:otherwise>
        		<xsl:value-of select="@RefinishLaborHoursAgreed"/>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>

    	<TD unselectable="on" class="GridTypeTD" width="4%" IDIndex="42" xmlAttribute="LaborTypeAgreedCD" ondblclick="EditableCellDblClick(this,5,1,'Dropdown','','LaborType')" onmouseover="EditableCellOver(this)" onmouseout="EditableCellOut(this)" onclick="this.focus()" nowrap="nowrap">
        <xsl:attribute name="style">
          <xsl:choose>
            <xsl:when test="@EnabledFlag = '0'">
              <xsl:text disable-output-escaping="yes">text-align:center</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text disable-output-escaping="yes">text-align:center; cursor:text</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:choose>
          <xsl:when test="@LaborTypeAgreedCD = ''">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <SPAN style="cursor:hand" onMouseOver="popUpDynMsg(this.lastChild.innerText)" onMouseOut="popOut()">
          		<xsl:value-of select="@LaborTypeAgreedCD"/>
              <SPAN style="display:none">-</SPAN>
              <SPAN style="display:none"><xsl:value-of select="/Root/Reference[@List='LaborType' and @ReferenceID=current()/@LaborTypeAgreedCD]/@Name"/></SPAN>
            </SPAN>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>

    	<TD unselectable="on" class="GridTypeTD" xmlAttribute="LynxComment" ondblclick="EditableCellDblClick(this,26,3)" onmouseover="EditableCellOver(this)" onmouseout="EditableCellOut(this)" onclick="this.focus()">
        <xsl:attribute name="style">
          <xsl:choose>
            <xsl:when test="@EnabledFlag = '0'">
              <xsl:text disable-output-escaping="yes">text-align:left</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text disable-output-escaping="yes">text-align:left; cursor:text</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:choose>
          <xsl:when test="string-length(@LynxComment) &lt; 1">
            <DIV unselectable="on" class="autoflowDiv" style="width:30px; height:26px; overflow-x:hidden; overflow-y:hidden;">
        		  <xsl:value-of select="@LynxComment"/>
            </DIV>
          </xsl:when>
          <xsl:otherwise>
            <DIV unselectable="on" class="autoflowDiv" style="width:30px; height:26px; overflow-x:hidden; overflow-y:hidden;" onMouseOver="popUpDynMsg(this.innerText)" onMouseOut="popOut()">
        		  <xsl:value-of select="@LynxComment"/>
            </DIV>
          </xsl:otherwise>
        </xsl:choose>
    	</TD>

    	<TD unselectable="on" xmlAttribute="Comment" style="display:none;"><xsl:value-of select="@Comment"/></TD>
    	<TD unselectable="on" xmlAttribute="OperationCd" style="display:none;"><xsl:value-of select="@OperationCd"/></TD>
    	<TD unselectable="on" xmlAttribute="PartTypeOriginalCD" style="display:none;"><xsl:value-of select="@PartTypeOriginalCD"/></TD>
    	<TD unselectable="on" xmlAttribute="Description" style="display:none;"><xsl:value-of select="@Description"/></TD>
    	<TD unselectable="on" xmlAttribute="APDPartCategoryOriginalCD" style="display:none;"><xsl:value-of select="@APDPartCategoryOriginalCD"/></TD>
    	<TD unselectable="on" xmlAttribute="QuantityOriginal" style="display:none;"><xsl:value-of select="@QuantityOriginal"/></TD>
    	<TD unselectable="on" xmlAttribute="PriceIncludedOriginalFlag" style="display:none;"><xsl:value-of select="@PriceIncludedOriginalFlag"/></TD>
    	<TD unselectable="on" xmlAttribute="PriceOriginal" style="display:none;"><xsl:value-of select="@PriceOriginal"/></TD>
    	<TD unselectable="on" xmlAttribute="PriceChangedFlag" style="display:none;"><xsl:value-of select="@PriceChangedFlag"/></TD>
    	<TD unselectable="on" xmlAttribute="DBPriceOriginal" style="display:none;"><xsl:value-of select="@DBPriceOriginal"/></TD>
    	<TD unselectable="on" xmlAttribute="LaborHoursIncludedOriginalFlag" style="display:none;"><xsl:value-of select="@LaborHoursIncludedOriginalFlag"/></TD>
    	<TD unselectable="on" xmlAttribute="LaborHoursOriginal" style="display:none;"><xsl:value-of select="@LaborHoursOriginal"/></TD>
    	<TD unselectable="on" xmlAttribute="LaborHoursChangedFlag" style="display:none;"><xsl:value-of select="@LaborHoursChangedFlag"/></TD>
    	<TD unselectable="on" xmlAttribute="DBLaborHoursOriginal" style="display:none;"><xsl:value-of select="@DBLaborHoursOriginal"/></TD>
    	<TD unselectable="on" xmlAttribute="RefinishLaborIncludedOriginalFlag" style="display:none;"><xsl:value-of select="@RefinishLaborIncludedOriginalFlag"/></TD>
    	<TD unselectable="on" xmlAttribute="RefinishLaborHoursOriginal" style="display:none;"><xsl:value-of select="@RefinishLaborHoursOriginal"/></TD>
    	<TD unselectable="on" xmlAttribute="RefinishLaborHoursChangedFlag" style="display:none;"><xsl:value-of select="@RefinishLaborHoursChangedFlag"/></TD>
    	<TD unselectable="on" xmlAttribute="DBRefinishLaborHoursOriginal" style="display:none;"><xsl:value-of select="@DBRefinishLaborHoursOriginal"/></TD>
    	<TD unselectable="on" xmlAttribute="LaborTypeOriginalCD" style="display:none;"><xsl:value-of select="@LaborTypeOriginalCD"/></TD>
    	<TD unselectable="on" xmlAttribute="APDPartCategoryAgreedCD" style="display:none;"><xsl:value-of select="@APDPartCategoryAgreedCD"/></TD>
    	<TD unselectable="on" xmlAttribute="QuantityAgreed" style="display:none;"><xsl:value-of select="@QuantityAgreed"/></TD>
    	<TD unselectable="on" xmlAttribute="PriceAgreed" style="display:none;"><xsl:value-of select="@PriceAgreed"/></TD>
    	<TD unselectable="on" xmlAttribute="LaborHoursAgreed" style="display:none;"><xsl:value-of select="@LaborHoursAgreed"/></TD>
    	<TD unselectable="on" xmlAttribute="RefinishLaborHoursAgreed" style="display:none;"><xsl:value-of select="@RefinishLaborHoursAgreed"/></TD>
    	<TD unselectable="on" xmlAttribute="LaborTypeAgreedCD" style="display:none;"><xsl:value-of select="@LaborTypeAgreedCD"/></TD>
    	<TD unselectable="on" xmlAttribute="PartTypeAgreedCD" style="display:none;"><xsl:value-of select="@PartTypeAgreedCD"/></TD>
    	<TD unselectable="on" xmlAttribute="SysLastUpdatedDate" style="display:none;"><xsl:value-of select="@SysLastUpdatedDate"/></TD>
    	<TD unselectable="on" xmlAttribute="ManualLineFlag" style="display:none;"><xsl:value-of select="@ManualLineFlag"/></TD>
    	<TD unselectable="on" xmlAttribute="PriceIncludedAgreedFlag" style="display:none;"><xsl:value-of select="@PriceIncludedAgreedFlag"/></TD>
    	<TD unselectable="on" xmlAttribute="LaborHoursIncludedAgreedFlag" style="display:none;"><xsl:value-of select="@LaborHoursIncludedAgreedFlag"/></TD>
    	<TD unselectable="on" xmlAttribute="RefinishLaborIncludedAgreedFlag" style="display:none;"><xsl:value-of select="@RefinishLaborIncludedAgreedFlag"/></TD>
    	<TD unselectable="on" xmlAttribute="QuantityAgreed" style="display:none;"><xsl:value-of select="@QuantityAgreed"/></TD>
    	<TD unselectable="on" xmlAttribute="PriceAgreed" style="display:none;"><xsl:value-of select="@PriceAgreed"/></TD>
    	<TD unselectable="on" xmlAttribute="LaborHoursAgreed" style="display:none;"><xsl:value-of select="@LaborHoursAgreed"/></TD>
    	<TD unselectable="on" xmlAttribute="RefinishLaborHoursAgreed" style="display:none;"><xsl:value-of select="@RefinishLaborHoursAgreed"/></TD>
    	<TD unselectable="on" xmlAttribute="LaborTypeAgreedCD" style="display:none;"><xsl:value-of select="@LaborTypeAgreedCD"/></TD>

    </TR>

  </xsl:template>

  <!-- Gets the Remarks Info -->
  <xsl:template name="RemarksInfo" >

  	<TABLE unselectable="on" width="712" border="0" cellspacing="0" style="border:1px solid #808080;">
      <TR>
        <TD unselectable="on" valign="top">Remarks:</TD>
        <TD unselectable="on" valign="top" width="100%">
          <DIV unselectable="on" class="autoflowDiv" style="height:40px;">
            <xsl:value-of select="/Document/Estimate/@Remarks"/>
          </DIV>
        </TD>
      </TR>
    </TABLE>

  </xsl:template>



<!-- ************************************************************************** -->
<!-- **        Estimate Summary                                                 -->
<!-- ************************************************************************** -->

<!-- Gets the Estimate Summary Info -->
<xsl:template name="EstimateSummaryInfo" >

  <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px solid #CCCCCC">
    <TR unselectable="on" bgcolor="#EFEFEF">
      <TD unselectable="on" width="144"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" width="180" align="center" class="tdHeader">Original</TD>
      <TD unselectable="on" width="18"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  <xsl:choose>
    <xsl:when test="/Root/Document/@DetailEditableFlag = 0 and /Root/Document/@VANFlag = 1">
      <TD unselectable="on" width="180" align="center" class="tdHeader" style="background-color:#FF9966;" title="Data received from VAN does not match the Summary totals">Agreed</TD>
    </xsl:when>
    <xsl:otherwise>
      <TD unselectable="on" width="186" align="center" class="tdHeader">Agreed</TD>
    </xsl:otherwise>
  </xsl:choose>
      <TD unselectable="on" width="184"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <!-- <TD unselectable="on" width="10"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD> -->
      <!-- <TD unselectable="on" width="184" align="center" class="tdHeader">Comments</TD> -->
    </TR>
  </TABLE>

  <DIV id="divEstimateSummary" unselectable="on" style="position:absolute; width:714px; height:456px; overflow-y:auto;">

  <TABLE unselectable="on" id="tblEstimateSummary" border="0" cellspacing="0" cellpadding="1" style="display:none;">
    <TR unselectable="on">
      <TD unselectable="on"><img src="/images/spacer.gif" width="1" height="0" border="0" /></TD>
      <TD unselectable="on"><img src="/images/spacer.gif" width="1" height="0" border="0" /></TD>
      <TD unselectable="on"><img src="/images/spacer.gif" width="1" height="0" border="0" /></TD>
      <TD unselectable="on"><img src="/images/spacer.gif" width="1" height="0" border="0" /></TD>
      <TD unselectable="on"><img src="/images/spacer.gif" width="1" height="0" border="0" /></TD>
      <TD unselectable="on"><img src="/images/spacer.gif" width="1" height="0" border="0" /></TD>
      <TD unselectable="on"><img src="/images/spacer.gif" width="15" height="0" border="0" /></TD>
      <TD unselectable="on"><img src="/images/spacer.gif" width="1" height="0" border="0" /></TD>
      <TD unselectable="on"><img src="/images/spacer.gif" width="1" height="0" border="0" /></TD>
      <TD unselectable="on"><img src="/images/spacer.gif" width="1" height="0" border="0" /></TD>
      <TD unselectable="on"><img src="/images/spacer.gif" width="1" height="0" border="0" /></TD>
      <TD unselectable="on"><img src="/images/spacer.gif" width="6" height="0" border="0" /></TD>
      <TD unselectable="on"><img src="/images/spacer.gif" width="1" height="0" border="0" /></TD>
    </TR>

    <TR unselectable="on" class="ClaimHeader">
      <TD unselectable="on" colspan="2">Parts Costs</TD>
      <TD unselectable="on" class="ClaimSubHeader">Prcnt</TD>
      <TD unselectable="on" class="ClaimSubHeader">Unit Price</TD>
      <TD unselectable="on" class="ClaimSubHeader">Ext Price</TD>
      <TD unselectable="on" class="ClaimSubHeader">Tax</TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader">Prcnt</TD>
      <TD unselectable="on" class="ClaimSubHeader">Unit Price</TD>
      <TD unselectable="on" class="ClaimSubHeader">Ext Price</TD>
      <TD unselectable="on" class="ClaimSubHeader">Tax</TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader">Comments</TD>
    </TR>

<SPAN id="taxableTotals">

<SPAN id="taxableTotals1">
    <TR unselectable="on">
      <TD unselectable="on">
      </TD>
    </TR>

<xsl:for-each select="EstimateSummary[@Name = 'PartsTaxable' and @CategoryCD = 'PC']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">1</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>1</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow1</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Taxable</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_1',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_1',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" style="text-align:center;">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','1','1')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_1',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_1',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" style="text-align:center;">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','1','1')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_1',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_1',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_1',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_1',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_1',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_1',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_1',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_1',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_1',10,'TaxTypeCD',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'PartsNonTaxable' and @CategoryCD = 'PC']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">1</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>2</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow2</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Non-Taxable</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_2',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_2',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','2','1')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_2',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_2',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','2','1')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_2',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_2',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_2',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_2',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_2',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_2',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_2',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_2',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_2',10,'TaxTypeCD',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'PartsMarkup' and @CategoryCD = 'PC']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">2</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>3</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow3</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;w/ Markup</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalPct_3',4,'OriginalPct',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_3',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_3',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','3','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedPct_3',4,'AgreedPct',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_3',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_3',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','3','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_3',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_3',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_3',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_3',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_3',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_3',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_3',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidAgreedPct_3">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedPct"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_3">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedTaxableFlag_3">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedTaxableFlag"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'PartsDiscount' and @CategoryCD = 'PC']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">4</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>4</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow4</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;w/ Discount</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalPct_4',4,'OriginalPct',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_4',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_4',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','4','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedPct_4',4,'AgreedPct',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_4',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_4',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','4','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_4',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_4',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_4',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_4',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_4',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_4',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_4',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidAgreedPct_4">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedPct"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_4">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedTaxableFlag_4">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedTaxableFlag"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'PartsOtherTaxable' and @CategoryCD = 'PC']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">1</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>5</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow5</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Other (taxable)</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_5',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_5',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','5','1')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_5',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_5',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','5','1')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_5',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_5',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_5',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_5',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_5',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_5',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_5',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_5',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_5',10,'TaxTypeCD',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'PartsOtherNonTaxable' and @CategoryCD = 'PC']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">1</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>6</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow6</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Other (non-taxable)</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_6',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_6',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','6','1')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_6',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_6',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','6','1')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_6',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_6',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_6',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_6',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_6',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_6',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_6',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_6',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_6',10,'TaxTypeCD',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'PartsTotal' and @CategoryCD = 'TT']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">0</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>7</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow7</xsl:text></xsl:attribute>
      <TD unselectable="on" colspan="4" class="TotalsHeader"><xsl:attribute name="nowrap"/>Parts Total<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_7',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="4"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_7',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_7',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_7',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_7',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_7',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_7',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalUnitAmt_7',10,'OriginalUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedUnitAmt_7',10,'AgreedUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_7',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_7',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('Comments_7',10,'Comments',.,'','',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_7',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_7',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_7',10,'TaxTypeCD',.,'',6)"/>

        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTaxablePartsOriginal',10,'TaxablePartsOriginal',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTaxablePartsAgreed',10,'TaxablePartsAgreed',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>

</SPAN>

    <TR unselectable="on">
      <TD unselectable="on" colspan="13"><img src="/images/spacer.gif" width="1" height="6" border="0" /></TD>
    </TR>
    <TR unselectable="on" class="ClaimHeader">
      <TD unselectable="on" colspan="2">Supply Costs</TD>
      <TD unselectable="on" class="ClaimSubHeader">Hrs</TD>
      <TD unselectable="on" class="ClaimSubHeader">Unit Price</TD>
      <TD unselectable="on" class="ClaimSubHeader">Ext Price</TD>
      <TD unselectable="on" class="ClaimSubHeader">Tax</TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader">Hrs</TD>
      <TD unselectable="on" class="ClaimSubHeader">Unit Price</TD>
      <TD unselectable="on" class="ClaimSubHeader">Ext Price</TD>
      <TD unselectable="on" class="ClaimSubHeader">Tax</TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader">Comments</TD>
    </TR>

<SPAN id="taxableTotals2">
    <TR unselectable="on">
      <TD unselectable="on">
      </TD>
    </TR>

<xsl:for-each select="EstimateSummary[@Name = 'Body' and @CategoryCD = 'MC']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">3</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>8</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow8</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Body</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalHrs_8',4,'OriginalHrs',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_8',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_8',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','8','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedHrs_8',4,'AgreedHrs',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_8',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_8',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','8','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_8',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_8',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_8',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_8',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_8',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_8',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_8',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidAgreedHrs_8">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedHrs"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_8">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedTaxableFlag_8">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedTaxableFlag"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'Paint' and @CategoryCD = 'MC']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">3</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>9</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow9</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Paint</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalHrs_9',4,'OriginalHrs',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_9',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_9',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','9','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedHrs_9',4,'AgreedHrs',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_9',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_9',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','9','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_9',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_9',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_9',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_9',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_9',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_9',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_9',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidAgreedHrs_9">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedHrs"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_9">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedTaxableFlag_9">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedTaxableFlag"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'SuppliesTotal' and @CategoryCD = 'TT']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">0</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>10</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow10</xsl:text></xsl:attribute>
      <TD unselectable="on" colspan="4" class="TotalsHeader"><xsl:attribute name="nowrap"/>Supplies Total<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_10',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="4"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_10',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_10',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_10',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_10',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_10',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_10',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalUnitAmt_10',10,'OriginalUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedUnitAmt_10',10,'AgreedUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_10',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_10',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('Comments_10',10,'Comments',.,'','',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_10',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_10',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_10',10,'TaxTypeCD',.,'',6)"/>

        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTaxableSuppliesOriginal',10,'TaxableSuppliesOriginal',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTaxableSuppliesAgreed',10,'TaxableSuppliesAgreed',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'PartsAndMaterialTotal' and @CategoryCD = 'TT']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">0</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>11</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow11</xsl:text></xsl:attribute>
      <TD unselectable="on" colspan="4" class="boldtext" align="right"><xsl:attribute name="nowrap"/>Parts and Supplies Total<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_11',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="4"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_11',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_11',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_11',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_11',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_11',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_11',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalUnitAmt_11',10,'OriginalUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedUnitAmt_11',10,'AgreedUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_11',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_11',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('Comments_11',10,'Comments',.,'','',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_11',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_11',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_11',10,'TaxTypeCD',.,'',6)"/>

        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTaxablePartsAndMaterialsOriginal',10,'TaxablePartsAndMaterialsOriginal',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTaxablePartsAndMaterialsAgreed',10,'TaxablePartsAndMaterialsAgreed',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>

</SPAN>

    <TR unselectable="on">
      <TD unselectable="on" colspan="13"><img src="/images/spacer.gif" width="1" height="6" border="0" /></TD>
    </TR>
    <TR unselectable="on" class="ClaimHeader">
      <TD unselectable="on" colspan="2">Labor Costs</TD>
      <TD unselectable="on" class="ClaimSubHeader">Hrs</TD>
      <TD unselectable="on" class="ClaimSubHeader">Unit Price</TD>
      <TD unselectable="on" class="ClaimSubHeader">Ext Price</TD>
      <TD unselectable="on" class="ClaimSubHeader">Tax</TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader">Hrs</TD>
      <TD unselectable="on" class="ClaimSubHeader">Unit Price</TD>
      <TD unselectable="on" class="ClaimSubHeader">Ext Price</TD>
      <TD unselectable="on" class="ClaimSubHeader">Tax</TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader">Comments</TD>
    </TR>

<SPAN id="taxableTotals3">

    <TR unselectable="on">
      <TD unselectable="on">
      </TD>
    </TR>

<xsl:for-each select="EstimateSummary[@Name = 'Body' and @CategoryCD = 'LC']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">3</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>12</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow12</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Body</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalHrs_12',4,'OriginalHrs',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_12',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_12',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','12','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedHrs_12',4,'AgreedHrs',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_12',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_12',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','12','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_12',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_12',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_12',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_12',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_12',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_12',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_12',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidAgreedHrs_12">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedHrs"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_12">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedTaxableFlag_12">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedTaxableFlag"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'Mechanical' and @CategoryCD = 'LC']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">3</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>13</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow13</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Mechanical</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalHrs_13',4,'OriginalHrs',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_13',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_13',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','13','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedHrs_13',4,'AgreedHrs',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_13',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_13',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','13','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_13',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_13',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_13',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_13',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_13',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_13',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_13',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidAgreedHrs_13">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedHrs"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_13">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedTaxableFlag_13">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedTaxableFlag"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'Frame' and @CategoryCD = 'LC']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">3</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>14</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow14</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Frame</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalHrs_14',4,'OriginalHrs',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_14',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_14',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','14','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedHrs_14',4,'AgreedHrs',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_14',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_14',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','14','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_14',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_14',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_14',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_14',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_14',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_14',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_14',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidAgreedHrs_14">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedHrs"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_14">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedTaxableFlag_14">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedTaxableFlag"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'Paint' and @CategoryCD = 'LC']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">3</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>15</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow15</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Paint</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalHrs_15',4,'OriginalHrs',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_15',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_15',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','15','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedHrs_15',4,'AgreedHrs',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_15',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_15',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','15','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_15',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_15',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_15',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_15',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_15',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_15',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_15',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidAgreedHrs_15">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedHrs"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_15">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedTaxableFlag_15">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedTaxableFlag"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'Refinish' and @CategoryCD = 'LC']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">3</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>16</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow16</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Refinish</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalHrs_16',4,'OriginalHrs',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_16',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_16',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','16','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedHrs_16',4,'AgreedHrs',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_16',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_16',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','16','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_16',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_16',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_16',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_16',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_16',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_16',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_16',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidAgreedHrs_16">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedHrs"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_16">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedTaxableFlag_16">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedTaxableFlag"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'Other' and @CategoryCD = 'LC']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">1</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>17</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow17</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Other</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_17',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_17',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','17','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_17',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_17',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','17','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_17',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_17',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_17',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_17',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_17',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_17',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_17',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_17',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_17',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidAgreedHrs_17">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedHrs"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_17">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedTaxableFlag_17">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedTaxableFlag"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'LaborTotal' and @CategoryCD = 'TT']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">0</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>18</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow18</xsl:text></xsl:attribute>
      <TD unselectable="on" colspan="4" class="TotalsHeader"><xsl:attribute name="nowrap"/>Labor Total<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_18',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="4"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_18',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_18',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_18',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_18',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_18',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_18',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalUnitAmt_18',10,'OriginalUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedUnitAmt_18',10,'AgreedUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_18',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_18',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('Comments_18',10,'Comments',.,'','',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_18',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_18',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_18',10,'TaxTypeCD',.,'',6)"/>

        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTaxableLaborOriginal',10,'TaxableLaborOriginal',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTaxableLaborAgreed',10,'TaxableLaborAgreed',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>

</SPAN>

    <TR unselectable="on">
      <TD unselectable="on" colspan="13"><img src="/images/spacer.gif" width="1" height="6" border="0" /></TD>
    </TR>
    <TR unselectable="on" class="ClaimHeader">
      <TD unselectable="on" colspan="2">Additional Charges</TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader">Amount</TD>
      <TD unselectable="on" class="ClaimSubHeader">Ext Price</TD>
      <TD unselectable="on" class="ClaimSubHeader">Tax</TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader">Amount</TD>
      <TD unselectable="on" class="ClaimSubHeader">Ext Price</TD>
      <TD unselectable="on" class="ClaimSubHeader">Tax</TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader">Comments</TD>
    </TR>

<SPAN id="taxableTotals4">

    <TR unselectable="on">
      <TD unselectable="on">
      </TD>
    </TR>

<xsl:for-each select="AdditionalCharges" >
  <xsl:if test="position() = 1">
    <TR unselectable="on">
      <xsl:attribute name="calcType">1</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>19</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow19</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" colspan="2"><xsl:attribute name="nowrap"/>
        <TABLE border="0" cellspacin="0" cellpadding="0">
        <TR>
        	<TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Charges</TD>
        	<TD>
            <xsl:variable name="rtAddChg19" select="@EstimateSummaryTypeID"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('EstimateSummaryTypeID',2,'onSelectChange',number($rtAddChg19),1,6,/Root/Reference[@List='AdditionalCharges'],'Name','ReferenceID',70,'','','','',1,19)"/>
          </TD>
        </TR>
        </TABLE>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_19',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_19',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','19','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_19',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_19',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','19','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_19',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_19',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_19',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_19',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_19',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_19',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_19',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_19',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidEstimateSummaryTypeID_19">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Reference[@List='AdditionalCharges' and @ReferenceID=current()/@EstimateSummaryTypeID]/@Name"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_19">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedTaxableFlag_19">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedTaxableFlag"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
  </xsl:if>


  <xsl:if test="position() = 2">
    <TR unselectable="on">
      <xsl:attribute name="calcType">1</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>20</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow20</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" colspan="2"><xsl:attribute name="nowrap"/>
        <TABLE border="0" cellspacin="0" cellpadding="0">
        <TR>
        	<TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Charges</TD>
        	<TD>
            <xsl:variable name="rtAddChg20" select="@EstimateSummaryTypeID"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('EstimateSummaryTypeID',2,'onSelectChange',number($rtAddChg20),1,5,/Root/Reference[@List='AdditionalCharges'],'Name','ReferenceID',70,'','','','',1,20)"/>
          </TD>
        </TR>
        </TABLE>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_20',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_20',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','20','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_20',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_20',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','20','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_20',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_20',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_20',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_20',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_20',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_20',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_20',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_20',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidEstimateSummaryTypeID_20">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Reference[@List='AdditionalCharges' and @ReferenceID=current()/@EstimateSummaryTypeID]/@Name"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_20">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedTaxableFlag_20">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedTaxableFlag"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
  </xsl:if>


  <xsl:if test="position() = 3">
    <TR unselectable="on">
      <xsl:attribute name="calcType">1</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>21</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow21</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" colspan="2"><xsl:attribute name="nowrap"/>
        <TABLE border="0" cellspacin="0" cellpadding="0">
        <TR>
        	<TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Charges</TD>
        	<TD>
            <xsl:variable name="rtAddChg21" select="@EstimateSummaryTypeID"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('EstimateSummaryTypeID',2,'onSelectChange',number($rtAddChg21),1,4,/Root/Reference[@List='AdditionalCharges'],'Name','ReferenceID',70,'','','','',1,21)"/>
          </TD>
        </TR>
        </TABLE>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_21',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_21',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','21','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_21',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_21',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','21','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_21',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_21',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_21',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_21',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_21',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_21',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_21',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_21',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidEstimateSummaryTypeID_21">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Reference[@List='AdditionalCharges' and @ReferenceID=current()/@EstimateSummaryTypeID]/@Name"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_21">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedTaxableFlag_21">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedTaxableFlag"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
  </xsl:if>


  <xsl:if test="position() = 4">
    <TR unselectable="on">
      <xsl:attribute name="calcType">1</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>22</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow22</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" colspan="2"><xsl:attribute name="nowrap"/>
        <TABLE border="0" cellspacin="0" cellpadding="0">
        <TR>
        	<TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Charges</TD>
        	<TD>
            <xsl:variable name="rtAddChg22" select="@EstimateSummaryTypeID"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('EstimateSummaryTypeID',2,'onSelectChange',number($rtAddChg22),1,3,/Root/Reference[@List='AdditionalCharges'],'Name','ReferenceID',70,'','','','',1,22)"/>
          </TD>
        </TR>
        </TABLE>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_22',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_22',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('OriginalTaxableFlag',boolean(@OriginalTaxableFlag='1'),'1','0','22','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_22',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_22',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('AgreedTaxableFlag',boolean(@AgreedTaxableFlag='1'),'1','0','22','')"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" >
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_22',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_22',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_22',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_22',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_22',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_22',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_22',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_22',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidEstimateSummaryTypeID_22">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Reference[@List='AdditionalCharges' and @ReferenceID=current()/@EstimateSummaryTypeID]/@Name"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_22">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedTaxableFlag_22">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedTaxableFlag"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
  </xsl:if>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'AdditionalChargesTotal' and @CategoryCD = 'TT']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">0</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>23</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow23</xsl:text></xsl:attribute>
      <TD unselectable="on" colspan="4" class="TotalsHeader"><xsl:attribute name="nowrap"/>Additional Charges Total<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_23',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="4"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_23',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_23',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_23',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_23',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_23',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_23',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalUnitAmt_23',10,'OriginalUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedUnitAmt_23',10,'AgreedUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_23',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_23',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('Comments_23',10,'Comments',.,'','',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_23',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_23',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_23',10,'TaxTypeCD',.,'',6)"/>

        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTaxableAddChrgOriginal',10,'TaxableAddChrgOriginal',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTaxableAddChrgAgreed',10,'TaxableAddChrgAgreed',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>

</SPAN>

</SPAN>

    <TR unselectable="on">
      <TD unselectable="on" colspan="13"><img src="/images/spacer.gif" width="1" height="4" border="0" /></TD>
    </TR>

<SPAN id="nontaxableTotals">

<xsl:for-each select="EstimateSummary[@Name = 'SubTotal' and @CategoryCD = 'TT']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">0</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>24</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow24</xsl:text></xsl:attribute>
      <TD unselectable="on" colspan="4" class="boldtext" align="right"><xsl:attribute name="nowrap"/>Subtotal<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_24',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="4"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_24',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_24',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_24',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_24',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_24',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_24',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalUnitAmt_24',10,'OriginalUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedUnitAmt_24',10,'AgreedUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_24',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_24',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('Comments_24',10,'Comments',.,'','',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_24',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_24',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_24',10,'TaxTypeCD',.,'',6)"/>

        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTaxableSubTotalOriginal',10,'TaxableSubTotalOriginal',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtTaxableSubTotalAgreed',10,'TaxableSubTotalAgreed',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>

    <TR unselectable="on">
      <TD unselectable="on" colspan="13"><img src="/images/spacer.gif" width="1" height="6" border="0" /></TD>
    </TR>
    <TR unselectable="on" class="ClaimHeader">
      <TD unselectable="on" colspan="2">Taxes</TD>
      <TD unselectable="on" class="ClaimSubHeader">Prcnt</TD>
      <TD unselectable="on" class="ClaimSubHeader">Applied Total</TD>
      <TD unselectable="on" class="ClaimSubHeader">Taxes</TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader">Prcnt</TD>
      <TD unselectable="on" class="ClaimSubHeader">Applied Total</TD>
      <TD unselectable="on" class="ClaimSubHeader">Taxes</TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader">Comments</TD>
    </TR>

<xsl:for-each select="Taxes" >
  <xsl:if test="position() = 1">
    <TR unselectable="on">
      <xsl:attribute name="calcType">2</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>25</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow25</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:variable name="rtTaxCD25" select="@EstimateSummaryTypeID"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('EstimateSummaryTypeID',2,'onSelectChange',number($rtTaxCD25),1,6,/Root/Reference[@List='Taxes'],'Name','ReferenceID',62,'','','','',1,25)"/>
        <img src="/images/spacer.gif" width="4" height="1" border="0" />
        <xsl:variable name="rtTaxType25" select="@TaxTypeCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('TaxTypeCD',2,'onTaxTypeChange',string($rtTaxType25),1,6,/Root/Reference[@List='TaxType'],'Name','ReferenceID',64,'','','','',1,25)"/>
        <img src="/images/spacer.gif" width="4" height="1" border="0" />
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalPct_25',4,'OriginalPct',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_25',8,'OriginalUnitAmt',.,'onChange=updField(this);',11,'-1')"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_25',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedPct_25',4,'AgreedPct',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_25',8,'AgreedUnitAmt',.,'onChange=updField(this);',11,'-1')"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_25',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_25',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_25',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_25',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_25',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_25',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_25',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_25',10,'EstimateSummaryID',.,'',6)"/>

        <INPUT type="hidden" id="hidEstimateSummaryTypeID_25">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Reference[@List='Taxes' and @ReferenceID=current()/@EstimateSummaryTypeID]/@Name"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidTaxTypeCD_25">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Reference[@List='TaxType' and @ReferenceID=current()/@TaxTypeCD]/@Name"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedPct_25">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedPct"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_25">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedExtendedAmt_25">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedExtendedAmt"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
  </xsl:if>

  <xsl:if test="position() = 2">
    <TR unselectable="on">
      <xsl:attribute name="calcType">2</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>26</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow26</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:variable name="rtTaxCD26" select="@EstimateSummaryTypeID"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('EstimateSummaryTypeID',2,'onSelectChange',number($rtTaxCD26),1,5,/Root/Reference[@List='Taxes'],'Name','ReferenceID',62,'','','','',1,26)"/>
        <img src="/images/spacer.gif" width="4" height="1" border="0" />
        <xsl:variable name="rtTaxType26" select="@TaxTypeCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('TaxTypeCD',2,'onTaxTypeChange',string($rtTaxType26),1,5,/Root/Reference[@List='TaxType'],'Name','ReferenceID',64,'','','','',1,26)"/>
        <img src="/images/spacer.gif" width="4" height="1" border="0" />
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalPct_26',4,'OriginalPct',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_26',8,'OriginalUnitAmt',.,'onChange=updField(this);',11,'-1')"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_26',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedPct_26',4,'AgreedPct',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_26',8,'AgreedUnitAmt',.,'onChange=updField(this);',11,'-1')"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_26',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_26',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_26',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_26',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_26',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_26',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_26',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_26',10,'EstimateSummaryID',.,'',6)"/>

        <INPUT type="hidden" id="EstimateSummaryTypeID_26">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Reference[@List='Taxes' and @ReferenceID=current()/@EstimateSummaryTypeID]/@Name"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="TaxTypeCD_26">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Reference[@List='TaxType' and @ReferenceID=current()/@TaxTypeCD]/@Name"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedPct_26">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedPct"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_26">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedExtendedAmt_26">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedExtendedAmt"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
  </xsl:if>

  <xsl:if test="position() = 3">
    <TR unselectable="on">
      <xsl:attribute name="calcType">2</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>27</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow27</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:variable name="rtTaxCD27" select="@EstimateSummaryTypeID"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('EstimateSummaryTypeID',2,'onSelectChange',number($rtTaxCD27),1,4,/Root/Reference[@List='Taxes'],'Name','ReferenceID',62,'','','','',1,27)"/>
        <img src="/images/spacer.gif" width="4" height="1" border="0" />
        <xsl:variable name="rtTaxType27" select="@TaxTypeCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('TaxTypeCD',2,'onTaxTypeChange',string($rtTaxType27),1,4,/Root/Reference[@List='TaxType'],'Name','ReferenceID',64,'','','','',1,27)"/>
        <img src="/images/spacer.gif" width="4" height="1" border="0" />
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalPct_27',4,'OriginalPct',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_27',8,'OriginalUnitAmt',.,'onChange=updField(this);',11,'-1')"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_27',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedPct_27',4,'AgreedPct',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_27',8,'AgreedUnitAmt',.,'onChange=updField(this);',11,'-1')"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_27',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_27',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_27',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_27',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_27',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_27',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_27',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_27',10,'EstimateSummaryID',.,'',6)"/>

        <INPUT type="hidden" id="EstimateSummaryTypeID_27">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Reference[@List='Taxes' and @ReferenceID=current()/@EstimateSummaryTypeID]/@Name"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="TaxTypeCD_27">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Reference[@List='TaxType' and @ReferenceID=current()/@TaxTypeCD]/@Name"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedPct_27">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedPct"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_27">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedExtendedAmt_27">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedExtendedAmt"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
  </xsl:if>

  <xsl:if test="position() = 4">
    <TR unselectable="on">
      <xsl:attribute name="calcType">2</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>28</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow28</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <xsl:variable name="rtTaxCD28" select="@EstimateSummaryTypeID"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('EstimateSummaryTypeID',2,'onSelectChange',number($rtTaxCD28),1,3,/Root/Reference[@List='Taxes'],'Name','ReferenceID',62,'','','','',1,28)"/>
        <img src="/images/spacer.gif" width="4" height="1" border="0" />
        <xsl:variable name="rtTaxType28" select="@TaxTypeCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('TaxTypeCD',2,'onTaxTypeChange',string($rtTaxType28),1,3,/Root/Reference[@List='TaxType'],'Name','ReferenceID',64,'','','','',1,28)"/>
        <img src="/images/spacer.gif" width="4" height="1" border="0" />
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalPct_28',4,'OriginalPct',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_28',8,'OriginalUnitAmt',.,'onChange=updField(this);',11,'-1')"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_28',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedPct_28',4,'AgreedPct',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_28',8,'AgreedUnitAmt',.,'onChange=updField(this);',11,'-1')"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_28',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_28',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_28',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_28',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_28',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_28',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_28',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_28',10,'EstimateSummaryID',.,'',6)"/>

        <INPUT type="hidden" id="EstimateSummaryTypeID_28">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Reference[@List='Taxes' and @ReferenceID=current()/@EstimateSummaryTypeID]/@Name"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="TaxTypeCD_28">
          <xsl:attribute name="value">
            <xsl:value-of select="/Root/Reference[@List='TaxType' and @ReferenceID=current()/@TaxTypeCD]/@Name"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedPct_28">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedPct"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedUnitAmt_28">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>
        <INPUT type="hidden" id="hidAgreedExtendedAmt_28">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedExtendedAmt"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
  </xsl:if>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'TaxTotal' and @CategoryCD = 'TT']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">0</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>29</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow29</xsl:text></xsl:attribute>
      <TD unselectable="on" colspan="4" class="TotalsHeader"><xsl:attribute name="nowrap"/>Taxes Total<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_29',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="4"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_29',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_29',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_29',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_29',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_29',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_29',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalUnitAmt_29',10,'OriginalUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedUnitAmt_29',10,'AgreedUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_29',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_29',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('Comments_29',10,'Comments',.,'','',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_29',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_29',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_29',10,'TaxTypeCD',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>

    <TR unselectable="on">
      <TD unselectable="on" colspan="13"><img src="/images/spacer.gif" width="1" height="6" border="0" /></TD>
    </TR>
<!-- 
    <TR unselectable="on">
      <TD unselectable="on" colspan="13">
        <TABLE unselectable="on" border="0" width="100%" cellspacing="0" cellpadding="0">
          <TR unselectable="on">
            <TD unselectable="on" class="ClaimHeader">Contract Price</TD>
          </TR>
        </TABLE>
      </TD>
    </TR>
 -->
<!-- 
<xsl:for-each select="EstimateSummary[@Name = 'ContractPrice' and @CategoryCD = 'CP']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">1</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>30</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow30</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Total</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_30',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_30',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_30',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_30',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_30',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_30',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_30',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_30',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_30',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_30',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_30',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_30',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_30',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_30',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_30',10,'TaxTypeCD',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>
 -->
<!-- 
    <TR unselectable="on">
      <TD unselectable="on" colspan="13"><img src="/images/spacer.gif" width="1" height="4" border="0" /></TD>
    </TR>
 -->
<xsl:for-each select="EstimateSummary[@Name = 'RepairTotal' and @CategoryCD = 'TT']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">0</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>31</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow31</xsl:text></xsl:attribute>
      <TD unselectable="on" colspan="4" class="boldtext" align="right"><xsl:attribute name="nowrap"/>Total Cost of Repair<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_31',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="4"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_31',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_31',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_31',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_31',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_31',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_31',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalUnitAmt_31',10,'OriginalUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedUnitAmt_31',10,'AgreedUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_31',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_31',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('Comments_31',10,'Comments',.,'','',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_31',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_31',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_31',10,'TaxTypeCD',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>

    <TR unselectable="on">
      <TD unselectable="on" colspan="13"><img src="/images/spacer.gif" width="1" height="6" border="0" /></TD>
    </TR>
    <TR unselectable="on" class="ClaimHeader">
      <TD unselectable="on" colspan="2">Adjustments</TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader">Amount</TD>
      <TD unselectable="on" class="ClaimSubHeader">Ext Price</TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader">Amount</TD>
      <TD unselectable="on" class="ClaimSubHeader">Ext Price</TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader"></TD>
      <TD unselectable="on" class="ClaimSubHeader">Comments</TD>
    </TR>

<xsl:for-each select="EstimateSummary[@Name = 'Deductible' and @CategoryCD = 'AJ']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">1</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>32</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow32</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Deductible</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_32',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_32',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_32',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_32',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_32',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_32',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_32',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_32',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_32',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_32',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_32',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_32',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_32',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_32',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_32',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidAgreedUnitAmt_32">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'AppearanceAllowance' and @CategoryCD = 'AJ']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">1</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>33</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow33</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <!-- <TD unselectable="on" colspan="2"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Appearance/Allowance</TD> -->
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Appearance/Allowance</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_33',8,'OriginalUnitAmt',.,'onBlur=chkNegative(this);updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_33',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_33',8,'AgreedUnitAmt',.,'onBlur=chkNegative(this);updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_33',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_33',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_33',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_33',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_33',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_33',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_33',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_33',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_33',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_33',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_33',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_33',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidAgreedUnitAmt_33">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'Betterment' and @CategoryCD = 'AJ']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">1</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>34</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow34</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Betterment</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_34',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_34',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_34',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_34',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_34',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_34',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_34',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_34',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_34',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_34',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_34',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_34',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_34',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_34',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_34',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidAgreedUnitAmt_34">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'Other' and @CategoryCD = 'AJ']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">1</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>38</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow38</xsl:text></xsl:attribute>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Other Adjustments</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalUnitAmt_38',8,'OriginalUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_38',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedUnitAmt_38',8,'AgreedUnitAmt',.,'onChange=updField(this);',8)"/>
      </TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_38',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtComments_38',28,'Comments',.,'onChange=updField(this); OnDblClick=showComments(this);')"/>
      </TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_38',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_38',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_38',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_38',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_38',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_38',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_38',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_38',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_38',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_38',10,'TaxTypeCD',.,'',6)"/>

        <INPUT type="hidden" id="hidAgreedUnitAmt_38">
          <xsl:attribute name="value">
            <xsl:value-of select="@AgreedUnitAmt"/>
          </xsl:attribute>
        </INPUT>

      </TD>
    </TR>
</xsl:for-each>

<xsl:for-each select="EstimateSummary[@Name = 'AdjustmentTotal' and @CategoryCD = 'TT']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">0</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>35</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow35</xsl:text></xsl:attribute>
      <TD unselectable="on" colspan="4" class="TotalsHeader"><xsl:attribute name="nowrap"/>Adjustments Total<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_35',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="4"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_35',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_35',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_35',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_35',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_35',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_35',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalUnitAmt_35',10,'OriginalUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedUnitAmt_35',10,'AgreedUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_35',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_35',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('Comments_35',10,'Comments',.,'','',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_35',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_35',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_35',10,'TaxTypeCD',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>

</SPAN>

    <TR unselectable="on">
      <TD unselectable="on" colspan="13"><img src="/images/spacer.gif" width="1" height="4" border="0" /></TD>
    </TR>

<xsl:for-each select="EstimateSummary[@Name = 'NetTotal' and @CategoryCD = 'TT']" >
    <TR unselectable="on">
      <xsl:attribute name="calcType">0</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>36</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow36</xsl:text></xsl:attribute>
      <TD unselectable="on" colspan="4" class="boldtext" align="right"><xsl:attribute name="nowrap"/>Net Total<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_36',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="4"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_36',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_36',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_36',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_36',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_36',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_36',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalUnitAmt_36',10,'OriginalUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedUnitAmt_36',10,'AgreedUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_36',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_36',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('Comments_36',10,'Comments',.,'','',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_36',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_36',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_36',10,'TaxTypeCD',.,'',6)"/>
      </TD>
    </TR>
</xsl:for-each>


<xsl:for-each select="EstimateSummary[@Name = 'Variance' and @CategoryCD = 'VA']" >
  <xsl:if test="@OriginalExtendedAmt != 0">
    <TR unselectable="on">
      <xsl:attribute name="calcType">0</xsl:attribute>
      <xsl:attribute name="rowID"><xsl:text>37</xsl:text></xsl:attribute>
      <xsl:attribute name="id"><xsl:text>tblrow37</xsl:text></xsl:attribute>
      <TD unselectable="on" colspan="4" class="boldtext" align="right" style="color:red"><xsl:attribute name="nowrap"/>Variance<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtOriginalExtendedAmt_37',8,'OriginalExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="4"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on">
        <img src="/images/2px_Ltgray.gif" width="100%" height="1" border="0" /><br />
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAgreedExtendedAmt_37',8,'AgreedExtendedAmt',.,'',11)"/>
      </TD>
      <TD unselectable="on" colspan="3"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD style="display:none">
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtSysLastUpdatedDate_37',10,'SysLastUpdatedDate',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalPct_37',10,'OriginalPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedPct_37',10,'AgreedPct',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalHrs_37',10,'OriginalHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedHrs_37',10,'AgreedHrs',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalUnitAmt_37',10,'OriginalUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedUnitAmt_37',10,'AgreedUnitAmt',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OriginalTaxableFlag_37',10,'OriginalTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AgreedTaxableFlag_37',10,'AgreedTaxableFlag',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('Comments_37',10,'Comments',.,'','',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryID_37',10,'EstimateSummaryID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('EstimateSummaryTypeID_37',10,'EstimateSummaryTypeID',.,'',6)"/>
        <xsl:value-of disable-output-escaping="yes" select="js:InputBox('TaxTypeCD_37',10,'TaxTypeCD',.,'',6)"/>
      </TD>
    </TR>
  </xsl:if>
</xsl:for-each>

  </TABLE>
  </DIV>

</xsl:template>

</xsl:stylesheet>
