<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="PMDAuditForm">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<!-- NONE -->

<xsl:template match="/Root">

  <!-- Initialize the session manager -->

	<xsl:variable name="SupervisorForm" select="@SupervisorForm"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDReinspectFormGetDetailXML,PMDAuditForm.xsl,471,0  -->

<HEAD>
  <TITLE>Reinspection</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>


<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">

<![CDATA[

function initPage(){
	return;
}

]]>

</SCRIPT>


</HEAD>

<BODY class="bodyAPDSub" unselectable="on" onLoad="initPage();" onunload="CheckDirty()" bgcolor="#FFFAEB" style="margin-right:none">


<table>
	<tr>
		<td unselectable="on" class="labelColumn" nowrap="nowrap">Customer:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td unselectable="on" width="120"></td>
		<td unselectable="on" class="labelColumn" nowrap="nowrap">Claim #:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td unselectable="on" width="120"></td>
	</tr>
	<tr>
		<td unselectable="on" class="labelColumn" nowrap="nowrap">Vehicle YMM:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td unselectable="on" width="120"></td>
	</tr>
	<tr>
		<td unselectable="on" class="labelColumn" nowrap="nowrap">Shop Name:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td unselectable="on" width="120"></td>
	</tr>
	<tr>
		<td unselectable="on" class="labelColumn" nowrap="nowrap">Repair CSR:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
		<td unselectable="on" width="120"></td>
	</tr>
</table>


<table>
  <tr>
    <td>
	  <table>
		<tr>
		  <td unselectable="on" class="labelColumn" nowrap="nowrap" align="center">Repair Cycle Time Service Audit:</td>
		</tr>
	  </table>
	</td>
  </tr>
  <tr>
	<td>
	  <table>
	    <tr>
		  <td unselectable="on" class="labelColumn" nowrap="nowrap">Assignment Date:</td>
		</tr>
	  </table>
	</td>
  </tr>
</table>


</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>
