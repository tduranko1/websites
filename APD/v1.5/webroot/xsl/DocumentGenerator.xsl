<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:js="urn:the-xml-files:xslt"
  xmlns:user="http://mycompany.com/mynamespace">

<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:import href="msxsl/msxsl-function-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<!-- Permissions params - names must end in 'CRUD' -->
<!-- <xsl:param name="DocumentCRUD" select="Document"/> -->

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="InsCoId"/>
<xsl:param name="LynxId"/>
<xsl:param name="UserId"/>
<xsl:param name="WindowID"/>
<xsl:param name="SrcAppCd"/>

<xsl:template match="/Root">

  <html>
    <head>
      <title>Document Generator</title>

      <link rel="stylesheet" href="/css/apdMain.css" type="text/css"/>

      <script language="JavaScript" src="/js/formvalid.js"></script>
	  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
	  <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,15);
		  event.returnValue=false;
		  };	
 	  </script>
      <script language="JavaScript">

        // Page Initialize
        function PageInit()
        {
        }

        function OnTemplateSel()
        {
          // TODO - Radio button selection logic.
          // TODO - Select default attachments upon template change.
        }

        function ToggleDocSel( oRow )
        {
          var oSel = oRow.cells[0].firstChild;
          oSel.checked = !oSel.checked;
        }

        function SelectTemplate( sName, bAttach )
        {
          theForm.Template.value = sName;

          // Disable or enable the attachments.
          var nIdx = 1;
          try {
            while ( true )
            {
              var oSel = document.getElementById( "chkSel" + nIdx );
              oSel.disabled = !bAttach;
              nIdx = nIdx + 1;
            }
          } catch (e) { /* alert( sDocs + "  " + e.message ); */ }
        }

        function Cancel()
        {
          theForm.theAction.value = "Cancel";
          theForm.submit();
        }

        function Generate()
        {
          // TODO - validate that a template is selected.
          // TODO - validate any required attachment rules.

          // Get a comma separated list of attachment document IDs.
          var nIdx = 1, sDocs = "", nEstimates = 0;
          try {
            while ( true )
            {
              var oSel = document.getElementById( "chkSel" + nIdx );
              if ( oSel.checked )
              {
                sDocs = sDocs + oSel.DocumentID + ",";
                nEstimates = parseInt(nEstimates) + parseInt(oSel.EstimateTypeFlag);
              }
              nIdx = nIdx + 1;
            }
          } catch (e) { /* alert( sDocs + "  " + e.message ); */ }

          // Force at least one attachment for electronic documents.
          if ( theForm.Template.value == "FolderUpload" &amp;&amp; sDocs == "" )
            ClientWarning( "You must select at least one attachment for this document template." );

          // Set a maximum attachment count.  This is currently limited by the varchar
          // we store the list in in the database.  This limit may be removed at a future
          // date when we develop a real document management system.  Until then this limit
          // will rarely get surpassed so Im not going to worry about it.
          else if ( sDocs.length &gt; 245 )
            ClientWarning( "You cannot select more than 30 attachments at this time.  Please split your attachments up across multiple documents." );

          // Otherwise, generate the document.
          else
          {
            // Strip off trailing comma.
            if ( sDocs.length &gt; 0 )
              sDocs = sDocs.substr( 0, sDocs.length - 1 );

            theForm.Attach.value = sDocs;
            theForm.theAction.value = "Generate";

            // Disable the buttons now that one has been clicked.
            theForm.btnCancel.disabled = true;
            theForm.btnGenerate.disabled = true;

            divProgress.style.visibility = "visible";

            theForm.submit();
          }
        }

      </script>

      <style type="text/css">
        tr {
          border : 1;
          text-align : center;
        }

        td {
          border : 1;
          text-align : center;
        }

        td.tdHead {
          border-color : #000000 #999999 #999999 #000000;
          border-style : solid;
          border-top-width : 0px;
          border-right-width : 1px;
          border-bottom-width : 1px;
          border-left-width : 0px;
          color : #000066;
          background-color : #CCCCCC;
          font-family : Verdana, Arial, Helvetica, sans-serif;
          font-size : 11px;
          font-weight : bold;
          text-align : center;
          vertical-align : middle;
          white-space : wrap;
        }
      </style>

    </head>
    <body unselectable="on" onLoad="PageInit();" scroll="no"
      leftmargin="15" topmargin="15" bottommargin="15" rightmargin="15">

      <div id="divTables" style="width:100%; height:100%; z-index:2;">

        <div style="width:100%;">
          <br/>
          <h3>Select a Document Template</h3>
        </div>

        <div style="width:100%; height:23%; overflow-y:scroll; border:thin solid #CCCCCC;">
          <xsl:call-template name="DocTemplateListTableHeader"/>
        </div>

        <div style="width:100% z-index:2;">
          <br/>
          <h3>Select Any Attachments</h3>
        </div>

        <div style="width:100%; height:46%; overflow-y:scroll; border:thin solid #CCCCCC;">
          <xsl:call-template name="DocumentListTableHeader"/>
        </div>

        <div style="width:100%; height:20px;">
          <br/>
          <form name="theForm" method="post" action="DocumentGenerator.asp" target="_self">

            <input type="hidden" name="theAction" value="Cancel"/>

            <input type="hidden" name="LynxId">
              <xsl:attribute name="value"><xsl:value-of select="$LynxId"/></xsl:attribute>
            </input>

            <input type="hidden" name="UserId">
              <xsl:attribute name="value"><xsl:value-of select="$UserId"/></xsl:attribute>
            </input>

            <input type="hidden" name="WindowID">
              <xsl:attribute name="value"><xsl:value-of select="$WindowID"/></xsl:attribute>
            </input>

            <input type="hidden" name="InsCoId">
              <xsl:attribute name="value"><xsl:value-of select="$InsCoId"/></xsl:attribute>
            </input>

            <input type="hidden" name="SrcAppCd">
              <xsl:attribute name="value"><xsl:value-of select="$SrcAppCd"/></xsl:attribute>
            </input>

            <input type="hidden" name="Template">
              <xsl:attribute name="value">DeskAuditStatusReport</xsl:attribute>
            </input>

            <input type="hidden" name="Attach" value=""/>

            <input type="button" class="formbutton" name="btnCancel" value="Cancel" onClick="Cancel()"/>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;

            <input type="button" class="formbutton" name="btnGenerate" value="Generate" onClick="Generate()">
              <!--<xsl:if test="$SrcAppCd!='SG'">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>-->
            </input>
          </form>
        </div>
      </div>

      <div id="divProgress" align="center" style="position:absolute; z-index:4; top:210px; left:310px; height:80px; width:100px; background-color:#ffffff; visibility:hidden;">
        <table width="100%" height="100%" border="2" cellpadding="10">
          <tr><td HEIGHT="100%">
            <font COLOR="#8D5018"><strong>Generating documents.<br/>This may take a few moments.<br/>Please be patient.</strong></font>
          </td></tr>
          <tr><td>
            <img src="images\progress.gif"/>
          </td></tr>
        </table>
      </div>

    </body>
  </html>

</xsl:template>

<!--
    Creates a document template list table "Header".
 -->
<xsl:template name="DocTemplateListTableHeader">

  <table height="1" width="100%" unselectable="on" border="0" cellspacing="0" cellpadding="2" style="border:1px solid #CCCCCC">

    <col width="5%" />
    <col width="80%"/>
    <col width="15%"/>

    <tr unselectable="on" bgcolor="#EFEFEF" style="height:20px">
      <td unselectable="on" class="tdHead">Sel</td>
      <td unselectable="on" class="tdHead">Template</td>
      <td unselectable="on" class="tdHead">Type</td>
    </tr>

    <!-- TODO - add real document templates as passed from the database. -->

    <xsl:call-template name="DocTemplate"/>

  </table>


</xsl:template>

<!--
    Adds a single document template row to a document template table.
 -->
<xsl:template name="DocTemplate">

  <!-- TODO - real template list generation other than this hardcoded crap. -->

  <!-- Desk Audit Status Report -->
  <tr unselectable="on" onclick="">
    <xsl:attribute name="bgColor">
      <xsl:value-of select="user:chooseBackgroundColor(1,'#ffffff','#fff7e5')"/>
    </xsl:attribute>

    <td unselectable="on">
      <input type="radio" name="rdbTemplate" onclick="SelectTemplate( 'DeskAuditStatusReport', false )" checked=""/>
    </td>
    <td unselectable="on">Desk Audit Status Report</td>
    <td unselectable="on">MS Word</td>
  </tr>

  <!-- Desk Audit Status Supplement -->
  <tr unselectable="on" onclick="">
    <xsl:attribute name="bgColor">
      <xsl:value-of select="user:chooseBackgroundColor(2,'#ffffff','#fff7e5')"/>
    </xsl:attribute>

    <td unselectable="on">
      <input type="radio" name="rdbTemplate" onclick="SelectTemplate( 'DeskAuditStatusSupplement', false )" />
    </td>
    <td unselectable="on">Desk Audit Status Supplement</td>
    <td unselectable="on">MS Word</td>
  </tr>

  <!-- Estimate Audit Summary -->
  <tr unselectable="on" onclick="">
    <xsl:attribute name="bgColor">
      <xsl:value-of select="user:chooseBackgroundColor(3,'#ffffff','#fff7e5')"/>
    </xsl:attribute>

    <td unselectable="on">
      <input type="radio" name="rdbTemplate" onclick="SelectTemplate( 'EstimateAuditSummary', false )" />
    </td>
    <td unselectable="on">Estimate Audit Summary</td>
    <td unselectable="on">MS Word</td>
  </tr>

  <!-- Document Upload to Carrier -->
  <!-- 07May2013 - TVD - Removed Scene uploads
  <xsl:if test="$SrcAppCd='SG'">
    <tr unselectable="on" onclick="">
      <xsl:attribute name="bgColor">
        <xsl:value-of select="user:chooseBackgroundColor(4,'#ffffff','#fff7e5')"/>
      </xsl:attribute>

      <td unselectable="on">
        <input type="radio" name="rdbTemplate" onclick="SelectTemplate( 'FolderUpload', true )" />
      </td>
      <td unselectable="on">Document Upload to Carrier</td>
      <td unselectable="on">Electronic</td>
    </tr>
  </xsl:if>
  -->
  
</xsl:template>

<!--
    Creates a document list table "Header"
 -->
<xsl:template name="DocumentListTableHeader">

  <table width="100%" unselectable="on" cellspacing="0" cellpadding="2" style="border:1px solid #CCCCCC;table-layout:fixed">
    <colgroup>
      <col width="4%" style="white-space:nowrap;"/>
      <col width="7%" />
      <col width="10%"/> 
  	  <col width="6%"/>
  	  <col width="6%"/>
      <col width="10%"/>
      <col width="6%" />
      <col width="7%"/>
      <col width="*"/>
      <col width="10%"/>
    </colgroup>

    <tr unselectable="on" bgcolor="#EFEFEF" style="height:32px">
      <td unselectable="on" class="tdHead">Sel</td>
      <td unselectable="on" class="tdHead">R/S</td>
      <td unselectable="on" class="tdHead">Pertains<br/>To</td>
	    <td unselectable="on" class="tdHead">Final<br/>Est</td>
	    <td unselectable="on" class="tdHead">Dir-to<br/>Pay</td>
      <td unselectable="on" class="tdHead">Type</td>
      <td unselectable="on" class="tdHead">Supp<br/>Seq<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;#</td>
      <td unselectable="on" class="tdHead">Source</td>
      <td unselectable="on" class="tdHead">Received By</td>
      <td unselectable="on" class="tdHead">Received Date</td>
    </tr>

    <xsl:choose>
      <xsl:when test="count(Document[@DocumentID!=0]) = 0">
        <tr unselectable="on" bgColor="#ffffff">
          <td unselectable="on">
            <input type="checkbox" name="chkSel" value="0" onclick="" disabled="true"/>
          </td>
          <td colspan="7" unselectable="on">
            <strong><font color="Red">No documents available for this claim for attachment.</font></strong>
          </td>
      </tr>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="Document[@DocumentID!=0]"/>
      </xsl:otherwise>
    </xsl:choose>

  </table>

</xsl:template>

<!--
    Adds a single document row to a document table
 -->
<xsl:template match="Document">

  <xsl:variable name="DocumentTypeID">
    <xsl:value-of select="@DocumentTypeID"/>
  </xsl:variable>

  <tr unselectable="on">

    <xsl:attribute name="bgColor">
      <xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/>
    </xsl:attribute>

    <td name="tdSel" unselectable="on">
	  <xsl:choose>
	  <xsl:when test="@SendToCarrierFlag='0'">
		   <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
	  </xsl:when>
	  <xsl:otherwise>
        <img src="/images/arrowup.gif" alt="" height="15"  width="10" border="0"/>
	  </xsl:otherwise>
	 </xsl:choose>
	 
      <input type="checkbox" name="chkSel" value="1" disabled="true">
        <xsl:attribute name="name"><xsl:value-of select="concat('chkSel',position())"/></xsl:attribute>
        <xsl:attribute name="DocumentID"><xsl:value-of select="@DocumentID"/></xsl:attribute>
        <xsl:attribute name="EstimateTypeFlag"><xsl:value-of select="//Reference[@List='DocumentType'][@ReferenceID=$DocumentTypeID]/@EstimateTypeFlag"/></xsl:attribute>
      </input>
    </td>

    <td unselectable="on">
      <xsl:choose>
        <xsl:when test="@DirectionalCD='I'">
           <xsl:text>Received</xsl:text>
        </xsl:when>
        <xsl:otherwise>
           <xsl:text>Sent</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </td>

    <td unselectable="on">
      <xsl:value-of select="/Root/Reference[@List='PertainsTo' and @ReferenceID=current()/@ClaimAspectID]/@Name"/>
      <xsl:if test="/Root/Reference[@List='PertainsTo' and @ReferenceID=current()/@ClaimAspectID and @AssignmentID=current()/@AssignmentID]/@AssignmentSuffix != ''">
        <xsl:text>/Asgn </xsl:text>
        <xsl:value-of select="/Root/Reference[@List='PertainsTo' and @ReferenceID=current()/@ClaimAspectID and @AssignmentID=current()/@AssignmentID]/@AssignmentSuffix"/>
      </xsl:if>
    </td>

	<td unselectable="on"> 
	 <xsl:choose>
	  <xsl:when test="@FinalEstimateFlag='0'">
		   <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:text>F</xsl:text>
	  </xsl:otherwise>
	 </xsl:choose> 
	</td>
	

	<td>
	 <xsl:choose>
	  <xsl:when test="@DirectionToPayFlag='0'">
		   <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:text>D</xsl:text>
	  </xsl:otherwise>
	 </xsl:choose> 
	</td>
	
    <td unselectable="on">
      <xsl:value-of select="/Root/Reference[@List='DocumentType' and @ReferenceID=current()/@DocumentTypeID]/@Name"/>
    </td>

    <td unselectable="on">
      <xsl:choose>
        <xsl:when test="@SupplementSeqNumber > 0">
          <xsl:value-of select="@SupplementSeqNumber" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <SPAN style="display:none"><xsl:value-of select="@SupplementSeqNumber" /></SPAN>
        </xsl:otherwise>
      </xsl:choose>
    </td>

    <td unselectable="on">
      <xsl:value-of select="/Root/Reference[@List='DocumentSource' and @ReferenceID=current()/@DocumentSourceID]/@Name"/>
    </td>

    <td unselectable="on">
      <xsl:value-of select="substring(@CreatedUserNameFirst,0,12)"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="substring(@CreatedUserNameLast,0,12)"/> - <xsl:value-of select="@CreatedUserRoleName"/>
    </td>

    <td unselectable="on">
      <xsl:variable name="chkReceivedDate" select="@ReceivedDate"/>
      <xsl:choose>
        <xsl:when test="contains($chkReceivedDate, '1900')">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="js:UTCConvertDate(string(current()/@ReceivedDate))" /> 
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <xsl:value-of select="js:UTCConvertTime(string(current()/ @ReceivedDate))"/>
        </xsl:otherwise>
      </xsl:choose>
    </td>

  </tr>

</xsl:template>

</xsl:stylesheet>
