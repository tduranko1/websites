<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="Claim">

  <xsl:import href="msxsl/msjs-client-library.js"/>
  <xsl:import href="msxsl/msjs-client-library-htc.js"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function AdjustCRUD( strCrud)
     {
      var strRet = "_" + strCrud.substr(1);
       return strRet;
     }
  ]]>
</msxsl:script>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ClaimCRUD" select="Claim"/>
<xsl:param name="CarrierCRUD" select="Data:Carrier"/>
<xsl:param name="InsuredCRUD" select="Insured"/>
<xsl:param name="CoverageCRUD" select="Coverage"/>
<xsl:param name="DocumentCRUD" select="Document"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserId"/>
<xsl:param name="WindowID"/>
<xsl:param name="ImageRootDir"/>
<xsl:param name="IMPSEmailBCC"/>
<xsl:param name="VehNum"/>
<xsl:param name="SessionKey"/>

<xsl:template match="/Root">

<xsl:variable name="InsCo" select="session:XslGetSession('InsuranceCompanyName')"/>
<xsl:variable name="ClaimID" select="session:XslGetSession('ClientClaimNumber')"/>
<xsl:variable name="ClaimRestrictedFlag" select="session:XslGetSession('RestrictedFlag')"/>
<xsl:variable name="ClaimOpen" select="session:XslGetSession('ClaimOpen')"/>
<xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>
<xsl:variable name="CSRNameFirst" select="session:XslGetSession('OwnerUserNameFirst')"/>
<xsl:variable name="CSRNameLast" select="session:XslGetSession('OwnerUserNameLast')"/>
<xsl:variable name="InsuredBusinessName" select="session:XslGetSession('InsuredBusinessName')"/>
<xsl:variable name="DemoFlag" select="session:XslGetSession('DemoFlag')"/>
<xsl:variable name="CoverageLimitWarningInd" select="session:XslGetSession('CoverageLimitWarningInd')"/>
<xsl:variable name="Claim_ClaimAspectID" select="session:XslGetSession('Claim_ClaimAspectID')"/>


<xsl:variable name="Context" select="session:XslUpdateSession('Context',string(/Root/@Context))"/>
<xsl:variable name="VehCount" select="session:XslUpdateSession('VehCount',count(/Root/Vehicle))"/>

<xsl:variable name="AdjustedDocumentPermissions" select="user:AdjustCRUD(string($DocumentCRUD))"/>

<xsl:variable name="LynxID" select="/Root/@LynxID" />
<xsl:variable name="InsuredFirst" select="/Root/Claim/Insured/@NameFirst"/>
<xsl:variable name="InsuredLast" select="/Root/Claim/Insured/@NameLast"/>
<xsl:variable name="LossDate" select="/Root/Claim/@LossDate"/>
<xsl:variable name="ClaimLastUpdatedDate" select="/Root/Claim/@SysLastUpdatedDate"/>
<!-- <xsl:variable name="pageReadOnly" select="string(boolean($ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'))"/> -->
<xsl:variable name="pageReadOnly" select="string(boolean($ClaimStatus='Claim Closed'))"/>

<!-- <xsl:if test="$ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'"> -->
<xsl:if test="$pageReadOnly = 'true'">
  <xsl:value-of select="js:setPageDisabled()"/>
</xsl:if>

<xsl:variable name="AdjClaimCRUD">
<xsl:choose>
  <xsl:when test="contains($ClaimCRUD, 'RU') = true()">_RU_</xsl:when>
  <xsl:when test="contains($ClaimCRUD, 'R') = true()">_R__</xsl:when>
  <xsl:otherwise><xsl:value-of select="$ClaimCRUD"/></xsl:otherwise>
</xsl:choose>
</xsl:variable>
<xsl:variable name="AdjInsuredCRUD">
<xsl:choose>
  <xsl:when test="contains($InsuredCRUD, 'RU') = true()">_RU_</xsl:when>
  <xsl:when test="contains($InsuredCRUD, 'R') = true()">_R__</xsl:when>
  <xsl:otherwise><xsl:value-of select="$InsuredCRUD"/></xsl:otherwise>
</xsl:choose>
</xsl:variable>
<xsl:variable name="AdjCoverageCRUD">
   <xsl:value-of select="$CoverageCRUD"/>
</xsl:variable>
<xsl:variable name="InsuredEnabled">
<xsl:choose>
  <xsl:when test="count(/Root/Vehicle[@ExposureCD='1']) &gt; 0">false</xsl:when>
  <xsl:otherwise>true</xsl:otherwise>
</xsl:choose>
</xsl:variable>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL    PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2.0\webroot\,{6D65EFC5-52B6-4302-84C4-080E14C8900B},19,uspClaimGetDetailXML,Claim.xsl, 147468, null  -->

<HEAD>
<TITLE>Claim Info</TITLE>
<LINK rel="stylesheet" href="/css/APDControls.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/APDGrid2.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/ZipCodeUtils.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker2.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formats.js"></SCRIPT>

<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>     
<script type="text/javascript"> 
          document.onhelp=function(){  
        RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,1);
        event.returnValue=false;
        };  
</script>

<SCRIPT language="JavaScript">
   var gbDirty = false;
   var gsClaimCRUD = '<xsl:value-of select="$ClaimCRUD"/>';
   var gsCarrierCRUD = '<xsl:value-of select="$CarrierCRUD"/>';
   var gsInsuredCRUD = '<xsl:value-of select="$InsuredCRUD"/>';
   var gsDocumentCRUD = '<xsl:value-of select="$DocumentCRUD"/>';
   var gsCoverageCRUD = '<xsl:value-of select="$AdjCoverageCRUD"/>';

  var gsWindowID = "<xsl:value-of select="$WindowID"/>";
  var gsLynxID = "<xsl:value-of select="$LynxID"/>";
  var gsClaimID = "<xsl:value-of select="js:cleanString(string($ClaimID))"/>";
  var gsIFname = "<xsl:value-of select="js:cleanString(string($InsuredFirst))"/>";
  var gsILname = "<xsl:value-of select="js:cleanString(string($InsuredLast))"/>";
   var gsIBusiness = "<xsl:value-of select="js:cleanString(string($InsuredBusinessName))"/>";
  var gsLossDate = "<xsl:value-of select="js:UTCConvertDate(string($LossDate))"/>";
  var gsInsco = "<xsl:value-of select="js:cleanString(string($InsCo))"/>";
  var gsClaimLastUpdatedDate = "<xsl:value-of select="$ClaimLastUpdatedDate"/>";
  var gsUserID = "<xsl:value-of select="$UserId"/>";
  var gsClaimRestrictedFlag = "<xsl:value-of select="$ClaimRestrictedFlag"/>";
  var gsClaimOpen = "<xsl:value-of select="$ClaimOpen"/>";
   var gsImageRootDir = "<xsl:value-of select="$ImageRootDir"/>";
  var gsCSRNameFirst = '<xsl:value-of select="js:cleanString(string($CSRNameFirst))"/>';
  var gsCSRNameLast = '<xsl:value-of select="js:cleanString(string($CSRNameLast))"/>';
  var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";
  var gsInsuranceCompanyID = "<xsl:value-of select="Claim/@InsuranceCompanyID"/>";
  var bPageReadOnly = <xsl:value-of select="$pageReadOnly"/>
  var gsDemoFlag = "<xsl:value-of select="$DemoFlag"/>";
  var gsIMPSEmailBCC = "<xsl:value-of select="$IMPSEmailBCC"/>";
  var gsVehNum = "<xsl:value-of select="$VehNum"/>";
  var sClaimAspectID;
  var gsCoverageLimitWarningInd = "<xsl:value-of select="$CoverageLimitWarningInd"/>";
  var gsVehicleList = new Array(<xsl:for-each select="/Root/Vehicle">
                        <xsl:variable name="VehOwner">
                          <xsl:choose>
                            <xsl:when test="@BusinessName != ''"><xsl:value-of select="@BusinessName"/></xsl:when>
                            <xsl:when test="@NameFirst != ''"><xsl:value-of select="concat(substring(@NameFirst, 1, 1), '. ', @NameLast)"/></xsl:when>
                            <xsl:otherwise><xsl:value-of select="@NameLast"/></xsl:otherwise>
                          </xsl:choose>
                        </xsl:variable>
                        "<xsl:value-of select="@VehicleNumber"/>|<xsl:value-of select="js:encode(concat(@VehicleNumber, ': ' , string($VehOwner)))"/> (<xsl:value-of select="js:encode(concat(@VehicleYear, ' ', @Make, ' ', @Model))"/>)"<xsl:if test="position() != last()">, </xsl:if>
                      </xsl:for-each>
                     );
  
  var iEmailVehNum = "";
  var gsClaim_ClaimAspectID = "<xsl:value-of select="$Claim_ClaimAspectID"/>";
  var gsCarrierOfficeEmailAddress = "<xsl:value-of select="js:cleanString(string(/Root/Claim/Carrier/@OfficeEmailAddress))"/>";
  var gsCarrierReturnDocDestinationCD = "<xsl:value-of select="js:cleanString(string(/Root/Claim/Carrier/@ReturnDocDestinationCD))"/>";
   var gblnNewCoverageMode = false;
   var CarrierRepActive = "<xsl:value-of select="/Root/Claim/Carrier/@ActiveFlag"/>";

   if (parent.gsCoverageLimitWarningInd)
   parent.gsCoverageLimitWarningInd = "<xsl:value-of select="$CoverageLimitWarningInd"/>";

<![CDATA[
  function pageInit(){
    // set the info in the main folder
    top.setClaimHeader(gsCSRNameFirst, gsCSRNameLast, gsLynxID, gsClaimID, gsIFname, gsILname, gsLossDate, gsInsco, gsClaimRestrictedFlag, gsClaimOpen, gsClaimStatus, gsIBusiness, gsDemoFlag, gsInsuranceCompanyID);
    if (gsDocumentCRUD.indexOf("C") == -1)
      top.DocumentsMenu(false, 1);
    top.sClaim_ClaimAspectID = gsClaim_ClaimAspectID;
    
    if (divEditCoverage) {
      with (divEditCoverage){
        style.top = (document.body.offsetHeight - divEditCoverage.offsetHeight) / 2;
        style.left = (document.body.offsetWidth - divEditCoverage.offsetWidth) / 2;
        style.visibility = "visible";
        style.display = "none";
        firstChild.style.filter = "";
      }
      with (divAddServiceChannel){
        style.top = (document.body.offsetHeight - divAddServiceChannel.offsetHeight) / 2;
        style.left = (document.body.offsetWidth - divAddServiceChannel.offsetWidth) / 2;
        style.visibility = "visible";
        style.display = "none";
        firstChild.style.filter = "";
      }
      
      if (ClientClaimNumberSquished){
         ClientClaimNumberSquished.style.display = "none";
         ClientClaimNumberSquished.value = ToAlphaNumeric(ClientClaimNumber.value);
      }
    }
  }
  
  function setDirty(){
    gbDirty = true;
  }
  
  function isDirty(){
    return gbDirty;
  }
  
  function loadVehicle(claimAspectID) {
    var iFrm;
    iFrm = document.all["frmVehDetail" + claimAspectID];
    if (iFrm) 
      iFrm.src = "claimVehicleCond.asp?WindowID=" + gsWindowID + "&InsuranceCompanyID=" + gsInsuranceCompanyID+ "&LynxID=" + gsLynxID + "&ClaimAspectID=" + claimAspectID;
    sClaimAspectID = claimAspectID;
    
    /* This is not required as we would have refreshed the notes and diary from Apdselect.asp.
       We have to uncomment this when we filter the diary/notes for that vehicle.
    if (typeof(top.refreshNotesWindow) == "function")
      top.refreshNotesWindow( gsLynxID, gsUserID );
    if (typeof(top.refreshCheckListWindow) == "function")
      top.refreshCheckListWindow( gsLynxID, gsUserID );*/
  }
  
  function setVehicleInfo() {
    top.sVehicleName = tabGrpVehicle.activetab.caption;
  }

  // show the claim carrier rep window
  function ShowClaimCarrierRep()
  {
    if (bPageReadOnly == true) return;
    var sURL = "ClaimCarrierRep.asp?WindowID=" + gsWindowID + "&InsuranceCompanyID=" + gsInsuranceCompanyID + "&SysLastUpdatedDate=" + gsClaimLastUpdatedDate + 
                "&CarrierRepUserID=" + document.getElementById('CarrierUserID').value;

    var strRet = window.showModalDialog(sURL, "", "dialogHeight:170px;dialogWidth:405px;center:1;help:0;resizable:0;scroll:0;status:0");

    if (strRet == undefined) return;  //if the dialog is closed by the browser close button [X}, rather than the app close button
    var arystrRet = strRet.split("||");
    if (arystrRet[0] != ""){
      var sCarrierOffice = arystrRet[0];
      if (arystrRet[0] != document.getElementById('CarrierOffice').value)
        document.getElementById('CarrierOffice').value = arystrRet[0];
      gsClaimLastUpdatedDate = arystrRet[2];  
      
      tabClaim.lastUpdatedDate = gsClaimLastUpdatedDate;
      tabCarrier.lastUpdatedDate = gsClaimLastUpdatedDate;
      
      var sProc;
      var sRequest = "UserID=" + arystrRet[1] + 
                     "&ApplicationCD=ALL";  // + (sCarrierOffice == "" ? "APD" : "CP") + "";
                     
      sProc = "uspSessionGetUserDetailXML";
      var aRequests = new Array();
      aRequests.push( { procName : sProc,
                        method   : "executespnpasxml",
                        data     : sRequest }
                    );

      var objRet = XMLSave(makeXMLSaveString(aRequests));
      if (objRet.code == 0) {
          var objXML = objRet.xml;

          var UserNode = objXML.documentElement.selectSingleNode("//Root/User");
          
          document.getElementById('CarrierUserID').value = UserNode.getAttribute("UserID");
          document.getElementById('CarrierNameTitle').value = UserNode.getAttribute("NameTitle");
          document.getElementById('CarrierNameFirst').value = UserNode.getAttribute("NameFirst");
          document.getElementById('CarrierNameLast').value = UserNode.getAttribute("NameLast");
          document.getElementById('CarrierEmailAddress').value = UserNode.getAttribute("EmailAddress");
          var sPhoneNumber = "(" + (UserNode.getAttribute("PhoneAreaCode") ? UserNode.getAttribute("PhoneAreaCode") : "") + ") "+
                              (UserNode.getAttribute("PhoneExchangeNumber") ? UserNode.getAttribute("PhoneExchangeNumber") : "") + "-" +
                              (UserNode.getAttribute("PhoneUnitNumber") ? UserNode.getAttribute("PhoneUnitNumber") : "") + " x" +
                              (UserNode.getAttribute("PhoneExtensionNumber") ? UserNode.getAttribute("PhoneExtensionNumber") : "");

          document.getElementById('CarrierPhone').value = sPhoneNumber;
          var sFaxNumber = "(" + (UserNode.getAttribute("FaxAreaCode") ? UserNode.getAttribute("FaxAreaCode") : "") + ") " +
                            (UserNode.getAttribute("FaxExchangeNumber") ? UserNode.getAttribute("FaxExchangeNumber") : "") + "-" +
                            (UserNode.getAttribute("FaxUnitNumber") ? UserNode.getAttribute("FaxUnitNumber") : "") + " x" +
                            (UserNode.getAttribute("FaxExtensionNumber") ? UserNode.getAttribute("FaxExtensionNumber") : "");
          document.getElementById('CarrierFax').value = sFaxNumber;
          
          gsCarrierOfficeEmailAddress = UserNode.getAttribute("OfficeEmailAddress");
      } else 
        ServerEvent();
    }
  }
  
  function ShowEstimate(docId, showTotals) {
    var sURL = "Estimates.asp?WindowID=" + gsWindowID + "&DocumentID=" + docId + "&showTotals=" + showTotals;
    var winName = "EstimateFull_" + gsLynxID;
    var sWinEstimates = window.open(sURL, winName,"Height=523px, Width=728px, Top=90px, Left=40px, resizable=Yes, status=No, menubar=No, titlebar=No");
    parent.gAryOpenedWindows.push(sWinEstimates);
    sWinEstimates.focus();
  }

  function CancelShowEstimate()  {
    var obj = document.getElementById("ifrmEstimateSummary");
    obj.style.visibility = "hidden";
    obj.style.display = "none";
    obj.src = "blank.asp";
  }
  
  function disableCurrentAssignment(bDisabled){
    var iFrm;
    iFrm = document.all["frmVehDetail" + sClaimAspectID];
    if (iFrm) {
      var ret = iFrm.contentWindow.disableCurrentAssignment(bDisabled);
    }
  }
  
  function VehicleDelete(){
    var iFrm;
    iFrm = document.all["frmVehDetail" + sClaimAspectID];
    if (iFrm) {
      var ret = iFrm.contentWindow.VehicleDelete();
      if (ret == true) {
        document.location.reload();
      }
    }
  }
  
   function emailCarrier()
   {
    if (CarrierRepActive == "0"){
      ClientWarning("Carrier Adjuster is inactive. Cannot send an email.");
      return;
    }
    if (iEmailVehNum == "") {
      if (gsVehicleList.length > 1) {
        showEntityPicker(0, 0, aEmail);
        return;
      } else {
        iEmailVehNum = gsVehicleList[0].split("|")[0];
      }
    }
    var _email;
    var _body = "";
    var _cc = "";
    switch(gsCarrierReturnDocDestinationCD){
      case "OFC":
        if (gsCarrierOfficeEmailAddress == ""){
          ClientWarning("An office email address is not specified for this Insurance Company. Please contact IT.");
          return;
        }
        _email = gsCarrierOfficeEmailAddress;
        _cc = " ";
        break;
      default: // email to carrier rep
        _email = document.getElementById("CarrierEmailAddress").value;
        _cc = gsCarrierOfficeEmailAddress;
    }
    var sInsuredName = (gsIBusiness != "" ? gsIBusiness : gsIFname + " " + gsILname)
	
	//- 10Jun2021 TVD - Check if HiRoad and change the subject line (HQCL-3083)
	//alert(gsInsuranceCompanyID);
	if (gsInsuranceCompanyID==588)
	{
		_email = "Appraisals@hiroad.com";
		var _subj = "MR Claim %23: " + gsClaimID + " / Insured: " + sInsuredName + " / Loss Date: " + gsLossDate.replace(/\//g, "-") + " / LYNX ID: " + gsLynxID + "-" + iEmailVehNum;
		_subj = _subj.replace(/&/g, "");
		document.location = "mailto:"+_email+"?bcc=" + gsIMPSEmailBCC + (_cc != "" ? "&cc=" + _cc : "") + "&subject="+_subj+"&body="+_body;
		iEmailVehNum = "";
	} else
	{
		var _subj = "Claim %23: " + gsClaimID + " / Insured: " + sInsuredName + " / Loss Date: " + gsLossDate.replace(/\//g, "-") + " / LYNX ID: " + gsLynxID + "-" + iEmailVehNum;
		_subj = _subj.replace(/&/g, "");
		document.location = "mailto:"+_email+"?bcc=" + gsIMPSEmailBCC + (_cc != "" ? "&cc=" + _cc : "") + "&subject="+_subj+"&body="+_body;
		iEmailVehNum = "";
	}
  }
  
  function showEntityPicker(x, y, obj){
    var contextHtml="";
    contextHtml = '<div style="height:100%; width:100%;overflow:auto;border:1pt solid #666666">';
    contextHtml+='<TABLE unselectable="on" STYLE="BGCOLOR="#F9F8F7" WIDTH="100%" HEIGHT1="100%" CELLPADDING="0" CELLSPACING="1">';
    contextHtml+='<ST'+'YLE TYPE="text/css">\n';
    contextHtml+='td {font-family:verdana; font-size:11px;height:18px;}\n';
    contextHtml+='</ST'+'YLE>\n';
    contextHtml+='<SC'+'RIPT LANGUAGE="JavaScript">\n';
    contextHtml+='\n<'+'!--\n';
    contextHtml+='window.onerror=null;\n';
    contextHtml+='/'+' -'+'->\n';
    contextHtml+='</'+'SCRIPT>\n';
    var iVehNum, sVehInfo;
    contextHtml+='<TR unselectable="on"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7;text-align:center;font-weight:bold;background-color:#C0C0C0;font-color:#FFFFFF" >Pick an Entity</TD></TR>';
    for (var i = 0; i < gsVehicleList.length; i++) {
      iVehNum = gsVehicleList[i].split("|")[0];
      sVehInfo = unescape(gsVehicleList[i].split("|")[1]);
      contextHtml+='<TR unselectable="on"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ID="i' + i + '" ONMOUSEOVER="document.all.i' + i + '.style.background=\'#B6BDD2\';document.all.i' + i + '.style.border=\'1pt solid #0A246A\';document.all.i' + i + '.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i' + i + '.style.background=\'#F9F8F7\';document.all.i' + i + '.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.setEmailVehNum(' + iVehNum + ');">&nbsp;' + sVehInfo + '</TD></TR>';
    }
    contextHtml+='</TABLE></div>';
    
    var iHeight = (gsVehicleList.length) * 24 + 7;
    var oPopupBody = oPopup.document.body;
    oPopupBody.innerHTML = contextHtml;
    oPopup.show(0, 18, 250, iHeight, obj);
  }
  
  function setEmailVehNum(iVehNum){
    iEmailVehNum = iVehNum;
    emailCarrier();
  }
  
  function checkLimit(){
    if (typeof(parent.displayCoverageLimitWarining) == "function"){
      if ((parseFloat(CollisionLimitAmt.value) > 0 && parseFloat(CollisionLimitAmt.value) <= 10000) ||
          (parseFloat(ComprehensiveLimitAmt.value) > 0 && parseFloat(ComprehensiveLimitAmt.value) <= 10000) ||
          (parseFloat(LiabilityLimitAmt.value) > 0 && parseFloat(LiabilityLimitAmt.value) <= 10000) ||
          (parseFloat(UnderInsuredLimitAmt.value) > 0 && parseFloat(UnderInsuredLimitAmt.value) <= 10000) ||
          (parseFloat(UnInsuredLimitAmt.value) > 0 && parseFloat(UnInsuredLimitAmt.value) <= 10000) )
        parent.displayCoverageLimitWarining(true);
      else
        parent.displayCoverageLimitWarining(false);
    }
    if (parseFloat(CollisionLimitAmt.value) > 0 && parseFloat(CollisionLimitAmt.value) <= 10000)
      CollisionLimitWarningInd.style.display = "inline";
    else
      CollisionLimitWarningInd.style.display = "none";

    if (parseFloat(ComprehensiveLimitAmt.value) > 0 && parseFloat(ComprehensiveLimitAmt.value) <= 10000)
      ComprehensiveLimitWarningInd.style.display = "inline";
    else
      ComprehensiveLimitWarningInd.style.display = "none";

    if (parseFloat(LiabilityLimitAmt.value) > 0 && parseFloat(LiabilityLimitAmt.value) <= 10000)
      LiabilityLimitWarningInd.style.display = "inline";
    else
      LiabilityLimitWarningInd.style.display = "none";

    if (parseFloat(UnderInsuredLimitAmt.value) > 0 && parseFloat(UnderInsuredLimitAmt.value) <= 10000)
      UnderInsuredLimitWarningInd.style.display = "inline";
    else
      UnderInsuredLimitWarningInd.style.display = "none";

    if (parseFloat(UnInsuredLimitAmt.value) > 0 && parseFloat(UnInsuredLimitAmt.value) <= 10000)
      UnInsuredLimitWarningInd.style.display = "inline";
    else
      UnInsuredLimitWarningInd.style.display = "none";
    
  }
  
  function updateLastUpdatedDate(){
    if (tabGrpClaim.activetab.id == "tabClaim")
       gsClaimLastUpdatedDate = tabClaim.lastUpdatedDate;
  }
  
  function doCoverageAdd(){
      if (gsCoverageCRUD.indexOf("C") == -1) {
         ClientWarning("You do not have permission to add coverage. Please contact your supervisor.");
         return;
      }
      
      if (tabGrpClaim.activetab && tabGrpClaim.activetab.caption == "Coverage") {         
         divMaskAll.style.display = "inline";
         divEditCoverage.style.display = "inline";
         
         divEditCoverage.firstChild.cells[0].innerText = "Add Coverage";
         selClientCoverageDesc.CCDisabled = false;
         selClientCoverageDesc.selectedIndex = -1;
         txtClaimCoverageID.value = "";
         txtCoverageType.value = "";
         txtAdditionalFlag.value = "";
         txtCoverageDeductible.value = "0";
         txtCoverageLimit.value = "0";
         txtCoverageMaxDays.value = "0";
         txtCoverageDailyLimit.value = "0";
         
         //clear existing client claim coverages
         selClientCoverageDesc.Clear();
         
         //add ones that does not exist.
         if (xmlClientCoverageType){
            var oClientCoverages = xmlClientCoverageType.selectNodes("/Root/Reference[@List='ClientCoverageType']");
            for (var i = 0; i < oClientCoverages.length; i++){
               var oClaimCoverage = xmlCoverages.selectSingleNode("/Root/Coverage[@ClientCoverageTypeID='" + oClientCoverages[i].getAttribute("ClientCoverageTypeID") + "']");
               if (oClaimCoverage == null){
                  selClientCoverageDesc.AddItem(oClientCoverages[i].getAttribute("ClientCoverageTypeID"), (oClientCoverages[i].getAttribute("ClientCode") != "" ? oClientCoverages[i].getAttribute("ClientCode") + "-" : "") + oClientCoverages[i].getAttribute("Name"));
               }
            }
         }
         
         selClientCoverageDesc.setFocus();
         
         if (xmlCoverages){
            var oNewNode = xmlCoverages.createElement("Coverage");
            if (oNewNode){
               oNewNode.setAttribute("ClaimCoverageID", "-1");
               oNewNode.setAttribute("ClientCoverageTypeID", "");
               oNewNode.setAttribute("LynxID", gsLynxID);
               oNewNode.setAttribute("AddtlCoverageFlag", "");
               oNewNode.setAttribute("CoverageTypeCD", "");
               oNewNode.setAttribute("Description", "");
               oNewNode.setAttribute("ClientCoverageDesc", "");
               oNewNode.setAttribute("DeductibleAmt", "");
               oNewNode.setAttribute("LimitAmt", "");
               oNewNode.setAttribute("LimitDailyAmt", "");
               oNewNode.setAttribute("MaximumDays", "");
               oNewNode.setAttribute("AddlCoverageFlagInd", "/images/spacer.gif");
               oNewNode.setAttribute("SysLastUpdatedDate", "");
               
               var oRoot = xmlCoverages.selectSingleNode("/Root");
               if (oRoot){
                  oRoot.appendChild(oNewNode);                  
                  //xmlCoverages.loadXML(xmlCoverages.xml);
                  //GrdCoverage.__initXML();
                  GrdCoverage.refreshData();
                  //GrdCoverage.selectRecordIndex(2);
                  
                  gblnNewCoverageMode = true;
               }
            }
         }
      }
  }
  
  function doCoverageDelete(){
      if (gsCoverageCRUD.indexOf("D") == -1) {
         ClientWarning("You do not have permission to delete coverage. Please contact your supervisor.");
         return;
      }
      
      if (tabGrpClaim.activetab && tabGrpClaim.activetab.caption == "Coverage") {
         var oSelRow = GrdCoverage.selectedRow;
         if (oSelRow && oSelRow.objXML){
            var oXML = oSelRow.objXML;
            if (oXML){
               if (oXML.getAttribute("CoverageApplied") == "1"){
                  ClientWarning("Deductible/Limit has been applied to this coverage. Cannot delete.");
                  return;
               }
               if (YesNoMessage("Confirm Delete", "Do you want to delete the selected coverage?. Click Yes to delete.") == "Yes"){               
               //var oXML = oSelRow.objXML;
                  var strClaimCoverageID = "";
                  var sRequest = "";
                  var sProc = "uspClaimCoverageDel";
                  strClaimCoverageID = oXML.getAttribute("ClaimCoverageID");
                  sRequest = "ClaimCoverageID=" + strClaimCoverageID +
                             "&ClientCoverageTypeID=" + oXML.getAttribute("ClientCoverageTypeID") +
                             "&LynxID=" + gsLynxID + 
                             "&UserID=" + gsUserID +
                             "&SysLastUpdatedDate=" + oXML.getAttribute("SysLastUpdatedDate");
                  //alert(sRequest);
                  //return;
                  var aRequests = new Array();
                  aRequests.push( { procName : sProc,
                                    method   : "ExecuteSpNpAsXML",
                                    data     : sRequest }
                                );
                  var sXMLRequest = makeXMLSaveString(aRequests);
                  var objRet = XMLSave(sXMLRequest);
                  if (objRet && objRet.code == 0 && objRet.xml) {
                     var oRetXML = objRet.xml;
                     if (oRetXML){
                        //alert(oRetXML.xml);
                        var oDeletedNode = oRetXML.selectSingleNode("/Root/StoredProc[@name='" + sProc + "']/Result/Root/Coverage");
                        if (oDeletedNode){
                           var oNode = xmlCoverages.selectSingleNode("/Root/Coverage[@ClaimCoverageID='" + strClaimCoverageID + "']")
                           if (oNode){
                              var oRoot = xmlCoverages.selectSingleNode("/Root");
                              if (oRoot) {
                                 oRoot.removeChild(oNode);
                                 //xmlCoverages.loadXML(xmlCoverages.xml);
                                 //GrdCoverage.__initXML();
                                 GrdCoverage.refreshData();
                                 gblnNewCoverageMode = false;
                              }
                           }
                        }
                     }
                  } else {               
                     if (gblnNewCoverageMode) {
                        cancelCoverage();
                     } else {
                        xmlCoverages.loadXML(strOldXML);
                     }
                     gblnNewCoverageMode = false;
                  }                  
               }
            }
         }        
      }
  }
  
  function doTabGrpSave(){
   if (tabGrpClaim.activetab && tabGrpClaim.activetab.caption == "Coverage")
      event.returnValue = false;
  }
  
  function editCoverage(){
      if (gsCoverageCRUD.indexOf("U") == -1) {
         ClientWarning("You do not have permission to edit coverage. Please contact your supervisor.");
         return;
      }
      
      if (tabGrpClaim.activetab && tabGrpClaim.activetab.caption == "Coverage") {         
         divMaskAll.style.display = "inline";
         divEditCoverage.style.display = "inline";
         
         var oSelRow = GrdCoverage.selectedRow;
         if (oSelRow && oSelRow.objXML){
            var oXML = oSelRow.objXML;
            if (oXML){
               divEditCoverage.firstChild.cells[0].innerText = "Edit Coverage";
               selClientCoverageDesc.CCDisabled = false;
               selClientCoverageDesc.selectedIndex = -1;

               //clear existing client claim coverages
               selClientCoverageDesc.Clear();
               
               //add ones that does not exist.
               if (xmlClientCoverageType){
                  var oClientCoverages = xmlClientCoverageType.selectNodes("/Root/Reference[@List='ClientCoverageType']");
                  for (var i = 0; i < oClientCoverages.length; i++){
                     //selClientCoverageDesc.AddItem(oClientCoverages[i].getAttribute("ClientCoverageTypeID"), oClientCoverages[i].getAttribute("Name"));
                     selClientCoverageDesc.AddItem(oClientCoverages[i].getAttribute("ClientCoverageTypeID"), (oClientCoverages[i].getAttribute("ClientCode") != "" ? oClientCoverages[i].getAttribute("ClientCode") + "-" : "") + oClientCoverages[i].getAttribute("Name"));
                  }
               }
      
               selClientCoverageDesc.value = oXML.getAttribute("ClientCoverageTypeID");
               selClientCoverageDesc.CCDisabled = true;
               txtClaimCoverageID.value = oXML.getAttribute("ClaimCoverageID");
               txtCoverageType.CCDisabled = false;
               txtCoverageType.value = oXML.getAttribute("CoverageTypeCD");
               txtCoverageType.CCDisabled = true;
               
               txtAdditionalFlag.value = (oXML.getAttribute("AddtlCoverageFlag") == "1" ? "Yes" : "No");
               txtCoverageDeductible.value = oXML.getAttribute("DeductibleAmt");
               txtCoverageLimit.value = oXML.getAttribute("LimitAmt");
               if (oXML.getAttribute("CoverageTypeCD") == "RENT"){
                  txtCoverageMaxDays.value = oXML.getAttribute("MaximumDays");
                  txtCoverageDailyLimit.value = oXML.getAttribute("LimitDailyAmt");
                  txtCoverageMaxDays.setFocus();
               }
               txtCoverageDeductible.setFocus();
            }
         }
      }      
  }
  
  function saveCoverage(){
      if (xmlCoverages){
         if (validateCoverage() == false) return;
         var oNode = null;
         var strOldXML = "";
         var strClientCoverageDescription = "";
         if (gblnNewCoverageMode) {
            if (xmlClientCoverageType){
               var oClientCoverage = xmlClientCoverageType.selectSingleNode("/Root/Reference[@List='ClientCoverageType' and @ClientCoverageTypeID='" + selClientCoverageDesc.value + "']");
               if (oClientCoverage){
                  strClientCoverageDescription = oClientCoverage.getAttribute("Name");
               }
            }
            oNode = xmlCoverages.selectSingleNode("/Root/Coverage[@ClaimCoverageID='-1']");
            oNode.setAttribute("ClientCoverageTypeID", selClientCoverageDesc.value);
            oNode.setAttribute("LynxID", gsLynxID);
            oNode.setAttribute("AddtlCoverageFlag", (txtAdditionalFlag.value == "Yes" ? 1 : 0));
            oNode.setAttribute("CoverageTypeCD", txtCoverageType.value);
            oNode.setAttribute("Description", strClientCoverageDescription);
            oNode.setAttribute("DeductibleAmt", txtCoverageDeductible.value);
            oNode.setAttribute("LimitAmt", txtCoverageLimit.value);
            oNode.setAttribute("LimitDailyAmt", txtCoverageDailyLimit.value);
            oNode.setAttribute("MaximumDays", txtCoverageMaxDays.value);
            oNode.setAttribute("AddlCoverageFlagInd", (txtAdditionalFlag.value == "Yes" ? "/images/tick.gif" : "/images/spacer.gif"));
            oNode.setAttribute("ClientCoverageDesc", selClientCoverageDesc.text);
            oNode.setAttribute("ClientCoverageDesc2", selClientCoverageDesc.text);
         } else {
            var oSelRow = GrdCoverage.selectedRow;
            if (oSelRow && oSelRow.objXML){
               var oXML = oSelRow.objXML;
               if (oXML){
                  strOldXML = oXML.xml;
                  oNode = xmlCoverages.selectSingleNode("/Root/Coverage[@ClaimCoverageID='" + txtClaimCoverageID.value + "']");
                  if (oNode){
                     var iDeductibleAppliedAmt = oNode.getAttribute("DeductibleApplied");
                     if (isNaN(iDeductibleAppliedAmt)) iDeductibleAppliedAmt = 0;
                     
                     if (parseFloat(txtCoverageDeductible.value) < iDeductibleAppliedAmt){
                        ClientWarning("$" + formatCurrencyString(iDeductibleAppliedAmt) + " has been applied towards this coverage. Please change the applied amount(s) first and try again.");
                        return;
                     }
                     oNode.setAttribute("DeductibleAmt", txtCoverageDeductible.value);
                     oNode.setAttribute("LimitAmt", txtCoverageLimit.value);
                     if (txtCoverageType.value == "RENT"){
                        oNode.setAttribute("LimitDailyAmt", txtCoverageDailyLimit.value);
                        oNode.setAttribute("MaximumDays", txtCoverageMaxDays.value);
                     }
                  }
               }
            }
         }

         
         //prepare the update request
         if (oNode) {
            btnCovSave.CCDisabled = btnCovCancel.CCDisabled = false;
            var sRequest = "";
            var sProc = "uspClaimCoverageUpdDetail";
            sRequest = "ClaimCoverageID=" + (oNode.getAttribute("ClaimCoverageID") == -1 ? "0" : oNode.getAttribute("ClaimCoverageID")) +
                       "&ClientCoverageTypeID=" + oNode.getAttribute("ClientCoverageTypeID") +
                       "&LynxID=" + gsLynxID + 
                       "&InsuranceCompanyID=" + gsInsuranceCompanyID + 
                       "&AddtlCoverageFlag=" + oNode.getAttribute("AddtlCoverageFlag") +
                       "&CoverageTypeCD=" + oNode.getAttribute("CoverageTypeCD") +
                       "&Description=" + escape(oNode.getAttribute("Description")) +
                       "&DeductibleAmt=" + oNode.getAttribute("DeductibleAmt") +
                       "&LimitAmt=" + oNode.getAttribute("LimitAmt") +
                       "&LimitDailyAmt=" + oNode.getAttribute("LimitDailyAmt") +
                       "&MaximumDays=" + oNode.getAttribute("MaximumDays") +
                       "&UserID=" + gsUserID +
                       "&SysLastUpdatedDate=" + oNode.getAttribute("SysLastUpdatedDate");
            //return;
            var aRequests = new Array();
            aRequests.push( { procName : sProc,
                              method   : "ExecuteSpNpAsXML",
                              data     : sRequest }
                          );
            var sXMLRequest = makeXMLSaveString(aRequests);
            var objRet = XMLSave(sXMLRequest);
            if (objRet && objRet.code == 0 && objRet.xml) {
               var oRetXML = objRet.xml;
               if (oRetXML){
                  //alert(oRetXML.xml);
                  var oUpdatedNode = oRetXML.selectSingleNode("/Root/StoredProc[@name='" + sProc + "']/Result/Root/Coverage");
                  if (oUpdatedNode){
                     var strSysLastUpdatedDate = oUpdatedNode.getAttribute("SysLastUpdatedDate")
                     oNode.setAttribute("ClaimCoverageID", oUpdatedNode.getAttribute("ClaimCoverageID"));
                     oNode.setAttribute("SysLastUpdatedDate", strSysLastUpdatedDate);
                     //xmlCoverages.loadXML(xmlCoverages.xml);
                     //GrdCoverage.__initXML();
                     GrdCoverage.refreshData();
                     gblnNewCoverageMode = false;
                     
                     //refresh the active vehicle.
                     if (isNaN(sClaimAspectID) == false) loadVehicle(sClaimAspectID);
                  }
               }
            } else {               
               if (gblnNewCoverageMode) {
                  cancelCoverage();
               } else {
                  xmlCoverages.loadXML(strOldXML);
               }
               gblnNewCoverageMode = false;
            }
            divMaskAll.style.display = "none";
            divEditCoverage.style.display = "none";
            divGridHolder.focus();
         }
      }
  }
  
  function cancelCoverage(){
      if (gblnNewCoverageMode) {
         if (xmlCoverages){
            var oNode = xmlCoverages.selectSingleNode("/Root/Coverage[@ClaimCoverageID='-1']");
            if (oNode){
               var oRoot = xmlCoverages.selectSingleNode("/Root");
               if (oRoot){
                  oRoot.removeChild(oNode);
                  //xmlCoverages.loadXML(xmlCoverages.xml);
                  //GrdCoverage.__initXML();
                  GrdCoverage.refreshData();
                  
                  divGridHolder.focus();
                  gblnNewCoverageMode = false;
               }
            }
         }
      }
      divMaskAll.style.display = "none";
      divEditCoverage.style.display = "none";
  }
  
  function validateCoverage(){
      if (gblnNewCoverageMode == true) {
         if (selClientCoverageDesc.selectedIndex == -1){
            ClientWarning("Please select the coverage description from the list.");
            selClientCoverageDesc.setFocus();
            return false;
         }
         
         //check for duplicate
         var oNode = xmlCoverages.selectSingleNode("/Root/Coverage[@ClientCoverageTypeID='" + selClientCoverageDesc.value + "']");
         if (oNode){
            ClientWarning("Cannot add duplicate coverage. Coverage '" + selClientCoverageDesc.text + "' is already available for this claim. To modify the data, please double click the coverage line item.");
            selClientCoverageDesc.setFocus();
            oNode = null;
            return false;
         }
         oNode = null;         
         
         if (txtCoverageType.value == ""){
            ClientWarning("Coverage could not be determined. Please contact Helpdesk.");
            return false;
         }
         if (txtAdditionalFlag.value == ""){
            ClientWarning("Additional Flag for the selected Coverage could not be determined. Please contact Helpdesk.");
            return false;
         }
      }
            
      if (txtCoverageType.value != "RENT"){
         if ((parseFloat(txtCoverageDeductible.value) < 0) || isNaN(parseFloat(txtCoverageDeductible.value))){
            ClientWarning("Please enter a deductible amount.");
            txtCoverageDeductible.setFocus();
            return false;
         }
      }
      
      return true;
  }
  
  function updateCoverageType(){
      if (xmlClientCoverageType) {
         var oClientCoverageNode = xmlClientCoverageType.selectSingleNode("/Root/Reference[@ClientCoverageTypeID='" + selClientCoverageDesc.value + "']");
         if (oClientCoverageNode) {
            txtCoverageClientCode.CCDisabled = txtCoverageType.CCDisabled = false;
            if (oClientCoverageNode){
               txtCoverageType.value = oClientCoverageNode.getAttribute("ReferenceID");
            } else {
               txtCoverageType.value = "";
            }
            txtCoverageClientCode.value = oClientCoverageNode.getAttribute("ReferenceID")
            txtCoverageClientCode.CCDisabled = txtCoverageType.CCDisabled = true;
            txtAdditionalFlag.value = (oClientCoverageNode.getAttribute("AddtlCoverageFlag") == "1" ? "Yes" : "No");
         } else {
            txtCoverageClientCode.CCDisabled = txtCoverageType.CCDisabled = false;
            txtCoverageType.value = "";
            txtCoverageClientCode.value = "";
            txtCoverageClientCode.CCDisabled = txtCoverageType.CCDisabled = true;
            txtAdditionalFlag.value = "";
         }
      }
  }
  
  function refreshCoverage(){
      if (txtCoverageType.value == "RENT"){
         txtCoverageDailyLimit.CCDisabled = txtCoverageMaxDays.CCDisabled = false;
         txtCoverageDailyLimit.style.visibility = txtCoverageMaxDays.style.visibility = "visible";
         divCoverageRental.style.display = "inline";
      } else {
         txtCoverageDailyLimit.CCDisabled = txtCoverageMaxDays.CCDisabled = true;
         txtCoverageDailyLimit.style.visibility = txtCoverageMaxDays.style.visibility = "hidden";
         divCoverageRental.style.display = "none";
      }
  }
  
  function addServiceChannel(){
    //alert("Add new service channel");
      if (selClientServiceChannel.Options.length > 0){
         divMaskAll.style.display = "inline";
         divAddServiceChannel.style.display = "inline";    
         
         selClientServiceChannel.selectedIndex = -1;
         chkPrimaryServiceChannel.value = 0;
      } else {
         ClientWarning("No more service channels can be added for this claim.<br/><br/>All service channels for this Insurance company has been added.");
      }
  }
  
  function cancelServiceChannel(){
  //alert("Cancel");
     divMaskAll.style.display = "none";
     divAddServiceChannel.style.display = "none";    
  }
  
  function saveServiceChannel(){
      if (selClientServiceChannel.selectedIndex == -1){
         ClientWarning("Please select a service channel from the list.");
         selClientServiceChannel.setFocus();
         return;
      }
      
      if (sClaimAspectID == "" || isNaN(sClaimAspectID)){
         ClientError("Invalid vehicle state. Client-side ClaimAspectID [" + sClaimAspectID + "] global variable not set or not valid");
      }
      
      iFrm = document.all["frmVehDetail" + sClaimAspectID];
      if (iFrm) {
         if (typeof(iFrm.contentWindow.IsClaimAspectServiceChannelExist) == "function") {
            var strNewServiceChannel = selClientServiceChannel.value;
            if (iFrm.contentWindow.IsClaimAspectServiceChannelExist(strNewServiceChannel) == false){
               //confirm user action
               if (YesNoMessage("Confirm Add", "Do you want to add " + selClientServiceChannel.text + " Service Channel to Vehicle " + gsVehNum + "?.") != "Yes"){
                  cancelServiceChannel();
                  return;
               }
               
               btnSCAdd.CCDisabled = btnSCCancel.CCDisabled = true;
               
               divAddServiceChannel.style.display = "none";   
               
               // new service channel does not exist for this claim Aspect. Go ahead and create one.
               var sRequest = "";
               var sProc = "uspClaimVehicleAddServiceChannel";
               sRequest = "ClaimAspectID=" + sClaimAspectID +
                          "&ServiceChannelCD=" + selClientServiceChannel.value + 
                          "&PrimaryFlag=" + chkPrimaryServiceChannel.value + 
                          "&UserID=" + gsUserID;
               //alert(sRequest);
               //return;
               var aRequests = new Array();
               aRequests.push( { procName : sProc,
                                 method   : "ExecuteSpNpAsXML",
                                 data     : sRequest }
                             );
               var sXMLRequest = makeXMLSaveString(aRequests);
               var objRet = XMLSave(sXMLRequest);
               if (objRet && objRet.code == 0 && objRet.xml) {
               //if (objRet && objRet.code == 0) {
                  var oRetXML = objRet.xml;
                  if (oRetXML){
                     //alert(oRetXML.xml);
                     if (iFrm) 
                       iFrm.src = "claimVehicleCond.asp?WindowID=" + gsWindowID + "&InsuranceCompanyID=" + gsInsuranceCompanyID+ "&LynxID=" + gsLynxID + "&ClaimAspectID=" + sClaimAspectID;
                  }
               }
            } else {
               ClientWarning(selClientServiceChannel.text + " Service Channel already exists for this vehicle. Cannot add a duplicate.");
            }
         }
      }
  }  
  
  function hideMask(){
    try {
      divMaskAll.style.display = "none";
      divAddServiceChannel.style.display = "none";    
      btnSCAdd.CCDisabled = btnSCCancel.CCDisabled = false;
    } catch (e) {}
  }
  
  function ToAlphaNumeric(strOriginal){
      var strRet = unescape(strOriginal);
      if (strRet != ""){
         strRet = strRet.replace(/\W/g, ""); // \W will keep a-z, A-Z, 0-9 and _
         strRet = strRet.replace(/[_]/g, ""); // now remove the underscore
      } 
      return strRet;
  }

  function updateClaimCoverages(strClaimCoverageID, iDeductibleChange, iLimitChange){
      var oCoverageNode = xmlCoverages.selectNodes("/Root/Coverage[@ClaimCoverageID='" + strClaimCoverageID + "']");
      var iNewDeductibleApplied = 0;
      var iNewLimitApplied = 0;
      if (oCoverageNode){
         for (var i = 0; i < oCoverageNode.length; i++){
            var iCurDeductibleApplied = parseFloat(oCoverageNode[i].getAttribute("DeductibleApplied"));
            if (isNaN(iCurDeductibleApplied)) iCurDeductibleApplied = 0;
            
            iNewDeductibleApplied = iCurDeductibleApplied + iDeductibleChange;
            if (iNewDeductibleApplied < 0) iNewDeductibleApplied = 0;
            oCoverageNode[i].setAttribute("DeductibleApplied", iNewDeductibleApplied);
            
            var iCurLimitApplied = parseFloat(oCoverageNode[i].getAttribute("LimitApplied"));
            if (isNaN(iCurLimitApplied)) iCurLimitApplied = 0;
            
            iNewLimitApplied = iCurLimitApplied + iLimitChange;
            if (iNewLimitApplied < 0) iNewLimitApplied = 0;
            oCoverageNode[i].setAttribute("LimitApplied", iNewLimitApplied);
            
            if (iNewDeductibleApplied == 0 && iNewLimitApplied == 0){
               oCoverageNode[i].setAttribute("CoverageApplied", 0);
            }
         }
         
      }
  }

	//-- 19Jul2021 - TVD - All the code below was added by me for the save button. 
	function getSaveData()
	{
		//-- Var init
		var sProc, sRequest;
		var InvolvedTypeList;

		//-- Get frame data
		sRequest = getData("divClaimDetail");

		//-- Decide on what SP to call
		sProc = "uspClaimCondUpdDetail";

		sRequest += "&UserID=" + gsUserID +
					"&ClaimAspectID=" + parent.gsClaimAspectID;
		
		sRequest += "&SysLastUpdatedDate=" + gsClaimLastUpdatedDate;
		
		//-- Get the save data into a array
		//alert(sProc + " - " + sRequest);
		var aRequests = new Array();
		aRequests.push( { procName : sProc,
						method   : "executespnpasxml",
						data     : sRequest }
					);
					
		//-- Convert html to XML and return
		var sXML;
		sXML = makeXMLSaveString(aRequests);
		//alert(sXML);
		return sXML;		
	}
	
 	function newSave(sTag)
	{
		var iFrm;
		var oFormData = new Array();
		iFrm = getData(sTag);
		oFormData = getSaveData();
		AsyncXMLSave(oFormData, SaveSucess, SaveFailure);
	}
 
 	function SaveSucess(objXml) {
		gbDirty = false;
		//alert("sucess");
		document.location.reload();
		resetDirty();
	}
	function SaveFailure(objXml) {
		document.location.reload();
		//alert("Failure");
	}
	
	function isDirty(){
		return gbDirty;
	}
	
	function checkDirty(){
	alert("dirty status :" +gbDirty +" tab name : "+event.fromTab.name);
		if (event.fromTab.name == "tabClaim") {
			if (isDirty()) {
				var strRet = NoCancelMessage("Need to Save", "Click Cancel to go back and press the save button");
				switch (strRet) {
					case "No":
					   gbDirty = false;
					   event.returnValue = true;
					   break;
					default:
					   event.returnValue = false;
					   break;
				}
			}		 
		} else if (event.fromTab.name == "tabInsured") {
			if (isDirty()) {
				var strRet = NoCancelMessage("Need to Save", "Click Cancel to go back and press the save button");
				switch (strRet) {
					case "No":
					   gbDirty = false;
					   event.returnValue = true;
					   break;
					default:
					   event.returnValue = false;
					   break;
				}
			}		 
		} else if (event.fromTab.name == "tabCoverage") {
				if (isDirty()) {
				var strRet = NoCancelMessage("Need to Save", "Click Cancel to go back and press the save button");
				switch (strRet) {
					case "No":
					   gbDirty = false;
					   event.returnValue = true;
					   break;
					default:
					   event.returnValue = false;
					   break;
				} 
			}
		} else if (event.fromTab.name == "tabCarrier") {
				if (isDirty()) {
				var strRet = NoCancelMessage("Needx to Save", "Click Cancel to go back and press the save button");
				switch (strRet) {
					case "No":
					   gbDirty = false;
					   event.returnValue = true;
					   break;
					default:
					   event.returnValue = false;
					   break;
				} 
			}
		}
  }
	
]]>
</SCRIPT></HEAD>

<BODY unselectable="on" class="bodyAPDSelect" style="margin:0px;padding:5px;overflow:hidden;" tabIndex="-1" onload="pageInit()">
  <div style="margin:0px;margin-bottom:25px">
  <IE:APDTabGroup id="tabGrpClaim" name="tabClaim" many="true" width="735px" height="155" preselectTab="0" CCDisabled="false" 
  CCTabIndex="1" showADS="true"  onTabBeforeSave= "doTabGrpSave()" >
    <IE:APDTab id="tabClaim" name="tabClaim" caption="Claim" tabPage="divClaimDetail" CCTabIndex="1" updStoredProc="uspClaimCondUpdDetail|executespnpasxml" onTabAfterSave="updateLastUpdatedDate()">
      <xsl:attribute name="CRUD"><xsl:value-of select="$AdjClaimCRUD"/></xsl:attribute>
      <xsl:attribute name="userID"><xsl:value-of select="$UserId"/></xsl:attribute>
      <xsl:attribute name="lastUpdatedDate"><xsl:value-of select="/Root/Claim/@SysLastUpdatedDate"/></xsl:attribute>
    </IE:APDTab>
    <IE:APDTab id="tabInsured" name="tabInsured" caption="Insured" tabPage="divInsuredDetail" CCTabIndex="1" updStoredProc="uspClaimInsuredUpdDetail|executespnpasxml">
      <!-- <xsl:attribute name="CRUD"><xsl:value-of select="$AdjInsuredCRUD"/></xsl:attribute> -->
      <!-- This tab is disabled for everyone because user can update the insured from here and the vehicle involved tab. This causes the LastUpdate issue when saving.-->
      <xsl:choose>
        <xsl:when test="contains($AdjInsuredCRUD, 'RU') = true() and $InsuredEnabled='true'">
          <xsl:attribute name="CRUD"><xsl:value-of select="$AdjInsuredCRUD"/></xsl:attribute>
        </xsl:when>
        <xsl:when test="contains($AdjInsuredCRUD, 'R') = true()">
          <xsl:attribute name="CRUD">_R__</xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="CRUD">____</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:attribute name="userID"><xsl:value-of select="$UserId"/></xsl:attribute>
      <xsl:attribute name="lastUpdatedDate"><xsl:value-of select="/Root/Claim/Insured/@SysLastUpdatedDate"/></xsl:attribute>
    </IE:APDTab>
    <IE:APDTab id="tabCoverage" name="tabCoverage" caption="Coverage" tabPage="divCoverageDetail" CCTabIndex="1" updStoredProc="uspClaimCoverageUpdDetail|executespnpasxml" onTabAdd="doCoverageAdd()" onTabDelete="doCoverageDelete()">
      <xsl:attribute name="CRUD"><xsl:value-of select="$AdjCoverageCRUD"/></xsl:attribute>
      <xsl:attribute name="userID"><xsl:value-of select="$UserId"/></xsl:attribute>
      <xsl:attribute name="lastUpdatedDate"><xsl:value-of select="/Root/Claim/Coverage/@SysLastUpdatedDate"/></xsl:attribute>
    </IE:APDTab>
    <IE:APDTab id="tabCarrier" name="tabCarrier" caption="Carrier" tabPage="divCarrierDetail" CCTabIndex="1"  onTabAfterSave="updateClaimLastUpdateDate()">
      <xsl:attribute name="CRUD">
        <xsl:choose>
          <xsl:when test="contains($CarrierCRUD, 'R') = true()">_R__</xsl:when>
          <xsl:otherwise>____</xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
    </IE:APDTab>
    
    <IE:APDTabPage name="divClaimDetail" id="divClaimDetail" style="display:none">
        <xsl:call-template name="ClaimDetail">
          <xsl:with-param name="ClaimCRUD" select="$ClaimCRUD"/>
          <xsl:with-param name="pageReadOnly" select="$pageReadOnly"/>
        </xsl:call-template>
    </IE:APDTabPage>

    <IE:APDTabPage name="divInsuredDetail" id="divInsuredDetail" style="display:none">
        <xsl:call-template name="InsuredDetail">
          <xsl:with-param name="InsuredCRUD" select="$InsuredCRUD"/>
          <xsl:with-param name="InsuredEnabled" select="$InsuredEnabled"/>
          <xsl:with-param name="pageReadOnly" select="$pageReadOnly"/>
        </xsl:call-template>
    </IE:APDTabPage>

    <IE:APDTabPage name="divCoverageDetail" id="divCoverageDetail" style="display:none">
        <xsl:call-template name="CoverageDetail">
          <xsl:with-param name="CoverageCRUD" select="$CoverageCRUD"/>
          <xsl:with-param name="pageReadOnly" select="$pageReadOnly"/>
        </xsl:call-template>
    </IE:APDTabPage>

    <IE:APDTabPage name="divCarrierDetail" id="divCarrierDetail" style="display:none">
        <xsl:call-template name="CarrierDetail">
          <xsl:with-param name="CarrierCRUD" select="$CarrierCRUD"/>
          <xsl:with-param name="pageReadOnly" select="$pageReadOnly"/>
        </xsl:call-template>
    </IE:APDTabPage>
  </IE:APDTabGroup>  
  </div>
  
  <div style="position:absolute;top:165px;">
  <xsl:variable name="preselectVeh">
    <xsl:if test="$VehNum != ''">
    <xsl:for-each select="/Root/Vehicle">
      <xsl:sort select="@VehicleNumber" data-type="number" order="ascending"/>
      <xsl:if test="@VehicleNumber = $VehNum">
        <xsl:value-of select="position() - 1"/>
      </xsl:if>
    </xsl:for-each>
    </xsl:if>
    </xsl:variable>
  <IE:APDTabGroup id="tabGrpVehicle" name="tabGrpVehicle" many="true" width="735px" height="345" CCDisabled="false" CCTabIndex="2" showADS="false" >
    <xsl:attribute name="preselectTab">
    <xsl:choose>
      <xsl:when test="$preselectVeh != ''"><xsl:value-of select="$preselectVeh"/></xsl:when>
      <xsl:otherwise>0</xsl:otherwise>
    </xsl:choose>
    </xsl:attribute>
    <xsl:for-each select="/Root/Vehicle">
      <xsl:sort select="@VehicleNumber" data-type="number" order="ascending"/>
      <xsl:variable name="VehName">
          <xsl:choose>
            <xsl:when test="@BusinessName != ''"><xsl:value-of select="@VehicleNumber"/>: <xsl:value-of select="@BusinessName"/></xsl:when>
            <xsl:when test="@NameFirst != '' or @NameLast != ''"><xsl:value-of select="@VehicleNumber"/>: <xsl:value-of select="concat(substring(@NameFirst, 1, 1), '. ', string(@NameLast))"/></xsl:when>
            <xsl:otherwise>Veh <xsl:value-of select="@VehicleNumber"/></xsl:otherwise>
          </xsl:choose>
      </xsl:variable>
      <IE:APDTab width="100">
        <xsl:attribute name="name">tabVehicle_<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
        <xsl:attribute name="id">tabVehicle_<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
        <xsl:attribute name="caption"><xsl:value-of select="$VehName"/></xsl:attribute>
        <xsl:attribute name="CCTabIndex"><xsl:value-of select="position() + 100"/></xsl:attribute>
        <xsl:attribute name="title"><xsl:value-of select="$VehName"/></xsl:attribute>
        <xsl:choose>
          <xsl:when test="@Status = 'Vehicle Closed'">
            <xsl:attribute name="imgSrc">/images/closedExposure.gif</xsl:attribute>
          </xsl:when>
          <xsl:when test="@Status = 'Vehicle Cancelled'">
            <xsl:attribute name="imgSrc">/images/cancelled.gif</xsl:attribute>
          </xsl:when>
          <xsl:when test="@Status = 'Vehicle Voided'">
            <xsl:attribute name="imgSrc">/images/voided.gif</xsl:attribute>
          </xsl:when>
        </xsl:choose>
        <xsl:attribute name="tabPage">divVeh<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
        <xsl:attribute name="onTabSelect">loadVehicle("<xsl:value-of select="@ClaimAspectID"/>")</xsl:attribute>
      </IE:APDTab>
    </xsl:for-each>

    <xsl:for-each select="/Root/Vehicle">
      <xsl:sort select="@VehicleNumber" data-type="number" order="ascending"/>
        <IE:APDTabPage style="display:none">
          <xsl:attribute name="name">divVeh<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
          <xsl:attribute name="id">divVeh<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
          <IFRAME SRC="/blank.asp" style="border:0px solid gray;position:relative;height:100%;width:100%;top:0px;left:0px;background1:transparent;" allowtransparency1="true">
            <xsl:attribute name="name">frmVehDetail<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
            <xsl:attribute name="id">frmVehDetail<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
          </IFRAME>          
        </IE:APDTabPage>
      </xsl:for-each>
  </IE:APDTabGroup>  
  </div>  

<!-- Estimate Summary Frame -->
<IFRAME id="ifrmEstimateSummary" src="blank.asp"  onreadystatechange='top.IFStateChange(this);' style='width:745px; height:100%; top:0px; left:0px; border:0px solid #000000; visibility:hidden; display:none; position:absolute; background1:transparent; overflow:hidden;' allowtransparency='true' >
</IFRAME>

<xml id="xmlCoverages" name="xmlCoverages">
   <Root>
      <xsl:for-each select="/Root/Claim/Coverage">
         <xsl:sort select="@ClientCoverageDesc" data-type="text" order="ascending"/>
         <xsl:variable name="ClientCoverageTypeID"><xsl:value-of select="@ClientCoverageTypeID"/></xsl:variable>
         <xsl:variable name="ClaimCoverageID"><xsl:value-of select="@ClaimCoverageID"/></xsl:variable>
         <Coverage>
            <xsl:copy-of select="./@*"/>
            <xsl:attribute name="AddlCoverageFlagInd">
               <xsl:choose>
                  <xsl:when test="@AddtlCoverageFlag = '1'">/images/tick.gif</xsl:when>
                  <xsl:otherwise>/images/spacer.gif</xsl:otherwise>
               </xsl:choose>            
            </xsl:attribute>
            <xsl:attribute name="AddlCoverageFlagInd">
               <xsl:choose>
                  <xsl:when test="@AddtlCoverageFlag = '1'">/images/tick.gif</xsl:when>
                  <xsl:otherwise>/images/spacer.gif</xsl:otherwise>
               </xsl:choose>            
            </xsl:attribute>
            <xsl:attribute name="ClientCoverageDesc2">
               <xsl:if test="/Root/Reference[@List = 'ClientCoverageType' and @ClientCoverageTypeID = $ClientCoverageTypeID]/@ClientCode != ''">
                  <xsl:value-of select="concat(/Root/Reference[@List = 'ClientCoverageType' and @ClientCoverageTypeID = $ClientCoverageTypeID]/@ClientCode, ' - ')"/>
               </xsl:if>
               <xsl:choose>
                  <xsl:when test="@Description != ''">
                     <xsl:value-of select="@Description"/>
                  </xsl:when>
                  <xsl:when test="@ClientCoverageDesc != ''">
                     <xsl:value-of select="@ClientCoverageDesc"/>
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:value-of select="@SystemDesc"/>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="DeductibleApplied">
               <xsl:value-of select="sum(/Root/CoverageApplied[@ClaimCoverageID=$ClaimCoverageID]/@DeductibleAppliedAmt)"/>
            </xsl:attribute>
            <xsl:attribute name="LimitApplied">
               <xsl:value-of select="sum(/Root/CoverageApplied[@ClaimCoverageID=$ClaimCoverageID]/@LimitAppliedAmt)"/>
            </xsl:attribute>
         </Coverage>
      </xsl:for-each>
   </Root>
</xml>

<xml id="xmlClientCoverageType" name="xmlClientCoverageType">
   <Root>
      <xsl:for-each select="/Root/Reference[@List='ClientCoverageType']">
      <xsl:copy-of select="."/>
      </xsl:for-each>
   </Root>
</xml>
<div id="divMaskAll" name="divMaskAll" style="display:none;position:absolute;top:3px;left:3px;height:100%;width:100%;background-color:#000000;FILTER: progid:DXImageTransform.Microsoft.Alpha( style=0,opacity=25);">
</div>

<div id="divEditCoverage" name="divEditCoverage" style="visibility:hidden;position:absolute;top:100px;left:100px;height:190px;width:250px;background-color:#FFFFFF;padding:3px;border:2px solid #FFA500;background-color:#F5F5DC;FILTER1: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#2F4F4F,strength=5);">
   <table cellspacing="0" cellpadding="2" border="0" style="table-layout:fixed;width:100%;background-color:#FFFFFF;border-collapse:collapse;">
      <colgroup>
         <col width="85px"/>
         <col width="*"/>
      </colgroup>
      <tr>
         <td colspan="2" style="font-weight:bold">
            Edit Coverage
         </td>
      </tr>
      <tr>
         <td colspan="2" style="font-weight:bold">
            <img src="images/background_top.gif" style="height:2px;width:125px"/>
            <input type="hidden" id="txtClaimCoverageID"/>
         </td>
      </tr>
      <tr>
         <td>Client Code:</td>
         <td>
            <IE:APDInput id="txtCoverageClientCode" name="txtCoverageClientCode" class="APDInput" value="" size="5" maxLength="" required="false" canDirty="false" CCDisabled="false" CCTabIndex="-1" onchange=""/>
         </td>
      </tr>
      <tr>
         <td>Description:</td>
         <td>
            <IE:APDCustomSelect id="selClientCoverageDesc" name="selClientCoverageDesc" class="APDCustomSelect" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="150" required="false" width="150" onChange="updateCoverageType()">
               <xsl:for-each select="/Root/Reference[@List='ClientCoverageType']">
               <xsl:sort select="@DisplayOrder" data-type="number" order="ascending"/>
               <xsl:sort select="@Name" data-type="text" order="ascending"/>
               <IE:dropDownItem value="" imgSrc="" style="display:none">
                  <xsl:attribute name="value"><xsl:value-of select="@ClientCoverageTypeID"/></xsl:attribute>
                  <xsl:value-of select="concat(@Name, ' [', @ClientCode, ']')"/>
               </IE:dropDownItem>
               </xsl:for-each>
            </IE:APDCustomSelect>
         </td>
      </tr>
      <tr>
         <td>Type:</td>
         <td>
            <IE:APDInput id="txtCoverageType" name="txtCoverageType" class="APDInput" value="" size="5" maxLength="" required="false" canDirty="false" CCDisabled="false" CCTabIndex="-1" onchange="refreshCoverage()"/>
         </td>
      </tr>
      <tr>
         <td>Additional Flag:</td>
         <td>
            <IE:APDLabel id="txtAdditionalFlag" name="txtAdditionalFlag" class="APDLabel" value="" width="150" CCDisabled="false" />
         </td>
      </tr>
      <tr>
         <td colspan="2">
            <div id="divCoverageNonRental" style="display1:none">
               <table cellspacing="0" cellpadding="2" border="0" style="table-layout:fixed;width:100%;background-color:#FFFFFF;border-collapse:collapse;">
                  <colgroup>
                     <col width="85px"/>
                     <col width="*"/>
                  </colgroup>
                  <tr>
                     <td>Deductible:</td>
                     <td>
                        <IE:APDInputCurrency id="txtCoverageDeductible" name="txtCoverageDeductible" class="APDInputCurrency" value="" precision="8" scale="2" required="false" canDirty="false" CCDisabled="false" CCTabIndex="151" onChange="" />                        
                     </td>
                  </tr>
                  <tr>
                     <td>Limit:</td>
                     <td>
                        <IE:APDInputCurrency id="txtCoverageLimit" name="txtCoverageLimit" class="APDInputCurrency" value="" precision="8" scale="2" required="false" canDirty="false" CCDisabled="false" CCTabIndex="152" onChange="" />
                     </td>
                  </tr>
               </table>
            </div>
            <div id="divCoverageRental" style="display:none">
               <table cellspacing="0" cellpadding="2" border="0" style="table-layout:fixed;width:100%;background-color:#FFFFFF;border-collapse:collapse;">
                  <colgroup>
                     <col width="85px"/>
                     <col width="*"/>
                  </colgroup>
                  <tr>
                     <td>Maximum Days:</td>
                     <td>                        
                        <IE:APDInputNumeric id="txtCoverageMaxDays" name="txtCoverageMaxDays" class="APDInputNumeric" value="" width="" precision="4" scale="0" neg="false" int="true" required="false" canDirty="false" CCDisabled="false" CCTabIndex="153" onChange="" />
                     </td>
                  </tr>
                  <tr>
                     <td>Limit:</td>
                     <td>                        
                        <IE:APDInputCurrency id="txtCoverageDailyLimit" name="txtCoverageDailyLimit" class="APDInputCurrency" value="" precision="8" scale="2" required="false" canDirty="false" CCDisabled="false" CCTabIndex="154" onChange="" />
                     </td>
                  </tr>
               </table>
            </div>
         </td>
      </tr>
      <tr>
         <td colspan="2">
            <IE:APDButton id="btnCovSave" name="btnCovSave" class="APDButton" value="Save" width="50" CCDisabled="false" CCTabIndex="155" onButtonClick="saveCoverage()"/>
            <IE:APDButton id="btnCovCancel" name="btnCovCancel" class="APDButton" value="Cancel" width="50" CCDisabled="false" CCTabIndex="156" onButtonClick="cancelCoverage()"/>
         </td>
      </tr>
   </table>
</div>

<div id="divAddServiceChannel" name="divAddServiceChannel" style="visibility:hidden;position:absolute;top:100px;left:100px;height:100px;width:250px;background-color:#FFFFFF;padding:3px;border:2px solid #FFA500;background-color:#F5F5DC;FILTER1: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#2F4F4F,strength=5);">
   <table cellspacing="0" cellpadding="2" border="0" style="table-layout:fixed;width:100%;background-color:#FFFFFF;border-collapse:collapse;">
      <colgroup>
         <col width="85px"/>
         <col width="*"/>
      </colgroup>
      <tr>
         <td colspan="2" style="font-weight:bold">
            Add New Service Channel
         </td>
      </tr>
      <tr>
         <td colspan="2" style="font-weight:bold">
            <img src="images/background_top.gif" style="height:2px;width:125px"/>
         </td>
      </tr>
      <tr>
         <td>Service Channel:</td>
         <td>
            <IE:APDCustomSelect id="selClientServiceChannel" name="selClientServiceChannel" class="APDCustomSelect" blankFirst="false" canDirty="false" CCDisabled="false" CCTabIndex="150" required="false" width="150" onChange="updateCoverageType()">
            </IE:APDCustomSelect>            
         </td>
      </tr>
      <tr>
         <td colspan="2">
          <IE:APDCheckBox id="chkPrimaryServiceChannel" name="chkPrimaryServiceChannel" class="APDCheckBox" caption="Primary" value="0" CCDisabled="false" required="false" CCTabIndex="" alignment="" canDirty="false" onBeforeChange="" onChange="" onAfterChange="" />
         </td>
      </tr>
      <tr style="height:10px;">
         <td colspan="2"></td>
      </tr>
      <tr>
         <td colspan="2">
            <IE:APDButton id="btnSCAdd" name="btnSCAdd" class="APDButton" value="Add" width="50" CCDisabled="false" CCTabIndex="153" onButtonClick="saveServiceChannel()"/>
            <IE:APDButton id="btnSCCancel" name="btnSCCancel" class="APDButton" value="Cancel" width="50" CCDisabled="false" CCTabIndex="154" onButtonClick="cancelServiceChannel()"/>
         </td>
      </tr>
   </table>
</div>

</BODY>
</HTML>
</xsl:template>

<xsl:template name="ClaimDetail">
  <xsl:param name="ClaimCRUD"/>
  <xsl:param name="pageReadOnly"/>
  <xsl:variable name="dataDisabled">
    <xsl:choose>
      <xsl:when test="contains($ClaimCRUD, 'U') = true() and $pageReadOnly = 'false'">false</xsl:when>
      <xsl:otherwise>true</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <IE:APDInput id="LynxID" name="LynxID" canDirty="false" style="display:none">
    <xsl:attribute name="value"><xsl:value-of select="/Root/Claim/@LynxID"/></xsl:attribute>
  </IE:APDInput>
  <table cellpadding="1" cellspacing="0" border="0" style="table-layout:fixed">
    <colgroup>
      <col width="100px"/>
      <col width="3px"/>
      <col width="275px"/>
      <col width="10px"/>
      <col width="90px"/>
      <col width="3px"/>
      <col width="*"/>
    </colgroup>
    <tr>
      <td unselectable="on">Policy:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('PolicyNumber', /Root/Claim, 'PolicyNumber', '', string($dataDisabled), 1, 'true', 'true', '')"/>
      </td>
      <td unselectable="on"></td>
      <td unselectable="on">Submitted By:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:variable name="submittedBy"><xsl:value-of select="concat(string(/Root/Claim/@IntakeUserFirstName), ' ' ,string(/Root/Claim/@IntakeUserLastName), '(', string(/Root/Claim/@IntakeUserCompany), ')')"/></xsl:variable>
        <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtSubmittedBy', string($submittedBy), '', 250, string($dataDisabled))"/>
      </td>
    </tr>
    <tr>
      <td unselectable="on">Client Claim #:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('ClientClaimNumber', /Root/Claim, 'ClientClaimNumber', '', string($dataDisabled), 2, 'true', 'true', 'ClientClaimNumberSquished.value = ToAlphaNumeric(this.value)', 'true', 'setDirty()')"/>
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('ClientClaimNumberSquished', /Root/Claim, 'ClientClaimNumberSquished', '', 'false', 2, 'false', 'true', '', 'true', 'setDirty()')"/>
      </td>
      <td unselectable="on"></td>
      <td unselectable="on">Submitted Date:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:variable name="submittedDate"><xsl:value-of select="js:formatSQLDateTime(string(/Root/Claim/@IntakeFinishDate))"/></xsl:variable>
        <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtSubmittedDate', string($submittedDate), '', 130, string($dataDisabled))"/>
      </td>
    </tr>
    <tr>
      <td unselectable="on">Date of Loss:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:variable name="LossDate"><xsl:value-of select="js:formatSQLDateTime(string(/Root/Claim/@LossDate))"/></xsl:variable>
        <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('txtLossDate', string($LossDate), '', 130, string($dataDisabled))"/>
      </td>
      <td unselectable="on"></td>
      <td unselectable="on">Claim Remarks:</td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
    </tr>
    <tr>
      <td unselectable="on">State:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('LossState', /Root/Claim, 'LossState', 'State', 'true', '6', string($dataDisabled), 2, 'true', '', '125', '', 'false', '')"/>
      </td>
      <td unselectable="on"></td>
      <td unselectable="on" colspan="2" rowspan="2">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDTextArea('Remarks', /Root/Claim, 'Remarks', 62, 310, string($dataDisabled), 3, 'true', 'true', '')"/>
      </td>
      <td unselectable="on"></td>
    </tr>
    <tr valign="top">
      <td unselectable="on">Loss Description:
		  <br/><br/>
		  <IE:APDButton id="btnClaimSave" name="btnClaimSave" class="APDButton" value="Save" CCDisabled="false" onButtonClick="newSave('divClaimDetail')"/>
	  </td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDTextArea('LossDescription', /Root/Claim, 'LossDescription', 43, 265, string($dataDisabled), 3, 'true', 'true', '')"/>
      </td>
      <td unselectable="on"></td>
      <!-- <td unselectable="on" colspan="2">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDTextArea('Remarks', /Root/Claim, 'Remarks', 55, 310, string($dataDisabled), 3, 'true', 'true', '')"/>
      </td>
      <td unselectable="on"></td> -->

	<tr style="position:absolute;top:0px;left:0px;display:none">
      <td unselectable="on" >SessionKey</td>
	<td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:APDTextArea('SessionKey', string($SessionKey), 43, 265, string($dataDisabled), 3, 'true', 'true', '')"/>
        </td>

	<td unselectable="on">WindowID</td>
	<td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:APDTextArea('WindowID', string($WindowID), 43, 265, string($dataDisabled), 3, 'true', 'true', '')"/>
        </td>   
    </tr>

    </tr>
  </table>
</xsl:template>

<xsl:template name="InsuredDetail">
  <xsl:param name="InsuredCRUD"/>
  <xsl:param name="InsuredEnabled"/>
  <xsl:param name="pageReadOnly"/>
  <xsl:variable name="dataDisabled">
    <xsl:choose>
      <xsl:when test="$InsuredEnabled='true'">false</xsl:when>
      <xsl:when test="contains($InsuredCRUD, 'U') = true() and $pageReadOnly = 'false'">true</xsl:when>
      <xsl:otherwise>true</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <IE:APDInput id="InvolvedID" name="InvolvedID" canDirty="false" style="display:none">
    <xsl:attribute name="value"><xsl:value-of select="/Root/Claim/Insured/@InvolvedID"/></xsl:attribute>
  </IE:APDInput>
  <table cellpadding="1" cellspacing="0" border="0" style="table-layout:fixed;border-collapse:collapse">
    <colgroup>
      <col width="50px"/>
      <col width="3px"/>
      <col width="235px"/>
      <col width="10px"/>
      <col width="100px"/>
      <col width="3px"/>
      <col width="110px"/>
      <col width="10px"/>
      <col width="40px"/>
      <col width="3px"/>
      <col width="*"/>
    </colgroup>
    <tr>
      <td unselectable="on">Name:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td style="padding-right:2px;">
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('NameTitle', /Root/Claim/Insured, 'NameTitle', '25', string($dataDisabled), 1, 'true', 'true', '')"/>
            </td>
            <td style="padding-right:2px;">
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('NameFirst', /Root/Claim/Insured, 'NameFirst', '90', string($dataDisabled), 2, 'true', 'true', '')"/>
            </td>
            <td style="padding-right:2px;">
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('NameLast', /Root/Claim/Insured, 'NameLast', '94', string($dataDisabled), 3, 'true', 'true', '')"/>
            </td>
          </tr>
        </table>
      </td>
      <td unselectable="on"></td>
      <td unselectable="on">E-mail:</td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('EmailAddress', /Root/Claim/Insured, 'EmailAddress', '225', string($dataDisabled), 10, 'true', 'true', 'checkEMail()')"/>
      </td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
    </tr>
    <tr>
      <td unselectable="on">Business:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('BusinessName', /Root/Claim/Insured, 'BusinessName', '225', string($dataDisabled), 4, 'true', 'true', '')"/>
      </td>
      <td unselectable="on"></td>
      <td unselectable="on">SSN/EIN:</td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('FedTaxID', /Root/Claim/Insured, 'FedTaxId', '75', string($dataDisabled), 10, 'true', 'true', '')"/>
      </td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
    </tr>
    <tr>
      <td unselectable="on">Address:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('Address1', /Root/Claim/Insured, 'Address1', '225', string($dataDisabled), 5, 'true', 'true', '')"/>
      </td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
    </tr>
    <tr>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('Address2', /Root/Claim/Insured, 'Address2', '225', string($dataDisabled), 6, 'true', 'true', '')"/>
      </td>
      <td unselectable="on"></td>
      <td unselectable="on">Best Number to Call:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('BestContactPhoneCD', /Root/Claim/Insured, 'BestContactPhoneCD', 'BestContactPhone', 'false', '', string($dataDisabled), 11, 'true', '', '', '', '', '')"/>
      </td>
      <td unselectable="on"></td>
      <td unselectable="on">Day:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('Day', /Root/Claim/Insured, 'DayAreaCode', 'DayExchangeNumber', 'DayUnitNumber', 'DayExtensionNumber', '', 'true', string($dataDisabled), 13, 'true', 'false', 'true', '')"/>
      </td>
    </tr>
    <tr>
      <td unselectable="on">City:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('AddressCity', /Root/Claim/Insured, 'AddressCity', '', string($dataDisabled), 7, 'true', 'true', '')"/>
      </td>
      <td unselectable="on"></td>
      <td unselectable="on">Best Time to Call:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('BestContactTime', /Root/Claim/Insured, 'BestContactTime', '100', string($dataDisabled), 12, 'true', 'true', '')"/>
      </td>
      <td unselectable="on"></td>
      <td unselectable="on">Night:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('Night', /Root/Claim/Insured, 'NightAreaCode', 'NightExchangeNumber', 'NightUnitNumber', 'NightExtensionNumber', '', 'true', string($dataDisabled), 14, 'true', 'false', 'true', '')"/>
      </td>
    </tr>
    <tr>
      <td unselectable="on">State:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDCustomSelect('AddressState', /Root/Claim/Insured, 'AddressState', 'State', 'false', '6', string($dataDisabled), 8, 'true', '', '125', '', 'false', '')"/>
            </td>
            <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Zip:</td>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInput('AddressZip', /Root/Claim/Insured, 'AddressZip', '75', string($dataDisabled), 9, 'true', 'true', 'ValidateZip(AddressZip, AddressCity, AddressState)')"/>
            </td>
          </tr>
        </table>
      </td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on"></td>
      <td unselectable="on">Alt.:</td>
      <td unselectable="on"></td>
      <td unselectable="on">
        <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDInputPhone('Alternate', /Root/Claim/Insured, 'AlternateAreaCode', 'AlternateExchangeNumber', 'AlternateUnitNumber', 'AlternateExtensionNumber', '', 'true', string($dataDisabled), 15, 'true', 'false', 'true', '')"/>
      </td>
    </tr>
  </table>  
</xsl:template>

<xsl:template name="CoverageDetail">
  <xsl:param name="CoverageCRUD"/>
  <xsl:param name="pageReadOnly"/>
  <xsl:variable name="dataDisabled">
    <xsl:choose>
      <xsl:when test="contains($CoverageCRUD, 'U') = true() and $pageReadOnly = 'false'">false</xsl:when>
      <xsl:otherwise>true</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <IE:APDInput id="LynxID" name="LynxID" canDirty="false" style="display:none">
    <xsl:attribute name="value"><xsl:value-of select="/Root/Claim/@LynxID"/></xsl:attribute>
  </IE:APDInput>

   <div id="divGridHolder" style="height:100px;overflow:hidden">
     <IE:APDDataGrid id="GrdCoverage" CCDataSrc="xmlCoverages" CCDataPageSize="4" showAlternateColor="false" altColor="#E6E6FA" CCWidth="723px" CCHeight="108" showHeader="true" gridLines="horizontal" keyField="" onrowselect="" onBeforeRowSelect1="" onclick="divGridHolder.focus()">
         <IE:APDDataGridHeader>
             <IE:APDDataGridTR>
                 <IE:APDDataGridTD width="225px" caption="Description" fldName="ClientCoverageDesc2" DataType="" sortable="true" CCStyle=""/>
                 <IE:APDDataGridTD width="60px" caption="Type" fldName="CoverageTypeCD" DataType="" sortable="true" CCStyle=""/>
                 <IE:APDDataGridTD width="50px" caption="Addl." fldName="AddlCoverageFlag" DataType="number" sortable="true" CCStyle="text-align:center"/>
                 <IE:APDDataGridTD width="75px" caption="Deductible" fldName="DeductibleAmt" DataType="number" sortable="true" CCStyle="text-align:right"/>
                 <IE:APDDataGridTD width="100px" caption="Limit" fldName="LimitAmt" DataType="number" sortable="true" CCStyle="text-align:right"/>
                 <IE:APDDataGridTD width="75px" caption="Max. Days" fldName="MaximumDays" DataType="number" sortable="true" CCStyle="text-align:right"/>
                 <IE:APDDataGridTD width="65px" caption="Daily Limit" fldName="LimitDailyAmt" DataType="number" sortable="true" CCStyle="text-align:right"/>
             </IE:APDDataGridTR>
         </IE:APDDataGridHeader>
         <IE:APDDataGridBody>
              <IE:APDDataGridTR ondblclick="editCoverage()">
                  <IE:APDDataGridTD>
                     <IE:APDDataGridFld fldName="ClientCoverageDesc2" />
                  </IE:APDDataGridTD>
                  <IE:APDDataGridTD>
                     <IE:APDDataGridFld fldName="CoverageTypeCD" />
                  </IE:APDDataGridTD>
                  <IE:APDDataGridTD>
                     <IE:APDDataGridFld fldName="AddlCoverageFlagInd" displayType="image"/>
                  </IE:APDDataGridTD>
                  <IE:APDDataGridTD>
                    <IE:APDDataGridFld fldName="DeductibleAmt" />
                  </IE:APDDataGridTD>
                  <IE:APDDataGridTD>
                    <IE:APDDataGridFld fldName="LimitAmt" />
                  </IE:APDDataGridTD>
                  <IE:APDDataGridTD>
                    <IE:APDDataGridFld fldName="MaximumDays" />
                  </IE:APDDataGridTD>
                  <IE:APDDataGridTD>
                    <IE:APDDataGridFld fldName="LimitDailyAmt" />
                  </IE:APDDataGridTD>
              </IE:APDDataGridTR>
         </IE:APDDataGridBody>
     </IE:APDDataGrid>   
   </div>
   
  <table>
  <tr>
	<td>
		<IE:APDButton id="btnCovAdd" name="btnCovAdd" class="APDButton" value="Add" CCDisabled="false" onButtonClick="doCoverageAdd()"/>
	</td>
	<td>
		<IE:APDButton id="btnCovDelete" name="btnCovDelete" class="APDButton" value="Del" CCDisabled="false" onButtonClick="doCoverageDelete()"/>
	</td>
	<td>
		<IE:APDButton id="btnCovSave" name="btnCovSave" class="APDButton" value="Save" CCDisabled="false" onButtonClick="newSave('divCoverage')"/>
	</td>
  </tr>
  </table>
</xsl:template>

<xsl:template name="CarrierDetail">
  <xsl:param name="CarrierCRUD"/>
  <xsl:param name="pageReadOnly"/>
  <IE:APDInput id="CarrierUserID" name="CarrierUserID" canDirty="false" style="display:none">
    <xsl:attribute name="value"><xsl:value-of select="/Root/Claim/Carrier/@UserID"/></xsl:attribute>
  </IE:APDInput>
  <table cellpadding="1" cellspacing="0" border="0" style="table-layout:fixed;border-collapse:collapse;">
    <colgroup>
      <col width="50px"/>
      <col width="*"/>
    </colgroup>
    <tr>
      <td unselectable="on">Name:</td>
      <td unselectable="on">
        <table border="0" cellpadding="1" cellspacing="0">
          <tr>
            <td>
              <xsl:if test="boolean(contains($CarrierCRUD,'R'))">
                     <IMG src="/images/next_button.gif" onClick="ShowClaimCarrierRep('visible')" width="19" height="19" border="0" align="absmiddle" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)" tabindex="1">
                  <xsl:if test="not(contains($CarrierCRUD, 'U'))">
                    <xsl:attribute name="disabled"/>
                    <xsl:attribute name="readonly"/>
                    <xsl:attribute name="style">display:none</xsl:attribute>
                  </xsl:if>
                </IMG>
              </xsl:if>  
            </td>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('CarrierNameTitle', /Root/Claim/Carrier, 'NameTitle', '', 30, 'false')"/>
            </td>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('CarrierNameFirst', /Root/Claim/Carrier, 'NameFirst', '', 150, 'false')"/>
            </td>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('CarrierNameLast', /Root/Claim/Carrier, 'NameLast', '', 208, 'false')"/>
            </td>
             <xsl:if test="/Root/Claim/Carrier/@ActiveFlag = '0'">
                <td style="font-weight:bold;color:#FF0000">(Inactive Adjuster)</td>
             </xsl:if>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td unselectable="on">Office:</td>
      <td unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('CarrierOffice', /Root/Claim/Carrier, 'OfficeName', '', 414, 'false')"/>
      </td>
    </tr>
    <tr>
      <td unselectable="on"><a id="aEmail" name="aEmail" href="Javascript:emailCarrier()" Title="Click to E-mail Carrier Representative" style="text-decoration:underline;" tabIndex="2">E-mail</a>:</td>
      <td unselectable="on">
          <xsl:value-of disable-output-escaping="yes" select="js:XSL_APDLabel('CarrierEmailAddress', /Root/Claim/Carrier, 'EmailAddress', '', 414, 'false')"/>
      </td>
    </tr>
    <tr>
      <td unselectable="on">Phone:</td>
      <td unselectable="on">
        <xsl:variable name="CarrierPhone">(<xsl:value-of select="/Root/Claim/Carrier/@PhoneAreaCode"/>) <xsl:value-of select="/Root/Claim/Carrier/@PhoneExchangeNumber"/>-<xsl:value-of select="/Root/Claim/Carrier/@PhoneUnitNumber"/> x<xsl:value-of select="/Root/Claim/Carrier/@PhoneExtensionNumber"/></xsl:variable>
        <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('CarrierPhone', string($CarrierPhone), '', 150, 'false')"/>
      </td>
    </tr>
    <tr>
      <td unselectable="on">Fax:</td>
      <td unselectable="on">
        <xsl:variable name="CarrierFax">(<xsl:value-of select="/Root/Claim/Carrier/@FaxreaCode"/>) <xsl:value-of select="/Root/Claim/Carrier/@FaxExchangeNumber"/>-<xsl:value-of select="/Root/Claim/Carrier/@FaxUnitNumber"/> x<xsl:value-of select="/Root/Claim/Carrier/@FaxExtensionNumber"/></xsl:variable>
        <xsl:value-of disable-output-escaping="yes" select="js:APDLabel('CarrierFax', string($CarrierFax), '', 150, 'false')"/>
      </td>
    </tr>
  </table>  
</xsl:template>
</xsl:stylesheet>