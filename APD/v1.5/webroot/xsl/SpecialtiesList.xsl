<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    id="SpecialtiesList">

<xsl:output method="html" indent="yes" encoding="UTF-8" />
<xsl:template match="/Root">
    <table cellpadding="2"  cellspacing="0" bordercolor="#cccccc" border="1" cols="2" rules="ALL" WIDTH="100%" nowrap="on">
    <xsl:for-each select="Specialty">
        <tr>
			<xsl:attribute name="bgcolor">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 1">
						<xsl:text>#fff7e5</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>white</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
            <td width="10%">
				<input type="checkbox" id="chkSpec" name="chkSpec">
					<xsl:attribute name="value">
						<xsl:value-of select="@SpecialtyID"/>
						<xsl:text>|</xsl:text>
						<xsl:value-of select="@SysLastUpdatedDate"/>
					</xsl:attribute>
				</input>
            </td>
            <td width="90%">
				<xsl:value-of select="@Name"/>
			</td>
        </tr>
    </xsl:for-each>
    </table>
</xsl:template>

</xsl:stylesheet>



