<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTWebSignupsSearchResults">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTShopLoadGetDetailXML,SMTWebSignupInfo.xsl, 1   -->

<HEAD>
  <TITLE>Search Results</TITLE>
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
  <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
  </script>

</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" style="background:transparent;">

  
<form id="frmShop" name="frmShop">
  
  <!-- Get Shop Info -->
  <table width="800" border="0" cellpadding="0" cellspacing="0" style="align:right;">
    <tr>
      <td width="100%"></td>
      <td align="right" nowrap=""><p class="bodyBlue"> All fields marked with an asterisk (*) are required.</p></td>
    </tr>
  </table>
  
  <strong>Business Information:</strong>
  <xsl:apply-templates select="ShopLoad"/>
  <br/>
  
  <strong>Shop Information:</strong>
  <xsl:apply-templates select="ShopLoad"/>
  <br/><br/>
  
  <strong>Business Principal / Owner:</strong>
  <xsl:apply-templates select="ShopLoad"/>
  <br/><br/>
  
  <strong>Shop Contact:</strong>
  <xsl:apply-templates select="ShopLoad"/>
  <br/><br/>

  <strong>Shop Pricing:</strong>
  <xsl:apply-templates select="ShopLoad"/>
  <br/>

  <xsl:apply-templates select="ShopLoad"/>
  <br/>
  
  
</form>

</BODY>
</HTML>      

</xsl:template>



<xsl:template match="ShopLoad">
  <input type="hidden" id="txtBusinessInfoID" name="BusinessInfoID">
    <xsl:attribute name="value"><xsl:value-of select="@ShopID"/></xsl:attribute>
  </input>
  
  <input type="hidden" id="txtShopID" name="ShopID">
    <xsl:attribute name="value"><xsl:value-of select="@ShopLocationID"/></xsl:attribute>
  </input>
  
  <input type="hidden" id="txtCertifiedFirstID" name="CertifiedFirstID">
    <xsl:attribute name="value"><xsl:value-of select="@CertifiedFirstID"/></xsl:attribute>
  </input>

  <input type="hidden" id="txtFinalSubmitFlag" name="FinalSubmitFlag">
    <xsl:attribute name="value"><xsl:value-of select="@FinalSubmitFlag"/></xsl:attribute>
  </input>
  
  <table border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td><p class="bodyBlue"> All fields marked with an asterisk (*) are required.</p></td>
    </tr>
  </table>
  <br/><br/>

  <table border="0" cellpadding="0" cellspacing="2" style="border:2px solid #666699; padding:4px">
    <tr>
      <td colspan="3" valign="top"><span style="font-weight:bold">Program Shop Administrator</span></td>
    </tr>
    <tr>
      <td valign="top"><span style="font-size:13px">Estimate Comm. Method: *</span></td>
      <td>
        <select id="selCommMethod" name="PreferredCommunicationMethodID" class="inputFld">
          <option></option>
          <xsl:for-each select="/Root/Reference[@ListName='CommunicationMethod']">
            <xsl:call-template name="BuildSelectOptions">
              <xsl:with-param name="current" select="/Root/ShopInfo/@PreferredCommunicationMethodID"/>
            </xsl:call-template>
          </xsl:for-each>
        </select>
      </td>
        <td rowspan="4" valign="top" nowrap="on">
          <span style="font-size:13px">Comments:</span>
          <span id="commShopAdmin" style="color:#000000; font-weight:normal; font-size:8pt">(max. 250 characters)</span><br/>
          <textarea id="txtAdminComments" name="AdminComments" rows="4" cols="55" onKeyDown="CheckInputLength(this, 250)"><xsl:value-of select="@AdminComments"/></textarea>
        </td>
      </tr>
      <tr>
        <td valign="top"><span style="font-size:13px">Estimate Comm. Address: *</span></td>
        <td>
          <input type="text" id="txtCommAddress" name="CommAddress" size="10" class="inputFld" maxlength="30">
	        <xsl:attribute name="value"><xsl:value-of select="@CommAddress"/></xsl:attribute>
	      </input> 
        </td>
      </tr>
      <tr>
        <td valign="top"><span style="font-size:13px">Program Manager: *</span></td>
        <td>
          <select id="selProgManager" name="ProgramManagerID" class="inputFld">
            <option></option>
            <xsl:for-each select="/Root/Reference[@ListName='ProgramManager']">
              <xsl:call-template name="BuildSelectOptions">
              <  xsl:with-param name="current" select="/Root/ShopInfo/@ProgramManagerID"/>
              </xsl:call-template>
            </xsl:for-each>
          </select>
      </td>
    </tr>
    <tr>
      <td valign="top"><span style="font-size:13px">Data Review Status:</span></td>
      <td>
        <select id="selDataReviewStatus" name="DataReviewStatusCD" class="inputFld">
          <option></option>
	        <xsl:for-each select="/Root/Reference[@ListName='DataReviewStatus']">
           <xsl:call-template name="BuildSelectOptions">
             <xsl:with-param name="current" select="/Root/ShopInfo/@DataReviewStatusCD"/>
           </xsl:call-template>
          </xsl:for-each>
        </select>
      </td>
    </tr>
  </table>
</xsl:template>


<xsl:template name="BusinessInfo">
  <table>
    <tr>
      <td class="bodyBlue" nowrap="on"><em>CertifiedFirst</em> Network ID:</td>
      <td><span style="font-size:15px"><xsl:value-of select="/Root/ShopInfo/@CertifiedFirstID"/></span></td>
      <td><img src="images/spacer.gif" width="20px"/></td>
      <td class="bodyBlue" nowrap="on">Federal Tax ID: *</td>
      <td>
        <input type="text" id="txtFedTaxID" name="FedTaxID" size="15" class="inputFld" maxlength="15">
         	<xsl:attribute name="onkeypress">NumbersOnly(event);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="@FedTaxID"/></xsl:attribute>
        </input>
      </td>
      <td><img src="images/spacer.gif" width="20px"/></td>
      <td class="bodyBlue" nowrap="on">Business Type: *</td>
      <td>
        <select id="selBusinessTypeCD" name="BusinessTypeCD" class="inputFld">
          <option></option>
          <xsl:for-each select="/Root/Reference[@ListName='BusinessType']">
            <xsl:call-template name="BuildSelectOptions">
              <xsl:with-param name="current" select="/Root/BusinessInfo/@BusinessTypeCD"/>
            </xsl:call-template>
          </xsl:for-each>
        </select>
      </td>
    </tr>
  </table>
</xsl:template>

<xsl:template name="ShopInfo">
  <TABLE cellSpacing="0" cellPadding="0" border="0">  <!-- master table -->
    <TR>
      <TD rowspan="2" valign="top"><!-- column 1 : shop info -->
        <TABLE border="0" cellspacing="0" cellpadding="1">
	        <TR>
		        <TD class="bodyBlue">Name: *</TD>
            <TD nowrap="nowrap">
		          <input type="text" id="txtName" name="Name" autocomplete="off" size="35" class="inputFld" maxlength="50">
			          <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
			        </input>
		        </TD>
          </TR>
          <TR>
            <TD class="bodyBlue">Address1: *</TD>
            <TD>
              <input type="text" id="txtAddress1" name="Address1" size="35" class="inputFld" maxlength="50">
			          <xsl:attribute name="value"><xsl:value-of select="@Address1"/></xsl:attribute>
			        </input>
            </TD>
		      </TR>
          <TR>
            <TD class="bodyBlue">Address2:</TD>
            <TD>
              <input type="text" id="txtAddress2" name="Address2" size="35" class="inputFld" maxlength="50">
			          <xsl:attribute name="value"><xsl:value-of select="@Address2"/></xsl:attribute>
			        </input> 
            </TD>
          </TR>
          <TR>
		        <TD class="bodyBlue">City: *</TD>
            <TD>
              <input type="text" id="txtAddressCity" name="AddressCity" size="20" class="inputFld" maxlength="30">
			          <xsl:attribute name="value"><xsl:value-of select="@AddressCity"/></xsl:attribute>
			        </input>
            </TD>
          </TR>
	        <TR>
	          <TD class="bodyBlue">State: *</TD>
            <TD nowrap="nowrap">
              <select id="selState" name="AddressState" class="inputFld">
                <option></option>
			          <xsl:for-each select="/Root/Reference[@ListName='State']">
			          <xsl:call-template name="BuildSelectOptions">
			            <xsl:with-param name="current"><xsl:value-of select="/Root/ShopInfo/@AddressState"/></xsl:with-param>
			          </xsl:call-template>
			        </xsl:for-each>
            </select>
		      </TD>
        </TR>
          <TR>
		        <TD class="bodyBlue">Zip: *</TD>
            <TD noWrap="nowrap">
              <input type="text" id="txtAddressZip" name="AddressZip" size="6" class="inputFld" onkeypress="NumbersOnly(event)" maxlength="5">
			          <xsl:attribute name="value"><xsl:value-of select="@AddressZip"/></xsl:attribute>
			        </input>
            </TD>
          </TR>
	      <TR>
	        <TD class="bodyBlue">County:</TD>
            <TD>
              <input type="text" id="txtAddressCounty" name="AddressCounty" size="20" class="inputFld" maxlength="30">
			          <xsl:attribute name="value"><xsl:value-of select="@AddressCounty"/></xsl:attribute>
			        </input>
            </TD>
	        </TR>
          <TR>
	          <TD class="bodyBlue">Phone: *</TD>
            <TD class="bodyBlue" noWrap="nowrap">
		          <xsl:call-template name="ContactNumbers"><xsl:with-param name="ContactMethod">Phone</xsl:with-param></xsl:call-template>
		        </TD>
          </TR>
          <TR>
            <TD class="bodyBlue">Fax: *</TD>
            <TD class="bodyBlue" noWrap="nowrap">
              <xsl:call-template name="ContactNumbers"><xsl:with-param name="ContactMethod">Fax</xsl:with-param></xsl:call-template>
            </TD>
            <TD noWrap="nowrap"></TD>
          </TR>
          <TR>
            <TD class="bodyBlue">Email: *</TD>
            <TD noWrap="nowrap">
              <input type="text" id="txtEmailAddress" name="EmailAddress" size="35" class="inputFld" onbeforedeactivate="checkEMail(this)" maxlength="50">
			          <xsl:attribute name="value"><xsl:value-of select="@EmailAddress"/></xsl:attribute>
			        </input>
            </TD>
            <TD noWrap="nowrap"></TD>
          </TR>
          <TR>
            <TD class="bodyBlue" noWrap="nowrap">Web Site:</TD>
            <TD noWrap="nowrap">
              <input type="text" id="txtWebSiteAddress" name="WebSiteAddress" size="35" class="inputFld" maxlength="256">
			          <xsl:attribute name="value"><xsl:value-of select="@WebSiteAddress"/></xsl:attribute>
			        </input>
            </TD>
            <TD noWrap="nowrap"></TD>
          </TR>
	      </TABLE>
      </TD><!-- end of column 1 -->
      <TD rowspan="2" noWrap="nowrap"><img src="images/spacer.gif" width="20px"/></TD> <!-- spacer column -->
      <TD valign="top"> <!-- column 2 Shop Program info -->
        <TABLE border="0" cellspacing="0" cellpadding="1">
          <TR>
            <TD class="bodyBlue" nowrap="nowrap">Repair Facility Type: *</TD>
            <TD>
              <select id="selRepairFacilityType" name="RepairFacilityTypeCD" class="inputFld">
                <option></option>
                <xsl:for-each select="/Root/Reference[@ListName='RepairFacilityType']">
			            <xsl:call-template name="BuildSelectOptions">
			              <xsl:with-param name="current" select="/Root/ShopInfo/@RepairFacilityTypeCD"/>
			            </xsl:call-template>
		            </xsl:for-each>
              </select>
            </TD>
          </TR>
          <TR>
            <TD class="bodyBlue" nowrap="nowrap">Workmanship Warranty: *</TD>
            <TD class="bodyBlue">
              <select id="selWorkmanshipWarranty" name="WarrantyPeriodWorkmanshipCD" class="inputFld">
                <option></option>
                <xsl:for-each select="/Root/Reference[@ListName='WarrantyPeriod']">
			            <xsl:call-template name="BuildSelectOptions">
			              <xsl:with-param name="current" select="/Root/ShopInfo/@WarrantyPeriodWorkmanshipCD"/>
			            </xsl:call-template>
		            </xsl:for-each>
              </select>
            </TD>
          </TR>
		      <TR>
            <TD class="bodyBlue" nowrap="nowrap">Refinish Warranty: *</TD>
            <TD>
              <select id="selRefinishWarranty" name="WarrantyPeriodRefinishCD" class="inputFld">
                <option></option>
                <xsl:for-each select="/Root/Reference[@ListName='WarrantyPeriod']">
			            <xsl:call-template name="BuildSelectOptions">
			              <xsl:with-param name="current" select="/Root/ShopInfo/@WarrantyPeriodRefinishCD"/>
			            </xsl:call-template>
		            </xsl:for-each>
              </select>
            </TD>
          </TR>
          <TR>
            <TD class="bodyBlue">Preferred Estimate Package: * </TD>
            <TD>
              <select id="selPreferredEstimatePackageID" name="PreferredEstimatePackageID" class="inputFld">
			          <option></option>
			          <xsl:for-each select="/Root/Reference[@ListName='EstimatePackage']">
			            <xsl:call-template name="BuildSelectOptions">
			              <xsl:with-param name="current" select="/Root/ShopInfo/@PreferredEstimatePackageID"/>
			            </xsl:call-template>
		            </xsl:for-each>
              </select>
            </TD>
          </TR>
		    </TABLE>
      </TD> <!-- end of column 2 -->
  </TR>
  <TR>
    <TD valign="top">
	    <xsl:apply-templates select="/Root/ShopHours"/>
	  </TD>
  </TR>
  <TR>
    <TD colspan="3" valign="top">
      <TABLE width="700" border="0" cellspacing="0" cellpadding="1">
       <TR>
        <TD noWrap="nowrap" valign="top" class="bodyBlue">How to Get There: * <br/>
        <span id="commFldMsg" style="color:#000000; font-weight:normal; font-size:8pt">(max. 250 characters)</span>
        </TD>
          <TD valign="top">
            <span style="font-size:13px">Please enter localized directions to your facilities such as:
           <em>"We are located southbound on US Route 9, 2 miles south of the Freehold mall and across from the Pinebrook Shopping Center."</em></span><br/>
            <textarea id="txtDrivingDirections" name="DrivingDirections" rows="4" cols="64" onKeyDown="CheckInputLength(this, 250)"><xsl:value-of select="@DrivingDirections"/></textarea>
          </TD>
        </TR>
      </TABLE>
	  </TD>
  </TR>
</TABLE>  <!-- end of master table -->

</xsl:template>

<xsl:template match="/Root/ShopHours">
 <table border="0" cellspacing="0" cellpadding="1">
  <TR>
    <TD rowspan="7" noWrap="nowrap" valign="top" class="bodyBlue">
    <img src="images/spacer.gif" width="1px" heigth="6px"/><br/>
    Hours of Operation:<br/>
    <span style="font-size:9px">(24 hour format i.e. 1700 = 5 PM)</span>
    </TD>
    <TD rowspan="7" noWrap="nowrap" valign="top" class="bodyBlue">
      <img src="images/spacer.gif" width="42px"/>
    </TD>
    <TD class="bodyBlue">Mon: *</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtMondayStartTime" name="OperatingMondayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)" maxlength="4">
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingMondayStartTime"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtMondayEndTime" name="OperatingMondayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)" maxlength="4">
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingMondayEndTime"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD class="bodyBlue">Tue: *</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtTuesdayStartTime" name="OperatingTuesdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)" maxlength="4">
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingTuesdayStartTime"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtTuesdayEndTime" name="OperatingTuesdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)" maxlength="4">
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingTuesdayEndTime"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD class="bodyBlue">Wed: *</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtWednesdayStartTime" name="OperatingWednesdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)" maxlength="4">
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingWednesdayStartTime"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtWednesdayEndTime" name="OperatingWednesdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)" maxlength="4">
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingWednesdayEndTime"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD class="bodyBlue">Thu: *</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtThursdayStartTime" name="OperatingThursdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)" maxlength="4">
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingThursdayStartTime"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtThursdayEndTime" name="OperatingThursdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)" maxlength="4">
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingThursdayEndTime"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD class="bodyBlue">Fri: *</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtFridayStartTime" name="OperatingFridayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)" maxlength="4">
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingFridayStartTime"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtFridayEndTime" name="OperatingFridayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)" maxlength="4">
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingFridayEndTime"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD class="bodyBlue">Sat:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtSaturdayStartTime" name="OperatingSaturdayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)" maxlength="4">
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingSaturdayStartTime"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtSaturdayEndTime" name="OperatingSaturdayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)" maxlength="4">
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingSaturdayEndTime"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
  <TR>
    <TD class="bodyBlue">Sun:</TD>
    <TD vAlign="top" noWrap="nowrap">
      <input type="text" id="txtSundayStartTime" name="OperatingSundayStartTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)" maxlength="4">
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingSundayStartTime"/></xsl:attribute>
	    </input>
      -
      <input type="text" id="txtSundayEndTime" name="OperatingSundayEndTime" size="2" class="inputFld" onblur="CheckMilitaryTime()" onkeypress="NumbersOnly(event)" maxlength="4">
		    <xsl:attribute name="value"><xsl:value-of select="@OperatingSundayEndTime"/></xsl:attribute>
	    </input>
    </TD>
  </TR>
</table>
</xsl:template>

<xsl:template match="BusinessPersonnel">
   <xsl:variable name="PersonnelID" select="@PersonnelID"/>
  
  <div style="height:95; width:675;">
    
	  <input type="hidden" id="txtSPPersonnelID" name="SPPersonnelID">
      <xsl:if test="@PersonnelID != ''">
        <xsl:attribute name="value"><xsl:value-of select="@PersonnelID"/></xsl:attribute>
      </xsl:if>
    </input>
	  
	  <input type="hidden" id="txtSPGenderCD" name="SPGenderCD"></input>
      
	  <input type="hidden" id="txtSPPersonnelTypeID" name="SPPersonnelTypeID">
      <xsl:attribute name="value">
        <xsl:choose>
          <xsl:when test="@PersonnelTypeID != ''"><xsl:value-of select="@PersonnelTypeID"/></xsl:when>
          <xsl:otherwise>6</xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
	  </input>
  
  <table cellSpacing="0" cellPadding="1" border="0">
    <tr>
      <td class="bodyBlue" nowrap="nowrap">Name: *</td>
      <td>
        <input type="text" id="txtSPName" name="SPName" size="35"  class="inputFld" autocomplete="off" maxlength="50"> 
		      <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
		    </input>
      </td>
      <td><img src="images/spacer.gif" width="20px"/></td>
      <td class="bodyBlue">Shop Manager:</td>
      <td>
        <input type="checkbox" id="cbSPShopManagerFlag" name="cbSPShopManagerFlag" onclick="txtSPShopManagerFlag.value=this.checked?1:0;">
		      <xsl:if test="@ShopManagerFlag='1'"><xsl:attribute name="checked"/></xsl:if>  
		    </input>
		    <input type="hidden" id="txtSPShopManagerFlag" name="SPShopManagerFlag"  class="inputFld">
		      <xsl:attribute name="value"><xsl:value-of select="@ShopManagerFlag"/></xsl:attribute>
		    </input>
        <!--     
        Yes
        <input type="radio" id="cbSPShopManagerFlagYes" name="cbSPShopManagerFlag" onclick="txtSPShopManagerFlag.value=1">
		      <xsl:if test="@ShopManagerFlag='1'"><xsl:attribute name="checked"/></xsl:if>  
		    </input>
         No
        <input type="radio" id="cbSPShopManagerFlagNo" name="cbSPShopManagerFlag" onclick="txtSPShopManagerFlag.value=0">
		      <xsl:if test="@ShopManagerFlag='0'"><xsl:attribute name="checked"/></xsl:if>  
		    </input>
		    <input type="hidden" id="txtSPShopManagerFlag" name="SPShopManagerFlag"  class="inputFld">
		      <xsl:attribute name="value"><xsl:value-of select="@ShopManagerFlag"/></xsl:attribute>
		    </input>
         -->
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">Email:</td>
      <td>
	      <input id="txtSPEmailAddress" name="SPEmailAddress" size="35" class="inputFld" onbeforedeactivate="checkEMail(this)" maxlength="50">
		      <xsl:attribute name="value"><xsl:value-of select="@EmailAddress"/></xsl:attribute>
		    </input>
	    </td>
      <td></td>
      <td class="bodyBlue" nowrap="nowrap">Preferred Contact Method:</td>
      <td>
        <select id="selSPPreferredContactMethodID" name="SPPreferredContactMethodID" class="inputFld">
		      <option></option>
		      <xsl:for-each select="/Root/Reference[@ListName='ContactMethod']">
            <xsl:call-template name="BuildSelectOptions">
			        <xsl:with-param name="current" select="/Root/BusinessPersonnel/@PreferredContactMethodID"/>
			      </xsl:call-template>
		      </xsl:for-each>
        </select>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">Phone:</td>
      <td class="bodyBlue" nowrap="nowrap" align="left">
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Phone</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SP</xsl:with-param>
          <xsl:with-param name="EventAction">SPPrefContact</xsl:with-param>
        </xsl:call-template>
      </td>
      <td width="10px"></td>
      <td class="bodyBlue">Fax:</td>
      <td class="bodyBlue" nowrap="nowrap">
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Fax</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SP</xsl:with-param>
        </xsl:call-template>
      </td>
	  </tr>
    <tr>
      <td class="bodyBlue">Cell:</td>
      <td class="bodyBlue">
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Cell</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SP</xsl:with-param>
        </xsl:call-template>            
      </td>
      <td width="10px"></td>
      <td class="bodyBlue">Pager:</td>
      <td class="bodyBlue">
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Pager</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SP</xsl:with-param>
        </xsl:call-template>
      </td>
    </tr>
  </table>
  </div>
</xsl:template>

<xsl:template match="ShopPersonnel">
   <xsl:variable name="PersonnelID" select="@PersonnelID"/>
  
  <div style="height:95; width:675;">
    
	  <input type="hidden" id="txtSLPPersonnelID" name="SLPPersonnelID">
      <xsl:if test="@PersonnelID != ''">
        <xsl:attribute name="value"><xsl:value-of select="@PersonnelID"/></xsl:attribute>
      </xsl:if>
    </input>
	  
	  <input type="hidden" id="txtSLPGenderCD" name="SLPGenderCD"></input>
      
	  <input type="hidden" id="txtSLPPersonnelTypeID" name="SLPPersonnelTypeID">
      <xsl:attribute name="value">
        <xsl:choose>
          <xsl:when test="@PersonnelTypeID != ''"><xsl:value-of select="@PersonnelTypeID"/></xsl:when>
          <xsl:otherwise>2</xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
	  </input>
  
  <table cellSpacing="0" cellPadding="1" border="0">
    <tr>
      <td class="bodyBlue" nowrap="nowrap">Name: *</td>
      <td>
        <input type="text" id="txtSLPName" name="SLPName" size="35"  class="inputFld" autocomplete="off" maxlength="50"> 
		      <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
		    </input>
      </td>
      <td><img src="images/spacer.gif" width="20px"/></td>
      <td class="bodyBlue">Shop Manager:</td>
      <td>
        <input type="checkbox" id="cbSLPShopManagerFlag" name="cbSLPShopManagerFlag" onclick="txtSLPShopManagerFlag.value=this.checked?1:0;">
		      <xsl:if test="@ShopManagerFlag='1'"><xsl:attribute name="checked"/></xsl:if>  
		    </input>
		    <input type="hidden" id="txtSLPShopManagerFlag" name="SLPShopManagerFlag"  class="inputFld">
		      <xsl:attribute name="value"><xsl:value-of select="@ShopManagerFlag"/></xsl:attribute>
		    </input> 
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">Email:</td>
      <td>
	      <input id="txtSLPEmailAddress" name="SLPEmailAddress" size="35" class="inputFld" onbeforedeactivate="checkEMail(this)" maxlength="50">
		      <xsl:attribute name="value"><xsl:value-of select="@EmailAddress"/></xsl:attribute>
		    </input>
	    </td>
      <td></td>
      <td class="bodyBlue" nowrap="nowrap">Preferred Contact Method:</td>
      <td>
        <select id="selSLPPreferredContactMethodID" name="SLPPreferredContactMethodID" class="inputFld">
		      <option></option>
		      <xsl:for-each select="/Root/Reference[@ListName='ContactMethod']">
            <xsl:call-template name="BuildSelectOptions">
			        <xsl:with-param name="current" select="/Root/ShopPersonnel/@PreferredContactMethodID"/>
			      </xsl:call-template>
		      </xsl:for-each>
        </select>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue" nowrap="nowrap">Phone: *</td>
      <td class="bodyBlue" nowrap="nowrap" align="left">
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Phone</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SLP</xsl:with-param>
          <xsl:with-param name="EventAction">SLPPrefContact</xsl:with-param>
        </xsl:call-template>
      </td>
      <td></td>
      <td class="bodyBlue">Fax:</td>
      <td class="bodyBlue" nowrap="nowrap">
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Fax</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SLP</xsl:with-param>
        </xsl:call-template>
      </td>
	  </tr>
    <tr>
      <td class="bodyBlue">Cell:</td>
      <td class="bodyBlue">
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Cell</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SLP</xsl:with-param>
        </xsl:call-template>            
      </td>
      <td></td>
      <td class="bodyBlue">Pager:</td>
      <td class="bodyBlue">
        <xsl:call-template name="ContactNumbers">
          <xsl:with-param name="ContactMethod">Pager</xsl:with-param>
          <xsl:with-param name="EntityAbbrev">SLP</xsl:with-param>
        </xsl:call-template>
      </td>
    </tr>
  </table>
  </div>
</xsl:template> 

<xsl:template match="Pricing">
<TABLE unselectable="on" border="0" cellspacing="0" cellpadding="0">
  <TR>
    <TD colspan="5"><img src="images/spacer.gif" height="6px"/></TD>
  </TR>
  <TR>
    <TD valign="top">
      <table unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2">
              <table border="0" width="100%" cellspacing="0" cellpadding="1">
              <tr>
                <td class="bodyBlue" colspan="4" style="color:#000000">Hourly Labor Rates:</td>
              </tr>
              <tr>
                <td width="25px"> </td>
                <td width="100%" class="bodyBlue">Sheet Metal: *</td>
                <td class="bodyBlue">$</td>
                <td class="bodyBlue">
                  <input type="text" id="txtSheetMetal" name="HourlyRateSheetMetal" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			         <xsl:attribute name="value"><xsl:value-of select="@HourlyRateSheetMetal"/></xsl:attribute>
      			      </input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Refinishing: *</td>
                <td class="bodyBlue">$</td>
                <td class="bodyBlue">
                  <input type="text" id="txtRefinish" name="HourlyRateRefinishing" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@HourlyRateRefinishing"/></xsl:attribute>
            			</input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Unibody/Frame: *</td>
                <td class="bodyBlue">$</td>
                <td class="bodyBlue">
                  <input type="text" id="txtUnibodyFrame" name="HourlyRateUnibodyFrame" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@HourlyRateUnibodyFrame"/></xsl:attribute>
            			</input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Mechanical: *</td>
                <td class="bodyBlue">$</td>
                <td class="bodyBlue">
                  <input type="text" id="txtMechanical" name="HourlyRateMechanical" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@HourlyRateMechanical"/></xsl:attribute>
            			</input>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td><img src="images/spacer.gif" height="7px"/></td>  <!-- spacer row -->
        </tr>
        <tr>
          <td colspan="2">
            <table unselectable="on" width="100%" border="0" cellpadding="0" cellspacing="1">
              <tr>
                <td class="bodyBlue" colspan="4" style="color:#000000">Refinish Materials Charges:</td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue" height="20" colspan="3">Single Stage:</td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td width="10px"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
                <td class="bodyBlue">Charge Per Hour:</td>
                <td class="bodyBlue">$</td>
                <td class="bodyBlue">
                  <input type="text" id="txtHourly1" name="RefinishSingleStageHourly" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@RefinishSingleStageHourly"/></xsl:attribute>
            			</input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td></td>
                <td class="bodyBlue">Maximum Charge:</td>
                <td class="bodyBlue">$</td>
                <td class="bodyBlue">
                  <input type="text" id="txtMax1" name="RefinishSingleStageMax" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@RefinishSingleStageMax"/></xsl:attribute>
            			</input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue" colspan="3">Two Stage:</td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td></td>
                <td class="bodyBlue">Charge Per Hour: *</td>
                <td class="bodyBlue">$</td>
                <td class="bodyBlue">
                  <input type="text" id="txtHourly2" name="RefinishTwoStageHourly" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageHourly"/></xsl:attribute>
            			</input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td></td>
                <td class="bodyBlue">Maximum Charge: *</td>
                <td class="bodyBlue">$</td>
                <td class="bodyBlue">
                  <input type="text" id="txtMax2" name="RefinishTwoStageMax" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageMax"/></xsl:attribute>
            			</input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td></td>
                <td class="bodyBlue">Clear Coat Max Hours: *</td>
                <td></td>
                <td class="bodyBlue" align="right">
                  <input type="text" id="txtCCMax2" name="RefinishTwoStageCCMaxHrs" style="text-align:right;" wizard="yes" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageCCMaxHrs"/></xsl:attribute>
            			</input>hrs
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue" colspan="3">Three Stage:</td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td></td>
                <td class="bodyBlue">Charge Per Hour:</td>
                <td class="bodyBlue">$</td>
                <td class="bodyBlue">
                  <input type="text" id="txtHourly3" name="RefinishThreeStageHourly" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageHourly"/></xsl:attribute>
            			</input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td></td>
                <td class="bodyBlue">Maximum Charge:</td>
                <td class="bodyBlue">$</td>
                <td class="bodyBlue">
                  <input type="text" id="txtMax3" name="RefinishThreeStageMax" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageMax"/></xsl:attribute>
            			</input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td></td>
                <td class="bodyBlue">Clear Coat Max Hours:</td>
                <td></td>
                <td class="bodyBlue" align="right">
                  <input type="text" id="txtCCMax3" name="RefinishThreeStageCCMaxHrs" style="text-align:right;" wizard="yes" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageCCMaxHrs"/></xsl:attribute>
            			</input>hrs
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td><img src="images/spacer.gif" height="7px"/></td>  <!-- spacer row -->
        </tr>
        <tr>
          <td>
      	  <div style="height:55">
            <table border="0" width="100%" cellpadding="0" cellspacing="1">
              <tr>
                <td class="bodyBlue" colspan="3" style="color:#000000"><b>Towing Charges:</b></td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Charge Type:</td>
                <td align="right">
                  <select id="selTowChargeType" name="TowInChargeTypeCD" style="text-align:right;" wizard="yes" onchange="selTowChgType_onchange()">
                    <option value=""></option>
              			  <xsl:for-each select="/Root/Reference[@ListName='TowInChargeType']">
              			    <xsl:call-template name="BuildSelectOptions">
              				  <xsl:with-param name="current" select="/Root/Pricing/@TowInChargeTypeCD"/>
              				</xsl:call-template>
              			  </xsl:for-each>
                  </select>
                </td>
              </tr>
              <tr>        
                <td width="5px"></td>
                <td class="bodyBlue"><label id="lblFlatFee">Flat Fee:</label></td>  
                <td class="bodyBlue" align="right"><label id="lblFFDollar">$</label>
                  <input type="text" id="txtFlatFee" name="TowInFlatFee" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@TowInFlatFee"/></xsl:attribute>
            			</input>
                </td>
              </tr>
            </table>
      	  </div>
          </td>
        </tr>
      </table>
    </TD>
    <TD><img src="images/spacer.gif" width="20px"/></TD> <!-- spacer column -->
    <TD valign="top">
      <table border="0" width="100%" valign="top" cellpadding="0" cellspacing="0">
        <tr>
          <td valign="top">
            <table border="0" width="100%" cellpadding="0" cellspacing="1">
              <tr>
                <td class="bodyBlue" colspan="4" style="color:#000000">Air Conditioning Service Charges:</td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue" height="20" colspan="3">R-12 Evaluate/Recharge:</td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td width="10px"></td>
                <td class="bodyBlue">With Freon:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtR12Freon" name="R12EvacuateRechargeFreon" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@R12EvacuateRechargeFreon"/></xsl:attribute>
            			</input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td width="10px"></td>
                <td class="bodyBlue">Without Freon:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtR12NoFreon" name="R12EvacuateRecharge" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@R12EvacuateRecharge"/></xsl:attribute>
            			</input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue" height="20" colspan="3">R-134 Evaluate/Recharge:</td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td width="10px"></td>
                <td class="bodyBlue">With Freon:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtR134Freon" name="R134EvacuateRechargeFreon" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@R134EvacuateRechargeFreon"/></xsl:attribute>
            			</input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td width="10px"></td>
                <td class="bodyBlue">Without Freon:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtR134NoFreon" name="R134EvacuateRecharge" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@R134EvacuateRecharge"/></xsl:attribute>
            			</input>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td><img src="images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
        </tr>
        <tr>
          <td>
            <table width="100%" cellpadding="0" cellspacing="1">
              <tr>
                <td class="bodyBlue" colspan="4" style="color:#000000">Stripe Replacement Charges:</td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue" height="20" colspan="3">Tape Stripes:</td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td width="10px"></td>
                <td class="bodyBlue">Per Panel:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtTapePerPanel" name="StripeTapePerPanel" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@StripeTapePerPanel"/></xsl:attribute>
            			</input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td></td>
                <td class="bodyBlue">Per Side:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtTapePerSide" name="StripeTapePerSide" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@StripeTapePerSide"/></xsl:attribute>
            			</input>
                </td>
          		</tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue" height="20" colspan="3">Paint Stripes:</td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td width="10px"></td>
                <td class="bodyBlue">Per Panel:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtPaintPerPanel" name="StripePaintPerPanel" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@StripePaintPerPanel"/></xsl:attribute>
            			</input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td></td>
                <td class="bodyBlue">Per Side:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtPaintPerSide" name="StripePaintPerSide" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@StripePaintPerSide"/></xsl:attribute>
            			</input>
                </td>
          		</tr>
            </table>
          </td>
        </tr> 
        <tr>
          <td><img src="images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
        </tr>
        <tr>
          <td>
            <table border="0" width="100%" cellpadding="0" cellspacing="1">
              <tr>
                <td class="bodyBlue" colspan="3" style="color:#000000">Frame Setup Charges (Hours):</td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">No Measurement:</td>
                <td class="bodyBlue" align="right">
                  <input type="text" id="txtPullNoMeasure" name="PullSetup" style="text-align:right;" wizard="yes" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@PullSetup"/></xsl:attribute>
            			</input>hrs
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">With Measurement</td>
                <td class="bodyBlue" align="right">
                  <input type="text" id="txtPullMeasure" name="PullSetupMeasure" style="text-align:right;" wizard="yes" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@PullSetupMeasure"/></xsl:attribute>
            			</input>hrs
                </td>
              </tr>
            </table>
          </td>
        </tr> 
        <tr>
          <td><img src="images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
        </tr>
        <tr>
          <td>
            <table border="0" cellpadding="0" cellspacing="1">
              <tr>
                <td class="bodyBlue" colspan="3" style="color:#000000">Glass Charges:</td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td width="140" class="bodyBlue">Replacement Type:</td>
                <td width="96" align="right">
                  <select id="selGlassReplChgType" name="GlassReplacementChargeTypeCD" style="text-align:right;" wizard="yes" onchange="selGlassReplChgType_onchange()">
                    <option value=""></option>
      			        <xsl:for-each select="/Root/Reference[@ListName='GlassReplacementChargeType']">
      			          <xsl:call-template name="BuildSelectOptions">
      				          <xsl:with-param name="current" select="/Root/Pricing/@GlassReplacementChargeTypeCD"/>
      				        </xsl:call-template>
      			        </xsl:for-each>
                  </select>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">
                  <label id="lblWindshield">Windshield Discount:</label>
                  <label id="lblSubletFee">Sublet Fee:</label>
                </td>
                <td class="bodyBlue" align="right">
                  <input type="text" id="txtWindshieldDiscount" name="DiscountPctWindshield" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1)">
            			  <xsl:attribute name="value"><xsl:value-of select="@DiscountPctWindshield"/></xsl:attribute>
            			</input>
            			<label id="lblWindshieldPercent">%</label>
            			<label id="lblGlassDollar">$</label>
                  <input type="text" id="txtGlassSubletSvcFee" name="GlassSubletServiceFee" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event); frmShop.GlassSubletServicePct.value = '';" onchange="frmShop.GlassSubletServicePct.value = '';">
            			  <xsl:attribute name="title">Please enter either a Sublet Service Fee OR a Sublet Discount Percentage</xsl:attribute>
            			  <xsl:attribute name="value"><xsl:value-of select="@GlassSubletServiceFee"/></xsl:attribute>
            			</input>
            			<input type="text" id="txtGlassOutsourceSvcFee" name="GlassOutsourceServiceFee" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@GlassOutsourceServiceFee"/></xsl:attribute>
            			</input>
                </td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">
      	        <label id="lblSideBack">Side/Back Discount:</label>
                  <label id="lblSubletPct"><img src="images/spacer.gif" width="25px" height="1px"/>OR<br/>Sublet Percent:</label>
                </td>
                <td class="bodyBlue" align="right">
                  <input type="text" id="txtSideBackGlassDiscount" name="DiscountPctSideBackGlass" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1)">
            			  <xsl:attribute name="value"><xsl:value-of select="@DiscountPctSideBackGlass"/></xsl:attribute>
            			</input>
            			<input type="text" id="txtGlassSubletSvcPct" name="GlassSubletServicePct" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1); frmShop.GlassSubletServiceFee.value = '';" onchange="frmShop.GlassSubletServiceFee.value = '';">
            			  <xsl:attribute name="title">Please enter either a Sublet Service Fee OR a Sublet Discount Percentage</xsl:attribute>
            			  <xsl:attribute name="value"><xsl:value-of select="@GlassSubletServicePct"/></xsl:attribute>
            			</input>
            			<label id="lblGlassPercent">%</label>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table> 
    </TD>
    <TD><img src="images/spacer.gif" width="20px"/></TD> <!-- spacer column -->
    <TD valign="top">	
      <table border="0" cellpadding="0" cellspacing="0">
        <tr valign="top">
          <td>
            <table border="0" width="100%" cellpadding="0" cellspacing="1">      
              <tr>
                <td class="bodyBlue" colspan="3" style="color:#000000">Recycled Parts:</td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Markup Percent:</td>
                <td class="bodyBlue" align="right">
                  <input type="text" id="txtRecycledPartsMarkup" name="PartsRecycledMarkupPct" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1)">
            			  <xsl:attribute name="value"><xsl:value-of select="@PartsRecycledMarkupPct"/></xsl:attribute>
            			</input>%
                </td>
              </tr>
            </table>
          </td>
        </tr> 
        <tr>
          <td><img src="images/spacer.gif" height="6px"/></td>  <!-- spacer row -->
        </tr>
        <tr>
          <td>
            <table border="0" cellpadding="0" cellspacing="1">
              <tr>
                <td class="bodyBlue" colspan="3" style="color:#000000"><b>Additional Repair Charges:</b></td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">2-Wheel Alignment:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtAlign2Wheel" name="AlignmentTwoWheel" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@AlignmentTwoWheel"/></xsl:attribute>
            			</input>
                </td>    
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">4-Wheel Alignment:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtAlign4Wheel" name="AlignmentFourWheel" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@AlignmentFourWheel"/></xsl:attribute>
            			</input>
                </td>    
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Tire Mount and Balance:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtTireMountBalance" name="TireMountBalance" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@TireMountBalance"/></xsl:attribute>
            			</input>
                </td>    
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Flex Additive:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtFlexAdditive" name="FlexAdditive" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
              			  <xsl:attribute name="value"><xsl:value-of select="@FlexAdditive"/></xsl:attribute>
              			</input>
                </td>    
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Coolant (Green):</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtCoolantGreen" name="CoolantGreen" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
              			  <xsl:attribute name="value"><xsl:value-of select="@CoolantGreen"/></xsl:attribute>
              			</input>
                </td>    
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Coolant (Red):</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtCoolantRed" name="CoolantRed" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@CoolantRed"/></xsl:attribute>
            			</input>
                </td>    
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Undercoat:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtUndercoat" name="Undercoat" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@Undercoat"/></xsl:attribute>
            			</input>
                </td>    
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Chip Guard:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtChipGuard" name="ChipGuard" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@ChipGuard"/></xsl:attribute>
            			</input>
                </td>    
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Caulking/Seam Sealer:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtCaulkSeamSeal" name="CaulkingSeamSealer" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@CaulkingSeamSealer"/></xsl:attribute>
            			</input>
                </td>    
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Corrosion Protection:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtCorrosionProt" name="CorrosionProtection" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@CorrosionProtection"/></xsl:attribute>
            			</input>
                </td>    
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Cover Car:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtCoverCar" name="CoverCar" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@CoverCar"/></xsl:attribute>
            			</input>
                </td>    
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Hazardous Waste:</td>
                <td class="bodyBlue" align="right">$
                  <input type="text" id="txtHazardousWaste" name="HazardousWaste" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
            			  <xsl:attribute name="value"><xsl:value-of select="@HazardousWaste"/></xsl:attribute>
            			</input>
                </td>    
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td><img src="images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
        </tr>
        <tr>
          <td>
            <table border="0" width="100%" cellpadding="0" cellspacing="1">
              <tr>
                <td class="bodyBlue" colspan="3" style="color:#000000">Tax Rates:</td>
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Sales:</td>
                <td class="bodyBlue" align="right">
                  <input type="text" id="txtSalesTax" name="SalesTaxPct" style="text-align:right;" wizard="yes" size="6" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
            			  <xsl:attribute name="value"><xsl:value-of select="@SalesTaxPct"/></xsl:attribute>
            			</input>%
                </td>    
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">County:</td>
                <td class="bodyBlue" align="right">
                  <input type="text" id="txtCountyTax" name="CountyTaxPct" style="text-align:right;" wizard="yes" size="6" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
            			  <xsl:attribute name="value"><xsl:value-of select="@CountyTaxPct"/></xsl:attribute>
            			</input>%
                </td>    
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Municipal:</td>
                <td class="bodyBlue" align="right">
                  <input type="text" id="txtMunicipalTax" name="MunicipalTaxPct" style="text-align:right;" wizard="yes" size="6" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
            			  <xsl:attribute name="value"><xsl:value-of select="@MunicipalTaxPct"/></xsl:attribute>
            			</input>%
                </td>    
              </tr>
              <tr>
                <td width="5px"></td>
                <td class="bodyBlue">Other:</td>
                <td class="bodyBlue" align="right">
                  <input type="text" id="txtOtherTax" name="OtherTaxPct" style="text-align:right;" wizard="yes" size="6" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
            			  <xsl:attribute name="value"><xsl:value-of select="@OtherTaxPct"/></xsl:attribute>
            			</input>%
                </td>    
              </tr>
            </table>
          </td>
        </tr>  
        <tr>
          <td><img src="images/spacer.gif" height="16px" border="0"/></td>  <!-- spacer row -->
        </tr>
      </table>
    </TD>
  </TR>
  <TR>
    <TD valign="top">
      <table border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td class="bodyBlue" colspan="3" style="color:#000000">OEM Replacement Parts Discount:</td>
      </tr>
      <tr>
        <td width="25px"> </td>
        <td width="140px" class="bodyBlue">Domestic:</td>
        <td class="bodyBlue">
          <input type="text" id="txtDiscountDomestic" name="DiscountDomestic" style="text-align:right;" wizard="yes" size="6" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
	         <xsl:attribute name="value"><xsl:value-of select="@DiscountDomestic"/></xsl:attribute>
	      </input>
          %
        </td>
        </tr>
        <tr>
          <td width="25px"></td>
          <td class="bodyBlue">Foreign:</td>
          <td class="bodyBlue">
            <input type="text" id="txtDiscountImport" name="DiscountImport" style="text-align:right;" wizard="yes" size="6" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
      			  <xsl:attribute name="value"><xsl:value-of select="@DiscountImport"/></xsl:attribute>
      			</input>
            %
          </td>
        </tr>
      </table>
    </TD>
    <TD width="530" colspan="4" valign="top">
      <span style="font-size:13px">Please enter any additional information regarding the OEM Replacement Parts Discount that may be specific to certain vehicle makes.</span> 
      <span id="OEMcommFldMsg" style="color:#000000; font-weight:normal; font-size:8pt">(max. 250 characters)</span>
      <textarea id="txtOEMDiscounts" name="OEMDiscounts" rows="4" cols="64" onKeyDown="CheckInputLength(this, 250, 'OEMcommFldMsg')"><xsl:value-of select="@OEMDiscounts"/></textarea>
    </TD>
  </TR>
</TABLE>
</xsl:template>

<xsl:template name="ContactNumbers">
  <xsl:param name="ContactMethod"/>
  <xsl:param name="EntityAbbrev"/>
  <xsl:param name="EventAction"/>
  
  <input maxlength="3" size="2" class="inputFld" wizard="yes">
  <!-- <input maxlength="3" size="2" onbeforedeactivate="checkAreaCode(this);" class="inputFld" wizard="yes"> -->
	<xsl:attribute name="id">txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>AreaCode</xsl:attribute>  
	<xsl:attribute name="name"><xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>AreaCode</xsl:attribute>
	<xsl:attribute name="onkeypress">NumbersOnly(event);</xsl:attribute>
	<!-- <xsl:attribute name="onkeypress">NumbersOnly(event);phoneAutoTab(this,txt<xsl:value-of select="$ContactMethod"/>ExchangeNumber,'A');</xsl:attribute> -->
	<xsl:attribute name="value">
	  <xsl:choose>
	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="@PhoneAreaCode"/></xsl:when>
		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="@FaxAreaCode"/></xsl:when>
		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="@CellAreaCode"/></xsl:when>
		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="@PagerAreaCode"/></xsl:when>
	  </xsl:choose>
	</xsl:attribute>
  </input> -
         
  <input maxlength="3" size="2" class="inputFld" wizard="yes">
  <!-- <input maxlength="3" size="2" onbeforedeactivate="checkPhoneExchange(this);" class="inputFld" wizard="yes"> -->
	<xsl:attribute name="id">txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>ExchangeNumber</xsl:attribute>  
	<xsl:attribute name="name"><xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>ExchangeNumber</xsl:attribute>
	<xsl:attribute name="onkeypress">NumbersOnly(event);</xsl:attribute>
	<!-- <xsl:attribute name="onkeypress">NumbersOnly(event);phoneAutoTab(this,txt<xsl:value-of select="$ContactMethod"/>UnitNumber,'E');</xsl:attribute> -->
	<xsl:attribute name="value">
	  <xsl:choose>
	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="@PhoneExchangeNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="@FaxExchangeNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="@CellExchangeNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="@PagerExchangeNumber"/></xsl:when>
	  </xsl:choose>
	</xsl:attribute>  
  </input> -
		 
  <input maxlength="4" size="3" class="inputFld" wizard="yes">
  <!-- <input maxlength="4" size="3" onbeforedeactivate="checkPhoneNumber(this);" class="inputFld" wizard="yes"> -->
  <xsl:attribute name="id">txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>UnitNumber</xsl:attribute>  
	<xsl:attribute name="name"><xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>UnitNumber</xsl:attribute>
	<xsl:attribute name="onkeypress">NumbersOnly(event);</xsl:attribute>
	<xsl:if test="$EventAction != ''">
  	<xsl:attribute name="onBlur"><xsl:value-of select="$EventAction"/>();</xsl:attribute>
	</xsl:if>

	<!-- <xsl:attribute name="onkeypress">NumbersOnly(event);phoneAutoTab(this,txt<xsl:value-of select="$ContactMethod"/>ExtensionNumber,'U');</xsl:attribute> -->
	<xsl:attribute name="value">
	  <xsl:choose>
	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="@PhoneUnitNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="@FaxUnitNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="@CellUnitNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="@PagerUnitNumber"/></xsl:when>
	  </xsl:choose>
	</xsl:attribute>
  </input>
  
  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
         
  ext. <input maxlength="5" size="5" onkeypress="NumbersOnly(window.event)" class="inputFld" wizard="yes">
    <xsl:attribute name="id">txt<xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>ExtensionNumber</xsl:attribute>  
	<xsl:attribute name="name"><xsl:value-of select="$EntityAbbrev"/><xsl:value-of select="$ContactMethod"/>ExtensionNumber</xsl:attribute>
	<xsl:attribute name="value">
	  <xsl:choose>
	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="@PhoneExtensionNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="@FaxExtensionNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="@CellExtensionNumber"/></xsl:when>
		<xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="@PagerExtensionNumber"/></xsl:when>
	  </xsl:choose>
	</xsl:attribute>
  </input>
</xsl:template>



<!-- Build an option list for a Select box from xml reference data.  Set the contact by wrapping call to
    this template in a for-each loop.  The parameter 'current' represents the option that should be 
	currently selected.  If you need a blank option, add it manually. -->	   
  <xsl:template name="BuildSelectOptions">
	<xsl:param name="current"/>
	<option>
		<xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
		
		<xsl:if test="@ReferenceID = $current">
			<xsl:attribute name="selected"/>
		</xsl:if>
		
		<xsl:value-of select="@Name"/>	
	</option>
  </xsl:template>


</xsl:stylesheet>  