<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    id="PMDReportViewer">

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="Report"/>
<xsl:param name="Args"/>

<xsl:template match="/Root">
		
<HTML>

<!--            APP _PATH                            SessionKey                   USERID                   SP               XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDReportViewerGetDetailXML,PMDReportViewer.xsl,1391  -->

<HEAD>
  <TITLE>Report Viewer</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/> 
  <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
  <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
  </script>
<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
var gsReport = '<xsl:value-of select="$Report"/>';
var gsArgs = '<xsl:value-of select="$Args"/>';

<![CDATA[
	
function InitPage(){
	switch (gsReport){
		case "reinspect":
		  var arrArgs = gsArgs.split("|");
			document.frames["ifrmReport"].frameElement.src = "PMDReinspectReport.asp?DocumentID="+arrArgs[0]+"&SupForm="+arrArgs[1];
			break;
    case "estimateaudit":
      document.frames["ifrmReport"].frameElement.src = "/EstimateAuditReport.asp?DocumentID=" + gsArgs;
      break;
	}
  
	obj = document.getElementById("ifrmReport");
  	obj.style.visibility = "visible";
  	obj.style.display = "inline";
}

function Print(){
	var oObj = new Object;
    oObj.strTitle = ifrmReport.document.title;
    oObj.strReport = ifrmReport.rptText.innerHTML;
	window.showModalDialog('/Reports/printReport.asp', oObj, 'dialogHeight:0px;dialogWidth:0px;dialogTop:0px;dialogLeft:0px;center:no;status:no;resizable:yes;dialogHide:yes');
}

]]>
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSub" unselectable="on" onload="InitPage()">
<div id="divFrame" style="position:absolute; left:12;">
<table border="0" width="100%">
  <tr>
    <td align="right">
	  <!--
	  <input type="button" id="btnEmail" value=" Send Email" class="formButton"/>
	  <input type="button" id="btnFax" value="Send Fax" class="formButton" style="width:80"/>
	  -->
	  <input type="button" id="btnPrint" value="Print" class="formButton" style="width:80" onclick="Print()"/>	  
	</td>
  </tr>
</table>


<div style="border-style:solid; border-width:1px;">
  <IFRAME id="ifrmReport" src="blank.asp" style="position:relative; top:4px; width:755; height:630; border:1px; visibility:hidden; overflow:hidden" allowtransparency="false" >
  </IFRAME>
</div>
  
</div>

</BODY>
</HTML>

</xsl:template>

</xsl:stylesheet>