<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTDetailLevel">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="mode"/>
<xsl:param name="UserId"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTDetailLevelGetListXML,SMTDetailLevel.xsl, 56006, null, 'B'   -->

<HEAD>
  <TITLE>Shop Maintenance</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>


<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->


<script language="javascript">
<!-- these global variables maintain state information for the various pages loaded into the content frame on this page -->
<xsl:if test="$mode != 'parentwizard'">
  top.gsBusinessParentID = '<xsl:value-of select="Parent/@BusinessParentID"/>';
  top.gsBusinessInfoID = '<xsl:value-of select="@SearchBusinessInfoID"/>';
  top.gsShopID = '<xsl:value-of select="@SearchShopID"/>';
</xsl:if>
var gsSearchType = '<xsl:value-of select="@SearchType"/>';
var gsShopCount = '<xsl:value-of select="count(Shop)"/>';
var gbDirtyFlag = false;
var gsPageName;
var gsCallBack;
var gsPage;
var gsMainTab;
var gaShopIDs = new Array(<xsl:for-each select="Shop"><xsl:value-of select="@ShopID"/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
var gsMode = '<xsl:value-of select="$mode"/>';
var gsFramePageFile = "SMTDetailLevel.asp";
var gsShopCRUD = '<xsl:value-of select="$ShopCRUD"/>';
var gsBottomTab;
	<xsl:choose>
  		<xsl:when test="@SearchType='S'">gsBottomTab = "shop";</xsl:when>
  		<xsl:when test="@SearchType='B'">gsBottomTab = "businfo";</xsl:when>
		<xsl:when test="@SearchType='P'">gsBottomTab = "parent";</xsl:when>
	</xsl:choose>

var gsFirstShopID = '<xsl:value-of select="/Root/Shop[position()=1]/@ShopID"/>';
var gsUserID = '<xsl:value-of select="$UserId"/>'

// these variable used to save the state of the 'BusInfo|Include Shop' search page
var gsSearchID = "";
var gsSearchName = "";
var gsSearchCity = "";
var gsSearchState = "";
var gsSearchPhoneAreaCode = "";
var gsSearchPhoneExchange = "";
var gsSearchPhoneUnit = "";
var gsSearchShopType = "";
var gsSearchSpecialties = "";

<![CDATA[

function initPage(){
  if (gsMode != "parentwizard"){
  	top.gsFramePageFile = gsFramePageFile;
	  if (gsShopCRUD.indexOf("C") > -1) top.AdminMenu(true,1);
  	parent.layerTabs.style.visibility="visible";
  	parent.SetMainTab(gsBottomTab);

	var objTabLast = Shops.lastChild;
    if ((objTabLast.offsetLeft + objTabLast.offsetWidth) > Shops.offsetWidth && gsBottomTab == "shop")
  	  tabScroll.style.display = "inline";
  }
  else{ 
  	Shops.style.visibility = "hidden";
	  ParentTabs.style.visibility = "visible";
  	document.frames["ifrmContent"].frameElement.src = "SMTBusinessParent.asp?BusinessParentID=0&mode=wizard";
   	obj = document.getElementById("ifrmContent");
   	obj.style.visibility = "visible";
   	obj.style.display = "inline";
  }
}

function MenuCheckDirty(){
    if (gbDirtyFlag == true) {
		var sSave = YesNoMessage(gsPageName, "Some information on this page has changed. Do you want to return to the page and save the changes?");
        if (sSave == "Yes") return false;
        else gbDirtyFlag = false;
    }
    return true;
 }

function CheckDirty() {
  
  if (gbDirtyFlag == true) {
    var sSave = YesNoMessage(gsPageName, "Some information on this page has changed. Do you want to save the changes?");
    if (sSave == "Yes")
       return ifrmContent.IsDirty();
  }
	gbDirtyFlag = false;
  return true;
}

function SetMainTab(sTab){
	BusTabs.style.visibility = "hidden";
	ShopTabs.style.visibility = "hidden";
	ParentTabs.style.visibility = "hidden";
  
	switch(sTab){
		case "shop":
      if (top.gsShopID == ""){
        tab41.className = "tabactive1";
        top.gsShopID = gsFirstShopID;
      }
      if (top.gsShopID == ""){
        var sErrMsg = "There are currently no Shops associated with this Business.  This is an invalid data state "
        sErrMsg += "and should be corrected immediately.  Possible corrective actions are to add a shop from the "
        sErrMsg += "Admin menu, Include a Shop, or to Delete this Business."
        ClientWarning(sErrMsg);
        top.SetMainTab("businfo");
        return;
      }
			ShopTabs.style.visibility = "visible";
			//if (top.gsOrigin == "wiz"){
			//	top.gsOrigin = "";
			//	TabChange(top.gsPageFile, sTab);
			//}
			//else{
				TabChange("SMTShopInfo.asp?ShopID=" + top.gsShopID, sTab);
      //}
			Shops.style.visibility = "visible";
			if (top.gbInfo) top.SetInfoHeader("  Shop ID:", top.gsShopID, top.gsInfoName, top.gsInfoAddress, top.gsInfoCity, top.gsInfoPhone);
			var objTabLast = Shops.lastChild;
    		if ((objTabLast.offsetLeft + objTabLast.offsetWidth) > Shops.offsetWidth)
      			tabScroll.style.display = "inline";
    		break;
		case "businfo":
			BusTabs.style.visibility = "visible";
			if (top.gsOrigin == "wiz"){
				top.gsOrigin = "";
				TabChange(top.gsPageFile, sTab);
			}
			else
				TabChange("SMTBusinessInfo.asp?BusinessInfoID=" + top.gsBusinessInfoID, sTab);
			Shops.style.visibility = "hidden";
			tabScroll.style.display = "none";
			if (top.gbInfo) top.SetInfoHeader("  Business ID:", top.gsBusinessInfoID, top.gsInfoName, top.gsInfoAddress, top.gsInfoCity, top.gsInfoPhone);
			break;
		case "parent":
			ParentTabs.style.visibility = "visible";
			TabChange("SMTBusinessParent.asp?BusinessParentID=" + top.gsBusinessParentID, sTab);
			Shops.style.visibility = "hidden";
			tabScroll.style.display = "none";
			break;
	}
}

function ShopTabChange(tab, sMainTab){
	var obj;

	//scroll the tab list automatically
    Shops.scrollLeft = 0;
    if (tab && tabScroll.style.display == "inline")
      if (((tab.offsetLeft + tab.offsetWidth) >= Shops.offsetWidth))
        tab.scrollIntoView(true);

	for (i=1; i<=gsShopCount; i++){
		obj = eval("tab4" + i);
		obj.className = "tab1";
	}
	top.gsShopID = tab.shopid;
  top.gsBillingID = tab.billingid;
	tab.className = "tabactive1";
	TabChange(tab.page + "?ShopID=" + tab.shopid, sMainTab);
  top.refreshNotesWindow(tab.shopid, gsUserID);
  top.refreshDocumentsWindow(tab.shopid, gsUserID);
}

function TabChange(sPage, sMainTab){
	var obj;
  var qString = "";
	var sEntityID;
	var sEntityType;

  btnSave.disabled = true;
  btnDelete.disabled = true;
  btnNew.style.display = "none";
  btnCancel.style.display = "none";

	switch(sMainTab){
		case "businfo":
			sEntityID = top.gsBusinessInfoID;
   		sEntityType = "B";
			break;
		case "shop":
			sEntityID = top.gsShopID;
			sEntityType = "S";
			break;
	}

	switch(sPage){
		case "SMTShopInfo.asp":
		case "SMTShopSpecialty.asp":
		case "SMTShopPricing.asp":
		case "SMTShopOEMDiscount.asp":
    case "SMTShopInsurance.asp":
			qString = "?ShopID=" + top.gsShopID;
			break;
		case "SMTShopBilling.asp":
      if (top.gsBillingID == 0){
        ClientWarning("The Billing page was unable to load.  Please try again once the current page is fully displayed.");
        return;
      }
			qString = "?BillingID=" + top.gsBillingID;
			break;
		case "SMTBusinessInfo.asp":
			qString = "?BusinessInfoID=" + top.gsBusinessInfoID;
			break;
		case "SMTBusinessParent.asp":
			qString = "?BusinessParentID=" + top.gsBusinessParentID;
			break;
		case "SMTPersonnel.asp":
			qString = "?EntityID=" + sEntityID + "&EntityType=" + sEntityType;
			break;
    case "SMTMain.asp":
      btnDelete.style.visibility = "hidden";
      if (gsShopCRUD.indexOf("U") > -1) btnSave.style.visibility = "visible";
      qString = "?PageID=BusinessIncludeShop&EntityID=" + sEntityID;
      break;
	}

	switch(sMainTab){
		case "businfo":
			tab11.className = "tab1";
   		tab12.className = "tab1";
      tab13.className = "tab1";
			break;
		case "shop":
			tab21.className = "tab1";
   		tab22.className = "tab1";
   		tab23.className = "tab1";
			tab24.className = "tab1";
   		tab25.className = "tab1";
			tab26.className = "tab1";
      tab27.className = "tab1";
			break;
	}

	var sHREF = sPage + qString;
	gsPage = sPage;
	gsMainTab = sMainTab;
	if (ifrmContent.document.all.txtCallBack){
		ifrmContent.document.all.txtCallBack.value = sHREF;
	}

	if (CheckDirty()){
    	document.frames["ifrmContent"].frameElement.src = sHREF;
   		obj = document.getElementById("ifrmContent");
   		obj.style.visibility = "visible";
   		obj.style.display = "inline";
	}
}

function IsDirty(){
	if (gbDirtyFlag == true){
		ifrmContent.DirtyPage();
		gbDirtyFlag = false;
	}
}

function Unload(){
  if (gsMode != "parentwizard"){
	  parent.layerTabs.style.visibility="hidden";
    top.AdminMenu(false, 1);
  }
}

if (gsMode != "parentwizard"){
  if (document.attachEvent)
    document.attachEvent("onclick", top.hideAllMenuScriptlets);
  else
    document.onclick = top.hideAllMenuScriptlets;
}
    
]]>

</script>

</HEAD>

<BODY class="bodyAPDSub" unselectable="on" onload="initPage()" onunload="Unload()" bgcolor="#FFFAEB">

  <!-- outer Shop Tabs for Businesses with multiple Shops -->
  <DIV unselectable="on" style="position:absolute; visibility: visible; top: 0px; left: 4px; z-index:1000">
  <div id="tabScroll" style="position:absolute;top:3px; left:695px;width:35px;z-index:200;display:none">
    <button class="toolbarButton" tabIndex="-1" onmouseover="this.className='toolbarButtonOver'" onmouseout="this.className='toolbarButton'" onmousedown="Shops.doScroll('pageleft');this.fireEvent('onblur');"><span style="font:10pt Marlett">3</span></button>
    <button class="toolbarButton" tabIndex="-1" onmouseover="this.className='toolbarButtonOver'" onmouseout="this.className='toolbarButton'" onmousedown="Shops.doScroll('pageright');this.fireEvent('onblur');"><span style="font:10pt Marlett">4</span></button>
  </div>
  <DIV unselectable="on" id="Shops" class="apdtabs" style="width:687px; overflow:hidden; top:-1px;">
    <SPAN id="tab10" unselectable="on" style="width:0px;height:0px;"></SPAN>
	<xsl:for-each select="Shop">
	  <SPAN unselectable="on" class="tab1" onclick="ShopTabChange(this, 'shop')" style="cursor:hand; margin-left:0px;">
	    <xsl:attribute name="title">
		    <xsl:value-of select="normalize-space(@Name)"/>
		    <xsl:if test="@Address1 != ''">&#13;&#10;<xsl:value-of select="normalize-space(@Address1)"/></xsl:if>
		    <xsl:if test="@AddressCity != ''">&#13;&#10;<xsl:value-of select="normalize-space(@AddressCity)"/></xsl:if>
		    <xsl:if test="@AddressCity != '' and @AddressState != ''">,</xsl:if>
		    <xsl:if test="@AddressCity = '' and @AddressState != ''">&#13;&#10;</xsl:if>
		    <xsl:if test="@AddressState != ''"><xsl:text> </xsl:text><xsl:value-of select="@AddressState"/></xsl:if>
		    <xsl:if test="(@AddressCity != '' or @AddressState != '') and @AddressZip != ''"><xsl:text>  </xsl:text></xsl:if>
		    <xsl:if test="@AddressCity = '' and @AddressState = '' and @AddressZip != ''">&#13;&#10;</xsl:if>
		    <xsl:if test="@AddressZip != ''"><xsl:value-of select="@AddressZip"/></xsl:if>
		  </xsl:attribute>
		  <xsl:attribute name="page">SMTShopInfo.asp</xsl:attribute>
		  <xsl:attribute name="shopid"><xsl:value-of select="@ShopID"/></xsl:attribute>
      <xsl:attribute name="billingid"><xsl:value-of select="@BillingID"/></xsl:attribute>
	    <xsl:attribute name="id">tab4<xsl:value-of select="position()"/></xsl:attribute>
		  <xsl:attribute name="class">
		    <xsl:choose>
		      <xsl:when test="@ShopID = /Root/@SearchShopID">tabactive1</xsl:when>
			    <xsl:otherwise>tab1</xsl:otherwise>
		    </xsl:choose>
		  </xsl:attribute>
		  <xsl:value-of select="substring(@Name, 0, 8)"/>..
	  </SPAN>
	</xsl:for-each>
  </DIV>
  </DIV>

  <!-- Shop Tabs -->
  <DIV unselectable="on" class="content1" id="content11" style="position:absolute; left:3px; top:19px; width:747; height:518;">
    <DIV unselectable="on" id="ShopTabs" class="apdtabs" style="position:absolute; left:5; top:4; visibility:hidden;">
      <SPAN unselectable="on" id="tab21" page="SMTShopInfo.asp" class="tabactive1" onclick="TabChange(this.page, 'shop')" style="cursor:hand; margin-left:0px;">Shop</SPAN>
	    <SPAN unselectable="on" id="tab22" page="SMTPersonnel.asp" class="tab1" onclick="TabChange(this.page, 'shop')" style="cursor:hand; margin-left:0px;">Personnel</SPAN>
	    <SPAN unselectable="on" id="tab23" page="SMTShopBilling.asp" class="tab1" onclick="TabChange(this.page, 'shop')" style="cursor:hand; margin-left:0px;">Payment</SPAN>
	    <SPAN unselectable="on" id="tab24" page="SMTShopSpecialty.asp" class="tab1" onclick="TabChange(this.page, 'shop')" style="cursor:hand; margin-left:0px;">Specialties</SPAN>
	    <SPAN unselectable="on" id="tab25" page="SMTShopPricing.asp" class="tab1" onclick="TabChange(this.page, 'shop')" style="cursor:hand; margin-left:0px;">Pricing</SPAN>
	    <SPAN unselectable="on" id="tab26" page="SMTShopOEMDiscount.asp" class="tab1" onclick="TabChange(this.page, 'shop')" style="cursor:hand; margin-left:0px;">OEM Discount</SPAN>
      <SPAN unselectable="on" id="tab27" page="SMTShopInsurance.asp" class="tab1" onclick="TabChange(this.page, 'shop')" style="cursor:hand; margin-left:0px;">Insurance</SPAN>
    </DIV>

  <!-- Business Tabs -->
  <DIV unselectable="on" id="BusTabs" class="apdtabs" style="position:absolute; left:5; top:4; visibility:hidden;">
    <SPAN unselectable="on" id="tab11" page="SMTBusinessInfo.asp" class="tabactive1" onclick="TabChange(this.page, 'businfo')" style="cursor:hand; margin-left:0px;">Business</SPAN>
	  <SPAN unselectable="on" id="tab12" page="SMTPersonnel.asp" class="tab1" onclick="TabChange(this.page, 'businfo')" style="cursor:hand; margin-left:0px;">Personnel</SPAN>
    <SPAN unselectable="on" id="tab13" page="SMTMain.asp" class="tab1" onclick="TabChange(this.page, 'businfo')" style="cursor:hand; margin-left:0px;">Include Shop</SPAN>
  </DIV>

  <!-- Parent Tabs -->
  <DIV unselectable="on" id="ParentTabs" class="apdtabs" style="position:absolute; left:5; top:4; visibility:hidden;">
    <SPAN unselectable="on" id="tab31" page="SMTBusinessParent.asp" class="tabactive1" style="cursor:hand; margin-left:0px;">Parent</SPAN>
  </DIV>

  <!-- action buttons -->
  <DIV align="right" style="position:absolute; left:555; top:3; width:180; height:16;">
    <input type="button" id="btnNew" value="New" class="formButton" style="width:45; visibility:hidden;" onclick="ifrmContent.Add()"/>
    <input type="button" id="btnCancel" value="Cancel" class="formButton" style="width:45; visibility:hidden;" onclick="ifrmContent.Cancel()"/>
	  <input type="button" id="btnDelete" value="Delete" class="formButton" style="width:45; visibility:hidden;" onclick="ifrmContent.Delete()"/>
    <input type="button" id="btnSave" value="Save" class="formButton" style="width:45; visibility:hidden;" onclick="ifrmContent.Save()"/>
  </DIV>

  <!-- content frame -->
  <DIV unselectable="on" class="SmallGrouping" style="position:absolute; left:3px; top:23px;">
    <IFRAME id="ifrmContent" src="../blank.asp" style="position:relative; width:720; height:475; border:0px; background:transparent; overflow:hidden" allowtransparency="true" >
    </IFRAME>
  </DIV>
 </DIV>
</BODY>
</HTML>
</xsl:template>

</xsl:stylesheet>
