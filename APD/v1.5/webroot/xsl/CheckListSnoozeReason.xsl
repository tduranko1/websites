<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:local="http://local.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt">

  <xsl:import href="msxsl/msjs-client-library-htc.js"/>

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="UserId"/>
  <xsl:param name="AlarmDateOld"/>
  <xsl:param name="AlarmDateNew"/>

  <xsl:template match="/">

    <html>
      <head>

        <title>Enter Delay Reason</title>
        <link rel="stylesheet" href="/css/apdcontrols.css" type="text/css" />
        <script type="text/javascript" language="JavaScript1.2"  src="/js/apdcontrols.js"></script>
        <script language="JavaScript">

			      var strCheckListID = '<xsl:value-of select="/Root/@CheckListID"/>';
						var strUserID = '<xsl:value-of select="$UserId"/>';
						var strAlarmDateOld = '<xsl:value-of select="$AlarmDateOld"/>';
						var strAlarmDateNew = '<xsl:value-of select="$AlarmDateNew"/>';

						var objReasons  = new Array();
						var objReason;

    		    <xsl:for-each select="/Root/TaskDelayReason">
								objReason = { comment:"<xsl:value-of select='@CommentText'/>" };
								objReasons.push( objReason );
    		    </xsl:for-each>

            <![CDATA[

            function initPage()
						{
								window.returnValue = "false";
            }

            function reasonSelected()
						{
								txtCommentText.CCDisabled = false;
								txtCommentText.value = objReasons[ selReason.selectedIndex ].comment;
            }

            function doSave()
						{
                var sProc = "";
                var sRequest = "";
                var aRequests = new Array();
                var sMethod = "";
                var sNotes = "";

                if ( selReason.value == null )
								{
                  	ClientWarning( "Please select a task delay reason." );
                  	return;
                }

                if ( /* txtCommentText.required == true && */ txtCommentText.value == "" )
								{
                  	ClientWarning( "Please enter a comment to be associated with this task delay reason." );
                  	return;
                }

                if ( selNoteType.value == null )
								{
                  	ClientWarning( "Please select a note type." );
                  	return;
                }

                sProc = "uspDiaryDelayTask";
                sRequest = "ChecklistID=" + strCheckListID +
                            "&AlarmDateOld=" + strAlarmDateOld +
                            "&AlarmDateNew=" + strAlarmDateNew +
                            "&DelayUserID=" + strUserID +
                            "&TaskDelayReasonID=" + selReason.value +
                            "&DelayComment=" + escape( txtCommentText.value ) +
														"&NoteTypeID=" + selNoteType.value;
                sMethod = "ExecuteSpNp";

                //alert(sProc + "\n" + sRequest); return;

                aRequests.push( { procName : sProc,
                                  method   : sMethod,
                                  data     : sRequest }
                              );
                var sXMLRequest = makeXMLSaveString(aRequests);
                //alert(sXMLRequest);

                var objRet = XMLSave(sXMLRequest);

                if ( objRet && objRet.code == 0 ) {
              			window.returnValue = "true";
              			window.close();
                }
						}

            function doCancel()
						{
              	window.returnValue = "false";
              	window.close();
            }


            ]]>

        </script>

      </head>
      <body style="margin:0px;padding:0px;" onload="initPage()">
        <div style="position:absolute; z-index:1; top:5px; left:5px;">
    		<table width='100%' border='0' cellspacing='0' cellpadding='2' style="table-layout:fixed">
    		  <colgroup>
    		    <col width="85px"/>
    		    <col width="*"/>
    		  </colgroup>
    		  <tr>
    		    <td>Delay Reason</td>
    		    <td>
    		      <IE:APDCustomSelect id="selReason" name="selReason" displayCount="5"
								blankFirst="false" canDirty="false" CCTabIndex="6" required="true" width="220" onChange="reasonSelected()">

								<!-- Auto select the first item ONLY if there is but one item in the list. -->
    		        <xsl:if test="count( /Root/TaskDelayReason ) = 1">
    		          <xsl:attribute name="selectedIndex">0</xsl:attribute>
    		          <xsl:attribute name="CCDisabled">true</xsl:attribute>
    		        </xsl:if>

    		        <xsl:for-each select="/Root/TaskDelayReason">
    		      	<IE:dropDownItem imgSrc="" style="display:none">
    		          <xsl:attribute name="value"><xsl:value-of select="@TaskDelayReasonID"/></xsl:attribute>
    		          <xsl:value-of select="@ReasonText"/>
    		        </IE:dropDownItem>
    		        </xsl:for-each>

    		      </IE:APDCustomSelect>
    		    </td>
    		  </tr>
    		  <tr>
    		    <td valign="top">Comment</td>
    		    <td>
    		      <IE:APDTextArea id="txtCommentText" name="txtCommentText" width="220" height="100"
								canDirty="false" required="true" CCTabIndex="9" onChange="" >

								<!-- Auto select the first comment ONLY if there is but one task reason in the list. -->
    		        <xsl:if test="count( /Root/TaskDelayReason ) = 1">
    		        	<xsl:attribute name="value"><xsl:value-of select="/Root/TaskDelayReason/@CommentText"/></xsl:attribute>
    		          <xsl:attribute name="CCDisabled">false</xsl:attribute>
    		        </xsl:if>

								<!-- Disable the comment field and force them to choose a reason first. -->
    		        <xsl:if test="count( /Root/TaskDelayReason ) != 1">
    		          <xsl:attribute name="CCDisabled">true</xsl:attribute>
    		        </xsl:if>

    		        <xsl:attribute name="maxLength"><xsl:value-of select="/Root/Metadata[@Entity='TaskDelayReason']/Column[@Name='CommentText']/@MaxLength"/></xsl:attribute>
    		        <xsl:attribute name="required"><xsl:value-of select="/Root/Metadata[@Entity='TaskDelayReason']/Column[@Name='CommentText']/@Nullable"/></xsl:attribute>
    		      </IE:APDTextArea>
    		    </td>
    		  </tr>
      		<tr>
      		  <td>Note Type</td>
      		  <td>
      		    <xsl:choose>

								<!-- When there is only one note type, display a simple label instead of a combo box -->
      		      <xsl:when test="count(/Root/NoteType)=1">
      		        <input type="hidden" id="selNoteType" name="selNoteType" >
      		          <xsl:attribute name="value"><xsl:value-of select="/Root/NoteType/@NoteTypeID"/></xsl:attribute>
									</input>
      		        <IE:APDLabel id="selNoteTypeLabel" name="selNoteTypeLabel" width="220" >
      		          <xsl:attribute name="value"><xsl:value-of select="/Root/NoteType/@Name"/></xsl:attribute>
      		        </IE:APDLabel>
      		      </xsl:when>

								<!-- Display the combo box and let the user choose a note type -->
      		      <xsl:otherwise>
      		        <IE:APDCustomSelect id="selNoteType" name="selNoteType" displayCount="5" blankFirst="false"
										canDirty="false" CCDisabled="false" CCTabIndex="1" required="false" width="216" onChange="">
      		          <xsl:attribute name="value"><xsl:value-of select="/Root/NoteType/@NoteTypeID"/></xsl:attribute>
      		          <xsl:for-each select="/Root/NoteType">
      		        	<IE:dropDownItem imgSrc="" style="display:none">
      		            <xsl:attribute name="value"><xsl:value-of select="@NoteTypeID"/></xsl:attribute>
      		            <xsl:value-of select="@Name"/>
      		          </IE:dropDownItem>
      		          </xsl:for-each>
      		        </IE:APDCustomSelect>
      		      </xsl:otherwise>

      		    </xsl:choose>
      		  </td>
      		</tr>
    		  <tr>
    		    <td colspan="2">
    		      <table border="0" cellpadding="0" cellspacing="0" style="width:100%;table-layout:fixed">
    		        <colgroup>
    		          <col width="*"/>
    		          <col width="150px"/>
    		          <col width="*"/>
    		        </colgroup>
    		        <tr>
    		          <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    		          <td>
    		            <IE:APDButton id="btnSave" name="btnSave" value="Save" width="" CCDisabled="false" CCTabIndex="11" onButtonClick="doSave()"/>
    		            <IE:APDButton id="btnCancel" name="btnCancel" value="Cancel" width="" CCDisabled="false" CCTabIndex="12" onButtonClick="doCancel()"/>
    		          </td>
    		          <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
    		        </tr>
    		      </table>
    		    </td>
    		  </tr>
    		</table>
        </div>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>

