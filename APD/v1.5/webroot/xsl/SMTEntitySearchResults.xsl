<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTEntitySearchResults">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTEntitySearchGetListXML,SMTEntitySearchResults.xsl,'S', null, '', 'FL', '', '', '', '239'   -->

<HEAD>
  <TITLE>Search Results</TITLE>
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  
<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDNavigate.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
var gsSearchType = '<xsl:value-of select="@SearchType"/>';
var gsCount = '<xsl:value-of select="count(Entity)"/>';

<![CDATA[

function initPage(){
	parent.ShowCount(gsCount);
	divResultList.scrollTop = parent.parent.gsScroll;	
}

function GridSelect(oRow){
  var sEntityID = oRow.cells[0].innerText;
  var sShopBusinessID = oRow.cells[0].getAttribute("ShopBusinessID");
	try{
    NavToShop(gsSearchType, sEntityID, sShopBusinessID );
  }
  catch(e){
    alert(e.message);
  }  
}

]]>
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" style="background:transparent;" onLoad="initPage();">

<DIV id="CNScrollTable">
<TABLE unselectable="on" id="tblHead" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSort1')" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
  <TBODY>
    <TR unselectable="on" class="QueueHeader">
      <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="99" width="52" nowrap="nowrap" style="height:20; cursor:default;"> ID </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" width="258" nowrap="nowrap" style="cursor:default;"> Name </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" nowrap="nowrap" style="cursor:default;"> 
	    <xsl:attribute name="width">
  		  <xsl:choose>
  		    <xsl:when test="@SearchType='S'">201</xsl:when>
  			  <xsl:otherwise>246</xsl:otherwise>
  		  </xsl:choose>
  		</xsl:attribute>
	    Address 
	  </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" width="125" nowrap="nowrap" style="cursor:default;"> City </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" width="47" style="cursor:default;"> Zip </TD>
      <xsl:if test="@SearchType='S'">
	    <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" width="55" style="cursor:default;"> Program </TD>
	  </xsl:if>
    </TR>
  </TBODY>
</TABLE>

<DIV unselectable="on" id="divResultList" class="autoflowTable" style="height:365px;">
  <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" border="0" cellspacing="1" cellpadding="0" style="table-layout:fixed;">
    <TBODY bgColor1="ffffff" bgColor2="fff7e5">
      <xsl:for-each select="Entity" >
	      <xsl:call-template name="SearchResults"><xsl:with-param name="SearchType" select="/Root/@SearchType"/></xsl:call-template>
      </xsl:for-each>
    </TBODY>
  </TABLE>
</DIV>
</DIV>
</BODY>
</HTML>
</xsl:template>

  <!-- Gets the search results -->
  <xsl:template name="SearchResults">
    <xsl:param name="SearchType"/>
    <!--<script>alert("<xsl:value-of select='$SearchType'/>")</script>-->
    <TR unselectable="on" onMouseOut="GridMouseOut(this)" onMouseOver="GridMouseOver(this)" onclick="GridSelect(this)">
      <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
      <TD unselectable="on" class="GridTypeTD" width="50" style="text-align:left">
        <xsl:attribute name="ShopBusinessID"><xsl:value-of select="@ShopBusinessID"/></xsl:attribute>
        <xsl:choose>
    			<xsl:when test = "@EntityID != ''"><xsl:value-of select="@EntityID"/></xsl:when>
    			<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
    		</xsl:choose>				
      </TD>
      <TD unselectable="on" width="257" class="GridTypeTD" style="text-align:left">
        <xsl:choose>
    			<xsl:when test = "@Name != ''"><xsl:value-of select="@Name"/></xsl:when>
    			<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
    		</xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" style="text-align:left">
  	    <xsl:attribute name="width">
    		  <xsl:choose>
    		    <xsl:when test="$SearchType='S'">200</xsl:when>
    			  <xsl:otherwise>245</xsl:otherwise>
    		  </xsl:choose>
    		</xsl:attribute>
        <xsl:choose>
    			<xsl:when test = "@Address1 != ''"><xsl:value-of select="@Address1"/></xsl:when>
    			<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
    		</xsl:choose>
      </TD>
      <TD unselectable="on" width="125" class="GridTypeTD" style="text-align:left">
    	  <xsl:choose>
          <xsl:when test = "@AddressCity != ''"><xsl:value-of select="@AddressCity"/></xsl:when>
    			<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
    		</xsl:choose>
        <xsl:if test="@AddressCity != '' and @AddressState != ''">,<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:if>
        <xsl:choose>
    			<xsl:when test = "normalize-space(@AddressState) != ''"><xsl:value-of select="@AddressState"/></xsl:when>
    			<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
    		</xsl:choose>
      </TD>
      <TD unselectable="on" width="45" class="GridTypeTD">
        <xsl:choose>
    			<xsl:when test = "normalize-space(@AddressZip) != ''"><xsl:value-of select="@AddressZip"/></xsl:when>
    			<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
    		</xsl:choose>  
      </TD>
      <xsl:if test="$SearchType='S'">
        <TD unselectable="on" width="55" class="GridTypeTD">
			<xsl:value-of select="@ShopType"/>
          <!--<xsl:choose>
      			<xsl:when test = "@ProgramFlag = 1">Yes</xsl:when>
      			<xsl:otherwise>No</xsl:otherwise>
      		</xsl:choose>-->
    		</TD>
    	</xsl:if>
    </TR>
  </xsl:template>

</xsl:stylesheet>