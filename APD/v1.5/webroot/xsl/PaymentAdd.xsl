<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimPayment">

<xsl:import href="msxsl/msxsl-function-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="WindowID"/>
<xsl:param name="LynxId"/>
<xsl:param name="Entity"/>
<xsl:param name="UserId"/>
<xsl:param name="ClaimAspectID"/>
<xsl:param name="IntervalID"/>
<xsl:param name="InvoiceID"/>
<xsl:param name="close"/>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="PaymentCRUD" select="Payment"/>

<xsl:template match="/Root">

<xsl:if test="$close='yes'">
  <script>
    var iIntervalID = '<xsl:value-of select="$IntervalID"/>';
    if (iIntervalID != "") window.clearInterval(iIntervalID);
    window.returnValue = "OK";
    window.close();
  </script>
</xsl:if>

<xsl:choose>
  <xsl:when test="contains($PaymentCRUD, 'C')">
    <xsl:call-template name="mainPage">
      <xsl:with-param name="LynxId" select="$LynxId"/>
      <xsl:with-param name="UserId" select="$UserId"/>
      <xsl:with-param name="Entity" select="$Entity"/>
      <xsl:with-param name="PaymentCRUD" select="$PaymentCRUD"/>
      <xsl:with-param name="ClaimAspectID" select="$ClaimAspectID"/>
      <xsl:with-param name="InvoiceID" select="$InvoiceID"/>
    </xsl:call-template>
  </xsl:when>
  <xsl:otherwise>
    <html>
    <head>
      <title>Add Payment</title>
        <LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
        <STYLE type="text/css">
            IE\:APDButton {behavior:url(/behaviors/APDButton.htc)}
        </STYLE>
    </head>
    <body unselectable="on" bgcolor="#FFFFFF">
    <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
      <tr>
        <td align="center">
          <font color="#ff0000"><strong>You do not have sufficient permission to add Payment information.
          <br/>Please contact administrator for permissions.</strong></font>
        </td>
      </tr>
      <tr style="height:50px">
        <td>
          <span style="position:absolute;left:490px;">
            <IE:APDButton id="btnCancel" name="btnCancel" value="Close" onButtonClick="window.returnValue='CANCEL';window.close()"/>
          </span>
        </td>
      </tr>
    </table>
    </body>
    </html>
  </xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="mainPage">
  <xsl:param name="LynxId"/>
  <xsl:param name="UserId"/>
  <xsl:param name="Entity"/>
  <xsl:param name="PaymentCRUD"/>
  <xsl:param name="ClaimAspectID"/>
  <xsl:param name="InvoiceID"/>


<HTML>
<HEAD>

<TITLE>Add Payment</TITLE>
<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>

<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formats.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,37);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
	var gsLynxID = "<xsl:value-of select="$LynxId"/>";
  var gsUserID = "<xsl:value-of select="$UserId"/>";
  var gsEntity = "<xsl:value-of select="$Entity"/>";
  var gsEntityName = "<xsl:value-of select="ClaimAspect[@EntityCode=$Entity]/@Name"/>";
  var gsClaimAspectID = "<xsl:value-of select="$ClaimAspectID"/>";
  var gsServiceChannelCD = "<xsl:value-of select="ClaimAspect[@ClaimAspectID=$ClaimAspectID]/ClaimAspectServiceChannel/@ServiceChannelCD"/>";
  var sCRUD = "<xsl:value-of select="$PaymentCRUD"/>";
  var gsLatestEstimateDocID = "<xsl:value-of select="ClaimAspect[@ClaimAspectID=$ClaimAspectID]/@LatestEstimateDocumentID"/>";
  var gsLatestEstimateTotal = "<xsl:value-of select="ClaimAspect[@ClaimAspectID=$ClaimAspectID]/@LatestEstimateTotal"/>";
  var gsLatestEstimateTaxTotal = "<xsl:value-of select="ClaimAspect[@ClaimAspectID=$ClaimAspectID]/@LatestEstimateTotalTax"/>";
  var gsLatestEstimateDeductible = "<xsl:value-of select="ClaimAspect[@ClaimAspectID=$ClaimAspectID]/@LatestEstimateDeductible"/>";

  var gsApprovedEstimateDocID = "<xsl:value-of select="ClaimAspect[@ClaimAspectID=$ClaimAspectID]/@ApprovedEstimateDocumentID"/>";
  var gsApprovedEstimateTotal = "<xsl:value-of select="ClaimAspect[@ClaimAspectID=$ClaimAspectID]/@ApprovedEstimateTotal"/>";
  var gsApprovedEstimateTaxTotal = "<xsl:value-of select="ClaimAspect[@ClaimAspectID=$ClaimAspectID]/@ApprovedEstimateTotalTax"/>";
  var gsApprovedEstimateDeductible = "<xsl:value-of select="ClaimAspect[@ClaimAspectID=$ClaimAspectID]/@ApprovedEstimateDeductible"/>";
  
  var gsEarlyBillFlag = "<xsl:value-of select="/Root/@EarlyBillFlag"/>";
  
  var gsPayoffAmountConfirmedFlag = "0";
  var gsLoGReceivedFlag = "0";
  var gsFederalTaxIDExistsFlag = "0";
  var gsLoGAmount = "<xsl:value-of select="ClaimAspect[@ClaimAspectID=$ClaimAspectID]/ClaimAspectServiceChannel/@LetterOfGuaranteeAmount"/>";


  var gsDisposition = "<xsl:value-of select="ClaimAspect[@ClaimAspectID=$ClaimAspectID]/@DispositionTypeCD"/>";
  var gbDataIslandAvailable = false;
  var gsInvoiceID = "<xsl:value-of select="$InvoiceID"/>";
  var gsSysLstUpdatedDate = "<xsl:value-of select="/Root/Invoice[@InvoiceID=$InvoiceID]/@SysLastUpdatedDate"/>";
  window.name = "thisWin";
  var strMECount = "<xsl:value-of select="count(/Root/ClaimAspect[@ServiceChannelCD='ME'])"/>";

  
  
  
  <![CDATA[
    function __pageInit(){
      try {        
        PaymentStat.style.top = (document.body.offsetHeight - 75) / 2;
        PaymentStat.style.left = (document.body.offsetWidth - 240) / 2;
        if (!gbDataIslandAvailable){
          btnSave.CCDisabled = true;
          var sErrMsg = "There is a problem with the Add Payment dialog's XML Data Island for LynxID=" + gsLynxID;
          sErrMsg += ", which consist of all instances of the following nodes:  'Payee', 'Claim', 'ClaimAspect', and 'Reference/PaymentType'.  ";
          sErrMsg += "Check for invalid attribute values such as values ending with an ampersand."
          ClientError(sErrMsg);
        }
        else{
          window.setTimeout("csPayee_change()", 150);
        }
                        
      } catch (e) {
        ClientError("An error occured while executing client side function pageInit(): \nError description:" + e.description);
      }
    }    
    
    function csPayee_change(){      
      if (csPayee.selectedIndex == -1)
        return;
    
      var sXPath, sPayeeTypeCD, oNode, oPayeeTypeNode;    
        
      sXPath = gsInvoiceID == "" ? '//Payee[@PayeeID="' + csPayee.value + '"]' : '//Invoice[@InvoiceID="' + gsInvoiceID + '"]';
      oNode = xmlPayment.documentElement.selectSingleNode( sXPath );
      
      sPayeeTypeCD = oNode.getAttribute("PayeeTypeCD");
      
      sXPath = '//Reference[@List="PayeeTypeCD" and @ReferenceID="' + sPayeeTypeCD + '"]';
      oPayeeTypeNode = xmlPayment.documentElement.selectSingleNode( sXPath );
            
      lblPayeeType.value = oPayeeTypeNode.getAttribute("Name");
      lblPayeeType.setAttribute("PayeeTypeCD", oPayeeTypeNode.getAttribute("ReferenceID"));
      lblPayeeAddress1.value = oNode.getAttribute("PayeeAddress1");
      lblPayeeAddress2.value = oNode.getAttribute("PayeeAddress2");
      lblPayeeCity.value = oNode.getAttribute("PayeeAddressCity");
      lblPayeeState.value = oNode.getAttribute("PayeeAddressState");
      lblPayeeZip.value = oNode.getAttribute("PayeeAddressZip");
      try {
      gsPayoffAmountConfirmedFlag = oNode.getAttribute("PayoffAmountConfirmedFlag");
      gsFederalTaxIDExistsFlag = oNode.getAttribute("FederalTaxIDExistsFlag");
      gsLoGReceivedFlag = oNode.getAttribute("LoGReceivedFlag");
      } catch (e) {}
      
      if (document.getElementById("selPaymentChannel"))
         if (selPaymentChannel.selectedIndex == -1)
            selPaymentChannel.value = oNode.getAttribute("PaymentChannelCD");
         
      if (document.getElementById("selPaymentTypeCD"))
         if (selPaymentTypeCD.selectedIndex == -1)
            selPaymentTypeCD.value = oNode.getAttribute("ItemTypeCD");
            
      //set the default payment type for shop so that the last estimate populates.
      if (selPaymentTypeCD.selectedIndex == -1 && lblPayeeType.value == "Shop")
         selPaymentTypeCD.value = "I";
         
      if (gsServiceChannelCD == "TL") {
         txtAmount.value = gsLoGAmount; 
         txtTotalTaxAmt.value = ""; 
         txtDeductibleAmt.value = ""; 
         txtDescription.value = "Total Loss Lien Holder Payment.";
      } else {
         if ((selPaymentTypeCD.value == "I") && (lblPayeeType.value == "Shop") && (txtAmount.value == "")){
           if (gsEarlyBillFlag == "1" && gsLatestEstimateDocID != gsApprovedEstimateDocID){
              if (oNode.getAttribute("PayeeWarrantyActiveAssignmentFlag") == "0"){
                 var iDifference = (isNaN(gsLatestEstimateTotal) == false && isNaN(gsApprovedEstimateTotal) == false ? parseFloat(gsLatestEstimateTotal) - parseFloat(gsApprovedEstimateTotal) : -1);
                 var sRet = "No";
                 if (iDifference > 0){
                     iDifference = formatCurrency(iDifference);
                     sRet = YesNoMessage("Confirm", "The latest Estimate/Supplement seems to be greater than the Approved Estimate. Would you like to add a payment for the difference?\n" +
                                                    "  Click Yes to add the difference of $" + formatCurrencyString(iDifference) + " or \n" +
                                                    "  Click No to add the full latest amount of $" + gsLatestEstimateTotal);
                 }
                 if (sRet == "Yes")
                     txtAmount.value = iDifference; 
                 else
                     txtAmount.value = gsLatestEstimateTotal; 
                 txtTotalTaxAmt.value = gsLatestEstimateTaxTotal; 
                 txtDeductibleAmt.value = gsLatestEstimateDeductible; 
              }
           } else {
              txtAmount.value = gsLatestEstimateTotal; 
              txtTotalTaxAmt.value = gsLatestEstimateTaxTotal; 
              txtDeductibleAmt.value = gsLatestEstimateDeductible; 
           }
         }
      }
    }
    
    
    function xmlInit(){     
        xmlPayment.setProperty('SelectionLanguage', 'XPath');    
        gbDataIslandAvailable = true;
    } 
            
    function RSrefreshPage(co){
      if (co.status == 0){
        sTblData = co.return_value;
        var listArray = sTblData.split("||");
        if ( listArray[1] == "0" )
            ServerEvent();
        else{
          if (co.context.value = "null" || co.context.value == "" || co.context.value == " " || co.context.value.indexOf("?") != -1)
            co.context.value = listArray[0].replace(/\s*$/, "");
        }
      }
      else ServerEvent();
    }

    function RSerrHandler(co){
      if (co.status != 0){
  	    co.context.value = co.message;
      }
    }
        
    function btnCancelClick(){
      window.returnValue = "CANCEL";
      window.close();
    }

    function btnSaveClick() {
      
      if (csPayee.selectedIndex == -1){
        ClientWarning("A Payee must be selected");
        return;
      }
      
      if (csPayee.text.indexOf("[Inactive") != -1){
         if (YesNoMessage("Confirm Payee", "The Payee you have selected does not have an active assignment on the vehicle. Please verify the Payee address. Click 'Yes' if you want the payment to go this Payee.") != "Yes"){
            return;
         }
      }
      
      if (txtAmount.value == "" || txtAmount.value == "$"){
        ClientWarning("A Payment Amount is required");
        txtAmount.setFocus();
        return;
      }
            
      if (!isNaN(txtAmount.value)){
        if (txtAmount.value <= 0){
          ClientWarning("A payment Amount greater than 0 is required");
          txtAmount.setFocus();
          return;
        }
      }
      if (gsServiceChannelCD != "TL") {
         if (txtAmount.value != gsLatestEstimateTotal && gsDisposition != "TL" && gsLatestEstimateTotal != ""){
           var sMsg = "The Amount you have entered, $" + txtAmount.value + ", does not match the most recent Estimate for this vehicle, $";
           sMsg += gsLatestEstimateTotal + ".  Are you sure you want to enter an Amount that differs from the most recent Estimate?";
           sMsg += "\n\n 'Yes' - to continue with the Amount you have entered.";
           sMsg += "\n 'No' - to replace the Amount with the most recent Estimate and continue.";
           sMsg += "\n 'Cancel' - to abort.";
           var sConfirm = YesNoMessage("Payment", sMsg);
           
           if (sConfirm == "No") txtAmount.value = gsLatestEstimateTotal;
           if (sConfirm == "") return;
         }
      } else {
         if (gsPayoffAmountConfirmedFlag != "1"){
            ClientWarning("Total Loss Payoff amount has not been confirmed yet. Cannot add a payment.");
            return;
         }
         if (gsFederalTaxIDExistsFlag != "1") {
            ClientWarning("Federal Tax ID has not been set for the Lien Holder. Cannot add a payment.");
            return;
         }
      }
        

      if (lblPayeeAddress1.value == ""){      
        ClientWarning("An Address 1 is required.  Contact Participant Services and ensure the assigned shop has all required data.");
        //lblPayeeAddress1.setFocus();
        return;
      }

      if (lblPayeeZip.value == ""){      
        ClientWarning("A Zip Code is required.  Contact Participant Services and ensure the assigned shop has all required data.");
        //txtAddressZip.setFocus();
        return;
      }

      if (lblPayeeCity.value == ""){      
        ClientWarning("A City is required.  Contact Participant Services and ensure the assigned shop has all required data.");
        //txtAddressCity.setFocus();
        return;
      }

      if (lblPayeeState.value == ""){      
        ClientWarning("A State is required. Contact Participant Services and ensure the assigned shop has all required data.");
        //txtAddressState.setFocus();
        return;
      }

      if (txtDescription.value == ""){
        ClientWarning("A Description is required");
        txtDescription.setFocus();
        return;
      }

      PaymentStat.Show("Please be patient.  Adding Payment info...");
      window.setTimeout("btnSaveClick2()", 150);
    }

    function btnSaveClick2() {
      try {
        if (SavePaymentInfo() == true) {
          window.returnValue = "OK";
          window.close();
        }
      } catch (e) {
        ClientError("An error occured while executing client side function btnSaveClick2(): \nError description:" + e.description);
      } finally {
        PaymentStat.Hide();
      }
    }

    function SavePaymentInfo() {
      try {
        var bAddSuccess = false;
        var strPaymentChannelCD = "";
        var sRequest = "";
        var sProc = gsInvoiceID == "" ? "uspInvoiceInsDetail" : "uspInvoiceUpdDetail";

        if (selPaymentTypeCD.selectedIndex == -1){
            ClientWarning("Please select a payment type from the list and try again.");
            return false;
        }
        
        if (document.getElementById("selPaymentChannel")){
            if (selPaymentChannel.selectedIndex == -1){
               ClientWarning("Please select a payment channel from the list and try again.");
               return false;
            }
            strPaymentChannelCD = selPaymentChannel.value;
        }
        
        if (lblPayeeState.value == "NY"){
            if (txtDeductibleAmt.value == ""){
               ClientWarning("Please enter the Deductible Amount.");
               return false;
            }
            if (txtTotalTaxAmt.value == ""){
               ClientWarning("Please enter the Total Tax Amount.");
               return false;
            }
        }
        
        var objPayee = xmlPayment.selectSingleNode( "//Payee[@PayeeID='" + csPayee.value + "']" );
        var strPayeeName = objPayee.getAttribute("PayeeName");
        if (strPayeeName != "" && strPayeeName != null){
            var iQuoteCount = strPayeeName.split("'").length;
            if (iQuoteCount > 0) iQuoteCount--;
            strPayeeName = strPayeeName.substr(0, 50 - iQuoteCount);
        }
        
        sRequest = "ClaimAspectID=" + gsClaimAspectID +
                  "&InvoiceID=" + gsInvoiceID + 
                  "&PayeeID=" + csPayee.value +
                  "&Description=" + escape(txtDescription.value) +
                  "&ItemTypeCD=" + selPaymentTypeCD.value + 
                  "&PayeeAddress1=" + escape(lblPayeeAddress1.value) +
                  "&PayeeAddress2=" + escape(lblPayeeAddress2.value) +
                  "&PayeeAddressCity=" + escape(lblPayeeCity.value) +
                  "&PayeeAddressState=" + escape(lblPayeeState.value) +
                  "&PayeeAddressZip=" + lblPayeeZip.value +
                  "&PayeeTypeCD=" + lblPayeeType.getAttribute("PayeeTypeCD") +
                  "&PayeeName=" + escape(strPayeeName) +
                  "&PaymentChannelCD=" + strPaymentChannelCD +
                  "&Amount=" + txtAmount.value +
                  "&DeductibleAmt=" + txtDeductibleAmt.value +
                  "&TaxTotalAmt=" + txtTotalTaxAmt.value +
                  "&SysLastUserID=" + gsUserID +
                  "&SysLastUpdatedDate=" + gsSysLstUpdatedDate
        
        //alert(sRequest);
        //return;  
                  
        var aRequests = new Array();;
        aRequests.push( { procName : sProc,
                        method   : "ExecuteSpNpAsXML",
                        data     : sRequest }
                    );
        var sXMLRequest = makeXMLSaveString(aRequests);
        //alert(sXMLRequest); return;
        var objRet = XMLSave(sXMLRequest);
        if (objRet && objRet.code == 0 && objRet.xml)
          bAddSuccess = true;
        
      } catch (e) {
          ClientError("An error occured while executing client side function SavePaymentInfo(): \nError description:" + e.description);
      } finally {
      }
      return bAddSuccess;
    }
    
    function showPaymentHistory(){
      if (divPaymentHistory.style.display == "inline")
         divPaymentHistory.style.display="none";
      else
         divPaymentHistory.style.display="inline";
    }

  ]]>
</SCRIPT>
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" style="background:transparent; border:0px; overflow:hidden;background-color:#FFFFFF;padding:0px;margin:5px;">
  <SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>

  <xsl:call-template name="AddPayment">
    <xsl:with-param name="Entity" select="$Entity"/>
    <xsl:with-param name="ClaimAspectID" select="$ClaimAspectID"/>
    <xsl:with-param name="InvoiceID" select="$InvoiceID"/>
  </xsl:call-template>

  <!-- Data Island -->
  <xml id="xmlPayment" name="xmlPayment" ondatasetcomplete="xmlInit()"><xsl:copy-of select="/"/></xml>  
  
  <IE:APDStatus id="PaymentStat" name="PaymentStat" width="240" height="75"/>
  <SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

  <form id="frmPayment" method="post" action="PaymentAdd.asp" target="thisWin">
    <input name="WindowID" type="hidden">
      <xsl:attribute name="value"><xsl:value-of select="$WindowID"/></xsl:attribute>
    </input>
  </form>
  <div id="divPaymentHistory" style="position:absolute;top:30px;left:100px;background-color:#FFF8DC;border:2px solid #006400;padding:5px;display:none">
      <div style="text-align:right;padding-bottom:5px" onclick="divPaymentHistory.style.display='none'">
         <table cellpadding="2" cellspacing="0" border="0" style="border-collapse:collapse;width:100%">
            <colgroup>
               <col width="*"/>
               <col width="15px"/>
            </colgroup>
            <tr>
               <td><b>Payment History for <xsl:value-of select="//ClaimAspect[@ClaimAspectID = $ClaimAspectID]/@Name"/></b></td>
               <td><img src="/images/close.gif"/></td>
            </tr>
         </table>
      </div>
      <table cellpadding="4" cellspacing="0" border="1" style="border-collapse:collapse;border-color:#C0C0C0">
         <colgroup>
            <col width="150px"/>
            <col width="75px"/>
            <col width="60px" style="text-align:right;"/>
            <col width="60px" style="text-align:right;"/>
            <col width="60px" style="text-align:right;"/>
         </colgroup>
         <tr style="font-weight:bold">
            <td>Payee</td>
            <td>Entered</td>
            <td>Amount</td>
            <td>Ded.</td>
            <td>Tax</td>
         </tr>
         <xsl:for-each select="//Invoice[@ItemTypeCD='I' and @ClaimAspectID = $ClaimAspectID]">
            <tr>
               <td><xsl:value-of select="@PayeeName"/></td>
               <td><xsl:value-of select="user:UTCConvertDate(string(@EntryDate))"/></td>
               <td><xsl:value-of select="@Amount"/></td>
               <td><xsl:value-of select="@DeductibleAmt"/></td>
               <td><xsl:value-of select="@TaxTotalAmt"/></td>
            </tr>
         </xsl:for-each>
         <tr>
            <td colspan="2"><b>Totals</b></td>
            <td><xsl:value-of select="format-number(sum(//Invoice[@ItemTypeCD='I' and @ClaimAspectID = $ClaimAspectID]/@Amount), '######.00')"/></td>
            <td><xsl:value-of select="format-number(sum(//Invoice[@ItemTypeCD='I' and @ClaimAspectID = $ClaimAspectID]/@DeductibleAmt), '######.00')"/></td>
            <td><xsl:value-of select="format-number(sum(//Invoice[@ItemTypeCD='I' and @ClaimAspectID = $ClaimAspectID]/@TaxTotalAmt), '######.00')"/></td>
         </tr>
      </table>
  </div>

</BODY>
</HTML>
</xsl:template>

<xsl:template name="AddPayment">
  <xsl:param name="Entity"/>
  <xsl:param name="ClaimAspectID"/>
  <xsl:param name="InvoiceID"/>
  
  <table border="0" cellspacing="1" cellpadding="2" style="border-collapse:collapse;table-layout:fixed">
    <colgroup>
      <col width="225px" valign="top"/>
      <col width="10px"/>
      <col width="305px" valign="top"/>
    </colgroup>

    <tr>
      <td colspan="2" style="font-size:18px;font-weight:bold">
        Payment for
         <xsl:choose>
           <xsl:when test="contains($Entity, 'clm')">LYNX ID <xsl:value-of select="@LynxID"/></xsl:when>
           <xsl:otherwise><xsl:value-of select="ClaimAspect[@EntityCode=$Entity]/@Name"/></xsl:otherwise>
         </xsl:choose>
      </td>
      <td>
         <span style="position:absolute;left:430px">
         <IE:APDButton id="btnPaymentHistory" name="btnPaymentHistory" value="Payment History" width="105" CCDisabled="false" CCTabIndex="1" onButtonClick="showPaymentHistory()"/>
         </span>
      </td>
    </tr>
    <tr>
      <td>
        <table>
          <colgroup>
            <col width="125px"/>
            <col width="100px"/>
          </colgroup>            
          <tr>
            <td>Amount:</td>
            <td>
              <IE:APDInputCurrency id="txtAmount" name="txtAmount" width1="220" precision="8" scale="2" required="true" canDirty="true" CCTabIndex="2">
                <xsl:attribute name="value">
                  <xsl:if test="$InvoiceID != ''">
                    <xsl:value-of select="/Root/Invoice[@InvoiceID=$InvoiceID]/@Amount"/>
                  </xsl:if>
                </xsl:attribute>
              </IE:APDInputCurrency>
            </td>
          </tr>   
          <tr>
            <td>Total Tax:</td>
            <td>
              <IE:APDInputCurrency id="txtTotalTaxAmt" name="txtTotalTaxAmt" width1="220" precision="8" scale="2" required="true" canDirty="true" CCTabIndex="3">
                <xsl:attribute name="value">
                  <xsl:if test="$InvoiceID != ''">
                    <xsl:value-of select="/Root/Invoice[@InvoiceID=$InvoiceID]/@TaxTotalAmt"/>
                  </xsl:if>
                </xsl:attribute>
              </IE:APDInputCurrency>
            </td>
          </tr>   
          <tr>
            <td>Deductible:</td>
            <td>
              <IE:APDInputCurrency id="txtDeductibleAmt" name="txtDeductibleAmt" width1="220" precision="8" scale="2" required="true" canDirty="true" CCTabIndex="4">
                <xsl:attribute name="value">
                  <xsl:if test="$InvoiceID != ''">
                    <xsl:value-of select="/Root/Invoice[@InvoiceID=$InvoiceID]/@DeductibleAmt"/>
                  </xsl:if>
                </xsl:attribute>
              </IE:APDInputCurrency>
            </td>
          </tr>   
          <tr>
            <td>Payment Type:</td>
            <td>
              <IE:APDCustomSelect id="selPaymentTypeCD" name="selPaymentTypeCD" displayCount="5" blankFirst="false" canDirty="true" CCDisabled="false" CCTabIndex="5" required="true" >
                <xsl:if test="$InvoiceID != ''">
                  <xsl:attribute name="value"><xsl:value-of select="/Root/Invoice/@ItemTypeCD"/>
                  <!-- <xsl:choose>
                    <xsl:when test="$InvoiceID=''">
                      <xsl:choose>
                        <xsl:when test="/Root/Payee[@ClaimAspectID=$ClaimAspectID]/@DispositionTypeCD='TL'">E</xsl:when>
                        <xsl:otherwise>I</xsl:otherwise>
                      </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:choose>
                        <xsl:when test="/Root/Invoice[@InvoiceID=$InvoiceID]/@ItemTypeCD='I'">I</xsl:when>
                        <xsl:otherwise>E</xsl:otherwise>
                      </xsl:choose>
                    </xsl:otherwise>
                  </xsl:choose> -->
                </xsl:attribute>
                </xsl:if>
                  <xsl:for-each select="/Root/Reference[@List='ClientPaymentType']">
               	   <IE:dropDownItem style="display:none">
                        <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                        <xsl:value-of select="@Name"/>
                     </IE:dropDownItem>
                  </xsl:for-each>
              </IE:APDCustomSelect>
              <!-- <IE:APDLabel width="120" id="lblPaymentType">
                <xsl:attribute name="value">
                  <xsl:choose>
                    <xsl:when test="$InvoiceID=''">
                      <xsl:choose>
                        <xsl:when test="/Root/Payee[@ClaimAspectID=$ClaimAspectID]/@DispositionTypeCD='TL'">Expense</xsl:when>
                        <xsl:otherwise>Indemnity</xsl:otherwise>                      
                      </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:choose>
                        <xsl:when test="/Root/Invoice[@InvoiceID=$InvoiceID]/@ItemTypeCD='I'">Indemnity</xsl:when>
                        <xsl:otherwise>Expense</xsl:otherwise>
                      </xsl:choose>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
              </IE:APDLabel>  -->
            </td>
          </tr>
         <xsl:if test="count(/Root/ClaimAspect[@ServiceChannelCD='ME']) != 0">   
          <tr>
            <td>Payment Channel:</td>
            <td>
               <IE:APDCustomSelect id="selPaymentChannel" name="selPaymentChannel" displayCount="5" blankFirst="false" canDirty="true" CCDisabled="false" CCTabIndex="6" required="true" >
               	<xsl:attribute name="value"><xsl:value-of select="/Root/Invoice/@PaymentChannelCD"/></xsl:attribute>
                  <xsl:for-each select="/Root/Reference[@List='PaymentChannelCD']">
               	   <IE:dropDownItem style="display:none">
                        <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                        <xsl:value-of select="@Name"/>
                     </IE:dropDownItem>
                  </xsl:for-each>
               </IE:APDCustomSelect>
            </td>
          </tr>         
         </xsl:if>
        </table>
      </td>

      <td></td>

      <td>
        <table>
        
          <xsl:variable name="PayeeCount" select="count(/Root/Payee[@ClaimAspectID=$ClaimAspectID])"/>
          
          <colgroup>
            <col width="70px"/>
            <col width="235px"/>
          </colgroup>
          <tr>
            <td>Payee:</td>
            <td>
              <IE:APDCustomSelect id="csPayee" name="csPayee" displayCount="10" width="220" blankFirst="false" required="true" sorted="true" CCTabIndex="11" type="list" onchange="csPayee_change()">
                
                <xsl:choose>
                  <xsl:when test="$InvoiceID=''">
                    <!-- There is only one available Payee, default to that Payee -->
                    <xsl:if test="$PayeeCount=1">
                      <xsl:attribute name="selectedIndex">0</xsl:attribute>
                    </xsl:if>
                    
                    <!-- We are adding a payment,  add all available Payees for this ClaimAspect. -->
                    <xsl:for-each select="Payee[@ClaimAspectID=$ClaimAspectID]">
                      <IE:dropDownItem style="display:none">
                        <xsl:attribute name="value"><xsl:value-of select="@PayeeID"/></xsl:attribute>
                        <xsl:value-of select="@PayeeName"/>
                        <xsl:if test="@PayeeActiveAssignmentFlag = '0'"> [Inactive Assg.]</xsl:if>
                        <xsl:if test="@PayeeWarrantyActiveAssignmentFlag = '1'"> [Warranty Assg.]</xsl:if>
                        <xsl:if test="number(@PayeeID) &gt;= 510000"> [Lien Holder]</xsl:if>
                      </IE:dropDownItem>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:otherwise>
                    <!-- We are editing a payment, add only the appropriate payee. -->
                    <xsl:variable name="PayeeID"><xsl:value-of select="/Root/Invoice[@InvoiceID=$InvoiceID]/@PayeeID"/></xsl:variable>
                    <xsl:attribute name="selectedIndex">0</xsl:attribute>
                    <xsl:for-each select="/Root/Payee[@PayeeID=$PayeeID]">
                      <IE:dropDownItem style="display:none">
                        <xsl:attribute name="value"><xsl:value-of select="@PayeeID"/></xsl:attribute>
                        <xsl:value-of select="@PayeeName"/>
                      </IE:dropDownItem>
                    </xsl:for-each>
                  </xsl:otherwise>
                </xsl:choose>
                
              </IE:APDCustomSelect>                
            </td>
          </tr>
          <tr>
            <td>Payee Type:</td>
            <td>
              <xsl:variable name="PayeeTypeCD">
                <xsl:choose>
                  <xsl:when test="$InvoiceID !=''"><xsl:value-of select="/Root/Invoice[@InvoiceID=$InvoiceID]/@PayeeTypeCD"/></xsl:when>
                  <xsl:when test="$PayeeCount=1"><xsl:value-of select="/Root/Payee[@ClaimAspectID=$ClaimAspectID]/@PayeeTypeCD"/></xsl:when>
                  <xsl:otherwise></xsl:otherwise>
                </xsl:choose>
              </xsl:variable> 
                
              <IE:APDLabel width="220" id="lblPayeeType">      
                <xsl:if test="$PayeeTypeCD != ''">
                  <xsl:attribute name="PayeeTypeCD"><xsl:value-of select="$PayeeTypeCD"/></xsl:attribute> 
                  <xsl:attribute name="value"><xsl:value-of select="/Root/Reference[@List='PayeeTypeCD' and @ReferenceID=$PayeeTypeCD]/@Name"/></xsl:attribute>
                </xsl:if>     
              </IE:APDLabel>
            </td>
          </tr>
          <tr>
            <td>Address 1:</td>
            <td>
              <IE:APDLabel width="220" id="lblPayeeAddress1">
                <xsl:attribute name="value">
                  <xsl:choose>
                    <xsl:when test="$InvoiceID !=''"><xsl:value-of select="/Root/Invoice[@InvoiceID=$InvoiceID]/@PayeeAddress1"/></xsl:when>
                    <xsl:when test="$PayeeCount=1"><xsl:value-of select="/Root/Payee[@ClaimAspectID=$ClaimAspectID]/@PayeeAddress1"/></xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                </xsl:choose>
                </xsl:attribute>
              </IE:APDLabel>                
            </td>
          </tr>
          <tr>
            <td>Address 2:</td>
            <td>
              <IE:APDLabel width="220" id="lblPayeeAddress2">
                <xsl:attribute name="value">
                  <xsl:choose>
                    <xsl:when test="$InvoiceID !=''"><xsl:value-of select="/Root/Invoice[@InvoiceID=$InvoiceID]/@PayeeAddress2"/></xsl:when>
                    <xsl:when test="$PayeeCount=1"><xsl:value-of select="/Root/Payee[@ClaimAspectID=$ClaimAspectID]/@PayeeAddress2"/></xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
              </IE:APDLabel>                
            </td>
          </tr>
          <tr>
            <td>City:</td>
            <td>
              <IE:APDLabel width="220" id="lblPayeeCity">
                <xsl:attribute name="value">
                  <xsl:choose>
                    <xsl:when test="$InvoiceID !=''"><xsl:value-of select="/Root/Invoice[@InvoiceID=$InvoiceID]/@PayeeAddressCity"/></xsl:when>
                    <xsl:when test="$PayeeCount=1"><xsl:value-of select="/Root/Payee[@ClaimAspectID=$ClaimAspectID]/@PayeeCity"/></xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
              </IE:APDLabel>                
            </td>
          </tr>
          <tr>
            <td>State:</td>
            <td>                
              <IE:APDLabel width="220" id="lblPayeeState">
                <xsl:attribute name="value">
                  <xsl:choose>
                    <xsl:when test="$InvoiceID !=''"><xsl:value-of select="/Root/Invoice[@InvoiceID=$InvoiceID]/@PayeeAddressState"/></xsl:when>
                    <xsl:when test="$PayeeCount=1"><xsl:value-of select="/Root/Payee[@ClaimAspectID=$ClaimAspectID]/@PayeeState"/></xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>  
              </IE:APDLabel>
            </td>
          </tr>
          <tr>
            <td>Zip:</td>
            <td>
              <IE:APDLabel width="220" id="lblPayeeZip">
                <xsl:attribute name="value">
                  <xsl:choose>
                    <xsl:when test="$InvoiceID !=''"><xsl:value-of select="/Root/Invoice[@InvoiceID=$InvoiceID]/@PayeeAddressZip"/></xsl:when>
                    <xsl:when test="$PayeeCount=1"><xsl:value-of select="/Root/Payee[@ClaimAspectID=$ClaimAspectID]/@PayeeZip"/></xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
              </IE:APDLabel>               
            </td>
          </tr>            
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="3">
        Description:<br/>
        <xsl:variable name="CoverageProfileCD" select="/Root/ClaimAspect[@ClaimAspectID=$ClaimAspectID]/@CoverageProfileCD"/>
        <IE:APDTextArea id="txtDescription" name="txtDescription" required="true" width="530" height="60" canDirty="false"  CCTabIndex="11">
          <xsl:attribute name="maxLength"><xsl:value-of select="/Root/Metadata[@Entity='Invoice']/Column[@Name='Description']/@MaxLength"/></xsl:attribute>
          <xsl:attribute name="value">
            
            <xsl:choose>
              <xsl:when test="$InvoiceID=''"><xsl:value-of select="/Root/Reference[@List='CoverageProfile' and @ReferenceID=$CoverageProfileCD]/@Name"/></xsl:when>
              <xsl:otherwise><xsl:value-of select="/Root/Invoice[@InvoiceID=$InvoiceID]/@Description"/></xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
        </IE:APDTextArea>
      </td>
    </tr>
  </table>
  
  <span id="lblSearching" style="visibility:hidden;position:absolute;left:5px;padding:0px;padding-top:5px; font-size:smaller; font-weight:bold; color:#D97925;"></span>
    
  <span style="position:absolute;left:435px;padding:0px;padding-top:5px;">
    <IE:APDButton id="btnSave" name="btnSave" value="Save" onButtonClick="btnSaveClick()"/>
    <IE:APDButton id="btnCancel" name="btnCancel" value="Cancel" onButtonClick="btnCancelClick()"/>
  </span>
</xsl:template>

</xsl:stylesheet>
