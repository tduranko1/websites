<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:ms="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:IE="http://mycompany.com/mynamespace"
    id="UtilClaimList">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="ImageRootDir"/>
<xsl:param name="LynxID"/>
<xsl:param name="ReloadFlag"/>

<xsl:template match="/Root">

<html>

<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>

<STYLE type="text/css">
  IE\:APDCheckBox {behavior:url("/behaviors/APDCheckBox.htc")}
</STYLE>

<SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<SCRIPT labguage="JavaScript">

  var gsVehNumber = '<xsl:value-of select="Claim/@ClaimAspectNumber"/>';
  var gsClaimAspectID = '<xsl:value-of select="@ClaimAspectID"/>';
  var gsImageRootDir = '<xsl:value-of select="$ImageRootDir"/>';
  var gsLynxID = '<xsl:value-of select="$LynxID"/>';
  var gsReloadFlag = '<xsl:value-of select="$ReloadFlag"/>';
  var gsCurrSelectedRow = "";
  var gsCurrSavedRowBGColor = "";

<![CDATA[

  function PageInit()
  {
    resizeScrollTable(document.getElementById("ESTScrollTable"));
    if (gsReloadFlag == 1 && document.getElementById("txtAllReviewed").value == "1")
    {
      var sFromDate = parent.gsFromDate;
      var sToDate = parent.gsToDate;
      var sNotReviewedOnlyFlag = parent.gsNotReviewedOnlyFlag;
      parent.document.getElementById("ifrmClaimsList").src = "UtilEstimateClaimList.asp?FromDate=" + sFromDate + "&ToDate=" + sToDate + "&NotReviewedOnlyFlag=" + sNotReviewedOnlyFlag;
    }
  }

  function GridSelect(oRow, sPath)
  {
    var sClaimAspectID = gsClaimAspectID;
    var sDocumentID = oRow.cells[8].innerText;
    var sImageLocation = sPath;
    var sDocPath = gsImageRootDir + sImageLocation;

    var sWarnUser = "";

    if (sDocumentID != '')
    {
      var cellsLength;
      if (gsCurrSelectedRow != "")
      {
        cellsLength = gsCurrSelectedRow.cells.length;
        for (var i = 0; i < cellsLength; i++)
          gsCurrSelectedRow.cells[i].style.backgroundColor = gsCurrSavedRowBGColor;
      }
      
      gsCurrSelectedRow = oRow;
      gsCurrSavedRowBGColor = oRow.cells[0].style.backgroundColor;
      cellsLength = oRow.cells.length;
      for (var i = 0; i < cellsLength; i++)
        oRow.cells[i].style.backgroundColor = "#DDDDDD";

      parent.document.getElementById("ifrmEstimateDetails").src = "UtilEstimateDocDetail.asp?DocumentID=" + sDocumentID + "&ClaimAspectID=" + sClaimAspectID + "&LynxID=" + gsLynxID + "&VehicleNumber=" + gsVehNumber;
    }
    else
    {
      parent.document.getElementById("ifrmEstimateDetails").src = "blank.asp";
      sWarnUser += "Document details won't be displayed. Document ID is not available.<br>";
    }

    if (sImageLocation != '')
    {
      if (document.getElementById("ShowSeperateWindowFlag").value == 1)
        ShowInModal(sDocPath);
      else
        parent.document.getElementById("ifrmDocViewer").src = "EstimateDocView.asp?docPath=" + sDocPath;
    }
    else
    {
      parent.document.getElementById("ifrmDocViewer").src = "blank.asp";
      sWarnUser += "Document image won't be displayed. Image source is not available.<br>";
    }

    if (sWarnUser != '')
      ClientWarning(sWarnUser);
  }

  function ShowInModal(strPath)
  {
    var sHREF = "/EstimateDocView.asp?docPath=" + strPath;
    window.showModalDialog(sHREF, "", "dialogHeight:500px; dialogWidth:700px; dialogTop:180px; dialogLeft:312px; resizable:yes; status:no; help:no; center:no;");
  }

  function resizeScrollTable(oElement)
  {
    var head = oElement.firstChild;
    var headTable = head.firstChild;
    var body = oElement.lastChild;
    var bodyTable = body.firstChild;
  
    body.style.height = Math.max(0, oElement.clientHeight - head.offsetHeight);
  
    var scrollBarWidth = body.offsetWidth - body.clientWidth;
    var headCells = headTable.rows[0].cells;
    if (bodyTable.rows.length > 0)
    {
      var bodyCells = bodyTable.rows[0].cells;
        var iLength = bodyCells.length;
      for (var i = 0; i < iLength; i++)
      {
        if (bodyCells[i].style.display != "none")
        {
          headCells[i].style.width = bodyCells[i].offsetWidth;
          if (i == (headCells.length-1)) bodyCells[i].style.width = bodyCells[i].offsetWidth - (scrollBarWidth-2 );
        }
      }
    }
  }

  function getSetTotal()
  {
    var sHighTotal = "";
    var sNewHighTotal = "";
    var tblRows = document.getElementById("tblSort1").rows;
    var liRowsLength = tblRows.length;
    for (var i = 0; i < liRowsLength; i++)
    {
      var strCellTotalVal = ldTrim(tblRows[i].cells[6].innerText);
      if (strCellTotalVal != "" || strCellTotalVal != 0)
      {
        sHighTotal = Number(sHighTotal);
        sNewHighTotal = Number(strCellTotalVal);
        if (sNewHighTotal > sHighTotal)
          sHighTotal = sNewHighTotal;
      }
    }
    return sHighTotal;
  }

  function chkEstimates(docTypeText, docSeq, estType, DocumentID)
  {
    var lMessage = "";
    var tblRows = document.getElementById("tblSort1").rows;
    var liRowsLength = tblRows.length;
    for (var i = 0; i < liRowsLength; i++)
    {
      var strCellSeqVal = ldTrim(tblRows[i].cells[10].innerText);
      if (strCellSeqVal == docSeq)
      {
        if (ldTrim(tblRows[i].cells[12].innerText) == estType && ldTrim(tblRows[i].cells[11].innerText) == "0")
        {
          if (ldTrim(tblRows[i].cells[8].innerText) != DocumentID)
          {
            if (docTypeText == "Supplement")
              lMessage = "There can only be one " + docTypeText + " " + docSeq + " " + estType +".<br>All other " + docTypeText + " " + docSeq + " " + estType + " must be marked as duplicate(s).";
            else
              lMessage = "There can only be one " + docTypeText + " " + estType +".<br>All other " + docTypeText + " " + estType + " must be marked as duplicate(s).";
          }
        }
      }
    }
    if (lMessage != "")
      return lMessage;
    else
      return false;
  }

  
  function ldTrim(strText)
  { 
    while (strText.substring(0,1) == ' ') // this will get rid of leading spaces
      strText = strText.substring(1, strText.length);
    
    while (strText.substring(strText.length-1,strText.length) == ' ') // this will get rid of trailing spaces
      strText = strText.substring(0, strText.length-1);
    
    return strText;
  } 

]]>
  
</SCRIPT>

<head>
	<title>APD Estimate Cleanup Utility - Document List</title>
</head>
<body bgcolor="#FFFFCC" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" onLoad="PageInit();">

  <TABLE unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed;">
    <TR>
      <TD unselectable="on" width="10%" nowrap="">
        <strong>LYNX ID:</strong>
      </TD>
      <TD unselectable="on" width="40%">
        <xsl:value-of select="$LynxID"/>
      </TD>
      <TD unselectable="on" width="10%" nowrap=""><strong>Type:</strong></TD>
      <TD unselectable="on" width="40%" nowrap="" style="overflow-x: hidden; overflow-y: hidden;">
        <xsl:value-of select="Claim/@Type"/> -
        <xsl:value-of select="Claim/@AssignmentType"/>
      </TD>
    </TR>
    <TR>
      <TD unselectable="on" nowrap="">
        <strong>Insured:</strong>
      </TD>
      <TD unselectable="on" nowrap="" style="overflow-x: hidden; overflow-y: hidden;">
        <xsl:choose>
          <xsl:when test="Claim/@InsuredBusinessName != ''">
            <xsl:value-of select="Claim/@InsuredBusinessName"/>
          </xsl:when>
          <xsl:when test="Claim/@InsuredBusinessName = ''">
            <xsl:value-of select="Claim/@InsuredNameFirst"/>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:value-of select="Claim/@InsuredNameLast"/>
          </xsl:when>
        </xsl:choose>
      </TD>
      <TD unselectable="on" nowrap=""><strong>Owner:</strong></TD>
      <TD unselectable="on" nowrap="" style="overflow-x: hidden; overflow-y: hidden;">
        <xsl:choose>
          <xsl:when test="Claim/@OwnerBusinessName != ''">
            <xsl:value-of select="Claim/@OwnerBusinessName"/>
          </xsl:when>
          <xsl:when test="Claim/@OwnerBusinessName = ''">
            <xsl:value-of select="Claim/@OwnerNameFirst"/>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:value-of select="Claim/@OwnerNameLast"/>
          </xsl:when>
        </xsl:choose>
      </TD>
    </TR>
    <TR>
      <TD unselectable="on" nowrap="">
        <strong>Vehicle:</strong>
      </TD>
      <TD unselectable="on" colspan="2" nowrap="" style="overflow-x: hidden; overflow-y: hidden;">
        <xsl:value-of select="Claim/@VehicleYear"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of select="Claim/@Make"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of select="Claim/@Model"/>
      </TD>
      <TD unselectable="on" nowrap="" align="right">
        <IE:APDCheckBox id="ShowSeperateWindowFlag" name="ShowSeperateWindowFlag" caption="Show image in separate window" value="0" alignment="2" CCTabIndex="1" />
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
    </TR>
  </TABLE>

  <img unselectable="on" src="/images/spacer.gif" alt="" width="1" height="2" border="0"/>

  <DIV id="ESTScrollTable" style="width:100%">
    <TABLE unselectable="on" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSort1')" width="100%" border="1" cellspacing="0">
      <TBODY>
        <TR class="QueueHeader" style="height:22px">
          <TD unselectable="on" class="TableSortHeader" type="Date" sIndex="99" style="cursor:default;">Created</TD>
          <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" style="cursor:default;">Doc</TD>
          <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" style="cursor:default;">Orig/Aud</TD>
          <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" style="cursor:default;">Dup</TD>
          <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" style="cursor:default;">Source</TD>
          <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" style="cursor:default;">Image</TD>
          <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="99" style="cursor:default;">Total</TD>
          <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" style="cursor:default;">Rvwd</TD>
        </TR>
      </TBODY>
    </TABLE>
    <DIV unselectable="on" class="autoflowTable" style="height:105px;">
     <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" width="100%" cellspacing="1" border="0" cellpadding="0">
       <TBODY bgColor1="ffffff" bgColor2="fff7e5">
            <xsl:for-each select="Estimate[@DocumentID &gt; 0]"  >
              <!-- <xsl:sort select="@CreatedDate" order="descending" /> -->
              <xsl:call-template name="EstimatesList"/>
            </xsl:for-each>
       </TBODY>
     </TABLE>
    </DIV>
	</DIV>

  <input type="hidden" name="AllReviewed" id="txtAllReviewed" value="1">
    <xsl:for-each select="Estimate">
      <xsl:if test="@ReviewedFlag = '0'">
        <xsl:attribute name="value">0</xsl:attribute>
      </xsl:if>
    </xsl:for-each>
  </input>

</body>
</html>
</xsl:template>


  <xsl:template name="EstimatesList">
    <TR unselectable="on" onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' style="height:18px" title="Click to open estimate">
    <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
    <xsl:attribute name="onClick">GridSelect(this, '<xsl:value-of select="@ImageLocation"/>')</xsl:attribute>
    <xsl:attribute name="onDblClick">GridSelect(this, '<xsl:value-of select="@ImageLocation"/>')</xsl:attribute>
      <TD unselectable="on" class="GridTypeTD" width="24%" style="text-align:left; padding-left:2px;" onclick="this.focus()">
      <xsl:attribute name="nowrap"/>
        <xsl:variable name="chkCreatedDate" select="@CreatedDate"/>
        <xsl:choose>
          <xsl:when test="contains($chkCreatedDate, '1900')">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="js:UTCConvertDate(string(current()/@CreatedDate))" />
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:value-of select="js:UTCConvertTime(string(current()/@CreatedDate))"/>
          </xsl:otherwise>            
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="14%" onclick="this.focus()">
        <xsl:if test="@DocumentType = 'Supplement'">
          <xsl:attribute name="nowrap"/>
        </xsl:if>
        <xsl:choose>
          <xsl:when test="@DocumentType = 'Supplement'">
            Suppl
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:value-of select="@SequenceNumber"/>
          </xsl:when>
          <xsl:otherwise>
        		<xsl:value-of select="@DocumentType"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="14%" onclick="this.focus()">
        <xsl:choose>
    	    <xsl:when test="@EstimateTypeCD = ''">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
    	    <xsl:otherwise>
            <xsl:value-of select="@EstimateTypeCD"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="8%" onclick="this.focus()">
        <xsl:choose>
    	    <xsl:when test="@DuplicateFlag = ''">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
    	    <xsl:otherwise>
            <xsl:if test="@DuplicateFlag = '1'">Y</xsl:if>
            <xsl:if test="@DuplicateFlag = '0'">N</xsl:if>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="10%" onclick="this.focus()">
        <xsl:choose>
    	    <xsl:when test="@Source = ''">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
    	    <xsl:when test="@Source = 'Claim Point'">
            CP
          </xsl:when>
    	    <xsl:otherwise>
            <xsl:value-of select="@Source"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="8%" onclick="this.focus()">
        <xsl:choose>
    	    <xsl:when test="@ImageType = ''">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
    	    <xsl:otherwise>
            <xsl:value-of select="@ImageType"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="12%" style="text-align:right" onclick="this.focus()">
        <xsl:choose>
    	    <xsl:when test="@EstimateTotal = ''">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
    	    <xsl:otherwise>
            <xsl:value-of select="@EstimateTotal"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="10%" onclick="this.focus()">
        <xsl:choose>
    	    <xsl:when test="@ReviewedFlag = ''">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
    	    <xsl:otherwise>
            <xsl:if test="@ReviewedFlag = '1'">Yes</xsl:if>
            <xsl:if test="@ReviewedFlag = '0'">No</xsl:if>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" style="display:none">
    		<xsl:value-of select="@DocumentID"/>
      </TD>
      <TD unselectable="on" style="display:none">
    		<xsl:value-of select="@DocumentType"/>
      </TD>
      <TD unselectable="on" style="display:none">
    		<xsl:value-of select="@SequenceNumber"/>
      </TD>
      <TD unselectable="on" style="display:none">
    		<xsl:value-of select="@DuplicateFlag"/>
      </TD>
      <TD unselectable="on" style="display:none">
    		<xsl:value-of select="@EstimateTypeCD"/>
      </TD>

    </TR>
  </xsl:template>

</xsl:stylesheet>
