<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:user="http://mycompany.com/mynamespace"
  xmlns:js="urn:the-xml-files:xslt"
  id="ClaimRepDesk">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="WindowID"/>

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function myNameConcat(firstPart, secondPart, delim){
        if (delim == "") delim = ", ";
        if (firstPart == "" && secondPart == "")
            return "&nbsp;";
        else if (firstPart != "" && secondPart != "")
            return firstPart + delim + secondPart;
        else
            return firstPart + secondPart;
    }
  ]]>
</msxsl:script>

<xsl:template match="/Root/ClaimRepDesktopScreen">

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:s:\websites\apd\v1.2\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},19,uspClaimRepDeskGetDetailXML,ClaimRepDesk.xsl,19,'P'   -->

<HTML>
<HEAD>
<TITLE></TITLE>
<!-- STYLES -->
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="STYLESHEET" href="/css/CoolSelect.css" type="text/css"/>

<!-- for radio buttons -->
<STYLE type="text/css">
  A {color:black; text-decoration:none;}
  .nowrap {text-overflow:ellipsis;overflow:hidden;white-space:nowrap;width:100%}
</STYLE>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/ClaimNavigate.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,22);
		  event.returnValue=false;
		  };	
</script>


<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">

var vWindowID = "<xsl:value-of select="$WindowID"/>";

<![CDATA[

function GridSelect(oRow)
{
  var strLynxID = oRow.cells[1].innerText;
  NavOpenWindow( "claim", strLynxID, "1" );
}

//update the selected elements from the table row
function GridClick(oRow)
{
  if (!oRow) return;

  //get the table name of the row that was clicked
  var oTable = oRow.parentElement.parentElement;
  //based on the table name update the form elements to the selectd row
  if (oTable.id == "tblSort2")
  {
    txtDateofLossP.value = oRow.cells[4].innerText;
    txtLynxIDP.value = oRow.cells[1].innerText;
    txtPHNameP.value = oRow.cells[3].innerText;		
    txtTimeOpenP.value = oRow.cells[11].innerText + " days " + oRow.cells[12].innerText + " hrs. " + oRow.cells[13].innerText + " mins.";
    txtClaimNumberP.value = oRow.cells[7].innerText;
    txtStateP.value = oRow.cells[8].innerText;
    txtInsuranceCoP.value = oRow.cells[6].innerText;
    //txtCoverageP.value = oRow.cells[2].innerText;
    spVehNumP.innerText = oRow.cells[9].innerText;
    spPropNumP.innerText = oRow.cells[10].innerText;

    var numVeh = parseInt(oRow.cells[9].innerText);
    var oSelect = document.getElementById("selPCVeh");
    oSelect.removeall();
    //alert(oSelect.options.length);

    var oOption = new Object();
    
    for(var indx=0;indx < numVeh; indx++)
    {
      var str = oRow.cells[18+(indx*4)].innerText;
      var strStatus = oRow.cells[19+(indx*4)].innerText;
      //alert(strStatus)
      if (str.substr(0,1) == ",") str = str.substr(2);
      if (str.length > 28) str = str.substring(0,28); 
      if (strStatus.indexOf("Closed") != -1)
        oOption.text = "<img src='/images/closedExposure.gif' title='Closed Vehicle'/><img src='/images/spacer.gif' style='height:1px;width:5px'/>" + str;
      else
      {
        if (str == "")
          oOption.text = "<img src='/images/spacer.gif' style='height:1px;width:15px'/> Veh " + oRow.cells[17+(indx*4)].innerText;
        else
          oOption.text = "<img src='/images/spacer.gif' style='height:1px;width:15px'/>" + str;
      }
      oOption.value = String(oRow.cells[17+(indx*4)].innerText +"||"+ oRow.cells[20+(indx*4)].innerText);
      oSelect.add(oOption);
    }

    if (oSelect.firstChild.nextSibling && oSelect.firstChild.nextSibling.className=="dropDown")
        oSelect.firstChild.nextSibling.style.height = Math.min(75, (numVeh * 15) + 1);

    if (numVeh > 0)
    {
     	oSelect.remove(0);
      oSelect.options[0].selected = true;
    }

    var numProp = parseInt(oRow.cells[10].innerText);
    oSelect = document.getElementById("selPCProp");
    oSelect.removeall();

      for(var indx=0;indx < numProp; indx++)
      {
        var str = oRow.cells[18+(indx*4)+(numVeh*4)].innerText;
        var strStatus = oRow.cells[19+(indx*4)+(numVeh*4)].innerText;
        if (str.substr(0,1) == ",") str = str.substr(2);
        if (str.length > 28) str = str.substring(0,28);       
        if (strStatus.indexOf("Closed") != -1)
          oOption.text = "<img src='/images/closedExposure.gif' title='Closed Property'/><img src='/images/spacer.gif' style='height:1px;width:5px'/>" + str;
        else
        {
          if (str == "")
            oOption.text = "<img src='/images/spacer.gif' style='height:1px;width:15px'/> Prop " + oRow.cells[17+(indx*4)+(numVeh*4)].innerText;
          else
            oOption.text = "<img src='/images/spacer.gif' style='height:1px;width:15px'/>" + str;
        }
        //oOption.text = str;
        oOption.value=String(oRow.cells[17+(indx*4)+(numVeh*4)].innerText +"||"+ oRow.cells[20+(indx*4)+(numVeh*4)].innerText);
        oSelect.add(oOption);
      }
  
      if (oSelect.firstChild.nextSibling && oSelect.firstChild.nextSibling.className=="dropDown")
          oSelect.firstChild.nextSibling.style.height = Math.min(75, (numProp * 15) + 1);
    
    if (numProp > 0)
    {
      oSelect.remove(0);
      oSelect.options[0].selected = true;
      return;
    }
  }

  if (oTable.id == "tblSort3")
  {
    txtLynxIDPV.value = oRow.cells[1].innerText;
    txtDateofLossPV.value = oRow.cells[6].innerText;
    txtPHNamePV.value = oRow.cells[3].innerText;				
    txtTimeOpenPV.value = oRow.cells[13].innerText + " days " + oRow.cells[14].innerText + " hrs. " + oRow.cells[15].innerText + " mins.";
    txtVehiclePV.value = oRow.cells[5].innerText;
    txtStatePV.value = oRow.cells[16].innerText;
    txtCoveragePV.value = oRow.cells[17].innerText;
    txtVehicleStatus.value = oRow.cells[20].innerText;
    txtParties.value = oRow.cells[4].innerText;
    txtTimeStatus.value = oRow.cells[18].innerText;
    var strStatus = oRow.cells[20].innerText;
    if (strStatus.indexOf("Closed") != -1)
        imgVehClosed1.style.display = "inline"
    else
        imgVehClosed1.style.display = "none";
    return;
  }

  if (oTable.id == "tblSort4")
  {
    txtLynxIDPP.value = oRow.cells[1].innerText;
    txtDateofLossPP.value = oRow.cells[5].innerText;
    txtPHNamePP.value = oRow.cells[3].innerText;				
    txtTimeOpenPP.value = oRow.cells[8].innerText + " days " + oRow.cells[9].innerText + " hrs. " + oRow.cells[10].innerText + " mins.";
    txtPropertyNamePP.value = oRow.cells[13].innerText;
    txtStatePP.value = oRow.cells[11].innerText;
    txtCoveragePP.value = oRow.cells[12].innerText;
    txtPropertyStatusPP.value = (oRow.cells[16].innerText == "" ? "Open": oRow.cells[16].innerText);
    txtTimeStatusPP.value = oRow.cells[14].innerText;
    var strStatus = txtPropertyStatusPP.value;
    if (strStatus.indexOf("Closed") != -1)
        imgPropClosed1.style.display = "inline"
    else
        imgPropClosed1.style.display = "none";
    //this was not in the SP
    //txtPropertyType = oRow.cells[14].innerText;
    return;
  }
}

function onSelectChange(selobj)
{
  if ((selobj.options[selobj.selectedIndex].innerText == "----") || (selobj.options[selobj.selectedIndex].innerText == "")) return;

  var strNum = selobj.options[selobj.selectedIndex].value;
  var aryNum = strNum.split("||");

	var sURL = "";
  var lynxid = "";
  var sEntity = "vehicle";
  
  if (selobj.id == "selNewVeh")  lynxid = txtLynxID.value;
  if (selobj.id == "selPCVeh") lynxid = txtLynxIDP.value;
  
  if (lynxid == "")
  {
    if (selobj.id == "selNewProp") lynxid = txtLynxID.value;
    if (selobj.id == "selPCProp")  lynxid = txtLynxIDP.value;

    sEntity = "property";
  }

  NavOpenWindow( sEntity, lynxid, aryNum[0] );
}

function resizeScrollTable(oElement)  
{
	var head = oElement.firstChild;
	var headTable = head.firstChild;
	var body = oElement.lastChild;
	var bodyTable = body.firstChild;
	
	body.style.height = Math.max(0, oElement.clientHeight - head.offsetHeight);
	
	var scrollBarWidth = body.offsetWidth - body.clientWidth;
	
	// go through each cell in the head and resize
	var headCells = headTable.rows[0].cells;
	if (bodyTable.rows.length > 0)
	{
		var bodyCells = bodyTable.rows[0].cells;
		var iLength = bodyCells.length;
		for (var i = 0; i < iLength; i++)
		{
			if (bodyCells[i].style.display != "none")
			{
				if (i == 0)
          headCells[i].style.width = bodyCells[i].offsetWidth + 2;
				else if (i == (headCells.length-1)) 
          headCells[i].style.width = bodyCells[i].offsetWidth + scrollBarWidth;
        else
          headCells[i].style.width = bodyCells[i].offsetWidth;
      }
		}
	}
}

//init the table selections, must be last
function initPage()
{
//  try
//  {
    tabInit(false);
    popupInit();
    initSelectBoxes();

    var oTbl = document.getElementById("tblSort2");
    GridClick(oTbl.rows[0]);
    oTbl = document.getElementById("tblSort3");
    GridClick(oTbl.rows[0]);
    oTbl = document.getElementById("tblSort4");
    GridClick(oTbl.rows[0]);

    tabsIntabs[0].tabs.onchange=onTabChangeBubble;
    tabsIntabs[0].tabs.onBeforechange=tabBeforeChange;

    if (top.PendingClaimsActiveTab != "" && top.PendingClaimsActiveTab != "content11") { //dont have to select the first tab. it is selected by default.
      var myTabArray = tabsIntabs[0].tabArray;
      for (var i = 0; i < myTabArray.length; i++) {
        if (myTabArray[i].content.id == top.PendingClaimsActiveTab) {
          myTabArray[i].depressTab(); //select the tab that was previously selected
          break;
        }
      }
    }
    top.closeClaim();
    top.setSB(100,top.sb)
  
    oTbl = document.getElementById("tblInvalidClaims");
    if (oTbl.rows.length > 1)
    {
      var strMsg = "The following Claims are in an Invalid State!<br><br>"
      var idxLength = oTbl.rows.length;
      for(var idx=1; idx < idxLength; idx++)
      {
        strMsg += "LynxID = " + oTbl.rows[idx].cells[0].innerText + " Reason = " + oTbl.rows[idx].cells[1].innerText + "<br>";
      }
     
      ClientWarning(strMsg);
    }
    window.setTimeout("resizeScrollTable(document.getElementById('VPScrollTable'))", 100);
    window.setTimeout("resizeScrollTable(document.getElementById('CPScrollTable'))", 100);
    window.setTimeout("resizeScrollTable(document.getElementById('PPScrollTable'))", 100);
//  } catch(e) { alert( e.message ); }
}

function tabBeforeChange(obj, tab){
  //store the current tab
  top.PendingClaimsActiveTab = tab.content.id;
}

function navigateTo(strEntity){
    var srcEle = event.srcElement;
    var curRow = srcEle.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
    switch(strEntity) {
        case "claim":
            GridSelect(curRow);
            break;
        case "vehicle":
            var numVeh = parseInt(curRow.cells[9].innerText);
            if (numVeh > 0){
                var firstVehNum = curRow.cells[17].innerText;
                var strLynxID = curRow.cells[1].innerText;
                if (strLynxID != "" && firstVehNum != "")
                    NavOpenWindow( strEntity, strLynxID, firstVehNum );
            }
            break;
        case "property":
            var numVeh = parseInt(curRow.cells[9].innerText);
            var numProp = curRow.cells[10].innerText;
            if (numProp > 0){
                var firstPropNum = curRow.cells[17+(numVeh*4)].innerText;
                var strLynxID = curRow.cells[1].innerText;
                if (strLynxID != "" && firstPropNum != "")
                    NavOpenWindow( strEntity, strLynxID, firstPropNum );
            }
            break;
    }
    window.event.cancelBubble = true;
}

function navigate2(strEntity, oRow){
    if (oRow != undefined)
      var curRow = oRow;
    else
    {
      var srcEle = event.srcElement;
      var curRow = srcEle.parentElement.parentElement;
    }
    switch(strEntity){
        case "vehicle":
            var VehNumber = curRow.cells[19].innerText;
            var strLynxID = curRow.cells[1].innerText;
            var strClaimAspectID = curRow.cells[21].innerText;
            if (strLynxID != "" && VehNumber != "" && strClaimAspectID != "")
                NavOpenWindow( strEntity, strLynxID, VehNumber );
            break;
        case "property":
            var propNumber = curRow.cells[15].innerText;
            var strLynxID = curRow.cells[1].innerText;
            var strClaimAspectID = curRow.cells[17].innerText;
            if (strLynxID != "" && propNumber != "" && strClaimAspectID != "")
                NavOpenWindow( strEntity, strLynxID, propNumber );
            break;
    }
    window.event.cancelBubble = true;
}
// Variables to set for tooltips:

messages= new Array()
// Write your descriptions in here.
messages[0]="Answer Wait Time";
messages[1]="Single-Click Column to sort";

if (document.attachEvent)
  document.attachEvent("onclick", top.hideAllMenuScriptlets);
else
  document.onclick = top.hideAllMenuScriptlets;

]]>
</SCRIPT>
</HEAD>

<BODY class="bodyAPDSub" unselectable="on" STYLE="background:transparent; border:0px; padding: 0px;" onLoad="initPage();" tabIndex="-1">

  <DIV unselectable="on" id="tabs1" class="apdtabs">
    <SPAN unselectable="on" id="tab11" class="tab1">Pending Claim View</SPAN>
    <SPAN unselectable="on" id="tab12" class="tab1">Pending Vehicle View</SPAN>
    <SPAN unselectable="on" id="tab13" class="tab1">Pending Property View</SPAN>
  </DIV>

  <!-- begin content 1 -->
  <DIV unselectable="on" class="content1" id="content11" style="left:0px; width:727px; height:464px;">
    <DIV id="CPScrollTable" style="position:relative; width:100%; padding:3px;">
      <SPAN>
        <TABLE unselectable="on" class="ClaimMiscInputs" width="100%" onClick="sortColumn(event, 'tblSort2')" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
          <TR unselectable="on" class="QueueHeader" style="height:28px">
          <TD unselectable="on" class="TableSortHeader" type="TimeD"> Time in System </TD>
          <!-- <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Coverage </TD> -->
          <TD unselectable="on" class="TableSortHeader" type="Number"> LYNX ID </TD>
          <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Status </TD>
          <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Policy Holder </TD>
          <TD unselectable="on" class="TableSortHeader" type="Date"> Date of Loss </TD>
          <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Assigned To </TD>
          <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Insurance Co. </TD>
          </TR>
        </TABLE>
      </SPAN>
      <DIV unselectable="on" class="autoflowTable" style="width:100%; height:288px;">
        <TABLE unselectable="on" id="tblSort2" class="GridTypeTable" width="100%" cellspacing="1" border="0" cellpadding="0">
          <TBODY bgColor1="ffffff" bgColor2="fff7e5">
            <xsl:for-each select="Claim[@NewlyAssigned = '0']"  >
            <xsl:sort data-type="number" select="@ClaimOpenTimeDays" order="descending" />
              <xsl:sort data-type="number" select="@LynxID" order="ascending" />
            <xsl:sort data-type="number" select="@ClaimOpenTimeHours" order="descending" />
              <xsl:sort data-type="number" select="@LynxID" order="ascending" />
            <xsl:sort data-type="number" select="@ClaimOpenTimeMinutes" order="descending" />
              <xsl:sort data-type="number" select="@LynxID" order="ascending" />
              <xsl:call-template name="Claims">
              </xsl:call-template>
            </xsl:for-each>
          </TBODY>
       </TABLE>
      </DIV>
    </DIV>      
    <DIV unselectable="on" id="Layer1" style="position:absolute; left:6px; top:324px; width:100%;">
      <DIV unselectable="on" id="CallerInfoTab" class="SmallGroupingTab" style="position:absolute; width:160px; z-index:2; top:7px; left:2px">
        Pending Claim Preview </DIV>
      <DIV unselectable="on" id="InsuredInfo" class="SmallGrouping" style="position:absolute; left:0px; top:23px; z-index:1; width:712px; height:109px">
        <TABLE unselectable="on" class="paddedTable" border="0" cellspacing="0" cellpadding="0" width="100%">
          <TR unselectable="on">
            <TD unselectable="on" ><xsl:attribute name="nowrap"/>Vehicles in Claim (<span id="spVehNumP">0</span>): </TD>
            <TD unselectable="on" >
               <script>addSelectCustomValues('selPCVeh',2,'onSelectChange',1,1,0,1,'CRUD',0,'----',220, 75);</script>
            </TD>
            <TD unselectable="on" ><xsl:attribute name="nowrap"/>Properties in Claim (<span id="spPropNumP">0</span>): </TD>
            <TD unselectable="on" >
              <script>addSelectCustomValues('selPCProp',2,'onSelectChange',1,1,0,1,'CRUD',0,'----',220, 75);</script>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on" width="116"> LYNX ID </TD>
            <TD  unselectable="on" width="251">
              <INPUT type="text" id="txtLynxIDP" class="InputReadonlyField" size="15"  >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
            <TD unselectable="on" width="97"> Date of Loss: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtDateofLossP" class="InputReadonlyField" size="18" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on" width="116"> Policy Holder: </TD>
            <TD unselectable="on" width="251">
              <INPUT type="text" id="txtPHNameP" class="InputReadonlyField" size="30" >
                <xsl:attribute name="readonly"/>
              </INPUT>
								
            </TD>
            <TD unselectable="on" width="97"> Time in System: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtTimeOpenP" class="InputReadonlyField" size="25" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on" width="116"> Claim Number: </TD>
            <TD unselectable="on" width="251">
              <INPUT type="text" id="txtClaimNumberP" class="InputReadonlyField" size="20" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
            <TD unselectable="on"  width="97"> State: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtStateP" class="InputReadonlyField" size="4" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on" width="116"> Insurance Co.: </TD>
            <TD unselectable="on"  width="251">
              <INPUT type="text" id="txtInsuranceCoP" class="InputReadonlyField" size="30" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
            <TD unselectable="on" width="97"> Coverage: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtCoverageP" class="InputReadonlyField" size="30" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
        </TABLE>
      </DIV>
    </DIV>
  </DIV>
  <DIV unselectable="on" class="content1" id="content12" style="left:3px; width:727px; height:464px;">
    <DIV id="VPScrollTable" style="position:relative; width:100%; padding:3px;">
      <span >
        <TABLE unselectable="on" id="VPTable" class="ClaimMiscInputs" width="100%" onClick="sortColumn(event, 'tblSort3');"  cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
          <TR unselectable="on" class="QueueHeader" style="height:28px">
            <TD unselectable="on" class="TableSortHeader" type="TimeD"> Time in System </TD>
            <TD unselectable="on" class="TableSortHeader" type="Number"> LYNX ID </TD>
            <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Status </TD>
            <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Owner </TD>
            <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Party </TD>
            <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> YMM </TD>
            <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Assigned To </TD>
          </TR>
        </TABLE>
      </span>    
      <DIV unselectable="on" class="autoflowTable" style="width:100%; height:288px;">
       <TABLE unselectable="on" id="tblSort3" class="GridTypeTable" width="100%" cellspacing="1" border="0" cellpadding="0">
        <TBODY bgColor1="ffffff" bgColor2="fff7e5">
          <!-- <xsl:for-each select="Claim[@NewlyAssigned = '0']"  >
            <xsl:sort data-type="number" select="@LynxID" order="ascending" />
              <xsl:sort data-type="number" select="Vehicle/@StatusOpenTimeDays" order="descending" />
              <xsl:sort data-type="number" select="Vehicle/@StatusOpenTimeHours" order="descending" />
              <xsl:sort data-type="number" select="Vehicle/@StatusOpenTimeMinutes" order="descending" /> -->
            <xsl:call-template name="Vehicles"/>
          <!-- </xsl:for-each> -->
        </TBODY>
       </TABLE>
      </DIV>
    </DIV>    
    <DIV unselectable="on" id="Layer1" style="position:absolute; left:6px; top:324px; width:100%">
      <DIV unselectable="on" id="CallerInfoTab" class="SmallGroupingTab" style="position:absolute; width:160px; z-index:2; top:7px; left:2px">
        Pending Vehicle Preview </DIV>
      <DIV unselectable="on" id="InsuredInfo" class="SmallGrouping" style="position:absolute; left:0px; top:23px; z-index:1; width:712px; height:109px">
        <TABLE unselectable="on" class="paddedTable" border="0" cellspacing="0" cellpadding="0" width="100%">
          <TR unselectable="on">
            <TD unselectable="on" width="116"> LYNX ID </TD>
            <TD unselectable="on"  width="251">
              <INPUT type="text" id="txtLynxIDPV" class="InputReadonlyField" size="15"  >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
            <TD unselectable="on" width="97"> Date of Loss: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtDateofLossPV" class="InputReadonlyField" size="18" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on" width="116"> Policy Holder: </TD>
            <TD unselectable="on" width="251">
              <INPUT type="text" id="txtPHNamePV" class="InputReadonlyField" size="30" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
            <TD unselectable="on" width="97"> Time in System: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtTimeOpenPV" class="InputReadonlyField" size="25" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on" width="116"> Vehicle: </TD>
            <TD unselectable="on" width="251">
              <INPUT type="text" id="txtVehiclePV" class="InputReadonlyField"  size="30" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
            <TD unselectable="on"  width="97"> State: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtStatePV" class="InputReadonlyField" size="4" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on" width="116"> Vehicel Status: </TD>
            <TD  unselectable="on" width="251">
              <INPUT type="text" id="txtVehicleStatus" class="InputReadonlyField" size="30" >
                <xsl:attribute name="readonly"/>
              </INPUT>
              <IMG src="/images/spacer.gif" style="height:1px;width:5px"/><IMG id="imgVehClosed1" src="/images/closedExposureBig.gif" style="display:none"/>
            </TD>
            <TD unselectable="on" width="97"> Coverage: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtCoveragePV" class="InputReadonlyField" size="30" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on">Parties:</TD>
            <TD unselectable="on">
              <INPUT type="text" id="txtParties" class="InputReadonlyField" size="10" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
            <TD unselectable="on">Time in System:</TD>
            <TD unselectable="on">
              <INPUT type="text" name="txtTimeStatus" class="InputReadonlyField" size="25">
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
        </TABLE>
      </DIV>
    </DIV>
  </DIV>
  <DIV unselectable="on" class="content1" id="content13" style="left:3px; width:727px; height:464px;">
    <DIV id="PPScrollTable" style="position:relative; width:100%; padding:3px;">
      <span >
        <TABLE unselectable="on" class="ClaimMiscInputs" width="100%" onClick="sortColumn(event, 'tblSort4')" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
          <TR unselectable="on" class="QueueHeader" style="height:28px">
            <TD unselectable="on" class="TableSortHeader" type="TimeD"> Time in System </TD>
            <TD unselectable="on" class="TableSortHeader" type="Number"> LYNX ID </TD>
            <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Status </TD>
            <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Owner </TD>
            <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString"> Assigned To </TD>
          </TR>
        </TABLE>
      </span>
      <DIV unselectable="on" class="autoflowTable" style="width:100%; height:288px;">
       <TABLE unselectable="on" id="tblSort4" class="GridTypeTable" width="100%" cellspacing="1" border="0" cellpadding="0">
        <TBODY bgColor1="ffffff" bgColor2="fff7e5">
          <!-- <xsl:for-each select="Claim[@NewlyAssigned = '0']"  >
              <xsl:sort data-type="number" select="Property/@StatusOpenTimeDays" order="descending" />
              <xsl:sort data-type="number" select="Property/@StatusOpenTimeHours" order="descending" />
              <xsl:sort data-type="number" select="Property/@StatusOpenTimeMinutes" order="descending" />
              <xsl:sort data-type="number" select="@LynxID" order="ascending" /> -->
            <xsl:call-template name="Properties"/>
          <!-- </xsl:for-each> -->
        </TBODY>
       </TABLE>
      </DIV>
    </DIV>
    <DIV unselectable="on" id="Layer1" style="position:absolute; left:6px; top:324px; width:100%">
      <DIV unselectable="on" id="CallerInfoTab" class="SmallGroupingTab" style="position:absolute; width:180px; z-index:2; top:7px; left:2px">
        Pending Property Preview </DIV>
      <DIV unselectable="on" id="InsuredInfo" class="SmallGrouping" style="position:absolute; left:0px; top:23px; z-index:1; width:712px; height:109px">
        <TABLE unselectable="on" class="paddedTable" border="0" cellspacing="0" cellpadding="0" width="100%">
          <TR unselectable="on">
            <TD unselectable="on" width="116"> LYNX ID </TD>
            <TD unselectable="on"  width="251">
              <INPUT type="text" id="txtLynxIDPP" class="InputReadonlyField" size="15" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
            <TD unselectable="on" width="97"> Date of Loss: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtDateofLossPP" class="InputReadonlyField" size="18" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on" width="116"> Policy Holder: </TD>
            <TD unselectable="on" width="251">
              <INPUT type="text" id="txtPHNamePP" class="InputReadonlyField" size="30" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
            <TD unselectable="on" width="97"> Time in System: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtTimeOpenPP" class="InputReadonlyField" size="25">
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on" width="116"> Property Name: </TD>
            <TD unselectable="on" width="251">
              <INPUT type="text" id="txtPropertyNamePP" class="InputReadonlyField" size="20" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
            <TD unselectable="on"  width="97"> State: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtStatePP" class="InputReadonlyField" size="4" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on" width="116"> Property Type: </TD>
            <TD  unselectable="on" width="251">
              <INPUT type="text" id="txtPropertyType" class="InputReadonlyField" size="30" value="">
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
            <TD unselectable="on" width="97"> Coverage: </TD>
            <TD unselectable="on" width="148">
              <INPUT type="text" id="txtCoveragePP" class="InputReadonlyField" size="30" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
          <TR unselectable="on">
            <TD unselectable="on">Property Status:</TD>
            <TD unselectable="on">
              <INPUT type="text" id="txtPropertyStatusPP" class="InputReadonlyField" size="30" >
                <xsl:attribute name="readonly"/>
              </INPUT>
              <IMG src="/images/spacer.gif" style="height:1px;width:5px"/><IMG id="imgPropClosed1" src="/images/closedExposureBig.gif" style="display:none"/>
            </TD>
            <TD unselectable="on">Time in System:</TD>
            <TD unselectable="on">
              <INPUT type="text" id="txtTimeStatusPP" class="InputReadonlyField" size="25" >
                <xsl:attribute name="readonly"/>
              </INPUT>
            </TD>
          </TR>
        </TABLE>
      </DIV>
    </DIV>
</DIV>

<!-- Tooltip Div -->
<DIV unselectable="on" id="divTooltip"> </DIV>
<!--Invalid Claim Information -->
<DIV style="visibility:hidden;display:none">
  <TABLE id="tblInvalidClaims" style="visibility:hidden;display:none">
    <TR><TD>Blank</TD>
    </TR>
    <xsl:for-each select="InvalidClaim">
      <TR>
        <TD><xsl:value-of select="@LynxID"/></TD>
        <TD><xsl:value-of select="@Reason"/></TD>
      </TR>
    </xsl:for-each>
  </TABLE>
</DIV>

</BODY>
</HTML>
</xsl:template>

  <!-- Gets the New or Pending Claims -->
  <xsl:template name="Claims">
    <TR unselectable="on" onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' RowType='Claim' onClick='GridClick(this)' onDblClick='GridSelect(this)' style="21px">
    <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
      <TD unselectable="on" width="65" class="GridTypeTD">
        <xsl:choose>
          <xsl:when test="@ClaimOpenTimeDays!=0"><xsl:value-of select="@ClaimOpenTimeDays"/><xsl:text> days</xsl:text></xsl:when>
          <xsl:when test="@ClaimOpenTimeHours!=0"><xsl:value-of select="@ClaimOpenTimeHours"/><xsl:text> hrs</xsl:text></xsl:when>
          <xsl:otherwise><xsl:value-of select="@ClaimOpenTimeMinutes"/><xsl:text> mins</xsl:text></xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" width="55" class="GridTypeTD">
        <xsl:value-of select="@LynxID"/>
      </TD>
      <TD unselectable="on" width="90" class="GridTypeTD" style="text-align:left">
        <!-- <xsl:value-of select="@CoverageStatus"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp; -->
        <table cellspacing="0" cellpadding="0" border="0">
        <tr>
        <td width="26px" style="text-align:center">
            <xsl:choose>
                <xsl:when test="contains(@ClaimStatus, 'Closed')">
                    <img src="/images/icon_claim_closed.gif" title="Claim Closed" onclick="event.returnValue=false;" ondblclick="navigateTo('claim');" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                </xsl:when>
                <xsl:otherwise>
                    <img src="/images/icon_claim_open.gif" title="Claim Open" onclick="event.returnValue=false;" ondblclick="navigateTo('claim');" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                </xsl:otherwise>
            </xsl:choose>
        </td>
        <td width="30px" style="text-align:center">
        <xsl:variable name="VehicleCount"><xsl:value-of select="count(Vehicle)"/></xsl:variable>
        <!-- <xsl:variable name="VehiclesOpen"><xsl:value-of select="count(Vehicle[@Status='Open'])"/></xsl:variable> -->
        <xsl:variable name="VehiclesOpen"><xsl:value-of select="count(Vehicle[contains(@Status,'Open') or @Status=''])"/></xsl:variable>        
        <xsl:if test="$VehicleCount > 0">
            <xsl:choose>
                <xsl:when test="$VehicleCount &gt; 1">
                    <xsl:choose>
                        <xsl:when test="$VehiclesOpen = $VehicleCount">
                            <img src="/images/icon_vehicle_multi_open.gif" title="All Vehicles Open" onclick="event.returnValue=false;" ondblclick="navigateTo('vehicle');" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                        </xsl:when>
                        <xsl:when test="$VehiclesOpen = 0">
                            <img src="/images/icon_vehicle_multi_closed.gif" title="All Vehicles Closed" onclick="event.returnValue=false;" ondblclick="navigateTo('vehicle');" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                        </xsl:when>
                        <xsl:when test="$VehicleCount &gt; 1 and $VehiclesOpen &lt; $VehicleCount">
                            <img src="/images/icon_vehicle_multi_open_closed.gif" title="Some Vehicles Closed" onclick="event.returnValue=false;" ondblclick="navigateTo('vehicle');" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                        </xsl:when>
                    </xsl:choose>
                    <!-- <img src="/images/Multi.gif" title="Multiple Vehicles"/> -->
                </xsl:when>
                <xsl:when test="$VehicleCount = 1">
                    <xsl:choose>
                        <xsl:when test="$VehiclesOpen = $VehicleCount">
                            <img src="/images/icon_vehicle_open.gif" title="Vehicle Open" onclick="event.returnValue=false;" ondblclick="navigateTo('vehicle');" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                        </xsl:when>
                        <xsl:when test="$VehiclesOpen = 0">
                            <img src="/images/icon_vehicle_closed.gif" title="Vehicle Closed" onclick="event.returnValue=false;" ondblclick="navigateTo('vehicle');" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                        </xsl:when>
                    </xsl:choose>
                    <!-- <img src="/images/Multi.gif" title="Multiple Vehicles"/> -->
                </xsl:when>
            </xsl:choose>
        </xsl:if>
        </td>
        <td width="30px" style="text-align:center">
        <xsl:variable name="PropertyCount"><xsl:value-of select="count(Property)"/></xsl:variable>
        <xsl:variable name="PropertiesOpen"><xsl:value-of select="count(Property[contains(@Status,'Open') or @Status=''])"/></xsl:variable>
        <xsl:if test="$PropertyCount > 0">
            <xsl:choose>
                <xsl:when test="$PropertyCount &gt; 1">
                    <xsl:choose>
                        <xsl:when test="$PropertiesOpen = $PropertyCount">
                            <img src="/images/icon_property_multi_open.gif" title="All Properties Open" onclick="event.returnValue=false;" ondblclick="navigateTo('property')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                        </xsl:when>
                        <xsl:when test="$PropertiesOpen = 0">
                            <img src="/images/icon_property_multi_closed.gif" title="All Properties Closed" onclick="event.returnValue=false;" ondblclick="navigateTo('property')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                        </xsl:when>
                        <xsl:when test="$PropertyCount &gt; 1 and $PropertiesOpen &lt; $PropertyCount">
                            <img src="/images/icon_property_multi_open_closed.gif" title="Some Properties Closed" onclick="event.returnValue=false;" ondblclick="navigateTo('property')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$PropertyCount = 1">
                    <xsl:choose>
                        <xsl:when test="$PropertiesOpen = $PropertyCount">
                            <img src="/images/icon_property_open.gif" title="Property Open" onclick="event.returnValue=false;" ondblclick="navigateTo('property')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                        </xsl:when>
                        <xsl:when test="$PropertiesOpen = 0">
                            <img src="/images/icon_property_closed.gif" title="Property Closed" onclick="event.returnValue=false;" ondblclick="navigateTo('property')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)"/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
        </td>
        </tr>
        </table>
      </TD>
      <TD unselectable="on" width="150" class="GridTypeTD" style="text-align:left">
        <span class="nowrap">
        <xsl:choose>
          <xsl:when test="string-length(@PolicyHolderBusinessName) > 0" >
            <xsl:value-of select="substring(@PolicyHolderBusinessName,1,25)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of disable-output-escaping="yes" select="user:myNameConcat(substring(@PolicyHolderLastName,0,12), substring(@PolicyHolderFirstName,0,12), ', ')"/>
            <!-- <xsl:value-of select="substring(@PolicyHolderLastName,0,12)"/>, <xsl:value-of select="substring(@PolicyHolderFirstName,0,12)"/> -->          
          </xsl:otherwise>
        </xsl:choose>
        </span>
      </TD>
      <TD unselectable="on" width='80' class="GridTypeTD">
        <xsl:value-of select="user:UTCConvertDateByNodeType(.,'LossDate','A')"/>
      </TD>
      <TD unselectable="on"  class="GridTypeTD">
        <span class="nowrap"><xsl:value-of select="@ClaimOwnerName"/></span>
      </TD>
      <TD unselectable="on"  class="GridTypeTD">
        <span class="nowrap"><xsl:value-of select="@InsuranceCompanyName"/></span>
      </TD>
      <TD style="display:none"><xsl:value-of select="@ClientClaimNumber"/></TD>
      <TD style="display:none"><xsl:value-of select="@LossState"/></TD>
      <TD style="display:none"><xsl:value-of select="count(Vehicle)"/></TD>
      <TD style="display:none"><xsl:value-of select="count(Property)"/></TD>
      <TD style="display:none"><xsl:value-of select="@ClaimOpenTimeDays"/></TD>
      <TD style="display:none"><xsl:value-of select="@ClaimOpenTimeHours"/></TD>
      <TD style="display:none"><xsl:value-of select="@ClaimOpenTimeMinutes"/></TD>
      <TD style="display:none"></TD><!--need to remove and update indexs-->
      <TD style="display:none"><xsl:value-of select="@PolicyHolderFirstName"/></TD>
      <TD style="display:none"><xsl:value-of select="@PolicyHolderLastName"/></TD>
      <xsl:for-each select="Vehicle">
        <!-- <xsl:sort data-type="text" select="@OwnerBusinessName" order="ascending" />
          <xsl:sort data-type="text" select="@OwnerLastName" order="ascending" />
        <xsl:sort data-type="text" select="@OwnerFirstName" order="ascending" /> -->
        <TD style="display:none"><xsl:value-of select="@VehicleNumber"/></TD>
        <TD style="display:none">
            <xsl:choose>
                <xsl:when test= "string-length(@OwnerBusinessName) &gt; 0">
                    <xsl:value-of select="@OwnerBusinessName"/>
                </xsl:when>
                <xsl:when test= "string-length(@OwnerLastName) &gt; 0 and string-length(@OwnerFirstName) &gt; 0">
                  <xsl:value-of select="user:myNameConcat(string(@OwnerLastName), string(@OwnerFirstName), ', ')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat(@OwnerLastName, @OwnerFirstName)"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="string(@VehicleYear) = '0'">
                    <xsl:variable name="MM"><xsl:value-of select="user:myNameConcat(normalize-space(string(@Make)), normalize-space(string(@Model)), ' ')"/></xsl:variable>
                    <xsl:if test="$MM != '&amp;nbsp;'">
                        (<xsl:value-of select="$MM"/><xsl:value-of select="string-length(normalize-space($MM))"/>)
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                    (<xsl:value-of select="user:myNameConcat(string(user:myNameConcat(string(@VehicleYear), string(@Make), ' ')), string(@Model), ' ')"/>)
                </xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD style="display:none"><xsl:value-of select="@Status"/></TD>
        <TD style="display:none"><xsl:value-of select="@ClaimAspectID"/></TD>
      </xsl:for-each>
      <xsl:for-each select="Property">
        <!-- <xsl:sort data-type="text" select="@OwnerBusinessName" order="ascending" />
          <xsl:sort data-type="text" select="@OwnerLastName" order="ascending" />
        <xsl:sort data-type="text" select="@OwnerFirstName" order="ascending" />
        <xsl:sort data-type="text" select="@PropertyName" order="ascending" /> -->
        <TD style="display:none"><xsl:value-of select="@PropertyNumber"/></TD>
        <TD style="display:none">
            <xsl:choose>
                <xsl:when test= "string-length(@OwnerBusinessName) &gt; 0">
                    <xsl:value-of select="@OwnerBusinessName"/>
                </xsl:when>
                <xsl:when test= "string-length(@OwnerLastName) &gt; 0 and string-length(@OwnerFirstName) &gt; 0">
                  <xsl:value-of select="user:myNameConcat(string(@OwnerLastName), string(@OwnerFirstName), ', ')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat(string(@OwnerLastName), string(@OwnerFirstName))"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="string-length(@PropertyName) &gt; 0">
             (<xsl:value-of select="@PropertyName"/>)
            </xsl:if>
        </TD>
        <TD style="display:none"><xsl:value-of select="@Status"/></TD>
        <TD style="display:none"><xsl:value-of select="@ClaimAspectID"/></TD>
      </xsl:for-each>
    </TR>
  </xsl:template>

  <!-- Gets the Pending Vehicles -->
  <xsl:template name="Vehicles">
    <xsl:for-each select="/Root/ClaimRepDesktopScreen/Claim[@NewlyAssigned = '0']/Vehicle">
        <!-- <xsl:sort data-type="text" select="concat(string(@StatusOpenTimeDays), string(@StatusOpenTimeHours), string(@StatusOpenTimeMinutes), string(/Root/ClaimRepDesktopScreen/Claim[@LynxID]))" order="ascending" /> -->
      <xsl:sort data-type="number" select="@StatusOpenTimeDays" order="descending" />
      <xsl:sort data-type="number" select="@StatusOpenTimeHours" order="descending" />
      <xsl:sort data-type="number" select="@StatusOpenTimeMinutes" order="descending" />
      <xsl:sort data-type="number" select="../@LynxID" order="ascending" />
      <TR unselectable="on" onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' RowType='Vehicle' onClick='GridClick(this)' onDblClick="navigate2('vehicle', this)" style="height:21px">
      <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
        <TD unselectable="on" class="GridTypeTD"  width='65'>
          <!--   <xsl:value-of select="concat(string(@StatusOpenTimeDays), string(@StatusOpenTimeHours), string(@StatusOpenTimeMinutes), string(/Root/ClaimRepDesktopScreen/Claim[@LynxID]))"/>- -->
          <xsl:choose>
            <xsl:when test="@StatusOpenTimeDays!=0"><xsl:value-of select="@StatusOpenTimeDays"/><xsl:text> days</xsl:text></xsl:when>
            <xsl:when test="@StatusOpenTimeHours!=0"><xsl:value-of select="@StatusOpenTimeHours"/><xsl:text> hrs</xsl:text></xsl:when>
            <xsl:when test="@StatusOpenTimeMinutes!=0"><xsl:value-of select="@StatusOpenTimeMinutes"/><xsl:text> mins</xsl:text></xsl:when>
            <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
          </xsl:choose>
        </TD>
        <TD unselectable="on" class="GridTypeTD" width='55' ><xsl:value-of select="../@LynxID"/></TD>
        <TD unselectable="on" class="GridTypeTD" width='45'>
            <xsl:choose>
                <xsl:when test="contains(@Status, 'Closed')">
                <!-- <xsl:when test="@Status = 'Vehicle Closed'"> -->
                    <!-- <img src="/images/closedExposure.gif"/><img src="/images/spacer.gif" style="height:1px;width:5px"/> -->
                    <img src="/images/icon_vehicle_closed.gif" onclick="event.returnValue=false;" ondblclick="navigate2('vehicle')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)">
                        <xsl:attribute name="title">Vehicle <xsl:value-of select="@VehicleNumber"/> Closed</xsl:attribute>
                    </img>
                </xsl:when>
                <xsl:otherwise>
                    <img src="/images/icon_vehicle_open.gif" onclick="event.returnValue=false;" ondblclick="navigate2('vehicle')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)">
                        <xsl:attribute name="title">Vehicle <xsl:value-of select="@VehicleNumber"/> Open</xsl:attribute>
                    </img>
                </xsl:otherwise>
            </xsl:choose>
            <!-- <xsl:value-of select="@Status"/> -->
            <!-- <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp; -->
        </TD>
        <TD unselectable="on" class="GridTypeTD" width='150' style="text-align:left">
        <span class="nowrap">
        <xsl:choose>
          <xsl:when test="string-length(@OwnerBusinessName) > 0" >
            <xsl:value-of select="substring(@OwnerBusinessName,1,25)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of disable-output-escaping="yes" select="user:myNameConcat(substring(@OwnerLastName,0,12), substring(@OwnerFirstName,0,12), ', ')"/>
            <!-- <xsl:value-of select="substring(@OwnerLastName,0,12)"/>, <xsl:value-of select="substring(@OwnerFirstName,0,12)"/> -->          
          </xsl:otherwise>
        </xsl:choose>
        </span>
		</TD>
        <TD unselectable="on" class="GridTypeTD" width='50'><xsl:value-of select="@Party"/></TD>
        <TD unselectable="on" class="GridTypeTD" style="text-align:left">
          <span class="nowrap">
          <xsl:choose>
            <xsl:when test="@VehicleYear != '0'">
              <xsl:value-of disable-output-escaping="yes" select="user:myNameConcat(string(user:myNameConcat(string(@VehicleYear), string(@Make), ' ')), string(@Model), ' ')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of disable-output-escaping="yes" select="user:myNameConcat(string(@Make), string(@Model), ' ')"/>
            </xsl:otherwise>
          </xsl:choose>
          </span>
            <!-- <xsl:value-of select="@VehicleYear"/><xsl:text> - </xsl:text><xsl:value-of select="@Make"/><xsl:text> </xsl:text><xsl:value-of select="@Model"/> -->
        </TD>
        <TD unselectable="on" class="GridTypeTD" width='100'><span class="nowrap"><xsl:value-of select="../@ClaimOwnerName"/></span></TD>
        <TD style="display:none"><xsl:value-of select="user:UTCConvertDateByNodeType(..,'LossDate','A')"/></TD>
        <TD style="display:none"><xsl:value-of select="@OwnerLastName"/></TD>
        <TD style="display:none"><xsl:value-of select="@OwnerFirstName"/></TD>
        <TD style="display:none"><xsl:value-of select="@VehicleYear"/></TD>
        <TD style="display:none"><xsl:value-of select="@Make"/></TD>
        <TD style="display:none"><xsl:value-of select="@Model"/></TD>
        <TD style="display:none"><xsl:value-of select="../@ClaimOpenTimeDays"/></TD>
        <TD style="display:none"><xsl:value-of select="../@ClaimOpenTimeHours"/></TD>
        <TD style="display:none"><xsl:value-of select="../@ClaimOpenTimeMinutes"/></TD>
        <TD style="display:none"><xsl:value-of select="../@LossState"/></TD>
        <TD style="display:none"><xsl:value-of select="../@CoverageStatus"/></TD>
        <TD style="display:none">
          <xsl:value-of select="@StatusOpenTimeDays"/><xsl:text> days </xsl:text>
          <xsl:value-of select="@StatusOpenTimeHours"/><xsl:text> hrs </xsl:text>
          <xsl:value-of select="@StatusOpenTimeMinutes"/><xsl:text> mins</xsl:text>
        </TD>
        <TD style="display:none"><xsl:value-of select="@VehicleNumber"/></TD>
        <TD style="display:none"><xsl:value-of select="@Status"/></TD>
        <TD style="display:none"><xsl:value-of select="@ClaimAspectID"/></TD>
      </TR>
    </xsl:for-each>
  </xsl:template>

  <!-- Gets the Pending Propertiess -->
  <xsl:template name="Properties">
    <xsl:for-each select="/Root/ClaimRepDesktopScreen/Claim[@NewlyAssigned = '0']/Property">
      <xsl:sort data-type="number" select="@StatusOpenTimeDays" order="descending" />
      <xsl:sort data-type="number" select="@StatusOpenTimeHours" order="descending" />
      <xsl:sort data-type="number" select="@StatusOpenTimeMinutes" order="descending" />
      <xsl:sort data-type="number" select="../@LynxID" order="ascending" />
      <TR unselectable="on" onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' RowType='Property' onClick='GridClick(this)' onDblClick="navigate2('property', this)" style="height:21px">
      <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
        <TD unselectable="on"  class="GridTypeTD" width='65'>
          <xsl:choose>
            <xsl:when test="@StatusOpenTimeDays!=0"><xsl:value-of select="@StatusOpenTimeDays"/><xsl:text> days</xsl:text></xsl:when>
            <xsl:when test="@StatusOpenTimeHours!=0"><xsl:value-of select="@StatusOpenTimeHours"/><xsl:text> hrs</xsl:text></xsl:when>
            <xsl:when test="@StatusOpenTimeMinutes!=0"><xsl:value-of select="@StatusOpenTimeMinutes"/><xsl:text> mins</xsl:text></xsl:when>
            <xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
          </xsl:choose>
        </TD>
        <TD unselectable="on" width='55' class="GridTypeTD"><xsl:value-of select="../@LynxID"/></TD>
        <TD unselectable="on" width='45' class="GridTypeTD">
            <!-- <xsl:value-of select="@Status"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp; -->
            <xsl:choose>
                <xsl:when test="contains(@Status, 'Closed')">
                    <img src="/images/icon_property_closed.gif" onclick="event.returnValue=false;" ondblclick="navigate2('property')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)">
                        <xsl:attribute name="title">Property <xsl:value-of select="@PropertyNumber"/> Closed</xsl:attribute>
                    </img>
                </xsl:when>
                <xsl:otherwise>
                    <img src="/images/icon_property_open.gif" onclick="event.returnValue=false;" ondblclick="navigate2('property')" style="FILTER:alpha(opacity=50); cursor:hand;" onmouseout="makeHighlight(this,1,50)" onmouseover="makeHighlight(this,0,100)">
                        <xsl:attribute name="title">Property <xsl:value-of select="@PropertyNumber"/> Open</xsl:attribute>
                    </img>
                </xsl:otherwise>
            </xsl:choose>
        </TD>
        <TD unselectable="on" class="GridTypeTD" style="text-align:left">
	        <span class="nowrap">
          <xsl:choose>
	          <xsl:when test="string-length(@OwnerBusinessName) > 0" >
	            <xsl:value-of select="substring(@OwnerBusinessName,1,25)"/>
	          </xsl:when>
	          <xsl:otherwise>
                <xsl:value-of disable-output-escaping="yes" select="user:myNameConcat(substring(@OwnerLastName,0,12), substring(@OwnerFirstName,0,12), ', ')"/>
	            <!-- <xsl:value-of select="substring(@OwnerLastName,0,12)"/>, <xsl:value-of select="substring(@OwnerFirstName,0,12)"/> -->          
	          </xsl:otherwise>
	        </xsl:choose>
          </span>
				</TD>
        <TD unselectable="on" class="GridTypeTD" width="100" style="text-align:left"><span class="nowrap"><xsl:value-of select="../@ClaimOwnerName"/></span></TD>
        <TD style="display:none"><xsl:value-of select="user:UTCConvertDateByNodeType(..,'LossDate','A')"/></TD>
        <TD style="display:none"><xsl:value-of select="@OwnerLastName"/></TD>
        <TD style="display:none"><xsl:value-of select="@OwnerFirstName"/></TD>
        <TD style="display:none"><xsl:value-of select="../@ClaimOpenTimeDays"/></TD>
        <TD style="display:none"><xsl:value-of select="../@ClaimOpenTimeHours"/></TD>
        <TD style="display:none"><xsl:value-of select="../@ClaimOpenTimeMinutes"/></TD>
        <TD style="display:none"><xsl:value-of select="../@LossState"/></TD>
        <TD style="display:none"><xsl:value-of select="../@CoverageStatus"/></TD>
        <TD style="display:none"><xsl:value-of select="@PropertyName"/></TD>
        <TD style="display:none">
          <xsl:value-of select="@StatusOpenTimeDays"/><xsl:text> days </xsl:text>
          <xsl:value-of select="@StatusOpenTimeHours"/><xsl:text> hrs </xsl:text>
          <xsl:value-of select="@StatusOpenTimeMinutes"/><xsl:text> mins</xsl:text>
        </TD>
        <TD style="display:none"><xsl:value-of select="@PropertyNumber"/></TD>
        <TD style="display:none"><xsl:value-of select="@Status"/></TD>
        <TD style="display:none"><xsl:value-of select="@ClaimAspectID"/></TD>
      </TR>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
