<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="Insurance">

<xsl:import href="msxsl/msjs-client-library-htc.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="InfoCRUD" select="Client Configuration"/>

<xsl:template match="/Root">
<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.0.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspAdmInsPaymentTypeGetListXML,refInsPaymentType.xsl, 999   -->

<HEAD>
<TITLE>Data Administration: Insurance Companies</TITLE>
<LINK rel="stylesheet" href="/css/apdControls.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>
<SCRIPT language="javascript" src="/js/apdcontrols.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<SCRIPT language="Javascript">
    var PTCount = <xsl:value-of select="count(/Root/Reference[@List='PaymentType'])"/>;
    var sCRUD = "<xsl:value-of select="$InfoCRUD"/>";
<![CDATA[

    var gbDirty = false;

    function pageInit(){
        if (sCRUD.indexOf("U") == -1)
            disableControls(true);
    }

    function InitInputs(){
    }

    function setDirty(){
        if (parent.sCRUD.indexOf("U") == -1) return;
        gbDirty = true;
        parent.gbDirty = true;

        //parent.setCurRowDirty();
    }

    function disableControls(val){
        for (var i = 1; i <= PTCount; i++){
            var PTFlg = document.all["CSFlag" + i];
            if (PTFlg)
              PTFlg.CCDisabled = val;
        }
    }

    function getSelectedPTFlags(){
      var str = "";
      for (var i = 1; i <= PTCount; i++){
        var PTFlg = document.all["CSFlag" + i];
        if (PTFlg)
          if (PTFlg.value == "1") {
            if (PTFlg.parentElement.parentElement.tagName == "TR") {
              str += PTFlg.parentElement.parentElement.lastChild.innerText + ",";
            }
          }
      }
      //remove the last comma
      str = str.substring(0, str.length - 1);
      return str;
    }

]]>

</SCRIPT>
</HEAD>
<BODY class="bodyAPDSub" unselectable="on" style="background-color:#FFFFFF;overflow:hidden" onload="pageInit();" tabIndex="-1" topmargin="0"  leftmargin="0">
    <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%;padding:10px;" unselectable="on">
        <tr valign="middle">
            <td unselectable="on" class="TDLabel">
                Select Payment Types: <!-- <input type="button" onclick="getSelectedPTFlags()" value="Click me"/> -->
                <div style="width:300px;height:180px;border:1px solid gray;padding:6px;overflow:auto">
                    <xsl:call-template name="PaymentTypes"/>
                </div>
            </td>
        </tr>
    </table>

</BODY>
</HTML>
</xsl:template>

<xsl:template name="PaymentTypes">
  <xsl:variable name="selectedPT">
    <xsl:for-each select="ClientPaymentType">
      <xsl:text>|</xsl:text>
      <xsl:value-of select="@PaymentTypeCD"/>
      <xsl:text>|</xsl:text>
    </xsl:for-each>
  </xsl:variable>
  <table border="0" cellspacing="0" cellpadding="2">
    <xsl:for-each select="/Root/Reference[@List='PaymentType']">
      <xsl:sort select="@Name"/>
      <xsl:variable name="PTId">
        <xsl:text>|</xsl:text>
        <xsl:value-of select="@ReferenceID"/>
        <xsl:text>|</xsl:text>
      </xsl:variable>
      <xsl:variable name="curSelected">
        <xsl:choose>
          <xsl:when test="contains($selectedPT, $PTId)">1</xsl:when>
          <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:variable name="cur">
        <xsl:value-of select="@ReferenceID"/>
      </xsl:variable>
      <tr>
        <td>
          <xsl:value-of disable-output-escaping="yes" select="js:APDCheckBox(concat('CSFlag', string(position())), string(@Name), string($curSelected), '', '', '', 23, 'true', '', 'true', 'setDirty')"/>
        </td>
        <td style="display:none"><xsl:value-of select="@ReferenceID"/></td>
      </tr>
    </xsl:for-each>
  </table>
</xsl:template>
</xsl:stylesheet>
