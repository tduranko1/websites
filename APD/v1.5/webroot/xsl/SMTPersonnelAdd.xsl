<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="../errorhandler.asp" -->
<!-- #include file="../vbs/SessionManagerV2.vbs" -->
<!-- #include file="../vbs/SecurityCheckV2.vbs" -->

<%
  On Error Resume Next

  Dim objExe, strHTML, strUserID, strEntityID, strEntityType

  strUserID = GetSession("UserID")
  
  'Get values from the query sring
  strEntityID = request("EntityID")
  strEntityType = request("EntityType")
  
    
  'Create and initialize DataPresenter
  Set objExe = CreateObject("DataPresenter.CExecute")
  CheckError()

  objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
  CheckError()
	
  select case request("mode")
    case "wizard"
		  objExe.AddXslParam "mode", "wizard"
		  CheckError()
  	case "update" 
	    objExe.ExecuteSpNp "uspSMTPersonnelUpdEntity", request.form
		  CheckError()
      if request("CallBack") <> vbnullstring then
			  response.redirect(request("CallBack"))
		  else
			  response.redirect "SMTPersonnel.asp?EntityID=" & strEntityID & "&EntityType=" & strEntityType
		  end if
   end select
  
  objExe.AddXslParam "UserID", strUserID
  CheckError()
  
  strHTML = objExe.ExecuteSpAsXML( "uspSMTPersonnelGetDetailXML", "SMTAddPersonnel.xsl", 0, strEntityType )
  CheckError()

  Set objExe = Nothing

  Response.Write strHTML

  On Error GoTo 0
%>
