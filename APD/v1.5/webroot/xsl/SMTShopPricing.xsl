<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:js="urn:the-xml-files:xslt"
	id="SMTShopPricing">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- This page is a child of SMTDetailLevel.  This page resides in that page's 'ifrmContent' iframe and
       is dependent on global variables in that page to maintain state -->

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserID"/>
<xsl:param name="ShopID"/>
<xsl:param name="mode"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTShopPricingGetDetailXML,SMTShopPricing.xsl,2025   -->

<HEAD>
  <TITLE>Shop Pricing</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>

<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/SMTValid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<!-- Page Specific Scripting -->
<script language="javascript">

var gsUserID = '<xsl:value-of select="$UserID"/>';
var gsBillingID = '<xsl:value-of select="@BillingID"/>';
var gsPricingCount = '<xsl:value-of select="count(Pricing/@SysLastUserID)"/>';
var gsMode = '<xsl:value-of select="$mode"/>';
var gsPageFile = "SMTShopPricing.asp";
var gsShopCRUD = '<xsl:value-of select="$ShopCRUD"/>';

<![CDATA[

function InitPage(){
	if(gsMode != "wizard")
		InitEditPage();
	else
		parent.frameLoaded(6);

	if (gsShopCRUD.indexOf("U") != -1) frmPricing.txtSheetMetal.focus();
	onpasteAttachEvents();

	var aInputs = null;
  var obj = null;

  aInputs = frmPricing.tags("INPUT");
  for (var i=0; i < aInputs.length; i++) {
    if (aInputs[i].type=="text"){
	    aInputs[i].attachEvent("onchange", SetDirty);
			aInputs[i].attachEvent("onkeypress", SetDirty);
      aInputs[i].attachEvent("onpropertychange", SetDirty);
    }
    if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
  }

  aInputs = frmPricing.tags("SELECT");
  for (var i=0; i < aInputs.length; i++) {
    aInputs[i].attachEvent("onchange", SetDirty);
    if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
  }
}

function InitEditPage(){
	top.gsPageFile = gsPageFile;
	parent.btnDelete.style.visibility = "hidden";

  if (gsShopCRUD.indexOf("U") > -1){
    parent.btnSave.style.visibility = "visible";
    parent.btnSave.disabled = false;
  }
  else
    parent.btnSave.style.visibility = "hidden";

	parent.tab25.className = "tabactive1";
	parent.gsPageName = "Shop Pricing";
}

function selGlassReplChgType_onchange(){
	lblWindshield.style.display = "none";
	lblGlassDollar.style.display = "none";
	lblWindshieldPercent.style.display = "none";
	frmPricing.txtWindshieldDiscount.style.display = "none";
	lblSideBack.style.display = "none";
	frmPricing.txtGlassOutsourceSvcFee.style.display = "none";
	lblGlassPercent.style.display = "none";

	frmPricing.txtSideBackGlassDiscount.style.display = "none";
	lblSubletFee.style.display = "none";
	frmPricing.txtGlassSubletSvcFee.style.display = "none";
	lblSubletPct.style.display = "none";
	frmPricing.txtGlassSubletSvcPct.style.display = "none";

	switch (frmPricing.selGlassReplChgType.value){
		case "N":
			lblWindshield.style.display = "inline";
			frmPricing.txtWindshieldDiscount.style.display = "inline";
			lblWindshieldPercent.style.display = "inline";
			lblSideBack.style.display = "inline";
			frmPricing.txtSideBackGlassDiscount.style.display = "inline";
			lblGlassPercent.style.display = "inline";
			break;
		case "S":
			lblSubletFee.style.display = "inline";
			lblGlassDollar.style.display = "inline";
			frmPricing.txtGlassSubletSvcFee.style.display = "inline";
			lblSubletPct.style.display = "inline";
			frmPricing.txtGlassSubletSvcPct.style.display = "inline";
			lblGlassPercent.style.display = "inline";
			break;
		case "O":
			lblGlassDollar.style.display = "inline";
			frmPricing.txtGlassOutsourceSvcFee.style.display = "inline";
			break;
	}
}

function selTowChgType_onchange(){
	if (frmPricing.selTowChargeType.value == "F"){
		lblFlatFee.style.display = "inline";
		lblFFDollar.style.display= "inline";
		frmPricing.txtFlatFee.style.display = "inline";
	}
	else{
		lblFlatFee.style.display = "none";
		lblFFDollar.style.display= "none";
		frmPricing.txtFlatFee.style.display = "none";
	}
}

function CheckNum(event){
	var obj = eval("frmPricing." + event.srcElement.id);
	var idx = obj.value.indexOf(".")
	var pct = false;
	var prec;
	var code = event.keyCode;

  	if (obj.getAttribute("precision") != null)
    	prec = obj.getAttribute("precision");
  	else
    	prec = 2;

	if ((code < 48 && code != 46) || code > 57){ // limit to numerals and decimal point
		event.returnValue = false;
		return;
	}

	if (arguments[1])
		pct = true;

	if (code == 45){				// no negatives
		event.returnValue = false;
		return;
	}

	if (idx > -1){
		//if (obj.value.length - idx > prec){		// allow only proper number decimal places -- 1 for percentages, 2 otherwise
		//	event.returnValue = false;
		//	return;
		//}

		if (code == 46){			// allow only one decimal point
			event.returnValue = false;
			return;
		}
	}

	if (pct){								// ensure percentages do not exceed 100
		if (obj.value == 100){
			event.returnValue = false
			return;
		}

		if (obj.value == 10 && code!= 46 && code != 48 && idx == -1){
			event.returnValue = false;
			return;
		}

		if (obj.value > 10 && code != 46 && idx == -1){
			event.returnValue = false;
			return;
		}
	}
}

function IsDirty(){
	Save();
}

function Save(){
	var mode;
	var elms = frmPricing.tags("INPUT");
  var obj;
  var precision;
  var scale;
  var whole;
  var decimal;
  var decPos;
  
	if (document.getElementById("selTowChargeType").value == "F" && document.getElementById("txtFlatFee").value == ""){
      parent.ClientWarning("Enter a Flat Fee amount.");
      document.getElementById("txtFlatFee").focus();
      return false;
   }
   
	if (document.getElementById("selGlassReplChgType").value == "N" && document.getElementById("txtWindshieldDiscount").value == ""){
      parent.ClientWarning("Enter a Windshield Discount.");
      document.getElementById("txtWindshieldDiscount").focus();
      return false;
   }
	if (document.getElementById("selGlassReplChgType").value == "N" && document.getElementById("txtSideBackGlassDiscount").value == ""){
      parent.ClientWarning("Enter the Side/Back Discount.");
      document.getElementById("txtSideBackGlassDiscount").focus();
      return false;
   }
	if (document.getElementById("selGlassReplChgType").value == "S" && (document.getElementById("txtGlassSubletSvcFee").value == "" && document.getElementById("txtGlassSubletSvcPct").value == "")){
      parent.ClientWarning("Enter a Sublet Fee or Sublet Percent.");
      document.getElementById("txtGlassSubletSvcFee").focus();
      return false;
   }
	if (document.getElementById("selGlassReplChgType").value == "O" && document.getElementById("txtGlassOutsourceSvcFee").value == ""){
      parent.ClientWarning("Enter a Outsourcing Fee.");
      document.getElementById("txtGlassOutsourceSvcFee").focus();
      return false;
   }
  
   for(var i = 0; i < elms.length; i++){
    obj = elms[i];
		if (obj.type == "text" && obj.name != ""){
			if (isNaN(obj.value)){
        parent.ClientWarning("Invalid Numeric value specified for '" + obj.label + "'. Please try again.");
        try{
          obj.select();
          obj.focus();
        }
        catch(e) {}
        return false;
      }
      
      precision = obj.getAttribute("precision");
      scale = obj.getAttribute("scale");
      decPos = obj.value.indexOf(".");
      whole = "";
      decimal = "";
      
      if (decPos > -1){
        whole = obj.value.slice(0, decPos);
        decimal = obj.value.slice(decPos + 1);
      }
      else
        whole = obj.value;
        
      if (whole.length > (precision - scale)){
        parent.ClientWarning("The whole portion of '" + obj.label + "' must not exceed " + (precision - scale).toString() + " digits. Please try again.");
        try{
          obj.select();
          obj.focus();
        }
        catch(e){}
        return false;
      }
      
      if (decimal.length > scale){
        //obj.value = Math.round(obj.value * scale)/scale;
        parent.ClientWarning("The decimal portion of " + obj.label + "' must not exceed " +  scale.toString() + " digits. Please try again.");
        try{
          obj.select();
          obj.focus();
        }
        catch(e) {}
        return false;
      }
    }
  
  }

  if (gsPricingCount == 0)
		mode = "insert";
	else
		mode = "update";

	parent.btnSave.disabled = true;
	frmPricing.action += "?mode=" + mode;
	frmPricing.submit();
	parent.gbDirtyFlag = false;
}

function SetDirty(){
	parent.gbDirtyFlag = true;
	frmPricing.txtDirtyFlag.value = "1";
}

]]>

</script>

</HEAD>

<body class="bodyAPDsub" onload="InitPage();" style="overflow:hidden; margin-left:0; margin-right:0;">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>

<div style="background-color:#FFFAEB; height:450px;">

<form id="frmPricing" method="post" style="overflow:hidden">
  <xsl:apply-templates select="Pricing"/> <!-- Get Pricing Info -->

  <input type="hidden" id="txtDirtyFlag" name="DirtyFlag" wizname="PricingDirtyFlag" value="0"/>
  <input type="hidden" id="txtShopID" name="ShopID">
    <xsl:attribute name="value"><xsl:value-of select="@ShopID"/></xsl:attribute>
  </input>

  <input type="hidden" id="txtSysLastUserID" name="SysLastUserID">
    <xsl:attribute name="value"><xsl:value-of select="$UserID"/></xsl:attribute>
  </input>
  <input type="hidden" id="txtSysLastUpdatedDate" name="SysLastUpdatedDate">
    <xsl:attribute name="value"><xsl:value-of select="Pricing/@SysLastUpdatedDate"/></xsl:attribute>
  </input>
  <input type="hidden" id="txtCallBack" name="CallBack"/>
</form>
</div>

<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

</body>
</HTML>
</xsl:template>

<xsl:template match="Pricing">
<TABLE unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="0">
<TR>
<TD width="10px"></TD>
<TD width="30%" valign="top">
<table unselectable="on" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2"><b>Hourly Labor Rates:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Sheet Metal:</td>
          <td align="right">$
            <input type="text" id="txtSheetMetal" name="HourlyRateSheetMetal" wizname="HourlyRateSheetMetal" label="Sheet Metal" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='HourlyRateSheetMetal']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='HourlyRateSheetMetal']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@HourlyRateSheetMetal"/></xsl:attribute>
            </input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Refinishing:</td>
          <td align="right">$
            <input type="text" id="txtRefinish" name="HourlyRateRefinishing" wizname="HourlyRateRefinishing" label="Refinishing" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='HourlyRateRefinishing']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='HourlyRateRefinishing']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@HourlyRateRefinishing"/></xsl:attribute>
			      </input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Unibody/Frame:</td>
          <td align="right">$
            <input type="text" id="txtUnibodyFrame" name="HourlyRateUnibodyFrame" wizname="HourlyRateUnibodyFrame" label="Unibody/Frame" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='HourlyRateUnibodyFrame']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='HourlyRateUnibodyFrame']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@HourlyRateUnibodyFrame"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Mechanical:</td>
          <td align="right">$
            <input type="text" id="txtMechanical" name="HourlyRateMechanical" wizname="HourlyRateMechanical" label="Mechanical" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='HourlyRateMechanical']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='HourlyRateMechanical']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@HourlyRateMechanical"/></xsl:attribute>
      			</input>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="7px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td colspan="2">
      <table unselectable="on" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="4"><b>Refinish Materials Charges:</b></td>
        </tr>
        <!-- <tr>
          <td width="5px"></td>
          <td colspan="3">Single Stage:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
          <td>Charge Per Hour:</td>
          <td align="right">$
            <input type="text" id="txtHourly1" name="RefinishSingleStageHourly" wizname="RefinishSingleStageHourly" label="Refinish Materials Single Stage Per Hour" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishSingleStageHourly']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishSingleStageHourly']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@RefinishSingleStageHourly"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Maximum Charge:</td>
          <td align="right">$
            <input type="text" id="txtMax1" name="RefinishSingleStageMax" wizname="RefinishSingleStageMax" label="Refinish Materials Single Stage Max Charge" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishSingleStageMax']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishSingleStageMax']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@RefinishSingleStageMax"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Two Stage:</td>
        </tr> -->
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Charge Per Hour:</td>
          <td align="right">$
            <input type="text" id="txtHourly2" name="RefinishTwoStageHourly" wizname="RefinishTwoStageHourly" label="Refinish Materials Two Stage Per Hour" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
          	  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishTwoStageHourly']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishTwoStageHourly']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageHourly"/></xsl:attribute>
          	</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Maximum Charge:</td>
          <td align="right">$
            <input type="text" id="txtMax2" name="RefinishTwoStageMax" wizname="RefinishTwoStageMax" label="Refinish Materials Two Stage Max Charge" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishTwoStageMax']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishTwoStageMax']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageMax"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Clear Coat Max Hours:</td>
          <td align="right">
            <input type="text" id="txtCCMax2" name="RefinishTwoStageCCMaxHrs" wizname="RefinishTwoStageCCMaxHrs" label="Refinish Materials Two Stage Clear Coat Max Hours" style="text-align:right;" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishTwoStageCCMaxHrs']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishTwoStageCCMaxHrs']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@RefinishTwoStageCCMaxHrs"/></xsl:attribute>
      			</input>hrs
          </td>
        </tr>
        <!-- <tr>
          <td width="5px"></td>
          <td colspan="3">Three Stage:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Charge Per Hour:</td>
          <td align="right">$
            <input type="text" id="txtHourly3" name="RefinishThreeStageHourly" wizname="RefinishThreeStageHourly" label="Refinish Materials Three Stage Per Hour" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishThreeStageHourly']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishThreeStageHourly']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageHourly"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Maximum Charge:</td>
          <td align="right">$
            <input type="text" id="txtMax3" name="RefinishThreeStageMax" wizname="RefinishThreeStageMax" label="Refinish Materials Three Stage Max Charge" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishThreeStageMax']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishThreeStageMax']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageMax"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Clear Coat Max Hours:</td>
          <td align="right">
            <input type="text" id="txtCCMax3" name="RefinishThreeStageCCMaxHrs" wizname="RefinishThreeStageCCMaxHrs" label="Refinish Materials Three Stage Clear Coat Max Hours" style="text-align:right;" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishThreeStageCCMaxHrs']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='RefinishThreeStageCCMaxHrs']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@RefinishThreeStageCCMaxHrs"/></xsl:attribute>
      			</input>hrs
          </td>
        </tr> -->
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="7px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
	  <div style="height:55">
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Towing Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Charge Type:</td>
          <td align="right">
            <select id="selTowChargeType" name="TowInChargeTypeCD" wizname="TowInChargeTypeCD" style="text-align:right;"  onchange="selTowChgType_onchange()">
              <option value=""></option>
      			  <xsl:for-each select="/Root/Reference[@ListName='TowingChargeType']">
      			    <xsl:call-template name="BuildSelectOptions">
      				  <xsl:with-param name="current" select="/Root/Pricing/@TowInChargeTypeCD"/>
      				</xsl:call-template>
      			  </xsl:for-each>
            </select>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>
            <label id="lblFlatFee">
              <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@TowInChargeTypeCD='F'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>
              Flat Fee:
            </label>
          </td>
          <td align="right">
            <label id="lblFFDollar">
              <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@TowInChargeTypeCD='F'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>
              $
            </label>
            <input type="text" id="txtFlatFee" name="TowInFlatFee" wizname="TowInFlatFee" label="Towing Charges Flat Fee" style="text-align:right;"  ize="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@TowInChargeTypeCD='F'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>              
              <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='TowInFlatFee']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='TowInFlatFee']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@TowInFlatFee"/></xsl:attribute>
      			</input>
          </td>
        </tr>
      </table>
	  </div>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="7px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Frame Setup Charges (Hours):</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>No Measurement:</td>
          <td align="right">
            <input type="text" id="txtPullNoMeasure" name="PullSetup" wizname="PullSetup" label="Frame Setup No Measurement" style="text-align:right;" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='PullSetUp']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='PullSetUp']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@PullSetup"/></xsl:attribute>
      			</input>hrs
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>With Measurement</td>
          <td align="right">
            <input type="text" id="txtPullMeasure" name="PullSetupMeasure" wizname="PullSetupMeasure" label="Frame Setup With Measurement" style="text-align:right;" size="7" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='PullSetUpMeasure']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='PullSetUpMeasure']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@PullSetupMeasure"/></xsl:attribute>
      			</input>hrs
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</TD>
<TD width="30px"></TD> <!-- spacer column -->
<TD width="30%" valign="top">
<table width="100%" valign="top" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <table width="100%" valign="top" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="4"><b>Air Conditioning Service Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">R-12 Evaluate/Recharge:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>With Freon:</td>
          <td align="right">$
            <input type="text" id="txtR12Freon" name="R12EvacuateRechargeFreon" wizname="R12EvacuateRechargeFreon" label="AC R-12 With Freon" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='R12EvacuateRechargeFreon']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='R12EvacuateRechargeFreon']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@R12EvacuateRechargeFreon"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Without Freon:</td>
          <td align="right">$
            <input type="text" id="txtR12NoFreon" name="R12EvacuateRecharge" wizname="R12EvacuateRecharge" label="AC R-12 Without Freon" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='R12EvacuateRecharge']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='R12EvacuateRecharge']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@R12EvacuateRecharge"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">R-134 Evaluate/Recharge:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>With Freon:</td>
          <td align="right">$
            <input type="text" id="txtR134Freon" name="R134EvacuateRechargeFreon" wizname="R134EvacuateRechargeFreon" label="AC R-134 With Freon" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='R134EvacuateRechargeFreon']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='R134EvacuateRechargeFreon']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@R134EvacuateRechargeFreon"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Without Freon:</td>
          <td align="right">$
            <input type="text" id="txtR134NoFreon" name="R134EvacuateRecharge" wizname="R134EvacuateRecharge" label="AC R-134 Without Freon" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='R134EvacuateRecharge']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='R134EvacuateRecharge']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@R134EvacuateRecharge"/></xsl:attribute>
      			</input>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="4"><b>Stripe Replacement Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Tape Stripes:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Per Panel:</td>
          <td align="right">$
            <input type="text" id="txtTapePerPanel" name="StripeTapePerPanel" wizname="StripeTapePerPanel" label="Tape Stripes Per Panel" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='StripeTapePerPanel']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='StripeTapePerPanel']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@StripeTapePerPanel"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Per Side:</td>
          <td align="right">$
            <input type="text" id="txtTapePerSide" name="StripeTapePerSide" wizname="StripeTapePerSide" label="Tape Stripes Per Side" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='StripeTapePerSide']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='StripeTapePerSide']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@StripeTapePerSide"/></xsl:attribute>
      			</input>
          </td>
		</tr>
        <tr>
          <td width="5px"></td>
          <td colspan="3">Paint Stripes:</td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td width="10px"></td>
          <td>Per Panel:</td>
          <td align="right">$
            <input type="text" id="txtPaintPerPanel" name="StripePaintPerPanel" wizname="StripePaintPerPanel" label="Paint Stripes Per Panel" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='StripePaintPerPanel']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='StripePaintPerPanel']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@StripePaintPerPanel"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td></td>
          <td>Per Side:</td>
          <td align="right">$
            <input type="text" id="txtPaintPerSide" name="StripePaintPerSide" wizname="StripePaintPerSide" label="Paint Stripes Per Side" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='StripePaintPerSide']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='StripePaintPerSide']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@StripePaintPerSide"/></xsl:attribute>
      			</input>
          </td>
		</tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Glass Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Replacement Type:</td>
          <td align="right">
            <select id="selGlassReplChgType" name="GlassReplacementChargeTypeCD" wizname="GlassReplacementChargeTypeCD" style="text-align:right;"  onchange="selGlassReplChgType_onchange()">
              <option value=""></option>
      			  <xsl:for-each select="/Root/Reference[@ListName='GlassReplacementType']">
      			    <xsl:call-template name="BuildSelectOptions">
      				  <xsl:with-param name="current" select="/Root/Pricing/@GlassReplacementChargeTypeCD"/>
      				</xsl:call-template>
      			  </xsl:for-each>
            </select>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>
            <label id="lblWindshield">
              <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@GlassReplacementChargeTypeCD='N'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>
              Windshield Discount:
            </label>
            <label id="lblSubletFee">
              <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@GlassReplacementChargeTypeCD='S'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>
              Sublet Fee:
            </label>
          </td>
          <td align="right">
            <input type="text" id="txtWindshieldDiscount" name="DiscountPctWindshield" wizname="DiscountPctWindshield" label="Windshield Discount" style="text-align:right;" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1)">
      			  <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@GlassReplacementChargeTypeCD='N'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>              
              <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='DiscountPctWindshield']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='DiscountPctWindshield']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@DiscountPctWindshield"/></xsl:attribute>
      			</input>
      			<label id="lblWindshieldPercent">
              <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@GlassReplacementChargeTypeCD='N'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>
              %
            </label>
      			<label id="lblGlassDollar">
              <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@GlassReplacementChargeTypeCD='S'">inline</xsl:when>
                  <xsl:when test="@GlassReplacementChargeTypeCD='O'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>
              $
            </label>
            <input type="text" id="txtGlassSubletSvcFee" name="GlassSubletServiceFee" wizname="GlassSubletServiceFee" label="Glass Sublet Fee" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event); frmPricing.GlassSubletServicePct.value = '';" onchange="frmPricing.GlassSubletServicePct.value = '';">
      			  <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@GlassReplacementChargeTypeCD='S'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>
              <xsl:attribute name="title">Please enter either a Sublet Service Fee OR a Sublet Discount Percentage</xsl:attribute>
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='GlassSubletServiceFee']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='GlassSubletServiceFee']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@GlassSubletServiceFee"/></xsl:attribute>
      			</input>
      			<input type="text" id="txtGlassOutsourceSvcFee" name="GlassOutsourceServiceFee" wizname="GlassOutsourceServiceFee" label="Glass Outsource Fee" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@GlassReplacementChargeTypeCD='O'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>              
              <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='GlassOutsourceServiceFee']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='GlassOutsourceServiceFee']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@GlassOutsourceServiceFee"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>
  	        <label id="lblSideBack">
              <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@GlassReplacementChargeTypeCD='N'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>
              Side/Back Discount:
            </label>
            <label id="lblSubletPct">
              <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@GlassReplacementChargeTypeCD='S'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>
              Sublet Percent:
            </label>
          </td>
          <td align="right">
            <input type="text" id="txtSideBackGlassDiscount" name="DiscountPctSideBackGlass" wizname="DiscountPctSideBackGlass" label="Side/Back Glass Discount" style="text-align:right;" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1)">
      			  <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@GlassReplacementChargeTypeCD='N'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>
              <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='DiscountPctSideBackGlass']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='DiscountPctSideBackGlass']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@DiscountPctSideBackGlass"/></xsl:attribute>
      			</input>
      			<input type="text" id="txtGlassSubletSvcPct" name="GlassSubletServicePct" wizname="GlassSubletServicePct" label="Glass Sublet Percent" style="text-align:right;" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1); frmPricing.GlassSubletServiceFee.value = '';" onchange="frmPricing.GlassSubletServiceFee.value = '';">
      			  <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@GlassReplacementChargeTypeCD='S'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>
              <xsl:attribute name="title">Please enter either a Sublet Service Fee OR a Sublet Discount Percentage</xsl:attribute>
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='GlassSubletServicePct']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='GlassSubletServicePct']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@GlassSubletServicePct"/></xsl:attribute>
      			</input>
      			<label id="lblGlassPercent">
              <xsl:attribute name="style">display:
                <xsl:choose>
                  <xsl:when test="@GlassReplacementChargeTypeCD='N'">inline</xsl:when>
                  <xsl:when test="@GlassReplacementChargeTypeCD='S'">inline</xsl:when>
                  <xsl:otherwise>none</xsl:otherwise>                
                </xsl:choose>
              </xsl:attribute>
              %
            </label>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</TD>
<TD width="30px"></TD> <!-- spacer column -->
<TD width="34%" valign="top">
<table border="0" width="235" cellpadding="0" cellspacing="0">
  <tr valign="top" width="100%">
    <td>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Recycled Parts:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Markup Percent:</td>
          <td align="right">
            <input type="text" id="txtRecycledPartsMarkup" name="PartsRecycledMarkupPct" wizname="PartsRecycledMarkupPct" label="Recycled Parts Markup Percent" style="text-align:right;" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event, 1)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='PartsRecycledMarkupPct']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='PartsRecycledMarkupPct']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@PartsRecycledMarkupPct"/></xsl:attribute>
      			</input>%
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="6px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="235" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Additional Repair Charges:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Daily Storage:</td>
          <td align="right">$
            <input type="text" id="txtDailyStorage" name="DailyStorage" wizname="DailyStorage" label="Daily Storage" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='DailyStorage']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='DailyStorage']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@DailyStorage"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>2-Wheel Alignment:</td>
          <td align="right">$
            <input type="text" id="txtAlign2Wheel" name="AlignmentTwoWheel" wizname="AlignmentTwoWheel" label="2-Wheel Alignment" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='AlignmentTwoWheel']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='AlignmentTwoWheel']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@AlignmentTwoWheel"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>4-Wheel Alignment:</td>
          <td align="right">$
            <input type="text" id="txtAlign4Wheel" name="AlignmentFourWheel" wizname="AlignmentFourWheel" label="4-Wheel Alignment" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='AlignmentFourWheel']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='AlignmentFourWheel']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@AlignmentFourWheel"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Tire Mount and Balance:</td>
          <td align="right">$
            <input type="text" id="txtTireMountBalance" name="TireMountBalance" wizname="TireMountBalance" label="Tire Mount and Balance" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='TireMountBalance']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='TireMountBalance']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@TireMountBalance"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Flex Additive:</td>
          <td align="right">$
            <input type="text" id="txtFlexAdditive" name="FlexAdditive" wizname="FlexAdditive" label="Flex Additive" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='FlexAdditive']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='FlexAdditive']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@FlexAdditive"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Coolant (Green):</td>
          <td align="right">$
            <input type="text" id="txtCoolantGreen" name="CoolantGreen" wizname="CoolantGreen" label="Coolant (Green)" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='CoolantGreen']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='CoolantGreen']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@CoolantGreen"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Coolant (Red):</td>
          <td align="right">$
            <input type="text" id="txtCoolantRed" name="CoolantRed" wizname="CoolantRed" label="Coolant (Red)" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='CoolantRed']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='CoolantRed']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@CoolantRed"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Undercoat:</td>
          <td align="right">$
            <input type="text" id="txtUndercoat" name="Undercoat" wizname="Undercoat" label="Undercoat" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='Undercoat']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='Undercoat']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@Undercoat"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Chip Guard:</td>
          <td align="right">$
            <input type="text" id="txtChipGuard" name="ChipGuard" wizname="ChipGuard" label="Chip Guard" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='ChipGuard']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='ChipGuard']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@ChipGuard"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Caulking/Seam Sealer:</td>
          <td align="right">$
            <input type="text" id="txtCaulkSeamSeal" name="CaulkingSeamSealer" wizname="CaulkingSeamSealer" label="Caulking/Seam Sealer" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='CaulkingSeamSealer']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='CaulkingSeamSealer']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@CaulkingSeamSealer"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Corrosion Protection:</td>
          <td align="right">$
            <input type="text" id="txtCorrosionProt" name="CorrosionProtection" wizname="CorrosionProtection" label="Corrosion Protection" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='CorrosionProtection']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='CorrosionProtection']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@CorrosionProtection"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Cover Car:</td>
          <td align="right">$
            <input type="text" id="txtCoverCar" name="CoverCar" wizname="CoverCar" label="Cover Car" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='CoverCar']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='CoverCar']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@CoverCar"/></xsl:attribute>
      			</input>
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Hazardous Waste:</td>
          <td align="right">$
            <input type="text" id="txtHazardousWaste" name="HazardousWaste" wizname="HazardousWaste" label="Hazardous Waste" style="text-align:right;" size="10" maxlength="8" class="inputFld" onkeypress="CheckNum(event)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='HazardousWaste']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='HazardousWaste']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@HazardousWaste"/></xsl:attribute>
      			</input>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><img src="/images/spacer.gif" height="8px"/></td>  <!-- spacer row -->
  </tr>
  <tr>
    <td>
      <table width="235" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3"><b>Tax Rates:</b></td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Sales:</td>
          <td align="right">
            <input type="text" id="txtSalesTax" name="SalesTaxPct" wizname="SalesTaxPct" label="Sales Tax" style="text-align:right;" size="8" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='SalesTaxPct']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='SalesTaxPct']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@SalesTaxPct"/></xsl:attribute>
      			</input>%
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>County:</td>
          <td align="right">
            <input type="text" id="txtCountyTax" name="CountyTaxPct" wizname="CountyTaxPct" label="County Tax" style="text-align:right;" size="8" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='CountyTaxPct']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='CountyTaxPct']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@CountyTaxPct"/></xsl:attribute>
      			</input>%
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Municipal:</td>
          <td align="right">
            <input type="text" id="txtMunicipalTax" name="MunicipalTaxPct" wizname="MunicipalTaxPct" label="Municipal Tax" style="text-align:right;" size="8" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='MunicipalTaxPct']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='MunicipalTaxPct']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@MunicipalTaxPct"/></xsl:attribute>
      			</input>%
          </td>
        </tr>
        <tr>
          <td width="5px"></td>
          <td>Other:</td>
          <td align="right">
            <input type="text" id="txtOtherTax" name="OtherTaxPct" wizname="OtherTaxPct" label="Other Tax" style="text-align:right;" size="8" maxlength="6" class="inputFld" precision="3" onkeypress="CheckNum(event, 3)">
      			  <xsl:attribute name="precision"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='OtherTaxPct']/@Precision"/></xsl:attribute>
              <xsl:attribute name="scale"><xsl:value-of select="/Root/Metadata[@Entity='Pricing']/Column[@Name='OtherTaxPct']/@Scale"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@OtherTaxPct"/></xsl:attribute>
      			</input>%
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</TD>
</TR>
</TABLE>
</xsl:template>

</xsl:stylesheet>
