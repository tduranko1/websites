<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMTEntityDistanceSearchResults">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">

<HTML>

<HEAD>
  <TITLE>Search Results</TITLE>
  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  
<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDNavigate.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
var gsSearchType = 'S'; //  Shop Search(Distance)
var gsCount = '<xsl:value-of select="count(Shop)"/>';

 
<![CDATA[

function initPage(){
	parent.ShowCount(gsCount);
	divResultList.scrollTop = parent.parent.gsScroll;	
}

function GridSelect(oRow){

  var sEntityID = oRow.cells[6].innerText; // get value of  Shop ID
  var sShopBusinessID = '';
  try{
    NavToShop(gsSearchType, sEntityID, sShopBusinessID );
  }
  catch(e){
    alert(e.message);
  }  
}

]]>
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" style="background:transparent;" onLoad="initPage();">

<DIV id="CNScrollTable">
<TABLE unselectable="on" id="tblHead" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSort1')" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
  <TBODY>
    <TR unselectable="on" class="QueueHeader">
      <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="99" width="52" nowrap="nowrap" style="height:20; cursor:default;"> Distance </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" width="238" nowrap="nowrap" style="cursor:default;"> Name </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" nowrap="nowrap" style="cursor:default;width=190;"> Address </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" width="126" nowrap="nowrap" style="cursor:default;"> City </TD>
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" width="70" style="cursor:default;"> Zip </TD>
      <!--<TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" width="46" style="cursor:default;"> CF </TD>-->
      <TD unselectable="on" class="TableSortHeader" type="CaseInsensitiveString" sIndex="99" width="65" style="cursor:default;"> Program </TD>
    </TR>
  </TBODY>
</TABLE>

<DIV unselectable="on" id="divResultList" class="autoflowTable" style="height:365px;">
  <TABLE unselectable="on" id="tblSort1" class="GridTypeTable" border="0" cellspacing="1" cellpadding="0" style="table-layout:fixed;">
    <TBODY bgColor1="ffffff" bgColor2="fff7e5">
      <xsl:for-each select="Shop" >
	     <xsl:sort select="Distance" data-type="number" order = "ascending"/>
	      <xsl:call-template name="SearchResults"> </xsl:call-template>
      </xsl:for-each>
    </TBODY>
  </TABLE>
</DIV>
</DIV>
</BODY>
</HTML>
</xsl:template>

  <!-- Gets the search results -->
  <xsl:template name="SearchResults">
    
	<TR unselectable="on" onMouseOut="GridMouseOut(this)" onMouseOver="GridMouseOver(this)" onclick="GridSelect(this)">
      <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
    
	  <TD unselectable="on" class="GridTypeTD" width="50" style="text-align:left">
       <xsl:choose>
          <xsl:when test="@Distance=''" >
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@Distance"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
	  
      <TD unselectable="on" width="237" class="GridTypeTD" style="text-align:left">
       <xsl:choose>
          <xsl:when test="@Name = ''" >
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:when test="string-length(@Name) > 32" >
            <xsl:value-of select="substring(@Name,1,32)"/><xsl:text>...</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@Name"/>
          </xsl:otherwise>
        </xsl:choose>	
      </TD>
	  
      <TD unselectable="on" class="GridTypeTD" style="text-align:left;width:190;">
      <xsl:choose>
          <xsl:when test="@Address1 = ''" >
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:when test="string-length(@Address1) > 24" >
            <xsl:value-of select="substring(@Address1,1,24)"/><xsl:text>., </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@Address1"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
     
	  <TD unselectable="on" width="125" class="GridTypeTD" style="text-align:left">
    	<xsl:choose>
          <xsl:when test = "@AddressCity != ''"><xsl:value-of select="@AddressCity"/></xsl:when>
    			<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
    		</xsl:choose>
        <xsl:if test="@AddressCity != '' and @AddressState != ''">,<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:if>
        <xsl:choose>
    			<xsl:when test = "normalize-space(@AddressState) != ''"><xsl:value-of select="@AddressState"/></xsl:when>
    			<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
    		</xsl:choose>	
      </TD>
	  
      <TD unselectable="on" width="68" class="GridTypeTD">
        <xsl:choose>
    			<xsl:when test = "normalize-space(@AddressZip) != ''"><xsl:value-of select="@AddressZip"/></xsl:when>
    			<xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:otherwise>
    		</xsl:choose>  
      </TD>
	  
	  
      <!--<TD unselectable="on" width="45" class="GridTypeTD">
        <xsl:choose>
       			<xsl:when test = "@CertifiedFirstFlag = 1">Yes</xsl:when>
      			<xsl:otherwise>No</xsl:otherwise>
      	</xsl:choose>
      </TD>-->

	  <!--<TD unselectable="on" width="63" class="GridTypeTD" align="center">
        <xsl:choose>
          <xsl:when test="@ShopType = 'Program'">
           <img src="/images/cmark.gif" alt="" width="11" height="11" border="0" title="Program Shop" />
          </xsl:when>
          <xsl:when test="@ShopType = 'CEI'">CEI</xsl:when>
          <xsl:when test="@ShopType != 'Program'">
           <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
        </xsl:choose>    
      </TD>  -->

	  <TD unselectable="on" width="63" class="GridTypeTD" align="center">
		<xsl:value-of select="@ShopType"/>
      </TD>  

			
	  <TD style="display:none">
        <xsl:value-of select="@ShopLocationID"/>
      </TD>

    </TR>

  </xsl:template>

</xsl:stylesheet>