<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="UserDetail">

<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:decimal-format NaN="0"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="InsertMode"/>
<xsl:param name="LoginUserID"/>
<xsl:param name="UserProfileCRUD" select="User Profile"/>

<xsl:template match="/Root">

    <xsl:choose>
        <xsl:when test="(substring($UserProfileCRUD, 2, 1) = 'R') or ((/Root/@UserID = '-1') and (substring($UserProfileCRUD, 1, 1) = 'C'))">
            <xsl:call-template name="mainHTML">
             <xsl:with-param name="UserProfileCRUD" select="$UserProfileCRUD"/>
            </xsl:call-template>
        </xsl:when>
     <xsl:otherwise>
          <html>
          <head>
              <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
              <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
          </head>
          <body class="bodyAPDSub" unselectable="on" style="background-color:#FFFFFF">
            <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%">
              <tr>
                <td align="center">
                  <font color="#ff0000"><strong>You do not have sufficient permission to add/view User detail. 
                  <br/>Please contact your supervisor.</strong></font>
                </td>
              </tr>
            </table>
          </body>
          </html>
        </xsl:otherwise>
    </xsl:choose>
	    
</xsl:template>

<xsl:template name="mainHTML">
  <xsl:param name="UserProfileCRUD"/>
  <xsl:variable name="ProfileReadOnly" select="boolean(substring($UserProfileCRUD, 3, 1) != 'U')"/>
  <xsl:variable name="APDAppID"><xsl:value-of select="/Root/Reference[@List='Application' and @Name='APD']/@ReferenceID"/></xsl:variable>
 

<html>
<title>User Information</title>
<head>
<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/CoolSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>

<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>

<!-- Script modules -->
<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formutilities.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/utilities.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,32);
		  event.returnValue=false;
		  };	
</script>

<script language="JavaScript">
    var gsCRUD = "<xsl:value-of select="/Root/@CRUDUserProfile"/>";
    var gsCRUDUserProfile = "<xsl:value-of select="$UserProfileCRUD"/>";
    var gsLoginUserID = "<xsl:value-of select="$LoginUserID"/>";
    var gsUserLastUpdatedDate = "<xsl:value-of select="/Root/User/@SysLastUpdatedDate"/>";
    var gbInsertMode = <xsl:value-of select="$InsertMode"/>
    var gbLicenseStateCount = <xsl:value-of select="count(/Root/User/States)"/>;
    var bResetAllOverride = false;    
    var gsStateLicenseRequired = new Array(<xsl:for-each select="/Root/User/States"><xsl:value-of select="@AnalystLicenseRequired"/><xsl:if test="position() != last()">, </xsl:if></xsl:for-each>);
    //var gsStateLicenseRequired = new Array(<xsl:for-each select="/Root/User/States"><xsl:value-of select="@LicenseRequired"/><xsl:if test="position() != last()">, </xsl:if></xsl:for-each>);
    var gsStateCodes = new Array(<xsl:for-each select="/Root/User/States">"<xsl:value-of select="@StateCode"/>"<xsl:if test="position() != last()">, </xsl:if></xsl:for-each>);
    var gsNoLicReqStCount = <xsl:value-of select="count(/Root/User/States[@AssignWorkFlag=1 and (@AnalystLicenseRequired=0 and @OwnerLicenseRequired=0)])"/>;
    <!-- var gsNoLicReqStCount = <xsl:value-of select="count(/Root/User/States[@AssignWorkFlag=1 and @LicenseRequired=0])"/>; -->
    var gsNoLicReqStCount2 = gsNoLicReqStCount;
    var sAppID = "<xsl:value-of select="Reference[@List='Application' and @Name='APD']/@ReferenceID"/>";
    var sAppSysLastUpdatedDate = "<xsl:value-of select="User/Application[@ApplicationID=$APDAppID]/@SysLastUpdatedDate"/>";
    var gsInsCoCount = "<xsl:value-of select='count(/Root/Reference[@List="InsuranceCompany"])'/>";
    var gsInsAssignWork = new Array(<xsl:for-each select="/Root/Reference[@List='InsuranceCompany']"><xsl:variable name="InsuranceCompanyID" select="@ReferenceID"></xsl:variable><xsl:choose><xsl:when test="/Root/User/Insurance[@InsuranceCompanyID=$InsuranceCompanyID]">"1"</xsl:when><xsl:otherwise>"0"</xsl:otherwise></xsl:choose><xsl:if test="position() != last()">, </xsl:if></xsl:for-each>);
    var gsInsAssignWorkCount = <xsl:value-of select="count(/Root/Reference[@List='InsuranceCompany'])"/>;
    var gsInsAssignWorkCount2 = <xsl:value-of select="count(/Root/User/Insurance)"/>;
    var gsInsCoIDs = new Array(<xsl:for-each select="/Root/Reference[@List='InsuranceCompany']"><xsl:value-of select="@ReferenceID"/><xsl:if test="position() != last()">, </xsl:if></xsl:for-each>);
    var gsUser16Profile = "<xsl:value-of select="/Root/User/Profile[@OverriddenFlag=1 and @ProfileID=16]/@ProfileValue"/>";
    var gsUser17Profile = "<xsl:value-of select="/Root/User/Profile[@OverriddenFlag=1 and @ProfileID=17]/@ProfileValue"/>";
    var gsUser18Profile = "<xsl:value-of select="/Root/User/Profile[@OverriddenFlag=1 and @ProfileID=18]/@ProfileValue"/>";
	
    
<![CDATA[

    var bRolesChanged = false; 
    var bAssignmentPoolChanged = false;
    var doc = document.all;
    var currentTabRow = 1;
    var curTabID = "";
    var tabArray = null;
    var tabsHIndend = 10;
    var tabsVIndend = 18;
  

  // Page Initialize
  function PageInit()
  {
    try
    {
      InitTabs();
      initSelectBoxes();  
      InitFields();  
      onpasteAttachEvents();        
      changePage(tab11);   
      initDateDefault();
      initOverrideCBDefault();
      if(window.parent.crudDivUsers)
        window.parent.crudDivUsers.style.display="";
    }
    catch(e)
    { }   
  }
    
      
  function initDateDefault()
  {
    document.getElementById("__AssignmentBeginDate").value = document.getElementById("AssignmentBeginDate").value;
    document.getElementById("__AssignmentEndDate").value = document.getElementById("AssignmentEndDate").value;
  }


  function initOverrideCBDefault()
  {
    document.getElementById("__chkAllInsurance").value = document.getElementById("chkAllInsurance").value;
    document.getElementById("__chkStateNotLicensed").value = document.getElementById("chkStateNotLicensed").value;
  }


  function tabBeforeChange(obj, tab)
  {
    var relatedTab = obj
    if (obj == tab) return;
  }

  
  function handleJSError(sFunction, e)
  {
    var result = "An error has occurred on the page.\nFunction = " + sFunction + "\n";
    for (var i in e) {
      result += "e." + i + " = " + e[i] + "\n";
    }
    ClientError( result );
  }
	
	
  var inADS_Pressed = false;		// re-entry flagging for buttons pressed
  var objDivWithButtons = null;
	// Handle click of Save button
	function ADS_Pressed(sAction, sName)
	{
    try
	 {
	    if (document.readyState != "complete") return false;
		
        if (sAction == "Update" && !gbDirtyFlag) return; //no change made to the page
		
		 var aRetValue = SaveData(sAction);        
	     if (!aRetValue) return false; 
		
		
  		if (inADS_Pressed == true) return;
  		inADS_Pressed = true;
      
        objDivWithButtons = window.parent.crudDivUsers;
        disableADS(objDivWithButtons, true);

  		var oDiv = document.getElementById(sName);
  		if (sAction == "Update") 
		{  
		    
          if (aRetValue != null)
		  {
            if (aRetValue[1] != 0) 
		    {
              var sReturnXML = aRetValue[0];
              var sRetUserID, sRetLastUpdatedDate;
              if (sReturnXML.substr(0, 6) == "<Root>" ) 
			  {
               var iStartIdx = sReturnXML.indexOf("UserID=");
               sRetUserID = sReturnXML.substring( iStartIdx + 8, sReturnXML.indexOf("\"", iStartIdx + 8))       
               iStartIdx = sReturnXML.indexOf("SysLastUpdatedDate=", iStartIdx);
               sRetLastUpdatedDate = sReturnXML.substring( iStartIdx + 20, sReturnXML.indexOf("\"", iStartIdx + 20))
              } // end of inner if(3)
			  
		     //defect #879 - refresh the page if the role membership changed.
              if (bRolesChanged || bAssignmentPoolChanged) {
               window.parent.navigate("/APDWorkAssignment.asp?sel=" + document.all["UserID"].value);
               return;
              }
              else
			  { 
      		     gbDirtyFlag = false;
          		 if (typeof(parent.gbDirtyFlag)=='boolean') parent.gbDirtyFlag = false;			 
                 resetControlBorders(document.getElementById("content11"));
                 var objCurRow = window.parent.curRow;
                 if (objCurRow) {
                  objCurRow.cells[0].innerText = document.all["__CSRNo"].value;
                  objCurRow.cells[1].innerText = document.all["NameLast"].value + ", " + document.all["NameFirst"].value;
				 }
              } // end of else part
		    } // end of aRetValue[1]!=0 if statement
          else
		  {
		  
             // If this code executes, most likely an error was raised in the middle tier or the database
             ServerEvent();
          }// end of else part
        } // end of 'aRetValue!=null'  if statement
      } // end of 'sAction=update' if statement
      else
	    {
          var userError = new Object();
          userError.message = "Unhandled ADS action: Action = " + sAction;
          userError.name = "MiscellaneousException";
          throw userError;
		}
  	    inADS_Pressed = false;
        disableADS(objDivWithButtons, false);
        objDivWithButtons = null;
    } // end of try statement
    catch(e)
	  {
       handleJSError("ADS_Pressed", e);
      } // end of catch
	} // end of function

	
	     
	function SaveData(sAction)
	{
    try {
      var sAssignmentBeginDate = "";
      var sAssignmentEndDate = "";
     
	    if (!ValidatePage()){
        return false;
      } 

      if (!gbInsertMode) // Begin Save
      {
        if (document.getElementById("AssignmentBeginDate").value == "01/01/1900 00:00:00 AM")
          document.getElementById("AssignmentBeginDate").value = "";
        
        if (document.getElementById("AssignmentEndDate").value == "01/01/1900 00:00:00 AM")
          document.getElementById("AssignmentEndDate").value = "";
        
        sAssignmentBeginDate = document.getElementById("AssignmentBeginDate").value;
        sAssignmentEndDate = document.getElementById("AssignmentEndDate").value;
      }

      var sRequest = "";
      var sProcName = "";
      var bLicenseState = false;
      var bInsAssignWork = false;
      var sLicenseState = "";
      var sInsAssignWork = "";
      var objLicenseState;
      var objAssignWork;

      if (!gbInsertMode)// Begin Save
      {
        for(idx=1; idx <= gbLicenseStateCount; idx++)
        {
          objLicenseState = document.getElementById("chkStateLicensed_" + idx);
          objAssignWork = document.getElementById("chkStateAssignWork_" + idx);
          if (objLicenseState && objAssignWork)
          {
            if (objLicenseState.value != "0" || objAssignWork.value != "0")
            {
              bLicenseState = true;
              //sLicenseState += objLicenseState.value + ",";
              sLicenseState += gsStateCodes[idx-1] + "," + 
              (objLicenseState.value != "0" ? "1" : "0") + "," +
              (objAssignWork.value != "0" ? "1" : "0") + ";";
            }
          }
        }
        if (bLicenseState)
        {
          sLicenseState = sLicenseState.substring(0, sLicenseState.length - 1);
        }
      
        for (idx=1; idx <= gsInsCoCount; idx++)// Get list of Insurance Company IDs from which this Rep may be assigned work.
        {
          var objInsAssignWork = document.getElementById("chkInsAssignWork_" + idx);  
          if (objInsAssignWork)
          {
            if (objInsAssignWork.value != "0")
            {
              bInsAssignWork = true;
              sInsAssignWork += gsInsCoIDs[idx-1] + ";";
            }
          }
        }
        if (bInsAssignWork)
          sInsAssignWork = sInsAssignWork.substring(0, sInsAssignWork.length - 1);//Strip off semi-colon and close parameter
      } // end of if(!gbInsertMode) statement
     

     // User Profile Overrides
      var sProfileList2 = "";
      var aNewProf = new Array();
      var sProfileList = "";
      var sProfile = "";
      if (!gbInsertMode) //Begin Save
      {
        // Step through Profile Fields
        var bProfileSelected = false;
        
        if (!gbInsertMode) //no profile stuff in new user
        { 
          var objProfileRows = tblProfile.rows;
          var iProfileCount = objProfileRows.length;
          for (var i = 0; i < iProfileCount; i++)
          {
            if (objProfileRows[i].cells.length > 1)
            {
              var iProfileID = objProfileRows[i].cells[objProfileRows[i].cells.length - 1].innerText;
              sProfile = "";
              var objProfileOverrides = document.getElementById("chkProfileOverridden_" + iProfileID);
              if (objProfileOverrides)
              {
                if (objProfileOverrides.value > 0)
                {
                  bProfileSelected = true;
                  var objTxt = document.getElementById("ProfileValue" + iProfileID);
                  var sValue = objTxt.value;
                  if (sValue == "Yes" || sValue == "No")
                    sValue = (sValue == "Yes" ? 1 : 0);
                  sProfile = objProfileOverrides.value + "," + sValue;
                  aNewProf.push(sProfile);
                  sProfileList += sProfile + ","; 
                  sProfileList2 += sProfile + ";";
                }
              }
            }
          }
          
          if (bProfileSelected) 
          {
          //Strip off final comma and close parameter
          sProfileList = sProfileList.substring(0, sProfileList.length - 1);
          }
        }
      }
	  
	  
      // Work Assignment Pools
      var sAssignmentPoolList = "";
      if (!gbInsertMode){
          var objIdx;
          var nPools = tblAssignmentPool.rows.length;
          for (var idx=0; idx < nPools; idx++) {
            var iPoolID = tblAssignmentPool.rows[idx].cells[3].innerText;
              var colPools = document.getElementById("chkWorkPool_" + iPoolID);
              if (colPools){
                  if (colPools.value > 0) {
                      sAssignmentPoolList += colPools.value.Trim() + ",";
                  }
              }
          }
          //Strip off final comma and close parameter
          sAssignmentPoolList = sAssignmentPoolList.substring(0, sAssignmentPoolList.length - 1);
      }


      //Generate a log Comment if the security items changed.
      var sUserID = document.all["UserID"].value;
      var logComment = "";

      // Assignment End Date changes
      var sAssgnEndDateComments = "";
      if (sAssignmentEndDate != document.all["__AssignmentEndDate"].value)
      {
        if (document.all["__AssignmentEndDate"].value.Trim() == "")
          sAssgnEndDateComments = "User id: " + sUserID + " - Stop Assignment Date set to " + sAssignmentEndDate + ".\n";
        else
          sAssgnEndDateComments = "User id: " + sUserID + " - Stop Assignment Date changed from " + document.all["__AssignmentEndDate"].value + " to " + sAssignmentEndDate + ".\n";
      }

      if (sAssgnEndDateComments != "")
        logComment += sAssgnEndDateComments;


      // Assignment Begin Date changes
      var sAssgnBeginDateComments = "";
      if (sAssignmentBeginDate != document.all["__AssignmentBeginDate"].value)
      {
        if (document.all["__AssignmentBeginDate"].value.Trim() == "")
          sAssgnBeginDateComments = "User id: " + sUserID + " - Restart Assignment Date set to " + sAssignmentBeginDate + ".\n";
        else
          sAssgnBeginDateComments = "User id: " + sUserID + " - Restart Assignment Date changed from " + document.all["__AssignmentBeginDate"].value + " to " + sAssignmentBeginDate + ".\n";
      }

      if (sAssgnBeginDateComments != "")
        logComment += sAssgnBeginDateComments;


      // Work Schedule Hours changes
      var sWorkScheduleComments = "";
      
      sWorkScheduleComments += getScheduleChanges("Monday", sUserID);
      sWorkScheduleComments += getScheduleChanges("Tuesday", sUserID);
      sWorkScheduleComments += getScheduleChanges("Wednesday", sUserID);
      sWorkScheduleComments += getScheduleChanges("Thursday", sUserID);
      sWorkScheduleComments += getScheduleChanges("Friday", sUserID);
      sWorkScheduleComments += getScheduleChanges("Saturday", sUserID);
      sWorkScheduleComments += getScheduleChanges("Sunday", sUserID);
        
      if (sWorkScheduleComments != "")
        logComment += sWorkScheduleComments;


      // Insurance Company changes
      var sInsComments = "";
      if (sInsAssignWork != document.all["__InsuranceCo"].value)
      {
        var oldInsCo = document.all["__InsuranceCo"].value.split(";");
        var newInsCo = sInsAssignWork.split(";");
        var bFound = false;
        var iLength = newInsCo.length;
        
        //check for any new additions
        if (newInsCo.length > 0)
        {
          for (var i = 0; i < iLength; i++)
          {
            bFound = true;
            var jLength = oldInsCo.length;
            for (var j = 0; j < jLength; j++)
            {
              if (newInsCo[i] != oldInsCo[j])
              {
                bFound = false;
                break;
              }
            }
            if (!bFound)
              sInsComments += "User id: " + sUserID + " - Added Insurance Company: " + getInsCoText(newInsCo[i]) + "\n";
          }
        }
        
        //check for any deletions
        var iLength = oldInsCo.length;
        for (var i = 0; i < iLength; i++)
        {
          bFound = false;
          var jLength = newInsCo.length;
          for (var j = 0; j < jLength; j++)
          {
            if (oldInsCo[i] == newInsCo[j])
            {
              bFound = true;
              break;
            }
          }
          if (!bFound)
            sInsComments += "User id: " + sUserID + " - Removed Insurance Company: " + getInsCoText(oldInsCo[i]) + "\n";
        }
      }

      if (document.getElementById("__chkAllInsurance").value != document.getElementById("chkAllInsurance").value)
      {
        var bCBAllInsurance = false;
        if (document.getElementById("chkAllInsurance").value == 1)
          bCBAllInsurance = true;

        sInsComments += "User id: " + sUserID + " - Assign work from all insurance companies checkbox was " +
                                                    (bWorkAssignStatus ? "set" : "removed") + ".\n";
      }

      if (sInsComments != "")
        logComment += sInsComments;

      
      // State changes
      var sStateComments = "";
      if (sLicenseState != document.all["__States"].value)
      {
        var aOldStates = document.all["__States"].value.split(";");
        var aNewState = sLicenseState.split(";");
        var oldStateID, newStateID;
        var aOld, aNew;

        if (sLicenseState != "")
        {
          var iLength = aNewState.length;
          //check for any states changed or added
          for (var i = 0; i < iLength; i++)
          {
            var bNewStateFound = false;
            aNew = aNewState[i].split(",");
            newStateID = aNew[0];
            var jLength = aOldStates.length;
            for (var j = 0; j < jLength; j++)
            {
              aOld = aOldStates[j].split(",");
              oldStateID = aOld[0];
              if (newStateID == oldStateID)       // found matching state
              {
                if (aNew[1] != aOld[1])         //License has been removed or added
                {
                  var bLicenseStatus = false;   //License removed by default
                  if (aNew[1] == 1)             //License has been added 
                    bLicenseStatus = true;
          
                  sStateComments += "User id: " + sUserID + " - Licensed for " + getStateText(newStateID) + " was " +
                                    (bLicenseStatus ? "added" : "removed") + ".\n";
                }
          
                if (aNew[2] != aOld[2])         //Work Assign has been removed or added
                {
                  var bWorkAssignStatus = false; //Work Assign removed by default
                  if (aNew[1] == 1)             //Work Assign has been added 
                    bWorkAssignStatus = true;
          
                  sStateComments += "User id: " + sUserID + " - Work Assign for " + getStateText(newStateID) + " was " +
                                    (bWorkAssignStatus ? "added" : "removed") + ".\n";
                }
                bNewStateFound = false;
                break;
              }
              else
              {
                bNewStateFound = true;
              }
            }
          
            if (bNewStateFound)             // found new state
            {
              sStateComments += "User id: " + sUserID + " - State added - " + getStateText(newStateID) + " - " +
                                "Licensed:" + (aNew[1] == 1 ? "checked" : "not checked") + " and Work Assign:" +(aNew[2] == 1 ? "checked" : "not checked") + ". \n";
            }
          }
        }
        
        //check for any states removed
        if (aOldStates != "")
        {
          var jLength = aOldStates.length;
          for (var j = 0; j < jLength; j++)
          {
            var bStateFound = false;
            aOld = aOldStates[j].split(",");
            oldStateID = aOld[0];
            var iLength = aNewState.length;
            for (var i = 0; i < iLength; i++)
            {
              aNew = aNewState[i].split(",");
              newStateID = aNew[0];
              if (oldStateID == newStateID)       // found matching state
              {
                bStateFound = true;
                break;
              }
            }
            if (!bStateFound)
              sStateComments += "User id: " + sUserID + " - State removed - " + getStateText(oldStateID) + ".\n"
          }
        }
      }
      

      if (document.getElementById("__chkStateNotLicensed").value != document.getElementById("chkStateNotLicensed").value)
      {
        var bCbAllStates = false;
        if (document.getElementById("chkStateNotLicensed").value == 1)
          bCbAllStates = true;

        sStateComments += "User id: " + sUserID + " - Assign work from all states with no license required checkbox was " +
                                                    (bCbAllStates ? "set" : "removed") + ".\n";
      }

      if (sStateComments != "")
        logComment += sStateComments;


      //Profile changes
      var sProfComments = "";
      var aOldProf = document.all["__Profile"].value.split(";");
      var bProfileChanged = false;
      var bFound = false;
      var oldProfID, newProfID;
      var aOld, aNew;
      var sProfComments = "";
      var iLength = aNewProf.length;
      //check for any new overrides
      for (var i = 0; i < iLength; i++){
        bFound = false;
        aNew = aNewProf[i].split(",");
        newProfID = aNew[0];
        var jLength = aOldProf.length;
        for (var j = 0; j < jLength; j++){
          aOld = aOldProf[j].split(",");
          oldProfID = aOld[0];
          if (newProfID == oldProfID) {
            //check if the permissions changed
            if (aNew[1] != aOld[1]) {
              sProfComments += "User id: " + sUserID + " - Profile Setting for \"" + getProfileText(newProfID) + "\" was changed from: " + aOld[1] +
                                                                                   " to " +
                                                                                   aNew[1] + "\n";
              bProfileChanged = true;
            }
            bFound = true;
            break;
          }
        }
        if (!bFound){
          sProfComments += "User id: " + sUserID + " - Added Profile Override for \"" + getProfileText(newProfID) + "\" : " + aNew[1] + "\n";
          bProfileChanged = true;
        }
      }
      var iLength = aOldProf.length;
      //check for any deletion of overrides
      for (var i = 0; i < iLength; i++){
        bFound = false;
        aOld = aOldProf[i].split(",");
        oldProfID = aOld[0];
        var jLength = aNewProf.length;
        for (var j = 0; j < jLength; j++){
          aNew = aNewProf[j].split(",");
          newProfID = aNew[0];
          if (oldProfID == newProfID) {
            bFound = true;
            break;
          }
        }
        if (!bFound && oldProfID != ""){
          if (!isNaN(oldProfID)){
            sProfComments += "User id: " + sUserID + " - Defaulted Profile Setting for \"" + getProfileText(oldProfID) + "\"\n";
          }
        }
      }
      
      if (sProfComments != "")
        logComment += sProfComments;


      //Work Assignment Pool changes
      var sWorkAssignComments = "";
      if (sAssignmentPoolList != document.all["__Pool"].value)
      {
        bAssignmentPoolChanged = true;
        var oldPool = document.all["__Pool"].value.split(",");
        var newPool = sAssignmentPoolList.split(",");
        var bFound = false;
        var iLength = newPool.length;
        
        //check for any new additions
        for (var i = 0; i < iLength; i++) {
          bFound = false;
          var jLength = oldPool.length;
          for (var j = 0; j < jLength; j++) {
            if (newPool[i] == oldPool[j]) {
              bFound = true;
              break;
            }
          }
          if (!bFound)
            sWorkAssignComments += "User id: " + sUserID + " - Added Assignment Pool: " + getWorkAssignmentText(newPool[i]) + "\n";
        }
        
        //check for any deletions
        var iLength = oldPool.length;
        for (var i = 0; i < iLength; i++) {
          bFound = false;
          var jLength = newPool.length;
          for (var j = 0; j < jLength; j++) {
            if (oldPool[i] == newPool[j]) {
              bFound = true;
              break;
            }
          }
          if (!bFound)
            sWorkAssignComments += "User id: " + sUserID + " - Removed Assignment Pool: " + getWorkAssignmentText(oldPool[i]) + "\n";
        }
      }

      if (sWorkAssignComments != "")
        logComment += sWorkAssignComments;

		  
		if(sProfileList == null || sProfileList =="") 
    {
		  if (gsUser16Profile != "" ) sProfileList = "16," + gsUser16Profile;
		  if (sProfileList !="" )
		  {
		    if(gsUser17Profile != "" ) sProfileList = sProfileList + ",17," + gsUser17Profile;
			}
		  else	if(gsUser17Profile != "" ) sProfileList = "17," + gsUser17Profile;
		  if (sProfileList !="" )
		  {
		    if(gsUser18Profile != "" ) sProfileList = sProfileList + ",18," + gsUser18Profile;
			}
		  else	if(gsUser18Profile != "" ) sProfileList = "18," + gsUser17Profile;
		}
	  else
    {
		  if (gsUser16Profile != "" ) sProfileList = sProfileList + ",16," + gsUser16Profile;
		  if (gsUser17Profile != "" ) sProfileList = sProfileList + ",17," + gsUser17Profile;
		  if (gsUser18Profile != "" ) sProfileList = sProfileList + ",18," + gsUser18Profile;
    }
		  
      sRequest = "UserID=" + document.all["UserID"].value + "&" +
                //   "OfficeID=" + "&" + // this should be empty for Lynx Employees
                "AssignmentBeginDate=" + sAssignmentBeginDate + "&" +
                "AssignmentEndDate=" + sAssignmentEndDate + "&" +
                "NameFirst=" + escape(document.all["NameFirst"].value) + "&" +
                "NameLast=" + escape(document.all["NameLast"].value) + "&" +
                "NameTitle=" + escape(document.all["NameTitle"].value) + "&" +
                "OperatingMondayStartTime=" + escape(document.all["OperatingMondayStartTime"].value) + "&" +
                "OperatingMondayEndTime=" + escape(document.all["OperatingMondayEndTime"].value) + "&" +
                "OperatingTuesdayStartTime=" + escape(document.all["OperatingTuesdayStartTime"].value) + "&" +
                "OperatingTuesdayEndTime=" + escape(document.all["OperatingTuesdayEndTime"].value) + "&" +
                "OperatingWednesdayStartTime=" + escape(document.all["OperatingWednesdayStartTime"].value) + "&" +
                "OperatingWednesdayEndTime=" + escape(document.all["OperatingWednesdayEndTime"].value) + "&" +
                "OperatingThursdayStartTime=" + escape(document.all["OperatingThursdayStartTime"].value) + "&" +
                "OperatingThursdayEndTime=" + escape(document.all["OperatingThursdayEndTime"].value) + "&" +
                "OperatingFridayStartTime=" + escape(document.all["OperatingFridayStartTime"].value) + "&" +
                "OperatingFridayEndTime=" + escape(document.all["OperatingFridayEndTime"].value) + "&" +
                "OperatingSaturdayStartTime=" + escape(document.all["OperatingSaturdayStartTime"].value) + "&" +
                "OperatingSaturdayEndTime=" + escape(document.all["OperatingSaturdayEndTime"].value) + "&" +
                "OperatingSundayStartTime=" + escape(document.all["OperatingSundayStartTime"].value) + "&" +
                "OperatingSundayEndTime=" + escape(document.all["OperatingSundayEndTime"].value) + "&" +		
                "ReceiveCCCOneAssignmentFlag=" + document.all["chkAssignCCCOneDirect1"].value + "&" +
                "LicenseAssignStList=" + sLicenseState + "&" +
                "InsAssignWorkList=" + sInsAssignWork + "&" +
                "ProfileList=" + sProfileList + "&" +
                "AssignmentPoolList=" + sAssignmentPoolList + "&" +
                "SysLastUserID=" + gsLoginUserID + "&" +
                "SysLastUpdatedDate=" + content11.LastUpdatedDate;
				
        var sProcName = "uspWorkAssignmentUpdDetail";
       
        var oReturn = RSExecute("/rs/UserManagementRS.asp", "RadExecute", sProcName, sRequest, logComment, gsLoginUserID);

      if (oReturn.status == -1) {
        ServerEvent();
        return;
  	  }
  	  else {
        aReturn = oReturn.return_value.split("||");
      	if (aReturn[1] == null || aReturn[1] == 0) {
          //SP returned a message (not an error)
          ServerEvent();
          return;
	   }
        
        //update the user's last updated date
        var sReturnXML = aReturn[0];
        var sRetUserID;
        if (sReturnXML.substr(0, 6) == "<Root>" ) {
          var iStartIdx = sReturnXML.indexOf("UserID=");
          sRetUserID = sReturnXML.substring( iStartIdx + 8, sReturnXML.indexOf("\"", iStartIdx + 8))

          var iStartIdx = sReturnXML.indexOf("SysLastUpdatedDate=", iStartIdx);
          sRetLastUpdatedDate = sReturnXML.substring( iStartIdx + 20, sReturnXML.indexOf("\"", iStartIdx + 20))

          var oMainDiv = document.getElementById("content11");

          if (oMainDiv)
            oMainDiv.setAttribute("LastUpdatedDate", sRetLastUpdatedDate);
            
        }
   
   
        if (sAction == "Update")
	    { 
          // update the application record
          sRequest = "UserID=" + sRetUserID + "&" +
                     "ApplicationID=" + sAppID + "&" +
                     "LogonId=" + escape(document.all["__CSRNo"].value) + "&" + 
                     "PasswordHint=&" +
                     "SysLastUserID=" + gsLoginUserID + "&" + 
                     "SysLastUpdatedDate=" + sAppSysLastUpdatedDate;
          var oReturn = null;
          oReturn = RSExecute("/rs/RSADSAction.asp", "RadExecute", "uspAdmUserApplicationUpdDetail", sRequest);
    
          if (oReturn.status == -1) 
		  {
            ServerEvent();
            return;
      	  }
		  else
		  {
            aReturn = oReturn.return_value.split("||");
          	if (aReturn[1] == null || aReturn[1] == 0)
			{
              //SP returned a message (not an error)
              ServerEvent();
              return;
    		}
          }

          var sReturnXML = aReturn[0];
          if (sReturnXML.substr(0, 6) == "<Root>" )
		  {
            var iStartIdx = sReturnXML.indexOf("SysLastUpdatedDate=");
            sAppSysLastUpdatedDate = sReturnXML.substring( iStartIdx + 20, sReturnXML.indexOf("\"", iStartIdx + 20))
          }
        }
        //reset the default place holders with the save data.
        document.all["__Profile"].value = sProfileList2;
        document.all["__Pool"].value = sAssignmentPoolList;
        return aReturn;
      }
 	  }
    catch(e) 
	{
      handleJSError("SaveData", e);
    }
  }

  
  function getScheduleChanges(weekday, userid)
  {
    var sWeekday = document.all["Operating"+weekday+"StartTime"].value +"-"+ document.all["Operating"+weekday+"EndTime"].value;
    if (document.all["__"+weekday+"Hours"].value != sWeekday)
    {
      if (document.all["__"+weekday+"Hours"].value == "-")
        return "User id: " + userid + " - "+weekday+" schedule changed to " + sWeekday + ".\n";
      else
        return "User id: " + userid + " - "+weekday+" schedule changed from " + document.all["__"+weekday+"Hours"].value + " to " + sWeekday + ".\n";
    }
    else
      return "";
  }
  
  
  function ChkMilitaryTime(tObj)
  {
	// Checks if time is valid Military time (no colon(:))
	var timeStr = String(tObj.value);
	if (timeStr == "") return true;
	var nums = '0123456789';
	var len = timeStr.length;

	if (len != 4){
        ClientWarning("This is not a valid Military time.  Must be in the form 'hhmm'.");
		tObj.select();
		return false;
	}

	var mins = timeStr.substr(len-2);
	var hrs = timeStr.substr(0, len-2);
	
    if (hrs < 0  || hrs > 24)
	{
        ClientWarning("Hour must be between 0 and 24 for military time");
		tObj.select();
		return false;
	}

	if (mins < 0 || mins > 59)
	{
       ClientWarning("Minute must be between 0 and 59.");
		tObj.select();
		return false;
	}

	if (hrs == 24 && mins != 0)
	{
       ClientWarning("2400 is the maximum value allowed for military time.");
		tObj.select();
		return false;
	}

	return true;
}

  
  
 function ValidatePage()
 {
  
   var MStimeObj = document.getElementById("OperatingMondayStartTime");
   if (!ChkMilitaryTime(MStimeObj)) return false;
   var MEtimeObj = document.getElementById("OperatingMondayEndTime");
   if (!ChkMilitaryTime(MEtimeObj)) return false;
   var TStimeObj = document.getElementById("OperatingTuesdayStartTime");
   if (!ChkMilitaryTime(TStimeObj)) return false;
   var TEtimeObj = document.getElementById("OperatingTuesdayEndTime");
   if ( !ChkMilitaryTime(TEtimeObj)) return false;
   var WStimeObj = document.getElementById("OperatingWednesdayStartTime");
   if (!ChkMilitaryTime(WStimeObj)) return false;
   var WEtimeObj = document.getElementById("OperatingWednesdayEndTime");
   if (!ChkMilitaryTime(WEtimeObj)) return false;
   var ThStimeObj = document.getElementById("OperatingThursdayStartTime");
   if (!ChkMilitaryTime(ThStimeObj)) return false;
   var ThEtimeObj = document.getElementById("OperatingThursdayEndTime");
   if (!ChkMilitaryTime(ThEtimeObj)) return false;
   var FStimeObj = document.getElementById("OperatingFridayStartTime");
   if (!ChkMilitaryTime(FStimeObj)) return false;
   var FEtimeObj = document.getElementById("OperatingFridayEndTime");
   if (!ChkMilitaryTime(FEtimeObj)) return false;
   var SStimeObj = document.getElementById("OperatingSaturdayStartTime");
   if (!ChkMilitaryTime(SStimeObj)) return false;
   var SEtimeObj = document.getElementById("OperatingSaturdayEndTime");
   if (!ChkMilitaryTime(SEtimeObj)) return false;
   var SuStimeObj = document.getElementById("OperatingSundayStartTime");
   if (!ChkMilitaryTime(SuStimeObj)) return false;
   var SuEtimeObj = document.getElementById("OperatingSundayEndTime");
   if (!ChkMilitaryTime(SuEtimeObj)) return false;
   
   var badHoursMsg = "The Hours of Operation Start time (left) must be less than the End time (right)."
      if ((MStimeObj.value != "" || MEtimeObj.value != "") &&
	    (MEtimeObj.value <= MStimeObj.value || MStimeObj.value == "")){
		  ClientWarning(badHoursMsg);
		  MStimeObj.select();
		  return false;
	  }

	  if ((TStimeObj.value != "" || TEtimeObj.value != "") &&
	    (TEtimeObj.value <= TStimeObj.value || TStimeObj.value == "")){
		  ClientWarning(badHoursMsg);
		  TStimeObj.select();
		  return false;
	  }

	  if ((WStimeObj.value != "" || WEtimeObj.value != "") &&
	    (WEtimeObj.value <= WStimeObj.value || WStimeObj.value == "")){
		  ClientWarning(badHoursMsg);
		  WStimeObj.select();
		  return false;
	  }

	  if ((ThStimeObj.value != "" || ThEtimeObj.value != "") &&
	    (ThEtimeObj.value <= ThStimeObj.value || ThStimeObj.value == "")){
		 ClientWarning(badHoursMsg);
		  ThStimeObj.select();
		  return false;
	  }

	  if ((FStimeObj.value != "" || FEtimeObj.value != "") &&
	    (FEtimeObj.value <= FStimeObj.value || FStimeObj.value == "")){
		  ClientWarning(badHoursMsg);
		  FStimeObj.select();
		  return false;
	  }

	  if ((SStimeObj.value != "" || SEtimeObj.value != "") &&
	    (SEtimeObj.value <= SStimeObj.value || SStimeObj.value == "")){
		  ClientWarning(badHoursMsg);
		  SStimeObj.select();
		  return false;
	  }

	  if ((SuStimeObj.value != "" || SuEtimeObj.value != "") &&
	    (SuEtimeObj.value <= SuStimeObj.value || SuStimeObj.value == "")){
		 ClientWarning(badHoursMsg);
		 SuStimeObj.select();
		  return false;
	  }
    return true; 
  }
  
  
  function getProfileText(sProfileID){
    if (tblProfile) {
      var iLength = tblProfile.rows.length;
      for (var i = 0; i < iLength; i++){
        if (tblProfile.rows[i].cells.length < 2) continue;
        if (tblProfile.rows[i].cells[4].innerText == sProfileID)
		{
          return tblProfile.rows[i].cells[1].innerText;
		}  
      }
    }
    return "";
  }

  function getWorkAssignmentText(sPoolID){
    if (tblAssignmentPool) {
      var iLength = tblAssignmentPool.rows.length;
      for (var i = 0; i < iLength; i++){
        if (tblAssignmentPool.rows[i].cells[3].innerText == sPoolID)
          return tblAssignmentPool.rows[i].cells[0].innerText;
      }
    }
    return "";
  }


  function getStateText(sStateID){
    if (tblState) {
      var iLength = tblState.rows.length;
      for (var i = 0; i < iLength; i++){
        if (tblState.rows[i].cells.length < 2) continue;
        if (tblState.rows[i].cells[4].innerText == sStateID)
          return tblState.rows[i].cells[0].innerText;
      }
    }
    return "";
  }


  function getInsCoText(sInsCoID){
    if (tblInsCo) {
      var iLength = tblInsCo.rows.length;
      for (var i = 0; i < iLength; i++){
        if (tblInsCo.rows[i].cells[2].innerText == sInsCoID)
          return tblInsCo.rows[i].cells[0].innerText;
      }
    }
    return "";
  }


  function myDivOnFocusIn(obj){
    if (gbInsertMode == false)
        DivOnFocusIn(content12);
        //DivOnFocusIn(obj);
  }
  
  
   
  function focusMyTab(){
    try {
      divStateLicensed.focus();
    } catch (e) {}
  }

    var tabsInited = false;
    function InitTabs(){
        var tmpTabsArray = document.all["tabsInTabs"].childNodes;
        tabArray = new Array();
        var iLength = tmpTabsArray.length;
        for (var i = 0; i < iLength; i++)
            if (tmpTabsArray[i].tagName == "DIV")
                tabArray.push(tmpTabsArray[i]);
            
        for (i = tabArray.length - 1; i > -1; i--){
            tabArray[i].style.top = (2 + tabsVIndend * (tabArray.length - i - 1)) + "px";
            tabArray[i].style.left = (3 + tabsHIndend * (i)) + "px";
            tabArray[i].style.visibility = "visible";
        }
        tabsInited = true;
    }

    function hoverTab(obj){
        if (obj.id != curTabID)
            obj.className = "tabhover1";
    }
    
    function hoverOff(obj){
        if (obj.id != curTabID)
            obj.className = "tab1";
    }


    function resetTabs(){
        var tabs;
        var tabContent = null;
        var iLength = tabArray.length;
        for (var i = 0; i < iLength; i++) {
            tabs = tabArray[i].getElementsByTagName("SPAN");
            var jLength = tabs.length;
            for (var j = 1; j < jLength; j++) {
                tabs[j].className = "tab1";
                tabContent = document.all[tabs[j].frm];
                if (tabContent)
                  tabContent.style.display = "none";
            }
        }
        curTabID = null;
    }    

    function changePage(obj){
        if (!tabsInited) return;
        if (curTabID == obj.id) return;

        resetTabs();
        obj.className = "tabactive1";
        curTabID = obj.id;

        var frm = obj.getAttribute("frm");
        var objFrm = null;
        if (frm.substr(0, 6) == "frame:") {
            objFrm = document.all[frm.substr(6)];
        }
        else
            objFrm = document.all[frm];

        if (objFrm) {
            objFrm.style.display = "inline";
        }
        if (frm.substr(0, 6) == "frame:"){
            var pg = obj.getAttribute("tabPage");
            var frmLoaded = objFrm.getAttribute("frmLoaded");

            //if (curInsuranceID && !frmLoaded) {
                //objFrm.src = pg + "?InsuranceID=" + curInsuranceID;
                //objFrm.frmLoaded = true;
            //}
        }
    }      
    
    function isDirty(){
      return gbDirtyFlag;
    }

   function showHideProfileGroup(GrpCode, tbl){
      if (tbl){
        var bShow = true;
        var sImgSrc = event.srcElement.src;
        if (sImgSrc.indexOf("plus.gif") != -1){
          event.srcElement.src = "/images/minus.gif";
          bShow = true;
        } else {
          event.srcElement.src = "/images/plus.gif";
          bShow = false;
        }
        var tblRows = tbl.rows;
        var tblRowsLen = tblRows.length;
        var sType = "";
        for (var i = 0; i < tblRowsLen; i++){
          if (tblRows[i].businessCode == GrpCode){
            sType = tblRows[i].getAttribute("DataTypeCD");
            if (bShow) {
              tblRows[i].cells[2].firstChild.style.display = "inline";
              tblRows[i].style.display = "inline";
            } else {
              tblRows[i].cells[2].firstChild.style.display = "none";
              tblRows[i].style.display = "none";
            }
          }
        }
      }
    }
    
    function checkProfileOverride(){
      var oTR = this.parentElement.parentElement;
      var sType = oTR.getAttribute("DataTypeCD");
      var iProfileID = oTR.lastChild.innerText;
      var sDefault = oTR.lastChild.previousSibling.innerText;
      var sValue = "";  
	 
	  eval("sValue=RBProfileValue" + iProfileID + ".value");
       
      if (this.lastChild.value > 0) {
          var objChkDiv = document.all["chkProfileOverriddenDiv"];
          if (objChkDiv && objChkDiv.lastChild.value == 0){
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
      }
      if (this.lastChild.value == 0 && sValue != sDefault){
        if (!bResetAllOverride)
          var sOverride = YesNoMessage("Reset to Default", "Do you want to reset profile settings for '" + this.parentElement.nextSibling.innerText + "' to default?");
        else
          var sOverride = "Yes";
          
  		  if (sOverride == "No"){
            CheckBoxChange(this.firstChild);
            return;
          }

		 if (sDefault == "Yes")
              eval("RBProfileValue" + iProfileID + ".change(0,'Yes')");
         else
              eval("RBProfileValue" + iProfileID + ".change(1,'No')");
	
      }
    }   
    
    function ProfileOverride(){
      try 
	    {
          if (this.disabled) return;
          var oTR = this.parentElement.parentElement;
        } 
		catch(e)
		{
          if (event.srcElement.disabled) return;
          var oTR = event.srcElement.parentElement.parentElement;
        }
       
	    var sType = oTR.getAttribute("DataTypeCD");
        var iProfileID = oTR.lastChild.innerText;

        var objChk = document.all["chkProfileOverridden_" + iProfileID];

        if (objChk)
		{

         if(objChk.value == 0)
		 {
          var objChkDiv = document.all["chkProfileOverridden_" + iProfileID + "Div"];
		
          if (objChkDiv)
		   {      
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
            gbDirtyFlag = true;
           }
      //     CheckBoxBlur(objChkDiv.firstChild);
          var objChkDiv = document.all["chkProfileOverriddenDiv"];
          if (objChkDiv && objChkDiv.lastChild.value == 0){
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        } 
      }
    }
    
     function defaultProfileOverride(){
      var objChk = document.all["chkProfileOverridden"];
      if (objChk){
        if(objChk.value == 0){
          var sOverride = YesNoMessage("Reset to Default", "Do you want to reset all profile settings to default?");
    			if (sOverride == "No"){
            CheckBoxChange(this.firstChild);
            return;
          } else {
            bResetAllOverride = true;
            var TRs = tblProfile.rows;
            var iTRsCount = TRs.length;
            for (var i = 0; i < iTRsCount; i++) {
              if (TRs[i].cells.length > 1) {
                var iProfileID = TRs[i].lastChild.innerText;
                var objChk1 = document.all["chkProfileOverridden_" + iProfileID];
                if (objChk1) {
                  if (objChk1.value > 0) {
                    var obj = document.all["chkProfileOverridden_" + iProfileID + "Div"];
                    if (obj)
					 {
                      CheckBoxChange(obj.firstChild);
                      CheckBoxBlur(obj.firstChild);
                      obj.fireEvent("onclick");
					 }
                  }
                }
              }
            }
            bResetAllOverride = false;
          }
        }
        else
          CheckBoxChange(document.all["chkProfileOverriddenDiv"].firstChild);
      }
    }    
    
   function selectAllNoLicenseRequired(){
      var obj = document.all["chkStateNotLicensed"];
      var iNewValue = 0;
      if (obj) {
        iNewValue = obj.value;
        for (var i = gsStateLicenseRequired.length; i > 0 ; i--) {
          var objChkDiv = document.all["chkStateAssignWork_" + i + "Div"];
          var objChk = document.all["chkStateAssignWork_" + i];
          if (objChkDiv && objChk.value != iNewValue && gsStateLicenseRequired[i-1] == 0) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
      }
    } 
        
    function selectAllInsCos(){
      var obj = document.all["chkAllInsurance"];
      var iNewValue = 0;
      if (obj) {
        iNewValue = obj.value;
        for (var i = gsInsAssignWork.length; i > 0 ; i--) {
          var objChkDiv = document.all["chkInsAssignWork_" + i + "Div"];
          var objChk = document.all["chkInsAssignWork_" + i];
          if (objChkDiv && ((objChk.value != iNewValue && gsInsAssignWork[i-1] == 0) || (objChk.value == 0))) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
      }
    }
    
    function resetAssignedWork(){
      try {
        document.all["cmdResetAssign"].disabled = true;
        for (var i = gsStateLicenseRequired.length; i > 0 ; i--) {
          var objChkDiv = document.all["chkStateAssignWork_" + i + "Div"];
          var objChk = document.all["chkStateAssignWork_" + i];
          if (objChkDiv && objChk.value == 1) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
        var objChkDiv = document.all["chkStateNotLicensedDiv"];
        var objChk = document.all["chkStateNotLicensed"];
        if (objChkDiv && objChk.value == 1) {
          CheckBoxChange(objChkDiv.firstChild);
          CheckBoxBlur(objChkDiv.firstChild);
        }
        gsNoLicReqStCount2 = 0;
      } catch (e) {}
      finally {
        document.all["cmdResetAssign"].disabled = false;
      }
    }

    function resetLicensedState(){
      try {
        document.all["cmdResetLicensed"].disabled = true;
        for (var i = gsStateLicenseRequired.length; i > 0 ; i--) {
          var objChkDiv = document.all["chkStateLicensed_" + i + "Div"];
          var objChk = document.all["chkStateLicensed_" + i];
          if (objChkDiv && objChk.value == 1) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
      } catch (e) {}
      finally {
        document.all["cmdResetLicensed"].disabled = false;
      }
    }
    
    function stateLicensedClicked(obj) {
      var idx = obj.index;
      if (idx > 0 && idx <= gsStateLicenseRequired.length) {
        if (gsStateLicenseRequired[idx-1] == 0) {
          var objChkDiv = document.all["chkStateLicensed_" + idx + "Div"];
          var objChk = document.all["chkStateLicensed_" + idx];
          if (objChkDiv && objChk.value == 1) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        } else {
          var objChkDiv = document.all["chkStateLicensed_" + idx + "Div"];
          var objChk = document.all["chkStateLicensed_" + idx];
          var objChkDiv2 = document.all["chkStateAssignWork_" + idx + "Div"];
          var objChk2 = document.all["chkStateAssignWork_" + idx];
          if (objChkDiv && objChk.value == 0 && objChk2.value == 1) {
            CheckBoxChange(objChkDiv2.firstChild);
            CheckBoxBlur(objChkDiv2.firstChild);
          }
          if (objChkDiv && objChk.value == 1 && objChk2.value == 0) {
            CheckBoxChange(objChkDiv2.firstChild);
            CheckBoxBlur(objChkDiv2.firstChild);
          }
        }
      }
    } 
    
    function resetInsAssignWork(){
      try {
        document.all["cmdResetInsAssignWork"].disabled = true;              
        for (var i = gsInsCoCount; i > 0 ; i--) {
          var objChkDiv = document.all["chkInsAssignWork_" + i + "Div"];
          var objChk = document.all["chkInsAssignWork_" + i];
          if (objChkDiv && objChk.value == 1) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
      } catch (e) {}
      finally {
        document.all["cmdResetInsAssignWork"].disabled = false;
      }    
    } 
    
    
    function InsAssignWorkClicked(obj){
      var idx = obj.index;
          
      var objChk = document.all["chkInsAssignWork_" + idx];
      if (objChk){ 
        if (objChk.value == 0)
          gsInsAssignWorkCount2--;
        else
          gsInsAssignWorkCount2++;
                  
        if (gsInsAssignWorkCount2 != gsInsAssignWorkCount) {
          var objChkDiv = document.all["chkAllInsuranceDiv"];
          var objChk = document.all["chkAllInsurance"];
          if (objChkDiv && objChk.value == 1) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        } else {
          var objChkDiv = document.all["chkAllInsuranceDiv"];
          var objChk = document.all["chkAllInsurance"];
          if (objChkDiv && objChk.value == 0) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
      }    
    }
    
   function assignWorkClicked(obj) {
      var idx = obj.index;

      var objChkDiv = document.all["chkStateLicensed_" + idx + "Div"];
      var objChk = document.all["chkStateLicensed_" + idx];
      if (objChkDiv && objChk.value == 0 && gsStateLicenseRequired[idx-1] == 1) {
        ClientWarning("Cannot mark assign work when the user is not licensed for that state and the state requires licensing.");
        var objChk = document.all["chkStateAssignWork_" + idx + "Div"];
        if (objChk) {
          CheckBoxChange(objChk.firstChild);
          CheckBoxBlur(objChk.firstChild);
        }
        return;
      }

      var objChk = document.all["chkStateAssignWork_" + idx];
      if (objChk && gsStateLicenseRequired[idx-1] == 0) {
        if (objChk.value == 0)
          gsNoLicReqStCount2--;
        else
          gsNoLicReqStCount2++;
          
        if (gsNoLicReqStCount2 != gsNoLicReqStCount) {
          var objChkDiv = document.all["chkStateNotLicensedDiv"];
          var objChk = document.all["chkStateNotLicensed"];
          if (objChkDiv && objChk.value == 1) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        } else {
          var objChkDiv = document.all["chkStateNotLicensedDiv"];
          var objChk = document.all["chkStateNotLicensed"];
          if (objChkDiv && objChk.value == 0) {
            CheckBoxChange(objChkDiv.firstChild);
            CheckBoxBlur(objChkDiv.firstChild);
          }
        }
      }
    }

]]>
</script>
</head>
<BODY unselectable="on" class="bodyAPDSub" onLoad="PageInit();" style="background-color:#FFFFFF;margin:0px;padding:0px;overflow:hidden;height:100%;width:100%" tabIndex="-1"> <!--SetTabVisibility(); -->
    <form name="frmUserDetail" method="post" action="">
        <SPAN id="content11" name="content11" style="position:relative; top: 0px; left: 0px;width:100%;height:230px;padding:5px;margin:0px;">
          <xsl:attribute name="LastUpdatedDate">
            <xsl:value-of select="/Root/User/@SysLastUpdatedDate"/>
          </xsl:attribute>
            <DIV unselectable="on" style="z-index:1;" name="tabsInTabs" id="tabsInTabs">
                <DIV id="tabs1" unselectable="on" class="apdtabs" style="position:relative;">
                    <SPAN id="tab10" class="" unselectable="on" tabPage="" style="width:0px;"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</SPAN>
                    <SPAN id="tab11" class="tab1" unselectable="on" frm="divUsrDetail" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Details</SPAN>
                    <SPAN id="tab12" class="tab1" unselectable="on" frm="divInsuranceCompanies" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Insurance Companies</SPAN>
                    <SPAN id="tab13" class="tab1" unselectable="on" frm="divLicensedStates" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">States</SPAN>
                    <SPAN id="tab14" class="tab1" unselectable="on" frm="divUsrProfile" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Profile Override</SPAN>
                    <SPAN id="tab15" class="tab1" unselectable="on" frm="divUsrAssignmentPool" onclick="changePage(this)" onmouseover="hoverTab(this)" onmouseout="hoverOff(this)">Work Assignment Pools</SPAN>
                </DIV>
            </DIV>
            
          <DIV name="divUsrDetail" id="divUsrDetail" style="border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
              <xsl:call-template name="tabGeneral">
			    <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
              </xsl:call-template>
          </DIV>

          <DIV name="divInsuranceCompanies" id="divInsuranceCompanies" style="display:none;border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
              <xsl:call-template name="InsuranceCompanies">
			    <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
			  </xsl:call-template>
          </DIV>

          <DIV name="divLicensedStates" id="divLicensedStates" style="display:none;border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
              <xsl:call-template name="LicStates">
			    <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
              </xsl:call-template>
          </DIV>

          <DIV name="divUsrProfile" id="divUsrProfile" style="display:none;border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
              <xsl:call-template name="profileOverrides">
		        <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
              </xsl:call-template>
          </DIV>

          <DIV name="divUsrAssignmentPool" id="divUsrAssignmentPool" SRC="/blank.asp" style="display:none;border:1px solid gray;position:relative;height:240px;width:100%;top:0px;left:0px;overflow:hidden">
              <xsl:call-template name="AssignmentPool">
                <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
              </xsl:call-template>
          </DIV>

        </SPAN>
    </form>
    <xsl:call-template name="pageDefaults"></xsl:call-template>
</BODY>
</html>
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
</xsl:template>

<xsl:template name="tabGeneral">
  <xsl:param name="ProfileReadOnly"/>
  <xsl:variable name="APDAppID"><xsl:value-of select="/Root/Reference[@List='Application' and @Name='APD']/@ReferenceID"/></xsl:variable>
 
   <table border="0" style="margin:10px;overflow:auto;">
 	      
	    <tr valign="bottom"> 
            <td colspan="2">Name</td>
            <td colspan="3">
              <table cellspacing="0" border="0" cellpadding="0">
                <tr>
                  <td unselectable="on">
                      <xsl:value-of disable-output-escaping="yes" select="js:InputBox('NameTitle',2,'NameTitle',/Root/User,'',5,-1)"/>
                  </td>
                  <td unselectable="on">
                    <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
                    <xsl:value-of disable-output-escaping="yes" select="js:InputBox('NameFirst',18,'NameFirst',/Root/User,'',5,-1)"/>
                    <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
                    <xsl:value-of disable-output-escaping="yes" select="js:InputBox('NameLast',18,'NameLast',/Root/User,'',5,-1)"/>
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('UserID',8,'UserID',/Root,'',6)"/>
                  </td>
                </tr>
              </table>  
            </td>
        </tr>
       
	    <tr>
            <td colspan="2" >Stop Assignment:</td>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AssignmentEndDate',35,'AssignmentEndDate',/Root/User,'',4,1)"/>
            </td>
        </tr>
        
		<tr> 
            <td colspan="2">Restart Assignment:</td>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="js:InputBox('AssignmentBeginDate',35,'AssignmentBeginDate',/Root/User,'',4,2)"/>
            </td>
        </tr>
        
		<tr> 
            <td colspan="2" unselectable="on">Last Assignment:</td>
            <td unselectable="on">
              <INPUT id='LastAssignmentDate' size='22' type='text' class='InputReadonlyField' name='LastAssignmentDate' tabindex='-1' >
                  <xsl:attribute name="readonly"/>
                  <xsl:attribute name="systemfield"/>
                  <xsl:if test="string-length(/Root/User/@LastAssignmentDate) > 0">
                      <xsl:attribute name="value">
                          <xsl:call-template name="formatDateTime"><xsl:with-param name="sDateTime"><xsl:value-of select="/Root/User/@LastAssignmentDate"/></xsl:with-param></xsl:call-template>
                      </xsl:attribute>
                  </xsl:if>
              </INPUT>
            </td>
        </tr>  	
		
		
       <tr><td colspan="2"><b>Work Schedule</b></td></tr>      	
	   <tr></tr>
	
  <TR>
	<TD colspan="2">Monday:</TD>
    <TD> 
	    <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingMondayStartTime',2,'OperatingMondayStartTime',/Root/User,'',10,3)"/>
        <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>   
	     <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>-
	    <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingMondayEndTime',2,'OperatingMondayEndTime',/Root/User,'',10,4)"/>
 	     <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text> 
	</TD>	
  </TR>
   
  <TR>
    <TD colspan="2">Tuesday:</TD>
    <TD>
	    <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingTuesdayStartTime',2,'OperatingTuesdayStartTime',/Root/User,'',10,5)"/>
		<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text> 
	  	<img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>-
	    <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingTuesdayEndTime',2,'OperatingTuesdayEndTime',/Root/User,'',10,6)"/>
		<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text> 
	</TD>
	<TD> </TD>
	<TD>Saturday:</TD>
    <TD>
 	   <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingSaturdayStartTime',2,'OperatingSaturdayStartTime',/Root/User,'',10,13)"/>
	   <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text> 
 	   <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>-
	   <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingSaturdayEndTime',2,'OperatingSaturdayEndTime',/Root/User,'',10,14)"/>
	   <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text> 
	</TD>
  </TR>

  
  <TR>
    <TD colspan="2">Wednesday:</TD>
      <TD>
 	    <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingWednesdayStartTime',2,'OperatingWednesdayStartTime',/Root/User,'',10,7)"/>
		<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text> 
	   	<img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>-
	    <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingWednesdayEndTime',2,'OperatingWednesdayEndTime',/Root/User,'',10,8)"/>
		<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text> 
	</TD>
	<TD> </TD>
	<TD>Sunday:</TD>
    <TD>
	  <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingSundayStartTime',2,'OperatingSundayStartTime',/Root/User,'',10,15)"/>
	  <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text> 
      <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>-
	  <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingSundayEndTime',2,'OperatingSundayEndTime',/Root/User,'',10,16)"/>
	  <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text> 
	</TD>	
  </TR>

  <TR>
    <TD colspan="2">Thursday:</TD>
    <TD>
	  <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingThursdayStartTime',2,'OperatingThursdayStartTime',/Root/User,'',10,9)"/>
	  <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text> 
      <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>-
	  <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingThursdayEndTime',2,'OperatingThursdayEndTime',/Root/User,'',10,10)"/>
	  <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text> 
	</TD>
  </TR>

  <TR>
    <TD colspan="2">Friday:</TD>
    <TD>
	  <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingFridayStartTime',2,'OperatingFridayStartTime',/Root/User,'',10,11)"/>
	  <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text> 
      <img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>-
      <xsl:value-of disable-output-escaping="yes" select="js:InputBox('OperatingFridayEndTime',2,'OperatingFridayEndTime',/Root/User,'',10,12)"/>
	  <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text> 
	</TD>
     <TD>
        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkAssignCCCOneDirect',string(/Root/User/@ReceiveCCCOneAssignmentFlag),'1','0',position(), '', 17)"/>
        
     </TD>
     <td colspan="2">Receive CCC One Assignment directly</td>
  </TR>
  
 </table>
   
</xsl:template>


<xsl:template name="InsuranceCompanies">
  <xsl:variable name="APDAppID"><xsl:value-of select="/Root/Reference[@List='Application' and @Name='APD']/@ReferenceID"/></xsl:variable>
  <table border="0" cellspacing="0" cellpadding="0" style="margin:10px;">
    <tr>  
      <td align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr valign="bottom">
            <td align="left">
              <table border="0" cellspacing="1" cellpadding="2" width="215" style="border-collapse:collapse;table-layout:fixed">
                <colgroup>
                  <col/>
                  <col width="60"/>
                </colgroup>
                <tr class="QueueHeader" style="height:32px;">
                  <td class="TableSortHeader">Insurance Company</td>
                  <td class="TableSortHeader">Assign Work</td>
                </tr>
              </table>
              <DIV id="divInsuranceAssign" unselectable="on" style="z-index:1; width:232px; height:130px; overflow: auto;">
                <table name="tblInsCo" id="tblInsCo" border="0" cellspacing="1" cellpadding="2" width="215" style="border-collapse:collapse;table-layout:fixed;">
                  <colgroup>
                    <col style="padding:0px;padding-left:10px;"/>
                    <col width="60" style="text-align:center"/>
                  </colgroup>
                
                  <xsl:for-each select="/Root/Reference[@List = 'InsuranceCompany']">
                    
                    <xsl:variable name="InsuranceCompanyID">
                      <xsl:value-of select="@ReferenceID"/>
                    </xsl:variable>
                    
                    <xsl:variable name="InsAssignWork">
                      <xsl:choose>
                        <xsl:when test="/Root/User/Insurance[@InsuranceCompanyID = $InsuranceCompanyID]">1</xsl:when>
                        <xsl:otherwise>0</xsl:otherwise>
                      </xsl:choose>
                    </xsl:variable>
                      
                  <tr style="height:18px;">
                    <td nowrap="true"><xsl:value-of select="@Name"/></td>
                    <td onclick="focusMyTab()">
                      <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkInsAssignWork_',string($InsAssignWork),'1','0',position(), '', 30)"/>
                      <script>
                        var obj = document.getElementById("chkInsAssignWork_<xsl:value-of select="position()"/>Div");
                        obj.onclick = new Function("", "InsAssignWorkClicked(this)");
                        obj.index = <xsl:value-of select="position()"/>;
                      </script>
                    </td>                      
                    <td style="display:none"><xsl:value-of select="@ReferenceID"/></td>
                  </tr>
                  </xsl:for-each>
                </table>
              </DIV>
            </td>
          </tr>
          <tr>
            <td>
              <table border="0">
                <tr><td colspan="4"><img src="/images/spacer.gif" alt="" height="7" border="0"/></td></tr>
                <tr>
                  <td><img src="/images/spacer.gif" alt="" width="155"  height="10" border="0"/></td>
                  <td>
                    <Button type="button" name="cmdResetInsAssignWork" class="formbutton" onClick="resetInsAssignWork()" tabindex="35">Clear</Button>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          
        </table>
      </td>
    </tr>
	 
	 <tr>
            <td>
              Assign work from all insurance companies:
            </td>
            <td valign="bottom">
              
              <xsl:variable name="allInsurance">
                <xsl:choose>
                  <xsl:when test="count(/Root/User/Insurance) = count(/Root/Reference[@List='InsuranceCompany'])">1</xsl:when>
                  <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              
              <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkAllInsurance',string($allInsurance),'1','0','','',32)"/>
              <script>
                var obj = document.getElementById("chkAllINsuranceDiv");
                obj.onclick = new Function("", "selectAllInsCos()");
              </script>
            </td>
          </tr>
  </table>
</xsl:template>


<xsl:template name="LicStates">
<td align="left">
        <table border="0" cellspacing="0" cellpadding="0">
         
		  <tr valign="bottom">
            <td align="left">
              <table border="0" cellspacing="1" cellpadding="2" style="border-collapse:collapse;table-layout:fixed">
                <colgroup>
                  <col width="125"/>
                  <col width="60"/>
                  <col width="60"/>
                  <col width="60"/>
                </colgroup>
                <tr class="QueueHeader" style="height:32px;">
                  <td class="TableSortHeader">State</td>
                  <td class="TableSortHeader">Licensed</td>
                  <td class="TableSortHeader">Assign Work</td>
                  <td class="TableSortHeader">License Required</td>
                </tr>
              </table>
              <DIV id="divStateLicensed" unselectable="on" style="z-index:1; width:327px; height:130px; overflow: auto;">
                <table name="tblState" id="tblState" border="0" cellspacing="1" cellpadding="2" style="border-collapse:collapse;table-layout:fixed;">
                  <colgroup>
                    <col width="125" style="padding:0px;padding-left:10px;"/>
                    <col width="60" style="text-align:center"/>
                    <col width="60" style="text-align:center"/>
                    <col width="60" style="text-align:center"/>
                  </colgroup>
             
                    <xsl:for-each select="/Root/User/States">
                        <xsl:variable name="readOnly">
                          <xsl:choose>
                            <!-- <xsl:when test="@LicenseRequired = 0">true</xsl:when> -->
                            <xsl:when test="@AnalystLicenseRequired = 0 and @OwnerLicenseRequired = 0">true</xsl:when>
                            <xsl:otherwise>false</xsl:otherwise>
                          </xsl:choose>
                        </xsl:variable>
                    <tr style="height:18px;">
                      <td><xsl:value-of select="@StateValue"/></td>
                      <td onclick="focusMyTab()">
                        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkStateLicensed_',string(@LicenseFlag),'1','0',position(), '', 28)"/>
                        <script>
                            var obj = document.getElementById("chkStateLicensed_<xsl:value-of select="position()"/>Div");
                            obj.onclick = new Function("", "stateLicensedClicked(this)");
                            obj.index = <xsl:value-of select="position()"/>;
                        </script>
                      </td>
                      <td onclick="focusMyTab()">
                        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkStateAssignWork_',string(@AssignWorkFlag),'1','0',position(),'',29)"/>
                        <script>
                            var obj = document.getElementById("chkStateAssignWork_<xsl:value-of select="position()"/>Div");
                            obj.onclick = new Function("", "assignWorkClicked(this)");
                            obj.index = <xsl:value-of select="position()"/>;
                        </script>
                      </td>
                      <td>
                        <xsl:choose>
                          <!-- <xsl:when test="@LicenseRequired = 1">Yes</xsl:when> -->
                          <xsl:when test="@AnalystLicenseRequired = 1 or @OwnerLicenseRequired = 1">Yes</xsl:when>
                          <xsl:otherwise>No</xsl:otherwise>
                        </xsl:choose>
                      </td>
                      <td style="display:none"><xsl:value-of select="@StateCode"/></td>
                    </tr>
                    </xsl:for-each>
                  </table>
              </DIV>
            </td>
          </tr>
          <tr>
            <td>
              <table>
                <tr><td colspan="4"><img src="/images/spacer.gif" alt="" height="7" border="0"/></td></tr>
                <tr>
                  <td><img src="/images/spacer.gif" alt="" width="128"  height="10" border="0"/></td>
                  <td><Button type="button" name="cmdResetLicensed" class="formbutton" onClick="resetLicensedState()" tabindex="34">Clear</Button></td>
                  <td><img src="/images/spacer.gif" alt="" width="12"  height="10" border="0"/></td>
                  <td><Button type="button" name="cmdResetAssign" class="formbutton" onClick="resetAssignedWork()" tabindex="33">Clear</Button></td>
				  
                </tr>
				
              </table>
            </td>
          </tr>
		 	  
		   <tr>
            <td>
              Assign work from all states with no license required:
            </td>
            <td valign="bottom">
              <xsl:variable name="allNotLicensed">
                <xsl:choose>
                  <!-- <xsl:when test="count(/Root/User/States[@LicenseRequired=0]) = count(/Root/User/States[@AssignWorkFlag=1 and @LicenseRequired=0])">1</xsl:when> -->
                  <xsl:when test="count(/Root/User/States[@AnalystLicenseRequired=0 and @OwnerLicenseRequired=0]) = count(/Root/User/States[@AssignWorkFlag=1 and @AnalystLicenseRequired=0 and @OwnerLicenseRequired=0])">1</xsl:when>
                  <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
                      
              <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkStateNotLicensed',string($allNotLicensed),'1','0','','',31)"/>
              <script>
                var obj = document.getElementById("chkStateNotLicensedDiv");
                obj.onclick = new Function("", "selectAllNoLicenseRequired()");
              </script>
            </td>
          </tr>
		  
        </table>
      </td>   
</xsl:template>


<xsl:template name="profileOverrides">
 <xsl:param name="ProfileReadOnly"/>
 <div style="margin:5px">  
    <table border="0">
          <colgroup>
			<col width="80px"/>
			<col width="180px"/>
			<col width="110px"/>
			<col width="50px"/>
          </colgroup>
        <tr style="font-weight:bold" valign="top">
            <td align="center">Overridden<br/> 
              <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkProfileOverridden',boolean(/Root/User/Profile[@OverriddenFlag='1']),'1','0','', $ProfileReadOnly,1,-1)"/>
              <script>
                 chkProfileOverriddenDiv.onclick=defaultProfileOverride;
              </script>
            </td>
            <td>Profile Name</td>
            <td>Value</td>
            <td>Default Value</td>
        </tr>
    </table>

      <DIV unselectable="on" style="z-index:1; width:465px; height:195px; overflow: auto;">
        <table border="0" nams="tblProfile" id="tblProfile" style="width:100%;">
          <colgroup>
			<col width="60px"/>
			<col width="130px"/>
			<col width="100px"/>
			<col width="50px"/>	
          </colgroup>
                <tr style="height:24px;font-color:#FF0000;">
              <td colspan="4"><img src="/images/minus.gif" style="margin-right:15px;cursor:hand;" onclick="showHideProfileGroup('C', tblProfile)"/><strong>Claims Management</strong></td>
            </tr>
            <xsl:for-each select="/Root/User/Profile[@BusinessFunctionCD='C']">
              <xsl:sort select="@ProfileName" data-type="text" order="ascending"/>
              <xsl:call-template name="profileRow">
                <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
              </xsl:call-template>
            </xsl:for-each>
        </table>
    </DIV>
  </div>
</xsl:template>

<xsl:template name="profileRow">
  <xsl:param name="ProfileReadOnly"/>
            <tr style="height:24px;" valign="middle">
              <xsl:attribute name="businessCode"><xsl:value-of select="@BusinessFunctionCD"/></xsl:attribute>
              <xsl:attribute name="DataTypeCD"><xsl:value-of select="@DataTypeCD"/></xsl:attribute>
                <td style="border-bottom:1px dashed #FFE4B5;" align="center">
                  <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkProfileOverridden_', string(@OverriddenFlag),string(@ProfileID),'0',string(@ProfileID), $ProfileReadOnly,1, 29)"/>		  
                  <script>
                    chkProfileOverridden_<xsl:value-of select="@ProfileID"/>Div.onclick=checkProfileOverride;
                  </script>
                </td>
                <td style="border-bottom:1px dashed #FFE4B5;"><xsl:value-of select="@ProfileName"/></td>
                <td style="border-bottom:1px dashed #FFE4B5;">
                  <xsl:call-template name="profileItemValue">
                    <xsl:with-param name="ProfileReadOnly" select="$ProfileReadOnly"/>
                  </xsl:call-template>
                </td>
                <td style="border-bottom:1px dashed #FFE4B5;">
                  <xsl:choose>
                    <xsl:when test="@DataTypeCD = 'B'">
                     <xsl:choose>
                      <xsl:when test="@DefaultProfileValue=1">Yes</xsl:when>
                      <xsl:otherwise>No</xsl:otherwise>
                     </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@DataTypeCD = 'S'">
                      <xsl:variable name="ProfileID"><xsl:value-of select="@ProfileID"/></xsl:variable>
                      <xsl:variable name="DefaultProfileValue"><xsl:value-of select="@DefaultProfileValue"/></xsl:variable>
                      <xsl:value-of select="/Root/Reference[@List='ProfileSelection' and @ProfileID=$ProfileID and @ReferenceID=$DefaultProfileValue]/@Name"/>
                    </xsl:when>
                    <xsl:when test="@DataTypeCD = 'N'">
                      <xsl:choose>
                        <xsl:when test="@DefaultProfileValue != ''">
                          <xsl:value-of select="@DefaultProfileValue"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:when>
                  </xsl:choose>
                </td>
                <td style="display:none"><xsl:value-of select="@ProfileID"/></td>
            </tr>
</xsl:template>

<xsl:template name="profileItemValue">
  <xsl:param name="ProfileReadOnly"/>
  <xsl:choose>
    <xsl:when test="@DataTypeCD = 'B'">
      <xsl:variable name="defValue">
       <xsl:choose>
        <xsl:when test="@ProfileValue=1">Yes</xsl:when>
        <xsl:otherwise>No</xsl:otherwise>
       </xsl:choose>
      </xsl:variable>
      <xsl:value-of disable-output-escaping="yes" select="js:AddRadio(concat('ProfileValue', @ProfileID), 0, string($defValue), 'Yes', 'No')"/>
     <script>
        ProfileValue<xsl:value-of select="@ProfileID"/>Div.onclick=ProfileOverride;
           <xsl:if test="boolean($ProfileReadOnly) = true()">
            ProfileValue<xsl:value-of select="@ProfileID"/>Div.disabled=true;
          </xsl:if>
      </script>
    </xsl:when>
    <xsl:when test="@DataTypeCD = 'S'">
      <xsl:variable name="ProfileID" select="@ProfileID"/>
      <xsl:value-of disable-output-escaping="yes" select="js:AddSelect(concat('ProfileValue', @ProfileID),2,'onSelectChange',string(@ProfileValue),1,(last()-position()),/Root/Reference[@List='ProfileSelection' and @ProfileID=$ProfileID],'Name','ReferenceID',0,90,'','',15)"/>
      <script>
        selProfileValue<xsl:value-of select="@ProfileID"/>.onclick=ProfileOverride;
           <xsl:if test="boolean($ProfileReadOnly) = true()">
           selProfileValue<xsl:value-of select="@ProfileID"/>.disabled=true;
          </xsl:if> 
      </script>
    </xsl:when>
    <xsl:when test="@DataTypeCD = 'N'">
      <INPUT size='8' type='text' maxlength='15' onchange="ProfileOverride()">
        <xsl:attribute name="name"><xsl:value-of select="concat('ProfileValue', @ProfileID)"/></xsl:attribute>
        <xsl:attribute name="id"><xsl:value-of select="concat('ProfileValue', @ProfileID)"/></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="string(@ProfileValue)"/></xsl:attribute>
        <xsl:choose>
          <xsl:when test="boolean($ProfileReadOnly) = true()">
            <xsl:attribute name="disabled"/>
            <xsl:attribute name="readonly"/>
            <xsl:attribute name="class">InputReadonlyField</xsl:attribute>
          </xsl:when>
          <xsl:otherwise> 
            <xsl:attribute name="class">InputField</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose> 
      </INPUT>
    </xsl:when>
  </xsl:choose>
</xsl:template>


<xsl:template name="AssignmentPool">
  <xsl:param name="ProfileReadOnly"/>
  <xsl:param name="PermissionReadOnly"/>  
  
  <div style="margin:8px">
    <table border="0">
      <colgroup>
        <col width="190px"/>
        <col width="80px"/>
        <col width="80px"/>
      </colgroup>
      <tr style="font-weight:bold" valign="top">
        <td>Assignment Pool</td>
        <td>Function</td>
        <td></td>
      </tr>
    </table>
    <DIV id="WorkAssignmentPoolDiv" unselectable="on" style="z-index:1; width:300px; height:195px; overflow: auto;">
        <table border="0" cellspacing="0" cellpadding="0" name="tblAssignmentPool" id="tblAssignmentPool" style="width:100%;">
          <colgroup>
            <col width="200px"/>
            <col width="80px"/>
            <col width="20px"/>
          </colgroup>

            <xsl:for-each select="/Root/Reference[@List='AssignmentPool']">
              <tr style="height:24px;">
                  <td style="border-bottom:1px dashed #FFE4B5;">
                    <xsl:value-of select="@Name"/>
                  </td>
                  <td style="border-bottom:1px dashed #FFE4B5;">
                    <!-- <xsl:value-of select="@FunctionCD"/> -->
                    <xsl:choose>
                      <xsl:when test="@FunctionCD = 'OWN'">Owner</xsl:when>
                      <xsl:when test="@FunctionCD = 'ALST'">Analyst</xsl:when>
                      <xsl:when test="@FunctionCD = 'SPRT'">Support</xsl:when>
                    </xsl:choose>
                  </td>
                  <td style="border-bottom:1px dashed #FFE4B5;">
                    <xsl:variable name="AssignmentPoolID"><xsl:value-of select="@ReferenceID"/></xsl:variable>
                    <xsl:variable name="selected">
                      <xsl:choose>
                        <xsl:when test="/Root/User/AssignmentPool[@AssignmentPoolID=$AssignmentPoolID]">1</xsl:when>
                        <xsl:otherwise>0</xsl:otherwise>
                      </xsl:choose>
                    </xsl:variable>
                    <xsl:choose>
                      <xsl:when test="$PermissionReadOnly = 'true'">
                        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkWorkPool_',string($selected),string(@ReferenceID),'0',string(@ReferenceID), string($ProfileReadOnly), 24)"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('chkWorkPool_',string($selected),string(@ReferenceID),'0',string(@ReferenceID), '', 24)"/>
                      </xsl:otherwise>
                    </xsl:choose>
                    <script>
                      var obj = document.getElementById("chkWorkPool_<xsl:value-of select="@ReferenceID"/>Div");
                      obj.index = <xsl:value-of select="position()-1"/>;
                      obj.FunctionCD = "<xsl:value-of select="@FunctionCD"/>";
                      <!-- obj.onclick = new Function("", "ValidatePoolFunction(this)"); -->
                    </script>
                  </td>
              <td style="display:none"><xsl:value-of select="@ReferenceID"/></td>
              </tr>
            </xsl:for-each>

        </table>
    </DIV>
  </div>
</xsl:template>


<xsl:template name="pageDefaults">
  <xsl:variable name="APDAppID"><xsl:value-of select="/Root/Reference[@List='Application' and @Name='APD']/@ReferenceID"/></xsl:variable>
  <div style="display:none">
    <input type="hidden" name="__CSRNo" id="__CSRNo" >
      <xsl:attribute name="value"><xsl:value-of select="/Root/User/Application[@ApplicationID=$APDAppID]/@LogonId"/></xsl:attribute>
    </input>

    <input type="hidden" name="__AssignmentBeginDate" id="__AssignmentBeginDate" />

    <input type="hidden" name="__AssignmentEndDate" id="__AssignmentEndDate" />

    <input type="hidden" name="__MondayHours" id="__MondayHours" >
      <xsl:attribute name="value">
        <xsl:value-of select="normalize-space(/Root/User/@OperatingMondayStartTime)"/>-<xsl:value-of select="normalize-space(/Root/User/@OperatingMondayEndTime)"/>
      </xsl:attribute>
    </input>

    <input type="hidden" name="__TuesdayHours" id="__TuesdayHours" >
      <xsl:attribute name="value">
        <xsl:value-of select="normalize-space(/Root/User/@OperatingTuesdayStartTime)"/>-<xsl:value-of select="normalize-space(/Root/User/@OperatingTuesdayEndTime)"/>
      </xsl:attribute>
    </input>

    <input type="hidden" name="__WednesdayHours" id="__WednesdayHours" >
      <xsl:attribute name="value">
        <xsl:value-of select="normalize-space(/Root/User/@OperatingWednesdayStartTime)"/>-<xsl:value-of select="normalize-space(/Root/User/@OperatingWednesdayEndTime)"/>
      </xsl:attribute>
    </input>
    
    <input type="hidden" name="__ThursdayHours" id="__ThursdayHours" >
      <xsl:attribute name="value">
        <xsl:value-of select="normalize-space(/Root/User/@OperatingThursdayStartTime)"/>-<xsl:value-of select="normalize-space(/Root/User/@OperatingThursdayEndTime)"/>
      </xsl:attribute>
    </input>

    <input type="hidden" name="__FridayHours" id="__FridayHours" >
      <xsl:attribute name="value">
        <xsl:value-of select="normalize-space(/Root/User/@OperatingFridayStartTime)"/>-<xsl:value-of select="normalize-space(/Root/User/@OperatingFridayEndTime)"/>
      </xsl:attribute>
    </input>

    <input type="hidden" name="__SaturdayHours" id="__SaturdayHours" >
      <xsl:attribute name="value">
        <xsl:value-of select="normalize-space(/Root/User/@OperatingSaturdayStartTime)"/>-<xsl:value-of select="normalize-space(/Root/User/@OperatingSaturdayEndTime)"/>
      </xsl:attribute>
    </input>

    <input type="hidden" name="__SundayHours" id="__SundayHours" >
      <xsl:attribute name="value">
        <xsl:value-of select="normalize-space(/Root/User/@OperatingSundayStartTime)"/>-<xsl:value-of select="normalize-space(/Root/User/@OperatingSundayEndTime)"/>
      </xsl:attribute>
    </input>

    <input type="hidden" name="__chkAllInsurance" id="__chkAllInsurance" />

    <input type="hidden" name="__chkStateNotLicensed" id="__chkStateNotLicensed" />

    <input type="hidden" name="__Profile" id="__Profile" >
      <xsl:attribute name="value">
        <xsl:for-each select="/Root/User/Profile[@OverriddenFlag=1]">
          <xsl:value-of select="@ProfileID"/>,<xsl:value-of select="@ProfileValue"/>
          <xsl:if test="position() != last()">;</xsl:if>
        </xsl:for-each>
      </xsl:attribute>
    </input>

    <input type="hidden" name="__Pool" id="__Pool" >
      <xsl:attribute name="value">
        <xsl:for-each select="/Root/User/AssignmentPool">
          <xsl:value-of select="@AssignmentPoolID"/>
          <xsl:if test="position() != last()">,</xsl:if>
        </xsl:for-each>
      </xsl:attribute>
    </input>

    <input type="hidden" name="__InsuranceCo" id="__InsuranceCo" >
      <xsl:attribute name="value">
        <xsl:for-each select="/Root/User/Insurance">
          <xsl:value-of select="@InsuranceCompanyID"/>
          <xsl:if test="position() != last()">;</xsl:if>
        </xsl:for-each>
      </xsl:attribute>
    </input>

    <input type="hidden" name="__States" id="__States" >
      <xsl:attribute name="value">
        <xsl:for-each select="/Root/User/States[@AssignWorkFlag=1 or @LicenseFlag=1]">
          <xsl:value-of select="@StateCode"/>,<xsl:value-of select="@LicenseFlag"/>,<xsl:value-of select="@AssignWorkFlag"/>
          <xsl:if test="position() != last()">;</xsl:if>
        </xsl:for-each>
      </xsl:attribute>
    </input>

  </div>
</xsl:template>



<xsl:template name="formatDateTime">
    <xsl:param name="sDateTime"/>
    <xsl:variable name="sYear" select="substring($sDateTime, 1,4)"/>
    <xsl:variable name="sMonth" select="substring($sDateTime, 6,2)"/>
    <xsl:variable name="sDay" select="substring($sDateTime, 9,2)"/>  
    <xsl:value-of select="concat($sMonth, '/', $sDay, '/', $sYear)"/>
    <xsl:variable name="sHH1" select="substring($sDateTime, 12,2)"/>
    <xsl:if test="number($sHH1) &gt; 12">
        <xsl:variable name="sHH" select="format-number(number($sHH1) - 12, '00')"/>
        <xsl:variable name="sAMPM" select="PM"/>
        <xsl:variable name="sMM" select="substring($sDateTime, 15,2)"/>
        <xsl:variable name="sSS" select="substring($sDateTime, 18,2)"/>
        <xsl:value-of select="concat(' ', $sHH, ':', $sMM, ':', $sSS, ' PM')"/>
    </xsl:if>
    <xsl:if test="number($sHH1) &lt; 13">
        <xsl:variable name="sHH" select="$sHH1"/>
        <xsl:variable name="sMM" select="substring($sDateTime, 15,2)"/>
        <xsl:variable name="sSS" select="substring($sDateTime, 18,2)"/>
        <xsl:value-of select="concat(' ', $sHH, ':', $sMM, ':', $sSS, ' AM')"/>
    </xsl:if>
</xsl:template>

</xsl:stylesheet>
