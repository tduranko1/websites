<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:session="http://lynx.apd/session-manager"
		xmlns:user="http://mycompany.com/mynamespace"
		xmlns:js="urn:the-xml-files:xslt"
		id="ClaimProperty">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="PropertyCRUD" select="Property"/>
<xsl:param name="InvolvedCRUD" select="Involved"/>
<xsl:param name="PropertyContactCRUD" select="Property Contact"/>
<xsl:param name="DocumentCRUD" select="Document"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="WindowID"/>
<xsl:param name="ImageRootDir"/>
<xsl:param name="LynxID"/>
<xsl:param name="ClaimAspectID"/>
<xsl:param name="UserId"/>

<xsl:template match="/Root">

<xsl:variable name="PertainsTo" select="/Root/@Context" />
<xsl:variable name="ContextSupported" select="/Root/@ContextSupportedFlag" />

<xsl:variable name="Context" select="session:XslUpdateSession('Context',string(/Root/@Context))"/>
<xsl:variable name="InsuranceCompanyID" select="session:XslGetSession('InsuranceCompanyID')"/>
<xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>

<xsl:variable name="pageReadOnly" select="string(boolean($ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'))"/>

<!--<xsl:variable name="LynxID" select="/Root/@LynxID" />-->
<xsl:variable name="PropertyNumber" select="/Root/@PropertyNumber" />
<xsl:variable name="PropertyLastUpdatedDate" select="/Root/Property/@SysLastUpdatedDate"/>
<xsl:value-of select="js:SetCRUD( string($PropertyCRUD) )"/>

<xsl:if test="$ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'">
  <xsl:value-of select="js:SetCRUD( '_R__' )"/>
  <xsl:value-of select="js:setPageDisabled()"/>
</xsl:if>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.2.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},10,uspClaimPropertyGetDetailXML,ClaimProperty.xsl,6366,1,145   -->

<HEAD>
<TITLE>Property Details</TITLE>

<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/CoolSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>

<STYLE type="text/css">
	  A {color:black; text-decoration:none;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/showimage.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,24);
		  event.returnValue=false;
		  };	
</script>

<SCRIPT language="JavaScript">

	var gsPropertyCRUD = '<xsl:value-of select="$PropertyCRUD"/>';
	var gsPropertyContactCRUD = '<xsl:value-of select="$PropertyContactCRUD"/>';
	var gsLynxID = '<xsl:value-of select="$LynxID"/>';
  var gsUserID = '<xsl:value-of select="$UserId"/>';
	//var gsPropNum = '<xsl:value-of select="$PropertyNumber"/>';
  var gsPropNum = parent.gsPropNum;
  var gsPropertyLastUpdatedDate = "<xsl:value-of select="$PropertyLastUpdatedDate"/>";
	var gsImageRootDir = '<xsl:value-of select="$ImageRootDir"/>';
  var gsInvolvedCount = '<xsl:value-of select="count(Property/Involved[@InvolvedID &gt; 0])"/>';
  var gsCRUD = "<xsl:value-of select="$DocumentCRUD"/>";
  var gsClaimAspectID = "<xsl:value-of select="$ClaimAspectID"/>";
  var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";
  var bPageReadonly = <xsl:value-of select="$pageReadOnly"/>;
  var gsCountNotes = '<xsl:value-of select="@CountNotes"/>';
  var gsCountTasks = '<xsl:value-of select="@CountTasks"/>';
  var gsCountBilling = '<xsl:value-of select="@CountBilling"/>';

  <xsl:variable name="ExposureCD" select="Property/@ExposureCD"/>
  var gsParty = "<xsl:value-of select="/Root/Reference[@List='Exposure' and @ReferenceID=$ExposureCD]/@Name"/>";
  var giInvPropClaimantCount = "<xsl:value-of select="count(/Root/Property/Involved/InvolvedType[@InvolvedTypeName = 'Claimant'])"/>";

<![CDATA[

	// Tooltip Variables to set:
	messages= new Array()
	// Write your descriptions in here.
	messages[0]="Double Click to Expand"
	messages[1]="Double Click to Close"

	// button images for ADD/DELETE/SAVE
	preload('buttonAdd','/images/but_ADD_norm.png')
	preload('buttonAddDown','/images/but_ADD_down.png')
	preload('buttonAddOver','/images/but_ADD_over.png')
	preload('buttonDel','/images/but_DEL_norm.png')
	preload('buttonDelDown','/images/but_DEL_down.png')
	preload('buttonDelOver','/images/but_DEL_over.png')
	preload('buttonSave','/images/but_SAVE_norm.png')
	preload('buttonSaveDown','/images/but_SAVE_down.png')
	preload('buttonSaveOver','/images/but_SAVE_over.png')

	// Select changes to update dirty flag and changed border
	function onSelectChange(selobj)
  {
		gbDirtyFlag = true;
		// check to make sure if in a collection
		if (selobj[0]) selobj = selobj[0];
		selobj.children[0].style.borderColor='#805050';
  }

	// Involved selection box change
	var inInvolvedChange = false;
	function onInvolvedChange(selobj)
	{
		if (inInvolvedChange == true) return;
		inInvolvedChange = true;

		//check if in insert mode and if so ignore change
    if (gbInsertMode == true)
		{
			setTimeout(selFixUp,200);
			inInvolvedChange = false;
		 	return;
		}

		if (selobj == null)
		{
			selobj = document.getElementById("selPropertyInvolved");
			var idxLength = selobj.options.length;
			for (var idx = 0;idx < idxLength; idx++)
			{
				var str = selobj.options[idx].innerText;
				if (str.indexOf("Owner") != -1)
					break;
			}
			if (idx < idxLength)
				selobj.optionselect(null, idx);
		}

		var involvedID = selobj.options[selobj.selectedIndex].value;
		if (involvedID > 0)
		  document.frames["ifrmPropertyInvolved"].frameElement.src = "PropertyInvolved.asp?InvolvedID="+involvedID
		else
			document.frames["ifrmPropertyInvolved"].frameElement.src = "blank.asp";

		inInvolvedChange = false;
	}

	function selFixUp()
	{
		selobj = document.getElementById("selPropertyInvolved");
		selobj.children[0].rows[0].cells[0].innerHTML = "<B>** New **</B>";
	}

	// Page Initialize
	function PageInit()
	{
		tabsIntabs[0].tabs.onchange=tabChange;
		tabsIntabs[1].tabs.onchange=tabChange;
    tabsIntabs[0].tabs.onBeforechange=tabBeforeChange;
    tabsIntabs[1].tabs.onBeforechange=tabBeforeChange;
		try
		{
			onInvolvedChange()
			//document.parentWindow.frameElement.style.visibility='inherit';

      if (gsCountNotes == "0" && gsCountTasks == "0" && gsCountBilling == "0" && gsPropertyCRUD.indexOf("D") != -1)
        top.PropertiesMenu(true,3);
      else
        top.PropertiesMenu(false,3);

      top.gsCountNotes = gsCountNotes;
      top.gsCountTasks = gsCountTasks;
      top.gsCountBilling = gsCountBilling;
      top.gsEntityCRUD = gsPropertyCRUD

      if (top.gaPropClaimantCount){
        top.gaPropClaimantCount[gsPropNum] = giInvPropClaimantCount;
      }
   	}catch(e) {}

    onpasteAttachEvents();
	}

	var inADS_Pressed = false;		// re-entry flagging for buttons pressed
  var objDivWithButtons = null;
	// ADS Button pressed
	function ADS_Pressed(sAction, sName)
	{
    if (ValidatePhones(sName) == false) return;

    if (sAction == "Update" && !gbDirtyFlag) return; //no change made to the page
		if (inADS_Pressed == true) return;
    inADS_Pressed = true;
    objDivWithButtons = divWithButtons;
    disableADS(objDivWithButtons, true);

		var oDiv = document.getElementById(sName)

		if (sAction == "Update")
		{
			if (oDiv.Many == "true")
			{
				var sType = oDiv.name;
				if (gbInsertMode == true) sAction = "Insert"
				var retArray = document.frames["ifrm"+sType].ADS_Pressed(sAction, sName);
        if (retArray[1] != "0")
				{
					//alert("Save Completed!");
					//need to set mode to non insert so ** NEw does not get displayed again
					var org_mode = gbInsertMode;
					gbInsertMode = false;
          UpdateSelects(sType, retArray[0]);
					gbInsertMode = org_mode;

					if (gbInsertMode == true)
					{
						oDiv.Count++;
						UpdateTabTextCount(oDiv);
					}

					selObj = document.getElementById(oDiv.id + "Sel");
					selObj.style.borderColor = "#cccccc";
					gbInsertMode = false;
					if (oDiv.getAttribute("CRUD").indexOf("C") != -1)
						eval(sName+"_ADD.style.visibility='inherit'");
					if (oDiv.getAttribute("CRUD").indexOf("D") != -1)
						eval(sName+"_DEL.style.visibility='inherit'");
				}
        else if (retArray[0] != "")
        {
          ClientInfo(retArray[0]);
        }
			}
			else
			{
				var sRequest = GetRequestString(sName);

				sRequest += "PropertySysLastUpdatedDate=" + gsPropertyLastUpdatedDate + "&";
  			sRequest += "PropertyNumber=" + gsPropNum + "&";
				sRequest += "LynxID=" + gsLynxID + "&";
				sRequest += "UserID=" + gsUserID + "&";
        sRequest += "ClaimAspectID=" + gsClaimAspectID + "&";

				var retArray = DoCrudAction(oDiv, sAction, sRequest);

        if (retArray[1] != "0")
				{
					var objXML = new ActiveXObject("Microsoft.XMLDOM");
					objXML.loadXML(retArray[0]);
					var rootNode = objXML.documentElement.selectSingleNode("/Root");

					var PropertyNode = rootNode.selectSingleNode("Property/@SysLastUpdatedDate");
					if (PropertyNode)
					{
						gsPropertyLastUpdatedDate = PropertyNode.nodeValue;
						oDiv.LastUpdatedDate = gsPropertyLastUpdatedDate
						//keep Description, Contact and Remarks tabs in sync
						var oDesc = document.getElementById("DescriptionInfo");
						if (oDesc) oDesc.LastUpdatedDate = gsPropertyLastUpdatedDate;
						var oCont = document.getElementById("content11");
						if (oCont) oCont.LastUpdatedDate = gsPropertyLastUpdatedDate;
						var oRemarks = document.getElementById("content12");
						if (oRemarks) oRemarks.LastUpdatedDate = gsPropertyLastUpdatedDate;
					}
					var contextDate = rootNode.selectSingleNode(oDiv.name+"/@SysLastUpdatedDate")
					if (contextDate)
						oDiv.LastUpdatedDate = contextDate.nodeValue;

	         var contextID = rootNode.selectSingleNode(oDiv.name+"/@InvolvedID")
	          if (contextID)
	            oDiv.ContextID = contextID.nodeValue;
				}
              top.document.frames["oIframe"].updateTabContent();
			}
		}
		else if (sAction == "Insert")
		{
			if (oDiv.Many == "true")
			{
				var sType = oDiv.name;

				var selObj = document.getElementById("sel"+sType)
				selObj.children[0].rows[0].cells[0].innerHTML = "<B>** New **</B>";
				document.frames["ifrm"+sType].frameElement.src = sType+".asp?InvolvedID=0";

				selObj = document.getElementById(oDiv.id + "Sel");
				selObj.style.borderColor = "#805050";

				eval(sName+"_ADD.style.visibility='hidden'");
				eval(sName+"_DEL.style.visibility='hidden'");

				if (oDiv.getAttribute("CRUD").indexOf("U") != -1)
					eval(sName+"_SAV.style.visibility='inherit'");

				gbInsertMode = true;
				gbDirtyFlag = true;
			}
		}
		else if (sAction == "Delete")
		{
			if (oDiv.Many == "true")
			{
				var sType = oDiv.name;
				if (gsInvolvedCount == 0) return;

        var objSel = document.getElementById("sel"+sType);
        if (objSel)
          if(objSel.options.length == 0) return;

        var objSel = document.getElementById("sel"+sType);
        var sInvolvedName = objSel.firstChild.innerText;
        if (sInvolvedName.indexOf("[") != -1)
          var sInvolved = sInvolvedName.substr(0, sInvolvedName.indexOf("["))
        else
          var sInvolved = sInvolvedName.substr(0, sInvolvedName.length-3);

        var sInvolvedConfirm = YesNoMessage("Are you sure that you want to delete Involved " +sInvolved+ "?",
                                            "Press YES to delete the selected Involved, or NO to cancel.");
        if (sInvolvedConfirm == "Yes")
        {
          var retArray = document.frames["ifrm"+sType].ADS_Pressed(sAction, sName);
          if (retArray[1] != "0")
  				{
            gsInvolvedCount--;
  					UpdateSelects(sType,retArray[0]);
  					if (parseInt(oDiv.Count) > 0) oDiv.Count--;
  					UpdateTabTextCount(oDiv);
						if (oDiv.Count == 0)
						{
				      eval(sName+"_SAV.style.visibility='hidden'");
				      eval(sName+"_DEL.style.visibility='hidden'");
						}
  				}
        }
			}
			else
			{
        if (gsClaimStatus == "Claim Cancelled" ||
            gsClaimStatus == "Claim Voided") {
            ClientWarning("Cannot delete property when the claim has been cancelled or voided.");

            inADS_Pressed = false;
            disableADS(objDivWithButtons, false);
            objDivWithButtons = null;

            return;
        }
				var sRequest = "SysLastUpdatedDate=" + gsPropertyLastUpdatedDate + "&";
  			sRequest += "PropertyNumber=" + gsPropNum + "&";
				sRequest += "LynxID=" + gsLynxID + "&";
				sRequest += "UserID=" + gsUserID + "&";
        sRequest += "ClaimAspectID=" + gsClaimAspectID + "&=";
				sRequest += "NotifyEvent=1&";

        var sIdProperty = "";
        if (txtPropName.value != "")
          sIdProperty += " (" +txtPropName.value+ ")";

        var sPropertyDelConfirm = YesNoMessage("Are you sure that you want to delete Property " +gsPropNum + sIdProperty+ "?",
                                               "Press YES to delete this Property, or NO to cancel.");
        if (sPropertyDelConfirm == "Yes")
				{
					var retArray = DoCrudAction(oDiv, sAction, sRequest);
          if (retArray[1] != "0")
						parent.document.parentWindow.frameElement.src="ClaimPropertyList.asp?LynxID=" + gsLynxID + "&PropContext=1";
				}
			}
		}

    inADS_Pressed = false;
    disableADS(objDivWithButtons, false);
    objDivWithButtons = null;
	}

	// Re-load the Involved select box from DXML
	function UpdateSelects(sType, sXML)
	{
		var objXML = new ActiveXObject("Microsoft.XMLDOM");
		objXML.loadXML(sXML);
		var objNodeList = objXML.documentElement.selectNodes("/Root/"+sType);
		var objSel = document.getElementById("sel"+sType);
    // var selIndex = objNodeList.length - 1;
    var selInvId = objXML.documentElement.selectSingleNode("/Root/NewPropertyInvolved/@InvolvedID")
    var sInvolvedID = selInvId.nodeValue;
    var selIndex = 0;
    var strOptions = "";
    var nLength = objNodeList.length;
    var iClaimantCount = 0;
    for (var n = 0; n < nLength; n++)
    {
			var objNode = objNodeList.nextNode();
			var strSel = ( n == selIndex ) ? " selected" : "";
			strOptions += "<div class='option' value='"	+ objNode.selectSingleNode( "@InvolvedID" ).text + "'" + strSel + " nowrap>";
        if (objNode.selectSingleNode( "@BusinessName" ).text != "")
        {
          strOptions += objNode.selectSingleNode( "@BusinessName" ).text + " ";
        }
        else
        {
  				strOptions += objNode.selectSingleNode( "@NameFirst" ).text + " ";
  			  strOptions += objNode.selectSingleNode( "@NameLast" ).text + " " ;
        }

			if (objNode.selectSingleNode( "@InvolvedID" ).text != "0") {
        strOptions += "[";
  			var objNodeTypes = objNode.selectNodes("PropertyInvolvedType");
        var xLength = objNodeTypes.length;
  			for(var x = 0; x < xLength; x++)
  			{
  				var objNodeT = objNodeTypes.nextNode();
          var InvType = objNodeT.selectSingleNode( "@InvolvedTypeName" ).text;
  	      strOptions += InvType;
  				if (x < (xLength - 1))
  					strOptions += ", ";
          if (InvType == "Claimant")
            iClaimantCount++;
  			}
  			strOptions += "]";
      }
      strOptions += "</div>";

      if (sInvolvedID == (objNode.selectSingleNode("@InvolvedID").text))
    		selIndex = n;
    }

    if (top.gaPropClaimantCount)
      top.gaPropClaimantCount[gsPropNum] = iClaimantCount;

    objSel.all.selectOptionDiv.innerHTML = strOptions;
    objSel.optionselect( null, selIndex );
	}

    // IMAGE module popup code
    function ShowImage(strPath)
    {
        if (gsCRUD.indexOf("U") == -1 ) return;
        ShowImagePopup( gsImageRootDir + strPath );
    }

    function tabBeforeChange(obj, tab)
    {
        var relatedTab = obj
		// if still on the same tab just ignore
		if (obj == tab) return;
        //var relatedTab = tabsIntabs[tab.tabindex-1].activeTab;
        return ValidatePhones(relatedTab.content.id);
    }

    function ValidatePhones(sName) {
        switch(sName) {
            case "content11": //Property contact
                if (validatePhoneSections(txtPropContactDayPhoneAC, txtPropContactDayPhoneEX, txtPropContactDayPhoneNM) == false) return false;
                break;
            case "content21": //Involved Info
                var oDiv = document.getElementById("content21");
                var sType = oDiv.name;
                try {
                    return document.frames["ifrm"+sType].ValidatePhones();
                }
                catch (e) {return true;}
                break;
        }
        return true;
    }

	if (document.attachEvent)
		document.attachEvent("onclick", top.hideAllMenuScriptlets);
	else
		document.onclick = top.hideAllMenuScriptlets;

]]>
</SCRIPT>

</HEAD>
<BODY unselectable="on" class="bodyAPDSub" onLoad="tabInit(false,'#FFFFFF'); initSelectBoxes(); InitFields(); PageInit();">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

<DIV unselectable="on" style="position:absolute; visibility: visible; top: 2px; left: 0px">
  <DIV unselectable="on" id="DescriptionInfoTab" class="SmallGroupingTab" style="position:absolute; z-index:2; top: 0px; left: 6px;">
    Description</DIV>

  <!-- Start Description Info -->
  <DIV unselectable="on" class="SmallGrouping" id="DescriptionInfo" name="PropertyDescription" Update="uspPropertyInfoUpdDetail" Delete="uspClaimPropertyDel" onfocusin="DivOnFocusIn(this)" style="position:absolute; left:3px; top:16px; width:727px;z-index:1">
    <xsl:attribute name="CRUD">
      <xsl:choose>
        <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
        <xsl:otherwise><xsl:value-of select="$PropertyCRUD"/></xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
		<xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Property/@SysLastUpdatedDate"/></xsl:attribute>

		<xsl:for-each select="Property" >
			<xsl:call-template name="DescriptionInfo"/>
		</xsl:for-each>

    <SCRIPT>ADS_Buttons('DescriptionInfo');</SCRIPT>
  </DIV>

  <!-- Start Tabs1 -->
  <DIV unselectable="on" style="position:absolute; visibility: visible; top: 133px; left: 0px">
    <DIV unselectable="on" id="tabs1" class="apdtabs">
      <SPAN unselectable="on" id="tab11" class="tab1">Property Contact</SPAN>
			<SPAN unselectable="on" id="tab12" class="tab1">Remarks to Claims Rep</SPAN>
		</DIV>

		<xsl:value-of select="js:SetCRUD( string($PropertyContactCRUD) )"/>
    <!-- Start Property Contact Info -->
    <DIV unselectable="on" class="content1" id="content11" name="PropertyContact" Update="uspPropertyContactUpdDetail" onfocusin="DivOnFocusIn(this)" style="width:727px;">
      <xsl:attribute name="CRUD">
        <xsl:choose>
          <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
          <xsl:otherwise><xsl:value-of select="$PropertyContactCRUD"/></xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
		  <xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Property/Contact/@SysLastUpdatedDate"/></xsl:attribute>
			<xsl:attribute name="ContextID"><xsl:value-of select="Property/Contact/@InvolvedID"/></xsl:attribute>
			<xsl:attribute name="ContextName">Involved</xsl:attribute>

  		<xsl:for-each select="Property/Contact" >
  			<xsl:call-template name="PropertyContactInfo"/>
  		</xsl:for-each>

      <SCRIPT>ADS_Buttons('content11');</SCRIPT>
    </DIV>

		<xsl:value-of select="js:SetCRUD( string($PropertyCRUD) )"/>
    <!-- Start Remarks to Claims Rep Info -->
    <DIV unselectable="on" class="content1" id="content12" name="PropertyRemarks" Update="uspPropertyRemarksUpdDetail" onfocusin="DivOnFocusIn(this)" style="width:727px; height:48px;">
      <xsl:attribute name="CRUD">
        <xsl:choose>
          <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
          <xsl:otherwise><xsl:value-of select="$PropertyCRUD"/></xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
	  	<xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Property/@SysLastUpdatedDate"/></xsl:attribute>

  		<xsl:for-each select="Property" >
  			<xsl:call-template name="RemarksToClaimsRepInfo"/>
  		</xsl:for-each>

      <SCRIPT>ADS_Buttons('content12');</SCRIPT>
    </DIV>

  </DIV>
  <!-- End Tabs1 -->

<!-- Start Tabs2 -->
<DIV unselectable="on" style="position:absolute; visibility:visible; top:205px; left:0px">
  <DIV unselectable="on" id="tabs2" class="apdtabs">
    <SPAN unselectable="on" id="tab21" class="tab2">Involved (<xsl:value-of select="count(Property/Involved[@InvolvedID &gt; 0])"/>)</SPAN>
    <SPAN unselectable="on" id="tab23" class="tab2">Estimates</SPAN>
    <xsl:if test="$ContextSupported = 1">
    <SPAN unselectable="on" id="tab24" class="tab2">Documents</SPAN>
    </xsl:if>
  </DIV>

    <xsl:choose>
        <xsl:when test="contains($InvolvedCRUD, 'R')">
            <!-- This will allow the user to atleast browse thru the Property involved List and the Property page will be readonly. -->
            <xsl:value-of select="js:setPageEnabled()"/>
            <xsl:value-of select="js:SetCRUD( '_RU_' )"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="js:SetCRUD( $InvolvedCRUD )"/>
        </xsl:otherwise>
    </xsl:choose>
  <!-- Start Involved Info -->
  <DIV unselectable="on" class="content2" id="content21" name="PropertyInvolved" Update="uspPropertyInvolvedUpdDetail" Insert="uspPropertyInvolvedInsDetail" Delete="uspPropertyInvolvedDel" Many="true" onfocusin="DivOnFocusIn(this)" style="visibility:visible; width:714px; height:234px;">
      <xsl:attribute name="CRUD">
        <xsl:choose>
          <xsl:when test="$pageReadOnly = 'true'">____</xsl:when>
          <xsl:otherwise><xsl:value-of select="$InvolvedCRUD"/></xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
		<xsl:attribute name="Count"><xsl:value-of select="count(Property/Involved[@InvolvedID &gt; 0])"/></xsl:attribute>

		<xsl:call-template name="InvolvedInfo"/>

		<SCRIPT>ADS_Buttons('content21');</SCRIPT>
  </DIV>

  <xsl:if test="$pageReadOnly = 'true'">
    <xsl:value-of select="js:setPageDisabled()"/>
  </xsl:if>
	<!-- Start Estimates Info -->
  <DIV unselectable="on" class="content2" id="content22" name="PropertyEstimates" style="width:726px; height:254px; padding:6px;" onfocusin="DivOnFocusIn(this)">
		<xsl:attribute name="CRUD"><xsl:value-of select="____"/></xsl:attribute>
		<xsl:attribute name="Count">0</xsl:attribute>
         <xsl:choose>
            <xsl:when test="contains($DocumentCRUD, 'R')">
        		<xsl:call-template name="EstimatesInfo">
                    <xsl:with-param name="myPermissions"><xsl:value-of select="$DocumentCRUD"/></xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <div style="width:100%;text-align:center">
                <br/>
                <font color="#FF0000">You do not have permissions to view this tab.</font>
                <br/>
                Please contact your supervisor.
                </div>
            </xsl:otherwise>
         </xsl:choose>

		<SCRIPT>ADS_Buttons('content22');</SCRIPT>
  </DIV>

  <!-- Start Documents -->
  <xsl:if test="$ContextSupported = 1">
  <DIV unselectable="on" class="content2" name="Documents" id="content23" CRUD="____" style="width:726px; height:253px;" onfocusin="DivOnFocusIn(this)">
    <xsl:value-of select="$ContextSupported"/>
    <IFRAME id="ifrmDocuments" onreadystatechange='top.IFStateChange(this);' style='width:724px; height:250px; top: 0px; left:0px; border:0px; margin:0px;  position:absolute; background:transparent; overflow:hidden;' allowtransparency='true' >
      <xsl:attribute name="src">Documents.asp?Lynxid=<xsl:value-of select="$LynxID"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>PertainsTo=<xsl:value-of select="$PertainsTo"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>InsuranceID=<xsl:value-of select="$InsuranceCompanyID"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>ClaimAspectID=<xsl:value-of select="$ClaimAspectID"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>EntityName=Property<xsl:text disable-output-escaping="yes">&amp;</xsl:text>EntityNumber=<xsl:value-of select="$PropertyNumber"/></xsl:attribute>
    </IFRAME>
    <SCRIPT>ADS_Buttons('content24');</SCRIPT>
  </DIV>
  </xsl:if>

</DIV>
<!-- End Tabs2 -->

</DIV>

<DIV id="divTooltip"> </DIV>
</BODY>
</HTML>

</xsl:template>

	<!-- Gets the Description Info -->
  <xsl:template name="DescriptionInfo" >
    <TABLE class="paddedTable" unselectable="on" border="0" cellspacing="0" cellpadding="0">
      <TR>
        <TD unselectable="on" valign="top">Desc.:</TD>
        <TD unselectable="on" colspan="3">
					<xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtPropDesc',60,3,'PropertyDescription',.,'','',1)"/>
        </TD>
        <TD unselectable="on" valign="top">Damage:</TD>
        <TD unselectable="on" colspan="4">
					<xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtPropDamage',50,3,'DamageDescription',.,'','',2)"/>
        </TD>
      </TR>
      <TR>
        <TD unselectable="on" colspan="7">
					<IMG unselectable="on" src="images/spacer.gif" border="0" height="2" width="1"/>
				</TD>
      </TR>
      <TR>
        <TD unselectable="on">Name:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropName',25,'Name',.,'','',3)"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
        <TD unselectable="on">Address:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropAddress1',22,'LocationAddress1',.,'','',8)"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
        <TD unselectable="on">City:</TD>
        <TD unselectable="on">
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropCity',12,'LocationCity',.,'','',10)"/>
        </TD>
      <TD unselectable="on">
            Party:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              <xsl:variable name="ExposureCD" select="@ExposureCD"/>
              <xsl:value-of select="/Root/Reference[@List='Exposure' and @ReferenceID=$ExposureCD]/@Name"/>
        <!--Party:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <br/>
        Coverage:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;-->
      </TD>
      </TR>
      <TR valign="top">
        <TD unselectable="on">Phone:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropLocationPhoneAC',1,'LocationAreaCode',., '', 10,4)"/>
					<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtPropLocationPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text>

					- <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropLocationPhoneEX',1,'LocationExchangeNumber',., '', 10,5)"/>
					<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtPropLocationPhoneNM, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text>

					- <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropLocationPhoneNM',2,'LocationUnitNumber',., '', 10,6)"/>
					<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtPropLocationPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>

          <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropLocationPhoneXT',3,'LocationExtensionNumber',., '', 10,7)"/>
					<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
        </TD>
        <TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD unselectable="on">
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropAddress2',22,'LocationAddress2',.,'','',9)"/>
        </TD>
        <TD unselectable="on">State:</TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropState',1,'LocationState',.,'',12,11)"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Zip:
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropZip',5,'LocationZip',.,'',10,12)"/>
					<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtPropState', 'txtPropCity');"></xsl:text>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
        <TD unselectable="on"><xsl:attribute name="nowrap"/>
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>Coverage:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
            <td>
              <xsl:variable name="rtCov" select="@CoverageProfileCD"/>
              <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('CoverageProfileCD',2,'onSelectChange',string($rtCov),1,2,/Root/Reference[@List='CoverageProfile'],'Name','ReferenceID','90','','','',13,1)"/>
            </td>
          </tr>
          <tr>
            <td nowrap="">Estimate Damage:</td>
            <td>
    					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropEst',10,'DamageAmt',.,'','',14)"/>
            </td>
          </tr>
        </table>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

	<!-- Gets the Property Contact Info -->
  <xsl:template name="PropertyContactInfo" >
      <TABLE class="paddedTable" unselectable="on" border="0" cellspacing="0" cellpadding="0">
        <TR>
          <TD unselectable="on">Name:</TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropNameTitle',3,'NameTitle',.,'','',15)"/>
            <img unselectable="on" src="/images/spacer.gif" alt="" width="2" height="1" border="0"/>
          </TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContNameFirst',15,'NameFirst',.,'','',16)"/>
            <img unselectable="on" src="/images/spacer.gif" alt="" width="3" height="1" border="0"/>
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContNameLast',15,'NameLast',.,'','',17)"/>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
          <TD unselectable="on">Address:</TD>
          <TD unselectable="on">
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContAddress1',15,'Address1',.,'','',21)"/>
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContAddress2',15,'Address2',.,'',6,22)"/>
         	</TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>Relation to Ins.:
          </TD>
          <TD unselectable="on">
					  <xsl:variable name="rtContactRelToIns" select="@InsuredRelationID"/>
            <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('InsuredRelationID_1',2,'onSelectChange',number($rtContactRelToIns),1,5,/Root/Reference[@List='ContactRelationToInsured'],'Name','ReferenceID',0,45,'','',26)"/>
					</TD>
        </TR>
        <TR>
          <TD unselectable="on">Phone:</TD>
          <TD unselectable="on" colspan="2"><xsl:attribute name="nowrap"/>
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContactDayPhoneAC',1,'DayAreaCode',., '', 10, 18)"/>
						<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtPropContactDayPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text>
						 - <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContactDayPhoneEX',1,'DayExchangeNumber',., '', 10,18)"/>
						 <xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtPropContactDayPhoneNM, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text>
						 - <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContactDayPhoneNM',2,'DayUnitNumber',., '', 10,19)"/>
						<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtPropContactDayPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
            <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContactDayPhoneXT',3,'DayExtensionNumber',., '', 10,20)"/>
						<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
          </TD>
          <TD unselectable="on">City:</TD>
          <TD unselectable="on">
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContCity',15,'AddressCity',.,'','',23)"/>
          </TD>
          <TD unselectable="on">State:</TD>
          <TD unselectable="on"><xsl:attribute name="nowrap"/>
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContState',2,'AddressState',.,'',12,24)"/>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Zip:
						<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtPropContZip',7,'AddressZip',.,'',10,25)"/>
						<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtPropContState', 'txtPropContCity');"></xsl:text>
          </TD>
        </TR>
      </TABLE>
  </xsl:template>

	<!-- Gets the Remarks to Claims Rep Info -->
  <xsl:template name="RemarksToClaimsRepInfo" >
      <TABLE class="paddedTable" unselectable="on" border="0" cellspacing="0" cellpadding="0">
        <TR>
          <TD unselectable="on">
						<xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtPropRemarks',130,2,'Remarks',.,'','',27)"/>
          </TD>
        </TR>
      </TABLE>
  </xsl:template>

	<!-- Gets the Involved Info -->
  <xsl:template name="InvolvedInfo" >
    <DIV unselectable="on" id="content21Sel" class="selBox" style="z-index:1; width:716px; height:234px;">
      <DIV unselectable="on" style="position:relative; left:10px; top:-10px; z-index:100000">
				<xsl:variable name="InvolvedName">
          <xsl:if test="count(Property/Involved[@InvolvedID &gt; 0]) > 0">

  					<xsl:for-each select="Property/Involved">

              <xsl:choose>
                <xsl:when test="@BusinessName!=''">
          				<xsl:value-of select="@BusinessName"/>
                </xsl:when>
                <xsl:when test="@BusinessName=''">
          				<xsl:value-of select="@NameFirst"/>
          				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          				<xsl:value-of select="@NameLast"/>
                </xsl:when>
              </xsl:choose>

      				<xsl:text> [</xsl:text>
          			<xsl:for-each select="InvolvedType">
          				<xsl:value-of select="@InvolvedTypeName"/>
          				<xsl:if test="not(position()=last())"><xsl:text>, </xsl:text></xsl:if>
          			</xsl:for-each>
      				<xsl:text>]</xsl:text>

  						<xsl:if test="not(position()=last())"><xsl:text>|</xsl:text></xsl:if>
  					</xsl:for-each>

          </xsl:if>
				</xsl:variable>
				<xsl:variable name="InvolvedIDs">
					<xsl:for-each select="Property/Involved">
						<xsl:value-of select="@InvolvedID"/>
						<xsl:if test="not(position()=last())"><xsl:text>|</xsl:text></xsl:if>
					</xsl:for-each>
				</xsl:variable>
				<xsl:value-of disable-output-escaping="yes" select="js:AddSelect('PropertyInvolved',2,'onInvolvedChange',0,1,2,string($InvolvedName),'',string($InvolvedIDs),'','100','','',28)"/>
			</DIV>
			<IFRAME id="ifrmPropertyInvolved" src="blank.asp" style='width:704px; height:214px; border:0px; visibility:hidden; position:absolute; background:transparent; overflow:hidden' allowtransparency='true' tabstop='false' tabindex='-1'>
			</IFRAME>
		</DIV>
  </xsl:template>

	<!-- Gets the Estimates Info -->
  <xsl:template name="EstimatesInfo" >
  	<TABLE unselectable="on" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSortEst')" width="100%" border="1" cellspacing="0">
      <TBODY>
  	    <TR class="QueueHeader" style="height:22px">
  				<TD unselectable="on" class="TableSortHeader" width="80" type="Number">Number</TD>
  				<TD unselectable="on" class="TableSortHeader" width="120" type="String">Type</TD>
  				<TD unselectable="on" class="TableSortHeader" width="200" type="String">Imaged On</TD>
  				<TD unselectable="on" class="TableSortHeader" width="300" type="String">Created By</TD>
  			</TR>
  		</TBODY>
    </TABLE>
  	<DIV unselectable="on" class="autoflowTable" style="height:216px;">
  	 <TABLE unselectable="on" id="tblSortEst" class="GridTypeTable" width="100%" cellspacing="1" border="0" cellpadding="0">
  	   <TBODY bgColor1="ffffff" bgColor2="fff7e5">
  			<xsl:for-each select="Property/Document[@DocumentID &gt; 0]"	>
  				<xsl:call-template name="Documents"/>
  			</xsl:for-each>
  	   </TBODY>
  	 </TABLE>
  	</DIV>
	</xsl:template>

	<xsl:template name="Documents">
        <xsl:param name="myPermissions"/>
			<!-- <TR onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' onClick='GridClick(this)' onDblClick='GridSelect(this)'> -->
	    <TR>
			<xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
				<TD unselectable="on" class="GridTypeTD" width="80"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
					<xsl:value-of select="@DocumentID"/>
				</TD>
				<TD unselectable="on" class="GridTypeTD" width="120"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
					<xsl:value-of select="@DocumentTypeName"/>
				</TD>
		    <TD unselectable="on" class="GridTypeTD" width="200"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
					<xsl:value-of select="js:UTCConvertDate(string(current()/@ImageDate))" /><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
					<xsl:value-of select="js:UTCConvertTime(string(current()/@ImageDate))"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
					<xsl:text disable-output-escaping="yes">&lt;IMG unselectable='on' src='/images/image.gif' width='17' height='17' style='cursor:hand' onClick="ShowImage('</xsl:text><xsl:value-of select="@ImageLocation"/><xsl:text disable-output-escaping="yes">');"></xsl:text>
				</TD>
		    <TD unselectable="on" class="GridTypeTD" width="272"><xsl:attribute name="nowrap"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
					<xsl:value-of select="@CreatedUserNameFirst"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:value-of select="@CreatedUserNameLast"/> - <xsl:value-of select="@CreatedUserRoleName"/>
				</TD>
			</TR>
		</xsl:template>
</xsl:stylesheet>
