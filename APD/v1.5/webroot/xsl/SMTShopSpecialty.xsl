<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:js="urn:the-xml-files:xslt"
	id="Billing">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- This page is a child of SMTDetailLevel.  This page resides in that page's 'ifrmContent' iframe and
       is dependent on global variables in that page to maintain state -->

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserID"/>
<xsl:param name="ShopID"/>

<xsl:template match="/Root">

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTShopSpecialtyGetListXML,SMTShopSpecialty.xsl,394   -->

<HEAD>
  <TITLE>Shop Maintenance</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>


<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/SMTValid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->

<script language="javascript">

var gsUserID = '<xsl:value-of select="$UserID"/>';
var gsBillingID = '<xsl:value-of select="@BillingID"/>';
var gsPageFile = "SMTShopSpecialty.asp";
var gsShopCRUD = '<xsl:value-of select="$ShopCRUD"/>';

<![CDATA[

function InitPage(){
	top.gsPageFile = gsPageFile;
  parent.btnDelete.style.visibility = "hidden";

  if (gsShopCRUD.indexOf("U") > -1){
    parent.btnSave.style.visibility = "visible";
	  parent.btnSave.disabled = false;
  }
  else
    parent.btnSave.style.visibility = "hidden";


	parent.tab24.className = "tabactive1";
	parent.gsPageName = "Shop Specialties";
	onpasteAttachEvents();

	var aInputs = document.all.tags("INPUT");
    for (var i=0; i < aInputs.length; i++) {
        aInputs[i].attachEvent("onclick", SetDirty);
        if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
    }
}

function SetDirty(){
	parent.gbDirtyFlag = true;
}

function IsDirty(){
	Save();
}

function Save(){
	var obj= document.all.tblSpecialty;
	var cBox;
	var sSpecialtyIDs = "";
	for (i=0; i<obj.rows.length;i++){
		cBox = obj.rows[i].cells[0].children[0]
		if (cBox.getAttribute("checked"))
			sSpecialtyIDs += cBox.getAttribute("SpecialtyID") + ",";
	}
	frmSpecialty.txtSpecialtyIDs.value = sSpecialtyIDs;
	parent.btnSave.disabled = true;
	frmSpecialty.action += "?mode=update";
	frmSpecialty.submit();
	parent.gbDirtyFlag = false;
}

]]>

</script>

</HEAD>

<body class="bodyAPDsub" onload="InitPage();" style="overflow:hidden; margin-left:0; margin-right:0; margin-top:0;">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>

<div style="background-color:#FFFAEB; height:450px;">
<IMG src="/images/spacer.gif" width="6" height="6" border="0" />

<form id="frmSpecialty" method="post" style="overflow:hidden">
 <table id="tblSpecialty" cellpadding="2"  cellspacing="0" bordercolor="#cccccc" border="1" rules="ALL" WIDTH="100%" nowrap="on">
    <xsl:apply-templates select="Specialty"/>
  </table>
  <input type="hidden" id="txtSpecialtyIDs" name="SpecialtyIDs"/>
  <input type="hidden" id="txtSysLastUserID" name="SysLastUserID">
    <xsl:attribute name="value"><xsl:value-of select="$UserID"/></xsl:attribute>
  </input>
  <input type="hidden" id="txtShopID" name="ShopID">
    <xsl:attribute name="value"><xsl:value-of select="@ShopID"/></xsl:attribute>
  </input>
  <input type="hidden" id="txtCallBack" name="CallBack"></input>
</form>
</div>

<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

</body>
</HTML>
</xsl:template>

<xsl:template match="Specialty">
  <xsl:variable name="SpecialtyID" select="@SpecialtyID"/>
  <tr>
    <xsl:attribute name="bgcolor">
	  <xsl:choose>
		<xsl:when test="position() mod 2 = 1"><xsl:text>#fff7e5</xsl:text></xsl:when>
		<xsl:otherwise><xsl:text>white</xsl:text></xsl:otherwise>
	  </xsl:choose>
	</xsl:attribute>
    <td>
	  <input type="checkbox">
	    <xsl:if test="boolean(/Root/ShopSpecialty[@SpecialtyID=$SpecialtyID])">
		  <xsl:attribute name="checked"/>
		</xsl:if>
	    <xsl:attribute name="id"><xsl:value-of select="concat('chkSpecialty', @SpecialtyID)"/></xsl:attribute>
		<xsl:attribute name="SpecialtyID"><xsl:value-of select="@SpecialtyID"/></xsl:attribute>
	  </input>
	</td>
	<td><xsl:value-of select="@Name"/></td>
  </tr>
</xsl:template>

</xsl:stylesheet>
