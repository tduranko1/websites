<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="UserList">
<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="msxsl/msjs-client-library.js"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:param name="CRUDInfo"/>

<xsl:template match="/Root">
<html>
<head>
<!-- STYLES -->
<LINK rel="stylesheet" href="../css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="../css/tabs.css" type="text/css"/>

<!-- Script modules -->
<SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
	var sCRUDInfo = "<xsl:value-of select="$CRUDInfo"/>";
<![CDATA[
	// button images for ADD/DELETE/SAVE
	preload('buttonAdd','/images/but_ADD_norm.png');
	preload('buttonAddDown','/images/but_ADD_down.png');
	preload('buttonAddOver','/images/but_ADD_over.png');
	preload('buttonDel','/images/but_DEL_norm.png');
	preload('buttonDelDown','/images/but_DEL_down.png');
	preload('buttonDelOver','/images/but_DEL_over.png');
	preload('buttonSave','/images/but_SAVE_norm.png');
	preload('buttonSaveDown','/images/but_SAVE_down.png');
	preload('buttonSaveOver','/images/but_SAVE_over.png');

  // Page Initialize
  function PageInit() 
  { 
    top.closeClaim(); 
    tabsIntabs[0].tabs.onchange=tabChange;
    tabsIntabs[0].tabs.onBeforechange=tabBeforeChange;
  }

  function handleJSError(sFunction, e)
  {
    var result = "An error has occurred on the page.\nFunction = " + sFunction + "\n";
    for (var i in e) {
      result += "e." + i + " = " + e[i] + "\n";
    }
    ClientError(result);
  }

  function UserGridSelect(oRow)
  {
    try {
  	  var strUserID = oRow.cells[5].innerText;
    	document.location="UserDetail.asp?UserID=" + strUserID;
    }
    catch(e) {
      handleJSError("UserGridSelect", e)
    }
  }

	var inADS_Pressed = false;		// re-entry flagging for buttons pressed
	// Handle click of Add button
	function ADS_Pressed(sAction, sName)
	{
    try {
  		if (inADS_Pressed == true) return;
  		inADS_Pressed = true;

  		var oDiv = document.getElementById(sName);

  		if (sAction == "Insert") {
  			if (oDiv.Many == "true") {
  				parent.oIframe.frameElement.src = "/admin/UserDetail.asp?UserID=New";
  			}
  		}
      else {
        var userError = new Object();
        userError.message = "Unhandled ADS action: Action = " + sAction;
        userError.name = "MiscellaneousException";
        throw userError;
			}

  		inADS_Pressed = false;
    }
    catch(e) {
      handleJSError("ADS_Pressed", e);
    }
	}
    
    function tabBeforeChange(obj, tab)
    {
        if (tab.id == "tab12"){
            var obj;
            obj = document.getElementById("ifrmRoles");
            obj.style.visibility = "visible";
            obj.style.display = "inline";
            obj.src = "roleList.asp";
        }
        else {
            var obj;
            obj = document.getElementById("ifrmRoles");
            obj.style.visibility = "hidden";
            //obj.style.display = "inline";
        }
    }
    
]]>
</SCRIPT>

<title>APD User and Role Management</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
</head>

<BODY unselectable="on" class="bodyAPDSub" onLoad="tabInit(false,'#FFFFFF'); InitFields(); PageInit()">
<!-- Start Tabs1 -->
<DIV unselectable="on" style="position:absolute; visibility: visible; top: 0px; left: 0px">

  <DIV unselectable="on" id="tabs1" class="apdtabs">
  	<SPAN unselectable="on" id="tab11" class="tab1">User List</SPAN>
  	<SPAN unselectable="on" id="tab12" class="tab1">Role List</SPAN>
  </DIV>
  
  <!-- Start User List -->
    <DIV unselectable="on" class="content1" id="content11" name="UserList" Many="true" style="visibility:visible; width:740px; height:490px;">
        <xsl:attribute name="CRUD"><xsl:value-of select="$CRUDInfo"/></xsl:attribute>
        <TABLE class="UserMiscInputs" onClick="sortColumn(event, 'tblUsers')" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
            <colgroup>
                <col width="103px"/>
                <col width="67px"/>
                <col width="283px"/>
                <col width="175px"/>
                <col width=""/>
            </colgroup>
            <TBODY>
        		<tr class="QueueHeader" style="height:21px">
         			<td class="TableSortHeader" type="String">Csr Number</td>
         			<td class="TableSortHeader" type="String">Active</td>
         			<td class="TableSortHeader" type="String">User Name</td>
         			<td class="TableSortHeader" type="String">Active/Inactive Since</td>
         			<td class="TableSortHeader" type="String">Supervisor</td>
        		</tr>
            </TBODY>
        </TABLE>
        <DIV unselectable="on" style="width:730px; height:460px; overflow: auto;">
        	<TABLE id="tblUsers" class="GridTypeTable" width="100%" cellspacing="1" border="0" cellpadding="3" style="table-layout:fixed;">
            <colgroup>
                <col width="100px"/>
                <col width="67px"/>
                <col width="283px"/>
                <col width="175px"/>
                <col width=""/>
            </colgroup>
        	<TBODY bgColor1="ffffff" bgColor2="fff7e5">
                <xsl:for-each select="/Root/User">
                    <TR onMouseOut='GridMouseOut(this)' onMouseOver='GridMouseOver(this)' onClick='UserGridSelect(this)' style='height:21px;'>
                        <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
                        <!-- <xsl:choose>
                            <xsl:when test="position() mod 2 = 0">
                                <xsl:attribute name="bgcolor">#fff7e5</xsl:attribute>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="bgcolor">#ffffff</xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose> -->
                        <td style="border:1px ridge;"><xsl:value-of select="@CSRNo"/></td>
                        <td style="border:1px ridge;">
                            <xsl:choose>
                                <xsl:when test="@Active = 1">
                                    <xsl:text>Yes</xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>No</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td style="border:1px ridge;"><xsl:value-of select="concat(@NameLast, ', ', @NameFirst)"/></td>
                        <td style="border:1px ridge;">
                            <xsl:call-template name="formatDateTime">
                                <xsl:with-param name="sDateTime">
                                    <xsl:value-of select="@AccessBeginDate"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </td>
                        <td align="center" style="border:1px ridge;">
                            <xsl:choose>
                                <xsl:when test="@SupervisorFlag = 1">
                                    <xsl:text>Yes</xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>No</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                        <td style="display:none"><xsl:value-of select="@UserID"/></td>
                    </TR>
                </xsl:for-each>
             </TBODY>
             </TABLE>
        </DIV>
        <SCRIPT>
          ADS_Buttons('content11'); 
          document.getElementById('crudDivcontent11').style.visibility="inherit";
          //if the user has read and create permission, we make the add button enabled. The hierarchial permissions
          //we did needs the update and create permission to exist together to show Add button, which caused this button
          //to disappear.
          if (sCRUDInfo.indexOf('C') != -1 &amp;&amp; sCRUDInfo.indexOf('R') != -1)
            document.getElementById('content11_ADD').style.visibility = "inherit";
        </SCRIPT>
    </DIV>
    <DIV unselectable="on" class="content1" id="content12" name="RoleList" Many="true" CRUD="" style="visibility:hidden; width:740px; height:490px;">
         <IFRAME id="ifrmRoles" src="../blank.asp" style='width:730px; height:480px; border:0px; visibility:inherit; position:absolute; background:transparent; overflow:hidden' allowtransparency='true' >
         </IFRAME>
    </DIV>
</DIV>
</BODY>
</html>
</xsl:template>

<xsl:template name="formatDateTime">
    <xsl:param name="sDateTime"/>

    <xsl:variable name="sYear" select="substring($sDateTime, 1,4)"/>
    <xsl:variable name="sMonth" select="substring($sDateTime, 6,2)"/>
    <xsl:variable name="sDay" select="substring($sDateTime, 9,2)"/>
    
    <xsl:value-of select="concat($sMonth, '/', $sDay, '/', $sYear)"/>
    
    <xsl:variable name="sHH1" select="substring($sDateTime, 12,2)"/>
    <xsl:if test="number($sHH1) &gt; 12">
        <xsl:variable name="sHH" select="format-number(number($sHH1) - 12, '00')"/>
        <xsl:variable name="sAMPM" select="PM"/>
        <xsl:variable name="sMM" select="substring($sDateTime, 15,2)"/>
        <xsl:variable name="sSS" select="substring($sDateTime, 18,2)"/>
        <xsl:value-of select="concat(' ', $sHH, ':', $sMM, ':', $sSS, ' PM')"/>
    </xsl:if>
    <xsl:if test="number($sHH1) &lt; 13">
        <xsl:variable name="sHH" select="$sHH1"/>
        <xsl:variable name="sMM" select="substring($sDateTime, 15,2)"/>
        <xsl:variable name="sSS" select="substring($sDateTime, 18,2)"/>
        <xsl:value-of select="concat(' ', $sHH, ':', $sMM, ':', $sSS, ' AM')"/>
    </xsl:if>
</xsl:template>
</xsl:stylesheet>
