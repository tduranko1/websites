<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="SMtdealerInfo">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:import href="PMDDateSearch.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserID"/>
<xsl:param name="Deleted"/>
<xsl:param name="mode"/>


<xsl:template match="/Root">

<xsl:if test="$Deleted='yes'"><script>parent.window.close();</script></xsl:if>

<xsl:variable name="DealerID" select="@DealerID"/>
<xsl:if test="$mode = 'edit'">
  <script>
    window.returnValue = "<xsl:value-of select='$DealerID'/>";
    window.close();
  </script>
</xsl:if>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSMTDealerGetdetailXML,SMTDealerInfo.xsl,887   -->

<HEAD>
  <TITLE>Dealer Maintenance</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>


<!-- CLIENT SCRIPTS -->
<!--<xsl:if test="$mode != 'wizard'"><SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT></xsl:if>-->
<SCRIPT language="JavaScript" src="/js/SMTValid.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">
var gsPageFile = "SMTDealerInfo.asp";
var gsShopCRUD = '<xsl:value-of select="$ShopCRUD"/>';

<!-- prep data to be sent to Header Info tab -->
if (parent.gsMode != "wizard"){
  top.gsDealerID = '<xsl:value-of select="$DealerID"/>';

  top.gsInfoName = top.StringPrep('<xsl:value-of select='translate(translate(Dealer/@Name, "&apos;", "|"), "\", "/")'/>');
  top.gsInfoAddress = top.StringPrep('<xsl:value-of select='translate(translate(Dealer/@Address1, "&apos;", "|"), "\", "/")'/>' + ' ' + '<xsl:value-of select='translate(translate(Dealer/@Address2, "&apos;", "|"), "\", "/")'/>');

  <xsl:variable name="gsCity">
    <xsl:value-of select="normalize-space(Dealer/@AddressCity)"/>
    <xsl:if test="Dealer/@AddressCity != '' and Dealer/@AddressState != ''">, </xsl:if>
    <xsl:value-of select="Dealer/@AddressState"/>
    <xsl:if test="Dealer/@AddressCity != '' or Dealer/@AddressState != ''"><xsl:text>  </xsl:text></xsl:if>
    <xsl:value-of select="Dealer/@AddressZip"/>
  </xsl:variable>

  top.gsInfoCity = top.StringPrep('<xsl:value-of select='translate(translate($gsCity, "&apos;", "|"), "\", "/")'/>');
  top.gsInfoPhone = '<xsl:value-of select="Dealer/@PhoneAreaCode"/>' + "-" + '<xsl:value-of select="Dealer/@PhoneExchangeNumber"/>' + "-" + '<xsl:value-of select="Dealer/@PhoneUnitNumber"/>';
  top.gsPageFile = gsPageFile;
}
<!-- end Header data prep -->

<![CDATA[

//init the table selections, must be last
function initPage(){
  InitEditPage();
  onpasteAttachEvents();

  //set dirty flags associated with all inputs and selects and checkboxes.
  var aInputs = null;
  var obj = null;
  aInputs = document.all.tags("INPUT");
  for (var i=0; i < aInputs.length; i++){
    if (aInputs[i].type=="text"){
      aInputs[i].attachEvent("onchange", SetDirty);
	    aInputs[i].attachEvent("onkeypress", SetDirty);
      aInputs[i].attachEvent("onpropertychange", SetDirty);
    }

    if (aInputs[i].type=="checkbox")
	  aInputs[i].attachEvent("onclick", SetDirty);

    if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
  }
  aInputs = document.all.tags("SELECT");
  for (var i=0; i < aInputs.length; i++){
    aInputs[i].attachEvent("onchange", SetDirty);
    if (gsShopCRUD.indexOf("U") == -1) aInputs[i].setAttribute("disabled", "true");
  }

  if (gsShopCRUD.indexOf("U") != -1) frmDealerInfo.txtName.focus();
}

function InitEditPage(){
	parent.btnSave.style.visibility = (parent.gsMode == "wizard") || (gsShopCRUD.indexOf("U") > -1) ? "visible" : "hidden";
	parent.btnDelete.style.display = (parent.gsMode == "wizard") || (gsShopCRUD.indexOf("D") == -1) ? "none" : "inline";
	parent.btnDelete.style.visibility = "visible";
  parent.btnCancel.style.display = "inline";
  parent.btnCancel.style.visibility = parent.gsMode == "wizard" ? "visible" : "hidden";
  parent.btnSave.disabled = false;
  parent.btnDelete.disabled = false;
	parent.btnCancel.disabled = false;
  parent.document.all.tab11.className = "tabactive1";
	parent.gsPageName = "Dealer Info";
	if (parent.gsMode != "wizard")
		top.SetInfoHeader("  Dealer ID:", top.gsDealerID, top.gsInfoName, top.gsInfoAddress, top.gsInfoCity, top.gsInfoPhone);
	//else
	//	top.layerEntityData.style.visibility = "hidden";
}

function Cancel(){
	//parent.window.navigate(top.CancelURL());
  parent.window.close();
}

function SetDirty(){
	parent.gbDirtyFlag = true;
}

function txtAddressZip_onBlur(){
	IsValidZip(frmDealerInfo.txtAddressZip);
	ValidateZip(frmDealerInfo.txtAddressZip, 'selState', 'AddressCity');
	ValidateZipToCounty(frmDealerInfo.txtAddressZip, 'AddressCounty');
}

function ValidateAllPhones() {
  if (validatePhoneSections(document.all.PhoneAreaCode, document.all.PhoneExchangeNumber, document.all.PhoneUnitNumber) == false) return false;
	if (validatePhoneSections(document.all.FaxAreaCode, document.all.FaxExchangeNumber, document.all.FaxUnitNumber) == false) return false;
    if (isNumeric(document.all.PhoneExtensionNumber.value) == false){
        parent.ClientWarning("Invalid numeric value specified. Please try again.");
        document.all.PhoneExtensionNumber.focus();
        document.all.PhoneExtensionNumber.select();
        return false;
    }

	if (isNumeric(document.all.FaxExtensionNumber.value) == false){
        parent.ClientWarning("Invalid numeric value specified. Please try again.");
        document.all.FaxExtensionNumber.focus();
        document.all.FaxExtensionNumber.select();
        return false;
    }
    validateAreaCodeSplit(document.all.PhoneAreaCode, document.all.PhoneExchangeNumber);
    validateAreaCodeSplit(document.all.FaxAreaCode, document.all.FaxExchangeNumber);
    return true;
}

function IsDirty(){
	Save();
}

function Save(){
    if (frmDealerInfo.txtName.value == ""){
        parent.ClientWarning("A value is required in the \"Name\" field.");
        frmDealerInfo.txtName.focus();
        return false;
	}

	if (frmDealerInfo.txtAddressCity.value == ""){
        parent.ClientWarning("A value is required in the \"City\" field.");
        frmDealerInfo.txtAddressCity.focus();
        return false;
	}

	if (frmDealerInfo.selState.value == ""){
        parent.ClientWarning("A value is required in the \"State\" field.");
        frmDealerInfo.selState.focus();
        return false;
	}

	if (frmDealerInfo.txtAddressZip.value == ""){
        parent.ClientWarning("A value is required in the \"Zip\" field.");
        frmDealerInfo.txtAddressZip.focus();
        return false;
	}

	if (!ValidateAllPhones())
		return false;

	parent.btnSave.disabled = true;
	parent.btnCancel.disabled = true;
	parent.btnDelete.disabled = true;
	frmDealerInfo.action += parent.gsMode == "wizard" ? "?mode=add" : "?mode=update";
	frmDealerInfo.submit();
	parent.gbDirtyFlag = false;
}

function Delete(){
  var msg = "Are you sure you want to delete this Dealer?  Delete cannot be undone.  Click 'Yes' to delete.";
	var sDelete = YesNoMessage("Dealer Maintenance", msg)
	if (sDelete == "Yes"){
		parent.btnDelete.disabled = true;
		frmDealerInfo.action += "?mode=delete";
		frmDealerInfo.submit();
		parent.gbDirtyFlag = false;
	}
}


]]>

</SCRIPT>
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>

</HEAD>

<BODY class="bodyAPDSubSub" unselectable="on" onLoad="initPage();" bgcolor="#FFFAEB" style="overflow:hidden; margin-left:0; margin-right:0; margin-top:0;">
  <div style="background-color:#FFFAEB;height:475px">
  <IMG src="/images/spacer.gif" width="6" height="12" border="0" />
  <!-- Render Business Info Data -->
  <FORM id="frmDealerInfo" method="post">
    <xsl:apply-templates select="Dealer"><xsl:with-param name="DealerID" select="@DealerID"/></xsl:apply-templates>

	<input type="hidden" id="txtSysLastUpdatedDate" name="SysLastUpdatedDate">
	  <xsl:attribute name="value"><xsl:value-of select="Dealer/@SysLastUpdatedDate"/></xsl:attribute>
	</input>

    <input type="hidden" id="txtSysLastUserID" name="SysLastUserID">
	  <xsl:attribute name="value"><xsl:value-of select="$UserID"/></xsl:attribute>
	</input>

	<input type="hidden" id="txtDealerID" name="DealerID"><xsl:attribute name="value"><xsl:value-of select="@DealerID"/></xsl:attribute></input>
  <input type="hidden" id="txtCallBack" name="CallBack"/>
	<input type="hidden" id="txtEnabledFlag" wizname="EnabledFlag" value="1"/>
  </FORM>
  <IMG src="/images/spacer.gif" width="1" height="4" border="0" />
  </div>

  <SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

</BODY>
</HTML>
</xsl:template>

<!-- Gets Dealer Info -->
<xsl:template match="Dealer">
  <xsl:param name="DealerID"/>

  <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
    <tr>
      <td>
        <table>
          <tr>
		    <td>*</td>
            <td>Name:</td>
            <td>
              <input type="text" id="txtName" name="Name" class="inputFld" size="51">
			    <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Dealer']/Column[@Name='Name']/@MaxLength"/></xsl:attribute>
	  	        <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
			  </input>
            </td>
          </tr>
          <tr>
		    <td></td>
            <td>Address1: </td>
            <td>
              <input type="text" id="txtAddress1" name="Address1" class="inputFld" size="51">
			    <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Dealer']/Column[@Name='Address1']/@MaxLength"/></xsl:attribute>
	  	        <xsl:attribute name="value"><xsl:value-of select="@Address1"/></xsl:attribute>
			  </input>
            </td>
          </tr>
          <tr>
		    <td></td>
            <td>Address2</td>
            <td>
              <input type="text" id="txtAddress2" name="Address2" class="inputFld" size="51">
			    <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Dealer']/Column[@Name='Address2']/@MaxLength"/></xsl:attribute>
	  	        <xsl:attribute name="value"><xsl:value-of select="@Address2"/></xsl:attribute>
			  </input>
            </td>
          </tr>
          <tr>
		    <td>*</td>
            <td>Zip: </td>
            <td>
              <input type="text" id="txtAddressZip" name="AddressZip" class="inputFld" size="9" maxlength="5" onblur="txtAddressZip_onBlur()" onkeypress="NumbersOnly(event)">
			    <xsl:attribute name="value"><xsl:value-of select="@AddressZip"/></xsl:attribute>
			  </input>
            </td>
          </tr>
          <tr>
		    <td>*</td>
            <td>City: </td>
            <td>
              <input type="text" id="txtAddressCity" name="AddressCity" class="inputFld" size="21">
			    <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Dealer']/Column[@Name='AddressCity']/@MaxLength"/></xsl:attribute>
	  	        <xsl:attribute name="value"><xsl:value-of select="@AddressCity"/></xsl:attribute>
			  </input>
            </td>
          </tr>
          <tr>
		    <td>*</td>
            <td>State: </td>
            <td>
              <select id="selState" name="AddressState" class="inputFld">
                <option></option>
				<xsl:for-each select="/Root/Reference[@ListName='State']">
				  <xsl:call-template name="BuildSelectOptions">
					<xsl:with-param name="current"><xsl:value-of select="/Root/Dealer/@AddressState"/></xsl:with-param>
				  </xsl:call-template>
			    </xsl:for-each>
              </select>
            </td>
		  </tr>
          <tr>
		    <td></td>
            <td>County:</td>
            <td>
              <input type="text" id="txtAddressCounty" name="AddressCounty" class="inputFld" size="31">
			    <xsl:attribute name="maxlength"><xsl:value-of select="//Metadata[@Entity = 'Dealer']/Column[@Name='AddressCounty']/@MaxLength"/></xsl:attribute>
	  	        <xsl:attribute name="value"><xsl:value-of select="@AddressCounty"/></xsl:attribute>
			  </input>
            </td>
          </tr>
        </table>
      </td>
      <td valign="top">
        <table>
          <tr>
            <td>Phone: </td>
            <td noWrap="noWrap">
              <xsl:call-template name="ContactNumbers"><xsl:with-param name="ContactMethod">Phone</xsl:with-param></xsl:call-template>
			</td>
          </tr>
          <tr>
            <td>Fax:</td>
            <td noWrap="noWrap">
              <xsl:call-template name="ContactNumbers"><xsl:with-param name="ContactMethod">Fax</xsl:with-param></xsl:call-template>
		    </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</xsl:template>


</xsl:stylesheet>

