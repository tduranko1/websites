<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://mycompany.com/mynamespace"
    id="PMDCycleTime">

<xsl:import href="msxsl/generic-template-library.xsl"/>
<xsl:output method="html" indent="yes" encoding="UTF-8"/>
<xsl:decimal-format NaN="0"/>
<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="ShopCRUD" select="Shop"/>

<xsl:param name="AssignmentID"/>

<xsl:template match="/Root">

<xsl:variable name="ShopLocationID" select="/Root/@ShopLocationID"/>
<xsl:variable name="BeginDate" select="/Root/@BeginDate"/>
<xsl:variable name="EndDate" select="/Root/@EndDate"/>
<xsl:variable name="AssignmentCode" select="/Root/@AssignmentCode"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspPMDCycleTimeGetDetailXML,PMDCycleTime.xsl,2550, '02/02/2002', '04/25/2004', 'B', 33   -->

<HEAD>
  <TITLE>Program Manager Desktop</TITLE>

  <LINK rel="stylesheet" href="/css/PMD.css" type="text/css"/>
  
  <style>
  	A{
		  color:blue
	  }
	  A:hover{
		  color : #B56D00;
		  font-family1 : Tahoma, Verdana, Geneva, Arial, Helvetica, sans-serif;
		  font-weight1 : bold;
		  font-size1 : 10px;
		  cursor : hand;
	  }
  </style>
  
<!-- CLIENT SCRIPTS -->
<SCRIPT language="JavaScript" src="/js/PMDValid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMD.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/PMDNavigate.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<!-- Page Specific Scripting -->
<SCRIPT language="JavaScript">


var gsShopLocationID = "<xsl:value-of select="$ShopLocationID"/>";
var gsBeginDate = "<xsl:value-of select="user:UTCConvertDate(string($BeginDate))"/>";
var gsEndDate = "<xsl:value-of select="user:UTCConvertDate(string($EndDate))"/>";
var gsAssignmentCode = "<xsl:value-of select="$AssignmentCode"/>";
var gsShopCRUD = "<xsl:value-of select="$ShopCRUD"/>";

var gsLynxID = "";
var gsClaimAspectID = "";
var gsAssignmentID = "<xsl:value-of select="$AssignmentID"/>";
var gsReferrer = "";
var gsEstimateURL = "";
var gsImageLocation = "";


<![CDATA[

var oActiveTab = null;
var iCurPage = iRecCount = iPgCount = 0;
var sOldBG = "";
//init the table selections, must be last
function initPage(){
  txtBeginDate.value = gsBeginDate;
  txtEndDate.value = gsEndDate;
  divStatus.style.left = (document.body.offsetWidth - 250) / 2;
  divStatus.style.top = (document.body.offsetHeight - 75) / 2;
  selectTab(tabCycleTime);
  
  if (gsAssignmentID != ""){
    //open the estimate for this assignment
    var oRows = tblCommonData.rows;
    for (var i = 0; i < oRows.length; i++){
      if (oRows[i].cells[7].innerText == gsAssignmentID){
        openEstimate(oRows[i]);
        break;
      }
    }
  }
}

function GridSelect1(oRow){
  parent.gsLynxID = oRow.cells[0].innerText;
  parent.gsClaimAspectID = oRow.cells[0].getAttribute("ClaimAspectID");
  parent.gsAssignmentID = oRow.cells[0].getAttribute("AssignmentID");
  parent.gsReferrer = "PMDCycleTime.asp";
  window.navigate("PMDEstimateList.asp?AssignmentID=" + parent.gsAssignmentID);
}

function selectTab(obj){
  if (oActiveTab){
    oActiveTab.className = "tab1";
    oActiveTab.style.top = oActiveTab.offsetTop - 2;
    oActiveTab.active = 0;
  }
  obj.className = "tabactive1";
  obj.style.top = obj.offsetTop + 2;
  obj.active = 1;
  oActiveTab = obj;
  
  spnCycleTimeHeader.style.display = "none";
  spnIndemnityHeader.style.display = "none";
  spnReinspectionHeader.style.display = "none";
  spnCommonData.style.display = "none";
  spnCycleTimeData.style.display = "none";
  spnIndemnityData.style.display = "none";
  spnReinspectionData.style.display = "none";
  divCycleTimeSummary.style.display = "none";
  divIndemnitySummary.style.display = "none";
  divReinspectionSummary.style.display = "none";
  
  
  switch (oActiveTab.innerText){
    case "Cycle Time":
      spnCycleTimeHeader.style.display = "inline";
      divCycleTimeSummary.style.display = "inline";
      break;
    case "Indemnity":
      spnIndemnityHeader.style.display = "inline";
      divIndemnitySummary.style.display = "inline";
      break;
    case "Reinspection":
      spnReinspectionHeader.style.display = "inline";
      divReinspectionSummary.style.display = "inline";
      break;
  }

  if (iRecCount > 0){
    spnCommonData.style.display = "inline";
    switch (oActiveTab.innerText){
      case "Cycle Time":
        spnCycleTimeData.style.display = "inline";
        break;
      case "Indemnity":
        spnIndemnityData.style.display = "inline";
        break;
      case "Reinspection":
        spnReinspectionData.style.display = "inline";
        break;
    }
  } 
}

function initData(){
    if (xmlAssignments) {
      xmlAssignments.setProperty("SelectionLanguage", "XPath");
      iRecCount = xmlAssignments.XMLDocument.firstChild.childNodes.length;
      
      if (iRecCount == 0){
        divNoData.style.display = "inline";
      }

      iCurPage = 1;
      
      if (iRecCount > 14){
        var iPageSize = 12;
        iPgCount = Math.ceil(iRecCount / iPageSize);
        tblCommonData.dataPageSize = tblCycleTimeData.dataPageSize = tblIndemnityData.dataPageSize = tblReinspectionData.dataPageSize = iPageSize;
        divData.style.height = "266px"
        divPagination.style.display = "inline";
        txtPage.innerText = "Page: 1 of " + iPgCount;
      }
    }  
}

function moveFirst(){
  iCurPage = 1;
  navPage();
}

function movePrev(){
  if (iCurPage > 1) iCurPage--;
  navPage();
}

function moveNext(){
  if (iCurPage < iPgCount) iCurPage++;
  navPage();
}

function moveLast(){
  iCurPage = iPgCount;
  navPage();
}

function navPage(){
  tblCommonData.firstPage();
  tblCycleTimeData.firstPage();
  tblIndemnityData.firstPage();
  tblReinspectionData.firstPage();
  for (var i = 1; i < iCurPage; i++){
    tblCommonData.nextPage();
    tblCycleTimeData.nextPage();
    tblIndemnityData.nextPage();
    tblReinspectionData.nextPage();
  }
  txtPage.innerText = "Page: " + iCurPage + " of " + iPgCount;
}

function initRec(){
  var oRows = tblCommonData.rows;
  var iRowCount = oRows.length;
  if (tblCommonData.readyState == "complete" && tblCycleTimeData.rows.length == iRowCount && 
      tblIndemnityData.rows.length == iRowCount && tblReinspectionData.rows.length == iRowCount){
    
    for (var i = 0; i < oRows.length; i++){
      oRows[i].bgColor = oRows[i].style.backgroundColor = 
        tblCycleTimeData.rows[i].style.backgroundColor = tblIndemnityData.rows[i].style.backgroundColor = 
        tblReinspectionData.rows[i].style.backgroundColor = (oRows[i].rowIndex % 2 != 0 ? "#fff7e5" : "#ffffff");
    }
  } else {
    window.setTimeout("initRec()", 100);
  }
}

function HighlightRow(obj){
  if (obj){
    sOldBG = obj.style.backgroundColor;
    var iRow = obj.rowIndex;
    tblCycleTimeData.rows[iRow].style.backgroundColor = tblIndemnityData.rows[iRow].style.backgroundColor = 
        tblReinspectionData.rows[iRow].style.backgroundColor = tblCommonData.rows[iRow].style.backgroundColor = "#FFEEBB";
  }
}

function UnHighlightRow(obj){
  if (obj){
    var iRow = obj.rowIndex;
    tblCycleTimeData.rows[iRow].style.backgroundColor = tblIndemnityData.rows[iRow].style.backgroundColor = 
        tblReinspectionData.rows[iRow].style.backgroundColor = tblCommonData.rows[iRow].style.backgroundColor = sOldBG;
  }
}

function btnSearch_onclick(){
  if (chkdate(txtBeginDate) == false){
    alert("Please enter a valid From Date.");
    return;
  }

  if (chkdate(txtEndDate) == false){
    alert("Please enter a valid End Date.");
    return;
  }

  window.navigate("PMDShopAssignment.asp?ShopLocationID=" + gsShopLocationID + 
                        "&BeginDate=" + txtBeginDate.value + "&EndDate=" + txtEndDate.value + "&AssignmentCode=" + txtAssignmentCode.value);
}

function showProgress(){
  divStatus.style.display = "inline";
}

function hideProgress(){
  divStatus.style.display = "none";
}

function openEstimate(obj){
  if (obj){
    var iRowIndex = obj.rowIndex;
    gsLynxID = tblCommonData.rows[iRowIndex].cells[8].innerText;
    gsClaimAspectID = tblCommonData.rows[iRowIndex].cells[9].innerText;
    gsAssignmentID = tblCommonData.rows[iRowIndex].cells[7].innerText;
    frmEstimateDetail.frameElement.style.display = "inline";
    showProgress();
    gsEstimateURL = "PMDEstimateList.asp?AssignmentID=" + gsAssignmentID;
    window.setTimeout("frmEstimateDetail.frameElement.src = gsEstimateURL", 100);
  }
}

function restoreEstimate(){  
  if (gsEstimateURL != ""){
    showProgress();
    window.setTimeout("frmEstimateDetail.frameElement.src = gsEstimateURL", 100);
  }
}

function hideEstimates(){
  frmEstimateDetail.frameElement.src = "/blank.asp";
  frmEstimateDetail.frameElement.style.display = "none";
}

function ShowPhotoViewer(){
	sHREF = "/Documents.asp?LynxID=" + gsLynxID + "&InsuranceID=" + gsInsuranceCompanyID + "&ClaimAspectID=" + gsClaimAspectID;
  window.showModelessDialog(sHREF, "", "dialogHeight:355px; dialogWidth:1038px; dialogTop:5px; dialogLeft:5px; resizable:yes; status:no; help:no; center:no;");
}

function ShowEstimateViewer(){
  if (gsImageLocation != ""){
  	var sHREF = "/EstimateDocView.asp?docPath=" + gsImageRootDir + gsImageLocation;
    window.showModelessDialog(sHREF, "", "dialogHeight:355px; dialogWidth:1038px; dialogTop:5px; dialogLeft:5px; resizable:yes; status:no; help:no; center:no;");
  }
}

function CheckDirty(){
  if (gbDirtyFlag) {
    var sSave = YesNoMessage(gsPageName, "Some information on this page has changed. Do you want to save the changes?");
    if (sSave == "Yes"){
      if (typeof(frmEstimateDetail.IsDirty == "function"))
        return frmEstimateDetail.IsDirty();
      else
        return false;
    }
      
  }
  gbDirtyFlag = false;
  return true;
}

]]>
</SCRIPT>

</HEAD>

<BODY class="bodyAPDSub" unselectable="on" onLoad="initPage();" bgcolor="#FFFAEB">
  <iframe id="frmEstimateDetail" src="/blank.asp" style="z-index:99;position:absolute;top:0px;left:0px;width:1010px;height:100%;display:none">
  </iframe>

<div id="divStatus" name="divStatus" style="z-index:999;position:absolute;top:0px;left:0px;padding:5px;border:1px solid #C0C0C0;height:75px;width:250px;background-color:#FFF8DC;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#C0C0C0,strength=3);display:none;">
  <table border="0" cellpadding="0" cellspacing="0" style="border:1px solid #C0C0C0;height:100%;width:100%;background-color:#FFFFFF">
    <tr>
      <td style="font-weight:bold;text-align:center;">Please wait...</td>
    </tr>
    <td style="font-weight:bold;text-align:center;">
      <img src="/images/progress.gif"/>
    </td>
  </table>
</div>

  
  <DIV unselectable="on" class="SmallGroupingTab" style="position:absolute; z-index:2; top: 5px; left: 6px;">
    <xsl:choose>
      <xsl:when test="string-length(@ShopName) &gt; 20"><xsl:value-of select="concat(substring(@ShopName, 1, 20), '...')"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="@ShopName"/></xsl:otherwise>
    </xsl:choose>
  </DIV>
  <DIV unselectable="on" class="SmallGrouping" style="position:absolute; left:3px; top:21px;width=1005px;height:405px;overflow:hidden">

  <xsl:call-template name="SearchTab"> <!-- search bar -->
    <xsl:with-param name="AssignmentCode" select="$AssignmentCode"/>
  </xsl:call-template>
	
    <IMG src="/images/spacer.gif" width="1" height="4" border="0" />

    <DIV id="CNScrollTable" style="width:100%;border:0px;height:400px;overflow:auto" >
        <DIV id="tabCycleTime" unselectable="on" class="tab1" active="1" style="position:absolute; z-index:2; top: 0px; left: 10px;" onmouseenter="if (this.active != 1) this.className='tabhover1'" onmouseleave="this.className=(this.active == '1' ? 'tabactive1' : 'tab1')" onclick="selectTab(this)">Cycle Time</DIV>
        <DIV id="tabIndemnity" unselectable="on" class="tab1" active="0" style="position:absolute; z-index:2; top: 0px; left: 94px;" onmouseenter="if (this.active != 1) this.className='tabhover1'" onmouseleave="this.className=(this.active == '1' ? 'tabactive1' : 'tab1')" onclick="selectTab(this)">Indemnity</DIV>
        <DIV id="tabReinspection" unselectable="on" class="tab1" active="0" style="position:absolute; z-index:2; top: 0px; left: 179px;" onmouseenter="if (this.active != 1) this.className='tabhover1'" onmouseleave="this.className=(this.active == '1' ? 'tabactive1' : 'tab1')" onclick="selectTab(this)">Reinspection</DIV>
        
        <DIV style="position:absolute; z-index:-99; top: 19px; left1: 0px; border:1px solid #000000;padding:3px;height:336px;width:100%">
			 <!-- Header tables -->
          <DIV style="height:36px;">
            <SPAN style="width:400px">
      	    <TABLE unselectable="on" class="ClaimMiscInputs" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
              <colgroup>
                <col width="30px"/>
                <col width="65px"/>
                <col width="140px"/>
                <col width="56px"/>
                <col width="146px"/>
                <col width="40px"/>
                <col width="150px"/>
              </colgroup>
              <TBODY>
                <TR unselectable="on" class="QueueHeader" style="height:36px">
                  <TD unselectable="on" class="TableSortHeader" > ReI Req</TD>
                  <TD unselectable="on" class="TableSortHeader" > LYNX ID</TD>
                  <TD unselectable="on" class="TableSortHeader" > Vehicle YMM </TD>
                  <TD unselectable="on" class="TableSortHeader">Drive-able</TD>
                  <TD unselectable="on" class="TableSortHeader" > Vehicle Owner </TD>
                  <TD unselectable="on" class="TableSortHeader" > Party </TD>
                  <TD unselectable="on" class="TableSortHeader" > Insurance Company </TD>
                </TR>
              </TBODY>
            </TABLE>
            </SPAN>
            <SPAN id="spnCycleTimeHeader" style="width:200px;display:none">
      	    <TABLE unselectable="on" class="ClaimMiscInputs" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
              <colgroup>
                <col width="75px"/>
                <col width="75px"/>
                <col width="75px"/>
                <col width="75px"/>
                <col width="55px"/>
              </colgroup>
              <TBODY>
                <TR unselectable="on" class="QueueHeader">
                  <TD unselectable="on" class="TableSortHeader" style="cursor:default" colspan="4"> Cycle Time - Dates </TD>
      			      <TD unselectable="on" class="TableSortHeader" style="cursor:default" rowspan="2"> Estimate Hours </TD>
                </TR>
                <TR>
                  <TD unselectable="on" class="TableSortHeader" style="cursor:default" > Assign </TD>
                  <TD unselectable="on" class="TableSortHeader" style="cursor:default" > Est </TD>
                  <TD unselectable="on" class="TableSortHeader" style="cursor:default" > Rpr Start </TD>
                  <TD unselectable="on" class="TableSortHeader" style="cursor:default" > Rpr End </TD>
                </TR>
              </TBODY>
            </TABLE>
            </SPAN>
            <SPAN id="spnIndemnityHeader" style="width:200px;display:none">
      	    <TABLE unselectable="on" class="ClaimMiscInputs" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
              <colgroup>
                <col width="63px"/>
                <col width="65px"/>
                <col width="45px"/>
                <col width="45px"/>
                <col width="45px"/>
                <col width="45px"/>
                <col width="45px"/>
              </colgroup>
              <TBODY>
                <TR unselectable="on" class="QueueHeader">
      			      <TD unselectable="on" class="TableSortHeader" style="cursor:default" rowspan="2"> Org. Est. </TD>
                  <TD unselectable="on" class="TableSortHeader" style="cursor:default" rowspan="2"> Last Supp. </TD>
                  <TD unselectable="on" class="TableSortHeader" style="cursor:default" rowspan="2"> Labor % Rpr</TD>
                  <TD unselectable="on" class="TableSortHeader" style="cursor:default" colspan="4"> Parts Utilization </TD>
                </TR>
                <TR>
                  <TD unselectable="on" class="TableSortHeader" style="cursor:default" > OEM </TD>
                  <TD unselectable="on" class="TableSortHeader" style="cursor:default" > LKQ </TD>
                  <TD unselectable="on" class="TableSortHeader" style="cursor:default" > AFMK </TD>
                  <TD unselectable="on" class="TableSortHeader" style="cursor:default" > Recyc. </TD>
                </TR>
              </TBODY>
            </TABLE>
            </SPAN>
            <SPAN id="spnReinspectionHeader" style="width:200px;display:none">
      	    <TABLE unselectable="on" class="ClaimMiscInputs" cellspacing="0" border="0" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; text-align:center; table-layout:fixed;">
              <colgroup>
                <col width="83px"/>
                <col width="180px"/>
                <col width="90px"/>
              </colgroup>
              <TBODY>
                <TR unselectable="on" class="QueueHeader" style="height:36px">
      			      <TD unselectable="on" class="TableSortHeader" style="cursor:default"> Reinspection Date </TD>
                  <TD unselectable="on" class="TableSortHeader" style="cursor:default"> Reinspection Type </TD>
                  <TD unselectable="on" class="TableSortHeader" style="cursor:default"> Percent Satisfaction</TD>
                </TR>
              </TBODY>
            </TABLE>
            </SPAN>
          </DIV>
          
            <!-- Data tables -->
          <DIV id="divData" style="border:0px solid #FF0000;height:299px;overflow:hidden"> <!-- 293 -->
            <DIV id="divNoData" style="color:#FF0000;width:100%;padding:10px;text-align:center;font-weight:bold;display:none">No Data available</DIV>
            <SPAN id="spnCommonData" style="width:400px;display:none;">
      	    <TABLE id="tblCommonData" unselectable="on" cellspacing="0" border="1" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px;table-layout:fixed;border:1px solid #C0C0C0;border-collapse:collapse" dataSrc="#xmlAssignments" dataPageSize="14" onreadystatechange="initRec()">
              <colgroup>
                <col width="29px" style="text-align:center;"/>
                <col width="65px" style="text-align:center;"/>
                <col width="139px"/>
                <col width="57px" style="text-align:center;"/>
                <col width="146px"/>
                <col width="40px" style="text-align:center;"/>
                <col width="149px"/>
                <col style="display:none"/>
                <col style="display:none"/>
                <col style="display:none"/>
              </colgroup>
              <TBODY>
                <TR unselectable="on" style="height:21px" onmouseenter="HighlightRow(this)" onmouseleave="UnHighlightRow(this)" onclick="openEstimate(this)">
                  <TD unselectable="on"><img dataFld="ReIReq"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="LynxID_ClaimAspect"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="Vehicle"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="Driveable"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="VehicleOwner"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="Party"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="InsuranceCo"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="AssignmentID"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="LynxID"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="ClaimAspectID"/></TD>
                </TR>
              </TBODY>
            </TABLE>
            </SPAN>
            <SPAN id="spnCycleTimeData" style="width:200px;display:none;">
      	    <TABLE id="tblCycleTimeData" unselectable="on" cellspacing="0" border="1" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px; table-layout:fixed;border:1px solid #C0C0C0;border-collapse:collapse" dataSrc="#xmlAssignments" dataPageSize="14">
              <colgroup>
                <col width="74px" style="text-align:center"/>
                <col width="74px" style="text-align:center"/>
                <col width="75px" style="text-align:center"/>
                <col width="75px" style="text-align:center"/>
                <col width="55px" style="text-align:right"/>
              </colgroup>
              <TBODY>
                <TR unselectable="on" style="height:21px;" onmouseenter="HighlightRow(this)" onmouseleave="UnHighlightRow(this)" onclick="openEstimate(this)">
                  <TD unselectable="on"><span class="NoWrap" dataFld="CTAssignDate"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="CTEstDate"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="CTRepairStart"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="CTRepairEnd"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="CTEstHours"/></TD>
                </TR>
              </TBODY>
            </TABLE>
            </SPAN>
            <SPAN id="spnIndemnityData" style="width:200px;display:none;">
      	    <TABLE id="tblIndemnityData" unselectable="on" cellspacing="0" border="1" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px;table-layout:fixed;border:1px solid #C0C0C0;border-collapse:collapse" dataSrc="#xmlAssignments" dataPageSize="14">
              <colgroup>
                <col width="62px" style="text-align:right"/>
                <col width="64px" style="text-align:right"/>
                <col width="45px" style="text-align:right"/>
                <col width="45px" style="text-align:right"/>
                <col width="45px" style="text-align:right"/>
                <col width="45px" style="text-align:right"/>
                <col width="45px" style="text-align:right"/>
              </colgroup>
              <TBODY>
                <TR unselectable="on" style="height:21px" onmouseenter="HighlightRow(this)" onmouseleave="UnHighlightRow(this)" onclick="openEstimate(this)">
      			      <TD unselectable="on"><span class="NoWrap" dataFld="IdOrgEstTotal"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="IdLastSuppTotal"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="IdLaborPercentRpr"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="IdPartsOEM"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="IdPartsLKQ"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="IdPartsAFMK"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="IdPartsRecy"/></TD>
                </TR>
              </TBODY>
            </TABLE>
            </SPAN>
            <SPAN id="spnReinspectionData" style="width:200px;display:none;">
      	    <TABLE id="tblReinspectionData" unselectable="on" cellspacing="0" border="1" cellpadding="2" style="font-family:Tahoma,Arial,Helvetica,sans-serif; font-size:10px;table-layout:fixed;border:1px solid #C0C0C0;border-collapse:collapse" dataSrc="#xmlAssignments" dataPageSize="14">
              <colgroup>
                <col width="82px" style="text-align:center"/>
                <col width="179px"/>
                <col width="90px" style="text-align:right"/>
              </colgroup>
              <TBODY>
                <TR unselectable="on" style="height:21px" onmouseenter="HighlightRow(this)" onmouseleave="UnHighlightRow(this)" onclick="openEstimate(this)">
      			      <TD unselectable="on"><span class="NoWrap" dataFld="ReIReinspectionDate"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="ReIReinspectionType"/></TD>
                  <TD unselectable="on"><span class="NoWrap" dataFld="ReIPercentSatisfaction"/></TD>
                </TR>
              </TBODY>
            </TABLE>
            </SPAN>
          </DIV>
          <DIV id="divPagination" style="border:1px solid #0000FF;height:23px;background-color:#C0C0C0;display:none">
            <table border="0" cellpadding="0" cellspacing="2" style="width:100%">
              <colgroup>
                <col width="*"/>
                <col width="100px" style="text-align:center"/>
                <col width="21px"/>
                <col width="21px"/>
                <col width="21px"/>
                <col width="21px"/>
              </colgroup>
              <tr>
                <td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
                <td>
                  <div id="txtPage">Page: 1 of 1</div>
                </td>
                <td><button style="border:1px outset;padding:0px;height:21px;width:21px;cursor:hand;" tabIndex="-1" onclick="moveFirst()">&lt;&lt;</button></td>
                <td><button style="border:1px outset;padding:0px;height:21px;width:21px;cursor:hand;" tabIndex="-1" onclick="movePrev()">&lt;</button></td>
                <td><button style="border:1px outset;padding:0px;height:21px;width:21px;cursor:hand;" tabIndex="-1" onclick="moveNext()">&gt;</button></td>
                <td><button style="border:1px outset;padding:0px;height:21px;width:21px;cursor:hand;" tabIndex="-1" onclick="moveLast()">&gt;&gt;</button></td>
              </tr>
            </table>
          </DIV>
        </DIV>
    </DIV>
  </DIV>

  <DIV unselectable="on" style="position:absolute; left:0px; top:360px">
    <xsl:for-each select="Shop"><xsl:call-template name="ShopDetails"/></xsl:for-each>
		<xsl:for-each select="CycleTimeSummary"><xsl:call-template name="CycleTimeSummary"/></xsl:for-each>
		<xsl:for-each select="CycleTimeSummary"><xsl:call-template name="IndemnitySummary"/></xsl:for-each>
		<xsl:for-each select="CycleTimeSummary"><xsl:call-template name="ReinspectionSummary"/></xsl:for-each>
  </DIV>

  <xml id="xmlAssignments" ondatasetcomplete="initData()">
    <Root>
      <xsl:for-each select="Assignment">
        <xsl:sort select="@LastAssignmentDate" order="descending"/>
        <Assignment>
          <xsl:attribute name="AssignmentID"><xsl:value-of select="@AssignmentID"/></xsl:attribute>
          <xsl:attribute name="ReIReq">
            <xsl:choose>
              <xsl:when test="@ReinspectionFlag='1'">/images/cbcheck.png</xsl:when>
              <xsl:otherwise>/images/spacer.gif</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="LynxID"><xsl:value-of select="@LynxID"/></xsl:attribute>
          <xsl:attribute name="LynxID_ClaimAspect"><xsl:value-of select="concat(@LynxID, '-', @ClaimAspectNumber)"/></xsl:attribute>
          <xsl:attribute name="ClaimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
          <xsl:variable name="VehYear"><xsl:if test="@VehicleYear != '0'"><xsl:value-of select="@VehicleYear"/></xsl:if></xsl:variable>
          <xsl:attribute name="Vehicle"><xsl:value-of select="concat(string($VehYear), ' ', string(@VehicleMake), ' ', string(@VehicleModel))"/></xsl:attribute>
          <xsl:attribute name="Driveable">
            <xsl:choose>
              <xsl:when test="@DrivableFlag='1'">Yes</xsl:when>
              <xsl:otherwise>No</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="VehicleOwner"> 
            <xsl:choose>
              <xsl:when test="@OwnerNameBusiness != ''"><xsl:value-of select="@OwnerNameBusiness"/></xsl:when>
              <xsl:otherwise><xsl:value-of select="concat(@OwnerNameLast, ', ', @OwnerNameFirst)"/></xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="Party"><xsl:value-of select="@ExposureCD"/></xsl:attribute>
          <xsl:attribute name="InsuranceCo"><xsl:value-of select="@InsuranceCoName"/></xsl:attribute>
          <xsl:attribute name="CTAssignDate">
            <xsl:if test="@AssignmentDate!='1900-01-01T00:00:00'">
              <xsl:value-of select="user:UTCConvertDate(string(@AssignmentDate))"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="CTEstDate">
            <xsl:if test="@OriginalEstDate!='1900-01-01T00:00:00'">
              <xsl:value-of select="user:UTCConvertDate(string(@OriginalEstDate))"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="CTRepairStart">
            <xsl:if test="@RepairStartDate!='1900-01-01T00:00:00'">
              <xsl:value-of select="user:UTCConvertDate(string(@RepairStartDate))"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="CTRepairEnd">
            <xsl:if test="@RepairEndDate!='1900-01-01T00:00:00'">
              <xsl:value-of select="user:UTCConvertDate(string(@RepairEndDate))"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="CTEstHours"><xsl:value-of select="format-number(@EstimateHours, '##0.0')"/></xsl:attribute>
          <xsl:attribute name="IdOrgEstTotal"><xsl:value-of select="format-number(@OriginalEstTotal, '$#,###,##0.00')"/></xsl:attribute>
          <xsl:attribute name="IdLastSuppTotal"><xsl:value-of select="format-number(@LastSuppTotal, '$#,###,##0.00')"/></xsl:attribute>
          <xsl:attribute name="IdLaborPercentRpr">
            <xsl:if test="@LaborPercentRepair != ''">
              <xsl:value-of select="format-number(@LaborPercentRepair, '##0.0%')"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="IdPartsOEM">
            <xsl:if test="@OEMPartsPercent != ''">
              <xsl:value-of select="format-number(@OEMPartsPercent, '##0.0%')"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="IdPartsLKQ">
            <xsl:if test="@LKQPartsPercent != ''">
              <xsl:value-of select="format-number(@LKQPartsPercent, '##0.0%')"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="IdPartsAFMK">
            <xsl:if test="@AFMKPartsPercent != ''">
              <xsl:value-of select="format-number(@AFMKPartsPercent, '##0.0%')"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="IdPartsRecy">
            <xsl:if test="@RecycledPartsPercent != ''">
              <xsl:value-of select="format-number(@RecycledPartsPercent, '##0.0%')"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="ReIReinspectionDate">
            <xsl:if test="@ReinspectionDate!='1900-01-01T00:00:00'">
              <xsl:value-of select="user:UTCConvertDate(string(@ReinspectionDate))"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="ReIReinspectionType"><xsl:value-of select="@ReinspectionType"/></xsl:attribute>
          <xsl:attribute name="ReIPercentSatisfaction">
            <xsl:if test="@ReinspectionSatisfactionPercent != ''">
              <xsl:value-of select="format-number(@ReinspectionSatisfactionPercent, '##0.0%')"/>
            </xsl:if>
          </xsl:attribute>          
        </Assignment>
      </xsl:for-each>
    </Root>
  </xml>
  
  <xml id="xmlCycleTimeSummary">
    <Root>
      <xsl:for-each select="CycleTimeSummary">
      <Summary>
        <xsl:copy-of select="@*"/>
        <xsl:attribute name="EstHours90"><xsl:value-of select="format-number(@EstimateHours90, '###0.0')"/></xsl:attribute>
        <xsl:attribute name="EstHours180"><xsl:value-of select="format-number(@EstimateHours180, '###0.0')"/></xsl:attribute>
      </Summary>
      </xsl:for-each>
    </Root>
  </xml>

  <xml id="xmlIndemnitySummary">
    <Root>
      <xsl:for-each select="IndemnitySummary">
      <Summary>
        <xsl:attribute name="Last3EstBodyRate"><xsl:value-of select="format-number(@Last3EstBodyRate, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="Last3EstRefinishRate"><xsl:value-of select="format-number(@Last3EstRefinishRate, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="Last3EstMechanicalRate"><xsl:value-of select="format-number(@Last3EstMechanicalRate, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="Last3EstFrameRate"><xsl:value-of select="format-number(@Last3EstFrameRate, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="Last3EstRefinishMaterials"><xsl:value-of select="format-number(@Last3EstRefinishMaterials, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="Last3EstTaxRate"><xsl:value-of select="concat(format-number(@Last3EstTaxRate, '##0.00'), '%')"/></xsl:attribute>
        <xsl:attribute name="ShopProfileBodyRate"><xsl:value-of select="format-number(@ShopProfileBodyRate, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="ShopProfileRefinishRate"><xsl:value-of select="format-number(@ShopProfileRefinishRate, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="ShopProfileMechanicalRate"><xsl:value-of select="format-number(@ShopProfileMechanicalRate, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="ShopProfileFrameRate"><xsl:value-of select="format-number(@ShopProfileFrameRate, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="ShopProfileRefinishMaterials"><xsl:value-of select="format-number(@ShopProfileRefinishMaterials, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="ShopProfileTaxRate"><xsl:value-of select="concat(format-number(@ShopProfileTaxRate, '##0.00'), '%')"/></xsl:attribute>
        <xsl:attribute name="MarketBodyRate"><xsl:value-of select="format-number(@MarketBodyRate, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="MarketRefinishRate"><xsl:value-of select="format-number(@MarketRefinishRate, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="MarketMechanicalRate"><xsl:value-of select="format-number(@MarketMechanicalRate, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="MarketFrameRate"><xsl:value-of select="format-number(@MarketFrameRate, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="MarketRefinishMaterials"><xsl:value-of select="format-number(@MarketRefinishMaterials, '$#,##0.00')"/></xsl:attribute>
        <xsl:attribute name="MarketTaxRate"><xsl:value-of select="concat(format-number(@MarketTaxRate, '##0.00'), '%')"/></xsl:attribute>
      </Summary>
      </xsl:for-each>
    </Root>
  </xml>

  <xml id="xmlReinspectionSummary">
    <Root>
      <xsl:for-each select="ReinspectionSummary">
      <Summary>
        <xsl:attribute name="Reinspection90"><xsl:value-of select="@Reinspection90"/></xsl:attribute>
        <xsl:attribute name="Reinspection180"><xsl:value-of select="@Reinspection180"/></xsl:attribute>
        <xsl:attribute name="BeforeRepair90"><xsl:value-of select="format-number(@BeforeRepair90, '##0.0%')"/></xsl:attribute>
        <xsl:attribute name="BeforeRepair180"><xsl:value-of select="format-number(@BeforeRepair180, '##0.0%')"/></xsl:attribute>
        <xsl:attribute name="DuringRepair90"><xsl:value-of select="format-number(@DuringRepair90, '##0.0%')"/></xsl:attribute>
        <xsl:attribute name="DuringRepair180"><xsl:value-of select="format-number(@DuringRepair180, '##0.0%')"/></xsl:attribute>
        <xsl:attribute name="AfterRepair90"><xsl:value-of select="format-number(@AfterRepair90, '##0.0%')"/></xsl:attribute>
        <xsl:attribute name="AfterRepair180"><xsl:value-of select="format-number(@AfterRepair180, '##0.0%')"/></xsl:attribute>
      </Summary>
      </xsl:for-each>
    </Root>
  </xml>
</BODY>
</HTML>
</xsl:template>
  
  <xsl:template name="SearchTab">
    <xsl:param name="AssignmentCode"/>
    <TABLE unselectable="on" class="ShopAssignTableSrh" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
      <colgroup>
        <col width="100px"/>
        <col width="40px"/>
        <col width="150px"/>
        <col width="30px"/>
        <col width="150px"/>
        <col width="175px"/>
        <col width="75px"/>
        <col width="*"/>
        <col width="125px"/>
        <col width="100px"/>
      </colgroup>
      <TR unselectable="on">
        <TD unselectable="on" nowrap="nowrap">Date Ranges:</TD>
        <TD unselectable="on" nowrap="nowrap">From</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtBeginDate" name="txtBeginDate" class="inputFld" size="9" maxlength="10" value="" onBlur="remove_XS_whitespace(this)" />
          <A  href='#' onclick='ShowCalendar(dimgBeginDate,txtBeginDate)' >
          <IMG  align='absmiddle' border='0' width='24' height='17' id='dimgBeginDate' src='/images/calendar.gif'/>
          </A>
          <DIV  id='divCalendar' class='PopupDiv' onMouseOut='CalMouseOut(this)' onMouseOver='CalMouseOver(this)' onDeactivate='CalBlur(this)' style='position:absolute;width:180px;height:127px;z-index:20000;display:none;visibility:hidden'></DIV>
        </TD>
        <TD unselectable="on" nowrap="nowrap">To</TD>
        <TD unselectable="on">
          <INPUT type="text" id="txtEndDate" name="txtEndDate" class="inputFld" size="9" maxlength="10" value="" onBlur="remove_XS_whitespace(this)" />
          <A  href='#' onclick='ShowCalendar(dimgEndDate,txtEndDate)' >
            <IMG  align='absmiddle' border='0' width='24' height='17' id='dimgEndDate' src='/images/calendar.gif'/>
          </A>
          <DIV  id='divCalendar' class='PopupDiv' onMouseOut='CalMouseOut(this)' onMouseOver='CalMouseOver(this)' onDeactivate='CalBlur(this)' style='position:absolute;width:180px;height:127px;z-index:20000;display:none;visibility:hidden'></DIV>
        </TD>
        <TD unselectable="on">
          <SELECT id="txtAssignmentCode" size="1" class="inputFld">
            <OPTION value="B">
              <xsl:if test="boolean($AssignmentCode='B')">
              <xsl:attribute name="SELECTED">
              <xsl:text>on</xsl:text>
              </xsl:attribute>
              </xsl:if>
              <xsl:text>Open and Closed Assignments</xsl:text>
            </OPTION>
            <OPTION value="O">
              <xsl:if test="boolean($AssignmentCode='O')">
              <xsl:attribute name="SELECTED">
              <xsl:text>on</xsl:text>
              </xsl:attribute>
              </xsl:if>
              <xsl:text>Open Assignments</xsl:text>
            </OPTION>
            <OPTION value="C">
              <xsl:if test="boolean($AssignmentCode='C')">
              <xsl:attribute name="SELECTED">
              <xsl:text>on</xsl:text>
              </xsl:attribute>
              </xsl:if>
              <xsl:text>Closed Assignments</xsl:text>
            </OPTION>
          </SELECT>
      </TD>
      <TD unselectable="on" align="center" nowrap="nowrap">
        <INPUT unselectable="on" type="button" id="btnSearch" name="btnSearch" value="Search" class="searchBtn" onClick="btnSearch_onclick();" />
      </TD>
      <TD></TD>
      <TD unselectable="on" align="center" style="font-weight:normal">Last Reinspection Date:</TD>
      <TD unselectable="on" style="font-weight:normal">
        <xsl:if test="/Root/@LastReIDate != '1900-01-01T00:00:00'">
          <xsl:value-of select="user:UTCConvertDate(string(/Root/@LastReIDate))"/>
        </xsl:if>
      </TD>
      </TR>
    </TABLE>
  </xsl:template>

<xsl:template name="ShopDetails">
  <DIV unselectable="on" class="SmallGroupingTab" style="position:absolute; z-index:2; top: 70px; left: 6px;">Shop Info</DIV>
  <DIV unselectable="on" class="SmallGrouping" style="position:absolute; left:3px; top:86px; z-index:1;width:450px">
  <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="3" style="table-layout:fixed; overflow:hidden;">
    <colgroup>
    	<col width="55" nowrap="nowrap"/>
    	<col width="40" nowrap="nowrap"/>
    	<col width="35" nowrap="nowrap"/>
    	<col width="90" nowrap="nowrap"/>
    	<col width="60" nowrap="nowrap"/>
    	<col width="155" nowrap="nowrap"/>
    </colgroup>

    <TR unselectable="on">
      <TD unselectable="on" align="right" nowrap="nowrap" colspan="4" class="TableHeader2">
        <SPAN class="boldtext">Shop ID:</SPAN>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of select="/Root/@ShopLocationID"/>
      </TD>
      <TD unselectable="on" class="TableHeader2"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
      <TD unselectable="on" align="right" nowrap="nowrap" class="TableHeader2">
        <SPAN class="boldtext">Program Score:</SPAN>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <xsl:value-of select="@ProgramScore"/>
      </TD>
    </TR>
    <TR unselectable="on" bgcolor="#FDF5E6">
      <TD unselectable="on" class="boldtext">Name:</TD>
      <TD unselectable="on" colspan="3" nowrap="nowrap" style="overflow:hidden; height1:34">
        <span class="NoWrap" >
        <xsl:choose>
          <xsl:when test="contains($ShopCRUD, 'R') = true()">
            <a>
              <xsl:attribute name="href">javascript:NavToShop("S", <xsl:value-of select="/Root/@ShopLocationID"/>, <xsl:value-of select="/Root/@ShopBusinessID"/>)</xsl:attribute>
              <xsl:value-of select="@Name"/>
            </a>
          </xsl:when>
          <xsl:otherwise><xsl:value-of select="@Name"/></xsl:otherwise>
        </xsl:choose>
        </span>
      </TD>
      <TD unselectable="on" class="boldtext">Pgm Mgr:</TD>
      <TD unselectable="on" nowrap="nowrap">
      <span class="NoWrap" >
       <xsl:attribute name="ProgramManagerUserID"><xsl:value-of select="@ProgramManagerUserID"/></xsl:attribute>
        <xsl:value-of select="@ProgramManagerName"/>
      </span>
      </TD>
    </TR>
    <TR unselectable="on" bgcolor="#FFFFEE">
      <TD unselectable="on" class="boldtext">Address:</TD>
      <TD unselectable="on" colspan="3" nowrap="nowrap">
        <span class="NoWrap" >
        <xsl:value-of select="@Address1"/>
        </span>
      </TD>
      <TD unselectable="on" class="boldtext">Contact:</TD>
      <TD unselectable="on" nowrap="nowrap"><span class="NoWrap" ><xsl:value-of select="@ContactName"/></span></TD>
    </TR>
    <TR unselectable="on" bgcolor="#FDF5E6">
      <TD unselectable="on" class="boldtext">City:</TD>
      <TD unselectable="on" colspan="3"><span class="NoWrap" ><xsl:value-of select="@AddressCity"/></span></TD>
      <TD unselectable="on" class="boldtext">Phone:</TD>
      <TD unselectable="on">
        <xsl:value-of select="@Phone"/>
      </TD>
    </TR>
    <TR unselectable="on" bgcolor="#FFFFEE">
      <TD unselectable="on" class="boldtext">State:</TD>
      <TD unselectable="on"><xsl:value-of select="@AddressState"/></TD>
      <TD unselectable="on" class="boldtext" nowrap="nowrap">ZIP:</TD>
      <TD unselectable="on"><xsl:value-of select="@AddressZip"/></TD>
      <TD unselectable="on" class="boldtext">Fax:</TD>
      <TD unselectable="on">
        <xsl:value-of select="@Fax"/>
      </TD>
    </TR>
    <TR unselectable="on" bgcolor="#FDF5E6">
      <TD unselectable="on" class="boldtext">County:</TD>
      <TD unselectable="on" colspan="3">
        <span class="NoWrap" >
        <xsl:choose>
          <xsl:when test="@AddressCounty=''" ><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</xsl:when>
          <xsl:otherwise><xsl:value-of select="@AddressCounty"/></xsl:otherwise>
        </xsl:choose> 
        </span>
      </TD>
      <TD unselectable="on" class="boldtext">E-mail:</TD>
      <TD unselectable="on"><span class="NoWrap" ><a><xsl:attribute name="href">mailto:<xsl:value-of select="@EmailAddress"/></xsl:attribute><xsl:value-of select="@EmailAddress"/></a></span></TD>
    </TR>
  </TABLE>
</DIV>
	
</xsl:template>

<xsl:template name="CycleTimeSummary">
  <DIV id="divCycleTimeSummary" style="position:absolute; z-index:2; top: 70px; left: 470px;;display:none">
  <DIV unselectable="on" class="SmallGroupingTab" style="position:relative; z-index:1; top:1px; left: 5px;width:180px;border-bottom:1px solid #FFFFFF;">Shop Cycle Time Averages</DIV>
  <DIV unselectable="on" class="SmallGrouping" style="position:relative; left:0px; top:0px; z-index:-10;height:132px;width:537px">
    <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed; overflow:hidden;" dataSrc="#xmlCycleTimeSummary">
      <colgroup>
      	<col width="150" nowrap="nowrap" style="font-weight:bold"/>
      	<col width="90" nowrap="nowrap"/>
      	<col width="90" nowrap="nowrap"/>
      	<col width="90" nowrap="nowrap"/>
      	<col width="90" nowrap="nowrap"/>
      </colgroup>
      <TR>
        <TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD><b>Last 90 Days:</b></TD>
        <TD><span dataFld="Assignnment90"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Assignments</TD>
        <TD><b>Last 180 Days:</b></TD>
        <TD><span dataFld="Assignnment180"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Assignments</TD>
      </TR>
      <TR bgcolor="#FDF5E6">
        <TD>Assignment to Estimate:</TD>
        <TD colspan="2"><span dataFld="Assignnment2Estimate90"/></TD>
        <TD colspan="2"><span dataFld="Assignnment2Estimate180"/></TD>
      </TR>
      <TR>
        <TD>Repair Days:</TD>
        <TD colspan="2"><span dataFld="RepairDays90"/></TD>
        <TD colspan="2"><span dataFld="RepairDays180"/></TD>
      </TR>
      <TR bgcolor="#FDF5E6">
        <TD>Estimate Hours:</TD>
        <TD colspan="2"><span dataFld="EstHours90"/></TD>
        <TD colspan="2"><span dataFld="EstHours180"/></TD>
      </TR>
    </TABLE>
  </DIV>
	</DIV>
</xsl:template>

<xsl:template name="IndemnitySummary">
  <DIV id="divIndemnitySummary" style="position:absolute; z-index:2; top: 70px; left: 470px;display:none">
  <DIV unselectable="on" class="SmallGroupingTab" style="position:relative; z-index:1; top:1px; left: 5px;width:150px;border-bottom:1px solid #FFFFFF;">Indemnity Summary</DIV>
  <DIV unselectable="on" class="SmallGrouping" style="position:relative; left:0px; top:0px; z-index:-10;height:132px;width:537px">
    <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed; overflow:hidden;" dataSrc="#xmlIndemnitySummary">
      <colgroup>
      	<col width="125" nowrap="nowrap" style="font-weight:bold"/>
      	<col width="125" nowrap="nowrap" style="text-align:right"/>
      	<col width="125" nowrap="nowrap" style="text-align:right"/>
      	<col width="125" nowrap="nowrap" style="text-align:right"/>
      </colgroup>
      <TR>
        <TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD style="font-weight:bold">Avg. Last 3</TD>
        <TD style="font-weight:bold">Shop Profile</TD>
        <TD style="font-weight:bold">Market</TD>
      </TR>
      <TR bgcolor="#FDF5E6">
        <TD>Body Labor:</TD>
        <TD><span dataFld="Last3EstBodyRate"/></TD>
        <TD><span dataFld="ShopProfileBodyRate"/></TD>
        <TD><span dataFld="MarketBodyRate"/></TD>
      </TR>
      <TR>
        <TD>Refinish Labor:</TD>
        <TD><span dataFld="Last3EstRefinishRate"/></TD>
        <TD><span dataFld="ShopProfileRefinishRate"/></TD>
        <TD><span dataFld="MarketRefinishRate"/></TD>
      </TR>
      <TR bgcolor="#FDF5E6">
        <TD>Mechanical Labor:</TD>
        <TD><span dataFld="Last3EstMechanicalRate"/></TD>
        <TD><span dataFld="ShopProfileMechanicalRate"/></TD>
        <TD><span dataFld="MarketMechanicalRate"/></TD>
      </TR>
      <TR>
        <TD>Frame Labor:</TD>
        <TD><span dataFld="Last3EstFrameRate"/></TD>
        <TD><span dataFld="ShopProfileFrameRate"/></TD>
        <TD><span dataFld="MarketFrameRate"/></TD>
      </TR>
      <TR bgcolor="#FDF5E6">
        <TD>Refinish Materials:</TD>
        <TD><span dataFld="Last3EstRefinishMaterials"/></TD>
        <TD><span dataFld="ShopProfileRefinishMaterials"/></TD>
        <TD><span dataFld="MarketRefinishMaterials"/></TD>
      </TR>
      <TR>
        <TD>Tax Rate:</TD>
        <TD><span dataFld="Last3EstTaxRate"/></TD>
        <TD><span dataFld="ShopProfileTaxRate"/></TD>
        <TD><span dataFld="MarketTaxRate"/></TD>
      </TR>
    </TABLE>
  </DIV>
	</DIV>
</xsl:template>

<xsl:template name="ReinspectionSummary">
  <DIV id="divReinspectionSummary" style="position:absolute; z-index:2; top: 70px; left: 470px;display:none">
  <DIV unselectable="on" class="SmallGroupingTab" style="position:relative; z-index:1; top:1px; left: 5px;width:160px;border-bottom:1px solid #FFFFFF;">Reinspection Summary</DIV>
  <DIV unselectable="on" class="SmallGrouping" style="position:relative; left:0px; top:0px; z-index:-10;height:132px;width:537px">
    <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed; overflow:hidden;" dataSrc="#xmlReinspectionSummary">
      <colgroup>
      	<col width="150" nowrap="nowrap" style="font-weight:bold"/>
      	<col width="90" nowrap="nowrap"/>
      	<col width="90" nowrap="nowrap"/>
      	<col width="90" nowrap="nowrap"/>
      	<col width="90" nowrap="nowrap"/>
      </colgroup>
      <TR>
        <TD><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
        <TD><b>Last 90 Days:</b></TD>
        <TD><span dataFld="Reinspection90"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Reinspections</TD>
        <TD><b>Last 180 Days:</b></TD>
        <TD><span dataFld="Reinspection180"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Reinspections</TD>
      </TR>
      <TR bgcolor="#FDF5E6">
        <TD>Before Repairs:</TD>
        <TD colspan="2"><span dataFld="BeforeRepair90"/></TD>
        <TD colspan="2"><span dataFld="BeforeRepair180"/></TD>
      </TR>
      <TR>
        <TD>During Repairs:</TD>
        <TD colspan="2"><span dataFld="DuringRepair90"/></TD>
        <TD colspan="2"><span dataFld="DuringRepair180"/></TD>
      </TR>
      <TR bgcolor="#FDF5E6">
        <TD>After Repairs/TL:</TD>
        <TD colspan="2"><span dataFld="AfterRepair90"/></TD>
        <TD colspan="2"><span dataFld="AfterRepair180"/></TD>
      </TR>
    </TABLE>
  </DIV>
	</DIV>
</xsl:template>
</xsl:stylesheet>

