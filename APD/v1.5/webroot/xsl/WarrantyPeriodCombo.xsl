<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    id="WorkmanshipWarrantyCombo">

<xsl:output method="html" indent="yes" encoding="UTF-8" />
<xsl:template match="/Root">
  <option value=""/>
  <xsl:for-each select="WarrantyPeriod">
		<option>
      <xsl:attribute name="value"><xsl:value-of select="@Code"/></xsl:attribute>
      <xsl:value-of select="@Name"/>
    </option>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>