<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:local="http://local.com/mynamespace"
    id="Diary">

<xsl:import href="msxsl/msjs-client-library_apdnet.js"/>
<xsl:import href="msxsl/generic-template-library_apdnet.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<msxsl:script language="JScript" implements-prefix="local">

    // Returns an integer based upon date and role logic.
    function getDateRoleState( date, complete_role )
    {
        var state = 0;
        var cur_date = new Date();
        var due_date = new Date( date );

        if ( ( due_date.valueOf() - ( 3600000 * 24 ) ) &gt; cur_date.valueOf() )
            state = 4;  // overdue
        else if ( due_date.valueOf() &gt; cur_date.valueOf() )
            state = 2;  // due within 24 hours

        if ( complete_role == '0' )
            state += 1;

        return state;
    }

</msxsl:script>

<!-- Permissions params - names must end in 'CRUD' -->
  <!-- xsl:param name="NoteCRUD" select="Note"/ -->
  <xsl:param name="NoteCRUD" select="Note"/>
  <xsl:param name="TaskCRUD" select="Task"/>

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="LynxID"/>
  <xsl:param name="UserID"/>
  <xsl:param name="WindowID"/>
  <xsl:param name="windowState"/>
  <xsl:param name="SubordinateUserID"/>
  <xsl:param name="SubordinatesFlag"/>
  <xsl:param name="ShowAllTasks"/>

  <xsl:template match="/Root">

  <!-- New -->
  <xsl:variable name="LynxID" select="LynxID"/>
  <xsl:variable name="UserID" select="@UserID"/>  <!-- @vars are from Attributes -->
  <xsl:variable name="WindowID" select="WindowID"/>
  <xsl:variable name="windowState" select="winState"/>
  <xsl:variable name="SubordinateUserID" select="SubordinateUserID"/>
  <xsl:variable name="SubordinatesFlag" select="SubordinatesFlag"/>
  <!-- xsl:variable name="ShowAllTasks" select="'true'"/ -->

  <xsl:variable name="VehClaimAspectID" select="VehClaimAspectID"/>
  <xsl:variable name="VehCount" select="VehCount"/>
  <xsl:variable name="NoteCRUD" select="Note"/>  <!-- Note from Nodes -->
  <xsl:variable name="TaskCRUD" select="Task"/>
  <xsl:variable name="RunTime" select="RunTime"/>

    <HTML>

<HEAD>
<TITLE>Diary ToDo List</TITLE>

<LINK rel="stylesheet" href="/css/apdwindow.css" type="text/css"/>
<STYLE type="text/css">
    A {color:black; text-decoration:none;}
</STYLE>
<SCRIPT language="JavaScript" src="js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="js/APDWindow.js"></SCRIPT>
<SCRIPT language="JavaScript" src="js/tablesort.js"></SCRIPT>
<SCRIPT language="JavaScript" src="js/ClaimNavigate.js"></SCRIPT>

<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>
<script type="text/javascript">
          document.onhelp=function(){
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,13);
		  event.returnValue=false;
		  };
</script>

<script language="javascript">
  var gsWindowID = "<xsl:value-of select="$WindowID"/>";
  var sWindowName = "winDiary";
  var sTaskCRUD = "<xsl:value-of select="$TaskCRUD"/>";
  var saveCheckListBG;
  var saveCheckListFont;
  var vCheckListLynxID = "<xsl:value-of select="$LynxID"/>";
  var vCheckListUserID = "<xsl:value-of select="$UserID"/>";
  var vCheckListCRUD = "<xsl:value-of select="$TaskCRUD"/>";
  var objCheckListPopup = window.createPopup();
  var bCompleted = false;
  var sWindowState = "<xsl:value-of select="$windowState"/>";
  var sSubordinateUserID = "<xsl:value-of select="$SubordinateUserID"/>";
  var sSubordinatesFlag = "<xsl:value-of select="$SubordinatesFlag"/>";
  var gbShowAllTasks = false;

  if ('<xsl:value-of select="$ShowAllTasks"/>' != "")
    gbShowAllTasks = "<xsl:value-of select="$ShowAllTasks"/>";

  var vCheckListShowAll = "false";
  if ( vCheckListLynxID == "0" )
    vCheckListShowAll = "true";

<![CDATA[

  function pageInit(){
    top.gbDiaryLocked = false;
    if (sWindowState == "max")
      setWindowState(sWindowState);

    if (document.getElementById("ShowAllFilter") && gbShowAllTasks == "true")
    {
      document.getElementById("ShowAllFilter").src = "/images/docfilter_single.gif";
      document.getElementById("ShowAllFilter").title = "Show tasks for selected vehicle only";
      document.getElementById("ShowAllFilter").onclick = new Function("toggleCheckListShowAll(false); return false;");
    }
  }

// Overrid from the standard version to handle checkList body popups.
  function checkListGridMouseOver( oObject )
  {
      try {
          saveCheckListBG = oObject.style.backgroundColor;
          saveCheckListFont = oObject.style.color;
          oObject.style.backgroundColor="#FFEEBB";
          oObject.style.color = "#000066";
          oObject.style.cursor='hand';
      } catch ( e ) { ClientError( "APDDiary.xsl::checkListGridMouseOver() " + e.message ); }
  }

  // Overrid from the standard version to handle checkList body popups.
  function checkListGridMouseOut( oObject )
  {
      try {
          oObject.style.backgroundColor = saveCheckListBG;
          oObject.style.color = saveCheckListFont;
      } catch ( e ) { ClientError( "APDDiary.xsl::checkListGridMouseOut() " + e.message ); }
  }

  // Overridden window function for display/hiding of 'extra' table columns.
  function setWindowState( newstate )
  {
      try {
          if ( this.state != newstate )
          {
              setCheckListExtraColDisplay( ( newstate == 'max' ) ? '' : 'none' );
              this.state = newstate;
              sWindowState = newstate;
          }
      } catch ( e ) { ClientError( "APDDiary.xsl::setWindowState() " + e.message ); }
  }

  // Sets/Clears the display state for the 'extra' table columns.
  function setCheckListExtraColDisplay( state )
  {
      try {
          if ( document.all.checkListComment != null )
          {
              var oAll = document.all;

              if ( state == "none" )
              {
                  oAll.checkListDueDateHeader.width = "55";
                  oAll.checkListDueDateHeader.innerText = "Due Dt";
                  oAll.checkListTaskNameHeader.width = "";
                  oAll.checkListEnteredByHeader.width = "80";

                  oAll.checkListDueDate.width = "55";
                  oAll.checkListTaskName.width = "";
                  oAll.checkListEnteredBy.width = "80";
              }
              else
              {
                  oAll.checkListDueDateHeader.width = "90";
                  oAll.checkListDueDateHeader.innerText = "Due Date";
                  oAll.checkListTaskNameHeader.width = "250";
                  oAll.checkListEnteredByHeader.width = "";

                  oAll.checkListDueDate.width = "90";
                  oAll.checkListTaskName.width = "250";
                  oAll.checkListEnteredBy.width = "";
              }

              oAll.checkListLynxId.style.display = state;
              oAll.checkListInsCo.style.display = state;
              oAll.checkListAssignedTo.style.display = state;
              oAll.checkListComment.style.display = state;
              oAll.checkListEnteredDate.style.display = state;
              oAll.checkListEnteredBy.style.display = state;

              oAll.checkListLynxIdHeader.style.display = state;
              oAll.checkListInsCoHeader.style.display = state;
              oAll.checkListAssignedToHeader.style.display = state;
              oAll.checkListCommentHeader.style.display = state;
              oAll.checkListEnteredDateHeader.style.display = state;
              oAll.checkListEnteredByHeader.style.display = state;
          }
      } catch ( e ) { ClientError( "APDDiary.xsl::setCheckListExtraColDisplay() " + e.message ); }
  }

  // User clicked on "complete" button.
  function completeCheckListItem( strLynxID, strTaskID, intState )
  {
      try {
          objCheckListPopup.hide();
          if ( vCheckListCRUD.substr( 2, 1 ) != "U" )
              ClientInfo( 'You do not have permission to complete tasks.  Please contact your supervisor.' );
          else if ( ( intState % 2 ) == 1 )
              ClientInfo( 'You do not have permission to complete this task.  Please contact your supervisor.' );
          else if ( vCheckListLynxID == null || vCheckListLynxID == "" || vCheckListLynxID == "0" )
              ClientInfo( 'You cannot mark tasks as complete unless you have the claim open' );
          else
          {
             if (top.gbDiaryLocked) return;
             var strReq = "CheckList.asp?WindowID=" + gsWindowID + "&Entity=CheckListComplete&TaskID=" + strTaskID + "&LynxID=" + strLynxID + "&TaskCompleted=" + bCompleted.toString();
             var arrDiary = window.showModalDialog( strReq, window, "dialogHeight:300px; dialogWidth:350px; resizable:no; status:no; help:no; center:yes;" );
             SetEntityMenu(arrDiary);
           }
      } catch ( e ) { ClientError( "APDDiary.xsl::completeCheckListItem() " + e.message ); }
  }

  // User clicked "add" toolbar button.
  function addCheckListItem()
  {
      try {
          objCheckListPopup.hide();
          if ( vCheckListCRUD.substr( 0, 1 ) != "C" )
          {
              ClientInfo( 'You do not have permission to create tasks.  Please contact your supervisor.' );
          }
          else if ( vCheckListLynxID != null && vCheckListLynxID != "" && vCheckListLynxID != "0" )
          {
              if (top.gbDiaryLocked) return;
              var strReq = "CheckList.asp?WindowID=" + gsWindowID + "&Entity=CheckListDetails&TaskID=0";

              // a retVal of '1' indicates a task was added to the current context
              var arrDiary = window.showModalDialog( strReq, window, "dialogHeight:280px; dialogWidth:350px; resizable:no; status:no; help:no; center:yes;" );
              SetEntityMenu(arrDiary)
          }
          else
              ClientInfo( "You must open a claim before creating tasks." );
      } catch ( e ) { ClientError( "APDDiary.xsl::addCheckListItem() " + e.message ); }
  }

  function SetEntityMenu(arr){
    var currentContext;
    var entity;
    var delta;

    if (arr){
      arr = arr.split("|");
      currentContext = arr[0];
      if (currentContext == "false") return;
      entity = arr[2];
      delta = arr[1];
      if (delta == "1") top.gsCountTasks++;
      if (delta == "-1") top.gsCountTasks--;

      var bVal = (top.gsCountNotes <= 0 && top.gsCountTasks <= 0 && top.gsCountBilling <= 0 && top.gsEntityCRUD.indexOf("D") != -1) ? true : false;

      switch(entity){
        case "veh":
          try { top.VehiclesMenu(bVal,3); } catch (e) {}
        case "prp":
          try {top.PropertiesMenu(bVal,3);} catch(e) {}
      }
    }
  }


  // User clicked on table row.
  function editCheckListItem( strLynxID, strTaskID )
  {
      try {
          objCheckListPopup.hide();
          if (top.gbDiaryLocked) return;
          var strReq = "CheckList.asp?WindowID=" + gsWindowID + "&Entity=CheckListDetails&TaskID=" + strTaskID + "&LynxID=" + strLynxID;
          window.showModalDialog( strReq, window, "dialogHeight:260px; dialogWidth:350px; resizable:no; status:no; help:no; center:yes;" );
      } catch ( e ) { ClientError( "APDDiary.xsl::editCheckListItem() " + e.message ); }
  }

  // Calls remote script to rebuild window contents.
  function refreshCheckListWindow(bFromTimer)
  {
      try {
          if ( ( String( bFromTimer ) == "undefined" ) || ( bFromTimer == null ) )
              bFromTimer = true;

          if (bFromTimer) {
            if (sWindowState == "max"){
              parent.loadContent("/APDDiary.asp?WindowID=" + gsWindowID + "&LynxID=" + vCheckListLynxID + "&UserID=" + vCheckListUserID + "&showAll=" + vCheckListShowAll + "&winState=" + sWindowState + "&SubordinateUserID=" + sSubordinateUserID + "&SubordinatesFlag=" + sSubordinatesFlag + "&ShowAllTasks=" + gbShowAllTasks);
              return;
            }
            window.location = "/APDDiary.asp?WindowID=" + gsWindowID + "&LynxID=" + vCheckListLynxID + "&UserID=" + vCheckListUserID + "&showAll=" + vCheckListShowAll + "&winState=" + sWindowState + "&SubordinateUserID=" + sSubordinateUserID + "&SubordinatesFlag=" + sSubordinatesFlag + "&ShowAllTasks=" + gbShowAllTasks;
            window.location.reload();
          } else {
            parent.loadContent("/APDDiary.asp?WindowID=" + gsWindowID + "&LynxID=" + vCheckListLynxID + "&UserID=" + vCheckListUserID + "&showAll=" + vCheckListShowAll + "&SubordinateUserID=" + sSubordinateUserID + "&SubordinatesFlag=" + sSubordinatesFlag + "&ShowAllTasks=" + gbShowAllTasks);
          }
    } catch ( e ) { alert( "APDDiary.xsl::refreshCheckListWindow() " + e.message ); }
  }

  // Used to navigate the main window to the passed context.
  function checkListNavigateTo( strEntity, strLynxID, strNumber )
  {
      try {
          objCheckListPopup.hide();

          // Currently on claim rep desktop?
          if ( vCheckListShowAll == "true" )
          {
              NavOpenWindow( strEntity, strLynxID, strNumber );
          }
          // currently within the claim window.
          else
          {
              // Check for dirty fields.  Wrapped because this can puke.
              try { top.document.oIframe.ParentMainTabChange(); } catch(e) {}

              // Browse to the pertains to.
              top.document.oIframe.frameElement.src = NavBuildUrl( strEntity, strLynxID, strNumber, gsWindowID );
          }
      } catch ( e ) { ClientError( "APDDiary.xsl::checkListNavigateTo() " + e.message ); }
  }

  //Used to popup the ClaimReassign page in reassign task mode.
  function checkListItemAssignTo(strLynxID, strClaimAspectID, strTaskID, strAlarmDate, strTaskDesc, strSysLastUpdatedDate)
  {
    try {
          objCheckListPopup.hide();
          if (top.gbDiaryLocked) return;
          var strReq = "ClaimReAssign.asp?WindowID=" + gsWindowID + "&Entity=Task&LynxID=" + strLynxID + "&ClaimAspectID=" + strClaimAspectID + "&CheckListID=" + strTaskID + "&AlarmDate=" + strAlarmDate + "&Desc=" + unescape(strTaskDesc) + "&SysLastUpdatedDate=" + strSysLastUpdatedDate;
          window.showModalDialog( strReq, window, "dialogHeight:200px; dialogWidth:300px; resizable:no; status:no; help:no; center:yes;" );
      } catch ( e ) { ClientError( "APDDiary.xsl::checkListItemAssignTo() " + e.message ); }
  }

  //Used to popup the snooze history dialog
  function checkListSnoozeHistory( strTaskID )
  {
    	try {
          objCheckListPopup.hide();
          if (top.gbDiaryLocked) return;
          var strReq = "CheckListSnoozeHistory.asp?WindowID=" + gsWindowID + "&TaskID=" + strTaskID;
          window.showModalDialog( strReq, window, "dialogHeight:400px; dialogWidth:600px; resizable:no; status:no; help:no; center:yes;" );
      } catch ( e ) { ClientError( "APDDiary.xsl::checkListSnoozeHistory() " + e.message ); }
  }

	// Puts up the context sensitive right-click menu over the task window.
  function checkListContextMenu( strEntity, strLynxID, strClaimAspectID, strNumber, strTaskID, intState, strAlarmDate, strUserTaskDescription, strSysLastUpdatedDate )
  {

      try {
          var lefter = event.offsetX + 10;
          var topper = event.offsetY + 10;
          var sTaskDesc = "";

          if (event.srcElement && event.srcElement.tagName == "TD"){
            var oTR = event.srcElement.parentElement;
            if (oTR.tagName == "TR" && oTR.cells.length > 2){//7
              sTaskDesc = oTR.cells[2].innerText;
            }
          }

          // Get the popup menu template.
          var strHtml = checkListPopupMenu.innerHTML;

          // Set all the onclick actions.
          strHtml = strHtml.replace( 'CompleteOnClick', "\"parent.completeCheckListItem('" + strLynxID + "','" + strTaskID + "'," + intState + " ); return false;\"" );
          strHtml = strHtml.replace( 'EditOnClick', "\"parent.editCheckListItem('" + strLynxID + "','" + strTaskID + "'); return false;\"" );
          strHtml = strHtml.replace( 'NavigateOnClick', "\"parent.checkListNavigateTo('" + strEntity + "','" + strLynxID + "','" + strNumber + "' ); return false;\"" );
          strHtml = strHtml.replace( 'ReassignOnClick', "\"parent.checkListItemAssignTo('" + strLynxID + "', '" + strClaimAspectID + "', '" + strTaskID + "', '" + strAlarmDate + "', '" + escape(strUserTaskDescription) + "', '" +  strSysLastUpdatedDate + "' ); return false;\"" );
          //strHtml = strHtml.replace( 'SnoozeHistoryOnClick', "\"parent.checkListSnoozeHistory('" + strTaskID + "' ); return false;\"" );

          // Customize the navigate description.
          var strNavigate = 'Navigate To ' + strEntity;
          if ( strNumber != "0" ) strNavigate += " " + strNumber;

          strHtml = strHtml.replace( 'Navigate To', strNavigate );

          // Pick an icon for the navigate item.
          switch( strEntity )
          {
              case "Vehicle": strNavigate = "icon_vehicle_closed"; break;
              case "Property": strNavigate = "icon_property_closed"; break;
              default: strNavigate = "icon_claim_closed"; break;
          }

          strHtml = strHtml.replace( 'navigate_image', strNavigate );

          // Now show the popup menu.
          objCheckListPopup.document.body.innerHTML = strHtml;
          objCheckListPopup.show(lefter, topper, 180, 110, event.srcElement);

      } catch ( e ) { ClientError( "APDDiary.xsl::checkListContextMenu() " + e.message ); }
  }

  function toggleCheckListShowAll(bTasksToggle)
  {
    gbShowAllTasks = bTasksToggle;
    refreshCheckListWindow(false);
  }

]]>
</script>
</HEAD>
<BODY unselectable="on" style="background-color:#FFFFFF;overflow:hidden;border:0px;padding:1px;" onload="pageInit()" tabIndex="-1" topmargin="0"  leftmargin="0">
  <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;background-color:#067BAF;width:100%;height:100%;" >
    <tr style="padding:1px;height:19px;" oncontextmenu="return false">
      <td>
        <div id="divWinToolBar" class="clWinToolBar" >
          <img border="0" src="/images/smnew2.gif" onClick="addCheckListItem(); return false" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Add" />
          <img src="/images/spacer.gif" width="8" height="2" border="0"/>
          <img border="0" src="/images/smrefresh.gif" onClick="refreshCheckListWindow(false); return false" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Refresh" />
          <xsl:if test="$VehCount &gt; 1">
            <img src="/images/spacer.gif" width="8" height="2" border="0"/>
            <img id="ShowAllFilter" border="0" src="/images/docfilter_many.gif" onClick="toggleCheckListShowAll(true); return false" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" title="Show tasks for all vehicles" />
          </xsl:if>
          &#160;&#160;&#160;<xsl:value-of select="$RunTime"/>
        </div>
      </td>
    </tr>
    <tr style="padding:1px;height:21px;">
      <td>

        <xml id="Reference"><xsl:copy-of select="//Reference[@List!='PertainsTo']"/></xml>

        <!-- Create checklist table. -->
          <table width="100%" class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSortCheckList')" cellspacing="0" border="0" cellpadding="1"
            style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size:10px; text-align:center; table-layout:fixed"  oncontextmenu="return false">
            <tbody>
              <tr class="QueueHeader" style="height:20px">
                <td style="display:none"></td>
                <td class="clWinGridTypeHeader" width="32" type="String"> Veh</td>
                <td class="clWinGridTypeHeader" width="20" type="String">Ch</td>
                <xsl:choose>
                  <xsl:when test="$windowState='max'">
                    <td class="clWinGridTypeHeader" width="90" type="Date" id="checkListDueDateHeader">Due Date</td>
                    <td class="clWinGridTypeHeader" width="250" type="String" id="checkListTaskNameHeader">Task Name</td>
                    <td class="clWinGridTypeHeader" width="50"  type="String" id="checkListLynxIdHeader">Lynx<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;ID</td>
                    <td class="clWinGridTypeHeader" width="90"  type="String" id="checkListInsCoHeader">Carrier</td>
                    <td class="clWinGridTypeHeader" width="94"  type="String" id="checkListAssignedToHeader">Assigned<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;To</td>
                    <td class="clWinGridTypeHeader" width="125" type="String" id="checkListCommentHeader">Comment</td>
                    <td class="clWinGridTypeHeader" width="90"  type="Date"   id="checkListEnteredDateHeader">Entered<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Date</td>
                    <td class="clWinGridTypeHeader" width=""    type="String" id="checkListEnteredByHeader">Entered<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;By</td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td class="clWinGridTypeHeader" width="55" type="Date" id="checkListDueDateHeader">Due Dt</td>
                    <td class="clWinGridTypeHeader" width=""    type="String" id="checkListTaskNameHeader">Task Name</td>
                    <td class="clWinGridTypeHeader" width="50"  type="String" style="display:none" id="checkListLynxIdHeader">Lynx<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;ID</td>
                    <td class="clWinGridTypeHeader" width="90"  type="String" style="display:none" id="checkListInsCoHeader">Carrier</td>
                    <td class="clWinGridTypeHeader" width="94"  type="String" style="display:none" id="checkListAssignedToHeader">Assigned<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;To</td>
                    <td class="clWinGridTypeHeader" width="125" type="String" style="display:none" id="checkListCommentHeader">Comment</td>
                    <td class="clWinGridTypeHeader" width="90"  type="Date"   style="display:none" id="checkListEnteredDateHeader">Entered<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Date</td>
                    <td class="clWinGridTypeHeader" width="80"  type="String" style="display:none" id="checkListEnteredByHeader">Entered<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;By</td>
                  </xsl:otherwise>
                </xsl:choose>
                <td class="clWinGridTypeTD" align="left" style="display:none"/>
              </tr>
            </tbody>
          </table>
      </td>
    </tr>
    <tr style="padding:1px;height:*;">
      <td>
        <div style="width:100%;height:100%;overflow:hidden;overflow:auto;background-color:#FFFFFF" class="clScrollTable">
          <table id="tblSortCheckList" class="clWinGridTypeTable" width="100%" cellspacing="0" border="0" cellpadding="1" style="table-layout:fixed">
            <col width="18"/>
            <col width="15"/>
            <col width="22"/>
            <xsl:choose>
              <xsl:when test="$windowState='max'">
                <col width="90" id="checkListDueDate"/>
                <col width="250" id="checkListTaskName"/>
                <col width="50"  id="checkListLynxId"/>
                <col width="90"  id="checkListInsCo"/>
                <col width="94"  id="checkListAssignedTo"/>
                <col width="125" id="checkListComment"/>
                <col width="90"  id="checkListEnteredDate"/>
                <col width=""    id="checkListEnteredBy"/>
              </xsl:when>
              <xsl:otherwise>
                <col width="55" id="checkListDueDate"/>
                <col width=""    id="checkListTaskName"/>
                <col width="50"  style="display:none" id="checkListLynxId"/>
                <col width="90"  style="display:none" id="checkListInsCo"/>
                <col width="94"  style="display:none" id="checkListAssignedTo"/>
                <col width="125" style="display:none" id="checkListComment"/>
                <col width="90"  style="display:none" id="checkListEnteredDate"/>
                <col width="80"  style="display:none" id="checkListEnteredBy"/>
              </xsl:otherwise>
            </xsl:choose>
            <col width="0" style="display:none"/>
            <tbody dataRows="1" bgColor1="fff7e5" bgColor2="ffffff">
              <xsl:if test="contains($TaskCRUD,'R')">
                <xsl:choose>
                  <xsl:when test="$ShowAllTasks = 'true'">
                    <xsl:for-each select="DiaryTask" >
                      <xsl:sort select="@ClaimAspectID" order="ascending"/>
                      <xsl:sort select="@AlarmDate" order="ascending"/>
                      <xsl:call-template name="DiaryTask"/>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:when test="$VehClaimAspectID = ''">
                    <xsl:for-each select="DiaryTask" >
                      <xsl:sort select="@ClaimAspectID" order="ascending"/>
                      <xsl:sort select="@AlarmDate" order="ascending"/>
                      <xsl:call-template name="DiaryTask"/>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:for-each select="DiaryTask[@ClaimAspectID = $VehClaimAspectID]" >
                      <xsl:sort select="@AlarmDate" order="ascending"/>
                      <xsl:call-template name="DiaryTask"/>
                    </xsl:for-each>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:if>
            </tbody>
          </table>
        </div>
      </td>
    </tr>
  </table>
<div id="checkListPopupMenu" style="display:none;">
  <table unselectable="on" cellspacing="1" cellpadding="0" width="180" height="110" style="border:1px solid #666666;table-layout:fixed;cursor:hand;font-family:verdana;font-size:11px;">
    <tr unselectable="on">
      <td unselectable="on" height="20" style="border:1pt solid #F9F8F7"
          onmouseover="this.style.background='#B6BDD2';this.style.borderColor='#0A246A';"
          onmouseout="this.style.background='#F9F8F7';this.style.borderColor='#F9F8F7';"
          onclick="CompleteOnClick">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <img src="/images/tasklist_action_grayscale.gif" border="0" hspace="0" vspace="0" align="absmiddle"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        Complete Task
      </td>
    </tr>
    <tr unselectable="on">
      <td unselectable="on" height="20" style="border:1pt solid #F9F8F7"
          onmouseover="this.style.background='#B6BDD2';this.style.borderColor='#0A246A';"
          onmouseout="this.style.background='#F9F8F7';this.style.borderColor='#F9F8F7';"
          onclick="EditOnClick">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <img src="/images/notepad_grayscale.gif" border="0" hspace="0" vspace="0" align="absmiddle"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        Edit Task
      </td>
    </tr>
    <tr unselectable="on">
      <td unselectable="on" height="20" style="border:1pt solid #F9F8F7"
          onmouseover="this.style.background='#B6BDD2';this.style.borderColor='#0A246A';"
          onmouseout="this.style.background='#F9F8F7';this.style.borderColor='#F9F8F7';"
          onclick="NavigateOnClick">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <img src="/images/navigate_image.gif" border="0" hspace="0" vspace="0" align="absmiddle"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        Navigate To
      </td>
    </tr>
    <tr unselectable="on">
      <td unselectable="on" height="20" style="border:1pt solid #F9F8F7"
          onmouseover="this.style.background='#B6BDD2';this.style.borderColor='#0A246A';"
          onmouseout="this.style.background='#F9F8F7';this.style.borderColor='#F9F8F7';"
          onclick="ReassignOnClick">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <img src="/images/notepad_grayscale.gif" border="0" hspace="0" vspace="0" align="absmiddle"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        Reassign Task
      </td>
    </tr>
    <!-- <tr unselectable="on">
      <td unselectable="on" height="20" style="border:1pt solid #F9F8F7"
          onmouseover="this.style.background='#B6BDD2';this.style.borderColor='#0A246A';"
          onmouseout="this.style.background='#F9F8F7';this.style.borderColor='#F9F8F7';"
          onclick="SnoozeHistoryOnClick">
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        <img src="/images/snooze_history.gif" border="0" hspace="0" vspace="0" align="absmiddle"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        Snooze History
      </td>
    </tr> -->
  </table>
</div>

</BODY>
</HTML>
</xsl:template>

  <!-- 06Jun2013 - TVD - New Code added to handle date formatting -->
  <xsl:template name="formatDate">
    <xsl:param name="dateTime" />
    <xsl:variable name="date" select="substring-before($dateTime, 'T')" />
    <xsl:variable name="year" select="substring-before($date, '-')" />
    <xsl:variable name="month" select="substring-before(substring-after($date, '-'), '-')" />
    <xsl:variable name="day" select="substring-after(substring-after($date, '-'), '-')" />
    <xsl:value-of select="concat($month, '/', $day, '/', $year)" />
  </xsl:template>

  <!-- 06Jun2013 - TVD - New Code added to handle time formatting -->
  <xsl:template name="formatTime">
    <xsl:param name="dateTime" />
    <xsl:variable name="time" select="substring-after($dateTime, 'T')" />
    <xsl:variable name="hh" select="substring-before($time, ':')" />
    <xsl:variable name="mm" select="substring-before(substring-after($time, ':'), ':')" />
    <xsl:variable name="ss" select="substring-after(substring-after($time, ':'), ':')" />
    <xsl:choose>
      <xsl:when test="$hh > 12">
        <xsl:variable name="nhh" select="substring-before($time, ':') - 12" />
        <xsl:value-of select="concat(concat($nhh, ':', $mm), ' PM')"/>
      </xsl:when>
      <xsl:when test="$hh = 12">
        <xsl:variable name="nhh" select="substring-before($time, ':')" />
        <xsl:value-of select="concat(concat($nhh, ':', $mm), ' PM')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="nhh" select="substring-before($time, ':')" />
        <xsl:value-of select="concat(concat($nhh, ':', $mm), ' AM')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- 06Jun2013 - TVD - New Code added to handle date/time formatting -->
  <xsl:template name="formatDateTime">
    <xsl:param name="dateTime" />
    <xsl:variable name="date" select="substring-before($dateTime, 'T')" />
    <xsl:variable name="year" select="substring(substring-before($date, '-'),3,2)" />
    <xsl:variable name="month" select="substring-before(substring-after($date, '-'), '-')" />
    <xsl:variable name="day" select="substring-after(substring-after($date, '-'), '-')" />

    <xsl:variable name="time" select="substring-after($dateTime, 'T')" />
    <xsl:variable name="hh" select="substring-before($time, ':')" />
    <xsl:variable name="mm" select="substring-before(substring-after($time, ':'), ':')" />
    <xsl:variable name="ss" select="substring-after(substring-after($time, ':'), ':')" />
    <xsl:choose>
      <xsl:when test="$hh > 12">
        <xsl:variable name="nhh" select="substring-before($time, ':') - 12" />
        <xsl:variable name="TimeFormatted" select="concat(concat($nhh, ':', $mm), ' PM')"/>
        <xsl:variable name="DateFormatted" select="concat($month, '/', $day, '/', $year)"/>
        <xsl:value-of select="concat($DateFormatted, ' ', $TimeFormatted)"/>
      </xsl:when>
      <xsl:when test="$hh = 12">
        <xsl:variable name="nhh" select="substring-before($time, ':')" />
        <xsl:variable name="TimeFormatted" select="concat(concat($nhh, ':', $mm), ' PM')"/>
        <xsl:variable name="DateFormatted" select="concat($month, '/', $day, '/', $year)"/>
        <xsl:value-of select="concat($DateFormatted, ' ', $TimeFormatted)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="nhh" select="substring-before($time, ':')" />
        <xsl:variable name="TimeFormatted" select="concat(concat($nhh, ':', $mm), ' AM')"/>
        <xsl:variable name="DateFormatted" select="concat($month, '/', $day, '/', $year)"/>
        <xsl:value-of select="concat($DateFormatted, ' ', $TimeFormatted)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!-- Each checklist gets its own table row -->
  <xsl:template name="DiaryTask">
    <xsl:variable name="AlarmDateTime">
      <xsl:value-of select="user:extractSQLDateTime( string(@AlarmDate) )"/>
    </xsl:variable>
    <xsl:variable name="State">
      <xsl:value-of select="local:getDateRoleState( string($AlarmDateTime), string(@CompletionAllowedFlag) )"/>
    </xsl:variable>

    <!-- Displayed table row -->
    <tr onMouseOut="checkListGridMouseOut(this)"
        onMouseOver="checkListGridMouseOver(this)">

      <!-- Double click on row opens claim aspect -->
      <xsl:attribute name="onDblClick">checkListNavigateTo(
        '<xsl:value-of select="@ClaimAspectTypeName"/>',
        '<xsl:value-of select="@LynxID"/>',
        '<xsl:value-of select="@ClaimAspectNumber"/>'
      ); return false;</xsl:attribute>

      <!-- Right click opens check list context menu -->
      <xsl:attribute name="onContextMenu">checkListContextMenu(
        '<xsl:value-of select="@ClaimAspectTypeName"/>',
        '<xsl:value-of select="@LynxID"/>',
        '<xsl:value-of select="@ClaimAspectID"/>',
        '<xsl:value-of select="@ClaimAspectNumber"/>',
        '<xsl:value-of select="@CheckListID"/>',
        '<xsl:value-of select="$State"/>',
        '<xsl:value-of select="@AlarmDate"/>',
        '<xsl:value-of select="js:cleanString(string(@UserTaskDescription))"/>',
        '<xsl:value-of select="@SysLastUpdatedDate"/>'
      ); return false;</xsl:attribute>

      <xsl:attribute name="idx">
        <xsl:value-of select="position()"/>
      </xsl:attribute>
      <xsl:attribute name="bgColor">
        <xsl:choose> <!-- Defect #1476 - Tasks for Closed Claim displayed as grayed -->
          <xsl:when test="@StatusName='Claim Closed' or @StatusName='Claim Cancelled' or @StatusName='Claim Voided'">C0C0C0</xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="user:chooseBackgroundColor(position(), 'fff7e5', 'ffffff' ) "/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="state">
        <xsl:value-of select="$State"/>
      </xsl:attribute>

      <td id="CompleteButton" class="clWinGridTypeTD" align="center" nowrap="">
        <!-- Click on button opens completion dialog -->
        <xsl:attribute name="onClick">completeCheckListItem(
          '<xsl:value-of select="@LynxID"/>',
          '<xsl:value-of select="@CheckListID"/>',
          '<xsl:value-of select="$State"/>'
        ); return false;</xsl:attribute>
        <img style="FILTER:alpha(opacity=70); cursor:hand;" onmouseout="makeHighlight(this,1,70)" onmouseover="makeHighlight(this,0,100)" width="17" height="17" border="0" >
          <xsl:choose>
            <xsl:when test="$State='0'">
              <xsl:attribute name="src">/Images/tasklist_action_00_red.gif</xsl:attribute>
              <xsl:attribute name="title">Past due</xsl:attribute>
            </xsl:when>
            <xsl:when test="$State='1'">
              <xsl:attribute name="src">/Images/tasklist_noaction_00_red.gif</xsl:attribute>
              <xsl:attribute name="title">Past due</xsl:attribute>
            </xsl:when>
            <xsl:when test="$State='2'">
              <xsl:attribute name="src">/Images/tasklist_action_01_green.gif</xsl:attribute>
              <xsl:attribute name="title">Due within 24 hours</xsl:attribute>
            </xsl:when>
            <xsl:when test="$State='3'">
              <xsl:attribute name="src">/Images/tasklist_noaction_01_green.gif</xsl:attribute>
              <xsl:attribute name="title">Due within 24 hours</xsl:attribute>
            </xsl:when>
            <xsl:when test="$State='4'">
              <xsl:attribute name="src">/Images/tasklist_action_02_blue.gif</xsl:attribute>
              <xsl:attribute name="title">Due more than 24 hours</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="src">/Images/tasklist_noaction_02_blue.gif</xsl:attribute>
              <xsl:attribute name="title">Due more than 24 hours</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
        </img>
      </td>

      <td id="PertainsTo" class="clWinGridTypeTD" align="center" valign="middle">
        <xsl:if test="@ManagerialTaskFlag = '1'">
          <xsl:attribute name="style">font-weight:bold;color:#0000FF</xsl:attribute>
        </xsl:if>
        <xsl:if test="not(@ClaimAspectNumber=0)">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <xsl:value-of select="@ClaimAspectNumber"/>
        </xsl:if>
      </td>

      <td id="ServiceChannelCD" class="clWinGridTypeTD" align="center" valign="middle">
        <xsl:if test="@ManagerialTaskFlag = '1'">
          <xsl:attribute name="style">font-weight:bold;color:#0000FF</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="@ServiceChannelCD"/>
      </td>

      <td id="AlarmDate" class="clWinGridTypeTD" align="center">
        <xsl:if test="@ManagerialTaskFlag = '1'">
          <xsl:attribute name="style">font-weight:bold;color:#0000FF</xsl:attribute>
        </xsl:if>
        <!-- xsl:value-of select="user:UTCConvertDateAndTimeByNodeTypeShort(.,'AlarmDate','a')"/ -->
        <xsl:value-of select="user:UTCConvertDateAndTimeByNodeTypeShortNew($AlarmDateTime,'AlarmDate','m')"/>
      </td>
      <td id="TaskName" class="clWinGridTypeTD" align="center">
        <xsl:if test="@ManagerialTaskFlag = '1'">
          <xsl:attribute name="style">font-weight:bold;color:#0000FF</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="@TaskName"/>
      </td>
      <td id="LynxID" class="clWinGridTypeTD" align="center">
        <xsl:value-of select="@LynxID"/>
      </td>
      <td id="InsCo" class="clWinGridTypeTD" align="center">
        <xsl:value-of select="@InsuranceCompanyName"/>
      </td>
      <td id="AssignedTo" class="clWinGridTypeTD" align="center">
        <xsl:value-of select="@AssignedUserNameLast"/>
        <xsl:if test="@AssignedUserNameFirst != ''">,
          <xsl:value-of select="substring(@AssignedUserNameFirst,1,1)"/>.
        </xsl:if>
      </td>
      <td id="UserTaskDescription" class="clWinGridTypeTD" align="center">
        <xsl:value-of select="@UserTaskDescription"/>
      </td>
      <td id="CreatedDate" class="clWinGridTypeTD" align="center">
        <xsl:call-template name="formatDateTime">
          <xsl:with-param name="dateTime" select="@CreatedDate" />
        </xsl:call-template>
      </td>
      <td id="CreatedUser" class="clWinGridTypeTD" align="center">
        <xsl:value-of select="@CreatedUserNameLast"/>
        <xsl:if test="@CreatedUserNameFirst != ''">,
          <xsl:value-of select="substring(@CreatedUserNameFirst,1,1)"/>.
        </xsl:if>
      </td>
      <td id="TaskID" class="clWinGridTypeTD" align="left" style="display:none">
        <xsl:value-of select="@CheckListID"/>
      </td>
      <td id="SysLastUpdatedDate" class="clWinGridTypeTD" align="left" style="display:none">
        <xsl:value-of select="@SysLastUpdatedDate"/>
      </td>
    </tr>

  </xsl:template>

</xsl:stylesheet>
