<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:fo="http://www.w3.org/1999/XSL/Format"
		xmlns:ms="urn:schemas-microsoft-com:xslt"
		xmlns:user="http://mycompany.com/mynamespace"
		xmlns:js="urn:the-xml-files:xslt"
		id="ClaimPedestrian">

<xsl:import href="msxsl/msjs-client-library.js"/>
<xsl:output method="html" indent="yes" encoding="UTF-8" />

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="PedestrianCRUD" select="Pedestrian"/>

<!-- XslParams - stuffed from COM before transform -->
<xsl:param name="UserId"/>
<xsl:param name="LynxId"/>

<xsl:template match="/Root">

<xsl:variable name="InvolvedID" select="/Root/Pedestrian/@InvolvedID" />
<xsl:variable name="PedestrianLastUpdatedDate" select="/Root/Pedestrian/@SysLastUpdatedDate"/>
<xsl:value-of select="js:SetCRUD( string($PedestrianCRUD) )"/>

<HTML>

<!--            APP _PATH                            SessionKey                   USERID     SP                     XSL    PARAMETERS TO SP -->
<!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},10,uspClaimPedestrianGetDetailXML,ClaimPedestrian.xsl,763,145   -->

<HEAD>
<TITLE>Vehicle Details</TITLE>

<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
<LINK rel="STYLESHEET" type="text/css" href="/css/CoolSelect.css"/>
<LINK rel="stylesheet" href="/css/datepicker.css" type="text/css"/>

<STYLE type="text/css">
	A {color:black; text-decoration:none;}
	A:Hover {color:navy; text-decoration:underline;}
</STYLE>

<SCRIPT language="JavaScript" src="/js/tooltips.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
<SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/checkbox.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/radio.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>


<SCRIPT language="JavaScript">
	var gsInvolvedID = '<xsl:value-of select="$InvolvedID"/>';
  var gsPedestrianLastUpdatedDate = '<xsl:value-of select="$PedestrianLastUpdatedDate"/>';
  var gsUserID = '<xsl:value-of select="$UserId"/>';
	var gsPedestrianCRUD = '<xsl:value-of select="$PedestrianCRUD"/>';
  var gsLynxID = '<xsl:value-of select="$LynxId"/>';

<![CDATA[

	messages= new Array()
	// Write your descriptions in here.
	messages[0]="Double Click to Expand"
	messages[1]="Double Click to Close"

	preload('checkbox0','/images/cbuncheck.png')
	preload('checkbox1','/images/cbcheck.png')
	preload('checkboxhover0','/images/cbhover.png')
	preload('checkboxhover1','/images/cbhoverck.png')

	preload('radio0','/images/radiounsel.png')
	preload('radio1','/images/radiosel.png')
	preload('radiohover0','/images/radiohover.png')
	preload('radiohover1','/images/radioselhv.png')

  var inADS_Pressed = false;    // re-entry flagging for buttons pressed
  var objDivWithButtons = null;
  var gbDirtyFlag = false;

	function ADS_Pressed(sAction, sName)
	{
    if (sAction != "Delete")
    {
  		if ((ValidatePhones() == false) || (checkDOB() == false)) return new Array("", "0");
      if (!gbDirtyFlag) return new Array("", "0"); //nothing changed.
    }
    if (inADS_Pressed == true) return new Array("", "0");
    inADS_Pressed = true;
    objDivWithButtons = window.parent.divWithButtons;
    disableADS(objDivWithButtons, true);

		var sRequest = GetRequestString(sName);
		sRequest += "LynxID=" + gsLynxID + "&";
		sRequest += "UserID=" + gsUserID + "&";

		var oDiv = document.getElementById(sName)
		var retArray = DoCrudAction(oDiv, sAction, sRequest);

		if (sAction != "Delete" && retArray[1] == "1")
		{
			//update the last Updated date...
			var objXML = new ActiveXObject("Microsoft.XMLDOM");
			objXML.loadXML(retArray[0]);

			var contextDate = objXML.documentElement.selectSingleNode("/Root/NewPedestrian/@SysLastUpdatedDate")
			oDiv.LastUpdatedDate = contextDate.nodeValue;
			gsWitnessLastUpdatedDate = oDiv.LastUpdatedDate;
			var InvolvedID = objXML.documentElement.selectSingleNode("/Root/NewPedestrian/@InvolvedID")
			oDiv.ContextID = InvolvedID.nodeValue;
			gsInvolvedID = oDiv.ContextID;

			objXML = null;
		}
    inADS_Pressed = false;
    disableADS(objDivWithButtons, false);
    objDivWithButtons = null;

		return retArray;
	}

	// show the contact window
	function ShowContactLayer(sLayer, sVis, sRoId, sTxtId)
	{
    ShowHideTxtArea(sLayer, sVis);
		if (sVis == 'visible')
			 document.getElementById(sTxtId).value = document.getElementById(sRoId).value
		else
		{
			gbDirtyFlag = true;
			document.getElementById(sRoId).style.borderColor='#805050';
			document.getElementById(sRoId).value = document.getElementById(sTxtId).value;
		}
	}

	function PageInit()
	{
		try {
      onpasteAttachEvents();
      ShowHideInjuryFlds(InjuredCD)

      if (txtInvolvedDOB.value != '//')
         CalculateAge(txtInvolvedDOB, txtInvolvedAge);
      else
         txtInvolvedDOB.value = '';
      txtInvolvedDOB.futureDate = false;
    } catch (e) {}
		top.setSB(100,top.sb);
	}

	function onRelatedInvolvedChange(selobj)
	{
		/*var sContext = selobj.options[selobj.selectedIndex].value;
		ShowHideTxtArea('RelatedInfo','hidden');

		if (sContext != "")
		{
			var oDiv = document.getElementById("RelatedInfo");
			var elms = oDiv.getElementsByTagName("INPUT");
			for(var i = 0; i < elms.length; i++)
			{
				if (elms[i].type != "button" && elms[i].name != "")
				{
					if (elms[i].id.indexOf("txt"+sContext) != -1)
					{
						elms[i].style.visibility = "visible";
						elms[i].style.display = "inline";
					}
					else
					{
						elms[i].style.visibility = "hidden";
						elms[i].style.display = "none";
					}
				}
			}

			var oTable = document.getElementById("RelatedInfoTable");
			oTable.rows[0].cells[0].innerText = sContext + " Contact Information"
			if (sContext != "Attorney")
				oTable.rows[3].style.visibility = "hidden";
			else
				oTable.rows[3].style.visibility = "visible";
			ShowHideTxtArea('RelatedInfo','visible');

			document.getElementById("selRelatedInvolved").children[0].style.borderColor='#805050';*/
            var objRet = null;
            var objArgument = new Object;
            var sLayer = selobj.options[selobj.selectedIndex].value;
            selobj.firstChild.fireEvent("onclick");
            switch (sLayer) {
                case "Doctor":
                    objArgument.BusinessName = txtDoctorBusinessName.value;
                    objArgument.Address1 = txtDoctorAddress1.value;
                    objArgument.Address2 = txtDoctorAddress2.value;
                    objArgument.City = txtDoctorCity.value;
                    objArgument.State = txtDoctorState.value;
                    objArgument.Zip = txtDoctorZip.value;
                    objArgument.PhoneAC = txtDoctorDayPhoneAC.value;
                    objArgument.PhoneEX = txtDoctorDayPhoneEX.value;
                    objArgument.PhoneUN = txtDoctorDayPhoneUN.value;
                    objArgument.PhoneXT = txtDoctorDayPhoneXT.value;
                    objRet = window.showModalDialog("popDocHospital.htm", objArgument, "dialogHeight:210px;dialogWidth:660px;center:1;help:0;resizable:0;scroll:0;status:0")
                    if (objRet){
                        txtDoctorBusinessName.value = objRet.BusinessName;
                        txtDoctorAddress1.value = objRet.Address1;
                        txtDoctorAddress2.value = objRet.Address2;
                        txtDoctorCity.value = objRet.City;
                        txtDoctorState.value = objRet.State;
                        txtDoctorZip.value = objRet.Zip;
                        txtDoctorDayPhoneAC.value = objRet.PhoneAC;
                        txtDoctorDayPhoneEX.value = objRet.PhoneEX;
                        txtDoctorDayPhoneUN.value = objRet.PhoneUN;
                        txtDoctorDayPhoneXT.value = objRet.PhoneXT;
                        setControlDirty(document.getElementById("selRelatedInvolved").children[0]);
                        //document.getElementById("selRelatedInvolved").children[0].style.borderColor='#805050';
                    }
                    break;
                case "Employer":
                    objArgument.BusinessName = txtEmployerBusinessName.value;
                    objArgument.Address1 = txtEmployerAddress1.value;
                    objArgument.Address2 = txtEmployerAddress2.value;
                    objArgument.City = txtEmployerCity.value;
                    objArgument.State = txtEmployerState.value;
                    objArgument.Zip = txtEmployerZip.value;
                    objArgument.PhoneAC = txtEmployerDayPhoneAC.value;
                    objArgument.PhoneEX = txtEmployerDayPhoneEX.value;
                    objArgument.PhoneUN = txtEmployerDayPhoneUN.value;
                    objArgument.PhoneXT = txtEmployerDayPhoneXT.value;

                    objRet = window.showModalDialog("popEmployerInfo.htm", objArgument, "dialogHeight:210px;dialogWidth:660px;center:1;help:0;resizable:0;scroll:0;status:0")
                    if (objRet){
                        txtEmployerBusinessName.value = objRet.BusinessName;
                        txtEmployerAddress1.value = objRet.Address1;
                        txtEmployerAddress2.value = objRet.Address2;
                        txtEmployerCity.value = objRet.City;
                        txtEmployerState.value = objRet.State;
                        txtEmployerZip.value = objRet.Zip;
                        txtEmployerDayPhoneAC.value = objRet.PhoneAC;
                        txtEmployerDayPhoneEX.value = objRet.PhoneEX;
                        txtEmployerDayPhoneUN.value = objRet.PhoneUN;
                        txtEmployerDayPhoneXT.value = objRet.PhoneXT;
                        setControlDirty(document.getElementById("selRelatedInvolved").children[0]);
                        //document.getElementById("selRelatedInvolved").children[0].style.borderColor='#805050';
                    }
                    break;
                case "Attorney":
                    objArgument.BusinessName = txtAttorneyBusinessName.value;
                    objArgument.Address1 = txtAttorneyAddress1.value;
                    objArgument.Address2 = txtAttorneyAddress2.value;
                    objArgument.City = txtAttorneyCity.value;
                    objArgument.State = txtAttorneyState.value;
                    objArgument.Zip = txtAttorneyZip.value;
                    objArgument.PhoneAC = txtAttorneyDayPhoneAC.value;
                    objArgument.PhoneEX = txtAttorneyDayPhoneEX.value;
                    objArgument.PhoneUN = txtAttorneyDayPhoneUN.value;
                    objArgument.PhoneXT = txtAttorneyDayPhoneXT.value;
                    objArgument.busiSelect = selAttorneyBusinessTypeCD.outerHTML;

                    objRet = window.showModalDialog("popAttorneyInfo.htm", objArgument, "dialogHeight:210px;dialogWidth:700px;center:1;help:0;resizable:0;scroll:0;status:0")
                    if (objRet){
                        txtAttorneyBusinessName.value = objRet.BusinessName;
                        txtAttorneyAddress1.value = objRet.Address1;
                        txtAttorneyAddress2.value = objRet.Address2;
                        txtAttorneyCity.value = objRet.City;
                        txtAttorneyState.value = objRet.State;
                        txtAttorneyZip.value = objRet.Zip;
                        txtAttorneyDayPhoneAC.value = objRet.PhoneAC;
                        txtAttorneyDayPhoneEX.value = objRet.PhoneEX;
                        txtAttorneyDayPhoneUN.value = objRet.PhoneUN;
                        txtAttorneyDayPhoneXT.value = objRet.PhoneXT;
                        if (objRet.BusinessCode > 0)
                            selAttorneyBusinessTypeCD.optionselect(null, objRet.BusinessCode);
                        setControlDirty(document.getElementById("selRelatedInvolved").children[0]);
                        //document.getElementById("selRelatedInvolved").children[0].style.borderColor='#805050';
                    }
                    break;
            }

		//}
	}

  function ShowHideInjuryFlds()
  {
    if (InjuredCD.value == 'Y')
    {
      obj = document.getElementById("InjuryQuestFieldDiv");
      obj.style.visibility = "visible";
      obj.style.display = "inline";
      obj = document.getElementById("InjuryDescFieldDiv");
      obj.style.visibility = "visible";
      obj.style.display = "inline";
    }
    else
    {
      obj = document.getElementById("InjuryQuestFieldDiv");
      obj.style.visibility = "hidden";
      obj.style.display = "none";
      obj = document.getElementById("InjuryDescFieldDiv");
      obj.style.visibility = "hidden";
      obj.style.display = "none";
    }
  }

  function ValidatePhones()
  {
    if (validatePhoneSections(txtInvolvedDayPhoneAC, txtInvolvedDayPhoneEX, txtInvolvedDayPhoneNM) == false) return false;
    if (validatePhoneSections(txtInvolvedNightPhoneAC, txtInvolvedNightPhoneEX, txtInvolvedNightPhoneNM) == false) return false;
    if (validatePhoneSections(txtInvolvedAltPhoneAC, txtInvolvedAltPhoneEX, txtInvolvedAltPhoneNM) == false) return false;
    return true;
  }

  function checkDOB(){
    if (txtInvolvedDOB.value.length > 0) {
      var dt = new Date(txtInvolvedDOB.value)
      var dtNow = new Date();
      if (dt > dtNow) {
        ClientWarning("Date of Birth cannot be in the future. Please try again");
        txtInvolvedDOB.select();
        txtInvolvedDOB.focus();
        return false;
      }
      else
        return true;
    }
  }

	if (document.attachEvent)
		document.attachEvent("onclick", top.hideAllMenuScriptlets);
	else
		document.onclick = top.hideAllMenuScriptlets;


]]>
</SCRIPT>

</HEAD>
<BODY class="bodyAPDSubSub" onLoad="initSelectBoxes(); InitFields();PageInit();" tabIndex="-1">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>
  <DIV id="content17" name="Pedestrian" Update="uspClaimPedestrianUpdDetail" Insert="uspClaimPedestrianInsDetail" Delete="uspClaimPedestrianDel">
		<xsl:attribute name="CRUD"><xsl:value-of select="$PedestrianCRUD"/></xsl:attribute>
		<xsl:attribute name="LastUpdatedDate"><xsl:value-of select="Pedestrian/@SysLastUpdatedDate"/></xsl:attribute>
		<xsl:attribute name="ContextID"><xsl:value-of select="@InvolvedID"/></xsl:attribute>
		<xsl:attribute name="ContextName">Involved</xsl:attribute>

  <!-- Start Involved Info -->
  <TABLE class="paddedTable" unselectable="on" border="0" cellspacing="0" cellpadding="0">
  	<TR>
  		<TD unselectable="on">Name:</TD>
  		<TD unselectable="on" ><xsl:attribute name="nowrap"/>
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedTitle',2,'NameTitle',Pedestrian,'','',1)"/>
        <img unselectable="on" src="/images/spacer.gif" alt="" width="2" height="1" border="0"/>
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedFirstName',10,'NameFirst',Pedestrian,'','',2)"/>
        <img unselectable="on" src="/images/spacer.gif" alt="" width="2" height="1" border="0"/>
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedLastName',14,'NameLast',Pedestrian,'','',3)"/>
  		</TD>
  		<TD unselectable="on">
        Gender:
  		</TD>
  		<TD unselectable="on">
        <xsl:variable name="rtGender" select="Pedestrian/@GenderCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('GenderCD',2,'onSelectChange',string($rtGender),1,2,/Root/Reference[@List='Gender'],'Name','ReferenceID','','','','',11,1)"/>
  		</TD>
  		<TD unselectable="on" >Contacts:</TD>
  		<TD unselectable="on">
  			<SCRIPT>
  				addSelectCustomValues('selRelatedInvolved',2,'onRelatedInvolvedChange','',1,8,4,gsPedestrianCRUD,'','','Doctor','Doctor','Employer','Employer','Attorney','Attorney','','',28);
  			</SCRIPT>
  		</TD>
  	</TR>
  	<TR>
  		<TD unselectable="on">Address:</TD>
  		<TD unselectable="on" >
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedAddress1',21,'Address1',Pedestrian,'','',4)"/>
  		</TD>
  		<TD unselectable="on"><xsl:attribute name="nowrap"/>
        SSN/EIN:
  		</TD>
  		<TD unselectable="on">
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedDriverSSN',20,'FedTaxId',Pedestrian,'onBlur=SSNCheck(this)','',12)"/>
  		</TD>
  		<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		<TD unselectable="on">
  			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
  	</TR>
  	<TR>
  		<TD unselectable="on" >
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
  		<TD unselectable="on" >
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedAddress2',21,'Address2',Pedestrian,'','',5)"/>
  		</TD>
  		<TD unselectable="on">DOB:</TD>
  		<TD unselectable="on">
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedDOB',12,'BirthDate',Pedestrian,'onBlur=CalculateAge(this,txtInvolvedAge)',2,13)"/>
  		</TD>
  		<TD unselectable="on">Injured?</TD>
  		<TD unselectable="on">
        <xsl:variable name="injInit" select="Pedestrian/@InjuredCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('InjuredCD',2,'onInjC',string($injInit),1,4,/Root/Reference[@List='Injured'],'Name','ReferenceID','','','','',29,1)"/>
      </TD>
  	</TR>
  	<TR>
  		<TD unselectable="on">City:</TD>
  		<TD unselectable="on" >
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedCity',15,'AddressCity',Pedestrian,'','',6)"/>
  		</TD>
  		<TD unselectable="on">Age:</TD>
  		<TD unselectable="on">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedAge',2,'Age',Pedestrian,'onkeypress=NumbersOnly(window.event)','',14)"/>
  		</TD>
  		<TD unselectable="on">Injury:</TD>
  		<TD unselectable="on"><xsl:attribute name="nowrap"/>
        <DIV id="InjuryQuestFieldDiv" style="visibility:hidden; display:none;">
    			<xsl:variable name="ijRtype" select="Pedestrian/Injury/@InjuryTypeID"/>
    			<xsl:value-of disable-output-escaping="yes" select="js:AddSelect('InjuryTypeID',2,'onSelectChange',number($ijRtype),1,2,/Root/Reference[@List='InjuryType'],'Name','ReferenceID',0,'','','',30)"/>
        </DIV>
  		</TD>
  	</TR>
  	<TR>
  		<TD unselectable="on">State:</TD>
  		<TD unselectable="on">
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedState',2,'AddressState',Pedestrian,'',12,7)"/>
        Zip:
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedZip',6,'AddressZip',Pedestrian,'',10,8)"/>
  			<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtInvolvedState', 'txtInvolvedCity');"></xsl:text>
  		</TD>
  		<TD unselectable="on"><xsl:attribute name="nowrap"/>
        Best Time to Call:
  		</TD>
  		<TD unselectable="on">
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedBTC',15,'BestContactTime',Pedestrian,'','',14)"/>
  		</TD>
  		<TD unselectable="on" rowspan="2" valign="top">Injury Desc.:</TD>
  		<TD unselectable="on" rowspan="5" valign="top">
        <DIV id="InjuryDescFieldDiv" style="Visibility:hidden; display:none;">
    			<xsl:value-of disable-output-escaping="yes" select="js:TextArea('txtInvolvedInjuryDesc',24,6,'InjuryDescription',Pedestrian/Injury,'','',31)"/>
        </DIV>
  		</TD>
  	</TR>
  	<TR>
  		<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		<TD unselectable="on" ><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		<TD unselectable="on"><xsl:attribute name="nowrap"/>
        Best Num to Call:
  		</TD>
  		<TD unselectable="on">
        <xsl:variable name="rtBCP" select="Pedestrian/@BestContactPhoneCD"/>
        <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('BestContactPhoneCD',2,'onSelectChange',string($rtBCP),1,2,/Root/Reference[@List='BestContactPhone'],'Name','ReferenceID','','','','',15,1)"/>
  		</TD>
  	</TR>
  	<TR>
  		<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		<TD unselectable="on">Day:</TD>
  		<TD unselectable="on"><xsl:attribute name="nowrap"/>
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedDayPhoneAC',1,'DayAreaCode',Pedestrian,'',10,16)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedDayPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedDayPhoneEX',1,'DayExchangeNumber',Pedestrian,'',10,17)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedDayPhoneNM, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedDayPhoneNM',2,'DayUnitNumber',Pedestrian,'',10,18)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedDayPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
        <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedDayPhoneXT',3,'DayExtensionNumber',Pedestrian,'',10,19)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
  		</TD>
  		<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  	</TR>
  	<TR>
  		<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		<TD unselectable="on" ><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		<TD unselectable="on">Night:</TD>
  		<TD unselectable="on"><xsl:attribute name="nowrap"/>
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedNightPhoneAC',1,'NightAreaCode',Pedestrian,'',10,20)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedNightPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedNightPhoneEX',1,'NightExchangeNumber',Pedestrian,'',10,21)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedNightPhoneNM, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedNightPhoneNM',2,'NightUnitNumber',Pedestrian,'',10,22)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedNightPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
        <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedNightPhoneXT',3,'NightExtensionNumber',Pedestrian,'',10,23)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
  		</TD>
  		<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  	</TR>
  	<TR>
  		<TD unselectable="on" ><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		<TD unselectable="on" ><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		<TD unselectable="on">Alt.:</TD>
  		<TD unselectable="on"><xsl:attribute name="nowrap"/>
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedAltPhoneAC',1,'AlternateAreaCode',Pedestrian,'',10,24)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedAltPhoneEX, 'A');" onbeforedeactivate="checkAreaCode(this);"></xsl:text> -
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedAltPhoneEX',1,'AlternateExchangeNumber',Pedestrian,'',10,25)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedAltPhoneNM, 'E');" onbeforedeactivate="checkPhoneExchange(this);"></xsl:text> -
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedAltPhoneNM',2,'AlternateUnitNumber',Pedestrian,'',10,26)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);phoneAutoTab(this, txtInvolvedAltPhoneXT, 'U');" onbeforedeactivate="checkPhoneNumber(this);"></xsl:text>
        <img unselectable="on" src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
  			<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtInvolvedAltPhoneXT',3,'AlternateExtensionNumber',Pedestrian,'',10,27)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  		</TD>
  		<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  	</TR>
  	<TR>
  		<TD unselectable="on">Violation?</TD>
  		<TD unselectable="on" colspan="2">
   			<xsl:value-of disable-output-escaping="yes" select="js:AddCheckBox('ViolationFlag',boolean(Pedestrian[@ViolationFlag='1']),'1','0','','',9)"/>
      </TD>
  		<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  		<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>

  	</TR>
  	<TR>
  		<TD unselectable="on" colspan="4"><xsl:attribute name="nowrap"/>
  		  Violation Desc:
      	<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtViolationDesc',32,'ViolationDescription',Pedestrian,'','',10)"/>
      </TD>
  		<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
  		<TD unselectable="on">
  			<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  		</TD>
  		<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
  	</TR>
  </TABLE>
  <!-- End Involved Info -->
  <!-- Related Involved Info -->
  <DIV id="RelatedInfo" class="popupWorkarea" style="position:absolute; z-index:60000; top: 20px; left: 20px;">
  	<TABLE id="RelatedInfoTable" unselectable="on" border="0" cellspacing="0" cellpadding="0">
  		<TR>
  			<TD unselectable="on" colspan="4" height="20" valign="top" class="boldtext" bgcolor="#e0e0e0" style="vertical-align:middle;text-align : center;" nowrap="">
        </TD>
  		</TR>
  		<TR style="height:4px">
  			<TD unselectable="on" colspan="4" height="4" >
        </TD>
  		</TR>
  		<TR>
  			<TD unselectable="on" nowrap="">
          Name:
  			</TD>
  			<TD unselectable="on" nowrap="">
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorBusinessName',50,'DoctorBusinessName',Pedestrian/Doctor,'style=visibility:hidden;display=none;','',32)"/>
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerBusinessName',50,'EmployerBusinessName',Pedestrian/Employer,'style=visibility:hidden;display=none;','',33)"/>
					<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyBusinessName',50,'AttorneyBusinessName',Pedestrian/Attorney,'style=visibility:hidden;display=none;','',34)"/>
  				<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  			</TD>
        <TD unselectable="on" nowrap="">
          Address:
        </TD>
  			<TD unselectable="on">
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorAddress1',50,'DoctorAddress1',Pedestrian/Doctor,'style=visibility:hidden;display=none;','',35)"/>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerAddress1',50,'EmployerAddress1',Pedestrian/Employer,'style=visibility:hidden;display=none;','',36)"/>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyAddress1',50,'AttorneyAddress1',Pedestrian/Attorney,'style=visibility:hidden;display=none;','',37)"/>
  			</TD>
  		</TR>
  		<TR>
	  		<TD unselectable="on" nowrap="">
	        Bus. Type:
	      </TD>
	  		<TD unselectable="on" nowrap="">
          <xsl:variable name="rtABT" select="Pedestrian/Attorney/@AttorneyBusinessTypeCD"/>
          <xsl:value-of disable-output-escaping="yes" select="js:AddSelect('AttorneyBusinessTypeCD',2,'onSelectChange',string($rtABT),1,2,/Root/Reference[@List='BusinessType'],'Name','ReferenceID','','','','',50,1)"/>
	      </TD>
  			<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  			<TD unselectable="on">
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorAddress2',50,'DoctorAddress2',Pedestrian/Doctor,'style=visibility:hidden;display=none;','',38)"/>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerAddress2',50,'EmployerAddress2',Pedestrian/Employer,'style=visibility:hidden;display=none;','',39)"/>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyAddress2',50,'AttorneyAddress2',Pedestrian/Attorney,'style=visibility:hidden;display=none;','',40)"/>
  			</TD>
  		</TR>
  		<TR>
  			<TD unselectable="on">Phone:</TD>
  			<TD unselectable="on" nowrap="">
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorDayPhoneAC',1,'DoctorDayAreaCode',Pedestrian/Doctor,'style=visibility:hidden;display=none;',10,51)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="checkAreaCode(this);"></xsl:text>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerDayPhoneAC',1,'EmployerDayAreaCode',Pedestrian/Employer,'style=visibility:hidden;display=none;',10,52)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="checkAreaCode(this);"></xsl:text>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyDayPhoneAC',1,'AttorneyDayAreaCode',Pedestrian/Attorney,'style=visibility:hidden;display=none;',10,53)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="checkAreaCode(this);"></xsl:text>
					-
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorDayPhoneEX',1,'DoctorDayExchangeNumber',Pedestrian/Doctor,'style=visibility:hidden;display=none;',10,54)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="checkPhoneExchange(this);"></xsl:text>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerDayPhoneEX',1,'EmployerDayExchangeNumber',Pedestrian/Employer,'style=visibility:hidden;display=none;',10,55)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="checkPhoneExchange(this);"></xsl:text>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyDayPhoneEX',1,'AttorneyDayExchangeNumber',Pedestrian/Attorney,'style=visibility:hidden;display=none;',10,56)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="checkPhoneExchange(this);"></xsl:text>
					-
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorDayPhoneUN',2,'DoctorDayUnitNumber',Pedestrian/Doctor,'style=visibility:hidden;display=none;',10,57)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="checkPhoneNumber(this);"></xsl:text>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerDayPhoneUN',2,'EmployerDayUnitNumber',Pedestrian/Employer,'style=visibility:hidden;display=none;',10,58)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="checkPhoneNumber(this);"></xsl:text>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyDayPhoneUN',2,'AttorneyDayUnitNumber',Pedestrian/Attorney,'style=visibility:hidden;display=none;',10,59)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="checkPhoneNumber(this);"></xsl:text>
					<img src="/images/spacer.gif" alt="" width="4" height="1" border="0"/>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorDayPhoneXT',3,'DoctorDayExtensionNumber',Pedestrian/Doctor,'style=visibility:hidden;display=none;',10,60)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerDayPhoneXT',3,'EmployerDayExtensionNumber',Pedestrian/Employer,'style=visibility:hidden;display=none;',10,61)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyDayPhoneXT',3,'AttorneyDayExtensionNumber',Pedestrian/Attorney,'style=visibility:hidden;display=none;',10,62)"/><xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);"></xsl:text>
  			</TD>
  			<TD unselectable="on">City:</TD>
  			<TD unselectable="on">
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorCity',30,'DoctorAddressCity',Pedestrian/Doctor,'style=visibility:hidden;display=none;','',41)"/>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerCity',30,'EmployerAddressCity',Pedestrian/Employer,'style=visibility:hidden;display=none;','',42)"/>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyCity',30,'AttorneyAddressCity',Pedestrian/Attorney,'style=visibility:hidden;display=none;','',43)"/>
        </TD>
  		</TR>
  		<TR>
  			<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  			<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  			<TD unselectable="on">State:</TD>
  			<TD unselectable="on" nowrap="">
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorState',2,'DoctorAddressState',Pedestrian/Doctor,'style=visibility:hidden;display=none;',12,44)"/>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerState',2,'EmployerAddressState',Pedestrian/Employer,'style=visibility:hidden;display=none;',12,45)"/>
  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyState',2,'AttorneyAddressState',Pedestrian/Attorney,'style=visibility:hidden;display=none;',12,46)"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Zip:

  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtDoctorZip',7,'DoctorAddressZip',Pedestrian/Doctor,'',10,47)"/>
 					<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtDoctorState', 'txtDoctorCity');" style='visibility:hidden;display:none;'></xsl:text>

  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtEmployerZip',7,'EmployerAddressZip',Pedestrian/Employer,'',10,48)"/>
 					<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtEmployerState', 'txtEmployerCity');" style='visibility:hidden;display:none;'></xsl:text>

  				<xsl:value-of disable-output-escaping="yes" select="js:InputBox('txtAttorneyZip',7,'AttorneyAddressZip',Pedestrian/Attorney,'',10,49)"/>
 					<xsl:text disable-output-escaping="yes">onKeypress="NumbersOnly(event);" onBlur="ValidateZip(this,'txtAttorneyState', 'txtAttorneyCity');" style='visibility:hidden;display:none;'></xsl:text>
        </TD>
  		</TR>
  		<TR style="height:4px">
  			<TD unselectable="on" colspan="4" height="4" >
        </TD>
  		</TR>
  		<TR style="height:4px">
  			<TD unselectable="on" colspan="4" height="4"  bgcolor="#e0e0e0" nowrap="">
        </TD>
  		</TR>
  		<TR style="height:4px">
  			<TD unselectable="on" colspan="4" height="4" >
        </TD>
  		</TR>
  		<TR>
  			<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  			<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  			<TD unselectable="on"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</TD>
  			<TD unselectable="on" align="right" nowrap="" >
          <INPUT type="button" name="CloseAttorneyInfo" value="Close" onClick="ShowHideTxtArea('RelatedInfo', 'hidden')" class="formbutton"/>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
  			</TD>
  		</TR>
  	</TABLE>
  </DIV>
 </DIV>



</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>
