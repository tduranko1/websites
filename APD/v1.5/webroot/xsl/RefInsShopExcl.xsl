<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="Insurance">

<xsl:import href="msxsl/msjs-client-library-htc.js"/>

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function myNameConcat(firstPart, secondPart, delim){
        if (delim == "") delim = ", ";
        if (firstPart != "" && secondPart != "")
            return firstPart + delim + secondPart;
        else if (firstPart != "" || secondPart != "")
            return firstPart + secondPart + "&nbsp;";
        else if (firstPart == "" && secondPart == "")
            return "&nbsp;";

    }
  ]]>
</msxsl:script>

<xsl:output method="html" indent="yes" encoding="UTF-8" />
<xsl:param name="InsID"/>
<xsl:param name="UseCEI"/>

<!-- Permissions params - names must end in 'CRUD' -->
<xsl:param name="InfoCRUD" select="Client Configuration"/>

<xsl:template match="/Root">
<HTML>

<HEAD>
<TITLE>Data Administration: Insurance Companies</TITLE>
<LINK rel="stylesheet" href="/css/APDControls.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/APDGrid.css" type="text/css"/>


<SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>
<SCRIPT language="Javascript">
    var sCRUD = "<xsl:value-of select="$InfoCRUD"/>";
    var sUseCEI = "<xsl:value-of select="/Root/@UseCEIShopsFlag"/>";
    
<![CDATA[

    var gbDirty = false;
    var ArrExclusion = new Array();
    var strLocList = "";

    function pageInit(){
        btnNavFirstInc.value = btnNavFirstExc.value = unescape("%u2039") + unescape("%u2039");
        btnNavPrevInc.value = btnNavPrevExc.value = unescape("%u2039");
        btnNavNextInc.value = btnNavNextExc.value = unescape("%u203A");
        btnNavLastInc.value = btnNavLastExc.value = unescape("%u203A") + unescape("%u203A");
        stat1.style.top = (document.body.offsetHeight - 75) / 2;
        stat1.style.left = (document.body.offsetWidth - 240) / 2;

        if (sCRUD.indexOf("U") == -1)
            window.setTimeout("disableControls(true)", 250);
    }

    function InitInputs(){
    }

    function disableControls(val){
        //btnExclude.CCDisabled = val;
        btnRemove.CCDisabled = val;
        btnClear.CCDisabled = val;
        //SelectAll.CCDisabled = val;
        SelectAllExc.CCDisabled = val;
        tblExclusion.disabled = val;
        var oRows = tblExclusion.rows;
        var iRows = oRows.length;
        for(var i = 0; i < iRows; i++){
            oRows[i].cells[0].firstChild.CCDisabled = val;
        }

        SelectAllInc.CCDisabled = val;
        tblInclude.disabled = val;
        var oRows = tblInclude.rows;
        var iRows = oRows.length;
        for(var i = 0; i < iRows; i++){
            oRows[i].cells[0].firstChild.CCDisabled = val;
        }

        try {
          frmShopSearch.txtSearchName.CCDisabled = val;
          frmShopSearch.txtSearchCity.CCDisabled = val;
          frmShopSearch.State.CCDisabled = val;
          frmShopSearch.btnSearch1.CCDisabled = val;
        } catch (e) {
        }
    }

    function doShopSearch(){
        var URLSplit = document.URL.split("/");
        URLSplit[URLSplit.length - 1] = "refInsShopSearch.asp";
        var sShopSearchURL = URLSplit.join("/");
        sShopSearchURL += "?InsuranceCompanyID=" + curInsID;
        sShopSearchURL += "&ShopTypeCode=P";
        sShopSearchURL += "&UseCEI=" + sUseCEI;
        frmShopSearch.frameElement.src = sShopSearchURL;
    }


    function btnShopSearchClick(){
        if (frmShopSearch.txtSearchName.value == "" &&
            frmShopSearch.txtSearchCity.value == "" &&
            frmShopSearch.State.value == "") {
            ClientWarning("Please select a search criteria and then click search.");
            return;
        }
        stat1.Show("Searching...");
        window.setTimeout("btnShopSearchClick2()", 100);
    }
    
    function btnShopSearchClick2(){
        frmShopSearch.divSearchStatus.style.display = "inline";
        var URLSplit = document.URL.split("/");
        URLSplit[URLSplit.length - 1] = "refInsShopSearch.asp";
        var sShopSearchURL = URLSplit.join("/");
        sShopSearchURL += "?InsuranceCompanyID=" + curInsID;
        sShopSearchURL += "&ShopName=" + escape(frmShopSearch.txtSearchName.value.replace(/'/g, "''"));
        sShopSearchURL += "&ShopCity=" + escape(frmShopSearch.txtSearchCity.value.replace(/'/g, "''"));
        sShopSearchURL += "&ShopState=" + frmShopSearch.State.value;
        sShopSearchURL += "&ShopTypeCode=A";
        sShopSearchURL += "&UseCEI=" + sUseCEI;
        //alert(sShopSearchURL);
        frmShopSearch.frameElement.src = sShopSearchURL;
    }

    var bDeleting = false;
    var objShop = null;
    
    function addExclusion(obj) {
        objShop = obj;
        stat1.Show("Adding shop to the Exclusion list...");
        window.setTimeout("addExclusion2()", 100);
    }
    
    function addExclusion2() {
        if (parent.sCRUD.indexOf("U") == -1) return;
        if (bDeleting) return;
        obj = objShop;
        objShop = null;        
        var bDoReloadXML = false;
        
        if (tabShopExclude.selected == false) {
          tabGrpInsShopIncExc.SelectTab(0);
        }

        if (obj && obj.tagName == "APDCheckBox") {
          var objTR = obj.parentElement.parentElement;
          var shopID = objTR.lastChild.innerText;
          if (obj.value == 0) { // obj is going to be set checked.
            //check if the current shop exists in the exclusion list
            var oRootNode = xmlShopExclude.XMLDocument.selectSingleNode("/Root");
            if (oRootNode.childNodes.length == 0)
              bDoReloadXML = true;

            if (xmlShopExclude.XMLDocument.selectSingleNode("/Root/Shop[@ShopLocationID='" + shopID + "']") == null && // shop does not already exist in the excluded list
                xmlShopInclude.XMLDocument.selectSingleNode("/Root/Shop[@ShopLocationID='" + shopID + "']") == null){ // shop does not exist in the included list
            
              var oShopNode = frmShopSearch.xmlSearchResult.XMLDocument.selectSingleNode("/Root/Entity[@EntityID='" + shopID + "']");
              var oNode = xmlShopExclude.XMLDocument.createElement("Shop");
              if (oNode) {
                var oAttrib = xmlShopExclude.XMLDocument.createAttribute("ShopLocationID");
                oAttrib.value = oShopNode.getAttribute("EntityID");
                oNode.attributes.setNamedItem(oAttrib);
                
                var oAttrib = xmlShopExclude.XMLDocument.createAttribute("Name");
                oAttrib.value = oShopNode.getAttribute("Name");
                oNode.attributes.setNamedItem(oAttrib);

                var oAttrib = xmlShopExclude.XMLDocument.createAttribute("AddressCity");
                oAttrib.value = oShopNode.getAttribute("AddressCity");
                oNode.attributes.setNamedItem(oAttrib);

                var oAttrib = xmlShopExclude.XMLDocument.createAttribute("AddressState");
                oAttrib.value = oShopNode.getAttribute("AddressState");
                oNode.attributes.setNamedItem(oAttrib);

                var oAttrib = xmlShopExclude.XMLDocument.createAttribute("CEI_PS");
                oAttrib.value = oShopNode.getAttribute("CEI_PS");
                oNode.attributes.setNamedItem(oAttrib);

                oRootNode.appendChild(oNode);
              }

              //reload the XML.
              if (bDoReloadXML == true)
                xmlShopExclude.loadXML(xmlShopExclude.xml);
  
              showShopExclude();
              btnSave.CCDisabled = false;
              setDirty();
              window.setTimeout("navMoveLast('Exc')", 250);
              
            }
          }
        }
        stat1.Hide();
    }
    
    function addInclusion(obj) {
      objShop = obj;
      stat1.Show("Adding shop to the Inclusion list...");
      window.setTimeout("addInclusion2()", 100);
    }

    function addInclusion2(obj) {
        if (parent.sCRUD.indexOf("U") == -1) return;
        if (bDeleting) return;
        var bDoReloadXML = false;
        
        obj = objShop;
        objShop = null;        

        if (tabShopInclude.selected == false) {
          tabGrpInsShopIncExc.SelectTab(1);
        }

        if (obj && obj.tagName == "APDCheckBox") {
          var objTR = obj.parentElement.parentElement;
          var shopID = objTR.lastChild.innerText;
          if (obj.value == 0) { // obj is going to be set checked.
            //check if the current shop exists in the exclusion list
            var oRootNode = xmlShopInclude.XMLDocument.selectSingleNode("/Root");
            if (oRootNode.childNodes.length == 0)
              bDoReloadXML = true;
            if (xmlShopExclude.XMLDocument.selectSingleNode("/Root/Shop[@ShopLocationID='" + shopID + "']") == null && // shop does not already exist in the excluded list
                xmlShopInclude.XMLDocument.selectSingleNode("/Root/Shop[@ShopLocationID='" + shopID + "']") == null){ // shop does not exist in the included list
            
              var oShopNode = frmShopSearch.xmlSearchResult.XMLDocument.selectSingleNode("/Root/Entity[@EntityID='" + shopID + "']");
              var oNode = xmlShopInclude.XMLDocument.createElement("Shop");
              if (oNode) {
                var oAttrib = xmlShopInclude.XMLDocument.createAttribute("ShopLocationID");
                oAttrib.value = oShopNode.getAttribute("EntityID");
                oNode.attributes.setNamedItem(oAttrib);
                
                var oAttrib = xmlShopInclude.XMLDocument.createAttribute("Name");
                oAttrib.value = oShopNode.getAttribute("Name");
                oNode.attributes.setNamedItem(oAttrib);

                var oAttrib = xmlShopInclude.XMLDocument.createAttribute("AddressCity");
                oAttrib.value = oShopNode.getAttribute("AddressCity");
                oNode.attributes.setNamedItem(oAttrib);

                var oAttrib = xmlShopInclude.XMLDocument.createAttribute("AddressState");
                oAttrib.value = oShopNode.getAttribute("AddressState");
                oNode.attributes.setNamedItem(oAttrib);

                var oAttrib = xmlShopInclude.XMLDocument.createAttribute("CEI_PS");
                oAttrib.value = oShopNode.getAttribute("CEI_PS");
                oNode.attributes.setNamedItem(oAttrib);

                oRootNode.appendChild(oNode);
              }

              //reload the XML.
              if (bDoReloadXML == true)
                xmlShopInclude.loadXML(xmlShopInclude.xml);
  
              showShopInclude();
              btnSave.CCDisabled = false;
              setDirty();
              window.setTimeout("navMoveLast('Inc')", 250);
              
            }
          }
        }
        stat1.Hide();
    }
    
    function selectAll2(){
        var sIncExc = "";
        var oRows = null;
        var iCheckAll = 0;
        
        if (tabShopInclude.selected == true) {
          sIncExc = "Inc";
          oRows = tblInclude.rows;
          iCheckAll = SelectAllInc.value;
        } else {
          sIncExc = "Exc";
          oRows = tblExclusion.rows;
          iCheckAll = SelectAllExc.value;
        }

        disableButtons(true, sIncExc);
        var iRows = oRows.length;

        for (var i = 0; i < iRows; i++){
            var ele = oRows[i].cells[0].firstChild;
            if (ele)
              ele.value = iCheckAll;
        }
        disableButtons(false, sIncExc);
     }

    function doRemoveFromExclude(){
        if (parent.sCRUD.indexOf("U") == -1) return;
        
        var sIncExc = "";
        
        if (tabShopInclude.selected == true) {
          sIncExc = "Inc";
          if (xmlShopInclude.XMLDocument.firstChild.childNodes.length == 0) return;
        } else {
          sIncExc = "Exc";
          if (xmlShopExclude.XMLDocument.firstChild.childNodes.length == 0) return;
        }
        
        var sRet = YesNoMessage("Confirm Delete", "Do you want to delete the selected shop(s) from the list?.\n" + 
                                                  "Note: Remember to click on save button to save these changes.");
        if (sRet != "Yes") return;
        
        disableButtons(true, sIncExc);
        
        var oRows = null;
        var oXML = null;
        var oSelectAll = null;
        if (sIncExc == "Exc") {
          oRows = tblExclusion.rows;
          oXML = xmlShopExclude;
          oSelectAll = SelectAllExc;
        } else {
          oRows = tblInclude.rows;
          oXML = xmlShopInclude;
          oSelectAll = SelectAllInc;
        }
          
        var iRows = oRows.length - 1;
        var shopLocationID = null;

        if (iRows >= 0){
            for (var i = iRows; i >= 0 ; i--){
                var ele = oRows[i].cells[0].firstChild;
                if (ele && ele.value == 1) {
                  shopLocationID = oRows[i].cells[4].innerText;
                  if (shopLocationID > 0) {
                    var oShopNode = oXML.XMLDocument.selectSingleNode("/Root/Shop[@ShopLocationID='" + shopLocationID + "']");
                    if (oShopNode) {
                      oXML.XMLDocument.firstChild.removeChild(oShopNode);
                    }
                  }
                }
            }
        }

        if (oSelectAll.value == 1)
          oSelectAll.value = 0;
        if (sIncExc == "Exc")
          showShopExclude();
        else
          showShopInclude();
        disableButtons(false, sIncExc);
        setDirty();
        btnSave.CCDisabled = false;
    }

    function setDirty(){
        gbDirty = true;
    }


    function getExcludedShopList(){
        var sExcludedList = "";
        var shopID = "";
        
        var oExclusionNodes = xmlShopExclude.XMLDocument.selectNodes("/Root/Shop");
        for (var i = 0; i < oExclusionNodes.length; i++){
          sExcludedList += oExclusionNodes[i].getAttribute("ShopLocationID") + ",";
        }
        //remove the last comma
        sExcludedList = sExcludedList.substring(0, sExcludedList.length - 1);
        return sExcludedList;
    }

    function getIncludedShopList(){
        var sIncludedList = "";
        var shopID = "";
        
        var oInclusionNodes = xmlShopInclude.XMLDocument.selectNodes("/Root/Shop");
        for (var i = 0; i < oInclusionNodes.length; i++){
          sIncludedList += oInclusionNodes[i].getAttribute("ShopLocationID") + ",";
        }
        //remove the last comma
        sIncludedList = sIncludedList.substring(0, sIncludedList.length - 1);
        return sIncludedList;
    }
    
        var recCount;
        var curPageExc;
        var curPageInc;
        var pgCountExc;
        var pgCountInc;
        
        function navMoveFirst(sIncExc){
		      if (sIncExc == 'Exc') {
              if (tblExclusion) {
                tblExclusion.firstPage();
                curPageExc = 1;
                pgShopExc.innerText = "Page: " + curPageExc + " / " + pgCountExc;
              }
          } else {
              if (tblInclude) {
                tblInclude.firstPage();
                curPageInc = 1;
                pgShopExc.innerText = "Page: " + curPageInc + " / " + pgCountInc;
              }
          }
        }

        function navMovePrev(sIncExc){
		      if (sIncExc == 'Exc') {
              if (tblExclusion && curPageExc > 1) {
                tblExclusion.previousPage();
                curPageExc--;
                pgShopExc.innerText = "Page: " + curPageExc + " / " + pgCountExc;
              }
          } else {
              if (tblInclude && curPageInc > 1) {
                tblInclude.previousPage();
                curPageInc--;
                pgShopInc.innerText = "Page: " + curPageInc + " / " + pgCountInc;
              }
          }
        }

        function navMoveNext(sIncExc){
		      if (sIncExc == 'Exc') {
              if (tblExclusion && curPageExc < pgCountExc) {
                tblExclusion.nextPage();
                curPageExc++;
                pgShopExc.innerText = "Page: " + curPageExc + " / " + pgCountExc;
              }
          } else {
              if (tblInclude && curPageInc < pgCountInc) {
                tblInclude.nextPage();
                curPageInc++;
                pgShopInc.innerText = "Page: " + curPageInc + " / " + pgCountInc;
              }
          }
        }

        function navMoveLast(sIncExc){
		      if (sIncExc == 'Exc') {
              if (tblExclusion) {
                tblExclusion.lastPage();
                curPageExc = pgCountExc;
                pgShopExc.innerText = "Page: " + curPageExc + " / " + pgCountExc;
              }
          } else {
              if (tblInclude) {
                tblInclude.lastPage();
                curPageInc = pgCountInc;
                pgShopInc.innerText = "Page: " + curPageInc + " / " + pgCountInc;
              }
          }
        }

        function showShopExclude(){
            if (__blnPageInitialized == false){
               window.setTimeout("showShopExclude()", 100);
               return;
            }
            var sPageSize = tblExclusion.getAttribute("DATAPAGESIZE");
            if (sPageSize != "")
              iPageSize = parseInt(sPageSize, 10);
            
            if (iPageSize < 1)
              iPageSize = 8; //default 8 records at a time

            recCount = xmlShopExclude.XMLDocument.firstChild.childNodes.length;
            if (recCount > 0)
              tblExclusion.dataSrc = "#xmlShopExclude";

            curPageExc = 1;
            
            pgCountExc = Math.ceil(recCount / iPageSize);
            if (pgCountExc == 0) {
                curPageExc = 0;
                pgShopExc.innerText = "";
                tblExclusion.style.display = "none";
                //SelectAllExc.CCDisabled = true;
                disableButtons(true, "Exc");
            }
            else {
                pgShopExc.innerText = "Page: " + curPageExc + " / " + pgCountExc;
                tblExclusion.style.display = "inline";
                disableButtons(false, "Exc");
            }
        }

        function showShopInclude(){
            if (__blnPageInitialized == false){
               window.setTimeout("showShopInclude()", 100);
               return;
            }
            var sPageSize = tblInclude.getAttribute("DATAPAGESIZE");
            if (sPageSize != "")
              iPageSize = parseInt(sPageSize, 10);
            
            if (iPageSize < 1)
              iPageSize = 8; //default 8 records at a time

            recCount = xmlShopInclude.XMLDocument.firstChild.childNodes.length;
            if (recCount > 0)
              tblInclude.dataSrc = "#xmlShopInclude";

            curPageInc = 1;
            
            pgCountInc = Math.ceil(recCount / iPageSize);
            if (pgCountInc == 0) {
                curPageInc = 0;
                pgShopInc.innerText = "";
                tblInclude.style.display = "none";
                disableButtons(true, "Inc");
            }
            else {
                pgShopInc.innerText = "Page: " + curPageInc + " / " + pgCountInc;
                tblInclude.style.display = "inline";
                disableButtons(false, "Inc");
            }
        }

        function disableButtons(val, sIncExc){
    			if (sIncExc == "Inc") {
            btnNavFirstInc.CCDisabled = val;
            btnNavPrevInc.CCDisabled = val;
            btnNavNextInc.CCDisabled = val;
            btnNavLastInc.CCDisabled = val;
    			} else {
            btnNavFirstExc.CCDisabled = val;
            btnNavPrevExc.CCDisabled = val;
            btnNavNextExc.CCDisabled = val;
            btnNavLastExc.CCDisabled = val;
    			}
        }
        
        function searchReady(){
          if (frmShopSearch.frameElement.readyState == "complete") {
            try {stat1.Hide(); } catch (e) {}
          }
        }
        
        function doSave(){
          stat1.Show("Saving changes ...");
          window.setTimeout("doSave2()", 150);
        }
        
        function doSave2(){
          var sProc = "uspAdmInsUpdShopList";
          var sRequest = "";
          
          sRequest = "InsuranceCompanyID=" + parent.txtID.value + "&" +
                     "ShopIncludeList=" + getIncludedShopList() + "&" +
                     "ShopExcludeList=" + getExcludedShopList() + "&" +
                     "SysUserID=" + parent.curUser;

          if (sProc != "" && sRequest != ""){
              var aRequests = new Array();
              aRequests.push( { procName : sProc,
                                method   : "ExecuteSpNp",
                                data     : sRequest }
                            );
              var objRet = XMLSave(makeXMLSaveString(aRequests));
              gbDirty = false;
              reloadShops();
          }
        }
        
        function reloadShops() {
          gbDirty = false;
          window.location.reload();
        }
        
        function doClear(){
          if (tabShopInclude.selected == true) {
            var sRet = YesNoMessage("Confirm Delete", "Do you want all the shops from the Include list mark for deletion?.\n" + 
                                                      "Note: Remember to click on save button to save these changes.");
            if (sRet != "Yes") return;
            stat1.Show("Clearing shops from the included list...");
          } else {
            var sRet = YesNoMessage("Confirm Delete", "Do you want all the shops from the Exclude list mark for deletion?.\n" + 
                                                      "Note: Remember to click on save button to save these changes.");
            if (sRet != "Yes") return;
            stat1.Show("Clearing shops from the excluded list...");
          }
          window.setTimeout("doClear2()", 150);
        }
        
        function doClear2(){
          var oXML = null;
          var oRootXML = null;
          if (tabShopInclude.selected == true) {
            oXML = xmlShopInclude;
            oRootXML = xmlShopInclude.XMLDocument.selectSingleNode("/Root");
          } else {
            oXML = xmlShopExclude;
            oRootXML = xmlShopExclude.XMLDocument.selectSingleNode("/Root");
          }
          if (oRootXML.childNodes.length > 0) {
            for (var i = oRootXML.childNodes.length - 1; i >= 0 ; i--) {
              oRootXML.removeChild(oRootXML.childNodes[i]);
            }
          }
          
          if (tabShopInclude.selected == true)
            showShopInclude();
          else
            showShopExclude();
            
          btnClear.CCDisabled = true;
          btnSave.CCDisabled = false;
          setDirty();
          stat1.Hide();
        }
        
        function setTitles(){
          if (tabShopInclude.selected == true) {
            btnClear.title = "Clear all shops from included list";
            btnRemove.title = "Remove selected shops from included list";
            var oRootXML = null;
            if (xmlShopInclude) {
              oRootXML = xmlShopInclude.XMLDocument.selectSingleNode("/Root");
              if (oRootXML.childNodes.length > 0)
                btnClear.CCDisabled = (sCRUD.indexOf("U") == -1 ? true : false);
              else
                btnClear.CCDisabled = (sCRUD.indexOf("U") == -1 ? true : false);
            }            
          } else {
            btnClear.title = "Clear all shops from excluded list";
            btnRemove.title = "Remove selected shops from excluded list";
            var oRootXML = null;
            oRootXML = xmlShopExclude.XMLDocument.selectSingleNode("/Root");
            if (xmlShopExclude) {
              if (oRootXML.childNodes.length > 0)
                btnClear.CCDisabled = (sCRUD.indexOf("U") == -1 ? true : false);
              else
                btnClear.CCDisabled = (sCRUD.indexOf("U") == -1 ? true : false);
            }
          }
        }

]]>

</SCRIPT>
</HEAD>
<BODY class="bodyAPDSub" unselectable="on" style="background-color:#FFFFFF;overflow:hidden" onload="setTimeout('pageInit()', 100)" tabIndex="-1" topmargin="0"  leftmargin="0">
    <IE:APDButton id="btnRemove" name="btnRemove" value="Del" CCTabIndex="24" width="35" onButtonClick="doRemoveFromExclude()" title="Remove from the Included/Excluded List" style="position:absolute; top:-3px; left:198px"/>
    <IE:APDButton id="btnClear" name="btnClear" value="Clear All" CCDisabled="true" CCTabIndex="1" width="63" onButtonClick="doClear()" style="position:absolute; top:-3px; left:235px"/>
    <IE:APDButton id="btnSave" name="btnSave" value="Save" CCDisabled="true" CCTabIndex="2" onButtonClick="doSave()" style="position:absolute; top:-3px; left:300px" title="Save changes to the include/exclude list"/>
    <span style="width:360px;height:225px;overflow:hidden;">
        <xsl:call-template name="ExcludedList"/>
    </span>
    <span style="width:355px;height:200px;position:absolute;top:0px;left:360px">
        <xsl:call-template name="SearchList"/>
    </span>
    <IE:APDStatus id="stat1" name="stat1"/>
</BODY>
</HTML>
</xsl:template>

<xsl:template name="ExcludedList">

<IE:APDTabGroup id="tabGrpInsShopIncExc" name="tabGrpInsShopIncExc" width="355px" height="221px" preselectTab="0" style="padding:0px;position:relative">
	<IE:APDTab id="tabShopExclude" name="tabShopExclude" caption="Exclude" tabPage="tabPageShopExclude" onTabSelect="setTitles()"/>
	<IE:APDTab id="tabShopInclude" name="tabShopInclude" caption="Include" tabPage="tabPageShopInclude" onTabSelect="setTitles()"/>


	<IE:APDTabPage id="tabPageShopExclude" name="tabPageShopExclude" style="display:none">
	        <table border="0" cellspacing="0" cellpadding="0" unselectable="on">
	          <colgroup>
	              <col width="80px"/>
	              <col width="130px"/>
	              <col width="80px"/>
	          </colgroup>
	          <tr valign="bottom">
	            <td><strong>Excluded List:</strong></td>
	            <td>
	              <IE:APDButton id="btnNavFirstExc" name="btnNavFirstExc" value="&lt; &lt;" onButtonClick="navMoveFirst('Exc')"/>
	              <IE:APDButton id="btnNavPrevExc" name="btnNavPrevExc" value="&lt; " onButtonClick="navMovePrev('Exc')"/>
	              <IE:APDButton id="btnNavNextExc" name="btnNavNextExc" value="&gt;" onButtonClick="navMoveNext('Exc')"/>
	              <IE:APDButton id="btnNavLastExc" name="btnNavLastExc" value="&gt; &gt;" onButtonClick="navMoveLast('Exc')"/>
	            </td>
	            <td>
	              <span id="pgShopExc" name="pgShopExc" unselectable="on">
	              </span>
	            </td>
	          </tr>
	        </table>
	        <table border="0" cellspacing="0" cellpadding="2" style="width:100%;" unselectable="on">
	            <colgroup>
	                <col width="21px"/>
	                <col width="160px"/>
	                <col width="106px"/>
	                <col width="*"/>
	            </colgroup>
	            <tr style="height:21px;cursor:hand;">
	                <td unselectable="on" class="TableSortHeader" title="Select/Deselect All current page">
	                        <IE:APDCheckBox id="SelectAllExc" name="SelectAllExc" canDirty="false" onChange="selectAll2()"/>
	                </td>
	                <td unselectable="on" class="TableSortHeader">Name</td>
	                <td unselectable="on" class="TableSortHeader">City, State</td>
	                <td unselectable="on" class="TableSortHeader">Prgm</td>
	            </tr>
	        </table>
	        <DIV unselectable="on" class="autoflowTable" style="width:100%; height:149px;">
	            <table border="0" cellspacing="1" cellpadding="2" style="table-layout:fixed;" name="tblExclusion" id="tblExclusion" data1src="#xmlShopExclude" DATAPAGESIZE="7">
	                <colgroup>
	                    <col width="25px"/>
	                    <col width="166px"/>
	                    <col width="111px"/>
	                    <col width="*"/>
	                </colgroup>
	                <tr style="height:20px">
	                    <td unselectable="on" class="GridTypeTD" nowrap="" >
	                        <IE:APDCheckBox canDirty="false"/>
	                    </td>
	                    <td unselectable="on" class="GridTypeTD" nowrap="" style="overflow:hidden;text-align:left;"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<span datafld="Name" DATAFORMATAS="html" unselectable="on" style="width:99%;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;margin:0px;"></span></td>
	                    <td unselectable="on" class="GridTypeTD" nowrap="" style="overflow:hidden;text-align:left;">
	                      <span style="width:99%;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;margin:0px;padding:0px;">
	                        <span datafld="AddressCity" DATAFORMATAS="html" unselectable="on"></span>, <span datafld="AddressState" DATAFORMATAS="html" unselectable="on"></span>
	                      </span>
	                    </td>
	                    <td unselectable="on" class="GridTypeTD" nowrap="" style="overflow:hidden;text-align:center;"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<span datafld="CEI_PS" DATAFORMATAS="html" unselectable="on"></span></td> 
	                    <td style="display:none"><div datafld="ShopLocationID"></div></td>
	                </tr>
	            </table>
	        </DIV>
	</IE:APDTabPage>

	<IE:APDTabPage id="tabPageShopInclude" name="tabPageShopInclude" style="display:none">
	        <table border="0" cellspacing="0" cellpadding="0" unselectable="on">
	          <colgroup>
	              <col width="80px"/>
	              <col width="130px"/>
	              <col width="80px"/>
	          </colgroup>
	          <tr valign="bottom">
	            <td><strong>Included List:</strong></td>
	            <td>
	              <IE:APDButton id="btnNavFirstInc" name="btnNavFirstInc" value="&lt; &lt;" onButtonClick="navMoveFirst('Inc')"/>
	              <IE:APDButton id="btnNavPrevInc" name="btnNavPrevInc" value="&lt; " onButtonClick="navMovePrev('Inc')"/>
	              <IE:APDButton id="btnNavNextInc" name="btnNavNextInc" value="&gt;" onButtonClick="navMoveNext('Inc')"/>
	              <IE:APDButton id="btnNavLastInc" name="btnNavLastInc" value="&gt; &gt;" onButtonClick="navMoveLast('Inc')"/>
	            </td>
	            <td>
	              <span id="pgShopInc" name="pgShopInc" unselectable="on">
	              </span>
	            </td>
	          </tr>
	        </table>
	        <table border="0" cellspacing="0" cellpadding="2" style="width:100%;" unselectable="on">
	            <colgroup>
	                <col width="21px"/>
	                <col width="160px"/>
	                <col width="106px"/>
	                <col width="*"/>
	            </colgroup>
	            <tr style="height:21px;cursor:hand;">
	                <td unselectable="on" class="TableSortHeader" title="Select/Deselect All current page">
	                        <IE:APDCheckBox id="SelectAllInc" name="SelectAllInc" canDirty="false" onChange="selectAll2()"/>
	                </td>
	                <td unselectable="on" class="TableSortHeader">Name</td>
	                <td unselectable="on" class="TableSortHeader">City, State</td>
	                <td unselectable="on" class="TableSortHeader">Prgm</td>
	            </tr>
	        </table>
	        <DIV unselectable="on" class="autoflowTable" style="width:100%; height:149px;">
	            <table border="0" cellspacing="1" cellpadding="2" style="table-layout:fixed;" name="tblInclude" id="tblInclude" data1src="#xmlShopInclude" DATAPAGESIZE="7">
	                <colgroup>
	                    <col width="25px"/>
	                    <col width="166px"/>
	                    <col width="111px"/>
	                    <col width="*"/>
	                </colgroup>
	                <tr style="height:20px">
	                    <td unselectable="on" class="GridTypeTD" nowrap="" >
	                        <IE:APDCheckBox canDirty="false"/>
	                    </td>
	                    <td unselectable="on" class="GridTypeTD" nowrap="" style="overflow:hidden;text-align:left;"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<span datafld="Name" DATAFORMATAS="html" unselectable="on" style="width:99%;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;margin:0px;"></span></td>
	                    <td unselectable="on" class="GridTypeTD" nowrap="" style="overflow:hidden;text-align:left;">
	                      <span style="width:99%;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;margin:0px;padding:0px;">
	                        <span datafld="AddressCity" DATAFORMATAS="html" unselectable="on"></span>, <span datafld="AddressState" DATAFORMATAS="html" unselectable="on"></span>
	                      </span>
	                    </td>
	                    <td unselectable="on" class="GridTypeTD" nowrap="" style="overflow:hidden;text-align:center;"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<span datafld="CEI_PS" DATAFORMATAS="html" unselectable="on"></span></td> 
	                    <td style="display:none"><div datafld="ShopLocationID"></div></td>
	                </tr>
	            </table>
	        </DIV>
	</IE:APDTabPage>
    
</IE:APDTabGroup>

    <xsl:element name="xml">
      <xsl:attribute name="id">xmlShopExclude</xsl:attribute>
      <xsl:attribute name="name">xmlShopExclude</xsl:attribute>
      <xsl:attribute name="ondatasetcomplete">showShopExclude()</xsl:attribute>
      <xsl:element name="Root">
        <xsl:for-each select="/Root/Shop[@ShopLocationID != '' and @ExcludeFlag='1']">
          <xsl:sort select="@Name"/>
          <xsl:copy-of select="."/>
        </xsl:for-each>
      </xsl:element>
    </xsl:element>

    <xsl:element name="xml">
      <xsl:attribute name="id">xmlShopInclude</xsl:attribute>
      <xsl:attribute name="name">xmlShopInclude</xsl:attribute>
      <xsl:attribute name="ondatasetcomplete">showShopInclude()</xsl:attribute>
      <xsl:element name="Root">
        <xsl:for-each select="/Root/Shop[@ShopLocationID != '' and @IncludeFlag='1']">
          <xsl:sort select="@Name"/>
          <xsl:copy-of select="."/>
        </xsl:for-each>
      </xsl:element>
    </xsl:element>
</xsl:template>

<xsl:template name="SearchList">
    <div style="border: 0px solid gray; width:100%;height:218px;padding:2px;overflow:hidden;" name="divShopSearch" id="divShopSearch">
        <iframe name="frmShopSearch" id="frmShopSearch" style="width:100%;height:100%;background:transparent" onreadyStateChange="searchReady()" allowtransparency="true">
            <xsl:attribute name="src">/admin/RefInsShopSearch.asp?InsuranceCompanyID=<xsl:value-of select="$InsID"/>&amp;UseCEI=<xsl:value-of select="$UseCEI"/></xsl:attribute>
        </iframe>
    </div>
    <input type="hidden" size="8" name="txtLocID" id="txtLocID" class="InputField"/><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
</xsl:template>

</xsl:stylesheet>
