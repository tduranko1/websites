<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:local="http://local.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt">

  <xsl:import href="msxsl/generic-template-library.xsl"/>
  <xsl:import href="msxsl/msjs-client-library.js"/>

  <msxsl:script language="JScript" implements-prefix="local">
    <![CDATA[
      function AdjustCRUD( strShopCRUD, strCurrentDate, strCreatedDate, strCurrentUserID, strCreatedUserID )
      {
        var varCurDate = new Date(strCurrentDate);
        var varCreatedDate = new Date(strCreatedDate);

        if ( ( strCreatedUserID != "" ) && ( ( strCurrentUserID != strCreatedUserID )
            || ( ( varCurDate.getTime() - varCreatedDate.getTime() ) > 1800000 ) ) )
            strShopCRUD = strShopCRUD.substr( 0, 2 ) + "_" + strShopCRUD.substr( 3, 1 );

        return strShopCRUD;
      }
     
     ]]>
  </msxsl:script>


  <!-- Permissions params - names must end in 'CRUD' -->
  <xsl:param name="ShopCRUD" select="Shop"/>

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="UserId"/>
  <xsl:param name="WindowID"/>
  <xsl:param name="ShopLocationID"/>
  
  <!-- APDXSL:C:\websites\apd\v1.2\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspNoteGetDetailXML,NoteDetails.xsl, 147468, 0   -->

  <xsl:template match="/">
        
    <html>
      <head>

        <title>Note Details</title>
        
        <link rel="stylesheet" href="/css/apdselect.css" type="text/css"/>
        <link rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
        
        <STYLE type="text/css">
             A {color:black; text-decoration:none;}          
        </STYLE>
        
        <SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
            
        <xsl:variable name="CurrentUserID"><xsl:value-of select="$UserId"/></xsl:variable>
        <xsl:variable name="CreatedUserID"><xsl:value-of select="/Root/Note/@CreatedUserID"/></xsl:variable>
		    <xsl:variable name="CurrentDate"><xsl:value-of select="user:extractSQLDateTime( string(/Root/Note/@CurrentDate) )"/></xsl:variable>
        <xsl:variable name="CreatedDate"><xsl:value-of select="user:extractSQLDateTime( string(/Root/Note/@CreatedDate) )"/></xsl:variable>
        <xsl:variable name="AdjustedCrud"><xsl:value-of select="local:AdjustCRUD( string($ShopCRUD), string($CurrentDate),string($CreatedDate), string($CurrentUserID), string($CreatedUserID) )"/></xsl:variable>
        <xsl:value-of select="js:SetCRUD( string($AdjustedCrud) )"/>

        <script language="JavaScript" src="/js/apdcontrols.js"></script>
        <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
		    <script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"/WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,10);
		  event.returnValue=false;
		  };	
 		</script>
		
		
    <script language="JavaScript" runat="Client">
      window.name = "thisWin";
      window.onload = InitPage;

			var strCurrentDateSQL = '<xsl:value-of select="/Root/Note/@CurrentDate"/>';
			var strCurrentDate = extractSQLDateTime( strCurrentDateSQL );
			var varCurrentDate = new Date( strCurrentDate );
			
      var strCreatedDateSQL = '<xsl:value-of select="/Root/Note/@CreatedDate"/>';
      var strCreatedDate    = extractSQLDateTime( strCreatedDateSQL );
      var varCreatedDate    = new Date( strCreatedDate );
      var strCrud           = '<xsl:value-of select="$AdjustedCrud"/>';
      
      // Do any client-side page initializations.
      function InitPage()
      {
        try {
          if ( strCrud.substr( 2, 1 ) == "_" )
          {
              document.all.butSaveNote.CCDisabled = "true";
              document.all.butSaveNote.style.cursor = 'default';
          }
          //InitFields();
          //initSelectBoxes();
          if (TimedOut()){
            form1.txtNote.className="readonlyField";
            form1.txtNote.readOnly="true";
          }
          window.returnValue = "false";
          
          if ( "<xsl:value-of select='$CreatedUserID'/>" == "" )
            document.getElementById("txtNote").focus();
              
        } 
        catch ( e ) { ClientError( e.message ); }
      }

      // Returns true if this note is older than 30 minutes.
      function TimedOut()
      {
        try {
          if ( strCreatedDateSQL != '' )
          {
             // var varDate = new Date();
              var varDate = varCurrentDate;
              return ( ( varDate.getTime() - varCreatedDate.getTime() ) > ( 1800000 ) );
          }
        }
        catch ( e ) { ClientError( e.message ); }
        return false;
      }

      // Do any final processing and form validation before submitting the form.
      function SaveNote()
      {
        try {
                        
          form1.Note.value = form1.txtNote.value;
          var intNoteLen = form1.Note.value.length;
          var intNoteMaxLen = "<xsl:value-of select="/Root/Metadata/Column[@Name='Note']/@MaxLength"/>";
          if (intNoteMaxLen == "") intNoteMaxLen = 250;

          if ( TimedOut() ){
              ClientInfo('30 minutes have passed since this note was created.\n You no longer have permission to modify this note.');
              return;
          }
          
          if ( intNoteLen == 0 ){
              ClientWarning('Please enter a note body before saving.');
              return;
          }
          
          if ( intNoteLen &gt; intNoteMaxLen ){
            var sMsg = "Note body cannot be longer than " + intNoteMaxLen + " characters.  It is currently " + intNoteLen + " characters long.";
            ClientWarning(sMsg);
            return;
          }
          
          butSaveNote.CCDisabled = true;
          
          if ( "<xsl:value-of select='$CreatedUserID'/>" == "" ){
            form1.Action.value = 'Insert';
            /*
            var sCurrentContext = strContext == strPertainsTo ? "true" : "false";
            window.returnValue = sCurrentContext + "|1|" + strPertainsTo.substr(0,3);
            */
          }
          else{
            form1.Action.value = 'Update';
            
          }

          document.all.butSaveNote.CCDisabled = true;
          document.all.butSaveNote.style.cursor = 'default';
          form1.UserID.value = '<xsl:value-of select="string($UserId)"/>';
          form1.submit();
        }
        catch ( e ) { ClientError( e.message ); }
      }

      
  </script>

      </head>
      <body bgcolor="#FFFAEB" text="#000000" onUnload="" style="overflow:hidden;">
        <div style="position:absolute; z-index:1; top:10px; left:10px;">
          <xsl:attribute name="CRUD"><xsl:value-of select="AdjustedCrud"/></xsl:attribute>
          <form name="form1" method="post" target="thisWin" action="/ProgramMgr/SMTNoteDetails.asp">
            <xsl:apply-templates select="/Root/Note"/>
          </form>
          <!-- Data Island for hiding 'reference' xml -->
          <xml id="xmlReference">
            <xsl:element name="Root">
              <xsl:copy-of select="Root/Reference"/>
            </xsl:element>
          </xml>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="Note">

    <!-- Hidden values we create here -->
    <input name="Action" id="Action" type="hidden" value="Update"/>
    <input name="UserID" type="hidden"/>

    <!-- Pass through values -->
    <input name="DocumentID" type="hidden">
      <xsl:attribute name="value"><xsl:value-of select="@DocumentID"/></xsl:attribute>
    </input>
    <input name="ShopLocationID" type="hidden">
      <xsl:attribute name="value"><xsl:value-of select="$ShopLocationID"/></xsl:attribute>
    </input>
    <input name="SysLastUpdatedDate" type="hidden">
      <xsl:attribute name="value"><xsl:value-of select="@SysLastUpdatedDate"/></xsl:attribute>
    </input>
    <input name="WindowID" type="hidden">
      <xsl:attribute name="value"><xsl:value-of select="$WindowID"/></xsl:attribute>
    </input>

    <!-- Default values passed through reference data to keep from hardcoding -->
    <input name="DocumentTypeID" type="hidden">
      <xsl:attribute name="value"><xsl:value-of select="//Reference[@List='DocumentType']/@ReferenceID"/></xsl:attribute>
    </input>
    <input name="DocumentSourceID" type="hidden">
      <xsl:attribute name="value"><xsl:value-of select="//Reference[@List='DocumentSource']/@ReferenceID"/></xsl:attribute>
    </input>

    <xsl:variable name="StatusID" select="@StatusID"/>
    
    <table width='100%' border='0' cellspacing='2' cellpadding='0'>
      <xsl:if test="@CreatedUserID!=''">
        <tr>
          <td width="16%">Date Entered</td>
          <td width="34%" colspan="3">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('CreatedDate',25,'CreatedDate',.,'',4)"/>
          </td>
        </tr>
        <tr>
          <td>Created By</td>
          <td>
            <xsl:value-of disable-output-escaping="yes"
                select="js:InputBox('CreatedUserNameLast',23,'',.,'',1)"/>
              <xsl:text disable-output-escaping="yes"> value='</xsl:text>
              <xsl:value-of select="user:FormatPersonName(@CreatedUserNameFirst,@CreatedUserNameLast)"/>
              <xsl:text disable-output-escaping="yes">'></xsl:text>
          </td>
          <td width="16%">User Role</td>
          <td width="34%">
            <xsl:value-of disable-output-escaping="yes" select="js:InputBox('CreatedUserRoleName',23,'CreatedUserRoleName',.,'',5)"/>
          </td>
        </tr>
      </xsl:if>
             
      <tr>
        <td colspan="4">
          <textarea id="txtNote" cols="87" rows="7"><xsl:value-of select="@Note" disable-output-escaping="yes"/></textarea>
          <input type="hidden" id="Note" name="Note"/>
        </td>
      </tr>
      <tr>
        <td colspan="4">
          <IE:APDButton id="butCancelNote" name="butCancelNote" width="80" value="Cancel" onButtonClick="window.close()"></IE:APDButton>
          <IE:APDButton id="butSaveNote" name="butSaveNote" width="80" value="Save" onButtonClick="SaveNote(true)"></IE:APDButton>
        </td>
      </tr>
    </table>
  </xsl:template>

</xsl:stylesheet>

