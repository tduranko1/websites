<?xml version="1.0" encoding="utf-8"?>

<!-- Version:  Initial Version  -->
<!-- Version:  08Sep2021 - TVD - Allow all direct open.  -->

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:session="http://lynx.apd/session-manager"
    xmlns:js="urn:the-xml-files:xslt"
    id="ClaimDocument">

  <xsl:import href="msxsl/generic-template-library.xsl"/>
  <xsl:import href="msxsl/msjs-client-library.js"/>

  <xsl:output method="html" indent="yes" encoding="UTF-8" />

  <msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
    var bPageDis = false;
    function AdjustCRUD( strCrud)
     {
      var strRet = "_" + strCrud.substr(1);

      //if the user has delete permission, remove it so that the delete button will not display.
      //Anyway, the delete permission from the actual crud is taken into consideration when showing
      // the delete item in the context menu.
      strRet = strRet.replace(/[D]/g, "_");

      if (bPageDis) strRet = "____";
       return strRet;
     }
    function setPageReadOnly(){
      bPageDis = true;
      return "";
    }
  ]]>
  </msxsl:script>

  <!-- Permissions params - names must end in 'CRUD' -->
  <xsl:param name="DocumentCRUD" select="Document"/>

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="UserId"/>
  <xsl:param name="WindowID"/>
  <xsl:param name="ImageRootDir"/>
  <xsl:param name="ImageRootArchiveDir"/>
  <xsl:param name="InsuranceCompanyName"/>
  <xsl:param name="ClientClaimNumber" />
  <xsl:param name="InsuredNameFirst"/>
  <xsl:param name="InsuredNameLast"/>
  <xsl:param name="LossDate"/>
  <xsl:param name="ReloadTab"/>

  <xsl:template match="/Root">

    <xsl:variable name="ClaimRestrictedFlag" select="session:XslGetSession('RestrictedFlag')"/>
    <xsl:variable name="ClaimOpen" select="session:XslGetSession('ClaimOpen')"/>
    <xsl:variable name="CSRNameFirst" select="session:XslGetSession('OwnerUserNameFirst')"/>
    <xsl:variable name="CSRNameLast" select="session:XslGetSession('OwnerUserNameLast')"/>
    <xsl:variable name="InsuredBusinessName" select="session:XslGetSession('InsuredBusinessName')"/>
    <xsl:variable name="ClaimStatus" select="session:XslGetSession('ClaimStatus')"/>
    <xsl:variable name="DemoFlag" select="session:XslGetSession('DemoFlag')"/>
    <xsl:variable name="InsuranceCompanyID" select="session:XslGetSession('InsuranceCompanyID')"/>
    <xsl:variable name="CoverageLimitWarningInd" select="session:XslGetSession('CoverageLimitWarningInd')"/>
    <xsl:variable name="Claim_ClaimAspectID" select="session:XslGetSession('Claim_ClaimAspectID')"/>

    <xsl:variable name="LynxID" select="/Root/@LynxID" />

    <xsl:if test="$ClaimStatus='Claim Cancelled' or $ClaimStatus='Claim Voided'">
      <xsl:value-of select="user:setPageReadOnly()"/>
    </xsl:if>
    <xsl:value-of select="js:SetCRUD( string($DocumentCRUD) )"/>

    <HTML>
      <HEAD>
        <TITLE>Claim Documents</TITLE>
        <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/tabs.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/CoolSelect.css" type="text/css"/>

        <STYLE type="text/css">
          A {color:black; text-decoration:none;}
        </STYLE>

        <SCRIPT language="JavaScript" src="/js/tabs.js"></SCRIPT>
        <SCRIPT language="javascript" src="/js/CoolSelect.js"></SCRIPT>
        <SCRIPT language="javascript" src="/js/fade.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/grid.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/showimage.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/datepicker.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT>
        <SCRIPT language="JavaScript" src="/js/formats.js"></SCRIPT>
        <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>

        <SCRIPT language="JavaScript">

          var gsLynxID = "<xsl:value-of select="$LynxID"/>";
          var gsInsuranceCompanyID = '<xsl:value-of select="$InsuranceCompanyID"/>';
          var gsWindowID = "<xsl:value-of select="$WindowID"/>";
          var gsUserID = "<xsl:value-of select="$UserId"/>";
          var gsImageRootDir = "<xsl:value-of select="$ImageRootDir"/>";
          var gsImageRootArchiveDir = "<xsl:value-of select="$ImageRootArchiveDir"/>";
          var gsClaimID = "<xsl:value-of select="$ClientClaimNumber"/>";
          var gsIFname = "<xsl:value-of select="js:cleanString(string($InsuredNameFirst))"/>";
          var gsILname = "<xsl:value-of select="js:cleanString(string($InsuredNameLast))"/>";
          var gsLossDate = "<xsl:value-of select="js:UTCConvertDate(string($LossDate))"/>";
          var gsInsco = "<xsl:value-of select="js:cleanString(string($InsuranceCompanyName))"/>";
          var gsClaimRestrictedFlag = "<xsl:value-of select="$ClaimRestrictedFlag"/>";
          var gsClaimOpen = "<xsl:value-of select="$ClaimOpen"/>";
          var gsCSRNameFirst = '<xsl:value-of select="js:cleanString(string($CSRNameFirst))"/>';
          var gsCSRNameLast = '<xsl:value-of select="js:cleanString(string($CSRNameLast))"/>';
          var gsIBusiness = "<xsl:value-of select="js:cleanString(string($InsuredBusinessName))"/>";
          var gsCRUD = "<xsl:value-of select="$DocumentCRUD"/>";
          var gsDocPath;
          var gsClaimStatus = "<xsl:value-of select="$ClaimStatus"/>";
          var gsDemoFlag = "<xsl:value-of select="$DemoFlag"/>";
          var gsReloadTab = "<xsl:value-of select="$ReloadTab"/>";
          var sCurrentTab = "";
          var sDirectionToPayColor = "#33cccc";
          var sFinalEstimateColor = "#00cc99";
          var sFinalAndDirectionToPayColor = "#66ff66";

          var sAEColor = "#E1C1EE";
          var sFBEColor = "#CC95E1";
          var sAEAndFBEColor = "#B564D4";

          var aEstimateTypes = new Array(<xsl:for-each select="/Root/Reference[@List='DocumentType' and @EstimateTypeFlag='1']">
            "<xsl:value-of select="js:cleanString(string(@Name))"/>"<xsl:if test="position() != last()">, </xsl:if>
          </xsl:for-each>);
          var bDeleteDoc = "<xsl:value-of select="contains($DocumentCRUD, 'D')"/>";
          var gsRec_Sent = "content11";
          var gsCoverageLimitWarningInd = "<xsl:value-of select="$CoverageLimitWarningInd"/>";
          var gsClaim_ClaimAspectID = "<xsl:value-of select="$Claim_ClaimAspectID"/>";
          var sCurNodePertainsTo = "";
          var sUserSupervisorFlag = "<xsl:value-of select="/Root/@UserSupervisorFlag"/>";
          var sEarlyBillFlag = "<xsl:value-of select="/Root/@EarlyBillFlag"/>";

          var sArchived = "<xsl:value-of select="/Root/Document/@Archived"/>";

          var iDirectionToPayCount = 0;
          var iFinalEstimateCount = 0;
          var iFinalAndDirectionToPayCount = 0;

          var iApprovedDocCount = 0;
          var iFBECount = 0;
          var iFBEAndApprovedDocCount = 0;

          // Table index constants
          var idxDocumentMenu                   = 0;
          var idxPertainsTo                     = 3;
          var idxServiceChannel                 = 4;
          var idxDocumentType                   = 5;
          var idxEstimateEdit                   = 6;
          var idxDocumentSource                 = 7;
          var idxOriginalAudited                = 8;
          var idxDuplicate                      = 9;
          var idxDocumentID                     = 13;
          var idxDocumentSourceID               = 14;
          var idxDocumentTypeID                 = 15;
          var idxClaimAspectID                  = 16;
          var idxSysLastUpdatedDate             = 17;
          var idxDirectionalCD                  = 18;
          var idxOrgClaimAspectID               = 19;
          var idxAssignmentID                   = 20;
          var idxImageLocation                  = 21;
          var idxDirectionToPayFlag             = 22;
          var idxFinalEstimateFlag              = 23;
          var idxLynxID                         = 24;
          var idxVANFlag                        = 25;
          var idxSuppSeqNum                     = 26;
          var idxClaimAspectServiceChannelID    = 27;
          var idxOrgClaimAspectServiceChannelID = 28;
          var idxApprovedFlag                   = 29;
          var idxApprovedDate                   = 30;
          var idxWarrantyFlag                   = 31;
          var idxFBEFlag                        = 32;
          var idxWarrantyExistsFlag             = 33;
          var idxBilledFlag                     = 34;
          var idxAgreedPriceMetCD               = 35;
          var idxEstimateNetAmt                 = 36;

          // Document Type ID constants
          var docTypeEstimate = 3;
          var docTypeSupplement = 10;
          var strApprovedEstimateNetAmt = null;
          var strInvoiceDocumentID = null;


          if (parent.gsCoverageLimitWarningInd)
          parent.gsCoverageLimitWarningInd = "<xsl:value-of select="$CoverageLimitWarningInd"/>";

          <![CDATA[

// button images for ADD/DELETE/SAVE
preload('buttonAdd','/images/but_ADD_norm.png')
preload('buttonAddDown','/images/but_ADD_down.png')
preload('buttonAddOver','/images/but_ADD_over.png')
preload('buttonDel','/images/but_DEL_norm.png')
preload('buttonDelDown','/images/but_DEL_down.png')
preload('buttonDelOver','/images/but_DEL_over.png')
preload('buttonSave','/images/but_SAVE_norm.png')
preload('buttonSaveDown','/images/but_SAVE_down.png')
preload('buttonSaveOver','/images/but_SAVE_over.png')

var editablecell = null;
var gsDocumentTypeID;

function CallHelpFn()
{
      if (gsRec_Sent=="content12" )
        RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,28); // Sent 28
      else
        RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,25); // Received 25
}


function rowMouseOver(tblObject)
{
   if (editablecell == null)
      {
         saveBG = tblObject.style.backgroundColor;
      tblObject.style.backgroundColor="#FFCC99";
      tblObject.style.color = "#000066";
         tblObject.style.cursor='default';
      }
}


function rowMouseOut(tblObject)
{
   if (editablecell == null)
   {
      tblObject.style.backgroundColor=saveBG;
      tblObject.style.color = "#000000";
   }
}

var saveBorder = "";
var editbg;

function EditableCellOver(obj)
{
      if (gsCRUD.indexOf("U") == -1 ) return;
    if (editablecell == null)
      {
         editbg = obj.style.backgroundColor;
         obj.style.backgroundColor = "#CCCCCC";
      }
}


function EditableCellOut(obj)
{
      if (gsCRUD.indexOf("U") == -1 ) return;
      if (editablecell == null)
         obj.style.backgroundColor = editbg;
      //obj.style.border = saveBorder;
}


function delChkChange(obj)
{
      if (gsCRUD.indexOf("D") == -1 ) return;
      var objRow = obj.parentElement.parentElement;
      objRow.deleteDoc = obj.checked;
      objRow.dirty = true;
      gbDirtyFlag = true;
}


function EditableCellChange(obj)
{
    obj.parentElement.style.border = "1px solid #800000";
      EditableCellClose(obj)
}


function EditableCellBlur(obj)
{
      if (editablecell)
      EditableCellClose(obj)
}


function EditableCellKeyPress(obj)
{
      if (event.keyCode == 13 || event.keyCode == 27)
         EditableCellClose(obj)
      else
         obj.parentElement.style.border = "1px solid #800000";
}


function EditableCellClose(obj)
{
    try
    {
      var objRow = editablecell.parentElement;
      var oldStrValue;
      if (obj.type == "text")
      {
        editablecell.innerHTML = ""
        editablecell.innerText = obj.value;
      }
      else if (obj.type == "select-one")
      {
        //update the selected id value in the table
        var tblIndex = editablecell.getAttribute( "IDIndex" );

        if (tblIndex)
          objRow.cells[parseInt(tblIndex)].innerText = obj.value;

        // Update the selected assignment id value in the table.
        // Applies only to the 'Service Channel' column.
        if ( editablecell.getAttribute( "AssignmentIDIndex" ) )
            objRow.cells[ idxAssignmentID ].innerText = obj.options[ obj.selectedIndex ].AssignmentID;

        //get text to place back in the cell
        if(tblIndex == idxDocumentTypeID)  // get text to revert back to old value
        {
            if ( gsDocumentTypeID == docTypeEstimate )
                oldStrValue = obj.options[2].innerText;
            else if (gsDocumentTypeID == docTypeSupplement )
                oldStrValue = obj.options[8].innerText;
        }

        // For the service channel combo, pull the assignment code off the value string.
        var strValue = obj.options[obj.selectedIndex].innerText;
        if (editablecell.id == "tdServiceChannel"){
          var idx = strValue.indexOf("--");
          if (idx > -1) {
            editablecell.title = strValue.substr(idx+2);
            strValue = strValue.substring(0, idx);
          }
        }

        // For supplements, add the supplement sequence number to the text.
        if (strValue.indexOf("Supplement") != -1)
          strValue += " " + objRow.cells[ idxSuppSeqNum ].innerText;

        // Force new selection in the service channel column after a pertains to change.
        if (editablecell.id == "tdPertainsTo")
        {
            var strClaimAspectID = objRow.cells[ idxClaimAspectID ].innerHTML;
            var strXPath = '//Reference[@List="ServiceChannel" and @ClaimAspectID="' + strClaimAspectID + '"]';
            var objNodeList = eval("xmlServiceChannel.documentElement.selectNodes('"+ strXPath +"')");

            if ( objNodeList.length > 0 )
            {
                var objNode = objNodeList[0];

                // Determine if this option should be selected.
                var strName = objNode.getAttribute("Name");
                if ( objNode.getAttribute("AssignmentSuffix") != "" )
                    strName += "/Asgn " + objNode.getAttribute("AssignmentSuffix");

                objRow.cells[ idxServiceChannel ].innerText = strName;
                objRow.cells[ idxServiceChannel ].style.border = "1px solid #800000";
                objRow.cells[ idxClaimAspectServiceChannelID ].innerText = objNode.getAttribute( "ReferenceID" );
            }
        }

        //clear out control
        editablecell.innerHTML = "";
        editablecell.innerText = strValue;
      }
      else
      {
        editablecell.innerHTML = "<DIV unselectable='on' class='autoflowDiv' style='width:163px;height:40px;'>"+obj.value+"</DIV>";
      }

      editablecell.style.backgroundColor = editbg;
      objRow.style.backgroundColor = editbg;
      objRow.dirty = true;
      gbDirtyFlag = true;

      // if user changed to Estimate or Supplement document type, popup the Quick Estimate dialog
      if (editablecell.id == "tdDocumentType" &&
        ( objRow.cells[ idxDocumentTypeID ].innerText == docTypeEstimate ||
        objRow.cells[ idxDocumentTypeID ].innerText == docTypeSupplement ) )
      {
        ShowEstWindow(objRow, false, true);
      }

      if ( editablecell.id == "tdDocumentType" &&
        ( gsDocumentTypeID == docTypeEstimate ||
        gsDocumentTypeID == docTypeSupplement ) &&
        objRow.cells[ idxDocumentTypeID ].innerText != docTypeEstimate &&
        objRow.cells[ idxDocumentTypeID ].innerText != docTypeSupplement )
      {
        var strRet = YesNoMessage("Confirm", "When you click SAVE, all the data related to the estimate will be lost. Do you want to continue?");
        if (strRet != "Yes")
        {
          objRow.cells[ idxDocumentTypeID ].innerText = gsDocumentTypeID;
          editablecell.innerHTML = "";
          editablecell.innerText = oldStrValue;
          editablecell.style.backgroundColor = editbg;
          objRow.style.backgroundColor = editbg;
          objRow.dirty = false;
          gbDirtyFlag = false;
        }
      }

      editablecell = null;
      resizeScrollTable(document.getElementById("DOCRScrollTable"));
      resizeScrollTable(document.getElementById("DOCSScrollTable"));
    }
    catch ( e ) { alert( e ); }
}

function ShowEstWindow(oRow, bFullSummary, bDisDocTypeSelect)
{
		if ( oRow.cells[ idxDocumentTypeID ].innerText != docTypeEstimate && oRow.cells[ idxDocumentTypeID ].innerText != docTypeSupplement )
		{
			ClientWarning("Cannot open Quick Estimate Update screen. You may have changed the document type. Please save the changes made to the documents and try again. Thank you.");
			return;
		}

		//update before poping up the windows if something has changed
		if (oRow.dirty == true)
		{
			var sAction = "update";

			var oDiv;
			if (oRow.cells[ idxDirectionalCD ].innerText == "I")
				oDiv = document.getElementById("content11")
			else
				oDiv = document.getElementById("content12")

			var sRequest;
			sRequest = "NewLynxID="+oRow.cells[ idxLynxID ].innerText+ "&";
			sRequest += "DirectionalCD="+oRow.cells[ idxDirectionalCD ].innerText+ "&";
			sRequest += "ClaimAspectServiceChannelID="+oRow.cells[ idxOrgClaimAspectServiceChannelID ].innerText+ "&";
			sRequest += "DocumentTypeID="+oRow.cells[ idxDocumentTypeID ].innerText+ "&";
			sRequest += "SupplementSeqNumber="+oRow.cells[ idxSuppSeqNum ].innerText+ "&";
			sRequest += "DocumentSourceID="+oRow.cells[ idxDocumentSourceID ].innerText+ "&";
      
			if (oRow.cells[ idxAssignmentID ].innerText > 0)
				sRequest += "AssignmentID="+oRow.cells[ idxAssignmentID ].innerText+"&";
        
			sRequest += "NewClaimAspectServiceChannelID="+oRow.cells[ idxClaimAspectServiceChannelID ].innerText+ "&";
			sRequest += "DocumentID="+oRow.cells[ idxDocumentID ].innerText+ "&";
			sRequest += "DirectionToPayFlag="+ oRow.cells[ idxDirectionToPayFlag ].innerText + "&";
			sRequest += "FinalEstimateFlag="+ oRow.cells[ idxFinalEstimateFlag ].innerText + "&";
			sRequest += "ApprovedFlag="+ oRow.cells[ idxApprovedFlag ].innerText + "&";
			sRequest += "FinalBillEstimateFlag="+ oRow.cells[ idxFBEFlag ].innerText + "&";
			sRequest += "WarrantyFlag="+ oRow.cells[ idxWarrantyFlag ].innerText + "&";
			sRequest += "SysLastUpdatedDate="+oRow.cells[ idxSysLastUpdatedDate ].innerText+ "&";
			sRequest += "LynxID=" + gsLynxID + "&";
			sRequest += "UserID=" + gsUserID;

			//alert(sRequest); return;
      
			var sProc = oDiv.getAttribute(sAction);
			var coObj = RSExecute("/rs/RSADSAction.asp", "RadExecute", sProc, sRequest );
			retArray = ValidateRS(coObj);

			if (retArray[1] == 1)
			{
				oRow.dirty = false;
				var cellsLength = oRow.cells.length;
				for (var i=0; i < cellsLength; i++)
				{
					if (oRow.cells[i].style.border != "")
					{
						oRow.cells[i].style.borderBottom = "1px solid #BBBBBB";
						oRow.cells[i].style.borderRight = "1px solid #BBBBBB";
						oRow.cells[i].style.borderLeft = "1px solid #FFF0F5";
						oRow.cells[i].style.borderTop = "1px solid #FFF0F5";
					}
				}

				var objXML = new ActiveXObject("Microsoft.XMLDOM");

				objXML.loadXML(retArray[0]);
				var rootNode = objXML.documentElement.selectSingleNode("/Root");

				var DocNode = rootNode.selectSingleNode("Document/@SysLastUpdatedDate");
        
				if (DocNode)
				{
					oRow.cells[ idxSysLastUpdatedDate ].innerText = DocNode.nodeValue;
				}
        
				if (oRow.reviseInvoice == true)
				{
					ReviseInvoice(oRow.cells[ idxClaimAspectID ].innerText,
						oRow.revisedAmount,
						gsUserID);
					oRow.reviseInvoice = false;
					oRow.revisedAmount = "";
				}
			}
			else
			{
				ClientWarning("Error saving row info before bringing up the Quick Estimate entry/update screen.");
				return;
			}
		}

		var sImageLocation = gsImageRootDir + oRow.cells[ idxImageLocation ].innerText.replace(/\\\\/g, "\\");
		var sDocumentID = oRow.cells[ idxDocumentID ].innerText;
		var sClaimAspectID = oRow.cells[ idxClaimAspectID ].innerText;
		var sServiceChannelID = oRow.cells[ idxClaimAspectServiceChannelID ].innerText;
		var sDisDocTypeSelect = "0";

		if (bDisDocTypeSelect == true)
			sDisDocTypeSelect = "1";
		if (sDocumentID != "")
		{
			if (bFullSummary == true)
			{
        //-- 18Aug2014 - TVD - New Dot Net Version --//
				var sURL = "Estimates.asp?WindowID=" + gsWindowID + "&DocumentID=" + sDocumentID + "&ShowTotals=0";
       
        var winName = "EstimateFull_" + gsLynxID;
				//window.showModalDialog(sURL,"","dialogHeight:540px; dialogWidth:738px; dialogTop:76px; dialogLeft:18px; center:No; help:No; resizable:Yes; status:No; unadorned:Yes");
        var sWinEstimates = window.open(sURL, winName,"Height=600px, Width=728px, Top=90px, Left=40px, resizable=Yes, status=No, menubar=No, titlebar=No");
				parent.gAryOpenedWindows.push(sWinEstimates);
				sWinEstimates.focus();
			}
			else
			{      
        //-- 18Aug2014 - TVD - New Dot Net Version --//
				sImageLocation = sImageLocation.replace(/\\/g, "\\\\");
				var sURL = "DocumentEstDetails.asp?EstimateUpd=1&ClaimAspectID=" + sClaimAspectID +
					"&ClaimAspectServiceChannelID=" + sServiceChannelID + "&DocumentID=" + sDocumentID +
					"&UserID=" + gsUserID + "&ImageLocation=" + sImageLocation + "&LynxId=" + gsLynxID +
					"&InsuranceCompanyID=" + gsInsuranceCompanyID + "&DisDocTypeSelect=" + sDisDocTypeSelect;

				//var sURL = "DocumentEstDetails.aspx?EstimateUpd=1&ClaimAspectID=" + sClaimAspectID +
				//"&ClaimAspectServiceChannelID=" + sServiceChannelID + "&DocumentID=" + sDocumentID +
				//"&UserID=" + gsUserID + "&ImageLocation=" + sImageLocation + "&LynxId=" + gsLynxID +
				//"&InsuranceCompanyID=" + gsInsuranceCompanyID + "&DisDocTypeSelect=" + sDisDocTypeSelect;

        //alert(sURL);
        //"&FileUploadFlag=" + sFileUploadFlag 

					//orig val:  dialogHeight:608px
				var strReturn = window.showModalDialog(sURL,"","dialogHeight:625px; dialogWidth:910px; center:Yes; help:No; resizable:No; status:No; unadorned:Yes");

				var sArray = new Array();
				if (strReturn && strReturn != "")
					sArray = strReturn.split("|");

				if (sArray[0] != "0")
				{
					//  sReturnValue = lsDocType + "|" + lsRepairTotalAmt + "|" + lsNetTotalAmt + "|" +
					//      lsEstimateTypeCD + "|" + lsDuplicate + "|" + lsAgreedPriceMetCD + "|" +
					//      gsDocumentVANSourceFlag + "|" + sSysLastUpdatedDate + "|" + bReinspectionRequested +
					//      "|" + lsDocTypeName + "|" + lsDocTypeID;

					oRow.cells[ idxDocumentTypeID ].innerText = sArray[10];
					oRow.cells[ idxDocumentType ].innerText = sArray[9];

					if (sArray[9].indexOf("Supplement") != -1) {
						//Get the supplement sequence number from the quick estimate window.
						//The return will be like "Supplement 5". So split at the space and get the number
						aSupSeqNo = sArray[9].split(" ");
						if (aSupSeqNo.length == 2)
						oRow.cells[ idxSuppSeqNum ].innerText = aSupSeqNo[1];
					}

					if (sArray[3] == "A")
						oRow.cells[ idxOriginalAudited ].innerText = "Audited";
					else if (sArray[3] == "O")
						oRow.cells[ idxOriginalAudited ].innerText = "Original";
					else
						oRow.cells[ idxOriginalAudited ].innerHTML = "&nbsp;";

					if (sArray[4] == "1")
						oRow.cells[ idxDuplicate ].innerText = "Yes";
					else if (sArray[4] == "0")
						oRow.cells[ idxDuplicate ].innerText = "No";
					else
						oRow.cells[ idxDuplicate ].innerHTML = "&nbsp;";
          
					oRow.cells[ idxAgreedPriceMetCD ].innerHTML = sArray[5];
          
					if (sArray[11] == "1") 
					{
						oRow.cells[ idxApprovedFlag ].innerText = "1";
						oRow.style.backgroundColor = (oRow.cells[ idxFBEFlag ].innerText == "1" ? sAEAndFBEColor : sAEColor);
					} 
					else 
					{
						oRow.cells[ idxApprovedFlag ].innerText = "0";
						oRow.style.backgroundColor = (oRow.cells[ idxFBEFlag ].innerText == "1" ? sFBEColor : ((oRow.rowIndex % 2) == 0 ? "#fff7e5" : "#FFFFFF"));
					}

					var sEditIcon = "<DIV style='WIDTH:24px; CURSOR:hand; TEXT-ALIGN:left' onclick='ShowEstWindow(this.parentElement.parentElement, false)'><IMG title='Manual Summary. Click to open quick summary screen.' style='CURSOR:hand; FILTER1:alpha(opacity=60)' src='/images/ManualEst.gif' border='0' unselectable='on' onmouseover1='makeHighlight(this,0,100)' onmouseout1='makeHighlight(this,1,60)' height1='15' width1='15'></DIV>";

					oRow.cells[ idxEstimateEdit ].innerHTML = sEditIcon;
					oRow.cells[ idxSysLastUpdatedDate ].innerHTML = sArray[7]; //SysLastUpdatedDate
				}
			}

			if (oDiv) {
				var tblObj = document.getElementById("tbl"+oDiv.name);
				var idxLength = tblObj.rows.length;
				gbDirtyFlag = false;
				
				for (var idx=0; idx < idxLength; idx++)
				{
					if (tblObj.rows[idx].dirty && tblObj.rows[idx].dirty == true)
						gbDirtyFlag = true;
				}
			}
		}
}


function ldTrim(strText)
{
    while (strText.substring(0,1) == ' ') // this will get rid of leading spaces
      strText = strText.substring(1, strText.length);

    while (strText.substring(strText.length-1,strText.length) == ' ') // this will get rid of trailing spaces
      strText = strText.substring(0, strText.length-1);

    return strText;
}


function EditableCellDblClick(obj, size, lines, UItype)
{
    if (gsCRUD.indexOf("U") == -1 ) return;
    if (editablecell != null)
    {
      EditableCellClose(editablecell.firstChild);
    }

    if (editablecell == null)
    {
      editablecell = obj;

      var sHTML = "";
      if ((UItype == null || UItype == "NumbersOnly") && (lines == null || lines == 1))
      {
        sHTML = "<input type='Text' id='editableInput' class='InputField' size='"+size+"' maxlength='"+size+"' onkeypress='"
        if (UItype == "NumbersOnly")
          sHTML += "NumbersOnly(event);"
        sHTML +="EditableCellKeyPress(this);' value='"+obj.innerText+"' onblur='EditableCellBlur(this)' >"
      }
      else
      {
        if (UItype == null && (lines != null && lines > 1))
          sHTML = "<textarea id='editableInput'  rows='"+lines+"' cols='"+size+"' onkeypress='EditableCellKeyPress(this)' onblur='EditableCellBlur(this)'>"+obj.innerText+"</textarea>"
        else
          sHTML = GetSelectInfo( UItype, obj );
      }

      obj.innerHTML= sHTML;
      objInput = document.getElementById("editableInput");
      //some controls don't handle select
      try { objInput.select(); } catch(e) {};
      objInput.focus();
      resizeScrollTable(document.getElementById("DOCRScrollTable"));
      resizeScrollTable(document.getElementById("DOCSScrollTable"));
    }
}


function GetSelectInfo(xmlNodeName, objCell)
{
  var strXPath = '';
  if (xmlNodeName == "DocumentSource")
    strXPath = '//Reference[@List="'+xmlNodeName+'" and @VANFlag="0"]';
  else if (xmlNodeName == "DocumentType")
    strXPath = '//Reference[@List="'+xmlNodeName+'" and @EnabledFlag="1"]';
  else if (xmlNodeName == "ServiceChannel")
  {
    var strClaimAspectID = objCell.parentNode.cells[ idxClaimAspectID ].innerHTML;
    strXPath = '//Reference[@List="' + xmlNodeName + '" and @ClaimAspectID="' + strClaimAspectID + '"]';
  }
  else
    strXPath = '//Reference[@List="'+xmlNodeName+'"]';

  var objNodeList = eval("xml"+xmlNodeName+".documentElement.selectNodes('"+ strXPath +"')");

  var cellText = removeSpaces(editablecell.innerText)
  var strRet = "<select id='editableInput' name='"+xmlNodeName+"' onchange='EditableCellChange(this)' onkeypress='EditableCellKeyPress(this)' onblur='EditableCellBlur(this)'>"

  if (cellText.indexOf("Supplement") != -1) //The cellText will be "Supplement " and the sequence number and we will not have a matching item in the select. So strip of the seq no so that the supplement option will be selected.
    cellText = "Supplement";

  if ( objNodeList.length > 0 )
  {
    var nLength = objNodeList.length;
    for ( var n=0; n < nLength; n++ )
    {
      var objNode = objNodeList[n];
      if (objNode.getAttribute("PertainsTo") != "clm")
      {
        strRet += "<option value='" + objNode.getAttribute("ReferenceID") + "' ";

        // Determine if this option should be selected.
        var strName = objNode.getAttribute("Name");
        if ( ( xmlNodeName == "ServiceChannel" ) && ( objNode.getAttribute("AssignmentSuffix") != "" ) )
            strName += "/Asgn " + objNode.getAttribute("AssignmentSuffix");

        if ( cellText == strName )
          strRet += "SELECTED ";

        if (xmlNodeName == "ServiceChannel")
          strRet += " AssignmentID='" + objNode.getAttribute("AssignmentID") + "' ";

        strRet += ">";

        if (objNode.getAttribute("Name") == "Received")
          strRet += "Recvd";
        else
          strRet += objNode.getAttribute("Name");

        if (xmlNodeName == "ServiceChannel")
        {
          if (objNode.getAttribute("AssignmentSuffix") != "")
            strRet += "/Asgn " + objNode.getAttribute("AssignmentSuffix");

          if (objNode.getAttribute("ShopLocationName") != "")
            strRet += "--" + objNode.getAttribute("ShopLocationName");
        }
        strRet += "</option>";
      }
    }
  }
  strRet += "</select>";
  return strRet;
}


var inADS_Pressed = false;    // re-entry flagging for buttons pressed
var objDivWithButtons = null;

// ADS Button pressed
function ADS_Pressed(sAction, sName)
{
  if (sAction == "Update" && !gbDirtyFlag) return; //no change made to the page
  if (sAction == "Delete" && !gbDirtyFlag) return; // no doc. selected on the page
  if (inADS_Pressed == true) return;
  inADS_Pressed = true;
  objDivWithButtons = divWithButtons;
  disableADS(objDivWithButtons, true);
//alert("save clicked");
  var reloadTab;
  if (sName == "content12")
    reloadTab = 2;
  else
    reloadTab = 1;

  gbDirtyFlag = false;
  var oDiv = document.getElementById(sName)
  var sRequest = "";
  if (sAction == "Update")
  {
    var tblObj = document.getElementById("tbl"+oDiv.name);

    // to disable/hide save button if SAV button was pressed.
    eval(sName+"_DEL.style.visibility='hidden'");
    eval(sName+"_SAV.style.visibility='hidden'");

   var idxLength = tblObj.rows.length;
   var  blnReload = false;
   for (var idx=0; idx < idxLength; idx++)
   {
      if (tblObj.rows[idx].dirty && tblObj.rows[idx].dirty == true)
      {  
         if (tblObj.rows[idx].reviseInvoice == true){
            ReviseInvoice(tblObj.rows[idx].cells[ idxClaimAspectID ].innerText,
                           tblObj.rows[idx].revisedAmount,
                           gsUserID);
            tblObj.rows[idx].reviseInvoice = false;
            tblObj.rows[idx].revisedAmount = "";
         }
		 
        sRequest = "NewLynxID="+tblObj.rows[idx].cells[ idxLynxID ].innerText+ "&";
        sRequest += "DirectionalCD="+tblObj.rows[idx].cells[ idxDirectionalCD ].innerText+ "&";
        sRequest += "ClaimAspectServiceChannelID="+tblObj.rows[idx].cells[ idxOrgClaimAspectServiceChannelID ].innerText+ "&";
        sRequest += "DocumentTypeID="+tblObj.rows[idx].cells[ idxDocumentTypeID ].innerText+ "&";
        sRequest += "SupplementSeqNumber="+tblObj.rows[idx].cells[ idxSuppSeqNum ].innerText+ "&";
        sRequest += "DocumentSourceID="+tblObj.rows[idx].cells[ idxDocumentSourceID ].innerText+ "&";
        if (tblObj.rows[idx].cells[ idxAssignmentID ].innerText > 0)
          sRequest += "AssignmentID="+tblObj.rows[idx].cells[ idxAssignmentID ].innerText+"&";
        sRequest += "NewClaimAspectServiceChannelID="+tblObj.rows[idx].cells[ idxClaimAspectServiceChannelID ].innerText+ "&";
        sRequest += "DocumentID="+tblObj.rows[idx].cells[ idxDocumentID ].innerText+ "&";
        sRequest += "DirectionToPayFlag="+ tblObj.rows[idx].cells[ idxDirectionToPayFlag ].innerText + "&";
        sRequest += "FinalEstimateFlag="+ tblObj.rows[idx].cells[ idxFinalEstimateFlag ].innerText + "&";
        sRequest += "ApprovedFlag="+ tblObj.rows[idx].cells[ idxApprovedFlag ].innerText + "&";
        sRequest += "FinalBillEstimateFlag="+ tblObj.rows[idx].cells[ idxFBEFlag ].innerText + "&";
        sRequest += "WarrantyFlag="+ tblObj.rows[idx].cells[ idxWarrantyFlag ].innerText + "&";
        sRequest += "SysLastUpdatedDate="+tblObj.rows[idx].cells[ idxSysLastUpdatedDate ].innerText+ "&";
        sRequest += "LynxID=" + gsLynxID + "&";
        sRequest += "UserID=" + gsUserID;
		// alert(sRequest);
        //alert(sRequest);// return;
         var sProc = oDiv.getAttribute(sAction);
         var coObj = RSExecute("/rs/RSADSAction.asp", "JustExecute", sProc, sRequest );
		 // alert(sProc);
         ValidateRS(coObj);
         //alert(coObj);
         
      } else {
        if (tblObj.rows[idx].deleteDoc && tblObj.rows[idx].deleteDoc == true){
          sRequest = "ClaimAspectServiceChannelID="+tblObj.rows[idx].cells[ idxClaimAspectServiceChannelID ].innerText+ "&";
          sRequest += "DocumentID="+tblObj.rows[idx].cells[ idxDocumentID ].innerText+ "&";
          sRequest += "SysLastUpdatedDate="+tblObj.rows[idx].cells[ idxSysLastUpdatedDate ].innerText+ "&";
            sRequest += "LynxID=" + gsLynxID + "&";
            sRequest += "UserID=" + gsUserID + "&";

            var sProc = oDiv.getAttribute("Delete");
            var coObj = RSExecute("/rs/RSADSAction.asp", "JustExecute", sProc, sRequest );
            ValidateRS(coObj);
        }
      }

   }
    //alert("refresh now");
    //window.location.reload();
    document.parentWindow.frameElement.src = "ClaimDocument.asp?WindowID=" + gsWindowID + "&LynxID=" + gsLynxID + "&ReloadTab=" + reloadTab;
  }
  else if (sAction == "Delete")
  {
    var tblObj = document.getElementById("tbl"+oDiv.name);
    var idxLength = tblObj.rows.length;

    // to disable/hide save button if DEL button was pressed.
    eval(sName+"_SAV.style.visibility='hidden'");
    eval(sName+"_DEL.style.visibility='hidden'");

   for (var idx=0; idx < idxLength; idx++)
   {
      if (tblObj.rows[idx].deleteDoc && tblObj.rows[idx].deleteDoc == true)
      {
        sRequest = "ClaimAspectServiceChannelID="+tblObj.rows[idx].cells[ idxClaimAspectServiceChannelID ].innerText+ "&";
        sRequest += "DocumentID="+tblObj.rows[idx].cells[ idxDocumentID ].innerText+ "&";
        sRequest += "SysLastUpdatedDate="+tblObj.rows[idx].cells[ idxSysLastUpdatedDate ].innerText+ "&";
         sRequest += "LynxID=" + gsLynxID + "&";
         sRequest += "UserID=" + gsUserID + "&";

         var sProc = oDiv.getAttribute(sAction);
         var coObj = RSExecute("/rs/RSADSAction.asp", "JustExecute", sProc, sRequest );
         ValidateRS(coObj);
      }
    }

    document.parentWindow.frameElement.src = "ClaimDocument.asp?WindowID=" + gsWindowID + "&LynxID=" + gsLynxID + "&ReloadTab=" + reloadTab;
  }
  inADS_Pressed = false;
  disableADS(objDivWithButtons, false);
  objDivWithButtons = null;
  window.location.reload();
  //if (blnReload)
}


function ReviseInvoice(strClaimAspectID, strRevisedAmount, strUserID){
   if (strClaimAspectID != "" && strRevisedAmount != "" && strUserID != ""){
      try {
      var strXML = "<Root claimAspectID=\"" + strClaimAspectID + 
                   "\" revisedAmount=\"" + strRevisedAmount + 
                   "\" userID=\"" + strUserID + "\" />"
      //alert(strXML);
      var objRet = XMLServerExecute("reviseinvoice.asp", strXML);
      if (objRet.code == 0){
         var strRet;
         if (objRet.xml){
            var oError = objRet.xml.selectSingleNode("//Error");
            if (oError){
               var strMessage = oError.text;
               var iPosExtMsg = strMessage.indexOf("External Message");
               var strMessage = strMessage.substring(iPosExtMsg + "External Message".length + 8, strMessage.indexOf("<|", iPosExtMsg));
               ClientWarning(strMessage);
               //disableButtons(false);
               return;
            } else {
               //window.close();
               blnReload = true;
            }
         }
      }
      } catch (e) {alert(e.description);return}
   } else {
      alert("ReviseInvoice: Invalid Parameters.");
   }
}


function PageInit()
{
  top.sClaim_ClaimAspectID = gsClaim_ClaimAspectID;
  top.setClaimHeader(gsCSRNameFirst, gsCSRNameLast, gsLynxID, gsClaimID, gsIFname, gsILname, gsLossDate, gsInsco, gsClaimRestrictedFlag, gsClaimOpen, gsClaimStatus, gsIBusiness, gsDemoFlag, gsInsuranceCompanyID);
  top.SetMainTab(4);
  if (gsClaimStatus == "Claim Cancelled" ||
      gsClaimStatus == "Claim Voided"){
      gsCRUD = "____";
  }
   tabInit(false,'#FFFFFF');

  top.VehiclesMenu(false, 1); // no vehicle list
  top.VehiclesMenu(false, 2); // no vehicle add

  if (gsReloadTab == 2) {
    tabsIntabs[0].tabArray[1].depressTab();
    sCurrentTab = "Sent";
  } else
    sCurrentTab = "Received";

  if (gsCRUD.indexOf("C") == -1)
    top.DocumentsMenu(false, 1);

  resizeScrollTable(document.getElementById("DOCRScrollTable"));
  resizeScrollTable(document.getElementById("DOCSScrollTable"));

  tabsIntabs[0].tabs.onchange=onTabChangeBubble;
}

function onTabChangeBubble()
{
  if (this.activeTab.content.id == "content11")
    sCurrentTab = "Received";
  else
    sCurrentTab = "Sent";
}


// IMAGE module popup code
function ShowImage(strPath)
{
var aPath = strPath.split(",");

//alert(strPath);
//alert(aPath[1]);

var sArchived = aPath[1];
strPath = aPath[0];

  if (strPath)
  {
	// -- 18Jan2016 - TVD - Archive Process
	if (sArchived==0)
	{
		var gsDocPath = gsImageRootDir + strPath;
	}
	else
	{
		var gsDocPath = gsImageRootArchiveDir + strPath;
	}
	
    var sHREF = "/EstimateDocView.asp?WindowID=" + gsWindowID + "&docPath=" + gsDocPath;
   	//window.showModalDialog(sHREF, "", "dialogHeight:500px; dialogWidth:700px; dialogTop:180px; dialogLeft:312px; resizable:yes; status:no; help:no; center:no;");
    var winName = "EstimateDocView_" + gsLynxID;
    var sWinEstimateDoc = window.open(sHREF, winName, "Height=490px, Width=800px, Top=150px, Left=90px, resizable=Yes, status=No, menubar=no");
    parent.gAryOpenedWindows.push(sWinEstimateDoc);
    sWinEstimateDoc.focus();
    gsDocPath = "";
  }
}

// IMAGE module direct popup code
function ShowImageDirect(strPath)
{
  if (strPath)
  {
    var gsDocPath = gsImageRootDir + strPath;
    window.open(gsDocPath);
  }
}

  // Kodak Imaging module popup code
  function EditImage(strPath)
  {
    if (strPath)
      gsDocPath = gsImageRootDir + strPath;

    var wsh = new ActiveXObject( "WSCript.shell" );
    if (gsDocPath.indexOf(".doc") == (gsDocPath.length - 4)) {
      wsh.Run( "winword " + gsDocPath );
    } else {
      wsh.Run( "Kodakimg " + gsDocPath );
    }
    wsh.AppActivate( "Imaging" );
    gsDocPath = "";
  }


  // Context menu for immage editing
  function callEstImgContextMenu(x,y,strPath)
  {
    if (gsCRUD.indexOf("U") == -1) return;
    gsDocPath = gsImageRootDir + strPath;

    var contextHtml="";
    contextHtml+='<TABLE unselectable="on" STYLE="border:1pt solid #666666" BGCOLOR="#F9F8F7" WIDTH="130" HEIGHT="44" CELLPADDING="0" CELLSPACING="1">';
    contextHtml+='<ST'+'YLE TYPE="text/css">\n';
    contextHtml+='td {font-family:verdana; font-size:11px;}\n';
    contextHtml+='</ST'+'YLE>\n';
    contextHtml+='<SC'+'RIPT LANGUAGE="JavaScript">\n';
    contextHtml+='\n<'+'!--\n';
    contextHtml+='window.onerror=null;\n';
    contextHtml+='/'+' -'+'->\n';
    contextHtml+='</'+'SCRIPT>\n';
    contextHtml+='<TR unselectable="on"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ID="i0" ONMOUSEOVER="document.all.i0.style.background=\'#B6BDD2\';document.all.i0.style.border=\'1pt solid #0A246A\';document.all.i0.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i0.style.background=\'#F9F8F7\';document.all.i0.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.EditImage();">&nbsp;<IMG SRC="/images/notepad.gif" WIDTH="12" HEIGHT="12" BORDER="0" HSPACE="0" VSPACE="0" ALIGN="absmiddle">&nbsp;&nbsp;Edit Image</TD></TR>';
    contextHtml+='<TR unselectable="on"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ID="i1" ONMOUSEOVER="document.all.i1.style.background=\'#B6BDD2\';document.all.i1.style.border=\'1pt solid #0A246A\';document.all.i1.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i1.style.background=\'#F9F8F7\';document.all.i1.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.ShowImage();">&nbsp;<IMG SRC="/images/EstNewItem.gif" WIDTH="12" HEIGHT="12" BORDER="0" HSPACE="0" VSPACE="0" ALIGN="absmiddle">&nbsp;&nbsp;View Image</TD></TR>';
    contextHtml+='</TABLE>';

    var oPopupBody = oPopup.document.body;
    oPopupBody.innerHTML = contextHtml;
    oPopup.show(x, y, 130, 44, window.event.srcElement);
  }


//Notification from APDSelect.asp that the main tabs have changed
function ParentMainTabChange()
{
   MainTabChange();
}


function resizeScrollTable(oElement)
{
   var head = oElement.firstChild;
   var headTable = head.firstChild;
   var body = oElement.lastChild;
   var bodyTable = body.firstChild;

   body.style.height = Math.max(0, oElement.clientHeight - head.offsetHeight);

   var scrollBarWidth = body.offsetWidth - body.clientWidth;

   // set width of the table in the head
   headTable.style.width = Math.max(0, Math.max(bodyTable.offsetWidth + scrollBarWidth, oElement.clientWidth));

   // go through each cell in the head and resize
   var headCells = headTable.rows[0].cells;
   if (bodyTable.rows.length > 0)
   {
      var bodyCells = bodyTable.rows[0].cells;
    var iLength = bodyCells.length;
      for (var i = 0; i < iLength; i++)
      {
         if (bodyCells[i].style.display != "none")
         {
            if (i != (headCells.length-1))
            {
               var iOff = 0;
               if (i == 0) iOff = 2;
               headCells[i].style.width = bodyCells[i].offsetWidth + iOff;
            }
            else
            {
               headCells[i].style.width = bodyCells[i].offsetWidth + scrollBarWidth;
               bodyCells[i].style.width = bodyCells[i].offsetWidth - (scrollBarWidth-2 ) ;
            }
         }
      }
   }
}


function ShowClaimReAssign()
{
  if (oCurRow) {
    var sDimensions = "dialogHeight:250px; dialogWidth:340px; ";
    var sSettings = "center:Yes; help:No; resizable:No; status:No; unadorned:Yes;";
    var sLynxID = oCurRow.cells[ idxLynxID ].innerText;
    var sPertains = oCurRow.cells[ idxPertainsTo ].innerText;
    var sDocument = oCurRow.cells[ idxDocumentType ].innerText;
    var sUrl = "DocumentReAssign.asp?WindowID=" + gsWindowID + "&CurrentLynxID=" + sLynxID + "&Document=" + sDocument + "&Pertains=" + sPertains;
    var strRet = window.showModalDialog(sUrl, "", sDimensions + sSettings);

    if (strRet != undefined)
    {
      var listArray = strRet.split(",");
      oCurRow.cells[ idxDocumentMenu ].firstChild.innerText = listArray[0];
      oCurRow.cells[ idxLynxID ].innerText = listArray[0];
      oCurRow.cells[ idxDocumentMenu ].style.border = "1px solid #800000";
      oCurRow.cells[ idxPertainsTo ].innerText = listArray[1];
      oCurRow.cells[ idxPertainsTo ].style.border = "1px solid #800000";
      oCurRow.cells[ idxServiceChannel ].innerText = listArray[4];
      oCurRow.cells[ idxServiceChannel ].style.border = "1px solid #800000";
      oCurRow.cells[ idxClaimAspectServiceChannelID ].innerText = listArray[5];
      oCurRow.cells[ idxClaimAspectID ].innerText = listArray[2];
      oCurRow.cells[ idxAssignmentID ].innerText = listArray[3];
      oCurRow.dirty = true;
      gbDirtyFlag = true;
      if (sCurrentTab == "Received")
        document.getElementById("content11").focus(); //to activate the ADS buttons
      else
        document.getElementById("content12").focus(); //to activate the ADS buttons
    }
    selectRow(null);
  }
}


function CalcDirectionToPayAndEstimateCount( sClaimAspectID, sServiceChannelID )
{
    var oDirectionToPayNodes = xmlDocuments.selectNodes("/Root/Document[@ClaimAspectID='" + sClaimAspectID +
      "' and @ClaimAspectServiceChannelID='" + sServiceChannelID +
      "' and @DirectionToPayFlag = '1' and @FinalEstimateFlag = '0']");


    iDirectionToPayCount = oDirectionToPayNodes.length;

    var oFinalEstNodes = xmlDocuments.selectNodes("/Root/Document[@ClaimAspectID='" + sClaimAspectID +
      "' and @ClaimAspectServiceChannelID='" + sServiceChannelID +
      "' and @DirectionToPayFlag = '0' and @FinalEstimateFlag = '1']");

    iFinalEstimateCount = oFinalEstNodes.length;

    var oFinalAndDTPNodes = xmlDocuments.selectNodes("/Root/Document[@ClaimAspectID='" + sClaimAspectID +
      "' and @ClaimAspectServiceChannelID='" + sServiceChannelID +
      "' and @DirectionToPayFlag = '1' and @FinalEstimateFlag = '1']");

    iFinalAndDirectionToPayCount = oFinalAndDTPNodes.length;
}


function CalcAEAndFBECount( sClaimAspectID, sServiceChannelID, sDocumentID )
{
    var oAENodes = xmlDocuments.selectNodes("/Root/Document[@ClaimAspectID='" + sClaimAspectID +
      "' and @ClaimAspectServiceChannelID='" + sServiceChannelID +
      "' and @ApprovedFlag = '1' and @FinalBillEstimateFlag = '0'" + 
      " and @DocumentID != '" + sDocumentID + "' and (@DocumentTypeID = '3' or @DocumentTypeID = '10')]");

    iApprovedDocCount = oAENodes.length;
    if (iApprovedDocCount > 0) strApprovedEstimateNetAmt = oAENodes[0].getAttribute("EstimateNetAmt");


    var oFBENodes = xmlDocuments.selectNodes("/Root/Document[@ClaimAspectID='" + sClaimAspectID +
      "' and @ClaimAspectServiceChannelID='" + sServiceChannelID +
      "' and @ApprovedFlag = '0' and @FinalBillEstimateFlag = '1'" + 
      " and @DocumentID != '" + sDocumentID + "' and (@DocumentTypeID = '3' or @DocumentTypeID = '10')]");

    iFBECount = oFBENodes.length;

    var oFBEAndAENodes = xmlDocuments.selectNodes("/Root/Document[@ClaimAspectID='" + sClaimAspectID +
      "' and @ClaimAspectServiceChannelID='" + sServiceChannelID +
      "' and @ApprovedFlag = '1' and @FinalBillEstimateFlag = '1'" + 
      " and @DocumentID != '" + sDocumentID + "' and (@DocumentTypeID = '3' or @DocumentTypeID = '10')]");

    iFBEAndApprovedDocCount = oFBEAndAENodes.length;
    
    strInvoiceDocumentID = null;
    var oInvoiceNodes = xmlDocuments.selectNodes("/Root/Document[@ClaimAspectID='" + sClaimAspectID +
      "' and @ClaimAspectServiceChannelID='" + sServiceChannelID +
      "' and @DocumentTypeID = '13']");
    
    if (oInvoiceNodes.length > 0) strInvoiceDocumentID = oInvoiceNodes[0].getAttribute("DocumentID");
}

var oCurRow = null;
var sCurRowColor = "";


function showDocumentMenu(obj){

	if (gsCRUD.indexOf("U") == -1) return;
    var contextHtml="";
    var oTR = obj.parentElement.parentElement;

    if (oTR.tagName != "TR") return;

    //oCurRow = oTR;

    var iDirectionToPay = oTR.cells[ idxDirectionToPayFlag ].innerText;
    var iFinalEstimate = oTR.cells[ idxFinalEstimateFlag ].innerText;
    var sVANFlag = oTR.cells[ idxVANFlag ].innerText;
    var bEstimateType = false;
    var sCurDocumentType = oTR.cells[ idxDocumentType ].innerText;
    var sClaimAspectID = oTR.cells[ idxClaimAspectID ].innerText;
    var sServiceChannelID = oTR.cells[ idxClaimAspectServiceChannelID ].innerText;
    var sApprovedFlag = oTR.cells[ idxApprovedFlag ].innerText;
    var sApprovedDate = oTR.cells[ idxApprovedDate ].innerText;
    var sWarrantyFlag = oTR.cells[ idxWarrantyFlag ].innerText;
    var sFBEFlag = oTR.cells[ idxFBEFlag ].innerText;
    var sWarrantyExistsFlag = oTR.cells[ idxWarrantyExistsFlag ].innerText;
    var sAgreedPriceMetCD = oTR.cells[ idxAgreedPriceMetCD ].innerText;
    var sBilledFlag = oTR.cells[ idxBilledFlag ].innerText;
	var sSvcChannel = oTR.cells[ idxServiceChannel ].innerText;

    CalcDirectionToPayAndEstimateCount( sClaimAspectID, sServiceChannelID );
    
    CalcAEAndFBECount( sClaimAspectID, sServiceChannelID );

    sCurNodePertainsTo = xmlPertainsTo.selectSingleNode("/Root/Reference[@List = 'PertainsTo' and @ReferenceID = '" + sClaimAspectID + "']/@Name").text;

    for (var i = 0; i < aEstimateTypes.length; i++) {
		if (sCurDocumentType.indexOf(aEstimateTypes[i]) != -1) {
			bEstimateType = true;
		}
    }

	//------------------------------------------//
	// Final Bill Section
	//------------------------------------------//
    var sDirectionToPayMenuText = "Mark as Direction To Pay";
    var sFinalEstimateMenuText = "Mark as Final Estimate";
    var sBothMenuText = "Mark as Final Estimate &amp; Direction To Pay";
	

    if (iDirectionToPay == "1" && iFinalEstimate == "1"){
		sBothMenuText = "Unmark as Final Estimate &amp; Direction To Pay";
    } else if (iDirectionToPay == "1") {
		sDirectionToPayMenuText = "Unmark as Direction To Pay";
    } else if (iFinalEstimate == "1") {
		sFinalEstimateMenuText = "Unmark as Final Estimate";
    }
    
	//------------------------------------------//
	// Initial Bill Estimate Section
	//------------------------------------------//
    var sApprovedDocMenuText = "Mark as Initial Bill Estimate";			//was  "Mark as Approved Estimate"
    var sFinalBillEstimateMenuText = "Mark as Final Bill Estimate";
    var sAFBEBothMenuText = "Mark as Initial Bill &amp; Final Bill Estimate";
   
    if (sFBEFlag == "1" && sApprovedFlag == "1"){
		sAFBEBothMenuText = "Unmark as Initial Bill &amp; Final Bill Estimate";
    } 
    
    if (sApprovedFlag == "1") {
		sApprovedDocMenuText = "Unmark as Initial Bill Estimate";
    } 
    
    if (sFBEFlag == "1") {
		sFinalBillEstimateMenuText = "Unmark as Final Bill Estimate";
    }
    
	var unApproveExpired = false;
	if (sApprovedDate != ""){
		var dtExpired = new Date(formatSQLDateTime(sApprovedDate));
		dtExpired.setMinutes(dtExpired.getMinutes() + 30);
		dtNow = new Date();
		
		if (dtNow >= dtExpired){
			unApproveExpired = true;
		}
		if (sUserSupervisorFlag == "1"){
			unApproveExpired = false;
		}
	}

    contextHtml+='<TABLE unselectable="on" STYLE="border:1pt solid #666666" BGCOLOR="#F9F8F7" WIDTH="100%" HEIGHT="100%" CELLPADDING="0" CELLSPACING="1">';
    contextHtml+='<ST'+'YLE TYPE="text/css">\n';
    contextHtml+='td {font-family:verdana; font-size:11px;}\n';
    contextHtml+='</ST'+'YLE>\n';
    contextHtml+='<SC'+'RIPT LANGUAGE="JavaScript">\n';
    contextHtml+='\n<'+'!--\n';
    contextHtml+='window.onerror=null;\n';
    contextHtml+='/'+' -'+'->\n';
    contextHtml+='</'+'SCRIPT>\n';
    
    if (sCurrentTab == "Received") {

	//------------------------------------------//
	// Active or InActive Section
	//------------------------------------------//
		if (sSvcChannel == "Program Shop" || sSvcChannel == "Total Loss" || sSvcChannel.indexOf("Repair Referral") != -1) {		//differ menu options for program shop or total loss or Repair Referral
			//------------------------------------------//
			// RRP Section
			//------------------------------------------//
			if (sSvcChannel.indexOf("Repair Referral") == 0) {
			contextHtml+='<TR unselectable="on"><TD unselectable="on" ID="i1" STYLE="border:1pt solid #F9F8F7;color:#c0c0c0;cursor:default"';
			contextHtml+='>&nbsp;' + sDirectionToPayMenuText + '</TD></TR>';
			}
			else
			{
			contextHtml+='<TR unselectable="on"><TD unselectable="on" ID="i1" STYLE="border:1pt solid #F9F8F7" ONMOUSEOVER="document.all.i1.style.background=\'#B6BDD2\';document.all.i1.style.border=\'1pt solid #0A246A\';document.all.i1.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i1.style.background=\'#F9F8F7\';document.all.i1.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.SetAsDirectionToPay();"'
			contextHtml+='>&nbsp;' + sDirectionToPayMenuText + '</TD></TR>';
			}

			//-----------------------------------------------------------//
			// Mark as Final Estimate Active/InActive
			//-----------------------------------------------------------//
			contextHtml+='<TR unselectable="on"><TD unselectable="on" ID="i2" '

			if (bEstimateType && sSvcChannel.indexOf("Repair Referral") == -1)

			if ((sSvcChannel.indexOf("Program Shop") == 0) && sEarlyBillFlag == 1) {				contextHtml+='STYLE="border:1pt solid #F9F8F7;color:#c0c0c0;cursor:default" ';
			}
			else {
				contextHtml+='STYLE="border:1pt solid #F9F8F7" ONMOUSEOVER="document.all.i2.style.background=\'#B6BDD2\';document.all.i2.style.border=\'1pt solid #0A246A\';document.all.i2.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i2.style.background=\'#F9F8F7\';document.all.i2.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.SetAsFinalEstimate();"';
			}

			else
				contextHtml+='STYLE="border:1pt solid #F9F8F7;color:#c0c0c0;cursor:default" ';
			
			
			contextHtml+= '>&nbsp;' + sFinalEstimateMenuText + '</TD></TR>';
				
			//-----------------------------------------------------------//
			// Mark as Final Estimate & Direction To Pay Active/InActive
			//-----------------------------------------------------------//
			contextHtml+='<TR unselectable="on"><TD unselectable="on" ID="i3" '
			
			if (bEstimateType && sSvcChannel.indexOf("Repair Referral") == -1)

			if (sSvcChannel.indexOf("Program Shop") == 0 && sEarlyBillFlag == 1) {
				contextHtml+='STYLE="border:1pt solid #F9F8F7;color:#c0c0c0;cursor:default" ';
			}
			else {
				contextHtml+='STYLE="border:1pt solid #F9F8F7" ONMOUSEOVER="document.all.i3.style.background=\'#B6BDD2\';document.all.i3.style.border=\'1pt solid #0A246A\';document.all.i3.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i3.style.background=\'#F9F8F7\';document.all.i3.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.SetAsBoth();"';
			}
			else
				contextHtml+='STYLE="border:1pt solid #F9F8F7;color:#c0c0c0;cursor:default" ';
	
			contextHtml+='>&nbsp;' + sBothMenuText + '</TD></TR>';
			contextHtml+='<TR unselectable="on" style="height:5px"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ><hr noshade style="height:1px"/></TD></TR>';
	
			//-----------------------------------------------//
			// Mark as Initial Bill Estimate Active/InActive
			//-----------------------------------------------//
			contextHtml+='<TR unselectable="on"><TD unselectable="on" ID="i4" '
			if (bEstimateType == true && sBilledFlag != "1" && sWarrantyFlag != "1" && unApproveExpired == false && sSvcChannel != "Desk Audit"  )
			
			if (sSvcChannel.indexOf("Program Shop") == 0 && sEarlyBillFlag == 0) {
				contextHtml+='STYLE="border:1pt solid #F9F8F7;color:#c0c0c0;cursor:default" ';
			}
			else {
				contextHtml+='STYLE="border:1pt solid #F9F8F7" ONMOUSEOVER="document.all.i4.style.background=\'#B6BDD2\';document.all.i4.style.border=\'1pt solid #0A246A\';document.all.i4.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i4.style.background=\'#F9F8F7\';document.all.i4.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.SetAsApprovedDocument();"'
			}
			else
				contextHtml+='STYLE="border:1pt solid #F9F8F7;color:#c0c0c0;cursor:default" ';
				contextHtml+='>&nbsp;' + sApprovedDocMenuText + '</TD></TR>';
			
			//-----------------------------------------------//
			// Mark as Final Bill Estimate Active/InActive
			//-----------------------------------------------//
			contextHtml+='<TR unselectable="on"><TD unselectable="on" ID="i5" '
			if (bEstimateType && sWarrantyFlag != "1" && sSvcChannel != "Desk Audit")
				if (sSvcChannel.indexOf("Program Shop") == 0 && sEarlyBillFlag == 0) {
					contextHtml+='STYLE="border:1pt solid #F9F8F7;color:#c0c0c0;cursor:default" ';
				}
				else {
					contextHtml+='STYLE="border:1pt solid #F9F8F7" ONMOUSEOVER="document.all.i5.style.background=\'#B6BDD2\';document.all.i5.style.border=\'1pt solid #0A246A\';document.all.i5.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i5.style.background=\'#F9F8F7\';document.all.i5.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.SetAsFinalBillEstimate();"';
				}	
			else
				contextHtml+='STYLE="border:1pt solid #F9F8F7;color:#c0c0c0;cursor:default" ';
				contextHtml+= '>&nbsp;' + sFinalBillEstimateMenuText + '</TD></TR>';
	   
			//------------------------------------------------------------//
			// Mark as Initial Bill & Final Bill Estimate Active/InActive
			//------------------------------------------------------------//
			contextHtml+='<TR unselectable="on"><TD unselectable="on" ID="i6" '

			if (sSvcChannel.indexOf("Program Shop") == 0 && sEarlyBillFlag == 1) {
				contextHtml+='STYLE="border:1pt solid #F9F8F7" ONMOUSEOVER="document.all.i6.style.background=\'#B6BDD2\';document.all.i6.style.border=\'1pt solid #0A246A\';document.all.i6.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i6.style.background=\'#F9F8F7\';document.all.i6.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.SetAsBothApprovedFinalBillEstimate();"';
			}
			else {
				if (bEstimateType == true &&  sBilledFlag != "1" && sWarrantyFlag != "1" && sSvcChannel != "Desk Audit" && (sApprovedFlag == "1" && sFBEFlag == "0" && unApproveExpired == true) )		
					contextHtml+='STYLE="border:1pt solid #F9F8F7" ONMOUSEOVER="document.all.i6.style.background=\'#B6BDD2\';document.all.i6.style.border=\'1pt solid #0A246A\';document.all.i6.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i6.style.background=\'#F9F8F7\';document.all.i6.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.SetAsBothApprovedFinalBillEstimate();"';
				else
					contextHtml+='STYLE="border:1pt solid #F9F8F7;color:#c0c0c0;cursor:default" ';
			}
			contextHtml+='>&nbsp;' + sAFBEBothMenuText + '</TD></TR>';
			contextHtml+='<TR unselectable="on" style="height:5px"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ><hr noshade style="height:1px"/></TD></TR>';
		}
	}

    contextHtml+='<TR unselectable="on"><TD unselectable="on" ID="i7" '

	if (sVANFlag == '1')
		contextHtml+='STYLE="border:1pt solid #F9F8F7;color:#c0c0c0;cursor:default" ';
    else
		contextHtml+='STYLE="border:1pt solid #F9F8F7" ONMOUSEOVER="document.all.i7.style.background=\'#B6BDD2\';document.all.i7.style.border=\'1pt solid #0A246A\';document.all.i7.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i7.style.background=\'#F9F8F7\';document.all.i7.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.ShowClaimReAssign();"';

    contextHtml+='>&nbsp;Reassign Document</TD></TR>';

    contextHtml+='<TR unselectable="on"><TD unselectable="on" ID="i8" ';
    
    if (sVANFlag == '1' || bDeleteDoc == "false")
		contextHtml+='STYLE="border:1pt solid #F9F8F7;color:#c0c0c0;cursor:default" ';
    else
		contextHtml+='STYLE="border:1pt solid #F9F8F7" ONMOUSEOVER="document.all.i8.style.background=\'#B6BDD2\';document.all.i8.style.border=\'1pt solid #0A246A\';document.all.i8.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i8.style.background=\'#F9F8F7\';document.all.i8.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.DeleteDocument();"';

    contextHtml+='>&nbsp;Delete Document</TD></TR>';


	if (sSvcChannel == "Program Shop" || sSvcChannel == "Total Loss" || sSvcChannel.indexOf("Repair Referral") == 0){
		//sWarrantyExistsFlag = true;
		contextHtml+='<TR unselectable="on"><TD unselectable="on" ID="i10" ';
		
		if (sWarrantyExistsFlag == false || sApprovedFlag == "1" || sFBEFlag == "1")
			contextHtml+='STYLE="border:1pt solid #F9F8F7;color:#c0c0c0;cursor:default" ';
		else
			contextHtml+='STYLE="border:1pt solid #F9F8F7" ONMOUSEOVER="document.all.i10.style.background=\'#B6BDD2\';document.all.i10.style.border=\'1pt solid #0A246A\';document.all.i10.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i10.style.background=\'#F9F8F7\';document.all.i10.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.SetAsWarrantyDocument();"';

		if (sWarrantyFlag == "1")
			contextHtml+='>&nbsp;Unmark as Warranty Document</TD></TR>';
		else
			contextHtml+='>&nbsp;Mark as Warranty Document</TD></TR>';
	}

    contextHtml+='<TR unselectable="on" style="height:5px"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ><hr noshade style="height:1px"/></TD></TR>';
    contextHtml+='<TR unselectable="on"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ID="i9" ONMOUSEOVER="document.all.i9.style.background=\'#B6BDD2\';document.all.i9.style.border=\'1pt solid #0A246A\';document.all.i9.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i9.style.background=\'#F9F8F7\';document.all.i9.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.selectRow(null);window.parent.oPopup.hide();">&nbsp;Cancel Menu</TD></TR>';
    contextHtml+='</TABLE>';

    var x = event.offsetX+4;
    var y = event.offsetY-1;
    var oPopupBody = oPopup.document.body;
	
	//window.clipboardData.setData("Text", contextHtml);
	
    oPopupBody.innerHTML = contextHtml;
    var sHeight = 220; 										//(sEarlyBillFlag == "1" ? 220 : 150);
    
    
	if (sSvcChannel == "Program Shop" || sSvcChannel == "Total Loss"|| sSvcChannel.indexOf("Repair Referral") == 0 ) 
		var sHeight = 220;
	else
		var sHeight = 85;
		
	
	if (sCurrentTab == "Received")
		oPopup.show(x, y, 275, sHeight, window.event.srcElement);
	else
		oPopup.show(x, y, 275, 80, window.event.srcElement);
	
}


function selectRow(obj){
  gsDocumentTypeID = "";
  if (oCurRow) {
    if (oCurRow.cells[ idxDirectionToPayFlag ].innerText == "1" &&
        oCurRow.cells[ idxFinalEstimateFlag ].innerText == "1")
      oCurRow.style.backgroundColor = sFinalAndDirectionToPayColor;
    else if (oCurRow.cells[ idxDirectionToPayFlag ].innerText == "1")
      oCurRow.style.backgroundColor = sDirectionToPayColor;
    else if (oCurRow.cells[ idxFinalEstimateFlag ].innerText == "1")
      oCurRow.style.backgroundColor = sFinalEstimateColor;
    else
      oCurRow.style.backgroundColor = sCurRowColor;
    sCurRowColor = "";
  }
  oCurRow = obj;
  if (oCurRow) {
    gsDocumentTypeID = oCurRow.cells[ idxDocumentTypeID ].innerText;
    sCurRowColor = oCurRow.style.backgroundColor;
    oCurRow.style.backgroundColor = "#FFEEBB";
  }
}


function SetAsDirectionToPay(){
  var iValue = 0;
  var iFinalEstimate = oCurRow.cells[ idxFinalEstimateFlag ].innerText;
  if (oCurRow.cells[ idxDirectionToPayFlag ].innerText == "0" ||
    (oCurRow.cells[ idxDirectionToPayFlag ].innerText == "1" &&
    oCurRow.cells[ idxFinalEstimateFlag ].innerText == "1"))
  {
    var sClaimAspectID = oCurRow.cells[ idxClaimAspectID ].innerText;
    var sServiceChannelID = oCurRow.cells[ idxClaimAspectServiceChannelID ].innerText;

    CalcDirectionToPayAndEstimateCount( sClaimAspectID, sServiceChannelID );

    //mark row as direction to pay
    if (parseInt(iDirectionToPayCount, 10) > 0 || parseInt(iFinalAndDirectionToPayCount, 10) > 0) {
      if (parseInt(iFinalAndDirectionToPayCount, 10) > 0){
        var strRet = YesNoMessage("Confirm", "A Final Estimate & Direction To Pay document already exist for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Direction to Pay?.");
        if (strRet == "Yes") iFinalAndDirectionToPayCount = 0;
      }
      if (parseInt(iDirectionToPayCount, 10) > 0) {
        var strRet = YesNoMessage("Confirm", "A Direction To Pay document already exists for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Direction to Pay?");
      }
      if (strRet != "Yes") {
        selectRow(null);
        return;
      }

      //Get the previous DirectionToPay document
      var oRows = tblReceived.rows;
      for (var i = 0; i < oRows.length; i++)
      {
        if (oRows[i].cells[ idxDirectionToPayFlag ].innerText == "1" &&
            oRows[i].cells[ idxClaimAspectID ].innerText == sClaimAspectID &&
            oRows[i].cells[ idxClaimAspectServiceChannelID ].innerText == sServiceChannelID
        ){
          oRows[i].cells[ idxDirectionToPayFlag ].innerText = "0";
          oRows[i].cells[ idxFinalEstimateFlag ].innerText = "0";
          oRows[i].title = "";
          oRows[i].dirty = true;
          if ((i % 2) == 0)
            oRows[i].style.backgroundColor = "#fff7e5";
          else
            oRows[i].style.backgroundColor = "#FFFFFF";
        }
      }
    }
    iValue = 1;
    sCurRowColor = oCurRow.style.backgroundColor = sDirectionToPayColor;
    oCurRow.title = "Document marked as the Direction To Pay";
    iDirectionToPayCount = iValue;
    iFinalAndDirectionToPayCount = 0;
    // row changing from Final Estimate to Direction To Pay
    if (iFinalEstimate == "1") iFinalEstimateCount = 0;
    oCurRow.cells[ idxDirectionToPayFlag ].innerText = iValue;
    oCurRow.cells[ idxFinalEstimateFlag ].innerText = 0;
    var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
    var oXMLNode = xmlDocuments.selectSingleNode("/Root/Document[@DocumentID = '" + sDocumentID + "']");
    if (oXMLNode){
      oXMLNode.setAttribute("DirectionToPayFlag", 1);
    }
  } else {
    //unmark row as direction to pay
    var strRet = YesNoMessage("Confirm", "Do you want to unmark this document from Direction To Pay?");
    if (strRet != "Yes") {
      selectRow(null);
      return;
    }
    iValue = 0;
    sCurRowColor = ((oCurRow.rowIndex % 2) == 0 ? "#fff7e5" : "#FFFFFF")
    oCurRow.style.backgroundColor = sCurRowColor;
    oCurRow.title = "";
    iDirectionToPayCount = iValue;
    iFinalAndDirectionToPayCount = 0;
    oCurRow.cells[ idxDirectionToPayFlag ].innerText = iValue;
    var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
    var oXMLNode = xmlDocuments.selectSingleNode("/Root/Document[@DocumentID = '" + sDocumentID + "']");
    if (oXMLNode){
      oXMLNode.setAttribute("DirectionToPayFlag", 0);
    }
  }
  if (iValue == 1) // current row is Direction to Pay. If Final Estimate was selected, then the row is both
    oCurRow.style.backgroundColor = (oCurRow.cells[ idxFinalEstimateFlag ].innerText == "1" ?
      sFinalAndDirectionToPayColor : sDirectionToPayColor);
   oCurRow.dirty = true;
   gbDirtyFlag = true;
  oPopup.hide();
  document.getElementById("content11").focus(); //to activate the ADS buttons
}

function SetAsFinalEstimate() {
  if (sEarlyBillFlag == "1") {
    SetAsFinalBillEstimate(); return;
  }
  var iValue = 0;
  var iDirToPay = oCurRow.cells[ idxDirectionToPayFlag ].innerText;
  if ( oCurRow.cells[ idxFinalEstimateFlag ].innerText == "0" ||
    (oCurRow.cells[ idxDirectionToPayFlag ].innerText == "1" &&
    oCurRow.cells[ idxFinalEstimateFlag ].innerText == "1"))
  {
    var sClaimAspectID = oCurRow.cells[ idxClaimAspectID ].innerText;
    var sServiceChannelID = oCurRow.cells[ idxClaimAspectServiceChannelID ].innerText;

    CalcDirectionToPayAndEstimateCount( sClaimAspectID, sServiceChannelID );

    if (parseInt(iFinalEstimateCount, 10) > 0 || parseInt(iFinalAndDirectionToPayCount, 10) > 0) {
      if (parseInt(iFinalAndDirectionToPayCount, 10) > 0){
        var strRet = YesNoMessage("Confirm", "A Final Estimate & Direction To Pay document already exists for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Final Estimate?.");
        if (strRet == "Yes") iFinalAndDirectionToPayCount = 0;
      }
      if (parseInt(iFinalEstimateCount, 10) > 0) {
        var strRet = YesNoMessage("Confirm", "A Final Estimate document already exists for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Final Estimate?");
      }

      if (strRet != "Yes") {
        selectRow(null);
        return;
      }

      //Get the previous DirectionToPay document
      var oRows = tblReceived.rows;
      for (var i = 0; i < oRows.length; i++)
      {
        if (oRows[i].cells[ idxFinalEstimateFlag ].innerText == "1" &&
            oRows[i].cells[ idxClaimAspectID ].innerText == sClaimAspectID &&
            oRows[i].cells[ idxClaimAspectServiceChannelID ].innerText == sServiceChannelID
        ){
          oRows[i].cells[ idxDirectionToPayFlag ].innerText = "0";
          oRows[i].cells[ idxFinalEstimateFlag ].innerText = "0";
          oRows[i].title = "";
          oRows[i].dirty = true;
          if ((i % 2) == 0)
            oRows[i].style.backgroundColor = "#fff7e5";
          else
            oRows[i].style.backgroundColor = "#FFFFFF";
        }
      }
    }
    iValue = 1;
    sCurRowColor = oCurRow.style.backgroundColor = sFinalEstimateColor;
    oCurRow.title = "Document marked as the Final Estimate";
    iFinalEstimateCount = iValue;
    iFinalAndDirectionToPayCount = 0;
    // row changing from Direction To Pay to Final Estimate
    if (iDirToPay == 1) iDirectionToPayCount = 0;
    oCurRow.cells[ idxFinalEstimateFlag ].innerText = iValue;
    oCurRow.cells[ idxDirectionToPayFlag ].innerText = 0;
    var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
    var oXMLNode = xmlDocuments.selectSingleNode("/Root/Document[@DocumentID = '" + sDocumentID + "']");
    if (oXMLNode){
      oXMLNode.setAttribute("FinalEstimateFlag", 1);
    }
  } else {
    //unmark row as final estimate
    var strRet = YesNoMessage("Confirm", "Do you want to unmark this document from Direction To Pay?");
    if (strRet != "Yes") {
      selectRow(null);
      return;
    }
    iValue = 0;
    sCurRowColor = ((oCurRow.rowIndex % 2) == 0 ? "#fff7e5" : "#FFFFFF")
    oCurRow.style.backgroundColor = sCurRowColor;
    oCurRow.title = "";
    iFinalEstimateCount = iValue;
    iFinalAndDirectionToPayCount = 0;
    oCurRow.cells[ idxFinalEstimateFlag ].innerText = iValue;
    var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
    var oXMLNode = xmlDocuments.selectSingleNode("/Root/Document[@DocumentID = '" + sDocumentID + "']");
    if (oXMLNode){
      oXMLNode.setAttribute("FinalEstimateFlag", 0);
    }
  }
    if (iValue == 1) // current row is Final Estimate. If Direction to Pay was selected, then the row is both
      oCurRow.style.backgroundColor = (oCurRow.cells[ idxDirectionToPayFlag ].innerText == "1" ? sFinalAndDirectionToPayColor : sFinalEstimateColor);
   oCurRow.dirty = true;
   gbDirtyFlag = true;
    oPopup.hide();
    document.getElementById("content11").focus(); //to activate the ADS buttons
}

function SetAsBoth(){
  var iValue = 0;
  if (oCurRow.cells[ idxDirectionToPayFlag ].innerText != "1" ||
      oCurRow.cells[ idxFinalEstimateFlag ].innerText != "1")
  {
    var sClaimAspectID = oCurRow.cells[ idxClaimAspectID ].innerText;
    var sServiceChannelID = oCurRow.cells[ idxClaimAspectServiceChannelID ].innerText;

    CalcDirectionToPayAndEstimateCount( sClaimAspectID, sServiceChannelID );

    var strRet = "Yes";
    if (parseInt(iDirectionToPayCount, 10) > 0 && parseInt(iFinalEstimateCount, 10) > 0) {
      strRet = YesNoMessage("Confirm", "A Final Estimate and Direction To Pay document already exists for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Final Estimate and Direction To Pay?", 140);
      if (strRet == "Yes") {
        iDirectionToPayCount = 0;
        iFinalEstimateCount = 0;
      }
    } else  if (parseInt(iDirectionToPayCount, 10) > 0) {
      strRet = YesNoMessage("Confirm", "A Direction To Pay document already exists for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Final Estimate and Direction To Pay?", 140);
      if (strRet == "Yes") {
        iDirectionToPayCount = 0;
      }
    } else if (parseInt(iFinalEstimateCount, 10) > 0) {
      strRet = YesNoMessage("Confirm", "A Final Estimate document already exists for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Final Estimate and Direction To Pay?", 140);
      if (strRet == "Yes") {
        iFinalEstimateCount = 0;
      }
    } else if (parseInt(iFinalAndDirectionToPayCount, 10) > 0) {
      strRet = YesNoMessage("Confirm", "A Final Estimate & Direction To Pay document already exists for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Final Estimate and Direction To Pay?", 140);
    }

    if (strRet != "Yes") {
      selectRow(null);
      return;
    }

    //Get the previous DirectionToPay document
    var oRows = tblReceived.rows;
    for (var i = 0; i < oRows.length; i++)
    {
      if (
          oRows[i].cells[ idxClaimAspectID ].innerText == sClaimAspectID &&
          oRows[i].cells[ idxClaimAspectServiceChannelID ].innerText == sServiceChannelID
      ){
        if (oRows[i].cells[ idxDirectionToPayFlag ].innerText == "1"){
          oRows[i].cells[ idxDirectionToPayFlag ].innerText = "0";
        }
        if (oRows[i].cells[ idxFinalEstimateFlag ].innerText == "1"){
          oRows[i].cells[ idxFinalEstimateFlag ].innerText = "0";
        }
        oRows[i].title = "";
        oRows[i].dirty = true;
        if ((i % 2) == 0)
          oRows[i].style.backgroundColor = "#fff7e5";
        else
          oRows[i].style.backgroundColor = "#FFFFFF";
      }
    }
    iValue = 1;
    sCurRowColor = oCurRow.style.backgroundColor = sFinalAndDirectionToPayColor;
    oCurRow.title = "Document marked as the Final Estimate and Direction To Pay";
    iFinalAndDirectionToPayCount = iValue;
    oCurRow.cells[ idxDirectionToPayFlag ].innerText = iValue;
    oCurRow.cells[ idxFinalEstimateFlag ].innerText = iValue;
    var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
    var oXMLNode = xmlDocuments.selectSingleNode("/Root/Document[@DocumentID = '" + sDocumentID + "']");
    if (oXMLNode){
      oXMLNode.setAttribute("DirectionToPayFlag", 1);
      oXMLNode.setAttribute("FinalEstimateFlag", 1);
    }
  } else {
    //unmark row as final estimate and direction to pay
    var strRet = YesNoMessage("Confirm", "Do you want to unmark this document from Final Estimate & Direction To Pay?");
    if (strRet != "Yes") {
      selectRow(null);
      return;
    }
    iValue = 0;
    sCurRowColor = ((oCurRow.rowIndex % 2) == 0 ? "#fff7e5" : "#FFFFFF")
    oCurRow.style.backgroundColor = sCurRowColor;
    oCurRow.title = "";
    iFinalAndDirectionToPayCount = iValue;
    oCurRow.cells[ idxDirectionToPayFlag ].innerText = iValue;
    oCurRow.cells[ idxFinalEstimateFlag ].innerText = iValue;
    var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
    var oXMLNode = xmlDocuments.selectSingleNode("/Root/Document[@DocumentID = '" + sDocumentID + "']");
    if (oXMLNode){
      oXMLNode.setAttribute("DirectionToPayFlag", 0);
      oXMLNode.setAttribute("FinalEstimateFlag", 0);
    }
  }
  if (iValue == 1)
    oCurRow.style.backgroundColor = sFinalAndDirectionToPayColor;
   oCurRow.dirty = true;
   gbDirtyFlag = true;
  oPopup.hide();
  document.getElementById("content11").focus(); //to activate the ADS buttons
  }

function DeleteDocument(){
  var strRet = YesNoMessage("Confirm", "Do you want to mark this document for deletion?");
  if (strRet != "Yes") {
    selectRow(null);
    return;
  }
  oCurRow.deleteDoc = true;
  gbDirtyFlag = true;
  oCurRow.cells[ idxDocumentMenu ].style.border = "1px solid #800000";
  oCurRow.title = "Document marked for deletion.";
}

function SetAsApprovedDocument(){
  var iValue = 0;
  var iFBEEstimate = oCurRow.cells[ idxFBEFlag ].innerText;
  var sVANFlag = oCurRow.cells[ idxVANFlag ].innerText;
  var sAgreedPriceMetCD = oCurRow.cells[ idxAgreedPriceMetCD ].innerText;
  var sSvcChannel = oCurRow.cells[ idxServiceChannel ].innerText;
   if (sVANFlag != "1" && sAgreedPriceMetCD != "Y" && sSvcChannel != "Repair Referral" ){
      ClientWarning("Cannot approve this document for early Billing. Price Agreed is not Yes.");
  } else {
     if	 (oCurRow.cells[ idxApprovedFlag ].innerText == "0" /*||
       (oCurRow.cells[ idxApprovedFlag ].innerText == "1" &&
       oCurRow.cells[ idxFBEFlag ].innerText == "1")*/)
     {
       var sClaimAspectID = oCurRow.cells[ idxClaimAspectID ].innerText;
       var sServiceChannelID = oCurRow.cells[ idxClaimAspectServiceChannelID ].innerText;
       var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
   
       CalcAEAndFBECount( sClaimAspectID, sServiceChannelID, sDocumentID );
   
       //mark row as direction to pay
       if (parseInt(iApprovedDocCount, 10) > 0 || parseInt(iFBEAndApprovedDocCount, 10) > 0) {
         if (parseInt(iFBEAndApprovedDocCount, 10) > 0){
           ClientWarning("An Initial Bill document already exists for this service channel. Cannot mark this as an Initial Bill document."); 
           return;
           //var strRet = YesNoMessage("Confirm", "An Initial Bill and Final Bill Estimate document already exist for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Initial Bill Estimate/Supplement?.");
           //if (strRet == "Yes") iFinalAndDirectionToPayCount = 0;
         }
         if (parseInt(iApprovedDocCount, 10) > 0) {
            ClientWarning("An Initial Bill document already exists for this service channel. Cannot mark this as an Initial Bill document.");
            return;
           //var strRet = YesNoMessage("Confirm", "An Initial Bill Estimate/Supplement document already exists for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Initial Bill Estimate/Supplement?");
         }
         if (strRet != "Yes") {
           selectRow(null);
           return;
         }
   
         //Reset the previous Approved document
         var oRows = tblReceived.rows;
         for (var i = 0; i < oRows.length; i++)
         {
           if (oRows[i].cells[ idxApprovedFlag ].innerText == "1" &&
               oRows[i].cells[ idxClaimAspectID ].innerText == sClaimAspectID &&
               oRows[i].cells[ idxClaimAspectServiceChannelID ].innerText == sServiceChannelID
           ){
             oRows[i].cells[ idxApprovedFlag ].innerText = "0";
             //oRows[i].cells[ idxApprovedFlag ].innerText = "0";
             oRows[i].title = "";
             oRows[i].dirty = true;
             if ((i % 2) == 0)
               oRows[i].style.backgroundColor = "#fff7e5";
             else
               oRows[i].style.backgroundColor = "#FFFFFF";
           }
         }
       }
       iValue = 1;
       sCurRowColor = oCurRow.style.backgroundColor = sAEColor;
       oCurRow.title = "Document marked as the Initial Bill Estimate/Supplement for Early Billing";
       iApprovedDocCount = iValue;
       iFBEAndApprovedDocCount = 0;
       // row changing from Final Estimate to Direction To Pay
       if (iFBEEstimate == "1") iFBECount = 0;
       oCurRow.cells[ idxApprovedFlag ].innerText = iValue;
       //oCurRow.cells[ idxFBEFlag ].innerText = 0;
       var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
       var oXMLNode = xmlDocuments.selectSingleNode("/Root/Document[@DocumentID = '" + sDocumentID + "']");
       if (oXMLNode){
         oXMLNode.setAttribute("ApprovedFlag", 1);
         //oXMLNode.setAttribute("FinalBillEstimateFlag", 0);
       }
     } else {
       //unmark row as initial bill (approved) estimate/supplement
	   // alert("unmarked, billed falg");
	   // alert(sBilledFlag);
       var sBilledFlag = oCurRow.cells[ idxBilledFlag ].innerText;
       if (sBilledFlag == "1"){
         ClientWarning("Cannot unmark this document from Initial Bill Estimate/Supplement. An Invoice has been generated against this document.");
         return;
       }
       var strRet = YesNoMessage("Confirm", "Do you want to unmark this document from Initial Bill Estimate/Supplement?");
       if (strRet != "Yes") {
         selectRow(null);
         return;
       }
       iValue = 0;
       sCurRowColor = ((oCurRow.rowIndex % 2) == 0 ? "#fff7e5" : "#FFFFFF")
       oCurRow.style.backgroundColor = sCurRowColor;
       oCurRow.title = "";
       iApprovedDocCount = iValue;
       iFBEAndApprovedDocCount = 0;
       oCurRow.cells[ idxApprovedFlag ].innerText = iValue;
       var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
       var oXMLNode = xmlDocuments.selectSingleNode("/Root/Document[@DocumentID = '" + sDocumentID + "']");
	  // alert(oXMLNode);
       if (oXMLNode){
         oXMLNode.setAttribute("ApprovedFlag", 0);
       }
     }
     //if (iValue == 1) // current row is Approved Document. If Final Bill Estimate was selected, then the row is both
     //  oCurRow.style.backgroundColor = (oCurRow.cells[ idxFBEFlag ].innerText == "1" ? sAEAndFBEColor : sAEColor);
      if (oCurRow.cells[ idxApprovedFlag ].innerText == "1")
         oCurRow.style.backgroundColor = sAEColor;
      if (oCurRow.cells[ idxFBEFlag ].innerText == "1")
         oCurRow.style.backgroundColor = sFBEColor;
      if (oCurRow.cells[ idxApprovedFlag ].innerText == "1" && oCurRow.cells[ idxFBEFlag ].innerText == "1")
         oCurRow.style.backgroundColor = sAEAndFBEColor;
      oCurRow.dirty = true;
      gbDirtyFlag = true;
  }
  oPopup.hide();
  document.getElementById("content11").focus(); //to activate the ADS buttons
}

function SetAsFinalBillEstimate(){
  if (sEarlyBillFlag != "1") {
    SetAsFinalEstimate(); return;
  }
  var iValue = 0;
  var iApprovedFlag = oCurRow.cells[ idxApprovedFlag ].innerText;
  if ( oCurRow.cells[ idxFBEFlag ].innerText == "0" /*||
    (oCurRow.cells[ idxApprovedFlag ].innerText == "1" &&
    oCurRow.cells[ idxFBEFlag ].innerText == "1")*/)
  {
    var sBilledFlag = oCurRow.cells[ idxBilledFlag ].innerText;
    if (iApprovedFlag == "1" && sBilledFlag == "1"){
        var strRet = YesNoMessage("Confirm", "This document was marked as Initial Bill Estimate/Supplement and an Invoice has been generated. Do you want to mark this document as both Initial Bill and Final Bill Estimate?.");
        if (strRet == "Yes") {
            SetAsBothApprovedFinalBillEstimate();
            return;
        } else {
            return;
        }
    }

    var sClaimAspectID = oCurRow.cells[ idxClaimAspectID ].innerText;
    var sServiceChannelID = oCurRow.cells[ idxClaimAspectServiceChannelID ].innerText;
    var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;

    CalcAEAndFBECount( sClaimAspectID, sServiceChannelID, sDocumentID );
 
    if (parseInt(iFBECount, 10) > 0 || parseInt(iFBEAndApprovedDocCount, 10) > 0) {
      if (parseInt(iFBEAndApprovedDocCount, 10) > 0){
        var strRet = YesNoMessage("Confirm", "An Initial Bill and Final Bill Estimate document already exist for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Initial Bill Estimate/Supplement?.");
        if (strRet == "Yes") iFBEAndApprovedDocCount = 0;
      }
      if (parseInt(iFBECount, 10) > 0) {
        var strRet = YesNoMessage("Confirm", "A Final Bill Estimate document already exists for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Final Bill Estimate?");
      }

      if (strRet != "Yes") {
        selectRow(null);
        return;
      }

      //Get the previous DirectionToPay document
      var oRows = tblReceived.rows;
      for (var i = 0; i < oRows.length; i++)
      {
        if (oRows[i].cells[ idxFBEFlag ].innerText == "1" &&
            oRows[i].cells[ idxClaimAspectID ].innerText == sClaimAspectID &&
            oRows[i].cells[ idxClaimAspectServiceChannelID ].innerText == sServiceChannelID
        ){
          //oRows[i].cells[ idxApprovedFlag ].innerText = "0";
          oRows[i].cells[ idxFBEFlag ].innerText = "0";
          oRows[i].title = "";
          oRows[i].dirty = true;
          if ((i % 2) == 0)
            oRows[i].style.backgroundColor = "#fff7e5";
          else
            oRows[i].style.backgroundColor = "#FFFFFF";
        }
      }
    }
    
    
    var strCurrentEstimateNetAmt = oCurRow.cells[ idxEstimateNetAmt ].innerText;
    oCurRow.reviseInvoice = false;
    
    if (iApprovedDocCount > 0){
       if (parseFloat(strApprovedEstimateNetAmt) > parseFloat(strCurrentEstimateNetAmt)){
         if (strInvoiceDocumentID != null) {
            if (YesNoMessage("Confirm", "Final Bill Estimate net amount [$" + formatCurrencyString(strCurrentEstimateNetAmt) + "] is less than the Initial Bill Estimate [$" + formatCurrencyString(strApprovedEstimateNetAmt) + "].\nIs this correct?.\n\n Click Yes if this information is correct and a revised invoice will be generated. Please send this revised invoice to the client.") == "Yes"){
               //alert("generate a revised invoice.");
               oCurRow.reviseInvoice = true;
               oCurRow.revisedAmount = strCurrentEstimateNetAmt;
            }
         } else {
            ClientWarning("An invoice was not generated. Please update the indemnity payment to the Final Bill Estimate before generating the invoice.");
         }
       }
    }

    iValue = 1;
    sCurRowColor = oCurRow.style.backgroundColor = sFBEColor;
    oCurRow.title = "Document marked as the Final Bill Estimate";
    iFBECount = iValue;
    iFBEAndApprovedDocCount = 0;
    if (iApprovedFlag == 1) iApprovedDocCount = 0;
    oCurRow.cells[ idxFBEFlag ].innerText = iValue;
    //oCurRow.cells[ idxApprovedFlag ].innerText = 0;
    var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
    var oXMLNode = xmlDocuments.selectSingleNode("/Root/Document[@DocumentID = '" + sDocumentID + "']");
    if (oXMLNode){
      //oXMLNode.setAttribute("ApprovedFlag", 0);
      oXMLNode.setAttribute("FinalBillEstimateFlag", 1);
    }
  } else {
    //unmark row as final estimate
    var strRet = YesNoMessage("Confirm", "Do you want to unmark this document from Final Bill Estimate?");
    if (strRet != "Yes") {
      selectRow(null);
      return;
    }
    iValue = 0;
    sCurRowColor = ((oCurRow.rowIndex % 2) == 0 ? "#fff7e5" : "#FFFFFF")
    oCurRow.style.backgroundColor = sCurRowColor;
    oCurRow.title = "";
    iFBECount = iValue;
    iFBEAndApprovedDocCount = 0;
    oCurRow.cells[ idxFBEFlag ].innerText = iValue;
    var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
    var oXMLNode = xmlDocuments.selectSingleNode("/Root/Document[@DocumentID = '" + sDocumentID + "']");
    if (oXMLNode){
      oXMLNode.setAttribute("FinalBillEstimateFlag", 0);
    }
  }
    //if (iValue == 1) // current row is Final Estimate. If Direction to Pay was selected, then the row is both
      //oCurRow.style.backgroundColor = (oCurRow.cells[ idxApprovedFlag ].innerText == "1" && oCurRow.cells[ idxFBEFlag ].innerText == "1"  ? sAEAndFBEColor : sFBEColor);
   if (oCurRow.cells[ idxApprovedFlag ].innerText == "1")
      oCurRow.style.backgroundColor = sAEColor;
   if (oCurRow.cells[ idxFBEFlag ].innerText == "1")
      oCurRow.style.backgroundColor = sFBEColor;
   if (oCurRow.cells[ idxApprovedFlag ].innerText == "1" && oCurRow.cells[ idxFBEFlag ].innerText == "1")
      oCurRow.style.backgroundColor = sAEAndFBEColor;
   oCurRow.dirty = true;
   gbDirtyFlag = true;
    oPopup.hide();
    document.getElementById("content11").focus(); //to activate the ADS buttons
}

function SetAsBothApprovedFinalBillEstimate(){
  var iValue = 0;
  var sAgreedPriceMetCD = oCurRow.cells[ idxAgreedPriceMetCD ].innerText;
  if (sAgreedPriceMetCD != "Y" ){
      ClientWarning("Cannot approve this document for early Billing. Price Agreed is not Yes.");
  } else {
     if (oCurRow.cells[ idxApprovedFlag ].innerText != "1" ||
         oCurRow.cells[ idxFBEFlag ].innerText != "1")
     {
       var sClaimAspectID = oCurRow.cells[ idxClaimAspectID ].innerText;
       var sServiceChannelID = oCurRow.cells[ idxClaimAspectServiceChannelID ].innerText;
       var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
   
       CalcAEAndFBECount( sClaimAspectID, sServiceChannelID, sDocumentID );
   
       var strRet = "Yes";
       if (parseInt(iApprovedDocCount, 10) > 0 && parseInt(iFBECount, 10) > 0) {
           ClientWarning("An Initial Bill document already exists for this service channel. Cannot mark this as an Initial Bill and Final Bill Estimate document."); 
           return;
         
         /*strRet = YesNoMessage("Confirm", "1: An Initial Bill and Final Bill Estimate document already exists for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Initial Bill and Final Bill Estimate?", 140);
         if (strRet == "Yes") {
           iApprovedDocCount = 0;
           iFBEAndApprovedDocCount = 0;
         }*/
       } else  if (parseInt(iApprovedDocCount, 10) > 0) {
           ClientWarning("An Approved document already exists for this service channel. Cannot mark this as an Approved and Final Bill Estimate document."); 
           return;
         /*strRet = YesNoMessage("Confirm", "An Initial Bill Estimate/Supplement document already exists for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Initial Bill and Final Bill Estimate?", 140);
         if (strRet == "Yes") {
           iDirectionToPayCount = 0;
         }*/
       } else if (parseInt(iFBECount, 10) > 0) {
         strRet = YesNoMessage("Confirm", "A Final Bill Estimate document already exists for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Initial Bill and Final Bill Estimate?", 140);
         if (strRet == "Yes") {
           iFinalEstimateCount = 0;
         }
       } else if (parseInt(iFBEAndApprovedDocCount, 10) > 0) {
         strRet = YesNoMessage("Confirm", "2: An Initial Bill and Final Bill Estimate document already exists for this service channel on " + sCurNodePertainsTo + ". Do you want to mark this document as the Initial Bill and Final Bill Estimate?", 140);
       }
   
       if (strRet != "Yes") {
         selectRow(null);
         return;
       }
   
       //Get the previous DirectionToPay document
       var oRows = tblReceived.rows;
       for (var i = 0; i < oRows.length; i++)
       {
         if (
             oRows[i].cells[ idxClaimAspectID ].innerText == sClaimAspectID &&
             oRows[i].cells[ idxClaimAspectServiceChannelID ].innerText == sServiceChannelID
         ){
           if (oRows[i].cells[ idxApprovedFlag ].innerText == "1"){
             oRows[i].cells[ idxApprovedFlag ].innerText = "0";
           }
           if (oRows[i].cells[ idxFBEFlag ].innerText == "1"){
             oRows[i].cells[ idxFBEFlag ].innerText = "0";
           }
           oRows[i].title = "";
           oRows[i].dirty = true;
           if ((i % 2) == 0)
             oRows[i].style.backgroundColor = "#fff7e5";
           else
             oRows[i].style.backgroundColor = "#FFFFFF";
         }
       }
       iValue = 1;
       sCurRowColor = oCurRow.style.backgroundColor = sAEAndFBEColor;
       oCurRow.title = "Document marked as the Initial Bill and Final Bill Estimate";
       iFBEAndApprovedDocCount = iValue;
       oCurRow.cells[ idxApprovedFlag ].innerText = iValue;
       oCurRow.cells[ idxFBEFlag ].innerText = iValue;
       var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
       var oXMLNode = xmlDocuments.selectSingleNode("/Root/Document[@DocumentID = '" + sDocumentID + "']");
       if (oXMLNode){
         oXMLNode.setAttribute("ApprovedFlag", 1);
         oXMLNode.setAttribute("FinalBillEstimateFlag", 1);
       }
     } else {
       //unmark row as final estimate and direction to pay
       var sBilledFlag = oCurRow.cells[ idxBilledFlag ].innerText;
       if (sBilledFlag == "1"){
         ClientWarning("Cannot unmark this document from Initial Bill Estimate/Supplement. An Invoice has been generated against this document.");
         return;
       }
   
       var strRet = YesNoMessage("Confirm", "Do you want to unmark this document from Initial Bill and Final Bill Estimate?");
       if (strRet != "Yes") {
         selectRow(null);
         return;
       }
       iValue = 0;
       sCurRowColor = ((oCurRow.rowIndex % 2) == 0 ? "#fff7e5" : "#FFFFFF")
       oCurRow.style.backgroundColor = sCurRowColor;
       oCurRow.title = "";
       iFBEAndApprovedDocCount = iValue;
       oCurRow.cells[ idxApprovedFlag ].innerText = iValue;
       oCurRow.cells[ idxFBEFlag ].innerText = iValue;
       var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
       var oXMLNode = xmlDocuments.selectSingleNode("/Root/Document[@DocumentID = '" + sDocumentID + "']");
       if (oXMLNode){
         oXMLNode.setAttribute("ApprovedFlag", 0);
         oXMLNode.setAttribute("FinalBillEstimateFlag", 0);
       }
     }
     if (iValue == 1)
       oCurRow.style.backgroundColor = sAEAndFBEColor;
      oCurRow.dirty = true;
      gbDirtyFlag = true;
  }
   if (oCurRow.cells[ idxApprovedFlag ].innerText == "1")
      oCurRow.style.backgroundColor = sAEColor;
   if (oCurRow.cells[ idxFBEFlag ].innerText == "1")
      oCurRow.style.backgroundColor = sFBEColor;
   if (oCurRow.cells[ idxApprovedFlag ].innerText == "1" && oCurRow.cells[ idxFBEFlag ].innerText == "1")
      oCurRow.style.backgroundColor = sAEAndFBEColor;
  oPopup.hide();
  document.getElementById("content11").focus(); //to activate the ADS buttons
}

function SetAsWarrantyDocument(){
  var iValue = (oCurRow.cells[ idxWarrantyFlag ].innerText != "1" ? "1" : "0");
  var sDocumentID = oCurRow.cells[ idxDocumentID ].innerText;
  var oXMLNode = xmlDocuments.selectSingleNode("/Root/Document[@DocumentID = '" + sDocumentID + "']");
  if (oXMLNode){
    oXMLNode.setAttribute("idxWarrantyFlag", iValue);
  }
  oCurRow.cells[ idxWarrantyFlag ].innerText = iValue;
  oCurRow.cells[ 0 ].getElementsByTagName("IMG")[1].src = (iValue == 1 ? "/images/warranty_icon.png" : "/images/spacer.gif");
  oCurRow.dirty = true;
  gbDirtyFlag = true;
  oPopup.hide();
  document.getElementById("content11").focus(); //to activate the ADS buttons
}

//--------------------------------------------//
// 25Nov2014 - TVD - Added Hyperquest Support
//--------------------------------------------//
function SendToHyperquest(obj,iDocumentID,bEnabled) {
  //alert(iDocumentID);
  //alert(gsInsuranceCompanyID);
  
	if (gsCRUD.indexOf("U") == -1) return;
    var contextHtml="";
    var oTR = obj.parentElement.parentElement;

    if (oTR.tagName != "TR") return;

    oCurRow = oTR;

    var iDirectionToPay = oTR.cells[ idxDirectionToPayFlag ].innerText;
    //var iFinalEstimate = oTR.cells[ idxFinalEstimateFlag ].innerText;
    //var sVANFlag = oTR.cells[ idxVANFlag ].innerText;
    //var bEstimateType = false;
    //var sCurDocumentType = oTR.cells[ idxDocumentType ].innerText;
    //var sClaimAspectID = oTR.cells[ idxClaimAspectID ].innerText;
    //var sServiceChannelID = oTR.cells[ idxClaimAspectServiceChannelID ].innerText;
    //var sApprovedFlag = oTR.cells[ idxApprovedFlag ].innerText;
    //var sApprovedDate = oTR.cells[ idxApprovedDate ].innerText;
    //var sWarrantyFlag = oTR.cells[ idxWarrantyFlag ].innerText;
    //var sFBEFlag = oTR.cells[ idxFBEFlag ].innerText;
    //var sWarrantyExistsFlag = oTR.cells[ idxWarrantyExistsFlag ].innerText;
    //var sAgreedPriceMetCD = oTR.cells[ idxAgreedPriceMetCD ].innerText;
    //var sBilledFlag = oTR.cells[ idxBilledFlag ].innerText;

    var sSvcChannel = oTR.cells[ idxServiceChannel ].innerText;

    //var sCurNodePertainsTo = xmlPertainsTo.selectSingleNode("/Root/Reference[@List = 'PertainsTo' and @ReferenceID = '" + sClaimAspectID + "']/@Name").text;
    //alert(sCurNodePertainsTo);

	//------------------------------------------//
	// Hyperquest Section
	//------------------------------------------//
    var sTransmitToHQText = "Transmit Estimate/Supplement to HQ";
    
    contextHtml+='<TABLE unselectable="on" STYLE="border:1pt solid #666666" BGCOLOR="#F9F8F7" WIDTH="100%" HEIGHT="100%" CELLPADDING="0" CELLSPACING="1">';
    contextHtml+='<ST'+'YLE TYPE="text/css">\n';
    contextHtml+='td {font-family:verdana; font-size:11px;}\n';
    contextHtml+='</ST'+'YLE>\n';
    contextHtml+='<SC'+'RIPT LANGUAGE="JavaScript">\n';
    contextHtml+='\n<'+'!--\n';
    contextHtml+='window.onerror=null;\n';
    contextHtml+='/'+' -'+'->\n';
    contextHtml+='</'+'SCRIPT>\n';
    
    if (sCurrentTab == "Received") {

	//------------------------------------------//
	// Active or InActive Section
	//------------------------------------------//
		//if (sSvcChannel == "Desk Audit" || sSvcChannel == "Total Loss" || sSvcChannel.indexOf("Repair Referral") != -1) {		//differ menu options for program shop or total loss or Repair Referral
		if (sSvcChannel == "Desk Audit" ) {		
			//------------------------------------------//
			// Allow/DisAllow Send to HQ
			//------------------------------------------//
			if (bEnabled != true) {
        contextHtml+='<TR unselectable="on"><TD unselectable="on" ID="i1" STYLE="border:1pt solid #F9F8F7;color:#c0c0c0;cursor:default"';
			  contextHtml+='>&nbsp;' + sTransmitToHQText + '</TD></TR>';
      }
			else
			{
			  contextHtml+='<TR unselectable="on"><TD unselectable="on" ID="i1" STYLE="border:1pt solid #F9F8F7" ONMOUSEOVER="document.all.i1.style.background=\'#B6BDD2\';document.all.i1.style.border=\'1pt solid #0A246A\';document.all.i1.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i1.style.background=\'#F9F8F7\';document.all.i1.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.createHQJob(' + gsInsuranceCompanyID + ',' + iDocumentID + ');"'
			  contextHtml+='>&nbsp;' + sTransmitToHQText + '</TD></TR>';
			}
		}
	}

    contextHtml+='<TR unselectable="on" style="height:5px"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ><hr noshade style="height:1px"/></TD></TR>';
    contextHtml+='<TR unselectable="on"><TD unselectable="on" STYLE="border:1pt solid #F9F8F7" ID="i9" ONMOUSEOVER="document.all.i9.style.background=\'#B6BDD2\';document.all.i9.style.border=\'1pt solid #0A246A\';document.all.i9.style.cursor=\'hand\';" ONMOUSEOUT="document.all.i9.style.background=\'#F9F8F7\';document.all.i9.style.border=\'1pt solid #F9F8F7\';" ONCLICK="window.parent.selectRow(null);window.parent.oPopup.hide();">&nbsp;Cancel Menu</TD></TR>';
    contextHtml+='</TABLE>';

    var x = event.offsetX+4;
    var y = event.offsetY-1;
    var oPopupBody = oPopup.document.body;
  	//window.clipboardData.setData("Text", contextHtml);
    oPopupBody.innerHTML = contextHtml;
    var sHeight = 150; 
		var sHeight = 55;
		
  	if (sCurrentTab == "Received")
	  	oPopup.show(x, y, 275, sHeight, window.event.srcElement);
}  // End of Hyperquest section

//function SendToHyperquest(strWinType, strPage, strParams, bEnabled, strScreen) {
function createHQJob(iInscCompID,iDocumentID) {
  //alert(iDocumentID);
  //alert(iInscCompID);
  var strReq = "../Hyperquest.aspx" + "?InscCompID=" + iInscCompID + " &DocumentID=" + iDocumentID;
  var arrDocView = window.showModalDialog(strReq, 'True' , 'dialogHeight:95px;dialogWidth:350px;center:yes;help:no;resizable:no;scroll:no;status:no;');
  //alert("refresh now");
  window.location.reload();
}

if (document.attachEvent)
  document.attachEvent("onclick", top.hideAllMenuScriptlets);
else
  document.onclick = top.hideAllMenuScriptlets;

]]>
        </SCRIPT>

      </HEAD>
      <BODY class="bodyAPDSub" unselectable="on" onLoad="PageInit();"  onHelp="CallHelpFn();return false;"  style="overflow:hidden">
        <SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
        <SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

        <DIV unselectable="on" style="position:absolute; z-index:1; top:0px; left:0px;">
          <!-- Start Tabs1 -->
          <DIV unselectable="on" style="position:absolute; visibility:visible; top:2px; left:1px">
            <DIV unselectable="on" id="tabs1" class="apdtabs" >
              <SPAN id="tab11" class="tab1"  onclick="gsRec_Sent='content11';"  unselectable="on">Received</SPAN>
              <SPAN id="tab12" class="tab1"  onclick="gsRec_Sent='content12';"  unselectable="on">Sent</SPAN>
            </DIV>

            <!-- Start recieved Info -->
            <DIV class="content1" id="content11" name="Received" Update="uspDocumentUpdDetail" Delete="uspDocumentDel" style="height:490px; width:740px; padding:6px;" onfocusin="DivOnFocusIn(this)" Many="true" >
              <xsl:attribute name="CRUD">
                <xsl:value-of select="user:AdjustCRUD(string($DocumentCRUD))"/>
              </xsl:attribute>
              <DIV id="DOCRScrollTable" style="width:100%;" >
                <TABLE class="ClaimMiscInputs" onClick="sortColumn(event, 'tblReceived')" width="100%" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
                  <TBODY>
                    <TR class="QueueHeader" style="height:32px">
                      <!-- <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="99" style="cursor:default;">LynxID</TD> -->
                      <TD unselectable="on" class="TableSortHeader" type="None">
                        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                      </TD>
                      <TD unselectable="on" class="TableSortHeader" type="None">Doc. Image</TD>
                      <TD unselectable="on" class="TableSortHeader" type="String">R/S</TD>
                      <TD unselectable="on" class="TableSortHeader" type="String">Pertains To</TD>
                      <TD unselectable="on" class="TableSortHeader" type="String">
                        Service<BR/>Channel
                      </TD>
                      <TD unselectable="on" class="TableSortHeader" type="String">Type</TD>
                      <TD unselectable="on" class="TableSortHeader" type="String" sIndex="99" style="cursor:default;">
                        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                      </TD>
                      <TD unselectable="on" class="TableSortHeader" type="String">Source</TD>
                      <TD unselectable="on" class="TableSortHeader" type="String" sIndex="99" style="cursor:default;">Orig/Audit</TD>
                      <TD unselectable="on" class="TableSortHeader" type="String" sIndex="99" style="cursor:default;">Dup</TD>
                      <TD unselectable="on" class="TableSortHeader" type="String">Imaged By</TD>
                      <TD unselectable="on" class="TableSortHeader" type="Date">
                        Rec/Attach<BR/>Date
                      </TD>
                      <!-- <TD unselectable="on" class="TableSortHeader" type="None" sIndex="99" style="cursor:default;">Del</TD> -->
                    </TR>
                  </TBODY>
                </TABLE>
                <DIV class="autoflowTable" style="height:425px">
                  <TABLE id="tblReceived" class="GridTypeTable" width="100%" cellspacing="1" border="0" cellpadding="0">
                    <TBODY bgColor1="ffffff" bgColor2="fff7e5">
                      <xsl:choose>
                        <xsl:when test="contains($DocumentCRUD, 'R')">
                          <xsl:for-each select="Document[@DirectionalCD='I']" >
                            <xsl:sort select="@DocumentID" order="descending" data-type="number"/>
                            <xsl:call-template name="Documents">
                              <xsl:with-param name="myCRUD">
                                <xsl:value-of select="$DocumentCRUD"/>
                              </xsl:with-param>
                              <xsl:with-param name="myClaimStatus">
                                <xsl:value-of select="$ClaimStatus"/>
                              </xsl:with-param>
                            </xsl:call-template>
                          </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                          <div style="width:100%;text-align:center">
                            <br/>
                            <font color="#FF0000">You do not have permissions to view this tab.</font><br/>
                            Please contact your supervisor.
                          </div>
                        </xsl:otherwise>
                      </xsl:choose>
                    </TBODY>
                  </TABLE>
                </DIV>
              </DIV>
              <table border="0" cellspacing="0" cellpadding="2">
                <colgroup>
                  <col width="75px"/>
                  <col width="15px"/>
                  <col width="30px"/>
                  <col width="15px"/>
                  <col width="30px"/>
                  <col width="15px"/>
                  <col width="55px"/>
                  <col width="15px"/>
                  <col width="30px"/>
                  <col width="15px"/>
                  <col width="30px"/>
                  <col width="15px"/>
                  <col width="*"/>
                </colgroup>
                <tr>
                  <td style="font-weight:bold">Legend:</td>
                  <td>
                    <div style="border:1px solid #C0C0C0; background-color:00CC99;height:15px;width:15px;"></div>
                  </td>
                  <td title="Final Estimate">FE</td>
                  <td>
                    <div style="border:1px solid #C0C0C0; background-color:33CCCC;height:15px;width:15px;"></div>
                  </td>
                  <td title="Direction To Pay">DTP</td>
                  <td>
                    <div style="border:1px solid #C0C0C0; background-color:66FF66;height:15px;width:15px;"></div>
                  </td>
                  <td title="Final Estimate and Direction To Pay">FE &amp; DTP</td>
                  <td>
                    <div style="border:1px solid #C0C0C0; background-color:#E1C1EE;height:15px;width:15px;"></div>
                  </td>
                  <td title="Initial Bill Estimate for Early Bill">AE</td>
                  <td>
                    <div style="border:1px solid #C0C0C0; background-color:#CC95E1;height:15px;width:15px;"></div>
                  </td>
                  <td title="Final Bill Estimate">FBE</td>
                  <td>
                    <div style="border:1px solid #C0C0C0; background-color:#7C07A9;height:15px;width:15px;"></div>
                  </td>
                  <td title="Initial Bill Estimate for Early Bill and Final Bill Estimate">AE &amp; FBE</td>
                </tr>
              </table>

              <SCRIPT>ADS_Buttons('content11');</SCRIPT>
            </DIV>

            <!-- Start Sent Info -->
            <DIV class="content1" id="content12" name="Sent" Update="uspDocumentUpdDetail" Delete="uspDocumentDel" style="height:488px; width:740px;  padding:6px;" onfocusin="DivOnFocusIn(this)"  Many="true">
              <xsl:attribute name="CRUD">
                <xsl:value-of select="user:AdjustCRUD(string($DocumentCRUD))"/>
              </xsl:attribute>
              <DIV id="DOCSScrollTable" style="width:100%;" >
                <TABLE class="ClaimMiscInputs" onClick="sortColumn(event, 'tblSent')" width="100%" cellspacing="0" border="0" cellpadding="2" style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 10px; text-align : center;table-layout:fixed;">
                  <TBODY>
                    <TR class="QueueHeader" style="height:32px">
                      <TD unselectable="on" class="TableSortHeader" type="Number" sIndex="99" style="cursor:default;">
                        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                      </TD>
                      <TD unselectable="on" class="TableSortHeader" type="None">Doc. Image</TD>
                      <TD unselectable="on" class="TableSortHeader" type="String">R/S</TD>
                      <TD unselectable="on" class="TableSortHeader" type="String">Pertains To</TD>
                      <TD unselectable="on" class="TableSortHeader" type="String">
                        Service<BR/>Channel
                      </TD>
                      <TD unselectable="on" class="TableSortHeader" type="String">Type</TD>
                      <TD unselectable="on" class="TableSortHeader" type="String" sIndex="99" style="cursor:default;">
                        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                      </TD>
                      <TD unselectable="on" class="TableSortHeader" type="String">Source</TD>
                      <TD unselectable="on" class="TableSortHeader" type="String" sIndex="99" style="cursor:default;">Orig/Audit</TD>
                      <TD unselectable="on" class="TableSortHeader" type="String" sIndex="99" style="cursor:default;">Dup</TD>
                      <TD unselectable="on" class="TableSortHeader" type="String">Imaged By</TD>
                      <TD unselectable="on" class="TableSortHeader" type="Date">
                        Rec/Attach<BR/>Date
                      </TD>
                      <!-- <TD unselectable="on" class="TableSortHeader" type="None" sIndex="99" style="cursor:default;">Del</TD> -->
                    </TR>
                  </TBODY>
                </TABLE>
                <DIV class="autoflowTable" style="height:444px">
                  <TABLE id="tblSent" class="GridTypeTable" width="100%" cellspacing="1" border="0" cellpadding="0">
                    <TBODY bgColor1="ffffff" bgColor2="fff7e5">
                      <xsl:choose>
                        <xsl:when test="contains($DocumentCRUD, 'R')">
                          <xsl:for-each select="Document[@DirectionalCD='O']" >
                            <xsl:call-template name="Documents">
                              <xsl:with-param name="myCRUD">
                                <xsl:value-of select="$DocumentCRUD"/>
                              </xsl:with-param>
                              <xsl:with-param name="myClaimStatus">
                                <xsl:value-of select="$ClaimStatus"/>
                              </xsl:with-param>
                            </xsl:call-template>
                          </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                          <div style="width:100%;text-align:center">
                            <br/>
                            <font color="#FF0000">You do not have permissions to view this tab.</font><br/>
                            Please contact your supervisor.
                          </div>
                        </xsl:otherwise>
                      </xsl:choose>
                    </TBODY>
                  </TABLE>
                </DIV>
              </DIV>

              <SCRIPT>ADS_Buttons('content12');</SCRIPT>
            </DIV>
          </DIV>
        </DIV>

        <xml id="xmlDocumentSource">
          <Root>
            <xsl:for-each select="/Root/Reference[@List='DocumentSource']">
              <xsl:apply-templates select="."/>
            </xsl:for-each>
          </Root>
        </xml>
        <xml id="xmlDocumentType">
          <Root>
            <xsl:for-each select="/Root/Reference[@List='DocumentType']">
              <xsl:apply-templates select="."/>
            </xsl:for-each>
          </Root>
        </xml>
        <xml id="xmlPertainsTo">
          <Root>
            <xsl:for-each select="/Root/Reference[@List='PertainsTo']">
              <xsl:apply-templates select="."/>
            </xsl:for-each>
          </Root>
        </xml>
        <xml id="xmlServiceChannel">
          <Root>
            <xsl:for-each select="/Root/Reference[@List='ServiceChannel']">
              <xsl:apply-templates select="."/>
            </xsl:for-each>
          </Root>
        </xml>
        <xml id="xmlDirectional">
          <Root>
            <Reference List="Directional" ReferenceID="I" Name="Received"/>
            <Reference List="Directional" ReferenceID="O" Name="Sent"/>
          </Root>
        </xml>
        <xml id="xmlDocuments">
          <Root>
            <xsl:copy-of select="/Root/Document"/>
          </Root>
        </xml>
      </BODY>
    </HTML>

  </xsl:template>

  <xsl:template match="node()">
    <xsl:text disable-output-escaping="yes">&lt;</xsl:text>Reference <xsl:apply-templates select="@*"/>/<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="@*">
    <xsl:value-of select="name(.)"/>="<xsl:value-of disable-output-escaping="no" select="."/>"
  </xsl:template>

  <xsl:template name="Documents">

    <xsl:param name="myCRUD"/>
    <xsl:param name="myClaimStatus"/>

    <TR unselectable="on" style="cursor:default;height:24px" onclick="selectRow(this)">
      <xsl:choose>
        <xsl:when test="@FinalEstimateFlag = '1' and @DirectionToPayFlag = '1'">
          <xsl:attribute name="bgColor">#66ff66</xsl:attribute>
          <xsl:attribute name="title">Document marked as the Final Estimate and Direction To Pay</xsl:attribute>
        </xsl:when>
        <xsl:when test="@FinalEstimateFlag = '1' and @DirectionToPayFlag = '0'">
          <xsl:attribute name="bgColor">#00cc99</xsl:attribute>
          <xsl:attribute name="title">Document marked as the Final Estimate</xsl:attribute>
        </xsl:when>
        <xsl:when test="@FinalEstimateFlag = '0' and @DirectionToPayFlag = '1'">
          <xsl:attribute name="bgColor">#33cccc</xsl:attribute>
          <xsl:attribute name="title">Document marked as the Direction To Pay</xsl:attribute>
        </xsl:when>
        <xsl:when test="@ApprovedFlag = '1' and @FinalBillEstimateFlag = '1'">
          <xsl:attribute name="bgColor">#B564D4</xsl:attribute>
          <xsl:attribute name="title">Document marked as the Initial Bill and Final Bill Estimate</xsl:attribute>
        </xsl:when>
        <xsl:when test="@ApprovedFlag = '1' and @FinalBillEstimateFlag = '0'">
          <xsl:attribute name="bgColor">#E1C1EE</xsl:attribute>
          <xsl:attribute name="title">Document marked as Initial Bill for Early Bill</xsl:attribute>
        </xsl:when>
        <xsl:when test="@ApprovedFlag = '0' and @FinalBillEstimateFlag = '1'">
          <xsl:attribute name="bgColor">#CC95E1</xsl:attribute>
          <xsl:attribute name="title">Document marked as the Final Bill Estimate</xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="bgColor">
            <xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <TD unselectable="on" class="GridTypeTD" width="8%" nowrap="" ondblclick1="ShowClaimReAssign(this)" onmouseover1="EditableCellOver(this)" onmouseout1="EditableCellOut(this)">
        <!-- <xsl:value-of select="/Root/@LynxID" /> -->
        <span style="cursor:hand;width:24px;" onclick="showDocumentMenu(this)" oncontextmenu="return false" title="Click for available options.">
          <img src="/images/claimDoc.gif" style="cursor:hand"/>
        </span>
        
        <!-- 25Nov2014 - ADDING Hyperquest Button -->
        <!-- xsl:value-of select="@ServiceChannelCD"/ -->
        <xsl:if test="@ServiceChannelCD = 'DA' and @HyperquestClientFlag = '1'">
          <xsl:if test="@DocumentSourceID != 16">
            <xsl:if test="(@DocumentTypeID = '10' or @DocumentTypeID = '3')">
              <!-- CHECK HQ STATE and change icon -->

              <xsl:variable name="ClaimAspectID" select="/Root/ClaimAspect/@ClaimAspectID"></xsl:variable>
              <xsl:variable name="HQVehStatus" select="/Root/ClaimAspect[@ClaimAspectID=$ClaimAspectID]/@Status"></xsl:variable>

              <xsl:variable name="HQActive">
                <xsl:choose>
                  <xsl:when test="$HQVehStatus='Active'">
                    <xsl:value-of select="'true'"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="'false'"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>

              <xsl:choose>
                <xsl:when test="@HyperquestStatus = ''">
                  <xsl:attribute name="bgColor">#66ff66</xsl:attribute>
                  <xsl:attribute name="title">Document waiting to be processed to Hyperquest</xsl:attribute>
                  <img src="/images/HQNew.png" style="cursor:hand"  title="Click for available options" onclick="SendToHyperquest(this,{@DocumentID},{$HQActive})" />  
                </xsl:when>
                <xsl:when test="@HyperquestStatus = 'Unprocessed'">
                  <xsl:attribute name="bgColor">#66ff66</xsl:attribute>
                  <xsl:attribute name="title">Document waiting to be processed to Hyperquest</xsl:attribute>
                  <img src="/images/HQNew.png" style="cursor:hand"  title="Click for available options" onclick="SendToHyperquest(this,{@DocumentID},{$HQActive})"/>
                </xsl:when>
                <xsl:when test="@HyperquestStatus = 'Processing'">
                  <xsl:attribute name="bgColor">#66ff66</xsl:attribute>
                  <xsl:attribute name="title">Document processing to Hyperquest</xsl:attribute>
                  <img src="/images/HQTrans.png" style="cursor:hand"  title="Document processing to Hyperquest..." onclick="SendToHyperquest(this,{@DocumentID},false)"/>
                </xsl:when>
                <xsl:when test="@HyperquestStatus = 'Processed'">
                  <xsl:attribute name="bgColor">#66ff66</xsl:attribute>
                  <xsl:attribute name="title">Document successfully processed to Hyperquest</xsl:attribute>
                  <img src="/images/HQRecv.png" style="cursor:hand"  title="Document successfully sent to Hyperquest" onclick="SendToHyperquest(this,{@DocumentID},false)"/>
                </xsl:when>
                <xsl:when test="@HyperquestStatus = 'Cancelled'">
                  <xsl:attribute name="bgColor">#66ff66</xsl:attribute>
                  <xsl:attribute name="title">Document cancelled by Hyperquest</xsl:attribute>
                  <img src="/images/HQCan.png" style="cursor:hand"  title="Document cancelled by Hyperquest" onclick="SendToHyperquest(this,{@DocumentID},false)"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="bgColor">
                    <xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/>
                  </xsl:attribute>
                  <img src="/images/HQNew.png" style="cursor:hand"  title="Click for available options" onclick="SendToHyperquest(this,{@DocumentID},{$HQActive})"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:if>
          </xsl:if>
        </xsl:if>
        <!-- 25Nov2014 - ADDING Hyperquest Button -->

        <span style="width:21px;" title="Warranty Document">
          <img>
            <xsl:choose>
              <xsl:when test="@WarrantyFlag = '1'">
                <xsl:attribute name="src">/images/warranty_icon.png</xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="src">/images/spacer.gif</xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
          </img>
        </span>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="7%" nowrap="">
       	<!-- Convert to lowercase -->
        <xsl:variable name="chkImageLocation" select="translate(@ImageLocation, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
        <xsl:choose>
          <xsl:when test="$chkImageLocation = ''">
            <xsl:text disable-output-escaping="yes">&lt;IMG unselectable='on' src='/images/NotEdited.gif' width='15' height='15' oncontextmenu='return false;' title='Image not available'" style="FILTER:alpha(opacity=60);"></xsl:text>
          </xsl:when>
          <xsl:when test="contains(substring-after($chkImageLocation, '.'), 'xml') or contains(substring-after($chkImageLocation, '.'), 'pdf')">
            <xsl:text disable-output-escaping="yes">&lt;IMG unselectable='on' src='/images/duplicate.gif' width='16' height='16' style='cursor:hand' onClick="ShowImage('</xsl:text>
            <xsl:value-of select="@ImageLocation"/>, <xsl:value-of select="@Archived"/>
            <xsl:text disable-output-escaping="yes">');" oncontextmenu="return false;" title='xml/pdf type image' style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"></xsl:text>

			<!-- 08Sep2021 - TVD - Allow all direct open.  -->
            <!-- xsl:if test="@DocumentTypeID = 12" -->
              <xsl:text disable-output-escaping="yes">&lt;IMG unselectable='on' src='/images/Edited.gif' width='16' height='16' style='cursor:hand' onClick="ShowImageDirect('</xsl:text>
              <xsl:value-of select="@ImageLocation"/>
              <xsl:text disable-output-escaping="yes">');" oncontextmenu="return false;" title='xml/pdf direct open' style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"></xsl:text>
            <!-- /xsl:if -->
            
          </xsl:when>
          <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">&lt;IMG unselectable='on' src='/images/imageview.gif' style='cursor:hand' onClick="ShowImage('</xsl:text>
            <xsl:value-of select="@ImageLocation"/>, <xsl:value-of select="@Archived"/>
            <xsl:text disable-output-escaping="yes">');" oncontextmenu1="callEstImgContextMenu(event.offsetX+4,event.offsetY-1,'</xsl:text>
            <xsl:value-of select="@ImageLocation"/>
            <xsl:text disable-output-escaping="yes">'); return false;" title='Graphic type image. Click to view.' style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"></xsl:text>
			
			<!-- 08Sep2021 - TVD - Allow all direct open.  -->
			<xsl:text disable-output-escaping="yes">&lt;IMG unselectable='on' src='/images/Edited.gif' width='16' height='16' style='cursor:hand' onClick="ShowImageDirect('</xsl:text>
            <xsl:value-of select="@ImageLocation"/>
            <xsl:text disable-output-escaping="yes">');" oncontextmenu="return false;" title='xml/pdf direct open' style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"></xsl:text>

		</xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="8%" nowrap="" IDIndex="18" >
        <xsl:if test="/Root/Reference[@List='DocumentSource' and @ReferenceID=current()/@DocumentSourceID]/@VANFlag='0'">
          <xsl:attribute name="ondblclick">EditableCellDblClick(this,3,1,'Directional')</xsl:attribute>
          <xsl:attribute name="onmouseover">EditableCellOver(this)</xsl:attribute>
          <xsl:attribute name="onmouseout">EditableCellOut(this)</xsl:attribute>
          <xsl:attribute name="style">cursor:text</xsl:attribute>
        </xsl:if>
        <xsl:choose>
          <xsl:when test="@DirectionalCD='I'">
            <xsl:text>Recvd</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>Sent</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" id="tdPertainsTo" class="GridTypeTD" width="10%" nowrap=""
          ondblclick="EditableCellDblClick(this,3,1,'PertainsTo')" onmouseover="EditableCellOver(this)"
          onmouseout="EditableCellOut(this)" style="cursor:text" IDIndex="16">

        <xsl:if test="/Root/Reference[@List='PertainsTo' and @ReferenceID=current()/@ClaimAspectID and @AssignmentID=current()/@AssignmentID]/@ShopLocationName != ''">
          <xsl:attribute name="title">
            <xsl:value-of select="/Root/Reference[@List='PertainsTo' and @ReferenceID=current()/@ClaimAspectID and @AssignmentID=current()/@AssignmentID]/@ShopLocationName"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:value-of select="/Root/Reference[@List='PertainsTo' and @ReferenceID=current()/@ClaimAspectID]/@Name"/>
      </TD>
      <TD unselectable="on" id="tdServiceChannel" class="GridTypeTD" width="10%" nowrap=""
          ondblclick="EditableCellDblClick(this,3,1,'ServiceChannel')" onmouseover="EditableCellOver(this)"
          onmouseout="EditableCellOut(this)" style="cursor:text" IDIndex="27" AssignmentIDIndex="20" >

        <xsl:value-of select="/Root/Reference[@List='ServiceChannel' and @ReferenceID=current()/@ClaimAspectServiceChannelID]/@Name"/>
        <xsl:if test="/Root/Reference[@List='ServiceChannel' and @ReferenceID=current()/@ClaimAspectServiceChannelID and @AssignmentID=current()/@AssignmentID]/@AssignmentSuffix != ''">
          <xsl:text>/Asgn </xsl:text>
          <xsl:value-of select="/Root/Reference[@List='ServiceChannel' and @ReferenceID=current()/@ClaimAspectServiceChannelID and @AssignmentID=current()/@AssignmentID]/@AssignmentSuffix"/>
        </xsl:if>
      </TD>
      <TD unselectable="on" id="tdDocumentType" class="GridTypeTD" width="10%" nowrap="" IDIndex="15">
        <xsl:if test="(@DocumentTypeID != '10' and @DocumentTypeID != '3') or (/Root/Reference[@List='DocumentSource' and @ReferenceID=current()/@DocumentSourceID]/@VANFlag='0')">
          <xsl:attribute name="ondblclick">EditableCellDblClick(this,3,1,'DocumentType')</xsl:attribute>
          <xsl:attribute name="onmouseover">EditableCellOver(this)</xsl:attribute>
          <xsl:attribute name="onmouseout">EditableCellOut(this)</xsl:attribute>
          <xsl:attribute name="style">cursor:text</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="/Root/Reference[@List='DocumentType' and @ReferenceID=current()/@DocumentTypeID]/@Name"/>

        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;

        <xsl:if test="@DocumentTypeID = '10' and @SupplementSeqNumber > 0">
          <xsl:value-of select="@SupplementSeqNumber" />
        </xsl:if>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="6%" nowrap="">
        <xsl:choose>
          <xsl:when test="(@DocumentTypeID = '3' or @DocumentTypeID = '10') and @FullSummaryExistsFlag = 1">
            <div style="width:24px;cursor:hand;text-align:left">
              <xsl:attribute name="onClick">ShowEstWindow(this.parentElement.parentElement, true)</xsl:attribute>
              <!-- doc_single -->
              <img unselectable="on" src="/images/ElectronicEst.gif" border="0" width1="24" height1="14"
                title="Electronic Summary. Click to open full summary screen." style="FILTER1:alpha(opacity=60);"
                onmouseout1="makeHighlight(this,1,60)" onmouseover1="makeHighlight(this,0,100)">
              </img>
            </div>
          </xsl:when>
          <xsl:when test="(@DocumentTypeID = '3' or @DocumentTypeID = '10') and @FullSummaryExistsFlag = 0">
            <div style="width:24px;cursor:hand;text-align:left">
              <xsl:attribute name="onClick">ShowEstWindow(this.parentElement.parentElement, false)</xsl:attribute>
              <img unselectable="on" src="/images/ManualEst.gif" border="0" width1="15" height1="15"
                title="Manual Summary. Click to open quick summary screen." style="FILTER1:alpha(opacity=60); cursor:hand;"
                onmouseout1="makeHighlight(this,1,60)" onmouseover1="makeHighlight(this,0,100)">
              </img>
            </div>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="10%" nowrap="" IDIndex="14">
        <xsl:if test="/Root/Reference[@List='DocumentSource' and @ReferenceID=current()/@DocumentSourceID]/@VANFlag='0'">
          <xsl:attribute name="ondblclick">EditableCellDblClick(this,3,1,'DocumentSource')</xsl:attribute>
          <xsl:attribute name="onmouseover">EditableCellOver(this)</xsl:attribute>
          <xsl:attribute name="onmouseout">EditableCellOut(this)</xsl:attribute>
          <xsl:attribute name="style">cursor:text</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="/Root/Reference[@List='DocumentSource' and @ReferenceID=current()/@DocumentSourceID]/@Name"/>
        <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="10%" nowrap="">
        <xsl:choose>
          <xsl:when test="@DocumentTypeID = '3' or @DocumentTypeID = '10'">
            <xsl:if test="@EstimateTypeCD = 'A'">Audited</xsl:if>
            <xsl:if test="@EstimateTypeCD = 'O'">Original</xsl:if>
            <xsl:if test="@EstimateTypeCD = ''">
              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            </xsl:if>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="8%" nowrap="">
        <xsl:choose>
          <xsl:when test="@DocumentTypeID = '3' or @DocumentTypeID = '10'">
            <xsl:if test="@DuplicateFlag = '1'">Yes</xsl:if>
            <xsl:if test="@DuplicateFlag = '0'">No</xsl:if>
            <xsl:if test="@DuplicateFlag = ''">
              <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            </xsl:if>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="15%" nowrap="" >
        <xsl:value-of select="@CreatedUserNameLast"/>
        <xsl:if test="@CreatedUserNameLast != 'SYSTEM'">
          <xsl:text disable-output-escaping="yes">,</xsl:text>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <xsl:value-of select="substring(@CreatedUserNameFirst,1,1)"/>
          <xsl:text disable-output-escaping="yes">.</xsl:text>
        </xsl:if>
        <!-- - <xsl:value-of select="@CreatedUserRoleName"/> -->
      </TD>
      <TD unselectable="on" class="GridTypeTD" width="80px" nowrap="">
        <xsl:variable name="chkImageDate" select="@CreatedDate"/>
        <xsl:choose>
          <xsl:when test="contains($chkImageDate, '1900')">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="js:UTCConvertDate(string(current()/@CreatedDate))" />
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:value-of select="js:UTCConvertTime(string(current()/@CreatedDate))"/>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <TD unselectable="on" class="GridTypeTD"  nowrap="" style="display:none">
        <input type="checkbox" name="chkDelete" value="1" onclick="delChkChange(this)">
          <xsl:if test="not(contains($myCRUD, 'D')) or $myClaimStatus = 'Claim Cancelled' or $myClaimStatus = 'Claim Voided'">
            <xsl:attribute name="disabled"/>
            <xsl:attribute name="readOnly"/>
          </xsl:if>
        </input>
      </TD>
      <TD style="display:none">
        <xsl:value-of select="@DocumentID"/>
      </TD>
      <!-- table index 13 idxDocumentID -->
      <TD style="display:none">
        <xsl:value-of select="@DocumentSourceID"/>
      </TD>
      <!-- table index 14 idxDocumentSourceID -->
      <TD style="display:none">
        <xsl:value-of select="@DocumentTypeID"/>
      </TD>
      <!-- table index 15 idxDocumentTypeID -->
      <TD style="display:none">
        <xsl:value-of select="@ClaimAspectID"/>
      </TD>
      <!-- table index 16 idxClaimAspectID -->
      <TD style="display:none">
        <xsl:value-of select="@SysLastUpdatedDate"/>
      </TD>
      <!-- table index 17 idxSysLastUpdatedDate -->
      <TD style="display:none">
        <xsl:value-of select="@DirectionalCD"/>
      </TD>
      <!-- table index 18 idxDirectionalCD -->
      <TD style="display:none">
        <xsl:value-of select="@ClaimAspectID"/>
      </TD>
      <!-- table index 19 idxOrgClaimAspectID, need for update -->
      <TD style="display:none">
        <xsl:value-of select="@AssignmentID"/>
      </TD>
      <!-- table index 20 idxAssignmentID -->
      <TD style="display:none">
        <xsl:value-of select="@ImageLocation"/>
      </TD>
      <!-- table index 21 idxImageLocation -->
      <TD style="display:none">
        <xsl:value-of select="@DirectionToPayFlag"/>
      </TD>
      <!-- table index 22 idxDirectionToPayFlag -->
      <TD style="display:none">
        <xsl:value-of select="@FinalEstimateFlag"/>
      </TD>
      <!-- table index 23 idxFinalEstimateFlag -->
      <TD style="display:none">
        <xsl:value-of select="/Root/@LynxID"/>
      </TD>
      <!-- table index 24 idxLynxID -->
      <TD style="display:none">
        <xsl:value-of select="/Root/Reference[@List='DocumentSource' and @ReferenceID=current()/@DocumentSourceID]/@VANFlag"/>
      </TD>
      <!-- table index 25 idxVANFlag -->
      <TD style="display:none">
        <xsl:value-of select="@SupplementSeqNumber" />
      </TD>
      <!-- table index 26 idxSuppSeqNum -->
      <TD style="display:none">
        <xsl:value-of select="@ClaimAspectServiceChannelID"/>
      </TD>
      <!-- table index 27 idxClaimAspectServiceChannelID -->
      <TD style="display:none">
        <xsl:value-of select="@ClaimAspectServiceChannelID"/>
      </TD>
      <!-- table index 28 idxOrgClaimAspectServiceChannelID -->
      <TD style="display:none">
        <xsl:value-of select="@ApprovedFlag"/>
      </TD>
      <!-- table index 29 idxApprovedFlag -->
      <TD style="display:none">
        <xsl:value-of select="@ApprovedDate"/>
      </TD>
      <!-- table index 30 idxApprovedDate -->
      <TD style="display:none">
        <xsl:value-of select="@WarrantyFlag"/>
      </TD>
      <!-- table index 31 idxWarrantyFlag -->
      <TD style="display:none">
        <xsl:value-of select="@FinalBillEstimateFlag"/>
      </TD>
      <!-- table index 32 idxFBEFlag -->
      <TD style="display:none">
        <xsl:value-of select="@WarrantyExistsFlag"/>
      </TD>
      <!-- table index 33 idxWarrantyExistsFlag -->
      <TD style="display:none">
        <xsl:value-of select="@BilledFlag"/>
      </TD>
      <!-- table index 34 idxBilledFlag -->
      <TD style="display:none">
        <xsl:value-of select="@AgreedPriceMetCD"/>
      </TD>
      <!-- table index 35 idxAgreedPriceMetCD -->
      <TD style="display:none">
        <xsl:value-of select="@EstimateNetAmt"/>
      </TD>
      <!-- table index 36 idxEstimateNetAmt -->
    </TR>
  </xsl:template>

</xsl:stylesheet>
