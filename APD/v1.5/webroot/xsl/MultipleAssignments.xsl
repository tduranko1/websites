<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:IE="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    id="MultiAssign">

<xsl:import href="msxsl/msxsl-function-library.xsl"/>

<xsl:output method="html" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">
  
<HTML>
<!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
<!-- APDXSL:C:\websites\apd\v1.3\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},11,uspMultipleAssignGetDetailXML,MultiAssign.xsl, '6611'   -->
<HEAD>

<TITLE>Multiple Assignments</TITLE>

<!-- STYLES -->
<LINK rel="stylesheet" href="/css/apdcontrols.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdgrid.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdmain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdselect.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdwindow.css" type="text/css"/>
  
<SCRIPT language="JavaScript" src="/js/apdcontrols.js"></SCRIPT> 
<script language="JavaScript" src="js/datetime.js"></script>
<script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script> 		
<script type="text/javascript"> 
          document.onhelp=function(){  
		  RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
		  event.returnValue=false;
		  };	
</script>

<SCRIPT language="JavaScript">

<![CDATA[

	function EditableCellOver(obj)
	{
			editbg = obj.style.backgroundColor;
			obj.style.backgroundColor = "#CCCCCC";
	}
		
	function EditableCellOut(obj)
	{
			obj.style.backgroundColor = editbg;
	}
]]>

</SCRIPT>

</HEAD>

<BODY unselectable="on" style="background-color:#FFFFFF;overflow:hidden;border:0px;padding:1px;" tabIndex="-1" topmargin="0"  leftmargin="0">
<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;background-color:#067BAF;width:100%;height:100%;" >
<tr style="padding:1px;height:21px;">
<td>
<table width="100%" class="ClaimMiscInputs" cellspacing="0" border="0" cellpadding="1"
        style="font-family: Tahoma, Arial, Helvetica, sans-serif; font-size:10px; text-align:center; table-layout:fixed"  oncontextmenu="return false">
     <tbody>
          <tr class="QueueHeader" style="height:20px">
		     <td class="clWinGridTypeHeader" width="100px"  type="String" id="disSelectionDate">Selection<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Date</td>
             <td class="clWinGridTypeHeader" width="100px"  type="String" id="disAssignmentDate">Assignment<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Date</td>
             <td class="clWinGridTypeHeader" width="100px"  type="String" id="disCancellationDate">Cancellation<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Date</td>
             <td class="clWinGridTypeHeader" width="200px"  type="String" id="disShopName">Appraiser<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Name</td>
			 <td class="clWinGridTypeHeader" width="100px"  type="string" id="disAssignType">Type</td>
             <td class="clWinGridTypeTD" align="left" style="display:none"/>
          </tr>
     </tbody>
</table>       
</td>
</tr>
<tr style="padding:1px;height:*;">
 <td>
  <div style="width:100%;height:100%;overflow:hidden;overflow:auto;background-color:#FFFFFF" class="clScrollTable">
          <table id="tblSortCheckList" class="clWinGridTypeTable" width="100%" cellspacing="0" border="0" cellpadding="1" style="table-layout:fixed"> 
		   <tbody dataRows="1" bgColor1="fff7e5" bgColor2="ffffff">		
		     <col width="100px"/>    			
             <col width="100px"/>
             <col width="100px"/>
	  		 <col width="200px"/>
			 <col width="100px"/>
		     <col width="0" style="display:none"/>
			<xsl:for-each select="/Root/Assignments">
			    <xsl:call-template name="AssignTask"> 
         	    </xsl:call-template>
	        </xsl:for-each>			 
       </tbody>
    </table>
   </div>
 </td>
 </tr>
</table>

</BODY>
</HTML>
</xsl:template>

 <xsl:template name="AssignTask">
    <tr unselectable="on" style="cursor:default"> 
       <xsl:attribute name="bgColor"><xsl:value-of select="user:chooseBackgroundColor(position(),'#ffffff','#fff7e5')"/></xsl:attribute>
	   <xsl:attribute name="onmouseover">EditableCellOver(this)</xsl:attribute>
       <xsl:attribute name="onmouseout">EditableCellOut(this)</xsl:attribute>   
	<td id="selSelectionDate" class="clWinGridTypeTD" align="center">
    	<xsl:value-of select="user:extractSQLDateTime(string(@SelectionDate))"/>
	</td> 	   
	<td id="selAssignmentDate" class="clWinGridTypeTD" align="center">
    <xsl:choose>
	    <xsl:when test="string(@AssignmentDate)=''">
		         <xsl:text disable-output-escaping="yes">Never Sent</xsl:text> 
		</xsl:when>
       <xsl:otherwise>
  	             <xsl:value-of select="user:extractSQLDateTime(string(@AssignmentDate))"/>
       </xsl:otherwise>
    </xsl:choose>
	</td> 
	<td id="selCancellationDate" class="clWinGridTypeTD" align="center">
        <xsl:value-of select="user:extractSQLDateTime(string(@CancellationDate))"/>
	</td>
	<td id="selShopName" class="clWinGridTypeTD" align="center">
	   <xsl:choose>
        <xsl:when test="@AssignmentTypeCD = 'LDAU'">LYNX Desk Audit</xsl:when>
        <xsl:otherwise><xsl:value-of select="@AppraiserName"/></xsl:otherwise>
      </xsl:choose>
	</td>
	<td id="selAssignType" class="clWinGridTypeTD" align="center">
       <xsl:choose>
        <xsl:when test="@AssignmentTypeCD = 'LDAU'">LYNX DA</xsl:when>
        <xsl:otherwise><xsl:value-of select="@AssignmentTypeCD"/></xsl:otherwise>
      </xsl:choose>
	</td>
  </tr>
   
 </xsl:template> 
 
</xsl:stylesheet>