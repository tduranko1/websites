<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ms="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:js="urn:the-xml-files:xslt"
    xmlns:session="http://lynx.apd/session-manager"
    id="ApdSelect">

  <xsl:import href="msxsl/generic-template-library.xsl"/>
  <xsl:import href="msxsl/msjs-client-library.js"/>
  <xsl:output method="html" indent="yes" encoding="UTF-8"/>

  <!-- Permissions params - names must end in 'CRUD'-->
  <xsl:param name="NoteCRUD" select="Note"/>
  <xsl:param name="TaskCRUD" select="Task"/>

  <xsl:param name="TransferClaimCRUD" select="Transfer Claim Ownership"/>
  <xsl:param name="CloseExpCRUD" select="Action:Close Exposure"/>

  <!-- XslParams - stuffed from COM before transform -->
  <xsl:param name="WindowID"/>
  <xsl:param name="LynxID"/>
  <xsl:param name="Entity"/>
  <xsl:param name="Context"/>
  <xsl:param name="EntityContext"/>
  <xsl:param name="Environment"/>
  <xsl:param name="ClaimView"/>
  <xsl:param name="VehNum"/>

  <xsl:include href="APDMenu.xsl"/>

  <!--            APP _PATH                            SessionKey                   USERID     SP             XSL        PARAMETERS TO SP     -->
  <!-- APDXSL:C:\websites\apd\v1.1.0\webroot\,{DC4C7718-7677-4620-88CF-01DE38532F6F},33,uspSessionGetUserDetailXML,ApdSelect.xsl, csr0901   -->
  <xsl:template match="/Root">

    <!-- Initialize the session manager -->
    <xsl:variable name="Context" select="session:XslUpdateSession('Context','')"/>
    <xsl:variable name="ViewCRD" select="session:XslUpdateSession('ViewCRD', string(User/DesktopPermission/@CRD))"/>
    <xsl:variable name="ViewPMD" select="session:XslUpdateSession('ViewPMD', string(User/DesktopPermission/@PMD))"/>
    <xsl:variable name="ViewUAD" select="session:XslUpdateSession('ViewUAD', string(User/DesktopPermission/@UAD))"/>
    <xsl:variable name="ViewMED" select="session:XslUpdateSession('ViewMED', string(User/DesktopPermission/@MED))"/>
    <xsl:variable name="ViewTLQ" select="session:XslUpdateSession('ViewTLQ', string(User/DesktopPermission/@TLQ))"/>

    <!-- Make sure we have a valid user -->
    <xsl:if test="count(User)=1 and User/DesktopPermission/@CRD=1">

      <xsl:variable name="CsrNo" select="@CsrNo"/>
      <xsl:variable name="UserID" select="User/@UserID"/>
      <xsl:variable name="NameFirst" select="User/@NameFirst"/>
      <xsl:variable name="NameLast" select="User/@NameLast"/>
      <xsl:variable name="NameTitle" select="User/@NameTitle"/>
      <xsl:variable name="SupervisorUserID" select="User/@SupervisorUserID"/>
      <xsl:variable name="SuperUserFlag" select="User/@SuperUserFlag"/>
      <xsl:variable name="SupervisorFlag" select="User/@SupervisorFlag"/>
      <xsl:variable name="RoleID" select="User/Role[@PrimaryRoleFlag = '1']/@RoleID"/>
      <xsl:variable name="RoleName" select="User/Role[@PrimaryRoleFlag = '1']/@Name"/>

      <xsl:value-of select="session:XslUpdateSession( 'UserID', string($UserID) )"/>
      <xsl:value-of select="session:XslUpdateSession( 'UserRole', string($RoleName) )"/>
      <xsl:value-of select="session:XslUpdateSession( 'UserRoleID', string($RoleID) )"/>
      <xsl:value-of select="session:XslUpdateSession( 'SupervisorUserID', string($SupervisorUserID) )"/>
      <xsl:value-of select="session:XslUpdateSession( 'SupervisorFlag', string($SupervisorFlag) )"/>

      <xsl:variable name="vehNum2">
        <xsl:choose>
          <xsl:when test="$EntityContext != ''">
            <xsl:value-of select="$EntityContext"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$VehNum"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <HTML>
        <HEAD>

          <TITLE>
            APD
            <xsl:choose>
              <xsl:when test="$LynxID=''"> - Desktop - </xsl:when>
              <xsl:otherwise>
                - <xsl:value-of select="$LynxID"/> -
              </xsl:otherwise>
            </xsl:choose>
            <xsl:value-of select="$Environment"/>
          </TITLE>

          <LINK rel="stylesheet" href="/css/apdTopMain.css" type="text/css"/>
          <LINK rel="stylesheet" href="/css/apdframe.css" type="text/css"/>
          <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
          <LINK rel="stylesheet" href="/css/Window.css" type="text/css"/>
          <LINK rel="stylesheet" href="/css/officexp/Menu.css" type="text/css" id="menuStyleSheet"/>

          <SCRIPT language="JavaScript" src="js/images.js"></SCRIPT>
          <SCRIPT language="JavaScript" src="js/browser.js"></SCRIPT>
          <SCRIPT language="JavaScript" src="/js/windowUtils.js"></SCRIPT>
          <SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
          <SCRIPT language="JavaScript" src="/js/tablesort.js"></SCRIPT>
          <SCRIPT language="JavaScript" src="/js/menu3.js"></SCRIPT>
          <SCRIPT language="JavaScript" src="/js/fade.js"></SCRIPT>
          <SCRIPT language="JavaScript" src="/js/ClaimNavigate.js"></SCRIPT>
          <SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>
          <script type="text/javascript" language="JavaScript1.2"  src="webhelp/RoboHelp_CSH.js"></script>
          <script type="text/javascript">
            document.onhelp=function(){
            RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,34);
            event.returnValue=false;
            };
          </script>

          <SCRIPT LANGUAGE="JavaScript">

            var vWindowInitTimer = "uninitialized";
            var vUserID = "<xsl:value-of select='string($UserID)'/>";
            var vLynxID = "<xsl:value-of select='string($LynxID)'/>";
            var vInsuranceCompanyID;
            var vWindowID = "<xsl:value-of select='string($WindowID)'/>";
            var sClaimView = "<xsl:value-of select='$ClaimView'/>";
            var bClaimRepDesktop = ( ( vLynxID == "" ) ? true : false );
            var sClaim_ClaimAspectID = null;
            var sVehicleClaimAspectID = null;
            var sVehicleName = "";
            var sVehicleNum = 1;
            var sPropertyClaimAspectID = null;
            var sPropertyName = "";
            var blnSupervisorFlag = "<xsl:value-of select="$SupervisorFlag"/>";

            // Array of opened windows for this claim.
            // Need to track opended non-modal windows in order to close them when the parent window is closed.
            var gAryOpenedWindows = new Array();

            //CRUDs
            var sTransferClaimCRUD = "<xsl:value-of select="$TransferClaimCRUD"/>";
            var sCloseExpCRUD = '<xsl:value-of select="$CloseExpCRUD"/>';
            var strEnvironment = "<xsl:value-of select="$Environment"/>";

            // then tab number currently being used
            var gsTabNum = 0;
            var gsCountNotes = 0;
            var gsCountTasks = 0;
            var gsCountBilling = 0;
            var gsEntityCRUD = "";
            var gbDiaryLocked = false;
            var gsCoverageLimitWarningInd = "99"; //default to something
            var strCurrentClaimAspectServiceChannelID = "";
            var strCurrentServiceChannelName = "";
            var strDispositionTypeCD = "";
            var strServiceChannelCD = "";
            var strPreOpenVehTab = "";
            var strPreOpenVehSCTab = "";
            var blnInitialLoad = true;
            var bIsLogUpdate = false;

            <xsl:choose>
              <xsl:when test="$ClaimView='cond' or $ClaimView='condnew'">
                preload('MainTabSel1','images/claimxp_sel.gif')
                preload('MainTabSel2','images/veh_sel.gif')
                preload('MainTabSel3','images/prop_sel.gif')
                preload('MainTabSel4','images/doc_sel.gif')
                //  preload('MainTabSel4','images/siu_sel.gif')
                //  preload('MainTabSel5','images/subro_sel.gif')
                //  preload('MainTabSel6','images/rental_sel.gif')
                preload('MainTabDes1','images/claimxp_des.gif')
                preload('MainTabDes2','images/veh_des.gif')
                preload('MainTabDes3','images/prop_des.gif')
                preload('MainTabDes4','images/doc_des.gif')
                //  preload('MainTabDes4','images/siu_des.gif')
                //  preload('MainTabDes5','images/subro_des.gif')
                //  preload('MainTabDes6','images/rental_des.gif')
              </xsl:when>
              <xsl:otherwise>
                preload('MainTabSel1','images/claim_sel.gif')
                preload('MainTabSel2','images/veh_sel.gif')
                preload('MainTabSel3','images/prop_sel.gif')
                preload('MainTabSel4','images/doc_sel.gif')
                //  preload('MainTabSel4','images/siu_sel.gif')
                //  preload('MainTabSel5','images/subro_sel.gif')
                //  preload('MainTabSel6','images/rental_sel.gif')
                preload('MainTabDes1','images/claim_des.gif')
                preload('MainTabDes2','images/veh_des.gif')
                preload('MainTabDes3','images/prop_des.gif')
                preload('MainTabDes4','images/doc_des.gif')
                //  preload('MainTabDes4','images/siu_des.gif')
                //  preload('MainTabDes5','images/subro_des.gif')
                //  preload('MainTabDes6','images/rental_des.gif')
              </xsl:otherwise>
            </xsl:choose>

            <![CDATA[
  
  function SetCurrentClaimAspectId(CurrentClaimAspectID){
	 sVehicleClaimAspectID = CurrentClaimAspectID;
  }

  function InitPage()
  {
     try {
	self.resizeTo("1050", "800");
     } catch (e) {
         window.setTimeout("resizeWindow()", 1000);
     }
     // Overrid to refresh notes and diary when focus comes.
     window.onfocus = DocOnFocus;

     fixSize();  // menu sizing
     logAPDScreenEvent(); //logging APD_MIG
     //set the default Claim View to the standard mode (ClaimsXP)
     if (sClaimView == "") {
        sClaimView = "cond"
        document.cookie = "ClaimView=" + sClaimView;
     }

     vWindowInitTimer = window.setInterval( "initSubWindows()", 1000 );
     setSB( 100, sb ); // Status bar at 100
	
	//Adding app_variable code
	 var strFunctionName = "GetApplicationVar";
            var strDataToSend = "?VariableName=APDNET_Flag";
            ApdNETHttpGet(strFunctionName, strDataToSend);
	
  }
  
   function ApdNETHttpGet(strFunctionName, strDataToSend) {
   try{
           var strServiceURL = GetApdWebServiceURL()
           var getURL = strServiceURL + "/" + strFunctionName + strDataToSend;
            var objXMLHTTP = new ActiveXObject("MSXML2.XMLHTTP");
            //dataToSend = "StoredProcedure=uspWorkflowSendShopAssignWSXML" + "&Parameters=" + strAssignmentID;
            objXMLHTTP.open("GET", getURL, false);
            objXMLHTTP.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            objXMLHTTP.send();
			
			  if (objXMLHTTP.responseXML.text == "True" || objXMLHTTP.responseXML.text == "TRUE")
			  {
			  if(document.getElementById("APDNET_Image") != null)
                document.getElementById("APDNET_Image").style.visibility = "visible";
				}
            else{
			if(document.getElementById("APDNET_Image") != null)
                document.getElementById("APDNET_Image").style.visibility = "hidden";
				}
        }
				catch(e){
				//if(e.message == "Permission denied"){
				//alert("Please Enable *Access data sources across domains* for avoiding the security issues. \n\n(internet option -> Security -> Trusted sites -> Custom level -> Miscellaneous -> Access data sources across domains)");
				//}
				}
        }

        function GetApdWebServiceURL() {
            var strmexMonikerString;
            switch (strEnvironment) {
                case "PGW_Development":
                    strmexMonikerString = "http://dlynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"
                    break;
                case "PGW_STAGE":
                    strmexMonikerString = "http://slynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"
                    break;
                case "PGW_PROD":
                   strmexMonikerString = "http://lynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"
                    break;
            }

            return strmexMonikerString;
        }
		
function logAPDScreenEvent(){
  //debugger;
  var sProc, sRequest;
  var sEventDetailedDescription = "";
  var sEventStatus = "";
  var sEventDescription = "LynxID="+vLynxID+" and VehicleNum="+sVehicleNum+" And UserID="+vUserID;
  var sEventXML = "ClaimView=" + sClaimView;
  var sEntity = getUrlParameter('Entity');
  var sContext = getUrlParameter('Context');
  
  if(sClaimView != "" && vLynxID !="" && bIsLogUpdate == false && sEntity.indexOf("claim") != -1 & sContext !=""){
   if(sClaimView == "cond"){
   sEventStatus = "APD";
   sEventDetailedDescription = "User open the old screen";
   }
   else if(sClaimView = "condnew"){
   sEventStatus = "APDNET";
   sEventDetailedDescription = "User open the new screen";
   }
          logEvent("APD_MIG", sEventStatus, sEventDescription, sEventDetailedDescription, sEventXML);
              bIsLogUpdate = true;
             }
  }
  
   function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}	
  
		
  function resizeWindow(){
     try {
      self.resizeTo("1050", "800");
     } catch (e) {}
  }


  // Overrid to refresh notes and diary when focus comes.
  var gbInDocOnFocus = false;
  function DocOnFocus()
  {
      // Prevent multiple entry.
      if ( !gbInDocOnFocus )
      {
          gbInDocOnFocus = true;

          try
          {
              // The following statement will throw if undefined.
              // This allows us to hold off on filling the sub windows
              // until we know for sure that remote scripting is ready.
              if ( MSRS != null )
              {
                  // Claims rep desktop.
                  if ( bClaimRepDesktop == true )
                  {
                      try {
                        //refresh the active tab (New/Pending).
                        if (document.frames["oIframe"].content11.style.visibility == "visible") {
                          document.frames["oIframe"].RefreshNewClaims();
                        } else if (document.frames["oIframe"].content12.style.visibility == "visible") {
                          document.frames["oIframe"].RefreshPendingClaims();
                        }
                      } catch (e) {}
                      refreshNotesWindow( "0", vUserID );
                      refreshCheckListWindow( "0", vUserID );
                  }
                  // Claim window.
                  else
                  {
                      refreshNotesWindow( vLynxID, vUserID );
                      refreshCheckListWindow( vLynxID, vUserID );
                  }
              }
          }
          catch ( e ) {}

          gbInDocOnFocus = false;
      }
  }

  function displayCoverageLimitWarining(bShow){
    var oImg = document.getElementById("LimitIndicator");
    if (oImg) {
      oImg.style.display = (bShow == true ? "inline" : "none");
    }
  }
  
  function setClaimHeader(RepFname, RepLname, LynxID, ClaimID, InsFname, InsLname, Loss, InsCo, RestrictFlag, ClaimOpen, ClaimStatus, InsBusiness, DemoFlag, InsuranceCompanyID)
  {
    //vInsuranceCompanyID param gets updated for the File Upload dialogue
    vInsuranceCompanyID = InsuranceCompanyID;
    layerClaimVoid.style.display = "none";

    if (RestrictFlag == 1)
      strClaimStatusImg = "<IMG unselectable='on' src='/images/claim_restricted.gif' alt='Claim is Restricted' width='40' height='28' border='0' />";
    else if (ClaimOpen == 0) {
      strClaimStatusImg = "<IMG unselectable='on' src='/images/claim_closed.gif' alt='Claim is Closed' width='40' height='28' border='0' />";
    } else {
      strClaimStatusImg = "<IMG unselectable='on' src='/images/spacer.gif' alt='' width1='40' height1='28' border='0' />";
    }

    if (ClaimStatus == "Claim Cancelled") {
      strClaimStatusImg = "<IMG unselectable='on' src='/images/claim_cancel.gif' alt='Claim is Cancelled' width='40' height='28' border='0' />";
    } else if (ClaimStatus == "Claim Voided") {
      strClaimStatusImg = "<IMG unselectable='on' src='/images/spacer.gif' alt='' width1='40' height1='28' border='0' />";
      layerClaimVoid.style.display = "inline";
    } else if (gsCoverageLimitWarningInd == "1"){
      displayCoverageLimitWarining(true);
      //strClaimStatusImg = "<IMG unselectable='on' src='/images/exclamation.gif' alt='' width1='40' height1='28' border='0' title='This claim has coverage limits.'/>";
    }
    //  strClaimStatusImg = "<IMG unselectable='on' src='/images/claim_void.gif' alt='Claim is Voided' width='40' height='28' border='0' />";


    document.getElementById("layerClaimData").style.visibility = "visible";
    document.getElementById("ClaimStatusImg").innerHTML = strClaimStatusImg;
    //document.getElementById("tdClaimRep").innerText = RepFname +" "+ RepLname;
    document.getElementById("tdLynxID").innerText = LynxID;
    
    if (DemoFlag == "1") {
      document.getElementById("DemoClaim").style.display = "inline";
    } else {
      document.getElementById("DemoClaim").style.display = "none";
    }

    if (ClaimID.length > 22)
      ClaimID = ClaimID.substr(0,22) + "...";

    document.getElementById("tdClaimID").innerText = ClaimID;

    var Insured = InsFname + " " + InsLname;
    if (InsBusiness)
    {
      Insured = InsBusiness;
      if (Insured.length > 21)
        Insured = Insured.substr(0,21) + "...";
    }
    else
    {
      if (Insured.length > 21)
        if (InsLname.length > 19)
          Insured = InsFname.substr(0,1) + ". " + InsLname.substr(0,19) + "...";
        else
          Insured = InsFname.substr(0,1) + ". " + InsLname;
    }

    document.getElementById("tdInsured").innerText = Insured;
    document.getElementById("tdLoss").innerText = Loss;

    if (InsCo.length > 25) InsCo = InsCo.substr(0,25);
    document.getElementById("tdInsCo").innerText = InsCo;

    document.getElementById("layerNoTabs").style.visibility = "hidden";
    document.getElementById("layerTabs").style.visibility = "visible";

    if ( winNotes && winNotes.sWindowState == "min")
        window_columnEqual();

    // set default menu items
    ClaimsMenu(true,1);
//    ClaimsMenu(true,2);
//    if (sTransferClaimCRUD.indexOf("U") != -1)
//      ClaimsMenu(true,3);
//    ClaimsMenu(true,5);
//    ClaimsMenu(true,6);
    if (ClaimStatus == "Claim Closed" ||
        ClaimStatus == "Claim Cancelled" ||
        ClaimStatus == "Claim Voided") {
//      ClaimsMenu(false,5); // Close Exposure
//      ClaimsMenu(false,8); // Cancel Claim
//      ClaimsMenu(false,9); // Void Claim
      FinancialMenu(true,1);   // Billing Info
      FinancialMenu(true,2);   // Journal
      if (ClaimStatus == "Claim Voided"){
        ClaimsMenu(false,3);
//        ClaimsMenu(false,5);
//        ClaimsMenu(false,6);
        //FinancialMenu(false,1);  // Billing Info
      }
      if (ClaimStatus == "Claim Cancelled"){
//        ClaimsMenu(false,5);
        //FinancialMenu(false,1);  // Billing Info
      }
    } else {
//      ClaimsMenu(true,8); // Cancel Claim
//      ClaimsMenu(true,9); // Void Claim
      FinancialMenu(true,1);  // Billing Info
      FinancialMenu(true,2);   // Journal
    }
    VehiclesMenu((sClaimView == "exp"),1);
    if (sClaimView == "cond" || sClaimView == "condnew"){
      VehiclesMenu(true,2);
      VehiclesMenu(true,3);
    }
    if (sClaimView == "exp")
      PropertiesMenu(true,1);
    DocumentsMenu(true,1);
    DocumentsMenu(true,2);
    DocumentsMenu(true,3);
    DocumentsMenu(true,4);
    DocumentsMenu(true,5);
  }

  function closeClaim()
  {
    document.getElementById("layerClaimData").style.visibility = "hidden";
    document.getElementById("layerTabs").style.visibility = "hidden";
    document.getElementById("layerNoTabs").style.visibility = "visible";
    layerClaimVoid.style.display = "none";
    gsTabNum = 0;

    refreshNotesWindow( "0", vUserID );
    refreshCheckListWindow( "0", vUserID );
    window_columnMax( "winDiary" );

    // gray out the menu items
    ClaimsMenu(false,1);
    ClaimsMenu(false,2);
    ClaimsMenu(false,3);
    ClaimsMenu(false,5);
    ClaimsMenu(false,6);
    ClaimsMenu(false,8);
    ClaimsMenu(false,9);
    VehiclesMenu(false,1);
    VehiclesMenu(false,2);
    VehiclesMenu(false,3);
    if (sClaimView == "exp") {
      PropertiesMenu(false,1);
      PropertiesMenu(false,2);
      PropertiesMenu(false,3);
    }
    DocumentsMenu(false,1);
    DocumentsMenu(false,2);
    DocumentsMenu(false,3);
    FinancialMenu(false,1);
    FinancialMenu(false,2);
  }

  function HideMainTabs()
  {
      document.getElementById("layerTabs").style.visibility = "hidden";
      document.getElementById("layerNoTabs").style.visibility = "visible";
  }

  // for Vehicle and Property need to make sure we are looking a them
  function VehicleDelete()
  {
    if (sClaimView == "exp") {
      if (gsTabNum == 2)
        document.frames["oIframe"].VehicleDelete();
      else
        ClientWarning("Not currently viewing a Vehicle!");
    } else {
      var sURL = document.frames["oIframe"].document.URL;
      if (sURL.indexOf("ClaimXP.asp") != -1)
        document.frames["oIframe"].VehicleDelete();
      else
        ClientWarning("Cannot delete vehicle from this screen. Please navigate to Claim view and try again.");
    }
  }

  function PropertyDelete()
  {
    try {
      if (gsTabNum == 3)
        document.frames["oIframe"].PropertyDelete();
      else
        ClientWarning("Not currently viewing a Property!");
    } catch (e) {}
  }

  function initSubWindows()
  {
      try
      {
          // The following statement will throw if undefined.
          // This allows us to hold off on filling the sub windows
          // until we know for sure that remote scripting is ready.
          if ( MSRS != null )
          {
              window.clearInterval( vWindowInitTimer );
              vWindowInitTimer = "";

              // Claims rep desktop.
              if ( bClaimRepDesktop == true )
              {
                  refreshNotesWindow( "0", vUserID );
                  refreshCheckListWindow( "0", vUserID );
                  window_columnMax("winDiary");
              }
              // Claim window.
              else
              {
                  refreshNotesWindow( vLynxID, vUserID );
                  refreshCheckListWindow( vLynxID, vUserID );
                  window_columnEqual();
              }
          }
      }
      catch ( e ) {}
  }

  // Used in logic below to reduce multiple loads at initial page build.
  var gbLoadingCheckList = false;
  var gbLoadingNotes = false;

  // Rebuilds the diary window.
  function refreshCheckListWindow(sLynxID, sUserID)
  {
      // Wrapped to ensure following command always executes.
      try
      {
          // Logic to reduce multiple loads at initial page build.
          if ( vWindowInitTimer == "" && !gbLoadingCheckList )
          {
              gbLoadingCheckList = true;
              gbDiaryLocked = true;
              if ( winDiary )
                  winDiary.loadContent("/APDDiary.asp?WindowID=" + vWindowID + "&LynxID=" + sLynxID + "&UserID=" + sUserID + "&SubordinateUserID=" + (top.SubordinateUserID == undefined ? "" : top.SubordinateUserID) + "&SubordinatesFlag=" + (top.SubordinatesFlag == undefined ? "" : top.SubordinatesFlag));
          }
      } catch ( e ) { }

      gbLoadingCheckList = false;
  }

  // Rebuilds the notes window.
  function refreshNotesWindow(sLynxID, sUserID)
  {
      // Wrapped to ensure following command always executes.
      try
      {
          // Logic to reduce multiple loads at initial page build.
          if ( vWindowInitTimer == "" && !gbLoadingNotes )
          {
              gbLoadingNotes = true;
              if ( winNotes )
                  winNotes.loadContent("/APDNotes.asp?WindowID=" + vWindowID + "&LynxID=" + sLynxID + "&UserID=" + sUserID);
          }
      } catch ( e ) { }

      gbLoadingNotes = false;
  }

  preload('WindowTool1','images/smnew2.gif')
  preload('WindowTool2','images/smnew.gif')
  preload('WindowTool3','images/smrefresh.gif')
  preload('WindowTool4','images/smsend.gif')
  preload('WindowTool5','images/smsearch.gif')

  preload('WindowToolOn1','images/smnew2_on.gif')
  preload('WindowToolOn2','images/smnew_on.gif')
  preload('WindowToolOn3','images/smrefresh_on.gif')
  preload('WindowToolOn4','images/smsend_on.gif')
  preload('WindowToolOn5','images/smsearch_on.gif')

  preload('WindowRefresh','images/window_refresh_clock.jpg')


  function ResetMainTabs()
  {
    for(var x=1; x <= 4; x++)
    {
      changeImage(null, "imgMainTab"+x, "MainTabDes"+x)
      str = "imgMainTabOverlap" + x;
      document.images[str].src = "/images/folder_Bottom.gif";
    }
  }

  function SetMainTab(num, sPage)
  {
    try { // need this here if changing tabs WHILE another page is loading -- i.e. mis-use
      if (sPage)
        document.frames["oIframe"].ParentMainTabChange();
    }catch(e) { }

    ResetMainTabs();
    changeImage(null, "imgMainTab" + num, "MainTabSel"+num)
    document.images["imgMainTabOverlap" + num].src = "/images/spacer.gif"
    gsTabNum = num;
    
    var sContext = "";
    
    try {
      sContext = top.sVehicleNum;
    } catch (e) {}

    if (sPage)
      document.frames["oIframe"].frameElement.src = sPage + "?WindowID=" + vWindowID + "&VehNum=" + sContext;
  }

  // StateChange event callback for the IFrame to show status bar
  function IFStateChange(obj)
  {
    if (obj.readyState == "loading")
      setSB(40,sb);
    else if (obj.readyState == "interactive")
      setSB(80,sb);
    else if (obj.readyState == "complete")
      //setSB(90,sb); put this back in when ClaimProperty and VehicleInvolved are updated for sb
      setSB(100,sb);
  }

  var SBstarted = false;
  function setSB(v, el)
  {
    if ((document.all && document.getElementsByTagName) || document.readyState == "complete")
    {
      filterEl = el.children[0];
      valueEl  = el.children[1];

      el.style.display="inline";

      if (filterEl.style.pixelWidth > 0)
      {
        var filterBackup = filterEl.style.filter;
        filterEl.style.filter = "";
        filterEl.style.filter = filterBackup;
      }

      filterEl.style.width = v + "%";
      valueEl.innerText = v + "%";

      //if (v < 10 && SBstarted == false)
      //{
      //  SBstarted = true;
      //  window.setTimeout("fakeProgress(" + (v + 1) + ", document.all['" + el.id + "'])", 100);
      //}

      if (v == 100)
      {
        SBstarted = false;
        window.setTimeout("HideSB(document.all['" + el.id + "'])", 300);
      }
    }
  }

  function fakeProgress(v, el)
  {
    setSB(v, el);
    if (SBstarted == true)
      window.setTimeout("fakeProgress(" + (v + 1) + ", document.all['" + el.id + "'])", 100);
  }

  function HideSB(el)
  {
    el.style.display="none";
  }

  // this makes sure the menubar is taken in to account on the sizing
  function fixSize()
  {
    contentDiv.style.height = document.body.offsetHeight - menuBar.offsetHeight;
  }

  // if we ever enable resizing of the window then need to enable this
  window.onresize = fixSize;

  // object builder for the skins
  function SkinInfo(name, href )
  {
    this.name = name;
    this.href = href;
  }

  // enable/disable the claim menu items
  // nMenu = index of menu item
  function ClaimsMenu(bEnable, nMenu)
  {
    if (nMenu == 4) return; //seperator bar.
      var oMenu = document.getElementById("claimsMenu");
    if (bEnable == true)
      oMenu.rows[nMenu-1].className="";
    else
      oMenu.rows[nMenu-1].className="disabled";
    forceRebuild(oMenu);
  }

  function FinancialMenu(bEnable, nMenu)
  {
    var oMenu = document.getElementById("financialMenu");
    if (bEnable == true)
      oMenu.rows[nMenu-1].className="";
    else
      oMenu.rows[nMenu-1].className="disabled";
    forceRebuild(oMenu);
  }


  // enable/disable the Vehicles Menu items
  // nMenu = index of menu item
  function VehiclesMenu(bEnable, nMenu)
  {
    var oMenu = document.getElementById("vehiclesMenu");
    if (bEnable == true)
      oMenu.rows[nMenu-1].className="";
    else
      oMenu.rows[nMenu-1].className="disabled";
    forceRebuild(oMenu);
  }

  // enable/disable the Properties Menu items
  // nMenu = index of menu item
  function PropertiesMenu(bEnable, nMenu)
  {
    var oMenu = document.getElementById("propertiesMenu");
    if (oMenu) {
      if (bEnable == true)
        oMenu.rows[nMenu-1].className="";
      else
        oMenu.rows[nMenu-1].className="disabled";
      forceRebuild(oMenu);
    }
  }

  // enable/disable the Documents Menu items
  // nMenu = index of menu item
  function DocumentsMenu(bEnable, nMenu)
  {
    var oMenu = document.getElementById("documentsMenu");
    if (bEnable == true)
      oMenu.rows[nMenu-1].className="";
    else
      oMenu.rows[nMenu-1].className="disabled";
    forceRebuild(oMenu);
  }

  function ShowClaimReAssign(strEntity)
  {
    var sClaimAspectID;
    var sEntityDesc = "";
    switch (strEntity) {
      case "Claim":
        sClaimAspectID = sClaim_ClaimAspectID;
        sEntityDesc = "";
        break;
      case "Vehicle":
        sClaimAspectID = sVehicleClaimAspectID;
        sEntityDesc = sVehicleName;
        break;
      case "Property":
        sClaimAspectID = sPropertyClaimAspectID;
        sEntityDesc = sPropertyName;
        break;
      default:
    }


    var sDimensions = strEntity == "Vehicle" ? "dialogHeight:280px; dialogWidth:300px; " : "dialogHeight:200px; dialogWidth:300px; ";
    var sSettings = "center:Yes; help:No; resizable:No; status:No; unadorned:Yes;";
    var strRet = window.showModalDialog("ClaimReAssign.asp?WindowID=" + vWindowID + "&claimAspectID=" + sClaimAspectID + "&Entity=" + strEntity + "&Desc=" + escape(sEntityDesc), "", sDimensions + sSettings);
    if (strRet == "")
    {
        top.window.location.reload();
    }
  }

  function ShowCloseExposureDialog(strEntity)
  {
      /*gbDiaryLocked = true;
      var sDimensions = "dialogHeight:280px; dialogWidth:445px; "
      var sSettings = "resizable:no; status:no; help:no; center:yes;"
      //var sReturn = window.showModalDialog('ClaimClose.asp','','dialogHeight: 200px; dialogWidth: 300px; center: Yes; help: No; resizable: No; status: No; unadorned: Yes');
      var sReturn = window.showModalDialog("ClaimExposure.asp?WindowID=" + vWindowID + "&LYNXID=" + tdLynxID.innerText + "&mode=close", "", sDimensions + sSettings);
      //alert("ClaimExposure.asp?LYNXID=" + tdLynxID.innerText + "&mode=close");
      if ( sReturn == "OK" )
        oIframe.frameElement.src = oIframe.frameElement.src;
      else
        gbDiaryLocked = false;*/
    if (sCloseExpCRUD.indexOf("U") == -1){
      ClientWarning ("You do not have permission to close exposure. Please contact your supervisor.");
      return;
    }
    
    var sRet = "";
    var sClaimAspectID = "";
    if (strEntity == "Vehicle") {
      sClaimAspectID = sVehicleClaimAspectID;
      sRet = YesNoMessage("Confirm Close " + strEntity, "Do you want to close vehicle " + sVehicleName + "?");
    }
    if (sRet == "Yes") {
      var sAction = "Update";
      var sRequest = "LynxID=" + vLynxID + "&" +
                      "ClaimAspectList=" + sClaimAspectID + "&" +
                      "UserID=" + vUserID + "&" +
                      "ApplicationCD=APD";
      var retArray = new Array;
      var sProc = "uspWorkflowCloseExposure";
      //alert(sProc + "\n" + sRequest); return
      if (sProc != "") {
         var coObj = RSExecute("/rs/RSADSAction.asp", "JustExecute", sProc, sRequest );
         retArray = ValidateRS( coObj );
        oIframe.frameElement.src = oIframe.frameElement.src;
      }
    }
  }

  function ShowReopenExposureDialog(){
      var sDimensions = "dialogHeight:280px; dialogWidth:445px; "
      var sSettings = "resizable:no; status:no; help:no; center:yes;"
      var sReturn = window.showModalDialog("ClaimExposure.asp?WindowID=" + vWindowID + "&LYNXID=" + tdLynxID.innerText + "&mode=open", "", sDimensions + sSettings);
      if ( sReturn == "OK" )
        top.document.location.reload();
          //oIframe.frameElement.src = oIframe.frameElement.src;
  }

  function ShowClaimCancelDialog(){
      var iLynxID = tdLynxID.innerText;
      var sDimensions = "dialogHeight:230px; dialogWidth:445px; "
      var sSettings = "resizable:no; status:no; help:no; center:yes;"
      var sReturn = window.showModalDialog("ClaimCancel.asp?WindowID=" + vWindowID + "&LYNXID=" + iLynxID, "", sDimensions + sSettings);
      if ( sReturn == "OK" ) { //reload the current page
          oIframe.frameElement.src = oIframe.frameElement.src;
          
          refreshNotesWindow( iLynxID, vUserID );
          refreshCheckListWindow( iLynxID, vUserID );
      }
  }

  function ShowClaimVoidDialog(){
      var iLynxID = tdLynxID.innerText;
      var sDimensions = "dialogHeight:230px; dialogWidth:445px; "
      var sSettings = "resizable:no; status:no; help:no; center:yes;"
      var sReturn = window.showModalDialog("ClaimVoid.asp?WindowID=" + vWindowID + "&LYNXID=" + iLynxID, "", sDimensions + sSettings);
      if ( sReturn == "OK" ) {//reload the current page
          oIframe.frameElement.src = oIframe.frameElement.src;
          refreshNotesWindow( iLynxID, vUserID );
          refreshCheckListWindow( iLynxID, vUserID );
      }
  }
  
  function ShowEntityCancelDialog(strEntity){
    var iLynxID = tdLynxID.innerText;
    var sDimensions = "dialogHeight:230px; dialogWidth:445px; "
    var sSettings = "resizable:no; status:no; help:no; center:yes;"
    var sEntityInfo = "";

    switch (strEntity) {
      case "vehicle":
        sEntityInfo = "&ClaimAspectID=" + sVehicleClaimAspectID + "&Entity=Vehicle" + "&Info=" + escape(sVehicleName);
        break;
      case "property":
        sEntityInfo = "&ClaimAspectID=" + sPropertyClaimAspectID + "&Entity=Property" + "&Info=" + escape(sPropertyName);
        break;
    }
      
    var sReturn = window.showModalDialog("EntityCancel.asp?WindowID=" + vWindowID + "&LYNXID=" + iLynxID + sEntityInfo, "", sDimensions + sSettings);
    if ( sReturn == "OK" ) { //reload the current page
      // try to append the veh number to the screen so that the refresh will open the vehicle in context.
      var sParams = oIframe.document.URL.split("&");
      
      for (var i = 0; i < sParams.length; i++) {
        if (sParams[i].toLowerCase().indexOf("vehnum") != -1){
          sParams[i] = "VehNum=" + sVehicleNum;
          break;
        }
      }
        oIframe.frameElement.src = sParams.join("&");
        refreshNotesWindow( iLynxID, vUserID );
        refreshCheckListWindow( iLynxID, vUserID );
    }
  }
  
  function ShowEntityVoidDialog(strEntity) {
    var iLynxID = tdLynxID.innerText;
    var sDimensions = "dialogHeight:230px; dialogWidth:445px; "
    var sSettings = "resizable:no; status:no; help:no; center:yes;"
    var sEntityInfo = "";

    switch (strEntity) {
      case "vehicle":
        sEntityInfo = "&ClaimAspectID=" + sVehicleClaimAspectID + "&Entity=Vehicle" + "&Info=" + escape(sVehicleName);
        break;
      case "property":
        sEntityInfo = "&ClaimAspectID=" + sPropertyClaimAspectID + "&Entity=Property" + "&Info=" + escape(sPropertyName);
        break;
    }
      
    var sReturn = window.showModalDialog("EntityVoid.asp?WindowID=" + vWindowID + "&LYNXID=" + iLynxID + sEntityInfo, "", sDimensions + sSettings);
    if ( sReturn == "OK" ) { //reload the current page
      // try to append the veh number to the screen so that the refresh will open the vehicle in context.
      var sParams = oIframe.document.URL.split("&");
      
      for (var i = 0; i < sParams.length; i++) {
        if (sParams[i].toLowerCase().indexOf("vehnum") != -1){
          sParams[i] = "VehNum=" + sVehicleNum;
          break;
        }
      }
        oIframe.frameElement.src = sParams.join("&");
        refreshNotesWindow( iLynxID, vUserID );
        refreshCheckListWindow( iLynxID, vUserID );
    }
  }

  // Puts up the modal document generator dialog.
  function ShowDocumentGeneratorDialog()
  {
      var iLynxID = tdLynxID.innerText;

      //var sDimensions = "dialogHeight:600px; dialogWidth:800px; "
      //var sSettings = "resizable:no; status:no; help:no; center:yes;"
      //var sReturn = window.showModalDialog("DocumentGenerator.asp?WindowID=" + vWindowID + "&LynxId=" + iLynxID + "&UserId=" + vUserID, "", sDimensions + sSettings);

      var sDimensions = "height=600, width=800, top=54, left=108, "
      var sSettings = "resizable=no, status=no, help=no, center=yes"
      var sReturn = window.open("DocumentGenerator.asp?WindowID=" + vWindowID + "&LynxId=" + iLynxID + "&UserId=" + vUserID, "", sDimensions + sSettings);
  }
  
  function ShowDocument2ClientDialog(){
      if (sVehicleClaimAspectID) {
         var iLynxID = tdLynxID.innerText;
         
         var sDimensions = "height=540, width=800, top=54, left=108, "
         var sSettings = "resizable=yes, status=no, help=no, center=yes"
         var sReturn = window.open("ClientDocuments.asp?WindowID=" + vWindowID + "&LynxId=" + iLynxID + "&UserId=" + vUserID + "&VehNum=" + sVehicleNum + "&VehClaimAspectID=" + sVehicleClaimAspectID, "", sDimensions + sSettings);    
         
         refreshNotesWindow( vLynxID, vUserID );
      } else {
         ClientWarning("You need to view the vehicle on the 'Claim' tab inorder to send the documents.");
      }
  }
  
  function ShowCustomFormsDialog(){
      var iLynxID = tdLynxID.innerText;
      
      var sDimensions = "height=470, width=800, top=54, left=108, "
      var sSettings = "resizable=no, status=no, help=no, center=yes"
      var sReturn = window.open("CustomForms.asp?WindowID=" + vWindowID + "&LynxId=" + iLynxID + "&UserId=" + vUserID + "&VehNum=" + sVehicleNum + "&InsuranceCompanyID=" + vInsuranceCompanyID , "", sDimensions + sSettings);    
      
      //refreshNotesWindow( vLynxID, vUserID );
  }

  // Puts up the modal document upload dialog.
  function ShowDocumentUploadDialog()
  {
      var sDimensions = "dialogHeight:361px; dialogWidth:600px; "
      var sSettings = "resizable:yes; status:no; help:no; center:yes;"
      var sReturn = window.showModalDialog("DocumentUpload.asp?WindowID=" + vWindowID + "&LynxId=" + vLynxID + "&InsuranceCompanyID=" + vInsuranceCompanyID + "&UserId=" + vUserID, "", sDimensions + sSettings);
      if (sClaimView == "cond" || sClaimView == "condnew") {
        var oVehFrame = oIframe.document.getElementById("frmVehDetail" + oIframe.sClaimAspectID);
        if (oVehFrame) {
          //oVehFrame.contentWindow.reloadTab();
            if (typeof(oVehFrame.contentWindow.refreshVehicle) == "function"){
               oVehFrame.contentWindow.refreshVehicle();
            }
        } else
          oIframe.document.location.reload();
      } else 
        oIframe.frameElement.src = oIframe.frameElement.src;
  }

  function chkBeforeUnloadWindow()
  {
    if (gAryOpenedWindows.length > 0)
    {
      for (var i=0; i<gAryOpenedWindows.length; i++)
      {
        if (typeof(gAryOpenedWindows[i]) != "undefined")  // test to see if window exists
        {
          if (!(gAryOpenedWindows[i].closed))             // test to see if a window is still open
            gAryOpenedWindows[i].close();
        }
      }
    }

    if (gbDirtyFlag == true)
    {
      event.returnValue = "The information on this page has changed. To save, please press CANCEL?";
    }
  }

  var dtLastNavToDate = new Date(0);

  // Quick goto navigaton on toolbar
  function navigateTo(strEntity)
  {
    try
    {
      // Only allow them to use this once every 1/2 second to prevent
      // repeat key server overload (Test Track 1911).
      var dtNowDate = new Date();
      var nMilliseconds = 1001;

      try {
        nMilliseconds = dtNowDate.getTime() - dtLastNavToDate.getTime();
      } catch ( e ) { }

      if ( nMilliseconds > 1000 )
      {
        dtLastNavToDate = dtNowDate;

        if (strEntity == "advSearch")
        {
          // Pop up the claim search dialog.
          var sHREF = "ClaimSearch.asp?WindowID=" + vWindowID;
          var sSettings = "dialogHeight:496px; dialogWidth:700px;  resizable:yes; status:no; help:no; center:yes;";
          var strRet = window.showModalDialog( sHREF, "", sSettings );

          // Return value signified a navigation needs to occur.
          if ( strRet != "" && strRet != undefined )
              NavOpenWindowSettings( strRet );
        }
        else
        {
          var sLynxID = txtSearchLynxID.value;
          if (sLynxID == "")
            return;

          sLynxID = parseNumbersOnly(sLynxID);

          // check for a valid LynxID
          var co = RSExecute("/rs/RSValidateLynxID.asp", "IsValidateLynxID", sLynxID);
          var sTblData = co.return_value;
          var listArray = sTblData.split("||");
          if (listArray[1] == 1)
          {
            if (listArray[0] == 0)
            {
              fade(InvIdMsg,1); //show error message
            }
            else
            {
              txtSearchLynxID.value = "";
              // Choose whether to open in this window or open a new window.
              if ( bClaimRepDesktop == true )
                  NavOpenWindow( strEntity, sLynxID, "1" );
              else
                  oIframe.frameElement.src = NavBuildUrl( strEntity, sLynxID, "1", vWindowID );
            }
          }
          else
          {
            ClientWarning("Unable to verify the Lynx ID that you entered. Please contact the system administrator.");
            return;
          }
        }
      }
    } catch ( e ) {
      ClientError( "ApdSelect.xsl::navigateTo( '" + strEntity + "' ) " + e.message );
    }
  }
  
  function setClaimViewPref(obj, sKey, sValue){
    var sID;
    if (obj) {
      try {
        sID = obj.id;
      } catch(e) {}
      
      switch (sID) {
        case "StdClaimView":
          if (setPreference(sKey, sValue) == true) {
            preferencesRoot.menu.scriptlet = null;
            ExpClaimView.cells[0].innerHTML = "&nbsp;";
            StdClaimView.cells[0].innerHTML = "<img src='/images/menuCheck.gif'/>";
            sClaimView = sValue;
          }
          break;
        case "ExpClaimView":
          if (setPreference(sKey, sValue) == true) {
            preferencesRoot.menu.scriptlet = null;
            StdClaimView.cells[0].innerHTML = "&nbsp;";
            ExpClaimView.cells[0].innerHTML = "<img src='/images/menuCheck.gif'/>";
            sClaimView = sValue;
          }
          break;
      }
    }
  }
  
  function setPreference(sKey, sValue){
    if (window.navigator.cookieEnabled) {
      document.cookie = sKey + "=" + sValue + "; expires=Fri, 31 Dec 2010 23:59:59 GMT;";
      return true;
    } else {
      ClientWarning("Cookies are not enabled on this machine. Cannot set preferences. To enable cookies, please contact Help desk.")
      return false;
    }
  }

  function refreshVehicles(vehNum){
    sURL = top.document.URL;
    if (sURL.indexOf("VehNum") == -1)
      top.window.navigate(sURL + "&VehNum=" + vehNum);
    else {
      var aURL = sURL.split("&");
      sURL = "";
      for (var i = 0; i < aURL.length; i++){
        if (aURL[i].indexOf("VehNum") == -1) {
          sURL += aURL[i] + "&";
        } else {
          sURL += "VehNum=" + vehNum;
        }
      }
      if (sURL != "")
        top.window.navigate(sURL);
      else
        top.document.location.reload();
    }
  }
  
  function showEntityOwners(){
    document.getElementById("layerOwners").style.display = "inline";
  }
  
    function highLightEntityNum(sEntityNum){
      for ( var i = 0; i < tblOwnerVehPrp.rows.length; i++){
        if (tblOwnerVehPrp.rows[i].cells[2].innerText == sEntityNum){
          tblOwnerVehPrp.rows[i].style.backgroundColor = "#AFEEEE";

          var obj = tblOwnerVehPrp.rows[i];
          var iScrollPos = (obj.offsetTop + obj.offsetHeight) - divScroller.scrollTop - divScroller.clientHeight;
          var iCount = 0;
          //alert(iScrollPos);
          if (iScrollPos > 0) {
             while (iScrollPos > 0 && iCount < 500) {
                divScroller.doScroll("down");
                iScrollPos = (obj.offsetTop + obj.offsetHeight) - divScroller.scrollTop - divScroller.clientHeight + 5; //add 15 pixel to display a portion of the next tab
                iCount++;
             }
          } else {
             iScrollPos = obj.offsetTop - divScroller.scrollTop;
             while (iScrollPos < 0 && iCount < 500) {
                divScroller.doScroll("up");
                iScrollPos = obj.offsetTop - divScroller.scrollTop - 5; //subract 15 pixel to display a portion of the previous tab
                iCount++;
             }
          }
          //break;
        } else {
          tblOwnerVehPrp.rows[i].style.backgroundColor = "#FFFFFF";
        }
      }
    }
    
    function OpenWorkAssignment(){
      var sDimensions = "height=460px, width=700px, ";
      var sSettings = "center=Yes, help=No, resizable=No, status=No, unadorned=Yes,";
      window.open("/APDWorkAssignment.asp", "", sDimensions + sSettings);
    }
    
    function CompleteServiceChannel(){
var sRetValue;
if(strDispositionTypeCD == "RC" && strServiceChannelCD =="PS")	{
sRetValue = YesNoMessage("Confirm Invoice Generation", "Have you generated the indemnity invoice?");
if(sRetValue == "No")
parent.document.getElementById("oIframe").src = "/InvoiceDetail.asp?WindowID="+vWindowID;
}

if(sRetValue != "No")	{
       if (strCurrentClaimAspectServiceChannelID != "" && isNaN(strCurrentClaimAspectServiceChannelID) == false) {
          var sRet = YesNoMessage("Confirm Close", "Do you want to close " + strCurrentServiceChannelName + " service channel for " + sVehicleName + "?");
          
          if (sRet == "Yes") {
             var sProc, sRequest;
         
             sRequest = "LynxID=" + vLynxID +
                         "&ClaimAspectServiceChannelList=" + strCurrentClaimAspectServiceChannelID +
                         "&UserID=" + vUserID;
                         
             sProc = "uspWorkflowCompleteServiceChannel";
             
             //alert(sRequest); return;
             
             var aRequests = new Array();
             aRequests.push( { procName : sProc,
                               method   : "executespnp",
                               data     : sRequest }
                           );
             var objRet = XMLSave(makeXMLSaveString(aRequests));
             if (objRet.code == 0) {
               top.window.location.reload();
             }
          }
       }
    }
    
    }
    
    function CancelServiceChannel(){
       var sDimensions = "dialogHeight:160px; dialogWidth:450px; ";
       var sSettings = "center:Yes; help:No; resizable:No; status:No; unadorned:Yes;";
       var obj = {
                     title : "Cancel Comments",
                     message : "Please enter comments as to why this service channel is being cancelled.",
                     maxLength : "1000",
                     defaultText : "",
                     commentRequired : "true"
                 };
       var strRet = window.showModalDialog("frmInputBox.htm", obj, sDimensions + sSettings);
       if (typeof(strRet) == "string")
       {
          var sProc, sRequest;
      
          sRequest = "ClaimAspectServiceChannelID=" + strCurrentClaimAspectServiceChannelID +
                      "&UserID=" + vUserID +
                      "&Comment=" + escape(strRet);
                      
          sProc = "uspWorkflowCancelServiceChannel";
          //alert(sRequest); return;
          
          var aRequests = new Array();
          aRequests.push( { procName : sProc,
                            method   : "executespnp",
                            data     : sRequest }
                        );
          var objRet = XMLSave(makeXMLSaveString(aRequests));
          if (objRet.code == 0) {
            top.window.location.reload();
          }
       }
    }
    
   function ReactivateServiceChannel(){
       var sDimensions = "dialogHeight:280px; dialogWidth:450px; ";
       var sSettings = "center:Yes; help:No; resizable:No; status:No; unadorned:Yes;";
       var sURL = "ReactivateServiceChannel.asp?LynxID=" + vLynxID + "&UserID=" + vUserID + "&InsuranceCompanyID=" + vInsuranceCompanyID;
       //alert(sURL); return;
       var strRet = window.showModalDialog(sURL, "", sDimensions + sSettings);
       if (typeof(strRet) == "string") {
         if (strRet == "Refresh"){
            top.window.location.reload();
         }
       }      
   }
   
   function ActivateWarranty(){  
       var strClaimAspectID = top.sVehicleClaimAspectID;
       
       if (strClaimAspectID != ""){
          var sRet = YesNoMessage("Confirm Activate Warranty", "Do you want to activate warranty on this vehicle?");
          
          if (sRet == "Yes") {
             var sProc, sRequest;
         
             sRequest = "ClaimAspectID=" + strClaimAspectID +
                         "&UserID=" + vUserID;
                         
             sProc = "uspActivateWarranty";
             
             //alert(sRequest); return;
             
             var aRequests = new Array();
             aRequests.push( { procName : sProc,
                               method   : "executespnp",
                               data     : sRequest }
                           );
             var objRet = XMLSave(makeXMLSaveString(aRequests));
             if (objRet.code == 0) {
               //loadVehicle(strClaimAspectID);
               if (typeof(document.frames["oIframe"].loadVehicle) == "function"){
                  document.frames["oIframe"].loadVehicle(strClaimAspectID);
               }
             }
          }
       } else {
         ClientWarning("Unable to determine the current vehicle.");
       }
   }
    
  /*function ShowMobileElectronics(){
    oIframe.frameElement.src = "MEAssignment.asp";
  }*/
  
  //This no longer needed. Changed ShowEstimate in Estimates to be a showModelessDialog window
  //function ShowEstimateDoc(imgLoc){
  //    if (imgLoc != "") {
  //        ifrmEstimateDoc.frameElement.style.display = "inline";
  //        ifrmEstimateDoc.frameElement.src = "EstimateDocView.asp?docPath=" + imgLoc;
  //    }
  //}


]]>
          </SCRIPT>

          <script type="text/javascript" language="JavaScript1.2"  src="webhelp/RoboHelp_CSH.js"></script>

          <script type="text/javascript">
            document.onhelp=function(){
            RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,34);
            event.returnValue=false;
            };
          </script>

        </HEAD>
        <BODY unselectable="on" class="bodyAPDSelect" scroll="no" onLoad="InitPage();" onbeforeunload="chkBeforeUnloadWindow()">

          <!-- choose a background image -->
          <xsl:attribute name="background">
            <xsl:choose>
              <xsl:when test="$LynxID=''">/images/fundo_desktop.gif</xsl:when>
              <xsl:otherwise>/images/fundo_default.gif</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>

          <!-- DO NOT DELETE. This is used to check the users browser security setting-->
          <!--<OBJECT id="brTest" tabIndex="-1" classid="clsid:50F16B26-467E-11D1-8271-00C04FC3183B" style="display:none" onerror="noPluginRights()"></OBJECT>-->
          <OBJECT id="testTif" name="testTif" classid="CLSID:106E49CF-797A-11D2-81A2-00E02C015623" codebase="/install/alttiff.cab#version=1,5,1,1" style="display:none" width="0" height="0" onerror="noPluginRights2()"></OBJECT>
          <!-- END OF DO NOT DELETE -->

          <!-- Menu bar -->
          <xsl:call-template name="APDMenu">
            <xsl:with-param name="Desktop">
              <xsl:choose>
                <xsl:when test="$LynxID=''">CRD</xsl:when>
                <xsl:otherwise>CLM</xsl:otherwise>
              </xsl:choose>
            </xsl:with-param>
          </xsl:call-template>
          <!-- Menu bar -->

          <!--Invalid Lynx ID message-->
          <DIV unselectable="on" id="InvIdMsg" style="position:absolute; z-index:10; left:638px; top:3px; visibility:hidden;">
            <TABLE unselectable="on" STYLE="border:1pt solid #FF0000" BGCOLOR="#EFEFEF" WIDTH="96" CELLPADDING="0" CELLSPACING="0">
              <TR unselectable="on">
                <TD unselectable="on" onclick="fade(InvIdMsg,0)" title="Click to Close" style="cursor:default; text-align:center">
                  <SPAN style="color:FF0000; font-weight:bold;">Invalid LYNX ID</SPAN>
                </TD>
              </TR>
            </TABLE>
          </DIV>

          <DIV id="contentDiv" style="width: 100%; height: 100%;">

            <SCRIPT language="JavaScript" src="_ScriptLibrary/rs.htm"></SCRIPT>
            <SCRIPT language="JavaScript">RSEnableRemoteScripting("_ScriptLibrary");</SCRIPT>

            <DIV style="position:absolute; left: 810px; top: 2px;"  border="0" cellspacing="0" cellpadding="0">
              <!-- Status Bar Starts -->
              <div id="sb" style="border: 1 solid black; width: 200px; height: 16px; background: #F3ECE5; text-align: left;">
                <div id="sbChild1" style="position: absolute; width: 0%; height: 14px; filter: Alpha(Opacity=0, FinishOpacity=100, Style=1, StartX=0, StartY=0, FinishX=100, FinishY=0);">
                  <div style="width: 100%; height: 100%; background: #ffcf9C; font-size: 1;"/>
                </div>
                <div style="position: absolute; width: 100%; text-align: center; font-family:  Verdana,arial; font-size: 11px; color: black;">0%</div>
              </div>
              <!-- Status Bar Ends -->
            </DIV>
            <script>setSB(30,sb)</script>
            <!-- The head is pretty large -->

            <!--Default IFrame for Non TABBED DIV's-->
            <DIV unselectable="on" id="layerData">
              <xsl:choose>
                <xsl:when test="$LynxID=''">
                  <xsl:attribute name="style">border : 0px #666666 none;position:absolute; width:1008px; height:591px; z-index:0; left: 4px; top: 26px; background-image1 : url(/images/workarea_bg.gif); visibility: visible;overflow:hidden</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="style">border : 0px #666666 none;position:absolute; width:783px; height:591px; z-index:0; left: 4px; top: 26px; background-image : url(/images/workarea_bg.gif); visibility: visible;overflow:hidden</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <table border="0" cellpadding="0" cellspacing="0" style="height:100%;width:100%">
                <colgroup>
                  <col width="5px"/>
                  <col width="*"/>
                  <col width="5px"/>
                </colgroup>
                <tr style="height:6px">
                  <td>
                    <img src="/images/folder_TLC.gif"/>
                  </td>
                  <td>
                    <img src="/images/folder_top.gif" style="height:6px;width:100%"/>
                  </td>
                  <td>
                    <img src="/images/folder_TRC.gif"/>
                  </td>
                </tr>
                <tr>
                  <td>
                    <img src="/images/folder_Left.gif" style="height:100%;width:5px"/>
                  </td>
                  <td>
                    <IFRAME vspace="6" name="oIframe" id="oIframe" onreadystatechange='IFStateChange(this);' style="border : 0px; visibility: visible;position:absolute;  left: 5px; top: 0px; width: 100%; height: 100%;overflow:hidden" allowtransparency1="true">
                      <xsl:attribute name="src">
                        <xsl:choose>
                          <xsl:when test="$LynxID=''">
                            ClaimRepDesk.asp?WindowID=<xsl:value-of select="$WindowID"/>
                          </xsl:when>
                          <xsl:when test="$Entity='vehicle' and $ClaimView !='cond' and $ClaimView !='condnew'">
                            ClaimVehicleList.asp?WindowID=<xsl:value-of select="$WindowID"/>&amp;LynxID=<xsl:value-of select="$LynxID"/>&amp;VehContext=<xsl:value-of select="$EntityContext"/>
                          </xsl:when>
                          <xsl:when test="$Entity='property'">
                            ClaimPropertyList.asp?WindowID=<xsl:value-of select="$WindowID"/>&amp;LynxID=<xsl:value-of select="$LynxID"/>&amp;PropContext=<xsl:value-of select="$EntityContext"/>
                          </xsl:when>
                          <xsl:when test="$Entity='document'">
                            ClaimDocument.asp?WindowID=<xsl:value-of select="$WindowID"/>&amp;LynxID=<xsl:value-of select="$LynxID"/>&amp;
                          </xsl:when>
                          <xsl:when test="$Entity='claimhistory'">
                            ClaimHistory.asp?WindowID=<xsl:value-of select="$WindowID"/>&amp;
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:choose>
                              <xsl:when test="$ClaimView='cond'">
                                ClaimXP.asp?WindowID=<xsl:value-of select="$WindowID"/>&amp;LynxID=<xsl:value-of select="$LynxID"/>&amp;VehNum=<xsl:value-of select="$vehNum2"/>
                              </xsl:when>
                              <xsl:when test="$ClaimView='condnew'">
                                ClaimXPNew.asp?WindowID=<xsl:value-of select="$WindowID"/>&amp;LynxID=<xsl:value-of select="$LynxID"/>&amp;VehNum=<xsl:value-of select="$vehNum2"/>
                              </xsl:when>

                              <xsl:otherwise>
                                Claim.asp?WindowID=<xsl:value-of select="$WindowID"/>&amp;LynxID=<xsl:value-of select="$LynxID"/>
                              </xsl:otherwise>
                            </xsl:choose>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:attribute>
                    </IFRAME>
                  </td>
                  <td>
                    <img src="/images/folder_Right.gif" style="height:100%;width:5px"/>
                  </td>
                </tr>
              </table>
              <!--  <IFRAME name="oIframe" id="oIframe" class="DataIFrame" allowtransparency="false">-->
            </DIV>
            <script>setSB(60,sb)</script>

            <!--include the Tab Folder Info DIV's-->
            <DIV unselectable="on" id="layerTabs" border="0" cellspacing="0" cellpadding="0">
              <xsl:choose>
                <xsl:when test="$LynxID=''">
                  <xsl:attribute name="style">position:absolute; left: 4px; top: 617px; width: 1008px; height: 36px; visibility: hidden</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="style">position:absolute; left: 4px; top: 617px; width: 783px; height: 36px; visibility: hidden</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <TABLE unselectable="on" width="100%" cellpadding="0" cellspacing="0" border="0">
                <xsl:choose>
                  <xsl:when test="$ClaimView='cond' or $ClaimView='condnew'">
                    <TR>
                      <TD unselectable="on">
                        <IMG unselectable="on" src="/images/folder_BLC.gif" alt="" width="5" height="6" border="0"/>
                        <IMG unselectable="on" src="/images/folder_Bottom.gif" alt="" width="5" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on" bgcolor="#FFFAEB" nowrap="" valign="bottom">
                        <IMG unselectable="on" src="/images/folder_Bottom.gif" alt="" width="2" height="6" border="0"/>
                        <IMG unselectable="on" id="imgMainTabOverlap1" src="/images/spacer.gif" alt="" width="91" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on" bgcolor="#FFFAEB">
                        <IMG unselectable="on" id="imgMainTabOverlap4" src="/images/folder_Bottom.gif" alt="" width="90" height="6" border="0"/>
                        <IMG unselectable="on" src="/images/folder_Bottom.gif" alt="" width="3" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on" bgcolor="#FFFAEB">
                        <IMG unselectable="on" id="imgMainTabOverlap2" src="/images/folder_Bottom.gif" alt="" width="90" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on" bgcolor="#FFFAEB" nowrap="" valign="bottom">
                        <IMG unselectable="on" id="imgMainTabOverlap3" src="/images/folder_Bottom.gif" alt="" width="91" height="6" border="0"/>
                        <IMG unselectable="on" src="/images/folder_Bottom.gif" alt="" width="2" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on" bgcolor="#FFFAEB">
                        <IMG unselectable="on" id="imgMainTabOverlap5" src="/images/folder_Bottom.gif" alt="" width="93" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on" bgcolor="#FFFAEB">
                        <IMG unselectable="on" id="imgMainTabOverlap6" src="/images/folder_Bottom.gif" alt="" width="93" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on" width="100%">
                        <IMG unselectable="on" src="/images/folder_Bottom.gif" alt="" width="100%" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on">
                        <IMG unselectable="on" src="/images/folder_BRC.gif" alt="" width="5" height="6" border="0"/>
                      </TD>
                    </TR>
                    <TR>
                      <TD unselectable="on">
                        <IMG unselectable="on" src="/images/spacer.gif" alt="" width="10" height="1" border="0"/>
                      </TD>
                      <TD unselectable="on">
                        <xsl:choose>
                          <xsl:when test="$ClaimView='condnew'">
                            <IMG unselectable="on" id="imgMainTab1" src="/images/claimxp_sel.gif" alt="" style="cursor:hand" width="93" height="19" border="0" onClick="SetMainTab(1,'ClaimXPNew.asp');setSB(0,top.sb);" onmouseover="window.status='Claim Info'; return true" onmouseout="window.status=''; return true" onFocus="if(this.blur)this.blur()"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <IMG unselectable="on" id="imgMainTab1" src="/images/claimxp_sel.gif" alt="" style="cursor:hand" width="93" height="19" border="0" onClick="SetMainTab(1,'ClaimXP.asp');setSB(0,top.sb);" onmouseover="window.status='Claim Info'; return true" onmouseout="window.status=''; return true" onFocus="if(this.blur)this.blur()"/>
                          </xsl:otherwise >
                        </xsl:choose >
                      </TD>
                      <TD unselectable="on">
                        <IMG unselectable="on" id="imgMainTab4" src="/images/doc_des.gif" alt="" style="cursor:hand" width="93" height="19" border="0" onClick="SetMainTab(4,'ClaimDocument.asp');setSB(0,top.sb);" onmouseover="window.status='Document Info'; return true" onmouseout="window.status=''; return true" onFocus="if(this.blur)this.blur()"/>
                      </TD>
                      <TD unselectable="on">
                        <IMG style="visibility:hidden" unselectable="on" id="imgMainTab2" src="/images/veh_des.gif" alt="" width="90" height="19" border="0" onClick="SetMainTab(2,'ClaimVehicleList.asp');setSB(0,top.sb);" onmouseover="window.status='Vehicle Info'; return true" onmouseout="window.status=''; return true" onFocus="if(this.blur)this.blur()"/>
                      </TD>
                      <TD unselectable="on">
                        <IMG style="visibility:hidden" unselectable="on" id="imgMainTab3" src="/images/prop_des.gif" alt="" width="90" height="19" border="0" onClick="SetMainTab(3,'ClaimPropertyList.asp');setSB(0,top.sb);" onmouseover="window.status='Property Info'; return true" onmouseout="window.status=''; return true" onFocus="if(this.blur)this.blur()"/>
                      </TD>
                      <TD unselectable="on">
                        <IMG style="visibility:hidden" unselectable="on" id="imgMainTab5" src="/images/subro_des.gif" alt="" width="93" height="19" border="0"/>
                      </TD>
                      <TD unselectable="on">
                        <IMG style="visibility:hidden" unselectable="on" id="imgMainTab6" src="/images/rental_des.gif" alt="" width="93" height="19" border="0"/>
                      </TD>
                      <TD unselectable="on" width="100%">
                        <IMG unselectable="on" src="/images/spacer.gif" alt="" width="100%" height="5" border="0"/>
                      </TD>
                      <TD unselectable="on">
                        <IMG unselectable="on" src="/images/spacer.gif" alt="" width="5" height="5" border="0"/>
                      </TD>
                    </TR>
                  </xsl:when>
                  <xsl:otherwise>
                    <TR>
                      <TD unselectable="on">
                        <IMG unselectable="on" src="/images/folder_BLC.gif" alt="" width="5" height="6" border="0"/>
                        <IMG unselectable="on" src="/images/folder_Bottom.gif" alt="" width="5" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on" bgcolor="#FFFAEB" nowrap="" valign="bottom">
                        <IMG unselectable="on" src="/images/folder_Bottom.gif" alt="" width="2" height="6" border="0"/>
                        <IMG unselectable="on" id="imgMainTabOverlap1" src="/images/spacer.gif" alt="" width="91" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on" bgcolor="#FFFAEB">
                        <IMG unselectable="on" id="imgMainTabOverlap2" src="/images/folder_Bottom.gif" alt="" width="90" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on" bgcolor="#FFFAEB">
                        <IMG unselectable="on" id="imgMainTabOverlap3" src="/images/folder_Bottom.gif" alt="" width="90" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on" bgcolor="#FFFAEB" nowrap="" valign="bottom">
                        <IMG unselectable="on" id="imgMainTabOverlap4" src="/images/folder_Bottom.gif" alt="" width="91" height="6" border="0"/>
                        <IMG unselectable="on" src="/images/folder_Bottom.gif" alt="" width="2" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on" bgcolor="#FFFAEB">
                        <IMG unselectable="on" id="imgMainTabOverlap5" src="/images/folder_Bottom.gif" alt="" width="93" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on" bgcolor="#FFFAEB">
                        <IMG unselectable="on" id="imgMainTabOverlap6" src="/images/folder_Bottom.gif" alt="" width="93" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on" width="100%">
                        <IMG unselectable="on" src="/images/folder_Bottom.gif" alt="" width="100%" height="6" border="0"/>
                      </TD>
                      <TD unselectable="on">
                        <IMG unselectable="on" src="/images/folder_BRC.gif" alt="" width="5" height="6" border="0"/>
                      </TD>
                    </TR>
                    <TR>
                      <TD unselectable="on">
                        <IMG unselectable="on" src="/images/spacer.gif" alt="" width="10" height="1" border="0"/>
                      </TD>
                      <TD unselectable="on">
                        <IMG unselectable="on" id="imgMainTab1" src="/images/claim_sel.gif" alt="" style="cursor:hand" width="93" height="19" border="0" onClick="SetMainTab(1,'Claim.asp');setSB(0,top.sb);" onmouseover="window.status='Claim Info'; return true" onmouseout="window.status=''; return true" onFocus="if(this.blur)this.blur()"/>
                      </TD>
                      <TD unselectable="on">
                        <IMG unselectable="on" id="imgMainTab2" src="/images/veh_des.gif" alt="" style="cursor:hand" width="90" height="19" border="0" onClick="SetMainTab(2,'ClaimVehicleList.asp');setSB(0,top.sb);" onmouseover="window.status='Vehicle Info'; return true" onmouseout="window.status=''; return true" onFocus="if(this.blur)this.blur()"/>
                      </TD>
                      <TD unselectable="on">
                        <IMG unselectable="on" id="imgMainTab3" src="/images/prop_des.gif" alt="" style="cursor:hand" width="90" height="19" border="0" onClick="SetMainTab(3,'ClaimPropertyList.asp');setSB(0,top.sb);" onmouseover="window.status='Property Info'; return true" onmouseout="window.status=''; return true" onFocus="if(this.blur)this.blur()"/>
                      </TD>
                      <TD unselectable="on">
                        <IMG unselectable="on" id="imgMainTab4" src="/images/doc_des.gif" alt="" style="cursor:hand" width="93" height="19" border="0" onClick="SetMainTab(4,'ClaimDocument.asp');setSB(0,top.sb);" onmouseover="window.status='Document Info'; return true" onmouseout="window.status=''; return true" onFocus="if(this.blur)this.blur()"/>
                      </TD>
                      <TD unselectable="on">
                        <IMG style="visibility:hidden" unselectable="on" id="imgMainTab5" src="/images/subro_des.gif" alt="" width="93" height="19" border="0"/>
                      </TD>
                      <TD unselectable="on">
                        <IMG style="visibility:hidden" unselectable="on" id="imgMainTab6" src="/images/rental_des.gif" alt="" width="93" height="19" border="0"/>
                      </TD>
                      <TD unselectable="on" width="100%">
                        <IMG unselectable="on" src="/images/spacer.gif" alt="" width="100%" height="5" border="0"/>
                      </TD>
                      <TD unselectable="on">
                        <IMG unselectable="on" src="/images/spacer.gif" alt="" width="5" height="5" border="0"/>
                      </TD>
                    </TR>
                  </xsl:otherwise>
                </xsl:choose>
              </TABLE>
            </DIV>
            <DIV unselectable="on" id="layerNoTabs" border="0" cellspacing="0" cellpadding="0">
              <xsl:choose>
                <xsl:when test="$LynxID=''">
                  <xsl:attribute name="style">position:absolute; left: 4px; top: 616px; width: 1008px; height: 36px; visibility: visible</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="style">position:absolute; left: 4px; top: 616px; width: 783px; height: 36px; visibility: visible</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <TABLE unselectable="on" width="100%" cellpadding="0" cellspacing="0" border="0">
                <TR>
                  <TD unselectable="on">
                    <IMG unselectable="on" src="/images/folder_BLC.gif" alt="" width="5" height="6" border="0"/>
                  </TD>
                  <TD unselectable="on" width="100%">
                    <IMG unselectable="on" src="/images/folder_Bottom.gif" alt="" width="100%" height="6" border="0"/>
                  </TD>
                  <TD unselectable="on">
                    <IMG unselectable="on" src="/images/folder_BRC.gif" alt="" width="5" height="6" border="0"/>
                  </TD>
                </TR>
              </TABLE>
            </DIV>

            <DIV unselectable="on" id="layerOwners" style="position:absolute; width:200px; height:78px; z-index:1; left: 350px; top: 630px; display:none; border:1px solid #C0C0CO;padding:1px;background-color:#FFFFFF">
              <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%;border:1px solid #556B2F;background-color:#FFFFFF;cursor:default">
                <tr style="height:16px;text-align:center;font-weight:bold;background-color:#067BAF;color1:#FFFFFF;">
                  <td style="color:#FFFFFF">Entity Owners</td>
                </tr>
                <tr valign="top">
                  <td>
                    <div id="divScroller" class="autoflowDiv" style="overflow:auto;height:100%;width:100%;padding:3px;">
                      <table id="tblOwnerVehPrp" border="0" cellpadding="1" cellspacing="0" style="table-layout:fixed;border-collapse:collapse;width:100%;" datasrc="#xmlEntityOwners">
                        <colgroup>
                          <col width="60px"/>
                          <col width="*"/>
                        </colgroup>
                        <tr valign="top">
                          <td style="border-bottom:1px dotted #C0C0C0;height1:17px;">
                            <span datafld="Name" style="cursor:default;font:11px Tahoma,Verdana;width:100%;vertical-align:middle;overflow:hidden;text-overflow:ellipsis;white-space:nowrap"/>
                          </td>
                          <td style="border-bottom:1px dotted #C0C0C0;height1:17px;">
                            <table border="0" cellpadding="0" cellspacing="0">
                              <colgroup>
                                <col width="8px"/>
                                <col width="*"/>
                              </colgroup>
                              <tr>
                                <td style="color:#4B0082">
                                  O:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                                </td>
                                <td>
                                  <span datafld="Owner" class="DataValNoWrap" style="cursor:default;color:#4B0082;font:11px Tahoma,Verdana;width:100%;vertical-align:middle;overflow:hidden;text-overflow:ellipsis;white-space:nowrap" title="File Owner"/>
                                </td>
                              </tr>
                              <tr>
                                <td style="color:#D2691E">
                                  A:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                                </td>
                                <td>
                                  <span datafld="Analyst" class="DataValNoWrap" style="cursor:default;color:#D2691E;font:11px Tahoma,Verdana;width:100%;vertical-align:middle;overflow:hidden;text-overflow:ellipsis;white-space:nowrap" title="File Analyst"/>
                                </td>
                              </tr>
                              <tr>
                                <td style="color:#8A2BE2">
                                  S:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                                </td>
                                <td>
                                  <span datafld="Support" class="DataValNoWrap" style="cursor:default;color:#8A2BE2;font:11px Tahoma,Verdana;width:100%;vertical-align:middle;overflow:hidden;text-overflow:ellipsis;white-space:nowrap" title="File Support"/>
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td style="display:none">
                            <span datafld="EntityNum" class="DataValNoWrap"/>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
            </DIV>

            <!--include the Claim Header Info DIV-->
            <DIV unselectable="on" id="layerClaimData" style="position:absolute; width:200px; height:108px; z-index:1; left: 557px; top: 630px; visibility: hidden;">
              <TABLE unselectable="on" id="tblClaimHeader" width="100%" height="108" border="0" cellspacing="0" cellpadding="0" background="/images/claim%20folder.gif">
                <TR>
                  <TD unselectable="on" valign="top" nowrap="nowrap">
                    <IMG unselectable="on" src="/images/spacer.gif" width="132" height="14" border="0"/>
                  </TD>
                  <TD unselectable="on" valign="bottom" nowrap="nowrap" rowspan="2">
                    <img name="LimitIndicator" id="LimitIndicator" unselectable="on" src="/images/Exclamation.gif" border="0" title="This claim has coverage limits." style="display:none"/>
                    <SPAN unselectable="on" id="ClaimStatusImg">
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                    </SPAN>
                    <br/>
                    <SPAN unselectable="on" id="DemoClaim" style="position:absolute;top:25px;left:160px;font-weight:bold;color:#FF0000;z-index:2;display:none">
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;DEMO
                    </SPAN>
                  </TD>
                </TR>
                <TR>
                  <TD unselectable="on" valign="top" nowrap="nowrap">
                    <SPAN unselectable="on" class="boldText">
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;LYNX ID:
                    </SPAN><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<SPAN unselectable="on" id="tdLynxID">
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                    </SPAN>
                  </TD>
                </TR>
                <TR>
                  <TD unselectable="on" valign="top" colspan="2" nowrap="nowrap">
                    <SPAN unselectable="on" class="boldText">
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Claim #:
                    </SPAN><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<SPAN unselectable="on" id="tdClaimID">
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                    </SPAN>
                  </TD>
                </TR>
                <TR>
                  <TD unselectable="on" valign="top" colspan="2" nowrap="nowrap">
                    <SPAN unselectable="on" class="boldText">
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Insured:
                    </SPAN><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<SPAN unselectable="on" id="tdInsured">
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                    </SPAN>
                  </TD>
                </TR>
                <TR>
                  <TD unselectable="on" valign="top" colspan="2">
                    <SPAN unselectable="on" class="boldText">
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Loss Date:
                    </SPAN><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<SPAN unselectable="on" id="tdLoss">
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                    </SPAN>
                  </TD>
                </TR>
                <TR>
                  <TD unselectable="on" valign="top" colspan="2" nowrap="nowrap">
                    <SPAN unselectable="on" class="boldText">
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Ins. Co.:
                    </SPAN><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<SPAN unselectable="on" id="tdInsCo">
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                    </SPAN>
                  </TD>
                </TR>
                <!-- <TR>
          <TD unselectable="on" valign="top" colspan="2" nowrap="nowrap">
            <SPAN unselectable="on" class="boldText"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;Claim Rep:</SPAN><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<SPAN unselectable="on" id="tdClaimRep"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</SPAN>
          </TD>
        </TR> -->
              </TABLE>
            </DIV>
            <!-- <DIV unselectable="on" id="layerClaimVoid" style="position:absolute; width:194px; height:96px; z-index:2; left: 560px; top: 570px;FILTER:alpha(opacity=50);display:none;">
        <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%;background-color:#FFFFFF;">
          <tr>
            <td style="text-align:center;font-size:24px;font-weight:bold;color:#FF0000;letter-spacing:0.5mm " unselectable="on">VOID</td>
          </tr>
        </table>
    </DIV> -->
            <DIV id="layerClaimVoid" name="layerClaimVoid" style="z-index:2;position:absolute; left:560px; top:570px; width: 194px; height: 96px;FILTER:alpha(opacity=50);display:none">
              <table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:100%;background-color:#FFFFFF;FILTER:alpha(opacity=100)">
                <tr>
                  <td style="text-align:center;font-size:24px;font-weight:bold;color:#FF0000;letter-spacing:0.5mm ">
                    VOID
                  </td>
                </tr>
              </table>
            </DIV>
            <!--include the APD Logo DIV-->
            <DIV unselectable="on" style="position:absolute; z-index:1; left: 7px; top: 680px;">
              <img src="/images/apdlogo.gif" alt="" width="154" height="36" border="0"/>
            </DIV>
          </DIV>

          <!--   Changed ShowEstimate in Estimates to be a showModelessDialog window
    <iframe id="ifrmEstimateDoc" name="ifrmEstimateDoc" style="position:absolute;top:80px;left:790px;height:475px;width:420px;z-index:10;display:none;overflow:hidden;filter:progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#000000,strength=5);"/>
 -->
          <xsl:if test="$LynxID!=''">
            <script>
              createSubWindow("winDiary", false, "Diary - TODO List", "forum.gif", 630555); // ~ 10.5 min
              createSubWindow("winNotes", true, "Notes", "win_logo2.gif", 320000);  // ~ 5.3 min
            </script>
            <script>setSB(90,sb)</script>
          </xsl:if>

        </BODY>
      </HTML>
    </xsl:if>
    <xsl:if test="count(User)=0 or User/DesktopPermission/@CRD=0">
      <xsl:text>Please check your login information.  If you continue to have problems, contact your supervisor.</xsl:text>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>