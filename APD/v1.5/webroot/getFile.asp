<%
  dim docPath
  docPath = request.QueryString("doc")

  if docPath <> "" then
    Dim oFSO, oStream
    Dim sFormat
    
    set oFSO = Server.createObject("Scripting.FileSystemObject")
    set oStream = Server.createObject("ADODB.Stream")
    if oFSO.FileExists(docPath) then
      sFormat = UCase(oFSO.GetExtensionName(docPath))
      SELECT CASE sFormat
        Case "DOC"
          Response.ContentType = "application/msword"
        Case "XLS"
          Response.ContentType = "application/x-msexcel"
        Case "RTF"
          Response.ContentType = "application/msword"
        Case "BMP"
          Response.ContentType = "image/bmp"
        Case "GIF"
          Response.ContentType = "image/gif"
        Case "JPG"
          Response.ContentType = "image/jpeg"
        Case "TIF", "TIFF"
          Response.ContentType = "image/tiff"
        Case "PDF"
          Response.ContentType = "application/pdf"
        Case ELSE
          Response.ContentType = "application/pdf"
      END SELECT
      Response.AddHeader "Content-Disposition", "inline;filename=" & oFSO.GetBaseName(docpath) & "." & oFSO.GetExtensionName(docPath)
      oStream.Charset = "UTF-8"
      oStream.Type = 1 'Binary
      oStream.Open
      oStream.LoadFromFile(docPath)
      Response.BinaryWrite oStream.Read()
    else
      response.write "<pre>" & docPath & "<br>does not exist or user does not have permission. " & request.ServerVariables("AUTH_USER") & " </pre>"
    end if
    
    set oFSO = nothing
    set oStream = nothing
  end if
%>