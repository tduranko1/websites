<%@ LANGUAGE="VBSCRIPT"%>
<% Option Explicit %>
<!-- #include file="errorhandler.asp" -->
<%
	'Builds up display of the internal messages.
    Function GetInternalHtml( strInternal )
        On Error Resume Next
        GetInternalHtml = "<td></td><td>Internal Event Data:<br>" & vbCrLf _
			& "<textarea name='txtXML' wrap='off' rows='25' cols='92'/>" & vbCrLf _
            & strInternal & "</textarea></td>"
    End Function

	'Builds up display of the external messages.
    Function GetExternalHtml( strExternal )
        On Error Resume Next

        Dim strExt, arMsgs, strHTML

        If Len(strExternal) > 0 Then

            arMsgs = Split( strExternal, "|" )

            For Each strExt In arMsgs
                strHTML = strHTML & "<tr><td><img src='/images/event_" _
                    & Left(strExt,1) & ".bmp'/>&nbsp</td><td align='left'><strong>" _
                    & Mid(strExt,3) & "</strong></td></tr>"
            Next

            GetExternalHtml = strHTML

        End If
    End Function

    Dim blnAdvanced, objExe, objEvents
    Dim strInternal, strExternal
    Dim strErrNumber, strErrSource, strErrDescription

    On Error Resume Next

    'Retrieve the event data before going to work.
	'Server side errors will come through the cookies.
	If Request("Number") = "" Then
        strErrSource = Request.Cookies("APDError")("Source")
        strErrNumber = Request.Cookies("APDError")("Number")
        strErrDescription = Request.Cookies("APDError")("Description")
	'Client side errors will come through the query string.
	Else
        strErrSource = Request.QueryString("Source")
        strErrDescription = Request.QueryString("Description")
        strErrNumber = CLng( CLng(Request.QueryString("Number")) + CLng(&H80080000) )
	End If

	'Save back in case 'Advanced' is clicked.
	SaveErrorData strErrNumber, strErrSource, strErrDescription

	If IsEmpty(strErrNumber) Or Not IsNumeric(strErrNumber) Then
		strErrNumber = 0
	End If

    'Create and initialize CExecute object.
    Set objExe = CreateObject("DataPresenter.CExecute")
    objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")

    'Object var pointing to CEvents object.
    Set objEvents = objExe.mToolkit.mEvents

    'Build messages and log events internally.
    objEvents.HandleEvent strErrNumber, strErrSource, strErrDescription

    'Get the internal and external messages.
    strInternal = Err.Description
	strExternal = objEvents.ExternalMessage

    'Get the debug mode status.
    blnAdvanced = CBool( "True" = objExe.Setting("EventHandling/ShowAdvancedButton") )

    Set objEvents = Nothing
    Set objExe = Nothing

    'Ensure that an external message exists at this point.
    If Len(strExternal) = 0 Then
        strExternal = "1>An internal error has occurred.  Contact your IS administrator."
    End If

	'Now build the internal and external html from the messages.
	strInternal = GetInternalHtml( strInternal )
	strExternal = GetExternalHtml( strExternal )

	'Do we need to show the internal message?
	Dim strShowInternal
	strShowInternal = Request("ShowInternal")
%>

<html>
<head>
	<title>Lynx Select</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
</head>
<body bgcolor="#FFFFFF" text="#000000">
	<script language="JavaScript">
		function OpenAdvanced() {

    		var sDimensions = "dialogHeight:568px; dialogWidth:824px; "
    		var sSettings = "resizable:yes; status:no; help:no; center:yes;"
    		var sQuery = "EventPopup.asp?ShowInternal=True";

    		window.showModalDialog( sQuery, window.parent, sDimensions + sSettings );
		}
	</script>
	<br>
    <table width="80%" align="center">
<%
    'Show the internal message, or hide it in a comment?
	If strShowInternal <> "True" Then
		Response.Write strExternal
		Response.Write "<!--" & strInternal & "-->"
	Else
		Response.Write "<tr>" & strInternal & "</tr>"
	End If
 %>
		<tr>
			<td>&nbsp;</td>
		</tr>
<%
    If blnAdvanced = False Then
 %>
		<tr align="middle">
			<td></td>
			<td>
				<input type='button' class='formbutton' name='closeMe' value='Close' onClick='window.close()'/>
			</td>
		</tr>
<%
	Else
 %>
		<tr align="middle">
			<td></td>
			<td>
				<input type='button' class='formbutton' name='closeMe' value='Close' onClick='window.close()'/>
<%
		If strShowInternal <> "True" Then
 %>
				<input type='button' class='formbutton' name='showInternal' value='Advanced' onClick='OpenAdvanced()'/>
<%
		End If
 %>
			</td>
		</tr>
<%
	End If
 %>
	</table>
</body>
</html>
