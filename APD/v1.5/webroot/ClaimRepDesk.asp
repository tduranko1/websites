<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
    On Error Resume Next

    Dim objExe
    Call GetHtml
    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()

        Dim strUserID
        strUserID = GetSession("UserID")

        'Create and initialize DataPresenter.CExecute object.
        Dim objExe
        Set objExe = CreateObject("DataPresenter.CExecute")

        objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
        
        objExe.AddXslParam "UserId", strUserID

        'Get XML and transform to HTML.
        Response.Write objExe.ExecuteSpAsXML( "uspAPDRAAUserGetListXML", "ClaimRepDesk.xsl", strUserID )

    End Sub
%>
