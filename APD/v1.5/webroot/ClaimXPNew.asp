<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SessionStuffer.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
    On Error Resume Next

    Dim objExe
    Call GetHtml
    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()
    
        Dim strLynxID
        Dim strUserID
        Dim strInsuranceCompanyID
        Dim strClaim_ClaimAspectID
        Dim strVeh_ClaimAspectID
        Dim strVehicleListXML
        Dim oXML
        Dim strVehNum
        Dim oVehNode

        strLynxID = Request("LynxID")
        If strLynxID = "" Then
            strLynxID = GetSession("LynxID")
        End If

        strUserID = GetSession("UserID")
        strVehNum = Request("VehNum")

        Set objExe = CreateObject("DataPresenter.CExecute")
        objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID

        'response.write(Request.ServerVariables("APPL_PHYSICAL_PATH"))
        'response.end

        ' Place claim information in session manager.
        StuffClaimSessionVars objExe.ExecuteSpAsXML( "uspSessionGetClaimDetailXML", "", strLynxID ), sSessionKey

        strClaim_ClaimAspectID = GetSession("Claim_ClaimAspectID")
        strInsuranceCompanyID = GetSession("InsuranceCompanyID")
        
        'strVehicleListXML = objExe.ExecuteSpAsXML( "uspClaimVehicleGetListXML", "", strLynxID, strInsuranceCompanyID )
        
        'set oXML = Server.CreateObject("MSXML2.DOMDocument")
        'oXML.async = false
        'oXML.loadXML strVehicleListXML
        
        'set oVehNode = oXML.selectSingleNode("/Root[@LynxID='" & strLynxID & "']/Vehicle[@VehicleNumber='" + strVehNum + "']")
        
        'if not oVehNode is nothing then
        '  strVeh_ClaimAspectID = oVehNode.getAttribute("ClaimAspectID")
          ' Entity has been touched!
        '  objExe.ExecuteSp "uspWorkflowSetEntityAsRead", strVeh_ClaimAspectID, strUserID
        'end if
        
        set oXML = nothing
        set oVehNode = nothing

        'Dim strImageRootDir
        'strImageRootDir = objExe.Setting("Document/RootDirectory")

        Dim strIMPSEmailBCC
        strIMPSEmailBCC = objExe.Setting("IMPSEmail/BCC")
		
        'objExe.AddXslParam "ImageRootDir", strImageRootDir
        objExe.AddXslParam "IMPSEmailBCC", strIMPSEmailBCC
        objExe.AddXslParam "VehNum", strVehNum

        ' Get claim XML and transform to HTML.
        'Response.Write objExe.ExecuteSpAsXML( "uspClaimCondGetDetailXML", "ClaimXP.xsl", strLynxID, strInsuranceCompanyID )

		'----------------------------------------------------------------------------------------------------------------'
		' ASPNet Code - Start
		'----------------------------------------------------------------------------------------------------------------'
		'------------------------------------'
		' Local Params
		'------------------------------------'
        dim sParams
        sParams = "VehNum:" & strVehNum
        'sParams = sParams + "|ImageRootDir:" & strImageRootDir
        sParams = sParams + "|IMPSEmailBCC:" & strIMPSEmailBCC

		'------------------------------------'
		' Setup Session Data
		'------------------------------------'
		Dim sASPXSessionKey, sASPXWindowID
		sASPXSessionKey = sSessionKey	
		sASPXWindowID = sWindowID
		
        'response.write("sSessionKey - " & sSessionKey)
        'response.write("<br/>")
        'response.write("sWindowID - " & sWindowID)
        'response.write("<br/>")
        'response.write("strLynxID - " & strLynxID)
        'response.write("<br/>")
        'response.write("strUserID - " & strUserID)
        'response.write("<br/>")
        'response.write("strVehNum - " & strVehNum)
        'response.write("<br/>")
        'response.write("strClaim_ClaimAspectID - " & strClaim_ClaimAspectID)
        'response.write("<br/>")
        'response.write("strInsuranceCompanyID - " & strInsuranceCompanyID)
        'response.write("<br/>")
        'response.write("strImageRootDir - " & strImageRootDir)
        'response.write("<br/>")
        'response.write("strIMPSEmailBCC - " & strIMPSEmailBCC)
		'response.end

		'------------------------------------'
		' Pass control over to a dot net
		' version of the DataPresenter
		'------------------------------------'
		response.redirect("ClaimXP.aspx?SessionKey=" & sASPXSessionKey & "&WindowID=" & SASPXWindowID & "&Params=" & sParams)
    End Sub
%>

