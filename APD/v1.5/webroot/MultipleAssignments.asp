<%@ LANGUAGE="VBSCRIPT"%>
<% option explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<%
    
	On Error Resume Next

  dim strUserID,strClaimAspectID,strInsuranceCompanyID
  dim objExe
  dim strHTML
     
   ReadSessionKey()
	
    strClaimAspectID = Request("ClaimAspectID")  
	strInsuranceCompanyID = Request("InsuranceCompanyID")
    
	strUserID = GetSession("UserID")
	
	'Create and initialize DataPresenter
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()
    
	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey
	CheckError()
    
	'Get Multple Assignments  XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspVehicleAssignGetDetailXML", "MultipleAssignments.xsl", strClaimAspectID, strInsuranceCompanyID)
	CheckError()

	Set objExe = Nothing

	Response.Write strHTML

	On Error GoTo 0
	
%>

