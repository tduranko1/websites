<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
    On Error Resume Next

    Dim objExe
    Call GetHtml
    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()

        Dim strLynxID
        Dim strUserID
        Dim strInvolvedID
        Dim strInsuranceCompanyID

        strLynxID = GetSession("LynxID")
        strUserID = GetSession("UserID")
        strInsuranceCompanyID = GetSession("InsuranceCompanyID")
        strInvolvedID = Request("InvolvedID")

        'Create and initialize DataPresenter
        Set objExe = CreateObject("DataPresenter.CExecute")

        objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID

        'Parameters to pass into XSL.  Note that UserID and SessionKey were set in Initialize.
        objExe.AddXslParam "LynxId", strLynxID

        'Get XML and transform to HTML.
        Response.Write objExe.ExecuteSpAsXML( "uspClaimWitnessGetDetailXML", "ClaimWitness.xsl", strInvolvedID, strInsuranceCompanyID )

    End Sub
%>
