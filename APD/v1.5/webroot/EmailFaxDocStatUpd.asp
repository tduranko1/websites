<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<%

  Dim oExe, oXML, oEvents
  Dim sLynxID, sUserID, sInsCoID, strFilePath
  Dim strClaimAspectID, strMessageNoteTypeID, strVehicleStatusID, strNotesMsg, strUserID
  Dim strNotifyEventSP, strDocSentEventID
  Dim strErrors
  
  strUserID = 0

  set oXML = Server.CreateObject("MSXML2.DOMDocument")
  oXML.async = false
  oXML.load(request)
  'oXML.loadXML request("txtDocumentsXML")
  
  if oXML.parseError.errorCode <> 0 then
    response.write "<Error><![CDATA[Error parsing the XML request at Line #: " & CStr(oXML.parseError.line) & " at character position: " & CStr(oXML.parseError.linepos) & ". Reason: " & oXML.parseError.reason & "]]></Error>"
    response.end
  end if
  
  
  strClaimAspectID = getXMLValue("/ClientDocuments/NotesMsg/@claimAspectID")
  strVehicleStatusID = getXMLValue("/ClientDocuments/NotesMsg/@vehicleStatusID")
  strMessageNoteTypeID = getXMLValue("/ClientDocuments/NotesMsg/@noteTypeID")
  strNotesMsg = getXMLValue("/ClientDocuments/NotesMsg")
  strUserID = getXMLValue("/ClientDocuments/NotesMsg/@userID")
  
  if strErrors <> "" then
    response.write "<Error><![CData[" & strErrors & "]]></Error>"
  else
    response.write submitData
  end if
  
  
'  response.write "strClaimAspectID=" & strClaimAspectID & "<br/>"
'  response.write "strVehicleStatusID=" & strVehicleStatusID & "<br/>"
'  response.write "strMessageNoteTypeID=" & strMessageNoteTypeID & "<br/>"
'  response.write "strNotesMsg=" & strNotesMsg & "<br/>"
'  response.write "strUserID=" & strUserID & "<br/>"
  

  function submitData()
    on error resume next
    dim lsRet, strNoteInsSP
    Dim oDoc, oDocs
    Dim strDocumentID, strPassThru, aPassThru, aKeyValue, strKey, strValue
    Dim strDocStatusUpdSP, i, blnNotesAdded
    
    blnNotesAdded = false
    
    Set oExe = CreateObject("DataPresenter.CExecute")
    
    'set oEvents =  oExe.mToolkit.mEvents
    'oEvents.ComponentInstance = "Document Status Update"
    'oExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, sUserID
    oExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH") & "..\config\config.xml"
    
    strNoteInsSP = oExe.Setting("ClaimPoint/SPMap/NoteInsDetail")
    strDocStatusUpdSP = oExe.Setting("PartnerSettings/UpdateDocumentSendStatus")
    strNotifyEventSP =  oExe.Setting("ShopAssignment/EventSpName")
    strDocSentEventID =  oExe.Setting("ClientDocument/Event[@name='Documents Sent to Carrier']/@id")
    
    if err.number <> 0 then
      strErrors = err.description
      err.clear
    end if
    
    
    'now update the status for each document
    set oDocs = oXML.selectNodes("//Document")
    if not oDocs is nothing then
      for each oDoc in oDocs
        strFilePath = oDoc.selectSingleNode("FilePath").text
        strPassThru = oDoc.selectSingleNode("PassThru").text
        if trim(strPassThru) <> "" then
           aPassThru = split(strPassThru, "|")
           
           for i = LBound(aPassThru) to UBound(aPassThru)
             if aPassThru(i) <> "" then
                aKeyValue = Split(aPassThru(i), "=")
                strKey = aKeyValue(0)
                strValue = aKeyValue(1)
               
                if strKey = "DocumentID" then
                  'oEvents.Trace "  Updating status for Document id: " & strValue & vbcrlf, ""
                  oExe.ExecuteSpNp strDocStatusUpdSP, "DocumentID=" & strValue & "&SendStatusCD=S"
                  
                  if err.number <> 0 then
                    strErrors = strErrors & vbcrlf & err.description
                    'oEvents.Trace "  Error: " & err.description & vbcrlf, ""
                    err.clear
                  end if
                end if
             end if
           next
        end if
      next
    end if
    
    err.clear
    
    'response.write "Adding notes...<br/>"
    
    'oEvents.Trace "  Adding note to the claim ... " & strValue & vbcrlf, ""
    if err.number <> 0 then
      strErrors = strErrors & vbcrlf & err.description
      'oEvents.Trace "  Error: " & err.description & vbcrlf, ""
      err.clear
    end if
    
    'response.write "strNotesMsg=" & strNotesMsg & "<br/>"
    'response.end
    
    'now add a note that the documents were sent
    if blnNotesAdded = false then
       lsRet = oExe.ExecuteSpNp(strNoteInsSP, "ClaimAspectID=" & strClaimAspectID & "&NoteTypeID=" & strMessageNoteTypeID & _
                                 "&StatusID=" & strVehicleStatusID & "&Note=" & strNotesMsg & "&UserID=" & strUserID)
       blnNotesAdded = true
    end if
    'response.write lsRet & "<br/>"
    
    'response.write "strNotifyEventSP=" & strNotifyEventSP & "<br/>"
    
    if strNotifyEventSP <> "" then
      'oEvents.Trace "  Throwing event id " & strDocSentEventID & " ... " & strValue & vbcrlf, ""
      if err.number <> 0 then
        strErrors = strErrors & vbcrlf & err.description
        'oEvents.Trace "  Error: " & err.description & vbcrlf, ""
        err.clear
      end if
      'response.write "throwing event<br/>"
      oExe.ExecuteSpNp strNotifyEventSP, "EventID=" & strDocSentEventID & _
                                     "&ClaimAspectID=" & strClaimAspectID & _
                                     "&Description=Documents Sent to Carrier" & _
                                     "&ConditionValue=" & _
                                     "&UserID=" + strUserID
    end if
    
    if err.number <> 0 then
      lsRet = "<Error><![CDATA[An Error occurred. Error Source: " & err.source & "; Description: " & err.description & "]]></Error>"
    end if
    set oExe = nothing
    submitData = lsRet
  end function
  
  Function getXMLValue(strXPath)
    dim strRet, oNode      
    strRet = ""      
    if not oXML is nothing then
      if strXPath <> "" then
        set oNode = oXML.selectSingleNode(strXPath)
        if not oNode is nothing then
          strRet = oNode.text
        end if
      end if
    end if
    getXMLValue = strRet
  End Function
%>
