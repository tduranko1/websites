<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- #include file="errorhandler.asp" -->

<%
	Dim strLynxID, strDocument, strPertains

	On Error Resume Next

	strLynxID = Request("CurrentLynxID")
	strDocument = Request("Document")
	strPertains = Request("Pertains")

	CheckErr()
%>

<HTML>
<HEAD>
  <title>Document ReAssign</title>
<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
<LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>

<!-- for select tag -->
<STYLE type="text/css">
SELECT
{
  background : url(../images/INPUT_bkgd.png) no-repeat;
  background : #White;
  font-family : Tahoma, Arial, Helvetica, sans-serif;
  font-size : 11px;
}
</STYLE>

<SCRIPT language="JavaScript" src="/js/images.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>

<SCRIPT language="JavaScript">

var arChannelLists = new Array();

function getExposures(sLynxID)
{
  if (sLynxID == "") return;
  sLynxID = parseNumbersOnly(sLynxID);

  // check for a valid LynxID
  var co = RSExecute("/rs/RSValidateLynxID.asp", "IsValidateLynxID", sLynxID);
  var sTblData = co.return_value;
  var listArray = sTblData.split("||");
  if (listArray[1] == 1)
  {
    if (listArray[0] == 0)
    {
      document.getElementById("txtLynxID").value = "Invalid ID";
      updSelect(",",'selExposuresList');
      updSelect(",",'selServiceChannelList');
      document.getElementById("txtLynxID").select();
      document.getElementById("txtLynxID").focus();
    }
    else
    {
      // get list of Exposures
      var co = RSExecute("/rs/RSADSAction.asp", "RadExecute", "uspClaimExposureGetListXML", "LynxID=" + sLynxID);
    	if (co.status == 0)
      {
        var sTblData = co.return_value;
        var listArray = sTblData.split("||");
        if ( listArray[1] == "0" )
          ServerEvent();
        else
        {
          var objXML = new ActiveXObject("Microsoft.XMLDOM");
          objXML.loadXML(listArray[0]);

					arChannelLists = new Array();

          var objNodeList = objXML.documentElement.selectNodes("/Root/Exposure");
          var objNodeListLength = objNodeList.length;
          var sExposures = "";
          for (i=0; i<objNodeListLength; i++)
          {
            sExposures += objNodeList[i].getAttribute("Name") + "," + objNodeList[i].getAttribute("ClaimAspectID") + "," + objNodeList[i].getAttribute("AssignmentID");
            if (i == objNodeListLength-1)
              sExposures += "";
            else
              sExposures += ",";

          	var objChannelList = objNodeList[i].selectNodes("ServiceChannel");
          	var objChannelListLength = objChannelList.length;
          	var sChannels = "";

          	for (j=0; j<objChannelListLength; j++)
          	{
          	  sChannels += objChannelList[j].getAttribute("ServiceChannelName") + "," +
								objChannelList[j].getAttribute("ClaimAspectServiceChannelID") + "," +
								objChannelList[j].getAttribute("AssignmentID");
          	  if (j == objChannelListLength-1)
          	    sChannels += "";
          	  else
          	    sChannels += ",";
          	}

						arChannelLists.push( sChannels );
					}

      		var expArray = sExposures.split(",");
          updSelect(expArray,'selExposuresList');
          document.getElementById("selExposuresList").focus();
        }
      }
      else ServerEvent();
    }
  }
  else
  {
    ClientWarning("Unable to verify the Lynx ID that you entered. Please contact the system administrator.");
    return;
  }
}

function updSelect(sArray, sElement)
{
  var loSel = document.getElementById(sElement);
	var liCurr = loSel.options.length;
  // update select
	for (var j=liCurr; j>0; j--)
  {
		loSel.options[j] = null;
  }
	for (var i=0; i<sArray.length; i++)
	{
		loSel.options[loSel.options.length] = new Option(sArray[i], sArray[i+1]);
    loSel.options[loSel.options.length-1].setAttribute("assignmentID", sArray[i+2]);
		i+=2;
	}
}

// Updates the service channel combo box when the exposure changes.
function updChannels()
{
		var selExp = document.getElementById("selExposuresList");
		var idx = selExp.selectedIndex;

		// Account for blank first element in combo.
		if ( idx > 0 )
		{
				var sChannels = arChannelLists[ idx - 1 ];
    		var chArray = sChannels.split(",");
    		updSelect(chArray,"selServiceChannelList");
    		document.getElementById("selServiceChannelList").focus();
		}
		else
		{
    		updSelect(",","selServiceChannelList");
		}
}

function ReAssignDocument()
{
	var sLynxID = document.getElementById('txtLynxID').value;
  var objSel = document.getElementById('selExposuresList');
  var objChan = document.getElementById("selServiceChannelList");
	var sExposure = objSel.options[objSel.selectedIndex].text;
  var sAspectID = objSel.options[objSel.selectedIndex].value;
	var sChannel = objChan.options[objChan.selectedIndex].text;
	var sChannelID = objChan.options[objChan.selectedIndex].value
  var sAssignmentID = objChan.options[objChan.selectedIndex].getAttribute("assignmentID");

	if ( sLynxID != "Invalid ID" && sLynxID.length > 0 && sExposure == "" )
	{
  	ClientWarning("Invalid 'Pertains To'.<br> Please select a 'Pertains To' from the drop-dowm.");
		return;
	}
	else if ( sLynxID != "Invalid ID" && sLynxID.length > 0 && sChannel == "")
	{
  	ClientWarning("Invalid 'Service Channel'.<br> Please select a 'Service Channel' from the drop-dowm.");
		return;
	}
	else if ( sLynxID == "Invalid ID" || (sLynxID == "" && sExposure == "" && sChannel == "") )
	{
    window.close();
	}
  else
  {
    var sReturnVal = sLynxID + "," + sExposure + "," + sAspectID + "," + sAssignmentID + "," + sChannel + "," + sChannelID;
    window.returnValue = sReturnVal;
    window.close();
  }
}

</SCRIPT>

</HEAD>
<BODY unselectable="on" class="bodyAPDSub" style="margin:0px 0px 0px 0px; padding:10px">
<SCRIPT language="JavaScript" src="/_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("/_ScriptLibrary/");</SCRIPT>

<DIV unselectable="on" style="width:104%; height:92%; background-color:#FFFFFF; border:1px solid #333377; padding:10px;">

  <DIV align="center">
    <b>Reassign Document to Another Claim</b>
  </DIV>
  <br>

  <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="2">
    <TR unselectable="on">
      <TD unselectable="on" nowrap>Current LynxID:</TD>
      <TD unselectable="on" nowrap><%=strLynxID%></TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" nowrap>Document:</TD>
      <TD unselectable="on" nowrap><%=strDocument%></TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on" nowrap>Pertains to:</TD>
      <TD unselectable="on" nowrap><%=strPertains%></TD>
    </TR>
  </TABLE>

  <TABLE unselectable="on" border="0" cellspacing="0" cellpadding="2">
    <TR unselectable="on">
      <TD unselectable="on" nowrap>New LynxID:</TD>
      <TD unselectable="on" nowrap>
        <INPUT class="InputField" type="text" id="txtLynxID" size="8" maxlength="15" value="" onBlur="getExposures(this.value);" onKeypress="if (event.keyCode==13) getExposures(this.value); else NumbersOnly(event);" />
      </TD>
      <TD unselectable="on" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
      <TD unselectable="on" nowrap>Pertains to:</TD>
      <TD unselectable="on" nowrap width="100%">
        <select id="selExposuresList" name="ExposuresList" onblur="updChannels()">
          <option value="" assignmentID=""></option>
        </select>
      </TD>
    </TR>
    <TR unselectable="on">
      <TD unselectable="on">&nbsp;</TD>
      <TD unselectable="on" align="right" colspan="3">Service Channel:</TD>
      <TD unselectable="on" nowrap width="100%">
        <select id="selServiceChannelList" name="ServiceChannelList">
          <option value="" serviceChannel=""></option>
        </select>
      </TD>
    </TR>
  </TABLE>
  <br>

  <P align="right">
  	<input type="button" class="formbutton" onclick="ReAssignDocument();" value="ReAssign"/>
    &nbsp;&nbsp;&nbsp;&nbsp;
  	<input type="button" class="formbutton" onclick="window.close();" value="Close"/>
  </P>

</DIV>

</BODY>
</HTML>
