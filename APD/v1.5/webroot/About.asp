<%@ Language=VBScript %>
<% Option Explicit %>
<!-- About.asp
    Puts up useful 'about' information like version numbers.
-->
 
<!-- #include file="errorhandler.asp" -->
<%
    Dim objExe, objRS

    On Error Resume Next
    StatusInfo
    CheckError

    Set objRS = Nothing
    Set objExe = Nothing

    On Error GoTo 0

    Function StatusInfo()
        On Error Goto 0

        Set objExe = CreateObject("DataPresenter.CExecute")
        objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")

        Response.Write "<html><head><title>About Lynx APD</title></head><body>"
        Response.Write "<table align='center' cellpadding='2' border='0' cellspacing='0'>"

        Response.Write "<tr><td colspan='2'>&nbsp;</td></tr>"

        Response.Write "<tr><td align='center' colspan='2'><img src='images\apdlogo_white.gif'/></td></tr>"

        Response.Write "<tr><td colspan='2'>&nbsp;</td></tr>"

        Response.Write "<tr><td align='right'>Application:</td><td>" _
            & objExe.mToolkit.mEvents.mSettings.Application & "</td></tr>"

        Response.Write "<tr><td align='right'>Current User:</td><td>" _
            & Request.ServerVariables("AUTH_USER") & "</td></tr>"

        Response.Write "<tr><td align='right'>Environment:</td><td>" _
            & objExe.mToolkit.mEvents.mSettings.Environment & "</td></tr>"

        Response.Write "<tr><td align='right'>Server:</td><td>" _
            & UCASE(Request.ServerVariables("server_name")) & "</td></tr>"

        Set objRS = objExe.ExecuteSpAsRS("uspSysVersionGetList")

        Response.Write "<tr><td align='right'>APD Database Version:</td><td>" _
            & objRS.Fields.Item(0) & "</td></tr>"

        objExe.ConnectionStringParent = "PartnerSettings"
        Set objRS = objExe.ExecuteSpAsRS("uspSysVersionGetList")

        Response.Write "<tr><td align='right'>Partner Database Version:</td><td>" _
            & objRS.Fields.Item(0) & "</td></tr>"

        Response.Write "<tr><td align='right'>Website Version:</td><td>" _
            & objExe.Setting("@version") & "</td></tr>"

        Response.Write "</table></body></html>"

        Set objRS = Nothing
        Set objExe = Nothing

    End Function
%>
