<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	On Error Resume Next

    Dim strUserID, strLynxID, strInsuranceCompanyID

	strUserID = Request("UserId")
	strLynxID = Request("LynxId")
	strInsuranceCompanyID = Request("InsuranceCompanyID")

	On Error GoTo 0
%>

<HTML xmlns:IE>
<HEAD>
  <title>APD Document Upload</title>

<LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>

<STYLE type="text/css">
  IE\:APDButton {behavior:url("/behaviors/APDButton.htc")}
  IE\:APDCheckBox {behavior:url("/behaviors/APDCheckBox.htc")}
</STYLE>

<SCRIPT language="JavaScript" src="/js/APDControls.js"></SCRIPT>
<SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>

<SCRIPT language="JavaScript">
var iDlgTop, iDlgLeft, iDlgWidth, iDlgHeight;
var bDlgResized = false;

iDlgTop = window.dialogTop;
iDlgLeft = window.dialogLeft;
iDlgWidth = window.dialogWidth;
iDlgHeight = window.dialogHeight;

function reloadDocDetailsIfrm()
{
  var sIfrmURL = "DocumentEstDetails.asp?LynxId=<%= strLynxID %>&InsuranceCompanyID=<%= strInsuranceCompanyID %>&UserId=<%= strUserID %>&FileUploadFlag=1&DisplayPricingFlag=0";
  document.getElementById("ifrmDocDetails").src = sIfrmURL;
}

function viewDoc()
{
  var ifrmDoc = document.getElementById("ifrmDocUploadBrowse").contentWindow;
  var lsFiles = ifrmDoc.document.getElementById("filUpload").value;
  if (lsFiles != "")
  {
    var aFiles = lsFiles.split(";");
    var lsFile = aFiles[0];
    if (lsFile.indexOf(".") == -1) return;
    if (lsFile.substr(lsFile.length - 5, 4).toLowerCase() == ".pdf"){
      var oWS = new ActiveXObject("WScript.Shell");
      if (oWS) {
        try {
          oWS.Run(lsFile);
          if (bDlgResized) {
            window.dialogTop = iDlgTop;
            window.dialogLeft = iDlgLeft;
            window.dialogWidth = iDlgWidth;
            window.dialogHeight = iDlgHeight;
            document.getElementById("ifrmDocDisplay").style.display = "none";
            document.getElementById("ifrmDocDisplay").src = "/blank.asp";
          }
        } catch (e) {}
      }
      else {
        ClientWarning("Unable to create Windows Scripting Host. Please contact Help desk.");
      }
    } else {
      lsFile = lsFile.replace(/\\/g, "\\\\"); //escape the back-slash
      lsFile = lsFile.replace(/\"/g, ""); // remove the double quote "

      var sHREF = "/EstimateDocView.asp?docPath=" + lsFile;
      var iTop = (window.screen.availHeight - 660) / 2;
      var iLeft = (window.screen.availWidth - 800) / 2;
      window.dialogTop = iTop + "px";
      window.dialogLeft = iLeft + "px";
      window.dialogWidth = "800px";
      window.dialogHeight = "660px";
      document.getElementById("ifrmDocDisplay").style.display = "inline";
      document.getElementById("ifrmDocDisplay").src = sHREF;
      bDlgResized = true;
    }
  }
  else
  {
    ClientWarning("Please specify a file first in the field above.");
    return false;
  }
}


</SCRIPT>

</HEAD>
<BODY class="bodyAPDSubSub" unselectable="on" style="padding:6px">

  <p align="center"><SPAN style="color: #000066; font-weight: bold; font-size: 12px;">Upload a New Document</SPAN></p>
  <br>
  <strong>Specify the File(s) to Upload:</strong> (2 MB size limit for each file)

  <IFRAME id="ifrmDocUploadBrowse" src="DocumentFileUploadBrowse.asp" frameborder="0" style="width:100%; height:30px; border:0px;" tabindex="-1"></IFRAME>

  <div unselectable="on" style="height:30px; border:0px; vertical-align:middle;">
    <IE:APDButton id="btnViewDoc" name="btnViewDoc" value="View Document" onButtonClick="viewDoc()" title="View Document" CCTabindex="3" />
    <img src="/images/spacer.gif" alt="" width="120" height="1" border="0"/>
    <IE:APDCheckBox id="cbDelFile" name="cbDelFile" value="0" caption="Delete original file after upload" title="Mark the checkbox to delete original file" CCTabindex="4" />
  </div>

  <IFRAME id="ifrmDocDetails" src="DocumentEstDetails.asp?LynxId=<%= strLynxID %>&InsuranceCompanyID=<%= strInsuranceCompanyID %>&UserId=<%= strUserID %>&FileUploadFlag=1&DisplayPricingFlag=0" frameborder="0" style="width:100%; height:200px; border:0px" tabindex="5"></IFRAME>

  <IFRAME id="ifrmDocDisplay" src="blank.asp" frameborder="0" style="display:none; width:100%; height:320px; border:0px;" tabindex="-1"></IFRAME>

</BODY>
</HTML>
