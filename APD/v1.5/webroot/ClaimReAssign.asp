<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	Dim strUserID
	Dim strHTML
	Dim strLynxID, strClaimAspectID, strEntity, strCheckListID, strClaimAspectIDList, arrClaimAspectID, strAssignmentID
  Dim strDesc, strAlarmDate, strSysLastUpdatedDate

	On Error Resume Next

	strUserID = GetSession("UserID")
	strLynxID = Request("LynxID")
	if strLynxID = "" then
		strLynxID = GetSession("LynxID")
	end if
  
  strClaimAspectIDList = Request("claimAspectID")
  strEntity = Request("Entity")
  strDesc = Request("Desc")
  strCheckListID = Request("CheckListID")
  strAlarmDate = Request("AlarmDate")
  strSysLastUpdatedDate = Request("SysLastUpdatedDate")
  strAssignmentID = Request("AssignmentID")
  
  'get the first claimAspectID in the list
  if instr(1, strClaimAspectIDList, ",") > 0 then
    arrClaimAspectID = split(strClaimAspectIDList, ",")
    strClaimAspectID = arrClaimAspectID(0)
  else
    strClaimAspectID = strClaimAspectIDList
  end if

	CheckErr()

	'Create and initialize DataPresenter.CExecute object.
	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckErr()

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
	CheckErr()

	' Add LynxId variable to be stuffed into xsl.
	' UserId and SessionKey were added in Initialize call above.
	objExe.AddXslParam "LynxId", strLynxID
	CheckErr()
	objExe.AddXslParam "ClaimAspectID", strClaimAspectIDList
	CheckErr()
	objExe.AddXslParam "AssignmentID", strAssignmentID
	CheckErr()
	objExe.AddXslParam "Entity", strEntity
	CheckErr()
	objExe.AddXslParam "Description", strDesc
	CheckErr()
  objExe.AddXslParam "CheckListID", strCheckListID
	CheckErr()
  objExe.AddXslParam "AlarmDate", strAlarmDate
	CheckErr()
  objExe.AddXslParam "SysLastUpdatedDate", strSysLastUpdatedDate
	CheckErr()

	'Get XML and transform to HTML. UI will be built using the first LynxID and ClaimAspectID
	strHTML = objExe.ExecuteSpAsXML( "uspClaimReassignmentGetListXML", "ClaimReAssign.xsl", strLynxID, strClaimAspectID, strEntity, strUserID )
	CheckErr()

	Set objExe = Nothing

    Call KillSessionObject

	Response.Write strHTML

	On Error GoTo 0

	Sub CheckErr()
		If Err.Number <> 0 Then
			ErrorHandler()
			Response.End
		End If
	End Sub

%>
