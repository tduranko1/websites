<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<%
  dim strDocumentID, strEstimateChanges, strUserID
  dim oXML, oEstimateChanges, oCData
  dim oDocMgr, strRet, aRet
  
  strDocumentID = Request("DocumentID")
  strUserID = Request("UserID")
  strEstimateChanges = Request("EstimateChanges")
  
  if strUserID = "" then
    strUserID = GetSession("UserID")
  end if
  
  set oXML = Server.createObject("MSXML2.DOMDocument")
  oXML.async = false
  oXML.loadXML("<EstimateChangeSummary DocumentID='" + strDocumentID + "' UserID='" + strUserID + "' DocumentType='Audited Estimate Report'><EstimateChanges/></EstimateChangeSummary>")
  
  set oEstimateChanges = oXML.selectSingleNode("/EstimateChangeSummary/EstimateChanges")
  if not (oEstimateChanges is nothing) then
    set oCData = oXML.createCDATASection(strEstimateChanges)
    oEstimateChanges.appendChild(oCData)
  end if
  
  set oDocMgr = Server.createObject("DocumentMgr.CDocMgr")
  strRet = oDocMgr.GenerateAndFaxAuditedEstimateReport(oXML.xml)
  set oDocMgr = nothing
  
  aRet = split(strRet, "|")
  'response.write strRet
  
  set oXML = nothing
%>
<script language="javascript">
  var strMsg = "";
<%if aRet(0) = "0" then%>
    strMsg = "The document was successfully attached to the claim and a fax was sent to the shop.";
<%else%>
    strMsg = "The document was successfully attached to the claim. An error occurred while sending the fax.";
<%end if%>
  if (typeof(parent.ClientWarning) == "function")
    parent.ClientWarning(strMsg);
  else
    alert(strMsg);
</script>