<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->

<HTML>

<HEAD>
<TITLE><%=request("caption")%></TITLE>
<STYLE type="text/css">
    A {color:black; text-decoration:none;}
  .clWinHead{
  	background-color : transparent;
  	color : white;
  	font-family : arial,arial,helvetica;
  	font-size : 11px;
  	font-weight : bold;
    padding-left:5px;
    cursor : default;
  }
</STYLE>
<SCRIPT language="JavaScript" src="js/formvalid.js"></SCRIPT>
<SCRIPT language="JavaScript">

  var sWindowName = "<%=request("winName")%>";
  var iAutoRefresh = parseInt(<%=request("autoRefresh")%>);
  var iRefreshTimer = null;
  var sContentPage = "";
  var sWindowState = "min";
  var oIFrame = null;
  
  function maxWindow(){
    parent.window_maximize(sWindowName); 
    if (typeof(frmWindowContent.setWindowState) == "function")
      frmWindowContent.setWindowState("max");
    if (sContentPage != "")
      sWindowState = "max";
    return false;
  }
  
  function colMaxWindow(){
    parent.window_columnMax(sWindowName); 
    if (typeof(frmWindowContent.setWindowState) == "function")
      frmWindowContent.setWindowState("min");
    if (sContentPage != "")
      sWindowState = "min";
    return false;
  }
  
  function equalizeWindow(){
    parent.window_columnEqual(); 
    if (typeof(frmWindowContent.setWindowState) == "function")
      frmWindowContent.setWindowState("min");
    if (sContentPage != "")
      sWindowState = "eq";
    return false;
  }
  
  function loadContent(sURL){
    if (sURL != "") {
      if (sWindowState == "max" && sURL.indexOf("winState=max") == -1)
        sURL += "&winState=max";
      else (sWindowState != "max")
        sURL = sURL.replace(/=max/g, "=" + sWindowState);

      tdContent.style.backgroundImage = "url('/images/window_refresh_clock.jpg')";
      if (oIFrame == null)
        oIFrame = document.getElementById("frmWindowContent");
        
      if (oIFrame) {
        oIFrame.style.display = "none";
        oIFrame.src = sURL;
      }
      sContentPage = sURL;
      if (iRefreshTimer)
        window.clearTimeout(iRefreshTimer);
      iRefreshTimer = window.setTimeout("loadContent(sContentPage)", iAutoRefresh);
    }
  }
  
  function frameContentChanged(obj) {
    //if (frmWindowContent.frameElement.readyState == "complete")
    //  frmWindowContent.frameElement.style.display = "inline";
    oIFrame = obj;
    if (obj.readyState == "complete")
      obj.style.display = "inline";
  }
</SCRIPT>

</HEAD>
<BODY unselectable="on" style="background-color:#FFFFFF;overflow:hidden;border:1px solid #000000;padding:1px;" onload="" tabIndex="-1" topmargin="0"  leftmargin="0" oncontextmenu="return false">
  <table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;background-color:#067BAF;width:100%;height:100%" >
    <colgroup>
      <col width="20"/>
      <col width="*"/>
      <col width="38"/>
    </colgroup>
    <tr style="height:19px;">
      <td>
        <div class="clLogo"><img alt="" src="/images/<%=request("icon")%>" width="19" height="18" border="0" align="top"/></div>
      </td>
      <td>
        <div id="divWinHead" class="clWinHead"><%=request("caption")%></div>
      </td>
      <td>
        <div id="divWinButtons">
          <img border="0" align="absmiddle" src="/images/window_max.gif" onClick="maxWindow()" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Maximize"/><img border="0" align="absmiddle" src="/images/window_col.gif" onClick="colMaxWindow()" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Columnize"/><img border="0" align="absmiddle" src="/images/window_reg.gif" onClick="equalizeWindow()" style="FILTER:alpha(opacity=80); cursor:hand;" onmouseout="makeHighlight(this,1,80)" onmouseover="makeHighlight(this,0,100)" alt="Normal"/>
        </div>
      </td>
    </tr>
    <tr>
      <td name="tdContent" id="tdContent" colspan="3" style="background-image:none;background-position: center center;background-repeat:no-repeat;background-attachment:fixed;background-color:#FDFAEB">  
        <iframe id="frmWindowContent" name="frmWindowContent" src="/blank.asp" style="height:100%;width:100%;border:0px" allowTransparency="true" onreadystatechange="frameContentChanged(this)"/>
      </td>
    </tr>
  </table>
  
</BODY>
</HTML>
