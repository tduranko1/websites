<%@ LANGUAGE="VBSCRIPT"%>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<%

    On Error Resume Next

    Dim objExe
    dim sClaimView
    Call GetHtml
    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError

    On Error GoTo 0

    Sub GetHtml()
      dim iLynxID
      dim strInsuranceCompanyID
      dim strUserDetailXML
      dim oXML, oUserNode
      dim strRepNameFirst, strRepNameLast, strRepOfficeName, strRepPhoneDay, strRepEmailAddress, strInsCompanyName
    	Dim strEventVehicleAssignmentCancelled

      Set objExe = InitializeSession()

			' TODO - add new event code when ready for the XSL
			strEventVehicleAssignmentCancelled = objExe.Setting("WorkFlow/EventCodes/Code[@name='MEAssignmentEntryCancellation']")

      strUserDetailXML = objExe.ExecuteSpAsXML( "uspSessionGetUserDetailXML", "", LCase(strCsrID))

      set oXML = Server.CreateObject("MSXML2.DOMDocument")

      oXML.async = false

      oXML.loadXML strUserDetailXML

      set oUserNode = oXML.selectSingleNode("/Root/User")
      if not oUserNode is nothing then
        if oUserNode.getAttribute("UserID") = 0 then
          call invalidUser
        end if
        strRepNameFirst = oUserNode.getAttribute("NameFirst")
        strRepNameLast = oUserNode.getAttribute("NameLast")
        strRepOfficeName = oUserNode.getAttribute("OfficeName")
        strRepPhoneDay = oUserNode.getAttribute("PhoneAreaCode") & "-" & oUserNode.getAttribute("PhoneExchangeNumber") & "-" & oUserNode.getAttribute("PhoneUnitNumber") & "x" & oUserNode.getAttribute("PhoneExtensionNumber")
        strRepEmailAddress = oUserNode.getAttribute("EmailAddress")
        strInsCompanyName = oUserNode.getAttribute("InsuranceCompanyName")
      end if

      set oXML = nothing

      strInsuranceCompanyID = 185
      objExe.AddXslParam "RepUserID", cstr(strApdUserID)
      objExe.AddXslParam "RepNameFirst", replace(cstr(strRepNameFirst), """", "\""")
      objExe.AddXslParam "RepNameLast", replace(cstr(strRepNameLast), """", "\""")
      'objExe.AddXslParam "RepOfficeName", cstr(strRepOfficeName)
      objExe.AddXslParam "RepPhoneDay", cstr(strRepPhoneDay)
      objExe.AddXslParam "RepEmailAddress", replace(cstr(strRepEmailAddress), """", "\""")
      'objExe.AddXslParam "RepInsCompanyName", cstr(strInsCompanyName)
    	objExe.AddXslParam "EventVehicleAssignmentCancelled", strEventVehicleAssignmentCancelled

      'Get ApdSelect XML and transform to HTML.
      Response.Write objExe.ExecuteSpAsXML( "uspECADGetInsuranceCoConfigXML", "MEAssignment.xsl", strInsuranceCompanyID)
    End Sub

    sub invalidUser()
      Response.write "<div style='font:10pt Tahoma;font-weight:bold;color:#FF0000;text-align:center'>" & strCsrID & " is not a valid/active user in APD application. Contact your supervisor." & "</div>"
      Response.end
    end sub
%>

<SCRIPT language="JavaScript" src="_ScriptLibrary/rs.htm"></SCRIPT>
<SCRIPT language="JavaScript">RSEnableRemoteScripting("_ScriptLibrary");</SCRIPT>
