<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	Dim strUserID, strInsuranceCompanyID, strHTML, strLynxID, strCarrierRepUserID, strSysLastUpdatedDate

    On Error Resume Next

	strUserID = GetSession("UserID")
    strLynxID = GetSession("LynxID")
    strInsuranceCompanyID = request("InsuranceCompanyID")
    strCarrierRepUserID = request("CarrierRepUserID")
    strSysLastUpdatedDate = request("SysLastUpdatedDate")

	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
	CheckError()

	objExe.AddXslParam "LynxID", strLynxID
	CheckError()
  
    objExe.AddXslParam "UserID", strUserID
	CheckError()
  
    objExe.AddXslParam "SysLastUpdatedDate", strSysLastUpdatedDate
	CheckError()
  
    objExe.AddXslParam "CarrierRepUserID", strCarrierRepUserID
	CheckError()

	' Get claim XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspClaimCarrierRepGetListXML", "ClaimCarrierRep.xsl", strInsuranceCompanyID)
	CheckError()

	Set objExe = Nothing

	Response.Write strHTML

    Call KillSessionObject

	On Error GoTo 0
%>

