<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SessionStuffer.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->


<%
	On Error Resume Next

	Dim objExe, strHTML, strClaimAspectID, strLynxID
  
  Set objExe = InitializeSession()
  
	strClaimAspectID = Request("ClaimAspectID")

	'Create and initialize DataPresenter if not already existing
  if objExe is nothing then
	  Set objExe = CreateObject("DataPresenter.CExecute")
    CheckError()

  	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")
 	  CheckError()
	end if

  objExe.AddXslParam "LynxID", Request("LynxID")
  objExe.AddXslParam "ReloadFlag", Request("ReloadFlag")
  objExe.AddXslParam "ImageRootDir", objExe.Setting("Document/RootDirectory")

	'Get XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspUtilEstimateListXML", "UtilEstimateDocList.xsl", strClaimAspectID )
	CheckError()


	Set objExe = Nothing

	Response.Write strHTML

	On Error GoTo 0
%>
