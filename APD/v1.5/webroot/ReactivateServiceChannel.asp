<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	On Error Resume Next

	Dim strLynxID, strUserID
	Dim strInsuranceCompanyID
	Dim objExe

	strLynxID = request("LynxID")
	strUserID = GetSession("UserID")
	CheckError()

	strInsuranceCompanyID = Request("InsuranceCompanyID")
  
	'Create and initialize DataPresenter
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

    objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
	  CheckError()
    objExe.AddXslParam "UserID", strUserID

	'Get XML and transform to HTML.
	'strHTML = objExe.ExecuteSpAsXML( "uspVehicleDeactivatedServiceChannelGetListXML", "ReactivateServiceChannel.xsl", strLynxID, strInsuranceCompanyID )
   Response.Write objExe.ExecuteSpAsXML( "uspVehicleDeactivatedServiceChannelGetListXML", "ReactivateServiceChannel.xsl", strLynxID, strInsuranceCompanyID )
	CheckError()

	Set objExe = Nothing

	Response.Write strHTML

    Call KillSessionObject

	On Error GoTo 0
%>
