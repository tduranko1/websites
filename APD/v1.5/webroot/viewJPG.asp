<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>APD Image Viewer</title>
  <LINK rel="stylesheet" href="/css/apdControls.css" type="text/css"/>
  <style>
    .button {
      cursor:hand;
      border:1px;
      text-align:center;
      vertical-align:bottom;
      overflow:hidden;
      background-color:#FFFFFF;
    }
    
    .buttonHover {
      cursor:hand;
      border:2px transparent;
      text-align:center;
      vertical-align:bottom;
      overflow:hidden;
      background-color:#FFFFFF;
    }
    .buttonPressed {
      cursor:hand;
      border:1px inset;
      text-align:center;
      vertical-align:bottom;
      overflow:hidden;
      background-color:#DCDCDC;
    }
  </style>
  <script language="Javascript">
    var imgHeight = imgWidth = iZoomPercent = iRotation = iMirror = iInvert = iXRay = iGrayScale = 0;
    var bFit = true;
    var iTimerID = iSliderThumb_offsetWidth = iSliderTicks_offsetLeft = iSliderTicks_offsetWidth = 0;
    var iSliderMin = 0.25; //25%
    var iSliderMax = 3; //300%
    var bImgOverflow = true;
    var iPanHoldLeft = iPanHoldTop = iPanHoldScrollTop = iPanHoldScrollLeft = 0;
    var sDlgProp = "center:yes;help:no;resizable:yes;scroll:no;status:no;unadorned=yes;"
		var newWin = null;
    var sDoc = "<%=replace(request.QueryString("docPath"), "\", "\\")%>";

    function initPage(){
      if (window.self == window.top) {
        //Preview.printable = true;
        //btnSaveLocal.parentElement.parentElement.style.height = "18px";
        btnSaveLocal.style.display = "inline";
        btnClose.style.display = "inline";
        btnPin.src = "images/pin.gif";
      } else {
        //btnSaveLocal.parentElement.parentElement.style.height = "0px";
        btnSaveLocal.style.display = "none";
        btnClose.style.display = "none";
        colRotateCW.style.display = "none";
        colRotateCC.style.display = "none";
        colMirror.style.display = "none";
        colInvert.style.display = "none";
        colXRay.style.display = "none";
        colGrayscale.style.display = "none";
        //Preview.printable = false;
      }
    }
    
    function initImg(){
      if (imgViewer.readyState == "complete"){
        imgHeight = imgViewer.offsetHeight + 10;
        imgWidth = imgViewer.offsetWidth + 10;
        imgViewer.style.visibility = "visible";
        setZoom(-1); //fit
        iSliderThumb_offsetWidth = sliderThumb.offsetWidth;
        iSliderTicks_offsetLeft = sliderTicks.offsetLeft;
        iSliderTicks_offsetWidth = sliderTicks.offsetWidth;
        
        if (divContainer.scrollWidth > imgViewer.offsetWidth || divContainer.scrollHeight > imgViewer.offsetHeight){
          imgViewer.style.cursor = "move";
          //bImgOverflow = true;
        } else {
          imgViewer.style.cursor = "default";
          //bImgOverflow = false;
        }
      }
    }
    
    function setZoom(val){
      switch (val) {
        case -1:
          if (divContainer.scrollWidth > divContainer.clientWidth || divContainer.scrollHeight > divContainer.clientHeight){
            var iHeightPercent = imgViewer.parentElement.offsetHeight * 100 / imgHeight;
            var iWidthPercent = imgViewer.parentElement.offsetWidth * 100 / imgWidth;
            
            iZoomPercent = Math.min(iHeightPercent, iWidthPercent);
            imgViewer.style.zoom = iZoomPercent + "%";
            setSliderPos(iZoomPercent);
          } else {
            iZoomPercent = 100;
            setSliderPos(iZoomPercent);
          }
          break;
        default:
          if (isNaN(val) == false){
            bFit = false;
            iZoomPercent = val;
            imgViewer.style.zoom = val + "%";
          }
          break;
      }
      
      spnZoomText.innerText = parseInt(iZoomPercent, 10) + " %";
      
      if (divContainer.scrollWidth > divContainer.clientWidth || divContainer.scrollHeight > divContainer.clientHeight){
        imgViewer.style.cursor = "move";
        bImgOverflow = true;
      } else {
        imgViewer.style.cursor = "default";
        bImgOverflow = false;
      }
    }
    
    function buttonClick(btn) {
      switch (btn.id){
        case "zoomin":
          if (iZoomPercent < (iSliderMax * 100)){
            iZoomPercent+=25;
            setSliderPos(iZoomPercent);
            setZoom(iZoomPercent);
          }
          break;
        case "zoomout":
          if (iZoomPercent > (iSliderMin * 200)){
            iZoomPercent-=25;
            setSliderPos(iZoomPercent);
            setZoom(iZoomPercent);
          }
          break;
        case "zoomfit":
          setZoom(-1);
          break;
        case "zoom100":
          setZoom(100);
          setSliderPos(iZoomPercent);
          break;
        case "rotateCW":
          iRotation--;
          if (iRotation < 0) iRotation = 3;
          applyFilter();
          break;
        case "rotateCC":
          iRotation++;
          if (iRotation > 3) iRotation = 0;
          applyFilter();
          break;
        case "mirror":
          iMirror = (iMirror == 0 ? 1 : 0);
          btn.className = (iMirror == 1 ? "buttonPressed" : "button")
          applyFilter();
          break;
        case "invert":
          iInvert = (iInvert == 0 ? 1 : 0);
          btn.className = (iInvert == 1 ? "buttonPressed" : "button")
          applyFilter();
          break;
        case "xray":
          iXRay = (iXRay == 0 ? 1 : 0);
          btn.className = (iXRay == 1 ? "buttonPressed" : "button")
          applyFilter();
          break;
        case "grayscale":
          iGrayScale = (iGrayScale == 0 ? 1 : 0);
          btn.className = (iGrayScale == 1 ? "buttonPressed" : "button")
          applyFilter();
          break;
      }
    }
    
    function applyFilter(){
      var sFilter = "FILTER: progid:DXImageTransform.Microsoft.BasicImage( Rotation=" + iRotation + ",Mirror=" + iMirror + ",Invert=" + iInvert + ",XRay=" + iXRay + ",Grayscale=" + iGrayScale + ",Opacity=1.00)"
      imgViewer.style.filter = sFilter;
      if (bFit) setZoom(-1);
    }
    
    function startMove(){
      event.cancelBubble = true;
      if (sliderThumb && event.button == 1) {
         sliderThumb.setCapture();
      }
    }
    
    function stopMove(){
      event.cancelBubble = true;
      if (sliderThumb) {
         sliderThumb.releaseCapture();
         var iAdjustedLeft = sliderThumb.offsetLeft;
         var iZoomPercent = (iAdjustedLeft * (iSliderMax - iSliderMin)) / 90 + iSliderMin;
         iZoomPercent = parseInt(iZoomPercent * 100, 10);
         setZoom(iZoomPercent);
      }
    }
    
    function doMove(){
       if (sliderThumb && event.button == 1) {
          if (iTimerID > 0) window.clearTimeout(iTimerID);
          iThumbLeft = event.clientX - iSliderTicks_offsetLeft;
          if (iThumbLeft < (iSliderTicks_offsetWidth - (iSliderThumb_offsetWidth/2)) && iThumbLeft > (iSliderThumb_offsetWidth / 2)){
            var iAdjustedLeft = iThumbLeft - (iSliderThumb_offsetWidth / 2);
            sliderThumb.style.left = iAdjustedLeft;
            var iZoomPercent = (iAdjustedLeft * (iSliderMax - iSliderMin)) / 90 + iSliderMin;
            iZoomPercent = parseInt(iZoomPercent * 100, 10);
            iTimerID = window.setTimeout("setZoom(" + iZoomPercent + ")", 200);
          } else {
            iTimerID = window.setTimeout("setZoom(" + iZoomPercent + ")", 200);
          }
       }
    }
    
    function setSliderPos(iPercent){
      var iAdjustedLeft = ((iPercent/100) - iSliderMin) * 90 / (iSliderMax - iSliderMin);
      sliderThumb.style.left = iAdjustedLeft;
    }
    
    function startImagePan(){
      event.cancelBubble = true;
      if (imgViewer && event.button == 1 && bImgOverflow) {
         iPanHoldLeft = event.clientX;
         iPanHoldTop = event.clientY;
         iPanHoldScrollTop = divContainer.scrollTop;
         iPanHoldScrollLeft = divContainer.scrollLeft;
         
         imgViewer.setCapture();
      }
    }
    
    function stopImagePan(){
      event.cancelBubble = true;
      if (imgViewer) {
         imgViewer.releaseCapture();
      }
    }
    
    function doImagePan(){
      if (imgViewer && event.button == 1 && bImgOverflow) {
        iPanLeftDelta = event.clientX - iPanHoldLeft;
        iPanTopDelta = event.clientY - iPanHoldTop;
        iScrollTop = iPanHoldScrollTop - iPanTopDelta;
        iScrollLeft = iPanHoldScrollLeft - iPanLeftDelta;
        
        divContainer.scrollTop = iScrollTop;
        divContainer.scrollLeft = iScrollLeft;
      }
    }

    function togglePinned() {
      var objImg = btnPin;
      
      if (objImg.src.indexOf("/images/pinned.gif") != -1) {
        var sDlgSize = "dialogHeight:600px;dialogWidth:800px;";
        var parentLoc = window.parent.location.href;
        var sURL = window.location.href;
        if (parentLoc.toLowerCase().indexOf("DocumentEstDetails.asp") != -1){
          newWin = window.showModalDialog(sURL, null, sDlgProp + sDlgSize);
        } else {
          objImg.src = "/images/pin.gif";
          btnPin.title = "Detach Photo Viewer";
          newWin = window.showModelessDialog(sURL, null, sDlgProp + sDlgSize);
          newWin.opener = window.self;
        }
      } else {
        objImg.src = "/images/pinned.gif";
        btnPin.title = "Close Detached Photo Viewer";
        if (newWin != null)
          newWin.close();
        window.close();
      }
    }


    function SaveLocal(){
      var sFeatures = "center:yes;help:no;resizable:yes;scroll:no;status:no;unadorned=yes;dialogHeight:245px;dialogWidth:400px;"
      var fso = new ActiveXObject("Scripting.FileSystemObject");
      //var sPath = fso.GetAbsolutePathName(sDoc);
      var sFileName = fso.GetFileName(sDoc);
      //var sFilePath = window.showModalDialog("/SaveAsDlg.htm", sFileName, sFeatures);
      try {
        var oXL = new ActiveXObject("Excel.Application");
        if (oXL) {
          sFileName2 = oXL.GetSaveAsFilename(sFileName, "JPEG Files (*.jpg),*.jpg", 0);
        } else {
          ClientWarning("One of the features in MS Office is incorrectly installed on your machine. Please contact administrator.");
          return;
        }
        if (sFileName2 == false) {
          return;
        }
      
        sFilePath = sFileName2;
      } catch (e) {
        ClientWarning("One of the features in MS Office is incorrectly installed on your machine. Please contact administrator.");
        return;
      } finally {
        if (oXL)
          oXL.Quit();
      }
      if (sFilePath) {
        if (fso.FileExists(sFilePath)) {
          var sReturn = YesNoMessage("Destination File Exists", "A file already exists at the destination folder with the same name. Do you want to overwrite?");
          if (sReturn == "Yes"){
            var f = fso.GetFile(sFilePath);
            if (f && (f.attributes && 1)) { // if the file is read-only
              f.attributes = 0; // set attribute to normal.
            }
            try {
              fso.CopyFile(sDoc, sFilePath, true);
            } catch (e) {
              ClientWarning("Could not copy the file to " + sFilePath + ". Please check the destination path.");
            }
          }
        } else {
          try {
            fso.CopyFile(sDoc, sFilePath, false);
          } catch (e) {
            ClientWarning("Could not copy the file to " + sFilePath + ". Please check the destination path.");
          }                  
        }
      }
    }    

    function reloadFrame() {
      if (window.opener) {	 
        var bObj = window.opener.document.getElementById('btnPin');
        if (bObj){
          var objImg = bObj;
          objImg.src="/images/pinned.gif";
        }
      }
    }

    function closeMe(){
      if (window.self == window.top) {
        window.close();
      } else {
        window.self.frameElement.style.display = "none";
      }
    }
  </script>
</head>

<body style="overflow:hidden;margin:0px;padding"0px" onload="initPage()" onunload="reloadFrame();" oncontextmenu="event.returnValue = false">
<div>
  <table border="0" cellpadding="0" cellspacing="0" style="margin:5px;height:98%;width:99%;table-layout:fixed;border-collapse:collapse">
    <colgroup>
      <col width="25px"/>
      <col width="30px"/>
      <col width="25px" style1="text-align:right"/>
      <col width="3px"/>
      <col width="98px"/>
      <col width="7px"/>
      <col width="25px" style="text-align:right"/>
      <col width="45px" style="text-align:center"/>
      <col width="30px" style="text-align:center"/>
      <col width="60px" style="text-align:center"/>
      <col width="30px"/>
      <col id="colRotateCW" width="25px" style="text-align:center"/>
      <col id="colRotateCC" width="25px" style="text-align:center"/>
      <col id="colMirror" width="62px" style="text-align:center"/>
      <col id="colInvert" width="62px" style="text-align:center"/>
      <col id="colXRay" width="62px" style="text-align:center"/>
      <col id="colGrayscale" width="62px" style="text-align:center"/>
      <col width="*"/>
      <col width="24px" style="text-align:center"/>
    </colgroup>
    <tr valign="middle" style="height:28px">
      <td><img id="btnPin" src="/images/pinned.gif" style="cursor:hand" onclick="togglePinned()"/></td>
      <td><img id="btnSaveLocal" src="/images/savecopy.gif" style="cursor:hand" onclick="SaveLocal()"/></td>
      <td><img id="zoomout" src="/images/zoomout.gif" style="cursor:hand" title="Zoom out" onclick="buttonClick(this)"/></td>
      <td><img src="/images/slider_l.png"/></td>
      <td>
        <span id="sliderTicks" style="position:relative;top:0px;width:100%;height:17px;background-image:url(/images/slider_m.png)"/>
        <img id="sliderThumb" src="/images/slider_thumb.png" style="position:absolute;top:-1px;left:0px;cursor:col-resize" onmousedown="startMove()" onmouseup="stopMove()" onmousemove="doMove()"/>
      </td>
      <td><img src="/images/slider_r.png"/></td>
      <td><img id="zoomin" src="/images/zoomin.gif" style="cursor:hand" title="Zoom in" onclick="buttonClick(this)"/></td>
      <td><img id="zoomfit" src="/images/zoomfit.gif" style="cursor:hand" title="Fit image to screen" onclick="buttonClick(this)"/></td>
      <td><img id="zoom100" src="/images/zoom100.gif" style="cursor:hand" title="Original size" onclick="buttonClick(this)"/></td>
      <td><span id="spnZoomText"/></td>
      <td/>
      <td>
        <img id="rotateCW" style="vertical-align:bottom" style="cursor:hand" src="/images/rotateCW.gif" title="Rotate left" onclick="buttonClick(this)"/>
      </td>
      <td>
        <img id="rotateCC" style="vertical-align:bottom" style="cursor:hand" src="/images/rotateCC.gif" title="Rotate right" onclick="buttonClick(this)"/>
      </td>
      <td>
        <img id="mirror" style="vertical-align:bottom;padding:2px;" style="cursor:hand;FILTER: progid:DXImageTransform.Microsoft.BasicImage( Rotation=0,Mirror=1,Invert=0,XRay=0,Grayscale=0,Opacity=1.00)" src="/images/car.png" title="Mirror image" onclick="buttonClick(this)"/>
      </td>
      <td>
        <img id="invert" style="vertical-align:bottom" style="cursor:hand;" src="/images/car_negative.png" title="Negative image" onclick="buttonClick(this)"/>
      </td>
      <td>
        <img id="xray" style="vertical-align:bottom" style="cursor:hand;" src="/images/car_xray.png" title="X-Ray image" onclick="buttonClick(this)"/>
      </td>
      <td>
        <img id="grayscale" style="vertical-align:bottom" style="cursor:hand" src="/images/car_grayscale.png" title="Grayscale image" onclick="buttonClick(this)"/>
      </td>
      <td/>
      <td>
        <img id="btnClose" style="vertical-align:bottom" style="cursor:hand" src="/images/close.gif" onclick="closeMe()"/>
      </td>      
    </tr>
    <tr style="height:5px;">
    </tr>
    <tr style="height:100%">
      <td colspan="19">
        <div id="divContainer" style="width:100%;height:100%;overflow:auto;border:1px solid #C0C0C0" onresizeend="initImg()">
          <img id="imgViewer" name="imgViewer" src="<%=replace(request.QueryString("docPath"), "\", "\\")%>" GALLERYIMG="no" style="visibility:hidden" onreadystatechange="initImg()" onmousedown="startImagePan()" onmouseup="stopImagePan()" onmousemove="doImagePan()" ondblclick="btnPin.fireEvent('onclick')"/> 
        </div>
      </td>
    </tr>
  </table>
</div>

</body>
</html>
