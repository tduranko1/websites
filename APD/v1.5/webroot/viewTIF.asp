<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
  <title></title>

  <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
  <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
  <style>
    .toolbarButton {
      width: 24px;
      height: 24px;
      cursor:arrow;
      background-color: #FFFFFF;
      border:0px;
    }

    .toolbarButtonOver {
      width: 24px;
      height: 24px;
      cursor:arrow;
      background-color: #FFFFFF;
      border-top:1px solid #D3D3D3;
      border-left:1px solid #D3D3D3;
      border-bottom:1px solid #696969;
      border-right:1px solid #696969;
    }

    .toolbarButtonOut {
      width: 24px;
      height: 24px;
      cursor:arrow;
      background-color: #FFFFFF;
      border:0px;
    }
    
    span {
      cursor: default;
    }
  </style>

  <SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>

  <script language="javascript">
    var bLoaded = false;
    var ZoomArray = new Array(5,10,17,25,33,50,70,100);
    var sDoc = "<%=replace(request.QueryString("docPath"), "\", "\\")%>";
    var zoomVal = 3;
    var sDlgProp = "center:yes;help:no;resizable:yes;scroll:no;status:no;unadorned=yes;"
	var newWin = null;
    
    function pageInit(){
      if (window.self == window.top) {
        btnSaveLocal.style.display = "inline";
        btnClose.style.display = "inline";
   	    btnPin.firstChild.src = "images/pin.gif";
      } else {
        btnSaveLocal.style.display = "none";
        btnClose.style.display = "none";
      }
    }

	
	function reloadFrame()
	{
	 if (window.opener) {	 
		var bObj = window.opener.document.getElementById('btnPin');
      if (bObj){
    		var objImg = bObj.firstChild;
    		objImg.src="/images/pinned.gif";
      }
	 }
	}
	
    function zoomIn(){
      if (zoomVal < ZoomArray.length){
        zoomVal++;
        tif1.SetValue(5, 0); //set to fixed size
        tif1.SetValue(6, ZoomArray[zoomVal]);
        tif1.SetValue(4, 1);
        tif1.focus();
      }
    }

    function zoomOut(){
      if (zoomVal > 0){
        zoomVal--;
        tif1.SetValue(5, 0); //set to fixed size
        tif1.SetValue(6, ZoomArray[zoomVal]);
        tif1.SetValue(4, 1);
        tif1.focus();
      }
    }

    function zoomFit(val){
      tif1.SetValue(5, val);
      if (val == 3)
        tif1.SetValue(4, 2);
      else
        tif1.SetValue(4, 1);
      tif1.focus();
    }

    function goToPage(val){
      tif1.GoToPageSpecial(val);
      showPageInfo();
      tif1.focus();
    }

    function rotate(val){
      tif1.SetValue(1, val);
      tif1.focus();
    }

    function showPageInfo(){
      if (bLoaded)
        pgNo.innerText = "Page " + tif1.GetCurrentPage() + " / " + tif1.GetNumberOfPages();
    }


    var bFileFound = false;
    var bDocTif = false;

    function InitTifViewer(){
      if (!bDocTif) {
        //document requested was not tif, clear the Interval
        //window.clearInterval(myTmr);
        //window.status = "CLEARED";
        //return;
      }
      try {
          if (tif1.GetState(1) == 1){
            pgNo.innerText = "Downloading file. Please wait...";
          }
    
          if (tif1.GetState(2) == 1) {
            //clear the init timer if the no Viewer span appears
            if (document.all["noTifViewer"] != null)
                window.clearInterval(myTmr);
                
            //clear the init timer if the tif viewer initialized successfully.
            if (tif1)
                window.clearInterval(myTmr);
            if (tif1.GetState(0) == 1) {
              enableControls();
              bLoaded = true;
              //tif1.SetValue(4, 2);
			  zoomIn();
			  tif1.SetValue(4, 1); // set the mouse mode to panning.
              showPageInfo();
              tif1.focus();
              //tif1.style.height = document.body.clientHeight - 28;
            }
            else {
              pgNo.innerText = "Scripting is disabled or problem initializing the TIF image viewer. Please contact Administrator.";
            }
            return;
          }
      }
      catch (e) {}
    }

    function enableControls(){
      var objs = toolbar.getElementsByTagName("BUTTON");
      for (var i = 0; i < objs.length; i++) {
        objs[i].disabled = false;
      }
      if (window.self !== window.top)
        btnPrint.style.display = "none";
    }

    function closeMe(){
        //window.close();
        if (window.self == window.top) {
            window.returnValue = true;
            close();
        }
        else {
            window.self.frameElement.style.display = "none";
        }
    }
    
    function tb_onmouseover(obj){
        obj.className = "toolbarButtonOver";
    }
    
    function tb_onmouseout(obj){
        obj.className = "toolbarButtonOut";
    }
    
	function togglePinned()
 	{
   	 var objImg = btnPin.firstChild;
	
   		 if (objImg.src.indexOf("/images/pinned.gif") != -1) 
		 {
     		 var sDlgSize = "dialogHeight:600px;dialogWidth:800px;";
	  		 var parentLoc = window.parent.location.href;
      		 var sURL = window.location.href;
     		 if (parentLoc.toLowerCase().indexOf("DocumentEstDetails.asp") != -1)
	  		 {
               newWin = window.showModalDialog(sURL, null, sDlgProp + sDlgSize);
     		 }
      	     else 
	 		 {
       		   objImg.src = "/images/pin.gif";
        	   btnPin.title = "Detach Photo Viewer";
        	   newWin = window.showModelessDialog(sURL, null, sDlgProp + sDlgSize);
       		   newWin.opener = window.self;
     		 }
  		}
   		else 
			{
     		   objImg.src = "/images/pinned.gif";
     		   btnPin.title = "Close Detached Photo Viewer";
   			   if (newWin != null)
      		    newWin.close();
    		   window.close();
    		 }
           }
	
    function PrintTIF(){
      if (tif1)
        tif1.Print(5);
    }

    function SaveLocal(){
      var sFeatures = "center:yes;help:no;resizable:yes;scroll:no;status:no;unadorned=yes;dialogHeight:245px;dialogWidth:400px;"
      var fso = new ActiveXObject("Scripting.FileSystemObject");
      //var sPath = fso.GetAbsolutePathName(sDoc);
      var sFileName = fso.GetFileName(sDoc);
      var sFileName2 = "";
      //var sFilePath = window.showModalDialog("/SaveAsDlg.htm", sFileName, sFeatures);
      try {
        var oXL = new ActiveXObject("Excel.Application");
        if (oXL) {
          sFileName2 = oXL.GetSaveAsFilename(sFileName, "TIFF Files (*.tif),*.tif", 0);
        } else {
          ClientWarning("One of the features in MS Office is incorrectly installed on your machine. Please contact administrator.");
          return;
        }
        if (sFileName2 == false) {
          return;
        }
          
       sFilePath = sFileName2;
      } catch (e) {
        ClientWarning("One of the features in MS Office is incorrectly installed on your machine. Please contact administrator.")
        return;
      } finally {
        if (oXL)
          oXL.Quit();
      }
      if (sFilePath) {
        if (fso.FileExists(sFilePath)) {
          var sReturn = YesNoMessage("Destination File Exists", "A file already exists at the destination folder with the same name. Do you want to overwrite?");
          if (sReturn == "Yes"){
            var f = fso.GetFile(sFilePath);
            if (f && (f.attributes && 1)) { // if the file is read-only
              f.attributes = 0; // set attribute to normal.
            }
            try {
              fso.CopyFile(sDoc, sFilePath, true);
            } catch (e) {
              ClientWarning("Could not copy the file to " + sFilePath + ". Please check the destination path.");
            }
          }
        } else {
          try {
            fso.CopyFile(sDoc, sFilePath, false);
          } catch (e) {
            ClientWarning("Could not copy the file to " + sFilePath + ". Please check the destination path.");
          }                  
        }
      }
    }
    
    var myTmr = window.setInterval("InitTifViewer()", 1000);


  </script>
</head>

<body onload="pageInit()" onunload="reloadFrame();" topmargin="0" bottommargin="0" rightmargin="0" leftmargin="0" margintop="0" marginleft="0" style="overflow:hidden;border:1px solid #000000;">
  <table border="0" cellspacing="0" cellpadding="0" style="width:100%; height:100%">
    <tr style="height:24px">
      <td>
        <div id="divToolbar" style="margin:2px;width:100%">
          <div id="toolbar" style="background-color:#FFFFFF">
            <table border="0" cellspacing="0" cellpadding="0" style="width:100%;">
            <tr>
            <td width="*">
			<button hidefocus="true" id="btnPin" name="btnPin" tabIndex = "-1" tabStop="false" onclick="togglePinned()" title="Unpin/Pin" class="toolbarButton"><img src="/images/pinned.gif" width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;"  onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)" /></button>
            <button hidefocus="true" id="btnSaveLocal" tabIndex="-1" tabStop="false" disabled onclick="SaveLocal()" title="Save a local copy" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/SaveCopy.gif" width="17" height="14" border="0"/></button>
            <button name="btnPrint" id="btnPrint" disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="PrintTIF()" title="Print" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/print.gif"></button>
            <img src="/images/spacer.gif" width="4px">
            <button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="zoomIn()" title="Zoom In" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/zoomin.gif"></button>
            <button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="zoomOut()" title="Zoom Out" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/zoomout.gif"></button>
            <!--<button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="zoomFit(3)" title="Best Fit Image" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/bestFit.gif"/></button>-->
            <!-- <button tabIndex="-1" tabStop="false" onclick="zoomFit(1)" title="Fit Image to window width"  class="toolbarButton"><img src="/images/spacer.gif" style="width:14px;height:7px;border:1px solid #000000;vertical-align:middle"></button>
            <button tabIndex="-1" tabStop="false" onclick="zoomFit(2)" title="Fit Image to window height" class="toolbarButton"><img src="/images/spacer.gif" style="width:7px;height:14px;border:1px solid #000000"></button> -->
            <img src="/images/spacer.gif" width="4px">
            <button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="rotate(5)" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/rotateCC.gif"/></button>
            <button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="rotate(6)" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><img src="/images/rotateCW.gif"/></button>
            <img src="/images/spacer.gif" width="4px">
            <button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(1)" title="First Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><span style="width:14px;height:14px">&lt;&lt;</span></button>
            <button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(3)" title="Previous Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><span style="width:14px;height:14px">&lt;</span></button>
            <button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(4)" title="Next Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><span style="width:14px;height:14px">&gt;</span></button>
            <button disabled hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(2)" title="Last Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)"><span style="width:14px;height:14px">&gt;&gt;</span></button>
            <span id="pgNo" name="pgNo" style=""height:100%;vertical-align:middle"></span>
            </td>
            <td align="right" valign="middle" style="width:24px" >
            <button id="btnClose" tabIndex="-1" tabStop="false" onclick="closeMe()" title="Close Window" class="toolbarButton"><img src="/images/close.gif" width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"></button>
            </td>
            </tr>
            </table>
            <img src="/images/spacer.gif" style="width:100%;height:0px;border-top:1px solid #696969;border-bottom:1px solid #D3D3D3;">
          </div>
        </div>
        
      </td>
    </tr>
    <tr>
      <td>
        <div>
          <!-- <embed id="tif1" name="tif1" src='<%=request.QueryString("docPath")%>' width="100%" height="100%"
            toolbar="off" smooth="yes" fit="best" bgcolor="#F5F5F5"
            access="512"
            pluginspage="http://www.alternatiff.com/install/"> -->
                <object id="tif1" name="tif1" classid="CLSID:106E49CF-797A-11D2-81A2-00E02C015623"
                   codebase="/install/alttiff.cab#version=1,5,1,1"
                   width="100%" height="100%">
                    <param name="src" value="<%=request.QueryString("docPath")%>">
                    <param name="toolbar" value="off">
                    <param name="smooth" value="yes">
                    <param name="bgcolor" value="#F5F5F5">
                    <!-- <param name="fit" value="best">
                    <param name="mousemode" value="pan"> -->
                    <param name="access" value="512">
                    <center>
                        <span id="noTifViewer">
                        Unable to view TIF document. Please contact administrator and install TIF Viewer.
                        </span>
                    </center>
                </object>
        </div>
      </td>
    </tr>
  </table>
  <script language="javascript">
    bDocTif = true;
  </script>
</body>
</html>