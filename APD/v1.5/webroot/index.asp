<%@ Language=VBScript %>
<% Option Explicit %>
<!-- #include file="errorhandler.asp" -->
<%
    On Error Resume Next

	' Get a window name based upon the environment name for this application.
	' This will allow us to open multiple instances of the app as long as they
	' are from different environments.  Note that you still will not be able
	' to open multiple instances from the same environment, which is by design.

    Dim objExe, rsUserDetail, strWinName, strPageName, strCsrID
    Dim strRoleID, strDefaultDesktop, strQueueUser

    Call ChooseDesktop

    Set rsUserDetail = Nothing
    Set objExe = Nothing

    CheckError()

    Sub ChooseDesktop()

        'Get current user from Server NT Authentification
        strCsrID = Request.ServerVariables("LOGON_USER")

        'If user id is not returned (SHOULD Never happen but just in case....)
        If strCsrID = "" Then
            Err.Raise ServerSideEvent(), "index.asp", _
                "'LOGON_USER' server variable was blank.  This should not happen with basic authentication."
        End If

        'Strip NT Domain from login
        If InStr(strCsrID,"\") Then
            strCsrID = Right(strCsrID, Len(strCsrID)-InStr(strCsrID,"\"))
        End If

        'Create and initialize DataPresenter.CExecute object.
        Set objExe = CreateObject("DataPresenter.CExecute")
        objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH")

		Set rsUserDetail = objExe.ExecuteSpAsRS("uspSessionGetUserDetail", strCsrID)
        If rsUserDetail.eof = False Then
            strRoleID = rsUserDetail("RoleID")
            strDefaultDesktop = rsUserDetail("RoleID")
            strQueueUser = "0"
            if strRoleID = 25 and strDefaultDesktop = 9 then strQueueUser = "1"
            Select Case ucase(rsUserDetail("DefaultDesktop"))
                Case "1"
                    strPageName = "ApdSelect.asp?NewSession=1"     
                    Call  GetRegionForNADA               
                Case "2"
                    strPageName = "/ProgramMgr/PMD.asp?NewSession=1"
                Case "3"
                    strPageName = "UAD.asp?NewSession=1"
                Case "4"
                    strPageName = "/ProgramMgr/SMT.asp?NewSession=1"
                Case "8"
                    strPageName = "MED.asp?NewSession=1"
                Case "9"
                    strPageName = "TLQ.asp?NewSession=1&UserID=" & rsUserDetail("UserID") & "&q=" & strQueueUser
                Case Else
                    'no default desktop set for this user
                    Response.Write "Please have your supervisor assign you a 'Default Desktop' then try again."

                    Set rsUserDetail = Nothing
                    Set objExe = Nothing
                    Response.End
            End Select

            strWinName = "APDUserFrame_" & objExe.mToolkit.mEvents.mSettings.Environment

        Else

            'not a valid user
            Response.Write "Please check your login information.  If you continue to have problems, contact your supervisor."

            Set rsUserDetail = Nothing
            Set objExe = Nothing
            Response.End

        End If

    End Sub

    Sub GetRegionForNADA()
        On error resume next
        'Get the region list from nada service             
        Dim postUrl, xmlhttp, strHtmlValue
        Dim DataToSend, oExe, strServiceURL
        
        Set oExe = CreateObject("DataPresenter.CExecute")
        oExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH") & "..\config\config.xml"
    
        strServiceURL = oExe.Setting("NADAWebservice/URL")
    
        
        postUrl = strServiceURL & "GetRegionsArray" 
        
        Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
 
        xmlhttp.Open "POST",postUrl,false
        xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
        xmlhttp.send 
        strHtmlValue =  xmlhttp.responseText
        set xmlhttp = nothing
       
        if len(strHtmlValue) > 0 then
    
             Dim xmlDoc
             set xmlDoc = createObject("MSXML2.DOMDocument")
             xmlDoc.async = False     
             xmlDoc.loadXML(strHtmlValue)
            
             Response.Cookies("NADARegionList")= replace(replace(replace(xmlDoc.getElementsByTagName("string").item(0).childNodes(0).text,"&amp;","&"),"&gt;",">"),"&lt;","<")
           ' response.Write postUrl
            'response.Write err.Description
            'response.End
             'response.Write replace(replace(replace(xmlDoc.getElementsByTagName("string").item(0).childNodes(0).text,"&amp;","&"),"&gt;",">"),"&lt;","<")
              'if err.number > 0 then
                'response.Write err.Description & "error"
              'end if 
          end if
        
        'response.Write  request.Cookies("testvalue") ' len(strHtmlValue & "error" & err.Description)
    End Sub

%>

<html>
<head>
	<title>APD Auto Start</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

	<script language="javascript" type="text/javascript">

	    function NewWindow() {
	        var settings = 'width=1012,height=670,top=0,left=0,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
	        var win = window.open('<%= strPageName %>', '<%= strWinName %>', settings);
	    }

	</script>

</head>
<body bgcolor="#FFFFFF" text="#000000" onload=NewWindow()>
	<!-- You are <%=Request.ServerVariables("LOGON_USER")%> -->
</body>
</html>
