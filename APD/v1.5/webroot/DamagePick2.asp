<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->

<%
    'On Error Resume Next
    Dim objExe
    Dim strHTML
    Dim strUserID
    
    strUserID = GetSession("UserID")
    
    Set objExe = CreateObject("DataPresenter.CExecute")
    objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
    ' Place claim information in session manager.
    strHTML = objExe.ExecuteSpAsXML( "uspImpactAreaGetListXML", "DamagePick2.xsl")
    
    Response.Write strHTML
    
    set objExe = nothing
%>
