<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
	Dim strLynxID
	Dim strUserID
	Dim strInsuranceCompanyID
	Dim strHTML

    On Error Resume Next

	strLynxID = Request("LynxID")
	If strLynxID = "" Then
		strLynxID = GetSession("LynxID")
	Else
		UpdateSession "LynxID", strLynxID
	End If

	strUserID = GetSession("UserID")

	Dim objExe
	Set objExe = CreateObject("DataPresenter.CExecute")
	CheckError()

	objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, strUserID
	CheckError()

	objExe.AddXslParam "SelectionText", request.QueryString("Sel")
	CheckError()

	' Get claim XML and transform to HTML.
	strHTML = objExe.ExecuteSpAsXML( "uspClaimLossTypeGetDetailXML", "ClaimLossType.xsl")
	CheckError()

	Set objExe = Nothing

    Call KillSessionObject

	Response.Write strHTML

	On Error GoTo 0
%>

