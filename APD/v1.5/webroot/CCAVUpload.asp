<%
  Option Explicit
  response.expires = -1
  
  On Error Resume Next
  lsPageName = "CCAVUpload.asp"  

  Dim lsStrXML, lsRetVal, lsCOMClass
  Dim loAPD, oXML
  
  lsCOMClass = "LAPDPartnerDataMgr.CExecute"

  Err.Clear()
  set loAPD = Server.CreateObject(lsCOMClass)
  
  If Err.Number <> 0 Then
    response.write "<Error><![CDATA[Error creating '" & lsCOMClass & "' object. " & Err.Description & "]]></Error>"
    response.end
  End If
  
  set oXML = Server.CreateObject("MSXML2.DOMDocument")
  oXML.async = false
  oXML.load(Request)
  
  if oXML.parseError.errorCode <> 0 then
    response.write "<Error><![CData[Error parsing the XML request at Line #: " & CStr(oXML.parseError.line) & " at character position: " & CStr(oXML.parseError.linepos) & ". Reason: " & oXML.parseError.reason & "]]></Error>"
    response.end
  end if

  lsRetVal = submitData(oXML.xml)

  response.write lsRetVal
  set loAPD = Nothing

function submitData(sXML)
  on error resume next
  dim lsRet
  lsRet = loAPD.InitiatePartnerXmlTransaction(sXML)
  if err.number <> 0 then
    lsRet = "<Error><![CDATA[An Error occurred. Error Source: " & err.source & "; Description: " & err.description & "]]></Error>"
  end if
  submitData = lsRet
end function
%>
