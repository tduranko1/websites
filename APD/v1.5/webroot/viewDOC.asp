<html>
    <head>
    <title>APD Image Viewer</title>
        <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
        <style>
             .toolbarButton {
              width: 24px;
              height: 24px;
              cursor:arrow;
              background-color: #FFFFFF;
              border:0px;
            }
            .toolbarButtonOver {
              width: 24px;
              height: 24px;
              cursor:arrow;
              background-color: #FFFFFF;
              border-top:1px solid #D3D3D3;
              border-left:1px solid #D3D3D3;
              border-bottom:1px solid #696969;
              border-right:1px solid #696969;
            }
        
            .toolbarButtonOut {
              width: 24px;
              height: 24px;
              cursor:arrow;
              background-color: #FFFFFF;
              border:0px;
            }

        </style>

    <SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>

        <script language="javascript">
  		    var sDlgProp = "center:yes;help:no;resizable:yes;scroll:no;status:no;unadorned=yes;"
			var newWin = null;
            var sDoc = "<%=replace(request.QueryString("docPath"), "\", "\\")%>";
			
            function closeMe(){
                if (window.self == window.top) {
                    //window.returnValue = true;
                    window.close();
                }
                else {
                    window.self.frameElement.style.display = "none";
                }
            }
            function initPage(){
                if (window.self == window.top) {
                  //Preview.printable = true;
                  //btnSaveLocal.parentElement.parentElement.style.height = "18px";
                  //btnSaveLocal.style.display = "inline";
                  btnClose.style.display = "inline";
				  btnPin.firstChild.src = "images/pin.gif";
                } else {
                  //btnSaveLocal.parentElement.parentElement.style.height = "0px";
                  //btnSaveLocal.style.display = "none";
                  btnClose.style.display = "none";
                  //Preview.printable = false;
                }
                //Preview.Show("<%=replace(request.QueryString("docPath"), "\", "\\")%>");
                
            }
            
            function tb_onmouseover(obj){
                obj.className = "toolbarButtonOver";
            }
            
            function tb_onmouseout(obj){
                obj.className = "toolbarButtonOut";
            }
            
			function reloadFrame()
			{
        if (window.opener)
        {	 
          var bObj = window.opener.document.getElementById('btnPin');
          if (bObj){
            var objImg = bObj.firstChild;
            objImg.src="/images/pinned.gif";
          }
          try {
            window.opener.document.getElementById('ifrmDoc').src =window.opener.document.getElementById('ifrmDoc').src ;   
          } catch (e) {}
        }
      }
			
			
			function togglePinned()
 			{
   			var objImg = btnPin.firstChild;
	
   			if (objImg.src.indexOf("/images/pinned.gif") != -1) 
			{
     		 var sDlgSize = "dialogHeight:600px;dialogWidth:800px;";
	  		 var parentLoc = window.parent.location.href;
      		 var sURL = window.location.href;
     		 if (parentLoc.toLowerCase().indexOf("DocumentEstDetails.asp") != -1)
	  		  {
               newWin = window.showModalDialog(sURL, null, sDlgProp + sDlgSize);
     		  }
      		 else 
	 		  {
       		   objImg.src = "/images/pin.gif";
        	   btnPin.title = "Detach Photo Viewer";
        	   newWin = window.showModelessDialog(sURL, null, sDlgProp + sDlgSize);
       		   newWin.opener = window.self;
     		  }
  			}
   			else 
			{
     		   objImg.src = "/images/pinned.gif";
     		   btnPin.title = "Close Detached Photo Viewer";
   			   if (newWin != null)
      		    newWin.close();
    		   window.close();
    		 }
           }
			
				
            function printDoc(){
              try {
                //ifrmDoc.print();
                ClientWarning("To print this document click on the document and using keyboard press &lt;CTRL&gt; + P.");
              } catch (e) {
                alert(e.description);
              }
            }
            
            function Save(){
              try {
                //ifrmDoc.print();
                ClientWarning("To save changes to the original document then click on the document and using keyboard press &lt;CTRL&gt; + S.");
              } catch (e) {
                alert(e.description);
              }
            }

            function SaveLocal(){
              var sFeatures = "center:yes;help:no;resizable:yes;scroll:no;status:no;unadorned=yes;dialogHeight:245px;dialogWidth:400px;"
              var fso = new ActiveXObject("Scripting.FileSystemObject");
              //var sPath = fso.GetAbsolutePathName(sDoc);
              var sFileName = fso.GetFileName(sDoc);
              //var sFilePath = window.showModalDialog("/SaveAsDlg.htm", sFileName, sFeatures);
              try {
                var oXL = new ActiveXObject("Excel.Application");
                if (oXL) {
                  sFileName2 = oXL.GetSaveAsFilename(sFileName, "Doc Files (*.doc),*.doc", 0);
                } else {
                  ClientWarning("One of the features in MS Office is incorrectly installed on your machine. Please contact administrator.");
                  return;
                }
                if (sFileName2 == false) {
                  return;
                }
                  
               sFilePath = sFileName2;
              } catch (e) {
                ClientWarning("One of the features in MS Office is incorrectly installed on your machine. Please contact administrator.");
                return;
              } finally {
                if (oXL)
                  oXL.Quit();
              }

              if (sFilePath) {
                if (fso.FileExists(sFilePath)) {
                  var sReturn = YesNoMessage("Destination File Exists", "A file already exists at the destination folder with the same name. Do you want to overwrite?");
                  if (sReturn == "Yes"){
                    var f = fso.GetFile(sFilePath);
                    if (f && (f.attributes && 1)) { // if the file is read-only
                      f.attributes = 0; // set attribute to normal.
                    }
                    try {
                      fso.CopyFile(sDoc, sFilePath, true);
                    } catch (e) {
                      ClientWarning("Could not copy the file to " + sFilePath + ". Please check the destination path.");
                    }
                  }
                } else {
                  try {
                    fso.CopyFile(sDoc, sFilePath, false);
                  } catch (e) {
                    ClientWarning("Could not copy the file to " + sFilePath + ". Please check the destination path.");
                  }                  
                }
              }
            }
        </script>
    </head>
    <body topmargin="0" bottommargin="0" rightmargin="0" leftmargin="0" margintop="0" marginleft="0" style="overflow:hidden;border:1px solid #000000;" onunload="reloadFrame();" onload="initPage()">
      <table border="0" cellspacing="0" cellpadding="0" style="width:100%; height:100%">
        <tr style="height:18px;">
          <td>
            <!-- commented out because this page is only viewed from the 'View' menu item, not the 'Save' item. -->
            <!-- <button hidefocus="true" id="btnSave" tabIndex="-1" tabStop="false" onclick="Save()" title="Save to Original" class="toolbarButton"><img src="/images/Save.gif" width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"/></button> -->
			<button hidefocus="true" id="btnPin" name="btnPin" tabIndex = "-1" tabStop="false" onclick="togglePinned()" title="Unpin/Pin" class="toolbarButton"><img src="/images/pinned.gif" width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;"  onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)" /></button>
            <button hidefocus="true" id="btnSaveLocal" tabIndex="-1" tabStop="false" onclick="SaveLocal()" title="Save a local copy" class="toolbarButton"><img src="/images/SaveCopy.gif" width="17" height="14" border="0" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"/></button>
            <button hidefocus="true" id="btnPrint" tabIndex="-1" tabStop="false" onclick="printDoc()" title="Print Document" class="toolbarButton"><img src="/images/print.gif"/ width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"></button>
          </td>
          <td align="right">
            <button hidefocus="true" id="btnClose" tabIndex="-1" tabStop="false" onclick="closeMe()" title="Close Window" class="toolbarButton"><img src="/images/close.gif" width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;" onmouseout="makeHighlight(this,1,60)" onmouseover="makeHighlight(this,0,100)"/></button>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <iframe id="ifrmDoc" name="ifrmDoc" style="width:100%;height:100%;border:0px;" src="<%=request.QueryString("docPath")%>" allowtransparency='true' >
            </iframe>
            <!--<OBJECT id="doc" tabIndex="-1" type12="application/msword" CLASSID="clsid:E7E4BC40-E76A-11CE-A9BB-00AA004AE837" style="width:100%;height:100%;border:0px;">
              <param name="src" value="<%=request.QueryString("docPath")%>">
            </OBJECT>-->
          </td>
        </tr>
      </table>
    </body>
</html>

