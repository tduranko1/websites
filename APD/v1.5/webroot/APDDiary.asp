<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->

<%
    On Error Resume Next
    Dim objExe

    Call GetDiaryList
    Call KillSessionObject
    Call CheckError

    Set objExe = Nothing

    Sub GetDiaryList()

        Dim strLynxID, strUserID, strShowAll, strWindowState, strHTML,strShowAllTasks

        strLynxID = Request("LynxID")
        If strLynxID = "" Then
            strLynxID = GetSession("LynxID")
        End If

        strUserID = Request("UserID")
        If strUserID = "" Then
            strUserID = GetSession("UserID")
        End If

        strShowAll = Request("showAll")
        strShowAllTasks = Request("ShowAllTasks")
        strWindowState = Request("winState")

        If strShowAll = "true" Or strLynxID = "" Then strLynxID = "0"

		'------------------------------------'
		' Local Params
		'------------------------------------'
        dim sParams
        sParams = "showAll:" & strShowAll
        sParams = sParams + "|ShowAllTasks:" & strShowAllTasks
        sParams = sParams + "|winState:" & strWindowState

		'------------------------------------'
		' Setup Session Data
		'------------------------------------'
		Dim sASPXSessionKey, sASPXWindowID
		sASPXSessionKey = sSessionKey	
		sASPXWindowID = sWindowID
		
		'response.write(sSessionKey & " - " & sWindowID)
		'response.end
		'------------------------------------'
		' Pass control over to a dot net
		' version of the DataPresenter
		'------------------------------------'
		response.redirect("APDDiary.aspx?SessionKey=" & sASPXSessionKey & "&WindowID=" & SASPXWindowID & "&Params=" & sParams)
    End Sub
%>

