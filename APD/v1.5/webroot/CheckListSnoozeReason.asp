<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>

<!-- CheckListSnoozeReason.asp
	Called from CheckList.asp ( CheckListDetails.xsl ).
	Prompts the user to select a delay reason and a note type and enter a comment.
	It is used in a popup dialog window.
-->

<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<%
    On Error Resume Next

    Dim objExe

    'Extract the query data.
    Dim UserID, CheckListID, AlarmDateOld, AlarmDateNew, strResult

    UserID = Request("UserID")
    CheckListID = Request("CheckListID")
    AlarmDateOld = Request("AlarmDateOld")
    AlarmDateNew = Request("AlarmDateNew")

    Set objExe = CreateObject("DataPresenter.CExecute")

    objExe.Initialize Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, UserID

    objExe.Trace "CheckListID='" & CheckListID & "'", "CheckListSnoozeReason.asp"

    objExe.AddXslParam "AlarmDateOld", AlarmDateOld
    objExe.AddXslParam "AlarmDateNew", AlarmDateNew

    strResult = objExe.ExecuteSpAsXML( "dbo.uspDiaryDelayReasonGetListXML", "CheckListSnoozeReason.xsl", CheckListID )

    Response.write strResult

    Set objExe = Nothing
    Call KillSessionObject
    Call CheckError

%>
