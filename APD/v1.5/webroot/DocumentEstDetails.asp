<%@ Language=VBScript %>
<% Option Explicit %>
<% Response.Expires = -1%>
<!-- #include file="errorhandler.asp" -->
<!-- #include file="vbs/SessionManagerV2.vbs" -->
<!-- #include file="vbs/SecurityCheckV2.vbs" -->
<!-- #include file="vbs/CRDSecurity.vbs" -->

<%
  On Error Resume Next

  Dim strUserID, strLynxID, strInsuranceCompanyID, strDocumentID, strClaimAspectID, strPageTitle
	Dim strClaimAspectServiceChannelID, strEstimateUpd
  Dim objExe, strHTML, bFileUploadFlag, strImageLocation, strDisDocTypeSelect, bDisplayPricingFlag

  strUserID = Request("UserId")
  strLynxID = Request("LynxId")
  strInsuranceCompanyID = Request("InsuranceCompanyID")
  bFileUploadFlag = Request("FileUploadFlag")
  bDisplayPricingFlag = Request("DisplayPricingFlag")
  strEstimateUpd = Request("EstimateUpd")

  If strEstimateUpd = 1 Then
    strDocumentID = Request("DocumentID")
    strClaimAspectID = Request("ClaimAspectID")
		strClaimAspectServiceChannelID = Request("ClaimAspectServiceChannelID")
    strImageLocation = Request("ImageLocation")
    strPageTitle = "Quick Estimate/Supplement Update Dialogue"
    strDisDocTypeSelect = Request("DisDocTypeSelect")
  Else
    strDocumentID = "0"
    strClaimAspectID = "0"
    strPageTitle = "Document Upload Dialogue"
    strEstimateUpd = "0"
  End If

  'Create and initialize DataPresenter if not already existing
  Set objExe = CreateObject("DataPresenter.CExecute")
  Call CheckError()

  objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
  Call CheckError()

  objExe.AddXslParam "DocumentID", strDocumentID
  objExe.AddXslParam "ClaimAspectID", strClaimAspectID
	objExe.AddXslParam "ClaimAspectServiceChannelID", strClaimAspectServiceChannelID
  objExe.AddXslParam "LynxID", strLynxID
  objExe.AddXslParam "PageTitle", strPageTitle
  objExe.AddXslParam "EstimateUpd", strEstimateUpd
  objExe.AddXslParam "ImageLocation", strImageLocation
  objExe.AddXslParam "FileUploadFlag", bFileUploadFlag
  objExe.AddXslParam "InsuranceCompanyID", strInsuranceCompanyID
  objExe.AddXslParam "DisDocTypeSelect", strDisDocTypeSelect
  objExe.AddXslParam "DisplayPricingFlag", bDisplayPricingFlag

  'Get XML and transform to HTML.
  strHTML = objExe.ExecuteSpAsXML( "uspEstimateQuickGetDetailXML", "DocumentEstDetails.xsl", strDocumentID, strClaimAspectID, strLynxID, strClaimAspectServiceChannelID, strUserID )

  Response.Write strHTML

  Set objExe = Nothing

  Call KillSessionObject
  Call CheckError

  On Error GoTo 0
%>
