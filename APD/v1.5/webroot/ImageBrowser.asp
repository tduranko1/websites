<%@LANGUAGE="JAVASCRIPT"%> 
<html>
<link rel="stylesheet" href="/styles/apdMain.css" type="text/css">
<head>
<title>APD Image Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<SCRIPT FOR=pixDsp EVENT=DblClick LANGUAGE="JavaScript">
	pixDsp.FitToWindow();
</SCRIPT>
<SCRIPT FOR=Slider1 EVENT=Scroll LANGUAGE="JavaScript">
	if (Slider1.Value > 0)
		pixDsp.ZoomPercent(Slider1.Value*20+ 100);
	else
		pixDsp.ZoomPercent(100);
</SCRIPT>
    

<script language="javascript">
function PageLoad()
{
	pixFile.PageIndex = 1;
	index.innerText = pixFile.PageIndex;
	count.innerText = pixFile.PageCount;
}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" onload="PageLoad()">
<table width="500" border="0" class="ClaimMiscInputs">
  <tr>
    <td>Page:</td>
    <td><span id="index"></span></td>
    <td>Total Pages</td>
    <td><span id="count"></span></td>
	<td>&nbsp;</td>
	<td><input type="button" name="next" value="Next Page" onClick="if (pixFile.PageIndex + 1 > pixFile.PageCount){ pixFile.PageIndex = 1;} else { pixFile.PageIndex = pixFile.PageIndex + 1;} index.innerText = pixFile.PageIndex;">
	</td>
  </tr>
  <tr>
    <td>Zoom:</td>
    <td colspan="5">
		<OBJECT id=Slider1 style="LEFT: 0px; TOP: 0px" codeBase="http://activex.microsoft.com/controls/vb6/Mscomctl.cab" 
	height=34 width=280 classid="clsid:F08DF954-8592-11D1-B16A-00C0F0283628">
	<PARAM NAME="_ExtentX" VALUE="7408">
	<PARAM NAME="_ExtentY" VALUE="900">
	<PARAM NAME="_Version" VALUE="393216">
	<PARAM NAME="BorderStyle" VALUE="1">
	<PARAM NAME="MousePointer" VALUE="0">
	<PARAM NAME="Enabled" VALUE="1">
	<PARAM NAME="OLEDropMode" VALUE="0">
	<PARAM NAME="Orientation" VALUE="0">
	<PARAM NAME="LargeChange" VALUE="5">
	<PARAM NAME="SmallChange" VALUE="1">
	<PARAM NAME="Min" VALUE="0">
	<PARAM NAME="Max" VALUE="10">
	<PARAM NAME="SelectRange" VALUE="0">
	<PARAM NAME="SelStart" VALUE="0">
	<PARAM NAME="SelLength" VALUE="0">
	<PARAM NAME="TickStyle" VALUE="2">
	<PARAM NAME="TickFrequency" VALUE="1">
	<PARAM NAME="Value" VALUE="0">
	<PARAM NAME="TextPosition" VALUE="0"></OBJECT>
	</td>
  </tr>
</table>
<OBJECT style="LEFT: 0px; TOP: 0px" name=pixFile classid="clsid:10000003-1024-11CF-A19E-0020AF333BD8" 
	width=32 height=32 id="pixFile" CODEBASE="activex/Project1.CAB">
	<PARAM NAME="_Version" VALUE="131075">
	<PARAM NAME="_ExtentX" VALUE="847">
	<PARAM NAME="_ExtentY" VALUE="847">
	<PARAM NAME="_StockProps" VALUE="0">
	<PARAM NAME="Active" VALUE="-1">
	<PARAM NAME="LinkID" VALUE="pixFile">
	<PARAM NAME="Reserved1" VALUE="1">
	<PARAM NAME="Reserved2" VALUE="IBBMIIMGINMOFGCAEDGNAAAAAAAAAAAAAAAAAAAAAAAAAAGBDBDCGCGDDDDEGEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">
	<PARAM NAME="ImageDataSource" VALUE="<None>">
	<PARAM NAME="InputFileName" VALUE="<%=String(Request("ImageFile"))%>">
	<PARAM NAME="OutputBitsPerSample" VALUE="1">
	<PARAM NAME="OutputFileAppend" VALUE="0">
	<PARAM NAME="OutputFileFormat" VALUE="24">
	<PARAM NAME="OutputPhotoInterp" VALUE="0">
	<PARAM NAME="OutputSamplesPerPixel" VALUE="1">
	<PARAM NAME="OutputScalingNum" VALUE="0">
	<PARAM NAME="OutputScalingDen" VALUE="0">
	<PARAM NAME="PageIndex" VALUE="1">
	<PARAM NAME="JPEGCompressionFactor" VALUE="0"> 
	<PARAM NAME="OutputRotation" VALUE="0">
	<PARAM NAME="OutputScalingYNum" VALUE="0">
	<PARAM NAME="OutputScalingYDen" VALUE="0">
  </OBJECT> 


<OBJECT style="LEFT: 0px; TOP: 0px" name=pixDsp classid="clsid:10000003-1006-11CF-A19E-0020AF333BD8" 
	width=700 height=550 id="pixDsp" CODEBASE="activex/Project1.CAB">
    <PARAM NAME="_Version" VALUE="131077">
    <PARAM NAME="_ExtentX" VALUE="8255">
    <PARAM NAME="_ExtentY" VALUE="6403">
    <PARAM NAME="_StockProps" VALUE="33">
    <PARAM NAME="BackColor" VALUE="#ffffff">
    <PARAM NAME="BorderStyle" VALUE="2">
    <PARAM NAME="Active" VALUE="-1">
    <PARAM NAME="LinkID" VALUE="pixDsp">
    <PARAM NAME="Reserved1" VALUE="1">
    <PARAM NAME="Reserved2" VALUE="IBBMIIMGINMOFFCAGKGNAAAAAAAAAAAAAAAAAAAAAAAAAAGBDBDCGCGDDDDEGEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">
    <PARAM NAME="PixDispZoomPercentToImg" VALUE="0">
    <PARAM NAME="UsePrintAccel" VALUE="0">
    <PARAM NAME="PixDispBrightness" VALUE="127">
    <PARAM NAME="PixDispContrast" VALUE="127">
    <PARAM NAME="MousePointer" VALUE="2">
    <PARAM NAME="PixDispRedBrightness" VALUE="127">
    <PARAM NAME="PixDispRedContrast" VALUE="127">
    <PARAM NAME="PixDispGreenBrightness" VALUE="127">
    <PARAM NAME="PixDispGreenContrast" VALUE="127">
    <PARAM NAME="PixDispBlueBrightness" VALUE="127">
    <PARAM NAME="PixDispBlueContrast" VALUE="127">
    <PARAM NAME="AcceptDragFiles" VALUE="0">
    <PARAM NAME="FireGUIEvents" VALUE="0">
    <PARAM NAME="ImageDataSource" VALUE="pixFile">
    <PARAM NAME="Invert" VALUE="0">
    <PARAM NAME="LeftButtonOption" VALUE="4">
    <PARAM NAME="LeftMouseBoxStyle" VALUE="1">
    <PARAM NAME="PrintTargetLeft" VALUE="0">
    <PARAM NAME="PrintTargetTop" VALUE="0">
    <PARAM NAME="PrintTargetRight" VALUE="0">
    <PARAM NAME="PrintTargetBottom" VALUE="0">
    <PARAM NAME="RightButtonOption" VALUE="6">
    <PARAM NAME="RightMouseBoxStyle" VALUE="2">
    <PARAM NAME="Rotation" VALUE="0">
    <PARAM NAME="ScaleToGray" VALUE="0">
    <PARAM NAME="ScaleToGrayLevel" VALUE="1">
    <PARAM NAME="ScrollBars" VALUE="3">
    <PARAM NAME="WorkingRegionStyle" VALUE="1">
    <PARAM NAME="ZoomInOutChange" VALUE="15">
  </OBJECT> 

<object classid="clsid:10000003-1003-11CF-A19E-0020AF333BD8" id="Annote1" CODEBASE="activex/Project1.CAB">
  <param name="_Version" value="131075">
  <param name="_ExtentX" value="847">
  <param name="_ExtentY" value="847">
  <param name="_StockProps" value="0">
  <param name="Active" value="-1">
  <param name="LinkID" value="Annote1">
  <param name="Reserved1" value="1">
  <param name="Reserved2" value="IBBMIIMGINMOEFCAHNGNAAAAAAAAAAAAAAAAAAAAAAAAAAGBDBDCGCGDDDDEGEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">
  <param name="PaintDCSource" value="pixDsp">
  <param name="LeftButtonOption" value="2">
  <param name="RightButtonOption" value="0">
  <param name="ShowNotes" value="1">
  <param name="SaveNotes" value="1">
  <param name="PrintNotes" value="1">
  <param name="PrintRedactionsBlack" value="-1">
</object>




</body>
</html>