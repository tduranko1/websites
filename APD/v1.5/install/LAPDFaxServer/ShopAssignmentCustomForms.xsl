<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes" indent="yes" method="xml"/>
  <msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function needExpandedComments(strComments) {
        var blnRet = false;
        if (typeof(strComments) == "string" && strComments != ""){
            var arrLines = strComments.split("\n");
            var iLines = arrLines.length;
            var iChars = strComments.length;
            
            if (iLines > 4 || iChars > 400){
               blnRet = true;
            }
        }
        return blnRet;
    }
    function needExpandedLossDesc(strComments) {
        var blnRet = false;
        if (typeof(strComments) == "string" && strComments != ""){
            var arrLines = strComments.split("\n");
            var iLines = arrLines.length;
            var iChars = strComments.length;

            if (iLines > 1 || iChars > 80){
               blnRet = true;
            }
        }
        return blnRet;
    }
  ]]>
  </msxsl:script>
  <xsl:template match="/">
    <xsl:element name="CustomForm">
      <!-- main form -->
      <xsl:for-each select="/Assignment/Form">
        <xsl:variable name="parentSQL" select="@SQLProcedure"/>
        <xsl:variable name="parentFormID" select="@FormID"/>
        <xsl:variable name="needExpandedComments" select="user:needExpandedComments(string(//Vehicle/@ShopRemarks))"/>
        <xsl:variable name="needExpandedLossDescription" select="user:needExpandedLossDesc(string(//Claim/@LossDescription))"/>
        <xsl:element name="Form">
          <xsl:attribute name="formid">
            <xsl:value-of select="@FormID"/>
          </xsl:attribute>
          <xsl:attribute name="supplementid">
            <xsl:value-of select="@FormSupplementID"/>
          </xsl:attribute>
          <xsl:attribute name="formfile">
            <xsl:value-of select="@PDFPath"/>
          </xsl:attribute>
          <xsl:attribute name="xslfdf">
            <!-- Shop Instructions don't have any data element and does not need a FDF file -->
            <xsl:if test="contains(@Name, 'Shop Instruction') = false()">
              <xsl:value-of select="concat(substring(@PDFPath, 1, string-length(@PDFPath) - 3), 'xsl')"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:element name="Data">
            <xsl:call-template name="copyData">
              <xsl:with-param name="suppressVehComments" select="$needExpandedComments"/>
              <xsl:with-param name="suppressLossDesc" select="$needExpandedLossDescription"/>
            </xsl:call-template>
          </xsl:element>
        </xsl:element>
        <!-- Form Supplements -->
        <xsl:for-each select="Supplement">
          <xsl:if test="(@Name != 'Expanded Comments') or ((@Name = 'Expanded Comments') and ($needExpandedComments = boolean('true') or $needExpandedLossDescription = boolean('true')))">
            <xsl:element name="Form">
              <xsl:attribute name="formid">
                <xsl:value-of select="$parentFormID"/>
              </xsl:attribute>
              <xsl:attribute name="supplementid">
                <xsl:value-of select="@FormSupplementID"/>
              </xsl:attribute>
              <xsl:attribute name="formfile">
                <xsl:value-of select="@PDFPath"/>
              </xsl:attribute>
              <xsl:attribute name="xslfdf">
                <!-- Shop Instructions don't have any data element and does not need a FDF file -->
                <xsl:if test="contains(@Name, 'Shop Instructions') = false()">
                  <xsl:value-of select="concat(substring(@PDFPath, 1, string-length(@PDFPath) - 3), 'xsl')"/>
                </xsl:if>
              </xsl:attribute>
              <xsl:choose>
                <xsl:when test="@SQLProcedure = $parentSQL or @SQLProcedure = ''">
                  <xsl:element name="Data">
                    <xsl:call-template name="copyData">
                      <xsl:with-param name="suppressVehComments">
                        <xsl:choose>
                          <xsl:when test="$needExpandedComments = boolean('true')"><xsl:value-of select="'false'"/></xsl:when>
                          <xsl:otherwise/>
                        </xsl:choose>
                      </xsl:with-param>
                      <xsl:with-param name="suppressLossDesc">
                        <xsl:choose>
                          <xsl:when test="$needExpandedLossDescription = boolean('true')"><xsl:value-of select="'false'"/></xsl:when>
                          <xsl:otherwise/>
                        </xsl:choose>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="SQLProcedure">
                    <xsl:value-of select="@SQLProcedure"/>
                  </xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:element>
          </xsl:if>
        </xsl:for-each>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
  <xsl:template name="copyData">
    <xsl:param name="suppressVehComments"/>
    <xsl:param name="suppressLossDesc"/>
    <xsl:for-each select="/Assignment">
      <xsl:element name="Assignment">
        <xsl:attribute name="suppressVehComments1"><xsl:value-of select="$suppressVehComments"/></xsl:attribute>
        <xsl:attribute name="suppressLossDesc1"><xsl:value-of select="$suppressLossDesc"/></xsl:attribute>
        <xsl:copy-of select="@*"/>
        <xsl:element name="Claim">
          <xsl:copy-of select="Claim/@*"/>
          <xsl:attribute name="LossDescription2">
            <xsl:choose>
              <xsl:when test="string($suppressLossDesc) = 'true'">See next page.</xsl:when>
              <xsl:when test="string($suppressLossDesc) = ''"/>
              <xsl:otherwise>
                <xsl:value-of select="Claim/@LossDescription"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
        </xsl:element>
        <xsl:element name="Coverage">
          <xsl:copy-of select="Coverage/@*"/>
        </xsl:element>
        <xsl:element name="Vehicle">
          <xsl:copy-of select="Vehicle/@*"/>
          <xsl:attribute name="ShopRemarks2">
            <xsl:choose>
              <xsl:when test="string($suppressVehComments) = 'true'">See next page.</xsl:when>
              <xsl:when test="string($suppressVehComments) = ''"/>
              <xsl:otherwise>
                <xsl:value-of select="Vehicle/@ShopRemarks"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
        </xsl:element>
        <xsl:element name="VehicleContact">
          <xsl:copy-of select="VehicleContact/@*"/>
        </xsl:element>
        <xsl:element name="Shop">
          <xsl:copy-of select="Shop/@*"/>
        </xsl:element>
        <xsl:element name="Owner">
          <xsl:copy-of select="Owner/@*"/>
        </xsl:element>
        <xsl:element name="LynxRep">
          <xsl:copy-of select="LynxRep/@*"/>
        </xsl:element>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
