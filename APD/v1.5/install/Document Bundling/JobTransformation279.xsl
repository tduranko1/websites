<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">

  <xsl:output method="xml"
              omit-xml-declaration="yes"
              cdata-section-elements="Document EmailSubject EmailBody Subject Body Message MessageEscaped MessageHTML AttachmentsHTML NotesMsg"
               />

  <msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    var dt = new Date();
    var intSeq = 0;
    function getDate(strFormat){
      var strRet = "";
      if (dt == null) dt = new Date();
      switch (strFormat) {
        case "SQL":
             strRet = dt.getFullYear() + "-" +
                      formatNumber((dt.getMonth() + 1), 2) + "-" +
                      formatNumber(dt.getDate(), 2) + "T" +
                      formatNumber(dt.getHours(), 2) + ":" +
                      formatNumber(dt.getMinutes(), 2) + ":" +
                      formatNumber(dt.getSeconds(), 2) + "-05:00";
             break;
        case "PLAIN":
             strRet = dt.getFullYear() +
                      formatNumber((dt.getMonth() + 1), 2) +
                      formatNumber(dt.getDate(), 2) +
                      formatNumber(dt.getHours(), 2) +
                      formatNumber(dt.getMinutes(), 2) +
                      formatNumber(dt.getSeconds(), 2);
             break;
      }
      return strRet;
    }
    
    function getFileName(strClaimNumber, intSeqNumber){
       var strRet = strClaimNumber.replace(/[^A-Za-z0-9]/g, "") +
                    getDate("PLAIN") +
                    formatNumber(intSeqNumber, 3);
       return strRet;
    }
    
    function getAutoFileName(strClaimNumber){
       var strRet = strClaimNumber.replace(/[^A-Za-z0-9]/g, "") +
                    getDate("PLAIN") +
                    formatNumber(intSeq, 3);
       intSeq++;
       return strRet;
    }
    
    function getSeqNumber(){
       return intSeq;
    }

    function formatNumber(intNum, intZeroPadLength) {
      var strNum = intNum + "";
      var strRet = "";
      for (var i = 0; i < intZeroPadLength - strNum.length; i++)
          strRet += "0";
      strRet += strNum;
      return strRet;
    }
    
    function getClaimNumber(strClaimNumber){
      var strRet = "";
      if (strClaimNumber != ""){
         var re = new RegExp("^(\\d{6})[\\s|\\-]{1,}(\\d{2})[\\s|\\-]{1,}([a-z]{1}\\s{1}[a-z0-9]{1}\\d{5,})$", "i");
         if (re.test(strClaimNumber) == true){
            var arr = re.exec(strClaimNumber);
            strRet = arr[1];
         } else {
           strRet = strClaimNumber;
         }
      }
      return strRet;
    }

    function getClaimantNumber(strClaimNumber){
      var strRet = "";
      if (strClaimNumber != ""){
         var re = new RegExp("^(\\d{6})[\\s|\\-]{1,}(\\d{2})[\\s|\\-]{1,}([a-z]{1}\\s{1}[a-z0-9]{1}\\d{5,})$", "i");
         if (re.test(strClaimNumber) == true){
            var arr = re.exec(strClaimNumber);
            strRet = arr[2];
         }
      }
      return strRet;
    }

    function getPolicyNumber(strClaimNumber){
      var strRet = "";
      if (strClaimNumber != ""){
         var re = new RegExp("^(\\d{6})[\\s|\\-]{1,}(\\d{2})[\\s|\\-]{1,}([a-z]{1}\\s{1}[a-z0-9]{1}\\d{5,})$", "i");
         if (re.test(strClaimNumber) == true){
            var arr = re.exec(strClaimNumber);
            strRet = arr[3];
         }
      }
      return strRet;
    }
  ]]>
  </msxsl:script>
  <xsl:template match="/">
  <xsl:choose>
      <xsl:when test="//Send/@via = 'FTP'">
          <xsl:call-template name="reformat"/>
      </xsl:when>
      <xsl:otherwise>
          <xsl:copy-of select="/"/>
      </xsl:otherwise>
  </xsl:choose>
  </xsl:template>
  <xsl:template name="reformat">
    <ClientDocument>
      <Send>
        <xsl:copy-of select="//Send/@*"/>
        <xsl:copy-of select="//Send/User"/>
        <xsl:copy-of select="//Send/EmailSubject"/>
        <xsl:copy-of select="//Send/EmailBody"/>
        <PackageType>PDF</PackageType>
        <!-- <xsl:copy-of select="//Send/PackageType"/> -->
        <MergedFileName>
          <xsl:value-of select="user:getFileName(string(//Claim/@ClientClaimNumber), 0)"/>
        </MergedFileName>
        <xsl:copy-of select="//Send/Success"/>
        <xsl:copy-of select="//Send/Failure"/>
      </Send>
      <Message merge="true"><xsl:value-of select="/ClientDocument/Message"/></Message>
      <MessageEscaped merge="true"><xsl:value-of select="/ClientDocument/MessageEscaped"/></MessageEscaped>
      <MessageHTML merge="true"><xsl:value-of select="/ClientDocument/MessageHTML"/></MessageHTML>
      <xsl:copy-of select="/ClientDocument/Claim"/>
      <xsl:copy-of select="/ClientDocument/Vehicle"/>
      <xsl:copy-of select="/ClientDocument/AttachmentsHTML"/>
      <xsl:copy-of select="/ClientDocument/NotesMsg"/>

      <!-- Generate the control files for pdf-->
      <xsl:if test="count(/ClientDocument/Document[@documentTypeID!='3' and @documentTypeID!='10' and @documentTypeID!='8']) &gt; 0 or /ClientDocument/Message != ''">
        <xsl:variable name="fileName" select="user:getAutoFileName(string(//Claim/@ClientClaimNumber))"/>
        <Document>
          <xsl:attribute name="id">-1</xsl:attribute>
          <xsl:attribute name="createDate"><xsl:value-of select="user:getDate('SQL')"/></xsl:attribute>
          <xsl:attribute name="sequenceNumber">-1</xsl:attribute>
          <xsl:attribute name="documentTypeID">-1</xsl:attribute>
          <xsl:attribute name="documentTypeName">__ControlFile</xsl:attribute>
          <xsl:attribute name="imageEmbedded">true</xsl:attribute>
          <xsl:attribute name="imageLocation"/>
          <xsl:attribute name="outputFormat">xml</xsl:attribute>
          <xsl:attribute name="outputFileName"><xsl:value-of select="concat($fileName, '.xml')"/></xsl:attribute>
          <xsl:attribute name="merge">false</xsl:attribute>
          <xsl:attribute name="htmlOptions">$DONTMERGEWITHJOBDOCUMENT$</xsl:attribute>
          <xsl:attribute name="exception">true</xsl:attribute>&lt;?xml version="1.0" standalone="yes"?&gt;
&lt;PhotoSetInfo xmlns="http://tempuri.org/PhotoSetInfo.xsd"&gt;
&lt;PhotoInfo&gt;
  &lt;Claim&gt;<xsl:value-of select="user:getClaimNumber(string(//Claim/@ClientClaimNumber))"/>&lt;/Claim&gt;
  &lt;UserID&gt;LYNX&lt;/UserID&gt;
  &lt;DateInput&gt;<xsl:value-of select="user:getDate('SQL')"/>&lt;/DateInput&gt;
  &lt;Type&gt;CORR&lt;/Type&gt;
  &lt;Claimant&gt;<xsl:value-of select="user:getClaimantNumber(string(//Claim/@ClientClaimNumber))"/>&lt;/Claimant&gt;
  &lt;Policy&gt;<xsl:choose><xsl:when test="//Claim/@PolicyNumber != ''"><xsl:value-of select="//Claim/@PolicyNumber"/></xsl:when><xsl:otherwise><xsl:value-of select="user:getPolicyNumber(string(//Claim/@ClientClaimNumber))"/></xsl:otherwise></xsl:choose>&lt;/Policy&gt;
  &lt;ItemNumber&gt;&lt;/ItemNumber&gt;
  &lt;MachineName&gt;&lt;/MachineName&gt;
&lt;/PhotoInfo&gt;
&lt;PhotoFiles&gt;
  &lt;File&gt;<xsl:value-of select="concat($fileName, '.pdf')"/>&lt;/File&gt;
  &lt;Description&gt;&lt;/Description&gt;
&lt;/PhotoFiles&gt;
&lt;/PhotoSetInfo&gt;
        </Document>
        <Document>
          <xsl:attribute name="id">-1</xsl:attribute>
          <xsl:attribute name="createDate"><xsl:value-of select="user:getDate('SQL')"/></xsl:attribute>
          <xsl:attribute name="sequenceNumber">-1</xsl:attribute>
          <xsl:attribute name="documentTypeID">-1</xsl:attribute>
          <xsl:attribute name="documentTypeName">__Message</xsl:attribute>
          <xsl:attribute name="imageEmbedded">true</xsl:attribute>
          <xsl:attribute name="imageLocation"/>
          <xsl:attribute name="outputFormat">htm</xsl:attribute>
          <xsl:attribute name="outputFileName"><xsl:value-of select="concat($fileName, '.htm')"/></xsl:attribute>
          <xsl:attribute name="merge">true</xsl:attribute>
          <xsl:attribute name="exception">false</xsl:attribute>&lt;html&gt;&lt;head&gt;&lt;/head&gt;&lt;body&gt;<xsl:value-of select="/ClientDocument/MessageHTML" disable-output-escaping="yes"/>&lt;/body&gt;&lt;/html&gt;
        </Document>
        <xsl:copy-of select="/ClientDocument/Document[@documentTypeID!='13' and @documentTypeID!='3' and @documentTypeID!='10' and @documentTypeID!='8']"/>
      </xsl:if>

      <!-- Generate the control files for invoice -->
      <xsl:if test="count(/ClientDocument/Document[@documentTypeID='13']) &gt; 0">
        <xsl:variable name="fileName" select="user:getAutoFileName(string(//Claim/@ClientClaimNumber))"/>
        <Document>
          <xsl:attribute name="id">-1</xsl:attribute>
          <xsl:attribute name="createDate"><xsl:value-of select="user:getDate('SQL')"/></xsl:attribute>
          <xsl:attribute name="sequenceNumber">-1</xsl:attribute>
          <xsl:attribute name="documentTypeID">-1</xsl:attribute>
          <xsl:attribute name="documentTypeName">__ControlFile</xsl:attribute>
          <xsl:attribute name="imageEmbedded">true</xsl:attribute>
          <xsl:attribute name="imageLocation"/>
          <xsl:attribute name="outputFormat">xml</xsl:attribute>
          <xsl:attribute name="outputFileName"><xsl:value-of select="concat($fileName, '.xml')"/></xsl:attribute>
          <xsl:attribute name="merge">false</xsl:attribute>
          <xsl:attribute name="htmlOptions">$DONTMERGEWITHJOBDOCUMENT$</xsl:attribute>
          <xsl:attribute name="exception">true</xsl:attribute>&lt;?xml version="1.0" standalone="yes"?&gt;
&lt;PhotoSetInfo xmlns="http://tempuri.org/PhotoSetInfo.xsd"&gt;
&lt;PhotoInfo&gt;
  &lt;Claim&gt;<xsl:value-of select="user:getClaimNumber(string(//Claim/@ClientClaimNumber))"/>&lt;/Claim&gt;
  &lt;UserID&gt;LYNX&lt;/UserID&gt;
  &lt;DateInput&gt;<xsl:value-of select="user:getDate('SQL')"/>&lt;/DateInput&gt;
  &lt;Type&gt;INVOICE&lt;/Type&gt;
  &lt;Claimant&gt;<xsl:value-of select="user:getClaimantNumber(string(//Claim/@ClientClaimNumber))"/>&lt;/Claimant&gt;
  &lt;Policy&gt;<xsl:choose><xsl:when test="//Claim/@PolicyNumber != ''"><xsl:value-of select="//Claim/@PolicyNumber"/></xsl:when><xsl:otherwise><xsl:value-of select="user:getPolicyNumber(string(//Claim/@ClientClaimNumber))"/></xsl:otherwise></xsl:choose>&lt;/Policy&gt;
  &lt;ItemNumber&gt;&lt;/ItemNumber&gt;
  &lt;MachineName&gt;&lt;/MachineName&gt;
&lt;/PhotoInfo&gt;
&lt;PhotoFiles&gt;
  &lt;File&gt;<xsl:value-of select="concat($fileName, '.pdf')"/>&lt;/File&gt;
  &lt;Description&gt;&lt;/Description&gt;
&lt;/PhotoFiles&gt;
&lt;/PhotoSetInfo&gt;
        </Document>
        <xsl:for-each select="/ClientDocument/Document[@documentTypeID='13']">
          <xsl:if test="position() = 1">
          <Document>
            <xsl:for-each select="@*">
              <xsl:choose>
                <xsl:when test="name(.) = 'outputFileName'">
                  <xsl:attribute name="outputFileName">
                    <xsl:value-of select="concat($fileName, '.pdf')"/>
                  </xsl:attribute>
                </xsl:when>
                <xsl:when test="name(.) = 'noconvert'">
                  <xsl:attribute name="noconvert">false</xsl:attribute>
                </xsl:when>
                <xsl:when test="name(.) = 'merge'">
                  <xsl:attribute name="merge">false</xsl:attribute>
                </xsl:when>
                <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </Document>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>

      <!-- Generate the control files for estimate -->
      <xsl:if test="count(/ClientDocument/Document[@documentTypeID='3' or @documentTypeID='10']) &gt; 0">
        <xsl:variable name="fileName" select="user:getAutoFileName(string(//Claim/@ClientClaimNumber))"/>
        <Document>
          <xsl:attribute name="id">-1</xsl:attribute>
          <xsl:attribute name="createDate"><xsl:value-of select="user:getDate('SQL')"/></xsl:attribute>
          <xsl:attribute name="sequenceNumber">-1</xsl:attribute>
          <xsl:attribute name="documentTypeID">-1</xsl:attribute>
          <xsl:attribute name="documentTypeName">__ControlFile</xsl:attribute>
          <xsl:attribute name="imageEmbedded">true</xsl:attribute>
          <xsl:attribute name="imageLocation"/>
          <xsl:attribute name="outputFormat">xml</xsl:attribute>
          <xsl:attribute name="outputFileName"><xsl:value-of select="concat($fileName, '.xml')"/></xsl:attribute>
          <xsl:attribute name="merge">false</xsl:attribute>
          <xsl:attribute name="htmlOptions">$DONTMERGEWITHJOBDOCUMENT$</xsl:attribute>
          <xsl:attribute name="exception">true</xsl:attribute>&lt;?xml version="1.0" standalone="yes"?&gt;
&lt;PhotoSetInfo xmlns="http://tempuri.org/PhotoSetInfo.xsd"&gt;
&lt;PhotoInfo&gt;
  &lt;Claim&gt;<xsl:value-of select="user:getClaimNumber(string(//Claim/@ClientClaimNumber))"/>&lt;/Claim&gt;
  &lt;UserID&gt;LYNX&lt;/UserID&gt;
  &lt;DateInput&gt;<xsl:value-of select="user:getDate('SQL')"/>&lt;/DateInput&gt;
  &lt;Type&gt;EST/TL&lt;/Type&gt;
  &lt;Claimant&gt;<xsl:value-of select="user:getClaimantNumber(string(//Claim/@ClientClaimNumber))"/>&lt;/Claimant&gt;
  &lt;Policy&gt;<xsl:choose><xsl:when test="//Claim/@PolicyNumber != ''"><xsl:value-of select="//Claim/@PolicyNumber"/></xsl:when><xsl:otherwise><xsl:value-of select="user:getPolicyNumber(string(//Claim/@ClientClaimNumber))"/></xsl:otherwise></xsl:choose>&lt;/Policy&gt;
  &lt;ItemNumber&gt;&lt;/ItemNumber&gt;
  &lt;MachineName&gt;&lt;/MachineName&gt;
&lt;/PhotoInfo&gt;
&lt;PhotoFiles&gt;
  &lt;File&gt;<xsl:value-of select="concat($fileName, '.pdf')"/>&lt;/File&gt;
  &lt;Description&gt;&lt;/Description&gt;
&lt;/PhotoFiles&gt;
&lt;/PhotoSetInfo&gt;
        </Document>
        <xsl:for-each select="/ClientDocument/Document[@documentTypeID='3' or @documentTypeID='10']">
          <xsl:if test="position() = 1">
          <Document>
            <xsl:for-each select="@*">
              <xsl:choose>
                <xsl:when test="name(.) = 'outputFileName'">
                  <xsl:attribute name="outputFileName">
                    <xsl:value-of select="concat($fileName, '.pdf')"/>
                  </xsl:attribute>
                </xsl:when>
                <xsl:when test="name(.) = 'noconvert'">
                  <xsl:attribute name="noconvert">false</xsl:attribute>
                </xsl:when>
                <xsl:when test="name(.) = 'merge'">
                  <xsl:attribute name="merge">false</xsl:attribute>
                </xsl:when>
                <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </Document>
          </xsl:if>
        </xsl:for-each>
      </xsl:if>

      <!-- photos -->
      <xsl:if test="count(/ClientDocument/Document[@documentTypeID='8']) &gt; 0">
        <xsl:variable name="fileName" select="user:getAutoFileName(string(//Claim/@ClientClaimNumber))"/>
        <xsl:variable name="seedPosition" select="user:getSeqNumber() - 1"/>
        <Document>
          <xsl:attribute name="id">-2</xsl:attribute>
          <xsl:attribute name="createDate"><xsl:value-of select="user:getDate('SQL')"/></xsl:attribute>
          <xsl:attribute name="sequenceNumber">-1</xsl:attribute>
          <xsl:attribute name="documentTypeID">-1</xsl:attribute>
          <xsl:attribute name="documentTypeName">__ControlFile</xsl:attribute>
          <xsl:attribute name="imageEmbedded">true</xsl:attribute>
          <xsl:attribute name="imageLocation"/>
          <xsl:attribute name="outputFormat">xml</xsl:attribute>
          <xsl:attribute name="outputFileName"><xsl:value-of select="concat($fileName, '.xml')"/></xsl:attribute>
          <xsl:attribute name="htmlOptions">$DONTMERGEWITHJOBDOCUMENT$</xsl:attribute>
          <xsl:attribute name="merge">false</xsl:attribute>
          <xsl:attribute name="exception">true</xsl:attribute>&lt;?xml version="1.0" standalone="yes"?&gt;
&lt;PhotoSetInfo xmlns="http://tempuri.org/PhotoSetInfo.xsd"&gt;
&lt;PhotoInfo&gt;
  &lt;Claim&gt;<xsl:value-of select="user:getClaimNumber(string(//Claim/@ClientClaimNumber))"/>&lt;/Claim&gt;
  &lt;UserID&gt;LYNX&lt;/UserID&gt;
  &lt;DateInput&gt;<xsl:value-of select="user:getDate('SQL')"/>&lt;/DateInput&gt;
  &lt;Type&gt;PHOTO&lt;/Type&gt;
  &lt;Claimant&gt;<xsl:value-of select="user:getClaimantNumber(string(//Claim/@ClientClaimNumber))"/>&lt;/Claimant&gt;
  &lt;Policy&gt;<xsl:choose><xsl:when test="//Claim/@PolicyNumber != ''"><xsl:value-of select="//Claim/@PolicyNumber"/></xsl:when><xsl:otherwise><xsl:value-of select="user:getPolicyNumber(string(//Claim/@ClientClaimNumber))"/></xsl:otherwise></xsl:choose>&lt;/Policy&gt;
  &lt;ItemNumber&gt;&lt;/ItemNumber&gt;
  &lt;MachineName&gt;&lt;/MachineName&gt;
&lt;/PhotoInfo&gt;<xsl:for-each select="//Document[@merge='false']">
&lt;PhotoFiles&gt;
  &lt;File&gt;<xsl:value-of select="concat(user:getFileName(string(//Claim/@ClientClaimNumber), number($seedPosition) + (position() - 1)), '.jpg')"/>&lt;/File&gt;
  &lt;Description&gt;&lt;/Description&gt;
&lt;/PhotoFiles&gt;</xsl:for-each>
&lt;/PhotoSetInfo&gt;
        </Document>
        <xsl:for-each select="/ClientDocument/Document[@documentTypeID='8']">
          <xsl:variable name="documentPosition" select="number($seedPosition) + (position() - 1)"/>
          <Document>
            <xsl:for-each select="@*">
              <xsl:choose>
                <xsl:when test="name(.) = 'outputFileName'">
                  <xsl:attribute name="outputFileName">
                    <xsl:value-of select="concat(user:getFileName(string(//Claim/@ClientClaimNumber), $documentPosition), '.jpg')"/>
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </Document>
        </xsl:for-each>
      </xsl:if>
    </ClientDocument>
  </xsl:template>
</xsl:stylesheet>