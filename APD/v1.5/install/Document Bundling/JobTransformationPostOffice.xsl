<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- Vars -->

  <xsl:template match="/">
    <xsl:param name="clientid" select="194"/>
    <xsl:param name="clientname" select="'TXFB'"/>
    <xsl:param name="claimnumber" select="'Z6132704'"/>
    <xsl:param name="lynxid" select="1848438"/>
    <xsl:param name="submittingserver" select="'hyapdschdst1'"/>

    <xsl:param name="source" select="'D:\Temp\Test PDFs\Doc000.jpg'"/>
    <xsl:param name="filename" select="'Doc000.jpg'"/>
    <xsl:param name="primary" select="1"/>

    <JobDetail>
      <xsl:attribute name="clientid">
        <xsl:value-of select="$clientid"/>
      </xsl:attribute>
      <xsl:attribute name="clientname">
        <xsl:value-of select="$clientname"/>
      </xsl:attribute>
      <xsl:attribute name="lynxid">
        <xsl:value-of select="$lynxid"/>
      </xsl:attribute>
      <xsl:attribute name="claimnumber">
        <xsl:value-of select="$claimnumber"/>
      </xsl:attribute>
      <xsl:attribute name="submittingserver">
        <xsl:value-of select="$submittingserver"/>
      </xsl:attribute>
      <FileList>
        <xsl:for-each select="/JobDetail/FileList/File">
            <File>
            <xsl:attribute name="source">
              <xsl:value-of select="@source"/>
            </xsl:attribute>
          </File>
        </xsl:for-each>
      </FileList>  
    </JobDetail>
  </xsl:template>
</xsl:stylesheet>