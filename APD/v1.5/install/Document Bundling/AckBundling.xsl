<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- Vars -->
  <xsl:param name="submittingserver" select="'HyperAPDSchdST1'"/>

  <xsl:template match="/">
    <JobDetail>
      <xsl:attribute name="clientid">
        <xsl:value-of select="/ClientDocument/Claim/@InsuranceCompanyID"/>
      </xsl:attribute>
      <xsl:attribute name="clientname">
        <xsl:value-of select="@Documents"/>
      </xsl:attribute>
      <xsl:attribute name="lynxid">
        <xsl:value-of select="/ClientDocument/Claim/@LynxID"/>
      </xsl:attribute>
      <xsl:attribute name="claimnumber">
        <xsl:value-of select="/ClientDocument/Claim/@ClientClaimNumber"/>
      </xsl:attribute>
      <xsl:attribute name="submittingserver">
        <xsl:value-of select="$submittingserver"/>
      </xsl:attribute>
      <FileList>
        <xsl:for-each select="/ClientDocument/Document">
          <File>
          <xsl:attribute name="source">
            <xsl:value-of select="@imageLocation"/>
          </xsl:attribute>
        </File>
      </xsl:for-each>
      </FileList>

      <EMAIL>
        <xsl:attribute name="ignoreenvoverride">
          <xsl:value-of select="'yes'"/>
        </xsl:attribute>
        <xsl:attribute name="priority">
          <xsl:value-of select="5"/>
        </xsl:attribute>
        <xsl:attribute name="stepid">
          <xsl:value-of select="1"/>
        </xsl:attribute>
        <Subject>
          <xsl:value-of select="/ClientDocument/Send/EmailSubject"/>
        </Subject>
        <Recipient>
          <xsl:attribute name="type">
            <xsl:value-of select="'to'"/>
          </xsl:attribute>
          <xsl:value-of select="/ClientDocument/Send/@value"/>
        </Recipient>
        <Body stylesheet="">
          <xsl:value-of select="/ClientDocument/Send/EmailBody"/>
        </Body>
        <Priority stylesheet="" basedonescalationvalue="no" />
      </EMAIL>      
    </JobDetail>
  </xsl:template>
</xsl:stylesheet>