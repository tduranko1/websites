<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">

  <xsl:output method="xml"
              omit-xml-declaration="yes"
              cdata-section-elements="Document EmailSubject EmailBody Subject Body Message MessageEscaped MessageHTML AttachmentsHTML NotesMsg"
               />


  <xsl:template match="/">
    <xsl:call-template name="reformat"/>
  </xsl:template>
  <xsl:template name="reformat">
    <xsl:variable name="isTest">
      <xsl:choose>
        <xsl:when test="contains(/ClientDocument/Send/@bundlingProfileName, 'Closing Repair Complete') = true()">1</xsl:when>
        <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="isExternal">
      <xsl:choose>
        <xsl:when test="/ClientDocument/Send[@via='EML' and contains(@value,'Maryellen') = false()]">1</xsl:when>
        <xsl:when test="/ClientDocument/Send[@via='EML' and contains(@value,'Maryellen') = false()] and contains(//Message, 'Total Loss') = true()">1</xsl:when>
        <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="ServiceChannelCD">
      <xsl:value-of select="//Vehicle/@ServiceChannelCD"/>
    </xsl:variable>
    <ClientDocument>
      <Send>
        <xsl:attribute name="isExternal">
          <xsl:value-of select="$isExternal"/>
        </xsl:attribute>
        <!-- we will skip the actual send. The subsequent File copy will take care of Nugen -->
        <xsl:if test="/ClientDocument/Send/@description='Web Services' and /ClientDocument/Vehicle/@isNugen = '1' and $ServiceChannelCD = 'PS'">
          <xsl:attribute name="skipSend">1</xsl:attribute>
        </xsl:if>
        <xsl:copy-of select="//Send/@*"/>
        <xsl:copy-of select="//Send/User"/>
        <EmailSubject>
          <xsl:value-of select="//Send/EmailSubject"/>
        </EmailSubject>
        <EmailBody>Please see the attached documents.|$AttachmentList$||This email message is for the exclusive use of the recipient (s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, copying, action taken in reliance on the contents or distribution is strictly prohibited. If you received this email in error, contact the sender by reply email and destroy all copies of the original message. Thank you.</EmailBody>
        <xsl:copy-of select="//Send/MergedFileName"/>
        <MergedFileName>
        </MergedFileName>
        <PackageType>PDF</PackageType>
        <xsl:copy-of select="//Send/Success"/>
        <xsl:copy-of select="//Send/Failure"/>
      </Send>
      <xsl:choose>
        <!--<xsl:when test="$isTest = '1' and /ClientDocument/Send/@via='EML'">
          <File location="\\pgw.local\dfs\intra\apddocuments\DEV\QBE"/>
        </xsl:when>-->
        <!-- Nugen Claims Electric insurance does not have Claim GUID so we make condition isTest is '0'-->
        <xsl:when test="/ClientDocument/Send/@description='Web Services' and /ClientDocument/Vehicle/@isNugen = '1' and $ServiceChannelCD = 'PS'">
          <File location="\\pgw.local\dfs\intra\apddocuments\STG\Electric"/>
        </xsl:when>
        <!-- Electric claims via old process -->
        <xsl:when test="$isTest != '0' and /ClientDocument/Send/@via='EML'">
          <File location="\\SFTMPRIMARY\apd$\Pathways\Assignments\SFTMAPDPRDFS1\PATHWAYS\PROCESSCLAIMS UPLOAD\Claim Docs"/>
        </xsl:when>
      </xsl:choose>
      <Message merge="true">
        <xsl:value-of select="/ClientDocument/Message"/>
      </Message>
      <MessageEscaped merge="true">
        <xsl:value-of select="/ClientDocument/MessageEscaped"/>
      </MessageEscaped>
      <MessageHTML merge="true">
        <xsl:value-of select="/ClientDocument/MessageHTML"/>
      </MessageHTML>
      <xsl:copy-of select="/ClientDocument/Claim"/>
      <xsl:copy-of select="/ClientDocument/Vehicle"/>
      <xsl:copy-of select="/ClientDocument/AttachmentsHTML"/>
      <xsl:copy-of select="/ClientDocument/NotesMsg"/>
      <!--  Now process all the documents -->
      <xsl:choose>
        <xsl:when test="/ClientDocument/Send/@via='EML' and /ClientDocument/Vehicle/@isNugen = '1' and $ServiceChannelCD = 'PS'">
          <!-- we have to add sequence number to the photograph because there can be multiplt -->
          <xsl:for-each select="/ClientDocument/Document[@documentTypeName = 'Photograph']">
            <Document>
              <xsl:variable name="merge">
                <xsl:choose>
                  <xsl:when test="@documentTypeName = 'Estimate' or @documentTypeName = 'Estimate EMS' or @documentTypeName = 'Invoice' or @documentTypeName = 'Photograph'">false</xsl:when>
                  <xsl:otherwise>true</xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:variable name="photoIndex" select="position()"/>
              <xsl:for-each select="@*">
                <xsl:choose>
                  <xsl:when test="name(.) = 'merge'">
                    <xsl:attribute name="merge">
                      <xsl:value-of select="$merge"/>
                    </xsl:attribute>
                  </xsl:when>
                  <xsl:when test="name(.) = 'exception'">
                    <xsl:attribute name="exception">true</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="name(.)='outputFileName'">
                    <xsl:attribute name="outputFileName">
                       <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Photograph ', $photoIndex, ' - ', //Claim/@LynxID, '-', //Vehicle/@number, '.jpg')"/>
                      <!--<xsl:value-of select="concat(substring-before(., '.jpg'), ' (', $photoIndex, ').jpg')"/>-->
                    </xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:copy-of select="."/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </Document>
          </xsl:for-each>
          <!-- now add all non-photograph documents -->
          <xsl:for-each select="/ClientDocument/Document[@documentTypeName != 'Photograph']">
            <Document>
              <xsl:variable name="merge">
                <xsl:choose>
                  <xsl:when test="@documentTypeName = 'Estimate' or @documentTypeName = 'Estimate EMS' or @documentTypeName = 'Invoice' or @documentTypeName = 'Photograph' or @documentTypeName = 'Rental Invoice' or @documentTypeName = 'Direction To Pay'">false</xsl:when>
                  <xsl:otherwise>true</xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:variable name="exception">
                <xsl:choose>
                  <xsl:when test="@documentTypeName = 'Estimate' or @documentTypeName = 'Estimate EMS' or @documentTypeName = 'Invoice' or @documentTypeName = 'Photograph' or @documentTypeName = 'Rental Invoice' or @documentTypeName = 'Direction To Pay'">true</xsl:when>
                  <xsl:otherwise>false</xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:for-each select="@*">
                <xsl:choose>
                  <xsl:when test="name(.) = 'merge'">
                    <xsl:attribute name="merge">
                      <xsl:value-of select="$merge"/>
                    </xsl:attribute>
                  </xsl:when>
                  <xsl:when test="name(.) = 'exception'">
                    <xsl:attribute name="exception">
                      <xsl:value-of select="$exception"/>
                    </xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:copy-of select="."/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </Document>
          </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
          <xsl:for-each select="/ClientDocument/Document">
            <xsl:copy-of select="."/>
          </xsl:for-each>
        </xsl:otherwise>
      </xsl:choose>
    </ClientDocument>
  </xsl:template>
</xsl:stylesheet>




















