<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">

  <xsl:output method="xml"
              omit-xml-declaration="yes"
              cdata-section-elements="Document EmailSubject EmailBody Subject Body Message MessageEscaped MessageHTML AttachmentsHTML NotesMsg"
               />


  <xsl:template match="/">
    <xsl:call-template name="reformat"/>
  </xsl:template>
  <xsl:template name="reformat">
    <xsl:variable name="isRRP">
      <xsl:choose>
        <xsl:when test="contains(/ClientDocument/Send/@bundlingProfileName, 'RRP -') = true()">1</xsl:when>
        <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="isTest">
      <xsl:choose>
        <xsl:when test="contains(/ClientDocument/Send/@bundlingProfileName, 'Program Shop Complete (EMS Test)') = true()">1</xsl:when>
        <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="isExternal">
      <xsl:choose>
        <xsl:when test="/ClientDocument/Send[@via='EML' and contains(@value,'qbeapd') = false()]">1</xsl:when>
        <xsl:when test="/ClientDocument/Send[@via='EML' and contains(@value,'qbeapd') = false()] and contains(//Message, 'Total Loss') = true()">1</xsl:when>
        <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="isTotalLoss">
      <xsl:if test="contains(//Send/@bundlingProfileName, 'Closing Total Loss') = true()">1</xsl:if>
    </xsl:variable>
    <xsl:variable name="isClosingSupplement">
      <xsl:choose>
        <xsl:when test="contains(//Send/@bundlingProfileName, 'Closing Supplement') = true() and $isTotalLoss != '1'">1</xsl:when>
        <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <ClientDocument>
      <Send>
        <xsl:attribute name="isExternal"><xsl:value-of select="$isExternal"/></xsl:attribute>
        <xsl:attribute name="isClosingSupplement"><xsl:value-of select="$isClosingSupplement"/></xsl:attribute>
        <xsl:attribute name="isTotalLoss"><xsl:value-of select="$isTotalLoss"/></xsl:attribute>
        <xsl:attribute name="isRRP"><xsl:value-of select="$isRRP"/></xsl:attribute>
        <xsl:copy-of select="//Send/@*"/>

        <xsl:copy-of select="//Send/User"/>
        <xsl:choose>
          <xsl:when test="$isExternal='1'">
            <EmailSubject>
              <xsl:choose>
                <xsl:when test="$isRRP='1'">
                  <xsl:value-of select="concat(//ClaimInfo/Claim/@ClientClaimNumber, ' :Claim # / Insured: ', //ClaimInfo/Claim/@InsuredName, ' / Loss Date: ', //ClaimInfo/Claim/@LossDate,' / LYNX ID: ', /ClientDocument/Claim/@LynxID, '-', /ClientDocument/Vehicle/@number)"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="concat('QBE ProcessClaims Upload LYNX ID:', //Claim/@LynxID, '-', //Vehicle/@number, ' - Claim #: ', //Claim/@ClientClaimNumber)"/>
                </xsl:otherwise>
              </xsl:choose>
            </EmailSubject>
            <EmailBody>Please see the attached documents.|$AttachmentList$||This email message is for the exclusive use of the recipient (s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, copying, action taken in reliance on the contents or distribution is strictly prohibited. If you received this email in error, contact the sender by reply email and destroy all copies of the original message. Thank you.</EmailBody>
            <xsl:copy-of select="//Send/MergedFileName"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="$isTotalLoss = '1'">
                <xsl:copy-of select="//Send/EmailSubject"/>
                <EmailBody>Please upload to ProcessClaims the following files on completed QBE <xsl:choose><xsl:when test="$isRRP='1'">RRP</xsl:when><xsl:otherwise>desk audit</xsl:otherwise></xsl:choose>: ||LYNX ID <xsl:value-of select="concat(//Claim/@LynxID,'-', //Vehicle/@number)"/>   Claim # <xsl:value-of select="//Claim/@ClientClaimNumber"/>||* Estimate (EMS Data): (15 files; Respond "See Audit Results" or "SAR" to RBI audit questions at upload.)|* Estimate: PDF Original Estimate|* Estimate: PDF Audited Estimate<xsl:choose><xsl:when test="$isRRP='1'">|* IA Fee Bill: Memo Bill|* Vehicle Valuation: Consolidated PDF Closing Message &amp; Other Docs|* Images: Photos</xsl:when><xsl:otherwise>|* IA Fee Bill: Invoice|* Vehicle Valuation: Consolidated PDF Total Loss Closing Message &amp; Desk Audit Summary Report</xsl:otherwise></xsl:choose>||** Mark this claim as Total Loss in Process Claims **||ProcessClaims Web Link: http://www.processclaims.com|ID: gc_lynx|PW: <xsl:choose><xsl:when test="$isRRP='1'">Xxxnnn</xsl:when>

<xsl:otherwise>Abc123</xsl:otherwise></xsl:choose>||QBE Claim Number Format|-----------------------|General Casualty (GC):        000-00-00000  or A000000 or BA000000|Unigard (UIC):                0000000|QBE Agri (QIC):|National Farmers Union (NFU): PA00000000 or  RU00000000 or FO00000000||Documents Upload Link: L:\\Assignments\SFTMPRIMARY\apd$\Pathways\ProcessClaims Upload\Claim Docs ||Upon successful upload, please move this email to subfolder "Archive - ProcessClaims Upload".||Thanks,|<xsl:value-of select="//Claim/@AnalystName"/>|<xsl:value-of select="//Claim/@AnalystPhone"/></EmailBody>
              </xsl:when>
              <xsl:when test="$isClosingSupplement = '1'">
                <xsl:copy-of select="//Send/EmailSubject"/>
                <EmailBody>Please upload to ProcessClaims the following files on completed QBE <xsl:choose><xsl:when test="$isRRP='1'">RRP</xsl:when><xsl:otherwise>desk audit</xsl:otherwise></xsl:choose>: ||LYNX ID <xsl:value-of select="concat(//Claim/@LynxID,'-', //Vehicle/@number)"/>   Claim # <xsl:value-of select="//Claim/@ClientClaimNumber"/>||* Supplement (EMS Data): (15 files; Respond "See Audit Results" or "SAR" to RBI audit questions at upload.)|* Supplement: PDF Original Supplement|* Supplement: PDF Audited Supplement|* Supplement: Vehicle Valuation: Consolidated PDF Closing Message &amp; Desk Audit Summary Report||ProcessClaims Web Link: http://www.processclaims.com|ID: gc_lynx|PW: <xsl:choose><xsl:when test="$isRRP='1'">Xxxnnn</xsl:when><xsl:otherwise>Abc123</xsl:otherwise></xsl:choose>||QBE Claim Number Format|-----------------------|General Casualty (GC):        000-00-00000  or A000000 or BA000000|Unigard (UIC):                0000000|QBE Agri (QIC):|National Farmers Union (NFU): PA00000000 or  RU00000000 or FO00000000||Documents Upload Link: L:\\Assignments\SFTMPRIMARY\apd$\Pathways\ProcessClaims Upload\Claim Docs ||Upon successful upload, please move this email to subfolder "Archive - ProcessClaims Upload".||Thanks,|<xsl:value-of select="//Claim/@AnalystName"/>

|<xsl:value-of select="//Claim/@AnalystPhone"/></EmailBody>|
              </xsl:when>
              <xsl:when test="contains(//Send/@bundlingProfileName, 'Closing Audit Complete') = true()">
                <xsl:copy-of select="//Send/EmailSubject"/>
                <EmailBody>Please upload to ProcessClaims the following files on completed QBE <xsl:choose><xsl:when test="$isRRP='1'">RRP</xsl:when><xsl:otherwise>desk audit</xsl:otherwise></xsl:choose>: ||LYNX ID <xsl:value-of select="concat(//Claim/@LynxID,'-', //Vehicle/@number)"/>   Claim # <xsl:value-of select="//Claim/@ClientClaimNumber"/>||* Estimate (EMS Data): (15 files; Respond "See Audit Results" or "SAR" to RBI audit questions at upload.)|* Estimate: PDF Audited Estimate|* Estimate: PDF Original Estimate<xsl:choose><xsl:when test="$isRRP='1'">* IA Fee Bill: Memo Bill|* Vehicle Valuation: Consolidated PDF Closing Message &amp; Other Docs|* Images: Photos|</xsl:when><xsl:otherwise>|* IA Fee Bill: Invoice|* Vehicle Valuation: Consolidated PDF Closing Message-Desk Audit Summary Report</xsl:otherwise></xsl:choose>|ProcessClaims Web Link: http://www.processclaims.com|ID: gc_lynx|PW: <xsl:choose><xsl:when test="$isRRP='1'">Xxxnnn</xsl:when><xsl:otherwise>Abc123</xsl:otherwise></xsl:choose>||QBE Claim Number Format|-----------------------|General Casualty (GC):        000-00-00000  or A000000 or BA000000|Unigard (UIC):                0000000|QBE Agri (QIC):|National Farmers Union (NFU): PA00000000 or  RU00000000 or FO00000000||Documents Upload Link: L:\\Assignments\SFTMPRIMARY\apd$\Pathways\ProcessClaims Upload\Claim Docs ||Upon successful upload, please move this email to subfolder "Archive - ProcessClaims Upload".||Thanks,|<xsl:value-of select="//Claim/@AnalystName"/>

|<xsl:value-of select="//Claim/@AnalystPhone"/></EmailBody>
              </xsl:when>
              <xsl:otherwise>
                <xsl:copy-of select="//Send/EmailSubject"/>
                <xsl:copy-of select="//Send/EmailBody"/>
              </xsl:otherwise>
            </xsl:choose>
            <MergedFileName>
              <!-- 123456, Fee Bill - Claim Message-Invoice.pdf -->
              <xsl:choose>
                <xsl:when test="//Send/@bundlingProfileName = 'RRP - Stale Alert &amp; Close' or
                                //Send/@bundlingProfileName = 'RRP - Closing Inspection Complete' or
                                //Send/@bundlingProfileName = 'RRP - Closing Supplement' or
                                //Send/@bundlingProfileName = 'RRP - Closing Total Loss'">
                  <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Vehicle Valuation - Claim Message - ', //Claim/@LynxID, '-', //Vehicle/@number, '.pdf')"/>
                </xsl:when>
                <xsl:when test="$isTotalLoss = '1'">
                  <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Vehicle Valuation - Closing Message - Desk Audit Summary Report - ', //Claim/@LynxID, '-', //Vehicle/@number, '.pdf')"/>
                </xsl:when>
                <xsl:when test="$isClosingSupplement = '1'">
                  <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Vehicle Valuation - Consolidated PDF Closing Message-Desk Audit Summary Report - ', //Claim/@LynxID, '-', //Vehicle/@number, '.pdf')"/>
                </xsl:when>
                <xsl:when test="//Send/@bundlingProfileName = 'Closing Audit Complete'">
                  <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Vehicle Valuation - Closing Message - Desk Audit Summary Report - ', //Claim/@LynxID, '-', //Vehicle/@number, '.pdf')"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Fee Bill - Claim Message-Invoice - ', //Claim/@LynxID, '-', //Vehicle/@number, '.pdf')"/>
                </xsl:otherwise>
              </xsl:choose>
            </MergedFileName>
          </xsl:otherwise>
        </xsl:choose>
        <PackageType>PDF</PackageType>
        <xsl:copy-of select="//Send/Success"/>
        <xsl:copy-of select="//Send/Failure"/>
      </Send>
      <xsl:choose>
        <xsl:when test="$isTest != '1' and /ClientDocument/Send/@via='EML'">
          <File location="\\pgw.local\dfs\intra\apddocuments\DEV\QBE"/>
        </xsl:when>
        <xsl:when test="$isExternal != '1' and /ClientDocument/Send/@via='EML'">
          <File location="\\SFTMPRIMARY\apd$\Pathways\Assignments\SFTMAPDPRDFS1\PATHWAYS\PROCESSCLAIMS UPLOAD\Claim Docs"/>
        </xsl:when>
      </xsl:choose>
      <Message merge="true"><xsl:value-of select="/ClientDocument/Message"/></Message>
      <MessageEscaped merge="true"><xsl:value-of select="/ClientDocument/MessageEscaped"/></MessageEscaped>
      <MessageHTML merge="true"><xsl:value-of select="/ClientDocument/MessageHTML"/></MessageHTML>
      <xsl:copy-of select="/ClientDocument/Claim"/>
      <xsl:copy-of select="/ClientDocument/Vehicle"/>
      <xsl:copy-of select="/ClientDocument/AttachmentsHTML"/>
      <xsl:copy-of select="/ClientDocument/NotesMsg"/>

        <xsl:choose>
          <xsl:when test="$isClosingSupplement='1'">
            <!-- <Document>
              <xsl:for-each select="/ClientDocument/Document[(@documentTypeID='3' or @documentTypeID='10') and contains(@outputFileName, 'Audited') = true()]/@*">
                <xsl:choose>
                  <xsl:when test="name(.)='merge'">
                    <xsl:attribute name="merge">true</xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:copy-of select="."/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </Document>
            <xsl:for-each select="/ClientDocument/Document[contains(@outputFileName, 'Audited') = false()]">
              <xsl:copy-of select="."/>
            </xsl:for-each>   -->
            <xsl:copy-of select="/ClientDocument/Document[@documentTypeID!='10' and @documentTypeID!='3' and @documentTypeID!='13' and @documentTypeID != '75' and @documentTypeID != '8']"/>

            <xsl:for-each select="/ClientDocument/Document[@documentTypeID='8']">
              <xsl:variable name="photoIndex" select="position()"/>
              <Document>
                <xsl:for-each select="@*">
                  <xsl:choose>
                    <xsl:when test="name(.)='outputFileName'">
                      <xsl:attribute name="outputFileName">
                        <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Photograph ', $photoIndex, ' - ', //Claim/@LynxID, '-', //Vehicle/@number, '.pdf')"/>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </Document>
            </xsl:for-each>

            <xsl:for-each select="/ClientDocument/Document[@documentTypeID='13']">
              <Document>
                <xsl:for-each select="@*">
                  <xsl:choose>
                    <xsl:when test="name(.)='outputFileName'">
                      <xsl:attribute name="outputFileName">
                        <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Fee Bill - Invoice - ', //Claim/@LynxID, '-', //Vehicle/@number, '.pdf')"/>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </Document>
            </xsl:for-each>

             <xsl:for-each select="/ClientDocument/Document[@documentTypeID='75']">
                <Document>
                   <xsl:for-each select="@*">
                      <xsl:choose>
                         <xsl:when test="name(.)='outputFileName'">
                            <xsl:attribute name="outputFileName">
                               <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Fee Bill - Memo Bill - ', //Claim/@LynxID, '-', //Vehicle/@number, '.pdf')"/>
                            </xsl:attribute>
                         </xsl:when>
                         <xsl:otherwise>
                            <xsl:copy-of select="."/>
                         </xsl:otherwise>
                      </xsl:choose>
                   </xsl:for-each>
                </Document>
             </xsl:for-each>

             <xsl:for-each select="/ClientDocument/Document[(@documentTypeID='3' or @documentTypeID='10') and contains(@outputFileName, 'Audited') = false()]">
              <Document>
                <xsl:for-each select="@*">
                  <xsl:choose>
                    <xsl:when test="name(.)='outputFileName'">
                      <xsl:attribute name="outputFileName">
                        <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Supplement - Original - ', //Claim/@LynxID, '-', //Vehicle/@number,  '.pdf')"/>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </Document>
            </xsl:for-each>

            <xsl:for-each select="/ClientDocument/Document[(@documentTypeID='3' or @documentTypeID ='10') and contains(@outputFileName, 'Audited') = true()]">
              <Document>
                <xsl:for-each select="@*">
                  <xsl:choose>
                    <xsl:when test="name(.)='outputFileName'">
                      <xsl:attribute name="outputFileName">
                        <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Supplement - Audited - ', //Claim/@LynxID, '-', //Vehicle/@number,  '.pdf')"/>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </Document>
            </xsl:for-each>
          </xsl:when>
          <xsl:when test="$isTotalLoss = '1'">
            <xsl:copy-of select="/ClientDocument/Document[@documentTypeID!='10' and @documentTypeID!='3' and @documentTypeID!='13' and @documentTypeID != '75' and @documentTypeID!='8']"/>

            <xsl:for-each select="/ClientDocument/Document[@documentTypeID='8']">
              <xsl:variable name="photoIndex" select="position()"/>
              <Document>
                <xsl:for-each select="@*">
                  <xsl:choose>
                    <xsl:when test="name(.)='outputFileName'">
                      <xsl:attribute name="outputFileName">
                        <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Photograph ', $photoIndex, ' - ', //Claim/@LynxID, '-', //Vehicle/@number, '.pdf')"/>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </Document>
            </xsl:for-each>


             <xsl:for-each select="/ClientDocument/Document[@documentTypeID='75']">
                <Document>
                   <xsl:for-each select="@*">
                      <xsl:choose>
                         <xsl:when test="name(.)='outputFileName'">
                            <xsl:attribute name="outputFileName">
                               <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Fee Bill - Memo Bill - ', //Claim/@LynxID, '-', //Vehicle/@number, '.pdf')"/>
                            </xsl:attribute>
                         </xsl:when>
                         <xsl:otherwise>
                            <xsl:copy-of select="."/>
                         </xsl:otherwise>
                      </xsl:choose>
                   </xsl:for-each>
                </Document>
             </xsl:for-each>

            <xsl:for-each select="/ClientDocument/Document[@documentTypeID='13']">
              <Document>
                <xsl:for-each select="@*">
                  <xsl:choose>
                    <xsl:when test="name(.)='outputFileName'">
                      <xsl:attribute name="outputFileName">
                        <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Fee Bill - Total Loss - Invoice - ', //Claim/@LynxID, '-', //Vehicle/@number, '.pdf')"/>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:when test="name(.)='merge'">
                      <xsl:attribute name="merge">false</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="name(.)='exception'">
                      <xsl:attribute name="noconvert">true</xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </Document>
            </xsl:for-each>

            <xsl:for-each select="/ClientDocument/Document[(@documentTypeID='3' or @documentTypeID='10') and contains(@outputFileName, 'Audited') = false()]">
              <Document>
                <xsl:for-each select="@*">
                  <xsl:choose>
                    <xsl:when test="name(.)='outputFileName'">
                      <xsl:attribute name="outputFileName">
                        <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Estimate - Original - ', //Claim/@LynxID, '-', //Vehicle/@number,  '.pdf')"/>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </Document>
            </xsl:for-each>

            <xsl:for-each select="/ClientDocument/Document[(@documentTypeID='3' or @documentTypeID ='10') and contains(@outputFileName, 'Audited') = true()]">
              <Document>
                <xsl:for-each select="@*">
                  <xsl:choose>
                    <xsl:when test="name(.)='outputFileName'">
                      <xsl:attribute name="outputFileName">
                        <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Estimate - Audited - ', //Claim/@LynxID, '-', //Vehicle/@number,  '.pdf')"/>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </Document>
            </xsl:for-each>

            <!--<xsl:for-each select="/ClientDocument/Document[@documentTypeID='3' or @documentTypeID='10']">
              <xsl:copy-of select="."/>
            </xsl:for-each>-->
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="/ClientDocument/Document[@documentTypeID!='10' and @documentTypeID!='3' and @documentTypeID!='13' and @documentTypeID!='75' and @documentTypeID!='8']"/>

            <xsl:for-each select="/ClientDocument/Document[@documentTypeID='8']">
              <xsl:variable name="photoIndex" select="position()"/>
              <Document>
                <xsl:for-each select="@*">
                  <xsl:choose>
                    <xsl:when test="name(.)='outputFileName'">
                      <xsl:attribute name="outputFileName">
                        <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Photograph ', $photoIndex, ' - ', //Claim/@LynxID, '-', //Vehicle/@number, '.pdf')"/>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </Document>
            </xsl:for-each>

             <xsl:for-each select="/ClientDocument/Document[@documentTypeID='75']">
                <Document>
                   <xsl:for-each select="@*">
                      <xsl:choose>
                         <xsl:when test="name(.)='outputFileName'">
                            <xsl:attribute name="outputFileName">
                               <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Fee Bill - Memo Bill - ', //Claim/@LynxID, '-', //Vehicle/@number, '.pdf')"/>
                            </xsl:attribute>
                         </xsl:when>
                         <xsl:otherwise>
                            <xsl:copy-of select="."/>
                         </xsl:otherwise>
                      </xsl:choose>
                   </xsl:for-each>
                </Document>
             </xsl:for-each>

            <xsl:for-each select="/ClientDocument/Document[@documentTypeID='13']">
              <Document>
                <xsl:for-each select="@*">
                  <xsl:choose>
                    <xsl:when test="name(.)='outputFileName'">
                      <xsl:attribute name="outputFileName">
                        <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Fee Bill - Invoice - ', //Claim/@LynxID, '-', //Vehicle/@number, '.pdf')"/>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:when test="name(.)='merge'">
                      <xsl:attribute name="merge">false</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="name(.)='exception'">
                      <xsl:attribute name="noconvert">true</xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </Document>
            </xsl:for-each>
            
            <xsl:for-each select="/ClientDocument/Document[(@documentTypeID='3' or @documentTypeID='10') and contains(@outputFileName, 'Audited') = false()]">
              <Document>
                <xsl:for-each select="@*">
                  <xsl:choose>
                    <xsl:when test="name(.)='outputFileName'">
                      <xsl:attribute name="outputFileName">
                        <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Estimate - Original - ', //Claim/@LynxID, '-', //Vehicle/@number,  '.pdf')"/>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </Document>
            </xsl:for-each>
            
            <xsl:for-each select="/ClientDocument/Document[(@documentTypeID='3' or @documentTypeID ='10') and contains(@outputFileName, 'Audited') = true()]">
              <Document>
                <xsl:for-each select="@*">
                  <xsl:choose>
                    <xsl:when test="name(.)='outputFileName'">
                      <xsl:attribute name="outputFileName">
                        <xsl:value-of select="concat(//Claim/@ClientClaimNumber, ', Estimate - Audited - ', //Claim/@LynxID, '-', //Vehicle/@number,  '.pdf')"/>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise><xsl:copy-of select="."/></xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </Document>
            </xsl:for-each>

            <!--<xsl:for-each select="/ClientDocument/Document[@documentTypeID='3' or @documentTypeID='10']">
              <xsl:copy-of select="."/>
            </xsl:for-each>-->
          </xsl:otherwise>
        </xsl:choose>

    </ClientDocument>
  </xsl:template>
</xsl:stylesheet>













