<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="no" method="text"/>
  <msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
		//IMPORTANT: This function must be used for all cdata or free-form text so that we
		//           can safely escape the paranthesis.
        function cleanData(str){
		   str = str.replace(/[(]/g, '\\(');
		   str = str.replace(/[)]/g, '\\)');
		   return str;
		}
		function crlf(){
			return String.fromCharCode(13, 10);
		}
		function toLowerCase(str){
		   if (str != "")
		      return str.toLowerCase();
           else
              return str;
		}
        function formatNumber(intNum, intZeroPadLength) {
          var strNum = intNum + "";
          var strRet = "";
          for (var i = 0; i < intZeroPadLength - strNum.length; i++)
              strRet += "0";
          strRet += strNum;
          return strRet;
        }
        function getDate(){
          var strRet = "";
          var dt = new Date();
          strRet = formatNumber((dt.getMonth() + 1), 2) + "/" +
                   formatNumber(dt.getDate(), 2) + "/" +
                   dt.getFullYear() + " " +
                   formatNumber(dt.getHours(), 2) + ":" +
                   formatNumber(dt.getMinutes(), 2) + ":" +
                   formatNumber(dt.getSeconds(), 2);
          return strRet;
        }

  ]]>
  </msxsl:script>
  <xsl:template match="/">%FDF-1.2
1 0 obj
&lt;&lt;
/FDF
&lt;&lt;
/Fields [
    <!--Read-only fields-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ReportDate'"/>
      <xsl:with-param name="fieldValue" select="//Data/ReportDate"/>
    </xsl:call-template>
    <xsl:choose>
      <xsl:when test="//Data/PHBusinessName != ''">
        <xsl:call-template name="field">
          <xsl:with-param name="fieldName" select="'PHFirstName'"/>
          <xsl:with-param name="fieldValue" select="concat('Sirs ', //Data/PHBusinessName, ',')"/>
        </xsl:call-template>
        <xsl:call-template name="field">
          <xsl:with-param name="fieldName" select="'PHName'"/>
          <xsl:with-param name="fieldValue" select="//Data/PHBusinessName"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="field">
          <xsl:with-param name="fieldName" select="'PHFirstName'"/>
          <xsl:with-param name="fieldValue" select="concat('Dear ', //Data/PHFirstName, ',')"/>
        </xsl:call-template>
        <xsl:call-template name="field">
          <xsl:with-param name="fieldName" select="'PHName'"/>
          <xsl:with-param name="fieldValue" select="concat(//Data/PHFirstName, ' ', //Data/PHLastName)"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'PHAddress1'"/>
      <xsl:with-param name="fieldValue" select="//Data/PHAddress1"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'PHAddress2'"/>
      <xsl:with-param name="fieldValue" select="//Data/PHAddress2"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'PHAddressCityStateZip'"/>
      <xsl:with-param name="fieldValue" select="concat(//Data/PHAddressCity, ', ', //Data/PHAddressState, ' ', //Data/PHAddressZip)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ClaimNumber'"/>
      <xsl:with-param name="fieldValue" select="//Data/ClaimNumber"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'PolicyNumber'"/>
      <xsl:with-param name="fieldValue" select="//Data/PolicyNumber"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VehicleYearMakeModel'"/>
      <xsl:with-param name="fieldValue" select="concat(//Data/VehicleYear, ' ', //Data/VehicleMake, ' ', //Data/VehicleModel)"/>
    </xsl:call-template>]
/F () &gt;&gt;
 &gt;&gt;
endobj
trailer
&lt;&lt;  /Root 1 0 R  &gt;&gt;
%%EOF
  </xsl:template>
  <xsl:template name="field">
    <xsl:param name="fieldName"/>
    <xsl:param name="fieldValue"/>
    <xsl:param name="fieldType"/>
    <xsl:variable name="LCaseFieldValue" select="user:toLowerCase(string($fieldValue))"/>
    <xsl:text>&lt;&lt; /T (</xsl:text>
    <xsl:value-of select="$fieldName"/>
    <xsl:text>) /V (</xsl:text>
    <xsl:choose>
      <xsl:when test="$fieldType = 'boolean'">
        <xsl:choose>
          <xsl:when test="$LCaseFieldValue = 'yes' or $LCaseFieldValue = 'y' or $LCaseFieldValue = '1' or $LCaseFieldValue = 'true' or $LCaseFieldValue = 't'">Yes</xsl:when>
          <xsl:otherwise>No</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="user:cleanData(string($fieldValue))"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>) &gt;&gt;</xsl:text>
    <xsl:value-of select="user:crlf()"/>
  </xsl:template>
</xsl:stylesheet>
