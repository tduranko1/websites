<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="no" method="text"/>
  <msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
		//IMPORTANT: This function must be used for all cdata or free-form text so that we
		//           can safely escape the paranthesis.
        function cleanData(str){
		   str = str.replace(/[(]/g, '\\(');
		   str = str.replace(/[)]/g, '\\)');
		   return str;
		}
		function crlf(){
			return String.fromCharCode(13, 10);
		}
		function toLowerCase(str){
		   if (str != "")
		      return str.toLowerCase();
           else
              return str;
		}
		function reverseString(str){
		   if (str != ""){
		      var strReverse = "";
		      for (var i = str.length - 1; i >= 0 ; i--)
		          strReverse += str.substr(i, 1);
              return strReverse;
		   } else
		      return "";
		}
		function toUpper(str){
		   return str.toUpperCase();
		}
  ]]>
  </msxsl:script>
  <xsl:template match="/">%FDF-1.2
1 0 obj
&lt;&lt;
/FDF
&lt;&lt;
/Fields [
<xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'RepName'"/>
      <xsl:with-param name="fieldValue" select="//Data/UserName"/>
    </xsl:call-template>
    <xsl:variable name="UserPhone">
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="fieldValue" select="//Data/UserPhone"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'RepPhone'"/>
      <xsl:with-param name="fieldValue" select="$UserPhone"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'RepEmail'"/>
      <xsl:with-param name="fieldValue" select="//Data/UserEmail"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ClaimNumber'"/>
      <xsl:with-param name="fieldValue" select="//Data/ClaimNumber"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'CarrierRepNameFirst'"/>
      <xsl:with-param name="fieldValue" select="//Data/CarrierRepFName"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'CarrierRepNameLast'"/>
      <xsl:with-param name="fieldValue" select="//Data/CarrierRepLName"/>
    </xsl:call-template>

    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LYNXID'"/>
      <xsl:with-param name="fieldValue" select="//Data/LYNXID"/>
    </xsl:call-template>
	
    <xsl:choose>
      <xsl:when test="//Data/InsuredBName != ''">
        <xsl:call-template name="field">
          <xsl:with-param name="fieldName" select="'InsuredName'"/>
          <xsl:with-param name="fieldValue" select="//Data/InsuredBName"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="field">
          <xsl:with-param name="fieldName" select="'InsuredName'"/>
          <xsl:with-param name="fieldValue" select="concat(//Data/InsuredFName, ' ', //Data/InsuredLName)"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:choose>
      <xsl:when test="//Data/OwnerBName != ''">
        <xsl:call-template name="field">
          <xsl:with-param name="fieldName" select="'OwnerName'"/>
          <xsl:with-param name="fieldValue" select="//Data/OwnerBName"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="field">
          <xsl:with-param name="fieldName" select="'OwnerName'"/>
          <xsl:with-param name="fieldValue" select="concat(//Data/OwnerFName, ' ', //Data/OwnerLName)"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:variable name="OwnerPhone">
      <xsl:choose>
        <xsl:when test="//Data/OwnerDayPhone != ''">
          <xsl:call-template name="formatPhone">
            <xsl:with-param name="fieldValue" select="//Data/OwnerDayPhone"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="//Data/OwnerNightPhone != ''">
          <xsl:call-template name="formatPhone">
            <xsl:with-param name="fieldValue" select="//Data/OwnerNightPhone"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="//Data/OwnerAltPhone != ''">
          <xsl:call-template name="formatPhone">
            <xsl:with-param name="fieldValue" select="//Data/OwnerAltPhone"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerPhone'"/>
      <xsl:with-param name="fieldValue" select="$OwnerPhone"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerZip'"/>
      <xsl:with-param name="fieldValue" select="//Data/OwnerZip"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerState'"/>
      <xsl:with-param name="fieldValue" select="//Data/OwnerState"/>
    </xsl:call-template>
    <xsl:variable name="FaxBackNumber">
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="fieldValue" select="'(239) 479-5943'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'FaxBackNumber'"/>
      <xsl:with-param name="fieldValue" select="$FaxBackNumber"/>
    </xsl:call-template>
    
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN'"/>
      <xsl:with-param name="fieldValue" select="//Data/VIN"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VehicleTag'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleTag"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'InsuredPhone'"/>
      <xsl:with-param name="fieldValue" select="//Data/InsuredPhone"/>
    </xsl:call-template>	
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Mileage'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleMileage"/>
    </xsl:call-template>	
	<!--
	<xsl:variable name="Vin" select="user:reverseString(user:toUpper(string(//Data/Assignment/Vehicle/@Vin)))"/>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_17'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 1, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_16'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 2, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_15'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 3, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_14'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 4, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_13'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 5, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_12'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 6, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_11'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 7, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_10'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 8, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_9'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 9, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_8'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 10, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_7'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 11, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_6'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 12, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_5'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 13, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_4'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 14, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_3'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 15, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_2'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 16, 1)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN_1'"/>
      <xsl:with-param name="fieldValue" select="substring($Vin, 17, 1)"/>
    </xsl:call-template> -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LossState'"/>
      <xsl:with-param name="fieldValue" select="//Data/LossState"/>
    </xsl:call-template>
    <xsl:variable name="LossDate">
      <xsl:call-template name="formatSQLDate">
        <xsl:with-param name="fieldValue" select="//Data/LossDate"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LossDate'"/>
      <xsl:with-param name="fieldValue" select="$LossDate"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VehicleYear'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleYear"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Make'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleMake"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Model'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleModel"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ShopName'"/>
      <xsl:with-param name="fieldValue" select="//Data/ShopName"/>		
    </xsl:call-template>
    <xsl:variable name="ShopFax">
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="fieldValue" select="//Data/ShopFaxNumber"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ShopFax'"/>
      <xsl:with-param name="fieldValue" select="concat('Fax: ', $ShopFax)"/>
    </xsl:call-template>]
/F () &gt;&gt;
 &gt;&gt;
endobj
trailer
&lt;&lt;  /Root 1 0 R  &gt;&gt;
%%EOF
  </xsl:template>
  <xsl:template name="field">
    <xsl:param name="fieldName"/>
    <xsl:param name="fieldValue"/>
    <xsl:param name="fieldType"/>
    <xsl:variable name="LCaseFieldValue" select="user:toLowerCase(string($fieldValue))"/>
    <xsl:text>&lt;&lt; /T (</xsl:text>
    <xsl:value-of select="$fieldName"/>
    <xsl:text>) /V (</xsl:text>
    <xsl:choose>
      <xsl:when test="$fieldType = 'boolean'">
        <xsl:choose>
          <xsl:when test="$LCaseFieldValue = 'yes' or $LCaseFieldValue = 'y' or $LCaseFieldValue = '1' or $LCaseFieldValue = 'true' or $LCaseFieldValue = 't'">Yes</xsl:when>
          <xsl:otherwise>No</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="user:cleanData(string($fieldValue))"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>) &gt;&gt;</xsl:text>
    <xsl:value-of select="user:crlf()"/>
  </xsl:template>
  <xsl:template name="formatPhone">
    <xsl:param name="fieldValue"/>
    <xsl:variable name="val" select="translate($fieldValue, ')(- ', '')"/>
    <xsl:if test="$fieldValue != ''">
      <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), ' ', substring($val, 7))"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="formatSQLDate">
    <xsl:param name="fieldValue"/>
    <xsl:choose>
      <xsl:when test="contains($fieldValue, 'T') = boolean('true')">
        <xsl:variable name="year" select="substring($fieldValue, 1, 4)"/>
        <xsl:variable name="month" select="substring($fieldValue, 6, 2)"/>
        <xsl:variable name="date" select="substring($fieldValue, 9, 2)"/>
        <xsl:value-of select="concat($month, '/', $date, '/', $year)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$fieldValue"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
