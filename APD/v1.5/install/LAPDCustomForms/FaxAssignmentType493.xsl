<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="no" method="text"/>
  <msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
		//IMPORTANT: This function must be used for all cdata or free-form text so that we
		//           can safely escape the paranthesis.
        function cleanData(str){
		   str = str.replace(/[(]/g, '\\(');
		   str = str.replace(/[)]/g, '\\)');
		   return str;
		}
		function crlf(){
			return String.fromCharCode(13, 10);
		}
		function toLowerCase(str){
		   if (str != "")
		      return str.toLowerCase();
           else
              return str;
		}
  ]]>
  </msxsl:script>
  <xsl:template match="/">
    %FDF-1.2
    1 0 obj
    &lt;&lt;
    /FDF
    &lt;&lt;
    /Fields [
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'AssignmentCodeAndHeader'"/>
      <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/@AssignmentCode, ' ', //Data/Assignment/@FaxHeader)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Header2'"/>
      <xsl:with-param name="fieldValue" select="''"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ShopName'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Shop/@ShopName"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ShopAddress'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Shop/@ShopAddress1"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ShopAddress2'"/>
      <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/Shop/@ShopCity, ' ', //Data/Assignment/Shop/@ShopState, ' ', //Data/Assignment/Shop/@ShopZip)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'DeliveryMethodDescription'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/@DeliveryMethodDescription"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'EstimatingPackage'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/@EstimatingPackage"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'RepName'"/>
      <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/LynxRep/@RepNameFirst, ' ', //Data/Assignment/LynxRep/@RepNameLast)"/>
    </xsl:call-template>
    <xsl:variable name="RepPhone">
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="fieldValue" select="//Data/Assignment/LynxRep/@RepPhone"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'RepPhone'"/>
      <xsl:with-param name="fieldValue" select="$RepPhone"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'RepPhoneExtension'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/LynxRep/@RepPhoneExtension"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'RepEmailAddress'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/LynxRep/@RepEmailAddress"/>
    </xsl:call-template>
    <xsl:variable name="RepFax">
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="fieldValue" select="//Data/Assignment/LynxRep/@RepFax"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'RepFax'"/>
      <xsl:with-param name="fieldValue" select="$RepFax"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'RepFaxExtension'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/LynxRep/@RepFaxExtension"/>
    </xsl:call-template>

    <!-- 29Oct2015 - TVD - Changes for the HYperQuest Email Address -->
    <!-- 09Dec2015 - TVD - Modification for the HYperQuest Email Address - Don't show email address if AUDATEX -->
    <xsl:choose>
      <xsl:when test ="//Data/Assignment/@EstimatingPackage=1">
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="field">
          <xsl:with-param name="fieldName" select="'HQEmailAddress'"/>
          <xsl:with-param name="fieldValue" select="//Data/Assignment/Claim/@HQEmailAddress"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>

    <!-- Changes for the HYperQuest ASsignment -->
 <!-- Done by Glsd451 on 23 apr2015 -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'InsuranceCompany'"/>
      <xsl:with-param name="fieldValue" select="concat('LYNX - ',//Data/Assignment/Claim/@InsuranceCompany)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LynxIDVehNum'"/>
      <xsl:with-param name="fieldValue" select="concat('LYNXID',//Data/Assignment/@LynxId, '-', //Data/Assignment/@VehicleNumber)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ClaimNumber'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Coverage/@ClaimNumber"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'InsuredName'"/>
      <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/Claim/@InsuredFirstName, ' ', //Data/Assignment/Claim/@InsuredLastName)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'CoverageConfirmed'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Coverage/@CoverageConfirmed"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:variable name="LossDate">
      <xsl:call-template name="formatSQLDate">
        <xsl:with-param name="fieldValue" select="//Data/Assignment/Claim/@LossDate"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LossDate'"/>
      <xsl:with-param name="fieldValue" select="$LossDate"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'CoverageType'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@CoverageType"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Deductible'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Coverage/@Deductible"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerName'"/>
      <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/Owner/@OwnerFirstName, ' ', //Data/Assignment/Owner/@OwnerLastName)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerAddress1'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Owner/@OwnerAddress1"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerAddress2'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Owner/@OwnerAddress2"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerAddress3'"/>
      <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/Owner/@OwnerAddressCity, ' ', //Data/Assignment/Owner/@OwnerAddressState, ' ', //Data/Assignment/Owner/@OwnerAddressZip)"/>
    </xsl:call-template>
    <xsl:variable name="VehicleContactCellPhone">
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="fieldValue" select="//Data/Assignment/VehicleContact/@VehicleContactCellPhone"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VehicleContactCellPhone'"/>
      <xsl:with-param name="fieldValue" select="$VehicleContactCellPhone"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VehicleContactPrefContactPhone'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/VehicleContact/@VehicleContactPrefContactPhone"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VehicleContactName'"/>
      <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/VehicleContact/@VehicleContactFirstName, ' ', //Data/Assignment/VehicleContact/@VehicleContactLastName)"/>
    </xsl:call-template>
    <xsl:variable name="OwnerNightPhone">
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="fieldValue" select="//Data/Assignment/Owner/@OwnerNightPhone"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerNightPhone'"/>
      <xsl:with-param name="fieldValue" select="$OwnerNightPhone"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerNightPhoneExtension'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Owner/@OwnerNightPhoneExtension"/>
    </xsl:call-template>
    <xsl:variable name="OwnerDayPhone">
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="fieldValue" select="//Data/Assignment/Owner/@OwnerDayPhone"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerDayPhone'"/>
      <xsl:with-param name="fieldValue" select="$OwnerDayPhone"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerDayPhoneExtension'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Owner/@OwnerDayPhoneExtension"/>
    </xsl:call-template>
    <xsl:variable name="OwnerAlternatePhone">
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="fieldValue" select="//Data/Assignment/Owner/@OwnerAlternatePhone"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerAlternatePhone'"/>
      <xsl:with-param name="fieldValue" select="$OwnerAlternatePhone"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerAlternatePhoneExtension'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Owner/@OwnerAlternatePhoneExtension"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerEmailAddress'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Owner/@OwnerEmailAddress"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VehicleYear'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@VehicleYear"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Make'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@Make"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Model'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@Model"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'BodyStyle'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@BodyStyle"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Vin'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@Vin"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Color'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@Color"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LicensePlateNo'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@LicensePlateNo"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LicensePlateState'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@LicensePlateState"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Driveable'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@Driveable"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LocationName'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@LocationName"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LocationAddress1'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@LocationAddress1"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LocationAddress2'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@LocationAddress2"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LocationAddress3'"/>
      <xsl:with-param name="fieldValue" select="''"/>
    </xsl:call-template>
    <xsl:variable name="LocationPhone">
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@LocationPhone"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LocationPhone'"/>
      <xsl:with-param name="fieldValue" select="$LocationPhone"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LocationPhoneExtension'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@LocationPhoneExtension"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LocationContactName'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@LocationContactName"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'PointOfImpactPrimary'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@PointOfImpactPrimary"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'PointOfImpactSecondary'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@PointOfImpactSecondary"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LossTypeDescription'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Claim/@LossDescription2"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'PointOfImpactUnrelatedPrior'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@PointOfImpactUnrelatedPrior"/>
    </xsl:call-template>
    <xsl:variable name="TotalLossWarningAmount">
      <xsl:if test="string-length(//Data/Assignment/Coverage/@TotalLossWarningAmount) &gt; 0">
        <xsl:choose>
          <!-- Default setting. If the value was provided and is equal to 0 -->
          <xsl:when test="number(//Data/Assignment/Coverage/@TotalLossWarningAmount) = 0">
            <xsl:text>If your estimate exceeds 70% of NADA value, DO NOT begin repairs.  Call LYNX Services immediately.</xsl:text>
          </xsl:when>
          <!-- 6/8/07 - the percentage of the book value already precalculated will be suppressed per JP and Business -->
          <!-- data is already calculated -->
          <!-- <xsl:when test="ceiling(number(.)) &gt; 1">
               <xsl:text>If your estimate exceeds $</xsl:text>
               <xsl:value-of select="round(.)"/>
               <xsl:text> DO NOT begin repairs.  Call LYNX Services immediately.</xsl:text>
            </xsl:when> -->
          <!-- A percentage value was provided. This will be less than 1 (like 0.75 for 75%)-->
          <xsl:when test="ceiling(number(//Data/Assignment/Coverage/@TotalLossWarningAmount)) = 1">
            <!-- data is a percentage value  -->
            <xsl:text>If your estimate exceeds </xsl:text>
            <xsl:value-of select="number(//Data/Assignment/Coverage/@TotalLossWarningAmount) * 100"/>
            <xsl:text>% of NADA value.  DO NOT begin repairs.  Call LYNX Services immediately.</xsl:text>
          </xsl:when>
          <xsl:when test="string(number(//Data/Assignment/Coverage/@TotalLossWarningAmount)) = 'NaN'">
            <xsl:value-of select="//Data/Assignment/Coverage/@TotalLossWarningAmount"/>
          </xsl:when>
          <!-- Default setting once again. -->
          <xsl:otherwise>
            <xsl:text>If your estimate exceeds 70% of NADA value, DO NOT begin repairs.  Call LYNX Services immediately.</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'TotalLossWarningAmount'"/>
      <xsl:with-param name="fieldValue" select="$TotalLossWarningAmount"/>
    </xsl:call-template>

    <!--<xsl:variable name="Remarks">
		  <xsl:choose>
			  <xsl:when test ="'RRP'='RRP'">
				  <xsl:text>***RRP COMMENTS TO BE INSERTED***</xsl:text>
				  <xsl:value-of select="//Data/Assignment/Vehicle/@ShopRemarks2"/>
			  </xsl:when>
			  <xsl:otherwise>
				  <xsl:text></xsl:text>
			  </xsl:otherwise>
		  </xsl:choose>
	  </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ShopRemarks'"/>
      <xsl:with-param name="fieldValue" select="$Remarks"/>
    </xsl:call-template>-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ShopRemarks'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@ShopRemarks2"/>
    </xsl:call-template>

    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'AdminFeeComments'"/>
      <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@AdminFeeComments"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'HandlingInstructions'"/>
      <xsl:with-param name="fieldValue" select="concat('* ', //Data/Assignment/Claim/@InsuranceCompany, ' Specific Handling Instructions attached')"/>
    </xsl:call-template>]
    /F () &gt;&gt;
    &gt;&gt;
    endobj
    trailer
    &lt;&lt;  /Root 1 0 R  &gt;&gt;
    %%EOF
  </xsl:template>
  <xsl:template name="field">
    <xsl:param name="fieldName"/>
    <xsl:param name="fieldValue"/>
    <xsl:param name="fieldType"/>
    <xsl:variable name="LCaseFieldValue" select="user:toLowerCase(string($fieldValue))"/>
    <xsl:text>&lt;&lt; /T (</xsl:text>
    <xsl:value-of select="$fieldName"/>
    <xsl:text>) /V (</xsl:text>
    <xsl:choose>
      <xsl:when test="$fieldType = 'boolean'">
        <xsl:choose>
          <xsl:when test="$LCaseFieldValue = 'yes' or $LCaseFieldValue = 'y' or $LCaseFieldValue = '1' or $LCaseFieldValue = 'true' or $LCaseFieldValue = 't'">Yes</xsl:when>
          <xsl:otherwise>No</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="user:cleanData(string($fieldValue))"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>) &gt;&gt;</xsl:text>
    <xsl:value-of select="user:crlf()"/>
  </xsl:template>
  <xsl:template name="formatPhone">
    <xsl:param name="fieldValue"/>
    <xsl:variable name="val" select="translate($fieldValue, ')(- ', '')"/>
    <xsl:if test="$fieldValue != ''">
      <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), ' ', substring($val, 7))"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="formatSQLDate">
    <xsl:param name="fieldValue"/>
    <xsl:choose>
      <xsl:when test="contains($fieldValue, 'T') = boolean('true')">
        <xsl:variable name="year" select="substring($fieldValue, 1, 4)"/>
        <xsl:variable name="month" select="substring($fieldValue, 6, 2)"/>
        <xsl:variable name="date" select="substring($fieldValue, 9, 2)"/>
        <xsl:variable name="hour" select="substring($fieldValue, 12, 2)"/>
        <xsl:variable name="min" select="substring($fieldValue, 15, 2)"/>
        <xsl:variable name="sec" select="substring($fieldValue, 18, 2)"/>
        <xsl:value-of select="concat($month, '/', $date, '/', $year, ' ', $hour, ':', $min, ':', $sec)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$fieldValue"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
