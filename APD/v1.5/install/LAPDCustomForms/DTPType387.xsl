<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="no" method="text"/>
  <msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
		//IMPORTANT: This function must be used for all cdata or free-form text so that we
		//           can safely escape the paranthesis.
        function cleanData(str){
		   str = str.replace(/[(]/g, '\\(');
		   str = str.replace(/[)]/g, '\\)');
		   return str;
		}
		function crlf(){
			return String.fromCharCode(13, 10);
		}
		function toLowerCase(str){
		   if (str != "")
		      return str.toLowerCase();
           else
              return str;
		}
  ]]>
  </msxsl:script>
  <xsl:template match="/">%FDF-1.2
1 0 obj
&lt;&lt;
/FDF
&lt;&lt;
/Fields [
<xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'RepName'"/>
         <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/LynxRep/@RepNameFirst, ' ', //Data/Assignment/LynxRep/@RepNameLast)"/>
      </xsl:call-template>
      
      <xsl:variable name="RepPhone">
         <xsl:call-template name="formatPhone">
            <xsl:with-param name="fieldValue" select="//Data/Assignment/LynxRep/@RepPhone"/>
         </xsl:call-template>
      </xsl:variable>
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'RepPhone'"/>
         <xsl:with-param name="fieldValue" select="$RepPhone"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'RepPhoneExtension'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/LynxRep/@RepPhoneExtension"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'RepEmailAddress'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/LynxRep/@RepEmailAddress"/>
      </xsl:call-template>
      
      <xsl:variable name="RepFax">
         <xsl:call-template name="formatPhone">
            <xsl:with-param name="fieldValue" select="//Data/Assignment/LynxRep/@RepFax"/>
         </xsl:call-template>
      </xsl:variable>
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'RepFax'"/>
         <xsl:with-param name="fieldValue" select="$RepFax"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'RepFaxExtension'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/LynxRep/@RepFaxExtension"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'InsuranceCompany'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/Claim/@InsuranceCompany"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'LynxIDVehNum'"/>
         <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/@LynxId, '-', //Data/Assignment/@VehicleNumber)"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'ClaimNumber'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/Coverage/@ClaimNumber"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'OwnerName'"/>
         <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/Owner/@OwnerFirstName, ' ', //Data/Assignment/Owner/@OwnerLastName)"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'Vin'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@Vin"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'VehicleYear'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@VehicleYear"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'Color'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@Color"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'MakeModel'"/>
         <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/Vehicle/@Make, ' ', //Data/Assignment/Vehicle/@Model)"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'LicensePlateNo'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@LicensePlateNo"/>
      </xsl:call-template>]
/F () &gt;&gt;
 &gt;&gt;
endobj
trailer
&lt;&lt;  /Root 1 0 R  &gt;&gt;
%%EOF
  </xsl:template>
  <xsl:template name="field">
    <xsl:param name="fieldName"/>
    <xsl:param name="fieldValue"/>
    <xsl:param name="fieldType"/>
    <xsl:variable name="LCaseFieldValue" select="user:toLowerCase(string($fieldValue))"/>
    <xsl:text>&lt;&lt; /T (</xsl:text>
    <xsl:value-of select="$fieldName"/>
    <xsl:text>) /V (</xsl:text>
    <xsl:choose>
      <xsl:when test="$fieldType = 'boolean'">
        <xsl:choose>
          <xsl:when test="$LCaseFieldValue = 'yes' or $LCaseFieldValue = 'y' or $LCaseFieldValue = '1' or $LCaseFieldValue = 'true' or $LCaseFieldValue = 't'">Yes</xsl:when>
          <xsl:otherwise>No</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="user:cleanData(string($fieldValue))"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>) &gt;&gt;</xsl:text>
    <xsl:value-of select="user:crlf()"/>
  </xsl:template>
  <xsl:template name="formatPhone">
    <xsl:param name="fieldValue"/>
    <xsl:variable name="val" select="translate($fieldValue, ')(- ', '')"/>
    <xsl:if test="$fieldValue != ''">
      <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), ' ', substring($val, 7))"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="formatSQLDate">
    <xsl:param name="fieldValue"/>
    <xsl:variable name="year" select="substring($fieldValue, 1, 4)"/>
    <xsl:variable name="month" select="substring($fieldValue, 6, 2)"/>
    <xsl:variable name="date" select="substring($fieldValue, 9, 2)"/>
    <xsl:variable name="hour" select="substring($fieldValue, 12, 2)"/>
    <xsl:variable name="min" select="substring($fieldValue, 15, 2)"/>
    <xsl:variable name="sec" select="substring($fieldValue, 18, 2)"/>
    <xsl:value-of select="concat($month, '/', $date, '/', $year, ' ', $hour, ':', $min, ':', $sec)"/>
  </xsl:template>
</xsl:stylesheet>
