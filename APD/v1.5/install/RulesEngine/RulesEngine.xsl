<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace">

<xsl:output method="xml" indent="no" encoding="UTF-8"/>

<xsl:param name="debug" select="'N'"/>

<msxsl:script language="JScript" implements-prefix="user">
	<![CDATA[
		var oRoot = null;
		var oConditionsNodeList = null;
		var objFacts = new Array();
		var aExpressions = new Array();
		var aLValues = new Array();
		var aRValues = new Array();
		var reSQLDateTime = /(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(.*)/;
		var reSysDateTime = /(\d{2})[-|\/]{1}(\d{2})[-|\/]{1}(\d{4})[ ](\d{2}):(\d{2}):(\d{2})(.*)/;
		var reSysDate = /(\d{2})[-|\/]{1}(\d{2})[-|\/]{1}(\d{4})/;
		var oCurrentNode = null;
	    
		function crlf(){
			return String.fromCharCode(13, 10);
		}
        
		function setRoot(objRoot){
			if (typeof(objRoot) == "object" && objRoot[0])
				oRoot = objRoot[0];
			return "";
		}

		function getValue(strXPath){
			if (oRoot && strXPath != ""){
				var oNode = oRoot.selectSingleNode(strXPath);
				if (oNode)
					return oNode.text;
			}
			return "None";
		}
		
		function resolveXPath(oFromNode, strXPath){
			if (oFromNode && strXPath != ""){
			    if (oFromNode == oRoot){
				   if (strXPath.substr(0,2) != "//") strXPath = "//" + strXPath;
                }
				var oNode = oFromNode.selectSingleNode(strXPath);
				if (oNode)
					return oNode.text;
				else
					return "ERROR: path unresolved";
			}
			return "";
		}
		
		function resolveAttrib(oCurrentNode, sAttrib){
			var sRet = "";
			if (oCurrentNode && sAttrib != ""){
				if (sAttrib.indexOf("/") != -1) {
					var oNode = oCurrentNode.selectSingleNode(sAttrib);
					if (oNode)
						sRet = oNode.text;
				} else
					sRet = oCurrentNode.getAttribute(sAttrib);
				if (sRet == null) sRet = "ERROR: Attribute not found";
			}

			return sRet;
		}
		
		function inStr(iStart, strText, strSearchFor){
			var i = iStart;
			var iTextLen = strText.length;
			var iSearchForLen = strSearchFor.length;

			while (i < iTextLen){
				if (strText.substr(i, iSearchForLen) == strSearchFor){
					return i;
				}
				i++;
			}
			return -1;
		}
		

		function evaluate(sData, sDataType, oCurrentNode){
			var sValue = "";
			var iMacroStart = iMacroEnd = -1;
			var sDataTmp = sData;
			
			//check for any $XPATH macros or $FACT
			if (sDataTmp.indexOf("$XPATH") != -1 ||
                sDataTmp.indexOf("$FACT") != -1 ||
                sDataTmp.indexOf("$CURRENT") != -1 ||
                sDataTmp.indexOf("$CALC") != -1){
				//Resolve all $FACT
				iMacroStart = inStr(0, sDataTmp, "$FACT");
				
				while (iMacroStart != -1){
					iMacroEnd = inStr(iMacroStart + 6, sDataTmp, ")");
					var sFactID = sDataTmp.substring(iMacroStart + 6, iMacroEnd);
					var sMacroValue = getFactData(sFactID);
					sMacroValue = convert(sMacroValue, sDataType);

					sDataTmp = sDataTmp.substr(0, iMacroStart) + sMacroValue + sDataTmp.substr(iMacroEnd + 1);
					iMacroStart = inStr(0, sDataTmp, "$FACT");
				}

				//resolve all $XPATHS
				iMacroStart = inStr(0, sDataTmp, "$XPATH");
				
				while (iMacroStart != -1){
					iMacroEnd = inStr(iMacroStart + 7, sDataTmp, ")");
					var sXpath = sDataTmp.substring(iMacroStart + 7, iMacroEnd);
					var sMacroValue = resolveXPath(oRoot, sXpath);
					sMacroValue = convert(sMacroValue, sDataType);

					sDataTmp = sDataTmp.substr(0, iMacroStart) + sMacroValue + sDataTmp.substr(iMacroEnd + 1);
					iMacroStart = inStr(0, sDataTmp, "$XPATH");
				}
				
				//Resolve all $CURRENT
				if (oCurrentNode){
                    iMacroStart = inStr(0, sDataTmp, "$CURRENT");
					
					while (iMacroStart != -1){
						iMacroEnd = inStr(iMacroStart + 9, sDataTmp, ")");
						var sAttrib = sDataTmp.substring(iMacroStart + 9, iMacroEnd);
						var sMacroValue = "";
						if (sAttrib.indexOf("/") != -1){
						   sMacroValue = resolveXPath(oCurrentNode, sAttrib);
						} else {
						   sMacroValue = resolveAttrib(oCurrentNode, sAttrib);
						}
						
						sMacroValue = convert(sMacroValue, sDataType);

						sDataTmp = sDataTmp.substr(0, iMacroStart) + sMacroValue + sDataTmp.substr(iMacroEnd + 1);
						iMacroStart = inStr(0, sDataTmp, "$CURRENT");
					}
				}
				
				//Resolve all $CALC
				iMacroStart = inStr(0, sDataTmp, "$CALC");
				
				while (iMacroStart != -1){
					iMacroEnd = inStr(iMacroStart + 6, sDataTmp, ")");
					var sCalcExpr = sDataTmp.substring(iMacroStart + 6, iMacroEnd);
					sMacroValue = jsEval(sCalcExpr);
					sMacroValue = convert(sMacroValue, sDataType);

					sDataTmp = sDataTmp.substr(0, iMacroStart) + sMacroValue + sDataTmp.substr(iMacroEnd + 1);
					iMacroStart = inStr(0, sDataTmp, "$CALC");
				}
				
			} else {
				sDataTmp = convert(sData, sDataType);
			}
			
			//check if the parameter value contains the array delimiter ||.
			//Evaluate the expression only when it is not an array.
			//if (sDataTmp.indexOf("||") == -1){
				//evaluate the expression
				//sDataTmp = jsEval(sDataTmp);
			//}
			
			return sDataTmp;
		}
		
		function convert(sData, sDataType){
			var sDataTmp = "";
			switch (sDataType){
				case "case-sensitive":
					sDataTmp = "\'" + sData + "\'";
					break;
				case "case-insensitive":
					sDataTmp = "\'" + sData.toLowerCase() + "\'";
					break;
				case "number":
					if (sData == "" || isNaN(sData)){
						sDataTmp = "\'" + sData + "\'";
					} else
						sDataTmp = sData;
					break;
				case "boolean":
					sData = sData.toLowerCase();
					if (sData == "true" || sData == "t" || sData == "yes" || sData == "y" || sData == "1")
						sDataTmp = "true";
					else
						sDataTmp = "false";
					break;
				case "date":
					if (isDate(sData))
						sDataTmp = "\'" + str2Date(sData) + "\'";
					else
						sDataTmp = "\'" + sData + "\'";
					break;
				default:
					sDataTmp = "\'" + sData + "\'";
					break;
			}
			return sDataTmp;
		}
		
		function jsEval(strExpression){
			var strRet = "";
			if (strExpression != ""){
				try {
					strRet = eval(strExpression);
				} catch (e) {
					strRet = "\"ERROR: " + e.description + "\"";
				}
			}
			return strRet;
		}
		
		function In(strLParm, strRParm){
			var bRet = false;
			var aArray = strRParm.split("||");
			for (var i = 0; i < aArray.length; i++){
				if (aArray[i] == strLParm){
					bRet = true;
					break;
				}
			}
			return bRet;
		}
		
		function NotIn(strLParm, strRParm){
			return !(In(strLParm, strRParm));
		}
		
		function Like(strLParm, strRParm){
			var bRet = false;
			if (strRParm != ''){
				strRParm = String(strRParm);
				if (strRParm.indexOf("||") != -1){
					//right parameter is an array
					var aRParm = strRParm.split("||");
					for (var i = 0; i < aRParm.length; i++){
						if (aRParm[i].indexOf(strLParm) != -1){
							bRet = true;
							break;
						}
					}
				 } else
					bRet = (strRParm.indexOf(strLParm) != -1)
			 }
				return bRet;
		}
		
		function Between(strLParm, strRParmBegin, strRParmEnd){
			var bRet = false;
			if (typeof(strLParm) == "number"){
				bRet = (strLParm >= strRParmBegin && strLParm <= strRParmEnd);
			} else if (typeof(strLParm == "string")){
				//check the parameter for date type
				if (isDate(strLParm) && isDate(strRParmBegin) && isDate(strRParmEnd)){
					var dteLParm = new Date(str2Date(strLParm))
					var dteRParmBegin = new Date(str2Date(strRParmBegin))
					var dteRParmEnd = new Date(str2Date(strRParmEnd))

					bRet = (dteLParm >= dteRParmBegin && dteLParm <= dteRParmEnd);
				} else
					bRet = (strLParm >= strRParmBegin && strLParm <= strRParmEnd);
			} else
				bRet = false;

				return bRet;
		}
		
		function isDate(str){
			var bRet;
			return reSQLDateTime.test(str) ||reSysDateTime.test(str) || reSysDate.test(str);
		}
		
		function str2Date(str){
			var dte = "";
			if (reSQLDateTime.test(str)){
				var a = str.match(reSQLDateTime);
				if (a) {
					if (a.length > 5){
						dte = a[2] + "-" + a[3] + "-" + a[1] + " " + a[4] + ":" + a[5] + ":" + a[6];
						}
				}
			 } else if (reSysDateTime.test(str)) {
				var a = str.match(reSysDateTime);
				if (a) {
					if (a.length > 5){
						dte = a[1] + "-" + a[2] + "-" + a[3] + " " + a[4] + ":" + a[5] + ":" + a[6];
						}
				}
			 } else if (reSysDate.test(str)){
				var a = str.match(reSysDate);
				if (a) {
					if (a.length > 2){
						dte = a[1] + "-" +  a[2] + "-" + a[3];
					}
				}
			 }

			 return dte;
		}

		function getFactIndex(sFactID){
			for (var i = 0; i < objFacts.length; i++){
				if (objFacts[i].id == sFactID){
					return i;
				}
			}
			return -1;
		}
		
		function initExpression(){
			aExpressions = new Array();
			aLValues = new Array();
			aRValues = new Array();
			return "";
		}
		
		function addExpression(strExpression){
			aExpressions.push(strExpression);
			return "";
		}
		
		function getExpressions(){
			return aExpressions.join("");
		}
		
		function getLValues(){
    		return aLValues.join("|");
		}

		function getRValues(){
    		return aRValues.join("|");
		}

		function setFactData(sID, sData, sDataType){
			var iFactIdx = getFactIndex(sID);
			var sValue = "";
			var sValueType = "";
			var objFact = null;
			if (iFactIdx > -1){
				return "ERROR: Fact id already exists";
			}
			if (iFactIdx == -1){
				if (sDataType == "") sDataType = "text";
				sValue = evaluate(sData, sDataType, null);
				sValue = jsEval(sValue);
				sValueType = typeof(sValue);
				
				objFact = { id : sID,
								value: sValue,
								type: sValueType };
				objFacts.push(objFact);
			}
			
			return sValue;
		}
		
		function getFactData(sID){
			var iFactIdx = getFactIndex(sID);
			if (iFactIdx > -1){
				return objFacts[iFactIdx].value;
			} else {
				return "ERROR: missing FactID - " + sID
			}
		}
		
		function expressionBuilder(oNode, strChildNode, oCurrentNode){
			var sRet = "";
			
			oCurrentNode = getCurrentNode();
			
			//initialize the expressions array
			initExpression();

			if (oNode && oNode[0]){

				for (var i = 0; i < oNode.length; i++){
					var sLogicalOperation = oNode[i].getAttribute("logicalOperation");

					if (i > 0){
						if (sLogicalOperation == "and")
							addExpression(" && ");
						else
							addExpression(" || ");
					}

					addExpression("(");

					var oChildNodes = oNode[i].childNodes;
					for (var j = 0; j < oChildNodes.length; j++){
						if (oChildNodes[j].nodeName == strChildNode){
							var strLParam = oChildNodes[j].getAttribute("lparm");
							var strRParam = oChildNodes[j].getAttribute("rparm");
							var strOperation = oChildNodes[j].getAttribute("operation");
							var strCompareMethod = oChildNodes[j].getAttribute("comparemethod");
							var strRBetweenBegin = oChildNodes[j].getAttribute("betweenBegin");
							var strRBetweenEnd = oChildNodes[j].getAttribute("betweenEnd");
							var strLogicalOperation = oChildNodes[j].getAttribute("logicalOperation");
							var strDataType = "case-sensitive";
							var strExpression = strOperation2 = "";

							switch(strOperation){
								case "equal": strOperation2 = " == "; break;
								case "notequal": strOperation2 = " != "; break;
								case "lesserthan": strOperation2 = " < "; break;
								case "greaterthan": strOperation2 = " > "; break;
								case "lesserthanequal": strOperation2 = " <= "; break;
								case "greaterthanequal": strOperation2 = " >= "; break;
								default: strOperation2 = " == "; break;
							}

							switch(strCompareMethod) {
								case "number": strDataType = "number"; break;
								case "boolean": strDataType = "boolean"; break;
								case "date": strDataType = "date"; break;
								case "case-sensitive": strDataType = "case-sensitive"; break;
								case "case-insensitive": strDataType = "case-insensitive"; break;
							}

							if (j > 0){
								if (strLogicalOperation == "and")
									addExpression(" && ");
								else
									addExpression(" || ");
							}

							//evaluate the left param
							if (strLParam != ""){
								strLParam = evaluate(strLParam, strDataType, oCurrentNode);
								aLValues.push(jsEval(strLParam));
							}

							//evaluate the right param
							if (strRParam != "" && strOperation != "between"){
								strRParam = evaluate(strRParam, strDataType, oCurrentNode);
								aRValues.push(jsEval(strRParam));
							}

							//evaluate the between begin parameter if applicable
							if (strRBetweenBegin != '' && strOperation == "between"){
								strRBetweenBegin = evaluate(strRBetweenBegin, strDataType, oCurrentNode);
							}

							//evaluate the between end parameter if applicable
							if (strRBetweenEnd != '' && strOperation == "between"){
								strRBetweenEnd = evaluate(strRBetweenEnd, strDataType, oCurrentNode);
							}

							switch(strOperation){
								case "in":
									strExpression = "In(" + strLParam + ", " + strRParam + ")";
									break;
								case "notin":
									strExpression = "NotIn(" + strLParam + ", " + strRParam + ")";
									break;
								case "like":
									strExpression = "Like(" + strLParam + ", " + strRParam + ")";
									break;
								case "between":
									strExpression = "Between(" + strLParam + ", " + strRBetweenBegin + ", " + strRBetweenEnd + ")";
									break;
								default:
									strExpression = strLParam + strOperation2 + strRParam;
									break;
							}

							addExpression("(" + strExpression + ")");
						}
					}

					addExpression(")");
				}
			}

			return sRet;
		}
		
		function getRecurseNodeList(strXPath, oConditions, strChildNode){
			var oFilteredList = null;
			if (oRoot != null && strXPath != ""){
				if (strXPath.substr(0, 2) != "//") strXPath = "//" + strXPath;

				if (oConditions && oConditions.length > 0){
					var sRecurseCondition = getRecurseCondition(oConditions, strChildNode);
					strXPath += "[" + sRecurseCondition + "]"
					if (sRecurseCondition != ""){
						oFilteredList = oRoot.selectNodes(strXPath);
					}
				} else
				oFilteredList = oRoot.selectNodes(strXPath);

			}
			return oFilteredList;
		}
		
		function getRecurseCondition(oConditions, strChildNode){
			var sRet = "";
			//initialize the expressions array
			initExpression();

			if (oConditions && oConditions.length > 0){
				var oConditions2 = oConditions[0].childNodes;
				for (var i = 0; i < oConditions2.length; i++){
					var sLogicalOperation = oConditions2[i].getAttribute("logicalOperation");

					if (i > 0){
						if (sLogicalOperation == "and")
							addExpression(" and ");
						else
							addExpression(" or ");
					}

					addExpression("(");

					var oChildNodes = oConditions2[i].childNodes;
					var iExpCount = 0;
					for (var j = 0; j < oChildNodes.length; j++){
						if (oChildNodes[j].nodeName == strChildNode){
							var strLParam = oChildNodes[j].getAttribute("lparm");
							var strRParam = oChildNodes[j].getAttribute("rparm");
							var strOperation = oChildNodes[j].getAttribute("operation");
							var strCompareMethod = oChildNodes[j].getAttribute("comparemethod");
							var strLogicalOperation = oChildNodes[j].getAttribute("logicalOperation");
							var strDataType = "case-sensitive";
							var strExpression = strOperation2 = "";

							switch(strOperation){
								case "equal": strOperation2 = " = "; break;
								case "notequal": strOperation2 = " != "; break;
								case "lesserthan": strOperation2 = " &lt; "; break;
								case "greaterthan": strOperation2 = " &gt; "; break;
								case "lesserthanequal": strOperation2 = " &lt;= "; break;
								case "greaterthanequal": strOperation2 = " &gt;= "; break;
								default: strOperation2 = " = "; break;
							}

							switch(strCompareMethod) {
								case "number": strDataType = "number"; break;
								case "boolean": strDataType = "boolean"; break;
								case "date": strDataType = "date"; break;
								case "case-sensitive": strDataType = "case-sensitive"; break;
								case "case-insensitive": strDataType = "case-insensitive"; break;
							}

							if (iExpCount > 0){
								if (strLogicalOperation == "and")
									addExpression(" and ");
								else
									addExpression(" or ");
							}

							if (strLParam.indexOf("/") == -1 && strLParam.indexOf("@") == -1) strLParam = "'" + strLParam + "'";
							if (strRParam.indexOf("/") == -1 && strRParam.indexOf("@") == -1) strRParam = "'" + strRParam + "'";

							addExpression(strLParam + strOperation2 + strRParam);
							iExpCount++;
						}
					}
					addExpression(")");
				}
			}
			sRet = getExpressions();
			return sRet;
		}
		
		function setConditionsNodes(oNodes){
			oConditionsNodeList = null;

			if (oNodes){
				oConditionsNodeList = oNodes;
			}

			return "";
		}
		
		function getConditionsNodes(){
			return oConditionsNodeList;
		}
		
		function setCurrentNode(oNodes){
			oCurrentNode = null;

			if (oNodes && oNodes[0]){
				oCurrentNode = oNodes[0];
			}

			return "";
		}
		
		function getCurrentNode(){
			return  oCurrentNode;
		}
		
		function getRecurseKeyNameValue(oCurNode, sKeyAttrib){
			var sRet = "";

			if (oCurNode && oCurNode[0] && sKeyAttrib != ""){
				var aAttribs = sKeyAttrib.split("|");
				var aAttribValues = new Array();

				for (var i = 0; i < aAttribs.length; i++){
				    var sAttrib = "";
                    if (aAttribs[i].indexOf("/") == -1){
    					sAttrib = oCurNode[0].getAttribute(aAttribs[i]);
    					if (sRet == null)
                           sAttrib = "!ERROR!";
    					aAttribValues.push(sAttrib);
    				} else {
    					var oNode = oCurNode[0];
    					while (aAttribs[i].substr(0, 3) == "../"){
    						oNode = oNode.parentNode;
    						aAttribs[i] = aAttribs[i].substr(3);
    					}

    					if (oNode){
    						if (aAttribs[i].substr(0, 1) == "@") {
    							aAttribs[i] = aAttribs[i].substr(1);
    							sAttrib = oNode.getAttribute(aAttribs[i]);
    						} else {
    							oNode = oNode.selectSingleNode(aAttribs[i]);
    							if (oNode)
    								sAttrib = oNode.text;
    							else
    								sAttrib = "!ERROR!";
    						}
    					} else {
    						sRet = "!ERROR!";
    					}
    					aAttribValues.push(sAttrib);
    				}
				}
				sRet = aAttribValues.join("|");
			}
			return sRet;
		}
		
		function evaluateAction(strAction, strKeyValue, strLValues, strRValues){
			var sRet = strAction;
		
			if (strAction.indexOf("$KEY") != -1){
			   //evaluate all key macros
    			iMacroStart = inStr(0, sRet, "$KEY");
    			
    			while (iMacroStart != -1){
    				iMacroEnd = inStr(iMacroStart + 4, sRet, ")");
    				var iIndex = sRet.substring(iMacroStart + 5, iMacroEnd);
    				var sMacroValue = getKeyValueByIndex(strKeyValue, iIndex);
    				sRet = sRet.substr(0, iMacroStart) + sMacroValue + sRet.substr(iMacroEnd + 1);
    				iMacroStart = inStr(0, sRet, "$KEY");
    			}
			}

			if (strAction.indexOf("$LVALUE") != -1){
			   //evaluate all LValue macros
    			iMacroStart = inStr(0, sRet, "$LVALUE");
    			
    			while (iMacroStart != -1){
    				iMacroEnd = inStr(iMacroStart + 7, sRet, ")");
    				var iIndex = sRet.substring(iMacroStart + 8, iMacroEnd);
    				var sMacroValue = getKeyValueByIndex(strLValues, iIndex);
    				sRet = sRet.substr(0, iMacroStart) + sMacroValue + sRet.substr(iMacroEnd + 1);
    				iMacroStart = inStr(0, sRet, "$LVALUE");
    			}
			}

			if (strAction.indexOf("$RVALUE") != -1){
			   //evaluate all RValue macros
    			iMacroStart = inStr(0, sRet, "$RVALUE");
    			
    			while (iMacroStart != -1){
    				iMacroEnd = inStr(iMacroStart + 7, sRet, ")");
    				var iIndex = sRet.substring(iMacroStart + 8, iMacroEnd);
    				var sMacroValue = getKeyValueByIndex(strRValues, iIndex);
    				sRet = sRet.substr(0, iMacroStart) + sMacroValue + sRet.substr(iMacroEnd + 1);
    				iMacroStart = inStr(0, sRet, "$RVALUE");
    			}
			}

			if (sRet.indexOf("$CALC") != -1 ||
                sRet.indexOf("$XPATH") != -1 ||
                sRet.indexOf("$FACT") != -1 ||
                sRet.indexOf("$CURRENT") != -1){
                //evaluate other macros
                sRet = evaluate(sRet, "number", getCurrentNode());
            }
			return sRet;
		}
		
		function getKeyValueByIndex(sKeyValue, iIndex){
		    var sRet = "";
		    if (sKeyValue != ""){
		        var aKeys = sKeyValue.split("|");
		        if (iIndex <= aKeys.length){
		           sRet = aKeys[iIndex];
		        } else
		           sRet ="ERROR:Index out-of-bound";
		    }
		    return sRet;
		}
		
	]]>
</msxsl:script>

<xsl:template match="/RulesPackage">
	<xsl:value-of select="user:setRoot(.)"/>
	<!-- Evaluate the Facts -->
	<xsl:variable name="Facts">
	<xsl:element name="Facts">
	<xsl:for-each select="Rules/Facts/Fact">
		<xsl:element name="Fact">
			<!-- General validations -->
			<xsl:if test="@id = ''">
				<xsl:message terminate="yes">Missing Fact id attribute</xsl:message>
			</xsl:if>
			<!-- Compile the Facts -->
			<xsl:for-each select="@*">
				<xsl:choose>
					<xsl:when test="name(.) = 'value'">
						<xsl:attribute name="value"><xsl:value-of select="user:setFactData(string(./../@id), string(.), string(./../@datatype))"/></xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy><xsl:value-of select="."/></xsl:copy>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:element>
	</xsl:for-each>	
	</xsl:element>
	</xsl:variable>

	<!-- Evaluate macros in the rules -->
	<xsl:variable name="Rules">
	<xsl:element name="Rules">
		<xsl:for-each select="Rules/Rule[@enabled='Y']">
			<xsl:variable name="recurseNode"><xsl:value-of select="Recurse/Node"/></xsl:variable>
			<xsl:variable name="recurseKey"><xsl:value-of select="Recurse/Key/Node"/></xsl:variable>
			<xsl:variable name="recurseKeyName"><xsl:value-of select="Recurse/Key/Description"/></xsl:variable>
			<xsl:variable name="recurseConditions">
				<xsl:copy-of select="Recurse/Conditions"/>
			</xsl:variable>
				<xsl:element name="Rule">
				<!-- copy the Rule attibutes -->
				<xsl:for-each select="@*">
					<xsl:copy-of select="."/>
				</xsl:for-each>
				
				<xsl:if test="string(@weight) = ''">
    				<!-- Default weight value -->
                    <xsl:attribute name="weight"><xsl:value-of select="0"/></xsl:attribute>
                </xsl:if>

				<!-- copy the recurse node -->
				<xsl:copy-of select="Recurse"/>

				<!-- Compile the exceptions -->
				<xsl:for-each select="Exception[@enabled='Y']">
    				<xsl:element name="Exception">
						<!-- Compile the context -->
						<xsl:value-of select="user:expressionBuilder(Context, 'Item', .)"/>
						<xsl:variable name="contextExpression"><xsl:value-of select="user:getExpressions()"/></xsl:variable>
						<xsl:variable name="inContext">
							<xsl:choose>
								<xsl:when test="$contextExpression != ''">
									<!-- check for any error while building the expression -->
									<xsl:call-template name="checkError">
										<xsl:with-param name="text"><xsl:value-of select="$contextExpression"/></xsl:with-param>
										<xsl:with-param name="ruleID"><xsl:value-of select="concat(../@id, ' - Exception ', string(position()))"/></xsl:with-param>
									</xsl:call-template>
									<xsl:value-of select="user:jsEval(string($contextExpression))"/>
								</xsl:when>
								<xsl:otherwise>false</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:element name="Context">
							<xsl:attribute name="incontext"><xsl:value-of select="$inContext"/></xsl:attribute>
							<xsl:value-of select="$contextExpression"/>
						</xsl:element>

						<xsl:if test="$inContext='true'">
							<!-- Compile the conditions only when the Rule is in context -->
							<xsl:choose>
								<xsl:when test="$recurseNode != ''">
									<!-- Store the Conditions nodes -->
									<xsl:value-of select="user:setConditionsNodes(Conditions)"/>
									<xsl:for-each select="user:getRecurseNodeList(string($recurseNode), $recurseConditions, 'Condition')">
										<!-- Build the expression for each of the recursing node -->
										<xsl:value-of select="user:setCurrentNode(.)"/>
										<xsl:call-template name="buildConditions">
											<xsl:with-param name="ConditionNodeName">Condition</xsl:with-param>
											<xsl:with-param name="recurseKeyNames"><xsl:value-of select="$recurseKeyName"/></xsl:with-param>
											<xsl:with-param name="recurseKeyNodes"><xsl:value-of select="$recurseKey"/></xsl:with-param>
										</xsl:call-template>
									</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="user:setConditionsNodes(Conditions)"/>
									<xsl:value-of select="user:setCurrentNode(.)"/>
									<xsl:call-template name="buildConditions">
										<xsl:with-param name="ConditionNodeName">Condition</xsl:with-param>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
    				</xsl:element>
				</xsl:for-each>

				<!-- Process the default condition -->
				<xsl:choose>
					<xsl:when test="$recurseNode != ''">
						<!-- Store the Conditions nodes -->
						<xsl:value-of select="user:setConditionsNodes(Default[@enabled='Y']/Conditions)"/>
						<xsl:element name="Default">
							<xsl:for-each select="user:getRecurseNodeList(string($recurseNode), $recurseConditions, 'Condition')">
								<!-- Build the expression for each of the recursing node -->
								<xsl:value-of select="user:setCurrentNode(.)"/>
								<xsl:call-template name="buildConditions">
									<xsl:with-param name="ConditionNodeName">Condition</xsl:with-param>
								    <xsl:with-param name="recurseKeyNames"><xsl:value-of select="$recurseKeyName"/></xsl:with-param>
									<xsl:with-param name="recurseKeyNodes"><xsl:value-of select="$recurseKey"/></xsl:with-param>
								</xsl:call-template>
							</xsl:for-each>
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
						<xsl:element name="Default">
							<xsl:value-of select="user:setConditionsNodes(Default[@enabled='Y']/Conditions)"/>
							<xsl:value-of select="user:setCurrentNode(.)"/>
							<xsl:call-template name="buildConditions">
								<xsl:with-param name="ConditionNodeName">Condition</xsl:with-param>
							</xsl:call-template>
						</xsl:element>
					</xsl:otherwise>
				</xsl:choose>

				<!-- Copy the Action element -->
				<xsl:copy-of select="Action"/>
			</xsl:element><!-- Rule Element -->
		</xsl:for-each> <!-- Rule level -->

		<!-- Rules that are disabled. These will not be processed. For recording purpose only -->
		<xsl:for-each select="Rules/Rule[@enabled != 'Y']">
			<xsl:element name="Rule">
				<!-- copy the Rule attibutes -->
				<xsl:for-each select="@*">
					<xsl:copy-of select="."/>
				</xsl:for-each>

				<!-- Copy the Action element -->
				<xsl:copy-of select="Action"/>
			</xsl:element>
		</xsl:for-each>
	</xsl:element>
	</xsl:variable> <!-- Rules variable -->

	<!-- Process the rules now -->
	<xsl:variable name="RulesFired">
		<xsl:for-each select="msxsl:node-set($Rules)/Rules/Rule[@enabled='Y']">
			<xsl:value-of select="user:setCurrentNode(.)"/>
			<xsl:choose>
				<xsl:when test="Recurse/Node != ''">
					<xsl:choose>
						<xsl:when test="count(Exception/Context[@incontext='true']) &gt; 0">
							<!-- Rule exception is in context -->
							<xsl:for-each select="Exception/Condition[@passed='true']">
								<xsl:call-template name="processRule">
									<xsl:with-param name="reasonText">Exception condition met</xsl:with-param>
								</xsl:call-template>
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<!-- check if any default condition passed -->
							<xsl:if test="count(Exception/Context) = 0">
								<xsl:for-each select="Default/Condition[@passed='true']">
									<xsl:call-template name="processRule">
										<xsl:with-param name="reasonText">Default condition met</xsl:with-param>
									</xsl:call-template>
								</xsl:for-each>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<!-- Non recursive Rule -->
					<xsl:choose>
						<xsl:when test="count(Exception/Context[@incontext='true']) &gt; 0">
							<!-- Rule exception is in context -->
							<xsl:if test="Exception/Condition[@passed='true']">
								<xsl:call-template name="processRule">
									<xsl:with-param name="reasonText">Exception condition met</xsl:with-param>
								</xsl:call-template>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<!-- check if the default condition passed -->
							<xsl:if test="count(Exception/Context) = 0 and Default/Condition/@passed = 'true'">
								<xsl:call-template name="processRule">
									<xsl:with-param name="reasonText">Default condition met</xsl:with-param>
								</xsl:call-template>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:variable>

	<xsl:element name="Root">
	
	<xsl:attribute name="auditWeight">
    	<xsl:value-of select="sum(msxsl:node-set($RulesFired)/Rule/@weight)"/>
    </xsl:attribute>
	
    <xsl:element name="RulesFired">
        <xsl:copy-of select="$RulesFired"/>
	</xsl:element>

	<xsl:element name="RulesNotFired">
		<xsl:for-each select="msxsl:node-set($Rules)/Rules/Rule[@enabled='Y']">
			<xsl:value-of select="user:setCurrentNode(.)"/>
            <xsl:choose>
				<xsl:when test="Recurse/Node != ''">
					<!-- Recursive rule -->
					<xsl:choose>
						<xsl:when test="count(Exception/Context) &gt; 0 and count(Exception/Context[@incontext='true']) = 0">
							<!-- Rule exception is not in context -->
							<xsl:call-template name="processRule">
								<xsl:with-param name="reasonText">Not in context</xsl:with-param>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="count(Exception/Condition) &gt; 0">
									<!-- check if any exception condition not passed -->
									<xsl:for-each select="Exception/Condition[@passed!='true']">
										<xsl:call-template name="processRule">
											<xsl:with-param name="reasonText">Exception condition not met</xsl:with-param>
										</xsl:call-template>
									</xsl:for-each>
								</xsl:when>
								<xsl:when test="count(Exception/Condition) = 0 and count(Default/Condition) = 0">
									<xsl:call-template name="processRule">
										<xsl:with-param name="reasonText">Recursive list returned no matching data</xsl:with-param>
									</xsl:call-template>
                                </xsl:when>
								<xsl:otherwise>
									<!-- check if any default condition not passed -->
									<xsl:for-each select="Default/Condition[@passed!='true']">
										<xsl:call-template name="processRule">
											<xsl:with-param name="reasonText">Default condition not met</xsl:with-param>
										</xsl:call-template>
									</xsl:for-each>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<!-- Non recursive Rule -->
					<xsl:choose>
						<xsl:when test="count(Exception/Context) &gt; 0 and count(Exception/Context[@incontext='true']) = 0">
							<!-- Rule exception is not in context -->
							<xsl:call-template name="processRule">
								<xsl:with-param name="reasonText">Not in context</xsl:with-param>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="count(Exception/Condition) &gt; 0">
									<!-- check if any exception condition not passed -->
									<xsl:if test="Exception/Condition[@passed!='true']">
										<xsl:call-template name="processRule">
											<xsl:with-param name="reasonText">Exception condition not met</xsl:with-param>
										</xsl:call-template>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
									<!-- check if the default condition not passed -->
									<xsl:if test="Default/Condition/@passed != 'true'">
										<xsl:call-template name="processRule">
											<xsl:with-param name="reasonText">Default condition not met</xsl:with-param>
										</xsl:call-template>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		<xsl:for-each select="msxsl:node-set($Rules)/Rules/Rule[@enabled!='Y']">
			<xsl:element name="Rule">
				<xsl:for-each select="@*">
					<xsl:copy-of select="."/>
				</xsl:for-each>
				<xsl:attribute name="reason">Rule disabled</xsl:attribute>
			</xsl:element>
		</xsl:for-each>
	</xsl:element>
	<!-- Debug information -->
	<xsl:attribute name="debug"><xsl:value-of select="$debug"/></xsl:attribute>
	<xsl:if test="$debug = 'Y'">
		<xsl:element name="Debug">
			<xsl:copy-of select="$Facts"/>
			<xsl:copy-of select="$Rules"/>
		</xsl:element>
	</xsl:if>
	</xsl:element>
</xsl:template>

<xsl:template name="buildConditions">
	<xsl:param name="ConditionNodeName"/>
	<xsl:param name="CurrentNode"/>
	<xsl:param name="recurseKeyNames"/>
	<xsl:param name="recurseKeyNodes"/>

	<xsl:value-of select="user:expressionBuilder(user:getConditionsNodes(), string($ConditionNodeName), $CurrentNode)"/>
	<xsl:variable name="conditionsExpression"><xsl:value-of select="user:getExpressions()"/></xsl:variable>
	<xsl:if test="$conditionsExpression != ''">
		<xsl:element name="Condition">
			<xsl:call-template name="checkError">
				<xsl:with-param name="text"><xsl:value-of select="$conditionsExpression"/></xsl:with-param>
				<xsl:with-param name="ruleID"><xsl:value-of select="concat(../@id, ' - Exception ', string(position()))"/></xsl:with-param>
			</xsl:call-template>
			<xsl:attribute name="passed"><xsl:value-of select="user:jsEval(string($conditionsExpression))"/></xsl:attribute>
			<xsl:attribute name="LValues"><xsl:value-of select="user:getLValues()"/></xsl:attribute>
			<xsl:attribute name="RValues"><xsl:value-of select="user:getRValues()"/></xsl:attribute>
			<xsl:choose>
    			<xsl:when test="$recurseKeyNodes != ''">
    				<xsl:variable name="keyNameValue"><xsl:value-of select="user:getRecurseKeyNameValue(user:getCurrentNode(), string($recurseKeyNodes))"/></xsl:variable>
    				<xsl:attribute name="keyNames"><xsl:value-of select="$recurseKeyNames"/></xsl:attribute>
    				<xsl:attribute name="keyValue"><xsl:value-of select="$keyNameValue"/></xsl:attribute>
    			</xsl:when>
    			<xsl:otherwise>
    			    <xsl:if test="Conditions/Condition/@keyValue != ''">
            			<xsl:attribute name="keyValue"><xsl:value-of select="Conditions/Condition/@keyValue"/></xsl:attribute>
        			</xsl:if>
                </xsl:otherwise>
            </xsl:choose>
			<xsl:value-of select="$conditionsExpression"/>
		</xsl:element>
	</xsl:if>
</xsl:template>

<xsl:template name="processRule">
	<xsl:param name="reasonText"/>
	<xsl:element name="Rule">
		<xsl:for-each select="user:getCurrentNode()/@*">
			<xsl:copy-of select="."/>
		</xsl:for-each>
		<xsl:if test="user:getCurrentNode()/Recurse/Node != ''">
			<xsl:attribute name="recursive">Y</xsl:attribute>
			<xsl:attribute name="keyName"><xsl:value-of select="@keyNames"/></xsl:attribute>
			<xsl:attribute name="keyValue"><xsl:value-of select="@keyValue"/></xsl:attribute>
		</xsl:if>
		<xsl:variable name="keyValue">
    		<xsl:choose>
        		<xsl:when test="@keyValue != ''">
        		    <xsl:value-of select="@keyValue"/>
                </xsl:when>
                <xsl:when test="user:getCurrentNode()/Exception[Context[@incontext='true']]/Condition/@keyValue != ''">
            		<xsl:value-of select="user:getCurrentNode()/Exception[Context[@incontext='true']]/Condition/@keyValue"/>
                </xsl:when>
                <xsl:when test="user:getCurrentNode()/Default/Condition/@keyValue != ''">
            		<xsl:value-of select="user:getCurrentNode()/Default/Condition/@keyValue"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
		<xsl:attribute name="reason"><xsl:value-of select="$reasonText"/></xsl:attribute>
		<xsl:variable name="LValues">
    		<xsl:choose>
        		<xsl:when test="@LValues != ''">
        		    <xsl:value-of select="@LValues"/>
                </xsl:when>
                <xsl:when test="user:getCurrentNode()/Exception[Context[@incontext='true']]/Condition/@LValues != ''">
            		<xsl:value-of select="user:getCurrentNode()/Exception[Context[@incontext='true']]/Condition/@LValues"/>
                </xsl:when>
                <xsl:when test="user:getCurrentNode()/Default/Condition/@LValues != ''">
            		<xsl:value-of select="user:getCurrentNode()/Default/Condition/@LValues"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
		<xsl:variable name="RValues">
    		<xsl:choose>
        		<xsl:when test="@RValues != ''">
        		    <xsl:value-of select="@RValues"/>
                </xsl:when>
                <xsl:when test="user:getCurrentNode()/Exception[Context[@incontext='true']]/Condition/@RValues != ''">
            		<xsl:value-of select="user:getCurrentNode()/Exception[Context[@incontext='true']]/Condition/@RValues"/>
                </xsl:when>
                <xsl:when test="user:getCurrentNode()/Default/Condition/@RValues != ''">
            		<xsl:value-of select="user:getCurrentNode()/Default/Condition/@RValues"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
		<xsl:element name="Action">
			<xsl:value-of select="user:evaluateAction(string(user:getCurrentNode()/Action), string($keyValue), string($LValues), string($RValues))"/>
		</xsl:element>
	</xsl:element>
</xsl:template>

<xsl:template name="checkError">
	<xsl:param name="text"/>
	<xsl:param name="ruleID"/>
	<xsl:if test="contains($text, 'ERROR:') = true()">
		<!-- <xsl:message terminate="yes">
			Error processing Rule id:<xsl:value-of select="concat(' ', $ruleID, user:crlf())"/>
			Expression: <xsl:value-of select="concat($text, user:crlf())"/>
		</xsl:message> -->
	</xsl:if>
</xsl:template>

</xsl:stylesheet>