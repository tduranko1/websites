<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:cefutil="http://cef.com/utilities">

  <!-- Scripts required below. -->
  <msxsl:script language="JScript" implements-prefix="cefutil">

    // Returns the current year.
    function GetYear()
    {
        var date = new Date();
        return date.getFullYear();
    }

  </msxsl:script>

  <!-- Convert model year to yyyy -->
  <xsl:template name="Year">
    <xsl:param name="In"/>
    <xsl:choose>

      <!-- Two character year passed in - convert to 4 -->
      <xsl:when test="string-length($In)=2">

        <xsl:variable name="Cur">
          <xsl:value-of select="cefutil:GetYear()"/>
        </xsl:variable>

        <!-- Compare to current year last two digits -->
        <xsl:choose>
          <xsl:when test="number(substring($Cur,3,2)) &gt;= number($In)">
            <xsl:value-of select="number(substring($Cur,1,2))"/>
            <xsl:value-of select="$In"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="number(substring($Cur,1,2))-1"/>
            <xsl:value-of select="$In"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <!-- Four digit year passed in - use it -->
      <xsl:otherwise>
        <xsl:value-of select="$In"/>
      </xsl:otherwise>

    </xsl:choose>
  </xsl:template>

  <!-- Extracts the date from a DateTime element in mm/dd/yyyy format -->
  <xsl:template match="DateTime" mode="Date">
    <xsl:if test="string-length(@Date)!=0">
      <xsl:value-of select="substring(@Date, 1, 2)"/><xsl:text>/</xsl:text>
      <xsl:value-of select="substring(@Date, 3, 2)"/><xsl:text>/</xsl:text>
      <xsl:value-of select="substring(@Date, 5, 4)"/>
    </xsl:if>
  </xsl:template>

  <!-- Extracts the time from a DateTime element in hh:mm:ss format -->
  <xsl:template match="DateTime" mode="Time">
    <xsl:if test="string-length(@Time)!=0">
      <xsl:value-of select="substring(@Time, 1, 2)"/><xsl:text>:</xsl:text>
      <xsl:value-of select="substring(@Time, 3, 2)"/><xsl:text>:</xsl:text>
      <xsl:value-of select="substring(@Time, 5, 2)"/>
    </xsl:if>
  </xsl:template>

  <!-- Converts an SG Originator code into an APD insurance company ID -->
  <xsl:template name="SGtoAPDInsCoID">
    <xsl:param name="Originator"/>
    <xsl:choose>
      <xsl:when test="$Originator='EIC'">158</xsl:when>
      <xsl:otherwise>0</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Converts an Address into APD address params -->
  <xsl:template match="Address">
    <xsl:param name="Title"/>

    <xsl:attribute name="{concat($Title,'Address1')}">
      <xsl:value-of select="Addr1"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'Address2')}">
      <xsl:value-of select="Addr2"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'AddressCity')}">
      <xsl:value-of select="City"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'AddressState')}">
      <xsl:value-of select="State"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'AddressZip')}">
      <xsl:value-of select="Zip"/>
    </xsl:attribute>

  </xsl:template>

  <!-- Converts a Contact Person into APD contact params -->
  <xsl:template match="ContactPerson" mode="ContactParams">
    <xsl:param name="Title"/>

    <xsl:apply-templates select="Address">
      <xsl:with-param name="Title"><xsl:value-of select="$Title"/></xsl:with-param>
    </xsl:apply-templates>

    <xsl:attribute name="{concat($Title,'BestPhoneCode')}">
      <xsl:value-of select="BestPhoneCode"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'BestTimeToCall')}">
      <xsl:value-of select="BestTimeToCall"/>
    </xsl:attribute>

    <xsl:attribute name="{concat($Title,'NameFirst')}">
      <xsl:value-of select="Person/@FirstName"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'NameLast')}">
      <xsl:value-of select="Person/@LastName"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'NameTitle')}">
      <xsl:value-of select="Person/@Title"/>
    </xsl:attribute>

    <xsl:attribute name="{concat($Title,'PhoneAlternate')}">
      <xsl:apply-templates select="." mode="BestPhoneAlt"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'PhoneAlternateExt')}">
      <xsl:apply-templates select="." mode="BestPhoneAltExtn"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'PhoneDay')}">
      <xsl:apply-templates select="." mode="BestPhoneDay"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'PhoneDayExt')}">
      <xsl:apply-templates select="." mode="BestPhoneDayExtn"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'Phone')}">
      <xsl:apply-templates select="." mode="BestPhoneDay"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'PhoneExt')}">
      <xsl:apply-templates select="." mode="BestPhoneDayExtn"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'PhoneNight')}">
      <xsl:apply-templates select="." mode="BestPhoneNight"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'PhoneNightExt')}">
      <xsl:apply-templates select="." mode="BestPhoneNightExtn"/>
    </xsl:attribute>

    <xsl:attribute name="{concat($Title,'EmailAddress')}">
      <xsl:value-of select="ContactEmail"/>
    </xsl:attribute>

  </xsl:template>

  <!-- Converts a Contact Person into APD contact params - light version -->
  <xsl:template match="ContactPerson" mode="ContactParamsLight">
    <xsl:param name="Title"/>

    <xsl:apply-templates select="Address">
      <xsl:with-param name="Title"><xsl:value-of select="$Title"/></xsl:with-param>
    </xsl:apply-templates>
    <xsl:attribute name="{concat($Title,'Name')}">
      <xsl:apply-templates select="." mode="FullName"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'Phone')}">
      <xsl:apply-templates select="." mode="BestPhone"/>
    </xsl:attribute>
    <xsl:attribute name="{concat($Title,'PhoneExt')}">
      <xsl:apply-templates select="." mode="BestPhoneExtn"/>
    </xsl:attribute>

  </xsl:template>

  <!-- Returns the full name of a CEF ContactPerson -->
  <xsl:template match="ContactPerson" mode="FullName">
    <xsl:value-of select="Person/@FirstName"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="Person/@LastName"/>
  </xsl:template>

  <!-- Telephone Types
    "BP1" - Business Phone 1
    "BP2" - Business Phone 2
    "BF1" - Business Fax 1
    "BF2" - Business Fax 2
    "CEL" - Cell
    "DAY" - Day
    "EVE" - Evening
    "HO1" - Home1
    "HO2" - Home2
    "HOF" - Home Fax
    "PAG" - Pager
    "UNK" - Unknown
  -->

  <!-- Returns the best phone of a ContactPerson -->
  <xsl:template match="ContactPerson" mode="BestPhone">
    <xsl:choose>
      <xsl:when test="ContactTelephone[@TelephoneType='BP1']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP1']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP1']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='DAY']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='DAY']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='DAY']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='BP2']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP2']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP2']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='CEL']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='CEL']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='CEL']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='UNK']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='UNK']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='UNK']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='PAG']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='PAG']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='PAG']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='EVE']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='EVE']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='EVE']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='HO1']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO1']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO1']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='HO2']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO2']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO2']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="ContactTelephone/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone/Telecom/TelecomNum"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Returns the best phone extension of a ContactPerson -->
  <xsl:template match="ContactPerson" mode="BestPhoneExtn">
    <xsl:choose>
      <xsl:when test="ContactTelephone[@TelephoneType='BP1']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP1']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='DAY']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='DAY']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='BP2']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP2']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='CEL']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='CEL']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='UNK']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='UNK']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='PAG']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='PAG']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='EVE']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='EVE']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='HO1']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO1']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='HO2']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO2']/Telecom/Extn"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="ContactTelephone/Telecom/Extn"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Returns the best day phone of a ContactPerson -->
  <xsl:template match="ContactPerson" mode="BestPhoneDay">
    <xsl:choose>
      <xsl:when test="ContactTelephone[@TelephoneType='BP1']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP1']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP1']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='DAY']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='DAY']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='DAY']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='BP2']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP2']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP2']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='CEL']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='CEL']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='CEL']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='UNK']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='UNK']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='UNK']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='PAG']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='PAG']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='PAG']/Telecom/TelecomNum"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- Returns the best day phone extension of a ContactPerson -->
  <xsl:template match="ContactPerson" mode="BestPhoneDayExtn">
    <xsl:choose>
      <xsl:when test="ContactTelephone[@TelephoneType='BP1']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP1']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='DAY']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='DAY']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='BP2']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP2']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='CEL']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='CEL']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='UNK']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='UNK']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='PAG']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='PAG']/Telecom/Extn"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="ContactTelephone/Telecom/Extn"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Returns the best night phone of a ContactPerson -->
  <xsl:template match="ContactPerson" mode="BestPhoneNight">
    <xsl:choose>
      <xsl:when test="ContactTelephone[@TelephoneType='EVE']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='EVE']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='EVE']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='HO1']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO1']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO1']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='HO2']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO2']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO2']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='CEL']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='CEL']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='CEL']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='PAG']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='PAG']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='PAG']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='UNK']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='UNK']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='UNK']/Telecom/TelecomNum"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- Returns the best night phone extension of a ContactPerson -->
  <xsl:template match="ContactPerson" mode="BestPhoneNightExtn">
    <xsl:choose>
      <xsl:when test="ContactTelephone[@TelephoneType='EVE']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='EVE']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='HO1']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO1']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='HO2']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO2']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='CEL']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='CEL']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='PAG']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='PAG']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='UNK']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='UNK']/Telecom/Extn"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- Returns the best alternate phone of a ContactPerson -->
  <xsl:template match="ContactPerson" mode="BestPhoneAlt">
    <xsl:choose>
      <xsl:when test="ContactTelephone[@TelephoneType='BP2']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP2']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP2']/Telecom/TelecomNum"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='HO2']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO2']/Telecom/AreaCode"/>
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO2']/Telecom/TelecomNum"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- Returns the best phone extension of a ContactPerson -->
  <xsl:template match="ContactPerson" mode="BestPhoneAltExtn">
    <xsl:choose>
      <xsl:when test="ContactTelephone[@TelephoneType='BP2']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='BP2']/Telecom/Extn"/>
      </xsl:when>
      <xsl:when test="ContactTelephone[@TelephoneType='HO2']/Telecom/TelecomNum!=''">
        <xsl:value-of select="ContactTelephone[@TelephoneType='HO2']/Telecom/Extn"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- Returns an APD impact point code that matches the passed CEF(EMS) impact point code. -->
  <xsl:template name="ConvertImpactPoint">
    <xsl:choose>
      <xsl:when test="ImpactAreaCode/@code=1">3</xsl:when>   <!-- Right Front         -> Passenger's Front Bumper -->
      <xsl:when test="ImpactAreaCode/@code=2">4</xsl:when>   <!-- Right Front Pillar  -> Passenger's Front Corner -->
      <xsl:when test="ImpactAreaCode/@code=3">6</xsl:when>   <!-- Right T-Bone        -> Passenger's Side -->
      <xsl:when test="ImpactAreaCode/@code=4">8</xsl:when>   <!-- Right Quarter Post  -> Passenger's Rear Corner -->
      <xsl:when test="ImpactAreaCode/@code=5">9</xsl:when>   <!-- Right Rear          -> Passenger's Rear Bumper -->
      <xsl:when test="ImpactAreaCode/@code=6">15</xsl:when>  <!-- Rear                -> Trunk -->
      <xsl:when test="ImpactAreaCode/@code=7">10</xsl:when>  <!-- Left Rear           -> Driver's Rear Bumper -->
      <xsl:when test="ImpactAreaCode/@code=8">11</xsl:when>  <!-- Left Quarter Post   -> Driver's Rear Corner -->
      <xsl:when test="ImpactAreaCode/@code=9">13</xsl:when>  <!-- Left T- Bone        -> Driver's Side -->
      <xsl:when test="ImpactAreaCode/@code=10">1</xsl:when>  <!-- Left Front Pillar   -> Driver's Front Corner -->
      <xsl:when test="ImpactAreaCode/@code=11">2</xsl:when>  <!-- Left Front          -> Driver's Front Bumper -->
      <xsl:when test="ImpactAreaCode/@code=12">17</xsl:when> <!-- Front               -> Hood -->
      <xsl:when test="ImpactAreaCode/@code=13">27</xsl:when> <!-- Rollover            -> Rollover -->
      <xsl:when test="ImpactAreaCode/@code=14">0</xsl:when>  <!-- Unknown             -> Unknown -->
      <xsl:when test="ImpactAreaCode/@code=15">28</xsl:when> <!-- Total Loss          -> Total Loss -->
      <xsl:when test="ImpactAreaCode/@code=16">0</xsl:when>  <!-- Non-Collision       -> Unknown -->
      <xsl:when test="ImpactAreaCode/@code=17">0</xsl:when>  <!-- Non-Collision       -> Unknown -->
      <xsl:when test="ImpactAreaCode/@code=18">0</xsl:when>  <!-- Non-Collision       -> Unknown -->
      <xsl:when test="ImpactAreaCode/@code=19">0</xsl:when>  <!-- All Over            -> Unknown -->
      <xsl:when test="ImpactAreaCode/@code=20">0</xsl:when>  <!-- Stripped            -> Unknown -->
      <xsl:when test="ImpactAreaCode/@code=21">18</xsl:when> <!-- Undercarriage       -> Undercarriage -->
      <xsl:when test="ImpactAreaCode/@code=22">0</xsl:when>  <!-- Total Burn          -> Unknown -->
      <xsl:when test="ImpactAreaCode/@code=23">20</xsl:when> <!-- Interior Burn       -> Interior -->
      <xsl:when test="ImpactAreaCode/@code=24">0</xsl:when>  <!-- Exterior Burn       -> Unknown -->
      <xsl:when test="ImpactAreaCode/@code=25">17</xsl:when> <!-- Hood                -> Hood -->
      <xsl:when test="ImpactAreaCode/@code=26">0</xsl:when>  <!-- Deck Lid            -> Unknown -->
      <xsl:when test="ImpactAreaCode/@code=27">16</xsl:when> <!-- Roof                -> Roof -->
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>

