<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">

<xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes" cdata-section-elements="ClaimMessage SenderName ClaimNumber VehicleMake VehicleModel"/>
<xsl:decimal-format NaN="0"/>

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[

   function formatNumberPadded(sVal, sPadChar, iLen){
      if (sPadChar == "") sPadChar = "0";
      if (sVal != ""){
         if (typeof(sVal) != "string")
            sVal = sVal.toString();
         
         var sFormat = charFill(sPadChar, iLen);
         return formatNumber(sVal, sFormat);
      } else {
      	var sFormat = charFill(sPadChar, iLen);
         return sFormat;
      }
   }

   function charFill(sChar, iLen){
      var sRet = "";
      if (isNaN(iLen)) iLen = 1;
      
      for (var i = iLen; i > 0; i--)
         sRet += sChar;
      
      return sRet;
   }
   
   function formatNumber(sVal, sFormat){
      if (sFormat != ""){
         if (typeof(sVal) != "string")
            sVal = sVal.toString();
            
         var sValLength = sVal.length;
         var sFormatLength = sFormat.length;
         var sRet = sFormat;
         
         if (sFormatLength > sValLength)
            sRet = sFormat.substr(0, sFormatLength - sValLength) + sVal;
         else
            sRet = sVal;
         
         return sRet;
         
      } else 
         return sVal;
   }
   
  ]]>
</msxsl:script>
  
<xsl:template match="/">
<PartnerTransfer Partner="CCAV">
  <Header>
    <SenderName><xsl:value-of select="/ClientDocument/Send/User/@Name"/></SenderName>
    <InsuranceCarrierID><xsl:value-of select="/ClientDocument/Claim/@InsuranceCompanyID"/></InsuranceCarrierID>
    <ClaimNumber><xsl:value-of select="/ClientDocument/Claim/@ClientClaimNumber"/></ClaimNumber>
    <LynxID><xsl:value-of select="/ClientDocument/Claim/@LynxID"/></LynxID>
    <VehicleNumber><xsl:value-of select="/ClientDocument/Vehicle/@number"/></VehicleNumber>
    <VehicleYear><xsl:value-of select="/ClientDocument/Vehicle/@year"/></VehicleYear>
    <VehicleMake><xsl:value-of select="/ClientDocument/Vehicle/@make"/></VehicleMake>
    <VehicleModel><xsl:value-of select="/ClientDocument/Vehicle/@model"/></VehicleModel>
  </Header>
  <ClaimMessage>
      <xsl:attribute name="DocumentID"><xsl:value-of select="/ClientDocument/Message/@id"/></xsl:attribute>
      <xsl:value-of select="/ClientDocument/Message"/>
  </ClaimMessage>
  <NumberOfDocuments><xsl:value-of select="count(/ClientDocument/Document)"/></NumberOfDocuments>
  <!-- transaction will have 1 or more <Document> blocks -->
  <xsl:for-each select="/ClientDocument/Document">
  <Document>
    <xsl:variable name="docID"><xsl:value-of select="@id"/></xsl:variable>
    <xsl:attribute name="DocumentID"><xsl:value-of select="@id"/></xsl:attribute>
    <xsl:attribute name="key">DocumentID</xsl:attribute>
    <CreateDateTime>
      <DateTime Date="$CreatedDate:Date$" Time="$CreatedDate:Time$" TimeZone="$CreatedDate:Zone$">
          <xsl:attribute name="Date"><xsl:value-of select="substring(@createDate, 1, 10)"/></xsl:attribute>
          <xsl:attribute name="Time"><xsl:value-of select="substring(@createDate, 11)"/></xsl:attribute>
          <xsl:attribute name="TimeZone">EST</xsl:attribute>
      </DateTime>
    </CreateDateTime>
    <xsl:variable name="docType">
        <xsl:choose>
            <xsl:when test="@documentTypeID = '3' or @documentTypeID = '10'">EstimatePI</xsl:when>
            <xsl:when test="@documentTypeID = '8'">DigitalImage</xsl:when>
            <xsl:otherwise>Document:<xsl:value-of select="@documentTypeName"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="seqNum">
        <xsl:choose>
            <xsl:when test="@documentTypeID = '3'">E01</xsl:when>
            <xsl:when test="@documentTypeID = '10'">
                <xsl:value-of select="concat('S', user:formatNumberPadded(string(@sequenceNumber), '0', 2))"/>
            </xsl:when>
            <xsl:when test="@documentTypeID = '8'">
                <xsl:for-each select="/ClientDocument/Document[@documentTypeID = '8']">
                    <xsl:if test="@id = $docID">
                        <xsl:value-of select="position()"/>
                    </xsl:if>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
    </xsl:variable>
    <DocumentType><xsl:value-of select="$docType"/></DocumentType>
    <SequenceNumber><xsl:value-of select="$seqNum"/></SequenceNumber>
    <FilePath>$DocumentPath$</FilePath>
  </Document>
  </xsl:for-each>
</PartnerTransfer>
</xsl:template>
</xsl:stylesheet>