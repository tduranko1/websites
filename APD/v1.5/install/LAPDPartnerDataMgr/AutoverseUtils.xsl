<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cefutil="http://cef.com/utilities"
                xmlns:user="urn:the-xml-files:xslt">
  <!-- Scripts required below. -->
<!--  <msxsl:script language="JScript" implements-prefix="cefutil">

    // Returns the current year.
    function GetYear()
    {
        var date = new Date();
        return date.getFullYear();
    }
    
    //Returns a string with quotes replaced with double quotes
    function CleanDataQuotes(str){
        return str.replace(/[\']/g, "''");
    }

  </msxsl:script>-->
  <msxsl:script language="JScript" implements-prefix="user">

    //Returns a string with quotes replaced with double quotes
    function CleanDataQuotes(str){
        return str.replace(/[\']/g, "''");
    }

  </msxsl:script>
  <!-- Caller Rep -->
  <xsl:template name="CarrierRep">
    <xsl:attribute name="CarrierRepLogin"/>
    <xsl:attribute name="CarrierRepAddress1"/>
    <xsl:attribute name="CarrierRepAddress2"/>
    <xsl:attribute name="CarrierRepAddressCity"/>
    <xsl:attribute name="CarrierRepAddressState"/>
    <xsl:attribute name="CarrierRepAddressZip"/>
    <xsl:attribute name="CarrierRepBestPhoneCode"/>
    <xsl:attribute name="CarrierRepBestTimeToCall"/>
    <xsl:attribute name="CarrierRepNameFirst"/>
    <xsl:attribute name="CarrierRepNameLast"/>
    <xsl:attribute name="CarrierRepNameTitle"/>
    <xsl:attribute name="CarrierRepPhoneAlternate"/>
    <xsl:attribute name="CarrierRepPhoneAlternateExt"/>
    <xsl:attribute name="CarrierRepPhoneDay"/>
    <xsl:attribute name="CarrierRepPhoneDayExt"/>
    <xsl:attribute name="CarrierRepPhoneNight"/>
    <xsl:attribute name="CarrierRepPhoneNightExt"/>
    <xsl:attribute name="CarrierRepEmailAddress"/>

    <xsl:choose>
      <!-- we will automatically add the adjuster information if we have their email address and full phone number and their last name -->
      <xsl:when test="(string-length(//CLM_EA)&gt;0) and (string-length(//CLM_CT_PH) = 10) and (string-length(//CLM_CT_LN)&gt;0)">
        <xsl:attribute name="CarrierRepLogin"><xsl:value-of select="user:CleanDataQuotes(string(//CLM_EA))"/></xsl:attribute>
        <xsl:attribute name="CarrierRepAddress1"><xsl:value-of select="//CLM_ADDR1"/></xsl:attribute>
        <xsl:attribute name="CarrierRepAddress2"><xsl:value-of select="//CLM_ADDR2"/></xsl:attribute>
        <xsl:attribute name="CarrierRepAddressCity"><xsl:value-of select="//CLM_CITY"/></xsl:attribute>
        <xsl:attribute name="CarrierRepAddressState"><xsl:value-of select="//CLM_ST"/></xsl:attribute>
        <xsl:attribute name="CarrierRepAddressZip"><xsl:value-of select="//CLM_ZIP"/></xsl:attribute>
        <xsl:attribute name="CarrierRepBestPhoneCode"/>
        <xsl:attribute name="CarrierRepBestTimeToCall"/>
        <xsl:attribute name="CarrierRepNameFirst"><xsl:value-of select="//CLM_CT_FN"/></xsl:attribute>
        <xsl:attribute name="CarrierRepNameLast"><xsl:value-of select="//CLM_CT_LN"/></xsl:attribute>
        <xsl:attribute name="CarrierRepNameTitle"><xsl:value-of select="//CLM_CT_TITLE"/></xsl:attribute>
        <xsl:attribute name="CarrierRepPhoneAlternate"/>
        <xsl:attribute name="CarrierRepPhoneAlternateExt"/>
        <xsl:attribute name="CarrierRepPhoneDay"><xsl:value-of select="//CLM_CT_PH"/></xsl:attribute>
        <xsl:attribute name="CarrierRepPhoneDayExtn"><xsl:value-of select="//CLM_CT_PHX"/></xsl:attribute>
        <xsl:attribute name="CarrierRepPhoneNight"/>
        <xsl:attribute name="CarrierRepPhoneNightExt"/>
        <xsl:attribute name="CarrierRepEmailAddress"><xsl:value-of select="//CLM_EA"/></xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="CarrierRepLogin"/>
        <xsl:attribute name="CarrierRepAddress1"/>
        <xsl:attribute name="CarrierRepAddress2"/>
        <xsl:attribute name="CarrierRepAddressCity"/>
        <xsl:attribute name="CarrierRepAddressState"/>
        <xsl:attribute name="CarrierRepAddressZip"/>
        <xsl:attribute name="CarrierRepBestPhoneCode"/>
        <xsl:attribute name="CarrierRepBestTimeToCall"/>
        <xsl:attribute name="CarrierRepNameFirst"/>
        <xsl:attribute name="CarrierRepNameLast"/>
        <xsl:attribute name="CarrierRepNameTitle"/>
        <xsl:attribute name="CarrierRepPhoneAlternate"/>
        <xsl:attribute name="CarrierRepPhoneAlternateExt"/>
        <xsl:attribute name="CarrierRepPhoneDay"/>
        <xsl:attribute name="CarrierRepPhoneDayExt"/>
        <xsl:attribute name="CarrierRepPhoneNight"/>
        <xsl:attribute name="CarrierRepPhoneNightExt"/>
        <xsl:attribute name="CarrierRepEmailAddress"/>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>
  <!-- Caller Info -->
  <xsl:template name="Caller">
    <xsl:attribute name="CallerAddress1"/>
    <xsl:attribute name="CallerAddress2"/>
    <xsl:attribute name="CallerAddressCity"/>
    <xsl:attribute name="CallerAddressState"/>
    <xsl:attribute name="CallerAddressZip"/>
    <xsl:attribute name="CallerBestPhoneCode"/>
    <xsl:attribute name="CallerBestTimeToCall"/>
    <xsl:attribute name="CallerNameFirst"/>
    <xsl:attribute name="CallerNameLast"/>
    <xsl:attribute name="CallerNameTitle"/>
    <xsl:attribute name="CallerPhoneAlternate"/>
    <xsl:attribute name="CallerPhoneAlternateExt"/>
    <xsl:attribute name="CallerPhoneDay"/>
    <xsl:attribute name="CallerPhoneDayExt"/>
    <xsl:attribute name="CallerPhoneNight"/>
    <xsl:attribute name="CallerPhoneNightExt"/>
    <xsl:attribute name="CallerEmailAddress"/>
    <xsl:attribute name="CallerRelationToInsuredID">14</xsl:attribute>
  </xsl:template>
  <!-- Contact Person -->
  <xsl:template name="ContactPerson">
    <xsl:attribute name="ContactAddress1"/>
    <xsl:attribute name="ContactAddress2"/>
    <xsl:attribute name="ContactAddressCity"/>
    <xsl:attribute name="ContactAddressState"/>
    <xsl:attribute name="ContactAddressZip"/>
    <xsl:attribute name="ContactBestPhoneCode"/>
    <xsl:attribute name="ContactBestTimeToCall"/>
    <xsl:attribute name="ContactNameFirst"/>
    <xsl:attribute name="ContactNameLast"/>
    <xsl:attribute name="ContactNameTitle"/>
    <xsl:attribute name="ContactPhoneAlternate"/>
    <xsl:attribute name="ContactPhoneAlternateExt"/>
    <xsl:attribute name="ContactPhoneDay"/>
    <xsl:attribute name="ContactPhoneDayExt"/>
    <xsl:attribute name="ContactPhoneNight"/>
    <xsl:attribute name="ContactPhoneNightExt"/>
    <xsl:attribute name="ContactEmailAddress"/>
  </xsl:template>
  <!-- Loss Info -->
  <xsl:template name="LossInfo">
    <xsl:attribute name="LossDate">
      <xsl:if test="string-length(//AD1/LOSS_DATE)!=0">
        <xsl:value-of select="substring(//AD1/LOSS_DATE,1,10)"/>
      </xsl:if>
    </xsl:attribute>
    <xsl:attribute name="LossAddress1"/>
    <xsl:attribute name="LossAddress2"/>
    <xsl:attribute name="LossAddressCity"/>
    <xsl:attribute name="LossAddressState"/>
    <xsl:attribute name="LossAddressZip"/>
    <xsl:attribute name="LossDescription">
      <xsl:value-of select="//AD1/LOSS_DESC"/>
      <xsl:text/>
      <xsl:value-of select="//AD1/LOSS_MEMO"/>
    </xsl:attribute>
    <xsl:attribute name="LossTime"/>
    <xsl:attribute name="LossTypeLevel1ID"/>
    <xsl:attribute name="LossTypeLevel2ID"/>
    <xsl:attribute name="LossTypeLevel3ID"/>
  </xsl:template>
  <!-- Vehicle Info -->
  <xsl:template name="VehicleInfo">
    <xsl:attribute name="ExposureCD">
      <xsl:choose>
        <xsl:when test="//AD1/LOSS_CAT='C'">1</xsl:when>
        <xsl:when test="//AD1/LOSS_CAT='M'">1</xsl:when>
        <xsl:when test="//AD1/LOSS_CAT='O'">1</xsl:when>
        <xsl:when test="//AD1/LOSS_CAT='U'">1</xsl:when>
        <xsl:when test="//AD1/LOSS_CAT='P'">3</xsl:when>
        <xsl:when test="//AD1/LOSS_CAT='L'">3</xsl:when>
        <xsl:otherwise/>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="VehicleYear">
      <xsl:value-of select="//VEH/V_MODEL_YR"/>
    </xsl:attribute>
    <xsl:attribute name="Make">
      <xsl:value-of select="//VEH/V_MAKECODE"/>
    </xsl:attribute>
    <xsl:attribute name="Model">
      <xsl:value-of select="//VEH/V_MODEL"/>
    </xsl:attribute>
    <xsl:attribute name="BodyStyle">
      <xsl:value-of select="//VEH/V_BSTYLE"/>
    </xsl:attribute>
    <xsl:attribute name="Color">
      <xsl:value-of select="//VEH/V_COLOR"/>
    </xsl:attribute>
    <xsl:attribute name="Mileage">
      <xsl:value-of select="//VEH/V_MILEAGE"/>
    </xsl:attribute>
    <xsl:attribute name="LicensePlateNumber">
      <xsl:value-of select="//VEH/PLATE_NO"/>
    </xsl:attribute>
    <xsl:attribute name="LicensePlateState">
      <xsl:value-of select="//VEH/PLATE_ST"/>
    </xsl:attribute>
    <xsl:attribute name="Vin">
      <xsl:value-of select="//VEH/V_VIN"/>
    </xsl:attribute>
  </xsl:template>
  <!-- Vehicle Contact Person -->
  <xsl:template name="VehicleContact">
    <xsl:attribute name="ContactAddress1"/>
    <xsl:attribute name="ContactAddress2"/>
    <xsl:attribute name="ContactAddressCity"/>
    <xsl:attribute name="ContactAddressState"/>
    <xsl:attribute name="ContactAddressZip"/>
    <xsl:attribute name="ContactBestPhoneCode"/>
    <xsl:attribute name="ContactBestTimeToCall"/>
    <xsl:attribute name="ContactNameFirst"/>
    <xsl:attribute name="ContactNameLast"/>
    <xsl:attribute name="ContactNameTitle"/>
    <xsl:attribute name="ContactPhoneAlternate"/>
    <xsl:attribute name="ContactPhoneAlternateExt"/>
    <xsl:attribute name="ContactPhoneDay"/>
    <xsl:attribute name="ContactPhoneDayExt"/>
    <xsl:attribute name="ContactPhoneNight"/>
    <xsl:attribute name="ContactPhoneNightExt"/>
    <xsl:attribute name="ContactEmailAddress"/>
  </xsl:template>
  <xsl:template name="VehicleLocation">
    <xsl:attribute name="LocationName">
      <xsl:value-of select="//LOC_NM"/>
    </xsl:attribute>
    <xsl:attribute name="LocationAddress1">
      <xsl:value-of select="//LOC_ADDR1"/>
    </xsl:attribute>
    <xsl:attribute name="LocationAddress2">
      <xsl:value-of select="//LOC_ADDR2"/>
    </xsl:attribute>
    <xsl:attribute name="LocationAddressCity">
      <xsl:value-of select="//LOC_CITY"/>
    </xsl:attribute>
    <xsl:attribute name="LocationAddressState">
      <xsl:value-of select="//LOC_ST"/>
    </xsl:attribute>
    <xsl:attribute name="LocationAddressZip">
      <xsl:value-of select="//LOC_ZIP"/>
    </xsl:attribute>
    <xsl:attribute name="LocationPhone">
      <xsl:value-of select="//LOC_PHONE"/>
    </xsl:attribute>
  </xsl:template>
  <xsl:template name="InvolvedInsured">
    <xsl:param name="vInvolvedTypeOwner"/>
    <xsl:element name="Involved">
      <!-- Insured is always assiged to vehicle 1 -->
      <xsl:attribute name="VehicleNumber">1</xsl:attribute>
      <xsl:attribute name="InvolvedTypeInsured">1</xsl:attribute>
      <xsl:attribute name="InvolvedTypeClaimant">0</xsl:attribute>
      <xsl:attribute name="InvolvedTypeOwner">
        <xsl:value-of select="$vInvolvedTypeOwner"/>
      </xsl:attribute>
      <xsl:attribute name="Address1">
        <xsl:value-of select="//INSD_ADDR1"/>
      </xsl:attribute>
      <xsl:attribute name="Address2">
        <xsl:value-of select="//INSD_ADDR2"/>
      </xsl:attribute>
      <xsl:attribute name="AddressCity">
        <xsl:value-of select="//INSD_CITY"/>
      </xsl:attribute>
      <xsl:attribute name="AddressState">
        <xsl:value-of select="//INSD_ST"/>
      </xsl:attribute>
      <xsl:attribute name="AddressZip">
        <xsl:value-of select="//INSD_ZIP"/>
      </xsl:attribute>
      <xsl:attribute name="NameFirst">
        <xsl:if test="//INSD_FN=''">{Not Supplied}</xsl:if>
        <xsl:if test="//INSD_FN!=''">
          <xsl:value-of select="//INSD_FN"/>
        </xsl:if>
      </xsl:attribute>
      <xsl:attribute name="NameLast">
        <xsl:if test="//INSD_LN=''">{Not Supplied}</xsl:if>
        <xsl:if test="//INSD_LN!=''">
          <xsl:value-of select="//INSD_LN"/>
        </xsl:if>
      </xsl:attribute>
      <xsl:attribute name="NameTitle">
        <xsl:call-template name="TranslateSocialTitle">
          <xsl:with-param name="Title">
            <xsl:value-of select="//INSD_TITLE"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="PhoneAlternate">
        <xsl:value-of select="//INSD_PH2"/>
      </xsl:attribute>
      <xsl:attribute name="PhoneAlternateExt">
        <xsl:value-of select="//INSD_PH2X"/>
      </xsl:attribute>
      <xsl:attribute name="PhoneDay">
        <xsl:value-of select="//INSD_PH1"/>
      </xsl:attribute>
      <xsl:attribute name="PhoneDayExt">
        <xsl:value-of select="//INSD_PH1X"/>
      </xsl:attribute>
      <xsl:attribute name="Phone"/>
      <xsl:attribute name="PhoneExt"/>
      <xsl:attribute name="PhoneNight"/>
      <xsl:attribute name="PhoneNightExt"/>
      <xsl:attribute name="EmailAddress">
        <xsl:value-of select="//INSD_EA"/>
      </xsl:attribute>
      <xsl:attribute name="BusinessName">
        <xsl:value-of select="//INSD_CO_NM"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:template>
  <xsl:template name="InvolvedOwner">
    <xsl:param name="vInvolvedTypeInsured"/>
    <xsl:element name="Involved">
      <xsl:attribute name="VehicleNumber">
            <xsl:choose>
              <!-- Third party -->
              <xsl:when test="//LOSS_CAT='P' or //LOSS_CAT='L'">2</xsl:when>
              <!-- First party -->
              <xsl:when test="//LOSS_CAT='C' or //LOSS_CAT='M' or //LOSS_CAT='O' or //LOSS_CAT='U'">1</xsl:when>
              <xsl:otherwise>1</xsl:otherwise>
          </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="InvolvedTypeInsured"><xsl:value-of select="$vInvolvedTypeInsured"/></xsl:attribute>
      <xsl:attribute name="InvolvedTypeClaimant">0</xsl:attribute>
      <xsl:attribute name="InvolvedTypeOwner">1</xsl:attribute>
      <xsl:attribute name="Address1">
        <xsl:value-of select="//OWNR_ADDR1"/>
      </xsl:attribute>
      <xsl:attribute name="Address2">
        <xsl:value-of select="//OWNR_ADDR2"/>
      </xsl:attribute>
      <xsl:attribute name="AddressCity">
        <xsl:value-of select="//OWNR_CITY"/>
      </xsl:attribute>
      <xsl:attribute name="AddressState">
        <xsl:value-of select="//OWNR_ST"/>
      </xsl:attribute>
      <xsl:attribute name="AddressZip">
        <xsl:value-of select="//OWNR_ZIP"/>
      </xsl:attribute>
      <xsl:attribute name="NameFirst">
        <xsl:value-of select="//OWNR_FN"/>
      </xsl:attribute>
      <xsl:attribute name="NameLast">
        <xsl:value-of select="//OWNR_LN"/>
      </xsl:attribute>
      <xsl:attribute name="NameTitle">
        <xsl:call-template name="TranslateSocialTitle">
          <xsl:with-param name="Title">
            <xsl:value-of select="//OWNR_TITLE"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="PhoneAlternate">
        <xsl:value-of select="//OWNR_PH2"/>
      </xsl:attribute>
      <xsl:attribute name="PhoneAlternateExt">
        <xsl:value-of select="//OWNR_PH2X"/>
      </xsl:attribute>
      <xsl:attribute name="PhoneDay">
        <xsl:value-of select="//OWNR_PH1"/>
      </xsl:attribute>
      <xsl:attribute name="PhoneDayExt">
        <xsl:value-of select="//OWNR_PH1X"/>
      </xsl:attribute>
      <xsl:attribute name="Phone"/>
      <xsl:attribute name="PhoneExt"/>
      <xsl:attribute name="PhoneNight"/>
      <xsl:attribute name="PhoneNightExt"/>
      <xsl:attribute name="EmailAddress">
        <xsl:value-of select="//OWNR_EA"/>
      </xsl:attribute>
      <xsl:attribute name="BusinessName">
        <xsl:value-of select="//OWNR_CO_NM"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:template>
  <xsl:template name="InvolvedClaimant">
    <xsl:element name="Involved">
      <xsl:attribute name="VehicleNumber">
            <xsl:choose>
              <!-- Third party -->
              <xsl:when test="//LOSS_CAT='P' or //LOSS_CAT='L'">2</xsl:when>
              <!-- First party -->
              <xsl:when test="//LOSS_CAT='C' or //LOSS_CAT='M' or //LOSS_CAT='O' or //LOSS_CAT='U'">1</xsl:when>
              <xsl:otherwise>1</xsl:otherwise>
          </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="InvolvedTypeInsured">0</xsl:attribute>
      <xsl:attribute name="InvolvedTypeClaimant">1</xsl:attribute>
      <xsl:attribute name="InvolvedTypeOwner">0</xsl:attribute>
      <xsl:attribute name="Address1">
        <xsl:value-of select="//CLMT_ADDR1"/>
      </xsl:attribute>
      <xsl:attribute name="Address2">
        <xsl:value-of select="//CLMT_ADDR2"/>
      </xsl:attribute>
      <xsl:attribute name="AddressCity">
        <xsl:value-of select="//CLMT_CITY"/>
      </xsl:attribute>
      <xsl:attribute name="AddressState">
        <xsl:value-of select="//CLMT_ST"/>
      </xsl:attribute>
      <xsl:attribute name="AddressZip">
        <xsl:value-of select="//CLMT_ZIP"/>
      </xsl:attribute>
      <xsl:attribute name="NameFirst">
        <xsl:value-of select="//CLMT_FN"/>
      </xsl:attribute>
      <xsl:attribute name="NameLast">
        <xsl:value-of select="//CLMT_LN"/>
      </xsl:attribute>
      <xsl:attribute name="NameTitle">
        <xsl:call-template name="TranslateSocialTitle">
          <xsl:with-param name="Title">
            <xsl:value-of select="//CLMT_TITLE"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="PhoneAlternate">
        <xsl:value-of select="//CLMT_PH2"/>
      </xsl:attribute>
      <xsl:attribute name="PhoneAlternateExt">
        <xsl:value-of select="//CLMT_PH2X"/>
      </xsl:attribute>
      <xsl:attribute name="PhoneDay">
        <xsl:value-of select="//CLMT_PH1"/>
      </xsl:attribute>
      <xsl:attribute name="PhoneDayExt">
        <xsl:value-of select="//CLMT_PH1X"/>
      </xsl:attribute>
      <xsl:attribute name="Phone"/>
      <xsl:attribute name="PhoneExt"/>
      <xsl:attribute name="PhoneNight"/>
      <xsl:attribute name="PhoneNightExt"/>
      <xsl:attribute name="EmailAddress">
        <xsl:value-of select="//CLMT_EA"/>
      </xsl:attribute>
      <xsl:attribute name="BusinessName">
        <xsl:value-of select="//CLMT_CO_NM"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:template>
  <xsl:template name="TranslateSocialTitle">
    <xsl:param name="Title"/>
    <xsl:choose>
      <xsl:when test="$Title='1'">Dr.</xsl:when>
      <xsl:when test="$Title='2'">Miss</xsl:when>
      <xsl:when test="$Title='3'">Mr.</xsl:when>
      <xsl:when test="$Title='4'">Mrs.</xsl:when>
      <xsl:when test="$Title='5'">Ms.</xsl:when>
      <xsl:when test="$Title='6'">Rev.</xsl:when>
      <xsl:otherwise>
        <xsl:text/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="TranslateImpactLocation">
    <xsl:param name="ImpactLoc"/>
    <xsl:choose>
      <xsl:when test="$ImpactLoc='1'">3</xsl:when>
      <xsl:when test="$ImpactLoc='2'">5</xsl:when>
      <xsl:when test="$ImpactLoc='3'">6</xsl:when>
      <xsl:when test="$ImpactLoc='4'">7</xsl:when>
      <xsl:when test="$ImpactLoc='5'">9</xsl:when>
      <xsl:when test="$ImpactLoc='6'">38</xsl:when>
      <xsl:when test="$ImpactLoc='7'">10</xsl:when>
      <xsl:when test="$ImpactLoc='8'">12</xsl:when>
      <xsl:when test="$ImpactLoc='9'">13</xsl:when>
      <xsl:when test="$ImpactLoc='10'">14</xsl:when>
      <xsl:when test="$ImpactLoc='11'">2</xsl:when>
      <xsl:when test="$ImpactLoc='12'">31</xsl:when>
      <xsl:when test="$ImpactLoc='13'">27</xsl:when>
      <xsl:when test="$ImpactLoc='14'">0</xsl:when>
      <xsl:when test="$ImpactLoc='15'">TL</xsl:when>
      <xsl:when test="$ImpactLoc='16'">30</xsl:when>
      <xsl:when test="$ImpactLoc='25'">17</xsl:when>
      <xsl:when test="$ImpactLoc='26'">15</xsl:when>
      <xsl:when test="$ImpactLoc='27'">16</xsl:when>
      <xsl:when test="$ImpactLoc='28'">18</xsl:when>
      <xsl:otherwise/>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
