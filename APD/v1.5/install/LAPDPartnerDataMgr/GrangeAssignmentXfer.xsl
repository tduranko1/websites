<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt">

  <xsl:import href="CEFUtilities2.xsl"/>

  <xsl:output method="xml" indent="no" encoding="UTF-8" omit-xml-declaration="yes"/>

  <!-- ================================================== -->
  <!-- Converts a Grange Assignment GrangeTransaction  -->
  <!-- Into an XML format compatible with                 -->
  <!-- LAPDECADAccessor.CWebAssignment.ApdLoadAssignment  -->
  <!-- ================================================== -->

  <xsl:template match="/">

    <xsl:element name="WebAssignment">

      <!-- For add new user params -->
      <xsl:attribute name="ApplicationCD">GRGO</xsl:attribute>
      <xsl:attribute name="ApplicationID">5</xsl:attribute>

      <!-- ================================================== -->
      <!-- CLAIM ELEMENT                                      -->
      <!-- ================================================== -->

      <!-- For uspFNOLLoadClaim -->
      <xsl:element name="Claim">

        <xsl:attribute name="AssignmentAtSelectionFlag">0</xsl:attribute>
        <xsl:attribute name="AssignmentDescription">Grange Assignment</xsl:attribute>
        <xsl:attribute name="SourceApplicationCD">GRGO</xsl:attribute>
       
	<!--
	 <xsl:attribute name="SourceApplicationPassThruData">
          <xsl:value-of select="//@TaskID"/>^<xsl:value-of select="//TaskFields"/>
        </xsl:attribute>
	-->
	
	
	  <xsl:attribute name="NewClaimFlag">
		<xsl:choose>
		    <xsl:when test="//ActionCode='New'">1</xsl:when>
		    <xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>	
	  </xsl:attribute>

        <!-- LynxID stuffed from COM -->
       
	<xsl:attribute name="CarrierRepLogin"/>
	
        <xsl:apply-templates select="//Claim/Adjuster/Person" mode="ContactParams">
          <xsl:with-param name="Title">CarrierRep</xsl:with-param>
        </xsl:apply-templates>
	
	 <!--
        <xsl:attribute name="CarrierRepLogin">
          <xsl:value-of select="//CarrierRep/GrangeUniqueId"/>
        </xsl:attribute>
        -->

        <xsl:apply-templates select="//Assignment/AssignedBy/ContactPerson" mode="ContactParams">
          <xsl:with-param name="Title">Caller</xsl:with-param>
        </xsl:apply-templates>

        <xsl:attribute name="CallerRelationToInsuredID">14</xsl:attribute>

        <xsl:attribute name="CollisionDeductibleAmt">	    
    		<xsl:if test="//Claim/LossCategoryCode/@code='C'">  	
		    <xsl:value-of select="//Policy/DeductibleAmt"/>
		</xsl:if>                       
        </xsl:attribute>

        <xsl:attribute name="ComprehensiveDeductibleAmt">
            <xsl:if test="//Claim/LossCategoryCode/@code='M'">  	
		  <xsl:value-of select="//Policy/DeductibleAmt"/>
	     </xsl:if>         
	  </xsl:attribute>

        <xsl:apply-templates select="//ClaimData/ContactPerson" mode="ContactParams">
          <xsl:with-param name="Title">Contact</xsl:with-param>
        </xsl:apply-templates>

        <xsl:attribute name="CoverageClaimNumber">
          <xsl:value-of select="//Claim/ClaimNumber"/>
        </xsl:attribute>

        <xsl:attribute name="DataSource">Grange Assignment</xsl:attribute>

        <xsl:attribute name="InsuranceCompanyID">193</xsl:attribute>		<!-- TODO: change this once we have the real ID -->

        <xsl:attribute name="LossDate">
          <xsl:apply-templates select="//Loss/LossDateTime/DlDate/DateTime" mode="Date"/>
        </xsl:attribute>

         <xsl:attribute name="LossAddressState"><xsl:value-of select="//Claim/LossState"/></xsl:attribute>

         <!--
        <xsl:apply-templates select="//Loss/LossLocation/Address">
          <xsl:with-param name="Title">Loss</xsl:with-param>
        </xsl:apply-templates>
         -->
        <xsl:attribute name="LossDescription">
          <xsl:value-of select="//Loss/LossDescription"/>
          <xsl:text>  </xsl:text>
          <xsl:value-of select="//Loss/LossMemo"/>
        </xsl:attribute>
        <xsl:attribute name="LossTime">
          <xsl:apply-templates select="//Loss/LossDateTime/DlDate/DateTime" mode="Time"/>
        </xsl:attribute>
        
        <xsl:attribute name="LossTypeLevel1ID">
          <xsl:value-of select="//Loss/LossTypeLevel1ID"/>
        </xsl:attribute>
        <xsl:attribute name="LossTypeLevel2ID">
          <xsl:value-of select="//Loss/LossTypeLevel2ID"/>
        </xsl:attribute>
        <xsl:attribute name="LossTypeLevel3ID">
          <xsl:value-of select="//Loss/LossTypeLevel3ID"/>
        </xsl:attribute>

        <xsl:attribute name="NoticeDate">
          <xsl:apply-templates select="//Claim/ReportedDateTime/Date" mode="Date"/>
        </xsl:attribute>
        <xsl:attribute name="NoticeMethodID">5</xsl:attribute>

        <xsl:attribute name="PoliceDepartmentName">
          <xsl:value-of select="//ClaimData/PoliceDepartmentName"/>
        </xsl:attribute>

        <xsl:attribute name="PolicyNumber">
          <xsl:value-of select="//Policy/PolicyNumber"/>
        </xsl:attribute>

        <xsl:attribute name="Remarks">
          <xsl:value-of select="//ClaimData/Remarks"/>
        </xsl:attribute>

        <xsl:attribute name="RentalCoverageFlag">
          <xsl:value-of select="//ClaimData/Rental/RentalCoverageFlag"/>
        </xsl:attribute>
        <xsl:attribute name="RentalDays">
          <xsl:value-of select="//ClaimData/Rental/RentalDays"/>
        </xsl:attribute>

	   <!-- Grange currently passes both daily and max rental amounts in the max amount field in the following format:  DAILY.MAX-->
	   <!-- We will split these into separate fields at the dot.  If the field contains no dot, the entire value will be placed in the max amount field.  -->
	
	   <xsl:variable name="RentalDayAmount">			
		  <xsl:if test="contains(//Policy/RentalAmt, '.')"><xsl:value-of select="substring-before(//Policy/RentalAmt, '.')"/></xsl:if>
	   </xsl:variable>
	
	    <xsl:variable name="RentalMaxAmount">
			<xsl:choose>
        			<xsl:when test="contains(//Policy/RentalAmt, '.')"><xsl:value-of select="substring-after(//Policy/RentalAmt, '.')"/></xsl:when>
                    <xsl:otherwise><xsl:value-of select="//Policy/RentalAmt"/></xsl:otherwise>
      		</xsl:choose>
	    </xsl:variable>

        <xsl:attribute name="RentalDayAmount">
          <xsl:value-of select="$RentalDayAmount"/>
        </xsl:attribute>
        <xsl:attribute name="RentalMaxAmount">
          <xsl:value-of select="$RentalMaxAmount"/>
        </xsl:attribute>
        <xsl:attribute name="RentalDaysAuthorized">
          <xsl:value-of select="//ClaimData/Rental/RentalDaysAuthorized"/>
        </xsl:attribute>
        <xsl:attribute name="RentalInstructions">
          <xsl:value-of select="//ClaimData/Rental/RentalInstructions"/>
        </xsl:attribute>

        <xsl:attribute name="RoadLocationID">
          <xsl:value-of select="//ClaimData/RoadLocationID"/>
        </xsl:attribute>
        <xsl:attribute name="RoadTypeID">
          <xsl:value-of select="//ClaimData/RoadTypeID"/>
        </xsl:attribute>

        <xsl:attribute name="TimeStarted">
          <xsl:apply-templates select="//Assignment/AssignmentDate/DateTime" mode="Date"/>
          <xsl:text> </xsl:text>
          <xsl:apply-templates select="//Assignment/AssignmentDate/DateTime" mode="Time"/>
        </xsl:attribute>
        <xsl:attribute name="TimeFinished">
          <xsl:apply-templates select="//Assignment/AssignmentDate/DateTime" mode="Date"/>
          <xsl:text> </xsl:text>
          <xsl:apply-templates select="//Assignment/AssignmentDate/DateTime" mode="Time"/>
        </xsl:attribute>

        <xsl:attribute name="TripPurpose">
          <xsl:value-of select="//ClaimData/TripPurpose"/>
        </xsl:attribute>
        <xsl:attribute name="WeatherConditionID">
          <xsl:value-of select="//ClaimData/WeatherConditionID"/>
        </xsl:attribute>

        <!-- ================================================== -->
        <!-- VEHICLE ELEMENT                                    -->
        <!-- ================================================== -->

        <!-- Make ident strings for Insured, Claimant and Owner. -->
        <xsl:variable name="Insured">	   
          <xsl:value-of select="//Party[PartyType/@code= 'P' and (PartyRoleType/@code = 'VI' or PartyRoleType/@code = 'IN')]/Person/FirstName"/>
          <xsl:value-of select="//Party[PartyType/@code='P' and (PartyRoleType/@code = 'VI' or PartyRoleType/@code = 'IN')]/Person/LastName"/>
	        <xsl:value-of select="//Party[PartyType/@code='C' and (PartyRoleType/@code = 'VI' or PartyRoleType/@code = 'IN')]/Person/Customer/CustomerName"/>          
        </xsl:variable>

        <xsl:variable name="Claimant">
	        <xsl:value-of select="//Party[PartyType/@code= 'P' and (PartyRoleType/@code != 'VI' and PartyRoleType/@code != 'VO' and PartyRoleType/@code != 'IN')]/Person/FirstName"/>
          <xsl:value-of select="//Party[PartyType/@code='P' and (PartyRoleType/@code != 'VI' and PartyRoleType/@code != 'VO' and PartyRoleType/@code != 'IN')]/Person/LastName"/>
	        <xsl:value-of select="//Party[PartyType/@code='C' and (PartyRoleType/@code != 'VI' and PartyRoleType/@code != 'VO' and PartyRoleType/@code != 'IN')]/Person/Customer/CustomerName"/>	
	      </xsl:variable>

        <xsl:variable name="Owner">
	        <xsl:value-of select="//Party[PartyType/@code= 'P' and (PartyRoleType/@code = 'VO' or PartyRoleType/@code = 'VI')]/Person/FirstName"/>
          <xsl:value-of select="//Party[PartyType/@code='P' and (PartyRoleType/@code = 'VO' or PartyRoleType/@code = 'VI')]/Person/LastName"/>
	        <xsl:value-of select="//Party[PartyType/@code='C' and (PartyRoleType/@code= 'VO' or PartyRoleType/@code = 'VI')]/Person/Customer/CustomerName"/>    
       </xsl:variable>

        <!-- Match up duplicate Insured, Claimant and Owner -->
        <xsl:variable name="InsuredIsClaimant">
          <xsl:value-of select="number($Insured=$Claimant)"/>
        </xsl:variable>
        <xsl:variable name="InsuredIsOwner">
          <xsl:value-of select="number($Insured=$Owner)"/>
        </xsl:variable>
        <xsl:variable name="ClaimantIsOwner">
          <xsl:value-of select="number($Claimant=$Owner)"/>
        </xsl:variable>

	   <xsl:variable name="VehicleParty">
	       <xsl:choose>
    			<xsl:when test="//Party/PartyRoleType[@code='VI' or @code='IN']/@code != ''">1</xsl:when>
               <xsl:otherwise>3</xsl:otherwise>
  		</xsl:choose> 
	   </xsl:variable>	   
	  
        <!-- Dummy Vehicle for 1st party vehicle on 3rd party claims -->
        <xsl:if test="$VehicleParty=3">

          <xsl:element name="Vehicle">

            <xsl:attribute name="VehicleNumber">1</xsl:attribute>

            <!-- Add an involved element as needed for Insured -->
            <xsl:if test="($Insured!='')">
              <xsl:for-each select="//Party[PartyRoleType/@code='VI' or PartyRoleType/@code='IN']">
                <xsl:call-template name="Involved">
                  <xsl:with-param name="VehNum">1</xsl:with-param>
                  <xsl:with-param name="Insured">1</xsl:with-param>
                  <xsl:with-param name="Claimant"><xsl:value-of select="$InsuredIsClaimant"/></xsl:with-param>
                  <xsl:with-param name="Owner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
                </xsl:call-template>
              </xsl:for-each>
            </xsl:if>

          </xsl:element>

        </xsl:if>

        <!-- This is where the passed Vehicle information goes.
            Vehicle 1 for first party claims.
            Vehicle 2 for third party claims. -->
        <xsl:element name="Vehicle">

          <!-- LynxID stuffed from COM -->

          <xsl:choose>
            <xsl:when test="$VehicleParty=1">
              <xsl:attribute name="VehicleNumber">1</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="VehicleNumber">2</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          
          <xsl:attribute name="AirBagDriverFront">
            <xsl:value-of select="//Vehicle/AirBagDriverFront"/>
          </xsl:attribute>
          <xsl:attribute name="AirBagDriverSide">
            <xsl:value-of select="//Vehicle/AirBagDriverSide"/>
          </xsl:attribute>
          <xsl:attribute name="AirBagHeadliner">
            <xsl:value-of select="//Vehicle/AirBagHeadliner"/>
          </xsl:attribute>
          <xsl:attribute name="AirBagPassengerFront">
            <xsl:value-of select="//Vehicle/AirBagPassengerFront"/>
          </xsl:attribute>
          <xsl:attribute name="AirBagPassengerSide">
            <xsl:value-of select="//Vehicle/AirBagPassengerSide"/>
          </xsl:attribute>

          <!-- AssignmentTypeID - 1=Adverse Subro Review, 2=Desk Audit, 4=Program Shop -->
          <!-- <xsl:if test="//Assignment/AssignmentToType='DRP'"> removed via spec changes. -->
          <xsl:attribute name="AssignmentTypeID">4</xsl:attribute>

          <xsl:attribute name="ExposureCD">
            <xsl:choose>
            	 <xsl:when test="$VehicleParty=1">1</xsl:when>
               <xsl:otherwise>3</xsl:otherwise>
	          </xsl:choose>
          </xsl:attribute>

          <xsl:attribute name="BodyStyle">
            <xsl:value-of select="//Vehicle/BodyStyle"/>
          </xsl:attribute>
          <xsl:attribute name="Color">
            <xsl:value-of select="//Vehicle/ExternalColor"/>
          </xsl:attribute>

          <xsl:apply-templates select="//Party[PartyRoleType/@code='VI' or PartyRoleType/@code='VO' or PartyRoleType/@code='IN' or PartyRoleType/@code='DR']/Person" mode="ContactParams">
            <xsl:with-param name="Title">Contact</xsl:with-param>
          </xsl:apply-templates>

          <xsl:attribute name="CoverageProfileCD">
            <xsl:choose>
              <xsl:when test="//Claim/LossCategoryCode/@code='C'">COLL</xsl:when>
              <xsl:when test="//Claim/LossCategoryCode/@code='M'">COMP</xsl:when>
              <xsl:when test="//Claim/LossCategoryCode/@code='L'">LIAB</xsl:when>
              <xsl:otherwise>UIM</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>

	    <xsl:attribute name="Drivable">
	        <xsl:choose>
		   	 <xsl:when test="//Vehicle/VehicleDrivable='Y'">1</xsl:when>
		      <xsl:otherwise>0</xsl:otherwise>
		   </xsl:choose>
	    </xsl:attribute>
	    
	    
          <xsl:attribute name="ImpactLocations">
            <xsl:for-each select="//Vehicle/ImpactPoint">
              <xsl:call-template name="ConvertImpactPoint"/>
              <xsl:if test="position()!=last()"><xsl:text>,</xsl:text></xsl:if>
            </xsl:for-each>
          </xsl:attribute>

          <xsl:attribute name="ImpactSpeed">
            <xsl:value-of select="//Vehicle/ImpactSpeed"/>
          </xsl:attribute>

          <xsl:attribute name="LicensePlateNumber">
            <xsl:value-of select="//Vehicle/LicensePlateNumber"/>
          </xsl:attribute>
          <xsl:attribute name="LicensePlateState">
            <xsl:value-of select="//Vehicle/LicensePlateState"/>
          </xsl:attribute>

          <xsl:attribute name="LocationName">
            <xsl:value-of select="//Vehicle/VehicleLocation/LocationName"/>
          </xsl:attribute>
          <xsl:apply-templates select="//Vehicle/VehicleLocation/ContactPerson/Address">
            <xsl:with-param name="Title">Location</xsl:with-param>
          </xsl:apply-templates>
          <xsl:attribute name="LocationPhone">
            <xsl:apply-templates select="//Vehicle/VehicleLocation/ContactPerson" mode="BestPhone"/>
          </xsl:attribute>

          <xsl:attribute name="Make">
            <xsl:value-of select="//Vehicle/MakeCode"/>
          </xsl:attribute>
          <xsl:attribute name="Mileage">
            <xsl:value-of select="//Vehicle/Mileage"/>
          </xsl:attribute>
          <xsl:attribute name="Model">
            <xsl:value-of select="//Vehicle/Model"/>
          </xsl:attribute>
          <xsl:attribute name="VehicleYear">
            <xsl:call-template name="Year">
              <xsl:with-param name="In">
                <xsl:value-of select="//Vehicle/ModelYear"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:attribute>

          <xsl:attribute name="NADAId">
            <xsl:value-of select="//Vehicle/NadaId"/>
          </xsl:attribute>
          <xsl:attribute name="PermissionToDrive">
            <xsl:value-of select="//Vehicle/PermissionToDrive"/>
          </xsl:attribute>
          <xsl:attribute name="PostedSpeed">
            <xsl:value-of select="//Vehicle/PostedSpeed"/>
          </xsl:attribute>

          <xsl:attribute name="PriorDamage">
            <xsl:for-each select="//Vehicle/PriorImpactPoint">
              <xsl:call-template name="ConvertImpactPoint"/>
              <xsl:if test="position()!=last()"><xsl:text>,</xsl:text></xsl:if>
            </xsl:for-each>
          </xsl:attribute>

          <xsl:attribute name="Remarks">
            <xsl:value-of select="//Loss/LossMemoText"/>
            <xsl:text>  </xsl:text>
            <xsl:value-of select="//Vehicle/DamageMemoText"/>
          </xsl:attribute>

          <xsl:attribute name="RentalDaysAuthorized">
            <xsl:value-of select="//Vehicle/Rental/RentalDaysAuthorized"/>
          </xsl:attribute>
          <xsl:attribute name="RentalInstructions">
            <xsl:value-of select="//Vehicle/Rental/RentalInstructions"/>
          </xsl:attribute>

          <xsl:attribute name="ShopLocationID">
            <xsl:value-of select="//AssignedTo/AssignedToCompany"/>
          </xsl:attribute>
          <xsl:attribute name="ShopRemarks">
            <xsl:value-of select="//AssignedTo/InstructionsToEstimator"/>            
          </xsl:attribute>

          <xsl:attribute name="Vin">
            <xsl:value-of select="//Vehicle/Vin"/>
          </xsl:attribute>

          <!-- Add involved elements as needed for Insured, Claimant and Owner -->

          <xsl:choose>

            <!-- First party -->
           <xsl:when test="$VehicleParty=1">
              
              <xsl:if test="($Insured!='')">                
                <xsl:for-each select="//Party[PartyRoleType/@code='VI' or PartyRoleType/@code='IN']">
                  <xsl:call-template name="Involved">
                    <xsl:with-param name="VehNum">1</xsl:with-param>
                    <xsl:with-param name="Insured">1</xsl:with-param>
                    <xsl:with-param name="Owner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
                  </xsl:call-template>
                </xsl:for-each>
              </xsl:if>
              <xsl:if test="($Owner!='') and ($Owner!=$Insured)">
                <xsl:for-each select="//Party[PartyRoleType/@code='VO']">
                  <xsl:call-template name="Involved">
                    <xsl:with-param name="VehNum">1</xsl:with-param>
                    <xsl:with-param name="Owner">1</xsl:with-param>
                  </xsl:call-template>
                </xsl:for-each>
              </xsl:if>

            </xsl:when>

            <!-- Third party -->
            <xsl:otherwise>

              <xsl:if test="($Claimant!='')">
                <xsl:for-each select="//Party[PartyRoleType/@code != 'VI' and PartyRoleType/@code != 'IN']">
                  <xsl:call-template name="Involved">
                    <xsl:with-param name="VehNum">2</xsl:with-param>
                    <xsl:with-param name="Claimant">1</xsl:with-param>
                    <xsl:with-param name="Owner"><xsl:value-of select="$ClaimantIsOwner"/></xsl:with-param>
                  </xsl:call-template>
                </xsl:for-each>
              </xsl:if>
              <xsl:if test="($Owner!='')">
                <!-- Special case - if owner passed and we dont have a claimant 
                      then make the owner the claimant -->
                <xsl:if test="($Claimant='')">
                  <xsl:for-each select="//Party[PartyRoleType/@code = 'VO']">
                    <xsl:call-template name="Involved">
                      <xsl:with-param name="VehNum">2</xsl:with-param>
                      <xsl:with-param name="Claimant">1</xsl:with-param>
                      <xsl:with-param name="Owner">1</xsl:with-param>
                    </xsl:call-template>
                  </xsl:for-each>
                </xsl:if>
                <!-- Normal case -->
                <xsl:if test="($Owner!=$Claimant) and ($Claimant!='')">
                  <xsl:for-each select="//Party[PartyRoleType/@code != 'VO' and PartyRoleType/@code != 'VI']">
                    <xsl:call-template name="Involved">
                      <xsl:with-param name="VehNum">2</xsl:with-param>
                      <xsl:with-param name="Owner">1</xsl:with-param>
                    </xsl:call-template>
                  </xsl:for-each>
                </xsl:if>
              </xsl:if>

            </xsl:otherwise>
          </xsl:choose>


        </xsl:element>

      </xsl:element>

      <xsl:element name="BeginInsert"/>

    </xsl:element>

  </xsl:template>

  <!-- ================================================== -->
  <!-- INVOLVED ELEMENT                                   -->
  <!-- ================================================== -->

  <xsl:template name="Involved">

    <xsl:param name="VehNum"/>
    <xsl:param name="Insured">0</xsl:param>
    <xsl:param name="Owner">0</xsl:param>
    <xsl:param name="Claimant">0</xsl:param>

    <xsl:element name="Involved">

      <!-- LynxID stuffed from COM -->

      <xsl:attribute name="VehicleNumber">
        <xsl:value-of select="$VehNum"/>
      </xsl:attribute>

      <xsl:attribute name="InvolvedTypeInsured">
        <xsl:value-of select="$Insured"/>
      </xsl:attribute>
      <xsl:attribute name="InvolvedTypeClaimant">
        <xsl:value-of select="$Claimant"/>
      </xsl:attribute>
      <xsl:attribute name="InvolvedTypeOwner">
        <xsl:value-of select="$Owner"/>
      </xsl:attribute>

      <xsl:apply-templates select="Person" mode="ContactParams">
        <xsl:with-param name="Title"></xsl:with-param>
      </xsl:apply-templates>

      <xsl:apply-templates select="Attorney/ContactPerson" mode="ContactParamsLight">
        <xsl:with-param name="Title">Attorney</xsl:with-param>
      </xsl:apply-templates>

      <xsl:apply-templates select="Doctor/ContactPerson" mode="ContactParamsLight">
        <xsl:with-param name="Title">Doctor</xsl:with-param>
      </xsl:apply-templates>

      <xsl:apply-templates select="Employer/ContactPerson" mode="ContactParamsLight">
        <xsl:with-param name="Title">Employer</xsl:with-param>
      </xsl:apply-templates>

      <xsl:attribute name="Age">
        <xsl:value-of select="Age"/>
      </xsl:attribute>
      <xsl:attribute name="BusinessName">
        <xsl:value-of select="BusinessName"/>
      </xsl:attribute>
      <xsl:attribute name="DateOfBirth">
        <xsl:value-of select="DateOfBirth"/>
      </xsl:attribute>
      <xsl:attribute name="DriverLicenseNumber">
        <xsl:value-of select="DriverLicenseNumber"/>
      </xsl:attribute>
      <xsl:attribute name="DriverLicenseState">
        <xsl:value-of select="DriverLicenseState"/>
      </xsl:attribute>
      <xsl:attribute name="Gender">
        <xsl:value-of select="Gender"/>
      </xsl:attribute>
      <xsl:attribute name="Injured">
        <xsl:value-of select="Injured"/>
      </xsl:attribute>
      <xsl:attribute name="InjuryDescription">
        <xsl:value-of select="InjuryDescription"/>
      </xsl:attribute>
      <xsl:attribute name="InjuryTypeId">
        <xsl:value-of select="InjuryTypeID"/>
      </xsl:attribute>

      <xsl:attribute name="LocationInVehicleId">
        <xsl:value-of select="LocationInVehicleID"/>
      </xsl:attribute>
      <xsl:attribute name="SeatBelt">
        <xsl:value-of select="SeatBelt"/>
      </xsl:attribute>
      <xsl:attribute name="TaxID">
        <xsl:value-of select="TaxID"/>
      </xsl:attribute>
      <xsl:attribute name="Violation">
        <xsl:value-of select="Violation"/>
      </xsl:attribute>
      <xsl:attribute name="ViolationDescription">
        <xsl:value-of select="ViolationDescription"/>
      </xsl:attribute>

      <xsl:attribute name="ThirdPartyInsuranceName">
        <xsl:value-of select="ThirdPartyInsurance/ThirdPartyInsuranceName"/>
      </xsl:attribute>
      <xsl:attribute name="ThirdPartyInsurancePhone">
        <xsl:value-of select="ThirdPartyInsurance/Telecom/AreaCode"/>
        <xsl:value-of select="ThirdPartyInsurance/Telecom/TelecomNum"/>
      </xsl:attribute>
      <xsl:attribute name="ThirdPartyInsurancePhoneExt">
        <xsl:value-of select="ThirdPartyInsurance/Telecom/Extn"/>
      </xsl:attribute>

      <xsl:attribute name="InvolvedTypeDriver">
        <xsl:value-of select="InvolvedTypeDriver"/>
      </xsl:attribute>
      <xsl:attribute name="InvolvedTypePassenger">
        <xsl:value-of select="InvolvedTypePassenger"/>
      </xsl:attribute>
      <xsl:attribute name="InvolvedTypePedestrian">
        <xsl:value-of select="InvolvedTypePedestrian"/>
      </xsl:attribute>
      <xsl:attribute name="InvolvedTypeWitness">
        <xsl:value-of select="InvolvedTypeWitness"/>
      </xsl:attribute>

      <xsl:attribute name="WitnessLocation">
        <xsl:value-of select="WitnessLocation"/>
      </xsl:attribute>

    </xsl:element>

  </xsl:template>

</xsl:stylesheet>

