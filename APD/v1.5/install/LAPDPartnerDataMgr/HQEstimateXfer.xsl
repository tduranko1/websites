<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
								xmlns:msxsl="urn:schemas-microsoft-com:xslt"
								xmlns:hqpxsl="http://www.lynxservices.com/adp"
                                xmlns:cu="urn:the-xml-files:xslt">

  <xsl:import href="EstXferUtils.xsl"/>

  <xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes"/>
  <xsl:decimal-format NaN="0"/>

  <!--- This styesheet transforms incoming ADP EMS Data to APD's Common Estimate Format -->


  <!--- Scripting necessary for the mathematical calculations in the summary -->
  <msxsl:script language="JScript" implements-prefix="hqpxsl">
    <![CDATA[
	var BodyMaterialAmount, BodyMaterialHours, BodyMaterialRate;
	var DiscountMarkupBase; 
	var TotalDiscountAmount, DiscountPct, DiscountTaxAmount;
	var TotalMarkupAmount, MarkupPct, MarkupTaxAmount;
	var PaintMaterialAmount, PaintMaterialHours, PaintMaterialRate;
	var TotalParts, TotalTaxParts, TotalNonTaxParts, TotalOtherTaxParts, TotalOtherNonTaxParts, TotalTaxMaterials, TotalTaxPartsMaterials,TotalTaxLabor;
	var TotalMaterials, TotalPartsMaterials;
  var TotalOEMParts, TotalLKQParts, TotalAFMKParts, TotalRemanParts;


	var BodyLaborAmount, BodyLaborHours, BodyLaborRate;
	var FrameLaborAmount, FrameLaborHours, FrameLaborRate;
	var MechLaborAmount, MechLaborHours, MechLaborRate;
	var PaintLaborAmount, PaintLaborHours, PaintLaborRate;
	var TotalOtherLabor;
	var TotalLaborAmount;

	var StorageAmount;
	var SubletAmount;
	var TowingAmount;
	var OtherAddlChargesAmount;
	var TotalAddlCharges;

	var AppearanceAllowanceAmount;
	var BettermentAmount;
	var DeductibleAmount;
	var OtherAdjustmentAmount;
	var TotalAdjustmentAmount;

	var SalesTaxAmountPM, SalesTaxBasePM, SalesTaxPctPM;
	var SalesTaxAmountLabor, SalesTaxBaseLabor, SalesTaxPctLabor;
     var OtherTaxAmount;
	var TotalTaxAmount;

	var NetTotalAmount;
	var RepairTotalAmount;
	var SubtotalAmount;
	var ZZVarianceAmount;

	var DetailPartTaxTotal, DetailPartNonTaxTotal, DetailPartOtherTaxTotal, DetailPartOtherNonTaxTotal;
	var DetailBodyLaborRepairHours, DetailBodyLaborReplaceHours;
	var DetailFrameLaborRepairHours, DetailFrameLaborReplaceHours; 
	var DetailMechLaborRepairHours, DetailMechLaborReplaceHours; 
	var DetailPaintLaborRepairHours, DetailPaintLaborReplaceHours;
	var DetailSubletTotal, DetailBettermentTotal;
	var DetailUnknownTotal;
	var DetailOEMParts, DetailLKQParts, DetailAFMKParts, DetailRemanParts;
	var Detail2WAlign, Detail4WAlign;

	var CurrentLineNumber;

     var OptionList, OptionListLength;
		
	function setvar(varname, value) {
		eval(varname + ' = ' + value);
		return '';
	}

	function getvar(varname) {	   
		    return eval(eval(varname));	
	}
  
  function getvar1(varname, index) {
	     if (index == undefined) {
		    return eval(eval(varname));
		}
		else {
			return eval(varname + '[' + index + ']');
	     }
	}
	
     function parseoptions(optionstring) {
          // This functions parses the contents of the Vehicle Option node passed to it and saves it in an array.  This array is made global so that it's contents can be retrieved through getvar()
		
	     var objOptionString = new String();
	     objOptionString = optionstring;
	
		// ADP adds an extra linefeed onto the end of the option string...we must remove this
		objOptionString = objOptionString.substr(0, objOptionString.length - 1)
	
          // Now split at each remaining linefeed
	     OptionList = objOptionString.split("\n");

          // Save the length of the array
          OptionListLength = OptionList.length;

          // Go throught the OptionList string array and remove the codes, leaving just the option descriptions
          for (var index=0; index < OptionListLength; index++) {
             	OptionList[index] = OptionList[index].substr(OptionList[index].indexOf(';') + 1, OptionList[index].length - OptionList[index].indexOf(';'));
          }

          return '';
     }

     function parsemessages(messagestring) {
          // This functions parses the contents of the messagestring passed to it and translates them into a message string.

	     // Set up translation matrix

	     var arMessageStore = new Array(   "01", "CALL DEALER FOR EXACT PART # / PRICE",
								 				 "02", "PART NO. DISCONTINUED, CALL DEALER FOR EXACT PART NO.",
												 "03", "GSMP PART - CONTACT DEALER FOR EXACT PRICE",
												 "04", "PRICE NOT YET AVAILABLE, CALL LOCAL DEALER",
												 "07", "STRUCTURAL PART AS IDENTIFIED BY I-CAR",
												 "10", "INCLUDES ADP TIME TO CLEAR ENTIRE PANEL",
												 "13", "INCLUDES 0.6 HOURS FIRST PANEL TWO-STAGE ALLOWANCE",
												 "14", "INCLUDES 1.0 HOURS FIRST PANEL THREE-STAGE ALLOWANCE",
												 "15", "INCLUDES 0.4 HOURS FIRST PANEL TWO-TONE ALLOWANCE",
												 "46", "PRINTABLE ALTERNATE PARTS COMPARE",
												 "49", "UNPRINTED ALTERNATE PARTS COMPARE",
												 "50", "SEE PREVIOUS ESTIMATE FOR SUPPLIER INFORMATION",
												 "60", "ALTERNATE PARTS PART PRICE INCLUDES LABOR",
												 "61", "ALTERNATE PARTS GLASS PART PRICE INCLUDES LABOR",
												 "70", "RECYCLED ADP SPPL",
												 "71", "RECYCLED 800#" );

	     var objMessageString = new String();
	     objMessageString = messagestring;
	
		var arMessageList;
		var translatedstring = "";
		var bFirstMessage = 1;
		var bMessageFound;
		
          // First split at each comma
	     arMessageList = objMessageString.split(",");

          // Go throught the arMessageList string array and translate the codes
          for (var indexL=0; indexL < arMessageList.length; indexL++) {
			
			// Search for the corresponding message
			bMessageFound = 0;
			
			for (var indexS=0; indexS < arMessageStore.length; indexS += 2) {
				if (arMessageStore[indexS] == arMessageList[indexL]) {
					
					// Concatenate the message to the translated string
					if (bFirstMessage == 0) {
						 translatedstring += "; ";
					}
					
					translatedstring += arMessageStore[indexS + 1];
					bMessageFound = 1;
					bFirstMessage = 0;
				}
			}
			
			if (bMessageFound == 0) {
				// The message was not found
				if (bFirstMessage == 0) {
					translatedstring += "; ";
				}
				
				translatedstring += "*** Unknown MC Message Code ***";
				bFirstMessage = 0;
			}
         }

          return translatedstring;
     }

    function evalDbPrice(str)
    {
      if (isNaN(str) || str == '')
        return parseInt("0",10);
      else
        return parseFloat(str);
    }

]]>
  </msxsl:script>

  <xsl:variable name="source" select="'HQ'"/>
  <xsl:variable name="estimatepackage" select="'Shoplink'"/>
  <xsl:variable name="version" select="'1.0.2'"/>
  <xsl:variable name="versiondate" select="'12/12/2014'"/>

  <xsl:template match="/EMSEstimateSpecial">
    <APDEstimate>
      <xsl:attribute name="Source">
        <xsl:value-of select="$source"/>
      </xsl:attribute>
      <xsl:attribute name="EstimatePackage">
        <xsl:value-of select="$estimatepackage"/>
      </xsl:attribute>
      <xsl:attribute name="SoftwareVersion">
        <!--<xsl:value-of select="EMS/ENV/SW_VERSION"/>-->
      </xsl:attribute>
      <xsl:attribute name="StylesheetVersion">
        <xsl:value-of select="$version"/>
      </xsl:attribute>
      <xsl:attribute name="StylesheetDate">
        <xsl:value-of select="$versiondate"/>
      </xsl:attribute>

      <xsl:apply-templates select="/" mode="Header"/>
      <xsl:apply-templates select="/" mode="Detail"/>
      <xsl:apply-templates select="/" mode="Profile"/>
      <xsl:apply-templates select="/" mode="Summary"/>
      <xsl:apply-templates select="/" mode="Audit"/>
    </APDEstimate>
  </xsl:template>


  <!--- Transform Header Information -->
  <xsl:template match="EMSEstimateSpecial" mode="Header">
    <Header>
      <xsl:attribute name="EstimateType">
        <xsl:choose>
          <xsl:when test="ENV/TRANS_TYPE = 'E'">Estimate</xsl:when>
          <xsl:when test="ENV/TRANS_TYPE = 'S'">Supplement</xsl:when>
          <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="SupplementNumber">
        <xsl:choose>
          <xsl:when test="ENV/TRANS_TYPE = 'E'">0</xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="number(substring(ENV/SUPP_NO, 2, 2))"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="WrittenDate">
        <!--COMMIT_DT instead of TRANSMT_DT-->
        <xsl:value-of select="ENV/COMMIT_DT"/>
      </xsl:attribute>
      <xsl:attribute name="AssignmentDate">
        <!--LOSS_DATE instead of ASGN_DATE-->
        <!--<xsl:value-of select="AD1/LOSS_DATE"/>-->
        <xsl:value-of select="ENV/CREATE_DT"/>
      </xsl:attribute>
      <xsl:attribute name="ClaimantName">
        <xsl:value-of select="AD2/CLMT_LN"/>
      </xsl:attribute>
      <xsl:attribute name="ClaimNumber">
        <xsl:value-of select="AD1/CLM_NO"/>
      </xsl:attribute>
      <xsl:attribute name="InsuranceCompanyName">
        <xsl:value-of select="AD1/INS_CO_NM"/>
      </xsl:attribute>
      <xsl:attribute name="InsuredName">
        <!--<xsl:value-of select="AD1/INSD_LN"/>-->
      </xsl:attribute>
      <xsl:attribute name="LossDate">
        <xsl:value-of select="AD1/LOSS_DATE"/>
      </xsl:attribute>
      <xsl:attribute name="LossType">
        <!--LOSS_CAT Not Available in HQ Xml-->
        <xsl:choose>
          <xsl:when test="AD1/LOSS_CAT='C'">Collision</xsl:when>
          <xsl:when test="AD1/LOSS_CAT='L'">Liability</xsl:when>
          <xsl:when test="AD1/LOSS_CAT='M'">Comprehensive</xsl:when>
          <xsl:when test="AD1/LOSS_CAT='O'">Other</xsl:when>
          <xsl:when test="AD1/LOSS_CAT='P'">Property</xsl:when>
          <xsl:when test="AD1/LOSS_CAT='U'">Unknown</xsl:when>
          <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="PolicyNumber">
        <!--AD1/POLICY_NO Not Available in HQ Xml-->
      </xsl:attribute>
      <xsl:attribute name="Remarks">
        <!--VEH/DMG_MEMO is not available in HQ Xml-->
      </xsl:attribute>
      <xsl:attribute name="RepairDays"/>
      <xsl:attribute name="SettlementType">
        <xsl:choose>
          <xsl:when test="AD1/TLOS_IND='1'">TotalLoss</xsl:when>
          <xsl:otherwise>Normal</xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="LynxID">
        <xsl:value-of select="/AD1/CLM_NO"/>
      </xsl:attribute>
      <xsl:attribute name="VehicleNumber">
        <!--<xsl:value-of select="/ADPTransaction/@VehicleID"/>-->
      </xsl:attribute>
      <xsl:attribute name="AssignmentID">
        <!--<xsl:value-of select="/ADPTransaction/@AssignmentID"/>-->
      </xsl:attribute>
      <xsl:attribute name="EstimateGuid">
        <xsl:value-of select="ENV/EstimateGuid"/>
      </xsl:attribute>
      <xsl:attribute name="KeyId">
        <xsl:value-of select="ENV/KeyId"/>
      </xsl:attribute>

      <Appraiser>
        <xsl:attribute name="AppraiserName">
          <xsl:value-of select="AD2/EST_CO_NM"/>
        </xsl:attribute>
        <xsl:attribute name="AppraiserLicenseNumber">
          <!--RF_LIC_NO instead of EST_LIC_NO-->
          <xsl:value-of select="AD2/RF_LIC_NO"/>
        </xsl:attribute>
      </Appraiser>

      <Estimator>
        <xsl:choose>
          <!--EST_ADDR1 instead of EST_CO_NM-->
          <xsl:when test="AD2/EST_ADDR1 != ''">
            <xsl:attribute name="CompanyAddress">
              <xsl:value-of select="concat(AD2/RF_ADDR1, AD2/RF_ADDR1)"/>
            </xsl:attribute>
            <xsl:attribute name="CompanyAreaCode">
              <xsl:value-of select="substring(AD2/EST_PH1, 1, 3)"/>
            </xsl:attribute>
            <xsl:attribute name="CompanyCity">
              <xsl:value-of select="AD2/RF_CITY"/>
            </xsl:attribute>
            <xsl:attribute name="CompanyName">
              <xsl:value-of select="AD2/EST_CO_NM"/>
            </xsl:attribute>
            <xsl:attribute name="CompanyExchangeNumber">
              <xsl:value-of select="substring(AD2/EST_PH1, 4, 3)"/>
            </xsl:attribute>
            <xsl:attribute name="CompanyState">
              <xsl:value-of select="AD2/RF_ST"/>
            </xsl:attribute>
            <xsl:attribute name="CompanyUnitNumber">
              <xsl:value-of select="substring(AD2/EST_PH1, 7, 4)"/>
            </xsl:attribute>
            <xsl:attribute name="CompanyZip">
              <xsl:value-of select="AD2/RF_ZIP"/>
            </xsl:attribute>
          </xsl:when>
          <xsl:when test="AD2/RF_CO_NM != ''">
            <xsl:attribute name="CompanyAddress">
              <xsl:value-of select="concat(AD2/RF_ADDR1, AD2/RF_ADDR2)"/>
            </xsl:attribute>
            <xsl:attribute name="CompanyAreaCode">
              <xsl:value-of select="substring(AD2/RF_PH1, 1, 3)"/>
            </xsl:attribute>
            <xsl:attribute name="CompanyCity">
              <xsl:value-of select="AD2/RF_CITY"/>
            </xsl:attribute>
            <xsl:attribute name="CompanyName">
              <xsl:value-of select="AD2/RF_CO_NM"/>
            </xsl:attribute>
            <xsl:attribute name="CompanyExchangeNumber">
              <xsl:value-of select="substring(AD2/RF_PH1, 4, 3)"/>
            </xsl:attribute>
            <xsl:attribute name="CompanyState">
              <xsl:value-of select="AD2/RF_ST"/>
            </xsl:attribute>
            <xsl:attribute name="CompanyUnitNumber">
              <xsl:value-of select="substring(AD2/RF_PH1, 7, 4)"/>
            </xsl:attribute>
            <xsl:attribute name="CompanyZip">
              <xsl:value-of select="AD2/RF_ZIP"/>
            </xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="CompanyAddress"/>
            <xsl:attribute name="CompanyAreaCode"/>
            <xsl:attribute name="CompanyCity"/>
            <xsl:attribute name="CompanyName">! Not Found In Estimate Data !</xsl:attribute>
            <xsl:attribute name="CompanyExchangeNumber"/>
            <xsl:attribute name="CompanyState"/>
            <xsl:attribute name="CompanyUnitNumber"/>
            <xsl:attribute name="CompanyZip"/>
          </xsl:otherwise>
        </xsl:choose>
      </Estimator>

      <Inspection>
        <xsl:attribute name="InspectionAddress">
          <xsl:value-of select="AD2/INSP_ADDR1"/>
        </xsl:attribute>
        <xsl:attribute name="InspectionAreaCode">
          <xsl:value-of select="substring(AD2/INSP_PH1, 1, 3)"/>
        </xsl:attribute>
        <xsl:attribute name="InspectionCity">
          <xsl:value-of select="AD2/INSP_CITY"/>
        </xsl:attribute>
        <xsl:attribute name="InspectionExchangeNumber">
          <xsl:value-of select="substring(AD2/INSP_PH1, 4, 3)"/>
        </xsl:attribute>
        <xsl:attribute name="InspectionLocation"/>
        <xsl:attribute name="InspectionState">
          <xsl:value-of select="AD2/INSP_ST"/>
        </xsl:attribute>
        <xsl:attribute name="InspectionType">
          <!--<xsl:value-of select="AD2/INSP_DESC"/>-->
        </xsl:attribute>
        <xsl:attribute name="InspectionUnitNumber">
          <xsl:value-of select="substring(AD2/INSP_PH1, 7, 4)"/>
        </xsl:attribute>
        <xsl:attribute name="InspectionZip">
          <xsl:value-of select="AD2/INSP_ZIP"/>
        </xsl:attribute>
      </Inspection>

      <Owner>
        <xsl:attribute name="OwnerAddress">
          <xsl:value-of select="concat(AD1/OWNR_ADDR1, AD1/OWNR_ADDR2)"/>
        </xsl:attribute>
        <xsl:attribute name="OwnerCity">
          <xsl:value-of select="AD1/OWNR_CITY"/>
        </xsl:attribute>
        <xsl:attribute name="OwnerDayAreaCode">
          <xsl:value-of select="substring(AD1/OWNR_PH1, 1, 3)"/>
        </xsl:attribute>
        <xsl:attribute name="OwnerDayExchangeNumber">
          <xsl:value-of select="substring(AD1/OWNR_PH1, 4, 3)"/>
        </xsl:attribute>
        <xsl:attribute name="OwnerDayExtensionNumber">
          <!--<xsl:value-of select="AD1/OWNR_PH1X"/>-->
        </xsl:attribute>
        <xsl:attribute name="OwnerDayUnitNumber">
          <xsl:value-of select="substring(AD1/OWNR_PH1, 7, 4)"/>
        </xsl:attribute>
        <xsl:attribute name="OwnerEveningAreaCode">
          <xsl:value-of select="substring(AD1/OWNR_PH2, 1, 3)"/>
        </xsl:attribute>
        <xsl:attribute name="OwnerEveningExchangeNumber">
          <xsl:value-of select="substring(AD1/OWNR_PH2, 4, 3)"/>
        </xsl:attribute>
        <xsl:attribute name="OwnerEveningExtensionNumber">
          <!--<xsl:value-of select="AD1/OWNR_PH2X"/>-->
        </xsl:attribute>
        <xsl:attribute name="OwnerEveningUnitNumber">
          <xsl:value-of select="substring(AD1/OWNR_PH2, 7, 4)"/>
        </xsl:attribute>
        <xsl:attribute name="OwnerName">
          <xsl:value-of select="AD1/OWNR_LN"/>
        </xsl:attribute>
        <xsl:attribute name="OwnerState">
          <xsl:value-of select="AD1/OWNR_ST"/>
        </xsl:attribute>
        <xsl:attribute name="OwnerZip">
          <xsl:value-of select="AD1/OWNR_ZIP"/>
        </xsl:attribute>
      </Owner>

      <Shop>
        <xsl:attribute name="ShopAddress">
          <xsl:value-of select="concat(AD2/RF_ADDR1, AD2/RF_ADDR2)"/>
        </xsl:attribute>
        <xsl:attribute name="ShopAreaCode">
          <xsl:value-of select="substring(AD2/RF_PH1, 2, 3)"/>
        </xsl:attribute>
        <xsl:attribute name="ShopCity">
          <xsl:value-of select="AD2/RF_CITY"/>
        </xsl:attribute>
        <xsl:attribute name="ShopExchangeNumber">
          <xsl:value-of select="substring(AD2/RF_PH1, 6, 3)"/>
        </xsl:attribute>
        <xsl:attribute name="ShopName">
          <xsl:value-of select="AD2/RF_CO_NM"/>
        </xsl:attribute>
        <xsl:attribute name="ShopRegistrationNumber">
          <xsl:value-of select="AD2/RF_LIC_NO"/>
        </xsl:attribute>
        <xsl:attribute name="ShopState">
          <xsl:value-of select="AD2/RF_ST"/>
        </xsl:attribute>
        <xsl:attribute name="ShopUnitNumber">
          <xsl:value-of select="substring(AD2/RF_PH1, 10, 4)"/>
        </xsl:attribute>
        <xsl:attribute name="ShopZip">
          <xsl:value-of select="AD2/RF_ZIP"/>
        </xsl:attribute>
      </Shop>

      <Vehicle>
        <xsl:attribute name="VehicleBodyStyle">
          <!--<xsl:value-of select="VEH/V_BSTYLE"/>-->
        </xsl:attribute>
        <xsl:attribute name="VehicleColor">
          <xsl:value-of select="VEH/V_COLOR"/>
        </xsl:attribute>
        <xsl:attribute name="VehicleEngineType">
          <xsl:value-of select="VEH/V_ENGINE"/>
        </xsl:attribute>
        <xsl:attribute name="VehicleMake">
          <xsl:value-of select="VEH/V_MAKEDESC"/>
        </xsl:attribute>
        <xsl:attribute name="VehicleMileage">
          <!--<xsl:value-of select="translate(VEH/V_MILEAGE, ',', '')"/>-->
        </xsl:attribute>
        <xsl:attribute name="VehicleModel">
          <xsl:value-of select="VEH/V_MODEL"/>
        </xsl:attribute>
        <xsl:attribute name="VehicleYear">
          <xsl:if test="number(VEH/V_MODEL_YR) &gt; 0">
            <xsl:value-of select="number(VEH/V_MODEL_YR)"/>
          </xsl:if>
        </xsl:attribute>
        <xsl:attribute name="Vin">
          <xsl:value-of select="VEH/V_VIN"/>
        </xsl:attribute>

        <!--There is IMPACT node came on HYPER QUEST-->
        <xsl:for-each select="VEH/*[starts-with(name(), 'IMPACT') and . != '']">
          <ImpactPoint>
            <xsl:attribute name="ImpactArea">
              <xsl:choose>
                <xsl:when test="number(.)='1'">
                  <xsl:value-of select="'Right Front'"/>
                </xsl:when>
                <xsl:when test="number(.)='2'">
                  <xsl:value-of select="'Right Front Pillar'"/>
                </xsl:when>
                <xsl:when test="number(.)='3'">
                  <xsl:value-of select="'Right T-Bone'"/>
                </xsl:when>
                <xsl:when test="number(.)='4'">
                  <xsl:value-of select="'Right Quarter Post'"/>
                </xsl:when>
                <xsl:when test="number(.)='5'">
                  <xsl:value-of select="'Right Rear'"/>
                </xsl:when>
                <xsl:when test="number(.)='6'">
                  <xsl:value-of select="'Rear'"/>
                </xsl:when>
                <xsl:when test="number(.)='7'">
                  <xsl:value-of select="'Left Rear'"/>
                </xsl:when>
                <xsl:when test="number(.)='8'">
                  <xsl:value-of select="'Left Quarter Post'"/>
                </xsl:when>
                <xsl:when test="number(.)='9'">
                  <xsl:value-of select="'Left T-Bone'"/>
                </xsl:when>
                <xsl:when test="number(.)='10'">
                  <xsl:value-of select="'Left Front Pillar'"/>
                </xsl:when>
                <xsl:when test="number(.)='11'">
                  <xsl:value-of select="'Left Front'"/>
                </xsl:when>
                <xsl:when test="number(.)='12'">
                  <xsl:value-of select="'Front'"/>
                </xsl:when>
                <xsl:when test="number(.)='13'">
                  <xsl:value-of select="'Rollover'"/>
                </xsl:when>
                <xsl:when test="number(.)='14'">
                  <xsl:value-of select="'Unknown'"/>
                </xsl:when>
                <xsl:when test="number(.)='15'">
                  <xsl:value-of select="'Total Loss'"/>
                </xsl:when>
                <xsl:when test="number(.)='16'">
                  <xsl:value-of select="'Non-Collision'"/>
                </xsl:when>
                <xsl:when test="number(.)='19'">
                  <xsl:value-of select="'All Over'"/>
                </xsl:when>
                <xsl:when test="number(.)='20'">
                  <xsl:value-of select="'Stripped'"/>
                </xsl:when>
                <xsl:when test="number(.)='21'">
                  <xsl:value-of select="'Undercarriage'"/>
                </xsl:when>
                <xsl:when test="number(.)='22'">
                  <xsl:value-of select="'Total Burn'"/>
                </xsl:when>
                <xsl:when test="number(.)='23'">
                  <xsl:value-of select="'Interior Burn'"/>
                </xsl:when>
                <xsl:when test="number(.)='24'">
                  <xsl:value-of select="'Exterior Burn'"/>
                </xsl:when>
                <xsl:when test="number(.)='25'">
                  <xsl:value-of select="'Hood'"/>
                </xsl:when>
                <xsl:when test="number(.)='26'">
                  <xsl:value-of select="'Deck Lid'"/>
                </xsl:when>
                <xsl:when test="number(.)='27'">
                  <xsl:value-of select="'Roof'"/>
                </xsl:when>
                <xsl:otherwise></xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="PrimaryImpactFlag">
              <xsl:choose>
                <xsl:when test="name()='IMPACT_1'">Yes</xsl:when>
                <xsl:otherwise>No</xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
          </ImpactPoint>
        </xsl:for-each>

        <!--- We need to parse out the Option String given to us.  These values are saved in a javascript array object accessible via getvar() -->
        <xsl:value-of select="hqpxsl:parseoptions(string(VEH/V_OPTIONS))"/>

        <!--- Now call a recursive template to process the array and generate the nodes we require -->
        <xsl:variable name="nextindex" select="0"/>
        <xsl:call-template name="generate-option-node">
          <xsl:with-param name="listindex" select="$nextindex"/>
        </xsl:call-template>
      </Vehicle>
    </Header>
  </xsl:template>

  <!--- Recursive template to retrieve the values from the optionlist array -->
  <xsl:template name="generate-option-node">
    <xsl:param name="listindex"/>

    <xsl:if test="$listindex &lt; hqpxsl:getvar('OptionListLength')">
      <VehicleOption>
        <xsl:attribute name="Option">
          <xsl:value-of select="hqpxsl:getvar1('OptionList', $listindex)"/>
        </xsl:attribute>
      </VehicleOption>

      <!--- Recursively call the template to get the next value -->
      <xsl:variable name="nextindex" select="$listindex + 1"/>
      <xsl:call-template name="generate-option-node">
        <xsl:with-param name="listindex" select="$nextindex"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!--- Transform Detail Line Information -->
  <xsl:template match="EMSEstimateSpecial" mode="Detail">
    <!--- First set up running totals for various detail components.  These will be used for the audit later -->
    <!--- Need to Check-->

    <xsl:value-of select="hqpxsl:setvar('DetailPartTaxTotal', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailPartNonTaxTotal', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailPartOtherTaxTotal', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailPartOtherNonTaxTotal', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailBodyLaborRepairHours', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailBodyLaborReplaceHours', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailFrameLaborRepairHours', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailFrameLaborReplaceHours', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailMechLaborRepairHours', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailMechLaborReplaceHours', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailPaintLaborRepairHours', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailPaintLaborReplaceHours', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailSubletTotal', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailBettermentTotal', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailUnknownTotal', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailOEMParts', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailLKQParts', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailAFMKParts', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('DetailRemanParts', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('Detail2WAlign', 0)"/>
    <xsl:value-of select="hqpxsl:setvar('Detail4WAlign', 0)"/>

    <!-- Grab out of profile the paint material rate and whether betterment is charged on Paint Materials and Labor.  These are used in the betterment amount calculation.  -->
    <xsl:variable name="BettermentLaborMaterials" select="PFH/ADJ_BTR_IN"/>
    <xsl:variable name="PaintMaterialRate" select="PFM[MATL_TYPE='MAPA']/CAL_LBRRTE"/>

    <!--- ADP does not provide discount or markup base in their subtotals...we must calculate from the detail lines -->
    <xsl:value-of select="hqpxsl:setvar('DiscountMarkupBase', 0)"/>

    <Detail>
      <xsl:for-each select="LIN">
        <!--- Here we sorting by LINE_NO instead of UNQ_SEQ-->
        <xsl:sort select="LINE_NO" data-type="number"/>
        <DetailLine>
          <xsl:attribute name="DetailNumber">
            <xsl:value-of select="number(LINE_NO)"/>
          </xsl:attribute>

          <xsl:attribute name="SequenceNumber">
            <xsl:value-of select="SeqenceNumber"/>
          </xsl:attribute>

          <xsl:attribute name="Comment">
            <!--- Here we used ALT_PART_I instead of ALT_PARTM. But it contains mismatch data. So need to verify-->
            <!--<xsl:if test="string-length(string(ALT_PART_I)) &gt; 0">
              <xsl:value-of select="hqpxsl:parsemessages(string(ALT_PART_I))"/>
            </xsl:if>-->
          </xsl:attribute>
          <xsl:attribute name="Description">
            <xsl:choose>
              <xsl:when test="OEM_PARTNO='APPEAR ALLOWANCE' or OEM_PARTNO='RELATED PRIOR' or OEM_PARTNO='UNRELATED PRIOR'">
                <xsl:value-of select="concat(LINE_DESC, ' (', OEM_PARTNO, ' of  $', ACT_PRICE, ')' )"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="LINE_DESC"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>

          <xsl:attribute name="OperationCode">
            <xsl:choose>
              <xsl:when test="MISC_SUBLT='1'">Sublet</xsl:when>
              <!--<xsl:when test="OEM_PARTNO='APPEAR ALLOWANCE' or OEM_PARTNO='RELATED PRIOR' or OEM_PARTNO='UNRELATED PRIOR'">Comment</xsl:when>-->
              <xsl:when test="not(ACT_PRICE) and not(Labor) and not(MISC_SUBLT='1')">Comment</xsl:when>
              <xsl:when test="MOD_LB_HRS=''"/>
              <xsl:when test="LBR_OP='OP0'"/>
              <xsl:when test="LBR_OP='OP1'">Repair</xsl:when>
              <xsl:when test="LBR_OP='OP2'">Remove/Install</xsl:when>
              <xsl:when test="LBR_OP='OP3'">Other</xsl:when>
              <xsl:when test="LBR_OP='OP4'">Alignment</xsl:when>
              <xsl:when test="LBR_OP='OP5'">Repair</xsl:when>
              <xsl:when test="LBR_OP='OP6'">Refinish</xsl:when>
              <xsl:when test="LBR_OP='OP7'">Other</xsl:when>
              <xsl:when test="LBR_OP='OP8'">Other</xsl:when>
              <xsl:when test="LBR_OP='OP9'">Repair</xsl:when>
              <xsl:when test="LBR_OP='OP10'">Repair</xsl:when>
              <xsl:when test="LBR_OP='OP11'">Replace</xsl:when>
              <xsl:when test="LBR_OP='OP12'">Replace</xsl:when>
              <xsl:when test="LBR_OP='OP13'">Other</xsl:when>
              <xsl:when test="LBR_OP='OP14'">Other</xsl:when>
              <xsl:when test="LBR_OP='OP15'">Blend</xsl:when>
              <xsl:when test="LBR_OP='OP16'">Sublet</xsl:when>
              <xsl:when test="LBR_OP='OP17'">Other</xsl:when>
              <xsl:when test="LBR_OP='OP18'">Other</xsl:when>
              <xsl:when test="LBR_OP='OP19'">Other</xsl:when>
              <xsl:when test="LBR_OP='OP20'">Remove/Install</xsl:when>
              <xsl:when test="LBR_OP='OP21'">Repair</xsl:when>
              <xsl:when test="LBR_OP='OP22'">Repair</xsl:when>
              <xsl:when test="LBR_OP='OP23'">Other</xsl:when>
              <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>

          <xsl:attribute name="LineAdded">
            <xsl:if test="LINE_IND!='E'">
              <xsl:value-of select="LINE_IND"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="ManualLineFlag"/>
          <!--- Here we using BettermentPct instead of BETT_PCTG-->
          <xsl:if test="number(BettermentPct) &gt; 0">
            <!--- ADP does not provide betterment amount.  We'll need to calculate it the best we can using the following formula:
							Part Price (+ If BettermentOnPaintAndLabor(Labor Amount + Refinish Amount + Paint Material Amount )) x Betterment % -->
            <xsl:attribute name="BettermentType">
              <!--<xsl:value-of select="BETT_TYPE"/>-->
            </xsl:attribute>
            <xsl:attribute name="BettermentPercent">
              <!--- Here we using BettermentPct instead of BETT_PCTG-->
              <xsl:value-of select="number(BettermentPct) * 100"/>
            </xsl:attribute>
            <xsl:attribute name="BettermentAmount">
              <xsl:choose>
                <xsl:when test="$BettermentLaborMaterials = '1'">
                  <xsl:choose>
                    <xsl:when test="MOD_LBR_TY='LAR'">
                      <xsl:value-of select="BettermentPct * (LBR_AMT + ($PaintMaterialRate * MOD_LB_HRS))"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="BettermentPct * (ACT_PRICE + LBR_AMT)"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="BettermentPct * ACT_PRICE"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="BettermentTaxable">
              <xsl:choose>
                <xsl:when test="TAX_BTR_IN='1'">Yes</xsl:when>
                <xsl:otherwise>No</xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
          </xsl:if>

          <!--- Save the line number for use later -->
          <!--- Here we use LINE_NO instead of UNQ_SEQ-->
          <xsl:value-of select="hqpxsl:setvar('CurrentLineNumber', number(LINE_NO))"/>

          <!--- Part Information -->
          <!---MISC_AMT Not available in HQ Xml-->
          <!--<xsl:if test="OEM_PARTNO!='APPEAR ALLOWANCE' and OEM_PARTNO!='RELATED PRIOR' and OEM_PARTNO!='UNRELATED PRIOR' and (number(ACT_PRICE) &gt; 0 or number(MISC_AMT) &gt; 0 or (number(ACT_PRICE) = 0 and PRICE_INC='1'))">-->
          <Part>
            <xsl:attribute name="APDPartCategory">
              <xsl:choose>
                <xsl:when test="PART_TYPE='PAS'">Sublet</xsl:when>
                <xsl:when test="TAX_PART='1'">TaxableParts</xsl:when>
                <xsl:when test="TAX_PART='0'">NonTaxableParts</xsl:when>
                <xsl:otherwise/>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="DBPartPrice">
              <xsl:choose>
                <!--DB_PRICE Not Available in HQ Xml-->
                <xsl:when test="PRICE_J='1' and DB_PRICE!='' and number(DB_PRICE)!=0">
                  <!--<xsl:value-of select="number(DB_PRICE)"/>-->
                </xsl:when>
                <xsl:otherwise/>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="PartIncludedFlag">
              <xsl:choose>
                <xsl:when test="PRICE_INC='1'">Yes</xsl:when>
                <xsl:otherwise>No</xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="PartPrice">
              <xsl:if test="ACT_PRICE">
                <xsl:choose>
                  <!--MISC_AMT Not Available in HQ Xml-->
                  <!--<xsl:when test="number(MISC_AMT) &gt; 0">
                    <xsl:value-of select="number(MISC_AMT)"/>
                  </xsl:when>-->
                  <xsl:when test="PRICE_INC='1'">
                    <!--<xsl:value-of select="hqpxsl:evalDbPrice(string(DB_PRICE))"/>-->
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="number(ACT_PRICE)"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:if>
            </xsl:attribute>
            <xsl:attribute name="PartTaxedFlag">
              <xsl:choose>
                <xsl:when test="MISC_TAX='1'">Yes</xsl:when>
                <xsl:when test="TAX_PART='1'">Yes</xsl:when>
                <xsl:otherwise>No</xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="PartType">
              <xsl:choose>
                <xsl:when test="PART_TYPE='PAN'">New</xsl:when>
                <xsl:when test="PART_TYPE='PAL'">LKQ</xsl:when>
                <xsl:when test="PART_TYPE='PAA'">Aftermarket</xsl:when>
                <xsl:when test="PART_TYPE='PAC'">Reconditioned</xsl:when>
                <xsl:when test="PART_TYPE='PAG'">Glass</xsl:when>
                <xsl:when test="PART_TYPE='PAO'">Other</xsl:when>
                <xsl:when test="PART_TYPE='PAS'">Sublet</xsl:when>
                <xsl:when test="PART_TYPE=''">Unspecified</xsl:when>
                <xsl:otherwise></xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="PriceChangedFlag">
              <xsl:choose>
                <!--DB_PRICE Not Available-->
                <xsl:when test="PRICE_J='1' and DB_PRICE!='' and number(DB_PRICE)!=0">Yes</xsl:when>
                <xsl:otherwise>No</xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="Quantity">
              <xsl:value-of select="PART_QTY"/>
            </xsl:attribute>
          </Part>

          <!--- Update running totals with any part price.  Make sure we put it in the right bucket -->
          <xsl:if test="PRICE_INC='0'">
            <xsl:choose>
              <xsl:when test="PART_TYPE='PAS'">
                <xsl:choose>
                  <!--Here we use PART_DESCJ instead of PRT_DSMK_P-->
                  <xsl:when test="number(PART_DESCJ) &gt; 0">
                    <xsl:value-of select="hqpxsl:setvar('DetailSubletTotal', round((hqpxsl:getvar('DetailSubletTotal') + number(MISC_AMT) + (number(MISC_AMT) * number(PART_DESCJ) div 100)) * 100) div 100)"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="hqpxsl:setvar('DetailSubletTotal', round((hqpxsl:getvar('DetailSubletTotal') + number(MISC_AMT)) * 100) div 100)"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:when test="TAX_PART='1'">
                <xsl:value-of select="hqpxsl:setvar('DetailPartTaxTotal', round((hqpxsl:getvar('DetailPartTaxTotal') + number(ACT_PRICE) * number(PART_QTY)) * 100) div 100)"/>
              </xsl:when>
              <xsl:when test="TAX_PART='0'">
                <xsl:value-of select="hqpxsl:setvar('DetailPartNonTaxTotal', round((hqpxsl:getvar('DetailPartNonTaxTotal') + number(ACT_PRICE) * number(PART_QTY)) * 100) div 100)"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="hqpxsl:setvar('DetailUnknownTotal', round((hqpxsl:getvar('DetailUnknownTotal') + number(ACT_PRICE) * number(PART_QTY)) * 100) div 100)"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>

          <!--- Update discount or markup base -->
          <!--Here we use PART_DESCJ instead of PRT_DSMK_P-->
          <xsl:if test="PRICE_INC='0' and PART_TYPE!='PAS' and number(PART_DESCJ) &gt; 0">
            <xsl:value-of select="hqpxsl:setvar('DiscountMarkupBase', round((hqpxsl:getvar('DiscountMarkupBase') + number(ACT_PRICE)* number(PART_QTY)) * 100) div 100)"/>
          </xsl:if>

          <!--- Update Detail Parts Totals -->
          <xsl:if test="PRICE_INC='0'">
            <xsl:choose>
              <xsl:when test="PART_TYPE='PAN'">
                <xsl:value-of select="hqpxsl:setvar('DetailOEMParts', round((hqpxsl:getvar('DetailOEMParts') + number(ACT_PRICE) * number(PART_QTY)) * 100) div 100)"/>
              </xsl:when>
              <xsl:when test="PART_TYPE='PAL'">
                <xsl:value-of select="hqpxsl:setvar('DetailLKQParts', round((hqpxsl:getvar('DetailLKQParts') + number(ACT_PRICE) * number(PART_QTY)) * 100) div 100)"/>
              </xsl:when>
              <xsl:when test="PART_TYPE='PAA'">
                <xsl:value-of select="hqpxsl:setvar('DetailAFMKParts', round((hqpxsl:getvar('DetailAFMKParts') + number(ACT_PRICE) * number(PART_QTY)) * 100) div 100)"/>
              </xsl:when>
              <xsl:when test="PART_TYPE='PAM'">
                <xsl:value-of select="hqpxsl:setvar('DetailRemanParts', round((hqpxsl:getvar('DetailRemanParts') + number(ACT_PRICE) * number(PART_QTY)) * 100) div 100)"/>
              </xsl:when>
              <xsl:when test="PART_TYPE='PAO' and not(MaterialRateTypeCd)">
                <xsl:choose>
                  <xsl:when test="TAX_PART='1'">
                    <xsl:value-of select="hqpxsl:setvar('DetailPartOtherTaxTotal', round((hqpxsl:getvar('DetailPartOtherTaxTotal') + number(ACT_PRICE) * number(PART_QTY)) * 100) div 100)"/>
                  </xsl:when>
                  <xsl:when test="TAX_PART='0'">
                    <xsl:value-of select="hqpxsl:setvar('DetailPartOtherNonTaxTotal', round((hqpxsl:getvar('DetailPartOtherNonTaxTotal') + number(ACT_PRICE) * number(PART_QTY)) * 100) div 100)"/>
                  </xsl:when>
                </xsl:choose>
              </xsl:when>
            </xsl:choose>
          </xsl:if>
          <!--Here we use BettermentPct instead of BETT_PCTG-->
          <!--BETT_AMT is Not Available in HQ Xml-->
          <xsl:if test="PRICE_INC='0' and number(BettermentPct) &gt; 0">
            <xsl:value-of select="hqpxsl:setvar('DetailBettermentTotal', round((hqpxsl:getvar('DetailBettermentTotal') + number(BETT_AMT)) * 100) div 100)"/>
          </xsl:if>

          <xsl:variable name="LineDescription" select="translate(LIN/LINE_DESC, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>

          <xsl:if test="$LineDescription = 'two wheel align' or $LineDescription = 'two wheel alignment' or $LineDescription = 'front wheel alignment' or $LineDescription = '2 wheel align' or $LineDescription = '2 wheel alignment' or $LineDescription = '2-wheel align' or $LineDescription = '2-wheel alignment'">
            <xsl:value-of select="hqpxsl:setvar('Detail2WAlign', round((hqpxsl:getvar('Detail2WAlign') + number(ACT_PRICE)* number(PART_QTY)) * 100) div 100)"/>
          </xsl:if>

          <xsl:if test="$LineDescription = 'four wheel align' or $LineDescription = 'four wheel alignment' or $LineDescription = '4 wheel align' or $LineDescription = '4 wheel alignment' or $LineDescription = '4-wheel align' or $LineDescription = '4-wheel alignment'">
            <xsl:value-of select="hqpxsl:setvar('Detail4WAlign', round((hqpxsl:getvar('Detail4WAlign') + number(ACT_PRICE)* number(PART_QTY)) * 100) div 100)"/>
          </xsl:if>
          <!--</xsl:if>-->

          <!--- Labor Information -->
          <!--<xsl:if test="OEM_PARTNO!='APPEAR ALLOWANCE' and OEM_PARTNO!='RELATED PRIOR' and OEM_PARTNO!='UNRELATED PRIOR' and (MOD_LB_HRS &gt; 0 or (MOD_LB_HRS = 0 and LBR_HRS_J='1'))">-->
          <Labor>
            <xsl:attribute name="DBLaborHours">
              <xsl:choose>
                <xsl:when test="LBR_HRS_J='1' and DB_HRS!=0">
                  <!--<xsl:value-of select="DB_HRS"/>-->
                </xsl:when>
                <xsl:otherwise/>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="LaborAmount">
              <xsl:value-of select="LBR_AMT"/>
            </xsl:attribute>
            <xsl:attribute name="LaborHoursChangedFlag">
              <xsl:choose>
                <!--DB_HRS is Not Available-->
                <xsl:when test="LBR_HRS_J='1' and DB_HRS!=0">Yes</xsl:when>
                <xsl:otherwise>No</xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="LaborHours">
              <xsl:choose>
                <!--LBR_INC is Not Available-->
                <xsl:when test="LBR_INC='1'">
                  <!--<xsl:value-of select="DB_HRS"/>-->
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="MOD_LB_HRS"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="LaborIncludedFlag">
              <xsl:choose>
                <xsl:when test="LBR_INC='1'">Yes</xsl:when>
                <xsl:otherwise>No</xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="LaborTaxedFlag">
              <xsl:choose>
                <xsl:when test="LBR_TAX='1'">Yes</xsl:when>
                <xsl:otherwise>No</xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="LaborType">
              <xsl:choose>
                <xsl:when test="MOD_LBR_TY='LAB'">Body</xsl:when>
                <xsl:when test="MOD_LBR_TY='LAD'">Mechanical</xsl:when>
                <xsl:when test="MOD_LBR_TY='LAE'">Mechanical</xsl:when>
                <xsl:when test="MOD_LBR_TY='LAR'">Refinish</xsl:when>
                <xsl:when test="MOD_LBR_TY='LAS'">Frame</xsl:when>
                <xsl:when test="MOD_LBR_TY='LAF'">Frame</xsl:when>
                <xsl:when test="MOD_LBR_TY='LAM'">Mechanical</xsl:when>
                <xsl:when test="MOD_LBR_TY='LAG'">Body</xsl:when>
                <xsl:when test="MOD_LBR_TY='LA1'">User Defined</xsl:when>
                <xsl:when test="MOD_LBR_TY='LA2'">User Defined</xsl:when>
                <xsl:when test="MOD_LBR_TY='LA3'">User Defined</xsl:when>
                <xsl:when test="MOD_LBR_TY='LA4'">User Defined</xsl:when>
                <xsl:otherwise></xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
          </Labor>

          <!--- Update running totals with any labor hours.  Make sure we put it in the right bucket -->
          <xsl:if test="LBR_INC='0' and MOD_LB_HRS &gt; 0">
            <xsl:choose>
              <xsl:when test="MOD_LBR_TY='LAB'">
                <xsl:choose>
                  <xsl:when test="LBR_OP='OP3' or LBR_OP='OP8' or LBR_OP='OP9' or LBR_OP='OP10'">
                    <xsl:value-of select="hqpxsl:setvar('DetailBodyLaborRepairHours', round((hqpxsl:getvar('DetailBodyLaborRepairHours') + number(MOD_LB_HRS)) * 10) div 10)"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="hqpxsl:setvar('DetailBodyLaborReplaceHours', round((hqpxsl:getvar('DetailBodyLaborReplaceHours') + number(MOD_LB_HRS)) *  10) div 10)"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:when test="MOD_LBR_TY='LAF' or MOD_LBR_TY='LAS'">
                <xsl:choose>
                  <xsl:when test="LBR_OP='OP3' or LBR_OP='OP8' or LBR_OP='OP9' or LBR_OP='OP10'">
                    <xsl:value-of select="hqpxsl:setvar('DetailFrameLaborRepairHours', round((hqpxsl:getvar('DetailFrameLaborRepairHours') + number(MOD_LB_HRS)) * 10) div 10)"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="hqpxsl:setvar('DetailFrameLaborReplaceHours', round((hqpxsl:getvar('DetailFrameLaborReplaceHours') + number(MOD_LB_HRS)) * 10) div 10)"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:when test="MOD_LBR_TY='LAD' or MOD_LBR_TY='LAE' or MOD_LBR_TY='LAM'">
                <xsl:choose>
                  <xsl:when test="LBR_OP='OP3' or LBR_OP='OP8' or LBR_OP='OP9' or LBR_OP='OP10'">
                    <xsl:value-of select="hqpxsl:setvar('DetailMechLaborRepairHours', round((hqpxsl:getvar('DetailMechLaborRepairHours') + number(MOD_LB_HRS)) * 10) div 10)"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="hqpxsl:setvar('DetailMechLaborReplaceHours', round((hqpxsl:getvar('DetailMechLaborReplaceHours') + number(MOD_LB_HRS)) * 10) div 10)"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:when test="MOD_LBR_TY='LAR'">
                <xsl:choose>
                  <!--LINE_NO instead of UNQ_SEQ-->
                  <xsl:when test="LINE_DESC = ../LIN[number(LINE_NO)= hqpxsl:getvar('CurrentLineNumber') - 1]/LINE_DESC">
                    <xsl:choose>
                      <!--LINE_NO instead of UNQ_SEQ-->
                      <xsl:when test="../LIN[number(LINE_NO)= hqpxsl:getvar('CurrentLineNumber') - 1]/LBR_OP='OP3' or ../LIN[number(LINE_NO)= hqpxsl:getvar('CurrentLineNumber') - 1]/LBR_OP='OP8' or ../LIN[number(LINE_NO)= hqpxsl:getvar('CurrentLineNumber') - 1]/LBR_OP='OP9' or ../LIN[number(LINE_NO)= hqpxsl:getvar('CurrentLineNumber') - 1]/LBR_OP='OP10'">
                        <xsl:value-of select="hqpxsl:setvar('DetailPaintLaborRepairHours', round((hqpxsl:getvar('DetailPaintLaborRepairHours') + number(MOD_LB_HRS)) * 10) div 10)"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="hqpxsl:setvar('DetailPaintLaborReplaceHours', round((hqpxsl:getvar('DetailPaintLaborReplaceHours') + number(MOD_LB_HRS)) *  10) div 10)"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="hqpxsl:setvar('DetailPaintLaborRepairHours', round((hqpxsl:getvar('DetailPaintLaborRepairHours') + number(MOD_LB_HRS)) *  10) div 10)"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
            </xsl:choose>
          </xsl:if>
          <!--</xsl:if>-->
        </DetailLine>
      </xsl:for-each>
    </Detail>
  </xsl:template>

  <!--- Transform Profile Information -->
  <xsl:template match="EMSEstimateSpecial" mode="Profile">
    <Profiles>
      <ProfileItem Category="Parts" Type="OEM">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFP[PRT_TYPE='PAN']/PRT_TAX_IN='1'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:if test="PFP[PRT_TYPE='PAN']/PRT_TAX_IN='1'">
          <xsl:attribute name="TaxPercent">
            <!--<xsl:value-of select="PFP[PRT_TYPE='PAN']/PRT_TAX_RT"/>-->
          </xsl:attribute>
        </xsl:if>
        <xsl:attribute name="MarkupPercent">
          <!--<xsl:value-of select="PFP[PRT_TYPE='PAN']/PRT_MKUPP"/>-->
          <xsl:choose>
            <xsl:when test="number(STL[TTL_TYPECD='PAN']/T_MKUPAMT &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DiscountPercent">
          <!--<xsl:value-of select="PFP[PRT_TYPE='PAN']/PRT_DISCP"/>-->
          <xsl:choose>
            <xsl:when test="number(STL[TTL_TYPECD='PAN']/T_DISCAMT &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:when test="number(STL[TTL_TYPECD='PAN']/NT_DISC &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </ProfileItem>
      <ProfileItem Category="Parts" Type="LKQ">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFP[PRT_TYPE='PAL']/PRT_TAX_IN='1'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:if test="PFP[PRT_TYPE='PAL']/PRT_TAX_IN='1'">
          <xsl:attribute name="TaxPercent">
            <!--<xsl:value-of select="PFP[PRT_TYPE='PAL']/PRT_TAX_RT"/>-->
          </xsl:attribute>
        </xsl:if>
        <xsl:attribute name="MarkupPercent">
          <!--<xsl:value-of select="PFP[PRT_TYPE='PAL']/PRT_MKUPP"/>-->
          <xsl:choose>
            <xsl:when test="number(STL[TTL_TYPECD='PAL']/T_MKUPAMT &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DiscountPercent">
          <!--<xsl:value-of select="PFP[PRT_TYPE='PAL']/PRT_DISCP"/>-->
          <xsl:choose>
            <xsl:when test="number(STL[TTL_TYPECD='PAL']/T_DISCAMT &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:when test="number(STL[TTL_TYPECD='PAL']/NT_DISC &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </ProfileItem>
      <ProfileItem Category="Parts" Type="Aftermarket">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFP[PRT_TYPE='PAA']/PRT_TAX_IN='1'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:if test="PFP[PRT_TYPE='PAA']/PRT_TAX_IN='1'">
          <xsl:attribute name="TaxPercent">
            <!--<xsl:value-of select="PFP[PRT_TYPE='PAA']/PRT_TAX_RT"/>-->
          </xsl:attribute>
        </xsl:if>
        <xsl:attribute name="MarkupPercent">
          <!--<xsl:value-of select="PFP[PRT_TYPE='PAA']/PRT_MKUPP"/>-->
          <xsl:choose>
            <xsl:when test="number(STL[TTL_TYPECD='PAA']/T_MKUPAMT &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DiscountPercent">
          <!--<xsl:value-of select="PFP[PRT_TYPE='PAA']/PRT_DISCP"/>-->
          <xsl:choose>
            <xsl:when test="number(STL[TTL_TYPECD='PAA']/T_DISCAMT &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:when test="number(STL[TTL_TYPECD='PAA']/NT_DISC &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </ProfileItem>
      <ProfileItem Category="Parts" Type="Recycled">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFP[PRT_TYPE='PAM']/PRT_TAX_IN='1'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:if test="PFP[PRT_TYPE='PAM']/PRT_TAX_IN='1'">
          <xsl:attribute name="TaxPercent">
            <!--<xsl:value-of select="PFP[PRT_TYPE='PAM']/PRT_TAX_RT"/>-->
          </xsl:attribute>
        </xsl:if>
        <xsl:attribute name="MarkupPercent">
          <!--<xsl:value-of select="PFP[PRT_TYPE='PAM']/PRT_MKUPP"/>-->
          <xsl:choose>
            <xsl:when test="number(STL[TTL_TYPECD='PAM']/T_MKUPAMT &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DiscountPercent">
          <!--<xsl:value-of select="PFP[PRT_TYPE='PAM']/PRT_DISCP"/>-->
          <xsl:choose>
            <xsl:when test="number(STL[TTL_TYPECD='PAM']/T_DISCAMT &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:when test="number(STL[TTL_TYPECD='PAM']/NT_DISC &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </ProfileItem>
      <ProfileItem Category="Parts" Type="Front Glass">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFP[PRT_TYPE='PAGF']/PRT_TAX_IN='1'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:if test="PFP[PRT_TYPE='PAGF']/PRT_TAX_IN='1'">
          <xsl:attribute name="TaxPercent">
            <!--<xsl:value-of select="PFP[PRT_TYPE='PAGF']/PRT_TAX_RT"/>-->
          </xsl:attribute>
        </xsl:if>
        <xsl:attribute name="MarkupPercent">
          <!--<xsl:value-of select="PFP[PRT_TYPE='PAGF']/PRT_MKUPP"/>-->
          <xsl:choose>
            <xsl:when test="number(STL[TTL_TYPECD='PAGF']/T_MKUPAMT &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DiscountPercent">
          <!--<xsl:value-of select="PFP[PRT_TYPE='PAGF']/PRT_DISCP"/>-->
          <xsl:choose>
            <xsl:when test="number(STL[TTL_TYPECD='PAGF']/T_DISCAMT &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:when test="number(STL[TTL_TYPECD='PAGF']/NT_DISC &gt; 0)">
              <!--<xsl:value-of select=""/>-->
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </ProfileItem>
      <ProfileItem Category="Labor" Type="Body">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFL[LBR_TYPE='LAB']/LBR_TAX_IN='0'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:if test="PFL[LBR_TYPE='LAB']/LBR_TAX_IN='0'">
          <xsl:attribute name="TaxPercent">
            <xsl:value-of select="PFL[LBR_TYPE='LAB']/LBR_TAXP"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:attribute name="Rate">
          <xsl:value-of select="PFL[LBR_TYPE='LAB']/LBR_RATE"/>
        </xsl:attribute>
      </ProfileItem>
      <ProfileItem Category="Labor" Type="Frame">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFL[LBR_TYPE='LAF']/LBR_TAX_IN='0'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:if test="PFL[LBR_TYPE='LAF']/LBR_TAX_IN='0'">
          <xsl:attribute name="TaxPercent">
            <xsl:value-of select="PFL[LBR_TYPE='LAF']/LBR_TAXP"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:attribute name="Rate">
          <xsl:value-of select="PFL[LBR_TYPE='LAF']/LBR_RATE"/>
        </xsl:attribute>
      </ProfileItem>
      <ProfileItem Category="Labor" Type="Mechanical">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFL[LBR_TYPE='LAM']/LBR_TAX_IN='0'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:if test="PFL[LBR_TYPE='LAM']/LBR_TAX_IN='0'">
          <xsl:attribute name="TaxPercent">
            <xsl:value-of select="PFL[LBR_TYPE='LAM']/LBR_TAXP"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:attribute name="Rate">
          <xsl:value-of select="PFL[LBR_TYPE='LAM']/LBR_RATE"/>
        </xsl:attribute>
      </ProfileItem>
      <ProfileItem Category="Labor" Type="Refinish">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFL[LBR_TYPE='LAR']/LBR_TAX_IN='0'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:if test="PFL[LBR_TYPE='LAR']/LBR_TAX_IN='0'">
          <xsl:attribute name="TaxPercent">
            <xsl:value-of select="PFL[LBR_TYPE='LAR']/LBR_TAXP"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:attribute name="Rate">
          <xsl:value-of select="PFL[LBR_TYPE='LAR']/LBR_RATE"/>
        </xsl:attribute>
      </ProfileItem>
      <ProfileItem Category="Materials" Type="Paint1Stage">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFM[MATL_TYPE='MAPA']/TAX_IND='1'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:if test="PFM[MATL_TYPE='MAPA']/TAX_IND='1'">
          <xsl:attribute name="TaxPercent">
            <!--<xsl:value-of select="PFM[MATL_TYPE='MAPA']/MAT_TAXP"/>-->
          </xsl:attribute>
        </xsl:if>
        <xsl:attribute name="Rate">
          <xsl:value-of select="PFM[MATL_TYPE='MAPA']/CAL_LBRRTE"/>
        </xsl:attribute>
        <xsl:attribute name="MaxDollars">
          <!--<xsl:value-of select="PFM[MATL_TYPE='MAPA']/CAL_MAXDLR"/>-->
        </xsl:attribute>
        <xsl:attribute name="MaxHours">
          <!--<xsl:value-of select="PFM[MATL_TYPE='MAPA']/CAL_LBRMAX"/>-->
        </xsl:attribute>
        <xsl:attribute name="CalcMethod">
          <!--<xsl:value-of select="PFM[MATL_TYPE='MAPA']/CAL_CODE"/>-->
        </xsl:attribute>
      </ProfileItem>
      <ProfileItem Category="Materials" Type="Paint2Stage" Taxable="No" Rate="" MaxDollars="" MaxHours="" CalcMethod=""/>
      <ProfileItem Category="Materials" Type="Paint3Stage" Taxable="No" Rate="" MaxDollars="" MaxHours="" CalcMethod=""/>
    </Profiles>
  </xsl:template>

  <!--- Transform Summary Information -->
  <xsl:template match="EMSEstimateSpecial" mode="Summary">
    <!--- First we need to get together all the values we need to build the summary -->

    <!--- Material Costs -->
    <!--- Body Materials Amount  -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='MAHW']/T_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('BodyMaterialAmount', number(STL[TTL_TYPECD='MAHW']/T_AMT))"/>
      </xsl:when>
      <xsl:when test="STL[TTL_TYPECD='MAHW']/NT_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('BodyMaterialAmount', number(STL[TTL_TYPECD='MAHW']/NT_AMT))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('BodyMaterialAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Body Material Rate -->
    <xsl:choose>
      <xsl:when test="PFM[MATL_TYPE='MAHW']/CAL_LBRRTE != ''">
        <xsl:value-of select="hqpxsl:setvar('BodyMaterialRate', number(PFM[MATL_TYPE='MAHW']/CAL_LBRRTE))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('BodyMaterialRate', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Body Material Hours -->
    <xsl:choose>
      <xsl:when test="hqpxsl:getvar('BodyMaterialRate') &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('BodyMaterialHours', sum(STL[TTL_TYPECD='LAB' or TTL_TYPECD='LAM' or TTL_TYPECD='LAF']/TTL_HRS))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('BodyMaterialHours', 0)"/>
      </xsl:otherwise>
    </xsl:choose>


    <!--- Paint Materials Amount -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='MAPA']/T_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('PaintMaterialAmount', number(STL[TTL_TYPECD='MAPA']/T_AMT))"/>
      </xsl:when>
      <xsl:when test="STL[TTL_TYPECD='MAPA']/NT_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('PaintMaterialAmount', number(STL[TTL_TYPECD='MAPA']/NT_AMT))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('PaintMaterialAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Paint Material Rate -->
    <xsl:choose>
      <xsl:when test="PFM[MATL_TYPE='MAPA']/CAL_LBRRTE != ''">
        <xsl:value-of select="hqpxsl:setvar('PaintMaterialRate', number(PFM[MATL_TYPE='MAPA']/CAL_LBRRTE))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('PaintMaterialRate', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Paint Material Hours -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='LAR']/TTL_HRS &gt; 0 and hqpxsl:getvar('PaintMaterialRate') &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('PaintMaterialHours', number(STL[TTL_TYPECD='LAR']/TTL_HRS))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('PaintMaterialHours', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Total Materials -->
    <xsl:value-of select="hqpxsl:setvar('TotalMaterials', round((hqpxsl:getvar('BodyMaterialAmount') + hqpxsl:getvar('PaintMaterialAmount')) * 100) div 100)"/>

    <!--- Part Costs -->

    <!--- Discount Amount (Parts) -->
    <xsl:choose>
      <xsl:when test="number(STL[TTL_TYPECD='PAT']/NT_DISC) &lt; 0">
        <xsl:value-of select="hqpxsl:setvar('TotalDiscountAmount', number(STL[TTL_TYPECD='PAT']/NT_DISC))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('TotalDiscountAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Discount Percent (Parts) -->
    <xsl:choose>
      <xsl:when test="hqpxsl:getvar('TotalDiscountAmount')='0'">
        <xsl:value-of select="hqpxsl:setvar('DiscountPct', 0)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('DiscountPct', round((-1 * hqpxsl:getvar('TotalDiscountAmount') div hqpxsl:getvar('DiscountMarkupBase') * 100) * 1000) div 1000)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Markup Amount (Parts) -->
    <xsl:choose>
      <xsl:when test="number(STL[TTL_TYPECD='PAT']/NT_MKUP) &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('TotalMarkupAmount', number(STL[TTL_TYPECD='PAT']/NT_MKUP))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('TotalMarkupAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Markup Percent (Parts) -->
    <xsl:choose>
      <xsl:when test="hqpxsl:getvar('TotalMarkupAmount')='0' or hqpxsl:getvar('DiscountMarkupBase')='0'">
        <xsl:value-of select="hqpxsl:setvar('MarkupPct', 0)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('MarkupPct', round((hqpxsl:getvar('TotalMarkupAmount') div hqpxsl:getvar('DiscountMarkupBase') * 100) * 1000) div 1000)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Total Taxable Parts -->
    <xsl:choose>
      <xsl:when test="round(STL[TTL_TYPECD='PAT']/T_AMT) &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('TotalTaxParts', round((STL[TTL_TYPECD='PAT']/T_AMT)  * 100) div 100)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('TotalTaxParts', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

   <!-- <xsl:value-of select="hqpxsl:setvar('TotalTaxParts', round((sum(STL[TTL_TYPE = 'PA' and TTL_TYPECD !='PAT' and TTL_TYPECD !='PAS']/T_AMT) - hqpxsl:getvar('TotalDiscountAmount') - hqpxsl:getvar('TotalMarkupAmount'))  * 100) div 100)"/> -->

    <xsl:value-of select="hqpxsl:setvar('TotalTaxMaterials', round((sum(STL[TTL_TYPECD ='MAHW' or TTL_TYPECD ='MAPA']/T_AMT))  * 100) div 100)"/>

    <xsl:value-of select="hqpxsl:setvar('TotalTaxPartsMaterials', round((hqpxsl:getvar('TotalTaxParts') + hqpxsl:getvar('TotalTaxMaterials'))  * 100) div 100)"/>

    <xsl:value-of select="hqpxsl:setvar('TotalTaxLabor', round(sum(STL[TTL_TYPE = 'LA' and TTL_TYPECD ='LAT']/T_AMT)  * 100) div 100)"/>

    <!--- Total Non-Taxable Parts -->
    <xsl:choose>
      <xsl:when test="round(STL[TTL_TYPECD='PAT']/NT_AMT) &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('TotalNonTaxParts', round((STL[TTL_TYPECD='PAT']/NT_AMT)  * 100) div 100)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('TotalNonTaxParts', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

   <!--  <xsl:value-of select="hqpxsl:setvar('TotalNonTaxParts', round((sum(STL[TTL_TYPE = 'PA' and TTL_TYPECD !='PAT' and TTL_TYPECD !='PAS']/NT_AMT) - hqpxsl:getvar('TotalDiscountAmount') - hqpxsl:getvar('TotalMarkupAmount'))  * 100) div 100)"/> -->

    <!--- Other Taxable Parts (Taxable PAO) -->
    <!--<xsl:value-of select="hqpxsl:setvar('TotalOtherTaxParts', 0)"/>-->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD = 'PAO']/T_AMT">
        <xsl:value-of select="hqpxsl:setvar('TotalOtherTaxParts', round(sum(STL[TTL_TYPECD = 'PAO']/T_AMT)  * 100) div 100)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('TotalOtherTaxParts', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Other NonTaxable Parts (NonTaxable PAO) 
    <xsl:value-of select="hqpxsl:setvar('TotalOtherNonTaxParts', 0)"/> -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD = 'PAO']/NT_AMT">
        <xsl:value-of select="hqpxsl:setvar('TotalOtherNonTaxParts', round(sum(STL[TTL_TYPECD = 'PAO']/NT_AMT)  * 100) div 100)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('TotalOtherNonTaxParts', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Total OEM Parts  -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD = 'PAN' or TTL_TYPECD='PAG' or TTL_TYPECD='PAP']/TTL_AMT">
        <xsl:value-of select="hqpxsl:setvar('TotalOEMParts', round(sum(STL[TTL_TYPECD = 'PAN' or TTL_TYPECD='PAG' or TTL_TYPECD='PAP']/TTL_AMT)  * 100) div 100)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('TotalOEMParts', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Total LKQ Parts  -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD = 'PAL']/TTL_AMT">
        <xsl:value-of select="hqpxsl:setvar('TotalLKQParts', round(sum(STL[TTL_TYPECD = 'PAL']/TTL_AMT)  * 100) div 100)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('TotalLKQParts', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Total AFMK Parts  -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD = 'PAA']/TTL_AMT">
        <xsl:value-of select="hqpxsl:setvar('TotalAFMKParts', round(sum(STL[TTL_TYPECD = 'PAA']/TTL_AMT)  * 100) div 100)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('TotalAFMKParts', 0)"/>
      </xsl:otherwise>
    </xsl:choose>
    
    <!--- Total Reman Parts  --> 
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD = 'PAM' or TTL_TYPECD='PAR' or TTL_TYPECD='PAS' or TTL_TYPECD='PASL']/TTL_AMT">
        <xsl:value-of select="hqpxsl:setvar('TotalRemanParts', round(sum(STL[TTL_TYPECD = 'PAM' or TTL_TYPECD='PAR' or TTL_TYPECD='PAS' or TTL_TYPECD='PASL']/TTL_AMT)  * 100) div 100)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('TotalRemanParts', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Total Parts -->
    <xsl:choose>
      <xsl:when test="round(STL[TTL_TYPECD='PAT']/TTL_AMT) &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('TotalParts', round((STL[TTL_TYPECD='PAT']/TTL_AMT)  * 100) div 100)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('TotalParts', 0)"/>
      </xsl:otherwise>
    </xsl:choose>


    <!-- <xsl:value-of select="hqpxsl:setvar('TotalParts', round((hqpxsl:getvar('TotalTaxParts') + hqpxsl:getvar('TotalNonTaxParts') + hqpxsl:getvar('TotalDiscountAmount') + hqpxsl:getvar('TotalMarkupAmount') + hqpxsl:getvar('TotalOtherTaxParts') + hqpxsl:getvar('TotalOtherNonTaxParts')) * 100) div 100)"/> -->


    <!--- Total Parts and Materials -->
    <xsl:value-of select="hqpxsl:setvar('TotalPartsMaterials', round((hqpxsl:getvar('TotalParts') + hqpxsl:getvar('TotalMaterials')) * 100) div 100)"/>

    <!-- Labor Costs -->

    <!--- Body Labor Amount -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='LAB']/T_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('BodyLaborAmount', number(STL[TTL_TYPECD='LAB']/T_AMT))"/>
      </xsl:when>
      <xsl:when test="STL[TTL_TYPECD='LAB']/NT_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('BodyLaborAmount', number(STL[TTL_TYPECD='LAB']/NT_AMT))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('BodyLaborAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Body Labor Hours -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='LAB']/T_HRS &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('BodyLaborHours', number(STL[TTL_TYPECD='LAB']/T_HRS))"/>
      </xsl:when>
      <xsl:when test="STL[TTL_TYPECD='LAB']/NT_HRS &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('BodyLaborHours', number(STL[TTL_TYPECD='LAB']/NT_HRS))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('BodyLaborHours', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Body Labor Rate -->
    <xsl:choose>
      <xsl:when test="PFL[LBR_TYPE='LAB']/LBR_RATE != ''">
        <xsl:value-of select="hqpxsl:setvar('BodyLaborRate', number(PFL[LBR_TYPE='LAB']/LBR_RATE))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('BodyLaborRate', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Frame Labor Amount -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/T_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('FrameLaborAmount', sum(STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/T_AMT))"/>
      </xsl:when>
      <xsl:when test="STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/NT_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('FrameLaborAmount', sum(STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/NT_AMT))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('FrameLaborAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Frame Labor Hours -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/T_HRS &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('FrameLaborHours', sum(STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/T_HRS))"/>
      </xsl:when>
      <xsl:when test="STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/NT_HRS &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('FrameLaborHours', sum(STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/NT_HRS))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('FrameLaborHours', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Frame Labor Rate -->
    <xsl:choose>
      <xsl:when test="PFL[LBR_TYPE='LAF']/LBR_RATE != ''">
        <xsl:value-of select="hqpxsl:setvar('FrameLaborRate', number(PFL[LBR_TYPE='LAF']/LBR_RATE))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('FrameLaborRate', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!---Mechanical Labor Amount -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/T_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('MechLaborAmount', sum(STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/T_AMT))"/>
      </xsl:when>
      <xsl:when test="STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/NT_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('MechLaborAmount', sum(STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/NT_AMT))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('MechLaborAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Mechanical Labor Hours -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/T_HRS &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('MechLaborHours', sum(STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/T_HRS))"/>
      </xsl:when>
      <xsl:when test="STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/NT_HRS &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('MechLaborHours', sum(STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/NT_HRS))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('MechLaborHours', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Mechanical Labor Rate -->
    <xsl:choose>
      <xsl:when test="PFL[LBR_TYPE='LAM']/LBR_RATE != ''">
        <xsl:value-of select="hqpxsl:setvar('MechLaborRate', number(PFL[LBR_TYPE='LAM']/LBR_RATE))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('MechLaborRate', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Paint Labor Amount -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='LAR']/T_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('PaintLaborAmount', number(STL[TTL_TYPECD='LAR']/T_AMT))"/>
      </xsl:when>
      <xsl:when test="STL[TTL_TYPECD='LAR']/NT_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('PaintLaborAmount', number(STL[TTL_TYPECD='LAR']/NT_AMT))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('PaintLaborAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Paint Labor Hours -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='LAR']/T_HRS &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('PaintLaborHours', number(STL[TTL_TYPECD='LAR']/T_HRS))"/>
      </xsl:when>
      <xsl:when test="STL[TTL_TYPECD='LAR']/NT_HRS &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('PaintLaborHours', number(STL[TTL_TYPECD='LAR']/NT_HRS))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('PaintLaborHours', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Paint Labor Rate -->
    <xsl:choose>
      <xsl:when test="PFL[LBR_TYPE='LAR']/LBR_RATE != ''">
        <xsl:value-of select="hqpxsl:setvar('PaintLaborRate', number(PFL[LBR_TYPE='LAR']/LBR_RATE))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('PaintLaborRate', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Total Labor Amount -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='LAT']/TTL_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('TotalLaborAmount', number(STL[TTL_TYPECD='LAT']/TTL_AMT))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('TotalLaborAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Other Labor (This is the difference between the Total Labor retrieved above and that calculated from the other values) -->
    <xsl:value-of select="hqpxsl:setvar('TotalOtherLabor', round((hqpxsl:getvar('TotalLaborAmount') - (hqpxsl:getvar('BodyLaborAmount') + hqpxsl:getvar('FrameLaborAmount') + hqpxsl:getvar('MechLaborAmount') + hqpxsl:getvar('PaintLaborAmount'))) * 100) div 100)"/>


    <!--- Additional Charges -->

    <!--- Storage Amount -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPE='OTST']/T_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('StorageAmount', number(STL[TTL_TYPE='OTST']/T_AMT))"/>
      </xsl:when>
      <xsl:when test="STL[TTL_TYPE='OTST']/NT_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('StorageAmount', number(STL[TTL_TYPE='OTST']/NT_AMT))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('StorageAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Sublet Amount -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPE='OTSL']/T_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('SubletAmount', number(STL[TTL_TYPE='OTSL']/T_AMT))"/>
      </xsl:when>
      <xsl:when test="STL[TTL_TYPE='OTSL']/NT_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('SubletAmount', number(STL[TTL_TYPE='OTSL']/NT_AMT))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('SubletAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Towing Amount -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPE='OTTW']/T_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('TowingAmount', number(STL[TTL_TYPE='OTTW']/T_AMT))"/>
      </xsl:when>
      <xsl:when test="STL[TTL_TYPE='OTTW']/NT_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('TowingAmount', number(STL[TTL_TYPE='OTTW']/NT_AMT))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('TowingAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Other Additional Charges -->
    <!--<xsl:value-of select="hqpxsl:setvar('OtherAddlChargesAmount', sum(STL[starts-with(TTL_TYPE, 'OT') and TTL_TYPE!='OTOC' and TTL_TYPE!='OTSL' and TTL_TYPE!='OTST' and TTL_TYPE!='OTTW' and TTL_TYPE!='OTAA']/TTL_AMT))"/>-->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPE = 'OTAC' and TTL_TYPE and TTL_TYPE !='OTST' and TTL_TYPE !='OTTW']/TTL_AMT">
        <xsl:value-of select="hqpxsl:setvar('OtherAddlChargesAmount', number(STL[TTL_TYPE = 'OTAC']/TTL_AMT))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('OtherAddlChargesAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Total Additional Charges (Storage, Sublet, Towing, and Other added together) -->
    <xsl:value-of select="hqpxsl:setvar('TotalAddlCharges', hqpxsl:getvar('StorageAmount') + hqpxsl:getvar('SubletAmount') + hqpxsl:getvar('TowingAmount') + hqpxsl:getvar('OtherAddlChargesAmount'))"/>


    <!--- Subtotal -->
    <xsl:value-of select="hqpxsl:setvar('SubtotalAmount', round((hqpxsl:getvar('TotalPartsMaterials') + hqpxsl:getvar('TotalLaborAmount') + hqpxsl:getvar('TotalAddlCharges')) * 100) div 100)"/>


    <!--- Parts & Materials Tax  -->

    <!--- Sales Tax Base -->
    <xsl:choose>
      <!--<xsl:when test="STL[TTL_TYPECD='PAT']/T_AMT &gt; 0 and PFM[1]/TAX_IND='0'">
        <xsl:value-of select="hqpxsl:setvar('SalesTaxBasePM', hqpxsl:getvar('TotalTaxParts'))"/>
      </xsl:when>-->
      <xsl:when test="STL[TTL_TYPECD='PAT']/T_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('SalesTaxBasePM',  hqpxsl:getvar('TotalTaxPartsMaterials'))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('SalesTaxBasePM', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Sales Tax Percent -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='PAT']/T_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('SalesTaxPctPM', number(TTL[1]/PartTaxRate))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('SalesTaxPctPM', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Sales Tax Amount -->
    <xsl:choose>
      <xsl:when test="hqpxsl:getvar('SalesTaxBasePM')='0'">
        <xsl:value-of select="hqpxsl:setvar('SalesTaxAmountPM', '0')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('SalesTaxAmountPM', round(hqpxsl:getvar('SalesTaxBasePM') * (hqpxsl:getvar('SalesTaxPctPM') div 100) * 100) div 100)"/>
      </xsl:otherwise>
    </xsl:choose>


    <!--- Labor Tax  -->

    <!--- Sales Tax Base -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='LAT']/T_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('SalesTaxBaseLabor', number(TTL[1]/LaborTaxRate))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('SalesTaxBaseLabor', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Sales Tax Percent -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPECD='LAT']/T_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('SalesTaxPctLabor', number(PFL[LBR_TYPE = 'LAB']/LBR_RATE))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('SalesTaxPctLabor', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Sales Tax Amount -->
    <xsl:choose>
      <xsl:when test="hqpxsl:getvar('SalesTaxBaseLabor')='0'">
        <xsl:value-of select="hqpxsl:setvar('SalesTaxAmountLabor', '0')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('SalesTaxAmountLabor', round(hqpxsl:getvar('SalesTaxBaseLabor') * (hqpxsl:getvar('SalesTaxPctLabor') div 100) * 100) div 100)"/>
      </xsl:otherwise>
    </xsl:choose>


    <!--- Total Tax Amount -->
    <xsl:choose>
      <xsl:when test="TTL/G_TAX">
        <xsl:value-of select="hqpxsl:setvar('TotalTaxAmount', number(TTL/G_TAX))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('TotalTaxAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Other Taxes -->
    <xsl:value-of select="hqpxsl:setvar('OtherTaxAmount', round((hqpxsl:getvar('TotalTaxAmount') - hqpxsl:getvar('SalesTaxAmountPM') - hqpxsl:getvar('SalesTaxAmountLabor')) * 100) div 100)"/>

    <!--- Total Repair Cost -->
    <xsl:value-of select="hqpxsl:setvar('RepairTotalAmount', round((hqpxsl:getvar('SubtotalAmount') + hqpxsl:getvar('TotalTaxAmount')) * 100) div 100)"/>


    <!--- Adjustments -->

    <!--- Appearance Allowance -->
    <xsl:choose>
      <xsl:when test="TTL/G_AA_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('AppearanceAllowanceAmount', -1 * number(TTL/G_AA_AMT))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('AppearanceAllowanceAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Betterment -->
    <xsl:variable name="BettermentValue" select="number(round(number(TTL/TaxableBettermentAmt) * (1 + (number(TTL/PartTaxRate) div 100)) + number(TTL/NonTaxableBettermentAmt) + number(TTL/TaxableBettermentLaborAmt) * (1 + (TTL/LaborTaxRate) div 100) + number(TTL/NonTaxableBettermentLaborAmt) + number(TTL/TaxableBettermentADCAmt) * (1 + (TTL/OtherTaxRate) div 100) + number(TTL/NonTaxableBettermentADCAmt)))"></xsl:variable>
    <xsl:choose>
      <xsl:when test="$BettermentValue &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('BettermentAmount', $BettermentValue)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('BettermentAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Deductible -->
    <xsl:choose>
      <xsl:when test="TTL/G_DED_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('DeductibleAmount', number(TTL/G_DED_AMT))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('DeductibleAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Other Adjustment Amount (For ADP, this is a special adjustment on some ADP estimates taken after taxes -->
    <xsl:choose>
      <xsl:when test="STL[TTL_TYPE='OTOC']/TTL_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('OtherAdjustmentAmount', number(STL[TTL_TYPE='OTOC']/TTL_AMT ))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('OtherAdjustmentAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>

    <!--- Total Adjustment (Deductible, Appearance Allowance, Betterment, and Other added together -->
    <xsl:value-of select="hqpxsl:setvar('TotalAdjustmentAmount', round((hqpxsl:getvar('AppearanceAllowanceAmount') + hqpxsl:getvar('BettermentAmount') + hqpxsl:getvar('DeductibleAmount') + hqpxsl:getvar('OtherAdjustmentAmount')) * 100) div 100)"/>


    <!--- Net Total -->
    <xsl:choose>
      <xsl:when test="TTL/N_TTL_AMT &gt; 0">
        <xsl:value-of select="hqpxsl:setvar('NetTotalAmount', number(TTL/G_TTL_AMT)- number(TTL/G_DED_AMT))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="hqpxsl:setvar('NetTotalAmount', 0)"/>
      </xsl:otherwise>
    </xsl:choose>


    <!--- Variance Amount  (This is a "bit bucket" to capture variances between what I calculate and what the "real" estimate total is) -->
    <xsl:value-of select="hqpxsl:setvar('ZZVarianceAmount', round((hqpxsl:getvar('RepairTotalAmount') - hqpxsl:getvar('AppearanceAllowanceAmount') - hqpxsl:getvar('BettermentAmount') - hqpxsl:getvar('DeductibleAmount') - hqpxsl:getvar('OtherAdjustmentAmount') - hqpxsl:getvar('NetTotalAmount')) * 100) div 100)"/>


    <!--- Continue building output -->
    <Summary>
      <LineItem Type="PartsTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="Yes">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalTaxParts')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalTaxParts')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="PartsNonTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="No">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalNonTaxParts')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalNonTaxParts')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="PartsMarkup" TransferLine="Yes" Category="PC" Note="">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('TotalTaxParts') &gt;= hqpxsl:getvar('TotalNonTaxParts')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalMarkupAmount')"/>
        </xsl:attribute>
        <xsl:attribute name="Percent">
          <xsl:value-of select="hqpxsl:getvar('MarkupPct')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('TotalMarkupAmount') = 0">0</xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="hqpxsl:getvar('DiscountMarkupBase')"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="PartsDiscount" TransferLine="Yes" Category="PC" Note="">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('TotalTaxParts') &gt;= hqpxsl:getvar('TotalNonTaxParts')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalDiscountAmount')"/>
        </xsl:attribute>
        <xsl:attribute name="Percent">
          <xsl:value-of select="hqpxsl:getvar('DiscountPct')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('TotalDiscountAmount') = 0">0</xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="hqpxsl:getvar('DiscountMarkupBase')"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="PartsOtherTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="Yes">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalOtherTaxParts')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalOtherTaxParts')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="PartsOtherNonTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="No">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalOtherNonTaxParts')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalOtherNonTaxParts')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="PartsTotal" TransferLine="Yes" Category="TT">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalParts')"/>
        </xsl:attribute>
      </LineItem>

      <LineItem Type="Body" TransferLine="Yes" Category="MC" Note="">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFM[MATL_TYPE='MAHW']/TAX_IND='1'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('BodyMaterialAmount')"/>
        </xsl:attribute>
        <xsl:attribute name="Hours">
          <xsl:value-of select="hqpxsl:getvar('BodyMaterialHours')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('BodyMaterialRate')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Paint" TransferLine="Yes" Category="MC" Note="">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFM[MATL_TYPE='MAPA']/TAX_IND='1'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('PaintMaterialAmount')"/>
        </xsl:attribute>
        <xsl:attribute name="Hours">
          <xsl:value-of select="hqpxsl:getvar('PaintMaterialHours')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('PaintMaterialRate')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="SuppliesTotal" TransferLine="Yes" Category="TT">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalMaterials')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="PartsAndMaterialTotal" TransferLine="Yes" Category="TT">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalPartsMaterials')"/>
        </xsl:attribute>
      </LineItem>

      <LineItem Type="Body" TransferLine="Yes" Category="LC" Note="">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFL[LBR_TYPE='LAB']/LBR_TAX_IN='0'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('BodyLaborAmount')"/>
        </xsl:attribute>
        <xsl:attribute name="Hours">
          <xsl:value-of select="hqpxsl:getvar('BodyLaborHours')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('BodyLaborRate')"/>
        </xsl:attribute>
        <xsl:attribute name="RepairHours">
          <xsl:value-of select="hqpxsl:getvar('DetailBodyLaborRepairHours')"/>
        </xsl:attribute>
        <xsl:attribute name="ReplaceHours">
          <xsl:value-of select="hqpxsl:getvar('DetailBodyLaborReplaceHours')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Mechanical" TransferLine="Yes" Category="LC" Note="">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFL[LBR_TYPE='LAM']/LBR_TAX_IN='0'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('MechLaborAmount')"/>
        </xsl:attribute>
        <xsl:attribute name="Hours">
          <xsl:value-of select="hqpxsl:getvar('MechLaborHours')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('MechLaborRate')"/>
        </xsl:attribute>
        <xsl:attribute name="RepairHours">
          <xsl:value-of select="hqpxsl:getvar('DetailMechLaborRepairHours')"/>
        </xsl:attribute>
        <xsl:attribute name="ReplaceHours">
          <xsl:value-of select="hqpxsl:getvar('DetailMechLaborReplaceHours')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Frame" TransferLine="Yes" Category="LC" Note="">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFL[LBR_TYPE='LAF']/LBR_TAX_IN='0'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('FrameLaborAmount')"/>
        </xsl:attribute>
        <xsl:attribute name="Hours">
          <xsl:value-of select="hqpxsl:getvar('FrameLaborHours')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('FrameLaborRate')"/>
        </xsl:attribute>
        <xsl:attribute name="RepairHours">
          <xsl:value-of select="hqpxsl:getvar('DetailFrameLaborRepairHours')"/>
        </xsl:attribute>
        <xsl:attribute name="ReplaceHours">
          <xsl:value-of select="hqpxsl:getvar('DetailFrameLaborReplaceHours')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Paint" TransferLine="Yes" Category="LC" Note="" Taxable="Yes">
        <xsl:attribute name="ExtendedAmount">0</xsl:attribute>
        <xsl:attribute name="Hours">0</xsl:attribute>
        <xsl:attribute name="UnitAmount">0</xsl:attribute>
        <xsl:attribute name="RepairHours">0</xsl:attribute>
        <xsl:attribute name="ReplaceHours">0</xsl:attribute>
      </LineItem>
      <LineItem Type="Refinish" TransferLine="Yes" Category="LC" Note="">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFL[LBR_TYPE='LAR']/LBR_TAX_IN='0'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('PaintLaborAmount')"/>
        </xsl:attribute>
        <xsl:attribute name="Hours">
          <xsl:value-of select="hqpxsl:getvar('PaintLaborHours')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('PaintLaborRate')"/>
        </xsl:attribute>
        <xsl:attribute name="RepairHours">
          <xsl:value-of select="hqpxsl:getvar('DetailPaintLaborRepairHours')"/>
        </xsl:attribute>
        <xsl:attribute name="ReplaceHours">
          <xsl:value-of select="hqpxsl:getvar('DetailPaintLaborReplaceHours')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Other" TransferLine="Yes" Category="LC" Note="" Taxable="Yes">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalOtherLabor')"/>
        </xsl:attribute>
        <xsl:attribute name="Hours">0</xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalOtherLabor')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="LaborTotal" TransferLine="Yes" Category="TT">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalLaborAmount')"/>
        </xsl:attribute>
      </LineItem>

      <xsl:if test="hqpxsl:getvar('TowingAmount') &gt; 0">
        <LineItem Type="Towing" TransferLine="Yes" Category="AC" Note="">
          <xsl:attribute name="Taxable">
            <xsl:choose>
              <xsl:when test="PFH/TAX_TOW_IN='1'">Yes</xsl:when>
              <xsl:otherwise>No</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="ExtendedAmount">
            <xsl:value-of select="hqpxsl:getvar('TowingAmount')"/>
          </xsl:attribute>
          <xsl:attribute name="UnitAmount">
            <xsl:value-of select="hqpxsl:getvar('TowingAmount')"/>
          </xsl:attribute>
        </LineItem>
      </xsl:if>
      <xsl:if test="hqpxsl:getvar('StorageAmount') &gt; 0">
        <LineItem Type="Storage" TransferLine="Yes" Category="AC" Note="">
          <xsl:attribute name="Taxable">
            <xsl:choose>
              <xsl:when test="PFH/TAX_STR_IN='1'">Yes</xsl:when>
              <xsl:otherwise>No</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="ExtendedAmount">
            <xsl:value-of select="hqpxsl:getvar('StorageAmount')"/>
          </xsl:attribute>
          <xsl:attribute name="UnitAmount">
            <xsl:value-of select="hqpxsl:getvar('StorageAmount')"/>
          </xsl:attribute>
        </LineItem>
      </xsl:if>
      <xsl:if test="hqpxsl:getvar('SubletAmount') &gt; 0">
        <LineItem Type="Sublet" TransferLine="Yes" Category="AC" Note="">
          <xsl:attribute name="Taxable">
            <xsl:choose>
              <xsl:when test="PFH/TAX_SUB_IN='1'">Yes</xsl:when>
              <xsl:otherwise>No</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="ExtendedAmount">
            <xsl:value-of select="hqpxsl:getvar('SubletAmount')"/>
          </xsl:attribute>
          <xsl:attribute name="UnitAmount">
            <xsl:value-of select="hqpxsl:getvar('SubletAmount')"/>
          </xsl:attribute>
        </LineItem>
      </xsl:if>
      <xsl:if test="hqpxsl:getvar('OtherAddlChargesAmount') &gt; 0">
        <LineItem Type="Other" TransferLine="Yes" Category="AC" Note="" Taxable="Yes">
          <xsl:attribute name="ExtendedAmount">
            <xsl:value-of select="hqpxsl:getvar('OtherAddlChargesAmount')"/>
          </xsl:attribute>
          <xsl:attribute name="UnitAmount">
            <xsl:value-of select="hqpxsl:getvar('OtherAddlChargesAmount')"/>
          </xsl:attribute>
        </LineItem>
      </xsl:if>
      <LineItem Type="AdditionalChargesTotal" TransferLine="Yes" Category="TT">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalAddlCharges')"/>
        </xsl:attribute>
      </LineItem>

      <LineItem Type="SubTotal" TransferLine="Yes" Category="TT">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('SubtotalAmount')"/>
        </xsl:attribute>
      </LineItem>


      <xsl:if test="hqpxsl:getvar('SalesTaxAmountPM') &gt; 0">
        <LineItem Type="Sales" TransferLine="Yes" Category="TX" Note="">
          <xsl:attribute name="ExtendedAmount">
            <xsl:value-of select="hqpxsl:getvar('SalesTaxAmountPM')"/>
          </xsl:attribute>
          <xsl:attribute name="Percent">
            <xsl:value-of select="hqpxsl:getvar('SalesTaxPctPM')"/>
          </xsl:attribute>
          <xsl:attribute name="UnitAmount">
            <xsl:value-of select="hqpxsl:getvar('SalesTaxBasePM')"/>
          </xsl:attribute>
          <xsl:attribute name="TaxBase">
            <xsl:choose>
              <xsl:when test="hqpxsl:getvar('SalesTaxBasePM') = hqpxsl:getvar('TotalTaxParts')">Parts</xsl:when>
              <xsl:when test="hqpxsl:getvar('SalesTaxBasePM') = hqpxsl:getvar('TotalTaxMaterials')">Materials</xsl:when>
              <xsl:when test="hqpxsl:getvar('SalesTaxBasePM') = hqpxsl:getvar('TotalTaxPartsMaterials')">PartsAndMaterials</xsl:when>
              <xsl:otherwise>Other</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
        </LineItem>
      </xsl:if>
      <xsl:if test="hqpxsl:getvar('SalesTaxAmountLabor') &gt; 0">
        <LineItem Type="Sales" TransferLine="Yes" Category="TX" Note="">
          <xsl:attribute name="ExtendedAmount">
            <xsl:value-of select="hqpxsl:getvar('SalesTaxAmountLabor')"/>
          </xsl:attribute>
          <xsl:attribute name="Percent">
            <xsl:value-of select="hqpxsl:getvar('SalesTaxPctLabor')"/>
          </xsl:attribute>
          <xsl:attribute name="UnitAmount">
            <xsl:value-of select="hqpxsl:getvar('SalesTaxBaseLabor')"/>
          </xsl:attribute>
          <xsl:attribute name="TaxBase">
            <xsl:choose>
              <xsl:when test="hqpxsl:getvar('SalesTaxBaseLabor') = hqpxsl:getvar('TotalTaxLabor')">Labor</xsl:when>
              <xsl:otherwise>Other</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
        </LineItem>
      </xsl:if>
      <xsl:if test="hqpxsl:getvar('OtherTaxAmount') &gt; 0">
        <LineItem Type="Sales" TransferLine="Yes" Category="TX">
          <xsl:attribute name="Note">
            <xsl:text>Tax from Sublet, Towing, Storage or other source</xsl:text>
          </xsl:attribute>
          <xsl:attribute name="ExtendedAmount">
            <xsl:value-of select="hqpxsl:getvar('OtherTaxAmount')"/>
          </xsl:attribute>
          <xsl:attribute name="Percent">100</xsl:attribute>
          <xsl:attribute name="UnitAmount">
            <xsl:value-of select="hqpxsl:getvar('OtherTaxAmount')"/>
          </xsl:attribute>
          <xsl:attribute name="TaxBase">Other</xsl:attribute>
        </LineItem>
      </xsl:if>


      <LineItem Type="TaxTotal" TransferLine="Yes" Category="TT">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalTaxAmount')"/>
        </xsl:attribute>
      </LineItem>

      <LineItem Type="RepairTotal" TransferLine="Yes" Category="TT">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('RepairTotalAmount')"/>
        </xsl:attribute>
      </LineItem>

      <LineItem Type="AppearanceAllowance" TransferLine="Yes" Category="AJ" Note="">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('AppearanceAllowanceAmount')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('AppearanceAllowanceAmount')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Betterment" TransferLine="Yes" Category="AJ" Note="">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('BettermentAmount')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('BettermentAmount')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Deductible" TransferLine="Yes" Category="AJ" Note="">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('DeductibleAmount')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('DeductibleAmount')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Other" TransferLine="Yes" Category="AJ" Note="">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('OtherAdjustmentAmount')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('OtherAdjustmentAmount')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="AdjustmentTotal" TransferLine="Yes" Category="TT">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalAdjustmentAmount')"/>
        </xsl:attribute>
      </LineItem>


      <LineItem Type="NetTotal" TransferLine="Yes" Category="TT">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('NetTotalAmount')"/>
        </xsl:attribute>
      </LineItem>


      <LineItem Type="Variance" TransferLine="Yes" Category="VA">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('ZZVarianceAmount')"/>
        </xsl:attribute>
      </LineItem>

      <LineItem Type="OEMParts" TransferLine="Yes" Category="OT">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFP[PRT_TYPE='PAN']/PRT_TAX_IN='1'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalOEMParts')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalOEMParts')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="LKQParts" TransferLine="Yes" Category="OT">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFP[PRT_TYPE='PAL']/PRT_TAX_IN='1'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalLKQParts')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalLKQParts')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="AFMKParts" TransferLine="Yes" Category="OT">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFP[PRT_TYPE='PAA']/PRT_TAX_IN='1'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalAFMKParts')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalAFMKParts')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="RemanParts" TransferLine="Yes" Category="OT">
        <xsl:attribute name="Taxable">
          <xsl:choose>
            <xsl:when test="PFP[PRT_TYPE='PAM']/PRT_TAX_IN='1'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalRemanParts')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('TotalRemanParts')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="2 Wheel Alignment" TransferLine="No" Category="OT">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('Detail2WAlign')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('Detail2WAlign')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="4 Wheel Alignment" TransferLine="No" Category="OT">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('Detail4WAlign')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('Detail4WAlign')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Clear Coat" TransferLine="No" Category="OT">
        <xsl:attribute name="Hours">
          <xsl:value-of select="sum(//LIN[cu:isClearCoat(string(LINE_DESC)) = true()]/MOD_LB_HRS)"/>
        </xsl:attribute>
        <xsl:attribute name="Amount">
          <xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isClearCoat(string(LINE_DESC)) = true()]/LBR_AMT)) +                                        cu:Number(sum(//LIN[cu:isClearCoat(string(LINE_DESC)) = true()]/ACT_PRICE)))"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Waste Management" TransferLine="No" Category="OT">
        <xsl:attribute name="Amount">
          <xsl:value-of select="cu:Number(sum(//LIN[cu:isWaste(string(LINE_DESC)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isWaste(string(LINE_DESC)) = true()]/ACT_PRICE))"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Car Cover" TransferLine="No" Category="OT">
        <xsl:attribute name="Amount">
          <xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isCarCover(string(LINE_DESC)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isCarCover(string(LINE_DESC)) = true()]/ACT_PRICE)))"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Manual Line" TransferLine="No" Category="OT">
        <xsl:attribute name="Count">
          <xsl:value-of select="count(//LIN[cu:isManualLine(string(LineSource)) = true()])"/>
        </xsl:attribute>
        <xsl:attribute name="Amount">
          <xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isManualLine(string(LineSource)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isManualLine(string(LineSource)) = true()]/ACT_PRICE)))"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Painted Pinstripe" TransferLine="No" Category="OT">
        <xsl:attribute name="Hours">
          <xsl:value-of select="cu:Number(sum(//LIN[cu:isPaintedPinstripe(string(LINE_DESC)) = true()]/MOD_LB_HRS))"/>
        </xsl:attribute>
        <xsl:attribute name="Amount">
          <xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isPaintedPinstripe(string(LINE_DESC)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isPaintedPinstripe(string(LINE_DESC)) = true()]/ACT_PRICE)))"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Taped Pinstripe" TransferLine="No" Category="OT">
        <xsl:attribute name="Hours">
          <xsl:value-of select="cu:Number(sum(//LIN[cu:isTapedPinstripe(string(LINE_DESC)) = true()]/MOD_LB_HRS))"/>
        </xsl:attribute>
        <xsl:attribute name="Amount">
          <xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isTapedPinstripe(string(LINE_DESC)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isTapedPinstripe(string(LINE_DESC)) = true()]/ACT_PRICE)))"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Freon" TransferLine="No" Category="OT">
        <xsl:attribute name="Amount">
          <xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isFreon(string(LINE_DESC)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isFreon(string(LINE_DESC)) = true()]/ACT_PRICE)))"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Towing" TransferLine="No" Category="OT">
        <xsl:attribute name="Amount">
          <xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isTowing(string(LINE_DESC)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isTowing(string(LINE_DESC)) = true()]/ACT_PRICE)))"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="Setup and Measure" TransferLine="No" Category="OT">
        <xsl:attribute name="Amount">
          <xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isSetupMeasure(string(LINE_DESC)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isSetupMeasure(string(LINE_DESC)) = true()]/ACT_PRICE)))"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="DeductiblesApplied" TransferLine="Yes" Category="OT">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="hqpxsl:getvar('DeductibleAmount')"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="hqpxsl:getvar('DeductibleAmount')"/>
        </xsl:attribute>
      </LineItem>
      <LineItem Type="LimitEffect" TransferLine="Yes" Category="OT">
        <xsl:attribute name="ExtendedAmount"/>
        <xsl:attribute name="UnitAmount"/>
      </LineItem>
      <LineItem Type="NetTotalEffect" TransferLine="Yes" Category="OT">
        <xsl:attribute name="ExtendedAmount">
          <xsl:value-of select="number(hqpxsl:getvar('RepairTotalAmount')) - number(hqpxsl:getvar('DeductibleAmount'))"/>
        </xsl:attribute>
        <xsl:attribute name="UnitAmount">
          <xsl:value-of select="number(hqpxsl:getvar('RepairTotalAmount')) - number(hqpxsl:getvar('DeductibleAmount'))"/>
        </xsl:attribute>
      </LineItem>
    </Summary>
  </xsl:template>


  <!--- Compile Audit Information -->
  <xsl:template match="EMSEstimateSpecial" mode="Audit">
    <Audit>
      <AuditItem Name="VerifyDetailPartTaxTotal">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('DetailPartTaxTotal') = hqpxsl:getvar('TotalTaxParts')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="hqpxsl:getvar('DetailPartTaxTotal')"/>
        </xsl:attribute>
        <xsl:attribute name="SummaryTotal">
          <xsl:value-of select="hqpxsl:getvar('TotalTaxParts')"/>
        </xsl:attribute>
      </AuditItem>
      <AuditItem Name="VerifyDetailPartNonTaxTotal">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('DetailPartNonTaxTotal') = hqpxsl:getvar('TotalNonTaxParts')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="hqpxsl:getvar('DetailPartNonTaxTotal')"/>
        </xsl:attribute>
        <xsl:attribute name="SummaryTotal">
          <xsl:value-of select="hqpxsl:getvar('TotalNonTaxParts')"/>
        </xsl:attribute>
      </AuditItem>
      <AuditItem Name="VerifyDetailPartOtherTaxTotal">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('DetailPartOtherTaxTotal') = hqpxsl:getvar('TotalOtherTaxParts')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="hqpxsl:getvar('DetailPartOtherTaxTotal')"/>
        </xsl:attribute>
        <xsl:attribute name="SummaryTotal">
          <xsl:value-of select="hqpxsl:getvar('TotalOtherTaxParts')"/>
        </xsl:attribute>
      </AuditItem>
      <AuditItem Name="VerifyDetailPartOtherNonTaxTotal">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('DetailPartOtherNonTaxTotal') = hqpxsl:getvar('TotalOtherNonTaxParts')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="hqpxsl:getvar('DetailPartOtherNonTaxTotal')"/>
        </xsl:attribute>
        <xsl:attribute name="SummaryTotal">
          <xsl:value-of select="hqpxsl:getvar('TotalOtherNonTaxParts')"/>
        </xsl:attribute>
      </AuditItem>
      <AuditItem Name="VerifyDetailPartOEMTotal">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('DetailOEMParts') = hqpxsl:getvar('TotalOEMParts')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="hqpxsl:getvar('DetailOEMParts')"/>
        </xsl:attribute>
        <xsl:attribute name="SummaryTotal">
          <xsl:value-of select="hqpxsl:getvar('TotalOEMParts')"/>
        </xsl:attribute>
      </AuditItem>
      <AuditItem Name="VerifyDetailPartLKQTotal">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('DetailLKQParts') = hqpxsl:getvar('TotalLKQParts')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="hqpxsl:getvar('DetailLKQParts')"/>
        </xsl:attribute>
        <xsl:attribute name="SummaryTotal">
          <xsl:value-of select="hqpxsl:getvar('TotalLKQParts')"/>
        </xsl:attribute>
      </AuditItem>
      <AuditItem Name="VerifyDetailPartAFMKTotal">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('DetailAFMKParts') = hqpxsl:getvar('TotalAFMKParts')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="hqpxsl:getvar('DetailAFMKParts')"/>
        </xsl:attribute>
        <xsl:attribute name="SummaryTotal">
          <xsl:value-of select="hqpxsl:getvar('TotalAFMKParts')"/>
        </xsl:attribute>
      </AuditItem>
      <AuditItem Name="VerifyDetailPartRemanTotal">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('DetailRemanParts') = hqpxsl:getvar('TotalRemanParts')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="hqpxsl:getvar('DetailRemanParts')"/>
        </xsl:attribute>
        <xsl:attribute name="SummaryTotal">
          <xsl:value-of select="hqpxsl:getvar('TotalRemanParts')"/>
        </xsl:attribute>
      </AuditItem>


      <AuditItem Name="VerifyBodyLaborHours">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="round((hqpxsl:getvar('DetailBodyLaborRepairHours') + hqpxsl:getvar('DetailBodyLaborReplaceHours')) * 10) div 10  = hqpxsl:getvar('BodyLaborHours')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="round((hqpxsl:getvar('DetailBodyLaborRepairHours') + hqpxsl:getvar('DetailBodyLaborReplaceHours')) * 10) div 10"/>
        </xsl:attribute>
        <xsl:attribute name="SummaryTotal">
          <xsl:value-of select="hqpxsl:getvar('BodyLaborHours')"/>
        </xsl:attribute>
      </AuditItem>
      <AuditItem Name="VerifyFrameLaborHours">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="round((hqpxsl:getvar('DetailFrameLaborRepairHours') + hqpxsl:getvar('DetailFrameLaborReplaceHours')) * 10) div 10 = hqpxsl:getvar('FrameLaborHours')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="round((hqpxsl:getvar('DetailFrameLaborRepairHours') + hqpxsl:getvar('DetailFrameLaborReplaceHours')) * 10) div 10"/>
        </xsl:attribute>
        <xsl:attribute name="SummaryTotal">
          <xsl:value-of select="hqpxsl:getvar('FrameLaborHours')"/>
        </xsl:attribute>
      </AuditItem>
      <AuditItem Name="VerifyMechLaborHours">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="round((hqpxsl:getvar('DetailMechLaborRepairHours') + hqpxsl:getvar('DetailMechLaborReplaceHours')) * 10) div 10 = hqpxsl:getvar('MechLaborHours')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="round((hqpxsl:getvar('DetailMechLaborRepairHours') + hqpxsl:getvar('DetailMechLaborReplaceHours')) * 10) div 10"/>
        </xsl:attribute>
        <xsl:attribute name="SummaryTotal">
          <xsl:value-of select="hqpxsl:getvar('MechLaborHours')"/>
        </xsl:attribute>
      </AuditItem>
      <AuditItem Name="VerifyPaintLaborHours">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="round((hqpxsl:getvar('DetailPaintLaborRepairHours') + hqpxsl:getvar('DetailPaintLaborReplaceHours')) * 10) div 10 = hqpxsl:getvar('PaintLaborHours')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="round((hqpxsl:getvar('DetailPaintLaborRepairHours') + hqpxsl:getvar('DetailPaintLaborReplaceHours')) * 10) div 10"/>
        </xsl:attribute>
        <xsl:attribute name="SummaryTotal">
          <xsl:value-of select="hqpxsl:getvar('PaintLaborHours')"/>
        </xsl:attribute>
      </AuditItem>

      <AuditItem Name="VerifyDetailSubletTotal">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('DetailSubletTotal') = hqpxsl:getvar('SubletAmount')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="hqpxsl:getvar('DetailSubletTotal')"/>
        </xsl:attribute>
        <xsl:attribute name="SummaryTotal">
          <xsl:value-of select="hqpxsl:getvar('SubletAmount')"/>
        </xsl:attribute>
      </AuditItem>

      <AuditItem Name="VerifyBettermentTotal">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('DetailBettermentTotal') = hqpxsl:getvar('BettermentAmount')">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="hqpxsl:getvar('DetailBettermentTotal')"/>
        </xsl:attribute>
        <xsl:attribute name="SummaryTotal">
          <xsl:value-of select="hqpxsl:getvar('BettermentAmount')"/>
        </xsl:attribute>
      </AuditItem>

      <AuditItem Name="VerifyNoDetailUnknownTotal">
        <xsl:attribute name="Passed">
          <xsl:choose>
            <xsl:when test="hqpxsl:getvar('DetailUnknownTotal') = 0">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="DetailTotal">
          <xsl:value-of select="hqpxsl:getvar('DetailUnknownTotal')"/>
        </xsl:attribute>
      </AuditItem>
    </Audit>
  </xsl:template>

</xsl:stylesheet>