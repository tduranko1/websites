<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
									xmlns:msxsl="urn:schemas-microsoft-com:xslt"
									xmlns:sgxsl="http://www.lynxservices.com/scenegenesis">
<xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes"/>
<xsl:decimal-format NaN="0"/>

<!--- This styesheet transforms incoming Scene Genesis EMS Data to APD's Common Estimate Format -->


<!--- Scripting necessary for the mathematical calculations in the summary -->
<msxsl:script language="JScript" implements-prefix="sgxsl">
	var BodyMaterialAmount, BodyMaterialHours, BodyMaterialRate;
	var DiscountMarkupBase; 
	var DiscountTaxAmount, DiscountNonTaxAmount, TotalDiscountAmount, DiscountPct;
	var MarkupTaxAmount, MarkupNonTaxAmount, TotalMarkupAmount, MarkupPct;
	var PaintMaterialAmount, PaintMaterialHours, PaintMaterialRate;
	var TotalParts, TotalTaxParts, TotalNonTaxParts, TotalOtherTaxParts, TotalOtherNonTaxParts;
	var TotalMaterials;
	var TotalPartsMaterials;

	var BodyLaborAmount, BodyLaborHours, BodyLaborRate;
	var FrameLaborAmount, FrameLaborHours, FrameLaborRate;
	var MechLaborAmount, MechLaborHours, MechLaborRate;
	var PaintLaborAmount, PaintLaborHours, PaintLaborRate;
	var TotalOtherLabor;
	var TotalLaborAmount;

	var StorageAmount;
	var SubletAmount;
	var TowingAmount;
	var OtherAddlChargesAmount;
	var TotalAddlCharges;

	var AppearanceAllowanceAmount;
	var BettermentAmount;
	var DeductibleAmount;
	var LessChargesAmount;
	var BottomLineAdjAmount;
	var RelatedPriorDamageAmount;
	var OtherAdjustmentAmount;
	var TotalAdjustmentAmount;

	var SalesTaxAmountPM, SalesTaxBasePM, SalesTaxPctPM;
	var SalesTaxAmountLabor, SalesTaxBaseLabor, SalesTaxPctLabor;
	var TotalTaxAmount;

	var NetTotalAmount;
	var RepairTotalAmount;
	var SubtotalAmount;
	var ZZVarianceAmount;

	var DetailPartTaxTotal, DetailPartNonTaxTotal, DetailPartOtherTaxTotal, DetailPartOtherNonTaxTotal, DetailOEMPartsTotal;
	var DetailBodyLaborHours, DetailFrameLaborHours, DetailMechLaborHours, DetailPaintLaborHours;
	var DetailSubletTotal;
	var DetailUnknownTotal;
	var Detail2WAlign, Detail4WAlign;

	var CurrentLineNumber;

	function setvar(varname, value) {
		eval(varname + ' = ' + value);
		return '';
	}

	function getvar(varname) {
		return eval(eval(varname));
	}

	function emsreplace(instring) {
   		var rexp = /#38;/g;					//Create regular expression pattern.
   		return(instring.replace(rexp, ""));	//Do the Replace and return
	}
</msxsl:script>

<xsl:variable name="source" select="'SG'"/>
<xsl:variable name="estimatepackage" select="'MIT'"/>
<xsl:variable name="version" select="'1.0.0'"/>
<xsl:variable name="versiondate" select="'2/5/2003'"/>

<xsl:template match="/SGTransaction">
	<xsl:call-template name="EstimateData"/>
</xsl:template>

<xsl:template name="EstimateData">
	<APDEstimate>
		<xsl:attribute name="Source"><xsl:value-of select="$source"/></xsl:attribute>
		<xsl:attribute name="EstimatePackage"><xsl:value-of select="$estimatepackage"/></xsl:attribute>
		<xsl:attribute name="SoftwareVersion"><xsl:value-of select="TransactionData/SoftwareVersion"/></xsl:attribute>
		<xsl:attribute name="StylesheetVersion"><xsl:value-of select="$version"/></xsl:attribute>
		<xsl:attribute name="StylesheetDate"><xsl:value-of select="$versiondate"/></xsl:attribute>

		<xsl:apply-templates select="TransactionData" mode="Header" />
		<xsl:apply-templates select="TransactionData" mode="LaborRates" />
		<xsl:apply-templates select="TransactionData/DETAILLINES" mode="Detail" />
		<xsl:apply-templates select="TransactionData" mode="Profile" />
		<xsl:apply-templates select="TransactionData" mode="Summary" />
		<xsl:apply-templates select="TransactionData" mode="Audit" />
	</APDEstimate>
</xsl:template>


<!--- Transform Header Information -->
<xsl:template match="TransactionData" mode="Header">
	<Header>
		<xsl:attribute name="EstimateType">
			<xsl:choose>
				<xsl:when test="starts-with(string(EstimateIdentifier), 'E')">Estimate</xsl:when>
				<xsl:when test="starts-with(string(EstimateIdentifier), 'S')">Supplement</xsl:when>
				<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<xsl:attribute name="SupplementNumber">
			<xsl:choose>
				<xsl:when test="starts-with(string(EstimateIdentifier), 'E')">0</xsl:when>
				<xsl:otherwise><xsl:value-of select="substring(string(EstimateIdentifier), 2)"/></xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
    		<xsl:attribute name="WrittenDate"><xsl:value-of select="substring(CreateDateTime/DateTime/@Date, 1, 2)"/>/<xsl:value-of select="substring(CreateDateTime/DateTime/@Date, 3, 2)"/>/<xsl:value-of select="substring(CreateDateTime/DateTime/@Date, 5, 4)"/><xsl:text> </xsl:text><xsl:value-of select="substring(CreateDateTime/DateTime/@Time, 1, 2)"/>:<xsl:value-of select="substring(CreateDateTime/DateTime/@Time, 3, 2)"/>:<xsl:value-of select="substring(CreateDateTime/DateTime/@Time, 5, 2)"/></xsl:attribute>
    		<xsl:attribute name="AssignmentDate"><xsl:value-of select="substring(Administrative/ClaimInformation/AssignmentDate/DateTime/@Date, 1, 2)"/>/<xsl:value-of select="substring(Administrative/ClaimInformation/AssignmentDate/DateTime/@Date, 3, 2)"/>/<xsl:value-of select="substring(Administrative/ClaimInformation/AssignmentDate/DateTime/@Date, 5, 4)"/><xsl:text> </xsl:text><xsl:value-of select="substring(Administrative/ClaimInformation/AssignmentDate/DateTime/@Time, 1, 2)"/>:<xsl:value-of select="substring(Administrative/ClaimInformation/AssignmentDate/DateTime/@Time, 3, 2)"/>:<xsl:value-of select="substring(Administrative/ClaimInformation/AssignmentDate/DateTime/@Time, 5, 2)"/></xsl:attribute>		
		<xsl:attribute name="ClaimantName"><xsl:value-of select="Administrative/Claimant/Customer/CustomerName"/></xsl:attribute>
		<xsl:attribute name="ClaimNumber"><xsl:value-of select="Administrative/ClaimInformation/ClaimNumber"/></xsl:attribute>
		<xsl:attribute name="InsuranceCompanyName"><xsl:value-of select="Administrative/InsuranceCompany/Customer/CustomerName"/></xsl:attribute>
		<xsl:attribute name="InsuredName"><xsl:value-of select="Administrative/Insured/Customer/CustomerName"/></xsl:attribute>
		<xsl:attribute name="LossDate">
			<xsl:if test="Administrative/Loss/LossDateTime/DateTime">
				<xsl:value-of select="substring(Administrative/Loss/LossDateTime/DateTime/@Date, 1, 2)"/>/<xsl:value-of select="substring(Administrative/Loss/LossDateTime/DateTime/@Date, 3, 2)"/>/<xsl:value-of select="substring(Administrative/Loss/LossDateTime/DateTime/@Date, 5, 4)"/>
				<xsl:if test="Administrative/Loss/LossDateTime/DateTime/@Time">
					<xsl:text>  </xsl:text><xsl:value-of select="substring(Administrative/Loss/LossDateTime/DateTime/@Time, 1, 2)"/>:<xsl:value-of select="substring(Administrative/Loss/LossDateTime/DateTime/@Time, 3, 2)"/>:<xsl:value-of select="substring(Administrative/Loss/LossDateTime/DateTime/@Time, 5, 2)"/>
				</xsl:if>
			</xsl:if>
		</xsl:attribute>
		<xsl:attribute name="LossType">
			<xsl:choose>
				<xsl:when test="Administrative/Loss/LossCategoryCode/@code='C'">Collision</xsl:when>
				<xsl:when test="Administrative/Loss/LossCategoryCode/@code='L'">Liability</xsl:when>
				<xsl:when test="Administrative/Loss/LossCategoryCode/@code='M'">Comprehensive</xsl:when>
				<xsl:when test="Administrative/Loss/LossCategoryCode/@code='O'">Other</xsl:when>
				<xsl:when test="Administrative/Loss/LossCategoryCode/@code='P'">Property</xsl:when>
				<xsl:when test="Administrative/Loss/LossCategoryCode/@code='U'">Unknown</xsl:when>
				<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<xsl:attribute name="PolicyNumber"><xsl:value-of select="Administrative/Policy/PolicyNumber"/></xsl:attribute>
		<xsl:attribute name="Remarks"><xsl:value-of select="Vehicle/VehicleMemoText"/></xsl:attribute>
		<xsl:attribute name="RepairDays"><xsl:value-of select="Administrative/RepairInformation/DaysToRepair"/></xsl:attribute>
		<xsl:attribute name="SettlementType">
			<xsl:choose>
				<xsl:when test="Administrative/Loss/TotalLossFlag='Y'">TotalLoss</xsl:when>
				<xsl:otherwise>Normal</xsl:otherwise>
	          </xsl:choose>
		</xsl:attribute>


		<Appraiser>
			<xsl:attribute name="AppraiserName"><xsl:value-of select="Administrative/Appraiser/Customer/CustomerName"/></xsl:attribute>
			<xsl:attribute name="AppraiserLicenseNumber"><xsl:value-of select="Administrative/Appraiser/AppraiserLicenseNumber"/></xsl:attribute>
		</Appraiser>

		<Estimator>
			<xsl:choose>
				<xsl:when test="Administrative/Appraiser/EstimatorFlag='Y'">
					<xsl:apply-templates mode="Estimator" select="Administrative/Appraiser/Customer"/>
				</xsl:when>
				<xsl:when test="Administrative/RepairFacility/EstimatorFlag='Y'">
					<xsl:apply-templates mode="Estimator" select="Administrative/RepairFacility/Customer"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="CompanyAddress"/>
					<xsl:attribute name="CompanyAreaCode"/>
					<xsl:attribute name="CompanyCity"/>
					<xsl:attribute name="CompanyName">! Not Found In Estimate Data !</xsl:attribute>
					<xsl:attribute name="CompanyExchangeNumber"/>
					<xsl:attribute name="CompanyState"/>
					<xsl:attribute name="CompanyUnitNumber"/>
					<xsl:attribute name="CompanyZip"/>
				</xsl:otherwise>
			</xsl:choose>
		</Estimator>

		<Inspection>
			<xsl:attribute name="InspectionAddress"><xsl:value-of select="concat(Administrative/InspectionSite/Address/Addr1, Administrative/InspectionSite/Address/Addr2)"/></xsl:attribute>
			<xsl:attribute name="InspectionAreaCode"><xsl:value-of select="Administrative/InspectionSite/ContactPerson[@PrimaryContact='Y']/ContactTelephone[@TelephoneType='BP1' or @TelephoneType='BP2']/Telecom/AreaCode"/></xsl:attribute>
			<xsl:attribute name="InspectionCity"><xsl:value-of select="Administrative/InspectionSite/Address/City"/></xsl:attribute>
			<xsl:attribute name="InspectionExchangeNumber"><xsl:value-of select="substring(Administrative/InspectionSite/ContactPerson[@PrimaryContact='Y']/ContactTelephone[@TelephoneType='BP1' or @TelephoneType='BP2']/Telecom/TelecomNum, 1, 3)"/></xsl:attribute>
			<xsl:attribute name="InspectionLocation"/>
			<xsl:attribute name="InspectionState"><xsl:value-of select="Administrative/InspectionSite/Address/State"/></xsl:attribute>
			<xsl:attribute name="InspectionType"><xsl:value-of select="Administrative/InspectionSite/InspectionCodeDescription"/></xsl:attribute>
			<xsl:attribute name="InspectionUnitNumber"><xsl:value-of select="substring(Administrative/InspectionSite/ContactPerson[@PrimaryContact='Y']/ContactTelephone[@TelephoneType='BP1' or @TelephoneType='BP2']/Telecom/TelecomNum, 4, 4)"/></xsl:attribute>
			<xsl:attribute name="InspectionZip"><xsl:value-of select="Administrative/InspectionSite/Address/Zip"/></xsl:attribute>
		</Inspection>

		<Owner>
			<xsl:attribute name="OwnerAddress"><xsl:value-of select="concat(Administrative/Owner/Customer/CustomerAddress[1]/Address/Addr1, Administrative/Owner/Customer/CustomerAddress[1]/Address/Addr2)"/></xsl:attribute>
			<xsl:attribute name="OwnerCity"><xsl:value-of select="Administrative/Owner/Customer/CustomerAddress[1]/Address/City"/></xsl:attribute>
			<xsl:attribute name="OwnerDayAreaCode"><xsl:value-of select="Administrative/Owner/Customer/ContactPerson[@PrimaryContact='Y']/ContactTelephone[1]/Telecom/AreaCode"/></xsl:attribute>
			<xsl:attribute name="OwnerDayExchangeNumber"><xsl:value-of select="substring(Administrative/Owner/Customer/ContactPerson[@PrimaryContact='Y']/ContactTelephone[1]/Telecom/TelecomNum, 1, 3)"/></xsl:attribute>
			<xsl:attribute name="OwnerDayExtensionNumber"><xsl:value-of select="Administrative/Owner/Customer/ContactPerson[@PrimaryContact='Y']/ContactTelephone[1]/Telecom/Extn"/></xsl:attribute>
			<xsl:attribute name="OwnerDayUnitNumber"><xsl:value-of select="substring(Administrative/Owner/Customer/ContactPerson[@PrimaryContact='Y']/ContactTelephone[1]/Telecom/TelecomNum, 4, 4)"/></xsl:attribute>
			<xsl:attribute name="OwnerEveningAreaCode"><xsl:value-of select="Administrative/Owner/Customer/ContactPerson[@PrimaryContact='Y']/ContactTelephone[2]/Telecom/AreaCode"/></xsl:attribute>
			<xsl:attribute name="OwnerEveningExchangeNumber"><xsl:value-of select="substring(Administrative/Owner/Customer/ContactPerson[@PrimaryContact='Y']/ContactTelephone[2]/Telecom/TelecomNum, 1, 3)"/></xsl:attribute>
			<xsl:attribute name="OwnerEveningExtensionNumber"><xsl:value-of select="Administrative/Owner/Customer/ContactPerson[@PrimaryContact='Y']/ContactTelephone[2]/Telecom/Extn"/></xsl:attribute>
			<xsl:attribute name="OwnerEveningUnitNumber"><xsl:value-of select="substring(Administrative/Owner/Customer/ContactPerson[@PrimaryContact='Y']/ContactTelephone[2]/Telecom/TelecomNum, 4, 4)"/></xsl:attribute>
			<xsl:attribute name="OwnerName"><xsl:value-of select="Administrative/Owner/Customer/CustomerName"/></xsl:attribute>
			<xsl:attribute name="OwnerState"><xsl:value-of select="Administrative/Owner/Customer/CustomerAddress[1]/Address/State"/></xsl:attribute>
			<xsl:attribute name="OwnerZip"><xsl:value-of select="Administrative/Owner/Customer/CustomerAddress[1]/Address/Zip"/></xsl:attribute>
		</Owner>

		<Shop>
			<xsl:attribute name="ShopAddress"><xsl:value-of select="concat(Administrative/RepairFacility/Customer/CustomerAddress[1]/Address/Addr1,  Administrative/RepairFacility/Customer/CustomerAddress[@AddressType='MA']/Address/Addr2)"/></xsl:attribute>
			<xsl:attribute name="ShopAreaCode"><xsl:value-of select="Administrative/RepairFacility/Customer/ContactPerson[@PrimaryContact='Y']/ContactTelephone[@TelephoneType='BP1' or @TelephoneType='BP2']/Telecom/AreaCode"/></xsl:attribute>
			<xsl:attribute name="ShopCity"><xsl:value-of select="Administrative/RepairFacility/Customer/CustomerAddress[1]/Address/City"/></xsl:attribute>
			<xsl:attribute name="ShopExchangeNumber"><xsl:value-of select="substring(Administrative/RepairFacility/Customer/ContactPerson[@PrimaryContact='Y']/ContactTelephone[@TelephoneType='BP1' or @TelephoneType='BP2']/Telecom/TelecomNum, 1, 3)"/></xsl:attribute>
			<xsl:attribute name="ShopName"><xsl:value-of select="Administrative/RepairFacility/Customer/CustomerName"/></xsl:attribute>
			<xsl:attribute name="ShopRegistrationNumber"><xsl:value-of select="Administrative/RepairFacility/CompanyLicenseNmbr"/></xsl:attribute>
			<xsl:attribute name="ShopState"><xsl:value-of select="Administrative/RepairFacility/Customer/CustomerAddress[1]/Address/State"/></xsl:attribute>
			<xsl:attribute name="ShopUnitNumber"><xsl:value-of select="substring(Administrative/RepairFacility/Customer/ContactPerson[@PrimaryContact='Y']/ContactTelephone[@TelephoneType='BP1' or @TelephoneType='BP2']/Telecom/TelecomNum, 4, 4)"/></xsl:attribute>
			<xsl:attribute name="ShopZip"><xsl:value-of select="Administrative/RepairFacility/Customer/CustomerAddress[1]/Address/Zip"/></xsl:attribute>
		</Shop>

		<Vehicle>
			<xsl:attribute name="VehicleBodyStyle"><xsl:value-of select="Vehicle/BodyStyle"/></xsl:attribute>
			<xsl:attribute name="VehicleColor"><xsl:value-of select="Vehicle/ExternalColor"/></xsl:attribute>
			<xsl:attribute name="VehicleEngineType"><xsl:value-of select="Vehicle/EngineSize"/></xsl:attribute>
			<xsl:attribute name="VehicleMake"><xsl:value-of select="Vehicle/MakeCode"/></xsl:attribute>
			<xsl:attribute name="VehicleMileage"><xsl:value-of select="translate(Vehicle/Mileage, ',', '')"/></xsl:attribute>
			<xsl:attribute name="VehicleModel"><xsl:value-of select="Vehicle/Model"/></xsl:attribute>
			<xsl:attribute name="VehicleYear"><xsl:value-of select="Vehicle/ModelYear"/></xsl:attribute>
			<xsl:attribute name="Vin"><xsl:value-of select="Vehicle/Vin"/></xsl:attribute>

			<xsl:for-each select="Vehicle/ImpactPoint">
				<xsl:if test="ImpactAreaCode/@code != '0'">
					<ImpactPoint>
						<xsl:attribute name="ImpactArea">
							<xsl:choose>
								<xsl:when test="ImpactAreaCode/@code='1'"><xsl:value-of select="'Right Front'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='2'"><xsl:value-of select="'Right Front Pillar'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='3'"><xsl:value-of select="'Right T-Bone'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='4'"><xsl:value-of select="'Right Quarter Post'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='5'"><xsl:value-of select="'Right Rear'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='6'"><xsl:value-of select="'Rear'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='7'"><xsl:value-of select="'Left Rear'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='8'"><xsl:value-of select="'Left Quarter Post'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='9'"><xsl:value-of select="'Left T-Bone'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='10'"><xsl:value-of select="'Left Front Pillar'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='11'"><xsl:value-of select="'Left Front'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='12'"><xsl:value-of select="'Front'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='13'"><xsl:value-of select="'Rollover'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='14'"><xsl:value-of select="'Unknown'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='15'"><xsl:value-of select="'Total Loss'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='16'"><xsl:value-of select="'Non-Collision'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='19'"><xsl:value-of select="'All Over'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='20'"><xsl:value-of select="'Stripped'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='21'"><xsl:value-of select="'Undercarriage'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='22'"><xsl:value-of select="'Total Burn'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='23'"><xsl:value-of select="'Interior Burn'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='24'"><xsl:value-of select="'Exterior Burn'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='25'"><xsl:value-of select="'Hood'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='26'"><xsl:value-of select="'Deck Lid'"/></xsl:when>
								<xsl:when test="ImpactAreaCode/@code='27'"><xsl:value-of select="'Roof'"/></xsl:when>
								<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="PrimaryImpactFlag">
							<xsl:choose>
								<xsl:when test="@PrimaryImpactPoint='Y'">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</ImpactPoint>
				</xsl:if>
			</xsl:for-each>

			<xsl:for-each select="Vehicle/VehicleOption">
				<VehicleOption>
					<xsl:attribute name="Option"><xsl:value-of select="OptionDescription"/></xsl:attribute>
				</VehicleOption>
			</xsl:for-each>
		</Vehicle>
	</Header>
</xsl:template>

<xsl:template match="Customer" mode="Estimator">
	<xsl:attribute name="CompanyAddress"><xsl:value-of select="concat(CustomerAddress[1]/Address/Addr1, CustomerAddress[1]/Address/Addr2)"/></xsl:attribute>
	<xsl:attribute name="CompanyAreaCode"><xsl:value-of select="ContactPerson[@PrimaryContact='Y']/ContactTelephone[@TelephoneType='BP1' or @TelephoneType='BP2']/Telecom/AreaCode"/></xsl:attribute>
	<xsl:attribute name="CompanyCity"><xsl:value-of select="CustomerAddress[1]/Address/City"/></xsl:attribute>
	<xsl:attribute name="CompanyName"><xsl:value-of select="CustomerName"/></xsl:attribute>
	<xsl:attribute name="CompanyExchangeNumber"><xsl:value-of select="substring(ContactPerson[@PrimaryContact='Y']/ContactTelephone[@TelephoneType='BP1' or @TelephoneType='BP2']/Telecom/TelecomNum, 1, 3)"/></xsl:attribute>
	<xsl:attribute name="CompanyState"><xsl:value-of select="CustomerAddress[1]/Address/State"/></xsl:attribute>
	<xsl:attribute name="CompanyUnitNumber"><xsl:value-of select="substring(ContactPerson[@PrimaryContact='Y']/ContactTelephone[@TelephoneType='BP1' or @TelephoneType='BP2']/Telecom/TelecomNum, 4, 4)"/></xsl:attribute>
	<xsl:attribute name="CompanyZip"><xsl:value-of select="CustomerAddress[1]/Address/Zip"/></xsl:attribute>
</xsl:template>


<xsl:template match="TransactionData" mode="LaborRates">
	<!--- Capture the rates as they are needed for both detail lines and summary -->
	
	<!--- Body Labor Rate -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborHourlyRate"><xsl:value-of select="sgxsl:setvar('BodyLaborRate', number(ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborHourlyRate))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('BodyLaborRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Frame Labor Rate -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborHourlyRate"><xsl:value-of select="sgxsl:setvar('FrameLaborRate', number(ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborHourlyRate))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('FrameLaborRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Mechanical Labor Rate -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborHourlyRate"><xsl:value-of select="sgxsl:setvar('MechLaborRate', number(ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborHourlyRate))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('MechLaborRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Paint Labor Rate -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborHourlyRate"><xsl:value-of select="sgxsl:setvar('PaintLaborRate', number(ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborHourlyRate))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('PaintLaborRate', 0)"/></xsl:otherwise>
     </xsl:choose>
</xsl:template>


<!--- Transform Detail Line Information -->
<xsl:template match="TransactionData/DETAILLINES" mode="Detail">
	<!--- First set up running totals for various detail components.  These will be used for the audit later -->

	<xsl:value-of select="sgxsl:setvar('DetailPartTaxTotal', 0)"/>
	<xsl:value-of select="sgxsl:setvar('DetailPartNonTaxTotal', 0)"/>
	<xsl:value-of select="sgxsl:setvar('DetailPartOtherTaxTotal', 0)"/>
	<xsl:value-of select="sgxsl:setvar('DetailPartOtherNonTaxTotal', 0)"/>
	<xsl:value-of select="sgxsl:setvar('DetailBodyLaborHours', 0)"/>
	<xsl:value-of select="sgxsl:setvar('DetailFrameLaborHours', 0)"/>
	<xsl:value-of select="sgxsl:setvar('DetailMechLaborHours', 0)"/>
	<xsl:value-of select="sgxsl:setvar('DetailPaintLaborHours', 0)"/>
	<xsl:value-of select="sgxsl:setvar('DetailSubletTotal', 0)"/>
	<xsl:value-of select="sgxsl:setvar('DetailUnknownTotal', 0)"/>
	<xsl:value-of select="sgxsl:setvar('DetailOEMPartsTotal', 0)"/>
	<xsl:value-of select="sgxsl:setvar('Detail2WAlign', 0)"/>
	<xsl:value-of select="sgxsl:setvar('Detail4WAlign', 0)"/>


	<!--- ADP does not provide discount or markup base in their subtotals...we must calculate from the detail lines -->
	<xsl:value-of select="sgxsl:setvar('DiscountMarkupBase', 0)"/>

	<Detail>
		<xsl:for-each select="DetailLine">
			<DetailLine>
				<xsl:attribute name="DetailNumber"><xsl:value-of select="DisplayLineNumber"/></xsl:attribute>
				<xsl:attribute name="Comment"/>
				<xsl:attribute name="Description">
					<xsl:choose>
						<xsl:when test="Part/OEMPartNmbr='APPEARANCE ALLOWANCE' or Part/OEMPartNmbr='RELATED PRIOR DAMAGE' or Part/OEMPartNmbr='UNRELATED PRIOR DAMAGE'"><xsl:value-of select="concat(LineDescription, ' (', Part/OEMPartNmbr, ' of  $', Part/ActualPartPrice, ')' )"/></xsl:when>				
						<xsl:otherwise><xsl:value-of select="LineDescription"/></xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>					
				<xsl:attribute name="OperationCode">
					<xsl:choose>
						<xsl:when test="Part/OEMPartNmbr='APPEARANCE ALLOWANCE' or Part/OEMPartNmbr='RELATED PRIOR DAMAGE' or Part/OEMPartNmbr='UNRELATED PRIOR DAMAGE'">Comment</xsl:when>
						<xsl:when test="not(Part/ActualPartPrice) and not(Labor) and not(Sublet/SubletAmtFlag='Y')">Comment</xsl:when>
						<xsl:when test="not(Labor)"></xsl:when>
						<xsl:when test=".//OperationCode/@code='OP0'"></xsl:when>
						<xsl:when test=".//OperationCode/@code='OP1'">Repair</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP2'">Remove/Install</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP3'">Other</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP4'">Alignment</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP5'">Repair</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP6'">Refinish</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP7'">Other</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP8'">Other</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP9'">Repair</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP10'">Repair</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP11'">Replace</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP12'">Replace</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP13'">Other</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP14'">Other</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP15'">Blend</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP16'">Sublet</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP17'">Other</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP18'">Other</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP19'">Other</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP20'">Remove/Install</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP21'">Repair</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP22'">Repair</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP23'">Other</xsl:when>
						<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>

				<xsl:attribute name="LineAdded">
					<xsl:if test="LineAddedIndicator!='E01'">
						<xsl:value-of select="LineAddedIndicator"/>
					</xsl:if>
				</xsl:attribute>
				<xsl:attribute name="ManualLineFlag"/>

				<!--- Part Information -->
				<xsl:if test="Part/OEMPartNmbr!='APPEARANCE ALLOWANCE' and Part/OEMPartNmbr!='RELATED PRIOR DAMAGE' and Part/OEMPartNmbr!='UNRELATED PRIOR DAMAGE' and Part/PartTypeCode/@code!='PAO'">
					<Part>
						<xsl:attribute name="APDPartCategory">
							<xsl:choose>
								<xsl:when test="Part/PartTypeCode/@code='PAS'">Sublet</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAO' and Part/PartTaxedFlag='Y'">OtherTaxableParts</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAO' and Part/PartTaxedFlag='N'">OtherNonTaxableParts</xsl:when>
								<xsl:when test="Part/PartTaxedFlag='Y'">TaxableParts</xsl:when>
								<xsl:when test="Part/PartTaxedFlag='N'">NonTaxableParts</xsl:when>
								<xsl:otherwise></xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="DBPartPrice">
							<xsl:choose>
								<xsl:when test="Part/UserChangedPartPriceFlag='Y' and number(Part/DBPartPrice) &gt; 0"><xsl:value-of select="number(Part/DBPartPrice) * number(Part/PartQuantity)"/></xsl:when>
								<xsl:otherwise></xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="PartIncludedFlag">
							<xsl:choose>
								<xsl:when test="number(Part/ActualPartPrice)=0">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="PartPrice">
							<xsl:choose>
								<xsl:when test="number(Part/ActualPartPrice)=0"><xsl:value-of select="number(Part/DBPartPrice) * number(Part/PartQuantity)"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="number(Part/ActualPartPrice) * number(Part/PartQuantity)"/></xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="PartTaxedFlag">
							<xsl:choose>
								<xsl:when test="Sublet/SubletAmtFlag='Y' and Sublet/SubletAmtTaxedFlag='Y'">Yes</xsl:when>
								<xsl:when test="Part/PartTaxedFlag='Y'">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="PartType">
							<xsl:choose>
								<xsl:when test="Part/PartTypeCode/@code='PAN'">New</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAL'">LKQ</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAA'">Aftermarket</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAG'">Glass</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAO'">Other</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAS'">Sublet</xsl:when>
								<xsl:when test="not(Part/PartTypeCode)">Unspecified</xsl:when>
								<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="PriceChangedFlag">
							<xsl:choose>
								<xsl:when test="Part/UserChangedPartPriceFlag='Y' and number(Part/DBPartPrice) &gt; 0">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="Quantity"><xsl:value-of select="Part/PartQuantity"/></xsl:attribute>
					</Part>

					<!--- Update running totals with any part price.  Make sure we put it in the right bucket -->
					<xsl:if test="number(Part/ActualPartPrice) &gt; 0">
						<xsl:choose>
							<xsl:when test="Sublet/SubletAmtFlag='Y'"><xsl:value-of select="sgxsl:setvar('DetailSubletTotal', round((sgxsl:getvar('DetailSubletTotal') + number(Sublet/SubletAmt)) * 100) div 100)"/></xsl:when>
							<xsl:when test="Part/PartTypeCode/@code='PAO' and Part/PartTaxedFlag='Y'"><xsl:value-of select="sgxsl:setvar('DetailPartOtherTaxTotal', round((sgxsl:getvar('DetailPartOtherTaxTotal') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:when>
							<xsl:when test="Part/PartTypeCode/@code='PAO' and Part/PartTaxedFlag='N'"><xsl:value-of select="sgxsl:setvar('DetailPartOtherNonTaxTotal', round((sgxsl:getvar('DetailPartOtherNonTaxTotal') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:when>
							<xsl:when test="Part/PartTaxedFlag='Y'"><xsl:value-of select="sgxsl:setvar('DetailPartTaxTotal', round((sgxsl:getvar('DetailPartTaxTotal') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:when>
							<xsl:when test="Part/PartTaxedFlag='N'"><xsl:value-of select="sgxsl:setvar('DetailPartNonTaxTotal', round((sgxsl:getvar('DetailPartNonTaxTotal') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="sgxsl:setvar('DetailUnknownTotal', round((sgxsl:getvar('DetailUnknownTotal') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:otherwise>
						</xsl:choose>
					</xsl:if>

					<!--- Update discount or markup base -->
					<xsl:if test="number(Part/ActualPartPrice) &gt; 0">
                            	<xsl:if test="number(Part/PartAdjustmentPercent) != 0">
							<xsl:value-of select="sgxsl:setvar('DiscountMarkupBase', round((sgxsl:getvar('DiscountMarkupBase') + number(Part/ActualPartPrice)) * 100) div 100)"/>
						</xsl:if>
					</xsl:if>				

					<!--- Update OEM Parts Total -->
					<xsl:if test="number(Part/ActualPartPrice) &gt; 0">
                            	<xsl:if test="Part/PartTypeCode/@code='PAN'">
							<xsl:value-of select="sgxsl:setvar('DetailOEMPartsTotal', round((sgxsl:getvar('DetailOEMPartsTotal') + number(Part/ActualPartPrice)) * 100) div 100)"/>
						</xsl:if>
					</xsl:if>

					<xsl:variable name="LineDescription" select="translate(LineDescription, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
					
					<xsl:if test="$LineDescription = 'two wheel align' or $LineDescription = 'two wheel alignment' or $LineDescription = 'front wheel alignment' or $LineDescription = '2 wheel align' or $LineDescription = '2 wheel alignment' or $LineDescription = '2-wheel align' or $LineDescription = '2-wheel alignment'"><xsl:value-of select="sgxsl:setvar('Detail2WAlign', round((sgxsl:getvar('Detail2WAlign') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:if>

					<xsl:if test="$LineDescription = 'four wheel align' or $LineDescription = 'four wheel alignment' or $LineDescription = '4 wheel align' or $LineDescription = '4 wheel alignment' or $LineDescription = '4-wheel align' or $LineDescription = '4-wheel alignment'"><xsl:value-of select="sgxsl:setvar('Detail4WAlign', round((sgxsl:getvar('Detail4WAlign') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:if> 									
				</xsl:if>


				<!--- Labor Information -->
				<xsl:if test="Part/OEMPartNmbr!='APPEARANCE ALLOWANCE' and Part/OEMPartNmbr!='RELATED PRIOR DAMAGE' and Part/OEMPartNmbr!='UNRELATED PRIOR DAMAGE' and Part/PartTypeCode/@code!='PAS'">
					<Labor>
						<xsl:attribute name="DBLaborHours">
							<xsl:choose>
								<xsl:when test="Labor/UserChangedLaborHoursFlag='Y' and Labor/DBLaborHours!=0"><xsl:value-of select="Labor/DBLaborHours"/></xsl:when>
								<xsl:otherwise></xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborAmount">
							<xsl:choose>
								<xsl:when test="Labor/LaborType/@code='LAB'"><xsl:value-of select="Labor/ActualLaborHours * sgxsl:getvar('BodyLaborRate')"/></xsl:when>
								<xsl:when test="Labor/LaborType/@code='LAD'"><xsl:value-of select="Labor/ActualLaborHours * sgxsl:getvar('MechLaborRate')"/></xsl:when>
								<xsl:when test="Labor/LaborType/@code='LAE'"><xsl:value-of select="Labor/ActualLaborHours * sgxsl:getvar('MechLaborRate')"/></xsl:when>
								<xsl:when test="Labor/LaborType/@code='LAR'"><xsl:value-of select="Labor/ActualLaborHours * sgxsl:getvar('PaintLaborRate')"/></xsl:when>
								<xsl:when test="Labor/LaborType/@code='LAS'"><xsl:value-of select="Labor/ActualLaborHours * sgxsl:getvar('FrameLaborRate')"/></xsl:when>
								<xsl:when test="Labor/LaborType/@code='LAF'"><xsl:value-of select="Labor/ActualLaborHours * sgxsl:getvar('FrameLaborRate')"/></xsl:when>
								<xsl:when test="Labor/LaborType/@code='LAM'"><xsl:value-of select="Labor/ActualLaborHours * sgxsl:getvar('MechLaborRate')"/></xsl:when>
								<xsl:when test="Labor/LaborType/@code='LAG'"><xsl:value-of select="Labor/ActualLaborHours * sgxsl:getvar('BodyLaborRate')"/></xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborHoursChangedFlag">
							<xsl:choose>
								<xsl:when test="Labor/UserChangedLaborHoursFlag='Y' and Labor/DBLaborHours!=0">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborHours">
							<xsl:choose>
								<xsl:when test="number(Labor/ActualLaborHours)=0"><xsl:value-of select="Labor/DBLaborHours"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="Labor/ActualLaborHours"/></xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborIncludedFlag">
							<xsl:choose>
								<xsl:when test="number(Labor/ActualLaborHours)=0">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborTaxedFlag">
							<xsl:choose>
								<xsl:when test="Labor/LaborTaxedFlag='Y'">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborType">
							<xsl:choose>
								<xsl:when test="Labor/LaborType/@code='LAB'">Body</xsl:when>
								<xsl:when test="Labor/LaborType/@code='LAD'">Mechanical</xsl:when>
								<xsl:when test="Labor/LaborType/@code='LAE'">Mechanical</xsl:when>
								<xsl:when test="Labor/LaborType/@code='LAR'">Refinish</xsl:when>
								<xsl:when test="Labor/LaborType/@code='LAS'">Frame</xsl:when>
								<xsl:when test="Labor/LaborType/@code='LAF'">Frame</xsl:when>
								<xsl:when test="Labor/LaborType/@code='LAM'">Mechanical</xsl:when>
								<xsl:when test="Labor/LaborType/@code='LAG'">Body</xsl:when>
								<xsl:when test="Labor/LaborType/@code='LA1'">User Defined</xsl:when>
								<xsl:when test="Labor/LaborType/@code='LA2'">User Defined</xsl:when>
								<xsl:when test="Labor/LaborType/@code='LA3'">User Defined</xsl:when>
								<xsl:when test="Labor/LaborType/@code='LA4'">User Defined</xsl:when>
								<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</Labor>

					<!--- Update running totals with any part price.  Make sure we put it in the right bucket -->
					<xsl:if test="number(Labor/ActualLaborHours) &gt; 0">
						<xsl:choose>
							<xsl:when test="Labor/LaborType/@code='LAB'"><xsl:value-of select="sgxsl:setvar('DetailBodyLaborHours', round((sgxsl:getvar('DetailBodyLaborHours') + number(Labor/ActualLaborHours)) * 10) div 10)"/></xsl:when>
							<xsl:when test="Labor/LaborType/@code='LAF' or Labor/LaborType/@code='LAS'"><xsl:value-of select="sgxsl:setvar('DetailFrameLaborHours', round((sgxsl:getvar('DetailFrameLaborHours') + number(Labor/ActualLaborHours)) * 10) div 10)"/></xsl:when>
							<xsl:when test="Labor/LaborType/@code='LAD' or Labor/LaborType/@code='LAE' or Labor/LaborType/@code='LAM'"><xsl:value-of select="sgxsl:setvar('DetailMechLaborHours', round((sgxsl:getvar('DetailMechLaborHours') + number(Labor/ActualLaborHours)) * 10) div 10)"/></xsl:when>
							<xsl:when test="Labor/LaborType/@code='LAR'"><xsl:value-of select="sgxsl:setvar('DetailPaintLaborHours', round((sgxsl:getvar('DetailPaintLaborHours') + number(Labor/ActualLaborHours)) * 10) div 10)"/></xsl:when>
						</xsl:choose>
					</xsl:if>
				</xsl:if>
			</DetailLine>
		</xsl:for-each>
	</Detail>
</xsl:template>


<!--- Transform Profile Information -->
<xsl:template match="TransactionData" mode="Profile">
	<Profiles>
		<ProfileItem Category="Parts" Type="OEM">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType/@code='PAN']/PartTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="MarkupPercent"><xsl:value-of select="ProfileRates/PartProfile[PartType/@code='PAN']/PartMarkupPercent"/></xsl:attribute>
			<xsl:attribute name="DiscountPercent"><xsl:value-of select="ProfileRates/PartProfile[PartType/@code='PAN']/PartDiscountPercent"/></xsl:attribute>			
		</ProfileItem>
		<ProfileItem Category="Parts" Type="LKQ">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType/@code='PAL']/PartTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="MarkupPercent"><xsl:value-of select="ProfileRates/PartProfile[PartType/@code='PAL']/PartMarkupPercent"/></xsl:attribute>
			<xsl:attribute name="DiscountPercent"><xsl:value-of select="ProfileRates/PartProfile[PartType/@code='PAL']/PartDiscountPercent"/></xsl:attribute>			
		</ProfileItem>
		<ProfileItem Category="Parts" Type="Aftermarket">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType/@code='PAA']/PartTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="MarkupPercent"><xsl:value-of select="ProfileRates/PartProfile[PartType/@code='PAA']/PartMarkupPercent"/></xsl:attribute>
			<xsl:attribute name="DiscountPercent"><xsl:value-of select="ProfileRates/PartProfile[PartType/@code='PAA']/PartDiscountPercent"/></xsl:attribute>			
		</ProfileItem>
		<ProfileItem Category="Parts" Type="Recycled">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType/@code='PAM']/PartTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="MarkupPercent"><xsl:value-of select="ProfileRates/PartProfile[PartType/@code='PAM']/PartMarkupPercent"/></xsl:attribute>
			<xsl:attribute name="DiscountPercent"><xsl:value-of select="ProfileRates/PartProfile[PartType/@code='PAM']/PartDiscountPercent"/></xsl:attribute>			
		</ProfileItem>
		<ProfileItem Category="Parts" Type="Front Glass">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType/@code='PAGW']/PartTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="MarkupPercent"><xsl:value-of select="ProfileRates/PartProfile[PartType/@code='PAGW']/PartMarkupPercent"/></xsl:attribute>
			<xsl:attribute name="DiscountPercent"><xsl:value-of select="ProfileRates/PartProfile[PartType/@code='PAGW']/PartDiscountPercent"/></xsl:attribute>			
		</ProfileItem>
		<ProfileItem Category="Labor" Type="Body">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="Rate"><xsl:value-of select="ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborHourlyRate"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Labor" Type="Frame">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="Rate"><xsl:value-of select="ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborHourlyRate"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Labor" Type="Mechanical">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="Rate"><xsl:value-of select="ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborHourlyRate"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Labor" Type="Refinish">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="Rate"><xsl:value-of select="ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborHourlyRate"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Materials" Type="Paint1Stage">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/MaterialsTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="Rate"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/LaborRate"/></xsl:attribute>
			<xsl:attribute name="MaxDollars"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/CalcMaxDollars"/></xsl:attribute>
			<xsl:attribute name="MaxHours"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/CalcLaborMaxHours"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Materials" Type="Paint2Stage" Taxable="No"/>
		<ProfileItem Category="Materials" Type="Paint3Stage" Taxable="No"/>
	</Profiles>
</xsl:template>


<!--- Transform Summary Information -->
<xsl:template match="TransactionData" mode="Summary">
	<!--- First we need to get together all the values we need to build the summary -->

	<!--- Part Costs -->

	<!--- Taxable Discount Amount (Parts) -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals/TaxableDiscountAmount"><xsl:value-of select="sgxsl:setvar('DiscountTaxAmount', sum(SUBTOTAL/Subtotals/TaxableDiscountAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('DiscountTaxAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Non-Taxable Discount Amount (Parts) -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals/NonTaxableDiscountAmount"><xsl:value-of select="sgxsl:setvar('DiscountNonTaxAmount', sum(SUBTOTAL/Subtotals/NonTaxableDiscountAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('DiscountNonTaxAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Discount Amount Add Taxable and Nontaxable discounts -->
	<xsl:value-of select="sgxsl:setvar('TotalDiscountAmount', round((sgxsl:getvar('DiscountTaxAmount') + sgxsl:getvar('DiscountNonTaxAmount')) * 100) div 100)"/>

	<!--- Discount Percent (Parts) -->
	<xsl:choose>
       	<xsl:when test="sgxsl:getvar('TotalDiscountAmount')='0'"><xsl:value-of select="sgxsl:setvar('DiscountPct', 0)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('DiscountPct', round((-1 * sgxsl:getvar('TotalDiscountAmount') div sgxsl:getvar('DiscountMarkupBase') * 100) * 1000) div 1000)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Taxable Markup Amount (Parts) -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals/TaxableMarkupAmount"><xsl:value-of select="sgxsl:setvar('MarkupTaxAmount', sum(SUBTOTAL/Subtotals/TaxableMarkupAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('MarkupTaxAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Non-Taxable Markup Amount (Parts) -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals/NonTaxableMarkupAmount"><xsl:value-of select="sgxsl:setvar('MarkupNonTaxAmount', sum(SUBTOTAL/Subtotals/NonTaxableMarkupAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('MarkupNonTaxAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Markup Amount Add Taxable and Nontaxable markups -->
	<xsl:value-of select="sgxsl:setvar('TotalMarkupAmount', round((sgxsl:getvar('MarkupTaxAmount') + sgxsl:getvar('MarkupNonTaxAmount')) * 100) div 100)"/>

	<!--- Markup Percent (Parts) -->
	<xsl:choose>
       	<xsl:when test="sgxsl:getvar('TotalMarkupAmount')='0'"><xsl:value-of select="sgxsl:setvar('MarkupPct', 0)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('MarkupPct', round((sgxsl:getvar('TotalMarkupAmount') div sgxsl:getvar('DiscountMarkupBase') * 100) * 1000) div 1000)"/></xsl:otherwise>
     </xsl:choose>


	<!--- Total Taxable Parts -->	
	<xsl:value-of select="sgxsl:setvar('TotalTaxParts', round((sum(SUBTOTAL/Subtotals[TotalType/@code='PA' and TotalTypeCode/@code!='PAT' and TotalTypeCode/@code!='PAS' and TotalTypeCode/@code!='PAO']/TaxableAmount))  * 100) div 100)"/> 

	<!--- Total Non-Taxable Parts -->	
	<xsl:value-of select="sgxsl:setvar('TotalNonTaxParts', round((sum(SUBTOTAL/Subtotals[TotalType/@code='PA' and TotalTypeCode/@code!='PAT' and TotalTypeCode/@code!='PAS' and TotalTypeCode/@code!='PAO']/NonTaxableAmount))  * 100) div 100)"/>

	<!--- Other Taxable Parts (Taxable PAO) (not used for ADP, just set to 0) -->	
	<xsl:value-of select="sgxsl:setvar('TotalOtherTaxParts', 0)"/>

	<!--- Other NonTaxable Parts (NonTaxable PAO) (not used for ADP, just set to 0) -->	
	<xsl:value-of select="sgxsl:setvar('TotalOtherNonTaxParts', 0)"/>

	<!--- Total Parts -->	
	<xsl:value-of select="sgxsl:setvar('TotalParts', round((sgxsl:getvar('TotalTaxParts') + sgxsl:getvar('TotalNonTaxParts') + sgxsl:getvar('TotalDiscountAmount') + sgxsl:getvar('TotalMarkupAmount') + sgxsl:getvar('TotalOtherTaxParts') + sgxsl:getvar('TotalOtherNonTaxParts')) * 100) div 100)"/> 


	<!--- Body Materials Amount  -->
	<xsl:choose>
       	<xsl:when test="number(SUBTOTAL/Subtotals[TotalTypeCode/@code='MASH']/TaxableAmount) &gt; 0"><xsl:value-of select="sgxsl:setvar('BodyMaterialAmount', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='MASH']/TaxableAmount))"/></xsl:when>
       	<xsl:when test="number(SUBTOTAL/Subtotals[TotalTypeCode/@code='MASH']/NonTaxableAmount) &gt; 0"><xsl:value-of select="sgxsl:setvar('BodyMaterialAmount', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='MASH']/NonTaxableAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('BodyMaterialAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Body Material Rate -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/MaterialsProfile[MaterialType/@code='MASH']/LaborRate"><xsl:value-of select="sgxsl:setvar('BodyMaterialRate', number(ProfileRates/MaterialsProfile[MaterialType/@code='MASH']/LaborRate))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('BodyMaterialRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Body Material Hours -->
	<xsl:choose>
       	<xsl:when test="number(SUBTOTAL/Subtotals[TotalTypeCode/@code='MASH']/TaxableAmount) &gt; 0"><xsl:value-of select="sgxsl:setvar('BodyMaterialHours', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAB' or TotalTypeCode/@code='LAM' or TotalTypeCode/@code='LAF']/TaxableHours))"/></xsl:when>
       	<xsl:when test="number(SUBTOTAL/Subtotals[TotalTypeCode/@code='MASH']/NonTaxableAmount) &gt; 0"><xsl:value-of select="sgxsl:setvar('BodyMaterialHours', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAB' or TotalTypeCode/@code='LAM' or TotalTypeCode/@code='LAF']/NonTaxableHours))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('BodyMaterialHours', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Paint Materials Amount -->
	<xsl:choose>
       	<xsl:when test="number(SUBTOTAL/Subtotals[TotalTypeCode/@code='MAPA']/TaxableAmount) &gt; 0"><xsl:value-of select="sgxsl:setvar('PaintMaterialAmount', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='MAPA']/TaxableAmount))"/></xsl:when>
       	<xsl:when test="number(SUBTOTAL/Subtotals[TotalTypeCode/@code='MAPA']/NonTaxableAmount) &gt; 0"><xsl:value-of select="sgxsl:setvar('PaintMaterialAmount', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='MAPA']/NonTaxableAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('PaintMaterialAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Paint Material Rate -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/LaborRate"><xsl:value-of select="sgxsl:setvar('PaintMaterialRate', number(ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/LaborRate))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('PaintMaterialRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Paint Material Hours -->
	<xsl:choose>
       	<xsl:when test="number(SUBTOTAL/Subtotals[TotalTypeCode/@code='MAPA']/TaxableAmount) &gt; 0"><xsl:value-of select="sgxsl:setvar('PaintMaterialHours', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAR']/TaxableHours))"/></xsl:when>
       	<xsl:when test="number(SUBTOTAL/Subtotals[TotalTypeCode/@code='MAPA']/NonTaxableAmount) &gt; 0"><xsl:value-of select="sgxsl:setvar('PaintMaterialHours', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAR']/NonTaxableHours))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('PaintMaterialHours', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Materials -->	
	<xsl:value-of select="sgxsl:setvar('TotalMaterials', round((sgxsl:getvar('BodyMaterialAmount') + sgxsl:getvar('PaintMaterialAmount')) * 100) div 100)"/> 

	<!--- Total Parts and Materials -->	
	<xsl:value-of select="sgxsl:setvar('TotalPartsMaterials', round((sgxsl:getvar('TotalParts') + sgxsl:getvar('TotalMaterials')) * 100) div 100)"/> 


	<!-- Labor Costs -->

	<!--- Body Labor Amount -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAB']/TaxableAmount &gt; 0"><xsl:value-of select="sgxsl:setvar('BodyLaborAmount', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAB']/TaxableAmount))"/></xsl:when>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAB']/NonTaxableAmount &gt; 0"><xsl:value-of select="sgxsl:setvar('BodyLaborAmount', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAB']/NonTaxableAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('BodyLaborAmount', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Body Labor Hours -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAB']/TaxableHours &gt; 0"><xsl:value-of select="sgxsl:setvar('BodyLaborHours', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAB']/TaxableHours))"/></xsl:when>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAB']/NonTaxableHours &gt; 0"><xsl:value-of select="sgxsl:setvar('BodyLaborHours', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAB']/NonTaxableHours))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('BodyLaborHours', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Frame Labor Amount -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAF' or TotalTypeCode/@code='LAS']/TaxableAmount &gt; 0"><xsl:value-of select="sgxsl:setvar('FrameLaborAmount', sum(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAF' or TotalTypeCode/@code='LAS']/TaxableAmount))"/></xsl:when>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAF' or TotalTypeCode/@code='LAS']/NonTaxableAmount &gt; 0"><xsl:value-of select="sgxsl:setvar('FrameLaborAmount', sum(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAF' or TotalTypeCode/@code='LAS']/NonTaxableAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('FrameLaborAmount', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Frame Labor Hours -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAF' or TotalTypeCode/@code='LAS']/TaxableHours &gt; 0"><xsl:value-of select="sgxsl:setvar('FrameLaborHours', sum(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAF' or TotalTypeCode/@code='LAS']/TaxableHours))"/></xsl:when>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAF' or TotalTypeCode/@code='LAS']/NonTaxableHours &gt; 0"><xsl:value-of select="sgxsl:setvar('FrameLaborHours', sum(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAF' or TotalTypeCode/@code='LAS']/NonTaxableHours))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('FrameLaborHours', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!---Mechanical Labor Amount -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAD' or TotalTypeCode/@code='LAE' or TotalTypeCode/@code='LAM']/TaxableAmount &gt; 0"><xsl:value-of select="sgxsl:setvar('MechLaborAmount', sum(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAD' or TotalTypeCode/@code='LAE' or TotalTypeCode/@code='LAM']/TaxableAmount))"/></xsl:when>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAD' or TotalTypeCode/@code='LAE' or TotalTypeCode/@code='LAM']/NonTaxableAmount &gt; 0"><xsl:value-of select="sgxsl:setvar('MechLaborAmount', sum(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAD' or TotalTypeCode/@code='LAE' or TotalTypeCode/@code='LAM']/NonTaxableAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('MechLaborAmount', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Mechanical Labor Hours -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAD' or TotalTypeCode/@code='LAE' or TotalTypeCode/@code='LAM']/TaxableHours &gt; 0"><xsl:value-of select="sgxsl:setvar('MechLaborHours', sum(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAD' or TotalTypeCode/@code='LAE' or TotalTypeCode/@code='LAM']/TaxableHours))"/></xsl:when>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAD' or TotalTypeCode/@code='LAE' or TotalTypeCode/@code='LAM']/NonTaxableHours &gt; 0"><xsl:value-of select="sgxsl:setvar('MechLaborHours', sum(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAD' or TotalTypeCode/@code='LAE' or TotalTypeCode/@code='LAM']/NonTaxableHours))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('MechLaborHours', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Paint Labor Amount -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAR']/TaxableAmount &gt; 0"><xsl:value-of select="sgxsl:setvar('PaintLaborAmount', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAR']/TaxableAmount))"/></xsl:when>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAR']/NonTaxableAmount &gt; 0"><xsl:value-of select="sgxsl:setvar('PaintLaborAmount', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAR']/NonTaxableAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('PaintLaborAmount', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Paint Labor Hours -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAR']/TaxableHours &gt; 0"><xsl:value-of select="sgxsl:setvar('PaintLaborHours', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAR']/TaxableHours))"/></xsl:when>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAR']/NonTaxableHours &gt; 0"><xsl:value-of select="sgxsl:setvar('PaintLaborHours', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAR']/NonTaxableHours))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('PaintLaborHours', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Total Labor Amount -->
     <xsl:value-of select="sgxsl:setvar('TotalLaborAmount', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAT']/TotalTypeAmountTaxed) + number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAT']/TotalTypeAmountNonTaxed))"/>

	<!--- Other Labor (This is the difference between the Total Labor retrieved above and that calculated from the other values) -->	
	<xsl:value-of select="sgxsl:setvar('TotalOtherLabor', round((sgxsl:getvar('TotalLaborAmount') - (sgxsl:getvar('BodyLaborAmount') + sgxsl:getvar('FrameLaborAmount') + sgxsl:getvar('MechLaborAmount') + sgxsl:getvar('PaintLaborAmount'))) * 100) div 100)"/>
	
	
	<!--- Additional Charges -->
	
	<!--- Storage Amount -->
	<xsl:choose>
       	<xsl:when test="number(SUBTOTAL/Subtotals[TotalType/@code='OTST']/TotalTypeAmountTaxed) &gt; 0"><xsl:value-of select="sgxsl:setvar('StorageAmount', number(SUBTOTAL/Subtotals[TotalType/@code='OTST']/TotalTypeAmountTaxed))"/></xsl:when>
       	<xsl:when test="number(SUBTOTAL/Subtotals[TotalType/@code='OTST']/TotalTypeAmountNonTaxed) &gt; 0"><xsl:value-of select="sgxsl:setvar('StorageAmount', number(SUBTOTAL/Subtotals[TotalType/@code='OTST']/TotalTypeAmountNonTaxed))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('StorageAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Sublet Amount -->
	<xsl:choose>
       	<xsl:when test="number(SUBTOTAL/Subtotals[TotalTypeCode/@code='PAS']/TaxableAmount) &gt; 0"><xsl:value-of select="sgxsl:setvar('SubletAmount', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='PAS']/TaxableAmount))"/></xsl:when>
       	<xsl:when test="number(SUBTOTAL/Subtotals[TotalTypeCode/@code='PAS']/NonTaxableAmount) &gt; 0"><xsl:value-of select="sgxsl:setvar('SubletAmount', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='PAS']/NonTaxableAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('SubletAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Towing Amount -->
	<xsl:choose>
       	<xsl:when test="number(SUBTOTAL/Subtotals[TotalType/@code='OTTW']/TotalTypeAmountTaxed) &gt; 0"><xsl:value-of select="sgxsl:setvar('TowingAmount', number(SUBTOTAL/Subtotals[TotalType/@code='OTTW']/TotalTypeAmountTaxed))"/></xsl:when>
       	<xsl:when test="number(SUBTOTAL/Subtotals[TotalType/@code='OTTW']/TotalTypeAmountNonTaxed) &gt; 0"><xsl:value-of select="sgxsl:setvar('TowingAmount', number(SUBTOTAL/Subtotals[TotalType/@code='OTTW']/TotalTypeAmountNonTaxed))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('TowingAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Other Additional Charges -->
	<xsl:value-of select="sgxsl:setvar('OtherAddlChargesAmount', 0)"/>

	<!--- Total Additional Charges (Storage, Sublet, Towing, and Other added together) -->	
	<xsl:value-of select="sgxsl:setvar('TotalAddlCharges', sgxsl:getvar('StorageAmount') + sgxsl:getvar('SubletAmount') + sgxsl:getvar('TowingAmount') + sgxsl:getvar('OtherAddlChargesAmount'))"/>


	<!--- Subtotal -->	
	<xsl:value-of select="sgxsl:setvar('SubtotalAmount', round((sgxsl:getvar('TotalPartsMaterials') + sgxsl:getvar('TotalLaborAmount') + sgxsl:getvar('TotalAddlCharges')) * 100) div 100)"/> 


	<!--- Taxes -->
	
	<!--- Parts & Materials Tax  -->

	<!--- Sales Tax Base -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='PAT']/TotalTypeTaxAmount &gt; 0 and ProfileRates/MaterialsProfile[1]/MaterialsTaxableFlag='N'"><xsl:value-of select="sgxsl:setvar('SalesTaxBasePM', sgxsl:getvar('TotalParts'))"/></xsl:when>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='PAT']/TotalTypeTaxAmount &gt; 0"><xsl:value-of select="sgxsl:setvar('SalesTaxBasePM', sgxsl:getvar('TotalPartsMaterials'))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('SalesTaxBasePM', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Sales Tax Percent -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='PAT']/TotalTypeTaxAmount &gt; 0"><xsl:value-of select="sgxsl:setvar('SalesTaxPctPM', number(ProfileRates/PartProfile[starts-with(PartType/@code, 'P')]/Tax/@rate))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('SalesTaxPctPM', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Sales Tax Amount -->
	<xsl:choose>
       	<xsl:when test="sgxsl:getvar('SalesTaxBasePM')='0'"><xsl:value-of select="sgxsl:setvar('SalesTaxAmountPM', '0')"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('SalesTaxAmountPM', round(sgxsl:getvar('SalesTaxBasePM') * (sgxsl:getvar('SalesTaxPctPM') div 100) * 100) div 100)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Labor Tax  -->
	
	<!--- Sales Tax Base -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAT']/TotalTypeTaxAmount &gt; 0"><xsl:value-of select="sgxsl:setvar('SalesTaxBaseLabor', number(SUBTOTAL/Subtotals[TotalTypeCode/@code='LAT']/TotalTypeAmountTaxed))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('SalesTaxBaseLabor', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Sales Tax Percent -->
	<xsl:choose>
       	<xsl:when test="SUBTOTAL/Subtotals[TotalTypeCode/@code='LAT']/TotalTypeTaxAmount &gt; 0"><xsl:value-of select="sgxsl:setvar('SalesTaxPctLabor',number(ProfileRates/LaborProfile[1]/Tax/@rate))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('SalesTaxPctLabor', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Sales Tax Amount -->
	<xsl:choose>
       	<xsl:when test="sgxsl:getvar('SalesTaxBaseLabor')='0'"><xsl:value-of select="sgxsl:setvar('SalesTaxAmountLabor', '0')"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('SalesTaxAmountLabor', round(sgxsl:getvar('SalesTaxBaseLabor') * (sgxsl:getvar('SalesTaxPctLabor') div 100) * 100) div 100)"/></xsl:otherwise>
     </xsl:choose>


	<!--- Total Tax Amount -->
	<xsl:choose>
       	<xsl:when test="Totals/TaxAmount"><xsl:value-of select="sgxsl:setvar('TotalTaxAmount', number(Totals/TaxAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('TotalTaxAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Other Taxes -->
	<xsl:value-of select="sgxsl:setvar('OtherTaxAmount', round((sgxsl:getvar('TotalTaxAmount') - sgxsl:getvar('SalesTaxAmountPM') - sgxsl:getvar('SalesTaxAmountLabor')) * 100) div 100)"/> 
	
	<!--- Total Repair Cost -->
	<xsl:value-of select="sgxsl:setvar('RepairTotalAmount', round((sgxsl:getvar('SubtotalAmount') + sgxsl:getvar('TotalTaxAmount')) * 100) div 100)"/> 

			
	<!--- Adjustments -->
	
	<!--- Appearance Allowance -->
	<xsl:choose>
       	<xsl:when test="Totals/AppearanceAllowanceAmount"><xsl:value-of select="sgxsl:setvar('AppearanceAllowanceAmount', -1 * number(Totals/AppearanceAllowanceAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('AppearanceAllowanceAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Betterment -->
	<xsl:choose>
       	<xsl:when test="Totals/BettermentAmount"><xsl:value-of select="sgxsl:setvar('BettermentAmount', number(Totals/BettermentAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('BettermentAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Deductible -->
	<xsl:choose>
       	<xsl:when test="Totals/DeductibleAmount"><xsl:value-of select="sgxsl:setvar('DeductibleAmount', number(Totals/DeductibleAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('DeductibleAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Other Adjustment Amount (There are additional adjustments that may appear on ADP estimates.  "Less Other Charges" - This is determined by taking the CustomerResponsibilityAmount.  Also "Bottom Line Adjustment" which is determined by looking at the BLA profile discount amount, and if > 0, multiplying the percentage by total Parts and Labor.  Related Prior Damage.  These are added together to obtain Other Adjustment Amount. -->
    	<xsl:value-of select="sgxsl:setvar('LessOtherAmount', number(Totals/CustomerResponsibilityAmount))"/>

	<xsl:choose>
          <xsl:when test="number(ProfileRates/PartProfile[PartType/@code='BLA']/PartDiscountPercent) &gt; 0"><xsl:value-of select="sgxsl:setvar('BottomLineAdjAmount', round(((sgxsl:getvar('TotalPartsMaterials') + sgxsl:getvar('TotalLaborAmount')) * (number(ProfileRates/PartProfile[PartType/@code='BLA']/PartDiscountPercent) div 100)) * 100) div 100)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('BottomLineAdjAmount', 0)"/></xsl:otherwise>
    </xsl:choose>

	<xsl:choose>
          <xsl:when test="SUBTOTAL/Subtotals[TotalType/@code='RPD']"><xsl:value-of select="sgxsl:setvar('RelatedPriorDamageAmount', round((number(SUBTOTAL/Subtotals[TotalType/@code='RPD']/TotalTypeAmountTaxed) + number(SUBTOTAL/Subtotals[TotalType/@code='RPD']/TotalTypeAmountNonTaxed)) * 100) div 100 )"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('RelatedPriorDamageAmount', 0)"/></xsl:otherwise>
    </xsl:choose>

	<xsl:value-of select="sgxsl:setvar('OtherAdjustmentAmount', round((sgxsl:getvar('LessOtherAmount') + sgxsl:getvar('BottomLineAdjAmount') + sgxsl:getvar('RelatedPriorDamageAmount')) * 100) div 100)"/> 
	
	<!--- Total Adjustment (Deductible, Appearance Allowance, and Betterment added together -->	
	<xsl:value-of select="sgxsl:setvar('TotalAdjustmentAmount', round((sgxsl:getvar('AppearanceAllowanceAmount') + sgxsl:getvar('BettermentAmount') + sgxsl:getvar('DeductibleAmount') + sgxsl:getvar('OtherAdjustmentAmount')) * 100) div 100)"/>

		
	<!--- Net Total -->
	<xsl:choose>
       	<xsl:when test="Totals/TotalAmount"><xsl:value-of select="sgxsl:setvar('NetTotalAmount', number(Totals/NetTotalAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="sgxsl:setvar('NetTotalAmount', 0)"/></xsl:otherwise>
     </xsl:choose>


	<!--- Variance Amount  (This is a "bit bucket" to capture variances between what I calculate and what the "real" estimate total is) -->
	<xsl:value-of select="sgxsl:setvar('ZZVarianceAmount', round((sgxsl:getvar('RepairTotalAmount') - sgxsl:getvar('AppearanceAllowanceAmount') - sgxsl:getvar('BettermentAmount') - sgxsl:getvar('DeductibleAmount') - sgxsl:getvar('OtherAdjustmentAmount') - sgxsl:getvar('NetTotalAmount')) * 100) div 100)"/> 


	<!--- Continue building output -->
	<Summary>
		<LineItem Type="PartsTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="Yes">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TotalTaxParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('TotalTaxParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsNonTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="No">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TotalNonTaxParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('TotalNonTaxParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsMarkup" TransferLine="Yes" Category="PC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="sgxsl:getvar('MarkupTaxAmount') &gt;= sgxsl:getvar('MarkupNonTaxAmount')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TotalMarkupAmount')"/></xsl:attribute>
			<xsl:attribute name="Percent"><xsl:value-of select="sgxsl:getvar('MarkupPct')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount">
				<xsl:choose>
                    	<xsl:when test="sgxsl:getvar('TotalMarkupAmount') = 0">0</xsl:when>
					<xsl:otherwise><xsl:value-of select="sgxsl:getvar('DiscountMarkupBase')"/></xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
		</LineItem>
		<LineItem Type="PartsDiscount" TransferLine="Yes" Category="PC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="sgxsl:getvar('DiscountTaxAmount') &lt;= sgxsl:getvar('DiscountNonTaxAmount')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TotalDiscountAmount')"/></xsl:attribute>
			<xsl:attribute name="Percent"><xsl:value-of select="sgxsl:getvar('DiscountPct')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount">
				<xsl:choose>
                    	<xsl:when test="sgxsl:getvar('TotalDiscountAmount') = 0">0</xsl:when>
					<xsl:otherwise><xsl:value-of select="sgxsl:getvar('DiscountMarkupBase')"/></xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
		</LineItem>
		<LineItem Type="PartsOtherTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="Yes">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TotalOtherTaxParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('TotalOtherTaxParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsOtherNonTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="No">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TotalOtherNonTaxParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('TotalOtherNonTaxParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TotalParts')"/></xsl:attribute>
		</LineItem>
		
		<LineItem Type="Body" TransferLine="Yes" Category="MC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/MaterialsProfile[MaterialType/@code='MASH']/MaterialsTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('BodyMaterialAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="sgxsl:getvar('BodyMaterialHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('BodyMaterialRate')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Paint" TransferLine="Yes" Category="MC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/MaterialsTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('PaintMaterialAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="sgxsl:getvar('PaintMaterialHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('PaintMaterialRate')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="SuppliesTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TotalMaterials')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsAndMaterialTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TotalPartsMaterials')"/></xsl:attribute>
		</LineItem>
		
		<LineItem Type="Body" TransferLine="Yes" Category="LC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('BodyLaborAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="sgxsl:getvar('BodyLaborHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('BodyLaborRate')"/></xsl:attribute>
			<xsl:attribute name="RepairHours"/>
			<xsl:attribute name="ReplaceHours"/>
		</LineItem>
		<LineItem Type="Mechanical" TransferLine="Yes" Category="LC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('MechLaborAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="sgxsl:getvar('MechLaborHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('MechLaborRate')"/></xsl:attribute>
			<xsl:attribute name="RepairHours"/>
			<xsl:attribute name="ReplaceHours"/>
		</LineItem>
		<LineItem Type="Frame" TransferLine="Yes" Category="LC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('FrameLaborAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="sgxsl:getvar('FrameLaborHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('FrameLaborRate')"/></xsl:attribute>
			<xsl:attribute name="RepairHours"/>
			<xsl:attribute name="ReplaceHours"/>
		</LineItem>
		<LineItem Type="Paint" TransferLine="Yes" Category="LC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('PaintLaborAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="sgxsl:getvar('PaintLaborHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('PaintLaborRate')"/></xsl:attribute>
			<xsl:attribute name="RepairHours"/>
			<xsl:attribute name="ReplaceHours"/>
		</LineItem>
		<LineItem Type="Refinish" TransferLine="Yes" Category="LC" Note="" Taxable="Yes" >
			<xsl:attribute name="ExtendedAmount">0</xsl:attribute>
			<xsl:attribute name="Hours">0</xsl:attribute>
			<xsl:attribute name="UnitAmount">0</xsl:attribute>
			<xsl:attribute name="RepairHours"/>
			<xsl:attribute name="ReplaceHours"/>
		</LineItem>
		<LineItem Type="Other" TransferLine="Yes" Category="LC" Note="" Taxable="Yes" >
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TotalOtherLabor')"/></xsl:attribute>
			<xsl:attribute name="Hours">0</xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('TotalOtherLabor')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="LaborTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TotalLaborAmount')"/></xsl:attribute>
		</LineItem>
			
          <xsl:if test="sgxsl:getvar('TowingAmount') > 0">
			<LineItem Type="Towing" TransferLine="Yes" Category="AC" Note="">
				<xsl:attribute name="Taxable">
					<xsl:choose>
	                    	<xsl:when test="ProfileRates/AdditionalCharges[AddtlChargesID='OTTW']/Tax">Yes</xsl:when>
	                		<xsl:otherwise>No</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TowingAmount')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('TowingAmount')"/></xsl:attribute>
			</LineItem>
          </xsl:if>
          <xsl:if test="sgxsl:getvar('StorageAmount') > 0">
			<LineItem Type="Storage" TransferLine="Yes" Category="AC" Note="">
				<xsl:attribute name="Taxable">
					<xsl:choose>
	                    	<xsl:when test="ProfileRates/AdditionalCharges[AddtlChargesID='OTST']/Tax">Yes</xsl:when>
	                		<xsl:otherwise>No</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('StorageAmount')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('StorageAmount')"/></xsl:attribute>
			</LineItem>
          </xsl:if>
          <xsl:if test="sgxsl:getvar('SubletAmount') > 0">
			<LineItem Type="Sublet" TransferLine="Yes" Category="AC" Note="">
				<xsl:attribute name="Taxable">
					<xsl:choose>
	                    	<xsl:when test="ProfileRates/PartProfile[PartType='PAS']/PartTaxableFlag='Y'">Yes</xsl:when>
	                		<xsl:otherwise>No</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('SubletAmount')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('SubletAmount')"/></xsl:attribute>
			</LineItem>
          </xsl:if>
          <xsl:if test="sgxsl:getvar('OtherAddlChargesAmount') > 0">
			<LineItem Type="Other" TransferLine="Yes" Category="AC" Note="" Taxable="Yes">
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('OtherAddlChargesAmount')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('OtherAddlChargesAmount')"/></xsl:attribute>
			</LineItem>
          </xsl:if>
		<LineItem Type="AdditionalChargesTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TotalAddlCharges')"/></xsl:attribute>
		</LineItem>

		<LineItem Type="SubTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('SubtotalAmount')"/></xsl:attribute>
		</LineItem>

          <xsl:if test="sgxsl:getvar('SalesTaxAmountPM') &gt; 0">
			<LineItem Type="Sales" TransferLine="Yes" Category="TX" Note="">
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('SalesTaxAmountPM')"/></xsl:attribute>
				<xsl:attribute name="Percent"><xsl:value-of select="sgxsl:getvar('SalesTaxPctPM')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('SalesTaxBasePM')"/></xsl:attribute>
				<xsl:attribute name="TaxBase">
					<xsl:choose>
		                    <xsl:when test="sgxsl:getvar('SalesTaxBasePM') = sgxsl:getvar('TotalParts')">Parts</xsl:when>
		                    <xsl:when test="sgxsl:getvar('SalesTaxBasePM') = sgxsl:getvar('TotalMaterials')">Materials</xsl:when>
		                    <xsl:when test="sgxsl:getvar('SalesTaxBasePM') = sgxsl:getvar('TotalPartsMaterials')">PartsAndMaterials</xsl:when>
						<xsl:otherwise>Other</xsl:otherwise>
	                	</xsl:choose>
				</xsl:attribute>
			</LineItem>
          </xsl:if>
          <xsl:if test="sgxsl:getvar('SalesTaxAmountLabor') &gt; 0">
			<LineItem Type="Sales" TransferLine="Yes" Category="TX" Note="">
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('SalesTaxAmountLabor')"/></xsl:attribute>
				<xsl:attribute name="Percent"><xsl:value-of select="sgxsl:getvar('SalesTaxPctLabor')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('SalesTaxBaseLabor')"/></xsl:attribute>
				<xsl:attribute name="TaxBase">
					<xsl:choose>
		                    <xsl:when test="sgxsl:getvar('SalesTaxBaseLabor') = sgxsl:getvar('TotalLaborAmount')">Labor</xsl:when>
						<xsl:otherwise>Other</xsl:otherwise>
	                	</xsl:choose>
				</xsl:attribute>
			</LineItem>
          </xsl:if>
          <xsl:if test="sgxsl:getvar('OtherTaxAmount') &gt; 0">
			<LineItem Type="Sales" TransferLine="Yes" Category="TX">
				<xsl:attribute name="Note"><xsl:text>Tax from Sublet, Towing, Storage or other source</xsl:text></xsl:attribute>
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('OtherTaxAmount')"/></xsl:attribute>
				<xsl:attribute name="Percent">100</xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('OtherTaxAmount')"/></xsl:attribute>
				<xsl:attribute name="TaxBase">Other</xsl:attribute>
			</LineItem>
          </xsl:if>
		<LineItem Type="TaxTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TotalTaxAmount')"/></xsl:attribute>
		</LineItem>

		<LineItem Type="RepairTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('RepairTotalAmount')"/></xsl:attribute>
		</LineItem>

		<LineItem Type="AppearanceAllowance" TransferLine="Yes" Category="AJ" Note="">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('AppearanceAllowanceAmount')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('AppearanceAllowanceAmount')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Betterment" TransferLine="Yes" Category="AJ" Note="">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('BettermentAmount')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('BettermentAmount')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Deductible" TransferLine="Yes" Category="AJ" Note="">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('DeductibleAmount')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('DeductibleAmount')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Other" TransferLine="Yes" Category="AJ" Note="">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('OtherAdjustmentAmount')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('OtherAdjustmentAmount')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="AdjustmentTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('TotalAdjustmentAmount')"/></xsl:attribute>
		</LineItem>


		<LineItem Type="NetTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('NetTotalAmount')"/></xsl:attribute>
		</LineItem>


		<LineItem Type="Variance" TransferLine="Yes" Category="VA">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('ZZVarianceAmount')"/></xsl:attribute>
		</LineItem>
		
		<LineItem Type="OEMParts" TransferLine="No">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType/@code='PAN']/PartTaxableFlag='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('DetailOEMPartsTotal')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('DetailOEMPartsTotal')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="2 Wheel Alignment" TransferLine="No">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('Detail2WAlign')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('Detail2WAlign')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="4 Wheel Alignment" TransferLine="No">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="sgxsl:getvar('Detail4WAlign')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="sgxsl:getvar('Detail4WAlign')"/></xsl:attribute>
		</LineItem>
		
	</Summary> 
</xsl:template>


<!--- Compile Audit Information -->
<xsl:template match="TransactionData" mode="Audit">
	<Audit>
		<AuditItem Name="VerifyDetailPartTaxTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="sgxsl:getvar('DetailPartTaxTotal') = sgxsl:getvar('TotalTaxParts')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="sgxsl:getvar('DetailPartTaxTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="sgxsl:getvar('TotalTaxParts')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyDetailPartNonTaxTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="sgxsl:getvar('DetailPartNonTaxTotal') = sgxsl:getvar('TotalNonTaxParts')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="sgxsl:getvar('DetailPartNonTaxTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="sgxsl:getvar('TotalNonTaxParts')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyDetailPartOtherTaxTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="sgxsl:getvar('DetailPartOtherTaxTotal') = sgxsl:getvar('TotalOtherTaxParts')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="sgxsl:getvar('DetailPartOtherTaxTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="sgxsl:getvar('TotalOtherTaxParts')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyDetailPartOtherNonTaxTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="sgxsl:getvar('DetailPartOtherNonTaxTotal') = sgxsl:getvar('TotalOtherNonTaxParts')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="sgxsl:getvar('DetailPartOtherNonTaxTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="sgxsl:getvar('TotalOtherNonTaxParts')"/></xsl:attribute>
		</AuditItem>

		<AuditItem Name="VerifyBodyLaborHours">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="sgxsl:getvar('DetailBodyLaborHours') = sgxsl:getvar('BodyLaborHours')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="sgxsl:getvar('DetailBodyLaborHours')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="sgxsl:getvar('BodyLaborHours')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyFrameLaborHours">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="sgxsl:getvar('DetailFrameLaborHours') = sgxsl:getvar('FrameLaborHours')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="sgxsl:getvar('DetailFrameLaborHours')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="sgxsl:getvar('FrameLaborHours')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyMechLaborHours">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="sgxsl:getvar('DetailMechLaborHours') = sgxsl:getvar('MechLaborHours')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="sgxsl:getvar('DetailMechLaborHours')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="sgxsl:getvar('MechLaborHours')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyPaintLaborHours">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="sgxsl:getvar('DetailPaintLaborHours') = sgxsl:getvar('PaintLaborHours')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="sgxsl:getvar('DetailPaintLaborHours')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="sgxsl:getvar('PaintLaborHours')"/></xsl:attribute>
		</AuditItem>
		
		<AuditItem Name="VerifyDetailSubletTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="sgxsl:getvar('DetailSubletTotal') = sgxsl:getvar('SubletAmount')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="sgxsl:getvar('DetailSubletTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="sgxsl:getvar('SubletAmount')"/></xsl:attribute>
		</AuditItem>		

		<AuditItem Name="VerifyNoDetailUnknownTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="sgxsl:getvar('DetailUnknownTotal') = 0">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="sgxsl:getvar('DetailUnknownTotal')"/></xsl:attribute>
		</AuditItem>		
	</Audit> 
</xsl:template>

</xsl:stylesheet>
