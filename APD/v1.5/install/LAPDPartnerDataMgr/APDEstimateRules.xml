<Rules>
  <Rule id="TotLosThreshold" desc="Total Loss Threshold Exceeded" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='RepairTotal' and @Category='TT']/@ExtendedAmount)" operation="greaterthan" comparemethod="number"
                       rparm="$FACT(TotalLossValue)"
                       betweenBegin="" betweenEnd="" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[Repair Total exceeds the Total Loss Threshold percentage ($CALC($XPATH(Root/Vehicle/@TotalLossWarningPercentage) * 100)%) of the Book value of the vehicle.]]>
    </Action>
  </Rule>
  <Rule id="VINMismatch" desc="VIN on estimate does not match claim" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Header/Vehicle/@Vin)" operation="notequal" comparemethod="text"
                       rparm="$XPATH(Root/Vehicle/@VIN)"
                       betweenBegin="" betweenEnd="" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The VIN on the estimate does not match the VIN in the claim file.]]>
    </Action>
  </Rule>
  <Rule id="VINMismatchNADA" desc="VIN not resolved with NADA" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Header/Vehicle/@VehicleYear)" operation="notequal" comparemethod="text"
                       rparm="$XPATH(Root/Vehicle/@VehicleYearNADA)"
                       betweenBegin="" betweenEnd=""/>
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The VIN either could not be resolved in the NADA used car database or it resolved to a vehicle Year other than that specified on the estimate.]]>
    </Action>
  </Rule>
  <Rule id="MileageZero" desc="Mileage not entered" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Header/Vehicle/@VehicleMileage)" operation="equal" comparemethod="text"
                       rparm="0"
                       betweenBegin="" betweenEnd="" />
        <Condition lparm="($XPATH(APDEstimate/Header/Vehicle/@VehicleMileage)).length" operation="equal" comparemethod="text"
                       rparm="0"
                       betweenBegin="" betweenEnd="" logicalOperation="or"/>
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Estimate did not contain an Odometer reading to indicate the vehicle mileage.]]>
    </Action>
  </Rule>
  <Rule id="PaintMaterialThreshold" desc="Max Refinish Materials Threshold Exceeded" enabled="Y" weight="0">
    <Default enabled="N">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='Paint' and @Category='MC']/@ExtendedAmount)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="$FACT(PaintMaterialThreshold)"
                       betweenBegin="" betweenEnd="" />
      </Conditions>
    </Default>
    <Exception enabled="Y">
      <Context>
        <Item lparm="$FACT(InsuranceCoID)" operation="equal" comparemethod="number" rparm="193"/>
      </Context>
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='Paint' and @Category='MC']/@ExtendedAmount)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="$FACT(PaintMaterialThresholdTrustgard)" />
      </Conditions>
    </Exception>
    <Exception enabled="Y">
      <Context>
        <Item lparm="1" operation="equal" comparemethod="number" rparm="1"/>
      </Context>
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='Paint' and @Category='MC']/@ExtendedAmount)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="$FACT(PaintMaterialThreshold)" />
      </Conditions>
    </Exception>
    <Action>
        <![CDATA[The charge for Refinish Materials specified on the Estimate exceeds Estimating guidelines ($$RVALUE(0)).]]>
    </Action>
  </Rule>
  <Rule id="PaintMaterialHourlyRate" desc="Refinish Materials Rate exceeds profile" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='Paint' and @Category='MC']/@UnitAmount)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="$XPATH(Root/ShopInfo/ShopPricing/@RefinishTwoStageHourly)"
                       betweenBegin="" betweenEnd="" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Refinish Materials Rate specified on the Estimate exceeds the rate specified in the shop profile ($$XPATH(Root/ShopInfo/ShopPricing/@RefinishTwoStageHourly)).]]>
    </Action>
  </Rule>
  <Rule id="BodyMaterialsIncluded" desc="Body Supply Charges Present" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='Body' and @Category='MC']/@ExtendedAmount)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="0"
                       betweenBegin="" betweenEnd="" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[Body Supplies Charges are included on the estimate.]]>
    </Action>
  </Rule>
  <Rule id="BodyLaborRate" desc="Body Labor Rate exceeds profile" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='Body' and @Category='LC']/@UnitAmount)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="$XPATH(Root/ShopInfo/ShopPricing/@HourlyRateSheetMetal)"
                       betweenBegin="" betweenEnd="" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Body Labor Rate specified on the Estimate exceeds the rate specified on the shop profile ($$XPATH(Root/ShopInfo/ShopPricing/@HourlyRateSheetMetal)).]]>
    </Action>
  </Rule>
  <Rule id="RefinishLaborRate" desc="Refinish Labor Rate exceeds profile" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='Refinish' and @Category='LC']/@UnitAmount)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="$XPATH(Root/ShopInfo/ShopPricing/@HourlyRateRefinishing)"
                       betweenBegin="" betweenEnd="" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Refinish Labor Rate specified on the Estimate exceeds the rate specified on the shop profile ($$XPATH(Root/ShopInfo/ShopPricing/@HourlyRateRefinishing)).]]>
    </Action>
  </Rule>
  <Rule id="MechanicalLaborRate" desc="Mechanical Labor Rate exceeds profile" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='Mechanical' and @Category='LC']/@UnitAmount)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="$XPATH(Root/ShopInfo/ShopPricing/@HourlyRateMechanical)"
                       betweenBegin="" betweenEnd="" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Refinish Labor Rate specified on the Estimate exceeds the rate specified on the shop profile ($$XPATH(Root/ShopInfo/ShopPricing/@HourlyRateMechanical)).]]>
    </Action>
  </Rule>
  <Rule id="FrameLaborRate" desc="Frame Labor Rate exceeds profile" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='Frame' and @Category='LC']/@UnitAmount)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="$XPATH(Root/ShopInfo/ShopPricing/@HourlyRateUnibodyFrame)"
                       betweenBegin="" betweenEnd="" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Frame Labor Rate specified on the Estimate exceeds the rate specified on the shop profile ($$XPATH(Root/ShopInfo/ShopPricing/@HourlyRateUnibodyFrame)).]]>
    </Action>
  </Rule>
  <Rule id="FrameLaborHours" desc="Frame Labor Hours Threshold exceeded" enabled="Y" weight="0">
    <Default enabled="N">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='Frame' and @Category='LC']/@Hours)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="$FACT(FrameLaborThreshold)"
                       betweenBegin="" betweenEnd="" />
      </Conditions>
    </Default>
    <Exception enabled="Y">
      <Context>
        <Item lparm="1" operation="equal" comparemethod="number" rparm="1"/>
      </Context>
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='Frame' and @Category='LC']/@Hours)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="$FACT(FrameLaborThreshold)" />
      </Conditions>
    </Exception>
    <Action>
        <![CDATA[The Total Frame Labor Hours specified by the Estimate exceeds Estimating Guidelines ($FACT(FrameLaborThreshold) hours).]]>
    </Action>
  </Rule>
  <Rule id="SupplementIncreaseThreshold" desc="Supplement Increase Threshold Exceeded" enabled="Y" weight="0">
    <Default enabled="N">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='RepairTotal' and @Category='TT']/@ExtendedAmount)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="($XPATH(Root/Vehicle/@LastRepairTotal) + 500)"
                       betweenBegin="" betweenEnd="" />
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='RepairTotal' and @Category='TT']/@ExtendedAmount)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="($XPATH(Root/Vehicle/@LastRepairTotal) * 1.5)"
                       betweenBegin="" betweenEnd="" logicalOperation="or"/>
      </Conditions>
    </Default>
    <Exception enabled="Y">
      <Context>
        <Item lparm="1" operation="equal" comparemethod="number" rparm="1"/>
      </Context>
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='RepairTotal' and @Category='TT']/@ExtendedAmount)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="($XPATH(Root/Vehicle/@LastRepairTotal) + $FACT(SuppIncrease500))"
                       betweenBegin="" betweenEnd="" />
      </Conditions>
    </Exception>
    <Exception enabled="Y">
      <Context>
        <Item lparm="1" operation="equal" comparemethod="number" rparm="1"/>
      </Context>
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='RepairTotal' and @Category='TT']/@ExtendedAmount)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="($XPATH(Root/Vehicle/@LastRepairTotal) * ($FACT(SuppIncrease150Percent) / 100))"
                       betweenBegin="" betweenEnd="" />
      </Conditions>
    </Exception>
    <Action>
        <![CDATA[The Estimate Repair Total exceeds the Repair Total of the previous estimate for this vehicle/assignment by more than the Threshold amount ($$FACT(SuppIncrease500) or $FACT(SuppIncrease150Percent)%)]]>
    </Action>
  </Rule>
  <Rule id="PartsDiscountRate" desc="Parts Discount Rate in profile not met" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='PartsDiscount' and @Category='PC']/@Percent)" operation="lesserthan" comparemethod="number"
                     rparm="$XPATH(Root/ShopInfo/ShopOEMDiscount[@VehicleMake = $FACT(VehicleMakeNADA)]/@DiscountPct)"  betweenBegin="" betweenEnd=""/>
      </Conditions>
    </Default>
    <Action>
    	<![CDATA[The Frame Labor Rate specified on the Estimate exceeds the rate specified on the shop profile ($XPATH(Root/ShopInfo/ShopOEMDiscount[@VehicleMake = $FACT(VehicleMakeNADA)]/@DiscountPct)%).]]>
	</Action>
  </Rule>
  <Rule id="RecyclePartsMarkup" desc="Recycle Parts Markup Percentage Threshold Exceeded" enabled="Y" weight="0">
    <Default enabled="N">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='PartsMarkup' and @Category='PC']/@Percent)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="$FACT(RecycleMarkupThreshold)"
                       betweenBegin="" betweenEnd=""/>
      </Conditions>
    </Default>
    <Exception enabled="Y">
      <Context>
        <Item lparm="1" operation="equal" comparemethod="number" rparm="1"/>
      </Context>
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='PartsMarkup' and @Category='PC']/@Percent)"
                       operation="greaterthan"
                       comparemethod="number"
                       rparm="$FACT(RecycleMarkupThreshold)"
                       betweenBegin="" betweenEnd="" />
      </Conditions>
    </Exception>
    <Action>
        <![CDATA[The Recycle Parts Markup Rate specified by the Estimate exceeds Estimating Guidelines ($FACT(RecycleMarkupThreshold)%).]]>
    </Action>
  </Rule>
  <Rule id="RefinishChargeOverride" desc="Refinish Charge Overridden Higher" enabled="Y" weight="0">
    <Recurse>
      <Node>APDEstimate/Detail/DetailLine/Labor</Node>
      <Conditions>
        <Condition lparm="@LaborType"
                           operation="equal" comparemethod="text"
                           rparm="Refinish"/>
      </Conditions>
      <Key>
        <Description>DetailNumber|DBLaborHours</Description>
        <Node>../@DetailNumber|DBLaborHours</Node>
      </Key>
    </Recurse>
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$CURRENT(LaborHoursChangedFlag)"
                           operation="equal" comparemethod="text"
                           rparm="Yes" />
        <Condition lparm="$CURRENT(LaborHours)"
                           operation="greaterthan" comparemethod="number"
                           rparm="$CURRENT(DBLaborHours)" logicalOperation="and"/>
        <Condition lparm="($CURRENT(DBLaborHours) + '').length"
                           operation="greaterthan" comparemethod="number"
                           rparm="0" logicalOperation="and"/>
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Refinish Hours specified by the Estimate Detail Line #$KEY(0) were overridden and are greater than the standard hours ($KEY(1)) specified in the database utilized by the estimating package.]]>
    </Action>
  </Rule>
  <Rule id="BodyChargeOverride" desc="Body Labor Charge Overridden Higher" enabled="Y" weight="0">
    <Recurse>
      <Node>APDEstimate/Detail/DetailLine/Labor</Node>
      <Conditions>
        <Condition lparm="@LaborType"
                           operation="equal" comparemethod="text"
                           rparm="Body"/>
      </Conditions>
      <Key>
        <Description>DetailNumber|DBLaborHours</Description>
        <Node>../@DetailNumber|DBLaborHours</Node>
      </Key>
    </Recurse>
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$CURRENT(LaborHoursChangedFlag)"
                           operation="equal" comparemethod="text"
                           rparm="Yes" />
        <Condition lparm="$CURRENT(LaborHours)"
                           operation="greaterthan" comparemethod="number"
                           rparm="$CURRENT(DBLaborHours)" logicalOperation="and"/>
        <Condition lparm="($CURRENT(DBLaborHours) + '').length"
                           operation="greaterthan" comparemethod="number"
                           rparm="0" logicalOperation="and"/>
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Labor Hours specified by the Estimate Detail Line #$KEY(0) were overridden and are greater than the standard hours ($KEY(1)) specified in the database utilized by the estimating package.]]>
    </Action>
  </Rule>
  <Rule id="BodyChargeOverride4Replace" desc="Body Labor Charge Overridden Higher on Replace" enabled="Y" weight="0">
    <Recurse>
      <Node>APDEstimate/Detail/DetailLine/Labor</Node>
      <Conditions>
        <Condition lparm="../@OperationCode"
                           operation="equal" comparemethod="text"
                           rparm="Replace"/>
        <Condition lparm="@LaborType"
                           operation="equal" comparemethod="text"
                           rparm="Body" logicalOperation="and"/>
      </Conditions>
      <Key>
        <Description>DetailNumber|DBLaborHours</Description>
        <Node>../@DetailNumber|DBLaborHours</Node>
      </Key>
    </Recurse>
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$CURRENT(LaborHoursChangedFlag)"
                           operation="equal" comparemethod="text"
                           rparm="Yes" />
        <Condition lparm="$CURRENT(LaborHours)"
                           operation="greaterthan" comparemethod="number"
                           rparm="$CURRENT(DBLaborHours)" logicalOperation="and"/>
        <Condition lparm="($CURRENT(DBLaborHours) + '').length"
                           operation="greaterthan" comparemethod="number"
                           rparm="0" logicalOperation="and"/>
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Labor Hours specified by the Estimate Detail Line #$KEY(0) were overridden and are greater than the standard hours ($KEY(1)) specified in the database utilized by the estimating package and the operation is a replacement operation.]]>
    </Action>
  </Rule>
  <Rule id="LKQAmount" desc="LKQ Amount + Markup Exceeds OEM Amount" enabled="Y" weight="0">
    <Recurse>
      <Node>APDEstimate/Detail/DetailLine/Part</Node>
      <Conditions>
        <Condition lparm="@PartType"
                           operation="equal" comparemethod="text"
                           rparm="LKQ"/>
      </Conditions>
      <Key>
        <Description>DetailNumber|DBPartPrice</Description>
        <Node>../@DetailNumber|DBPartPrice</Node>
      </Key>
    </Recurse>
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$FACT(EstimatingPackage)"
                           operation="equal" comparemethod="number"
                           rparm="CCC"/>
        <Condition lparm="$CURRENT(PartPrice)"
                           operation="greaterthan" comparemethod="number"
                           rparm="$CURRENT(DBPartPrice)" logicalOperation="and"/>
        <Condition lparm="($CURRENT(DBPartPrice) + '').length"
                           operation="greaterthan" comparemethod="number"
                           rparm="0" logicalOperation="and"/>
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The LKQ part amount with markup added specified by Estimate Detail Line #$KEY(0) exceeds the equivalent OEM part amount ($$KEY(1)).]]>
    </Action>
  </Rule>
  <Rule id="TwoWheelAlignmentAmount" desc="Two wheel alignment exceeds profile" enabled="Y" weight="0">
    <Default enabled="N">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='2 Wheel Alignment' and @Category='OT']/@ExtendedAmount)"
                           operation="greaterthan" comparemethod="number"
                           rparm="$FACT(TwoWheelAlignmentThreshold)" />
      </Conditions>
    </Default>
    <Exception enabled="Y">
      <Context>
        <Item lparm="1" operation="equal" comparemethod="number" rparm="1"/>
      </Context>
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='2 Wheel Alignment' and @Category='OT']/@ExtendedAmount)"
                           operation="greaterthan"
                           comparemethod="number"
                           rparm="$FACT(TwoWheelAlignmentThreshold)" />
      </Conditions>
    </Exception>
    <Action>
        <![CDATA[The total two-wheel alignment charge specified by the Estimate differs from the charge specified in the estimating guidelines ($$FACT(TwoWheelAlignmentThreshold)).]]>
    </Action>
  </Rule>
  <Rule id="FourWheelAlignmentAmount" desc="Four wheel alignment exceeds profile" enabled="Y" weight="0">
    <Default enabled="N">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='4 Wheel Alignment' and @Category='OT']/@ExtendedAmount)"
                           operation="greaterthan" comparemethod="number"
                           rparm="$FACT(FourWheelAlignmentThreshold)" />
      </Conditions>
    </Default>
    <Exception enabled="Y">
      <Context>
        <Item lparm="1" operation="equal" comparemethod="number" rparm="1"/>
      </Context>
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type='4 Wheel Alignment' and @Category='OT']/@ExtendedAmount)"
                           operation="greaterthan"
                           comparemethod="number"
                           rparm="$FACT(FourWheelAlignmentThreshold)" />
      </Conditions>
    </Exception>
    <Action>
        <![CDATA[The total four-wheel alignment charge specified by the Estimate differs from the charge specified in the estimating guidelines ($$FACT(FourWheelAlignmentThreshold)).]]>
    </Action>
  </Rule>
  <Rule id="ClearCoatHrs" desc="Clearcoat hours must be entered when an amount exist" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Clear Coat']/@Hours)"
                           operation="equal" comparemethod="number"
                           rparm="0" />
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Clear Coat']/@Amount)"
                           operation="greaterthan" comparemethod="number"
                           rparm="0" logicalOperation="and"/>
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Estimate contains Clear coat amount and no hours have been entered.]]>
    </Action>
  </Rule>
  <Rule id="ClearCoatHrsExceeded" desc="Max Clearcoat hours exceeded" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Clear Coat']/@Hours)"
                           operation="greaterthan" comparemethod="number"
                           rparm="$FACT(ClearCoatThreshold)" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The maximum number of hours for Clear Coat exceeds the maximum specified in the estimating guidelines ($FACT(ClearCoatThreshold)).]]>
    </Action>
  </Rule>
  <Rule id="HazMaterialThresholdExceeded" desc="Hazardous material disposal threshold exceeded" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Waste Management']/@Amount)"
                           operation="greaterthan" comparemethod="number"
                           rparm="$FACT(HazThreshold)" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Hazardous Disposal Amount specified by the Estimate exceeds estimating guidelines ($$FACT(HazThreshold)).]]>
    </Action>
  </Rule>
  <Rule id="CarCoverThresholdExceeded" desc="Car cover threshold exceeded" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Car Cover']/@Amount)"
                           operation="greaterthan" comparemethod="number"
                           rparm="$FACT(CarCoverThreshold)" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Hazardous Disposal Amount specified by the Estimate exceeds estimating guidelines ($$FACT(CarCoverThreshold)).]]>
    </Action>
  </Rule>
  <Rule id="ManualLineAmtThresholdExceeded" desc="Manual Line amount threshold exceeded" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Manual Line']/@Amount)"
                           operation="greaterthan" comparemethod="number"
                           rparm="$FACT(ManualLineAmtThreshold)" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The total amount disclosed in manual lines on the estimate exceeds estimating guidelines ($$FACT(ManualLineAmtThreshold)).]]>
    </Action>
  </Rule>
  <Rule id="ManualLineCountThresholdExceeded" desc="Manual Line count threshold exceeded" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Manual Line']/@Count)"
                           operation="greaterthan" comparemethod="number"
                           rparm="$FACT(ManualLineCountThreshold)" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The total number of charging manual lines on the estimate exceeds estimating guidelines ($FACT(ManualLineCountThreshold)).]]>
    </Action>
  </Rule>
  <Rule id="FreonThresholdExceeded" desc="Freon threshold exceeded" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Freon']/@Amount)"
                           operation="greaterthan" comparemethod="number"
                           rparm="$FACT(FreonAmountThreshold)" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The freon charge amount specified by the estimate exceeds estimating guidelines ($$FACT(FreonAmountThreshold)).]]>
    </Action>
  </Rule>
  <Rule id="TowingThresholdExceeded" desc="Towing threshold exceeded" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Towing']/@Amount)"
                           operation="greaterthan" comparemethod="number"
                           rparm="$FACT(TowingAmountThreshold)" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The towing charge amount specified by the estimate exceeds estimating guidelines ($$FACT(TowingAmountThreshold)).]]>
    </Action>
  </Rule>
  <Rule id="PaintedPinstripeHrs" desc="Painted Pinstripe hours must be entered when an amount exist" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Painted Pinstripe']/@Hours)"
                           operation="equal" comparemethod="number"
                           rparm="0" />
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Painted Pinstripe']/@Amount)"
                           operation="greaterthan" comparemethod="number"
                           rparm="0" logicalOperation="and"/>
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Estimate contains Painted Pinstripe amount and no hours have been entered.]]>
    </Action>
  </Rule>
  <Rule id="PaintedPinstripeThreshold" desc="Painted Pinstripe Threshold Exceeded" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Painted Pinstripe']/@Hours)"
                           operation="greaterthan" comparemethod="number"
                           rparm="((0.3 * $XPATH(APDEstimate/Summary/LineItem[@Type = 'Painted Pinstripe']/@Panels)) + 0.2)" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Painted pinstripe labor hours specified by the estimate exceeds estimating guidelines ($CALC(0.3 * $XPATH(APDEstimate/Summary/LineItem[@Type = 'Painted Pinstripe']/@Panels) + 0.2) hours).]]>
    </Action>
  </Rule>
  <Rule id="TapedPinstripeHrs" desc="Taped Pinstripe hours must be entered when an amount exist" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Taped Pinstripe']/@Hours)"
                           operation="equal" comparemethod="number"
                           rparm="0" />
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Taped Pinstripe']/@Amount)"
                           operation="greaterthan" comparemethod="number"
                           rparm="0" logicalOperation="and"/>
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Estimate contains Taped Pinstripe amount and no hours have been entered.]]>
    </Action>
  </Rule>
  <Rule id="TapedPinstripeThreshold" desc="Taped Pinstripe Threshold Exceeded" enabled="Y" weight="0">
    <Default enabled="Y">
      <Conditions>
        <Condition lparm="$XPATH(APDEstimate/Summary/LineItem[@Type = 'Taped Pinstripe']/@Amount)"
                           operation="greaterthan" comparemethod="number"
                           rparm="((10 * $XPATH(APDEstimate/Summary/LineItem[@Type = 'Taped Pinstripe']/@Panels)) + 5)" />
      </Conditions>
    </Default>
    <Action>
        <![CDATA[The Taped pinstripe amount specified by the estimate exceeds estimating guidelines ($$CALC(10 * $XPATH(APDEstimate/Summary/LineItem[@Type = 'Painted Pinstripe']/@Panels) + 5)).]]>
    </Action>
  </Rule>
  <Facts>
    <!-- Fact value can be a user-defined string or an XPATH using $XPATH macro -->
    <Fact id="InsuranceCoID" value="$XPATH(Root/@InsuranceCompanyID)" datatype="text" format=""/>
    <Fact id="LossState" value="OH" datatype="text" format=""/>
    <Fact id="EstimatingPackage" value="$XPATH(APDEstimate/@Source)" datatype="text" format=""/>
    <Fact id="TotalLossValue" value="$XPATH(Root/Vehicle/@TotalLossWarningPercentage) * $XPATH(Root/Vehicle/@BookValue)" datatype="number" format=""/>
    <Fact id="ClearCoatThreshold" value="2.5" datatype="number" format=""/>
    <Fact id="HazThreshold" value="5" datatype="number" format=""/>
    <Fact id="CarCoverThreshold" value="10" datatype="number" format=""/>
    <Fact id="ManualLineAmtThreshold" value="10" datatype="number" format=""/>
    <Fact id="ManualLineCountThreshold" value="4" datatype="number" format=""/>
    <Fact id="FreonAmountThreshold" value="30" datatype="number" format=""/>
    <Fact id="TowingAmountThreshold" value="65" datatype="number" format=""/>
    <Fact id="VehicleMakeNADA" value="$XPATH(Root/Vehicle/@VehicleMakeNADA)" datatype="text" format=""/>
    <Fact id="FrameLaborThreshold" value="8" datatype="number" format=""/>
    <Fact id="SuppIncrease500" value="500" datatype="number" format=""/>
    <Fact id="SuppIncrease150Percent" value="150" datatype="number" format=""/>
    <Fact id="TwoWheelAlignmentThreshold" value="49.95" datatype="number" format=""/>
    <Fact id="FourWheelAlignmentThreshold" value="79.95" datatype="number" format=""/>
    <Fact id="PaintMaterialThreshold" value="475" datatype="number" format=""/>
    <Fact id="PaintMaterialThresholdTrustgard" value="375" datatype="number" format=""/>
    <Fact id="RecycleMarkupThreshold" value="25" datatype="number" format=""/>
  </Facts>
</Rules>