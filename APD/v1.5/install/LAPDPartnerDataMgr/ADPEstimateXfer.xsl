<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"   xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
								xmlns:msxsl="urn:schemas-microsoft-com:xslt"
								xmlns:adpxsl="http://www.lynxservices.com/adp"
                                xmlns:cu="urn:the-xml-files:xslt">

<xsl:import href="EstXferUtils.xsl"/>

<xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes"/>
<xsl:decimal-format NaN="0"/>

<!--- This styesheet transforms incoming ADP EMS Data to APD's Common Estimate Format -->


<!--- Scripting necessary for the mathematical calculations in the summary -->
<msxsl:script language="JScript" implements-prefix="adpxsl">
<![CDATA[
	var BodyMaterialAmount, BodyMaterialHours, BodyMaterialRate;
	var DiscountMarkupBase; 
	var TotalDiscountAmount, DiscountPct;
	var TotalMarkupAmount, MarkupPct;
	var PaintMaterialAmount, PaintMaterialHours, PaintMaterialRate;
	var TotalParts, TotalTaxParts, TotalNonTaxParts, TotalOtherTaxParts, TotalOtherNonTaxParts;
	var TotalMaterials;
	var TotalPartsMaterials;

	var BodyLaborAmount, BodyLaborHours, BodyLaborRate;
	var FrameLaborAmount, FrameLaborHours, FrameLaborRate;
	var MechLaborAmount, MechLaborHours, MechLaborRate;
	var PaintLaborAmount, PaintLaborHours, PaintLaborRate;
	var TotalOtherLabor;
	var TotalLaborAmount;

	var StorageAmount;
	var SubletAmount;
	var TowingAmount;
	var OtherAddlChargesAmount;
	var TotalAddlCharges;

	var AppearanceAllowanceAmount;
	var BettermentAmount;
	var DeductibleAmount;
	var OtherAdjustmentAmount;
	var TotalAdjustmentAmount;

	var SalesTaxAmountPM, SalesTaxBasePM, SalesTaxPctPM;
	var SalesTaxAmountLabor, SalesTaxBaseLabor, SalesTaxPctLabor;
     var OtherTaxAmount;
	var TotalTaxAmount;

	var NetTotalAmount;
	var RepairTotalAmount;
	var SubtotalAmount;
	var ZZVarianceAmount;

	var DetailPartTaxTotal, DetailPartNonTaxTotal, DetailPartOtherTaxTotal, DetailPartOtherNonTaxTotal;
	var DetailBodyLaborRepairHours, DetailBodyLaborReplaceHours;
	var DetailFrameLaborRepairHours, DetailFrameLaborReplaceHours; 
	var DetailMechLaborRepairHours, DetailMechLaborReplaceHours; 
	var DetailPaintLaborRepairHours, DetailPaintLaborReplaceHours;
	var DetailSubletTotal, DetailBettermentTotal;
	var DetailUnknownTotal;
	var DetailOEMParts, DetailLKQParts, DetailAFMKParts, DetailRemanParts;
	var Detail2WAlign, Detail4WAlign;

	var CurrentLineNumber;

     var OptionList, OptionListLength;
		
	function setvar(varname, value) {
		eval(varname + ' = ' + value);
		return '';
	}

	function getvar(varname, index) {
	     if (index == undefined) {
		    return eval(eval(varname));
		}
		else {
			return eval(varname + '[' + index + ']');
	     }
	}
	
     function parseoptions(optionstring) {
          // This functions parses the contents of the Vehicle Option node passed to it and saves it in an array.  This array is made global so that it's contents can be retrieved through getvar()
		
	     var objOptionString = new String();
	     objOptionString = optionstring;
	
		// ADP adds an extra linefeed onto the end of the option string...we must remove this
		objOptionString = objOptionString.substr(0, objOptionString.length - 1)
	
          // Now split at each remaining linefeed
	     OptionList = objOptionString.split("\n");

          // Save the length of the array
          OptionListLength = OptionList.length;

          // Go throught the OptionList string array and remove the codes, leaving just the option descriptions
          for (var index=0; index < OptionListLength; index++) {
             	OptionList[index] = OptionList[index].substr(OptionList[index].indexOf(';') + 1, OptionList[index].length - OptionList[index].indexOf(';'));
          }

          return '';
     }

     function parsemessages(messagestring) {
          // This functions parses the contents of the messagestring passed to it and translates them into a message string.

	     // Set up translation matrix

	     var arMessageStore = new Array(   "01", "CALL DEALER FOR EXACT PART # / PRICE",
								 				 "02", "PART NO. DISCONTINUED, CALL DEALER FOR EXACT PART NO.",
												 "03", "GSMP PART - CONTACT DEALER FOR EXACT PRICE",
												 "04", "PRICE NOT YET AVAILABLE, CALL LOCAL DEALER",
												 "07", "STRUCTURAL PART AS IDENTIFIED BY I-CAR",
												 "10", "INCLUDES ADP TIME TO CLEAR ENTIRE PANEL",
												 "13", "INCLUDES 0.6 HOURS FIRST PANEL TWO-STAGE ALLOWANCE",
												 "14", "INCLUDES 1.0 HOURS FIRST PANEL THREE-STAGE ALLOWANCE",
												 "15", "INCLUDES 0.4 HOURS FIRST PANEL TWO-TONE ALLOWANCE",
												 "46", "PRINTABLE ALTERNATE PARTS COMPARE",
												 "49", "UNPRINTED ALTERNATE PARTS COMPARE",
												 "50", "SEE PREVIOUS ESTIMATE FOR SUPPLIER INFORMATION",
												 "60", "ALTERNATE PARTS PART PRICE INCLUDES LABOR",
												 "61", "ALTERNATE PARTS GLASS PART PRICE INCLUDES LABOR",
												 "70", "RECYCLED ADP SPPL",
												 "71", "RECYCLED 800#" );

	     var objMessageString = new String();
	     objMessageString = messagestring;
	
		var arMessageList;
		var translatedstring = "";
		var bFirstMessage = 1;
		var bMessageFound;
		
          // First split at each comma
	     arMessageList = objMessageString.split(",");

          // Go throught the arMessageList string array and translate the codes
          for (var indexL=0; indexL < arMessageList.length; indexL++) {
			
			// Search for the corresponding message
			bMessageFound = 0;
			
			for (var indexS=0; indexS < arMessageStore.length; indexS += 2) {
				if (arMessageStore[indexS] == arMessageList[indexL]) {
					
					// Concatenate the message to the translated string
					if (bFirstMessage == 0) {
						 translatedstring += "; ";
					}
					
					translatedstring += arMessageStore[indexS + 1];
					bMessageFound = 1;
					bFirstMessage = 0;
				}
			}
			
			if (bMessageFound == 0) {
				// The message was not found
				if (bFirstMessage == 0) {
					translatedstring += "; ";
				}
				
				translatedstring += "*** Unknown MC Message Code ***";
				bFirstMessage = 0;
			}
         }

          return translatedstring;
     }

    function evalDbPrice(str)
    {
      if (isNaN(str) || str == '')
        return parseInt("0",10);
      else
        return parseFloat(str);
    }

]]>
</msxsl:script>

<xsl:variable name="source" select="'ADP'"/>
<xsl:variable name="estimatepackage" select="'Shoplink'"/>
<xsl:variable name="version" select="'1.0.2'"/>
<xsl:variable name="versiondate" select="'03/06/2006'"/>

<xsl:template match="/ADPTransaction/TransactionData/ADP_ESTIMATE/ADP_TRANSACTION">
	<APDEstimate>
		<xsl:attribute name="Source"><xsl:value-of select="$source"/></xsl:attribute>
		<xsl:attribute name="EstimatePackage"><xsl:value-of select="$estimatepackage"/></xsl:attribute>
		<xsl:attribute name="SoftwareVersion"><xsl:value-of select="EMS/ENV/SW_VERSION"/></xsl:attribute>
		<xsl:attribute name="StylesheetVersion"><xsl:value-of select="$version"/></xsl:attribute>
		<xsl:attribute name="StylesheetDate"><xsl:value-of select="$versiondate"/></xsl:attribute>

		<xsl:apply-templates select="EMS" mode="Header"/>
		<xsl:apply-templates select="EMS" mode="Detail"/>
		<xsl:apply-templates select="EMS" mode="Profile"/>
		<xsl:apply-templates select="EMS" mode="Summary"/>
		<xsl:apply-templates select="EMS" mode="Audit"/>
	</APDEstimate>	
</xsl:template>


<!--- Transform Header Information -->
<xsl:template match="EMS" mode="Header">
	<Header>
		<xsl:attribute name="EstimateType">
			<xsl:choose>
				<xsl:when test="ENV/TRANS_TYPE = 'E'">Estimate</xsl:when>
				<xsl:when test="ENV/TRANS_TYPE = 'S'">Supplement</xsl:when>
				<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
			</xsl:choose> 
		</xsl:attribute>
		<xsl:attribute name="SupplementNumber">
			<xsl:choose>
				<xsl:when test="ENV/TRANS_TYPE = 'E'">0</xsl:when>
				<xsl:otherwise><xsl:value-of select="number(substring(ENV/SUPP_NO, 2, 2))"/></xsl:otherwise>
			</xsl:choose> 
		</xsl:attribute>
		<xsl:attribute name="WrittenDate"><xsl:value-of select="ENV/TRANSMT_DT"/></xsl:attribute>
		<xsl:attribute name="AssignmentDate"><xsl:value-of select="AD1/ASGN_DATE"/></xsl:attribute>
		<xsl:attribute name="ClaimantName"><xsl:value-of select="AD2/CLMT_LN"/></xsl:attribute>
		<xsl:attribute name="ClaimNumber"><xsl:value-of select="AD1/CLM_NO"/></xsl:attribute>
		<xsl:attribute name="InsuranceCompanyName"><xsl:value-of select="AD1/INS_CO_NM"/></xsl:attribute>
		<xsl:attribute name="InsuredName"><xsl:value-of select="AD1/INSD_LN"/></xsl:attribute>
		<xsl:attribute name="LossDate"><xsl:value-of select="AD1/LOSS_DATE"/></xsl:attribute>
		<xsl:attribute name="LossType">
			<xsl:choose>
				<xsl:when test="AD1/LOSS_CAT='C'">Collision</xsl:when>
				<xsl:when test="AD1/LOSS_CAT='L'">Liability</xsl:when>
				<xsl:when test="AD1/LOSS_CAT='M'">Comprehensive</xsl:when>
				<xsl:when test="AD1/LOSS_CAT='O'">Other</xsl:when>
				<xsl:when test="AD1/LOSS_CAT='P'">Property</xsl:when>
				<xsl:when test="AD1/LOSS_CAT='U'">Unknown</xsl:when>
				<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
			</xsl:choose> 
		</xsl:attribute>
		<xsl:attribute name="PolicyNumber"><xsl:value-of select="AD1/POLICY_NO"/></xsl:attribute>
		<xsl:attribute name="Remarks"><xsl:value-of select="VEH/DMG_MEMO"/></xsl:attribute>
		<xsl:attribute name="RepairDays"/>
		<xsl:attribute name="SettlementType">
			<xsl:choose>
				<xsl:when test="AD1/TLOS_IND='T'">TotalLoss</xsl:when>
				<xsl:otherwise>Normal</xsl:otherwise>
	          </xsl:choose>
		</xsl:attribute>
    <xsl:attribute name="LynxID"><xsl:value-of select="/ADPTransaction/@LynxID"/></xsl:attribute>
    <xsl:attribute name="VehicleNumber"><xsl:value-of select="/ADPTransaction/@VehicleID"/></xsl:attribute>
    <xsl:attribute name="AssignmentID"><xsl:value-of select="/ADPTransaction/@AssignmentID"/></xsl:attribute>

		<Appraiser>
			<xsl:attribute name="AppraiserName"><xsl:value-of select="AD2/EST_CO_NM"/></xsl:attribute>
			<xsl:attribute name="AppraiserLicenseNumber"><xsl:value-of select="AD2/EST_LIC_NO"/></xsl:attribute>
		</Appraiser>	

		<Estimator>
			<xsl:choose>
				<xsl:when test="AD2/EST_CO_NM != ''">
					<xsl:attribute name="CompanyAddress"><xsl:value-of select="concat(AD2/EST_ADDR1, AD2/EST_ADDR2)"/></xsl:attribute>
					<xsl:attribute name="CompanyAreaCode"><xsl:value-of select="substring(AD2/EST_PH1, 2, 3)"/></xsl:attribute>
					<xsl:attribute name="CompanyCity"><xsl:value-of select="AD2/EST_CITY"/></xsl:attribute>
					<xsl:attribute name="CompanyName"><xsl:value-of select="AD2/EST_CO_NM"/></xsl:attribute>
					<xsl:attribute name="CompanyExchangeNumber"><xsl:value-of select="substring(AD2/EST_PH1, 6, 3)"/></xsl:attribute>
					<xsl:attribute name="CompanyState"><xsl:value-of select="AD2/EST_ST"/></xsl:attribute>
					<xsl:attribute name="CompanyUnitNumber"><xsl:value-of select="substring(AD2/EST_PH1, 10, 4)"/></xsl:attribute>
					<xsl:attribute name="CompanyZip"><xsl:value-of select="AD2/EST_ZIP"/></xsl:attribute>
				</xsl:when>
				<xsl:when test="AD2/RF_CO_NM != ''">
					<xsl:attribute name="CompanyAddress"><xsl:value-of select="concat(AD2/RF_ADDR1, AD2/RF_ADDR2)"/></xsl:attribute>
					<xsl:attribute name="CompanyAreaCode"><xsl:value-of select="substring(AD2/RF_PH1, 2, 3)"/></xsl:attribute>
					<xsl:attribute name="CompanyCity"><xsl:value-of select="AD2/RF_CITY"/></xsl:attribute>
					<xsl:attribute name="CompanyName"><xsl:value-of select="AD2/RF_CO_NM"/></xsl:attribute>
					<xsl:attribute name="CompanyExchangeNumber"><xsl:value-of select="substring(AD2/RF_PH1, 6, 3)"/></xsl:attribute>
					<xsl:attribute name="CompanyState"><xsl:value-of select="AD2/RF_ST"/></xsl:attribute>
					<xsl:attribute name="CompanyUnitNumber"><xsl:value-of select="substring(AD2/RF_PH1, 10, 4)"/></xsl:attribute>
					<xsl:attribute name="CompanyZip"><xsl:value-of select="AD2/RF_ZIP"/></xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="CompanyAddress"/>
					<xsl:attribute name="CompanyAreaCode"/>
					<xsl:attribute name="CompanyCity"/>
					<xsl:attribute name="CompanyName">! Not Found In Estimate Data !</xsl:attribute>
					<xsl:attribute name="CompanyExchangeNumber"/>
					<xsl:attribute name="CompanyState"/>
					<xsl:attribute name="CompanyUnitNumber"/>
					<xsl:attribute name="CompanyZip"/>
				</xsl:otherwise>
			</xsl:choose>
		</Estimator>

		<Inspection>
			<xsl:attribute name="InspectionAddress"><xsl:value-of select="concat(AD2/INSP_ADDR1, AD2/INSP_ADDR2)"/></xsl:attribute>
			<xsl:attribute name="InspectionAreaCode"><xsl:value-of select="substring(AD2/INSP_PH1, 2, 3)"/></xsl:attribute>			
			<xsl:attribute name="InspectionCity"><xsl:value-of select="AD2/INSP_CITY"/></xsl:attribute>
			<xsl:attribute name="InspectionExchangeNumber"><xsl:value-of select="substring(AD2/INSP_PH1, 6, 3)"/></xsl:attribute>		
			<xsl:attribute name="InspectionLocation"/>
			<xsl:attribute name="InspectionState"><xsl:value-of select="AD2/INSP_ST"/></xsl:attribute>
			<xsl:attribute name="InspectionType"><xsl:value-of select="AD2/INSP_DESC"/></xsl:attribute>
			<xsl:attribute name="InspectionUnitNumber"><xsl:value-of select="substring(AD2/INSP_PH1, 10, 4)"/></xsl:attribute>			
			<xsl:attribute name="InspectionZip"><xsl:value-of select="AD2/INSP_ZIP"/></xsl:attribute>
		</Inspection>

		<Owner>
			<xsl:attribute name="OwnerAddress"><xsl:value-of select="concat(AD1/OWNR_ADDR1, AD1/OWNR_ADDR2)"/></xsl:attribute>
			<xsl:attribute name="OwnerCity"><xsl:value-of select="AD1/OWNR_CITY"/></xsl:attribute>
			<xsl:attribute name="OwnerDayAreaCode"><xsl:value-of select="substring(AD1/OWNR_PH1, 2, 3)"/></xsl:attribute>
			<xsl:attribute name="OwnerDayExchangeNumber"><xsl:value-of select="substring(AD1/OWNR_PH1, 6, 3)"/></xsl:attribute>
			<xsl:attribute name="OwnerDayExtensionNumber"><xsl:value-of select="AD1/OWNR_PH1X"/></xsl:attribute>
			<xsl:attribute name="OwnerDayUnitNumber"><xsl:value-of select="substring(AD1/OWNR_PH1, 10, 4)"/></xsl:attribute>
			<xsl:attribute name="OwnerEveningAreaCode"><xsl:value-of select="substring(AD1/OWNR_PH2, 2, 3)"/></xsl:attribute>
			<xsl:attribute name="OwnerEveningExchangeNumber"><xsl:value-of select="substring(AD1/OWNR_PH2, 6, 3)"/></xsl:attribute>
			<xsl:attribute name="OwnerEveningExtensionNumber"><xsl:value-of select="AD1/OWNR_PH2X"/></xsl:attribute>
			<xsl:attribute name="OwnerEveningUnitNumber"><xsl:value-of select="substring(AD1/OWNR_PH2, 10, 4)"/></xsl:attribute>
			<xsl:attribute name="OwnerName"><xsl:value-of select="AD1/OWNR_LN"/></xsl:attribute>
			<xsl:attribute name="OwnerState"><xsl:value-of select="AD1/OWNR_ST"/></xsl:attribute>
			<xsl:attribute name="OwnerZip"><xsl:value-of select="AD1/OWNR_ZIP"/></xsl:attribute>
		</Owner>

		<Shop>
			<xsl:attribute name="ShopAddress"><xsl:value-of select="concat(AD2/RF_ADDR1, AD2/RF_ADDR2)"/></xsl:attribute>
			<xsl:attribute name="ShopAreaCode"><xsl:value-of select="substring(AD2/RF_PH1, 2, 3)"/></xsl:attribute>
			<xsl:attribute name="ShopCity"><xsl:value-of select="AD2/RF_CITY"/></xsl:attribute>
			<xsl:attribute name="ShopExchangeNumber"><xsl:value-of select="substring(AD2/RF_PH1, 6, 3)"/></xsl:attribute>
			<xsl:attribute name="ShopName"><xsl:value-of select="AD2/RF_CO_NM"/></xsl:attribute>
			<xsl:attribute name="ShopRegistrationNumber"><xsl:value-of select="AD2/RF_LIC_NO"/></xsl:attribute>
			<xsl:attribute name="ShopState"><xsl:value-of select="AD2/RF_ST"/></xsl:attribute>
			<xsl:attribute name="ShopUnitNumber"><xsl:value-of select="substring(AD2/RF_PH1, 10, 4)"/></xsl:attribute>
			<xsl:attribute name="ShopZip"><xsl:value-of select="AD2/RF_ZIP"/></xsl:attribute>
		</Shop>

		<Vehicle>
			<xsl:attribute name="VehicleBodyStyle"><xsl:value-of select="VEH/V_BSTYLE"/></xsl:attribute>
			<xsl:attribute name="VehicleColor"><xsl:value-of select="VEH/V_COLOR"/></xsl:attribute>
			<xsl:attribute name="VehicleEngineType"><xsl:value-of select="VEH/V_ENGINE"/></xsl:attribute>
			<xsl:attribute name="VehicleMake"><xsl:value-of select="VEH/V_MAKEDESC"/></xsl:attribute>
			<xsl:attribute name="VehicleMileage"><xsl:value-of select="translate(VEH/V_MILEAGE, ',', '')"/></xsl:attribute>
			<xsl:attribute name="VehicleModel"><xsl:value-of select="VEH/V_MODEL"/></xsl:attribute>
			<xsl:attribute name="VehicleYear"><xsl:if test="number(VEH/V_MODEL_YR) &gt; 0"><xsl:value-of select="number(VEH/V_MODEL_YR)"/></xsl:if></xsl:attribute>
			<xsl:attribute name="Vin"><xsl:value-of select="VEH/V_VIN"/></xsl:attribute>

			<xsl:for-each select="VEH/*[starts-with(name(), 'IMPACT') and . != '']"> 
				<ImpactPoint>
					<xsl:attribute name="ImpactArea">
						<xsl:choose>
							<xsl:when test="number(.)='1'"><xsl:value-of select="'Right Front'"/></xsl:when>
							<xsl:when test="number(.)='2'"><xsl:value-of select="'Right Front Pillar'"/></xsl:when>
							<xsl:when test="number(.)='3'"><xsl:value-of select="'Right T-Bone'"/></xsl:when>
							<xsl:when test="number(.)='4'"><xsl:value-of select="'Right Quarter Post'"/></xsl:when>
							<xsl:when test="number(.)='5'"><xsl:value-of select="'Right Rear'"/></xsl:when>
							<xsl:when test="number(.)='6'"><xsl:value-of select="'Rear'"/></xsl:when>
							<xsl:when test="number(.)='7'"><xsl:value-of select="'Left Rear'"/></xsl:when>
							<xsl:when test="number(.)='8'"><xsl:value-of select="'Left Quarter Post'"/></xsl:when>
							<xsl:when test="number(.)='9'"><xsl:value-of select="'Left T-Bone'"/></xsl:when>
							<xsl:when test="number(.)='10'"><xsl:value-of select="'Left Front Pillar'"/></xsl:when>
							<xsl:when test="number(.)='11'"><xsl:value-of select="'Left Front'"/></xsl:when>
							<xsl:when test="number(.)='12'"><xsl:value-of select="'Front'"/></xsl:when>
							<xsl:when test="number(.)='13'"><xsl:value-of select="'Rollover'"/></xsl:when>
							<xsl:when test="number(.)='14'"><xsl:value-of select="'Unknown'"/></xsl:when>
							<xsl:when test="number(.)='15'"><xsl:value-of select="'Total Loss'"/></xsl:when>
							<xsl:when test="number(.)='16'"><xsl:value-of select="'Non-Collision'"/></xsl:when>
							<xsl:when test="number(.)='19'"><xsl:value-of select="'All Over'"/></xsl:when>
							<xsl:when test="number(.)='20'"><xsl:value-of select="'Stripped'"/></xsl:when>
							<xsl:when test="number(.)='21'"><xsl:value-of select="'Undercarriage'"/></xsl:when>
							<xsl:when test="number(.)='22'"><xsl:value-of select="'Total Burn'"/></xsl:when>
							<xsl:when test="number(.)='23'"><xsl:value-of select="'Interior Burn'"/></xsl:when>
							<xsl:when test="number(.)='24'"><xsl:value-of select="'Exterior Burn'"/></xsl:when>
							<xsl:when test="number(.)='25'"><xsl:value-of select="'Hood'"/></xsl:when>
							<xsl:when test="number(.)='26'"><xsl:value-of select="'Deck Lid'"/></xsl:when>
							<xsl:when test="number(.)='27'"><xsl:value-of select="'Roof'"/></xsl:when>
							<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="PrimaryImpactFlag">
						<xsl:choose>
							<xsl:when test="name()='IMPACT_1'">Yes</xsl:when>
							<xsl:otherwise>No</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
				</ImpactPoint>
			</xsl:for-each>

			<!--- We need to parse out the Option String given to us.  These values are saved in a javascript array object accessible via getvar() -->

               <xsl:value-of select="adpxsl:parseoptions(string(VEH/V_OPTIONS))"/>

			<!--- Now call a recursive template to process the array and generate the nodes we require -->
			<xsl:variable name="nextindex" select="0"/>
			<xsl:call-template name="generate-option-node">
				<xsl:with-param name="listindex" select="$nextindex"/>
			</xsl:call-template>			
		</Vehicle>
	</Header>	
</xsl:template>


<!--- Recursive template to retrieve the values from the optionlist array -->
<xsl:template name="generate-option-node">
	<xsl:param name="listindex"/>
	
	<xsl:if test="$listindex &lt; adpxsl:getvar('OptionListLength')">
		<VehicleOption>
			<xsl:attribute name="Option"><xsl:value-of select="adpxsl:getvar('OptionList', $listindex)"/></xsl:attribute>
		</VehicleOption>
		
		<!--- Recursively call the template to get the next value -->
		<xsl:variable name="nextindex" select="$listindex + 1"/>
		<xsl:call-template name="generate-option-node">
			<xsl:with-param name="listindex" select="$nextindex"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>


<!--- Transform Detail Line Information -->
<xsl:template match="EMS" mode="Detail">
	<!--- First set up running totals for various detail components.  These will be used for the audit later -->

	<xsl:value-of select="adpxsl:setvar('DetailPartTaxTotal', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailPartNonTaxTotal', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailPartOtherTaxTotal', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailPartOtherNonTaxTotal', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailBodyLaborRepairHours', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailBodyLaborReplaceHours', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailFrameLaborRepairHours', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailFrameLaborReplaceHours', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailMechLaborRepairHours', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailMechLaborReplaceHours', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailPaintLaborRepairHours', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailPaintLaborReplaceHours', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailSubletTotal', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailBettermentTotal', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailUnknownTotal', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailOEMParts', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailLKQParts', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailAFMKParts', 0)"/>
	<xsl:value-of select="adpxsl:setvar('DetailRemanParts', 0)"/>
	<xsl:value-of select="adpxsl:setvar('Detail2WAlign', 0)"/>
	<xsl:value-of select="adpxsl:setvar('Detail4WAlign', 0)"/>

	<!-- Grab out of profile the paint material rate and whether betterment is charged on Paint Materials and Labor.  These are used in the betterment amount calculation.  -->
	<xsl:variable name="BettermentLaborMaterials" select="PFH/ADJ_BTR_IN"/>
	<xsl:variable name="PaintMaterialRate" select="PFM[MATL_TYPE='MAPA']/CAL_LBRRTE"/>

	<!--- ADP does not provide discount or markup base in their subtotals...we must calculate from the detail lines -->
	<xsl:value-of select="adpxsl:setvar('DiscountMarkupBase', 0)"/>

	<Detail>
		<xsl:for-each select="LIN">
			<xsl:sort select="UNQ_SEQ" data-type="number"/>
			<DetailLine>
				<xsl:attribute name="DetailNumber"><xsl:value-of select="number(UNQ_SEQ)"/></xsl:attribute>
				
				<xsl:attribute name="Comment">
					<xsl:if test="string-length(string(ALT_PARTM)) &gt; 0">
						<xsl:value-of select="adpxsl:parsemessages(string(ALT_PARTM))"/>
					</xsl:if>
				</xsl:attribute>
				<xsl:attribute name="Description">
					<xsl:choose>
						<xsl:when test="OEM_PARTNO='APPEAR ALLOWANCE' or OEM_PARTNO='RELATED PRIOR' or OEM_PARTNO='UNRELATED PRIOR'"><xsl:value-of select="concat(LINE_DESC, ' (', OEM_PARTNO, ' of  $', ACT_PRICE, ')' )"/></xsl:when>				
						<xsl:otherwise><xsl:value-of select="LINE_DESC"/></xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>

				<xsl:attribute name="OperationCode">
					<xsl:choose>
						<xsl:when test="OEM_PARTNO='APPEAR ALLOWANCE' or OEM_PARTNO='RELATED PRIOR' or OEM_PARTNO='UNRELATED PRIOR'">Comment</xsl:when>
						<xsl:when test="MOD_LB_HRS=''"/>
						<xsl:when test="LBR_OP='OP0'"/>
						<xsl:when test="LBR_OP='OP1'">Repair</xsl:when>
						<xsl:when test="LBR_OP='OP2'">Remove/Install</xsl:when>
						<xsl:when test="LBR_OP='OP3'">Other</xsl:when>
						<xsl:when test="LBR_OP='OP4'">Alignment</xsl:when>
						<xsl:when test="LBR_OP='OP5'">Repair</xsl:when>
						<xsl:when test="LBR_OP='OP6'">Refinish</xsl:when>
						<xsl:when test="LBR_OP='OP7'">Other</xsl:when>
						<xsl:when test="LBR_OP='OP8'">Other</xsl:when>
						<xsl:when test="LBR_OP='OP9'">Repair</xsl:when>
						<xsl:when test="LBR_OP='OP10'">Repair</xsl:when>
						<xsl:when test="LBR_OP='OP11'">Replace</xsl:when>
						<xsl:when test="LBR_OP='OP12'">Replace</xsl:when>
						<xsl:when test="LBR_OP='OP13'">Other</xsl:when>
						<xsl:when test="LBR_OP='OP14'">Other</xsl:when>
						<xsl:when test="LBR_OP='OP15'">Blend</xsl:when>
						<xsl:when test="LBR_OP='OP16'">Sublet</xsl:when>
						<xsl:when test="LBR_OP='OP17'">Other</xsl:when>
						<xsl:when test="LBR_OP='OP18'">Other</xsl:when>
						<xsl:when test="LBR_OP='OP19'">Other</xsl:when>
						<xsl:when test="LBR_OP='OP20'">Remove/Install</xsl:when>
						<xsl:when test="LBR_OP='OP21'">Repair</xsl:when>
						<xsl:when test="LBR_OP='OP22'">Repair</xsl:when>
						<xsl:when test="LBR_OP='OP23'">Other</xsl:when>
						<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>				

				<xsl:attribute name="LineAdded">
					<xsl:if test="LINE_IND!='E'">
						<xsl:value-of select="LINE_IND"/>
					</xsl:if>
				</xsl:attribute>
				<xsl:attribute name="ManualLineFlag"/>
				<xsl:if test="number(BETT_PCTG) &gt; 0">
					<!--- ADP does not provide betterment amount.  We'll need to calculate it the best we can using the following formula:
							Part Price (+ If BettermentOnPaintAndLabor(Labor Amount + Refinish Amount + Paint Material Amount )) x Betterment % -->
					<xsl:attribute name="BettermentType"><xsl:value-of select="BETT_TYPE"/></xsl:attribute>
					<xsl:attribute name="BettermentPercent"><xsl:value-of select="number(BETT_PCTG) * 100"/></xsl:attribute>
					<xsl:attribute name="BettermentAmount">
						<xsl:choose>
                            		<xsl:when test="$BettermentLaborMaterials = 'T'">
								<xsl:choose>
                                    		<xsl:when test="MOD_LBR_TY='LAR'"><xsl:value-of select="BETT_PCTG * (LBR_AMT + ($PaintMaterialRate * MOD_LB_HRS))"/></xsl:when>
									<xsl:otherwise><xsl:value-of select="BETT_PCTG * (ACT_PRICE + LBR_AMT)"/></xsl:otherwise>
                                		</xsl:choose>
							</xsl:when>
							<xsl:otherwise><xsl:value-of select="BETT_PCTG * ACT_PRICE"/></xsl:otherwise>
                        		</xsl:choose> 
					</xsl:attribute>
					<xsl:attribute name="BettermentTaxable">
						<xsl:choose>
							<xsl:when test="BETT_TAX='T'">Yes</xsl:when>
							<xsl:otherwise>No</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>			
				</xsl:if>

				<!--- Save the line number for use later -->
				<xsl:value-of select="adpxsl:setvar('CurrentLineNumber', number(UNQ_SEQ))"/>
							
				<!--- Part Information -->
				<xsl:if test="OEM_PARTNO!='APPEAR ALLOWANCE' and OEM_PARTNO!='RELATED PRIOR' and OEM_PARTNO!='UNRELATED PRIOR' and (number(ACT_PRICE) &gt; 0 or number(MISC_AMT) &gt; 0 or (number(ACT_PRICE) = 0 and PRICE_INC='T'))">
					<Part>
						<xsl:attribute name="APDPartCategory">
							<xsl:choose>
								<xsl:when test="PART_TYPE='PAS'">Sublet</xsl:when>
								<xsl:when test="TAX_PART='T'">TaxableParts</xsl:when>
								<xsl:when test="TAX_PART='F'">NonTaxableParts</xsl:when>
								<xsl:otherwise/>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="DBPartPrice">
							<xsl:choose>
								<xsl:when test="PRICE_J='T' and DB_PRICE!='' and number(DB_PRICE)!=0"><xsl:value-of select="number(DB_PRICE)"/></xsl:when>
								<xsl:otherwise/>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="PartIncludedFlag">
							<xsl:choose>
								<xsl:when test="PRICE_INC='T'">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="PartPrice">
							<xsl:choose>
								<xsl:when test="number(MISC_AMT) &gt; 0"><xsl:value-of select="number(MISC_AMT)"/></xsl:when>
								<xsl:when test="PRICE_INC='T'"><xsl:value-of select="adpxsl:evalDbPrice(string(DB_PRICE))"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="number(ACT_PRICE)"/></xsl:otherwise>
							</xsl:choose>								
						</xsl:attribute>
						<xsl:attribute name="PartTaxedFlag">
							<xsl:choose>
								<xsl:when test="MISC_TAX='T'">Yes</xsl:when>
								<xsl:when test="TAX_PART='T'">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="PartType">
							<xsl:choose>
								<xsl:when test="PART_TYPE='PAN'">New</xsl:when>
								<xsl:when test="PART_TYPE='PAL'">LKQ</xsl:when>
								<xsl:when test="PART_TYPE='PAA'">Aftermarket</xsl:when>
								<xsl:when test="PART_TYPE='PAC'">Reconditioned</xsl:when>
								<xsl:when test="PART_TYPE='PAG'">Glass</xsl:when>
								<xsl:when test="PART_TYPE='PAO'">Other</xsl:when>
								<xsl:when test="PART_TYPE='PAS'">Sublet</xsl:when>
								<xsl:when test="PART_TYPE=''">Unspecified</xsl:when>	
								<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="PriceChangedFlag">
							<xsl:choose>
								<xsl:when test="PRICE_J='T' and DB_PRICE!='' and number(DB_PRICE)!=0">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="Quantity"><xsl:value-of select="PART_QTY"/></xsl:attribute>
					</Part>
					
					<!--- Update running totals with any part price.  Make sure we put it in the right bucket -->
					<xsl:if test="PRICE_INC='F'">
						<xsl:choose>
							<xsl:when test="PART_TYPE='PAS'">
								<xsl:choose>
                                    		<xsl:when test="number(PRT_DSMK_P) &gt; 0"><xsl:value-of select="adpxsl:setvar('DetailSubletTotal', round((adpxsl:getvar('DetailSubletTotal') + number(MISC_AMT) + (number(MISC_AMT) * number(PRT_DSMK_P) div 100)) * 100) div 100)"/></xsl:when>
									<xsl:otherwise><xsl:value-of select="adpxsl:setvar('DetailSubletTotal', round((adpxsl:getvar('DetailSubletTotal') + number(MISC_AMT)) * 100) div 100)"/></xsl:otherwise>
		                               </xsl:choose>
							</xsl:when>
							<xsl:when test="TAX_PART='T'"><xsl:value-of select="adpxsl:setvar('DetailPartTaxTotal', round((adpxsl:getvar('DetailPartTaxTotal') + number(ACT_PRICE)) * 100) div 100)"/></xsl:when>
							<xsl:when test="TAX_PART='F'"><xsl:value-of select="adpxsl:setvar('DetailPartNonTaxTotal', round((adpxsl:getvar('DetailPartNonTaxTotal') + number(ACT_PRICE)) * 100) div 100)"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="adpxsl:setvar('DetailUnknownTotal', round((adpxsl:getvar('DetailUnknownTotal') + number(ACT_PRICE)) * 100) div 100)"/></xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					
					<!--- Update discount or markup base -->
					<xsl:if test="PRICE_INC='F' and PART_TYPE!='PAS' and number(PRT_DSMK_P) &gt; 0">
						<xsl:value-of select="adpxsl:setvar('DiscountMarkupBase', round((adpxsl:getvar('DiscountMarkupBase') + number(ACT_PRICE)) * 100) div 100)"/>
					</xsl:if>
					
					<!--- Update Detail Parts Totals -->
					<xsl:if test="PRICE_INC='F'">
						<xsl:choose>
                            		<xsl:when test=" PART_TYPE='PAN'"><xsl:value-of select="adpxsl:setvar('DetailOEMParts', round((adpxsl:getvar('DetailOEMParts') + number(ACT_PRICE)) * 100) div 100)"/></xsl:when>
                            		<xsl:when test=" PART_TYPE='PAL'"><xsl:value-of select="adpxsl:setvar('DetailLKQParts', round((adpxsl:getvar('DetailLKQParts') + number(ACT_PRICE)) * 100) div 100)"/></xsl:when>
                            		<xsl:when test=" PART_TYPE='PAA'"><xsl:value-of select="adpxsl:setvar('DetailAFMKParts', round((adpxsl:getvar('DetailAFMKParts') + number(ACT_PRICE)) * 100) div 100)"/></xsl:when>
                            		<xsl:when test=" PART_TYPE='PAM'"><xsl:value-of select="adpxsl:setvar('DetailRemanParts', round((adpxsl:getvar('DetailRemanParts') + number(ACT_PRICE)) * 100) div 100)"/></xsl:when>
                        		</xsl:choose>						
					</xsl:if>

					<xsl:if test="PRICE_INC='F' and number(BETT_PCTG) &gt; 0">
						<xsl:value-of select="adpxsl:setvar('DetailBettermentTotal', round((adpxsl:getvar('DetailBettermentTotal') + number(BETT_AMT)) * 100) div 100)"/>
					</xsl:if>

					<xsl:variable name="LineDescription" select="translate(LIN/LINE_DESC, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
					
					<xsl:if test="$LineDescription = 'two wheel align' or $LineDescription = 'two wheel alignment' or $LineDescription = 'front wheel alignment' or $LineDescription = '2 wheel align' or $LineDescription = '2 wheel alignment' or $LineDescription = '2-wheel align' or $LineDescription = '2-wheel alignment'"><xsl:value-of select="adpxsl:setvar('Detail2WAlign', round((adpxsl:getvar('Detail2WAlign') + number(ACT_PRICE)) * 100) div 100)"/></xsl:if>

					<xsl:if test="$LineDescription = 'four wheel align' or $LineDescription = 'four wheel alignment' or $LineDescription = '4 wheel align' or $LineDescription = '4 wheel alignment' or $LineDescription = '4-wheel align' or $LineDescription = '4-wheel alignment'"><xsl:value-of select="adpxsl:setvar('Detail4WAlign', round((adpxsl:getvar('Detail4WAlign') + number(ACT_PRICE)) * 100) div 100)"/></xsl:if> 									
				</xsl:if>

				<!--- Labor Information -->
				<xsl:if test="OEM_PARTNO!='APPEAR ALLOWANCE' and OEM_PARTNO!='RELATED PRIOR' and OEM_PARTNO!='UNRELATED PRIOR' and (MOD_LB_HRS &gt; 0 or (MOD_LB_HRS = 0 and LBR_HRS_J='T'))">
					<Labor>
						<xsl:attribute name="DBLaborHours">
							<xsl:choose>
								<xsl:when test="LBR_HRS_J='T' and DB_HRS!=0"><xsl:value-of select="DB_HRS"/></xsl:when>
								<xsl:otherwise/>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborAmount"><xsl:value-of select="LBR_AMT"/></xsl:attribute>
						<xsl:attribute name="LaborHoursChangedFlag">
							<xsl:choose>
								<xsl:when test="LBR_HRS_J='T' and DB_HRS!=0">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborHours">
							<xsl:choose>
                                		<xsl:when test="LBR_INC='T'"><xsl:value-of select="DB_HRS"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="MOD_LB_HRS"/></xsl:otherwise>
                          		</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborIncludedFlag">
							<xsl:choose>
								<xsl:when test="LBR_INC='T'">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborTaxedFlag">
							<xsl:choose>
								<xsl:when test="LBR_TAX='T'">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborType">
							<xsl:choose>
								<xsl:when test="MOD_LBR_TY='LAB'">Body</xsl:when>
								<xsl:when test="MOD_LBR_TY='LAD'">Mechanical</xsl:when>
								<xsl:when test="MOD_LBR_TY='LAE'">Mechanical</xsl:when>
								<xsl:when test="MOD_LBR_TY='LAR'">Refinish</xsl:when>
								<xsl:when test="MOD_LBR_TY='LAS'">Frame</xsl:when>
								<xsl:when test="MOD_LBR_TY='LAF'">Frame</xsl:when>
								<xsl:when test="MOD_LBR_TY='LAM'">Mechanical</xsl:when>
								<xsl:when test="MOD_LBR_TY='LAG'">Body</xsl:when>
								<xsl:when test="MOD_LBR_TY='LA1'">User Defined</xsl:when>
								<xsl:when test="MOD_LBR_TY='LA2'">User Defined</xsl:when>
								<xsl:when test="MOD_LBR_TY='LA3'">User Defined</xsl:when>
								<xsl:when test="MOD_LBR_TY='LA4'">User Defined</xsl:when>
								<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</Labor>
					
					<!--- Update running totals with any labor hours.  Make sure we put it in the right bucket -->
					<xsl:if test="LBR_INC='F' and MOD_LB_HRS &gt; 0">
						<xsl:choose>
							<xsl:when test="MOD_LBR_TY='LAB'">
								<xsl:choose>
	                                		<xsl:when test="LBR_OP='OP3' or LBR_OP='OP8' or LBR_OP='OP9' or LBR_OP='OP10'"><xsl:value-of select="adpxsl:setvar('DetailBodyLaborRepairHours', round((adpxsl:getvar('DetailBodyLaborRepairHours') + number(MOD_LB_HRS)) * 10) div 10)"/></xsl:when>
									<xsl:otherwise><xsl:value-of select="adpxsl:setvar('DetailBodyLaborReplaceHours', round((adpxsl:getvar('DetailBodyLaborReplaceHours') + number(MOD_LB_HRS)) *  10) div 10)"/></xsl:otherwise>
		                             </xsl:choose>
							</xsl:when>
							<xsl:when test="MOD_LBR_TY='LAF' or MOD_LBR_TY='LAS'">
								<xsl:choose>
	                                		<xsl:when test="LBR_OP='OP3' or LBR_OP='OP8' or LBR_OP='OP9' or LBR_OP='OP10'"><xsl:value-of select="adpxsl:setvar('DetailFrameLaborRepairHours', round((adpxsl:getvar('DetailFrameLaborRepairHours') + number(MOD_LB_HRS)) * 10) div 10)"/></xsl:when>
									<xsl:otherwise><xsl:value-of select="adpxsl:setvar('DetailFrameLaborReplaceHours', round((adpxsl:getvar('DetailFrameLaborReplaceHours') + number(MOD_LB_HRS)) * 10) div 10)"/></xsl:otherwise>
		                             </xsl:choose>
							</xsl:when>
							<xsl:when test="MOD_LBR_TY='LAD' or MOD_LBR_TY='LAE' or MOD_LBR_TY='LAM'">
								<xsl:choose>
	                                		<xsl:when test="LBR_OP='OP3' or LBR_OP='OP8' or LBR_OP='OP9' or LBR_OP='OP10'"><xsl:value-of select="adpxsl:setvar('DetailMechLaborRepairHours', round((adpxsl:getvar('DetailMechLaborRepairHours') + number(MOD_LB_HRS)) * 10) div 10)"/></xsl:when>
									<xsl:otherwise><xsl:value-of select="adpxsl:setvar('DetailMechLaborReplaceHours', round((adpxsl:getvar('DetailMechLaborReplaceHours') + number(MOD_LB_HRS)) * 10) div 10)"/></xsl:otherwise>
		                             </xsl:choose>
							</xsl:when>
							<xsl:when test="MOD_LBR_TY='LAR'">
							 	<xsl:choose>
									<xsl:when test="LINE_DESC = ../LIN[number(UNQ_SEQ)= adpxsl:getvar('CurrentLineNumber') - 1]/LINE_DESC">
										<xsl:choose>
			                                		<xsl:when test="../LIN[number(UNQ_SEQ)= adpxsl:getvar('CurrentLineNumber') - 1]/LBR_OP='OP3' or ../LIN[number(UNQ_SEQ)= adpxsl:getvar('CurrentLineNumber') - 1]/LBR_OP='OP8' or ../LIN[number(UNQ_SEQ)= adpxsl:getvar('CurrentLineNumber') - 1]/LBR_OP='OP9' or ../LIN[number(UNQ_SEQ)= adpxsl:getvar('CurrentLineNumber') - 1]/LBR_OP='OP10'"><xsl:value-of select="adpxsl:setvar('DetailPaintLaborRepairHours', round((adpxsl:getvar('DetailPaintLaborRepairHours') + number(MOD_LB_HRS)) * 10) div 10)"/></xsl:when>
											<xsl:otherwise><xsl:value-of select="adpxsl:setvar('DetailPaintLaborReplaceHours', round((adpxsl:getvar('DetailPaintLaborReplaceHours') + number(MOD_LB_HRS)) *  10) div 10)"/></xsl:otherwise>
			     	                        </xsl:choose>
									</xsl:when>
									<xsl:otherwise><xsl:value-of select="adpxsl:setvar('DetailPaintLaborRepairHours', round((adpxsl:getvar('DetailPaintLaborRepairHours') + number(MOD_LB_HRS)) *  10) div 10)"/></xsl:otherwise>
                            			</xsl:choose>
							</xsl:when>
						</xsl:choose>
					</xsl:if>
				</xsl:if>					
			</DetailLine>
		</xsl:for-each>
	</Detail>
</xsl:template>


<!--- Transform Profile Information -->
<xsl:template match="EMS" mode="Profile">
	<Profiles>
		<ProfileItem Category="Parts" Type="OEM">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFP[PRT_TYPE='PAN']/PRT_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="PFP[PRT_TYPE='PAN']/PRT_TAX_IN='T'">
				<xsl:attribute name="TaxPercent"><xsl:value-of select="PFP[PRT_TYPE='PAN']/PRT_TAX_RT"/></xsl:attribute>
			</xsl:if>			
			<xsl:attribute name="MarkupPercent"><xsl:value-of select="PFP[PRT_TYPE='PAN']/PRT_MKUPP"/></xsl:attribute>
			<xsl:attribute name="DiscountPercent"><xsl:value-of select="PFP[PRT_TYPE='PAN']/PRT_DISCP"/></xsl:attribute>			
		</ProfileItem>
		<ProfileItem Category="Parts" Type="LKQ">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFP[PRT_TYPE='PAL']/PRT_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="PFP[PRT_TYPE='PAL']/PRT_TAX_IN='T'">
				<xsl:attribute name="TaxPercent"><xsl:value-of select="PFP[PRT_TYPE='PAL']/PRT_TAX_RT"/></xsl:attribute>
			</xsl:if>			
			<xsl:attribute name="MarkupPercent"><xsl:value-of select="PFP[PRT_TYPE='PAL']/PRT_MKUPP"/></xsl:attribute>
			<xsl:attribute name="DiscountPercent"><xsl:value-of select="PFP[PRT_TYPE='PAL']/PRT_DISCP"/></xsl:attribute>			
		</ProfileItem>
		<ProfileItem Category="Parts" Type="Aftermarket">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFP[PRT_TYPE='PAA']/PRT_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="PFP[PRT_TYPE='PAA']/PRT_TAX_IN='T'">
				<xsl:attribute name="TaxPercent"><xsl:value-of select="PFP[PRT_TYPE='PAA']/PRT_TAX_RT"/></xsl:attribute>
			</xsl:if>			
			<xsl:attribute name="MarkupPercent"><xsl:value-of select="PFP[PRT_TYPE='PAA']/PRT_MKUPP"/></xsl:attribute>
			<xsl:attribute name="DiscountPercent"><xsl:value-of select="PFP[PRT_TYPE='PAA']/PRT_DISCP"/></xsl:attribute>			
		</ProfileItem>
		<ProfileItem Category="Parts" Type="Recycled">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFP[PRT_TYPE='PAM']/PRT_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="PFP[PRT_TYPE='PAM']/PRT_TAX_IN='T'">
				<xsl:attribute name="TaxPercent"><xsl:value-of select="PFP[PRT_TYPE='PAM']/PRT_TAX_RT"/></xsl:attribute>
			</xsl:if>			
			<xsl:attribute name="MarkupPercent"><xsl:value-of select="PFP[PRT_TYPE='PAM']/PRT_MKUPP"/></xsl:attribute>
			<xsl:attribute name="DiscountPercent"><xsl:value-of select="PFP[PRT_TYPE='PAM']/PRT_DISCP"/></xsl:attribute>			
		</ProfileItem>
		<ProfileItem Category="Parts" Type="Front Glass">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFP[PRT_TYPE='PAGF']/PRT_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="PFP[PRT_TYPE='PAGF']/PRT_TAX_IN='T'">
				<xsl:attribute name="TaxPercent"><xsl:value-of select="PFP[PRT_TYPE='PAGF']/PRT_TAX_RT"/></xsl:attribute>
			</xsl:if>			
			<xsl:attribute name="MarkupPercent"><xsl:value-of select="PFP[PRT_TYPE='PAGF']/PRT_MKUPP"/></xsl:attribute>
			<xsl:attribute name="DiscountPercent"><xsl:value-of select="PFP[PRT_TYPE='PAGF']/PRT_DISCP"/></xsl:attribute>			
		</ProfileItem>
		<ProfileItem Category="Labor" Type="Body">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFL[LBR_TYPE='LAB']/LBR_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="PFL[LBR_TYPE='LAB']/LBR_TAX_IN='T'">
				<xsl:attribute name="TaxPercent"><xsl:value-of select="PFL[LBR_TYPE='LAB']/LBR_TAXP"/></xsl:attribute>
			</xsl:if>			
			<xsl:attribute name="Rate"><xsl:value-of select="PFL[LBR_TYPE='LAB']/LBR_RATE"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Labor" Type="Frame">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFL[LBR_TYPE='LAF']/LBR_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="PFL[LBR_TYPE='LAF']/LBR_TAX_IN='T'">
				<xsl:attribute name="TaxPercent"><xsl:value-of select="PFL[LBR_TYPE='LAF']/LBR_TAXP"/></xsl:attribute>
			</xsl:if>			
			<xsl:attribute name="Rate"><xsl:value-of select="PFL[LBR_TYPE='LAF']/LBR_RATE"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Labor" Type="Mechanical">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFL[LBR_TYPE='LAM']/LBR_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="PFL[LBR_TYPE='LAM']/LBR_TAX_IN='T'">
				<xsl:attribute name="TaxPercent"><xsl:value-of select="PFL[LBR_TYPE='LAM']/LBR_TAXP"/></xsl:attribute>
			</xsl:if>			
			<xsl:attribute name="Rate"><xsl:value-of select="PFL[LBR_TYPE='LAM']/LBR_RATE"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Labor" Type="Refinish">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFL[LBR_TYPE='LAR']/LBR_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="PFL[LBR_TYPE='LAR']/LBR_TAX_IN='T'">
				<xsl:attribute name="TaxPercent"><xsl:value-of select="PFL[LBR_TYPE='LAR']/LBR_TAXP"/></xsl:attribute>
			</xsl:if>			
			<xsl:attribute name="Rate"><xsl:value-of select="PFL[LBR_TYPE='LAR']/LBR_RATE"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Materials" Type="Paint1Stage">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFM[MATL_TYPE='MAPA']/TAX_IND='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="PFM[MATL_TYPE='MAPA']/TAX_IND='T'">
				<xsl:attribute name="TaxPercent"><xsl:value-of select="PFM[MATL_TYPE='MAPA']/MAT_TAXP"/></xsl:attribute>
			</xsl:if>			
			<xsl:attribute name="Rate"><xsl:value-of select="PFM[MATL_TYPE='MAPA']/CAL_LBRRTE"/></xsl:attribute>
			<xsl:attribute name="MaxDollars"><xsl:value-of select="PFM[MATL_TYPE='MAPA']/CAL_MAXDLR"/></xsl:attribute>
			<xsl:attribute name="MaxHours"><xsl:value-of select="PFM[MATL_TYPE='MAPA']/CAL_LBRMAX"/></xsl:attribute>
			<xsl:attribute name="CalcMethod"><xsl:value-of select="PFM[MATL_TYPE='MAPA']/CAL_CODE"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Materials" Type="Paint2Stage" Taxable="No" Rate="" MaxDollars="" MaxHours="" CalcMethod=""/>
		<ProfileItem Category="Materials" Type="Paint3Stage" Taxable="No" Rate="" MaxDollars="" MaxHours="" CalcMethod=""/>
	</Profiles>
</xsl:template>


<!--- Transform Summary Information -->
<xsl:template match="EMS" mode="Summary">
	<!--- First we need to get together all the values we need to build the summary -->

	
	<!--- Material Costs -->
	
	<!--- Body Materials Amount  -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='MASH']/T_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('BodyMaterialAmount', number(STL[TTL_TYPECD='MASH']/T_AMT))"/></xsl:when>
       	<xsl:when test="STL[TTL_TYPECD='MASH']/NT_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('BodyMaterialAmount', number(STL[TTL_TYPECD='MASH']/NT_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('BodyMaterialAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Body Material Rate -->
	<xsl:choose>
       	<xsl:when test="PFM[MATL_TYPE='MASH']/CAL_LBRRTE != ''"><xsl:value-of select="adpxsl:setvar('BodyMaterialRate', number(PFM[MATL_TYPE='MASH']/CAL_LBRRTE))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('BodyMaterialRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Body Material Hours -->
	<xsl:choose>
        <xsl:when test="adpxsl:getvar('BodyMaterialRate') &gt; 0"><xsl:value-of select="adpxsl:setvar('BodyMaterialHours', sum(STL[TTL_TYPECD='LAB' or TTL_TYPECD='LAM' or TTL_TYPECD='LAF']/TTL_HRS))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('BodyMaterialHours', 0)"/></xsl:otherwise>
    </xsl:choose>
    

	<!--- Paint Materials Amount -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='MAPA']/T_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('PaintMaterialAmount', number(STL[TTL_TYPECD='MAPA']/T_AMT))"/></xsl:when>
       	<xsl:when test="STL[TTL_TYPECD='MAPA']/NT_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('PaintMaterialAmount', number(STL[TTL_TYPECD='MAPA']/NT_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('PaintMaterialAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Paint Material Rate -->
	<xsl:choose>
       	<xsl:when test="PFM[MATL_TYPE='MAPA']/CAL_LBRRTE != ''"><xsl:value-of select="adpxsl:setvar('PaintMaterialRate', number(PFM[MATL_TYPE='MAPA']/CAL_LBRRTE))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('PaintMaterialRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Paint Material Hours -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='LAR']/TTL_HRS &gt; 0 and adpxsl:getvar('PaintMaterialRate') &gt; 0"><xsl:value-of select="adpxsl:setvar('PaintMaterialHours', number(STL[TTL_TYPECD='LAR']/TTL_HRS))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('PaintMaterialHours', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Materials -->	
	<xsl:value-of select="adpxsl:setvar('TotalMaterials', round((adpxsl:getvar('BodyMaterialAmount') + adpxsl:getvar('PaintMaterialAmount')) * 100) div 100)"/> 


	<!--- Part Costs -->

	<!--- Discount Amount (Parts) -->
	<xsl:choose>
       	<xsl:when test="number(STL[TTL_TYPECD='PAT']/NT_DISC) &lt; 0"><xsl:value-of select="adpxsl:setvar('TotalDiscountAmount', number(STL[TTL_TYPECD='PAT']/NT_DISC))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('TotalDiscountAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Discount Percent (Parts) -->
	<xsl:choose>
       	<xsl:when test="adpxsl:getvar('TotalDiscountAmount')='0'"><xsl:value-of select="adpxsl:setvar('DiscountPct', 0)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('DiscountPct', round((-1 * adpxsl:getvar('TotalDiscountAmount') div adpxsl:getvar('DiscountMarkupBase') * 100) * 1000) div 1000)"/></xsl:otherwise>
     </xsl:choose>
			
	<!--- Markup Amount (Parts) -->
	<xsl:choose>
       	<xsl:when test="number(STL[TTL_TYPECD='PAT']/NT_MKUP) &gt; 0"><xsl:value-of select="adpxsl:setvar('TotalMarkupAmount', number(STL[TTL_TYPECD='PAT']/NT_MKUP))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('TotalMarkupAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Markup Percent (Parts) -->
	<xsl:choose>
       	<xsl:when test="adpxsl:getvar('TotalMarkupAmount')='0' or adpxsl:getvar('DiscountMarkupBase')='0'"><xsl:value-of select="adpxsl:setvar('MarkupPct', 0)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('MarkupPct', round((adpxsl:getvar('TotalMarkupAmount') div adpxsl:getvar('DiscountMarkupBase') * 100) * 1000) div 1000)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Taxable Parts -->	
	<xsl:choose>
       	<xsl:when test="round(STL[TTL_TYPECD='PAT']/T_AMT - adpxsl:getvar('TotalMaterials')) &gt; 0"><xsl:value-of select="adpxsl:setvar('TotalTaxParts', round((STL[TTL_TYPECD='PAT']/T_AMT - adpxsl:getvar('TotalMaterials') - adpxsl:getvar('TotalDiscountAmount') - adpxsl:getvar('TotalMarkupAmount'))  * 100) div 100)"/></xsl:when> 
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('TotalTaxParts', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Non-Taxable Parts -->	
	<xsl:choose>
       	<xsl:when test="round(STL[TTL_TYPECD='PAT']/NT_AMT - adpxsl:getvar('TotalMaterials')) &gt; 0"><xsl:value-of select="adpxsl:setvar('TotalNonTaxParts', round((STL[TTL_TYPECD='PAT']/NT_AMT - adpxsl:getvar('TotalMaterials') - adpxsl:getvar('TotalDiscountAmount') - adpxsl:getvar('TotalMarkupAmount'))  * 100) div 100)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('TotalNonTaxParts', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Other Taxable Parts (Taxable PAO) (not used for ADP, just set to 0) -->	
	<xsl:value-of select="adpxsl:setvar('TotalOtherTaxParts', 0)"/>

	<!--- Other NonTaxable Parts (NonTaxable PAO) (not used for ADP, just set to 0) -->	
	<xsl:value-of select="adpxsl:setvar('TotalOtherNonTaxParts', 0)"/>

	<!--- Total Parts -->	
	<xsl:value-of select="adpxsl:setvar('TotalParts', round((adpxsl:getvar('TotalTaxParts') + adpxsl:getvar('TotalNonTaxParts') + adpxsl:getvar('TotalDiscountAmount') + adpxsl:getvar('TotalMarkupAmount') + adpxsl:getvar('TotalOtherTaxParts') + adpxsl:getvar('TotalOtherNonTaxParts')) * 100) div 100)"/> 


	<!--- Total Parts and Materials -->	
	<xsl:value-of select="adpxsl:setvar('TotalPartsMaterials', round((adpxsl:getvar('TotalParts') + adpxsl:getvar('TotalMaterials')) * 100) div 100)"/> 


	<!-- Labor Costs -->

	<!--- Body Labor Amount -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='LAB']/T_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('BodyLaborAmount', number(STL[TTL_TYPECD='LAB']/T_AMT))"/></xsl:when>
       	<xsl:when test="STL[TTL_TYPECD='LAB']/NT_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('BodyLaborAmount', number(STL[TTL_TYPECD='LAB']/NT_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('BodyLaborAmount', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Body Labor Hours -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='LAB']/T_HRS &gt; 0"><xsl:value-of select="adpxsl:setvar('BodyLaborHours', number(STL[TTL_TYPECD='LAB']/T_HRS))"/></xsl:when>
       	<xsl:when test="STL[TTL_TYPECD='LAB']/NT_HRS &gt; 0"><xsl:value-of select="adpxsl:setvar('BodyLaborHours', number(STL[TTL_TYPECD='LAB']/NT_HRS))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('BodyLaborHours', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Body Labor Rate -->
	<xsl:choose>
       	<xsl:when test="PFL[LBR_TYPE='LAB']/LBR_RATE != ''"><xsl:value-of select="adpxsl:setvar('BodyLaborRate', number(PFL[LBR_TYPE='LAB']/LBR_RATE))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('BodyLaborRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Frame Labor Amount -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/T_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('FrameLaborAmount', sum(STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/T_AMT))"/></xsl:when>
       	<xsl:when test="STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/NT_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('FrameLaborAmount', sum(STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/NT_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('FrameLaborAmount', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Frame Labor Hours -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/T_HRS &gt; 0"><xsl:value-of select="adpxsl:setvar('FrameLaborHours', sum(STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/T_HRS))"/></xsl:when>
       	<xsl:when test="STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/NT_HRS &gt; 0"><xsl:value-of select="adpxsl:setvar('FrameLaborHours', sum(STL[TTL_TYPECD='LAF' or TTL_TYPECD='LAS']/NT_HRS))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('FrameLaborHours', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Frame Labor Rate -->
	<xsl:choose>
       	<xsl:when test="PFL[LBR_TYPE='LAF']/LBR_RATE != ''"><xsl:value-of select="adpxsl:setvar('FrameLaborRate', number(PFL[LBR_TYPE='LAF']/LBR_RATE))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('FrameLaborRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!---Mechanical Labor Amount -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/T_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('MechLaborAmount', sum(STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/T_AMT))"/></xsl:when>
       	<xsl:when test="STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/NT_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('MechLaborAmount', sum(STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/NT_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('MechLaborAmount', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Mechanical Labor Hours -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/T_HRS &gt; 0"><xsl:value-of select="adpxsl:setvar('MechLaborHours', sum(STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/T_HRS))"/></xsl:when>
       	<xsl:when test="STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/NT_HRS &gt; 0"><xsl:value-of select="adpxsl:setvar('MechLaborHours', sum(STL[TTL_TYPECD='LAD' or TTL_TYPECD='LAE' or TTL_TYPECD='LAM']/NT_HRS))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('MechLaborHours', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Mechanical Labor Rate -->
	<xsl:choose>
       	<xsl:when test="PFL[LBR_TYPE='LAM']/LBR_RATE != ''"><xsl:value-of select="adpxsl:setvar('MechLaborRate', number(PFL[LBR_TYPE='LAM']/LBR_RATE))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('MechLaborRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Paint Labor Amount -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='LAR']/T_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('PaintLaborAmount', number(STL[TTL_TYPECD='LAR']/T_AMT))"/></xsl:when>
       	<xsl:when test="STL[TTL_TYPECD='LAR']/NT_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('PaintLaborAmount', number(STL[TTL_TYPECD='LAR']/NT_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('PaintLaborAmount', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Paint Labor Hours -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='LAR']/T_HRS &gt; 0"><xsl:value-of select="adpxsl:setvar('PaintLaborHours', number(STL[TTL_TYPECD='LAR']/T_HRS))"/></xsl:when>
       	<xsl:when test="STL[TTL_TYPECD='LAR']/NT_HRS &gt; 0"><xsl:value-of select="adpxsl:setvar('PaintLaborHours', number(STL[TTL_TYPECD='LAR']/NT_HRS))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('PaintLaborHours', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Paint Labor Rate -->
	<xsl:choose>
       	<xsl:when test="PFL[LBR_TYPE='LAR']/LBR_RATE != ''"><xsl:value-of select="adpxsl:setvar('PaintLaborRate', number(PFL[LBR_TYPE='LAR']/LBR_RATE))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('PaintLaborRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Labor Amount -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='LAT']/TTL_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('TotalLaborAmount', number(STL[TTL_TYPECD='LAT']/TTL_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('TotalLaborAmount', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Other Labor (This is the difference between the Total Labor retrieved above and that calculated from the other values) -->	
	<xsl:value-of select="adpxsl:setvar('TotalOtherLabor', round((adpxsl:getvar('TotalLaborAmount') - (adpxsl:getvar('BodyLaborAmount') + adpxsl:getvar('FrameLaborAmount') + adpxsl:getvar('MechLaborAmount') + adpxsl:getvar('PaintLaborAmount'))) * 100) div 100)"/>
	
	
	<!--- Additional Charges -->
	
	<!--- Storage Amount -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPE='OTST']/T_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('StorageAmount', number(STL[TTL_TYPE='OTST']/T_AMT))"/></xsl:when>
       	<xsl:when test="STL[TTL_TYPE='OTST']/NT_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('StorageAmount', number(STL[TTL_TYPE='OTST']/NT_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('StorageAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Sublet Amount -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPE='OTSL']/T_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('SubletAmount', number(STL[TTL_TYPE='OTSL']/T_AMT))"/></xsl:when>
       	<xsl:when test="STL[TTL_TYPE='OTSL']/NT_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('SubletAmount', number(STL[TTL_TYPE='OTSL']/NT_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('SubletAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Towing Amount -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPE='OTTW']/T_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('TowingAmount', number(STL[TTL_TYPE='OTTW']/T_AMT))"/></xsl:when>
       	<xsl:when test="STL[TTL_TYPE='OTTW']/NT_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('TowingAmount', number(STL[TTL_TYPE='OTTW']/NT_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('TowingAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Other Additional Charges -->
	<xsl:value-of select="adpxsl:setvar('OtherAddlChargesAmount', sum(STL[starts-with(TTL_TYPE, 'OT') and TTL_TYPE!='OTOC' and TTL_TYPE!='OTSL' and TTL_TYPE!='OTST' and TTL_TYPE!='OTTW' and TTL_TYPE!='OTAA']/TTL_AMT))"/>

	<!--- Total Additional Charges (Storage, Sublet, Towing, and Other added together) -->	
	<xsl:value-of select="adpxsl:setvar('TotalAddlCharges', adpxsl:getvar('StorageAmount') + adpxsl:getvar('SubletAmount') + adpxsl:getvar('TowingAmount') + adpxsl:getvar('OtherAddlChargesAmount'))"/>


	<!--- Subtotal -->	
	<xsl:value-of select="adpxsl:setvar('SubtotalAmount', round((adpxsl:getvar('TotalPartsMaterials') + adpxsl:getvar('TotalLaborAmount') + adpxsl:getvar('TotalAddlCharges')) * 100) div 100)"/> 


	<!--- Parts & Materials Tax  -->

	<!--- Sales Tax Base -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='PAT']/T_AMT &gt; 0 and PFM[1]/TAX_IND='F'"><xsl:value-of select="adpxsl:setvar('SalesTaxBasePM', adpxsl:getvar('TotalParts'))"/></xsl:when>
       	<xsl:when test="STL[TTL_TYPECD='PAT']/T_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('SalesTaxBasePM', adpxsl:getvar('TotalPartsMaterials'))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('SalesTaxBasePM', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Sales Tax Percent -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='PAT']/T_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('SalesTaxPctPM', number(PFP[1]/PRT_TAX_RT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('SalesTaxPctPM', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Sales Tax Amount -->
	<xsl:choose>
       	<xsl:when test="adpxsl:getvar('SalesTaxBasePM')='0'"><xsl:value-of select="adpxsl:setvar('SalesTaxAmountPM', '0')"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('SalesTaxAmountPM', round(adpxsl:getvar('SalesTaxBasePM') * (adpxsl:getvar('SalesTaxPctPM') div 100) * 100) div 100)"/></xsl:otherwise>
     </xsl:choose>


	<!--- Labor Tax  -->
	
	<!--- Sales Tax Base -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='LAT']/T_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('SalesTaxBaseLabor', number(STL[TTL_TYPECD='LAT']/T_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('SalesTaxBaseLabor', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Sales Tax Percent -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPECD='LAT']/T_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('SalesTaxPctLabor', number(PFL[1]/LBR_TAXP))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('SalesTaxPctLabor', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Sales Tax Amount -->
	<xsl:choose>
       	<xsl:when test="adpxsl:getvar('SalesTaxBaseLabor')='0'"><xsl:value-of select="adpxsl:setvar('SalesTaxAmountLabor', '0')"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('SalesTaxAmountLabor', round(adpxsl:getvar('SalesTaxBaseLabor') * (adpxsl:getvar('SalesTaxPctLabor') div 100) * 100) div 100)"/></xsl:otherwise>
     </xsl:choose>


	<!--- Total Tax Amount -->
	<xsl:choose>
       	<xsl:when test="TTL/G_TAX"><xsl:value-of select="adpxsl:setvar('TotalTaxAmount', number(TTL/G_TAX))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('TotalTaxAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Other Taxes -->
	<xsl:value-of select="adpxsl:setvar('OtherTaxAmount', round((adpxsl:getvar('TotalTaxAmount') - adpxsl:getvar('SalesTaxAmountPM') - adpxsl:getvar('SalesTaxAmountLabor')) * 100) div 100)"/> 
	
	<!--- Total Repair Cost -->
	<xsl:value-of select="adpxsl:setvar('RepairTotalAmount', round((adpxsl:getvar('SubtotalAmount') + adpxsl:getvar('TotalTaxAmount')) * 100) div 100)"/> 

			
	<!--- Adjustments -->
	
	<!--- Appearance Allowance -->
	<xsl:choose>
       	<xsl:when test="TTL/G_AA_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('AppearanceAllowanceAmount', -1 * number(TTL/G_AA_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('AppearanceAllowanceAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Betterment -->
	<xsl:choose>
       	<xsl:when test="TTL/G_BETT_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('BettermentAmount', number(TTL/G_BETT_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('BettermentAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Deductible -->
	<xsl:choose>
       	<xsl:when test="TTL/G_DED_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('DeductibleAmount', number(TTL/G_DED_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('DeductibleAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Other Adjustment Amount (For ADP, this is a special adjustment on some ADP estimates taken after taxes -->
	<xsl:choose>
       	<xsl:when test="STL[TTL_TYPE='OTOC']/TTL_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('OtherAdjustmentAmount', number(STL[TTL_TYPE='OTOC']/TTL_AMT ))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('OtherAdjustmentAmount', 0)"/></xsl:otherwise>
     </xsl:choose>
	
	<!--- Total Adjustment (Deductible, Appearance Allowance, Betterment, and Other added together -->	
	<xsl:value-of select="adpxsl:setvar('TotalAdjustmentAmount', round((adpxsl:getvar('AppearanceAllowanceAmount') + adpxsl:getvar('BettermentAmount') + adpxsl:getvar('DeductibleAmount') + adpxsl:getvar('OtherAdjustmentAmount')) * 100) div 100)"/>

		
	<!--- Net Total -->
	<xsl:choose>
       	<xsl:when test="TTL/N_TTL_AMT &gt; 0"><xsl:value-of select="adpxsl:setvar('NetTotalAmount', number(TTL/N_TTL_AMT))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="adpxsl:setvar('NetTotalAmount', 0)"/></xsl:otherwise>
     </xsl:choose>


	<!--- Variance Amount  (This is a "bit bucket" to capture variances between what I calculate and what the "real" estimate total is) -->
	<xsl:value-of select="adpxsl:setvar('ZZVarianceAmount', round((adpxsl:getvar('RepairTotalAmount') - adpxsl:getvar('AppearanceAllowanceAmount') - adpxsl:getvar('BettermentAmount') - adpxsl:getvar('DeductibleAmount') - adpxsl:getvar('OtherAdjustmentAmount') - adpxsl:getvar('NetTotalAmount')) * 100) div 100)"/> 


	<!--- Continue building output -->
	<Summary>
		<LineItem Type="PartsTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="Yes">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TotalTaxParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('TotalTaxParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsNonTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="No">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TotalNonTaxParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('TotalNonTaxParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsMarkup" TransferLine="Yes" Category="PC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="adpxsl:getvar('TotalTaxParts') &gt;= adpxsl:getvar('TotalNonTaxParts')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TotalMarkupAmount')"/></xsl:attribute>
			<xsl:attribute name="Percent"><xsl:value-of select="adpxsl:getvar('MarkupPct')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount">
				<xsl:choose>
                    	<xsl:when test="adpxsl:getvar('TotalMarkupAmount') = 0">0</xsl:when>
					<xsl:otherwise><xsl:value-of select="adpxsl:getvar('DiscountMarkupBase')"/></xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
		</LineItem>
		<LineItem Type="PartsDiscount" TransferLine="Yes" Category="PC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="adpxsl:getvar('TotalTaxParts') &gt;= adpxsl:getvar('TotalNonTaxParts')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TotalDiscountAmount')"/></xsl:attribute>
			<xsl:attribute name="Percent"><xsl:value-of select="adpxsl:getvar('DiscountPct')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount">
				<xsl:choose>
                    	<xsl:when test="adpxsl:getvar('TotalDiscountAmount') = 0">0</xsl:when>
					<xsl:otherwise><xsl:value-of select="adpxsl:getvar('DiscountMarkupBase')"/></xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
		</LineItem>
		<LineItem Type="PartsOtherTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="Yes">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TotalOtherTaxParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('TotalOtherTaxParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsOtherNonTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="No">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TotalOtherNonTaxParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('TotalOtherNonTaxParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TotalParts')"/></xsl:attribute>
		</LineItem>
		
		<LineItem Type="Body" TransferLine="Yes" Category="MC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFM[MATL_TYPE='MASH']/TAX_IND='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('BodyMaterialAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="adpxsl:getvar('BodyMaterialHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('BodyMaterialRate')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Paint" TransferLine="Yes" Category="MC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFM[MATL_TYPE='MAPA']/TAX_IND='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('PaintMaterialAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="adpxsl:getvar('PaintMaterialHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('PaintMaterialRate')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="SuppliesTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TotalMaterials')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsAndMaterialTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TotalPartsMaterials')"/></xsl:attribute>
		</LineItem>
		
		<LineItem Type="Body" TransferLine="Yes" Category="LC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFL[LBR_TYPE='LAB']/LBR_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('BodyLaborAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="adpxsl:getvar('BodyLaborHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('BodyLaborRate')"/></xsl:attribute>
			<xsl:attribute name="RepairHours"><xsl:value-of select="adpxsl:getvar('DetailBodyLaborRepairHours')"/></xsl:attribute>
			<xsl:attribute name="ReplaceHours"><xsl:value-of select="adpxsl:getvar('DetailBodyLaborReplaceHours')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Mechanical" TransferLine="Yes" Category="LC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFL[LBR_TYPE='LAM']/LBR_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('MechLaborAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="adpxsl:getvar('MechLaborHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('MechLaborRate')"/></xsl:attribute>
			<xsl:attribute name="RepairHours"><xsl:value-of select="adpxsl:getvar('DetailMechLaborRepairHours')"/></xsl:attribute>
			<xsl:attribute name="ReplaceHours"><xsl:value-of select="adpxsl:getvar('DetailMechLaborReplaceHours')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Frame" TransferLine="Yes" Category="LC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFL[LBR_TYPE='LAF']/LBR_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('FrameLaborAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="adpxsl:getvar('FrameLaborHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('FrameLaborRate')"/></xsl:attribute>
			<xsl:attribute name="RepairHours"><xsl:value-of select="adpxsl:getvar('DetailFrameLaborRepairHours')"/></xsl:attribute>
			<xsl:attribute name="ReplaceHours"><xsl:value-of select="adpxsl:getvar('DetailFrameLaborReplaceHours')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Paint" TransferLine="Yes" Category="LC" Note="" Taxable="Yes">
			<xsl:attribute name="ExtendedAmount">0</xsl:attribute>
			<xsl:attribute name="Hours">0</xsl:attribute>
			<xsl:attribute name="UnitAmount">0</xsl:attribute>
			<xsl:attribute name="RepairHours">0</xsl:attribute>
			<xsl:attribute name="ReplaceHours">0</xsl:attribute>
		</LineItem>
		<LineItem Type="Refinish" TransferLine="Yes" Category="LC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFL[LBR_TYPE='LAR']/LBR_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('PaintLaborAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="adpxsl:getvar('PaintLaborHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('PaintLaborRate')"/></xsl:attribute>
			<xsl:attribute name="RepairHours"><xsl:value-of select="adpxsl:getvar('DetailPaintLaborRepairHours')"/></xsl:attribute>
			<xsl:attribute name="ReplaceHours"><xsl:value-of select="adpxsl:getvar('DetailPaintLaborReplaceHours')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Other" TransferLine="Yes" Category="LC" Note="" Taxable="Yes">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TotalOtherLabor')"/></xsl:attribute>
			<xsl:attribute name="Hours">0</xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('TotalOtherLabor')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="LaborTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TotalLaborAmount')"/></xsl:attribute>
		</LineItem>
			
          <xsl:if test="adpxsl:getvar('TowingAmount') &gt; 0">
			<LineItem Type="Towing" TransferLine="Yes" Category="AC" Note="">
				<xsl:attribute name="Taxable">
					<xsl:choose>
	                    	<xsl:when test="PFH/TAX_TOW_IN='T'">Yes</xsl:when>
	                		<xsl:otherwise>No</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TowingAmount')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('TowingAmount')"/></xsl:attribute>
			</LineItem>
          </xsl:if>
          <xsl:if test="adpxsl:getvar('StorageAmount') &gt; 0">
			<LineItem Type="Storage" TransferLine="Yes" Category="AC" Note="">
				<xsl:attribute name="Taxable">
					<xsl:choose>
	                    	<xsl:when test="PFH/TAX_STR_IN='T'">Yes</xsl:when>
	                		<xsl:otherwise>No</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('StorageAmount')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('StorageAmount')"/></xsl:attribute>
			</LineItem>
          </xsl:if>
          <xsl:if test="adpxsl:getvar('SubletAmount') &gt; 0">
			<LineItem Type="Sublet" TransferLine="Yes" Category="AC" Note="">
				<xsl:attribute name="Taxable">
					<xsl:choose>
	                    	<xsl:when test="PFH/TAX_SUB_IN='T'">Yes</xsl:when>
	                		<xsl:otherwise>No</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('SubletAmount')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('SubletAmount')"/></xsl:attribute>
			</LineItem>
          </xsl:if>
          <xsl:if test="adpxsl:getvar('OtherAddlChargesAmount') &gt; 0">
			<LineItem Type="Other" TransferLine="Yes" Category="AC" Note="" Taxable="Yes">
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('OtherAddlChargesAmount')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('OtherAddlChargesAmount')"/></xsl:attribute>
			</LineItem>
          </xsl:if>
		<LineItem Type="AdditionalChargesTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TotalAddlCharges')"/></xsl:attribute>
		</LineItem>

		<LineItem Type="SubTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('SubtotalAmount')"/></xsl:attribute>
		</LineItem>
          <xsl:if test="adpxsl:getvar('SalesTaxAmountPM') &gt; 0">
			<LineItem Type="Sales" TransferLine="Yes" Category="TX" Note="">
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('SalesTaxAmountPM')"/></xsl:attribute>
				<xsl:attribute name="Percent"><xsl:value-of select="adpxsl:getvar('SalesTaxPctPM')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('SalesTaxBasePM')"/></xsl:attribute>
				<xsl:attribute name="TaxBase">
					<xsl:choose>
		                    <xsl:when test="adpxsl:getvar('SalesTaxBasePM') = adpxsl:getvar('TotalParts')">Parts</xsl:when>
		                    <xsl:when test="adpxsl:getvar('SalesTaxBasePM') = adpxsl:getvar('TotalMaterials')">Materials</xsl:when>
		                    <xsl:when test="adpxsl:getvar('SalesTaxBasePM') = adpxsl:getvar('TotalPartsMaterials')">PartsAndMaterials</xsl:when>
						<xsl:otherwise>Other</xsl:otherwise>
	                	</xsl:choose>
				</xsl:attribute>
			</LineItem>
          </xsl:if>
          <xsl:if test="adpxsl:getvar('SalesTaxAmountLabor') &gt; 0">
			<LineItem Type="Sales" TransferLine="Yes" Category="TX" Note="">
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('SalesTaxAmountLabor')"/></xsl:attribute>
				<xsl:attribute name="Percent"><xsl:value-of select="adpxsl:getvar('SalesTaxPctLabor')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('SalesTaxBaseLabor')"/></xsl:attribute>
				<xsl:attribute name="TaxBase">
					<xsl:choose>
		                    <xsl:when test="adpxsl:getvar('SalesTaxBaseLabor') = adpxsl:getvar('TotalLaborAmount')">Labor</xsl:when>
						<xsl:otherwise>Other</xsl:otherwise>
	                	</xsl:choose>
				</xsl:attribute>
			</LineItem>
          </xsl:if>
          <xsl:if test="adpxsl:getvar('OtherTaxAmount') &gt; 0">
			<LineItem Type="Sales" TransferLine="Yes" Category="TX">
				<xsl:attribute name="Note"><xsl:text>Tax from Sublet, Towing, Storage or other source</xsl:text></xsl:attribute>
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('OtherTaxAmount')"/></xsl:attribute>
				<xsl:attribute name="Percent">100</xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('OtherTaxAmount')"/></xsl:attribute>
				<xsl:attribute name="TaxBase">Other</xsl:attribute>
			</LineItem>
          </xsl:if>
		<LineItem Type="TaxTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TotalTaxAmount')"/></xsl:attribute>
		</LineItem>

		<LineItem Type="RepairTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('RepairTotalAmount')"/></xsl:attribute>
		</LineItem>

		<LineItem Type="AppearanceAllowance" TransferLine="Yes" Category="AJ" Note="">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('AppearanceAllowanceAmount')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('AppearanceAllowanceAmount')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Betterment" TransferLine="Yes" Category="AJ" Note="">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('BettermentAmount')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('BettermentAmount')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Deductible" TransferLine="Yes" Category="AJ" Note="">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('DeductibleAmount')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('DeductibleAmount')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Other" TransferLine="Yes" Category="AJ" Note="">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('OtherAdjustmentAmount')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('OtherAdjustmentAmount')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="AdjustmentTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('TotalAdjustmentAmount')"/></xsl:attribute>
		</LineItem>


		<LineItem Type="NetTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('NetTotalAmount')"/></xsl:attribute>
		</LineItem>


		<LineItem Type="Variance" TransferLine="Yes" Category="VA">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('ZZVarianceAmount')"/></xsl:attribute>
		</LineItem>
		
		<LineItem Type="OEMParts" TransferLine="Yes" Category="OT">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFP[PRT_TYPE='PAN']/PRT_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('DetailOEMParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('DetailOEMParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="LKQParts" TransferLine="Yes" Category="OT">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFP[PRT_TYPE='PAL']/PRT_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('DetailLKQParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('DetailLKQParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="AFMKParts" TransferLine="Yes" Category="OT">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFP[PRT_TYPE='PAA']/PRT_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('DetailAFMKParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('DetailAFMKParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="RemanParts" TransferLine="Yes" Category="OT">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="PFP[PRT_TYPE='PAM']/PRT_TAX_IN='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('DetailRemanParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('DetailRemanParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="2 Wheel Alignment" TransferLine="No" Category="OT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('Detail2WAlign')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('Detail2WAlign')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="4 Wheel Alignment" TransferLine="No" Category="OT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('Detail4WAlign')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('Detail4WAlign')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Clear Coat" TransferLine="No" Category="OT">
			<xsl:attribute name="Hours">
				<xsl:value-of select="sum(//LIN[cu:isClearCoat(string(LINE_DESC)) = true()]/MOD_LB_HRS)"/>
            </xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isClearCoat(string(LINE_DESC)) = true()]/LBR_AMT)) +                                        cu:Number(sum(//LIN[cu:isClearCoat(string(LINE_DESC)) = true()]/ACT_PRICE)))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Waste Management" TransferLine="No" Category="OT">
			<xsl:attribute name="Amount">
                <xsl:value-of select="cu:Number(sum(//LIN[cu:isWaste(string(LINE_DESC)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isWaste(string(LINE_DESC)) = true()]/ACT_PRICE))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Car Cover" TransferLine="No" Category="OT">
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isCarCover(string(LINE_DESC)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isCarCover(string(LINE_DESC)) = true()]/ACT_PRICE)))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Manual Line" TransferLine="No" Category="OT">
			<xsl:attribute name="Count">
				<xsl:value-of select="count(//LIN[cu:isManualLine(string(LineSource)) = true()])"/>
            </xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isManualLine(string(LineSource)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isManualLine(string(LineSource)) = true()]/ACT_PRICE)))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Painted Pinstripe" TransferLine="No" Category="OT">
			<xsl:attribute name="Hours">
				<xsl:value-of select="cu:Number(sum(//LIN[cu:isPaintedPinstripe(string(LINE_DESC)) = true()]/MOD_LB_HRS))"/>
            </xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isPaintedPinstripe(string(LINE_DESC)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isPaintedPinstripe(string(LINE_DESC)) = true()]/ACT_PRICE)))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Taped Pinstripe" TransferLine="No" Category="OT">
			<xsl:attribute name="Hours">
				<xsl:value-of select="cu:Number(sum(//LIN[cu:isTapedPinstripe(string(LINE_DESC)) = true()]/MOD_LB_HRS))"/>
            </xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isTapedPinstripe(string(LINE_DESC)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isTapedPinstripe(string(LINE_DESC)) = true()]/ACT_PRICE)))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Freon" TransferLine="No" Category="OT">
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isFreon(string(LINE_DESC)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isFreon(string(LINE_DESC)) = true()]/ACT_PRICE)))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Towing" TransferLine="No" Category="OT">
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isTowing(string(LINE_DESC)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isTowing(string(LINE_DESC)) = true()]/ACT_PRICE)))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Setup and Measure" TransferLine="No" Category="OT">
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//LIN[cu:isSetupMeasure(string(LINE_DESC)) = true()]/LBR_AMT)) + cu:Number(sum(//LIN[cu:isSetupMeasure(string(LINE_DESC)) = true()]/ACT_PRICE)))"/>
            </xsl:attribute>
        </LineItem>			
		<LineItem Type="DeductiblesApplied" TransferLine="Yes" Category="OT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="adpxsl:getvar('DeductibleAmount')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="adpxsl:getvar('DeductibleAmount')"/></xsl:attribute>
        </LineItem>			
		<LineItem Type="LimitEffect" TransferLine="Yes" Category="OT">
			<xsl:attribute name="ExtendedAmount"/>
			<xsl:attribute name="UnitAmount"/>
        </LineItem>			
		<LineItem Type="NetTotalEffect" TransferLine="Yes" Category="OT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="number(adpxsl:getvar('RepairTotalAmount')) - number(adpxsl:getvar('DeductibleAmount'))"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="number(adpxsl:getvar('RepairTotalAmount')) - number(adpxsl:getvar('DeductibleAmount'))"/></xsl:attribute>
        </LineItem>			
	</Summary>
</xsl:template>


<!--- Compile Audit Information -->
<xsl:template match="EMS" mode="Audit">
	<Audit>
		<AuditItem Name="VerifyDetailPartTaxTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="adpxsl:getvar('DetailPartTaxTotal') = adpxsl:getvar('TotalTaxParts')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="adpxsl:getvar('DetailPartTaxTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="adpxsl:getvar('TotalTaxParts')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyDetailPartNonTaxTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="adpxsl:getvar('DetailPartNonTaxTotal') = adpxsl:getvar('TotalNonTaxParts')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="adpxsl:getvar('DetailPartNonTaxTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="adpxsl:getvar('TotalNonTaxParts')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyDetailPartOtherTaxTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="adpxsl:getvar('DetailPartOtherTaxTotal') = adpxsl:getvar('TotalOtherTaxParts')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="adpxsl:getvar('DetailPartOtherTaxTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="adpxsl:getvar('TotalOtherTaxParts')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyDetailPartOtherNonTaxTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="adpxsl:getvar('DetailPartOtherNonTaxTotal') = adpxsl:getvar('TotalOtherNonTaxParts')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="adpxsl:getvar('DetailPartOtherNonTaxTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="adpxsl:getvar('TotalOtherNonTaxParts')"/></xsl:attribute>
		</AuditItem>

		<AuditItem Name="VerifyBodyLaborHours">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="round((adpxsl:getvar('DetailBodyLaborRepairHours') + adpxsl:getvar('DetailBodyLaborReplaceHours')) * 10) div 10  = adpxsl:getvar('BodyLaborHours')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="round((adpxsl:getvar('DetailBodyLaborRepairHours') + adpxsl:getvar('DetailBodyLaborReplaceHours')) * 10) div 10"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="adpxsl:getvar('BodyLaborHours')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyFrameLaborHours">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="round((adpxsl:getvar('DetailFrameLaborRepairHours') + adpxsl:getvar('DetailFrameLaborReplaceHours')) * 10) div 10 = adpxsl:getvar('FrameLaborHours')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="round((adpxsl:getvar('DetailFrameLaborRepairHours') + adpxsl:getvar('DetailFrameLaborReplaceHours')) * 10) div 10"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="adpxsl:getvar('FrameLaborHours')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyMechLaborHours">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="round((adpxsl:getvar('DetailMechLaborRepairHours') + adpxsl:getvar('DetailMechLaborReplaceHours')) * 10) div 10 = adpxsl:getvar('MechLaborHours')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="round((adpxsl:getvar('DetailMechLaborRepairHours') + adpxsl:getvar('DetailMechLaborReplaceHours')) * 10) div 10"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="adpxsl:getvar('MechLaborHours')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyPaintLaborHours">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="round((adpxsl:getvar('DetailPaintLaborRepairHours') + adpxsl:getvar('DetailPaintLaborReplaceHours')) * 10) div 10 = adpxsl:getvar('PaintLaborHours')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="round((adpxsl:getvar('DetailPaintLaborRepairHours') + adpxsl:getvar('DetailPaintLaborReplaceHours')) * 10) div 10"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="adpxsl:getvar('PaintLaborHours')"/></xsl:attribute>
		</AuditItem>
		
		<AuditItem Name="VerifyDetailSubletTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="adpxsl:getvar('DetailSubletTotal') = adpxsl:getvar('SubletAmount')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="adpxsl:getvar('DetailSubletTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="adpxsl:getvar('SubletAmount')"/></xsl:attribute>
		</AuditItem>		

		<AuditItem Name="VerifyBettermentTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="adpxsl:getvar('DetailBettermentTotal') = adpxsl:getvar('BettermentAmount')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="adpxsl:getvar('DetailBettermentTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="adpxsl:getvar('BettermentAmount')"/></xsl:attribute>
		</AuditItem>		

		<AuditItem Name="VerifyNoDetailUnknownTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="adpxsl:getvar('DetailUnknownTotal') = 0">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="adpxsl:getvar('DetailUnknownTotal')"/></xsl:attribute>
		</AuditItem>		
	</Audit> 
</xsl:template>

</xsl:stylesheet>
