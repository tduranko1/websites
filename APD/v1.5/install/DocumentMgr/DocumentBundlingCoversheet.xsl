<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output method="html" indent="yes" encoding="UTF-8"/>
  <msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function getServerDateTime(){
        var dt = new Date();
        return dt.toLocaleString(); //dt.toUTCString();
    }
    
    function resolveBody(strBody, strAttachments){
        var strRet = strBody;
        strRet = strBody.replace(/\$AttachmentList\$/g, strAttachments);
        strRet = strRet.replace(/[|]/g, "<br/>\n");
        return strRet;
    }
  ]]>
  </msxsl:script>
  <xsl:template match="/">
    <html>
      <head>
        <title>Bundling Coversheet</title>
        <style>
         .header {
            background-color: #000000;
            Font: 16pt Arial Black, Arial, Helvetica, sans-serif;
            color: #FFFFFF;
         }
         td, div {
            font: 10pt Arial, Helvetica, sans-serif;
         }
      </style>
      </head>
      <body>
        <div style="border:0px;border-bottom:1mm solid #000000;font:12pt Arial;font-weight:bold;padding:1mm">Auto Physical Damage - Document Bundling</div>
        <br/>
        <table border="0" cellpadding="1" cellspacing="0">
          <colgroup>
            <col style="width:2cm"/>
            <col width="*"/>
          </colgroup>
          <tr>
            <td>
              <b>From:</b>
            </td>
            <td>
              <xsl:value-of select="/ClientDocument/Send/@from"/>
            </td>
          </tr>
          <tr>
            <td>
              <b>Sent:</b>
            </td>
            <td>
              <xsl:value-of select="user:getServerDateTime()"/>
            </td>
          </tr>
          <tr>
            <td>
              <b>To:</b>
            </td>
            <td>
                <xsl:choose>
                <xsl:when test="/ClientDocument/Send/@via='EML'"><xsl:value-of select="/ClientDocument/Send/@value"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="concat(/ClientDocument/Send/@via, ': ', /ClientDocument/Send/@value)"/></xsl:otherwise>
                </xsl:choose>
            </td>
          </tr>
          <tr valign="top">
            <td>
              <b>Subject:</b>
            </td>
            <td>
              <xsl:value-of select="/ClientDocument/Send/EmailSubject"/>
            </td>
          </tr>
        </table>
        <br/>
        <div>
          <xsl:value-of disable-output-escaping="yes" select="user:resolveBody(string(/ClientDocument/Send/EmailBody), string(/ClientDocument/AttachmentsHTML))"/>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>