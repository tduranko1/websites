<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes" cdata-section-elements=""/>
  <xsl:template match="/ClientDocument">
    <xsl:element name="root">
      <xsl:attribute name="LynxID">
        <xsl:value-of select="Claim/@LynxID"/>
      </xsl:attribute>
      <xsl:attribute name="PertainsTo">
        <xsl:value-of select="Send/@PertainsTo"/>
      </xsl:attribute>
      <xsl:attribute name="ClaimAspectServiceChannelID">
        <xsl:value-of select="Send/@ClaimAspectServiceChannelID"/>
      </xsl:attribute>
      <xsl:attribute name="FileType">
        <xsl:value-of select="Send/@SaveAs"/>
      </xsl:attribute>
      <xsl:attribute name="SupplementSeqNo"/>
      <xsl:attribute name="UserID">0</xsl:attribute>
      <xsl:attribute name="DocumentSource">
        <xsl:variable name="InsCompId">
          <xsl:value-of  select="//Claim/@InsuranceCompanyID"/>
        </xsl:variable>
        <xsl:if test="$InsCompId = '158'">
          <xsl:choose>
            <xsl:when test="Vehicle/@isNugen='1' and //Send/@description='Web Services'">Web Services</xsl:when>
            <xsl:otherwise>Email</xsl:otherwise>
          </xsl:choose>
        </xsl:if>
        <xsl:if test="$InsCompId = '387'">
          <xsl:choose>
            <xsl:when test="//Send/@description='Web Services'">Web Services</xsl:when>
            <xsl:otherwise>Email</xsl:otherwise>
          </xsl:choose>
        </xsl:if>
      </xsl:attribute>
      <xsl:attribute name="mergeAll">true</xsl:attribute>
      <xsl:attribute name="settingsOverride">
        <xsl:value-of select="Send/@SettingsOverride"/>
      </xsl:attribute>

      <xsl:element name="File">
        <xsl:attribute name="FileName"/>
        <xsl:attribute name="FileExt">
          <xsl:value-of select="Send/@PackageType"/>
        </xsl:attribute>
        <xsl:attribute name="FileLength"/>
        <xsl:attribute name="key">$JOBDOCUMENT$</xsl:attribute>
        <xsl:attribute name="NotifyEvent">0</xsl:attribute>
        <xsl:attribute name="DirectionalCD">O</xsl:attribute>
        <xsl:attribute name="SendToCarrierStatusCD">S</xsl:attribute>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
