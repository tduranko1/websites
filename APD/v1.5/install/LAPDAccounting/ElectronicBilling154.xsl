<?xml version="1.0" encoding="UTF-8"?>

<!--  This sheet converts the electronic billing source into a stable format before shipping it off to GLAXIS.  The BizTalk process at GO requires a stable format.  Any changes, even additional, unused elements or attributes
        must be reflected in the BizTalk template or the process will fail.  This sheet allows us to maintain a stable format for that purpose while continuing to evolve our own invoicing xml formats as necessary.  -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/Root">
		<Root>
			<xsl:for-each select="@*">
				<xsl:attribute name="{name()}">
					<xsl:value-of select="."/>
				</xsl:attribute>
			</xsl:for-each>
			<xsl:for-each select="Claim">
				<Claim>		
					<xsl:for-each select="@*">					
						<xsl:attribute name="{name()}">
							<xsl:value-of select="."/>
						</xsl:attribute>
					</xsl:for-each>		
					<xsl:for-each select="Exposure">
						<Exposure>
							<xsl:for-each select="@*">	
								<xsl:choose>
						                	<xsl:when test="name()='HandlingShopLocationID'">		<!-- rename HandlingShopLocationID to LastShopLocationID -->
										<xsl:attribute name="LastShopLocationID">
											<xsl:value-of select="."/>
										</xsl:attribute >
									</xsl:when>
									<xsl:when test="name()='APDShopLocationID'"></xsl:when>		<!-- omit APDShopLocationID -->
									<xsl:when test="name()='Deductible'"></xsl:when>			<!-- omit Deductible -->											
									<xsl:when test="name()='ServiceChannel'"></xsl:when>		<!-- omit ServiceChannel -->											
									<xsl:otherwise>													
										<xsl:attribute name="{name()}">
											<xsl:value-of select="."/>
										</xsl:attribute>
									</xsl:otherwise>
						              </xsl:choose>
							</xsl:for-each>	
							<xsl:for-each select="Invoice">
								<Invoice>
									<xsl:for-each select="@*">
										<xsl:choose>
							                      	<!-- <xsl:when test="name()='PayeeName'"></xsl:when>		omit PayeeName -->
											<xsl:when test="name()='Deductible'"></xsl:when>		<!-- omit Deductible -->
											<xsl:when test="name()='ServiceChannel'"></xsl:when>	<!-- omit ServiceChannel -->
											<xsl:when test="name()='InitialPayment'"></xsl:when>    <!-- omit InitialPayment -->									
											<xsl:otherwise>
												<xsl:attribute name="{name()}">
													<xsl:value-of select="."/>
												</xsl:attribute>
											</xsl:otherwise>
							                    </xsl:choose>										
									</xsl:for-each>	
								</Invoice>
							</xsl:for-each>
						</Exposure>
					</xsl:for-each>
				</Claim>
			</xsl:for-each>
		</Root>
	</xsl:template>
</xsl:stylesheet>
