<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="yes" indent="yes" method="xml"
               cdata-section-elements="Body"/>
  <msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
    function needExpandedComments(strComments) {
        var blnRet = false;
        if (typeof(strComments) == "string" && strComments != ""){
            var arrLines = strComments.split("\n");
            var iLines = arrLines.length;
            var iChars = strComments.length;
            
            if (iLines > 4 || iChars > 400){
               blnRet = true;
            }
        }
        return blnRet;
    }


    function getMailTo(strEmailAddress, strSubject){
      return 'mailto:' + strEmailAddress + '&subject=' + escape(strSubject);
    }
  ]]>
  </msxsl:script>
  <xsl:template match="/">
    <xsl:variable name="LossDate">
      <xsl:value-of select="/Assignment/Claim/@LossDate"/>
    </xsl:variable>
    <xsl:variable name="ClaimRepPhone">
      <xsl:call-template name="formatPhone">
        <xsl:with-param name="fieldValue" select="/Assignment/LynxRep/@RepPhone"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="EmailSubject">
      <xsl:value-of select="concat('Claim #: ', /Assignment/Coverage/@ClaimNumber, ' / Insured: ', /Assignment/Claim/@InsuredFirstName, ' ', /Assignment/Claim/@InsuredLastName, ' / Loss Date: ', $LossDate, ' / LYNX ID: ', /Assignment/@LynxId, '-', /Assignment/@VehicleNumber)"/>
    </xsl:variable>
    <xsl:variable name="BaseURL">
      <xsl:choose>
        <!-- Production and DR -->
        <xsl:when test="contains(/Assignment/@Environment, 'PROD') = true() or contains(/Assignment/@Environment, 'DR') = true()">
          <xsl:value-of select="'https://www.lynxservices.com/APD/UploadPortal/Uploadportal.aspx?id='"/>
        </xsl:when>
        <!-- Stage -->
        <xsl:when test="contains(/Assignment/@Environment, 'STAGE') = true()">
          <xsl:value-of select="'https://swww.lynxservices.com/APD/UploadPortal/Uploadportal.aspx?id='"/>
        </xsl:when>
        <!-- DEFAULT: Development -->
        <xsl:otherwise>
          <xsl:value-of select="'http://dwww.lynxservices.pgw.local/APD/UploadPortal/Uploadportal.aspx?id='"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="QueryStringValue">
      <xsl:value-of select="concat(/Assignment/@LynxId, '-', /Assignment/Vehicle/@ClaimAspectID, '-', /Assignment/Claim/@InsuranceCompanyID)"/>
    </xsl:variable>
    <xsl:variable name="UploadURL">
      <xsl:value-of select="concat($BaseURL, $QueryStringValue)"/>
    </xsl:variable>
    <xsl:variable name="EmailBody">&lt;html&gt;
        &lt;head&gt;
        &lt;style&gt;
          p {font: 10pt Arial, Helvetica, sans-serif;}
        &lt;/style&gt;
        &lt;/head&gt;
        &lt;body&gt;
        <xsl:choose>
          <xsl:when test="/Assignment/@AssignmentCode = 'Cancel'">
            <!-- Put the cancel assignment body text here. -->
            &lt;p&gt;Attached is a &lt;font style="color:#FF0000;font-weight:bold"&gt;Cancellation&lt;/font&gt; for a recent LYNXAdvantage Repair Referral Assignment to your repair facility. &lt;u&gt;Please cancel this assignment&lt;/u&gt;, as the vehicle owner has decided not to move forward with this repair at this time.&lt;/p&gt;
            &lt;p&gt;Inquiries associated with this request should be directed to LYNX Services at the contact info listed below: &lt;/p&gt;
          </xsl:when>
          <xsl:otherwise>
            <!-- We are going to assume the assignment code is New. Put the new assignment body text here. -->
            &lt;p&gt;
            Attached is a new LYNXAdvantage Repair Referral Assignment where the vehicle owner has chosen to have your repair facility handle the repair of their vehicle.
            &lt;/p&gt;
            &lt;p&gt;
            Upon vehicle inspection, to help expedite the payment process for this claim we ask that you please forward your repair estimate with related photos as soon as possible. For direct document uploads the following link is provided for your convenience (or you can enter the displayed address directly into your web browser). If needed, your estimate and photos can also be sent by email to
            &lt;a href="<xsl:value-of select="user:getMailTo('apdnol@lynxservices.com', string($EmailSubject))"/>"&gt;apdnol@lynxservices.com&lt;/a&gt;
            &lt;/p&gt;
            &lt;p&gt;
            Upload Link: &lt;a href="<xsl:value-of select="$UploadURL"/>"&gt;Click here&lt;/a&gt;<br/>
            Or Paste the following in your Internet Browser: <xsl:value-of select="$UploadURL"/>
            &lt;/p&gt;
            &lt;p&gt;
            Please direct any inquiries associated with this request to LYNX Services at the contact info listed below:
            &lt;/p&gt;
          </xsl:otherwise>
        </xsl:choose>
        &lt;p&gt;
        <xsl:value-of select="concat(/Assignment/LynxRep/@RepNameFirst, ' ', /Assignment/LynxRep/@RepNameLast, ', LYN')"/>&lt;font color="#FF0000"&gt;X&lt;/font&gt; APD Claim Representative&lt;br/&gt;
        <xsl:value-of select="$ClaimRepPhone"/>&lt;br/&gt;
        &lt;a href="<xsl:value-of select="user:getMailTo(string(/Assignment/LynxRep/@RepEmailAddress), string($EmailSubject))"/>"&gt;<xsl:value-of select="/Assignment/LynxRep/@RepEmailAddress"/>&lt;/a&gt;
        &lt;/p&gt;
        &lt;/body&gt;
        &lt;/html&gt;</xsl:variable>

    <xsl:element name="CustomForm">
      <!-- Processing Instructions -->
      <xsl:element name="ProcessingInstructions">
        <xsl:attribute name="filename"><xsl:value-of select="concat('RRPAssignment ', /Assignment/@LynxId, '-', /Assignment/@VehicleNumber, '.pdf')"/></xsl:attribute>
        <xsl:element name="APD">
          <xsl:attribute name="documentType">Shop Assignment</xsl:attribute>
          <xsl:attribute name="userid">0</xsl:attribute>
          <xsl:attribute name="Lynxid"><xsl:value-of select="/Assignment/@LynxId"/></xsl:attribute>
          <xsl:attribute name="vehNum"><xsl:value-of select="/Assignment/@VehicleNumber"/></xsl:attribute>
          <xsl:attribute name="direction">O</xsl:attribute>
        </xsl:element>
        <xsl:element name="Destinations">
          <xsl:element name="Email">
            <xsl:attribute name="from"><xsl:value-of select="/Assignment/LynxRep/@RepEmailAddress"/></xsl:attribute>
            <xsl:attribute name="to"><xsl:value-of select="/Assignment/@DeliveryAddress"/></xsl:attribute>
            <xsl:attribute name="cc"/>
            <xsl:attribute name="bcc"/>
            <xsl:attribute name="replyto"><xsl:value-of select="/Assignment/LynxRep/@RepEmailAddress"/></xsl:attribute>
            <xsl:attribute name="subject"><xsl:value-of select="$EmailSubject"/></xsl:attribute>
            <xsl:element name="Body"><xsl:value-of select="$EmailBody"/></xsl:element>
          </xsl:element>
        </xsl:element>
      </xsl:element>
      <!-- main form -->
      <xsl:for-each select="/Assignment/Form">
        <xsl:variable name="parentSQL" select="@SQLProcedure"/>
        <xsl:variable name="parentFormID" select="@FormID"/>
        <xsl:variable name="needExpandedComments" select="user:needExpandedComments(string(//Vehicle/@ShopRemarks))"/>
        <xsl:element name="Form">
          <xsl:attribute name="formid">
            <xsl:value-of select="@FormID"/>
          </xsl:attribute>
          <xsl:attribute name="supplementid">
            <xsl:value-of select="@FormSupplementID"/>
          </xsl:attribute>
          <xsl:attribute name="formfile">
            <xsl:value-of select="@PDFPath"/>
          </xsl:attribute>
          <xsl:attribute name="xslfdf">
            <!-- Shop Instructions don't have any data element and does not need a FDF file -->
            <xsl:if test="@Name != 'Shop Instructions'">
              <xsl:value-of select="concat(substring(@PDFPath, 1, string-length(@PDFPath) - 3), 'xsl')"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:element name="Data">
            <xsl:call-template name="copyData">
              <xsl:with-param name="suppressVehComments" select="$needExpandedComments"/>
            </xsl:call-template>
          </xsl:element>
        </xsl:element>
        <!-- Form Supplements -->
        <xsl:for-each select="Supplement">
          <xsl:if test="(@Name != 'Expanded Comments') or ((@Name = 'Expanded Comments') and ($needExpandedComments = boolean('true')))">
            <xsl:element name="Form">
              <xsl:attribute name="formid">
                <xsl:value-of select="$parentFormID"/>
              </xsl:attribute>
              <xsl:attribute name="supplementid">
                <xsl:value-of select="@FormSupplementID"/>
              </xsl:attribute>
              <xsl:attribute name="formfile">
                <xsl:value-of select="@PDFPath"/>
              </xsl:attribute>
              <xsl:attribute name="xslfdf">
                <!-- Shop Instructions don't have any data element and does not need a FDF file -->
                <xsl:if test="contains(@Name, 'Shop Instructions') = false()">
                  <xsl:value-of select="concat(substring(@PDFPath, 1, string-length(@PDFPath) - 3), 'xsl')"/>
                </xsl:if>
              </xsl:attribute>
              <xsl:choose>
                <xsl:when test="@SQLProcedure = $parentSQL or @SQLProcedure = ''">
                  <xsl:element name="Data">
                    <xsl:call-template name="copyData"/>
                  </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="SQLProcedure">
                    <xsl:value-of select="@SQLProcedure"/>
                  </xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:element>
          </xsl:if>
        </xsl:for-each>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
  <xsl:template name="copyData">
    <xsl:param name="suppressVehComments"/>
    <xsl:for-each select="/Assignment">
      <xsl:element name="Assignment">
        <xsl:copy-of select="@*"/>
        <xsl:element name="Claim">
          <xsl:copy-of select="Claim/@*"/>
        </xsl:element>
        <xsl:element name="Coverage">
          <xsl:copy-of select="Coverage/@*"/>
        </xsl:element>
        <xsl:element name="Vehicle">
          <xsl:copy-of select="Vehicle/@*"/>
          <xsl:attribute name="ShopRemarks2">
            <xsl:choose>
              <xsl:when test="$suppressVehComments = boolean('true')">See Expanded comments</xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="Vehicle/@ShopRemarks"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
        </xsl:element>
        <xsl:element name="VehicleContact">
          <xsl:copy-of select="VehicleContact/@*"/>
        </xsl:element>
        <xsl:element name="Shop">
          <xsl:copy-of select="Shop/@*"/>
        </xsl:element>
        <xsl:element name="Owner">
          <xsl:copy-of select="Owner/@*"/>
        </xsl:element>
        <xsl:element name="LynxRep">
          <xsl:copy-of select="LynxRep/@*"/>
        </xsl:element>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>
<xsl:template name="formatSQLDate">
    <xsl:param name="fieldValue"/>
    <xsl:variable name="year" select="substring($fieldValue, 1, 4)"/>
    <xsl:variable name="month" select="substring($fieldValue, 6, 2)"/>
    <xsl:variable name="date" select="substring($fieldValue, 9, 2)"/>
    <xsl:variable name="hour" select="substring($fieldValue, 12, 2)"/>
    <xsl:variable name="min" select="substring($fieldValue, 15, 2)"/>
    <xsl:variable name="sec" select="substring($fieldValue, 18, 2)"/>
    <xsl:value-of select="concat($month, '/', $date, '/', $year)"/>
</xsl:template>
<xsl:template name="formatPhone">
    <xsl:param name="fieldValue"/>
    <xsl:variable name="val" select="translate($fieldValue, ')(- ', '')"/>
    <xsl:if test="$fieldValue != ''">
      <xsl:value-of select="concat('(', substring($val, 1, 3), ') ', substring($val, 4, 3), ' ', substring($val, 7))"/>
    </xsl:if>
</xsl:template>
</xsl:stylesheet>


