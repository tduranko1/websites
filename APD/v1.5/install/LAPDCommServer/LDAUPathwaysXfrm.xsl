<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace">
    
<xsl:output method="text" indent="no" encoding="UTF-8"/>

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
   var reClean = /[\t\n\f]/g;
   function crlf(){
   	return String.fromCharCode(13, 10);
   }
   
   function transformTimeStamp() {
      if (sTimeStamp == ""){
         var dt = new Date();
         sTimeStamp = formatNumber(dt.getMonth() + 1, "00") + formatNumber(dt.getDate(), "00") + formatNumber(dt.getFullYear(), "0000") + formatNumber(dt.getHours(), "00") + formatNumber(dt.getMinutes(), "00") + formatNumber(dt.getSeconds(), "00");
         return sTimeStamp;
      }
   }
   
   function IncrTimeStamp(){
   	sTimeStamp = (parseInt(sTimeStamp, 10) + 1) + "";
   	return sTimeStamp;
   }
   
   function formatNumberPadded(sVal, sPadChar, iLen){
      if (sPadChar == "") sPadChar = "0";
      if (sVal != ""){
         if (typeof(sVal) != "string")
            sVal = sVal.toString();
         
         var sFormat = charFill(sPadChar, iLen);
         return formatNumber(sVal, sFormat);
      } else
      	return "";
   }
   
   function formatNumber(sVal, sFormat){
      if (sFormat != ""){
         if (typeof(sVal) != "string")
            sVal = sVal.toString();
            
         var sValLength = sVal.length;
         var sFormatLength = sFormat.length;
         var sRet = sFormat;
         
         if (sFormatLength > sValLength)
            sRet = sFormat.substr(0, sFormatLength - sValLength) + sVal;
         else
            sRet = sVal;
         
         return sRet;
         
      } else 
         return sVal;
   }
   
   function formatDeductible(dVal, iIntLen, iDecLen){
      var sRet = charFill("0", iIntLen + iDecLen);
      
      if (dVal > 0){
         var sVal;
         if (iDecLen > 0)
            sVal = parseInt(dVal * Math.pow(10, iDecLen), 10);
         else
            sVal = parseInt(dVal, 10);
         sRet = formatNumberPadded(sVal, "0", (iIntLen + iDecLen));
      }
      
      return sRet;
   }
   
   function charFill(sChar, iLen){
      var sRet = "";
      if (isNaN(iLen)) iLen = 1;
      
      for (var i = iLen; i > 0; i--)
         sRet += sChar;
      
      return sRet;
   }

   function space(iLen){
      if (iLen > 0)
         return charFill(" ", iLen);
         //return sStringSpace.substr(0, iLen);
      else
         //return charFill(" ", iLen);
         return "";
   }
   
   function formatStringPadded(sVal, sPadChar, iLen){
      if (sPadChar == "") sPadChar = " ";
      if (sVal != ""){
         if (typeof(sVal) != "string")
            sVal = sVal.toString();
         
         var sFormat = "";
         //if (sPadChar == " ") 
         //	sFormat = sStringSpace.substr(0, iLen);
         //else
         	sFormat = charFill(sPadChar, iLen);
         	
        	//clean the string for any crlf, tabs etc.
        	sVal = sVal.replace(reClean, " ");
         
         if (sVal.length > sFormat.length)
         	return sVal.substr(0, iLen);
        	else
        		return sVal + sFormat.substr(0, (iLen - sVal.length));
      } else
      	return charFill(sPadChar, iLen);   	
   }
   
   function getAttrib(oNode, sAttrib){
   	if (typeof(oNode) == "object") {
   		var oNode2;
   		if (oNode[0])
   			oNode2 = oNode[0];
   		else
   			oNode2 = oNode;
         var objVal = oNode2.selectSingleNode("@" + sAttrib);
         var sRet = "";
         if (objVal)
            sRet = objVal.value;
         else
            sRet = "";
         return sRet;
   		
   	} else
   		return "";
   }
   
   function getAttribFormatted(oNode, sAttrib, sPadChar, iLen){
   	var sVal = getAttrib(oNode, sAttrib);
   	if (iLen == 0) iLen = sVal.length;
   	return formatStringPadded(sVal, sPadChar, iLen);
   }
   
   function formatSQLDateTime(str, sFormat){
      if (str != "") {
         var re = /(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(.*)/;
         
         var a = str.match(re);
         if (a) {
         	if (a.length == 8){
         		if (parseInt(a[2], 10) < 10) a[2] = "0" + parseInt(a[2], 10);
         		if (parseInt(a[3], 10) < 10) a[3] = "0" + parseInt(a[3], 10);
         		if (parseInt(a[4], 10) < 10) a[4] = "0" + parseInt(a[4], 10);
         		if (parseInt(a[5], 10) < 10) a[5] = "0" + parseInt(a[5], 10);
         		if (parseInt(a[6], 10) < 10) a[6] = "0" + parseInt(a[6], 10);
         		
					switch (sFormat){
						case "ccyymmdd":
							return a[1] + a[2] + a[3];
							break;
						default:
							return a[2] + "/" + a[3] + "/" + a[1] + " " + a[4] + ":" + a[5] + ":" + a[6];
							break;
					}
            }
         }
      }

      return "";
   }
   
   function formatDateTime(str, sFormat){
   	var sRet;
   	switch (sFormat){
   		case "ccyymmdd": sRet = space(8); break;
   		case "mmddyyyy": sRet = space(8); break;
   	}
   	
      if (str != "") {
         var re = /(\d{1,2})\/(\d{1,2})\/(\d{2,4})/;
         
         var a = str.match(re);
         if (a) {
				switch (sFormat){
					case "ccyymmdd":
						if (parseInt(a[1], 10) < 10) a[1] = "0" + parseInt(a[1], 10);
						if (parseInt(a[2], 10) < 10) a[2] = "0" + parseInt(a[2], 10);
						sRet = a[3] + a[1] + a[2];
						break;
					case "mmddyyyy":
						if (parseInt(a[1], 10) < 10) a[1] = "0" + parseInt(a[1], 10);
						if (parseInt(a[2], 10) < 10) a[2] = "0" + parseInt(a[2], 10);
						sRet = a[1] + a[2] + a[3];
						break;
				}
         }
      }

      return sRet;
   }
   
   function formatSQLDateTime(str, sFormat){
   	var sRet;
   	switch (sFormat){
   		case "ccyymmdd": sRet = space(8); break;
   		case "mmddyyyy": sRet = space(8); break;
   	}
   	
      if (str != "") {
         var re = /(\d{2,4})-(\d{1,2})-(\d{1,2})T(\d{1,2}):(\d{1,2}):(\d{1,2})[.]{0,1}(\d{0,3})/;

         var a = str.match(re);
         if (a) {
				switch (sFormat){
					case "ccyymmdd":
						if (parseInt(a[1], 10) < 10) a[1] = "0" + parseInt(a[1], 10);
						if (parseInt(a[2], 10) < 10) a[2] = "0" + parseInt(a[2], 10);
						sRet = a[3] + a[1] + a[2];
						break;
					case "mmddyyyy":
						if (parseInt(a[1], 10) < 10) a[1] = "0" + parseInt(a[1], 10);
						if (parseInt(a[2], 10) < 10) a[2] = "0" + parseInt(a[2], 10);
						sRet = a[2] + a[3] + a[1];
						break;
				}
         }
      }

      return sRet;
   }

   function formatTime(str, sFormat){
   	var sRet = space(4);
   	
      if (str != "") {
         var re;
         if (sFormat == "hhmm2")
         	re = /(\d{1,2}):(\d{1,2})/;
         else
         	re = /(\d{1,2}):(\d{1,2}):(\d{1,2}) [?:A|P]M/;
         
         var a = str.match(re);
         if (a) {
         	if (parseInt(a[1], 10) < 10) a[1] = "0" + parseInt(a[1], 10);
         	if (parseInt(a[2], 10) < 10) a[2] = "0" + parseInt(a[2], 10);
         	
         	if (sFormat == "hhmm2" && a.length == 3){
					sRet = a[1] + a[2];
            }
            
         	if (sFormat == "hhmm" && a.length == 3){
					sRet = a[1] + a[2];
            }
         }
      }

      return sRet;
   }
   
   function getAMPM(sVal){
   	var sRet = space(2);
   	
   	if (sVal.indexOf("M") == -1){
			var re = /(\d{1,2})/;
			var a = sVal.match(re);
			if (a){
				if (parseInt(a[1], 10) > 12)
					sRet = "PM";
				else
					sRet = "AM";
			}
		} else {
			if (sVal.indexOf("AM") != -1)
				sRet = "AM";
			else
				sRet = "PM"
		}
		return sRet;
   }
   
   function formatZip(sVal, iLen){
     var sRet = charFill(' ', iLen);
     if (sRet != ''){
        var re = /[^0-9]/g;
        sVal = sVal.replace(re, "");
        sRet = formatStringPadded(sVal, ' ', iLen);
     }
     return sRet;
   }
   
   function getFieldDataFormatted(ctx, sAttrib, sPadChar, iLen){
   	var sRet = charFill(sPadChar, iLen);
   	if (typeof(ctx) == "object" && sAttrib != "" && ctx[0]) {
		sRet = getAttribFormatted(ctx, sAttrib, sPadChar, iLen);
   	}
   	return sRet;
   }
   
   function getFieldDateFormatted(ctx, sAttrib, sFormat){
   	var sRet = space(sFormat.length);
   	if (typeof(ctx) == "object" && sAttrib != "" && ctx[0]) {
   	   var sDate = getAttrib(ctx, sAttrib);
   	   if (sDate.indexOf("T") != 0)
    	 sRet = formatSQLDateTime(sDate, sFormat);
  	   else
  	     sRet = formatDateTime(sDate, sFormat);
   	}
   	return sRet;
   }
   
  ]]>
</msxsl:script>

<xsl:template match="/Assignment">
    <!-- Field Name, Field Size, From-To Position -->
    
    <!-- Unused, 6, 1-6 -->
    <xsl:value-of select="user:space(6)"/>
    
    <!-- BRANCH CODE, 5, 7-11 -->
    <xsl:value-of select="user:getFieldDataFormatted(., 'CCCClaimOfficeCode', ' ', 5)"/>
    
    <!-- INTERNAL ASSIGNED-BY CODE, 7, 12-18 -->
    <xsl:value-of select="user:space(7)"/>
    
    <!-- INTERNAL ASSIGNED-TO CODE, 7, 19-25 -->
    <xsl:value-of select="user:formatStringPadded('LDAU', ' ', 7)"/>
    
    <!-- ADJUSTER CODE, 7, 26-32 -->
    <xsl:value-of select="user:space(7)"/>
    
    <!-- CLAIM REFERENCE NUMBER, 25, 33-57 -->
    <xsl:value-of select="user:formatStringPadded(concat('LYNX ID ', string(@LynxId), '-', string(@VehicleNumber), string(@Suffix)), ' ', 25)"/>
    
    <!-- POLICY NUMBER, 25, 58-82 -->
    <xsl:value-of select="user:formatStringPadded(concat('N/A CLAIM # ', string(Coverage/@ClaimNumber)), ' ', 25)"/>
    
    <!-- Unused, 6 , 83-88 -->
    <xsl:value-of select="user:space(6)"/>
    
    <!-- TYPE OF LOSS CODE, 7, 89-95 -->
    <xsl:variable name="TypeOfLossCode">
    <xsl:choose>
       <xsl:when test="Vehicle/@CoverageType = 'Collision'">C</xsl:when>
       <xsl:when test="Vehicle/@CoverageType = 'Comprehensive'">M</xsl:when>
       <xsl:when test="Vehicle/@CoverageType = 'Liability'">L</xsl:when>
       <xsl:when test="Vehicle/@CoverageType = 'Property'">P</xsl:when>
       <xsl:when test="Vehicle/@CoverageType = 'Other'">O</xsl:when>
       <xsl:otherwise>U</xsl:otherwise>
    </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="user:formatStringPadded(string($TypeOfLossCode), ' ', 7)"/>

    <!-- REFERENCE FILE NAME, 15, 96-110 -->
    <xsl:value-of select="user:space(15)"/>

    <!-- TIME OF LOSS, 4 , 111-114 -->
    <xsl:value-of select="user:space(4)"/>

    <xsl:variable name="InsuredName">
        <xsl:choose>
            <xsl:when test="Claim/@InsuredBusinessName != ''"><xsl:value-of select="Claim/@InsuredBusinessName"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="concat(Claim/@InsuredFirstName, ' ', Claim/@InsuredLastName)"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:variable name="OwnerName">
        <xsl:choose>
            <xsl:when test="Owner/@OwnerBusinessName != ''"><xsl:value-of select="Owner/@OwnerBusinessName"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="concat(Owner/@OwnerFirstName, ' ', Owner/@OwnerLastName)"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <!-- INSURED IS THE VEHICLE OWNER, 1, 115 -->
    <xsl:variable name="InsuredIsOwner">
    <xsl:choose>
       <xsl:when test="$InsuredName = $OwnerName and Vehicle/@Party = '1'">Y</xsl:when>
       <xsl:otherwise>N</xsl:otherwise>
    </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="user:formatStringPadded(string($InsuredIsOwner), ' ', 1)"/>

    <!-- DEDUCTIBLE AMOUNT, 6, 116-121 -->
    <xsl:value-of select="user:formatDeductible(number(./Coverage/@Deductible), 4, 2)"/>

    <!-- Unused, 159, 122-280 -->
    <xsl:value-of select="user:space(159)"/>

    <!-- VEHICLE VIN NUMBER, 17, 281-297 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Vehicle, 'Vin', ' ', 17)"/>

    <!-- Unused, 2, 298-299 -->
    <xsl:value-of select="user:space(2)"/>

    <!-- VEHICLE MAKE, 4, 300-303 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Vehicle, 'Make', ' ', 4)"/>

    <!-- VEHICLE MODEL, 50, 304-353 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Vehicle, 'Model', ' ', 50)"/>

    <!-- VEHICLE BODY STYLE, 8, 354-361 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Vehicle, 'BodyStyle', ' ', 8)"/>

    <!-- VEHICLE COLOR, 10, 362-371 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Vehicle, 'Color', ' ', 10)"/>

    <!-- Unused, 4, 372-375 -->
    <xsl:value-of select="user:space(4)"/>

    <!-- VEHICLE MILEAGE, 8, 376-383 -->
    <xsl:choose>
       <xsl:when test="number(Vehicle/@Mileage) &gt; 0"><xsl:value-of select="user:formatNumberPadded(number(Vehicle/@Mileage), '0', 8)"/></xsl:when>
       <xsl:otherwise><xsl:value-of select="user:space(8)"/></xsl:otherwise>
    </xsl:choose>

    <!-- VEHICLE LICENSE PLATE NUMBER, 8, 384-391 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Vehicle, 'LicensePlateNo', ' ', 8)"/>

    <!-- VEHICLE STATE OF LICENSE, 2, 392-393 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Vehicle, 'LicensePlateState', ' ', 2)"/>

    <!-- PRIMARY IMPACT AREA, 2, 394-395 -->
    <xsl:value-of select="user:space(2)"/>

    <!-- SECONDARY IMPACT AREA, 2, 396-397 -->
    <xsl:value-of select="user:space(2)"/>

    <!-- DRIVE-IN/FIELD/OTHER INDICATOR, 1, 398 -->
    <xsl:value-of select="user:formatStringPadded('N', ' ', 1)"/>

    <!-- DRIVABLE INDICATOR, 1, 399 -->
    <xsl:choose>
      <xsl:when test="Vehicle/@Driveable = '1'">Y</xsl:when>
      <xsl:otherwise>N</xsl:otherwise>
    </xsl:choose>

    <!-- VEHICLE LOCATION ADDRESS 1, 40, 400-439 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Vehicle, 'LocationAddress1', ' ', 40)"/>

    <!-- VEHICLE LOCATION CITY, 20, 440-459 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Vehicle, 'LocationCity', ' ', 20)"/>

    <!-- VEHICLE LOCATION STATE, 2 , 460-461 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Vehicle, 'LocationState', ' ', 2)"/>

    <!-- VEHICLE LOCATION ZIP, 9, 462-470 -->
    <xsl:value-of select="user:formatZip(string(./Vehicle/@LocationZip), 9)"/>

    <!-- VEHICLE LOCATION PHONE, 14, 471-484 -->
    <xsl:value-of select="user:formatStringPadded(concat(string(Vehicle/@LocationPhone), string(Vehicle/@LocationPhoneExtension)), ' ', 14)"/>

    <!-- INSTRUCTIONS TO ESTIMATOR 1, 66, 485-550 -->
    <!-- INSTRUCTIONS TO ESTIMATOR 2, 66, 551-616 -->
    <!-- INSTRUCTIONS TO ESTIMATOR 3, 66, 617-682 -->
    <!-- INSTRUCTIONS TO ESTIMATOR 4 , 66, 683-748 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Vehicle, 'ShopRemarks', ' ', (66+66+66+66))"/>

    <!-- Unused, 6, 749-754 -->
    <xsl:value-of select="user:space(6)"/>

    <!-- APPOINTMENT TIME , 4, 755-758 -->
    <xsl:value-of select="user:space(4)"/>

    <!-- Unused, 6, 759-764 -->
    <xsl:value-of select="user:space(6)"/>

    <!-- CCC LAYOUT VERSION NUMBER, 2, 765-766 -->
    <xsl:value-of select="user:formatStringPadded('AF', ' ', 2)"/>

    <!-- Unused, 2, 767-768 -->
    <xsl:value-of select="user:space(2)"/>

    <!-- FROM OFFICE ID, 6, 769-774 -->
    <xsl:value-of select="user:space(6)"/>

    <!-- ASSIGNMENT TYPE, 1, 775 -->
    <xsl:value-of select="user:formatStringPadded('E', ' ', 1)"/>

    <!-- EXTERNAL ASSIGNED TO LOCATION, 7, 776-782 -->
    <xsl:value-of select="user:space(7)"/>

    <!-- EXTERNAL ASSIGNED TO PERSON CODE, 7, 783-789 -->
    <xsl:value-of select="user:space(7)"/>

    <!-- SPECIAL NOTICE NUMBER, 2, 790-791 -->
    <xsl:value-of select="user:space(2)"/>

    <!-- CCC STANDARD TYPE OF LOSS CODE, 1, 792 -->
    <xsl:value-of select="user:formatStringPadded(string($TypeOfLossCode), ' ', 1)"/>

    <!-- CLAIM UNIT, 4, 793-796 -->
    <xsl:value-of select="user:space(4)"/>

    <!-- EXTERNAL LOCATION ALIAS, 15, 797-811 -->
    <xsl:value-of select="user:getFieldDataFormatted(., 'DeliveryAddress', ' ', 15)"/>

    <!-- ASSIGNED BY NAME, 26, 812-837 -->
    <xsl:value-of select="user:space(26)"/>

    <!-- ADJUSTER NAME, 26, 838-863 -->
    <xsl:value-of select="user:formatStringPadded(concat(Claim/@CarrierRepNameLast, ', ', Claim/@CarrierRepNameFirst), ' ', 26)"/>

    <!-- IMPACT NOTES, 65, 864-928 -->
    <xsl:value-of select="user:formatStringPadded(string(Claim/@LossDescription), ' ', 65)"/>

    <!-- POLICY NUMBER EXTENSION, 3, 929-931 -->
    <xsl:value-of select="user:space(3)"/>

    <!-- TOTAL LOSS FLAG, 1, 932 -->
    <xsl:value-of select="user:formatStringPadded('N', ' ', 1)"/>

    <!-- RENTAL CAR FLAG, 1, 933 -->
    <xsl:value-of select="user:formatStringPadded('N', ' ', 1)"/>

    <!-- VEHICLE LOCATION ADDRESS 2, 40, 934-973 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Vehicle, 'LocationAddress2', ' ', 40)"/>

    <!-- EXTERNAL ASSIGNMENT LOG NUM, 5, 974-978 -->
    <xsl:value-of select="user:space(5)"/>

    <!-- TYPE OF CLAIM, 2, 979-980 -->
    <xsl:value-of select="user:formatStringPadded('VE', ' ', 2)"/>

    <!-- ASSOCIATED NOTES FILENAME, 8, 981-988 -->
    <xsl:value-of select="user:space(8)"/>

    <!-- DATE ASSIGNED, 8, 989-996 -->
    <xsl:value-of select="user:getFieldDateFormatted(., 'CurrentDateTime', 'mmddyyyy')"/>

    <!-- DEDUCTIBLE TYPE, 1, 997 -->
    <xsl:value-of select="user:space(1)"/>

    <!-- ECHO FIELD, 50, 998-1047 -->
    <xsl:variable name="myAssignmentCode">
      <xsl:choose>
        <xsl:when test="@AssignmentCode = 'New'">N</xsl:when>
        <xsl:when test="@AssignmentCode = 'Cancel'">C</xsl:when>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="user:formatStringPadded(concat(string(@LynxId), '-', string(@VehicleNumber), '-', string(@AssignmentID), '-', $myAssignmentCode), ' ', 50)"/>

    <!-- ASSIGNMENT MAINTENANCE, 1, 1048 -->
    <xsl:variable name="myActionCode">
      <xsl:choose>
        <xsl:when test="@AssignmentCode = 'New'">A</xsl:when>
        <xsl:when test="@AssignmentCode = 'Cancel'">X</xsl:when>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="user:formatStringPadded(string($myActionCode), ' ', 1)"/>

    <!-- CATASTROPHE CODE, 2, 1049-1050 -->
    <xsl:value-of select="user:space(2)"/>

    <!-- 3RD PARTY INSURANCE CARRIER, 40, 1051-1090 -->
    <xsl:value-of select="user:space(40)"/>

    <!-- 3RD PARTY POLICY NUMBER, 25, 1091-1115 -->
    <xsl:value-of select="user:space(25)"/>

    <!-- SECONDARY FILE ID, 25, 1116-1140 -->
    <xsl:value-of select="user:space(25)"/>

    <!-- SECONDARY BRANCH CODE, 5, 1141-1145 -->
    <xsl:value-of select="user:space(5)"/>

    <!-- COVERAGE INDICATOR, 3, 1146-1148 -->
    <xsl:value-of select="user:formatStringPadded(string($TypeOfLossCode), ' ', 3)"/>

    <!-- POLICY EXPIRATION DATE, 8, 1149-1156 -->
    <xsl:value-of select="user:space(8)"/>

    <!-- OKAY TO PAY INDICATOR, 1, 1157 -->
    <xsl:value-of select="user:space(1)"/>

    <!-- LICENSE PLATE EXPIRATION DATE, 6, 1158-1163 -->
    <xsl:value-of select="user:space(6)"/>

    <!-- ZIP VEHICLE PRINCIPALLY GARAGED, 9, 1164-1172 -->
    <xsl:value-of select="user:space(9)"/>

    <!-- INSPECTION REQUIREMENTS, 35, 1173-1207 -->
    <xsl:value-of select="user:space(35)"/>

    <!-- INJURY FLAG, 1, 1208 -->
    <xsl:value-of select="user:formatStringPadded('U', ' ', 1)"/>

    <!-- UNDERWRITING COMPANY NAME, 60, 1209-1268 -->
    <xsl:value-of select="user:formatStringPadded(string(Claim/@InsuranceCompany), ' ', 60)"/>

    <!-- AGENT NAME, 40, 1269-1308 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Claim, 'AgentName', ' ', 40)"/>

    <!-- AGENT PHONE, 14, 1309-1322 -->
    <xsl:value-of select="user:space(14)"/>

    <!-- OPERATOR’S DRIVERS LICENSE NBR, 20, 1323-1342 -->
    <xsl:value-of select="user:space(20)"/>

    <!-- OPERATOR’S DRIVER’S LICENSE STATE, 2, 1343-1344 -->
    <xsl:value-of select="user:space(2)"/>

    <!-- OPERATOR’S SOCIAL SECURITY NBR, 9, 1345-1353 -->
    <xsl:value-of select="user:space(9)"/>

    <!-- PLACE OF LOSS, 80, 1354-1433 -->
    <xsl:value-of select="user:space(80)"/>

    <!-- STATE OF LOSS, 2, 1434-1435 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Claim, 'LossState', ' ', 2)"/>

    <!-- PRIOR DAMAGE TEXT, 80, 1436-1515 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Vehicle, 'PointOfImpactUnrelatedPrior', ' ', 80)"/>

    <!-- SUBSTITUTE TRANSPORT DAILY COST, 3, 1516-1518 -->
    <xsl:value-of select="user:space(3)"/>

    <!-- VEHICLE LOCATION INDICATOR, 1, 1519 -->
    <xsl:value-of select="user:space(1)"/>

    <!-- FROM COMPANY CODE, 4, 1520-1523 -->
    <xsl:value-of select="user:formatStringPadded('7D', ' ', 4)"/>

    <!-- ASSIGNMENT COMPANY CODE, 4, 1524-1527 -->
    <xsl:value-of select="user:space(4)"/>

    <!-- DATE REPORTED, 8, 1528-1535 -->
    <xsl:value-of select="user:getFieldDateFormatted(./Claim, 'NoticeDate', 'mmddyyyy')"/>

    <!-- DATE OF LOSS, 8, 1536-1543 -->
    <xsl:value-of select="user:getFieldDateFormatted(./Claim, 'LossDate', 'mmddyyyy')"/>

    <!-- VEHICLE YEAR, 4, 1544-1547 -->
    <xsl:value-of select="user:getFieldDataFormatted(./Vehicle, 'VehicleYear', ' ', 4)"/>

    <!-- PRODUCTION DATE, 6, 1548-1553 -->
    <xsl:value-of select="user:space(6)"/>

    <!-- APPOINTMENT DATE, 8, 1554-1561 -->
    <xsl:value-of select="user:space(8)"/>

    <!-- Unused, 108, 1562-1669 -->
    <xsl:value-of select="user:space(108)"/>

    <!-- Defining the table record sizes -->
    <xsl:variable name="nameMaxRecords">15</xsl:variable>
    <xsl:variable name="nameRecordSize">43</xsl:variable>
    <xsl:variable name="addressMaxRecords">10</xsl:variable>
    <xsl:variable name="addressRecordSize">114</xsl:variable>
    <xsl:variable name="phoneMaxRecords">25</xsl:variable>
    <xsl:variable name="phoneRecordSize">17</xsl:variable>

    <!-- Maximum Name records = 15 -->
    <xsl:variable name="nameCount">
    <xsl:choose>
       <xsl:when test="Vehicle/@Party = '3'">2</xsl:when>
       <xsl:otherwise>
           <xsl:choose>
               <xsl:when test="$InsuredIsOwner != 'Y'">2</xsl:when>
               <xsl:otherwise>1</xsl:otherwise>
           </xsl:choose>
       </xsl:otherwise>
    </xsl:choose>
    </xsl:variable>
    <!-- NUMBER OF NAME TBL OCCURRENCES, 3, 1670-1672 -->
    <xsl:value-of select="user:formatNumberPadded(number($nameCount), '0', 3)"/>

    <!-- Maximum Name records = 10 -->
    <xsl:variable name="addressCount">
    <xsl:choose>
       <xsl:when test="Vehicle/@Party = '3'">2</xsl:when>
       <xsl:otherwise>1</xsl:otherwise>
    </xsl:choose>
    </xsl:variable>
    <!-- NUMBER OF ADDRESS TBL OCCURRENCES, 3, 1673-1675 -->
    <xsl:value-of select="user:formatNumberPadded(number($addressCount), '0', 3)"/>

    <!-- Maximum Name records = 25 -->
    <xsl:variable name="insuredPhoneTab">
       <xsl:if test="normalize-space(Claim/@InsuredDayPhone) != ''">1</xsl:if>
       <xsl:if test="normalize-space(Claim/@InsuredNightPhone) != ''">1</xsl:if>
       <xsl:if test="normalize-space(Claim/@InsuredAltPhone) != ''">1</xsl:if>
    </xsl:variable>

    <xsl:variable name="ownerPhoneTab">
       <!-- Owner phone information will be sent only for 3rd party vehicle -->
       <xsl:if test="Vehicle/@Party='3'">
           <xsl:if test="normalize-space(Owner/@OwnerDayPhone) != ''">1</xsl:if>
           <xsl:if test="normalize-space(Owner/@OwnerNightPhone) != ''">1</xsl:if>
           <xsl:if test="normalize-space(Owner/@OwnerAlternatePhone) != ''">1</xsl:if>
       </xsl:if>
    </xsl:variable>
    
   <xsl:choose>
      <xsl:when test="number(string-length($insuredPhoneTab) + string-length($ownerPhoneTab)) &gt; 0">
        <!-- NUMBER OF PHONE TBL OCCURRENCES, 3, 1676-1678 -->
        <xsl:value-of select="user:formatNumberPadded(number(string-length($insuredPhoneTab) + string-length($ownerPhoneTab)), '0', 3)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>000</xsl:text>
      </xsl:otherwise>
   </xsl:choose>


    <!-- NAME TABLE, ,  -->
    <xsl:call-template name="tblName">
        <xsl:with-param name="InsuredIsOwner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
    </xsl:call-template>

    <!-- ADDRESS TABLE, ,  -->
    <xsl:call-template name="tblAddress">
        <xsl:with-param name="InsuredIsOwner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
    </xsl:call-template>

    <!-- PHONE TABLE, ,  -->
    <xsl:call-template name="tblPhone">
        <xsl:with-param name="InsuredIsOwner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
    </xsl:call-template>
    

    <!-- Now pad the remainder of the Name records -->
    <!-- <xsl:value-of select="(number($nameMaxRecords) - number($nameCount)) * number($nameRecordSize)"/> -->
    <xsl:value-of select="user:space((number($nameMaxRecords) - number($nameCount)) * number($nameRecordSize))"/>

    <!-- Now pad the remainder of the Address records -->
    <!-- <xsl:value-of select="(number($addressMaxRecords) - number($addressCount)) * number($addressRecordSize)"/> -->
    <xsl:value-of select="user:space((number($addressMaxRecords) - number($addressCount)) * number($addressRecordSize))"/>

    <!-- Now pad the remainder of the Phone records -->
    <!-- <xsl:value-of select="(number($phoneMaxRecords) - number(string-length($insuredPhoneTab)) - number(string-length($ownerPhoneTab))) * number($phoneRecordSize)"/> -->
    <xsl:value-of select="user:space((number($phoneMaxRecords) - number(string-length($insuredPhoneTab)) - number(string-length($ownerPhoneTab))) * number($phoneRecordSize))"/>

</xsl:template>

<xsl:template name="tblName">
    <xsl:param name="InsuredIsOwner"/>
    <xsl:choose>
        <xsl:when test="Vehicle/@Party='3'">
            <!-- Write the Insured table -->
            <xsl:call-template name="NameRecord">
                <xsl:with-param name="Party">1</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
                <xsl:with-param name="FirstName"><xsl:value-of select="Claim/@InsuredFirstName"/></xsl:with-param>
                <xsl:with-param name="LastName"><xsl:value-of select="Claim/@InsuredLastName"/></xsl:with-param>
                <xsl:with-param name="BusinessName"><xsl:value-of select="Claim/@InsuredBusinessName"/></xsl:with-param>
            </xsl:call-template>

            <!-- Write the 3rd party table -->
            <xsl:call-template name="NameRecord">
                <xsl:with-param name="Party">3</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner">N</xsl:with-param>
                <xsl:with-param name="FirstName"><xsl:value-of select="Owner/@OwnerFirstName"/></xsl:with-param>
                <xsl:with-param name="LastName"><xsl:value-of select="Owner/@OwnerLastName"/></xsl:with-param>
                <xsl:with-param name="BusinessName"><xsl:value-of select="Owner/@OwnerBusinessName"/></xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <!-- Write the Insured table -->
            <xsl:call-template name="NameRecord">
                <xsl:with-param name="Party">1</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
                <xsl:with-param name="FirstName"><xsl:value-of select="Claim/@InsuredFirstName"/></xsl:with-param>
                <xsl:with-param name="LastName"><xsl:value-of select="Claim/@InsuredLastName"/></xsl:with-param>
                <xsl:with-param name="BusinessName"><xsl:value-of select="Claim/@InsuredBusinessName"/></xsl:with-param>
            </xsl:call-template>
            <xsl:if test="$InsuredIsOwner != 'Y'">
                <xsl:call-template name="NameRecord">
                    <xsl:with-param name="Party">3</xsl:with-param>
                    <xsl:with-param name="InsuredIsOwner">N</xsl:with-param>
                    <xsl:with-param name="FirstName"><xsl:value-of select="Owner/@OwnerFirstName"/></xsl:with-param>
                    <xsl:with-param name="LastName"><xsl:value-of select="Owner/@OwnerLastName"/></xsl:with-param>
                    <xsl:with-param name="BusinessName"><xsl:value-of select="Owner/@OwnerBusinessName"/></xsl:with-param>
                </xsl:call-template>
            </xsl:if>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="NameRecord">
    <xsl:param name="Party"/>
    <xsl:param name="InsuredIsOwner"/>
    <xsl:param name="FirstName"/>
    <xsl:param name="LastName"/>
    <xsl:param name="BusinessName"/>
    <!-- INDICATOR, 2, n/a -->
    <xsl:choose>
        <xsl:when test="$Party = '1' and $InsuredIsOwner = 'Y'">
            <xsl:value-of select="user:formatStringPadded('VI', ' ', 2)"/>
        </xsl:when>
        <xsl:when test="$Party = '1'">
            <xsl:value-of select="user:formatStringPadded('IN', ' ', 2)"/>
        </xsl:when>
        <xsl:when test="$Party = '3'">
            <xsl:value-of select="user:formatStringPadded('VO', ' ', 2)"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="user:formatStringPadded('OT', ' ', 2)"/>
        </xsl:otherwise>
    </xsl:choose>

    <!-- TYPE, 1, n/a -->
    <xsl:call-template name="PersonOrBusinessType">
        <xsl:with-param name="BusinessName"><xsl:value-of select="$BusinessName"/></xsl:with-param>
    </xsl:call-template>

    <!-- NAME, 40, n/a -->
    <xsl:call-template name="PersonOrBusinessName">
        <xsl:with-param name="FirstName"><xsl:value-of select="$FirstName"/></xsl:with-param>
        <xsl:with-param name="LastName"><xsl:value-of select="$LastName"/></xsl:with-param>
        <xsl:with-param name="BusinessName"><xsl:value-of select="$BusinessName"/></xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template name="PersonOrBusinessType">
    <xsl:param name="BusinessName"/>
    <xsl:choose>
        <xsl:when test="normalize-space($BusinessName) != ''">
            <xsl:value-of select="user:formatStringPadded('C', ' ', 1)"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="user:formatStringPadded('P', ' ', 1)"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="PersonOrBusinessName">
    <xsl:param name="FirstName"/>
    <xsl:param name="LastName"/>
    <xsl:param name="BusinessName"/>
    <xsl:choose>
        <xsl:when test="normalize-space($BusinessName) != ''">
            <xsl:value-of select="user:formatStringPadded(string($BusinessName), ' ', 40)"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="user:formatStringPadded(string($FirstName), ' ', 20)"/>
            <xsl:value-of select="user:formatStringPadded(string($LastName), ' ', 20)"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="tblAddress">
    <xsl:param name="InsuredIsOwner"/>
    <xsl:choose>
        <xsl:when test="Vehicle/@Party='3'">
            <!-- Write the Insured table -->
            <xsl:call-template name="AddressRecord">
                <xsl:with-param name="Party">1</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
                <xsl:with-param name="Address1"><xsl:value-of select="Claim/@InsuredAddress1"/></xsl:with-param>
                <xsl:with-param name="Address2"><xsl:value-of select="Claim/@InsuredAddress2"/></xsl:with-param>
                <xsl:with-param name="City"><xsl:value-of select="Claim/@InsuredAddressCity"/></xsl:with-param>
                <xsl:with-param name="State"><xsl:value-of select="Claim/@InsuredAddressState"/></xsl:with-param>
                <xsl:with-param name="Zip"><xsl:value-of select="Claim/@InsuredAddressZip"/></xsl:with-param>
            </xsl:call-template>

            <!-- Write the 3rd party table -->
            <xsl:call-template name="AddressRecord">
                <xsl:with-param name="Party">3</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner">N</xsl:with-param>
                <xsl:with-param name="Address1"><xsl:value-of select="Owner/@OwnerAddress1"/></xsl:with-param>
                <xsl:with-param name="Address2"><xsl:value-of select="Owner/@OwnerAddress2"/></xsl:with-param>
                <xsl:with-param name="City"><xsl:value-of select="Owner/@OwnerCity"/></xsl:with-param>
                <xsl:with-param name="State"><xsl:value-of select="Owner/@OwnerState"/></xsl:with-param>
                <xsl:with-param name="Zip"><xsl:value-of select="Owner/@OwnerZip"/></xsl:with-param>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <!-- Write the Insured table -->
            <xsl:call-template name="AddressRecord">
                <xsl:with-param name="Party">1</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
                <xsl:with-param name="Address1"><xsl:value-of select="Claim/@InsuredAddress1"/></xsl:with-param>
                <xsl:with-param name="Address2"><xsl:value-of select="Claim/@InsuredAddress2"/></xsl:with-param>
                <xsl:with-param name="City"><xsl:value-of select="Claim/@InsuredAddressCity"/></xsl:with-param>
                <xsl:with-param name="State"><xsl:value-of select="Claim/@InsuredAddressState"/></xsl:with-param>
                <xsl:with-param name="Zip"><xsl:value-of select="Claim/@InsuredAddressZip"/></xsl:with-param>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="AddressRecord">
    <xsl:param name="Party"/>
    <xsl:param name="InsuredIsOwner"/>
    <xsl:param name="Address1"/>
    <xsl:param name="Address2"/>
    <xsl:param name="City"/>
    <xsl:param name="State"/>
    <xsl:param name="Zip"/>

    <!-- INDICATOR, 2, n/a -->
    <xsl:choose>
        <xsl:when test="$Party = '1' and $InsuredIsOwner = 'Y'">
            <xsl:value-of select="user:formatStringPadded('VI', ' ', 2)"/>
        </xsl:when>
        <xsl:when test="$Party = '1'">
            <xsl:value-of select="user:formatStringPadded('IN', ' ', 2)"/>
        </xsl:when>
        <xsl:when test="$Party = '3'">
            <xsl:value-of select="user:formatStringPadded('VO', ' ', 2)"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="user:formatStringPadded('OT', ' ', 2)"/>
        </xsl:otherwise>
    </xsl:choose>

    <!-- TYPE, 1, n/a -->
    <xsl:value-of select="user:formatStringPadded('H', ' ', 1)"/>
    
    <!-- STREET ADDRESS 1, 40, n/a -->
    <xsl:value-of select="user:formatStringPadded(string($Address1), ' ', 40)"/>

    <!-- STREET ADDRESS 2, 40, n/a -->
    <xsl:value-of select="user:formatStringPadded(string($Address2), ' ', 40)"/>

    <!-- CITY, 20, n/a -->
    <xsl:value-of select="user:formatStringPadded(string($City), ' ', 20)"/>

    <!-- STATE, 2, n/a -->
    <xsl:value-of select="user:formatStringPadded(string($State), ' ', 2)"/>

    <!-- ZIP, 9, n/a -->
    <xsl:value-of select="user:formatZip(string($Zip), 9)"/>
</xsl:template>

<xsl:template name="tblPhone">
    <xsl:param name="InsuredIsOwner"/>
    <xsl:choose>
        <xsl:when test="Vehicle/@Party='3'">
            <!-- Write the Insured table -->
            <xsl:if test="Claim/@InsuredDayPhone != ''">
            <xsl:call-template name="PhoneRecord">
                <xsl:with-param name="Party">1</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
                <xsl:with-param name="Type">D</xsl:with-param>
                <xsl:with-param name="Phone"><xsl:value-of select="Claim/@InsuredDayPhone"/></xsl:with-param>
                <xsl:with-param name="PhoneExtension"><xsl:value-of select="Claim/@InsuredDayPhoneExt"/></xsl:with-param>
            </xsl:call-template>
            </xsl:if>

            <xsl:if test="Claim/@InsuredNightPhone != ''">
            <xsl:call-template name="PhoneRecord">
                <xsl:with-param name="Party">1</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
                <xsl:with-param name="Type">E</xsl:with-param>
                <xsl:with-param name="Phone"><xsl:value-of select="Claim/@InsuredNightPhone"/></xsl:with-param>
                <xsl:with-param name="PhoneExtension"><xsl:value-of select="Claim/@InsuredNightPhoneExt"/></xsl:with-param>
            </xsl:call-template>
            </xsl:if>

            <xsl:if test="Claim/@InsuredAltPhone != ''">
            <xsl:call-template name="PhoneRecord">
                <xsl:with-param name="Party">1</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
                <xsl:with-param name="Type">O</xsl:with-param>
                <xsl:with-param name="Phone"><xsl:value-of select="Claim/@InsuredAltPhone"/></xsl:with-param>
                <xsl:with-param name="PhoneExtension"><xsl:value-of select="Claim/@InsuredAltPhoneExt"/></xsl:with-param>
            </xsl:call-template>
            </xsl:if>

            <!-- Write the 3rd party table -->
            <xsl:if test="Owner/@OwnerDayPhone != ''">
            <xsl:call-template name="PhoneRecord">
                <xsl:with-param name="Party">3</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner">N</xsl:with-param>
                <xsl:with-param name="Type">D</xsl:with-param>
                <xsl:with-param name="Phone"><xsl:value-of select="Owner/@OwnerDayPhone"/></xsl:with-param>
                <xsl:with-param name="PhoneExtension"><xsl:value-of select="Owner/@OwnerDayPhoneExtension"/></xsl:with-param>
            </xsl:call-template>
            </xsl:if>

            <xsl:if test="Owner/@OwnerNightPhone != ''">
            <xsl:call-template name="PhoneRecord">
                <xsl:with-param name="Party">3</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner">N</xsl:with-param>
                <xsl:with-param name="Type">E</xsl:with-param>
                <xsl:with-param name="Phone"><xsl:value-of select="Owner/@OwnerNightPhone"/></xsl:with-param>
                <xsl:with-param name="PhoneExtension"><xsl:value-of select="Owner/@OwnerNightPhoneExtension"/></xsl:with-param>
            </xsl:call-template>
            </xsl:if>

            <xsl:if test="Owner/@OwnerAlternatePhone != ''">
            <xsl:call-template name="PhoneRecord">
                <xsl:with-param name="Party">3</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner">N</xsl:with-param>
                <xsl:with-param name="Type">O</xsl:with-param>
                <xsl:with-param name="Phone"><xsl:value-of select="Owner/@OwnerAlternatePhone"/></xsl:with-param>
                <xsl:with-param name="PhoneExtension"><xsl:value-of select="Owner/@OwnerAlternatePhoneExtension"/></xsl:with-param>
            </xsl:call-template>
            </xsl:if>
        </xsl:when>
        <xsl:otherwise>
            <!-- Write the Insured table -->
            <xsl:if test="Claim/@InsuredDayPhone != ''">
            <xsl:call-template name="PhoneRecord">
                <xsl:with-param name="Party">1</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
                <xsl:with-param name="Type">D</xsl:with-param>
                <xsl:with-param name="Phone"><xsl:value-of select="Claim/@InsuredDayPhone"/></xsl:with-param>
                <xsl:with-param name="PhoneExtension"><xsl:value-of select="Claim/@InsuredDayPhoneExt"/></xsl:with-param>
            </xsl:call-template>
            </xsl:if>

            <xsl:if test="Claim/@InsuredNightPhone != ''">
            <xsl:call-template name="PhoneRecord">
                <xsl:with-param name="Party">1</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
                <xsl:with-param name="Type">E</xsl:with-param>
                <xsl:with-param name="Phone"><xsl:value-of select="Claim/@InsuredNightPhone"/></xsl:with-param>
                <xsl:with-param name="PhoneExtension"><xsl:value-of select="Claim/@InsuredNightPhoneExt"/></xsl:with-param>
            </xsl:call-template>
            </xsl:if>

            <xsl:if test="Claim/@InsuredAltPhone != ''">
            <xsl:call-template name="PhoneRecord">
                <xsl:with-param name="Party">1</xsl:with-param>
                <xsl:with-param name="InsuredIsOwner"><xsl:value-of select="$InsuredIsOwner"/></xsl:with-param>
                <xsl:with-param name="Type">O</xsl:with-param>
                <xsl:with-param name="Phone"><xsl:value-of select="Claim/@InsuredAltPhone"/></xsl:with-param>
                <xsl:with-param name="PhoneExtension"><xsl:value-of select="Claim/@InsuredAltPhoneExt"/></xsl:with-param>
            </xsl:call-template>
            </xsl:if>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="PhoneRecord">
    <xsl:param name="Party"/>
    <xsl:param name="InsuredIsOwner"/>
    <xsl:param name="Type"/>
    <xsl:param name="Phone"/>
    <xsl:param name="PhoneExtension"/>

    <!-- INDICATOR, 2, n/a -->
    <xsl:choose>
        <xsl:when test="$Party = '1' and $InsuredIsOwner = 'Y'">
            <xsl:value-of select="user:formatStringPadded('VI', ' ', 2)"/>
        </xsl:when>
        <xsl:when test="$Party = '1'">
            <xsl:value-of select="user:formatStringPadded('IN', ' ', 2)"/>
        </xsl:when>
        <xsl:when test="$Party = '3'">
            <xsl:value-of select="user:formatStringPadded('VO', ' ', 2)"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="user:formatStringPadded('OT', ' ', 2)"/>
        </xsl:otherwise>
    </xsl:choose>

    <!-- TYPE, 1, n/a -->
    <xsl:value-of select="user:formatStringPadded(string($Type), ' ', 1)"/>

    <!-- PHONE NUMBER, 14, n/a -->
    <xsl:value-of select="user:formatStringPadded(concat($Phone, $PhoneExtension), ' ', 14)"/>
</xsl:template>

</xsl:stylesheet>