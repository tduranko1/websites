<?xml version="1.0" encoding="UTF-8"?>
<!-- ========================================================== -->
<!-- CommEvents.xsl                                             -->
<!--                                                            -->
<!-- This style sheet is used to map CommServer entity          -->
<!-- data to WorkFlow Event SP calls.  It allows us to          -->
<!-- eliminate a bunch of custom code in CommServer.            -->
<!--                                                            -->
<!-- ========================================================== -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" version="1.0" encoding="UTF-8" indent="yes"/>

    <!-- Parmaters passed in -->
    <xsl:param name="Event">NullEvent</xsl:param>
    <!-- <xsl:param name="Event">FaxCancelAssignmentSucceeded</xsl:param> -->

    <!-- ================================================== -->
    <!-- Assignment                                         -->
    <!--                                                    -->
    <!-- This template builds up the WorkFlow Event SP call -->
    <!-- based upon the Assignment XML we are styling and   -->
    <!-- the Event parameter passed in                      -->
    <!--                                                    -->
    <!-- ================================================== -->
    <xsl:template match="/Assignment">

        <!-- Event Description -->
        <xsl:choose>

            <!-- Fax Failed -->
            <xsl:when test="$Event='FaxAssignmentFailed'">
                <xsl:text>Error sending fax!</xsl:text>
            </xsl:when>

            <!-- Fax Succeeded -->
            <xsl:when test="$Event='FaxAssignmentSucceeded'">
                <xsl:call-template name="ShopType">
                    <xsl:with-param name="ShopProgramFlag">
                        <xsl:value-of select="Shop/@ShopProgramFlag"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:text disable-output-escaping="yes"> (FAX): </xsl:text>
                <xsl:call-template name="AssignmentType">
                    <xsl:with-param name="ShopProgramFlag">
                        <xsl:value-of select="Shop/@ShopProgramFlag"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:text disable-output-escaping="yes">fax for Vehicle </xsl:text>
                <xsl:value-of select="@VehicleNumber"/>
                <xsl:text disable-output-escaping="yes"> was sent to </xsl:text>
                <xsl:value-of select="Shop/@ShopName"/>
                <xsl:text disable-output-escaping="yes"> (Shop ID </xsl:text>
                <xsl:value-of select="Shop/@ShopLocationID"/>
                <xsl:text disable-output-escaping="yes">, FAX </xsl:text>
                <xsl:value-of select="Shop/@ShopFax"/>
                <xsl:text disable-output-escaping="yes">).</xsl:text>
            </xsl:when>

            <!-- Cancel Assignment Fax Succeeded -->
            <xsl:when test="$Event='FaxCancelAssignmentSucceeded'">
                <xsl:call-template name="ShopType">
                    <xsl:with-param name="ShopProgramFlag">
                        <xsl:value-of select="Shop/@ShopProgramFlag"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:text disable-output-escaping="yes"> (FAX): Cancellation </xsl:text>
                <xsl:call-template name="AssignmentType">
                    <xsl:with-param name="ShopProgramFlag">
                        <xsl:value-of select="Shop/@ShopProgramFlag"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:text disable-output-escaping="yes">fax for Vehicle </xsl:text>
                <xsl:value-of select="@VehicleNumber"/>
                <xsl:text disable-output-escaping="yes"> was sent to </xsl:text>
                <xsl:value-of select="Shop/@ShopName"/>
                <xsl:text disable-output-escaping="yes"> (Shop ID </xsl:text>
                <xsl:value-of select="Shop/@ShopLocationID"/>
                <xsl:text disable-output-escaping="yes">, FAX </xsl:text>
                <xsl:value-of select="Shop/@ShopFax"/>
                <xsl:text disable-output-escaping="yes">).</xsl:text>
            </xsl:when>

            <!-- Electronic Assignment HTTP Post to PPG failed -->
            <xsl:when test="$Event='VehicleAssignmentFailureAtGO'">
                <xsl:text disable-output-escaping="yes">Error sending assignment to </xsl:text>
                <xsl:value-of select="@DeliveryMethod"/>
            </xsl:when>

            <!-- Receipt received from CCC for an assignment -->
            <xsl:when test="$Event='VehicleAssignmentSentToShop'">
                <xsl:call-template name="ShopType">
                    <xsl:with-param name="ShopProgramFlag">
                        <xsl:value-of select="Shop/@ShopProgramFlag"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:text disable-output-escaping="yes"> (</xsl:text>
                <xsl:value-of select="@DeliveryMethod"/>
                <xsl:text disable-output-escaping="yes">): </xsl:text>
                <xsl:call-template name="AssignmentType">
                    <xsl:with-param name="ShopProgramFlag">
                        <xsl:value-of select="Shop/@ShopProgramFlag"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:text disable-output-escaping="yes">for Vehicle </xsl:text>
                <xsl:value-of select="@VehicleNumber"/>
                <xsl:text disable-output-escaping="yes"> was sent to </xsl:text>
                <xsl:value-of select="Shop/@ShopName"/>
                <xsl:text disable-output-escaping="yes"> (Shop ID </xsl:text>
                <xsl:value-of select="Shop/@ShopLocationID"/>
                <xsl:text disable-output-escaping="yes">).</xsl:text>
            </xsl:when>
            
            <!-- Electronic Assignment sent via Pathways directory interface -->
            <xsl:when test="($Event='VehicleAssignmentSentToAutoTech')">
                <xsl:text disable-output-escaping="yes">LDAU</xsl:text>
                <xsl:text disable-output-escaping="yes">: </xsl:text>
                <xsl:text disable-output-escaping="yes">Assignment for Vehicle </xsl:text>
                <xsl:value-of select="@VehicleNumber"/>
                <xsl:text disable-output-escaping="yes"> was sent to LYNX Auto-tech.</xsl:text>
            </xsl:when>

            <!-- Unknown event type! -->
            <xsl:otherwise>Unknown Event</xsl:otherwise>

        </xsl:choose>

    </xsl:template>

    <!-- ================================================== -->
    <!-- Outputs an assignment type based upon the passed   -->
    <!-- program shop flag parameter.                       -->
    <!-- ================================================== -->
    <xsl:template name="AssignmentType">
        <xsl:param name="ShopProgramFlag"/>
        <xsl:choose>
            <xsl:when test="$ShopProgramFlag='0'">
                <xsl:text>Notice of loss </xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>Assignment </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- ================================================== -->
    <!-- Outputs a shop type based upon the passed program  -->
    <!-- shop flag parameter.                               -->
    <!-- ================================================== -->
    <xsl:template name="ShopType">
        <xsl:param name="ShopProgramFlag"/>
        <xsl:choose>
            <xsl:when test="$ShopProgramFlag='0'">
                <xsl:text>NON-PROGRAM SHOP</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>PROGRAM SHOP </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
