<?xml version="1.0" encoding="UTF-8"?>

<!-- ShopAssignmentFax.xsl

    This XSL is used to convert shop assigment fax XML data with business rules.

    We this XSL to replace hardcoded schtuff from our compiled VB COM components.

-->

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:localvb="http://localvb.com/mynamespace">

    <xsl:output method="xml"/>

    <!-- Visual Basic Scripting -->
    <msxsl:script language="VBScript" implements-prefix="localvb">
    <![CDATA[

        Function CurrentDateTime()
            CurrentDateTime = CStr(Now)
        End Function

        Function BuildAddressLine( objNodeList, strEntity, intIdx )
            Dim strValue, objNode

            Set objNode = objNodeList.nextNode()
            strValue = objNode.selectSingleNode("//@" & strEntity & "Address2").Text

            If intIdx = 2 Then
                If Len(strValue) = 0 Then
                    strValue = objNode.selectSingleNode("//@" & strEntity & "City").Text _
                        & ", " & objNode.selectSingleNode("//@" & strEntity & "State").Text _
                        & "  " & objNode.selectSingleNode("//@" & strEntity & "Zip").Text
                End If
            ElseIf Len(strValue) > 0 Then
                strValue = objNode.selectSingleNode("//@" & strEntity & "City").Text _
                    & ", " & objNode.selectSingleNode("//@" & strEntity & "State").Text _
                    & "  " & objNode.selectSingleNode("//@" & strEntity & "Zip").Text
            End If

            BuildAddressLine = strValue
        End Function

    ]]>
    </msxsl:script>

    <!-- Recursively digs down through every element and attribute, starting at the root. -->
    <xsl:template match="/ | @* | node()">

        <xsl:variable name="HasVehicleContact" select="boolean( concat( //@VehicleContactFirstName, //@VehicleContactLastName ) )"/>

        <xsl:choose>

            <!-- Format dates -->
            <xsl:when test="name() = 'LossDate' or name() = 'NoticeDate'">
                <xsl:call-template name="FormatDate"/>
            </xsl:when>

            <!-- Format phone numbers -->
            <xsl:when test="name() = 'RepPhone' or name() = 'RepFax' or name() = 'LocationPhone' or name() = 'ShopPhone' or name() = 'VehicleContactDayPhone' or name() = 'VehicleContactNightPhone' or name() = 'VehicleContactAlternatePhone' or name() = 'InsuredDayPhone' or name() = 'InsuredNightPhone' or name() = 'InsuredAltPhone'">
                <xsl:call-template name="FormatPhone"/>
            </xsl:when>

            <!-- Format shop fax number and add required 'FaxNumber' attribute for LAPDFaxServer -->
            <xsl:when test="name() = 'ShopFax'">
                <xsl:call-template name="FormatPhone"/>
                <xsl:call-template name="FormatPhone">
                    <xsl:with-param name="AttName">FaxNumber</xsl:with-param>
                </xsl:call-template>
            </xsl:when>

            <!-- Add required 'FaxDestinationName' attribute for LAPDFaxServer -->
            <xsl:when test="name() = 'ShopName'">
                <xsl:copy><xsl:apply-templates select="@* | node()"/></xsl:copy>
                <xsl:attribute name="FaxDestinationName"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>

            <!-- Current date -->
            <xsl:when test="name() = 'CurrentDateTime' or name() = 'RepFax' or name() = 'LocationPhone' or name() = 'ShopPhone' or name() = 'ShopFax'">
                <xsl:attribute name="{name()}">
                    <xsl:value-of select="localvb:CurrentDateTime()"/>
                </xsl:attribute>
            </xsl:when>

            <!-- Convert booleans to 'Yes' and 'No' -->
            <xsl:when test="name() = 'Driveable' or name() = 'CoverageConfirmed'">
                <xsl:call-template name="FormatBoolean"/>
            </xsl:when>

            <!-- Total Loss Warning Amount -->
            <xsl:when test="name() = 'TotalLossWarningAmount'">
                <xsl:attribute name="{name()}">
                    <xsl:if test="string-length(.) &gt; 0">
                        <xsl:choose>
					        <!-- Default setting. If the value was provided and is equal to 0 -->
                            <xsl:when test="number(.) = 0">
						        <xsl:text>If your estimate exceeds 70% of NADA value, DO NOT begin repairs.  Call LYNX Services immediately.</xsl:text>  		
				            </xsl:when>
				            <!-- 6/8/07 - the percentage of the book value already precalculated will be suppressed per JP and Business -->
                            <!-- data is already calculated -->
                            <!-- <xsl:when test="ceiling(number(.)) &gt; 1">
                                <xsl:text>If your estimate exceeds $</xsl:text>
                                <xsl:value-of select="round(.)"/>
                                <xsl:text> DO NOT begin repairs.  Call LYNX Services immediately.</xsl:text>
                            </xsl:when> -->
					        <!-- A percentage value was provided. This will be less than 1 (like 0.75 for 75%)-->
                            <xsl:when test="ceiling(number(.)) = 1"> <!-- data is a percentage value  -->
                                <xsl:text>If your estimate exceeds </xsl:text>
                                <xsl:value-of select="number(.) * 100"/>
                                <xsl:text>% of NADA value.  DO NOT begin repairs.  Call LYNX Services immediately.</xsl:text>
                            </xsl:when>
                            <!-- Default setting once again. -->
                            <xsl:otherwise>
                                <xsl:text>If your estimate exceeds 70% of NADA value, DO NOT begin repairs.  Call LYNX Services immediately.</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                </xsl:attribute>
            </xsl:when>

            <!-- Test Track Defect 554

                If a business name is provided for either the vehicle owner or
                the insured, it should be printed on the FAX instead of the name.
                If there is a business name, put it's value into the first name
                field and blank the last name.
            -->
            <xsl:when test="name() = 'OwnerFirstName'">
                <xsl:call-template name="ReplaceWithOther">
                    <xsl:with-param name="Other" select="//@OwnerBusinessName"/>
                </xsl:call-template>
            </xsl:when>

            <xsl:when test="name() = 'OwnerLastName'">
                <xsl:call-template name="BlankIfOther">
                    <xsl:with-param name="Other" select="//@OwnerBusinessName"/>
                </xsl:call-template>
            </xsl:when>

            <xsl:when test="name() = 'InsuredFirstName'">
                <xsl:call-template name="ReplaceWithOther">
                    <xsl:with-param name="Other" select="//@InsuredBusinessName"/>
                </xsl:call-template>
            </xsl:when>

            <xsl:when test="name() = 'InsuredLastName'">
                <xsl:call-template name="BlankIfOther">
                    <xsl:with-param name="Other" select="//@InsuredBusinessName"/>
                </xsl:call-template>
            </xsl:when>

            <!-- Test Track defect 647

                If the vehicle has a vehicle contact name (first and/or last name,
                not business name), use it and the corresponding phone numbers and
                e-mail address instead of the vehicle owner information.
            -->
            <xsl:when test="name() = 'OwnerDayPhoneExtension'">
                <xsl:attribute name="{name()}">
                    <xsl:if test="$HasVehicleContact"><xsl:value-of select="//@VehicleContactDayPhoneExtension"/></xsl:if>
                    <xsl:if test="not($HasVehicleContact)"><xsl:value-of select="."/></xsl:if>
                </xsl:attribute>
            </xsl:when>

            <xsl:when test="name() = 'OwnerNightPhoneExtension'">
                <xsl:attribute name="{name()}">
                    <xsl:if test="$HasVehicleContact"><xsl:value-of select="//@VehicleContactNightPhoneExtension"/></xsl:if>
                    <xsl:if test="not($HasVehicleContact)"><xsl:value-of select="."/></xsl:if>
                </xsl:attribute>
            </xsl:when>

            <xsl:when test="name() = 'OwnerAlternatePhoneExtension'">
                <xsl:attribute name="{name()}">
                    <xsl:if test="$HasVehicleContact"><xsl:value-of select="//@VehicleContactAlternatePhoneExtension"/></xsl:if>
                    <xsl:if test="not($HasVehicleContact)"><xsl:value-of select="."/></xsl:if>
                </xsl:attribute>
            </xsl:when>

            <xsl:when test="name() = 'OwnerEmailAddress'">
                <xsl:attribute name="{name()}">
                    <xsl:if test="$HasVehicleContact"><xsl:value-of select="//@VehicleContactEmailAddress"/></xsl:if>
                    <xsl:if test="not($HasVehicleContact)"><xsl:value-of select="."/></xsl:if>
                </xsl:attribute>
            </xsl:when>

            <xsl:when test="name() = 'OwnerDayPhone'">
                <xsl:if test="$HasVehicleContact"><xsl:call-template name="FormatPhone"><xsl:with-param name="Number" select="//@VehicleContactDayPhone"/></xsl:call-template></xsl:if>
                <xsl:if test="not($HasVehicleContact)"><xsl:call-template name="FormatPhone"/></xsl:if>
            </xsl:when>

            <xsl:when test="name() = 'OwnerNightPhone'">
                <xsl:if test="$HasVehicleContact"><xsl:call-template name="FormatPhone"><xsl:with-param name="Number" select="//@VehicleContactNightPhone"/></xsl:call-template></xsl:if>
                <xsl:if test="not($HasVehicleContact)"><xsl:call-template name="FormatPhone"/></xsl:if>
            </xsl:when>

            <xsl:when test="name() = 'OwnerAlternatePhone'">
                <xsl:if test="$HasVehicleContact"><xsl:call-template name="FormatPhone"><xsl:with-param name="Number" select="//@VehicleContactAlternatePhone"/></xsl:call-template></xsl:if>
                <xsl:if test="not($HasVehicleContact)"><xsl:call-template name="FormatPhone"/></xsl:if>
            </xsl:when>

            <!-- If and address doesn't have a second address line,
                then move the city, state, and zip up to the second address line.
            -->
            <xsl:when test="name() = 'ShopAddress2'">
                <xsl:attribute name="{name()}"><xsl:value-of select="localvb:BuildAddressLine(.,'Shop',2)"/></xsl:attribute>
                <xsl:attribute name="ShopAddress3"><xsl:value-of select="localvb:BuildAddressLine(.,'Shop',3)"/></xsl:attribute>
            </xsl:when>

            <xsl:when test="name() = 'OwnerAddress2'">
                <xsl:attribute name="{name()}"><xsl:value-of select="localvb:BuildAddressLine(.,'Owner',2)"/></xsl:attribute>
                <xsl:attribute name="OwnerAddress3"><xsl:value-of select="localvb:BuildAddressLine(.,'Owner',3)"/></xsl:attribute>
            </xsl:when>

            <xsl:when test="name() = 'LocationAddress2'">
                <xsl:attribute name="{name()}"><xsl:value-of select="localvb:BuildAddressLine(.,'Location',2)"/></xsl:attribute>
                <xsl:attribute name="LocationAddress3"><xsl:value-of select="localvb:BuildAddressLine(.,'Location',3)"/></xsl:attribute>
            </xsl:when>

            <!-- Copy recursion -->
            <xsl:otherwise>
                <xsl:copy><xsl:apply-templates select="@* | node()"/></xsl:copy>
            </xsl:otherwise>

        </xsl:choose>

    </xsl:template>

    <!-- Converts our custom date-time string to a standard formatted date. -->
    <xsl:template name="FormatDate">
        <xsl:attribute name="{name()}">
            <xsl:value-of select="concat( substring(.,6,2), '/', substring(.,9,2), '/', substring(.,1,4) )"/>
        </xsl:attribute>
    </xsl:template>

    <!-- Formats phone numbers -->
    <xsl:template name="FormatPhone">
        <xsl:param name="Number" select="."/>
        <xsl:param name="AttName" select="name()"/>

        <xsl:attribute name="{$AttName}">
            <xsl:choose>
                <xsl:when test="string-length($Number) = 7">
                    <xsl:value-of select="concat( substring($Number,1,3), '-', substring($Number,4,4) )"/>
                </xsl:when>
                <xsl:when test="string-length($Number) = 10">
                    <xsl:value-of select="concat( substring($Number,1,3), '-', substring($Number,4,3), '-', substring($Number,7,4) )"/>
                </xsl:when>
                <xsl:when test="string-length($Number) = 11">
                    <xsl:value-of select="concat( substring($Number,2,3), '-', substring($Number,5,3), '-', substring($Number,8,4) )"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$Number"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
    </xsl:template>

    <!-- Convert booleans to 'Yes' and 'No' -->
    <xsl:template name="FormatBoolean">
        <xsl:attribute name="{name()}">
            <xsl:if test=". = '1'">Yes</xsl:if>
            <xsl:if test=". != '1'">No</xsl:if>
        </xsl:attribute>
    </xsl:template>

    <!-- Replaces the current with Other if Other is not blank -->
    <xsl:template name="ReplaceWithOther">
        <xsl:param name="Other"/>

        <xsl:attribute name="{name()}">
            <xsl:choose>
                <xsl:when test="string-length($Other) = 0">
                    <xsl:value-of select="."/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$Other"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
    </xsl:template>

    <!-- Blanks the current if Other is not blank -->
    <xsl:template name="BlankIfOther">
        <xsl:param name="Other"/>

        <xsl:attribute name="{name()}">
            <xsl:choose>
                <xsl:when test="string-length($Other) = 0">
                    <xsl:value-of select="."/>
                </xsl:when>
            </xsl:choose>
        </xsl:attribute>
    </xsl:template>

</xsl:stylesheet>
