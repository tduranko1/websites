<?xml version="1.0" encoding="UTF-8"?>

<!-- ShopAssignmentValidate.xsl

    This XSL is used to validate XML data with business rules.

    We this XSL to replace hardcoded compiled VB COM components.

    If the XML validates successfully then the result will be a blank string.

    If there are errors then the return string takes the form of an argument
        passable to SiteUtilities.CEvents.HandleMultiEvents.  This means that
        the resulting string must take the form of a '|' delimited list:

    C#|error message[|...]
    C = 'I','E' or 'B' for 'internal', 'external' or 'both'
    # = '1','2',or '4' for 'error', 'warning' or 'information'
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- Turn off that annoying <?xml ... ?> tag at the top of the output -->
  <xsl:output omit-xml-declaration="yes"/>

  <xsl:template match="/Assignment">

    <!-- 1) Shop Fax number must be specified and be 10 digits long. -->
    <xsl:if test="10 > string-length(//@ShopFax)">
      <xsl:text>|E2|The shop must have a fax number specified that is 10 digits long.</xsl:text>
    </xsl:if>

    <!-- 2) Claim rep phone number must be specified and be 10 digits long. -->
    <xsl:if test="10 > string-length(//@RepPhone)">
      <xsl:text>|E2|The claim rep must have a phone number specified that is 10 digits long.</xsl:text>
    </xsl:if>

    <!-- 3) The name of the insured (whether person or business) must be specified. -->
    <xsl:if test="0 = string-length(//@InsuredFirstName)">
      <xsl:if test="0 = string-length(//@InsuredLastName)">
        <xsl:if test="0 = string-length(//@InsuredBusinessName)">
          <xsl:text>|E2|The insured must have a person or business name specified.</xsl:text>
        </xsl:if>
      </xsl:if>
    </xsl:if>

    <!-- 4) Claim number must be specified. -->
    <xsl:if test="0 = string-length(//@ClaimNumber)">
      <xsl:text>|E2|The claim number must be specified.</xsl:text>
    </xsl:if>

    <!-- 5) Coverage Type (for the vehicle) must be specified. -->
    <xsl:if test="0 = string-length(//@CoverageType)">
      <xsl:text>|E2|The vehicle coverage type must be specified.</xsl:text>
    </xsl:if>

    <!-- 6) The name of the vehicle owner (whether person or business) must be specified. -->
    <xsl:if test="0 = string-length(//@OwnerFirstName)">
      <xsl:if test="0 = string-length(//@OwnerLastName)">
        <xsl:if test="0 = string-length(//@OwnerBusinessName)">
          <xsl:text>|E2|The vehicle owner must have a person or business name specified.</xsl:text>
        </xsl:if>
      </xsl:if>
    </xsl:if>

    <!-- 7) At least one phone number for the vehicle owner must be specified. -->
    <xsl:if test="0 = string-length(//@OwnerDayPhone)">
      <xsl:if test="0 = string-length(//@OwnerNightPhone)">
        <xsl:if test="0 = string-length(//@OwnerAlternatePhone)">
          <xsl:text>|E2|At least one phone number for the vehicle owner must be specified.</xsl:text>
        </xsl:if>
      </xsl:if>
    </xsl:if>

    <!-- 8) Either the VIN or the License plate # must be specified. -->
    <xsl:if test="0 = string-length(//@Vin)">
      <xsl:if test="0 = string-length(//@LicensePlateNo) and  (//@ServiceChannelCD) != 'RRP'">
        <xsl:text>|E2|Either the vehicle VIN or the License plate # must be specified.</xsl:text>
      </xsl:if>
    </xsl:if>

    <!-- 9) The vehicle year, make and model must all be specified. -->
    <xsl:if test="(0=string-length(//@Make)) or (0=string-length(//@Model)) or (0=string-length(//@VehicleYear)) or ('0'=//@VehicleYear)">
      <xsl:text>|E2|The vehicle year, make and model must all be specified.</xsl:text>
    </xsl:if>

    <!-- 10) If the Vehicle is not drivable, then you must
                have either of the following vehicle information:
                a) Location - either the name or the address - and a Phone # OR
                b) A Contact name and a phone #
        -->
    <xsl:if test="//@Driveable='0'">
      <xsl:if test="( (0=string-length(//@VehicleContactFirstName)) and (0=string-length(//@VehicleContactLastName)) ) or ( (0=string-length(//@VehicleContactDayPhone)) and (0=string-length(//@VehicleContactNightPhone)) and (0=string-length(//@VehicleContactAlternatePhone)) )">
        <xsl:if test="(0=string-length(//@LocationPhone)) or ( (0=string-length(//@LocationName)) and (0=string-length(//@LocationAddress1)) )">
          <xsl:text>|E2|When the vehicle is non-driveable you must specify sufficient information about its location.  This must be either the vehicle contact (name and phone) or the vehicle location (phone, plus name or address).</xsl:text>
        </xsl:if>
      </xsl:if>
    </xsl:if>

    <!-- 11) The Primary Damage impact point must be specified. -->
    <xsl:if test="0 = string-length(//@PointOfImpactPrimary)">
      <xsl:text>|E2|The Primary Damage impact point must be specified.</xsl:text>
    </xsl:if>

    <!-- 12) The shop must have an alias if it is configured for electronic transmission. -->
    <xsl:if test="'Y' = //@SendElectronic">
      <xsl:if test="0 = string-length(//@DeliveryAddress)">
        <xsl:text>|E1|The selected shop is configured for electronic transmission but does not have a VAN delivery address.</xsl:text>
      </xsl:if>
    </xsl:if>

    <!-- 13) The shop must have an alias if it is configured for email transmission. 
      <xsl:if test="'Y' = //@SendEmail">
        <xsl:if test="0 = string-length(//@DeliveryAddress)">
          <xsl:text>|E1|The selected shop is configured for email transmission but does not have a Email address.</xsl:text>
        </xsl:if>
      </xsl:if>-->

  </xsl:template>

</xsl:stylesheet>
