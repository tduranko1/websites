<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template name="DetermineApplicationInfo">
    <xsl:choose>
      <xsl:when test="//Claim/DataSource = 'Mobile Electronics Assignment'">
        <xsl:attribute name="ApplicationCD">APD</xsl:attribute>
        <xsl:attribute name="ApplicationID">4</xsl:attribute>
      </xsl:when>
      <xsl:when test="//Claim/DataSource = 'FNOL Assignment'">
        <xsl:attribute name="ApplicationCD">FNOL</xsl:attribute>
        <xsl:attribute name="ApplicationID">4</xsl:attribute>
      </xsl:when>
      <xsl:when test="//Claim/DataSource = 'Web Services Assignment'">
        <xsl:attribute name="ApplicationCD">WS</xsl:attribute>
        <xsl:attribute name="ApplicationID">7</xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="ApplicationCD">CP</xsl:attribute>
        <xsl:attribute name="ApplicationID">2</xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="SourceAppCD">
    <xsl:choose>
      <xsl:when test="//Claim/DataSource = 'Mobile Electronics Assignment'">
        <xsl:attribute name="SourceApplicationCD">APD</xsl:attribute>
      </xsl:when>
      <xsl:when test="//Claim/DataSource = 'FNOL Assignment'">
        <xsl:attribute name="SourceApplicationCD">FNOL</xsl:attribute>
      </xsl:when>
      <xsl:when test="//Claim/DataSource = 'Web Services Assignment'">
        <xsl:attribute name="SourceApplicationCD">WS</xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="SourceApplicationCD">CP</xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="TranslateOldCoverageToNewFormat">
    <!-- First check collision -->
    <xsl:if test="CollisionDeductibleAmt!='' or CollisionLimitAmt!=''">
       <xsl:element name="Coverage">
          <xsl:attribute name="CoverageTypeCD">COLL</xsl:attribute>
          <xsl:attribute name="DeductibleAmt"><xsl:value-of select="CollisionDeductibleAmt"/></xsl:attribute>
          <xsl:attribute name="LimitAmt"><xsl:value-of select="CollisionLimitAmt"/></xsl:attribute>
       </xsl:element>
    </xsl:if>
    <xsl:if test="ComprehensiveDeductibleAmt!='' or ComprehensiveLimitAmt!=''">
       <xsl:element name="Coverage">
         <xsl:attribute name="CoverageTypeCD">COMP</xsl:attribute>
         <xsl:attribute name="DeductibleAmt"><xsl:value-of select="ComprehensiveDeductibleAmt"/></xsl:attribute>
         <xsl:attribute name="LimitAmt"><xsl:value-of select="ComprehensiveLimitAmt"/></xsl:attribute>
       </xsl:element>
    </xsl:if>
    <xsl:if test="LiabilityDeductibleAmt!='' or LiabilityLimitAmt!=''">
       <xsl:element name="Coverage">
         <xsl:attribute name="CoverageTypeCD">LIAB</xsl:attribute>
         <xsl:attribute name="DeductibleAmt"><xsl:value-of select="LiabilityDeductibleAmt"/></xsl:attribute>
         <xsl:attribute name="LimitAmt"><xsl:value-of select="LiabilityLimitAmt"/></xsl:attribute>
       </xsl:element>
    </xsl:if>
    <xsl:if test="UnderinsuredDeductibleAmt!='' or UnderinsuredLimitAmt!=''">
       <xsl:element name="Coverage">
         <xsl:attribute name="CoverageTypeCD">UIM</xsl:attribute>
         <xsl:attribute name="DeductibleAmt"><xsl:value-of select="UnderinsuredDeductibleAmt"/></xsl:attribute>
         <xsl:attribute name="LimitAmt"><xsl:value-of select="UnderinsuredLimitAmt"/></xsl:attribute>
       </xsl:element>
    </xsl:if>
    <xsl:if test="UnderInsuredDeductibleAmt!='' or UnderInsuredLimitAmt!=''">
       <xsl:element name="Coverage">
         <xsl:attribute name="CoverageTypeCD">UIM</xsl:attribute>
         <xsl:attribute name="DeductibleAmt"><xsl:value-of select="UnderInsuredDeductibleAmt"/></xsl:attribute>
         <xsl:attribute name="LimitAmt"><xsl:value-of select="UnderInsuredLimitAmt"/></xsl:attribute>
       </xsl:element>
    </xsl:if>
    <xsl:if test="UninsuredDeductibleAmt!='' or UninsuredLimitAmt!=''">
       <xsl:element name="Coverage">
         <xsl:attribute name="CoverageTypeCD">UM</xsl:attribute>
         <xsl:attribute name="DeductibleAmt"><xsl:value-of select="UninsuredDeductibleAmt"/></xsl:attribute>
         <xsl:attribute name="LimitAmt"><xsl:value-of select="UninsuredLimitAmt"/></xsl:attribute>
       </xsl:element>
    </xsl:if>
    <xsl:if test="UnInsuredDeductibleAmt!='' or UnInsuredLimitAmt!=''">
       <xsl:element name="Coverage">
         <xsl:attribute name="CoverageTypeCD">UM</xsl:attribute>
         <xsl:attribute name="DeductibleAmt"><xsl:value-of select="UnInsuredDeductibleAmt"/></xsl:attribute>
         <xsl:attribute name="LimitAmt"><xsl:value-of select="UnInsuredLimitAmt"/></xsl:attribute>
       </xsl:element>
    </xsl:if>
    <xsl:if test="(CollisionDeductibleAmt = '' and CollisionLimitAmt = '') or (ComprehensiveDeductibleAmt!='' or ComprehensiveLimitAmt!='') or (LiabilityDeductibleAmt!='' or LiabilityLimitAmt!='') or (UnderinsuredDeductibleAmt!='' or UnderinsuredLimitAmt!='') or (UnderInsuredDeductibleAmt!='' or UnderInsuredLimitAmt!='') or (UninsuredDeductibleAmt!='' or UninsuredLimitAmt!='') or (UnInsuredDeductibleAmt!='' or UnInsuredLimitAmt!='') and (//Vehicle/ExposureCD = '1' and //Vehicle/DeductibleAmt != '')">
       <xsl:element name="Coverage">
          <xsl:attribute name="CoverageTypeCD">
              <xsl:value-of select="//Vehicle/CoverageProfileCD"/>
          </xsl:attribute>
          <xsl:attribute name="DeductibleAmt"><xsl:value-of select="//Vehicle/DeductibleAmt"/></xsl:attribute>
          <xsl:attribute name="LimitAmt"/>
       </xsl:element>
    </xsl:if>
    <xsl:choose>
       <xsl:when test="LynxID != ''">
          <xsl:if test="//Vehicle[ExposureCD = '1' and (RentalDays!='' or RentalDayAmount!='' or RentalMaxAmount!='')]">
             <xsl:element name="Coverage">
                <xsl:attribute name="CoverageTypeCD">RENT</xsl:attribute>
                <xsl:attribute name="DeductibleAmt"><xsl:value-of select="//Vehicle[ExposureCD = '1']/RentalDeductibleAmt"/></xsl:attribute>
                <xsl:attribute name="LimitDailyAmt"><xsl:value-of select="//Vehicle[ExposureCD = '1']/RentalDayAmount"/></xsl:attribute>
                <xsl:attribute name="LimitAmt"><xsl:value-of select="//Vehicle[ExposureCD = '1']/RentalMaxAmount"/></xsl:attribute>
                <xsl:attribute name="MaximumDays"><xsl:value-of select="//Vehicle[ExposureCD = '1']/RentalDays"/></xsl:attribute>
             </xsl:element>
          </xsl:if>
       </xsl:when>
       <xsl:otherwise>
          <xsl:if test="RentalDays!='' or RentalDayAmount!='' or RentalMaxAmount!=''">
             <xsl:element name="Coverage">
                <xsl:attribute name="CoverageTypeCD">RENT</xsl:attribute>
                <xsl:attribute name="DeductibleAmt"><xsl:value-of select="RentalDeductibleAmt"/></xsl:attribute>
                <xsl:attribute name="LimitDailyAmt"><xsl:value-of select="RentalDayAmount"/></xsl:attribute>
                <xsl:attribute name="LimitAmt"><xsl:value-of select="RentalMaxAmount"/></xsl:attribute>
                <xsl:attribute name="MaximumDays"><xsl:value-of select="RentalDays"/></xsl:attribute>
             </xsl:element>
          </xsl:if>
       </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="InvolvedInsured">
    <xsl:param name="Number"/>
    <xsl:element name="Involved">
       <xsl:attribute name="InvolvedTypeInsured">1</xsl:attribute>
       <xsl:attribute name="VehicleNumber"><xsl:value-of select="$Number"/></xsl:attribute>
        <xsl:for-each select="//*">
          <xsl:variable name="NodeName"><xsl:value-of select="name()"/></xsl:variable>
          <xsl:choose>
            <!-- these first few strip out those elements which are not involved related -->
            <xsl:when test="starts-with($NodeName,'Insured')=false()"/>
            <xsl:when test="$NodeName='InsuredNameFirst'">
               <xsl:attribute name="NameFirst"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='InsuredNameLast'">
               <xsl:attribute name="NameLast"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='InsuredNameTitle'">
               <xsl:attribute name="NameTitle"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='InsuredPhoneDay' or $NodeName='InsuredPhone'">
               <xsl:attribute name="PhoneDay"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='InsuredPhoneDayExt' or $NodeName='InsuredPhoneExt'">
               <xsl:attribute name="PhoneDayExt"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='InsuredBusinessName'">
               <xsl:attribute name="BusinessName"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='InsuredPhoneNight' or $NodeName='InsuredNightPhone'">
               <xsl:attribute name="PhoneNight"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='InsuredPhoneNightExt' or $NodeName='InsuredNightPhoneExt'">
               <xsl:attribute name="PhoneNightExt"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='InsuredPhoneAlternate' or $NodeName='InsuredPhoneAlt' or $NodeName='InsuredAltPhone' or $NodeName='InsuredAlternatePhone'">
               <xsl:attribute name="PhoneAlternate"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='InsuredPhoneAlternateExt' or $NodeName='InsuredPhoneAltExt' or $NodeName='InsuredAltPhoneExt' or $NodeName='InsuredAlternatePhoneExt'">
               <xsl:attribute name="PhoneAlternateExt"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='InsuredBestPhoneCD' or $NodeName='InsuredBestPhoneCode'">
               <xsl:attribute name="BestPhoneCode"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='InsuredAddressCity' or $NodeName='InsuredCity'">
               <xsl:attribute name="AddressCity"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='InsuredAddressState' or $NodeName='InsuredState'">
               <xsl:attribute name="AddressState"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='InsuredAddressZip' or $NodeName='InsuredZip'">
               <xsl:attribute name="AddressZip"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
               <xsl:attribute name="{substring-after($NodeName,'Insured')}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
        <xsl:if test="contains('1,2,5,6,7,8,12,13,', concat(//Vehicle/AssignmentTypeID, ',')) = true()">
           <xsl:if test="(string(//InsuredAddressState) = '') or (string(//InsuredState) = '')">
              <xsl:attribute name="AddressState"><xsl:value-of select="//Claim/LossAddressState"/></xsl:attribute>
           </xsl:if>
        </xsl:if>
    </xsl:element>
  </xsl:template>
  
  <xsl:template name="InvolvedOwner">
     <xsl:param name="Number"/>
     <xsl:param name="Insured">0</xsl:param>
     <xsl:element name="Involved">
        <xsl:attribute name="InvolvedTypeOwner">1</xsl:attribute>
        <xsl:attribute name="InvolvedTypeInsured"><xsl:value-of select="$Insured"/></xsl:attribute>
        <xsl:attribute name="VehicleNumber"><xsl:value-of select="$Number"/></xsl:attribute>
        <xsl:if test="$Number!=1">
           <xsl:attribute name="InvolvedTypeClaimant">1</xsl:attribute>
        </xsl:if>
        <xsl:for-each select="*">
          <xsl:variable name="NodeName"><xsl:value-of select="name()"/></xsl:variable>
          <xsl:choose>
            <!-- these first few strip out those elements which are not involved related -->
            <xsl:when test="starts-with($NodeName,'Owner')=false()"/>
            <xsl:when test="$NodeName='OwnerNameFirst'">
               <xsl:attribute name="NameFirst"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='OwnerNameLast'">
               <xsl:attribute name="NameLast"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='OwnerNameTitle'">
               <xsl:attribute name="NameTitle"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='OwnerPhoneDay' or $NodeName='OwnerPhone'">
               <xsl:attribute name="PhoneDay"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='OwnerPhoneDayExt' or $NodeName='OwnerPhoneExt'">
               <xsl:attribute name="PhoneDayExt"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='OwnerBusinessName'">
               <xsl:attribute name="BusinessName"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='OwnerPhoneNight' or $NodeName='OwnerNightPhone'">
               <xsl:attribute name="PhoneNight"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='OwnerPhoneNightExt' or $NodeName='OwnerNightPhoneExt'">
               <xsl:attribute name="PhoneNightExt"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='OwnerPhoneAlternate' or $NodeName='OwnerPhoneAlt' or $NodeName='OwnerAltPhone' or $NodeName='OwnerAlternatePhone'">
               <xsl:attribute name="PhoneAlternate"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='OwnerPhoneAlternateExt' or $NodeName='OwnerPhoneAltExt' or $NodeName='OwnerAltPhoneExt' or $NodeName='OwnerAlternatePhoneExt'">
               <xsl:attribute name="PhoneAlternateExt"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='OwnerBestPhoneCD' or $NodeName='OwnerBestPhoneCode'">
               <xsl:attribute name="BestPhoneCode"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='OwnerAddressCity' or $NodeName='OwnerCity'">
               <xsl:attribute name="AddressCity"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='OwnerAddressState' or $NodeName='OwnerState'">
               <xsl:attribute name="AddressState"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:when test="$NodeName='OwnerAddressZip' or $NodeName='OwnerZip'">
               <xsl:attribute name="AddressZip"><xsl:value-of select="."/></xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
               <xsl:attribute name="{substring-after($NodeName,'Owner')}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
        <xsl:if test="$Insured = '1'">
            <!-- Owner is the insured. We don't collect address information at the owner level. So transfer the data from the insured. -->
            <xsl:if test="(concat(OwnerAddress1, '') = '') and (//Claim/InsuredAddress1 != '')">
               <xsl:attribute name="Address1">
                  <xsl:value-of select="//Claim/InsuredAddress1"/>
               </xsl:attribute>
            </xsl:if>
            <xsl:if test="(concat(OwnerAddress2, '') = '') and (//Claim/InsuredAddress2 != '')">
               <xsl:attribute name="Address2">
                  <xsl:value-of select="//Claim/InsuredAddress2"/>
               </xsl:attribute>
            </xsl:if>
            <xsl:if test="(concat(OwnerAddressCity, OwnerCity) = '') and (//Claim/InsuredAddressCity != '' or //Claim/InsuredCity != '')">
               <xsl:attribute name="AddressCity">
                  <xsl:choose>
                     <xsl:when test="//Claim/InsuredAddressCity != ''">
                        <xsl:value-of select="//Claim/InsuredAddressCity"/>
                     </xsl:when>
                     <xsl:otherwise>
                        <xsl:value-of select="//Claim/InsuredCity"/>
                     </xsl:otherwise>
                  </xsl:choose>
               </xsl:attribute>
            </xsl:if>
            <xsl:if test="(concat(OwnerAddressCity, OwnerCity) = '') and (//Claim/InsuredAddressCity != '' or //Claim/InsuredCity != '')">
               <xsl:attribute name="AddressCity">
                  <xsl:choose>
                     <xsl:when test="//Claim/InsuredAddressCity != ''">
                        <xsl:value-of select="//Claim/InsuredAddressCity"/>
                     </xsl:when>
                     <xsl:otherwise>
                        <xsl:value-of select="//Claim/InsuredCity"/>
                     </xsl:otherwise>
                  </xsl:choose>
               </xsl:attribute>
            </xsl:if>
            <xsl:if test="(concat(OwnerAddressState, OwnerState) = '') and (//Claim/InsuredAddressState != '' or //Claim/InsuredState != '')">
               <xsl:attribute name="AddressState">
                  <xsl:choose>
                     <xsl:when test="//Claim/InsuredAddressState != ''">
                        <xsl:value-of select="//Claim/InsuredAddressState"/>
                     </xsl:when>
                     <xsl:otherwise>
                        <xsl:value-of select="//Claim/InsuredState"/>
                     </xsl:otherwise>
                  </xsl:choose>
               </xsl:attribute>
            </xsl:if>
            <xsl:if test="(concat(OwnerAddressZip, OwnerZip) = '') and (//Claim/InsuredAddressZip != '' or //Claim/InsuredZip != '')">
               <xsl:attribute name="AddressZip">
                  <xsl:choose>
                     <xsl:when test="//Claim/InsuredAddressZip != ''">
                        <xsl:value-of select="//Claim/InsuredAddressZip"/>
                     </xsl:when>
                     <xsl:otherwise>
                        <xsl:value-of select="//Claim/InsuredZip"/>
                     </xsl:otherwise>
                  </xsl:choose>
               </xsl:attribute>
            </xsl:if>
        </xsl:if>
        <xsl:if test="contains('1,2,5,6,7,8,12,13,', concat(//Vehicle/AssignmentTypeID, ',')) = true()">
           <xsl:if test="((string(//OwnerAddressState) = '') or (string(//OwnerState) = '')) and (//Claim/InsuredAddressState = '' or //Claim/InsuredState = '')">
              <xsl:attribute name="AddressState"><xsl:value-of select="//Claim/LossAddressState"/></xsl:attribute>
           </xsl:if>
        </xsl:if>
     </xsl:element>
  </xsl:template>

  <xsl:template match="Vehicle">
    <xsl:param name="Number"><xsl:value-of select="VehicleNumber"/></xsl:param>
    <xsl:element name="Vehicle">
       <!-- Add a Vehicle Number from passed parameter -->
       <xsl:attribute name="VehicleNumber"><xsl:value-of select="$Number"/></xsl:attribute>

       <!-- This will fix the issue with the client not sending in the coverage type and FNOL load defaults it to Collision -->
       <xsl:variable name="CoverageCD"><xsl:value-of select="CoverageProfileUiCD"/></xsl:variable>
       <xsl:variable name="CoverageProfileCD"><xsl:value-of select="CoverageProfileCD"/></xsl:variable>
	   <xsl:variable name="ClientCoverageTypeID"><xsl:value-of select="ClientCoverageTypeID"/></xsl:variable>
	   <xsl:variable name="ClientCoverageTypeDesc"><xsl:value-of select="ClientCoverageTypeDesc"/></xsl:variable>


       <!--  Copy all elements of Vehicle as attributes -->
       <xsl:for-each select="*">
         <xsl:choose>
            <xsl:when test="starts-with(name(),'Owner')"/>
		    <xsl:when test="name()='CoverageProfileCD' and $CoverageProfileCD = ''">
               <xsl:attribute name="{name()}"><xsl:value-of select="substring-before($CoverageCD, '|')"/></xsl:attribute>
            </xsl:when>
		    <xsl:when test="name()='ClientCoverageTypeID' and $ClientCoverageTypeID = ''">
			   <xsl:attribute name="{name()}"><xsl:value-of select="substring-after($CoverageCD, '|')"/></xsl:attribute>
            </xsl:when>
		    <xsl:when test="name()='ClientCoverageTypeDesc' and $ClientCoverageTypeDesc = ''">
			   <xsl:attribute name="{name()}"><xsl:value-of select="substring-before($CoverageCD, '|')"/></xsl:attribute>
		    </xsl:when>
		    <xsl:otherwise>
			   <xsl:attribute name="{name()}"><xsl:value-of select="."/></xsl:attribute>
		    </xsl:otherwise>
         </xsl:choose>
       </xsl:for-each>
       <xsl:attribute name="INF"><xsl:value-of select="//InsuredNameFirst"/></xsl:attribute>
      
       <!-- Now determine Involveds -->
       <xsl:choose>
          <!-- 1st party vehicle owner is insured -->
          <xsl:when test="($Number=1) and (//InsuredNameFirst=OwnerNameFirst) and (//InsuredNameLast=OwnerNameLast) and ((//InsuredBusinessName=OwnerBusinessName) or (//InsuredBusinessName='' and //OwnerBusinessName=''))">
             <xsl:call-template name="InvolvedOwner">
                <xsl:with-param name="Number">1</xsl:with-param>
                <xsl:with-param name="Insured">1</xsl:with-param>
             </xsl:call-template>
          </xsl:when>
          <!-- 1st party vehicle owner is not the insured -->
          <xsl:when test="($Number=1)">
            <xsl:call-template name="InvolvedOwner">
               <xsl:with-param name="Number">1</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="InvolvedInsured">
               <xsl:with-param name="Number">1</xsl:with-param>
            </xsl:call-template>
          </xsl:when>
          <!-- Any other vehicle -->
          <xsl:otherwise>
            <xsl:call-template name="InvolvedOwner">
               <xsl:with-param name="Number"><xsl:value-of select="$Number"/></xsl:with-param>
            </xsl:call-template>
          </xsl:otherwise>
       </xsl:choose>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>