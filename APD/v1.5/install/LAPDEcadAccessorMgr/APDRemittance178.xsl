<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt">
  

  <xsl:output method="xml" indent="no" encoding="UTF-8" omit-xml-declaration="yes"/>
  

  <xsl:template match="/">
		<html>
			<head>
				<title>Invoice Remittance</title>
			</head>
			
			<body>
							
				<xsl:apply-templates select="APDRemittance/Invoice"/>
				
				<table border="1">
					<tr>
						<th>WCSI Claim Number</th>
						<th>Lynx Claim Number</th>
						<th>Loss Date</th>
						<th>Total Invoiced Amt</th>
						<th>Total Paid Amt</th>
						<th>Total Fee Amt</th>
						<th>Invoice Amt</th>
						<th>Invoice Paid Amt</th>
						<th>Dispatch Or Invoice Number</th>
						<th>Reserve Number</th>
						<th>Line Type Number</th>
						<th>Remit Code</th>
					</tr>				
				
					<xsl:for-each select="APDRemittance/Details/Claim">
						<xsl:apply-templates select="."/>
					</xsl:for-each>

				</table>
			</body>
		</html>
   </xsl:template>


   <xsl:template match="Invoice">
		
		<table border="0">
			<tr>
				<td style="font-weight:bold;">Insurance Company</td>
				<td width="150" bgcolor="#DDDDDD"><xsl:value-of select="InsuranceCompany"/></td>
				<td style="font-weight:bold;">Batch Invoice Number</td>
				<td width="150" bgcolor="#DDDDDD"><xsl:value-of select="BatchInvoiceNumber"/></td>
				<td style="font-weight:bold;">Batch Invoice Date</td>
				<td bgcolor="#DDDDDD">
					<xsl:value-of select="substring(BatchInvoiceDate, 5, 2)"/>/<xsl:value-of select="substring(BatchInvoiceDate, 7, 2)"/>/<xsl:value-of select="substring(BatchInvoiceDate, 1, 4)"/>
			     </td>
			</tr>
			<tr>
				<td style="font-weight:bold;">Batch Total Paid Indemnity Amt</td>
				<td bgcolor="#DDDDDD"><xsl:value-of select="format-number(BatchTotalPaidIndemnityAmt, '$###,###,###.00')"/></td>	
				<td style="font-weight:bold;">Batch Total Expense Amt</td>
				<td bgcolor="#DDDDDD"><xsl:value-of select="format-number(BatchTotalExpenseAmt, '$###,###,###.00')"/></td>
							
			</tr>
			<tr>
				<td style="font-weight:bold;">Batch Total Payment Count</td>
				<td bgcolor="#DDDDDD"><xsl:value-of select="format-number(BatchTotalPaymentCount, '###,###,###')"/></td>				
				<td style="font-weight:bold;">Batch Total Payment Paid Count</td>
				<td bgcolor="#DDDDDD"><xsl:value-of select="format-number(BatchTotalPaymentPaidCount, '###,###,###')"/></td>				
				<td style="font-weight:bold;">Batch Status Description</td>
				<td bgcolor="#DDDDDD"><xsl:value-of select="BatchStatusDescription"/></td>
			</tr>			
		</table>
				
		<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
       
   </xsl:template>


   <xsl:template match="Claim">
		<tr>
		<td><xsl:value-of select="WCSIClaimNumber"/></td>
		<td><xsl:value-of select="LynxClaimNumber"/></td>
		<td><xsl:value-of select="substring(LossDate, 5, 2)"/>/<xsl:value-of select="substring(LossDate, 7, 2)"/>/<xsl:value-of select="substring(LossDate, 1, 4)"/></td>
		<td><xsl:value-of select="format-number(TotalInvoicedAmt, '$###,###,###.00')"/></td>
		<td><xsl:value-of select="format-number(TotalPaidAmt, '$###,###,###.00')"/></td>
		<td><xsl:value-of select="format-number(TotalFeeAmt, '$###,###,###.00')"/></td>
		
		<xsl:for-each select="Payment">
			<xsl:apply-templates select=".">
				<xsl:with-param name="position" select="position()"/>
			</xsl:apply-templates>
		</xsl:for-each>		
		</tr>
   </xsl:template>

   
   <xsl:template match="Payment">
		
		<xsl:param name="position"/>
		
		<xsl:choose>    
			<xsl:when test="$position = 1">
				<td><xsl:value-of select="format-number(InvoiceAmt, '$###,###,###.00')"/></td>
				<td><xsl:value-of select="format-number(InvoicePaidAmt, '$###,###,###.00')"/></td>
				<td><xsl:value-of select="DispatchOrInvoiceNumber"/></td>
				<td><xsl:value-of select="ReserveNumber"/></td>
				<td><xsl:value-of select="LineTypeNumber"/></td>
				<td>
					<xsl:choose>
	          			<xsl:when test="starts-with(RemitCode, '500')">OK</xsl:when>
						<xsl:otherwise><xsl:value-of select="RemitCode"/></xsl:otherwise>
	        			</xsl:choose>
				</td>
			</xsl:when>
			<xsl:otherwise>
			     <tr>
					<td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
					<td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
					<td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
					<td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
					<td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
					<td><xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;</td>
					<td><xsl:value-of select="format-number(InvoiceAmt, '$###,###,###.00')"/></td>
					<td><xsl:value-of select="format-number(InvoicePaidAmt, '$###,###,###.00')"/></td>
					<td><xsl:value-of select="DispatchOrInvoiceNumber"/></td>
					<td><xsl:value-of select="ReserveNumber"/></td>
					<td><xsl:value-of select="LineTypeNumber"/></td>
					<td>
						<xsl:choose>
		          			<xsl:when test="starts-with(RemitCode, '500')">OK</xsl:when>
							<xsl:otherwise><xsl:value-of select="RemitCode"/></xsl:otherwise>
		        			</xsl:choose>
					</td>				
				</tr>				
			</xsl:otherwise>
		</xsl:choose>
		
   </xsl:template>

</xsl:stylesheet>
