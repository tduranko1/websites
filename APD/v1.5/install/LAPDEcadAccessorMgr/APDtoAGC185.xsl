<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" omit-xml-declaration="yes"/>

    <xsl:param name="TransactionDate"/>
    <xsl:param name="TransactionTime"/>
    <xsl:param name="EnvironmentID"/>
    <xsl:param name="TransactionID"/>
    <xsl:param name="AgcID"/>

    <xsl:template match="/WebAssignment">
        <LossNotice>  
            <LossNoticeHeader>
                <EnvironmentID><xsl:value-of select="$EnvironmentID"/></EnvironmentID>
                <TransactionDate><xsl:value-of select="$TransactionDate"/></TransactionDate>
                <TransactionTime><xsl:value-of select="$TransactionTime"/></TransactionTime>
                <TransactionID><xsl:value-of select="$TransactionID"/></TransactionID>
                <VersionNumber>1.0</VersionNumber>
            </LossNoticeHeader>    
            <LossNoticeDetails>
		  <LossType>Mobile Electronics</LossType>
                <ClaimID></ClaimID>
                <InsurerID><xsl:value-of select="$AgcID"/></InsurerID>        
                <InsuredOrClaimantFirstName><xsl:value-of select="Vehicle/OwnerNameFirst"/></InsuredOrClaimantFirstName>
                <InsuredOrClaimantLastName><xsl:value-of select="Vehicle/OwnerNameLast"/></InsuredOrClaimantLastName>       
                <!-- The phone number marked as primary -->
                <InsuredOrClaimantTelephone>
                    <xsl:choose>
                        <xsl:when test="Vehicle/ContactBestPhoneCD='N'"><xsl:value-of select="Vehicle/OwnerNightPhone"/></xsl:when>
                        <xsl:when test="Vehicle/ContactBestPhoneCD='A'"><xsl:value-of select="Vehicle/OwnerAltPhone"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="Vehicle/OwnerPhone"/></xsl:otherwise>          
                    </xsl:choose>
                </InsuredOrClaimantTelephone>       
                <!-- Attempt to select the best phone number not marked as primary -->
                <InsuredOrClaimantAlternateTelephone>
                    <xsl:choose>
                        <xsl:when test="Vehicle/ContactBestPhoneCD='D'">
                            <xsl:choose>
                                <xsl:when test="Vehicle/OwnerNightPhone != ''"><xsl:value-of select="Vehicle/OwnerNightPhone"/></xsl:when>
                                <xsl:otherwise><xsl:value-of select="Vehicle/OwnerAltPhone"/></xsl:otherwise>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:when test="Vehicle/ContactBestPhoneCD='N'">
                            <xsl:choose>
                                <xsl:when test="Vehicle/OwnerPhone != ''"><xsl:value-of select="Vehicle/OwnerPhone"/></xsl:when>
                                <xsl:otherwise><xsl:value-of select="Vehicle/OwnerAltPhone"/></xsl:otherwise>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:when test="Vehicle/ContactBestPhoneCD='A'">
                            <xsl:choose>
                                <xsl:when test="Vehicle/OwnerPhone != ''"><xsl:value-of select="Vehicle/OwnerPhone"/></xsl:when>
                                <xsl:otherwise><xsl:value-of select="Vehicle/OwnerNightPhone"/></xsl:otherwise>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise><xsl:value-of select="Vehicle/OwnerPhone"/></xsl:otherwise>
                    </xsl:choose>
                </InsuredOrClaimantAlternateTelephone>      
                <InsuredOrClaimantAddress><xsl:value-of select="Vehicle/Address1"/></InsuredOrClaimantAddress>
                <InsuredOrClaimantCity><xsl:value-of select="Vehicle/AddressCity"/></InsuredOrClaimantCity>
                <InsuredOrClaimantStateProvince><xsl:value-of select="Vehicle/AddressState"/></InsuredOrClaimantStateProvince>
                <InsuredOrClaimantPostalCode><xsl:value-of select="Vehicle/AddressZip"/></InsuredOrClaimantPostalCode>      
                <InsuredOrClaimantPolicyNumber></InsuredOrClaimantPolicyNumber>
                <InsuredOrClaimantPolicyState></InsuredOrClaimantPolicyState>
                <LossIdentificationNumber><xsl:value-of select="Claim/CoverageClaimNumber"/></LossIdentificationNumber>         
                <DateOfLoss><xsl:value-of select="Claim/LossDate"/></DateOfLoss>
                <Deductible><xsl:value-of select="Vehicle/DeductibleAmt"/></Deductible>
                <VehicleIDNumber><xsl:value-of select="Vehicle/VIN"/></VehicleIDNumber>
                <VehicleYear><xsl:value-of select="Vehicle/VehicleYear"/></VehicleYear>
                <VehicleMake><xsl:value-of select="Vehicle/Make"/></VehicleMake>
                <VehicleModel><xsl:value-of select="Vehicle/Model"/></VehicleModel>
                <GlassOnlyDamage>false</GlassOnlyDamage>        <!-- Glass only should go directly to AGC sans APD -->
                <LossDescription><xsl:value-of select="Claim/LossDescription"/></LossDescription>
                <Comments><xsl:value-of select="Vehicle/SourceApplicationPassThruData"/></Comments>       
                <GlassDamageFlag>true</GlassDamageFlag>         <!-- If there was no glass damage, this stylesheet would never be invoked. -->
                <ExteriorDamageFlag>
                    <xsl:choose>
                        <xsl:when test="contains(Vehicle/ShopRemarks, 'No Body Damage reported')">false</xsl:when>
                        <xsl:otherwise>true</xsl:otherwise>
                    </xsl:choose>
                </ExteriorDamageFlag>
                <PassThru><xsl:value-of select="Vehicle/ShopRemarks"/></PassThru>
                <ClaimRep></ClaimRep>
                <ClaimRepPhone></ClaimRepPhone>
                <ClaimRepEmail></ClaimRepEmail>
            </LossNoticeDetails>
        </LossNotice>
    </xsl:template>
</xsl:stylesheet>