
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:head="http://www.cccis.com/interfaces/iatpa/header" xmlns:com="http://www.cccis.com/interfaces/iatpa/commontypes">
  <xsl:import href="WebAssignmentXferUtil.xsl"/>
  <xsl:output method="xml" indent="no" encoding="UTF-8" omit-xml-declaration="yes"/>
  <!-- ================================================== -->
  <!-- Converts WebAssignment 300 format to APD's internal-->
  <!-- attribute centric format                           -->
  <!-- LAPDECADAccessor.CWebAssignment.ApdLoadAssignment  -->
  <!-- ================================================== -->
  <xsl:template match="/WebAssignment">
    <!-- Web Assignment root template -->
    <xsl:element name="WebAssignment">
      <xsl:call-template name="DetermineApplicationInfo"/>
      <xsl:apply-templates select="Claim"/>
      <xsl:element name="BeginInsert"/>
    </xsl:element>
  </xsl:template>
  <xsl:template match="Claim">
    <xsl:element name="Claim">
      <xsl:call-template name="SourceAppCD"/>
      <!-- Copy elements as attributes -->
      <xsl:for-each select="*">
        <xsl:if test="name()!='Coverage' and name()!='Vehicle'">
           <xsl:attribute name="{name()}"><xsl:value-of select="."/></xsl:attribute>
        </xsl:if>
      </xsl:for-each>
      <xsl:attribute name="CarrierRepLogin">
        <xsl:value-of select="CarrierRepEmailAddress"/>
      </xsl:attribute>
      <xsl:attribute name="SourceApplicationPassThruData"/>
      <xsl:for-each select="//Claim/Coverage">
        <xsl:element name="Coverage">
            <xsl:for-each select="./*">
               <xsl:attribute name="{name()}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
        </xsl:element>
      </xsl:for-each>
      <xsl:choose>
        <xsl:when test="boolean(//Claim/NewClaimFlag=0)">
          <xsl:apply-templates select="//Vehicle"/>
        </xsl:when>
        <!-- New vehicle is being created. -->
        <xsl:otherwise>
          <!-- Add 1st party vehicle -->
          <xsl:choose>
            <!-- 1st party vehicle exists? -->
            <xsl:when test="boolean(//Vehicle[ExposureCD=1])">
              <xsl:apply-templates select="//Vehicle[ExposureCD=1]">
                <xsl:with-param name="Number">1</xsl:with-param>
              </xsl:apply-templates>
            </xsl:when>
            <!-- otherwise pass a blank 1st party vehicle with insured -->
            <xsl:otherwise>
              <xsl:element name="Vehicle">
                <xsl:attribute name="VehicleNumber">1</xsl:attribute>
                <xsl:call-template name="InvolvedInsured">
                  <xsl:with-param name="Number">1</xsl:with-param>
                </xsl:call-template>
              </xsl:element>
            </xsl:otherwise>
          </xsl:choose>
          <!-- Add 3rd Party vehicles -->
          <xsl:apply-templates select="//Vehicle[ExposureCD=3]"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:element>
  </xsl:template>
  

  

</xsl:stylesheet>