﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClaimXP.aspx.vb" Inherits="APDNet.ClaimXP" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <title>Claim XP</title>
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />

    <!-- ============== JQuery - Loader =============== -->
    <script type="text/javascript" src="js/jQuery/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="js/jQuery/jquery-ui.js"></script>
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <script type="text/javascript" src="js/jQuery/APDData.js"></script>
    <script type="text/javascript" src="js/jQuery/APDBase.js"></script>
    <script type="text/javascript" src="js/jQuery/ClaimTabs.js"></script>
    <script type="text/javascript" src="js/jQuery/json2.js"></script>
    <script type="text/javascript" src="js/jQuery/jquery.mask.js"></script>

    <script type="text/javascript" src="js/formats.js"></script>

    <script type="text/javascript">
        //-----------------------------------//
        // Page Init
        //-----------------------------------//
        function pageInit() {
            $.each($("[id$=hidVehNumber]"), function () {
                //Using this loop we can replace each tabs 
                var obj = $("#" + this.id.replace("hidVehNumber", "txtWorkEndDate"));
                if (obj != null) {
                    if (top.blnSupervisorFlag == "1") {
                        obj.prop('disabled', false);
                    }
                    //alert(obj[0].id);
                }

                var obj = $("#" + this.id.replace("hidVehNumber", "txtWorkStartDate"));
                if (obj != null) {
                    if (top.blnSupervisorFlag == "1") {
                        obj.prop('disabled', false);
                    }
                    //alert(obj[0].id);
                }

                try {
                    var oNetRepairTotal = $("#" + this.id.replace("hidVehNumber", "txtNetRepairTotal"));
                    var strNetRepairTotal = oNetRepairTotal[0].value;
                    var strCurEstNetAmt = "";

                    //-- Remove the $ from the Net Amt then check if ""
                    if (strNetRepairTotal.includes("$")) {
                        strCurEstNetAmt = strNetRepairTotal.substring(1).trim();
                    } else {
                        strCurEstNetAmt = strNetRepairTotal;
                    }

                    if (strCurEstNetAmt == "")
                        window.setTimeout("calculateNetAmt(" + this.id + ")", 150);
                } catch (e) { }

                if (parent.stat1) {
                    parent.stat1.Hide();
                }

                //alert("PageInit2");
                pageInit2($("[id$=hidVehNumber]"), "hidVehNumber");
            });
        }

        function pageInit2(sender, senderID) {
            //-- Page Init2 --//
            if (senderID == "hidVehNumber") {
                sender = sender[0];
            }

            var strServiceChannelCD = $("#" + sender.id.replace(senderID, "hidServiceChannelCD"))[0].value;
            if (strServiceChannelCD == "DA" || strServiceChannelCD == "DR") {
                var strShopLocationCity = $("#" + sender.id.replace(senderID, "txtRepairCity"))[0].value;
                var objShopLocationCity = $("#" + sender.id.replace(senderID, "txtRepairCity"));
                var strShopLocationCounty = $("#" + sender.id.replace(senderID, "ddlRepairCounty"))[0].value;
                var strShopLocationState = $("#" + sender.id.replace(senderID, "ddlRepairState"))[0].value;
                var objShopLocationState = $("#" + sender.id.replace(senderID, "ddlRepairState"));
                var objShopLocationStateOptions = $("#" + sender.id.replace(senderID, "ddlRepairState") + " option");

                if (strShopLocationCity != "" && strShopLocationCounty != "" && strShopLocationState != "") {
                    lsCityData = [strShopLocationCity, strShopLocationState, strShopLocationCounty];
                }

                $.get("csclookup.asp?cty=" + strShopLocationCity, function (data) {
                    var oCitiesCache = null;
                    if (data != "") {
                        var matches = data.split("\n");
                        var strAdded = "";
                        var strState = "";
                        var strStatesCache = "";

                        //-----------------------------//
                        // Populate states string from 
                        // dropdown options
                        //-----------------------------//
                        if (strStatesCache == "") {
                            objShopLocationStateOptions.each(function () {
                                strStatesCache += "," + $(this).val();
                            });
                        }

                        //----------------------------//
                        // Get matching state strings
                        //----------------------------//
                        var strMatchStatesCache = "";
                        for (var i = 0; i < matches.length; i++) {
                            data2 = matches[i].split("|");
                            var strCity = data2[0];
                            var strState = data2[1];
                            if (strState) {
                                strMatchStatesCache += strState + ",";
                            }
                        }

                        //----------------------------//
                        // Array the States
                        //----------------------------//
                        var oStatesCacheArray = null;
                        oStatesCacheArray = strMatchStatesCache.split(",");

                        //-------------------------//
                        // Check matching state array
                        // against state dropdown and
                        // remove non-matching
                        //-------------------------//
                        if (senderID == "hidVehNumber") {
                            for (j = 0; j < oStatesCacheArray.length; j++) {
                                if (oStatesCacheArray[j].length == 2) {
                                    if (strMatchStatesCache.indexOf(oStatesCacheArray[j]) == -1) {
                                        objShopLocationState.find('option[value="' + oStatesCacheArray[j] + '"]').remove();
                                    }
                                }
                            }
                        } else {
                            //-----------------------------//
                            // Populate the Repair States 
                            //-----------------------------//
                            var strState = "";
                            var objDDLRepairState = $("#" + sender.id.replace(senderID, "ddlRepairState"));
                            objDDLRepairState.empty();

                            for (j = 0; j < oStatesCacheArray.length; j++) {
                                //if (oStatesCacheArray[j].length == 2) {
                                strState = lookupState(oStatesCacheArray[j]);
                                objDDLRepairState.append('<option value=' + oStatesCacheArray[j] + '>' + strState + '</option>');
                                //}
                            }
                        }

                    } else {
                        alert("City could not be resolved. Please enter a valid City.");
                        objShopLocationCity.focus();
                    }
                });
            }
        }

        function lookupState(strStateAbbr) {
            var objStateListOptions = $("#ddlState option[value='" + strStateAbbr + "']");
            return objStateListOptions.text();
        }

        //function addDropDownOption(sender, senderID, ddlDropDownObj, strOptionValue, strOptionText) {
        //    debugger;
        //    var ddlDropDown = $("#" + sender.id.replace(senderID, ddlDropDownObj));
        //    //ddlDropDown.empty();
        //    ddlDropDown.append('<option value=' + strOptionValue + '>' + strOptionText + '</option>');
        //}

        function populateCounty(sender, senderID) {
            var strRepairCounty = "";
            var strRepairCountyArray = null;
            var strServiceChannelCD = $("#" + sender.id.replace(senderID, "hidServiceChannelCD"))[0].value;

            if (strServiceChannelCD == "DA" || strServiceChannelCD == "DR") {
                var ddlRepairCounty = $("#" + sender.id.replace(senderID, "ddlRepairCounty"));
                var objRepairCounty = $("#" + sender.id.replace(senderID, "ddlRepairCounty") + " option");

                pageInit2(sender, senderID);

                //-----------------------------//
                // Change all existing value to 
                // optional
                //-----------------------------//
                objRepairCounty.each(function () {
                    strRepairCounty += "," + $(this).val() + " [Override]";
                });

                strRepairCountyArray = strRepairCounty.split(",");
                ddlRepairCounty.empty();
                for (var i = 0; i < strRepairCountyArray.length; i++) {
                    ddlRepairCounty.append('<option value=' + i + '>' + strRepairCountyArray[i] + '</option>');
                }
            }
        }

        function IsDirty(nIndex) {
            if (nIndex == -1)
                return true;
            else
                if (arrayDirty.length > nIndex)
                    return arrayDirty[nIndex];
                else
                    return true;
        }




        function calculateNetAmt(domObj) {
            //-----------------------//
            //-- Calc Current Estimate
            //-----------------------//
            var oNetRepairTotal = $("#" + domObj.id.replace("hidVehNumber", "txtNetRepairTotal"));
            var strNetRepairTotal = oNetRepairTotal[0].value;

            var oDeductibleAppliedAmt = $("#" + domObj.id.replace("hidVehNumber", "txtDeductibleApplied"));
            var strDeductibleAppliedAmt = oDeductibleAppliedAmt[0].value;

            var oLimitsEffect = $("#" + domObj.id.replace("hidVehNumber", "txtLimitsEffect"));
            var strLimitsApplied = oLimitsEffect[0].value;

            var strCurEstGrossAmt = $("#" + domObj.id.replace("hidVehNumber", "txtGrossAmt"))[0].value;

            if ((strNetRepairTotal != "") && (parseFloat(strCurEstGrossAmt) > 0)) {
                var strCurEstNetAmt = strNetRepairTotal.substring(1).Trim();
            } else {
                //window.setTimeout("calculateNetAmt('" + this.id + "')", 150);
                var dblDeductions = 0;

                if ((isNaN(strDeductibleAppliedAmt) == false) && (strDeductibleAppliedAmt != "")) {
                    dblDeductions += parseFloat(strDeductibleAppliedAmt);
                    oDeductibleAppliedAmt.value = "$ " + formatCurrencyString(strDeductibleAppliedAmt);
                }
                if ((isNaN(strLimitsApplied) == false) && (strLimitsApplied != "")) {
                    oLimitsEffect.value = "$ " + formatCurrencyString(strLimitsApplied);
                }
                if (isNaN(strCurEstGrossAmt) == false) {
                    oNetRepairTotal.value = "$ " + formatCurrencyString(parseFloat(strCurEstGrossAmt) - dblDeductions);
                }
            }
        }

        function centerDiv(obj) {
            with (obj) {
                style.top = (document.body.offsetHeight - obj.offsetHeight) / 2;
                style.left = (document.body.offsetWidth - obj.offsetWidth) / 2;
                firstChild.style.filter = "";
            }
        }

        function zipResolve(objXML) {
            if (objXML && objXML.xml) {
                var objData = objXML.xml.selectSingleNode("//Result/Root/Detail");
                if (objData) {
                    txtRepairLocationCity.value = objData.getAttribute("City");
                    txtRepairLocationState.value = objData.getAttribute("StateCode");
                    lsCityData = [objData.getAttribute("City"), objData.getAttribute("StateCode"), objData.getAttribute("County")];
                    $("#txtRepairLocationCity").trigger("blur");
                }
            }
        }

        //-----------------------------------//
        // Send Fax Assignment 
        //-----------------------------------//
        function FaxAssignment(strServiceChannelCD) {
            //alert(strServiceChannelCD);
            var strStaffAppraiser = "false";
            if (strServiceChannelCD == "DA" || strServiceChannelCD == "DR" || (strServiceChannelCD == "RRP" && strServiceChannelCD == "LDAU")) {
                strStaffAppraiser = "true";
            }
            strRequest = "<Root><Assignment assignmentID='" + strAssignmentID[0].value + "' userID='" + strUserID[0].value + "' staffAppraiser='" + strStaffAppraiser + "' hqAssignment='false' /></Root>"
            XMLServerExecute("AssignShop.asp", strRequest);
            //alert(objRet.code);
        }

        //-----------------------------------//
        // Repair Change Reason
        //-----------------------------------//
        $(function () {
            $("#diaRepairChangeReason").dialog({
                autoOpen: false,
                modal: true,
                appendTo: "form",
                show: {
                    effect: "blind",
                    duration: 1000
                },
                hide: {
                    effect: "explode",
                    duration: 1000
                }
            });
        });


        //-----------------------------------//
        // Show Confirm WorkStart Checkbox 
        //-----------------------------------//
        function ValidateStartDate(sender, senderID) {
            var btnRepairStart = $("#" + sender.id.replace(senderID, "btnRepairStart"));  //-- Find the button object
            var txtWorkStartDate = $("#" + sender.id.replace(senderID, "txtWorkStartDate"));  //-- Find the textbox object
            //-- Validate
            if (btnRepairStart) {
                if (txtWorkStartDate[0].value != "") {
                    var dtRepairStart = new Date(txtWorkStartDate[0].value);
                    var dtToday = new Date();
                    if (dtRepairStart <= dtToday) {
                        btnRepairStart.attr("style", "display: inline;");
                    } else {
                        btnRepairStart.attr("style", "display: none;");
                    }
                } else {
                    btnRepairStart.attr("style", "display: none;");
                }
            }
        }

        //-----------------------------------//
        // Show Confirm WorkEnd Checkbox 
        //-----------------------------------//
        function ValidateEndDate(sender, senderID) {
            var btnRepairEnd = $("#" + sender.id.replace(senderID, "btnRepairEnd"));  //-- Find the button object
            var txtWorkStartDate = $("#" + sender.id.replace(senderID, "txtWorkStartDate"));  //-- Find the textbox object
            var txtWorkEndDate = $("#" + sender.id.replace(senderID, "txtWorkEndDate"));  //-- Find the textbox object
            //-- Validate
            if (btnRepairEnd) {
                if (txtWorkEndDate[0].value != "") {
                    var dtRepairEnd = new Date(txtWorkEndDate[0].value);
                    var dtToday = new Date();
                    if (dtRepairEnd <= dtToday)
                        btnRepairEnd.attr("style", "display: inline;");
                } else if (txtWorkEndDate[0].value == "" || txtWorkStartDate[0].value == "" || txtRepairStartDate.CCDisabled == true) {
                    btnRepairStart.attr("style", "display: none;");
                }
            }
        }
        //-----------------------------------//
        // SShow Confirm war Repair start 
        //-----------------------------------//
        function ValidateWarStartDate(sender, senderID) {
            var btnWarRepairStart = $("#" + sender.id.replace(senderID, "btnWarRepairStart"));  //-- Find the button object
            var txtWarRepairStartDate = $("#" + sender.id.replace(senderID, "txtWarRepairStartDate"));  //-- Find the textbox object
            //-- Validate
            if (btnWarRepairStart) {
                if (txtWarRepairStartDate[0].value != "") {
                    var btWarRepairStart = new Date(txtWarRepairStartDate[0].value);
                    var dtToday = new Date();
                    if (btWarRepairStart <= dtToday) {
                        btnWarRepairStart.attr("style", "display: inline;");
                    } else {
                        btnWarRepairStart.attr("style", "display: none;");
                    }
                } else {
                    btnWarRepairStart.attr("style", "display: none;");
                }
            }
        }

        //-----------------------------------//
        // SShow Confirm war Repair End 
        //-----------------------------------//
        function ValidateWarEndDate(sender, senderID) {
            var btnWarRepairEnd = $("#" + sender.id.replace(senderID, "btnWarRepairEnd"));  //-- Find the button object
            var txtWarRepairStartDate = $("#" + sender.id.replace(senderID, "txtWarRepairStartDate"));  //-- Find the textbox object
            var txtWarRepairEndDate = $("#" + sender.id.replace(senderID, "txtWarRepairEndDate"));  //-- Find the textbox object
            //-- Validate
            if (btnWarRepairEnd) {
                if (txtWarRepairEndDate[0].value != "") {
                    var btWarRepairEnd = new Date(txtWarRepairEndDate[0].value);
                    var dtToday = new Date();
                    if (btWarRepairEnd <= dtToday)
                        btnWarRepairEnd.attr("style", "display: inline;");
                } else if (txtWarRepairEndDate[0].value == "" || txtWarRepairSatrtDate[0].value == "" || txtWarRepairStartDate.CCDisabled == true) {
                    btnWarRepairEnd.attr("style", "display: none;");
                }
            }
        }

        //-----------------------------------//
        // Show Spinner - Dialog
        //-----------------------------------//
        $(function () {
            $("#diaLoading").dialog({
                autoOpen: false,
                modal: true,
                appendTo: "form",
                width: 370,
                height: 100,
                show: {
                    effect: "blind",
                    duration: 1000
                },
                hide: {
                    effect: "explode",
                    duration: 1000
                }
            });
        });

        //-----------------------------------//
        // Hide Spinner when document ready
        //-----------------------------------//
        $("#btnEditCarrier").on("click", function () {
            $("#diaCarrierRep").dialog("open");
        });

        function doAssignment(sender, senderID) {
            var resentFlag = 0;
            var assignmentFlag = '';
            var obtnSendAssignment = $("#" + sender.id.replace(senderID, senderID));

            if (obtnSendAssignment[0].value.toUpperCase() == 'RESEND ASSIGNMENT') {
                resendFlag = 1;
                var oCurrentCommunicationMethodID = $("#" + sender.id.replace("btnResendAssignment", "hidCurrentCommunicationMethodID"));
                var strServiceChannelCD = $("#" + sender.id.replace("btnResendAssignment", "hidServiceChannelCD"))[0].value;
                var ohidAssignmentAction = $("#" + sender.id.replace("btnResendAssignment", "hidAssignmentAction"));

                var strJobID = $("#" + sender.id.replace("btnResendAssignment", "hidJobID"))[0].value;
                var strJobStatus = $("#" + sender.id.replace("btnResendAssignment", "hidJobStatus"))[0].value;
                strJobStatus = strJobStatus.toUpperCase();

            } else {
                resendFlag = 0;
                var oCurrentCommunicationMethodID = $("#" + sender.id.replace("btnSendAssignment", "hidCurrentCommunicationMethodID"));
                var strServiceChannelCD = $("#" + sender.id.replace("btnSendAssignment", "hidServiceChannelCD"))[0].value;
                var ohidAssignmentAction = $("#" + sender.id.replace("btnSendAssignment", "hidAssignmentAction"));

                //- Normal Send Assignment -//
                var strAssignmentID = $("#" + sender.id.replace(senderID, "hidAssignmentID"));
                var strUserID = $("#" + sender.id.replace(senderID, "hidUserID"));

                strRequest = "<Root><Assignment assignmentID='" + strAssignmentID[0].value + "' userID='" + strUserID[0].value + "' CommunicationMethodID='" + oCurrentCommunicationMethodID[0].value + "' staffAppraiser='true' hqAssignment='true' /></Root>"
                XMLServerExecute("AssignShop.asp", strRequest);
            }

            if (oCurrentCommunicationMethodID[0].value == "17" && resendFlag == 1) {
                if (strJobStatus == "UNPROCESSED" || strJobStatus == "SENDING") {
                    var resp = confirm('Assignment already sent waiting to process. Do you want To send the fax assignment again? Select OK To send the fax and electronic assignment again or press cancel to resend only the electronic assignment.');
                    if (resp == true) {
                        FaxAssignment(strServiceChannelCD);
                    } else if (strJobStatus == "FAILED") {
                        var bResult;
                        var bResult = confirm('Do you want to send the fax? Select Ok to send fax and electronic assignment, press Cancel to send only the electronic assignment.');
                        if (bResult == true) {
                            ohidAssignmentAction[0].value = 'SENDFAXONLY|' + strJobID;
                            FaxAssignment(strServiceChannelCD);
                        } else {
                            ohidAssignmentAction[0].value = 'SENDFAXONLY|' + strJobID;
                        }
                    }
                } else if (strJobStatus == "PROCESSED") {
                    var respElec = confirm('Electronic assignment already sent.  Do you want to send the fax? Select Ok to send fax assignment.');
                    if (respElec == true) {
                        FaxAssignment(strServiceChannelCD);
                    }
                } else if (strJobStatus == "") {
                    SendAssignment(sender, senderID, resendFlag, assignmentFlag);
                }

                window.location.reload();
            } else
                SendAssignment(sender, senderID, resendFlag, assignmentFlag);
        }

        //-----------------------------------//
        // Call Classic ASP Shop Cancel
        //-----------------------------------//
        function doCancelAssignment(sender, senderID) {
            var strRequest = "";
            //alert("Cancelling Shop Assignment");

            var strAssignmentID = $("#" + sender.id.replace(senderID, "hidAssignmentID")).val();

            var strClaimAspectServiceChannelID = $("#" + sender.id.replace(senderID, "hidClaimAspectServiceChannelID")).val();
            var strUserID = $("#" + sender.id.replace(senderID, "hidUserID")).val();
            var strShopRemarks = escape($("#" + sender.id.replace(senderID, "txtAssignmentRemarks")).val());
            var strShopLocationID = escape($("#" + sender.id.replace(senderID, "hidShopLocationID")).val());
            var strUserID = $("#" + sender.id.replace(senderID, "hidUserID")).val();
            var strVehNum = $("#" + sender.id.replace(senderID, "hidVehNumber")).val();

            //----------------------------
            // Send Assignment Cancel
            //----------------------------
            strRequest = "<Root><Assignment assignmentID='" + strAssignmentID + "' userID='" + strUserID + "' staffAppraiser='true' hqAssignment='true' /></Root>"
            var oRetOBJ = XMLServerExecute("AssignShop.asp", strRequest);

           // alert(oRetOBJ.code);
        }

        //-----------------------------------//
        // Call Classic ASP Warranty Shop Cancel
        //-----------------------------------//
        function doWarCancelAssignment(sender, senderID) {
            //debugger;
            var strRequest = "";
            //alert("Cancelling Shop Assignment");

            var strWarAssignmentID = $("#" + sender.id.replace(senderID, "hidWarAssignmentID")).val();

            var strClaimAspectID = $("#" + sender.id.replace(senderID, "hidClaim_ClaimAspectID")).val();
            var strUserID = $("#" + sender.id.replace(senderID, "hidUserID")).val();
            var strShopRemarks = escape($("#" + sender.id.replace(senderID, "txtAssignmentRemarks")).val());
            var strShopLocationID = escape($("#" + sender.id.replace(senderID, "hidShopLocationID")).val());
            var strUserID = $("#" + sender.id.replace(senderID, "hidUserID")).val();
            var strVehNum = $("#" + sender.id.replace(senderID, "hidVehNumber")).val();

            //----------------------------
            // Send Assignment Cancel
            //----------------------------
            strRequest = "<Root><Assignment assignmentID='" + strWarAssignmentID + "' userID='" + strUserID + "' staffAppraiser='true' hqAssignment='true' /></Root>"
            var oRetOBJ = XMLServerExecute("AssignShop.asp", strRequest);

            //alert(oRetOBJ.code);
        }

        //-----------------------------------//
        // Call Classic ASP Shop Selection
        //-----------------------------------//
        function SelectShop(sender, senderID) {
            var btnSelectShop = $("#" + sender.id.replace(senderID, "btnSelectShop"));
            var objServiceChannelCD = $("#" + sender.id.replace(senderID, "hidServiceChannelCD"));
            var strServiceChannelCD = objServiceChannelCD[0].value
            var sShopRemarks = escape($("#" + sender.id.replace(senderID, "txtAssignmentRemarks")).val());
            var sZIP = sCTY = sST = "";

            var sAssignmentTo;
            switch (strServiceChannelCD) {
                case "DA":
                    sAssignmentTo = "LDAU";
                    break;
                case "ME":
                    sAssignmentTo = "IA";
                    break;
                case "PS":
                    sAssignmentTo = "SHOP";
                    break;
                case "CS":
                    sAssignmentTo = "SHOP";
                    break;
                case "RRP":
                    sAssignmentTo = "SHOP";
                    break;
            }

            var strLocationPhone = $("#" + sender.id.replace(senderID, "txtLocationPhone")).val();
            var strURL = "ShopLookup.asp?AreaCode=" + strLocationPhone.substring(1, 4) +
                            "&ExchangeNumber=" + strLocationPhone.substring(5, 8) + "&ZipCode=" + $("#" + sender.id.replace(senderID, "txtLocationZip")).val() + "&City=" + $("#" + sender.id.replace(senderID, "txtLocationCity")).val() + "&State=" + $("#" + sender.id.replace(senderID, "ddlLocationState")).val() +
                            "&UserID=" + $("#" + sender.id.replace(senderID, "hidUserID")).val() + "&LynxID=" + $("#" + sender.id.replace(senderID, "hidLynxID")).val() + "&VehicleNumber=" + $("#" + sender.id.replace(senderID, "hidVehNumber")).val() +
                            "&ClaimAspectID=" + $("#" + sender.id.replace(senderID, "hidVehicleClaimAspectID")).val() + "&ClaimAspectServiceChannelID=" + $("#" + sender.id.replace(senderID, "hidClaimAspectServiceChannelID")).val() + "&InsuranceCompanyID=" + $("#" + sender.id.replace(senderID, "hidInsuranceCompanyID")).val() +
                            "&LocationName=" + $("#" + sender.id.replace(senderID, "txtLocationName")).val() + "&AssignmentTo=" + sAssignmentTo +
                            "&getUser=false" + "&ShopRemarks=" + sShopRemarks;
            //alert(strURL);

            OpenDialogWindow(strURL, "ModalPopUp", "titlebar=no,status=no,location=no,toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=no,width=720px,height=400px,left = 120,top=100,center=yes;")

            return false;
        }

        function XMLServerExecute(sASPPage, sXMLRequest) {
            if (sASPPage != "" && sASPPage.toLowerCase().indexOf(".asp") == -1) {
                ClientWarning("Invalid remote page specified"); // need to change this to ClientError.
                return;
            }
            var retObj = {
                code: null,
                xml: null
            }

            var objXMLHTTP = new ActiveXObject("MSXML2.XMLHTTP");
            if (objXMLHTTP) {
                objXMLHTTP.open("POST", "/rs/" + sASPPage, false);
                objXMLHTTP.setRequestHeader("content-type", "application/x-www-form-urlencoded");
                objXMLHTTP.send(sXMLRequest);

                try {
                    if (objXMLHTTP.responseXML.xml != "") {

                        var oRetXML = objXMLHTTP.responseXML;
                        var oRetCode = oRetXML.documentElement.selectSingleNode("/Root/@returnCode");
                        var oRoot = oRetXML.documentElement

                        if (oRetCode && oRetCode.value != "0") {
                            var strUserInfo = "";
                            var aUserInfos = new Array();

                            var objUserInfo = {
                                code: oRetCode.value,
                                userText: oRoot.selectSingleNode("/Root/error/user").text,
                                advanced: oRoot.selectSingleNode("/Root/error/advanced").text
                            }

                            aUserInfos.push(objUserInfo);
                            //alert("User information count: " + aUserInfos.length);
                            var sURL = "/userMessage.htm";
                            var sFeatures = "dialogHeight:200px;dialogWidth:424px;center:yes;help:no;resizable:no;scroll:no;status:no"

                            window.showModalDialog(sURL, aUserInfos, sFeatures);
                            retObj.code = 1;
                        } else {
                            retObj.code = 0;
                            retObj.xml = objXMLHTTP.responseXML;
                            retObj.text = objXMLHTTP.responseText;
                            //alert(retObj.text);
                        }
                    } else {
                        retObj.code = 0;

                        try {
                            var oXML = new ActiveXObject("MSXML2.DOMDocument");
                            oXML.async = false;
                            oXML.loadXML(objXMLHTTP.responseText);

                            if (oXML.parseError.errorCode == 0) {
                                retObj.xml = oXML;
                            } else {
                                retObj.errorXML = oXML.parseError.reason;
                            }
                        } catch (e) { }

                        retObj.text = objXMLHTTP.responseText;
                    }
                } catch (e) { alert(e.description) }
            }

            return retObj;
        }

        function hideMask(sender, senderID) {
            $("#" + sender.id.replace(senderID, "btnNewServiceChannel")).hide();
        }

        function ToAlphaNumeric(strOriginal) {
            var strRet = unescape(strOriginal);
            if (strRet != "") {
                strRet = strRet.replace(/\W/g, ""); // \W will keep a-z, A-Z, 0-9 and _
                strRet = strRet.replace(/[_]/g, ""); // now remove the underscore
            }
            return strRet;
        }

        var bMouseInToolTip = true;
        function showToolTip(sender, senderID) {
            try {
                $("#" + sender.id.replace(senderID, "divShopDetail")).css("display", "block");
               // divShopDetail.style.display = "inline";
            } catch (e) { }
        }
        function hideToolTip() {
            if (bMouseInToolTip == true) {
                $("[id$=divShopDetail]").css("display","none");
            }
        }

        //--- This code refreshes the notes window ---//
        //top.refreshNotesWindow(top.vLynxID, top.vUserID);

        //--- This code refreshes the diary window ---//
        //top.refreshCheckListWindow(top.vLynxID, top.vUserID);

        //$("#diaLoading").hide(1000, "blind");

        function reloadPage() {
            if (parent.stat1) {
                $("#diaLoading").show(1000, "blind");
            }
            window.setTimeout("reloadPage2()", 200);
        }

        function reloadPage2() {
            if (typeof (top.refreshNotesWindow) == "function")
                top.refreshNotesWindow(top.vLynxID, top.vUserID);
            if (typeof (top.refreshCheckListWindow) == "function")
                top.refreshCheckListWindow(top.vLynxID, top.vUserID);
            document.location.reload();
        }


       function disableButtons(sender, senderID, bDisabled) {
            var objAssign = $("#" + sender.id.replace(senderID, "btnSendAssignment"));
            if (objAssign)
                objAssign[0].disabled = bDisabled;

            var objSelectShop = $("#" + sender.id.replace(senderID, "btnSelectShop"));
            if (objSelectShop)
                objSelectShop[0].disabled = bDisabled;

            if (typeof (parent.disableButton) == "function") {
                var objbtnHistory = $("#" + sender.id.replace(senderID, "btnHistory"));
                if (objbtnHistory)
                    objbtnHistory[0].disabled = bDisabled;

                var objbtnSaveAssignment = $("#" + sender.id.replace(senderID, "btnSaveAssignment"));
                if (objbtnSaveAssignment)
                    objbtnSaveAssignment[0].disabled = bDisabled;
            }
            return false;
        }

        function ShopOnClick(sender, senderID) {
            var ohidServiceChannelCD = $("#" + sender.id.replace(senderID, "hidServiceChannelCD"));
            var ohidAssignmentTypeCD = $("#" + sender.id.replace(senderID, "hidAssignmentTypeCD"));
            var ohidShopID = $("#" + sender.id.replace(senderID, "hidShopID"));
            var ohidBusinessID = $("#" + sender.id.replace(senderID, "hidBusinessID"));

            if (ohidAssignmentTypeCD[0].value == "SHOP" || ohidServiceChannelCD[0].value == "RRP")
                NavToShop("S", ohidShopID[0].value, ohidBusinessID[0].value);
            return false;
        }

        function NavToShop(sSearchType, sEntityID, sShopBusinessID) {
            // The parameter 'sShopBusinessID' is the Business (utb_shop) to which the Shop (utb_shop_location) belongs.  This is only needed for 
            // SearchType 'S'.  This is so the Business ID can be used in the window name rather than the ShopID.  This will prevent opening multiple 
            // sibling shops in different windows and then being able to change tabs in those windows possibly resulting in multiple windows with 
            // the same shop.  
            try {
                var sSettings = 'width=1012,height=670,top=0,left=0,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
                var sUrl = "ProgramMgr/SMT.asp?SearchType=" + sSearchType + "&EntityID=" + sEntityID;
                var sWinName;

                switch (sSearchType) {
                    case "S":
                        sWinName = "SMTFrame_B" + sShopBusinessID;
                        break;
                    case "B":
                        sWinName = "SMTFrame_B" + sEntityID;
                        break;
                    case "D":
                        sWinName = "SMTFrame_D" + sEntityID;
                        break;
                    case "BusinessWiz":
                    case "ShopWiz":
                    case "DealerWiz":
                        ShowWizard(sSearchType);
                        return;
                        break;
                    default:
                        throw "Invalid value was passed for sSearchType.  Value must be one of the following: 'S', 'B', or 'D'.";
                }

                if (sUrl != "") {
                    var oWin = window.open(sUrl, sWinName, sSettings);
                    oWin.oCallingWin = top.window.self;  // send the new window a handle to this window.
                    oWin.focus();
                    //alert(oWin.oCallingWin.name);      
                }
            } catch (e) {
                throw "VehicleShopAssignment.xsl:NavToShop( sSearchType='" + StrVal(sSearchType)
                                            + "', sEntityID='" + StrVal(sEntityID)
                                            + "', sShopBusinessID='" + StrVal(sShopBusinessID)
                                            + "' ) Error: " + e.message;
            }
        }

        function getLocationInfo(sender, senderID) {
            var strServiceChannelCD = $("#" + sender.id.replace(senderID, "hidServiceChannelCD"))[0].value;

            var strState;
            var strCity;
            var strCounty;
            if (strServiceChannelCD == "DA" || strServiceChannelCD == "DR") {
                //?????????????? Need to get DA missing fields in place ??????????????????????//
                //strState = $("#" + sender.id.replace(senderID, "ddlLocationState")).selectedOptions[0].text;
                strCity = $("#" + sender.id.replace(senderID, "txtLocationCity"))[0].value;
                strCounty = $("#" + sender.id.replace(senderID, "txtLocationCounty"))[0].value;

            } else {
                strState = $("#" + sender.id.replace(senderID, "hidShopLocationState"))[0].value;
                strCity = $("#" + sender.id.replace(senderID, "hidShopLocationCity"))[0].value;
                strCounty = $("#" + sender.id.replace(senderID, "hidShopLocationCounty"))[0].value;
            }
            return { InsuranceCompanyID: gsInsuranceCompanyID, InsuranceCompanyName: strInsuranceCompanyName, State: strState, City: strCity, County: strCounty, ShopLocationID: gsShopId, ServiceChannelCD: strServiceChannelCD };
        }

        function navigate(sender, senderID) {
            //debugger;
            var iLynxID = $("#" + sender.id.replace(senderID, "hidLynxID"))[0].value;
            var iClaimAspectServiceChannelID = $("#" + sender.id.replace(senderID, "hidClaimAspectServiceChannelID"))[0].value;

            //var strReq = "HQAssignmentDetails.aspx?LynxID=" + iLynxID + "&UserID=" + iUserID + "&ClaimAspectID=" + iClaimAspectID + "&ClaimAspectServiceChannelID=" + iClaimAspectServiceChannelID + "&InsuranceCompanyID=" + iInsuranceCompanyID;
            var strReq = "HQAssignmentDetails.aspx?LynxID=" + iLynxID + "&ClaimAspectServiceChannelID=" + iClaimAspectServiceChannelID;

            var strWinName = <%= sLynxID %> + " - Assignment Details";
            var strSettings = "dialogHeight:200px; dialogWidth:700px; resizable:no; status:no; help:no; center:yes;";
            var strReturn = window.open(strReq, strWinName, strSettings);

            return false;
        }

<%--            $(document).ready(function(){
                $("#divCoverageScroll table tbody tr").dblclick(function(){
                    debugger;
                   var id = $(this).find("td:first").text();
                   alert(id);
                   //doSavCoverage("SavCoverage.aspx", "?LynxID=<%=sLynxID %>&InscCompID=<%=sInscCompany %>&UserID=<%=sUserID %>&Params=<%=sParams %>","ModalPopUp","toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=350px,height=280px,left = 490,top=300,center=1,help=0,scroll=0,status=0;"
                   //window.location = "<%#ResolveUrl("~/Default2.aspx")%>?record=" + id;
               });
            });--%>

        var parseUrlParameter = function parseUrlParameter(strParams, strParamTag) {
            var sPageURL = strParams,
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === strParamTag) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

       <%-- $(function () {
            var DELAY = 700, clicks = 0, timer = null;
            $("[id*=gvCoverage] tr:has(td)").on("click", function (e) {
                var row = $(this);
                //debugger;

                // Reset all highlighted
                $('td').each(function (index, value) {
                    $(this).removeClass('highlight');
                })

                // React to Click
                clicks++;
                if (clicks === 1) {
                    timer = setTimeout(function () {
                         row.find("td").addClass("highlight");
                        clicks = 0;
                    }, DELAY);
                } else {
                    // React to dbl Click
                    clearTimeout(timer);
               }
            }).on("dblclick", function (e) {
                //debugger;
                var row = $(this);
                row.find("td").addClass("highlight");
                    clicks = 0;

                    var $row = jQuery(this).closest('tr');
                    var $columns = $row.find('td');
                    var values = "";
                    var arrParams = new Array(8);
                    
                    jQuery.each($columns, function (i, item) {
                        arrParams[i] = item.innerText;
                    });

                    var strParams = '?LynxID=<%=sLynxID %>&InscCompID=<%=sInscCompany %>&UserID=<%=sUserID %>' + '&Description=' + arrParams[0] + '&Type=' + arrParams[1] + '&Additional=' + arrParams[2] + '&Deductable=' + arrParams[3] + '&Limit=' + arrParams[4] + '&MaxDays=' + arrParams[5] + '&DayLimit=' + arrParams[6];
                    //alert(strParams);
                    doSavCoverage("SavCoverage.aspx", strParams, "ModalPopUp", "toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=350px,height=280px,left = 490,top=300,center=1,help=0,scroll=0,status=0;");
               
                //e.preventDefault();
            });
        });--%>

        function addCoverageApplied(strURL, strParams, strWindowStyle, strWindowParams) {
            OpenDialogWindow(strURL + strParams, strWindowStyle, strWindowParams)
            return false;
        }

        function editCoverageApplied(strURL, strParams, strWindowStyle, strWindowParams) {
            var SelectedClaimCoverageID = $("#hidSelectedClientCoverageTypeID").val();
            var SelectedClientCoverageTypeID = $("#hidSelectedClaimCoverageID").val();
            if ((SelectedClaimCoverageID == 0 || SelectedClaimCoverageID == "") && (SelectedClientCoverageTypeID == 0 || SelectedClientCoverageTypeID == "")) {
                alert("Please select the coverage description from the list.");
                return false;
            }
            OpenDialogWindow(strURL + strParams, strWindowStyle, strWindowParams)
            return false;
        }

    </script>

    <style type="text/css">
        #diaLoading {
            width: 100%;
            height: 100%;
            top: 0px;
            left: 0px;
            position: fixed;
            display: block;
            opacity: 0.7;
            background-color: #fff;
            z-index: 99;
            text-align: center;
        }

        #imgLoading {
            position: absolute;
            top: 171px;
            left: 350px;
            z-index: 100;
        }
        /* Hide Title bar and close button for Jquery UI dialog
            ----------------------------------*/

        .ui-dialog-titlebar-close {
            visibility: hidden;
        }

        .ui-dialog-titlebar {
            display: none;
        }
    </style>

</head>
<body onload="">
    <form id="frmClaimView" runat="server">
        <!-- Claim info tab started here -->
        <div class="tab" style="position: absolute; top: 2px; left: 2px; right: 0px;">
            <button class="tablinks active" onclick="MenuTab(event,'tablinks','divClaim','TabClaimContent');return false;">
                Claim</button>
            <button class="tablinks" onclick="MenuTab(event, 'tablinks', 'divInsured', 'TabClaimContent');return false;">
                Insured</button>
            <button class="tablinks" onclick="MenuTab(event, 'tablinks', 'divCoverage', 'TabClaimContent');return false;">
                Coverage</button>
            <button class="tablinks" onclick="MenuTab(event, 'tablinks', 'divCarrier', 'TabClaimContent');return false;">
                Carrier</button>
        </div>

        <!-- Hidden Vars -->
        <asp:HiddenField ID="hidClaimSysLastUpdatedDate" runat="server" />
        <asp:HiddenField ID="hidInsuranceCompanyID" runat="server" />
        <asp:HiddenField ID="hidUserID" runat="server" />
        <asp:HiddenField ID="hidRepFname" runat="server" />
        <asp:HiddenField ID="hidRepLname" runat="server" />
        <asp:HiddenField ID="hidLynxID" runat="server" />
        <asp:HiddenField ID="hidClaimID" runat="server" />
        <asp:HiddenField ID="hidClaimIDSquished" runat="server" />
        <asp:HiddenField ID="hidInsFname" runat="server" />
        <asp:HiddenField ID="hidInsLname" runat="server" />
        <asp:HiddenField ID="hidLoss" runat="server" />
        <asp:HiddenField ID="hidRestrictedFlag" runat="server" />
        <asp:HiddenField ID="hidClaimOpen" runat="server" />
        <asp:HiddenField ID="hidClaimStatus" runat="server" />
        <asp:HiddenField ID="hidDemoFlag" runat="server" />
        <asp:HiddenField ID="hidInsCo" runat="server" />
        <asp:HiddenField ID="hidDocumentCRUD" runat="server" />
        <asp:HiddenField ID="hidClaimCRUD" runat="server" />

        <asp:HiddenField ID="hidReOpenExpCRUD" runat="server" />
        <asp:HiddenField ID="hidCloseExpCRUD" runat="server" />
        <asp:HiddenField ID="hidTransferClaimExpCRUD" runat="server" />
        <asp:HiddenField ID="hidReassignExpCRUD" runat="server" />
        <asp:HiddenField ID="hidCancelExpCRUD" runat="server" />
        <asp:HiddenField ID="hidVoidExpCRUD" runat="server" />
        <asp:HiddenField ID="hidServiceChannelCRUD" runat="server" />

        <asp:HiddenField ID="hidClaim_ClaimAspectID" runat="server" />
        <asp:HiddenField ID="hidWindowID" runat="server" />
        <asp:HiddenField ID="hidRepActiveFlag" runat="server" />
        <asp:HiddenField ID="hidVehicleList" runat="server" />
        <asp:HiddenField ID="hidCoverageSelected" runat="server" />
        <asp:HiddenField ID="hidInsInvolvedID" runat="server" />
        <asp:HiddenField ID="hidInsFedTaxID" runat="server" />
        <asp:HiddenField ID="hidInsSysLastUpdatedDate" runat="server" />
        <asp:HiddenField ID="hidVehCount" runat="server" />

        <!-- Claim Section Start -->
        <div id="divClaim" class="TabClaimContent showDefaultTab">
            <!-- Claim Header Bar -->
            <table style="height: 160px; width: 785px; border-color: #667; border-spacing: 0px;">
                <tr style="vertical-align: top;">
                    <td>
                        <!-- Claim Details -->
                        <div id="divClaimScroll" class="Claim" style="width: 760px; height: 95px; overflow: auto scroll;">
                            <table class="topTable" cellpadding="0" cellspacing="0" style="border-collapse: separate; border-spacing: 0px; width: 710px;">
                                <tr style="vertical-align: top;">
                                    <td>
                                        <table class="topTable" style="border-spacing: 0px; width: 330px;">
                                            <tr style="vertical-align: top;">
                                                <td>Policy:
                                                </td>
                                                <td>
                                                    <asp:TextBox class="Claim" ID="txtPolicyNumber" runat="server" CssClass="inputField"
                                                        Width="170px" MaxLength="20"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;">
                                                <td>Client Claim #:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtClientClaimNumber" runat="server" CssClass="inputField" Width="200px"
                                                        MaxLength="30"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;">
                                                <td>Date of Loss:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLossDate" runat="server" CssClass="inputField"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;">
                                                <td>State:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlState" runat="server" CssClass="inputField">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;">
                                                <td>Loss Description:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLossDescription" runat="server" CssClass="inputField" Height="35px"
                                                        Width="200" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="topTable" style="border-spacing: 0px; width: 355px;">
                                            <tr style="vertical-align: top;">
                                                <td>Submitted By:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSubmittedBy" runat="server" CssClass="inputField" Width="240px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;">
                                                <td>Submitted Date:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtIntakeFinishDate" runat="server" CssClass="inputField"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;">
                                                <td>Claim Remarks:
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                        <table class="topTable" style="border-spacing: 0px; width: 250px;">
                                            <tr style="vertical-align: top;">
                                                <td>
                                                    <asp:TextBox ID="txtRemarks" runat="server" Height="62px" Width="310px" MaxLength="1000"
                                                        TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <asp:Button runat="server" ID="btnClaimSave" CssClass="formbutton" Text="Save" />
                            <asp:Label runat="server" ID="lblClaimSaved" ForeColor="Blue" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <!-- Insured Section Start -->
        <div id="divInsured" class="TabClaimContent">
            <!-- Insured Header Bar -->
            <table style="height: 160px; width: 785px; border-color: #667; border-spacing: 0px; border: 0px 0px 0px 0px;">
                <tr style="vertical-align: top;">
                    <td>
                        <!-- Insured Details -->
                        <div id="divInsuredScroll" class="Claim" style="width: 760px; height: 95px; overflow: auto scroll;">
                            <table class="topTable" style="border-spacing: 0px; width: 710px; border: 0px 0px 0px 0px;">
                                <tr style="vertical-align: top;">
                                    <td>
                                        <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0"
                                            border="0">
                                            <tr style="vertical-align: top;">
                                                <td>
                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0"
                                                        border="0">
                                                        <tr align="left" style="vertical-align: top;">
                                                            <td align="left" style="width: 74px;">Name:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtNameTitle" runat="server" CssClass="inputField" Width="30px"
                                                                    MaxLength="15"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtNameFirst" runat="server" CssClass="inputField" Width="100px"
                                                                    MaxLength="50"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtNameLast" runat="server" CssClass="inputField" Width="100px"
                                                                    MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="topTable" style="border-spacing: 0px; border: 0px 0px 0px 0px;">
                                            <tr style="vertical-align: top;">
                                                <td style="width: 75px;">Business:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtBusinessName" runat="server" CssClass="inputField" Width="234px"
                                                        MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;">
                                                <td style="width: 75px;">Address:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAddress1" runat="server" CssClass="inputField" Width="234px"
                                                        MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;">
                                                <td style="width: 75px;">&nbsp;
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="inputField" Width="234px"
                                                        MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;">
                                                <td style="width: 75px;">City:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAddressCity" runat="server" CssClass="inputField" Width="190px"
                                                        MaxLength="30"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="topTable" style="border-spacing: 0px; width: 312px;">
                                            <tr style="vertical-align: top;">
                                                <td style="width: 75px;">State:
                                                </td>
                                                <td style="width: 15px;">
                                                    <asp:DropDownList ID="ddlAddressState" runat="server" CssClass="inputField">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 15px;">&nbsp;Zip:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAddressZip" runat="server" CssClass="inputField" Width="72px"
                                                        MaxLength="8"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Button runat="server" ID="btnInsuredSave" OnClick="btnInsuredSave_Click" CssClass="formbutton" Text="Save" />
                                        <asp:Label runat="server" ID="lblInsuredSave" ForeColor="Blue" />
                                    </td>
                                    <td>
                                        <table class="topTable" style="border-spacing: 0px; width: 350px;">
                                            <tr style="vertical-align: top;">
                                                <td style="width: 75px;">E-mail:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="inputField" Width="236px"
                                                        MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;">
                                                <td style="width: 75px;">SSN/EIN:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFedTaxID" runat="server" CssClass="inputField" Width="100px"
                                                        MaxLength="15"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;">
                                                <td style="width: 25px;">&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="topTable" style="border-spacing: 0px; width: 360px;">
                                            <tr style="vertical-align: top;">
                                                <td style="width: 200px;">Best Number to Call:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlBestContactPhoneCD" runat="server" CssClass="inputField">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>&nbsp;Day:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDay" runat="server" CssClass="inputField" Width="115px" MaxLength="50"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="txtDayFieldValidator" runat="server" ControlToValidate="txtDay" ErrorMessage="Format (nnn)nnn-nnnn" ForeColor="Red" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;">
                                                <td style="width: 75px;">Best Time to Call:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtBestContactTime" runat="server" CssClass="inputField" Width="90px"
                                                        MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;Night:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNight" runat="server" CssClass="inputField" Width="115px" MaxLength="50"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="txtNightFieldValidator" runat="server" ControlToValidate="txtNight" ErrorMessage="Format (nnn)nnn-nnnn" ForeColor="Red" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;">
                                                <td style="width: 25px;">&nbsp;
                                                </td>
                                                <td style="width: 125px;">&nbsp;
                                                </td>
                                                <td>&nbsp;Alt:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAlternate" runat="server" CssClass="inputField" Width="110px"
                                                        MaxLength="50"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="txtAlternateFieldValidator" runat="server" ControlToValidate="txtAlternate" ErrorMessage="Format (nnn)nnn-nnnn" ForeColor="Red" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <!-- Coverage info tab started here -->
        <div id="divCoverage" class="TabClaimContent">
            <!-- Coverage Header Bar -->
            <table style="height: 160px; width: 785px; border-color: #667; border-spacing: 0px;">
                <tr style="vertical-align: top;">
                    <td>
                        <!-- Coverage Details -->
                        <div id="divCoverageScroll" style="width: 760px; height: 95px; overflow: auto scroll;">
                            <table class="topTable" style="border-spacing: 0px; width: 710px;">
                                <tr style="vertical-align: top;">
                                    <td>
                                        <table class="topTable" style="border-spacing: 0px; width: 680px; height: 135px;">
                                            <tr style="vertical-align: top;">
                                                <td>
                                                    <asp:GridView ID="gvCoverage" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma"
                                                        Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None"
                                                        BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnRowDataBound="gvCoverage_RowDataBound">
                                                        <AlternatingRowStyle BackColor="#F7F7F7" />
                                                        <Columns>
                                                            <asp:BoundField DataField="ClaimCoverageID" HeaderText="" SortExpression=""
                                                                HeaderStyle-Width="1px" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="ClientCoverageTypeID" HeaderText="ClientCoverageTypeID" SortExpression="ClientCoverageTypeID"
                                                                HeaderStyle-Width="1px" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="CoverageSysLastUpdatedDate" HeaderText="CoverageSysLastUpdatedDate" SortExpression="CoverageSysLastUpdatedDate"
                                                                HeaderStyle-Width="1px" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:TemplateField HeaderText="Description" SortExpression="Description"
                                                                HeaderStyle-Width="400px" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <%# Eval("CoverageTypeCD") + " - " + Eval("Description")%>
                                                                    <asp:HiddenField ID="hidCoverageDescription" runat="server" Value='<%#Eval("Description")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="CoverageTypeCD" HeaderText="Type" SortExpression="CoverageTypeCD"
                                                                HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:ImageField DataImageUrlField="AddtlCoverageFlag" HeaderText="Addl."></asp:ImageField>
                                                            <asp:BoundField DataField="DeductibleAmt" HeaderText="Deductable" SortExpression="DeductibleAmt"
                                                                HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="LimitAmt" HeaderText="Limit" SortExpression="LimitAmt"
                                                                HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="MaximumDays" HeaderText="Max. Days" SortExpression="MaximumDays"
                                                                HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="LimitDailyAmt" HeaderText="Daily Limit" SortExpression="LimitDailyAmt"
                                                                HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                        </Columns>
                                                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                        <HeaderStyle BackColor="#629ad6" Font-Bold="True" ForeColor="#F7F7F7" />
                                                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                                    </asp:GridView>
                                                    <asp:HiddenField ID="hidSelectedClaimCoverageID" runat="server" Value='' />
                                                    <asp:HiddenField ID="hidSelectedClientCoverageTypeID" runat="server" Value='' />
                                                    <asp:HiddenField ID="hidCoverageSysLastUpdatedDate" runat="server" Value='' />
                                                </td>
                                            </tr>
                                        </table>
                                        <button type="button" id="btnAddCoverage" class="formbutton" onclick='doAddCoverage("AddCoverage.aspx", "?LynxID=<%=sLynxID %>&InscCompID=<%=sInscCompany %>&UserID=<%=sUserID %>&SelectedClaimCoverageID=<%=sSelectedClaimCoverageID %>&SelectedClientCoverageTypeID=<%=sSelectedClientCoverageTypeID %>&CoverageSysLastUpdatedDate=<%=sCoverageSysLastUpdatedDate %>", "ModalPopUp", "titlebar=no,status=no,location=no,toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=no,width=350px,height=280px,left = 490,top=300,center=yes;" )'>Add </button>
                                        <button type="button" id="btnDelCoverage" class="formbutton" onclick='doDelCoverage("DelCoverage.aspx", "?LynxID=<%=sLynxID %>&InscCompID=<%=sInscCompany %>&UserID=<%=sUserID %>","ModalPopUp","toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=360px,height=120px,left = 490,top=300,center=1,help=0,scroll=0,status=0;")'>Del </button>
                                        <%--                                                    <button type="button" id="btnSavCoverage" onclick='OpenDialogWindow("SavCoverage.aspx?LynxID=<%=sLynxID %>&InscCompID=<%=sInscCompany %>&UserID=<%=sUserID %>&Params=<%=sParams %>","ModalPopUp","toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=350px,height=280px,left = 490,top=300,center=1,help=0,scroll=0,status=0;")'>Save </button>--%>
                                        <button type="button" id="btnSavCoverage" class="formbutton">Save </button>
                                        <asp:Label runat="server" ID="lblCoverageStatus" ForeColor="Blue" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <!-- Coverage info tab Ended here -->
        <!-- Carrier info tab started here -->
        <div id="divCarrier" class="TabClaimContent">
            <table style="height: 160px; width: 785px; border-color: #666; border-spacing: 0px;">
                <tr style="vertical-align: top;">
                    <td>
                        <!-- Carrier Details -->
                        <div id="divCarrierScroll" style="width: 760px; height: 95px; overflow: auto scroll;">
                            <table class="topTable" style="border-spacing: 0px; width: 700px;">
                                <tr style="vertical-align: top;">
                                    <td>
                                        <table class="topTable" style="border-spacing: 0px; width: 760px;">
                                            <tr style="vertical-align: top;">
                                                <td>
                                                    <table class="topTable" style="border-spacing: 0px; width: 350px;">
                                                        <tr style="vertical-align: top;">
                                                            <td style="width: 85px;">Name:
                                                            </td>
                                                            <td>
                                                                <img id="btnEditCarrier" src="images/next_button.gif" alt="Edit Carrier" onclick='OpenDialogWindow("UpdateCarrierRep.aspx?LynxID=<%=sLynxID %>&InscCompID=<%=sInscCompany %>&UserID=<%=sUserID %>&CurrentOfficeID=<%=sCurrentOfficeID %>&CarrierRepUserID=<%=sCarrierRepUserID %>&CarrierRepSysLastUpdatedDate=<%=sCarrierRepSysLastUpdatedDate %>","ModalPopUp","toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=380px,height=120px,left = 490,top=300,center=yes;")' />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCarrierNameTitle" runat="server" CssClass="inputField" Width="30px" Enabled="false"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCarrierNameFirst" runat="server" CssClass="inputField" Width="100px" Enabled="false"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCarrierNameLast" runat="server" CssClass="inputField" Width="100px" Enabled="false"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table class="topTable" style="border-spacing: 0px; width: 760px;">
                                                        <tr style="vertical-align: top;">
                                                            <td style="width: 65px;">Office:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCarrierOfficeName" runat="server" CssClass="inputField" Width="450px" Enabled="false"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr style="vertical-align: top;">
                                                            <td style="width: 65px;">
                                                                <asp:LinkButton runat="server" ID="lnkEmail" ToolTip="Click to E-mail carrier representative." Text="E-mail:" OnClientClick="return emailCarrier()" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCarrierEmailAddress" runat="server" CssClass="inputField" Width="450px" Enabled="false"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr style="vertical-align: top;">
                                                            <td style="width: 65px;">Phone:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCarrierPhone" runat="server" CssClass="inputField" Width="200px" Enabled="false"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr style="vertical-align: top;">
                                                            <td style="width: 65px;">Fax:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCarrierFax" runat="server" CssClass="inputField" Width="200px" Enabled="false"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <!-- Carrier info tab Ended here -->

        <!-- =============================================== VEHICLE =========================================================== -->
        <hr />

        <!-- ==================== Page Loading - Modal Dialog ============================== -->
        <div id="diaLoading" title="Page Loading" style="display: block; height: 5px;">
            <asp:Image ID="imgLoading" runat="server" ImageUrl="images/ajax-loader.gif" />
        </div>

        <!-- ==================== Repair Change Reason - Modal Dialog ============================== -->
        <div id="diaRepairChangeReason" title="Repair Change Reason" style="display: none;">
            <table class="topTable" style="border-spacing: 0px;">
                <tr style="vertical-align: top;">
                    <td>
                        <b>LynxID:</b>
                    </td>
                    <td>
                        <asp:Label ID="lblLynxID" runat="server" CssClass="inputField" Width="20"></asp:Label>
                        <asp:HiddenField ID="curClaimAspectServiceChannelID" runat="server" />
                        <asp:HiddenField ID="curRepeaterID" runat="server" />
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td>
                        <b>Reason:</b>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlRepairReason" runat="server" CssClass="inputField" Width="150" AutoPostBack="false" onchange="checkOther(this)"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td><b>Other:</b>
                    </td>
                    <td>
                        <asp:TextBox ID="txtOther" runat="server" CssClass="inputField" MaxLength="150" Width="170" Height="75" Enabled="false" TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnReasonOk" CssClass="formbutton" runat="server" Text="Ok" OnClientClick="javascript:doOk(this, 'btnReasonOk'); return false;" />
                    </td>
                </tr>
            </table>
        </div>

        <!-- ==================== Add Coverage - Modal Dialog ============================== -->
        <div id="diaAddCoverage" title="Add Coverage" style="display: none;">
            <!-- Hidden Holders -->
            <asp:HiddenField ID="HIDCoverageParams" runat="server" />

            <table class="topTable" style="border-spacing: 0px; width: 350px;">
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Client Code:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageClientCode" runat="server" CssClass="inputField" Width="50px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Description:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCoverageDescription" runat="server" CssClass="inputField" Width="175px"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Type:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageType" runat="server" CssClass="inputField" Width="50px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Additional Flag:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageAdditionalFlag" runat="server" CssClass="inputField" Width="175px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Deductible:  $
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageDeductible" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Limit:  $
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageLimit" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                    </td>
                </tr>
                <tr id="divRentalMax" style="vertical-align: top;">
                    <td style="width: 85px;">Rental Max Days:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageMaxDays" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                    </td>
                </tr>
                <tr id="divRentalLimit" style="vertical-align: top;">
                    <td style="width: 85px;">Rental Limit:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageDailyLimit" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td>&nbsp;</td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnCoverageAddSave" runat="server" Text="Save" />
                        <asp:Button runat="server" ID="btnCoverageAddCancel" CssClass="formbutton" Text="Cancel" OnClientClick="" />
                    </td>
                </tr>
            </table>
        </div>

        <!-- ==================== Edit Coverage - Modal Dialog ============================== -->
        <div id="diaEditCoverage" title="Edit Coverage" style="display: none;">
            <table class="topTable" style="border-spacing: 0px; width: 350px;">
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Client Code:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEditCoverageClientCode" runat="server" CssClass="inputField" Width="50px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Description:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEditCoverageName" runat="server" CssClass="inputField" Width="175px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Type:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEditCoverageType" runat="server" CssClass="inputField" Width="50px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Additional Flag:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEditCoverageAdditionalFlag" runat="server" CssClass="inputField" Width="175px"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Deductible:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEditCoverageDeductible" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Limit:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEditCoverageLimit" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td>&nbsp;</td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">&nbsp;
                    </td>
                    <td>
                        <asp:Button runat="server" ID="btnEditCoverageSave" CssClass="formbutton" Text="Save" />
                        <asp:Button runat="server" ID="btnEditCoverageCancel" CssClass="formbutton" Text="Cancel" />
                    </td>
                </tr>
            </table>
        </div>

        <!-- ==================== Modal Dialog Block ============================== -->
        <div id="diaModal" title="Dialog Open" style="display: none;">
            <p>
                A modal dialog is open
            </p>
            <asp:Button runat="server" ID="chkDialogWindow" Text="Yes" />
        </div>

        <!-- ==================== Del Coverage - Modal Dialog ============================== -->
        <%--    <div id="diaDelCoverage" title="Delete Coverage" style="display: none;">
        <p>
            Do you want to delete the selected coverage?  Click Yes to delete.
        </p>
        <asp:Button runat="server" ID="btnDelCoverageYes" Text="Yes" />
        <asp:Button runat="server" ID="btnDelCoverageNo" Text="No" />
        <asp:Button runat="server" ID="btnDelCoverageCancel" Text="Cancel" />
    </div>--%>


        <!-- ==================== NADA - Modal Dialog ============================== -->
        <div id="diaNADA" title="NADA Services" style="display: none;">
            <table class="topTable" style="border-spacing: 0px; width: 400px;">
                <tr style="vertical-align: top;">
                    <td style="width: 35px;" align="left">Region:
                    </td>
                    <td style="width: 85px;" align="left">
                        <asp:DropDownList ID="DropDownList2" runat="server" CssClass="inputField" Width="125px"></asp:DropDownList>
                    </td>
                    <td style="width: 150px;" align="right">Insured State/County:
                    </td>
                    <td style="width: 85px;">
                        <asp:TextBox ID="TextBox1" runat="server" CssClass="inputField" Width="75px"></asp:TextBox>
                    </td>
                </tr>
            </table>

            <%--NADA Returned Data--%>
            <table class="topTable" style="border-spacing: 0px; width: 400px;">
                <tr style="vertical-align: top;">
                    <td>
                        <hr />
                    </td>
                </tr>
            </table>

            <table class="topTable" style="border-spacing: 0px; width: 400px;">
                <tr style="vertical-align: top;">
                    <td style="width: 20px;">VIN:
                    </td>
                    <td style="width: 85px;">
                        <asp:TextBox ID="TextBox6" runat="server" CssClass="inputField" Width="150px"></asp:TextBox>
                    </td>
                    <td style="width: 20px;">
                        <button id="btnDecode" class="formbutton">Decode</button>
                    </td>
                    <td style="width: 85px;">&nbsp;
                    </td>
                </tr>
            </table>

            <table class="topTable" style="border-spacing: 0px; width: 400px;">
                <tr style="vertical-align: top;">
                    <td>
                        <hr />
                    </td>
                </tr>
            </table>

            <table class="topTable" style="border-spacing: 0px; width: 400px;">
                <tr style="vertical-align: top;">
                    <td style="width: 20px;">Year:
                    </td>
                    <td style="width: 85px;">
                        <asp:TextBox ID="TextBox3" runat="server" CssClass="inputField" Width="50px" Enabled="false"></asp:TextBox>
                    </td>
                    <td style="width: 20px;">Year:
                    </td>
                    <td style="width: 85px;">
                        <asp:DropDownList ID="DropDownList1" runat="server" CssClass="inputField" Width="75px"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 20px;">Year:
                    </td>
                    <td style="width: 85px;">
                        <asp:TextBox ID="TextBox2" runat="server" CssClass="inputField" Width="50px" Enabled="false"></asp:TextBox>
                    </td>
                    <td style="width: 20px;">Make:
                    </td>
                    <td style="width: 85px;">
                        <asp:DropDownList ID="DropDownList3" runat="server" CssClass="inputField" Width="75px"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 20px;">Model:
                    </td>
                    <td style="width: 85px;">
                        <asp:TextBox ID="TextBox4" runat="server" CssClass="inputField" Width="100px" Enabled="false"></asp:TextBox>
                    </td>
                    <td style="width: 20px;">Model:
                    </td>
                    <td style="width: 85px;">
                        <asp:DropDownList ID="DropDownList5" runat="server" CssClass="inputField" Width="175px"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 20px;">Body:
                    </td>
                    <td style="width: 85px;">
                        <asp:TextBox ID="TextBox5" runat="server" CssClass="inputField" Width="100px" Enabled="false"></asp:TextBox>
                    </td>
                    <td style="width: 20px;">Body:
                    </td>
                    <td style="width: 85px;">
                        <asp:DropDownList ID="DropDownList6" runat="server" CssClass="inputField" Width="175px"></asp:DropDownList>
                    </td>
                </tr>
            </table>

            <table class="topTable" style="border-spacing: 0px; width: 820px;">
                <tr style="vertical-align: top;">
                    <td>
                        <hr />
                    </td>
                </tr>
            </table>

            <%--Accessories Section--%>
            <table class="topTable" style="border-spacing: 0px; width: 400px;">
                <tr style="vertical-align: top;">
                    <td>
                        <b>Accessories:</b>
                    </td>
                </tr>
            </table>

            <table class="topTable" style="border-spacing: 0px; width: 800px;">
                <tr style="vertical-align: top;">
                    <td>
                        <div style="overflow: scroll; height: 120px;">
                            <table class="topTable" style="border-spacing: 0px; width: 400px;">
                                <tr style="vertical-align: top;">
                                    <th class="QueueHeader" style="width: 20px;">&nbsp;</th>
                                    <th class="QueueHeader" style="width: 200px;">&nbsp;</th>
                                    <th class="QueueHeader" style="width: 20px;">&nbsp;</th>
                                    <th class="QueueHeader" style="width: 20px;">Retail:</th>
                                    <th class="QueueHeader" style="width: 20px;">Trade:</th>
                                </tr>
                                <tr style="vertical-align: top;">
                                    <td>
                                        <asp:CheckBox ID="chk1" runat="server" />
                                    </td>
                                    <td>Sample Data
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox18" runat="server" CssClass="inputField" Width="75px" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox19" runat="server" CssClass="inputField" Width="75px" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td>
                        <table class="topTable" style="border-spacing: 0px; width: 400px;">
                            <tr style="vertical-align: top;">
                                <th class="QueueHeader" style="width: 20px;">&nbsp;</th>
                                <th class="QueueHeader" style="width: 20px;">&nbsp;</th>
                                <th class="QueueHeader" style="width: 20px;">&nbsp;</th>
                                <th class="QueueHeader" style="width: 20px;">&nbsp;</th>
                                <th class="QueueHeader" style="width: 20px;">Retail:</th>
                                <th class="QueueHeader" style="width: 20px;">Trade:</th>
                            </tr>
                            <tr style="vertical-align: top;">
                                <td>MSRP:
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox8" runat="server" CssClass="inputField" Width="75px" Enabled="false"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox9" runat="server" CssClass="inputField" Width="75px" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr style="vertical-align: top;">
                                <td>Base:
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox10" runat="server" CssClass="inputField" Width="75px" Enabled="false"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox11" runat="server" CssClass="inputField" Width="75px" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr style="vertical-align: top;">
                                <td>Miles:
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox7" runat="server" CssClass="inputField" Width="100px" Enabled="true"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button runat="server" ID="Button1" Text="Adjust" />
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox13" runat="server" CssClass="inputField" Width="75px" Enabled="false"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox12" runat="server" CssClass="inputField" Width="75px" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr style="vertical-align: top;">
                                <td>Accessories:
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox14" runat="server" CssClass="inputField" Width="75px" Enabled="false"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox15" runat="server" CssClass="inputField" Width="75px" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr style="vertical-align: top;">
                                <td>Adjusted:
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox17" runat="server" CssClass="inputField" Width="75px" Enabled="false"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox16" runat="server" CssClass="inputField" Width="75px" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table class="topTable" style="border-spacing: 0px; width: 820px;">
                <tr style="vertical-align: top;">
                    <td>
                        <hr />
                    </td>
                </tr>
            </table>

            <table class="topTable" style="border-spacing: 0px; width: 800px;">
                <tr>
                    <td>
                        <table class="topTable" style="border-spacing: 0px; width: 640px;">
                            <tr style="vertical-align: top;">
                                <td>
                                    <b>Save:</b>
                                </td>
                            </tr>
                        </table>

                        <table class="topTable" style="border-spacing: 0px; width: 620px;">
                            <tr style="vertical-align: top;">
                                <th class="QueueHeader" style="width: 20px;">&nbsp;</th>
                                <th class="QueueHeader" style="width: 20px;">&nbsp;</th>
                                <th class="QueueHeader" style="width: 20px;">&nbsp;</th>
                                <th class="QueueHeader" style="width: 20px;">&nbsp;</th>
                                <th class="QueueHeader" style="width: 20px;">&nbsp;</th>
                            </tr>
                            <tr style="vertical-align: top;">
                                <td>
                                    <asp:CheckBox ID="Checkbox1" runat="server" Text="VIN" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="Checkbox2" runat="server" Text="Year/Make/Model/Body" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="Checkbox3" runat="server" Text="Mileage" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="Checkbox4" runat="server" Text="Adjusted Retail" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="Checkbox5" runat="server" Text="Adjusted Trade" />
                                </td>
                            </tr>
                        </table>

                        <table class="topTable" style="border-spacing: 0px; width: 620px;">
                            <tr style="vertical-align: top;">
                                <td>
                                    <hr />
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>

            <table class="topTable" style="border-spacing: 0px; width: 400px;">
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">
                        <asp:Button runat="server" ID="Button3" Text="Add" />
                        <asp:Button runat="server" ID="Button4" Text="Cancel" />
                    </td>
                </tr>
            </table>
        </div>

        <table>
            <tr>
                <td>
                    <!-- Vehicle tab repeater starts here -->
                    <asp:Repeater ID="RepeaterVehTab" runat="server" OnItemDataBound="RepeaterVehTab_ItemDataBound">
                        <HeaderTemplate>
                            <div class="tabveh" style="width: 100%;">
                        </HeaderTemplate>

                        <ItemTemplate>
                            <button class='<%# IIf(Container.ItemIndex = 0, "tabVehlinks active", "tabVehlinks")%>'
                                onclick="MenuTab(event,'tabVehlinks', 'Tab<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>', 'tabVehcontent','divDescription_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>','tabVehDetcontent');return false;">
                                <asp:Image ID="imgVehStatus" runat="server" />
                                <%#DataBinder.Eval(Container, "DataItem.VehDisplayName")%></button>
                        </ItemTemplate>

                        <FooterTemplate>
                            </div> 
                        </FooterTemplate>
                    </asp:Repeater>

                    <!-- Vehicle tab repeater ends here -->

                    <!-- Vehicle Details repeater starts here -->
                    <asp:Repeater ID="RepeaterVehdetTab" runat="server" OnItemDataBound="RepeaterVehdetTab_ItemDataBound" OnItemCommand="RepeaterVehdetTab_ItemCommand">
                        <HeaderTemplate></HeaderTemplate>
                        <ItemTemplate>
                            <!-- Tab content -->
                            <div id="Tab<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class='<%# IIf(Container.ItemIndex = 0, "tabVehcontent showDefaultTab", "tabVehcontent")%>'>
                                <div class="tabVehDetail" style="width: 100%;">
                                    <button class="tabVehDetlinks active" onclick="MenuTab(event,'tabVehDetlinks', 'divDescription_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>', 'tabVehDetcontent' );return false;">Description</button>
                                    <button class="tabVehDetlinks" onclick="MenuTab(event, 'tabVehDetlinks','divVehicleLocation_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>', 'tabVehDetcontent' );return false;">Vehicle Location</button>
                                    <button class="tabVehDetlinks" onclick="MenuTab(event, 'tabVehDetlinks','divContact_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>', 'tabVehDetcontent');return false;">Contact</button>
                                    <button class="tabVehDetlinks" onclick="MenuTab(event, 'tabVehDetlinks','divInvolved_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>', 'tabVehDetcontent');return false;">Involved</button>
                                    <!-- Warranty tab added dynamacally based on vehicle WarrantyExistsFlag -->
                                    <button class="tabVehDetlinks" style='<%# IIf(DataBinder.Eval(Container,"DataItem.WarrantyExistsFlag")= 0,"display:none", "display:block")%>' onclick="MenuTab(event, 'tabVehDetlinks','divWarranty_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>', 'tabVehDetcontent');return false;">Warranty</button>

                                    <button class="tabVehDetlinks" onclick="MenuTab(event, 'tabVehDetlinks','divMainAssignment_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>', 'tabVehDetcontent','divAssignment_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>','tabAssignmentcontent');return false;">
                                        <asp:Image ID="imgVehAssignmentStatus" runat="server" />
                                        <%#DataBinder.Eval(Container, "DataItem.ServiceChannels")%>
                                    </button>
                                    <div id='divVehSave_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>' style="float: left; width: 100%; max-width: 150px; display: none">
                                        <table style="width: 100%; margin-left: 25px" cellspacing="0" cellpadding="0" border="0">
                                            <tr>
                                                <td style="width: 100%" align="right">
                                                    <asp:Button ID="btnVehRental" runat="server" Text="Rental mgt" CssClass="formbutton" OnClientClick="return callRental(this,'btnVehRental')" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div id="divDescription_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabVehDetcontent showDefaultTab">
                                    <table style="border-spacing: 0px; height: 325px; width: 60px; border-color: #667;">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                <!-- Vehicle Description Details -->
                                                <div id="divDescriptionScroll" style="height: 295px; overflow: auto scroll;">
                                                    <asp:HiddenField ID="hidInsuranceCompanyID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.InsuranceCompanyID")%>' />
                                                    <asp:HiddenField ID="hidLynxID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.LynxID")%>' />
                                                    <asp:HiddenField ID="hidUserID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.UserID")%>' />
                                                    <asp:HiddenField ID="hidLDAUID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.LDAUID")%>' />

                                                    <asp:HiddenField ID="hidVehNumber" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>' />
                                                    <asp:HiddenField ID="hidAssignmentID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentID")%>' />
                                                    <asp:HiddenField ID="hidAssignmentTypeCD" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentTypeCD")%>' />
                                                    <asp:HiddenField ID="hidServiceChannelCD" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ServiceChannelCD")%>' />
                                                    <asp:HiddenField ID="hidClaimAspectServiceChannelID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.ClaimAspectServiceChannelID")%>' />
                                                    <asp:HiddenField ID="hidCurrentDisposition" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.CurrentDisposition")%>' />
                                                    <asp:HiddenField ID="hidCurrentCommunicationMethodID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentPreferredCommunicationMethodID")%>' />
                                                    <asp:HiddenField ID="hidAssignmentAction" runat="server" />

                                                    <asp:HiddenField ID="hidRepairScheduleChangeReasonID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.RepairScheduleChangeReasonID")%>' />
                                                    <asp:HiddenField ID="hidRepairScheduleChangeReason" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.RepairScheduleChangeReason")%>' />

                                                    <asp:HiddenField ID="hidCurrentAssignmentTypeID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.CurrentAssignmentTypeID")%>' />
                                                    <asp:HiddenField ID="hidAssignmentSysLastUpdatedDate" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentSysLastUpdatedDate")%>' />
                                                    <asp:HiddenField ID="hidDeductibleAppliedAmt" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.DeductibleAppliedAmt")%>' />

                                                    <asp:HiddenField ID="hidCoverageProfileCD" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.CoverageProfileCD")%>' />
                                                    <asp:HiddenField ID="hidClientCoverageTypeID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ClientCoverageTypeID")%>' />
                                                    <asp:HiddenField ID="hidPermissionToDriveCD" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.PermissionToDriveCD")%>' />

                                                    <asp:HiddenField ID="hidVehicleSysLastUpdatedDate" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.SysLastUpdatedDate")%>' />
                                                    <asp:HiddenField ID="hidVehicleClaimAspectID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ClaimAspectID")%>' />
                                                    <asp:HiddenField ID="hidAlternateAreaCode" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.AlternateAreaCode")%>' />
                                                    <asp:HiddenField ID="hidAlternateExchangeNumber" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.AlternateExchangeNumber")%>' />
                                                    <asp:HiddenField ID="hidAlternateExtensionNumber" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.AlternateExtensionNumber")%>' />
                                                    <asp:HiddenField ID="hidAlternateUnitNumber" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.AlternateUnitNumber")%>' />

                                                    <asp:HiddenField ID="hidBestContactPhoneCD" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.BestContactPhoneCD")%>' />
                                                    <asp:HiddenField ID="hidBestContactTime" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.BestContactTime")%>' />
                                                    <asp:HiddenField ID="hidDayAreaCode" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.DayAreaCode")%>' />
                                                    <asp:HiddenField ID="hidDayExchangeNumber" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.DayExchangeNumber")%>' />
                                                    <asp:HiddenField ID="hidDayExtensionNumber" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.DayExtensionNumber")%>' />
                                                    <asp:HiddenField ID="hidDayUnitNumber" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.DayUnitNumber")%>' />
                                                    <asp:HiddenField ID="hidNightAreaCode" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.NightAreaCode")%>' />
                                                    <asp:HiddenField ID="hidNightExchangeNumber" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.NightExchangeNumber")%>' />
                                                    <asp:HiddenField ID="hidNightExtensionNumber" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.NightExtensionNumber")%>' />
                                                    <asp:HiddenField ID="hidNightUnitNumber" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.NightUnitNumber")%>' />

                                                    <asp:HiddenField ID="hidInsuredRelationID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.InsuredRelationID")%>' />
                                                    <asp:HiddenField ID="hidContactSysLastUpdatedDate" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ContactSysLastUpdatedDate")%>' />
                                                    <!-- Added for Save involved -- glsd451 -->
                                                    <asp:HiddenField ID="hidClaimAspectID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ClaimAspectID")%>' />
                                                    <asp:HiddenField ID="hidShopLocationID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.ShopLocationID")%>' />
                                                    <asp:HiddenField ID="hidShopLocationCity" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.ShopLocationCity")%>' />
                                                    <asp:HiddenField ID="hidShopLocationCounty" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.ShopLocationCounty")%>' />
                                                    <asp:HiddenField ID="hidShopLocationState" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.ShopLocationState")%>' />
                                                    <asp:HiddenField ID="hidInsuranceCompanyName" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.InsuranceCompanyName")%>' />
                                                    <asp:HiddenField ID="hidShopID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.ShopID")%>' />
                                                    <asp:HiddenField ID="hidBusinessID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.BusinessID")%>' />

                                                    <asp:HiddenField ID="hidJobID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.JobID")%>' />
                                                    <asp:HiddenField ID="hidJobStatus" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.JobStatus")%>' />

                                                    <asp:HiddenField ID="hidWorkStartDate" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.WorkStartDate")%>' />
                                                    <asp:HiddenField ID="hidWorkEndDate" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.WorkEndDate")%>' />
                                                    <asp:HiddenField ID="hidImpactPoints" runat="server" Value='' />
                                                    <asp:HiddenField ID="hidPriorImpactPoints" runat="server" Value='' />
                                                    <asp:HiddenField ID="hidVehicleStatus" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.Status")%>' />

                                                    <%--                                                    <asp:HiddenField ID="hidActiveServiceChannels" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ServiceChannels")%>' />--%>

                                                    <table class="topTable" style="border-spacing: 0px;">
                                                        <tr style="vertical-align: top;">
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px; width: 180px;">
                                                                    <tr style="vertical-align: top;">
                                                                        <td>
                                                                            <table class="topTable" style="border-spacing: 0px; width: 290px;" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 120px;"><b>Year:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtVehicleYear" runat="server" CssClass="inputField" Width="75px" Text='<%#DataBinder.Eval(Container, "DataItem.VehicleYear")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 120px;"><b>Make:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtMake" runat="server" CssClass="inputField" Width="150px" MaxLength="5" Text='<%#DataBinder.Eval(Container, "DataItem.Make")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 120px;"><b>Model:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtModel" runat="server" CssClass="inputField" Width="150px" MaxLength="50" Text='<%#DataBinder.Eval(Container, "DataItem.Model")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 110px;"><b>Body Style:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtBodyStyle" runat="server" CssClass="inputField" Width="150px" MaxLength="50" Text='<%#DataBinder.Eval(Container, "DataItem.BodyStyle")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 120px;"><b>Color:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtColor" runat="server" CssClass="inputField" Width="150px" MaxLength="20" Text='<%#DataBinder.Eval(Container, "DataItem.Color")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 120px;"><b>VIN:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtVIN" runat="server" CssClass="inputField" Width="150px" MaxLength="20" Text='<%#DataBinder.Eval(Container, "DataItem.VIN")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 120px;">
                                                                                        <b>VIN (Est):</b>
                                                                                        <asp:ImageButton ID="imgbtnVINEdit" runat="server" ImageUrl="images/Edit.gif" OnClientClick="return updateVIN(this,'imgbtnVINEdit')" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtVINEst" runat="server" CssClass="inputField" Width="150px" MaxLength="20" Text='<%#DataBinder.Eval(Container, "DataItem.EstimateVIN")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 120px;"><b>License Plate:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtLicensePlateNumber" runat="server" CssClass="inputField" Width="125px" MaxLength="20" Text='<%#DataBinder.Eval(Container, "DataItem.LicensePlateNumber")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td><b>License Plate State:</b></td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="ddlLicensePlateState" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 120px;"><b>Odometer Reading:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtMileage" runat="server" CssClass="inputField" Width="100px" MaxLength="20" Text='<%#DataBinder.Eval(Container, "DataItem.Mileage")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 120px;"><b>Book Value:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtBookValueAmt" runat="server" CssClass="inputField" Width="115px" MaxLength="20" Text='<%#DataBinder.Eval(Container, "DataItem.BookValueAmt")%>'></asp:TextBox>
                                                                                        <img id="imgNADA" src="images/NADA.png" alt="NADA" onclick="callNada('<%#DataBinder.Eval(Container, "DataItem.VIN")%>','<%#DataBinder.Eval(Container, "DataItem.Make")%>','<%#DataBinder.Eval(Container, "DataItem.Model")%>','<%#DataBinder.Eval(Container, "DataItem.VehicleYear")%>','<%#DataBinder.Eval(Container, "DataItem.BodyStyle")%>','<%#DataBinder.Eval(Container, "DataItem.Mileage")%>','<%#DataBinder.Eval(Container, "DataItem.LocationZip")%>')" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 120px;"><b>Rental Days Authorized:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtRentalDaysAuthorized" runat="server" CssClass="inputField" Width="50px" MaxLength="20" Text='<%#DataBinder.Eval(Container, "DataItem.RentalDaysAuthorized")%>'></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 120px;">
                                                                                        <b>Rental instructions:</b>
                                                                                        <table class="topTable" style="border-spacing: 0px;">
                                                                                            <tr style="vertical-align: top;">
                                                                                                <td>&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr style="vertical-align: top;">
                                                                                                <td>
                                                                                                    <asp:Button runat="server" ID="btnVehicleDescSave" CssClass="formbutton" OnClientClick="return getImpactNodes(this,'btnVehicleDescSave')" OnClick="btnVehicleDescSave_Click" Text="Save" />
                                                                                                    <asp:Label runat="server" ID="lblVehicleDescSaved" ForeColor="Blue" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td style="vertical-align: top;">
                                                                                        <asp:TextBox ID="txtRentalInstructions" runat="server" CssClass="inputField" Width="150px" Height="50px" MaxLength="20" TextMode="MultiLine" Text='<%#DataBinder.Eval(Container, "DataItem.RentalInstructions")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>

                                                                        <td>
                                                                            <table class="topTable" style="border-spacing: 0px; width: 280px;">
                                                                                <tr style="vertical-align: top;">
                                                                                    <td>
                                                                                        <table class="topTable" style="border-spacing: 0px; width: 400px;">
                                                                                            <tr style="vertical-align: top;">
                                                                                                <td>Impact Area:
                                                                                                    <asp:TextBox ID="txtImpact" ReadOnly="true" runat="server" CssClass="inputField" Width="150px" Height="50px" TextMode="MultiLine" Text='<%#DataBinder.Eval(Container, "DataItem.StrImpactPoints")%>'></asp:TextBox>
                                                                                                </td>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:ImageButton ID="btnImpact" runat="server" ImageUrl="images/next_button.gif" Style="position: relative; top: 25px" OnClientClick='return doImpact(this,"btnImpact","DamagePickNet.asp","New Damage","ModalPopUp","toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=520px,height=410px,left = 190,top=100,center=yes;")' />
                                                                                    </td>
                                                                                    <td>Prior Damage Area:
                                                                                                    <asp:TextBox ID="txtPriorImpact" ReadOnly="true" runat="server" CssClass="inputField" Width="150px" Height="50px" TextMode="MultiLine" Text='<%#DataBinder.Eval(Container, "DataItem.StrPriorImpactPoints")%>'></asp:TextBox></td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="btnPriorImpact" runat="server" ImageUrl="images/next_button.gif" Style="position: relative; top: 25px" OnClientClick='return doImpact(this,"btnPriorImpact","DamagePickNet.asp","Previous Damage","ModalPopUp","toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=520px,height=410px,left = 190,top=100,center=yes;")' /></td>
                                                                        <td>
                                                                            <b>Party:</b><asp:Label ID="txtParty" runat="server" CssClass="inputField" Width="60px" Enabled="false"></asp:Label>
                                                                            <asp:CheckBox ID="chkDriveableFlag" runat="server" Text="Driveable" />
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                                <!-- <table class="topTable" style="border-spacing: 0px; width: 350px;">
                                                                                            <tr style="vertical-align: top;">
                                                                                                <td style="width: 75px; padding: 0px; margin: 0px; border: 0px;">Impact Speed:</td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="txtImpactSpeed" runat="server" CssClass="inputField" Width="35px" Text='<%#DataBinder.Eval(Container, "DataItem.ImpactSpeed")%>'></asp:TextBox>
                                                                                                </td>
                                                                                                <td style="width: 75px; padding: 0px; margin: 0px; border: 0px;">&nbsp;Posted Speed:</td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="txtPostedSpeed" runat="server" CssClass="inputField" Width="35px" Text='<%#DataBinder.Eval(Container, "DataItem.PostedSpeed")%>'></asp:TextBox>
                                                                                                </td>
                                                                       
                                                                                            </tr>
                                                                </table> -->

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px; width: 403px;">
                                                                    <tr style="vertical-align: top;">
                                                                        <td style="width: 100px;">Init Assign. Type:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtInitialAssignmentType" runat="server" CssClass="inputField" Width="140px" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.InitialAssignmentType")%>'></asp:TextBox></td>
                                                                        <td style="width: 55px;">&nbsp;Coverage:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtInitialCoverageProfile" runat="server" CssClass="inputField" Width="94px" Text='<%#DataBinder.Eval(Container, "DataItem.InitialCoverageProfile")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top;">
                                                                        <td style="width: 100px;">&nbsp;</td>
                                                                        <td>
                                                                        &nbsp;
                                                                                                <td style="width: 55px;">&nbsp;</td>
                                                                        <td>
                                                                            <button type="button" id="btnNewServiceChannel" class="formbutton" onclick='OpenDialogWindow("AddService.aspx?LynxID=<%=sLynxID %>&InscCompID=<%=sInscCompany %>&UserID=<%=sUserID %>&ClaimAspectID=<%#DataBinder.Eval(Container, "DataItem.ClaimAspectID")%>","ModalPopUp","toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=300px,height=100px,left = 490,top=300,center=yes;")'>New Channel</button>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align: top;">
                                                                <div id="divServiceChannel" style="width: 400px; height: 110px; overflow: auto scroll; border: 1px solid black;">
                                                                    <table class="topTable" style="border-spacing: 0px; width: 400px;">
                                                                        <tr style="vertical-align: top;">
                                                                            <td style="width: 160px;"><b>Service Channel Summary:</b></td>
                                                                        </tr>
                                                                        <tr style="vertical-align: top;">
                                                                            <td>
                                                                                <asp:GridView ID="gvServiceChannel" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
                                                                                    <AlternatingRowStyle BackColor="#F7F7F7" />
                                                                                    <Columns>
                                                                                        <asp:BoundField DataField="Channel" HeaderText="Channel" SortExpression="Channel" HeaderStyle-Width="400px" HeaderStyle-HorizontalAlign="Left" />
                                                                                        <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                                                        <asp:BoundField DataField="EstAmount" HeaderText="EstAmount." SortExpression="EstAmount" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                                                        <asp:BoundField DataField="Coverage" HeaderText="Coverage" SortExpression="Coverage" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                                                                                        <asp:BoundField DataField="Deductable" HeaderText="Deductable" SortExpression="Deductable" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                                                                        <asp:BoundField DataField="OverageAndLmt" HeaderText="Overage/Lmt" SortExpression="OverageAndLmt" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                                                    </Columns>
                                                                                    <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                                                    <HeaderStyle BackColor="#629ad6" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                                    <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                                                                    <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                                                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr style="vertical-align: top;">
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px; width: 390px;">
                                                                    <tr style="vertical-align: top;">
                                                                        <td style="width: 115px;"><b>Vehicle Remarks:</b></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top;">
                                                                        <td>
                                                                            <asp:TextBox ID="txtVehicleRemarks" runat="server" CssClass="inputField" Width="398px" Height="56px" TextMode="MultiLine" Text='<%#DataBinder.Eval(Container, "DataItem.VehicleRemarks")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                            </td>
                                        </tr>
                                    </table>
                                    </td>
                                                        </tr>
                                                    </table>
                                </div>
                                </td>
                                        </tr>
                                    </table>
                            </div>

                            <%--                                <div id="divCoverageApplied" name="divCoverageApplied" style="display:none;position:absolute;top:100px;left:100px;height:100px;width:280px;background-color:#FFFFFF;padding:3px;border:2px solid #FFA500;background-color:#F5F5DC;FILTER1: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#2F4F4F,strength=5);">
                                  <table cellspacing="0" cellpadding="2" border="0" style="table-layout:fixed;width:100%;background-color:#FFFFFF;border-collapse:collapse;">
                                    <colgroup>
                                      <col width="150px"/>
                                      <col width="*"/>
                                    </colgroup>
                                    <tr>
                                      <td colspan="2" style="font-weight:bold">
                                        Apply Coverage
                                      </td>
                                    </tr>
                                    <tr>
                                      <td colspan="2" style="font-weight:bold">
                                        <img src="images/background_top.gif" style="height:2px;width:125px"/>
                                        <input type="hidden" id="txtClaimCoverageID"/>
                                      </td>
                                    </tr>
                                  </table>
                                </div>--%>

                            <div id="divVehicleLocation_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabVehDetcontent">
                                <table style="height: 300px; width: 755px; border-color: #667; border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                    <tr style="vertical-align: top;">
                                        <td>
                                            <!-- Vehicle Location Details -->
                                            <div id="divVehicleLocationScroll" style="width: 755px; height: 290px; overflow: auto scroll;">
                                                <table class="topTable" style="border-spacing: 0px; width: 500px;" cellpadding="0" cellspacing="0" border="0">
                                                    <tr style="vertical-align: top;">
                                                        <td>
                                                            <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">Location:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtLocationName" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.LocationName")%>'></asp:TextBox></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">Address:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtLocationAddress1" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.LocationAddress1")%>'></asp:TextBox></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">&nbsp;</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtLocationAddress2" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.LocationAddress2")%>'></asp:TextBox></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">City:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtLocationCity" runat="server" CssClass="inputField" Width="100px" Text='<%#DataBinder.Eval(Container, "DataItem.LocationCity")%>'></asp:TextBox></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                            <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                <tr style="vertical-align: top;">
                                                                    <td>
                                                                        <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr style="vertical-align: top;">
                                                                                <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">State:</td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlLocationState" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList>
                                                                                    Zip:</td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtLocationZip" runat="server" CssClass="inputField" Width="75px" Text='<%#DataBinder.Eval(Container, "DataItem.LocationZip")%>'></asp:TextBox></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table class="topTable" style="border-spacing: 0px; width: 500px;" cellpadding="0" cellspacing="0" border="0">
                                                    <tr style="vertical-align: top;">
                                                        <td style="width: 60px;">Phone:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtLocationPhone" runat="server" CssClass="inputField" Width="90px" MaxLength="50" Text='<%#DataBinder.Eval(Container, "DataItem.LocationPhone")%>'></asp:TextBox>
                                                            <%--<asp:RegularExpressionValidator ID="txtLocationPhoneFieldValidator" runat="server" ControlToValidate="txtLocationPhone" ErrorMessage="Format (nnn) nnn-nnnn" ForeColor="Red" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$"></asp:RegularExpressionValidator>--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:Button runat="server" ID="btnVehicleLocationSave_Click" CssClass="formbutton" OnClick="btnVehicleLocationSave_Click" Text="Save" />
                                                <asp:Label runat="server" ID="lblVehicleLocationSave" ForeColor="Blue" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </div>

                            <div id="divContact_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabVehDetcontent">

                                <asp:HiddenField runat="server" ID="oDataItem" Value='<%#DataBinder.Eval(Container, "DataItem")%>' />

                                <table style="height: 175px; width: 755px; border-color: #667; border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                    <tr style="vertical-align: top;">
                                        <td>
                                            <!-- Vehicle Contact Details -->
                                            <div id="divContactScroll" style="width: 755px; height: 140px; overflow: auto scroll;">
                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                    <tr style="vertical-align: top;">
                                                        <td>
                                                            <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                <tr align="left" style="vertical-align: top;">
                                                                    <td align="left" style="width: 60px;">Name:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtContactNameTitle" runat="server" CssClass="inputField" Width="30px" MaxLength="15" Text='<%#DataBinder.Eval(Container, "DataItem.ContactNameTitle")%>'></asp:TextBox></td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtContactNameFirst" runat="server" CssClass="inputField" Width="100px" MaxLength="50" Text='<%#DataBinder.Eval(Container, "DataItem.ContactNameFirst")%>'></asp:TextBox></td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtContactNameLast" runat="server" CssClass="inputField" Width="100px" MaxLength="50" Text='<%#DataBinder.Eval(Container, "DataItem.ContactNameLast")%>'></asp:TextBox></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">Address:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtContactAddress1" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.ContactAddress1")%>'></asp:TextBox></td>
                                                                </tr>
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 30px; padding: 0px; margin: 0px; border: 0px;">&nbsp;</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtContactAddress2" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.ContactAddress2")%>'></asp:TextBox></td>
                                                                </tr>
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 30px; padding: 0px; margin: 0px; border: 0px;">City:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtContactCity" runat="server" CssClass="inputField" Width="100px" Text='<%#DataBinder.Eval(Container, "DataItem.ContactCity")%>'></asp:TextBox></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr style="vertical-align: top;">
                                                        <td>
                                                            <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                <tr style="vertical-align: top;">
                                                                    <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">State:</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlContactState" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList>
                                                                        Zip:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtContactZip" runat="server" CssClass="inputField" Width="75px" Text='<%#DataBinder.Eval(Container, "DataItem.ContactZip")%>'></asp:TextBox></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr style="vertical-align: top;">
                                                        <td>
                                                            <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">Email:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtContactEmailAddress" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.ContactEmailAddress")%>'></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <%--Adding Missed fields in Contact tab--%>
                                                <div style="float: right; position: relative; top: -115px; left: -10px;">
                                                    <div>
                                                        <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                            <tr style="vertical-align: top;">
                                                                <td style="padding: 0px; margin: 0px; border: 0px;">Relation to ins.:</td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlContactInsuredRelation" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div style="margin-top: 50px;">
                                                        <table class="topTable" style="border-spacing: 0px; width: 400px;">
                                                            <tr style="vertical-align: top;">
                                                                <td style="width: 120px;">Best Number to Call:
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlBestContactTabPhoneCD" runat="server" CssClass="inputField" Width="110px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td>&nbsp;Day:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtContactDay" runat="server" CssClass="inputField" Text='<%#DataBinder.Eval(Container, "DataItem.ContactDay")%>' Width="115px" MaxLength="50"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr style="vertical-align: top;">
                                                                <td style="width: 75px;">Best Time to Call:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtBestContactTabTime" runat="server" CssClass="inputField" Width="108px" Text='<%#DataBinder.Eval(Container, "DataItem.BestContactTabTime")%>'
                                                                        MaxLength="50"></asp:TextBox>
                                                                </td>
                                                                <td>&nbsp;Night:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtContactNight" runat="server" CssClass="inputField" Text='<%#DataBinder.Eval(Container, "DataItem.ContactNight")%>' Width="115px" MaxLength="50"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr style="vertical-align: top;">
                                                                <td style="width: 75px;"></td>
                                                                <td></td>
                                                                <td>&nbsp;Alt:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtContactAlternate" runat="server" CssClass="inputField" Text='<%#DataBinder.Eval(Container, "DataItem.ContactAlternate")%>' Width="115px"
                                                                        MaxLength="50"></asp:TextBox>

                                                                </td>
                                                            </tr>
                                                            <tr style="vertical-align: top;">
                                                                <td style="width: 25px;">&nbsp;
                                                                </td>
                                                                <td style="width: 125px;">&nbsp;
                                                                </td>
                                                                <td>&nbsp;Cell:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtContactCell" runat="server" CssClass="inputField" Text='<%#DataBinder.Eval(Container, "DataItem.ContactCell")%>' Width="115px" MaxLength="50"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <%--End--%>

                                                <asp:Button runat="server" ID="btnVehicleContactSave" CssClass="formbutton" OnClick="btnVehicleContactSave_Click" Text="Save" />
                                                <asp:Label runat="server" ID="lblVehicleContactSave" ForeColor="Blue" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>


                            <div id="divInvolved_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabVehDetcontent">
                                <!-- Vehicle Involved Header Bar -->
                                <asp:HiddenField ID="hidInvolvedSysLastUpdatedDate" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedSysLastUpdatedDate")%>' />
                                <table style="height: 275px; width: 755px; border-color: #667; border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                    <tr style="vertical-align: top;">
                                        <td>
                                            <!-- Vehicle Involved Details -->
                                            <div id="divInvolvedScroll" style="width: 755px; height: 225px; overflow: auto scroll;">
                                                <table style="height: 225px; width: 675px; border-color: #667; border-spacing: 0px;">
                                                    <tr style="vertical-align: top;">
                                                        <td>
                                                            <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                <tr style="vertical-align: top;">
                                                                    <td>
                                                                        <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr align="left" style="vertical-align: top;">
                                                                                <td align="left" style="width: 90px;">Involved:</td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlInvolved" runat="server" CssClass="inputField" Width="150px" onchange="selectInvolved(this,'ddlInvolved')"></asp:DropDownList></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr align="left" style="vertical-align: top;">
                                                                                <td align="left" style="width: 90px;">Name:</td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtInvolvedNameTitle" runat="server" CssClass="inputField" Width="30px" MaxLength="15" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedNameTitle")%>'></asp:TextBox></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtInvolvedNameFirst" runat="server" CssClass="inputField" Width="100px" MaxLength="50" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedNameFirst")%>'></asp:TextBox></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtInvolvedNameLast" runat="server" CssClass="inputField" Width="100px" MaxLength="50" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedNameLast")%>'></asp:TextBox></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                                <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Business:</td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtInvolvedBusinessName" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedBusinessName")%>'></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                                <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Business Type:</td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlInvolvedBusinessType" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList>
                                                                            </tr>
                                                                            <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                                <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Address:</td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtInvolvedAddress1" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedAddress1")%>'></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                                <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">&nbsp;</td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtInvolvedAddress2" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedAddress2")%>'></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                                <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">City:</td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtInvolvedAddressCity" runat="server" CssClass="inputField" Width="100px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedCity")%>'></asp:TextBox></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr style="vertical-align: top;">
                                                                    <td>
                                                                        <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr style="vertical-align: top;">
                                                                                <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">State:</td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlInvolvedState" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList>
                                                                                    Zip:</td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtInvolvedAddressZip" runat="server" CssClass="inputField" Width="75px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedZip")%>'></asp:TextBox></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr style="vertical-align: top;">
                                                                    <td>
                                                                        <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                                <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Email:</td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtInvolvedEmailAddress" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedEmailAddress")%>'></asp:TextBox></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr style="vertical-align: top;">
                                                                    <td>
                                                                        <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                                <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Involved Type:</td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="cbInsured" Style='<%# IIf(DataBinder.Eval(Container,"DataItem.CurrentExposure")= "1","display:block", "display:none")%>' runat="server" Text="Insured"></asp:CheckBox>
                                                                                    <asp:CheckBox ID="cbClaimant" Style='<%# IIf((DataBinder.Eval(Container,"DataItem.CurrentExposure")= "3"),"display:block", "display:none")%>' runat="server" Text="Claimant"></asp:CheckBox>

                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="cbOwner" runat="server" Text="Owner"></asp:CheckBox></td>
                                                                            </tr>
                                                                            <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                                <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">&nbsp;</td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="cbDriver" runat="server" Text="Driver"></asp:CheckBox></td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="cbPassenger" runat="server" Text="Passenger"></asp:CheckBox></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                <tr align="left" style="vertical-align: top;">
                                                                    <td align="left" style="width: 110px;">Gender:</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlInvolvedGender" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList></td>
                                                                </tr>
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 110px; padding: 0px; margin: 0px; border: 0px;">SSN/EIN:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtInvolvedFedTaxId" runat="server" CssClass="inputField" Width="120px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedFedTaxId")%>'></asp:TextBox></td>
                                                                </tr>
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 110px; padding: 0px; margin: 0px; border: 0px;">DOB:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtInvolvedBirthDate" runat="server" CssClass="inputField" Width="90px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedBirthDate")%>'></asp:TextBox></td>
                                                                </tr>
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 110px; padding: 0px; margin: 0px; border: 0px;">Age:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtInvolvedAge" runat="server" CssClass="inputField" Width="50px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedAge")%>'></asp:TextBox></td>
                                                                </tr>
                                                                <tr align="left" style="vertical-align: top;">
                                                                    <td align="left" style="width: 110px;">Best Number to Call:</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlBestNumberCall" runat="server" CssClass="inputField" Width="100px"></asp:DropDownList></td>
                                                                </tr>
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 110px; padding: 0px; margin: 0px; border: 0px;">Best Time to Call:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtInvolvedBestTimeToCall" runat="server" CssClass="inputField" Width="150px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedBestTimeToCall")%>'></asp:TextBox></td>
                                                                </tr>
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 110px; padding: 0px; margin: 0px; border: 0px;">Day:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtInvolvedPhoneDay" runat="server" CssClass="inputField" Width="120px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedPhoneDay")%>'></asp:TextBox>
                                                                        <%--<asp:RegularExpressionValidator ID="RegInvolvedPhoneDay" runat="server" ControlToValidate="txtInvolvedPhoneDay" ErrorMessage="Format (nnn) nnn-nnnn" ForeColor="Red" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$"></asp:RegularExpressionValidator>--%>
                                                                    </td>
                                                                </tr>
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 110px; padding: 0px; margin: 0px; border: 0px;">Night:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtInvolvedPhoneNight" runat="server" CssClass="inputField" Width="120px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedPhoneNight")%>'></asp:TextBox>
                                                                        <%--<asp:RegularExpressionValidator ID="RegInvolvedPhoneNight" runat="server" ControlToValidate="txtInvolvedPhoneNight" ErrorMessage="Format (nnn) nnn-nnnn" ForeColor="Red" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$"></asp:RegularExpressionValidator>--%>
                                                                    </td>
                                                                </tr>
                                                                <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                    <td style="width: 110px; padding: 0px; margin: 0px; border: 0px;">Alt.:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAlt" runat="server" CssClass="inputField" Width="120px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedPhoneAlt")%>'></asp:TextBox>
                                                                        <%--<asp:RegularExpressionValidator ID="RegtxtAlt" runat="server" ControlToValidate="txtAlt" ErrorMessage="Format (nnn) nnn-nnnn" ForeColor="Red" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$"></asp:RegularExpressionValidator>--%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table style="width: 100%; margin-left: 3px; margin-top: 3px;" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td style="width: 100%">
                                                            <input type="button" id="btnAddInvolved" class="formbutton" value="Add" height="16px" runat="server" onclick="AddInvolved(this, 'btnAddInvolved')" />
                                                            &nbsp; 
                                                                    <asp:Button ID="btnDeleteInvolved" runat="server" CssClass="formbutton" Text="Delete" OnClick="btnDeleteInvolved_Click" OnClientClick="return delInvolved(this, 'btnDeleteInvolved')"></asp:Button>
                                                            &nbsp;
                                                                    <asp:Button ID="btnSaveInvolved" runat="server" CssClass="formbutton" Text="Save" OnClick="btnSaveInvolved_Click" OnClientClick="return SaveInvolved(this, 'btnSaveInvolved')" />
                                                            &nbsp; 
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>

                                </table>
                            </div>

                            <div id="divWarranty_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabVehDetcontent">
                                <div class="tabWarAssignment active">
                                    <button class="tabWarAssignmentlinks active" onclick="MenuTab(event, 'tabWarAssignmentlinks','divWarAssignment_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>','tabWarAssignmentcontent');return false;">Assignment</button>
                                    <button class="tabWarAssignmentlinks" onclick="MenuTab(event, 'tabWarAssignmentlinks','divWarDocuments_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>','tabWarAssignmentcontent');return false;">Documents</button>
                                </div>

                                <div id="divWarAssignment_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabWarAssignmentcontent showDefaultTab">
                                    <table style="height: 314px; width: 755px; border-color: #667; border-spacing: 0px;">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                <div style="float: left; margin: 3px;">
                                                    <div style="height: 250px; overflow: scroll scroll; width: 170px;">
                                                        <asp:GridView ID="gvWarAssignment" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnRowDataBound="gvWarAssignment_RowDataBound">
                                                            <AlternatingRowStyle BackColor="#F7F7F7" />
                                                            <Columns>
                                                                <asp:BoundField DataField="ShopName" HeaderText="ShopName" SortExpression="ShopName" HeaderStyle-Width="100PX" HeaderStyle-HorizontalAlign="Left" />
                                                                <asp:BoundField DataField="WorkStatus" HeaderText="Status" SortExpression="Status" HeaderStyle-Width="70px" HeaderStyle-HorizontalAlign="Left" />
                                                            </Columns>
                                                            <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                            <HeaderStyle BackColor="#629ad6" Font-Bold="True" ForeColor="#F7F7F7" />
                                                            <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                                            <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                                        </asp:GridView>
                                                    </div>
                                                    <asp:Button ID="btnNewWarranty" runat="server" CssClass="formbutton" name="btnNewWarranty" Text="New Warranty" Width="100" CCDisabled="false" CCTabIndex="1" OnClientClick="return selectShopWarranty(this,'btnNewWarranty')" />
                                                </div>
                                                <table id="gvWarTextTable" style="height: 200px; border-color: #667; border-spacing: 0px; margin-left: 185p">
                                                    <tr>
                                                        <td>Assgn. To:</td>
                                                        <td colspan="3">
                                                            <asp:TextBox runat="server" ID="txtWarShopName" name="txtWarShopName" value="" Height="" Width="215" />
                                                        </td>
                                                        <td>Disposition:</td>
                                                        <td colspan="2">
                                                            <asp:DropDownList ID="ddlWarDisposition" runat="server" name="ddlWarDisposition">
                                                                <asp:ListItem Value="0" Text=""></asp:ListItem>
                                                                <asp:ListItem Value="CA" Text="Cancelled"></asp:ListItem>
                                                                <asp:ListItem Value="CO" Text="Cash-Out"></asp:ListItem>
                                                                <asp:ListItem Value="IC" Text="Insurance Co Managing"></asp:ListItem>
                                                                <asp:ListItem Value="IME" Text="Included with ME Estimate"></asp:ListItem>
                                                                <asp:ListItem Value="IPD" Text="Included with Body Estimate"></asp:ListItem>
                                                                <asp:ListItem Value="NA" Text="Not Authorized"></asp:ListItem>
                                                                <asp:ListItem Value="NOCV" Text="Not Covered"></asp:ListItem>
                                                                <asp:ListItem Value="PCO" Text="Partial Cash-Out"></asp:ListItem>
                                                                <asp:ListItem Value="RC" Text="Repair Complete"></asp:ListItem>
                                                                <asp:ListItem Value="RNC" Text="Repair Not Completed"></asp:ListItem>
                                                                <asp:ListItem Value="ST" Text="Stale"></asp:ListItem>
                                                                <asp:ListItem Value="TL" Text="Total Loss"></asp:ListItem>
                                                                <asp:ListItem Value="VD" Text="Voided"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Contact:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtWarContactName" runat="server" name="txtWarContactName" value="" Height="" Width="170" />
                                                        </td>
                                                        <td>MSA:</td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="txtWarMSA" runat="server" name="txtWarMSA" value="" Height="" Width="30" CCDisabled="false" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Phone:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtWarShopPhone" runat="server" name="txtWarShopPhone" />
                                                        </td>
                                                        <td colspan="2" style="text-align: right">
                                                            <asp:ImageButton ID="btnWarRepairStart" runat="server" ImageUrl="images/tick.gif" OnClientClick="return confirmWarRepairStart(this, 'btnRepairStart')" OnClick="btnWarRepairStart_Click" Style='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.WorkStartConfirmFlag")%>' />
                                                        </td>
                                                        <td>Rpr. St. Dt.:</td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtWarRepairStartDate" runat="server" name="txtWarRepairStartDate" onchange="javascript: return ValidateWarStartDate( this,'txtWarRepairStartDate' );" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fax:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtWarShopFax" runat="server" name="txtWarShopFax" />
                                                        </td>
                                                        <td colspan="2" style="text-align: right">
                                                            <asp:ImageButton ID="btnWarRepairEnd" runat="server" ImageUrl="images/tick.gif" OnClientClick="return confirmWarRepairEnd(this, 'btnWarRepairEnd')" OnClick="btnWarRepairEnd_Click" Style='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.WorkEndConfirmFlag")%>' />
                                                        </td>
                                                        <td>Rpr. End Dt.:</td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtWarRepairEndDate" runat="server" name="txtWarRepairEndDate" onchange="javascript: return ValidateWarEndDate( this,'txtWarRepairEndDate' );" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fax Status:</td>
                                                        <td colspan="6">
                                                            <asp:TextBox ID="txtWarFaxStatus" runat="server" name="txtWarFaxStatus" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Elec. Status:</td>
                                                        <td colspan="6">
                                                            <asp:TextBox ID="txtWarElecStatus" runat="server" name="txtWarElecStatus" Height="" Width="170" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Assgn. Dt:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtWarCurAssgDate" runat="server" name="txtWarCurAssgDate" />
                                                        </td>
                                                        <td colspan="2">Prev.</td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="txtWarPrevAssgDate" runat="server" name="txtWarPrevAssgDate" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Work Status:</td>
                                                        <td colspan="6">
                                                            <asp:TextBox ID="txtWarWorkStatus" runat="server" name="txtWarWorkStatus" Width="170" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Remarks:</td>
                                                        <td colspan="6">
                                                            <asp:TextBox ID="txtWarRemarks" runat="server" Height="62px" Width="310px" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div>
                                                    <asp:HiddenField ID="hidWarAssignmentID" runat="server" />
                                                    <asp:Button ID="btnWarSendAssignment" runat="server" CssClass="formbutton" name="btnSendAssignment" Text="Send Assignment" Width="130" OnClientClick="return sendWarrantyAssignment(this,'btnWarSendAssignment')" />
                                                    <asp:Button ID="btnWarCancelAssignment" runat="server" Text="Cancel Assignment" name="btnWarCancelAssignment" CssClass="formbutton" OnClientClick="doWarCancelAssignment(this , 'btnWarCancelAssignment')" OnClick="btnWarCancelAssignment_click" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div id="divWarDocuments_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabWarAssignmentcontent">
                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                        <tr style="vertical-align: top;">
                                            <!-- Top View Estimate -->
                                            <td>
                                                <fieldset>
                                                    <legend style="font-weight:bold">Estimates</legend>
                                                    <asp:Panel runat="server" ID="pnlWarEstimate" GroupingText="Estimates" Font-Bold="true" Height="130" ScrollBars="Vertical">
                                                        <asp:GridView ID="gvWarEstimate" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnRowDataBound="gvEstimate_RowDataBound">
                                                            <AlternatingRowStyle BackColor="#F7F7F7" />
                                                            <Columns>
                                                                <asp:BoundField DataField="EstRecvDate" HeaderText="Recd./Attach Date" SortExpression="EstRecvDate" HeaderStyle-Width="20%" HeaderStyle-HorizontalAlign="Left" />
                                                                <asp:ImageField DataImageUrlField="EstSummary" HeaderText="Summary" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left"></asp:ImageField>
                                                                <asp:BoundField DataField="EstSeq" HeaderText="Seq." SortExpression="EstSeq" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" />
                                                                <asp:BoundField DataField="EstSource" HeaderText="Source" SortExpression="EstSource" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" />
                                                                <asp:BoundField DataField="EstGross" HeaderText="Gross $" SortExpression="EstGross" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" />
                                                                <asp:BoundField DataField="EstNet" HeaderText="Net $" SortExpression="EstNet" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" />
                                                                <asp:BoundField DataField="EstOrgAud" HeaderText="Org./Audited" SortExpression="EstOrgAud" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left" />
                                                                <asp:BoundField DataField="EstDup" HeaderText="Dup." SortExpression="EstDup" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" />
                                                                <asp:BoundField DataField="EstAgreed" HeaderText="Agreed" SortExpression="EstAgreed" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" />
                                                            </Columns>
                                                            <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                            <HeaderStyle BackColor="#629ad6" Font-Bold="True" ForeColor="#F7F7F7" />
                                                            <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                                            <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </fieldset>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <fieldset>
                                                    <legend style="font-weight:bold">Thumbnail Images</legend>
                                                    <asp:Panel runat="server" ID="pnlWarDocuments" Font-Bold="true" Width="740" ScrollBars="Horizontal">
                                                        <asp:Repeater ID="repWarDocuments" runat="server" EnableViewState="false" DataSource='<%# DataBinder.Eval(Container.DataItem, "WarrantyDocuments")%>' OnItemDataBound="repDocuments_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table class="topTable" cellpadding="0" cellspacing="0" border="2">
                                                                    <tr>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <td align="center" width="85">
                                                                    <div ondblclick="return displayImage('<%# DataBinder.Eval(Container.DataItem, "ImageLocation").ToString().Replace("\", "\\") %>','<%# DataBinder.Eval(Container.DataItem, "DocumentTypeName")%>',false)">
                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkShare" runat="server" /></td>
                                                                                <td>
                                                                                    <asp:Image ID="imgDocument" Title='<%# DataBinder.Eval(Container.DataItem, "ImageInfo")%>' runat="server" Width="50" Height="50" BorderStyle="Solid" BorderWidth="1" ImageAlign="Middle" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "ImageLocationThumbnail") %>' /></td>
                                                                </td>
                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <asp:Label ID="lblDocName" runat="server" Font-Size="X-Small"><b><%# DataBinder.Eval(Container.DataItem, "ImageInfolbl")%></b></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                </table>
                                                                                    </div>
                                                                                </td>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tr>
                                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </asp:Panel>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </div>

                            <div id="divMainAssignment_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabVehDetcontent">
                                <div class="tabAssignment active">
                                    <button class="tabAssignmentlinks active" onclick="MenuTab(event, 'tabAssignmentlinks','divAssignment_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>','tabAssignmentcontent');return false;">Assignment</button>
                                    <button class="tabAssignmentlinks" onclick="MenuTab(event, 'tabAssignmentlinks','divDocuments_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>','tabAssignmentcontent');return false;">Documents</button>
                                    <button class="tabAssignmentlinks" onclick="MenuTab(event, 'tabAssignmentlinks','divConcessions_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>','tabAssignmentcontent');return false;">Concessions</button>
                                    <div style="float: left; width: 50%; max-width: 200px;">
                                        <table style="width: 100%; margin-left: 25px" cellspacing="0" cellpadding="0" border="0">
                                            <tr>
                                                <td style="width: 33%">
                                                    <asp:Button ID="btnHistory" runat="server" CssClass="formbutton" Text="History" OnClientClick="return viewAssignmentHistory(this,'btnHistory')" />
                                                </td>
                                                <td style="width: 33%" align="center">
                                                    <input type="button" class="formbutton" value="Profile" height="16px" onclick="showAssignmentProfile(this, 'btnProfile')" id="btnProfile" runat="server" />
                                                </td>
                                                <td style="width: 33%" align="Right">
                                                    <!-- input type="button" class="formbutton" value="Save" height="16px" / -->
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div id="divAssignment_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabAssignmentcontent showDefaultTab">
                                    <table style="height: 314px; width: 755px; border-color: #667; border-spacing: 0px;">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                <!-- Assignment Details -->
                                                <div id="divAssignmentScroll" style="width: 755px; height: 275px; overflow: auto scroll;">
                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                        <tr style="vertical-align: top;">
                                                            <!-- Left Side Assignment -->
                                                            <td>
                                                                <table class="topTable" style="width: 310px; border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <!-- a style="font-weight: bold" onclick="return ShopOnClick(this,'txtShopAssignmentAssignTo')" onmouseover="showToolTip(this)" onmouseout="bMouseInToolTip = false;window.setTimeout('hideToolTip()', 1000)"><u>Assign. To:</u></!-->

                                                                            <%--<a style="font-weight: bold" onmouseover="showToolTip(this)" onmouseout="bMouseInToolTip = false;window.setTimeout('hideToolTip()', 1000)"></a>--%>
                                                                            <asp:LinkButton ID="lbtnAssignTo" runat="server" onmouseover="showToolTip(this,'lbtnAssignTo')" OnClientClick="return ShopOnClick(this,'lbtnAssignTo')" onmouseout="bMouseInToolTip = true;window.setTimeout('hideToolTip()', 1000)">
                                                                                <img src="images/doc_single.gif" />
                                                                                            <u>Assign. To:</u>
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtShopAssignmentAssignTo" runat="server" ReadOnly="true" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentAssignTo")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Contact:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtShopAssignmentContact" runat="server" CssClass="inputField" ReadOnly="true" Width="150px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopContact")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Phone:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtShopAssignmentContactPhone" runat="server" CssClass="inputField" Width="150px" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopPhone")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Fax:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtShopAssignmentContactFax" runat="server" CssClass="inputField" Width="150px" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopFax")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Elec. Status:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtVANAssignmentStatusName" runat="server" ReadOnly="true" CssClass="inputField" Width="210px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentElecStatus")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Fax Status:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="FAXAssignmentStatusName" runat="server" CssClass="inputField" Width="210px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentFaxStatus")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                </table>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Assgn. Dt.:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtAssignmentDate" runat="server" CssClass="inputField" Width="70px" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentAssignDate")%>'></asp:TextBox></td>
                                                                        <td style="width: 40px; padding: 0px; margin: 0px; border: 0px;">&nbsp;Prev.:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtAssignmentPrevDate" runat="server" CssClass="inputField" Width="70px" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentPrevDate")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                </table>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Work Status:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtAssignmentWorkStatus" runat="server" CssClass="inputField" Width="200px" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentWorkStatus")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Eff. Ded. Sent:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtAssignmentDeductSent" runat="server" CssClass="inputField" Width="80px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentDeductSent")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Remarks:
                                                                                <asp:ImageButton ID="imgbtnRemarks" runat="server" ImageUrl="images/ShowLess.gif" alt="More Remarks" OnClientClick="return openMoreRemarks(this,'imgbtnRemarks')" />
                                                                        </td>
                                                                        <td style="vertical-align: top;">
                                                                            <asp:TextBox ID="txtAssignmentRemarks" runat="server" CssClass="inputField" Width="200px" Height="30px" TextMode="MultiLine" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentRemarks")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                </table>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="padding: 0px; margin: 0px; border: 0px;">
                                                                            <br />
                                                                            <asp:Button runat="server" ID="btnSelectShop" CssClass="formbutton" OnClientClick="return SelectShop(this,'btnSelectShop');" Text="Select" />
                                                                            <asp:Button runat="server" ID="btnSendAssignment" CssClass="formbutton" Width="120" Text="Send Assignment" OnClientClick="return doAssignment(this,'btnSendAssignment');" OnClick="btnSendAssignment_Click" CommandArgument='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.ClaimAspectServiceChannelID") + "," + DataBinder.Eval(Container, "DataItem.ShopAssignmentData.InsuranceCompanyID") + "," + DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentPreferredCommunicationMethodID") %>' />
                                                                            <asp:Button runat="server" ID="btnResendAssignment" CssClass="formbutton" Width="120" Text="Resend Assignment" OnClientClick="return doAssignment(this,'btnResendAssignment');" OnClick="btnSendAssignment_Click" CommandArgument='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.ClaimAspectServiceChannelID") + "," + DataBinder.Eval(Container, "DataItem.ShopAssignmentData.InsuranceCompanyID") + "," + DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentPreferredCommunicationMethodID") %>' />
                                                                            <asp:Button runat="server" ID="btnCancelAssignment" CssClass="formbutton" Width="120" Text="Cancel Assignment" OnClientClick="return doCancelAssignment(this,'btnCancelAssignment');" OnClick="btnShopCancel_Click" />
                                                                            <br />
                                                                            <asp:Button runat="server" ID="btnSaveAssignment" CssClass="formbutton" OnClientClick="return saveAssignment(this,'btnSaveAssignment');" OnClick="btnSaveAssignment_Click" Text="Save" CommandArgument='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.ClaimAspectServiceChannelID") + "," + DataBinder.Eval(Container, "DataItem.ShopAssignmentData.InsuranceCompanyID")%>' />
                                                                            <!-- button type="button" id="btnSaveAssignment1" )'>Save1 </ -->
                                                                            <asp:Label runat="server" ID="lblSaveAssignment" ForeColor="Blue" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <!-- Right Side Assignment -->
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td>
                                                                            <asp:Panel runat="server" ID="CurrentEstimate" GroupingText="Current Estimate" Font-Bold="true" Width="400" Height="65" ScrollBars="None">
                                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Gross Amt:</td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtGrossAmt" runat="server" CssClass="inputField" Width="100px" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.CurEstGrossRepairTotal")%>'></asp:TextBox></td>
                                                                                        <td style="width: 140px; padding: 0px; margin: 0px; border: 0px;">&nbsp;Deduct Applied:</td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtDeductibleApplied" runat="server" CssClass="inputField" Width="100px" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.CurEstDeductiblesApplied")%>'></asp:TextBox></td>
                                                                                    </tr>
                                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Net Amt:</td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtNetRepairTotal" runat="server" CssClass="inputField" Width="100px" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.CurEstNetRepairTotal")%>'></asp:TextBox></td>
                                                                                        <td style="width: 140px; padding: 0px; margin: 0px; border: 0px;">&nbsp;Limits Applied:</td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtLimitsEffect" runat="server" CssClass="inputField" Width="100px" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.CurEstLimitsEffect")%>'></asp:TextBox></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                                <div id="divDADRDetail" runat="server">
                                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td style="width: 280px;">&nbsp;</td>
                                                                            <td>
                                                                                <button type="button" id="btnVehAddCoverage" class="formbutton" onclick='addCoverageApplied("AddVehDACoverage.aspx","?LynxID=<%=sLynxID %>&InscCompID=<%=sInscCompany %>&UserID=<%=sUserID %>&Params=<%=sParams %>&ClaimAspectServiceChannelID=<%=sClaimAspectServiceChannelID %>","ModalPopUp","toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=425px,height=280px,left = 490,top=300,center=1,help=0,scroll=0,status=0;")'>Add </button>
                                                                                <button type="button" id="btnVehEditCoverage" class="formbutton" onclick='editCoverageApplied("AddVehDACoverage.aspx","?LynxID=<%=sLynxID %>&InscCompID=<%=sInscCompany %>&UserID=<%=sUserID %>&Params=<%=sParams %>&ClaimAspectServiceChannelID=<%=sClaimAspectServiceChannelID %>","ModalPopUp","toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=425px,height=280px,left = 490,top=300,center=1,help=0,scroll=0,status=0;")'>Edit </button>
                                                                                <button type="button" id="btnVehDelCoverage" class="formbutton" onclick='doDelCoverage("DelCoverage.aspx", "?LynxID=<%=sLynxID %>&InscCompID=<%=sInscCompany %>&UserID=<%=sUserID %>&SelectedClaimCoverageID=<%=sSelectedClaimCoverageID %>&SelectedClientCoverageTypeID=<%=sSelectedClientCoverageTypeID %>&CoverageSysLastUpdatedDate=<%=sCoverageSysLastUpdatedDate %>","ModalPopUp","toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=425px,height=120px,left = 490,top=300,center=1,help=0,scroll=0,status=0;")'>Del </button>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td>
                                                                                <asp:GridView ID="gvAssignmentCoverage" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" Visible="true">
                                                                                    <AlternatingRowStyle BackColor="#F7F7F7" />
                                                                                    <Columns>
                                                                                        <asp:BoundField DataField="Coverage" HeaderText="Coverage" SortExpression="Coverage" HeaderStyle-Width="400px" HeaderStyle-HorizontalAlign="Left" />
                                                                                        <asp:BoundField DataField="Deductable" HeaderText="Ded. Applied" SortExpression="Deductable" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" />
                                                                                        <asp:BoundField DataField="Overage" HeaderText="Overage/Limit" SortExpression="Overage" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                                                        <asp:BoundField DataField="Partial" HeaderText="Partial" SortExpression="Partial" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                                                                                    </Columns>
                                                                                    <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                                                    <HeaderStyle BackColor="#629ad6" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                                    <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                                                                    <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                                                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                                </asp:GridView>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>

                                                                <%--11NOV--%>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="5" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">Disposition:</td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlDisposition" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:Label ID="lblInspection" runat="server" Text="Inspection:" />
                                                                        </td>
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:TextBox ID="txtInspectionDate" runat="server" CssClass="inputField" Width="70px" Text='<%#DataBinder.Eval(Container, "DataItem.InspectionDate")%>'></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 30px; padding: 0px; margin: 0px; border: 0px;">&nbsp;
                                                                        </td>
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:Label ID="lblOrigEstDate" runat="server" Text="Orig. Est. Dt.:" />
                                                                        </td>
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:TextBox ID="txtOriginalEstimateDate" runat="server" CssClass="inputField" Width="70px" Text='<%#DataBinder.Eval(Container, "DataItem.OriginalEstimateDate")%>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>

                                                                    <tr style="vertical-align: middle; padding: 0px; margin: 0px;">
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:Label ID="lblRepairZip" runat="server" Text="Repair Zip:" />
                                                                        </td>
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:TextBox ID="txtRepairZip" runat="server" CssClass="inputField" Width="70px" Text='<%#DataBinder.Eval(Container, "DataItem.InspectionDate")%>'></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 30px; padding: 0px; margin: 0px; border: 0px;">&nbsp;
                                                                        </td>
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:Label ID="lblRepairCity" runat="server" Text="City:" />
                                                                        </td>
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:TextBox ID="txtRepairCity" runat="server" CssClass="inputField" Width="70px" onchange="return populateCounty(this, 'txtRepairCity');" AutoPostBack="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.RepairLocationCity")%>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>

                                                                    <tr style="vertical-align: middle; padding: 0px; margin: 0px;">
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:Label ID="lblRepairStartDate" runat="server" Text="Repair Start Dt.:" />
                                                                        </td>
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:TextBox ID="txtWorkStartDate" runat="server" CssClass="inputField" Width="70px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.WorkStartDate")%>'></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 30px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:ImageButton ID="btnRepairStart" runat="server" ImageUrl="images/tick.gif" OnClientClick="return confirmRepairStart(this, 'btnRepairStart')" OnClick="btnRepairStart_Click" Style='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.WorkStartConfirmFlag")%>' />
                                                                        </td>
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:Label ID="lblRepairEndDate" runat="server" Text="End Dt.:" />
                                                                        </td>
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:TextBox ID="txtWorkEndDate" runat="server" CssClass="inputField" Width="70px"  Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.WorkEndDate")%>'></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 30px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:ImageButton ID="btnRepairEnd" runat="server" ImageUrl="images/tick.gif" OnClientClick="return confirmRepairEnd(this, 'btnRepairEnd')" OnClick="btnRepairEnd_Click" Style='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.WorkEndConfirmFlag")%>' />
                                                                        </td>
                                                                    </tr>

                                                                    <tr style="vertical-align: middle; padding: 0px; margin: 0px;">
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:Label ID="lblRepairState" runat="server" Text="State:" />
                                                                        </td>
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:DropDownList ID="ddlRepairState" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList>
                                                                        </td>
                                                                        <td style="width: 30px; padding: 0px; margin: 0px; border: 0px;">&nbsp;
                                                                        </td>
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:Label ID="lblRepairCounty" runat="server" Text="County:" />
                                                                        </td>
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:DropDownList ID="ddlRepairCounty" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList>
                                                                        </td>
                                                                    </tr>

                                                                    <tr style="vertical-align: middle; padding: 0px; margin: 0px;">
                                                                        <td style="width: 100px; padding: 0px; margin: 0px; border: 0px;">
                                                                            <asp:Label ID="lblFinalEstDate" runat="server" Text="Final Est. Rcvd. Dt.:" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtFinalEstDate" runat="server" CssClass="inputField" Width="70px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AppraiserInvoiceDate")%>'></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 30px; padding: 0px; margin: 0px; border: 0px;">&nbsp;
                                                                        </td>
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">&nbsp;
                                                                            <asp:Label ID="lblCODate" runat="server" Text="C/O Dt.:" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtCODate" runat="server" CssClass="inputField" Width="70px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.CashOutDate")%>'></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 30px; padding: 0px; margin: 0px; border: 0px;">&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>



                                <!-- Assignment History -->
                                <div id="divAssignmentHistory" name="divAssignmentHistory" style="display: none; width: 520px; height: 100px; position: absolute; background-color: #FFFFFF; padding: 3px; border: 2px solid #FFA500; background-color: #F5F5DC; filter1: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#2F4F4F,strength=5);" runat="server">
                                    <table cellspacing="0" cellpadding="2" border="0" style="table-layout: fixed; width: 100%; background-color: #FFFFFF; border-collapse: collapse;">
                                        <tr>
                                            <td colspan="2" style="font-weight: bold">Assignment History
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="font-weight: bold">
                                                <img src="images/background_top.gif" style="height: 2px; width: 125px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:GridView ID="gvAssignmentHistory" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
                                                    <AlternatingRowStyle BackColor="#F7F7F7" />
                                                    <Columns>
                                                        <asp:BoundField DataField="SelectionDate" HeaderText="Selection Date" SortExpression="SelectionDate" ItemStyle-Wrap="false" HeaderStyle-Width="1px" HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="AssignmentDate" HeaderText="Assignment Date" SortExpression="AssignmentDate" ItemStyle-Wrap="false" HeaderStyle-Width="1px" HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="CancellationDate" HeaderText="Cancellation Date" SortExpression="CancellationDate" ItemStyle-Wrap="false" HeaderStyle-Width="1px" HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="AppraiserName" HeaderText="Appraiser Name" SortExpression="AppraiserName" ItemStyle-Wrap="false" HeaderStyle-Width="1px" HeaderStyle-HorizontalAlign="Left" />
                                                    </Columns>
                                                    <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                    <HeaderStyle BackColor="#629ad6" Font-Bold="True" ForeColor="#F7F7F7" />
                                                    <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                                    <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />

                                                </asp:GridView>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Button ID="btnAssignmentHistoryClose" runat="server" Text="Close" OnClientClick="return closeAssignmentHistory(this,'btnAssignmentHistoryClose')" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <!-- Documents Tab in Program Shop Tab Start -->
                                <div id="divDocuments_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabAssignmentcontent">

                                    <table style="height: 280px; width: 755px; border-color: #667; border-spacing: 0px;">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                <!-- Estimate Details -->
                                                <div id="divEstimateScroll" style="width: 755px;">
                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                        <tr style="vertical-align: top;">
                                                            <!-- Top View Estimate -->
                                                            <td>
                                                                <fieldset>
                                                                    <legend style="font-weight:bold">Estimates</legend>
                                                                    <asp:Panel runat="server" ID="pnlEstimate" Font-Bold="true" Height="110" ScrollBars="Vertical">
                                                                        <asp:GridView ID="gvEstimate" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnRowDataBound="gvEstimate_RowDataBound">
                                                                            <AlternatingRowStyle BackColor="#F7F7F7" />
                                                                            <Columns>
                                                                                <asp:BoundField DataField="EstRecvDate" HeaderText="Recd./Attach Date" SortExpression="EstRecvDate" HeaderStyle-Width="20%" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:ImageField DataImageUrlField="EstSummary" HeaderText="Summary" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left"></asp:ImageField>
                                                                                <asp:BoundField DataField="EstSeq" HeaderText="Seq." SortExpression="EstSeq" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="EstSource" HeaderText="Source" SortExpression="EstSource" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="EstGross" HeaderText="Gross $" SortExpression="EstGross" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="EstNet" HeaderText="Net $" SortExpression="EstNet" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="EstOrgAud" HeaderText="Org./Audited" SortExpression="EstOrgAud" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="EstDup" HeaderText="Dup." SortExpression="EstDup" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="EstAgreed" HeaderText="Agreed" SortExpression="EstAgreed" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" />
                                                                            </Columns>
                                                                            <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                                            <HeaderStyle BackColor="#629ad6" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                            <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                                                            <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                                                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
                                                                    <tr class="sectionTitle" style="height: 16px; padding: 0px; margin: 0px; color: #F7F7F7; background-color: #629AD6; font-weight: bold;">
                                                                        <td>
                                                                            <asp:ImageButton ID="imgbtnPinned" runat="server" ImageUrl="/images/pinned.gif" OnClientClick="return togglePinned(this,'imgbtnPinned')" ToolTip="Unpin/Pin from/to Documents tab" Width="18" Height="18" />
                                                                        </td>
                                                                        <td style="text-align: center">Thumbnail Images</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <fieldset>
                                                                    <legend style="font-weight:bold">Thumbnail Images</legend>
                                                                    <asp:Panel runat="server" ID="pnlDocuments" Font-Bold="true" Width="740" ScrollBars="Horizontal">
                                                                        <asp:Repeater ID="repDocuments" runat="server" EnableViewState="false" DataSource='<%# DataBinder.Eval(Container.DataItem, "Documents")%>' OnItemDataBound="repDocuments_ItemDataBound">
                                                                            <HeaderTemplate>
                                                                                <table class="topTable" cellpadding="0" cellspacing="0" border="2">
                                                                                    <tr>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <td align="center" width="85">
                                                                                    <div ondblclick="return displayImage('<%# DataBinder.Eval(Container.DataItem, "ImageLocation").ToString().Replace("\", "\\") %>','<%# DataBinder.Eval(Container.DataItem, "DocumentTypeName")%>',false)">
                                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkShare" runat="server" /></td>
                                                                                                <td>
                                                                                                    <asp:Image ID="imgDocument" Title='<%# DataBinder.Eval(Container.DataItem, "ImageInfo")%>' runat="server" Width="50" Height="50" BorderStyle="Solid" BorderWidth="1" ImageAlign="Middle" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "ImageLocationThumbnail") %>' /></td>
                                                                                </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <asp:Label ID="lblDocName" runat="server" Font-Size="X-Small"><b><%# DataBinder.Eval(Container.DataItem, "ImageInfolbl")%></b></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                </table>
                                                                                    </div>
                                                                                </td>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </tr>
                                                                                </table>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </asp:Panel>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                </div>

                                <!-- Consessions Tab in Program Shop Tab Start -->
                                <div id="divConcessions_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabAssignmentcontent">

                                    <table style="height: 275px; width: 755px; border-color: #667; border-spacing: 0px;">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                <!-- Concession Details -->
                                                <div id="divConcessionScroll" style="width: 755px; height: 275px; overflow: auto scroll;">
                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>Indemnity Total:
                                                                    <asp:Label runat="server" ID="lblConcessionTotal" ForeColor="Blue" /></td>

                                                        </tr>
                                                        <tr style="vertical-align: top;">
                                                            <!-- Top Full Screen -->
                                                            <td colspan="2">
                                                                <asp:GridView ID="gvConcessions" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" Visible="true" OnRowDataBound="gvConcessions_RowDataBound">
                                                                    <AlternatingRowStyle BackColor="#F7F7F7" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="ClaimAspectServiceChannelConcessionID" HeaderText="ClaimAspectServiceChannelConcessionID" SortExpression="ClaimAspectServiceChannelConcessionID" HeaderStyle-Width="1px" HeaderStyle-HorizontalAlign="Left" />
                                                                        <asp:BoundField DataField="CreatedDate" HeaderText="Last Updated" SortExpression="CreatedDate" HeaderStyle-Width="118px" HeaderStyle-HorizontalAlign="Left" />
                                                                        <asp:BoundField DataField="TypeDescription" HeaderText="Type" SortExpression="TypeDescription" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                                                                        <asp:BoundField DataField="ReasonDescription" HeaderText="Reason" SortExpression="ReasonDescription" HeaderStyle-Width="300px" HeaderStyle-HorizontalAlign="Left" />
                                                                        <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                                    </Columns>
                                                                    <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                                    <HeaderStyle BackColor="#629ad6" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                    <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                                                    <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                </asp:GridView>
                                                                <asp:HiddenField ID="hidClaimAspectServiceChannelConcessionID" runat="server" Value='' />

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="align-content: flex-end; padding-top: 5px;">
                                                                <asp:Button ID="btnAddConcession" runat="server" CssClass="formbutton" Text="Add" OnClientClick="return AddConcession(this,'btnAddConcession')" />
                                                                &nbsp; 
                                                                    <asp:Button ID="btnConcessionEdit" runat="server" CssClass="formbutton" Text="Edit" OnClientClick="return EditConcession(this,'btnConcessionEdit')" />
                                                                <asp:Button ID="btnDelConcession" runat="server" CssClass="formbutton" Text="Delete" OnClientClick="return doConcessionDelete(this,'btnDelConcession')" OnClick="btnDelConcession_Click" />
                                                                &nbsp; 
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                </div>


                            </div>

                            </div>

                            <!-- Shop details -->
                            <div id="divShopDetail" runat="server" style="position: absolute; width:600px; top: 100px; left: 94px; display: none; overflow: auto; filter: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)" onmouseenter="bMouseInToolTip=false;showToolTip(this,'divShopDetail')" onmouseleave="bMouseInToolTip=true;hideToolTip()">
                                <asp:Panel runat="server" BorderStyle="Solid" BorderWidth="1px">
                                    <table border="0" cellspacing="2" cellpadding="0" style="height: 100%; width: 100%; background-color: #FFFFFF; border-collapse: collapse; table-layout: fixed">
                                        <colgroup>
                                            <col width="150px" />
                                            <col width="*" />
                                            <col width="25px" />
                                            <col width="55px" />
                                            <col width="75px" />
                                            <col width="20px" />
                                        </colgroup>
                                        <tr style="height: 21px; text-align: center; font-weight: bold; background-color: #556B2F; color: #FFFFFF">
                                            <td colspan="12">Shop Detail</td>
                                        </tr>
                                        <tr>
                                            <td align="right"><b>Program Shop:&nbsp;</b></td>
                                            <td colspan="5">
                                                <asp:TextBox runat="server" ID="popProgramShop" Width="50" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentProgramType")%>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right"><b>Communication Method:&nbsp;</b></td>
                                            <td colspan="5">
                                                <asp:TextBox runat="server" ID="popPreferredCommunicationMethod" Width="200" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentPreferredCommunicationMethodName")%>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right"><b>Pref Estimate Package:&nbsp;</b></td>
                                            <td colspan="5">
                                                <asp:TextBox runat="server" ID="popPreferredEstimatePackage" Width="200" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentPreferredEstimatePackageName")%>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right"><b>Name:&nbsp;</b></td>
                                            <td colspan="5">
                                                <asp:TextBox runat="server" ID="popShopName" Width="200" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentAssignTo")%>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right"><b>Address:&nbsp;</b></td>
                                            <td colspan="5">
                                                <asp:TextBox runat="server" ID="popShopAddress1" Width="200" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopAddress1")%>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right"><b>&nbsp;</b></td>
                                            <td colspan="5">
                                                <asp:TextBox runat="server" ID="popShopAddress2" Width="200" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopAddress2")%>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right"><b>City:&nbsp;</b></td>
                                            <td colspan="5">
                                                <asp:TextBox runat="server" ID="popShopAddressCity" Width="150" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopAddressCity")%>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right"><b>County:&nbsp;</b></td>
                                            <td colspan="5">
                                                <asp:TextBox runat="server" ID="popShopAddressCounty" Width="150" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopAddressCounty")%>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right"><b>State:&nbsp;</b></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="popShopAddressState" Width="25" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopAddressState")%>' />
                                            </td>
                                            <td align="right"><b>Zip:&nbsp;</b></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="popShopAddressZip" Width="45" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopAddressZip")%>' />
                                            </td>
                                            <td align="right"><b>MSA Code:&nbsp;</b></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="popShopMSACode" Width="45" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopMSACode")%>' />
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellspacing="2" cellpadding="0" style="height: 100%; width: 100%; background-color: #FFFFFF; border-collapse: collapse; table-layout: fixed">
                                        <colgroup>
                                            <col width="150px" />
                                            <col width="*" />
                                        </colgroup>
                                        <tr style="width: 100px">
                                            <td style="width: 35px" align="right"><b>Phone:&nbsp;</b></td>
                                            <td colspan="5">
                                                <asp:TextBox runat="server" ID="popShopPhone" Width="100" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopPhone")%>' />
                                            </td>
                                            <td style="width: 35px" align="right"><b>Fax:&nbsp;</b></td>
                                            <td colspan="5">
                                                <asp:TextBox runat="server" ID="popShopFax" Width="100" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopFax")%>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6"></td>
                                        </tr>
                                        <tr>
                                            <td align="right"><b>Contact:&nbsp;</b></td>
                                            <td colspan="5">
                                                <asp:TextBox runat="server" ID="popShopContact" Width="175" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopContact")%>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right"><b>Phone:&nbsp;</b></td>
                                            <td colspan="5">
                                                <asp:TextBox runat="server" ID="popShopContactPhone" Width="100" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopContactPhone")%>' />
                                            </td>
                                            <td align="right"><b>Email:&nbsp;</b></td>
                                            <td colspan="5">
                                                <asp:TextBox runat="server" ID="popShopContactEmail" Width="200" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopContactEmail")%>' />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate></FooterTemplate>

                    </asp:Repeater>


                    <!-- Vehicle Details repeater starts here -->


                    <!-- VEHICLE SECTION ENDED HERE -->

                </td>
            </tr>
        </table>
        <div id="divBackground" style="position: fixed; z-index: 999; height: 100%; width: 100%; top: 0; left: 0; background-color: Black; filter: alpha(opacity=60); opacity: 0.6; -moz-opacity: 0.8; display: none">
        </div>
        <div id="divMaskAll" name="divMaskAll" style="display: none; position: absolute; top: 0px; left: 0px; height: 100%; width: 100%; background-color: #000000; filter: progid:DXImageTransform.Microsoft.Alpha( style=0,opacity=25);">
        </div>
    </form>
</body>


<script type="text/javascript">
    $(document).ready(function () {
        SessionPageLoad();

        //-----------------------------------//
        // Page Init - CRUD Control Actions
        //-----------------------------------//
        var strClaim_ClaimAspectID = $("#hidClaim_ClaimAspectID").val();
        var strDocumentCRUD = $("#hidDocumentCRUD").val();

        //- C-Create
        if (strDocumentCRUD.indexOf("C") == -1)
            top.DocumentsMenu(false, 1);

        top.sClaim_ClaimAspectID = strClaim_ClaimAspectID;

        $("[id$=btnVehRental]").css("display", "block");
        if ($("#hidInsuranceCompanyID").val() != "184") {
            $("[id$=btnVehRental]").css("display", "none");
        }


        //-- Not used, but this is good way to refresh the todo and notes --//
        //top.refreshNotesWindow( gsLynxID, gsUserID );
        //top.refreshCheckListWindow( gsLynxID, gsUserID );
        //ServerEvent();

        //-- Test Area of ShowEstimate
        //var strWindowID = $("#hidWindowID").val();
        //var strLynxID = $("#hidLynxID").val();
        //var docID =
        //var showTotals = 
        //ShowEstimate(strWindowID, strLynxID, docId, showTotals)

    });

    pageInit();
</script>

</html>