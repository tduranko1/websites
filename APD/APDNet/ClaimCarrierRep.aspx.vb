﻿'--------------------------------------------------------------
' Program: Claim Carrier Rep Detail
' Author:  Mahes
' Date:    Jul 02, 2013
' Version: 1.0
'
' Description:  This site provides the ClaimCarrierRep Details for APD replacing the 
'               existing COM based classic asp functions. 
'--------------------------------------------------------------
Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Public Class ClaimCarrierRep
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService

    '--------------------------------------------------------------
    ' Page Load
    '--------------------------------------------------------------

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '-------------------------------
        ' Globals
        '-------------------------------
        Dim sLynxID As String = ""
        Dim sUserID As String = ""
        Dim sInsuranceCompanyID As String = ""
        Dim sCarrierRepUserID As String = ""
        Dim sSysLastUpdatedDate As String = ""
        Dim sCarrierCRUD = ""
        Dim sParams As String = ""
        Dim oSession As Object
        Dim sXslHash As New Hashtable
        Dim XMLCRUD As XmlElement
        Dim sVehCount = ""
        Dim uspXMLRaw As XmlElement
        Dim uspXMLXDoc As New XmlDocument
        Dim sAPDNetDebugging As String = ""

        '-------------------------------
        ' Session/Local Variables
        '-------------------------------
        Dim sStoredProcedure As String = "uspClaimCarrierRepGetListWSXML"
        Dim sXSLProcedure As String = "ClaimCarrierRep.xsl"

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        Dim sSessionKey = Request.QueryString("SessionKey")
        Dim sWindowID = Request.QueryString("WindowID")
        sCarrierRepUserID = Request.QueryString("CarrierRepUserID")
        sSysLastUpdatedDate = Request.QueryString("SysLastUpdatedDate")

        Try
            'If sSessionKey = "" Then
            '    Throw New SystemException("SessionValidationError: No session information received from classic ASP page.")
            'Else
            '    'Dim APDDBUtils As New APDDBUtilities(sSessionKey, sWindowID)

            '    '---------------------------------
            '    ' Debug Data
            '    '---------------------------------
            '    If UCase(APDDBUtils.APDNetDebugging) = "TRUE" Then
            '        wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "ClaimCarrierRep - Start: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
            '    End If

            '    '---------------------------------
            '    ' Get the session data
            '    '---------------------------------
            '    If Not Request("LynxID") Then
            '        sLynxID = APDDBUtils.LynxID
            '    Else
            '        sLynxID = Request("LynxID")
            '    End If

            '    If Not Request("UserID") Then
            '        sUserID = APDDBUtils.UserID
            '    Else
            '        sUserID = Request("UserID")
            '    End If

            '    oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "InsuranceCompanyID-" & sWindowID)
            '    sInsuranceCompanyID = oSession(0).SessionValue

            '    '---------------------------------
            '    ' Get the permission data
            '    '---------------------------------
            '    XMLCRUD = wsAPDFoundation.ExecuteSpAsXML("uspCRUDGetDetailWSXML", "Carrier, " & sUserID)
            '    sCarrierCRUD = APDDBUtils.GetCrud(XMLCRUD)

            '    '-----------------------------------
            '    ' Add config variables as XslParams
            '    '-----------------------------------
            '    sXslHash.Add("LynxID", sLynxID)
            '    sXslHash.Add("UserID", sUserID)
            '    sXslHash.Add("CarrierRepUserID", sCarrierRepUserID)
            '    sXslHash.Add("SysLastUpdatedDate", sSysLastUpdatedDate)
            '    ' Need to clarify with TOM
            '    sXslHash.Add("Claim", sCarrierCRUD)

            '    '---------------------------------
            '    ' Debug Data
            '    '---------------------------------
            '    If UCase(APDDBUtils.APDNetDebugging) = "TRUE" Then
            '        wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls/Parameters." _
            '                                 , "Session controls for Window: " & sWindowID _
            '                                 & " and SessionKey: " & sSessionKey _
            '                                 , "Vars: StoredProc = " & sStoredProcedure _
            '                                 & ", XSLPage = " & sXSLProcedure _
            '                                 & ", UserID(Session) = " & sUserID _
            '                                 & ", LynxID(Session) = " & sLynxID _
            '                                 & ", CarrierRepUserID = " & sCarrierRepUserID _
            '                                 & ", SysLastUpdatedDate = " & sSysLastUpdatedDate _
            '                                 )
            '    End If

            '    '---------------------------------
            '    ' XSL Transformation
            '    '---------------------------------
            '    sParams = sInsuranceCompanyID

            '    '---------------------------------
            '    ' StoredProc XML Data
            '    '---------------------------------
            '    uspXMLRaw = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '    If uspXMLRaw.OuterXml = Nothing Then
            '        ' Throw exception
            '    Else
            '        uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)

            '        Response.Write(APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sParams, sXslHash))
            '    End If

            '    If UCase(APDDBUtils.APDNetDebugging) = "TRUE" Then
            '        wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "ClaimCarrierRep.aspx - Finished: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
            '    End If
            'End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls/parameter not found or page pre-transformation failed.", "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID)

            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        End Try

    End Sub

End Class