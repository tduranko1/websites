﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddCoverage.aspx.vb" Inherits="APDNet.AddCoverge" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Claim Add Coverage</title>
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />

    <!-- ============== JQuery - Loader =============== -->
    <script type="text/javascript" src="js/jQuery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/jQuery/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jQuery/ClaimTabs.js"></script>

    <link rel="stylesheet" href="css/jquery-ui.css" />
    <%--    <script type="text/javascript" src="Scripts/ClaimTabs.js"></script>
    <script type="text/javascript" src="Scripts/APDData.js"></script>--%>
    <script type="text/javascript">
        function OnClose(strSource) {
            //alert(window.opener.location);
            if (window.opener != null && !window.opener.closed) {
                window.opener.HideModalDiv();
            }
            if (strSource == "Save") {
                window.opener.location.href = window.opener.location.href;
                window.close();
            }
            else {
                window.close();
            }
        }
        function addCoverageValidator() {
            var coverageDesc = document.getElementById("ddlCoverageDescription").value;
            if (coverageDesc == null || coverageDesc == 0 || coverageDesc == "") {
                alert("Please Select the Description");
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="frmAddCoverage" runat="server">
        <div id="diaAddCoverage" title="Add Coverage">
            <!-- Title Bar -->
            <%--            <table style="padding: 0; width: 300px; border: 0;">
                <tr>
                    <td style="text-align: left; vertical-align: top; width: 8px; background-image: url(images/PanelLeft.png); background-repeat: no-repeat;" />
                    <td style="text-align: left; vertical-align: top; background-image: url(images/PanelMiddle.png); background-repeat: repeat;" class="Header">
                        <a name="InvoiceInfo" style="color: White; font-weight: bold;">LynxID:
                            <asp:Label ID="lblLynxID" runat="server"></asp:Label>
                        </a>
                    </td>
                    <td style="text-align: left; vertical-align: top; width: 8px; background-image: url(images/PanelRight.png); background-repeat: no-repeat;" />
                </tr>
            </table>--%>
            <!-- /Title Bar -->

            <table class="topTable" style="border-spacing: 0px; width: 350px;">
                <tr style="vertical-align: top;">
                    <td style="width: 95px;">
                        <b>LynxID:</b>
                    </td>
                    <td>
                        <asp:TextBox ID="txtLynxID" runat="server" CssClass="inputField" Enabled="false" />
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Client Code:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageClientCode" runat="server" CssClass="inputField" Width="50px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Description:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCoverageDescription" runat="server" CssClass="inputField" Width="175px" AutoPostBack="True"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Type:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageType" runat="server" CssClass="inputField" Width="50px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Additional Flag:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageAdditionalFlag" runat="server" CssClass="inputField" Width="175px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Deductible:  $
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageDeductible" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="txtCoverageDeductibleNotBlankValidator" runat="server" ErrorMessage="*Required" ControlToValidate="txtCoverageDeductible" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="txtCoverageDeductibleValidator" runat="server" ControlToValidate="txtCoverageDeductible" ErrorMessage="Numeric only" ForeColor="Red" ValidationExpression="^\d+(\.\d{1,2})?$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Limit:  $
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageLimit" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="txtCoverageLimitNotBlankValidator" runat="server" ErrorMessage="*Required" ControlToValidate="txtCoverageLimit" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="txtCoverageLimitValidator" runat="server" ControlToValidate="txtCoverageLimit" ErrorMessage="Numeric only" ForeColor="Red" ValidationExpression="^\d+(\.\d{1,2})?$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr id="divRentalMax" style="vertical-align: top;">
                    <td style="width: 85px;">
                        <asp:Label ID="lblRentalMax" runat="server" Text="Rental Max Days:" Visible="false"></asp:Label>
                        <asp:RequiredFieldValidator ID="txtCoverageMaxDaysNotBlankValidator" runat="server" ErrorMessage="*Required" ControlToValidate="txtCoverageMaxDays" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="txtCoverageMaxDaysValidator" runat="server" ControlToValidate="txtCoverageMaxDays" ErrorMessage="Numeric only" ForeColor="Red" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageMaxDays" runat="server" CssClass="inputField" Width="50px" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr id="divRentalLimit" style="vertical-align: top;">
                    <td style="width: 85px;">
                        <asp:Label ID="lblRentalLimit" runat="server" Text="Rental Limit:" Visible="false"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageDailyLimit" runat="server" CssClass="inputField" Width="50px" Visible="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="txtCoverageDailyLimitNotBlankValidator" runat="server" ErrorMessage="*Required" ControlToValidate="txtCoverageDailyLimit" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="txtCoverageDailyLimitValidator" runat="server" ControlToValidate="txtCoverageDailyLimit" ErrorMessage="Numeric only" ForeColor="Red" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td>&nbsp;</td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 185px;">
                        &nbsp;
                    </td>
                    <td>
                        <asp:HiddenField ID="hidSelectedCoverageTypeID" runat="server" Value='' />
                        <asp:Button ID="btnCoverageAddSave" runat="server" Text="Save"  onClientClick = "return addCoverageValidator();"/>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:OnClose('cancel'); return false;" />
                    </td>
                </tr>
            </table>

            <!-- Footer Bar -->
            <%--            <table style="padding: 0; width: 300px; border: 0;">
                <tr>
                    <td style="text-align: left; vertical-align: top; width: 8px; background-image: url(images/PanelBotLeft.png); background-repeat: no-repeat;" />
                    <td style="text-align: left; vertical-align: top; background-image: url(images/PanelMiddle.png); background-repeat: repeat;" class="Header">&nbsp;
                    </td>
                    <td style="text-align: left; vertical-align: top; width: 8px; background-image: url(images/PanelBotRight.png); background-repeat: no-repeat;" />
                </tr>
            </table>--%>
            <!-- /Footer Bar -->
        </div>
    </form>
</body>
</html>
