﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DelCoverage.aspx.vb" Inherits="APDNet.DelCoverge" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Claim Delete Coverage</title>
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />

    <!-- ============== JQuery - Loader =============== -->      
    <script type="text/javascript" src="js/jQuery/jquery-1.4.1.min.js"></script>
    <script type="text/javascript" src="js/jQuery/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jQuery/ClaimTabs.js"></script>

    <link rel="stylesheet" href="css/jquery-ui.css" />
<%--    <script type="text/javascript" src="Scripts/ClaimTabs.js"></script>
    <script type="text/javascript" src="Scripts/APDData.js"></script>--%>

    <script type="text/javascript">
        function OnClose() {
            //alert(window.opener.location);
            if (window.opener != null && !window.opener.closed) {
                window.opener.HideModalDiv();
            }
            window.returnValue = "false";
            window.close();
        }

        function doCancel() {
            OnClose();
        }

    </script>
</head>
<body onunload="javascript:refreshParent()">
    <form id="frmDelCoverage" runat="server">
    <div id="diaDelCoverage" title="Del Coverage" >
        <!-- Title Bar -->
<%--        <table style="padding: 0; width: 350px; border: 0; " >
            <tr>
                <td style="text-align: left; vertical-align: top; width: 8px; background-image: url(images/PanelLeft.png); background-repeat:no-repeat;" />
                <td style="text-align: left; vertical-align: top; background-image: url(images/PanelMiddle.png); background-repeat:repeat;" class="Header" >
                    <a name="LynxID" style="color: White; font-weight: bold;" >
                        LynxID: <asp:Label ID="lblLynxID" runat="server"></asp:Label>
                    </a>
                </td>
                <td style="text-align: left; vertical-align: top; width: 8px; background-image: url(images/PanelRight.png); background-repeat:no-repeat;" />
            </tr>
        </table> --%>
        <!-- /Title Bar -->

        <p>
            Do you want to delete the selected coverage?  Click Yes to delete.
        </p>
        <asp:Button runat="server" ID="btnDelCoverageYes" Text="Yes" />
        <asp:Button runat="server" ID="btnDelCoverageNo" Text="No" OnClientClick="javascript:doCancel(); return false;" />
        <asp:Button runat="server" ID="btnDelCoverageCancel" Text="Cancel" OnClientClick="javascript:doCancel(); return false;" />

        <!-- Footer Bar -->
<%--        <table style="padding: 0; width: 350px; border: 0; " >
            <tr>
                <td style="text-align: left; vertical-align: top; width: 8px; background-image: url(images/PanelBotLeft.png); background-repeat:no-repeat;" />
                <td style="text-align: left; vertical-align: top; background-image: url(images/PanelMiddle.png); background-repeat:repeat;" class="Header" >
                    &nbsp;
                </td>
                <td style="text-align: left; vertical-align: top; width: 8px; background-image: url(images/PanelBotRight.png); background-repeat:no-repeat;" />
            </tr>
        </table> --%>
        <!-- /Footer Bar -->
    </div>
    </form>
</body>
</html>
