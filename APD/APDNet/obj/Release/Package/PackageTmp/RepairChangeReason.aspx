﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RepairChangeReason.aspx.vb" Inherits="APDNet.RepairChangeReason" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Repair Change Reason</title>
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />

    <!-- ============== JQuery - Loader =============== -->
    <script type="text/javascript" src="js/jQuery/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="js/jQuery/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jQuery/ClaimTabs.js"></script>

    <link rel="stylesheet" href="css/jquery-ui.css" />
    <%--    <script type="text/javascript" src="Scripts/ClaimTabs.js"></script>
    <script type="text/javascript" src="Scripts/APDData.js"></script>--%>

    <script type="text/javascript">
        function getUrlParameter(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }

        function OnClose(sRet) {
            debugger;
            if (window.opener != null && !window.opener.closed) {
                window.opener.HideModalDiv();

                var objText, strParentDOMID;
                strParentDOMID = getUrlParameter("hidval");

                objText = window.opener.document.getElementById(strParentDOMID);
                objText.value = sRet //document.getElementById("ddlRepairReason").value;
            }
            window.returnValue = "false";
            window.close();
        }

        function doCancel() {
            OnClose('');
            //window.returnValue = "false";
            //window.close();
        }

        function checkOther(ddlID) {
            var oTXTOther = $("#txtOther");
            oTXTOther[0].disabled = !(ddlID.selectedOptions[0].text == "Other");
        }

        function doOk() {
            var sRet;
            var oDDLRepairReason = $("#ddlRepairReason");
            var oTXTOther = $("#txtOther");

            if (oDDLRepairReason[0].selectedIndex == 0) {
                alert("Please select a reason from the list.");
                return;
            }
            debugger;

            if (oDDLRepairReason[0].selectedOptions[0].text == "Other") {
                if (oTXTOther[0].value == "") {
                    alert("Please describe the Other reason and try again.");
                    if (oTXTOther[0].disabled == false)
                        $("#txtOther").focus();
                    return;
                }
                sRet = oDDLRepairReason[0].value + "|" + oTXTOther[0].value;
            } else {
                sRet = oDDLRepairReason[0].value + "|" + oDDLRepairReason[0].selectedOptions[0].text;
            }
            OnClose(sRet);
        }
    </script>
</head>
<body>
    <form id="frmRepairChangeReason" runat="server">
        <div id="diaRepairChangeReason" title="Repair Change Reason">
            <table class="topTable" style="border-spacing: 0px; width: 350px;">
                <tr style="vertical-align: top; ">
                    <td style="width: 95px;">
                        <b>LynxID:</b>
                    </td>
                    <td>
                        <asp:Label ID="lblLynxID" runat="server" CssClass="inputField" Width="20"></asp:Label>
                    </td>
                </tr>
                <tr style="vertical-align: top; ">
                    <td style="width: 95px;">
                        <b>Reason:</b>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlRepairReason" runat="server" CssClass="inputField" Width="260" AutoPostBack="false" onchange="checkOther(this)"></asp:DropDownList>
                    </td>
                </tr>         
                <tr style="vertical-align: top;">
                    <td style="width: 85px;"><b>Other:</b>
                    </td>
                    <td>
                        <asp:TextBox ID="txtOther" runat="server" CssClass="inputField" maxLength="150" width="307" height="75" Enabled="false" TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnReasonOk" runat="server" Text="Ok" OnClientClick="javascript:doOk(); return false;" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
