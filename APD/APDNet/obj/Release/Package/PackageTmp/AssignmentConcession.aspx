﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssignmentConcession.aspx.vb" Inherits="APDNet.AssignmentConcession" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="css/APD2.css" rel="stylesheet" type="text/css" />
       <script type="text/javascript">
         function getUrlParameter(name) {
             name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
             var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
             var results = regex.exec(location.search);
             return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
         }

         //This EventListener used for closing window X mark from window  when server event fire window closed need to check with syed/mubs
        // window.addEventListener("unload", OnClose)

         //window.onbeforeunload = function (event) {
         function OnClose(strSource) {
             debugger;
             if (window.opener != null && !window.opener.closed) {
                 window.opener.HideModalDiv();
                 

                 if (strSource=="Save")
                 {
                     window.opener.location.reload();
                 }

               }
             window.returnValue = "false";
             window.close();
        
        }

        function doCancel(strSource) {
            OnClose(strSource);
 
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td>Type :</td>
                    <td>Reason :</td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlType" runat="server" OnSelectedIndexChanged ="ddlType_SelectedIndexChanged" AutoPostBack="true" CssClass="inputField"  width="100"></asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlReason" runat="server" CssClass="inputField"  width="265"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Concession Amount:</td>
                    <td>
                        <asp:TextBox ID="txtConcessionAmt" runat="server" CssClass="inputField"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Comment:</td>
                </tr>
                <tr>
                    <td colspan="2"> 
                        <asp:TextBox ID="txtComment" runat="server" CssClass="inputField" Width="377px" Height="72px" TextMode="MultiLine"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td> &nbsp;</td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" /> &nbsp; 
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:doCancel('Cancel'); return false;" />
                    </td>
                </tr>

                
            </table>

        </div>
    </form>
</body>
</html>
