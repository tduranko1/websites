﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DamagePick.aspx.vb" Inherits="APDNet.DamagePick" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Vwhicle Damage Pick</title>
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />

    <!-- ============== JQuery - Loader =============== -->
    <script type="text/javascript" src="js/jQuery/jquery-1.4.1.min.js"></script>
    <script type="text/javascript" src="js/jQuery/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jQuery/ClaimTabs.js"></script>

    <link rel="stylesheet" href="css/jquery-ui.css" />
    <%--    <script type="text/javascript" src="Scripts/ClaimTabs.js"></script>
    <script type="text/javascript" src="Scripts/APDData.js"></script>--%>

    <script type="text/javascript">
        function OnClose() {
            if (window.opener != null && !window.opener.closed) {
                window.opener.HideModalDiv();
            }
            window.returnValue = "false";
            window.close();
        }

        function doCancel() {
            OnClose();
        }

    </script>
</head>
<body>
    <form id="frmDamagePick" runat="server">
        <!-- ==================== New Service Channel - Modal Dialog ============================== -->    
        <div id="diaDamagePick" title="DamagePick">
            <table class="topTable" style="border-spacing: 0px; width: 400px;" >
                <tr style="vertical-align: top;">
                    <th class="QueueHeader" style="width:200px;">Primary Impact:</th>
                    <th class="QueueHeader" style="width:200px;">Secondary Impact:</th>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">
                        <asp:DropDownList ID="ddlNewServiceChannel" runat="server" CssClass="inputField" Width="275px" ></asp:DropDownList>
                    </td>
                    <td style="width: 85px;">
                        (Discrete selection - Ctrl + Item; Continuous selection = Shift + Item)
                        <asp:ListBox ID="lstImpactPoints" runat="server" CssClass="inputField" Height="100px"></asp:ListBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">
                        <asp:Button runat="server" ID="btnDamagePick_SAVE" Text="Save" />
                        <asp:Button runat="server" ID="btnDamagePick_CANCEL" Text="Cancel" OnClientClick="javascript:doCancel(); return false;" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
