﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddService.aspx.vb" Inherits="APDNet.AddService" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Vwhicle Add Service Channel</title>
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />

    <!-- ============== JQuery - Loader =============== -->
    <script type="text/javascript" src="js/jQuery/jquery-1.4.1.min.js"></script>
    <script type="text/javascript" src="js/jQuery/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jQuery/ClaimTabs.js"></script>

    <link rel="stylesheet" href="css/jquery-ui.css" />
    <%--    <script type="text/javascript" src="Scripts/ClaimTabs.js"></script>
    <script type="text/javascript" src="Scripts/APDData.js"></script>--%>

    <script type="text/javascript">
        function OnClose() {
            if (window.opener != null && !window.opener.closed) {
                window.opener.HideModalDiv();
            }
            window.returnValue = "false";
            window.close();
        }

        function doCancel() {
            OnClose();
        }

        function doAddServiceChannel(strType, strNewServChannel) {
            debugger;
            sClaimAspectID = <%=sClaimAspectID %>;
            //var oDDL = document.getElementById("ddlNewServiceChannel");
            //alert("ok: " + oDDL.selectedOptions[0].value );
            //alert("ok: " + oDDL.selectedOptions[0].text );
            //var strddlNewServiceChannel = $("select ddlNewServiceChannel:selected").text();

            Confirm("Confirm Add: Do you want to add new Service Channel to Vehicle <%= sVehicleNum %>?");

            /*
            if (strType == true) {
                //-- Add Service Channel
                alert("Validate");
                //confirm user action
                if (YesNoMessage("Confirm Add", "Do you want to add " + selClientServiceChannel.text + " Service Channel to Vehicle " + gsVehNum + "?.") != "Yes") {
                    cancelServiceChannel();
                    return;
                }
            } else {
                //-- Change/Delete
                alert("Validate1");
                if (strClientServiceChannel == -1) {
                    ClientWarning("Please select a service channel from the list.");
                    //selClientServiceChannel.setFocus();
                    return;
                }
            }
*/
        }

        function Confirm(strQuestion) {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm(strQuestion)) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</head>
<body>
    <form id="frmAddServiceChannel" runat="server">
        <!-- ==================== New Service Channel - Modal Dialog ============================== -->    
        <div id="diaNewServiceChannel" title="Add New Service Channel">
            <table class="topTable" style="border-spacing: 0px; width: 350px;" >
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">
                        <asp:DropDownList ID="ddlNewServiceChannel" runat="server" CssClass="inputField" Width="275px" ></asp:DropDownList>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td>
                        <asp:CheckBox ID="chkNewServiceChannelPrimary" runat="server" Text="Primary" />
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">
                        <%=ddlNewServiceChannel.SelectedItem.Text %>
                        <asp:Button runat="server" ID="btnNewServiceChannel_ADD" Text="Add" OnClientClick="javascript:doAddServiceChannel(true); return true;" />
                        <asp:Button runat="server" ID="btnNewServiceChannel_CANCEL" Text="Cancel" OnClientClick="javascript:doCancel(); return false;" />
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">
                        <asp:Label runat="server" ID="lblMessage" ForeColor="Red" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
