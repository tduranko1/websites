﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DocumentEstDetails.aspx.vb" Inherits="APDNet.DocumentEstDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Document Est Details</title>
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmDocumentEstDetails" runat="server">
    <div id="Main" style="position:absolute; z-index:1; top:5px; left:5px;">
        <!-- Main Bar -->
        <table border="0" cellpadding="0" cellspacing="0" width="900">
            <colgroup>
                <col width="100px"/>
                <col width="20px"/>
                <col width="300px"/>
                <col width="20px"/>
                <col width="*"/>
            </colgroup>
        <tr>
            <td>
                <!-- Document Title Bar -->
                <table cellpadding="0" cellspacing="0" width="250" border="0">
                    <tr>
                        <td align="left" valign="top" width="8" style="background-image:url(images/PanelLeft.png); background-repeat:no-repeat;">
                        </td>
                        <td align="left" class="Header"style="background-image:url(images/PanelMiddle.png); background-repeat:repeat;">
                            <a name="InvoiceInfo" style="color: White; font-weight: bold;" >
                                LynxID: <asp:Label ID="lblLynxID" runat="server"><%= sLynxID%></asp:Label>
                            </a>
                        </td>
                        <td align="left" valign="top" width="8" style="background-image:url(images/PanelRight.png); background-repeat:no-repeat;">
                        </td>
                    </tr>
                </table> 
                <table width='100%' border='0' cellspacing='0' cellpadding='2' style="table-layout:fixed">
                  <colgroup>
                    <col width="100px"/>
                    <col width="184px"/>
                    <col width="*"/>
                  </colgroup>
                <tr>
                    <td>Document Type:</td>
                    <td colspan="2">
                        <asp:DropDownList ID="ddlDocumentType" Width="145" CssClass="APDInput" runat="server" AutoPostBack="True"></asp:DropDownList>
                        <asp:DropDownList ID="ddlSupplementSeqNumber" CssClass="APDInput" runat="server" Visible="false">
                            <asp:ListItem Text="" Value="0" />
                            <asp:ListItem Text="1" Value="1" />
                            <asp:ListItem Text="2" Value="2" />
                            <asp:ListItem Text="3" Value="3" />
                            <asp:ListItem Text="4" Value="4" />
                            <asp:ListItem Text="5" Value="5" />
                            <asp:ListItem Text="6" Value="6" />
                            <asp:ListItem Text="7" Value="7" />
                            <asp:ListItem Text="8" Value="8" />
                            <asp:ListItem Text="9" Value="9" />
                            <asp:ListItem Text="10" Value="10" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="trDuplicateFlag" runat="server">
                    <td>Duplicate</td>
                    <td colspan="2">
                        <asp:DropDownList ID="ddlDuplicate" CssClass="APDInput" runat="server">
                            <asp:ListItem Text="No" Value="0" />
                            <asp:ListItem Text="Yes" Value="1" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="trEstimateTypeCD" runat="server">
                    <td>Original/Audited</td>
                    <td colspan="2">
                        <asp:DropDownList ID="ddlOriginalAudited" Width="70" CssClass="APDInput" runat="server"></asp:DropDownList>
                    </td>
                </tr>
                <tr id="trAgreedPriceMetCD" runat="server">
                    <td valign="top">Price Agreed</td>
                    <td>
                        <asp:DropDownList ID="ddlPriceAgreed" Width="60" CssClass="APDInput" runat="server"></asp:DropDownList>
                    </td>
                </tr>
                </table>

                <table id="trShopContacted" runat="server" width='100%' border='0' cellspacing='0' cellpadding='2' style="table-layout:fixed">
                  <colgroup>
                    <col width="100px"/>
                    <col width="184px"/>
                    <col width="*"/>
                  </colgroup>
                <tr>
                    <td valign="top">Shop Contacted</td>
                <td>
                    <asp:DropDownList ID="ddlShopContacted" Width="60" CssClass="APDInput" runat="server" Visible="true"></asp:DropDownList>
                </td>
                </tr>
                <tr>
                    <td valign="top">Shop Contact</td>
                    <td>
                        <asp:TextBox ID="txtShopContacted" Width="60" CssClass="APDInput" runat="server" Visible="true"></asp:TextBox>
                    </td>
                </tr>
                </table>

                <table id="trShopContactActive" runat="server" width='100%' border='0' cellspacing='0' cellpadding='2' style="table-layout:fixed;display:none">
                  <tr>
                    <td valign="top">
                        <br />
                        <b>Non-Agreed Reason Code:</b>
                        <table id="tbShopContactActive" runat="server" border="0" cellspacing="0" cellpadding="2" width="400" style="display:none;border:1px solid #00C0FF;">
                            <colgroup>
                                <col width="170px"/>
                                <col width="300px"/>
                                <col width="*"/>
                            </colgroup>
                        <tr>
                            <td colspan="2">(Select a maximum of two reason codes)</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkAltPartsAvailable" text="Alternate Parts Available" Width="170" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtAltPartsAvailableComments" CssClass="APDInput" Width="250" runat="server" Visible="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkRateDiff" text="Rate Differences" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtRateDiffComments" CssClass="APDInput" Width="250" runat="server" Visible="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkSystemDiff" text="System/Database Differences" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtSysDiffComments" CssClass="APDInput" Width="250" runat="server" Visible="true"></asp:TextBox>
                            </td>
                            </tr>
                            <tr>
                            <td>
                                <asp:CheckBox ID="chkPriceNotAgreedOther" text="Other" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtPriceNotAgreedOther" CssClass="APDInput" Width="250" runat="server" Visible="true"></asp:TextBox>
                            </td>
                        </tr>
                        </table>
                    </td>
                  </tr>
                </table>

                <table id="trNoShopContactActive" runat="server" width='100%' border='0' cellspacing='0' cellpadding='2' style="table-layout:fixed;display:none">
                  <tr>
                    <td valign="top">
                        <br />
                        <b>Non-Agreed Reason Code:</b>
                        <table id="tbNoShopContactActive" runat="server" border="0" cellspacing="0" cellpadding="2" width="400" style="display:none;border:1px solid #00C0FF;">
                            <colgroup>
                                <col width="170px"/>
                                <col width="300px"/>
                                <col width="*"/>
                            </colgroup>
                        <tr>
                            <td colspan="2">(Select only one reason code)</td>
                        </tr>
                        <tr>
                        <td>
                            <asp:CheckBox ID="chkThreeUnsuccessfulAttempts" text="3 Unsuccessful Contact Attempts" Width="170" runat="server" />
                        </td>
                        <td>
                             <asp:TextBox ID="txtThreeUnsuccessfulAttemptsComments" CssClass="APDInput" Width="250" runat="server" Visible="true"></asp:TextBox>
                        </td>
                      </tr>
                      <tr>
                        <td>
                            <asp:CheckBox ID="chkRepairComplete" text="Repair Complete Estimate" Width="170" runat="server" />
                        </td>
                        <td>
                             <asp:TextBox ID="txtRepairCompleteComments" CssClass="APDInput" Width="250" runat="server" Visible="true"></asp:TextBox>
                        </td>
                      </tr>
                      <tr>
                        <td>
                            <asp:CheckBox ID="chkAdverseSubro" text="Adverse Subro Estimate" Width="170" runat="server" />
                        </td>
                        <td>
                             <asp:TextBox ID="txtAdverseSubroComments" CssClass="APDInput" Width="250" runat="server" Visible="true"></asp:TextBox>
                        </td>
                      </tr>
                      <tr>
                        <td>
                            <asp:CheckBox ID="chkRecommendLowerOrgEst" text="Recommend Lower Original Estimate" Width="170" runat="server" />
                        </td>
                        <td>
                             <asp:TextBox ID="txtRecommendLowerOrgEstComments" CssClass="APDInput" Width="250" runat="server" Visible="true"></asp:TextBox>
                        </td>
                      </tr>
                      <tr>
                        <td>
                            <asp:CheckBox ID="chkProhibited" text="Prohibited Because Shop Not State Registered" Width="170" runat="server" />
                        </td>
                        <td>
                             <asp:TextBox ID="txtProhibitedComments" CssClass="APDInput" Width="250" runat="server" Visible="true"></asp:TextBox>
                        </td>
                      </tr>
                      <tr>
                        <td>
                            <asp:CheckBox ID="chkProbableTL" text="Probable Total Loss" Width="170" runat="server" />
                        </td>
                        <td>
                             <asp:TextBox ID="txtProbableTLComments" CssClass="APDInput" Width="250" runat="server" Visible="true"></asp:TextBox>
                        </td>
                      </tr>
                      <tr>
                        <td>
                            <asp:CheckBox ID="chkNoShopContactOther" text="Other" Width="170" runat="server" />
                        </td>
                        <td>
                             <asp:TextBox ID="txtNoShopContactOther" CssClass="APDInput" Width="250" runat="server" Visible="true"></asp:TextBox>
                        </td>
                      </tr>
                      </table>
                    </td>
                  </tr>
                </table>
        
                <table width='250' border='0' cellspacing='0' cellpadding='0' style="table-layout:fixed">
                  <tr>
                    <td colspan="2">
                        <hr />
                        <asp:CheckBox ID="chkInitialBillEstimate" runat="server" Text="Mark as Initial Bill Estimage" />
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                        <asp:CheckBox ID="chkAppliesToWarranty" runat="server" Text="Applies to Warranty" />
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                  </tr>
                </table>
               
                <!-- Document Type Footer Bar -->
                <table cellpadding="0" cellspacing="0" width="250" border="0">
                    <tr>
                        <td align="left" valign="top" width="8" style="background-image:url(images/PanelBotLeft.png); background-repeat:no-repeat;">
                        </td>
                        <td align="left" class="Header"style="background-image:url(images/PanelMiddle.png); background-repeat:repeat;">
                            &nbsp;
                        </td>
                        <td align="left" valign="top" width="8" style="background-image:url(images/PanelBotRight.png); background-repeat:no-repeat;">
                        </td>
                    </tr>
                </table> 
            </td>

            <td valign="top" id="tblTotals" runat="server">
                <!-- Repair Est Title Bar -->
                <table cellpadding="0" cellspacing="0" width="285" border="0">
                    <tr>
                        <td align="left" valign="top" width="8" style="background-image:url(images/PanelLeft.png); background-repeat:no-repeat;">
                        </td>
                        <td align="left" class="Header"style="background-image:url(images/PanelMiddle.png); background-repeat:repeat;">
                            <a name="InvoiceInfo" style="color: White; font-weight: bold;" >
                                Repair Estimate: <asp:Label ID="Label1" runat="server"></asp:Label>
                            </a>
                        </td>
                        <td align="left" valign="top" width="8" style="background-image:url(images/PanelRight.png); background-repeat:no-repeat;">
                        </td>
                    </tr>
                </table> 
                
                <table width='100%' border='0' cellspacing='0' cellpadding='2' style="table-layout:fixed">
                  <colgroup>
                    <col width="100px"/>
                    <col width="184px"/>
                    <col width="*"/>
                  </colgroup>
                <tr id="trDataRetrieve" runat="server">
                    <td>Retrieve Data:</td>
                    <td colspan="2">
                        <asp:DropDownList ID="ddlDataRetrieve" Width="75" CssClass="APDInput" runat="server">
                            <asp:ListItem Text="Mitchell" Value="Mitchell" />
                            <asp:ListItem Text="Pathways" Value="Pathways" />
                        </asp:DropDownList> 
                    </td>
                </tr>
                <tr>
                    <td>Repair Total:</td>
                    <td colspan="2">
                        <asp:TextBox ID="txtRepairTotal" Width="75" CssClass="APDInput" style="text-align:right" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td>Betterment:</td>
                    <td colspan="2">
                        <asp:TextBox ID="txtBetterment" Width="75" CssClass="APDInput" style="text-align:right" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td>Deductable:</td>
                    <td colspan="2">
                        <asp:TextBox ID="txtDeductable" Width="75" CssClass="APDInput" style="text-align:right" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td>Other Adjustments:</td>
                    <td colspan="2">
                        <asp:TextBox ID="txtOtherAdjustments" Width="75" CssClass="APDInput" style="text-align:right" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td>Tax Total:</td>
                    <td colspan="2">
                        <asp:TextBox ID="txtTaxTotal" Width="75" CssClass="APDInput" style="text-align:right" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td>Net Total:</td>
                    <td colspan="2">
                        <asp:TextBox ID="txtNetTotal" Width="75" CssClass="APDInput" Enabled="false" style="text-align:right" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td>Savings:</td>
                    <td colspan="2">
                        <asp:TextBox ID="txtSavings" Width="75" CssClass="APDInput" Enabled="false" style="text-align:right" runat="server"/>
                    </td>
                </tr>
                </table> 

                <!-- Repair Est Footer Bar -->
                <table cellpadding="0" cellspacing="0" width="285" border="0">
                    <tr>
                        <td align="left" valign="top" width="8" style="background-image:url(images/PanelBotLeft.png); background-repeat:no-repeat;">
                        </td>
                        <td align="left" class="Header"style="background-image:url(images/PanelMiddle.png); background-repeat:repeat;">
                            &nbsp;
                        </td>
                        <td align="left" valign="top" width="8" style="background-image:url(images/PanelBotRight.png); background-repeat:no-repeat;">
                        </td>
                    </tr>
                </table> 
            </td>

            <td>
                &nbsp;
            </td>

            <td valign="top">
                <!-- Shop Est Title Bar -->
                <table cellpadding="0" cellspacing="0" width="285" border="0">
                    <tr>
                        <td align="left" valign="top" width="8" style="background-image:url(images/PanelLeft.png); background-repeat:no-repeat;">
                        </td>
                        <td align="left" class="Header"style="background-image:url(images/PanelMiddle.png); background-repeat:repeat;">
                            <a name="InvoiceInfo" style="color: White; font-weight: bold;" >
                                Shop Estimate <asp:Label ID="Label2" runat="server"></asp:Label>
                            </a>
                        </td>
                        <td align="left" valign="top" width="8" style="background-image:url(images/PanelRight.png); background-repeat:no-repeat;">
                        </td>
                    </tr>
                </table> 

                <table width='100%' border='0' cellspacing='0' cellpadding='2' style="table-layout:fixed">
                    <tr>
                        <td>
                            <b>Shop Pricing as of Assignment</b>
                        </td>
                    </tr>
                </table> 

                <table width='100%' border='0' cellspacing='0' cellpadding='2' style="table-layout:fixed">
                  <colgroup>
                    <col width="100px"/>
                    <col width="184px"/>
                    <col width="*"/>
                  </colgroup>
                <tr>
                    <td>Sheet Metal:</td>
                    <td colspan="2">
                        <asp:TextBox ID="txtSheetMetal" Width="75" CssClass="APDInput" style="text-align:right" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td>Refinishing:</td>
                    <td colspan="2">
                        <asp:TextBox ID="txtRefinishing" Width="75" CssClass="APDInput" style="text-align:right" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td>Unibody/Frame:</td>
                    <td colspan="2">
                        <asp:TextBox ID="txtUnibodyFrame" Width="75" CssClass="APDInput" style="text-align:right" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td>Mechanical:</td>
                    <td colspan="2">
                        <asp:TextBox ID="txtMechanical" Width="75" CssClass="APDInput" style="text-align:right" runat="server"/>
                    </td>
                </tr>
                </table> 

                <!-- Shop Est Footer Bar -->
                <table cellpadding="0" cellspacing="0" width="285" border="0">
                    <tr>
                        <td align="left" valign="top" width="8" style="background-image:url(images/PanelBotLeft.png); background-repeat:no-repeat;">
                        </td>
                        <td align="left" class="Header"style="background-image:url(images/PanelMiddle.png); background-repeat:repeat;">
                            &nbsp;
                        </td>
                        <td align="left" valign="top" width="8" style="background-image:url(images/PanelBotRight.png); background-repeat:no-repeat;">
                        </td>
                    </tr>
                </table> 
            </td>
        </tr>
        </table> 

        <table width='100%' border='0' cellspacing='0' cellpadding='2' style="table-layout:fixed">
            <tr>
            <td colspan="2">
                <br />
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                <asp:Button ID="btnClose" Text="Close" CssClass="formbutton" runat="server" OnClientClick="return doCancel();" />  
                <asp:Button ID="btnSave" Text="Save" CssClass="formbutton" runat="server" UseSubmitBehavior="false" OnClientClick="return doSave();"/>  
                <asp:Button ID="btnReqReinspection" Text="Req. Reinspection" CssClass="formbutton" runat="server" UseSubmitBehavior="false" OnClientClick=""/>  
                <asp:Button ID="btnAuditEstimateValues" Text="Audit Estimate Values" CssClass="formbutton" runat="server" UseSubmitBehavior="false" OnClientClick=""/>  
            </td>
            </tr>
            <tr>
            <td colspan="2">
                <asp:Label ID="lblMsg" runat="server" Font-Bold="true" ForeColor="Red" Width="240" Height="50"></asp:Label>
            </td>
            </tr>
        </table>
        
        <asp:LinkButton ID="btnShowDoc" runat="server" Text="View" PostBackUrl="http://dapd.pgw.local/EstimateDocView.asp?docPath=\\pgw.local\dfs\intra\apddocuments\Dev\Storage\0\6\9\8\DU20140805080758LYNXID1600698VDoc0001.pdf" />
        <!-- iframe id="ifrmViewEstimate" name="ifrmViewEstimate" frameborder="0" style="width:892px; height:376px; border:0px"  src="<%= sDocViewURL %>" / -->

        <img alt="DotNet" src="Images/DotNet.png" />

    <br />
    <asp:Label ID="TODO001" runat="Server" ForeColor="Red" Text="**** TODO 001 **** Shop Estimage needs to be a repeater..."></asp:Label>
    <br />
    <asp:Label ID="Label3" runat="Server" ForeColor="Red" Text="**** TODO 002 **** Add code to hide things like the Non-Agreed Reason Code..."></asp:Label>
    <br />
    <asp:Label ID="Label4" runat="Server" ForeColor="Red" Text="**** TODO 003 **** Convert all the javascript..."></asp:Label>


    </div>
    </form>
</body>
</html>
