﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssignmentRemarks.aspx.vb" Inherits="APDNet.AssignmentRemarks" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <script type="text/javascript">
         function getUrlParameter(name) {
             name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
             var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
             var results = regex.exec(location.search);
             return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
         }


         //window.onbeforeunload = function (event) {
         function OnClose() {
             debugger;
             if (window.opener != null && !window.opener.closed) {
                 window.opener.HideModalDiv();

                 var objText, strparenttextID;
                 strparenttextID = getUrlParameter("RemarksTextID");

                 objText = window.opener.document.getElementById(strparenttextID);
                 objText.value = document.getElementById("txtAssignmentRemarksMore").value;
             }
             window.returnValue = "false";
             window.close();
        }

        function doCancel() {
            OnClose();
 
        }
    </script>
</head>
<body>
    <form id="frmAssignmentReview" runat="server">
    <div>
     <table>
            <tr>
                <td>
                    Template :
                </td>
                <td>
                    <asp:dropdownlist id="ddlselShopRemarksTemplate" runat="server" CssClass="inputField" Width="175px" AutoPostBack="true" OnSelectedIndexChanged="ddlselShopRemarksTemplate_SelectedIndexChanged"></asp:dropdownlist>
                </td>
            </tr>
            <tr>
                <td>
                    Rental Company :
                </td>
                <td>
                    <asp:dropdownlist id="ddlselRentalCompany" runat="server" CssClass="inputField" Width="175px" AutoPostBack="True">
                        <asp:listitem value="E">Enterprise</asp:listitem>
                        <asp:listitem value="H">Hertz</asp:listitem>
                        <asp:listitem value="H">Rental Handled by Client</asp:listitem>
                        <asp:listitem value="H">Rental Not Required</asp:listitem> 
                    </asp:dropdownlist>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="txtAssignmentRemarksMore" runat="server" CssClass="inputField" Width="262px" Height="72px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnClose" runat="server" Text="Close" OnClientClick="javascript:doCancel(); return false;" />
                </td>
            </tr>
        </table>
    </div>

    </form>
</body>
     <script type="text/javascript">
    $(document).ready(function () {
        if (window.opener != null && !window.opener.closed) {
            debugger;
            strparenttextID = getUrlParameter("RemarksTextID");

            objText = window.opener.document.getElementById(strparenttextID);
            document.getElementById("txtAssignmentRemarksMore").value = objText.value;
        }
    });
</script>

</html>
