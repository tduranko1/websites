﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DocumentUpload.aspx.vb" Inherits="APDNet.DocumentUpload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>APD Document Upload</title>
    <link rel="stylesheet" href="/css/apdMain.css" type="text/css"/>

    <style type="text/css">
        IE\:APDButton {behavior:url("/behaviors/APDButton.htc")}
        IE\:APDCheckBox {behavior:url("/behaviors/APDCheckBox.htc")}
    </style>

    <script language="JavaScript" src="/js/APDControls.js"></script>
    <script language="JavaScript" src="/js/formvalid.js"></script>

    <script language="JavaScript">
        var iDlgTop, iDlgLeft, iDlgWidth, iDlgHeight;
        var bDlgResized = false;

        iDlgTop = window.dialogTop;
        iDlgLeft = window.dialogLeft;
        iDlgWidth = window.dialogWidth;
        iDlgHeight = window.dialogHeight;

        function reloadDocDetailsIfrm() {
            var sIfrmURL = "DocumentEstDetails.asp?LynxId=<%= sLynxID %>&InsuranceCompanyID=<%= sInsuranceCompanyID %>&UserId=<%= sUserID %>&FileUploadFlag=1&DisplayPricingFlag=0";
            document.getElementById("ifrmDocDetails").src = sIfrmURL;
        }

        function unViewDoc() {
            document.getElementById("ifrmDocDetails").style.display = "block";
            document.getElementById("btnUnViewDoc").style.display = "none";
            document.getElementById("ifrmDocDisplay").style.display = "none";
        }

        function viewDoc() {
            var ifrmDoc = document.getElementById("ifrmDocUploadBrowse").contentWindow;
            var lsFiles = ifrmDoc.document.getElementById("filUpload").value;

            if (lsFiles != "") {
                var aFiles = lsFiles.split(";");
                var lsFile = aFiles[0];
                if (lsFile.indexOf(".") == -1) return;
                if (lsFile.substr(lsFile.length - 5, 4).toLowerCase() == ".pdf") {
                    var oWS = new ActiveXObject("WScript.Shell");
                    if (oWS) {
                        try {
                            oWS.Run(lsFile);
                            if (bDlgResized) {
                                window.dialogTop = iDlgTop;
                                window.dialogLeft = iDlgLeft;
                                window.dialogWidth = iDlgWidth;
                                window.dialogHeight = iDlgHeight;
                                document.getElementById("ifrmDocDisplay").style.display = "none";
                                document.getElementById("ifrmDocDisplay").src = "/blank.asp";
                            }
                        } catch (e) { }
                    }
                    else {
                        ClientWarning("Unable to create Windows Scripting Host. Please contact Help desk.");
                    }
                } else {
                    document.getElementById("ifrmDocDetails").style.display = "none";
                    document.getElementById("btnUnViewDoc").style.display = "block";

                    lsFile = lsFile.replace(/\\/g, "\\\\"); //escape the back-slash
                    lsFile = lsFile.replace(/\"/g, ""); // remove the double quote "

                    var sHREF = "/EstimateDocView.asp?docPath=" + lsFile;
                    var iTop = (window.screen.availHeight - 660) / 2;
                    var iLeft = (window.screen.availWidth - 800) / 2;
                    window.dialogTop = iTop + "px";
                    window.dialogLeft = iLeft + "px";
                    window.dialogWidth = "800px";
                    window.dialogHeight = "660px";
                    document.getElementById("ifrmDocDisplay").style.display = "inline";
                    document.getElementById("ifrmDocDisplay").src = sHREF;
                    bDlgResized = true;
                }
            }
            else {
                ClientWarning("Please specify a file first in the field above.");
                return false;
            }
        }
    </script>
</head>
<body class="bodyAPDSubSub" unselectable="on" style="padding:6px">
    <form id="frmDocumentUpload" runat="server">
    <div>
        <!-- Main Upload Selection -->
        <p align="center">
            <SPAN style="color: #000066; font-weight: bold; font-size: 12px;">
                Upload a New Document
            </SPAN>
        </p>
        <asp:Panel ID="pnlUpload" runat="server" GroupingText="Document Upload:" 
            Height="100%" Width="745px" CssClass="pnlUpload" ScrollBars="None">
            <div id="container" style="position:relative; height:580px;border:0px solid #000;">
                <div id="divUploadContent" style="border:0px solid #000; position:absolute; top:9%; left:5px; width:95%; height:40px; margin-top:-40px; text-align:left;">
                    <strong>
                        Specify the File(s) to Upload:
                    </strong>
                        (<asp:Label ForeColor="Red" runat="server"><strong>2 MB size limit for each file</strong></asp:Label>)
    
                    <iframe id="ifrmDocUploadBrowse" src="DocumentFileUploadBrowse.asp" frameborder="0" style="width:100%; height:30px; border:0px;" tabindex="-1"></iframe>
                    <button id="btnViewDoc" name="btnViewDoc" onClick="viewDoc()" value="View Document" CCTabindex="3" UseSubmitBehavior="False">View Document</button>
                    <asp:Image src="/images/spacer.gif" width="120" height="1" runat="server" />
                    <asp:CheckBox BorderStyle="Solid" BorderWidth="0px" id="cbDelFile" 
                        name="cbDelFile" value="0" caption="Delete original file after upload" 
                        title="Mark the checkbox to delete original file" CCTabindex="4" runat="server" 
                        Text="Delete original file after upload" />
                </div>

                <!-- Document Details Selection -->
                <div id="divDocumentDetails" style="border:0px solid #000; position:absolute; top:9%; left:5px; width:98%; height:240px; margin-top:40px; text-align:left;">
                    <asp:Panel ID="pnlDocumentDetails" runat="server" GroupingText="Document Details:" 
                        Height="100%" Width="728px" CssClass="pnlDocumentDetails">
                        <table width="100%">
                        <tr style="height:200px;" valign="top">
                            <td style="width:5px;"></td>
                            <td>
                                <button id="btnUnViewDoc" name="btnUnViewDoc" onClick="unViewDoc()" value="UnView Document" CCTabindex="3" UseSubmitBehavior="False" style="display:none">UnView Document</button>
                                <iframe id="ifrmDocDetails" src="DocumentEstDetails.asp?LynxId=<%= sLynxID %>&InsuranceCompanyID=<%= sInsuranceCompanyID %>&UserId=<%= sUserID %>&FileUploadFlag=1&DisplayPricingFlag=0" frameborder="0" style="width:100%; height:425px; border:0px" tabindex="5" scrolling="no"></iframe>
                                <iframe id="ifrmDocDisplay" src="blank.asp" frameborder="5" style="display:none; width:100%; height:320px; border:0px;" tabindex="-1"></iframe>
                            </td>
                        </tr>
                        </table>
                    </asp:Panel>
                </div>
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
