﻿//------------------------------------//
//-- Show Error/Info/Warning Messages
//------------------------------------//
function ClientError(sDescription) { ClientEvent(0, sDescription); }
function ClientWarning(sDescription) { ClientEvent(1, sDescription); }
function ClientInfo(sDescription) { ClientEvent(2, sDescription); }

function ClientEvent(nNumber, sDescription, sSource) {
    if ((String(sSource) == "undefined") || (sSource == null))
        sSource = window.location

    var sDimensions = "dialogHeight:200px; dialogWidth:424px; "
    var sSettings = "resizable:yes; status:no; help:no; center:yes;"
    var sQuery = "../EventPopup.asp?Number=" + nNumber + "&Description=" + escape(sDescription) + "&Source=" + escape(sSource);

    top.showModalDialog(sQuery, top, sDimensions + sSettings);
    //window.open(sQuery, "", "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=1,width=424px,height=200px,left=490,top=300,center=yes;");
}

//------------------------------//
//-- Show ERROR events
//------------------------------//
function ServerEvent() {
    var sQuery = "EventPopup.asp";
    window.open(sQuery, "", "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=1,width=424px,height=200px,left=490,top=300,center=yes;");
}

function ShowEstimate(strWindowID, strLynxID, docId, showTotals) {
    var sURL = "Estimates.asp?WindowID=" + strWindowID + "&DocumentID=" + docId + "&showTotals=" + showTotals;
    var winName = "EstimateFull_" + strLynxID;
    var sWinEstimates = window.open(sURL, winName, "Height=523px, Width=728px, Top=90px, Left=40px, resizable=Yes, status=No, menubar=No, titlebar=No");
    parent.gAryOpenedWindows.push(sWinEstimates);
    sWinEstimates.focus();
}

function emailCarrier() {
    var strCarrierRepActive = $("#hidRepActiveFlag").val();
    var strTo = $("#txtCarrierEmailAddress").val();
    var strClaimNumber = $("#txtClientClaimNumber").val();
    var strInsured = $("#txtNameFirst").val() + " " + $("#txtNameLast").val();
    var strLossDate = $("#txtLossDate").val();
    var strLynxID = $("#hidLynxID").val();
    //var strVehNum = $("#hidVehNum").val();

    if (strCarrierRepActive == "0") {
        ClientWarning("Carrier Adjuster is inactive. Cannot send an email.");
        return;
    }

    //var strSubject = 'Claim #: ' + strClaimNumber + ' / Insured: ' + strInsured + ' / LossDate: ' + strLossDate + ' / LynxID: ' + strLynxID + '-' + strVehNum;
    var strSubject = 'Claim #: ' + strClaimNumber + ' / Insured: ' + strInsured + ' / LossDate: ' + strLossDate + ' / LynxID: ' + strLynxID;
    window.location = 'mailto:?subject=' + escape(strSubject) + '&To=' + strTo;
}

function checkLimit() {
    if (typeof (parent.displayCoverageLimitWarining) == "function") {
        if ((parseFloat(CollisionLimitAmt.value) > 0 && parseFloat(CollisionLimitAmt.value) <= 10000) ||
            (parseFloat(ComprehensiveLimitAmt.value) > 0 && parseFloat(ComprehensiveLimitAmt.value) <= 10000) ||
            (parseFloat(LiabilityLimitAmt.value) > 0 && parseFloat(LiabilityLimitAmt.value) <= 10000) ||
            (parseFloat(UnderInsuredLimitAmt.value) > 0 && parseFloat(UnderInsuredLimitAmt.value) <= 10000) ||
            (parseFloat(UnInsuredLimitAmt.value) > 0 && parseFloat(UnInsuredLimitAmt.value) <= 10000))
            parent.displayCoverageLimitWarining(true);
        else
            parent.displayCoverageLimitWarining(false);
    }
}

//------------------------//
//-- Check if date
//------------------------//
function isDate(dtDate) {
    var dateWrapper = new Date(dtDate);
    return !isNaN(dateWrapper.getDate());
}