﻿'--------------------------------------------------------------
' Program: APD Document Est Details
' Author:  Thomas Duranko
' Date:    Aug 05, 2014
' Version: 1.0
'
' Description:  This site provides the document Est Details for 
'               APD using Dot Net
'--------------------------------------------------------------
Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Public Class DocumentEstDetails
    Inherits System.Web.UI.Page

    Dim APDFoundation As New PGWAPDFoundation.APDService
    Dim sAPDNetDebugging As String = ""
    Dim sEstimateUpd As String = ""
    Dim sDocumentID As String = ""
    Dim sClaimAspectID As String = ""
    Public sLynxID As String = ""
    Dim sUserID As String = ""
    Dim sClaimAspectServiceChannelID As String = ""
    Dim sDisDocTypeSelect As String = ""
    Dim sSupplementSeqNumber As String = ""
    Dim bCCDisabled As Boolean = False
    Dim sFullSummaryExistsFlag As String = ""
    Dim sDuplicateFlag As String = ""
    Dim sEstimateTypeCD As String = ""
    Dim sAgreedPriceMetCD As String = ""
    Dim sApprovedFlag As String = ""
    Dim sServiceChannelCD As String = ""
    Dim sWarrantyExistsFlag As String = ""
    Dim sFileUploadFlag As String = ""

    Dim sRepairTotalAmt As String = ""
    Dim sBettermentAmt As String = ""
    Dim sDeductibleAmt As String = ""
    Dim sOtherAdjustmentAmt As String = ""
    Dim sTaxTotalAmt As String = ""
    Dim sNetTotalAmt As String = ""
    Dim sNetTotalAmtCalc As String = ""

    Dim sDocLocation As String = ""
    Public sDocViewURL As String = ""

    '--------------------------------------------------------------
    ' Page Load
    '--------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '----------------------------------------
        ' Check to see if we need to debug by
        ' looking in the web.config
        '----------------------------------------
        If UCase(AppSettings("Debug")) = "TRUE" Then
            sAPDNetDebugging = "TRUE"
        End If

        '----------------------------------------
        ' Debugging
        '----------------------------------------
        If UCase(sAPDNetDebugging) = "TRUE" Then
            APDFoundation.LogEvent("APDNet", "DEBUGGING", "DocumentEstDetails.aspx - Start: " & Date.Now, "", "")
        End If

        '----------------------------------------
        ' This makes sure the postback comes
        ' back to the modal dialog
        '----------------------------------------
        Dim htmlbase As String = "<base target='_self'>"
        Response.Write(htmlbase)

        '----------------------------------------
        ' Passed in Vars
        '----------------------------------------
        sEstimateUpd = Request.QueryString("EstimateUpd")
        sDocumentID = Request.QueryString("DocumentID")
        sClaimAspectID = Request.QueryString("ClaimAspectID")
        sLynxID = Request.QueryString("LynxID")
        sClaimAspectServiceChannelID = Request.QueryString("ClaimAspectServiceChannelID")
        sUserID = Request.QueryString("UserID")
        sDisDocTypeSelect = Request.QueryString("DisDocTypeSelect")
        sDocLocation = Request.QueryString("ImageLocation")
        sFileUploadFlag = Request.QueryString("FileUploadFlag")

        '----------------------------------------
        ' Test Case/Debugging
        '----------------------------------------
        ' Test Case
        Dim bTestCaseEnabled As Boolean = True
        sEstimateUpd = "1"
        sDisDocTypeSelect = "1"
        sDocumentID = "4955582"
        sClaimAspectID = "640799"
        sLynxID = "1600698"
        sClaimAspectServiceChannelID = "212325"
        sUserID = "5840"
        sDocLocation = "\\pgw.local\dfs\intra\apddocuments\Dev\Storage\0\6\9\8\DU20140805080758LYNXID1600698VDoc0001.pdf"
        sFileUploadFlag = 0

        '-------------------------------
        ' Page Initialization
        '-------------------------------
        If bTestCaseEnabled Then
            sDocViewURL = "http://dapd.pgw.local/EstimateDocView.asp?docPath=" & sDocLocation
        Else
            sDocViewURL = "EstimateDocView.asp?docPath=" & sDocLocation
        End If

        'if (gIsSaved == "Saved") {  ?????

        If sEstimateUpd = "1" Then
            'updDocumentType();
            ddlDocumentType.Focus()
            'window.setTimeout("ifrmViewEstimate.frameElement.src = strDocViewURL", 150); ????                              
            'getEstimates();
            'tdDataRetrieve.style.visibility = "hidden";
            'tdDataRetrieveSel.style.visibility = "hidden";
            'chkApproveDocument.CCDisabled = (strAgreedPriceMetCD != "Y")//false;
        End If


        If Not IsPostBack Then
            '----------------------------------------
            ' Get Document Estimate Details from the DB
            '----------------------------------------
            Call GetDocumentEstDetails(sDocumentID, sClaimAspectID, sLynxID, sClaimAspectServiceChannelID, sUserID)
        End If




        ''-------------------------------
        '' Globals
        ''-------------------------------
        'Dim oSession As Object
        'Dim sAPDNetDebugging As String = ""
        'Dim dtStart As Date
        'Dim APDDBUtils As APDDBUtilities = Nothing
        'Dim sInsuranceCompanyID As String = ""
        'Dim sXslHash As New Hashtable
        'Dim sClaimAspectID As String = ""
        'Dim sParams As String = ""
        'Dim uspXMLRaw As XmlElement
        'Dim iRC As Integer = 0
        'Dim uspXMLXDoc As New XmlDocument
        'Dim sHTML As String = ""

        ''-------------------------------
        '' Session/Local Variables
        ''-------------------------------
        'Dim sStoredProcedure As String = "uspEstimateQuickGetDetailWSXML"
        'Dim sXSLProcedure As String = "DocumentEstDetails.xsl"

        ''-------------------------------
        '' Get Session Controls from
        '' Classic ASP page
        ''-------------------------------
        'Dim sLynxID As String = Request.QueryString("LynxID")
        'Dim sUserID As String = Request.QueryString("UserID")
        'Dim sSessionKey = Request.QueryString("SessionKey")
        'Dim sWindowID = Request.QueryString("WindowID")
        'Dim sVehClaimAspectID = Request.QueryString("ClaimAspectID")
        'Dim sASPParams = Request.QueryString("Params")

        'Try
        '    If sSessionKey = "" Then
        '        Throw New SystemException("SessionValidationError: No session information received from classic ASP page.")
        '    Else
        '        '---------------------------------
        '        ' New Session Data
        '        '---------------------------------
        '        oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "LynxID-" & sWindowID)
        '        sLynxID = oSession(0).SessionValue

        '        oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "UserID-" & sWindowID)
        '        sUserID = oSession(0).SessionValue

        '        oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "APDNetDebugging-" & sWindowID)
        '        sAPDNetDebugging = ""
        '        If UCase(oSession(0).SessionValue) = "TRUE" Or UCase(AppSettings("Debug")) = "TRUE" Then
        '            sAPDNetDebugging = "TRUE"
        '        End If

        '        APDDBUtils = New APDDBUtilities()
        '        dtStart = Date.Now

        '        If UCase(sAPDNetDebugging) = "TRUE" Then
        '            wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls.", " LynxID: " & sLynxID & "UserID: " & sUserID & "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey & " VehClaimAspectID: " & sVehClaimAspectID, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
        '        End If

        '        'oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "InsuranceCompanyID-" & sWindowID)
        '        'sInsuranceCompanyID = oSession(0).SessionValue

        '        'oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "VehClaimAspectID-" & sWindowID)
        '        'sVehClaimAspectID = oSession(0).SessionValue

        '        '---------------------------------
        '        ' Get the permission data
        '        '---------------------------------
        '        'XMLCRUD = wsAPDFoundation.ExecuteSpAsXML("uspCRUDGetDetailWSXML", "'Client Billing', " & sUserID)
        '        'sInfoCRUD = APDDBUtils.GetCrud(XMLCRUD)

        '        '------------------------------------
        '        ' If ASPParms, parse and add to hash
        '        '------------------------------------
        '        If sASPParams <> "" Then
        '            Dim aParams As Array
        '            aParams = Split(sASPParams, "|")
        '            For Each sASPParam In aParams
        '                Dim aValues As Array
        '                aValues = Split(sASPParam, ":")
        '                sXslHash.Add(aValues(0), aValues(1))
        '                'Response.Write(aValues(0) & " - " & aValues(1) & "<br/>")
        '            Next
        '        End If

        '        '-------------------------------------
        '        ' Exec the SP to update the status
        '        '-------------------------------------
        '        'sClaimAspectID = sXslHash("ClaimAspectID")

        '        '---------------------------------
        '        ' Debug Data
        '        '---------------------------------
        '        If UCase(sAPDNetDebugging) = "TRUE" Then
        '            wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls/Parameters." _
        '                                     , "Session controls for Window: " & sWindowID _
        '                                     & " and SessionKey: " & sSessionKey _
        '                                     , "Vars: StoredProc = " & sStoredProcedure _
        '                                     & ", XSLPage = " & sXSLProcedure _
        '                                     & ", UserID(Session) = " & sUserID _
        '                                     & ", LynxID(Session) = " & sLynxID _
        '                                     & ", VehClaimAspectID = " & sVehClaimAspectID _
        '                                     & ", sASPParams = " & sASPParams _
        '                                     )
        '        End If

        '        '---------------------------------
        '        ' XSL Transformation
        '        '---------------------------------
        '        sParams = sXslHash("DocumentID")
        '        sParams += "," & sXslHash("ClaimAspectID")
        '        sParams += "," & sXslHash("LynxID")
        '        sParams += "," & IIf(sXslHash("ClaimAspectServiceChannelID") = Nothing, "NULL", sXslHash("ClaimAspectServiceChannelID"))
        '        sParams += "," & sXslHash("UserID")

        '        '-------------------------------------
        '        ' Debugging
        '        '-------------------------------------
        '        'Response.Write("sSessionKey" & " - " & sSessionKey & "<br/>")
        '        'Response.Write("sWindowID" & " - " & sWindowID & "<br/>")
        '        'Response.Write("sUserID" & " - " & sUserID & "<br/>")
        '        'Response.Write("sLynxID" & " - " & sLynxID & "<br/>")
        '        'Response.Write("sASPParams" & " - " & sASPParams & "<br/>")
        '        'Response.Write("sParams" & " - " & sParams & "<br/>")
        '        'Response.End()

        '        '---------------------------------
        '        ' StoredProc XML Data
        '        '---------------------------------
        '        uspXMLRaw = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

        '        If uspXMLRaw.OuterXml = Nothing Then
        '            ' Throw exception
        '        Else
        '            uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)

        '            If UCase(sAPDNetDebugging) = "TRUE" Then
        '                sXslHash.Add("RunTime", Now.Subtract(dtStart).Milliseconds & " msec")
        '            Else
        '                sXslHash.Add("RunTime", Chr(149))
        '            End If

        '            '---------------------------------
        '            ' Update Session
        '            '---------------------------------
        '            Dim XMLNodeValidate As XmlNode = uspXMLXDoc.SelectSingleNode("Root/@Context")
        '            If XMLNodeValidate Is Nothing Then
        '            Else
        '                iRC = wsAPDFoundation.UpdateSessionVar(sSessionKey, "Context-" & sWindowID, XMLNodeValidate.InnerText)
        '            End If

        '            sHTML = APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sParams, sXslHash)

        '            Response.Write(sHTML)
        '        End If

        '        'uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)
        '    End If

        '    If UCase(sAPDNetDebugging) = "TRUE" Then
        '        wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "APDInvoice.aspx - Finished: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
        '    End If
        'Catch oExcept As Exception
        '    '---------------------------------
        '    ' Error handler and notifications
        '    '---------------------------------
        '    Dim sError As String = ""
        '    Dim sBody As String = ""
        '    Dim FunctionName As New System.Diagnostics.StackFrame

        '    sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

        '    wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls/parameter not found or page pre-transformation failed.", "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID)

        '    'Response.Write(sError)
        '    'Response.End()
        '    '---------------------------------
        '    ' Email notifications
        '    '---------------------------------
        '    'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        'End Try
    End Sub

    Protected Sub GetDocumentEstDetails(ByVal sDocumentID As String, ByVal sClaimAspectID As String, ByVal sLynxID As String, ByVal sClaimAspectServiceChannelID As String, ByVal sUserID As String)
        'Dim xmlCRUDPermission As XmlElement = Nothing
        'Dim sCRUD As String = ""
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLReferencesNodeList As XmlNodeList = Nothing
        'Dim oXMLReferencesNode As XmlNode = Nothing
        'Dim iRequired As Integer = 0
        'Dim sReasonPrefix As String = ""
        'Dim sReason As String = ""
        'Dim iNotesNewLen As Integer = 0
        'Dim sErrorDescription As String = ""
        Dim sDocumentTypeID As String = ""
        Dim sDocumentVANSourceFlag As String = ""
        Dim sReferenceID As String = ""

        '-------------------------------
        ' Database Calls
        '-------------------------------  
        Dim sStoredProcedure As String = "uspEstimateQuickGetDetailWSXML"
        Dim sParams As String = sDocumentID & "," & sClaimAspectID & "," & sClaimAspectServiceChannelID & "," & sLynxID & "," & sUserID

        Try
            '----------------------------------------
            ' Debugging
            '----------------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("APDNet", "DEBUGGING", "Calling uspDiaryGetDetailWSXML to get the detailed XML", "Params passed to SP: ", sParams)
            End If

            '----------------------------------
            ' Check CRUD - Permissions
            '----------------------------------
            '---------------------------------
            ' Get the permission data
            '---------------------------------
            'xmlCRUDPermission = APDFoundation.ExecuteSpAsXML("uspCRUDGetDetailWSXML", "'Task', " & iUserID)
            'sCRUD = APDUtilites.GetCrud(xmlCRUDPermission)

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = APDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '----------------------------------------
            ' Debugging
            '----------------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("APDNet", "DEBUGGING", "XML Returned from " & sStoredProcedure & " to get the detailed XML", "XML in EventXML: ", oReturnXML.InnerXml)
            End If

            If oReturnXML.InnerXml <> "" Then
                If sEstimateUpd = "1" Then
                    sDocumentTypeID = oReturnXML.SelectNodes("/Estimate/@DocumentTypeID").Item(0).Value
                End If

                '-------------------------------
                ' Single Nodes
                '-------------------------------
                sDocumentVANSourceFlag = oReturnXML.SelectNodes("/Estimate/@DocumentVANSourceFlag").Item(0).Value
                sSupplementSeqNumber = oReturnXML.SelectNodes("/Estimate/@SupplementSeqNumber").Item(0).Value
                sFullSummaryExistsFlag = oReturnXML.SelectNodes("/Estimate/@FullSummaryExistsFlag").Item(0).Value
                sDuplicateFlag = oReturnXML.SelectNodes("/Estimate/@DuplicateFlag").Item(0).Value
                sEstimateTypeCD = oReturnXML.SelectNodes("/Estimate/@EstimateTypeCD").Item(0).Value
                sAgreedPriceMetCD = oReturnXML.SelectNodes("/Estimate/@AgreedPriceMetCD").Item(0).Value
                sApprovedFlag = oReturnXML.SelectNodes("/Estimate/@ApprovedFlag").Item(0).Value
                sServiceChannelCD = oReturnXML.SelectNodes("/@ServiceChannelCD").Item(0).Value
                sWarrantyExistsFlag = oReturnXML.SelectNodes("/@WarrantyExistsFlag").Item(0).Value

                '-------------------------------
                ' Single Nodes - Repair Est
                '-------------------------------
                sRepairTotalAmt = oReturnXML.SelectNodes("/Estimate/@RepairTotalAmt").Item(0).Value
                sBettermentAmt = oReturnXML.SelectNodes("/Estimate/@BettermentAmt").Item(0).Value
                sDeductibleAmt = oReturnXML.SelectNodes("/Estimate/@DeductibleAmt").Item(0).Value
                sOtherAdjustmentAmt = oReturnXML.SelectNodes("/Estimate/@OtherAdjustmentAmt").Item(0).Value
                sTaxTotalAmt = oReturnXML.SelectNodes("/Estimate/@TaxTotalAmt").Item(0).Value
                sNetTotalAmt = oReturnXML.SelectNodes("/Estimate/@NetTotalAmt").Item(0).Value

                Try
                    sNetTotalAmtCalc = oReturnXML.SelectNodes("/Reference[@List='PertainsTo' and @ClaimAspectID='" & CInt(sClaimAspectID) & "']/@DeductibleAmt").Item(0).Value
                Catch ex As Exception
                    ' PertainsTo Node doesn't exist, so just ignore this
                End Try

                If sDocumentVANSourceFlag = "1" Or sDisDocTypeSelect = "1" Then
                    bCCDisabled = True
                End If

                '-------------------------------
                ' Document Type populate
                '-------------------------------
                oXMLReferencesNodeList = oReturnXML.SelectNodes("/Reference[@List='DocumentType']")

                Dim iCnt As Integer = 0
                For Each oXMLReferencesNode In oXMLReferencesNodeList
                    sReferenceID = oXMLReferencesNode.Attributes.ItemOf("ReferenceID").value

                    '-------------------------------
                    ' Document Types to populate in
                    ' the dropdowns
                    '-------------------------------
                    Select Case sReferenceID
                        Case "3", "10"
                            If sEstimateUpd = "1" Then
                                ddlDocumentType.Items.Add(oXMLReferencesNode.Attributes.ItemOf("Name").value)
                                ddlDocumentType.Items(iCnt).Value = sReferenceID
                                iCnt += 1
                            End If
                        Case "4", "15"
                        Case Else
                            If sEstimateUpd = "0" Then
                                ddlDocumentType.Items.Add(oXMLReferencesNode.Attributes.ItemOf("Name").value)
                                ddlDocumentType.Items(iCnt).Value = sReferenceID
                                iCnt += 1
                            End If
                    End Select
                Next

                '-------------------------------
                ' Original/Audited populate 
                '-------------------------------
                oXMLReferencesNodeList = oReturnXML.SelectNodes("/Reference[@List='EstimateTypeCD']")

                iCnt = 0
                For Each oXMLReferencesNode In oXMLReferencesNodeList
                    sReferenceID = oXMLReferencesNode.Attributes.ItemOf("ReferenceID").value

                    '-------------------------------
                    ' Document Types to populate in
                    ' the dropdowns
                    '-------------------------------
                    ddlOriginalAudited.Items.Add(oXMLReferencesNode.Attributes.ItemOf("Name").value)
                    ddlOriginalAudited.Items(iCnt).Value = sReferenceID
                    iCnt += 1
                Next

                '-------------------------------
                ' Price Agreed populate 
                '-------------------------------
                oXMLReferencesNodeList = oReturnXML.SelectNodes("/Reference[@List='AgreedPriceMetCD']")

                iCnt = 0
                For Each oXMLReferencesNode In oXMLReferencesNodeList
                    sReferenceID = oXMLReferencesNode.Attributes.ItemOf("ReferenceID").value

                    '-------------------------------
                    ' Document Types to populate in
                    ' the dropdowns
                    '-------------------------------
                    ddlPriceAgreed.Items.Add(oXMLReferencesNode.Attributes.ItemOf("Name").value)
                    ddlPriceAgreed.Items(iCnt).Value = sReferenceID
                    iCnt += 1
                Next

                '----------------------------------
                ' Initialize Form
                '----------------------------------
                If sEstimateUpd = "1" Then
                    ddlSupplementSeqNumber.SelectedValue = sSupplementSeqNumber
                    ddlDuplicate.SelectedValue = sDuplicateFlag
                    ddlOriginalAudited.SelectedValue = sEstimateTypeCD
                    ddlPriceAgreed.SelectedValue = sAgreedPriceMetCD
                    txtRepairTotal.Text = String.Format("${0:c}", sRepairTotalAmt)
                    txtTaxTotal.Text = String.Format("${0:c}", sTaxTotalAmt)
                    txtNetTotal.Text = String.Format("${0:c}", sNetTotalAmt)
                Else
                    txtNetTotal.Text = String.Format("${0:c}", sNetTotalAmt)
                End If

                If sDocumentVANSourceFlag = "1" And sFullSummaryExistsFlag = "1" Then
                    ddlSupplementSeqNumber.Enabled = False
                    txtRepairTotal.Enabled = False
                    txtBetterment.Enabled = False
                    txtDeductable.Text = String.Format("${0:c}", sDeductibleAmt)
                    txtDeductable.Enabled = False
                    txtOtherAdjustments.Enabled = False
                End If

                If ddlDocumentType.SelectedValue = 10 Then
                    ddlSupplementSeqNumber.Visible = True
                Else
                    ddlSupplementSeqNumber.Visible = False
                End If

                If sServiceChannelCD = "RRP" Then
                    chkInitialBillEstimate.Text = "Mark as Approved"
                Else
                    chkInitialBillEstimate.Text = "Mark as Initial Bill Estimate"
                End If
                chkInitialBillEstimate.Checked = sApprovedFlag

                If sWarrantyExistsFlag = "1" Then
                    chkAppliesToWarranty.Checked = sWarrantyExistsFlag
                End If

                If sBettermentAmt = "" And sDocumentVANSourceFlag <> 1 Then
                    txtBetterment.Text = String.Format("${0:c}", 0)
                Else
                    txtBetterment.Text = String.Format("${0:c}", sBettermentAmt)
                End If

                If sOtherAdjustmentAmt = "" And sDocumentVANSourceFlag <> 1 Then
                    txtOtherAdjustments.Text = String.Format("${0:c}", 0)
                Else
                    txtOtherAdjustments.Text = String.Format("${0:c}", sOtherAdjustmentAmt)
                End If

                If sFullSummaryExistsFlag = "0" Then
                    If sDeductibleAmt <> "" Then
                        txtDeductable.Text = String.Format("${0:c}", sDeductibleAmt)
                    Else
                        txtDeductable.Text = String.Format("${0:c}", sNetTotalAmtCalc)
                    End If
                End If

                '-------------------------------
                ' Enable/Disable Objects
                '-------------------------------
                chkInitialBillEstimate.Enabled = bCCDisabled
                chkAppliesToWarranty.Enabled = bCCDisabled

                '-------------------------------
                ' Enable/Disable - Non-Agreed
                '-------------------------------
                If ddlShopContacted.Visible = True And UCase(ddlShopContacted.SelectedValue) = "YES" Then
                    trShopContactActive.Visible = False
                End If

                Call UpdateDocumentType()

                '-------------------------------
                ' Get MaxLength/Required
                '-------------------------------
                'iMaxLength = oReturnXML.SelectNodes("/Metadata[@Entity='DiaryTask']/Column[@Name='Note']/@MaxLength").Item(0).Value
                'iRequired = oReturnXML.SelectNodes("/DiaryTask/@CommentCompleteRequiredFlag").Item(0).Value
                'sCommentNARequired = oReturnXML.SelectNodes("/DiaryTask/@CommentNARequiredFlag").Item(0).Value
                'sTaskSysLastUpdated = oReturnXML.SelectNodes("/DiaryTask/@SysLastUpdatedDate").Item(0).Value

                'sReasonPrefix = "I marked the task '" & sTaskName & "' as 'Complete' because"

                '    '----------------------------------------
                '    ' Debugging
                '    '----------------------------------------
                '    If UCase(sAPDNetDebugging) = "TRUE" Then
                '        APDFoundation.LogEvent("APDNet", "DEBUGGING", "Parsed vars from XML", "Vars: in EventXML", "iMaxLength: " & CStr(iMaxLength) & ", iRequired: " & CStr(iRequired) & ", sCommentNARequired: " & sCommentNARequired & ", sTaskSysLastUpdated: " & sTaskSysLastUpdated & ", sReasonPrefix: " & sReasonPrefix)
                '    End If

                '    If rbMarkTaskAs_NotApplicable.Checked Then
                '        sReason = "I marked the task '" & sTaskName & "' as 'Not Applicable' because"
                '        sReasonPrefix = sReason

                '        iNotesNewLen = iMaxLength - sReasonPrefix.Length - 1
                '        txtNote.MaxLength = iNotesNewLen
                '        txtNote.Text = ""
                '        txtReasonPrefix.Visible = True
                '    End If

                '    If rbMarkTaskAs_Complete.Checked Then
                '        sReason = ""
                '        sReasonPrefix = sReason
                '        iNotesNewLen = iMaxLength - sReasonPrefix.Length - 1
                '        txtNote.MaxLength = iNotesNewLen
                '        sReason = "'" & sTaskName & "' task completed."
                '        txtNote.Text = sReason
                '        txtReasonPrefix.Visible = False
                '    End If

                '    txtNote.MaxLength = iNotesNewLen

                '    '----------------------------------------
                '    ' Debugging
                '    '----------------------------------------
                '    If UCase(sAPDNetDebugging) = "TRUE" Then
                '        APDFoundation.LogEvent("APDNet", "DEBUGGING", "Final business logic results", "Vars: in EventXML", "sReason: " & sReason & ", iNotesNewLen: " & CStr(iNotesNewLen) & ",  txtReasonPrefix.Visible: " & txtReasonPrefix.Visible)
                '    End If

                'Else
                '    sErrorDescription = "WSXMLError(5001): No XML data returned from the database or parameter errors.  XML Returned: "
                '    sErrorDescription += oReturnXML.InnerXml

                '    lblMsg.Text = "Missing WS-XML data from database.  Please contact support..."
                '    btnSave.Enabled = False
                '    Throw New SystemException(sErrorDescription)
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            APDFoundation.LogEvent("APDNet(0001", "ERROR", "Complete Task item not found or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
        Finally
            oReturnXML = Nothing
        End Try
    End Sub

    Protected Sub ddlDocumentType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlDocumentType.SelectedIndexChanged
        If ddlDocumentType.SelectedValue = 10 Then
            ddlSupplementSeqNumber.Visible = True
        Else
            ddlSupplementSeqNumber.Visible = False
        End If
    End Sub

    Protected Sub UpdateDocumentType()
        Select Case ddlDocumentType.SelectedValue
            Case "3", "10"
                trDuplicateFlag.Visible = True
                trEstimateTypeCD.Visible = True
                tblTotals.Visible = True

                If sServiceChannelCD <> "DA" Then
                    trAgreedPriceMetCD.Visible = True
                End If

                If sFileUploadFlag = "1" Then
                    trDataRetrieve.Visible = True
                End If

                If sEstimateUpd = "1" Then
                    UpdateDuplicateFlag()
                End If

            Case Else
                trDuplicateFlag.Visible = False
                trEstimateTypeCD.Visible = False
                trAgreedPriceMetCD.Visible = False
                tblTotals.Visible = False

                If UCase(sServiceChannelCD) <> "DESK AUDIT" Then
                    trShopContactActive.Visible = False
                    trNoShopContactActive.Visible = False
                    tbShopContactActive.Visible = False
                    tbNoShopContactActive.Visible = False
                End If

                Try
                    trDataRetrieve.Visible = True
                    'document.getElementById("trDataRetrieve").cols[0].style.visible = "hidden"; ????
                    'document.getElementById("trDataRetrieve").cols[1].style.visible = "hidden"; ????
                    'document.getElementById("trDataRetrieve").cols[2].style.visible = "visible"; ????
                Catch ex As Exception
                End Try

                ' *** Doesn't appear to be used ***
                'If (document.getElementById("divLockEstimate")) Then
                '    document.getElementById("divLockEstimate").style.display = "none";
                '}

                'ddlSupplementSeqNumber.Visible = False
                'ddlSupplementSeqNumber.SelectedValue = 0
        End Select
    End Sub

    Protected Sub UpdateDuplicateFlag()
        If UCase(sEstimateTypeCD) = "AUDITED" And UCase(sServiceChannelCD) = "DA" Then
            trShopContacted.visible = True
            trAgreedPriceMetCD.Visible = True
        Else
            trShopContacted.visible = False
            trAgreedPriceMetCD.Visible = False
        End If

        If UCase(sServiceChannelCD) <> "DA" Then
            trAgreedPriceMetCD.Visible = True
        End If

        Select Case UCase(sEstimateTypeCD)
            Case "O"
                trShopContactActive.Visible = False
                trNoShopContactActive.Visible = False
                tbShopContactActive.Visible = False
                tbNoShopContactActive.Visible = False
            Case "A"
                'showOriginalData(); ????
                'trSavings.style.visibility = "visible"; ????
            Case Else
                'trSavings.style.visibility = "hidden";  ????
        End Select

        'colOrigData.style.display = (txtEstimateTypeCD.value == "A" ? "inline" : "none");  ??????

        If sEstimateUpd = "!" Then
            UpdateTotals()
        End If
    End Sub

    Protected Sub UpdateTotals()
        Dim lNetTotalAmt As Long = 0
        Dim lNetTotal As Long = 0
        Dim lTotal As Long = 0
        Dim lOrigTotal As Long = 0

        lNetTotalAmt = txtNetTotal.Text
        lTotal = txtRepairTotal.Text - txtBetterment.Text - txtDeductable.Text - txtOtherAdjustments.Text
        lNetTotal = txtRepairTotal.Text - txtBetterment.Text - txtOtherAdjustments.Text
        '     lOrigTotal = txt

        'var OrigTotal = Number(document.getElementById("divOrigRepairTotal").innerText) - Number(document.getElementById("divOrigBetterment").innerText) - Number(document.getElementById("divOrigOthAdj").innerText);

        ' if (OrigTotal > 0) {
        '   if (NetTotal < OrigTotal){
        '      //divSavings.innerText = formatCurrency(((intOrigNetTotalAmt - Total) * 1.00 / intOrigNetTotalAmt) * 100.00) + "%";
        '      divSavings.innerText = formatCurrency(((OrigTotal - NetTotal) * 1.00 / OrigTotal) * 100.00) + "%";
        '   } else {
        '      divSavings.innerText = "--";
        '   }
        ' } else {
        '   divSavings.innerText = "--";
        ' }

        ' if (Total == "") Total = "0.00";

        ' if ((gsEstimateUpd == "1") && (Total < 0)) Total = "0.00";

        ' loNetTotalAmt.CCDisabled = false;
        ' loNetTotalAmt.value = Total;
        ' loNetTotalAmt.CCDisabled = true;

    End Sub


End Class