﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClaimXP.aspx.vb" Inherits="APDNet.ClaimXP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Claim XP</title>
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />

    <!-- ============== JQuery - Loader =============== -->      
    <script type="text/javascript" src="js/jQuery/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <script type="text/javascript" src="js/jQuery/jquery-ui.js"></script>
 
    <!-- ============== JQuery - Functions =============== -->      
    <script type="text/javascript">
        var docReady = 0;

        //------------------------//
        // Globals
        //------------------------//
        // See if jQuery available
        if (typeof ($) !== "undefined") {
            $(document).ready(function () {
                var docReady = 1;
            }
        )}

        $(function () {
            //------------------------//
            // Functions
            //------------------------//
            $("#diaAddCoverage").dialog({
                autoOpen: false, modal: true, show: "blind", hide: "blind"
            });

            $("#btnAddCoverage").click(function () {
                $("#diaAddCoverage").dialog("open");
                return false;
            });
        });

        //$(function () {
        //    $("#diaAddCoveragexx").dialog({
        //        autoOpen: true,
        //        resizable: false,
        //        height: "auto",
        //        width: "auto",
        //        modal: true,
        //        show: {
        //            effect: "blind",
        //            duration: 1000
        //        },
        //        hide: {
        //            effect: "explode",
        //            duration: 1000
        //        }
        //    });

        //    $("#btnAddCoverage").on("click", function () {
        //        $("#diaAddCoverage").dialog("open");
        //    });
        //});

        //-----------------------------------//
        // Add Coverage Description Changed
        //-----------------------------------//
        $(function () {
            $("#ddlCoverageDescription").change(function () {
                //alert(
                //    $("#ddlCoverageDescription :selected").val()
                //);
                //alert(
                //    $("#ddlCoverageDescription :selected").text()
                //);
                //switch ($("#ddlCoverageDescription").val().toUpperCase()) {
                //    case "COLL":
                //        break;
                //    case "COMP":
                //        break;
                //    case "LIAB":
                //        break;
                //    case "RENT":
                //        break;
                //    case "UM":
                //        break;
                //    case "UIM":
                //        break;
                //    default:
                //}
                var aCoverageClientCode =  $("#ddlCoverageDescription").val().toUpperCase().split("|");
                $("#txtCoverageClientCode").val(aCoverageClientCode[0]).val().toUpperCase();
                $("#txtCoverageType").val(aCoverageClientCode[0]).val().toUpperCase();

                switch (aCoverageClientCode[1]) {
                    case "0":
                        $("#txtCoverageAdditionalFlag").val("No");
                        break;
                    case "1":
                        $("#txtCoverageAdditionalFlag").val("Yes");
                        break;
                    default:
                        $("#txtCoverageAdditionalFlag").val("No");
                }
            })
        })

    </script>

        <script type="text/javascript">
        $(function () {
            $("#diaDelCoverage").dialog({
                autoOpen: false,
                resizable: false,
                height: 100,
                width: "auto",
                modal: true,
                show: {
                    effect: "blind",
                    duration: 1000
                },
                hide: {
                    effect: "explode",
                    duration: 1000
                }
            });

            $("#btnDelCoverage").on("click", function () {
                $("#diaDelCoverage").dialog("open");
            });
        });
    </script>

            <script type="text/javascript">
        $(function () {
            $("#diaEditCoverage").dialog({
                autoOpen: false,
                resizable: false,
                height: "auto",
                width: "auto",
                modal: true,
                show: {
                    effect: "blind",
                    duration: 1000
                },
                hide: {
                    effect: "explode",
                    duration: 1000
                }
            });

            $("#btnSavCoverage").on("click", function () {
                $("#diaEditCoverage").dialog("open");
            });
        });
    </script>

    <script type="text/javascript" language="javascript">
        //-------------------------------------------//
        // Get all the elements that have a class 
        // of Claim and Vehicle
        //-------------------------------------------//
        function getElementsByClassName(className) {
            var found = [];
            var elements = document.getElementsByTagName("*");
            for (var i = 0; i < elements.length; i++) {
                var names = elements[i].className.split(' ');
                for (var j = 0; j < names.length; j++) {
                    if (names[j] == className) found.push(elements[i]);
                }
            }
            return found;
        }

        //-------------------------------------------//
        // Handle the screen Div changes each time
        // the tabs are clicked
        //-------------------------------------------//
        function MenuTab(evt, currentClass, tabName, tabContainer, subTabContainer, subTab) {
            var ct;
            var tablinks = getElementsByClassName(currentClass);
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            if (tabContainer) {
                tabcontent = getElementsByClassName(tabContainer);
                for (ct = 0; ct < tabcontent.length; ct++) {
                    tabcontent[ct].style.display = "none";
                }
            }
            evt.srcElement.className += " active";
            document.getElementById(tabName).style.display = "block";

            if (subTab) {

                var subTabcontent = getElementsByClassName(subTab);
                for (ct = 0; ct < subTabcontent.length; ct++) {
                    if (subTabcontent[ct].style.display === 'block') {
                        subTabcontent[ct].style.display = 'none';
                    }
                }
            }
            if (subTabContainer) {
                document.getElementById(subTabContainer).style.display = "block";
                var vehDetails = document.getElementById(subTabContainer).parentNode.children[0];
                for (ct = 0; ct < vehDetails.children.length; ct++) {
                    vehDetails.children[ct].className = vehDetails.children[ct].className.replace(" active", "");
                }
                vehDetails.children[0].className += " active";
            }
        }
    </script>

    <script type="text/javascript">
        function ShopOnClick() {
            alert("ShopData");
        }

        function showToolTip() {
            //alert("showToolTip");
            //if (gsAssignmentID != "" && strServiceChannelCD == "PS" || strServiceChannelCD == "RRP" || strServiceChannelCD == "CS") {
                try {
                    divShopDetail.style.display = "inline";
                    bMouseInToolTip = true;
                } catch (e) { }
            //}
        }

        function keepToolTip() {
            //alert("keepToolTip");
        }

        function hideToolTip() {
            if (bMouseInToolTip == false) {
                divShopDetail.style.display = "none";
            }
        }

    </script>


    <style type="text/css">
        /*************************************/
        /* These styles control how the tabs */
        /* visually look as they are clicked */
        /*************************************/
        body
        {
            font-family: Arial;
        }
        
        /* Style the tab */
        .tab, .tabsub, .tabveh, .tabVehDetail, .tabAssignment
        {
            overflow: hidden;
        }
        
        /* Style the buttons inside the tab */
        .tab button, .tabsub button, .tabveh button, .tabVehDetail button, .tabAssignment button
        {
            cursor: pointer;
            transition: 0.3s;
            padding: 2px 4px 2px 4px;
            float: left;
            background: url("../images/InitialImage.png") no-repeat right top;
            color: Black;
            font-size: smaller;
            font-weight: bold;
            border: none;
            margin-right: 2px;
        }
        
        /* Change background color of buttons on hover */
        .tab button:hover, .tabsub button:hover, .tabveh button:hover, .tabVehDetail button:hover, .tabAssignment button:hover
        {
            color: White;
            background: url("../images/SelectedButton.png") no-repeat right top;
        }
        
        /* Create an active/current tablink class */
        .tab button.active, .tabsub button.active, .tabveh button.active, .tabVehDetail button.active, .tabAssignment button.active
        {
            float: left;
            display: block;
            background: url("../images/SelectedButton.png") no-repeat right top;
            padding: 2px 4px 2px 4px;
            color: Black;
            font-weight: bold;
            color: White;
            border: none;
        }
        
        /* Style the tab content */
        .tabcontent, .tabsubcontent, .TabClaimContent, .tabVehcontent, .tabVehDetcontent, .tabAssignmentcontent
        {
            display: none;
            overflow: hidden;
            padding: 0px 0px;
            border-top: none;
        }
        
        .showDefaultTab
        {
            display: block;
        }

/*******************************/
/* Modal Controls (background) */ 
/*******************************/
.modal {
    display: none;      /* Hidden by default */
    position: fixed;    /* Stay in place */
    z-index: 1;         /* Sit on top */
    left: 0;
    top: 0;
    width: 100%;        /* Full width */
    height: 100%;       /* Full height */
    overflow: auto;     /* Enable scroll if needed */
    background-color: rgb(0,0,0);       /* Fallback color */
    background-color: rgba(0,0,0,0.4);  /* Black w/ opacity */
}

/********************************/
/* Modal Controls (Content/Box) */ 
/********************************/
.modal-content {
    background-color: #fefefe;
    margin: 15% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}

/*********************************/
/* Modal Controls (Close Button) */ 
/*********************************/
.close {
    color: #aaa;
    float: right;
    font-size: 10px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
} 

/***************************/
/* Modal Controls (Header) */ 
/***************************/
.modal-header {
    padding: 1px 5px;
    background-color: #629ad6;
    color: white;
}

/***************************/
/* Modal Controls (Body)   */ 
/***************************/
.modal-body {padding: 2px 16px;}

/***************************/
/* Modal Controls (Footer) */ 
/***************************/
.modal-footer {
    padding: 1px 5px;
    background-color: #5cb85c;
    color: white;
}

/****************************/
/* Modal Controls (Content) */ 
/****************************/
.modal-content {
    position: absolute;     /* Location from Top Right */
    top: 50px;
    left: 50px;
    width: 370px;           /* Size */
    height: 28%;

    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    animation-name: animatetop;
    animation-duration: 0.4s
}

/******************************/
/* Modal Controls (Animation) */ 
/******************************/
@keyframes animatetop {
    from {top: -300px; opacity: 0}
    to {top: 0; opacity: 1}
} 


    </style>

</head>
<body>
    <form id="frmClaimView" runat="server">
    <!-- Claim info tab started here -->
    <div class="tab" style="position: absolute; top: 2px; left: 2px; right: 0px;">
        <button class="tablinks active" onclick="MenuTab(event,'tablinks','divClaim','TabClaimContent');return false;">
            Claim</button>
        <button class="tablinks" onclick="MenuTab(event, 'tablinks', 'divInsured', 'TabClaimContent');return false;">
            Insured</button>
        <button class="tablinks" onclick="MenuTab(event, 'tablinks', 'divCoverage', 'TabClaimContent');return false;">
            Coverage</button>
        <button class="tablinks" onclick="MenuTab(event, 'tablinks', 'divCarrier', 'TabClaimContent');return false;">
            Carrier</button>
    </div>
    <!-- Claim Section Start -->
    <div id="divClaim" class="TabClaimContent showDefaultTab">
        <!-- Claim Header Bar -->
        <table style="height: 135px; width: 785px; border-color: #667; border-spacing: 0px;">
            <tr style="vertical-align: top;">
                <td>
                    <!-- Claim Details -->
                    <div id="divClaimScroll" class="Claim" style="width: 760px; height: 95px; overflow: auto scroll;">
                        <table class="topTable" cellpadding="0" cellspacing="0" style="border-collapse: separate;
                            border-spacing: 0px; width: 710px;">
                            <tr style="vertical-align: top;">
                                <td>
                                    <table class="topTable" style="border-spacing: 0px; width: 330px;">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                Policy:
                                            </td>
                                            <td>
                                                <asp:TextBox class="Claim" ID="txtPolicyNumber" runat="server" CssClass="inputField"
                                                    Width="170px" MaxLength="20"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td>
                                                Client Claim #:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtClientClaimNumber" runat="server" CssClass="inputField" Width="200px"
                                                    MaxLength="30"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td>
                                                Date of Loss:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtLossDate" runat="server" CssClass="inputField"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td>
                                                State:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlState" runat="server" CssClass="inputField">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td>
                                                Loss Description:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtLossDescription" runat="server" CssClass="inputField" Height="35px"
                                                    Width="200" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table class="topTable" style="border-spacing: 0px; width: 355px;">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                Submitted By:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSubmittedBy" runat="server" CssClass="inputField" Width="240px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td>
                                                Submitted Date:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtIntakeFinishDate" runat="server" CssClass="inputField"></asp:TextBox>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Button runat="server" ID="btnClaimSave" Text="Save" />
                                                <asp:Label runat="server" ID="lblClaimSaved" ForeColor="Blue" />
                                                <%--                                                <button onclick="OpenWindow('Modal','AddCoverge.aspx','True','dialogHeight:355px;dialogWidth:520px;center:yes;help:no;resizable:no;scroll:no;status:no;')">AddNew</button>--%>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td>
                                                Claim Remarks:
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="topTable" style="border-spacing: 0px; width: 250px;">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                <asp:TextBox ID="txtRemarks" runat="server" Height="65px" Width="310px" MaxLength="1000"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <!-- Insured Section Start -->
    <div id="divInsured" class="TabClaimContent">
        <!-- Insured Header Bar -->
        <table style="height: 135px; width: 785px; border-color: #667; border-spacing: 0px;
            border: 0px 0px 0px 0px;">
            <tr style="vertical-align: top;">
                <td>
                    <!-- Insured Details -->
                    <div id="divInsuredScroll" class="Claim" style="width: 760px; height: 95px; overflow: auto scroll;">
                        <table class="topTable" style="border-spacing: 0px; width: 710px; border: 0px 0px 0px 0px;">
                            <tr style="vertical-align: top;">
                                <td>
                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0"
                                        border="0">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0"
                                                    border="0">
                                                    <tr align="left" style="vertical-align: top;">
                                                        <td align="left" style="width: 74px;">
                                                            Name:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNameTitle" runat="server" CssClass="inputField" Width="30px"
                                                                MaxLength="15"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNameFirst" runat="server" CssClass="inputField" Width="100px"
                                                                MaxLength="50"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNameLast" runat="server" CssClass="inputField" Width="100px"
                                                                MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="topTable" style="border-spacing: 0px; border: 0px 0px 0px 0px;">
                                        <tr style="vertical-align: top;">
                                            <td style="width: 75px;">
                                                Business:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBusinessName" runat="server" CssClass="inputField" Width="234px"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td style="width: 75px;">
                                                Address:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddress1" runat="server" CssClass="inputField" Width="234px"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td style="width: 75px;">
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddress2" runat="server" CssClass="inputField" Width="234px"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td style="width: 75px;">
                                                City:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddressCity" runat="server" CssClass="inputField" Width="190px"
                                                    MaxLength="30"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="topTable" style="border-spacing: 0px; width: 312px;">
                                        <tr style="vertical-align: top;">
                                            <td style="width: 75px;">
                                                State:
                                            </td>
                                            <td style="width: 15px;">
                                                <asp:TextBox ID="txtAddressState" runat="server" CssClass="inputField" Width="133px"
                                                    MaxLength="30"></asp:TextBox>
                                            </td>
                                            <td style="width: 15px;">
                                                &nbsp;Zip:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddressZip" runat="server" CssClass="inputField" Width="72px"
                                                    MaxLength="8"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table class="topTable" style="border-spacing: 0px; width: 350px;">
                                        <tr style="vertical-align: top;">
                                            <td style="width: 75px;">
                                                E-mail:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="inputField" Width="236px"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td style="width: 75px;">
                                                SSN/EIN:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFedTaxID" runat="server" CssClass="inputField" Width="100px"
                                                    MaxLength="15"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td style="width: 25px;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="topTable" style="border-spacing: 0px; width: 360px;">
                                        <tr style="vertical-align: top;">
                                            <td style="width: 200px;">
                                                Best Number to Call:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBestContactPhoneCD" runat="server" CssClass="inputField" Width="90px"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td>
                                                &nbsp;Day:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDay" runat="server" CssClass="inputField" Width="115px" MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td style="width: 75px;">
                                                Best Time to Call:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBestContactTime" runat="server" CssClass="inputField" Width="90px"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td>
                                                &nbsp;Night:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtNight" runat="server" CssClass="inputField" Width="115px" MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td style="width: 25px;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 125px;">
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;Alt:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAlternate" runat="server" CssClass="inputField" Width="110px"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <!-- Coverage info tab started here -->
    <div id="divCoverage" class="TabClaimContent">

        <asp:DropDownList runat="server" ID="ddlAddCovDesc" AutoPostBack="true" />

        <!-- Coverage Header Bar -->
        <table style="height: 135px; width: 785px; border-color: #667; border-spacing: 0px;">
            <tr style="vertical-align: top;">
                <td>
                    <!-- Coverage Details -->
                    <div id="divCoverageScroll" style="width: 760px; height: 95px; overflow: auto scroll;">
                        <table class="topTable" style="border-spacing: 0px; width: 710px;">
                            <tr style="vertical-align: top;">
                                <td>
                                    <table class="topTable" style="border-spacing: 0px; width: 680px; height: 150px;">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                <asp:GridView ID="gvCoverage" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma"
                                                    Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None"
                                                    BorderWidth="1px" CellPadding="3" GridLines="Horizontal" >
                                                    <AlternatingRowStyle BackColor="#F7F7F7" />
                                                    <Columns>
                                                        <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description"
                                                            HeaderStyle-Width="400px" HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="CoverageTypeCD" HeaderText="Type" SortExpression="CoverageTypeCD"
                                                            HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="AddtlCoverageFlag" HeaderText="Addl." SortExpression="AddtlCoverageFlag"
                                                            HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="DeductibleAmt" HeaderText="Deductable" SortExpression="DeductibleAmt"
                                                            HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="LimitAmt" HeaderText="Limit" SortExpression="LimitAmt"
                                                            HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="MaximumDays" HeaderText="Max. Days" SortExpression="MaximumDays"
                                                            HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="LimitDailyAmt" HeaderText="Daily Limit" SortExpression="LimitDailyAmt"
                                                            HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                    </Columns>
                                                    <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                    <HeaderStyle BackColor="#629ad6" Font-Bold="True" ForeColor="#F7F7F7" />
                                                    <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                                    <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <button type="button" id="btnAddCoverage">Add</button>
                                                <button type="button" id="btnDelCoverage">Del</button>
                                                <button type="button" id="btnSavCoverage">Save</button>
                                                <asp:Label runat="server" ID="lblCoverageStatus" ForeColor="Blue" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <!-- ==================== Add Coverage - Modal Dialog ============================== -->    
    <div id="diaAddCoverage" title="Add Coverage"  >
        <table class="topTable" style="border-spacing: 0px; width: 350px;">
            <tr style="vertical-align: top;">
                <td style="width: 85px;">
                    Client Code:
                </td>
                <td>
                    <asp:TextBox ID="txtCoverageClientCode" runat="server" CssClass="inputField" Width="50px" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr style="vertical-align: top;">
                <td style="width: 85px;">
                    Description:
                </td>
                <td>
                    <asp:DropDownList ID="ddlCoverageDescription" runat="server" CssClass="inputField" Width="175px" ></asp:DropDownList>
                    <asp:DropDownList ID="ddlCoverageAddtlCoverageFlag" runat="server" Visible="false" />
                </td>
            </tr>
            <tr style="vertical-align: top;">
                <td style="width: 85px;">
                    Type:
                </td>
                <td>
                    <asp:TextBox ID="txtCoverageType" runat="server" CssClass="inputField" Width="50px" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr style="vertical-align: top;">
                <td style="width: 85px;">
                    Additional Flag:
                </td>
                <td>
                    <asp:TextBox ID="txtCoverageAdditionalFlag" runat="server" CssClass="inputField" Width="175px" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr style="vertical-align: top;">
                <td style="width: 85px;">
                    Deductible:
                </td>
                <td>
                    <asp:TextBox ID="txtCoverageDeductible" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                </td>
            </tr>
            <tr style="vertical-align: top;">
                <td style="width: 85px;">
                    Limit:
                </td>
                <td>
                    <asp:TextBox ID="txtCoverageLimit" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                </td>
            </tr>
            <tr style="vertical-align: top;">
                <td>&nbsp;</td>
            </tr>
            <tr style="vertical-align: top;">
                <td style="width: 85px;">
                    &nbsp;
                </td>
                <td>
                    <asp:Button ID="btnCoverageAddSave" runat="server" Text="Save" />
                    <asp:Button runat="server" ID="btnCoverageAddCancel" Text="Cancel" OnClientClick="" />
                </td>
            </tr>
        </table>
    </div>

    <!-- Coverage info tab Ended here -->
    <!-- Carrier info tab started here -->
    <div id="divCarrier" class="TabClaimContent">
        <table style="height: 135px; width: 785px; border-color: #666; border-spacing: 0px;">
            <tr style="vertical-align: top;">
                <td>
                    <!-- Carrier Details -->
                    <div id="divCarrierScroll" style="width: 760px; height: 95px; overflow: auto scroll;">
                        <table class="topTable" style="border-spacing: 0px; width: 700px;">
                            <tr style="vertical-align: top;">
                                <td>
                                    <table class="topTable" style="border-spacing: 0px; width: 760px;">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                <table class="topTable" style="border-spacing: 0px; width: 350px;">
                                                    <tr style="vertical-align: top;">
                                                        <td style="width: 85px;">
                                                            Name:
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnEditCarrier" runat="server" ImageUrl="images/next_button.gif" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCarrierNameTitle" runat="server" CssClass="inputField" Width="30px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCarrierNameFirst" runat="server" CssClass="inputField" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCarrierNameLast" runat="server" CssClass="inputField" Width="100px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table class="topTable" style="border-spacing: 0px; width: 760px;">
                                                    <tr style="vertical-align: top;">
                                                        <td style="width: 65px;">
                                                            Office:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCarrierOfficeName" runat="server" CssClass="inputField" Width="450px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="vertical-align: top;">
                                                        <td style="width: 65px;">
                                                            E-mail:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCarrierEmailAddress" runat="server" CssClass="inputField" Width="450px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="vertical-align: top;">
                                                        <td style="width: 65px;">
                                                            Phone:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCarrierPhone" runat="server" CssClass="inputField" Width="200px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="vertical-align: top;">
                                                        <td style="width: 65px;">
                                                            Fax:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCarrierFax" runat="server" CssClass="inputField" Width="200px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <!-- Carrier info tab Ended here -->

    <!-- =============================================== VEHICLE =========================================================== -->
    <hr />

    <!-- ==================== Edit Coverage - Modal Dialog ============================== -->    
    <div id="diaEditCoverage" title="Edit Coverage" style="display: none;">
        <table class="topTable" style="border-spacing: 0px; width: 350px;">
            <tr style="vertical-align: top;">
                <td style="width: 85px;">
                    Client Code:
                </td>
                <td>
                    <asp:TextBox ID="txtEditCoverageClientCode" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                </td>
            </tr>
            <tr style="vertical-align: top;">
                <td style="width: 85px;">
                    Description:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server" Width="175px"></asp:DropDownList>
                </td>
            </tr>
            <tr style="vertical-align: top;">
                <td style="width: 85px;">
                    Type:
                </td>
                <td>
                    <asp:TextBox ID="txtEditCoverageType" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                </td>
            </tr>
            <tr style="vertical-align: top;">
                <td style="width: 85px;">
                    Additional Flag:
                </td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server" CssClass="inputField" Width="175px"></asp:TextBox>
                </td>
            </tr>
            <tr style="vertical-align: top;">
                <td style="width: 85px;">
                    Deductible:
                </td>
                <td>
                    <asp:TextBox ID="txtEditCoverageDeductible" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                </td>
            </tr>
            <tr style="vertical-align: top;">
                <td style="width: 85px;">
                    Limit:
                </td>
                <td>
                    <asp:TextBox ID="txtEditCoverageLimit" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                </td>
            </tr>
            <tr style="vertical-align: top;">
                <td>&nbsp;</td>
            </tr>
            <tr style="vertical-align: top;">
                <td style="width: 85px;">
                    &nbsp;
                </td>
                <td>
                    <asp:Button runat="server" ID="btnEditCoverageSave" Text="Save" />
                    <asp:Button runat="server" ID="btnEditCoverageCancel" Text="Cancel" />
                </td>
            </tr>
        </table>
    </div>

    <!-- ==================== Del Coverage - Modal Dialog ============================== -->    
    <div id="diaDelCoverage" title="Delete Coverage" style="display: none;">
        <p>
            Do you want to delete the selected coverage?  Click Yes to delete.
        </p>
        <asp:Button runat="server" ID="btnDelCoverageYes" Text="Yes" />
        <asp:Button runat="server" ID="btnDelCoverageNo" Text="No" />
        <asp:Button runat="server" ID="btnDelCoverageCancel" Text="Cancel" />
    </div>

     <table>
            <tr>
                <td>
                    <!-- Vehicle tab repeater starts here -->
                    <asp:Repeater ID="RepeaterVehTab" runat="server">
                        <HeaderTemplate>
                            <div class="tabveh">
                        </HeaderTemplate>

                        <ItemTemplate>
                            <button class='<%# IIf(Container.ItemIndex = 0, "tabVehlinks active", "tabVehlinks")%>'
                                onclick="MenuTab(event,'tabVehlinks', 'Tab<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>', 'tabVehcontent','divDescription_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>','tabVehDetcontent');return false;">
                                <%#DataBinder.Eval(Container, "DataItem.Businessname")%></button>
                        </ItemTemplate>

                        <FooterTemplate>
                            </div> 
                        </FooterTemplate>
                    </asp:Repeater>


                    <!-- Vehicle tab repeater ends here -->

                    <!-- Vehicle Details repeater starts here -->
                    <asp:Repeater ID="RepeaterVehdetTab" runat="server" OnItemDataBound="RepeaterVehdetTab_ItemDataBound" OnItemCommand="RepeaterVehdetTab_ItemCommand"   >
                        <HeaderTemplate></HeaderTemplate>
                        <ItemTemplate>


                            <!-- Tab content -->
                            <div id="Tab<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class='<%# IIf(Container.ItemIndex = 0, "tabVehcontent showDefaultTab", "tabVehcontent")%>'>

                                <div class="tabVehDetail">
                                    <button class="tabVehDetlinks active" onclick="MenuTab(event,'tabVehDetlinks', 'divDescription_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>', 'tabVehDetcontent' );return false;">Description</button>
                                    <button class="tabVehDetlinks" onclick="MenuTab(event, 'tabVehDetlinks','divVehicleLocation_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>', 'tabVehDetcontent' );return false;">Vehicle Location</button>
                                    <button class="tabVehDetlinks" onclick="MenuTab(event, 'tabVehDetlinks','divContact_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>', 'tabVehDetcontent');return false;">Contact</button>
                                    <button class="tabVehDetlinks" onclick="MenuTab(event, 'tabVehDetlinks','divInvolved_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>', 'tabVehDetcontent');return false;">Involved</button>
                                    <button class="tabVehDetlinks" onclick="MenuTab(event, 'tabVehDetlinks','divMainAssignment_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>', 'tabVehDetcontent','divAssignment_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%> ');return false;"><%#DataBinder.Eval(Container, "DataItem.ActiveServiceChannel")%></button>
                                </div>

                                <div id="divDescription_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabVehDetcontent showDefaultTab">
                                    <table style="border-spacing: 0px; height: 155px; width: 60px; border-color: #667;">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                <!-- Vehicle Description Details -->
                                                <div id="divDescriptionScroll" style="height: 295px; overflow: auto scroll;">
                                                    <table class="topTable" style="border-spacing: 0px;">
                                                        <tr style="vertical-align: top;">
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px; width: 180px; ">
                                                                    <tr style="vertical-align: top;">
                                                                        <td>
                                                                            <table class="topTable" style="border-spacing: 0px; width: 280px;" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 100px;"><b>Year:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtVehicleYear" runat="server" CssClass="inputField" Width="75px" Text='<%#DataBinder.Eval(Container, "DataItem.VehicleYear")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 100px;"><b>Make:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtMake" runat="server" CssClass="inputField" Width="150px" MaxLength="5" Text='<%#DataBinder.Eval(Container, "DataItem.Make")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 100px;"><b>Model:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtModel" runat="server" CssClass="inputField" Width="150px" MaxLength="50" Text='<%#DataBinder.Eval(Container, "DataItem.Model")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 100px;"><b>Body Style:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtBodyStyle" runat="server" CssClass="inputField" Width="150px" MaxLength="50" Text='<%#DataBinder.Eval(Container, "DataItem.BodyStyle")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 100px;"><b>Color:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtColor" runat="server" CssClass="inputField" Width="150px" MaxLength="20" Text='<%#DataBinder.Eval(Container, "DataItem.Color")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 100px;"><b>VIN:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtVIN" runat="server" CssClass="inputField" Width="150px" MaxLength="20" Text='<%#DataBinder.Eval(Container, "DataItem.VIN")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 100px;"><b>VIN (Est):</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtVINEst" runat="server" CssClass="inputField" Width="150px" MaxLength="20" Text='<%#DataBinder.Eval(Container, "DataItem.EstimateVIN")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 100px;"><b>License Plate:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtLicensePlateNumber" runat="server" CssClass="inputField" Width="125px" MaxLength="20" Text='<%#DataBinder.Eval(Container, "DataItem.LicensePlateNumber")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td><b>License Plate State:</b></td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="ddlLicensePlateState" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 100px;"><b>Odometer Reading:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtMileage" runat="server" CssClass="inputField" Width="100px" MaxLength="20" Text='<%#DataBinder.Eval(Container, "DataItem.Mileage")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 100px;"><b>Book Value:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtBookValueAmt" runat="server" CssClass="inputField" Width="115px" MaxLength="20" Text='<%#DataBinder.Eval(Container, "DataItem.BookValueAmt")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 65px;"><b>Rental Days Authorized:</b></td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtRentalDaysAuthorized" runat="server" CssClass="inputField" Width="50px" MaxLength="20" Text='<%#DataBinder.Eval(Container, "DataItem.RentalDaysAuthorized")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 100px;"><b>Rental instructions:</b></td>
                                                                                    <td style="vertical-align: top;">
                                                                                        <asp:TextBox ID="txtRentalInstructions" runat="server" CssClass="inputField" Width="150px" Height="50px" MaxLength="20" TextMode="MultiLine" Text='<%#DataBinder.Eval(Container, "DataItem.RentalInstructions")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>

                                                                        <td>
                                                                            <table class="topTable" style="border-spacing: 0px; width: 280px;">
                                                                                <tr style="vertical-align: top;">
                                                                                    <td>
                                                                                        <table class="topTable" style="border-spacing: 0px; width: 400px;">
                                                                                            <tr style="vertical-align: top;">
                                                                                                <td style="width: 75px;">Impact Area:</td>
                                                                                                <td style="width: 75px;">&nbsp;</td>
                                                                                                <td style="width: 110px;">Prior Damage Area:</td>
                                                                                                <td style="width: 75px;">&nbsp;</td>
                                                                                                <td style="width: 35px;">Party:</td>
                                                                                                <td style="width: 75px;">&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr style="vertical-align: top;">
                                                                                                <td>
                                                                                                    <asp:TextBox ID="txtImpactPoints" runat="server" CssClass="inputField" Width="150px" Height="25" TextMode="MultiLine" Text='<%#DataBinder.Eval(Container, "DataItem.ImpactPoints")%>'></asp:TextBox></td>
                                                                                                <td>
                                                                                                    <asp:ImageButton ID="btnImpact" runat="server" ImageUrl="images/next_button.gif" /></td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="txtPriorDamagePoints" runat="server" CssClass="inputField" Width="150px" Height="25" TextMode="MultiLine" Text='<%#DataBinder.Eval(Container, "DataItem.PriorImpactPoints")%>'></asp:TextBox></td>
                                                                                                <td>
                                                                                                    <asp:ImageButton ID="btnPrior" runat="server" ImageUrl="images/next_button.gif" /></td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="txtParty" runat="server" CssClass="inputField" Width="60px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr style="vertical-align: top;">
                                                                                                <td style="width: 75px;">&nbsp;</td>
                                                                                                <td style="width: 75px;">&nbsp;</td>
                                                                                                <td style="width: 75px;">&nbsp;</td>
                                                                                                <td style="width: 75px;">&nbsp;</td>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkDrivableFlag" runat="server" Text="Drivable" /></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table class="topTable" style="border-spacing: 0px; width: 403px;">
                                                                                            <tr style="vertical-align: top;">
                                                                                                <td style="width: 100px;">Init Assign. Type:</td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="txtInitialAssignmentType" runat="server" CssClass="inputField" Width="140px" Text='<%#DataBinder.Eval(Container, "DataItem.InitialAssignmentType")%>'></asp:TextBox></td>
                                                                                                <td style="width: 55px;">&nbsp;Coverage:</td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="txtInitialCoverageProfile" runat="server" CssClass="inputField" Width="94px" Text='<%#DataBinder.Eval(Container, "DataItem.InitialCoverageProfile")%>'></asp:TextBox></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="vertical-align: top;">
                                                                                        <div id="divServiceChannel" style="width: 400px; height: 110px; overflow: auto scroll; border: 1px solid black;">
                                                                                            <table class="topTable" style="border-spacing: 0px; width: 400px;">
                                                                                                <tr style="vertical-align: top;">
                                                                                                    <td style="width: 160px;">Service Channel Summary:</td>
                                                                                                </tr>
                                                                                                <tr style="vertical-align: top;">
                                                                                                    <td>
                                                                                                        <asp:GridView ID="gvServiceChannel" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
                                                                                                            <AlternatingRowStyle BackColor="#F7F7F7" />
                                                                                                            <Columns>
                                                                                                                <asp:BoundField DataField="Channel" HeaderText="Channel" SortExpression="Channel" HeaderStyle-Width="400px" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                <asp:BoundField DataField="EstAmount" HeaderText="EstAmount." SortExpression="EstAmount" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                <asp:BoundField DataField="Coverage" HeaderText="Coverage" SortExpression="Coverage" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                <asp:BoundField DataField="Deductable" HeaderText="Deductable" SortExpression="Deductable" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                <asp:BoundField DataField="Overage" HeaderText="Overage" SortExpression="Overage" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            </Columns>
                                                                                                            <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                                                                            <HeaderStyle BackColor="#629ad6" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                                                            <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                                                                                            <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                                                                                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                                                        </asp:GridView>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="vertical-align: top;">
                                                                                    <td>
                                                                                        <table class="topTable" style="border-spacing: 0px; width: 390px;">
                                                                                            <tr style="vertical-align: top;">
                                                                                                <td style="width: 115px;">Vehicle Remarks:</td>
                                                                                            </tr>
                                                                                            <tr style="vertical-align: top;">
                                                                                                <td>
                                                                                                    <asp:TextBox ID="txtVehicleRemarks" runat="server" CssClass="inputField" Width="398px" Height="56px" TextMode="MultiLine" Text='<%#DataBinder.Eval(Container, "DataItem.VehicleRemarks")%>'></asp:TextBox></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div id="divVehicleLocation_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabVehDetcontent">

                                    <table style="height: 175px; width: 785px; border-color: #667; border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                <!-- Vehicle Location Details -->
                                                <div id="divVehicleLocationScroll" style="width: 760px; height: 140px; overflow: auto scroll;">
                                                    <table class="topTable" style="border-spacing: 0px; width: 500px;" cellpadding="0" cellspacing="0" border="0">
                                                        <tr style="vertical-align: top;">
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">Location:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtLocationName" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.LocationName")%>'></asp:TextBox></td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">Address:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtLocationAddress1" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.LocationAddress1")%>'></asp:TextBox></td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">&nbsp;</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtLocationAddress2" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.LocationAddress2")%>'></asp:TextBox></td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">City:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtLocationCity" runat="server" CssClass="inputField" Width="100px" Text='<%#DataBinder.Eval(Container, "DataItem.LocationCity")%>'></asp:TextBox></td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top;">
                                                                        <td>
                                                                            <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr style="vertical-align: top;">
                                                                                    <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">State:</td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="ddlLocationState" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList>
                                                                                        Zip:</td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtLocationZip" runat="server" CssClass="inputField" Width="75px" Text='<%#DataBinder.Eval(Container, "DataItem.LocationZip")%>'></asp:TextBox></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table class="topTable" style="border-spacing: 0px; width: 500px;" cellpadding="0" cellspacing="0" border="0">
                                                        <tr style="vertical-align: top;">
                                                            <td style="width: 60px;">Phone:</td>
                                                            <td>
                                                                <asp:TextBox ID="txtLocationPhone" runat="server" CssClass="inputField" Width="90px" MaxLength="50" Text='<%#DataBinder.Eval(Container, "DataItem.LocationPhone")%>'></asp:TextBox></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                </div>

                                <div id="divContact_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabVehDetcontent">

                                    <asp:HiddenField runat="server" ID="oDataItem" Value='<%#DataBinder.Eval(Container, "DataItem")%>' />

                                    <table style="height: 175px; width: 785px; border-color: #667; border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                <!-- Vehicle Contact Details -->
                                                <div id="divContactScroll" style="width: 760px; height: 140px; overflow: auto scroll;">
                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                        <tr style="vertical-align: top;">
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr align="left" style="vertical-align: top;">
                                                                        <td align="left" style="width: 60px;">Name:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtContactNameTitle" runat="server" CssClass="inputField" Width="30px" MaxLength="15" Text='<%#DataBinder.Eval(Container, "DataItem.ContactNameTitle")%>'></asp:TextBox></td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtContactNameFirst" runat="server" CssClass="inputField" Width="100px" MaxLength="50" Text='<%#DataBinder.Eval(Container, "DataItem.ContactNameFirst")%>'></asp:TextBox></td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtContactNameLast" runat="server" CssClass="inputField" Width="100px" MaxLength="50" Text='<%#DataBinder.Eval(Container, "DataItem.ContactNameLast")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">Address:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtContactAddress1" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.ContactAddress1")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 30px; padding: 0px; margin: 0px; border: 0px;">&nbsp;</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtContactAddress2" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.ContactAddress2")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 30px; padding: 0px; margin: 0px; border: 0px;">City:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtContactCity" runat="server" CssClass="inputField" Width="100px" Text='<%#DataBinder.Eval(Container, "DataItem.ContactCity")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr style="vertical-align: top;">
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top;">
                                                                        <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">State:</td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlContactState" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList>
                                                                            Zip:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtContactZip" runat="server" CssClass="inputField" Width="75px" Text='<%#DataBinder.Eval(Container, "DataItem.ContactZip")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr style="vertical-align: top;">
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 60px; padding: 0px; margin: 0px; border: 0px;">Email:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtContactEmailAddress" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.ContactEmailAddress")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>


                                <div id="divInvolved_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabVehDetcontent">
                                    <!-- Vehicle Involved Header Bar -->
                                    <table style="height: 225px; width: 675px; border-color: #667; border-spacing: 0px;">
                                        <tr style="vertical-align: top;">
                                            <td>
                                                <!-- Vehicle Involved Details -->
                                                <div id="divInvolvedScroll" style="width: 360px; height: 140px; overflow: auto scroll;">
                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                        <tr style="vertical-align: top;">
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr align="left" style="vertical-align: top;">
                                                                        <td align="left" style="width: 90px;">Involved:</td>
                                                                        <td>
                                                                            <asp:DropDownList ID="DropDownList4" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr align="left" style="vertical-align: top;">
                                                                        <td align="left" style="width: 90px;">Name:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtInvolvedNameTitle" runat="server" CssClass="inputField" Width="30px" MaxLength="15" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedNameTitle")%>'></asp:TextBox></td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtInvolvedNameFirst" runat="server" CssClass="inputField" Width="100px" MaxLength="50" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedNameFirst")%>'></asp:TextBox></td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtInvolvedNameLast" runat="server" CssClass="inputField" Width="100px" MaxLength="50" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedNameLast")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Business:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtInvolvedBusinessName" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedBusinessName")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Business Type:</td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlInvolvedBusinessType" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Address:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtInvolvedAddress1" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedAddress1")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">&nbsp;</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtInvolvedAddress2" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedAddress2")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">City:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtInvolvedAddressCity" runat="server" CssClass="inputField" Width="100px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedCity")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr style="vertical-align: top;">
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">State:</td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlInvolvedState" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList>
                                                                            Zip:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtInvolvedAddressZip" runat="server" CssClass="inputField" Width="75px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedZip")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr style="vertical-align: top;">
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Email:</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtInvolvedEmailAddress" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedEmailAddress")%>'></asp:TextBox></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr style="vertical-align: top;">
                                                            <td>
                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Involved Type:</td>
                                                                        <td>
                                                                            <asp:CheckBox ID="cbInsured" runat="server" Text="Insured"></asp:CheckBox></td>
                                                                        <td>
                                                                            <asp:CheckBox ID="cbOwner" runat="server" Text="Owner"></asp:CheckBox></td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">&nbsp;</td>
                                                                        <td>
                                                                            <asp:CheckBox ID="cbDriver" runat="server" Text="Driver"></asp:CheckBox></td>
                                                                        <td>
                                                                            <asp:CheckBox ID="cbPassenger" runat="server" Text="Passenger"></asp:CheckBox></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                            <td>
                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                    <tr align="left" style="vertical-align: top;">
                                                        <td align="left" style="width: 110px;">Gender:</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlInvolvedGender" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList></td>
                                                    </tr>
                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                        <td style="width: 110px; padding: 0px; margin: 0px; border: 0px;">SSN/EIN:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtFedTaxId" runat="server" CssClass="inputField" Width="120px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedFedTaxId")%>'></asp:TextBox></td>
                                                    </tr>
                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                        <td style="width: 110px; padding: 0px; margin: 0px; border: 0px;">DOB:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtBirthDate" runat="server" CssClass="inputField" Width="90px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedBirthDate")%>'></asp:TextBox></td>
                                                    </tr>
                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                        <td style="width: 110px; padding: 0px; margin: 0px; border: 0px;">Age:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtAge" runat="server" CssClass="inputField" Width="50px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedAge")%>'></asp:TextBox></td>
                                                    </tr>
                                                    <tr align="left" style="vertical-align: top;">
                                                        <td align="left" style="width: 110px;">Best Number to Call:</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlBestNumberCall" runat="server" CssClass="inputField" Width="100px"></asp:DropDownList></td>
                                                    </tr>
                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                        <td style="width: 110px; padding: 0px; margin: 0px; border: 0px;">Best Time to Call:</td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox30" runat="server" CssClass="inputField" Width="150px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                        <td style="width: 110px; padding: 0px; margin: 0px; border: 0px;">Day:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtInvolvedPhoneDay" runat="server" CssClass="inputField" Width="120px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedPhoneDay")%>'></asp:TextBox></td>
                                                    </tr>
                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                        <td style="width: 110px; padding: 0px; margin: 0px; border: 0px;">Night:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtInvolvedPhoneNight" runat="server" CssClass="inputField" Width="120px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedPhoneNight")%>'></asp:TextBox></td>
                                                    </tr>
                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                        <td style="width: 110px; padding: 0px; margin: 0px; border: 0px;">Alt.:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtAlt" runat="server" CssClass="inputField" Width="120px" Text='<%#DataBinder.Eval(Container, "DataItem.Involveddata.InvolvedPhoneAlt")%>'></asp:TextBox></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div id="divMainAssignment_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%>" class="tabVehDetcontent">
                                    <div class="tabAssignment active">
                                       <button class="tabAssignmentlinks active" onclick="MenuTab(event, 'tabAssignmentlinks','divAssignment_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%> ','tabAssignmentcontent');return false;">Assignment</button>
                                        <button class="tabAssignmentlinks" onclick="MenuTab(event, 'tabAssignmentlinks','divDocuments_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%> ','tabAssignmentcontent');return false;">Documents</button>
                                        <button class="tabAssignmentlinks" onclick="MenuTab(event, 'tabAssignmentlinks','divConcessions_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%> ','tabAssignmentcontent');return false;">Concessions</button>
                                     </div>

                                    <div id="divAssignment_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%> " class="tabAssignmentcontent showDefaultTab">

                                        <table style="height: 275px; width: 785px; border-color: #667; border-spacing: 0px;">
                                            <tr style="vertical-align: top;">
                                                <td>
                                                    <!-- Assignment Details -->
                                                    <div id="divAssignmentScroll" style="width: 760px; height: 140px; overflow: auto scroll;">
                                                        <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                            <tr style="vertical-align: top;">
                                                                <!-- Left Side Assignment -->
                                                                <td>
                                                                    <table class="topTable" style="width: 310px; border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">
                                                                                <a style="font-weight:bold" onclick="showToolTip(this)" onmouseover="showToolTip(this)" onmouseout="bMouseInToolTip = false;window.setTimeout('hideToolTip()', 1000)"><u>Assign. To:</u></a>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtShopAssignmentAssignTo" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentAssignTo")%>'></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Contact:</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtShopAssignmentContact" runat="server" CssClass="inputField" Width="150px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopContact")%>'></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Phone:</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtShopAssignmentContactPhone" runat="server" CssClass="inputField" Width="150px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopPhone")%>'></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Fax:</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtShopAssignmentContactFax" runat="server" CssClass="inputField" Width="150px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopFax")%>'></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Elec. Status:</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtVANAssignmentStatusName" runat="server" CssClass="inputField" Width="210px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentElecStatus")%>'></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Fax Status:</td>
                                                                            <td>
                                                                                <asp:TextBox ID="FAXAssignmentStatusName" runat="server" CssClass="inputField" Width="210px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentFaxStatus")%>'></asp:TextBox></td>
                                                                        </tr>
                                                                    </table>
                                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Assgn. Dt.:</td>
                                                                            <td><asp:TextBox ID="txtAssignmentDate" runat="server" CssClass="inputField" Width="70px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentAssignDate")%>'></asp:TextBox></td>
                                                                            <td style="width: 40px; padding: 0px; margin: 0px; border: 0px;">&nbsp;Prev.:</td>
                                                                            <td><asp:TextBox ID="txtAssignmentPrevDate" runat="server" CssClass="inputField" Width="70px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentPrevDate")%>'></asp:TextBox></td>
                                                                        </tr>
                                                                    </table>
                                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Work Status:</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtAssignmentWorkStatus" runat="server" CssClass="inputField" Width="200px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentWorkStatus")%>'></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Eff. Ded. Sent:</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtAssignmentDeductSent" runat="server" CssClass="inputField" Width="80px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentDeductSent")%>' ></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Remarks:</td>
                                                                            <td style="vertical-align: top;" >
                                                                                <asp:TextBox ID="txtAssignmentRemarks" runat="server" CssClass="inputField" Width="200px" Height="30px" TextMode="MultiLine" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentRemarks")%>' ></asp:TextBox></td>
                                                                        </tr>
                                                                    </table>
                                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td style="padding: 0px; margin: 0px; border: 0px;">
                                                                                <br />
                                                                                <asp:Button runat="server" ID="btnShopSelect" Text="Select" />
                                                                                <asp:Button runat="server" ID="btnSendAssignment" OnClick="btnSendAssignment_Click" Text="Send Assignment" CommandArgument='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.ClaimAspectServiceChannelID") + "," + DataBinder.Eval(Container, "DataItem.ShopAssignmentData.InsuranceCompanyID")%>' />
                                                                                <asp:Button runat="server" ID="btnResendAssignment" Text="Resend Assignment" />
                                                                                <asp:Button runat="server" ID="btnCancelAssignment" Text="Cancel Assignment" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <!-- Right Side Assignment -->
                                                                <td>
                                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td>
                                                                                <asp:Panel runat="server" Id="CurrentEstimate" GroupingText="Current Estimate" Font-Bold="true" Width="400" Height="80" ScrollBars="None">
                                                                                <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Gross Amt:</td>
                                                                                        <td><asp:TextBox ID="txtGrossAmt" runat="server" CssClass="inputField" Width="100px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.CurEstGrossRepairTotal")%>'></asp:TextBox></td>
                                                                                        <td style="width: 140px; padding: 0px; margin: 0px; border: 0px;">&nbsp;Deduct Applied:</td>
                                                                                        <td><asp:TextBox ID="txtDeductibleApplied" runat="server" CssClass="inputField" Width="100px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.CurEstDeductiblesApplied")%>'></asp:TextBox></td>
                                                                                    </tr>
                                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Net Amt:</td>
                                                                                        <td><asp:TextBox ID="txtNetRepairTotal" runat="server" CssClass="inputField" Width="100px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.CurEstNetRepairTotal")%>'></asp:TextBox></td>
                                                                                        <td style="width: 140px; padding: 0px; margin: 0px; border: 0px;">&nbsp;Limits Applied:</td>
                                                                                        <td><asp:TextBox ID="txtLimitsEffect" runat="server" CssClass="inputField" Width="100px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.CurEstLimitsEffect")%>'></asp:TextBox></td>
                                                                                    </tr>
                                                                                </table>
                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td>
                                                                                <asp:GridView ID="gvAssignmentCoverage" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" Visible="true">
                                                                                    <AlternatingRowStyle BackColor="#F7F7F7" />
                                                                                    <Columns>
                                                                                        <asp:BoundField DataField="Coverage" HeaderText="Coverage" SortExpression="Coverage" HeaderStyle-Width="400px" HeaderStyle-HorizontalAlign="Left" />
                                                                                        <asp:BoundField DataField="Deductable" HeaderText="Ded. Applied" SortExpression="Deductable" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                                                        <asp:BoundField DataField="Overage" HeaderText="Overage/Limit" SortExpression="Overage" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                                                        <asp:BoundField DataField="Partial" HeaderText="Partial" SortExpression="Partial" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                                                                                    </Columns>
                                                                                    <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                                                    <HeaderStyle BackColor="#629ad6" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                                    <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                                                                    <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                                                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                                </asp:GridView>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                        <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Disposition:</td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlDisposition" runat="server" CssClass="inputField" Width="150px"></asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    </table>
                                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Inspection:</td>
                                                                            <td><asp:TextBox ID="txtInspectionDate" runat="server" CssClass="inputField" Width="70px" Text='<%#DataBinder.Eval(Container, "DataItem.InspectionDate")%>'></asp:TextBox></td>
                                                                            <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">&nbsp;Orig. Est. Dt.:</td>
                                                                            <td><asp:TextBox ID="txtOriginalEstimateDate" runat="server" CssClass="inputField" Width="70px" Text='<%#DataBinder.Eval(Container, "DataItem.OriginalEstimateDate")%>'></asp:TextBox></td>
                                                                        </tr>
                                                                    </table>
                                                                    <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr style="vertical-align: top; padding: 0px; margin: 0px;">
                                                                            <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">Repair Start Dt.:</td>
                                                                            <td><asp:TextBox ID="txtWorkStartDate" runat="server" CssClass="inputField" Width="70px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.WorkStartDate")%>'></asp:TextBox></td>
                                                                            <td style="width: 90px; padding: 0px; margin: 0px; border: 0px;">&nbsp;End Dt.:</td>
                                                                            <td><asp:TextBox ID="txtWorkEndDate" runat="server" CssClass="inputField" Width="70px" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.WorkEndDate")%>'></asp:TextBox></td>
                                                                        </tr>
                                                                    </table>


                                                                </td>
                                                                </tr>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>


                                    <div id="divShopDetail" style="position:absolute;top:100px;left:94px;display:none;height:290px;width:550px;overflow:auto;FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=135,color=#A9A9A9,strength=3)" onclick="bMouseInToolTip=false;hideToolTip()" onmouseenter="keepToolTip()" onmouseleave="bMouseInToolTip=false;hideToolTip()">
                                      <asp:Panel runat="server" BorderStyle="Solid" BorderWidth="1px">
                                        <table border="0" cellspacing="2" cellpadding="0" style="height:100%;width:100%;border:1px solid #556B2F;background-color:#FFFFFF;border-collapse:collapse;table-layout:fixed">
                                        <colgroup>
                                          <col width="150px"/>
                                          <col width="*"/>
                                          <col width="25px"/>
                                          <col width="55px"/>
                                          <col width="55px"/>
                                          <col width="20px"/>
                                        </colgroup>
                                        <tr style="height:21px;text-align:center;font-weight:bold;background-color:#556B2F;color:#FFFFFF">
                                          <td colspan="12">Shop Detail</td>
                                        </tr>
                                        <tr>
                                          <td align="right"><b>Program Shop:&nbsp;</b></td>
                                          <td colspan="5">
                                              <asp:TextBox runat="server" id="popProgramShop" width="50" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentProgramType")%>' />
                                          </td>
                                        </tr>
                                        <tr>
                                          <td align="right"><b>Communication Method:&nbsp;</b></td>
                                          <td colspan="5">
                                              <asp:TextBox runat="server" id="popPreferredCommunicationMethod" width="250" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentPreferredCommunicationMethodID")%>' />
                                          </td>
                                        </tr>
                                        <tr>
                                          <td align="right"><b>Pref Estimate Package:&nbsp;</b></td>
                                          <td colspan="5">
                                              <asp:TextBox runat="server" id="popPreferredEstimatePackage" width="250" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentPreferredEstimatePackageID")%>' />
                                          </td>
                                        </tr>
                                        <tr>
                                          <td align="right"><b>Name:&nbsp;</b></td>
                                          <td colspan="5">
                                              <asp:TextBox runat="server" id="popShopName" width="250" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentAssignTo")%>' />
                                          </td>
                                        </tr>
                                        <tr>
                                          <td align="right"><b>Address:&nbsp;</b></td>
                                          <td colspan="5">
                                              <asp:TextBox runat="server" id="popShopAddress1" width="250" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopAddress1")%>' />
                                          </td>
                                        </tr>
                                        <tr>
                                          <td align="right"><b>&nbsp;</b></td>
                                          <td colspan="5">
                                              <asp:TextBox runat="server" id="popShopAddress2" width="250" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopAddress2")%>' />
                                          </td>
                                        </tr>
                                        <tr>
                                          <td align="right"><b>City:&nbsp;</b></td>
                                          <td colspan="5">
                                              <asp:TextBox runat="server" id="popShopAddressCity" width="230" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopAddressCity")%>' />
                                          </td>
                                        </tr>
                                        <tr>
                                          <td align="right"><b>State:&nbsp;</b></td>
                                          <td>
                                              <asp:TextBox runat="server" id="popShopAddressState" width="25" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopAddressState")%>' />
                                          </td>
                                          <td align="right"><b>Zip:&nbsp;</b></td>
                                          <td>
                                              <asp:TextBox runat="server" id="popShopAddressZip" width="45" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopAddressZip")%>' />
                                          </td>
                                          <td align="right"><b>MSA Code:&nbsp;</b></td>
                                        </tr>
                                        <tr>
                                          <td align="right"><b>Phone:&nbsp;</b></td>
                                          <td colspan="5">
                                              <asp:TextBox runat="server" id="popShopPhone" width="100" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopPhone")%>' />
                                          </td>
                                          <td align="right"><b>Fax:&nbsp;</b></td>
                                          <td colspan="5">
                                              <asp:TextBox runat="server" id="popShopFax" width="100" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopFax")%>' />
                                          </td>
                                        </tr>
                                        <tr>
                                          <td colspan="6"></td>
                                        </tr>
                                        <tr>
                                          <td align="right"><b>Contact:&nbsp;</b></td>
                                          <td colspan="5">
                                              <asp:TextBox runat="server" id="popShopContact" width="250" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopContact")%>' />
                                          </td>
                                        </tr>
                                        <tr>
                                          <td align="right"><b>Phone:&nbsp;</b></td>
                                          <td colspan="5">
                                              <asp:TextBox runat="server" id="popShopContactPhone" width="100" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopContactPhone")%>' />
                                          </td>
                                        </tr>
                                        <tr>
                                          <td align="right"><b>Email:&nbsp;</b></td>
                                          <td colspan="5">
                                              <asp:TextBox runat="server" id="popShopContactEmail" width="200" Enabled="false" Text='<%#DataBinder.Eval(Container, "DataItem.ShopAssignmentData.AssignmentShopContactEmail")%>' />
                                          </td>
                                        </tr>
                                      </table>
                                    </asp:Panel>
                                    </div>


                                    <!-- Documents Tab in Program Shop Tab Start -->
                                    <div id="divDocuments_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%> " class="tabAssignmentcontent">

                                        <table style="height: 280px; width: 785px; border-color: #667; border-spacing: 0px;">
                                            <tr style="vertical-align: top;">
                                                <td>
                                                    <!-- Estimate Details -->
                                                    <div id="divEstimateScroll" style="width: 760px; height: 140px; overflow: auto scroll;">
                                                        <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                            <tr style="vertical-align: top;">
                                                                <!-- Top View Estimate -->
                                                                <td>
                                                                   <asp:Panel runat="server" Id="pnlEstimate" GroupingText="Estimates" Font-Bold="true" Width="670" Height="140" ScrollBars="Horizontal">
                                                                        <asp:GridView ID="gvEstimate" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
                                                                            <AlternatingRowStyle BackColor="#F7F7F7" />
                                                                            <Columns>
                                                                                <asp:BoundField DataField="EstRecvDate" HeaderText="Recd./Attach Date" SortExpression="EstRecvDate" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:ImageField DataImageUrlField="EstSummary" HeaderText="Summary"></asp:ImageField> 
                                                                                <asp:BoundField DataField="EstSeq" HeaderText="Seq." SortExpression="EstSeq" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="EstSource" HeaderText="Source" SortExpression="EstSource" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="EstGross" HeaderText="Gross $" SortExpression="EstGross" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="EstNet" HeaderText="Net $" SortExpression="EstNet" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="EstOrgAud" HeaderText="Org./Audited" SortExpression="EstOrgAud" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="EstDup" HeaderText="Dup." SortExpression="EstDup" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="EstAgreed" HeaderText="Agreed" SortExpression="EstAgreed" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Left" />
                                                                            </Columns>
                                                                            <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                                            <HeaderStyle BackColor="#629ad6" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                            <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                                                            <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                                                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                   <asp:Panel runat="server" Id="pnlDocuments" GroupingText="Thumbnail Images" Font-Bold="true" Width="670" Height="150" ScrollBars="Horizontal">
                                                                        <asp:Repeater ID="repDocuments" runat="server" EnableViewState="false" DataSource='<%# DataBinder.Eval(Container.DataItem, "Documents")%>' >
                                                                            <HeaderTemplate>
                                                                                <table class="topTable" cellpadding="0" cellspacing="0" border="2">
                                                                                <tr>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                    <td align="center" width="85">
                                                                                        <asp:Image ID="imgDocument" Title='<%# DataBinder.Eval(Container.DataItem, "ImageInfo")%>'  runat="server" Width="50" Height="50" BorderStyle="Solid" BorderWidth="1" ImageAlign="Middle" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "ImageLocationThumbnail") %>' />
                                                                                        <br />
                                                                                        <a href id="lnkDocView" onclick="displayImage('<%# DataBinder.Eval(Container.DataItem, "ImageLocationRaw") %>')">
                                                                                            <asp:Label ID="lblDocName" runat="server" Font-Size="X-Small"><b><%# DataBinder.Eval(Container.DataItem, "ImageInfo")%></b></asp:Label>
                                                                                        </a>
                                                                                    </td>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </tr>
                                                                                </table>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>

                                    <!-- Consessions Tab in Program Shop Tab Start -->
                                    <div id="divConcessions_<%#DataBinder.Eval(Container, "DataItem.Vehnumber")%> " class="tabAssignmentcontent">

                                        <table style="height: 275px; width: 785px; border-color: #667; border-spacing: 0px;">
                                            <tr style="vertical-align: top;">
                                                <td>
                                                    <!-- Concession Details -->
                                                    <div id="divConcessionScroll" style="width: 760px; height: 140px; overflow: auto scroll;">
                                                        <table class="topTable" style="border-spacing: 0px;" cellpadding="0" cellspacing="0" border="0">
                                                            <tr style="vertical-align: top;">
                                                                <!-- Top Full Screen -->
                                                                <td>
                                                                    <asp:GridView ID="gvConcessions" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal"  Visible="true">
                                                                        <AlternatingRowStyle BackColor="#F7F7F7" />
                                                                        <Columns>
                                                                            <asp:BoundField DataField="ConcLastUpdated" HeaderText="Last Updated" SortExpression="ConcLastUpdated" HeaderStyle-Width="400px" HeaderStyle-HorizontalAlign="Left" />
                                                                            <asp:BoundField DataField="ConcType" HeaderText="Type" SortExpression="ConcType" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                                            <asp:BoundField DataField="ConcReason" HeaderText="Reason" SortExpression="ConcReason" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                                            <asp:BoundField DataField="ConcAmount" HeaderText="Amount" SortExpression="ConcAmount" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left" />
                                                                        </Columns>
                                                                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                                        <HeaderStyle BackColor="#629ad6" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                                                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                                                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>


                                </div>

                            </div>
                        </ItemTemplate>
                        <FooterTemplate></FooterTemplate>

                    </asp:Repeater>


                    <!-- Vehicle Details repeater starts here -->


                    <!-- VEHICLE SECTION ENDED HERE -->

                </td>
            </tr>
        </table>

    </form>
</body>
</html>
