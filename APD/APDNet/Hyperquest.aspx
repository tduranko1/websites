﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Hyperquest.aspx.vb" Inherits="APDNet.Hyperquest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hyperquest Document Send</title>
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />

    <script language="JavaScript">
        function closeWindow() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="frmHyperquest" runat="server">

    <!-- HQ Title -->
    <div id="divHQTitle" runat="server">
        <!-- Claim Titlebar -->
        <table cellpadding="0" cellspacing="0" width="350" border="0">
            <tr>
                <td align="left" valign="top" width="8" style="background-image:url(images/PanelLeft.png); background-repeat:no-repeat;">
                </td>
                <td align="left" class="Header"style="background-image:url(images/PanelMiddle.png); background-repeat:repeat;">
                    <a name="ClaimInfo" style="color: White; font-weight: bold;" >Hyperquest Send:</a>
                </td>
                <td align="left" valign="top" width="8" style="background-image:url(images/PanelRight.png); background-repeat:no-repeat;">
                </td>
            </tr>
        </table>
    </div>
    <table cellpadding="0" cellspacing="0" width="350" border="1">
        <tr>
            <td align="center">
                <div>
                    <h3>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </h3>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btnClose" runat="server" Text="Close" OnClientClick="javascript:closeWindow(); return false;" /> 
            </td>
        </tr>
    </table>
    <!-- HQ Footer Bar -->
    <div id="divPreFooter" runat="server">
        <table cellpadding="0" cellspacing="0" width="350" border="0">
            <tr>
                <td align="left" valign="top" width="8" style="background-image:url(images/PanelBotLeft.png); background-repeat:no-repeat;">
                </td>
                <td align="left" class="Header"style="background-image:url(images/PanelMiddle.png); background-repeat:repeat;">
                    &nbsp;
                </td>
                <td align="left" valign="top" width="8" style="background-image:url(images/PanelBotRight.png); background-repeat:no-repeat;">
                </td>
            </tr>
        </table> 
    </div>
    </form>
</body>
</html>
