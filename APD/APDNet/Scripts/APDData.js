﻿//-------------------------------------//
// Web Service Call - Globals
//-------------------------------------//
var webserUrl = "http://localhost:51790/APDFoundationWrapper.asmx";

//-------------------------------------//
// Web Service Call - ExecuteSQL
//-------------------------------------//
$(document).ready(function () {
    WSExecuteSQL = function (storedProc, params) {
        if (!storedProc || storedProc === "") {
            alert("Internal Error. No storedProc Name specified.\\r\\n" + 'System Communications Error');
        }
        alert(params);
        var soapRequest = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> \
                                <soap:Body> \
                                    <ExecuteSQL xmlns="http://tempuri.org/"> \
                                    <strStoredProcedure>' + storedProc + ' 184</strStoredProcedure> \
                                    </ExecuteSQL> \
                                </soap:Body> \
                                </soap:Envelope>';


        alert(soapRequest);

        $.ajax({
            url: webserUrl,
            type: "POST",
            dataType: "xml",
            data: soapRequest,
            contentType: "text/xml; charset=\"utf-8\"",
            success: WSExecuteSQLSuccessOccur,
            error: WSExecuteSQLErrorOccur
        });
    };
});

function WSExecuteSQLSuccessOccur(data, status, req) {
    if (status == "WSExecuteSQLSuccessOccur")
        alert(req.responseText);
}

function WSExecuteSQLErrorOccur(data, status, req) {
    alert(req.responseText + "1111111 " + status + "2222");
}

//-------------------------------------//
// Web Service Call - ExecuteSpAsXML
//-------------------------------------//
var returnData;

$(document).ready(function () {
    WSExecuteSpAsXML = function (storedProc, params) {
        if (!storedProc || storedProc === "") {
            alert("Internal Error. No storedProc Name specified.\\r\\n" + 'System Communications Error');
        }
        //alert(params);
        var soapRequest = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> \
                                <soap:Body> \
                                    <ExecuteSpAsXML xmlns="http://tempuri.org/"> \
                                    <StoredProcedure>' + storedProc + ' </StoredProcedure> \
                                    <Parameters>' + params + ' </Parameters> \
                                    </ExecuteSpAsXML> \
                                </soap:Body> \
                                </soap:Envelope>';
        //alert(soapRequest);

        $.ajax({
            url: webserUrl,
            type: "POST",
            dataType: "xml",
            cache: false,
            data: soapRequest,
            async: false,
            contentType: "text/xml; charset=\"utf-8\"",
            success: SuccessOccur, 
            error: ErrorOccur 
        });

        return returnData;
    };
});

function SuccessOccur(data, status, req) {
    returnData = data;
    //alert("Success");
}

function ErrorOccur(data, status, req) {
    alert("failed");
    //alert(req.responseText + "1111111 " + status + "2222");
}