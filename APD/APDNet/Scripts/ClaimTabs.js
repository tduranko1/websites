﻿//-------------------------------------------//
// Handle the screen Div changes each time
// the tabs are clicked
//-------------------------------------------//

//-- Globals -->
var oWindowHandler

function MenuTab(evt, currentClass, tabName, tabContainer, subTabContainer, subTab) {
    var ct;

    //debugger;
    ////var tablinks = getElementsByClassName(currentClass);
    var tablinks = $("." + currentClass);
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    if (tabContainer) {
        //// tabcontent = getElementsByClassName(tabContainer);
        tabcontent = $("." + tabContainer);
        for (ct = 0; ct < tabcontent.length; ct++) {
            tabcontent[ct].style.display = "none";
        }
    }
    evt.srcElement.className += " active";

    ////document.getElementById(tabName).style.display = "block";
    $("#" + tabName).removeAttr("style");
    $("#" + tabName).attr("style", "display: block;");

    if (subTab) {

        ////var subTabcontent = getElementsByClassName(subTab);
        var subTabcontent = $("." + subTab);
        for (ct = 0; ct < subTabcontent.length; ct++) {
            if (subTabcontent[ct].style.display === 'block') {
                subTabcontent[ct].style.display = 'none';
            }
        }
    }
    if (subTabContainer) {
        ////document.getElementById(subTabContainer).style.display = "block";
        $("#" + subTabContainer).css("display", "block");

        //// var vehDetails = document.getElementById(subTabContainer).parentNode.children[0];
        var vehDetails = $("#" + subTabContainer).parent().children(":first-child");
        if (vehDetails.length > 0) {
            for (ct = 0; ct < vehDetails.children.length; ct++) {
                vehDetails.children(0)[ct].className = vehDetails.children(0)[ct].className.replace(" active", "");
            }
            vehDetails.children(0)[0].className += " active";
        }
    }
}

//-----------------------------------//
//Coverage popup for ADD/Edit and delete
//-----------------------------------//
//$(function () {
//    $("#diaAddCoverage").dialog({
//        autoOpen: false,
//        modal: true,
//        appendTo: "form",
//        show: {
//            effect: "blind",
//            duration: 1000
//        },
//        hide: {
//            effect: "explode",
//            duration: 1000
//        }
//    });

//    $("#divRentalMax").hide();
//    $("#divRentalLimit").hide();

//    $("#btnAddCoverage").on("click", function () {
//        $("#diaAddCoverage").dialog("open");
//    });

//    $("#btnCoverageAddCancel").on("click", function () {
//        $("#diaAddCoverage").dialog("close");
//    });
//});

//-----------------------------------//
// Del Coverage 
//-----------------------------------//
//$(function () {
//    $("#diaDelCoverage").dialog({
//        autoOpen: false,
//        modal: true,
//        appendTo: "form",
//        show: {
//            effect: "blind",
//            duration: 1000
//        },
//        hide: {
//            effect: "explode",
//            duration: 1000
//        }
//    });

//    $("#btnDelCoverage").on("click", function () {
//        $("#diaDelCoverage").dialog("open");
//    });

//    $("#btnCoverageDelCancel").on("click", function () {
//        $("#diaDelCoverage").dialog("close");
//    });
//});

//Edit coverage
//$(function () {
//    $("#diaEditCoverage").dialog({
//        autoOpen: false,
//        modal: true,
//        appendTo: "form",
//        show: {
//            effect: "blind",
//            duration: 1000
//        },
//        hide: {
//            effect: "explode",
//            duration: 1000
//        }
//    });

//    $("#btnSavCoverage").on("click", function () {
//        $("#diaEditCoverage").dialog("open");
//    });

//    $("#btnEditCoverageCancel").on("click", function () {
//        $("#diaEditCoverage").dialog("close");
//    });
//});

//-----------------------------------//
// Add Coverage Description Changed
//-----------------------------------//
//$(function () {
//    $("#ddlCoverageDescription").change(function () {
//        var aCoverageClientCode = $("#ddlCoverageDescription").val().toUpperCase().split("|");
//        $("#HIDCoverageParams").val($("#ddlCoverageDescription").val());
//        $("#txtCoverageClientCode").val(aCoverageClientCode[0]).val().toUpperCase();
//        $("#txtCoverageType").val(aCoverageClientCode[0]).val().toUpperCase();
//        //$("#HIDCoverageTypeID").val(aCoverageClientCode[2]).val();

//        //alert(aCoverageClientCode[0]);
//        //alert(aCoverageClientCode[1]);
//        //alert(aCoverageClientCode[2]);
//        //alert(aCoverageClientCode[3]);

//        //-- Additional Flag --//
//        switch (aCoverageClientCode[3]) {
//            case "0":
//                $("#txtCoverageAdditionalFlag").val("No");
//                break;
//            case "1":
//                $("#txtCoverageAdditionalFlag").val("Yes");
//                break;
//            default:
//                $("#txtCoverageAdditionalFlag").val("No");
//        }

//        //-- Rental Area --//
//        if (aCoverageClientCode[0] == 'RENT') {
//            $("#divRentalMax").show();
//            $("#divRentalLimit").show();
//        } else {
//            $("#divRentalMax").hide();
//            $("#divRentalLimit").hide();
//        }

//    })
//})

//-----------------------------------//
// Add Coverage Description Changed
//-----------------------------------//
$(function () {
    $("#ddlCarrierOffice").change(function () {

        //-- Carrier Office Changed - Get Users --//
        //alert($("#ddlCarrierOffice option:selected").text());
        //alert($("#ddlCarrierOffice option:selected").val());
        //alert($("#hidInsuranceCompanyID").val());

        //-- Clear Carrier Rep Users Dropdown --//
        $('#ddlCarrierRep').empty();

        //-- Call Web Service and get Carrier Rep Users --//
        var params = "@InsuranceCompanyID=" + $("#hidInsuranceCompanyID").val() + ", @OfficeID=" + $("#ddlCarrierOffice option:selected").val();
        var xmlReturnData = WSExecuteSpAsXML("uspClaimCarrierRepGetListByOfficeWSXML", params);

        //alert((new XMLSerializer()).serializeToString(xmlReturnData));
        //alert($(xmlReturnData).find('OfficeUser').attr('NameLast'));
        
        $(xmlReturnData).find('OfficeUser').each(function () {
                var attribData = $(this);
                var carrierRepName = attribData.attr('NameLast') + ", " + attribData.attr('NameFirst');
                $('#ddlCarrierRep').append($("<option     />").val(attribData.attr('UserID')).text(carrierRepName));
        })
    })
})

//-----------------------------------//
// CarrierRep dialog
//-----------------------------------//
$(function () {
    $("#diaCarrierRep").dialog({
        autoOpen: false,
        modal: true,
        appendTo: "form",
        width: 370,
        height: 100,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

    $("#btnEditCarrier").on("click", function () {
        $("#diaCarrierRep").dialog("open");
    });

    $("#btnCarrierRepCancel").on("click", function () {
        $("#diaCarrierRep").dialog("close");
    });

});

//-----------------------------------//
// New Service Channel - dialog
//-----------------------------------//
$(function () {
    $("#diaNewServiceChannel").dialog({
        autoOpen: false,
        modal: true,
        appendTo: "form",
        width: 370,
        height: 100,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

    $("#btnNewServiceChannel").on("click", function () {
        $("#diaNewServiceChannel").dialog("open");
    });

    $("#btnCarrierRepCancel").on("click", function () {
        $("#diaNewServiceChannel").dialog("close");
    });

});

//-----------------------------------//
// NADA - dialog
//-----------------------------------//
//$(function () {
//    $("#diaNADA").dialog({
//        autoOpen: false,
//        modal: true,
//        appendTo: "form",
//        width: 840,
//        height: 460,
//        show: {
//            effect: "blind",
//            duration: 1000
//        },
//        hide: {
//            effect: "explode",
//            duration: 1000
//        }
//    });

//    $("#imgNADA").on("click", function () {
//        $("#diaNADA").dialog("open");
//    });

//    $("#imgNADACancel").on("click", function () {
//        $("#diaNADA").dialog("close");
//    });
//});

//-----------------------------------//
// Concession - dialog
//-----------------------------------//
$(function () {
    $("#diaAddConcession").dialog({
        autoOpen: false,
        modal: true,
        appendTo: "form",
        width: 420,
        height: 230,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

    $("#btnAddConcession").on("click", function () {
        $("#diaAddConcession").dialog("open");
    });
});

$(function () {
    $("#diaEditConcession").dialog({
        autoOpen: false,
        modal: true,
        appendTo: "form",
        width: 420,
        height: 230,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

    $("#btnEditConcession").on("click", function () {
        $("#diaEditConcession").dialog("open");
    });
});

$(function () {
    $("#diaDelConcession").dialog({
        autoOpen: false,
        modal: true,
        appendTo: "form",
        width: 400,
        height: 100,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

    $("#btnDelConcession").on("click", function () {
        $("#diaDelConcession").dialog("open");
    });
});

//-----------------------------------//
// Modal Block - dialog
//-----------------------------------//
$(function () {
    $("#diaModal").dialog({
        autoOpen: false,
        modal: true,
        appendTo: "form",
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

    //$("#diaModal").focus(function () {
    //    alert("here");
    //    $("#diaModal").dialog("close");
    //});

    $("#btnLaunch").on("click", function () {
        $("#diaModal").dialog("open");
        oWindowHandler = window.open("AddCoverage.aspx?WindowID=45", "", "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=750px,height=480px,left = 490,top=300,center=yes;");

        //alert("Launched...");

        setTimeout(function () {
            if (oWindowHandler && !oWindowHandler.closed) {  //checks to see if window is open
                //alert("Opened");
                //winPop.focus();
            } else {
                //alert("Closed");
                $("#diaModal").dialog("close");
                setInterval(setTimeout, 3000);
            }
          }, 3000
        );


        return false;
    });

    //$("#chkDialogWindow").on("click", function () {
        //alert("incode");

        //if (oWindowHandler && !oWindowHandler.closed) {  //checks to see if window is open
        //    alert("Opened");
        //    //winPop.focus();
        //} else {
        //    alert("Closed");
        //}
    //});

});

//-----------------------------------//
// Dialog Window 
//-----------------------------------//
//function OpenDialogWindow(strWebPageWinType, strPage, strParams, bEnabled, strScreen) {
function OpenDialogWindow(strWebPageWinType, strPage, strParams) {
    //alert(strWebPageWinType);
    //alert(strPage);
    //alert(strParams);
    strRetVal = window.open(strWebPageWinType, strPage, strParams);
    LoadModalDiv();

    // strRetVal = window.showModalDialog("VehicleEvaluator.asp?WindowID=" + gsWindowID + myreq, "", "dialogHeight:480px; dialogWidth:750px; resizable:no; status:no; help:no; center:yes;");
    if (strRetVal == null || strRetVal == 'cancel')
        return false;

    //window.location = 'ClaimXP.aspx';
}

//Javascript dialog for NADA button added below code 
// NADA module popup code

var strRetVal;

function callNada(strVin, strMake, strModel, strVehicleYear, strBodyStyle, strMileage, sZIP) {
    ////  if (gsVehicleCRUD.indexOf("U") == -1 || gsVehicleCRUD.indexOf("R") == -1) return;
    var myreq = "";
    var strNada = "";
    var arrRetVal;
    var arrNameValue;
    var control;
    myreq = "&strVIN=" + strVin;
    if (strNada != "null")
        myreq += "&strVehId=" + strNada +
                  "&strMake=" + strMake +
                  "&strModel=" + strModel +
                  "&strYear=" + strVehicleYear +
                  "&strBody=" + strBodyStyle +
                  "&txtMileage=" + strMileage +
                  "&strReadOnly=" + 0 +
                  "&strZip=" + sZIP;

    // The object "myObject" is sent to the modal window.
    strRetVal = window.open("VehicleEvaluator.asp?WindowID=" + 0 + myreq, "", "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=750px,height=480px,left = 490,top=300,center=yes;");
    LoadModalDiv();

    // strRetVal = window.showModalDialog("VehicleEvaluator.asp?WindowID=" + gsWindowID + myreq, "", "dialogHeight:480px; dialogWidth:750px; resizable:no; status:no; help:no; center:yes;");
    if (strRetVal == null || strRetVal == 'cancel')
        return false;

    arrRetVal = strRetVal.split('||');
    var iLength = arrRetVal.length;
    for (var j = 0; j < iLength; j++) {
        arrNameValue = arrRetVal[j].split('--');

        switch (arrNameValue[0]) {
            case 'year':
                VehicleYear.value = arrNameValue[1];
                break;
            case 'make':
                arrNameValue[1] = arrNameValue[1].substr(0, 15);  // 15 character limit imposed by database
                Make.value = arrNameValue[1];
                break;
            case 'model':
                arrNameValue[1] = arrNameValue[1].substr(0, 15);
                Model.value = arrNameValue[1];
                break;
            case 'body':
                arrNameValue[1] = arrNameValue[1].substr(0, 15);
                BodyStyle.value = arrNameValue[1];
                break;
            case 'vin':
                if (arrNameValue[1] != "")
                    VIN.value = arrNameValue[1];
                break;
            case 'value':
                BookValueAmt.value = arrNameValue[1].substr(1, arrNameValue[1].length);
                break;
            case 'miles':
                Mileage.value = arrNameValue[1];
                break;
        }
    }
}

//Load Parent window div for gray out
function LoadModalDiv() {
    var bcgDiv = document.getElementById("divBackground");
    bcgDiv.style.display = "block";
}

//Unload Popup 
function HideModalDiv() {
    var bcgDiv = document.getElementById("divBackground");
    bcgDiv.style.display = "none";
}
function OnUnload() {
    if (false == strRetVal.closed) {
        strRetVal.close();

    }
}
//window.onunload = OnUnload;







/*
 //OLD JS without JQuery

 //-------------------------------------------//
 // Get all the elements that have a class 
 // of Claim and Vehicle
 //-------------------------------------------//
 function getElementsByClassName(className) {
     var found = [];
     var elements = document.getElementsByTagName("*");
     for (var i = 0; i < elements.length; i++) {
         var names = elements[i].className.split(' ');
         for (var j = 0; j < names.length; j++) {
             if (names[j] == className) found.push(elements[i]);
         }
     }
     return found;
 }

 //-------------------------------------------//
 // Handle the screen Div changes each time
 // the tabs are clicked
 //-------------------------------------------//
 function MenuTab(evt, currentClass, tabName, tabContainer, subTabContainer, subTab) {
     var ct;

     debugger;
     var tablinks = getElementsByClassName(currentClass);
     for (i = 0; i < tablinks.length; i++) {
         tablinks[i].className = tablinks[i].className.replace(" active", "");
     }

     if (tabContainer) {
         tabcontent = getElementsByClassName(tabContainer);
         for (ct = 0; ct < tabcontent.length; ct++) {
             tabcontent[ct].style.display = "none";
         }
     }
     evt.srcElement.className += " active";

     document.getElementById(tabName).style.display = "block";
      if (subTab) {

         var subTabcontent = getElementsByClassName(subTab);

         for (ct = 0; ct < subTabcontent.length; ct++) {
             if (subTabcontent[ct].style.display === 'block') {
                 subTabcontent[ct].style.display = 'none';
             }
         }
     }
     if (subTabContainer) {
         document.getElementById(subTabContainer).style.display = "block";
         var vehDetails = document.getElementById(subTabContainer).parentNode.children[0];
         for (ct = 0; ct < vehDetails.children.length; ct++) {
             vehDetails.children[ct].className = vehDetails.children[ct].className.replace(" active", "");
         }
         vehDetails.children[0].className += " active";
     }
 }
*/