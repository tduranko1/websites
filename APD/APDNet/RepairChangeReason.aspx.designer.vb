﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class RepairChangeReason
    
    '''<summary>
    '''frmRepairChangeReason control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents frmRepairChangeReason As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''lblLynxID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLynxID As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlRepairReason control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRepairReason As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''txtOther control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOther As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''btnReasonOk control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnReasonOk As Global.System.Web.UI.WebControls.Button
End Class
