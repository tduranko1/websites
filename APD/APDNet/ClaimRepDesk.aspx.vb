﻿'--------------------------------------------------------------
' Program: ClaimRepDesk
' Author:  Mahes
' Date:    Oct 03, 2013
' Version: 1.0
'
' Description:  This site provides the ClaimResDesk for APD replacing the 
'               existing COM based classic asp functions. 
'--------------------------------------------------------------
Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Imports System.Web.Caching
Imports System.Web.Caching.Cache


Public Class ClaimRepDesk
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService

    '--------------------------------------------------------------
    ' Page Load
    '--------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '-------------------------------
        ' Globals
        '-------------------------------
        Dim sLynxID As String = ""
        Dim sUserID As String = ""
        Dim sShowAll As String = ""
        Dim sWindowState As String = ""
        Dim sInsuranceCompanyID As String = ""
        Dim sParams As String = ""
        Dim sASPParam As Object
        Dim oSession As Object
        Dim sXslHash As New Hashtable
        Dim XMLCRUD As XmlElement
        Dim sVehClaimAspectID = ""
        Dim sVehCount = ""
        Dim sNoteCRUD = ""
        Dim sTaskCRUD = ""
        Dim uspXMLRaw As XmlElement
        Dim uspXMLXDoc As New XmlDocument
        Dim sAPDNetDebugging As String = ""
        Dim dtStart As Date
        Dim APDDBUtils As APDDBUtilities = Nothing


        'Dim ctx As HttpContext = HttpContext.Current
        'Dim sData As String = ctx.Cache.Get("xslPage")

        'If sData <> "" Then
        '    Response.Write("Got Data")
        'Else
        '    Response.Write("NO DATA")
        '    ctx.Cache.Add("xslPage", "SOMEDATA", New System.Web.Caching.CacheDependency(sData), System.Web.Caching.Cache.NoAbsoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, Nothing)
        'End If

        'Response.End()

        '-------------------------------
        ' Session/Local Variables
        '-------------------------------
        Dim sStoredProcedure As String = "uspAPDRAAUserGetListWSXML"
        Dim sXSLProcedure As String = "ClaimRepDesk.xsl"

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        Dim sSessionKey = Request.QueryString("SessionKey")
        Dim sWindowID = Request.QueryString("WindowID")
        sUserID = Request.QueryString("UserID")

        'sASPParams = "showAll:|ShowAllTasks:|winState:"
        'sSessionKey = "{4730C0D9-75F6-4C92-8DE0-C2A51157FD6D}"
        'sSessionKey = "{D1B5711E-813F-4198-82D0-26BEDDFB257B}"
        'sSessionKey = "{7E8F1898-73A8-4FEA-A570-F9DEB880CA74}"
        'sSessionKey = "{7E8F1898-73A8-4FEA-A570-F9DEB880CA74}"
        'sWindowID = 1

        'Dim APDDBUtils As New APDDBUtilities

        Try
            If sSessionKey = "" Then
                Throw New SystemException("SessionValidationError: No session information received from classic ASP page.")
            Else
                '---------------------------------
                ' New Session Data
                '---------------------------------
                'oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "LynxID-" & sWindowID)
                'sLynxID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "UserID-" & sWindowID)
                sUserID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "APDNetDebugging-" & sWindowID)
                sAPDNetDebugging = ""
                If UCase(oSession(0).SessionValue) = "TRUE" Or UCase(AppSettings("Debug")) = "TRUE" Then
                    sAPDNetDebugging = "TRUE"
                End If

                APDDBUtils = New APDDBUtilities()
                dtStart = Date.Now

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "ClaimRepDesk.aspx - Start: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", ASPParam = " & sUserID)
                End If

                '---------------------------------
                ' Get configuration data
                '---------------------------------
                'XMLReturnElem = wsAPDFoundation.GetConfigXMLData("Root/Application/WorkFlow/EventCodes/Code[@name='VehicleAssignmentSentToShop']")
                'If XMLReturnElem.InnerXml.Contains("ERROR:") Then
                ' Throw New SystemException("ConfigXMLError: Could not find the VehicleAssignmentSentToShop configuration variable.")
                'Else
                'sEvtVeicleAssignmentSentToShop = XMLReturnElem.InnerText
                'End If

                '---------------------------------
                ' Get the session data
                '---------------------------------
                If Not Request("UserID") Then
                    'sUserID = APDDBUtils.UserID
                Else
                    sUserID = Request("UserID")
                End If

                'If Not Request("LynxID") Then
                '    'sLynxID = APDDBUtils.LynxID
                'Else
                '    sLynxID = Request("LynxID")
                'End If

                'oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "InsuranceCompanyID-" & sWindowID)
                'sInsuranceCompanyID = oSession(0).SessionValue

                'oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "VehClaimAspectID-" & sWindowID)
                'sVehClaimAspectID = oSession(0).SessionValue

                'oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "VehCount-" & sWindowID)
                'sVehCount = oSession(0).SessionValue

                'sWindowState = Request("winState")
                'sShowAll = Request("showAll")
                'If sShowAll = "true" Or sLynxID = "" Then sLynxID = "0"

                '---------------------------------
                ' Get the permission data
                '---------------------------------
                'XMLCRUD = wsAPDFoundation.ExecuteSpAsXML("uspCRUDGetDetailWSXML", "Note, " & sUserID)
                'sNoteCRUD = APDDBUtils.GetCrud(XMLCRUD)

                'XMLCRUD.InnerXml = ""
                'XMLCRUD = wsAPDFoundation.ExecuteSpAsXML("uspCRUDGetDetailWSXML", "Task, " & sUserID)
                'sTaskCRUD = APDDBUtils.GetCrud(XMLCRUD)

                '-----------------------------------
                ' Add config variables as XslParams
                '-----------------------------------
                sXslHash.Add("UserID", sUserID)
                sXslHash.Add("WindowID", sWindowID)

                '------------------------------------
                ' If ASPParms, parse and add to hash
                '------------------------------------
                'If sASPParams <> "" Then
                '    Dim aParams As Array
                '    aParams = Split(sASPParams, "|")
                '    For Each sASPParam In aParams
                '        Dim aValues As Array
                '        aValues = Split(sASPParam, ":")
                '        sXslHash.Add(aValues(0), aValues(1))
                '    Next
                'End If

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls/Parameters." _
                                             , "Session controls for Window: " & sWindowID _
                                             & " and SessionKey: " & sSessionKey _
                                             , "Vars: StoredProc = " & sStoredProcedure _
                                             & ", XSLPage = " & sXSLProcedure _
                                             & ", UserID(Session) = " & sUserID _
                                             )
                End If

                '---------------------------------
                ' XSL Transformation
                '---------------------------------
                sParams = sUserID

                '---------------------------------
                ' StoredProc XML Data
                '---------------------------------
                uspXMLRaw = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)
                If uspXMLRaw.OuterXml = Nothing Then
                    ' Throw exception
                Else
                    uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)
                    sXslHash.Add("RunTime", Now.Subtract(dtStart).Milliseconds & " msec")

                    Response.Write(APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sParams, sXslHash))
                End If

                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "ClaimRepDesk.aspx - Finished: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
                End If

                '-----------------------------------
                ' Clean-up the mess
                '-----------------------------------
                'APDDBUtils = Nothing
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls/parameter not found or page pre-transformation failed.", "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID & ", ASPParam = " & sUserID)

            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        Finally
            '-----------------------------------
            ' Clean-up the mess
            '-----------------------------------
            wsAPDFoundation.Dispose()
            APDDBUtils = Nothing
            sASPParam = Nothing
            oSession = Nothing
            sXslHash = Nothing
            XMLCRUD = Nothing
            uspXMLRaw = Nothing
            uspXMLXDoc = Nothing
            'APDDBUtils = Nothing
            GC.Collect()
            GC.SuppressFinalize(Me)
        End Try


    End Sub

End Class