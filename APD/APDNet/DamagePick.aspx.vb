﻿Imports System.Xml

Public Class DamagePick
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    Dim sCRUD As String = ""
    Dim iLynxID As Integer = 0
    Dim iInsuranceCompanyID As Integer = 0
    Dim iSelectedCoverageTypeID As Integer
    Dim sUserID As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim oReturnXML As XmlElement = Nothing
        'Dim oXMLNode As XmlNode = Nothing
        'Dim oXMLNodeList As XmlNodeList = Nothing
        'Dim iClientCoverageType As Integer = 0
        'Dim DTCoverage As New DataTable
        'Dim DCCoverage As New DataColumn
        'Dim sDDLParams As String = ""

        'Try
        '    '-------------------------------
        '    ' Check Session Data
        '    '-------------------------------
        '    iLynxID = Request.QueryString("LynxID")
        '    iInsuranceCompanyID = Request.QueryString("InscCompID")
        '    sUserID = Request.QueryString("UserID")

        '    'txtLynxID.Text = CStr(iLynxID)

        '    '-------------------------------
        '    ' Process Validation
        '    '-------------------------------
        '    '??? Verify we got good params

        '    '-------------------------------
        '    ' Apply CRUD
        '    '-------------------------------

        '    '-------------------------------
        '    ' Process Main Code
        '    '-------------------------------
        '    '-------------------------------
        '    ' Database access
        '    '-------------------------------  
        '    Dim sStoredProcedure As String = "uspClaimCondGetDetailWSXML"
        '    Dim sParams As String = iLynxID & "," & iInsuranceCompanyID

        '    '-------------------------------
        '    ' Call WS and get data
        '    '-------------------------------
        '    oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

        '    '---------------------------------------
        '    ' Load Current Coverages
        '    '---------------------------------------
        '    oXMLNodeList = oReturnXML.SelectNodes("Claim/Coverage")

        '    DTCoverage.Columns.Add(New DataColumn("Description", GetType(String)))
        '    DTCoverage.Columns.Add(New DataColumn("CoverageTypeCD", GetType(String)))

        '    For Each oXMLNode In oXMLNodeList
        '        Dim DRCoverage As DataRow = DTCoverage.NewRow
        '        DRCoverage("Description") = oXMLNode.Attributes("CoverageTypeCD").InnerText & " - " & oXMLNode.Attributes("Description").InnerText
        '        DRCoverage("CoverageTypeCD") = oXMLNode.Attributes("CoverageTypeCD").InnerText
        '        DTCoverage.Rows.Add(DRCoverage)
        '    Next

        '    '-------------------------------
        '    ' Load Reference data
        '    '-------------------------------
        '    oXMLNodeList = oReturnXML.SelectNodes("Reference")
        '    iClientCoverageType = 0

        '    '--------------------------------
        '    ' Populate Coverage Add/Edit
        '    ' Description Dropdown
        '    '--------------------------------
        '    For Each oXMLNode In oXMLNodeList
        '        '---------------------------------------
        '        ' Load Reference data - AssignmentType
        '        '---------------------------------------
        '        If iClientCoverageType = 0 Then
        '            ddlCoverageDescription.Items.Add("")
        '            iClientCoverageType += 1
        '        End If

        '        If oXMLNode.Attributes("List").InnerText = "ClientCoverageType" Then
        '            If DTCoverage.Select("Description='" & oXMLNode.Attributes("ReferenceID").Value & " - " & oXMLNode.Attributes("Name").Value & "'").Length = 0 Then
        '                sDDLParams = oXMLNode.Attributes("ReferenceID").Value & "|" & oXMLNode.Attributes("Name").Value & "|" & oXMLNode.Attributes("ClientCoverageTypeID").Value & "|" & oXMLNode.Attributes("AddtlCoverageFlag").Value & "|" & oXMLNode.Attributes("AddtlCoverageFlag").Value
        '                ddlCoverageDescription.Items.Insert(iClientCoverageType, New ListItem(oXMLNode.Attributes("ReferenceID").Value & " - " & oXMLNode.Attributes("Name").InnerText, sDDLParams))
        '                iClientCoverageType += 1
        '            End If
        '        End If
        '    Next

        '    '-------------------------------
        '    ' Clean-up
        '    '-------------------------------
        'Catch oExcept As Exception
        '    '---------------------------------
        '    ' Error handler and notifications
        '    '---------------------------------
        '    Dim sError As String = ""
        '    Dim sBody As String = ""
        '    Dim FunctionName As New System.Diagnostics.StackFrame

        '    sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
        '    wsAPDFoundation.LogEvent("APDCoverage", "ERROR", "Coverage data not found or data error occured.", oExcept.Message, sError)

        '    Response.Write(oExcept.ToString)
        '    'Return ""
        'Finally
        '    'oXMLNode = Nothing
        '    'oReturnXML = Nothing
        '    'oXMLNodeList = Nothing
        'End Try
    End Sub

    'Protected Sub btnNewServiceChannel_ADD_Click(sender As Object, e As EventArgs) Handles btnNewServiceChannel_ADD.Click
    '    Dim oReturnXML As XmlElement = Nothing
    '    Dim oXMLNode As XmlNode = Nothing
    '    Dim sLimitDailyAmt As String = 0
    '    Dim sMaximumDays As String = 0
    '    Dim aCoverageParams As Array = Nothing
    '    Dim sClaimCoverageID As String = ""
    '    Dim iAdditionalCoverageFlag As Integer = 0

    '    Try
    '        '-------------------------------
    '        ' Validate the web page data
    '        '-------------------------------  
    '        '??????????? CODE THIS ????????????????????
    '        'lblClaimSaved.Text = ""

    '        '-------------------------------
    '        ' Process DB Call
    '        '-------------------------------  
    '        Dim sStoredProcedure As String = "uspClaimCoverageUpdDetailWSXML"

    '        sClaimCoverageID = "0"
    '        If UCase(txtCoverageAdditionalFlag.Text) = "YES" Then
    '            iAdditionalCoverageFlag = 1
    '        Else
    '            iAdditionalCoverageFlag = 0
    '        End If


    '        Dim sParams As String = "@ClaimCoverageID = " & sClaimCoverageID _
    '            & ", @ClientCoverageTypeID = " & CStr(iSelectedCoverageTypeID) _
    '            & ", @LynxID = " & CStr(iLynxID) _
    '            & ", @InsuranceCompanyID = " & CStr(iInsuranceCompanyID) _
    '            & ", @AddtlCoverageFlag = " & iAdditionalCoverageFlag _
    '            & ", @CoverageTypeCD = '" & txtCoverageClientCode.Text & "'" _
    '            & ", @Description = '" & ddlCoverageDescription.SelectedItem.Text & "'" _
    '            & ", @DeductibleAmt = " & txtCoverageDeductible.Text _
    '            & ", @LimitAmt = " & txtCoverageLimit.Text _
    '            & ", @LimitDailyAmt = " & txtCoverageDailyLimit.Text _
    '            & ", @MaximumDays = " & txtCoverageMaxDays.Text _
    '            & ", @UserID = " & sUserID _
    '            & ", @SysLastUpdatedDate = " & "''"

    '        '?????? Code This ?????
    '        '    oNode.setAttribute("AddlCoverageFlagInd", (txtAdditionalFlag.value == "Yes" ? "/images/tick.gif" : "/images/spacer.gif"));

    '        'Debug.Print(sParams)

    '        '-------------------------------
    '        ' Call WS and get data
    '        '-------------------------------
    '        oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

    '        '???? check for success and save new Coverage syslastupdateddate ????

    '        'response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)

    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "myCloseScript", "window.opener.location.reload(true); window.close();", True)
    '    Catch oExcept As Exception
    '        '---------------------------------
    '        ' Error handler and notifications
    '        '---------------------------------
    '        Dim sError As String = ""
    '        Dim sBody As String = ""
    '        Dim FunctionName As New System.Diagnostics.StackFrame

    '        sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
    '        wsAPDFoundation.LogEvent("CoverageAddSaveDetails", "ERROR", "Add Coverage Save not processed or data error occured.", oExcept.Message, sError)

    '        Response.Write(oExcept.ToString)

    '    Finally
    '        oXMLNode = Nothing
    '    End Try
    'End Sub

    'Protected Sub ddlCoverageDescription_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCoverageDescription.SelectedIndexChanged
    '    Dim aCoverageData As Array = Nothing
    '    Dim iCoverageTypeID As Integer = 0

    '    aCoverageData = ddlCoverageDescription.SelectedItem.Value.Split("|")
    '    txtCoverageClientCode.Text = aCoverageData(0)
    '    txtCoverageType.Text = aCoverageData(1)
    '    iCoverageTypeID = CInt(aCoverageData(2))
    '    txtCoverageAdditionalFlag.Text = aCoverageData(3)
    '    txtCoverageDailyLimit.Text = 0
    '    txtCoverageDeductible.Text = 0
    '    txtCoverageLimit.Text = 0
    '    txtCoverageMaxDays.Text = 0
    '    iSelectedCoverageTypeID = iCoverageTypeID

    '    '-------------------------
    '    ' Additional Flag 
    '    '-------------------------
    '    Select Case txtCoverageAdditionalFlag.Text
    '        Case "0"
    '            txtCoverageAdditionalFlag.Text = "No"
    '        Case "1"
    '            txtCoverageAdditionalFlag.Text = "Yes"
    '    End Select

    '    '-------------------------
    '    ' Rental 
    '    '-------------------------
    '    If UCase(txtCoverageClientCode.Text) = "RENT" Then
    '        lblRentalMax.Visible = True
    '        lblRentalLimit.Visible = True
    '        txtCoverageMaxDays.Visible = True
    '        txtCoverageDailyLimit.Visible = True
    '    Else
    '        lblRentalMax.Visible = False
    '        lblRentalLimit.Visible = False
    '        txtCoverageMaxDays.Visible = False
    '        txtCoverageDailyLimit.Visible = False
    '    End If
    'End Sub

End Class