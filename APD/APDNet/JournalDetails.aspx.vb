﻿'--------------------------------------------------------------
' Program: APD Invoice Detail
' Author:  Thomas Duranko
' Date:    Sept 24, 2013
' Version: 1.0
'
' Description:  This site provides functions for APD replacing the 
'               existing COM based classic asp functions. 
'--------------------------------------------------------------
Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Public Class _Default
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService

    '--------------------------------------------------------------
    ' Page Load
    '--------------------------------------------------------------

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '-------------------------------
        ' Globals
        '-------------------------------
        Dim sParams As String = ""
        Dim oSession As Object
        Dim sXslHash As New Hashtable
        Dim sVehCount = ""
        Dim iRC As Integer = 0
        Dim sSessionVariable As String = ""
        Dim sSessionVariableValue As String = ""
        Dim uspXMLRaw As XmlElement
        Dim uspXMLXDoc As New XmlDocument
        Dim XMLSessionNode As XmlNode
        Dim sHTML As String = ""
        Dim sAPDNetDebugging As String = ""
        Dim dtStart As Date
        Dim APDDBUtils As APDDBUtilities = Nothing
        Dim sInsuranceCompanyID As String = ""
        Dim sContext As String = ""
        Dim sClaimStatus As String = ""
        Dim XMLCRUD As XmlElement = Nothing
        Dim sInfoCRUD As String = ""
        Dim sAction As String = ""

        '-------------------------------
        ' Session/Local Variables
        '-------------------------------
        Dim sStoredProcedure As String = "uspJournalGetDetailWSXML"
        Dim sXSLProcedure As String = "JournalDetail.xsl"

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        Dim sLynxID As String = Request.QueryString("LynxID")
        Dim sUserID As String = Request.QueryString("UserID")
        Dim sSessionKey = Request.QueryString("SessionKey")
        Dim sWindowID = Request.QueryString("WindowID")
        Dim sVehClaimAspectID = Request.QueryString("ClaimAspectID")
        Dim sASPParams = Request.QueryString("Params")

        '---------------------------------
        ' Debug Data
        '---------------------------------
        'sSessionKey = "{F061E217-1CBB-4D79-9FF3-C0A001E12B00}"
        'sWindowID = 2

        Try
            If sSessionKey = "" Then
                Throw New SystemException("SessionValidationError: No session information received from classic ASP page.")
            Else
                '---------------------------------
                ' New Session Data
                '---------------------------------
                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "LynxID-" & sWindowID)
                sLynxID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "UserID-" & sWindowID)
                sUserID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "APDNetDebugging-" & sWindowID)
                sAPDNetDebugging = ""
                If UCase(oSession(0).SessionValue) = "TRUE" Or UCase(AppSettings("Debug")) = "TRUE" Then
                    sAPDNetDebugging = "TRUE"
                End If

                APDDBUtils = New APDDBUtilities()
                dtStart = Date.Now

                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls.", " LynxID: " & sLynxID & "UserID: " & sUserID & "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
                End If
                '---------------------------------
                ' Get the session data
                '---------------------------------
                If Not Request("LynxID") Then
                    'sLynxID = APDDBUtils.LynxID
                Else
                    sLynxID = Request("LynxID")
                End If

                If Not Request("UserID") Then
                    'sUserID = APDDBUtils.UserID
                Else
                    sUserID = Request("UserID")
                End If

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "InsuranceCompanyID-" & sWindowID)
                sInsuranceCompanyID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "VehClaimAspectID-" & sWindowID)
                sVehClaimAspectID = oSession(0).SessionValue

                'oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "Context-" & sWindowID)
                'sContext = oSession(0).SessionValue

                'oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "ClaimStatus-" & sWindowID)
                'sClaimStatus = oSession(0).SessionValue

                'oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "VehCount-" & sWindowID)
                'sVehCount = oSession(0).SessionValue

                '---------------------------------
                ' Get the permission data
                '---------------------------------
                'XMLCRUD = wsAPDFoundation.ExecuteSpAsXML("uspCRUDGetDetailWSXML", "'Client Billing', " & sUserID)
                'sInfoCRUD = APDDBUtils.GetCrud(XMLCRUD)

                '-----------------------------------
                ' Add config variables as XslParams
                '-----------------------------------
                sXslHash.Add("LynxID", sLynxID)
                sXslHash.Add("UserID", sUserID)
                'sXslHash.Add("VehicleNum", sVehCount)
                sXslHash.Add("WindowID", sWindowID)
                'sXslHash.Add("ClaimStatus", sClaimStatus)
                'sXslHash.Add("InfoCRUD", sInfoCRUD)
                'sXslHash.Add("Context", sContext)
                sXslHash.Add("InsuranceCompanyID", sInsuranceCompanyID)


                '------------------------------------
                ' If ASPParms, parse and add to hash
                '------------------------------------
                If sASPParams <> "" Then
                    Dim aParams As Array
                    aParams = Split(sASPParams, "|")
                    For Each sASPParam In aParams
                        Dim aValues As Array
                        aValues = Split(sASPParam, ":")
                        sXslHash.Add(aValues(0), aValues(1))
                    Next
                End If

                '-------------------------------------
                ' Debugging
                '-------------------------------------
                'Response.Write("sSessionKey" & " - " & sSessionKey & "<br/>")
                'Response.Write("sWindowID" & " - " & sWindowID & "<br/>")
                'Response.Write("sUserID" & " - " & sUserID & "<br/>")
                'Response.Write("sLynxID" & " - " & sLynxID & "<br/>")
                'Response.Write("strAction" & " - " & sAction & "<br/>")
                'Response.Write("sASPParams" & " - " & sASPParams & "<br/>")
                'Response.End()

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls/Parameters." _
                                             , "Session controls for Window: " & sWindowID _
                                             & " and SessionKey: " & sSessionKey _
                                             , "Vars: StoredProc = " & sStoredProcedure _
                                             & ", XSLPage = " & sXSLProcedure _
                                             & ", UserID(Session) = " & sUserID _
                                             & ", LynxID(Session) = " & sLynxID _
                                             & ", Action = " & sAction _
                                             & ", sASPParams = " & sASPParams _
                                             )
                End If

                '---------------------------------
                ' XSL Transformation
                '---------------------------------
                sParams = sLynxID

                '---------------------------------
                ' StoredProc XML Data
                '---------------------------------
                uspXMLRaw = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

                If uspXMLRaw.OuterXml = Nothing Then
                    ' Throw exception
                Else
                    uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)

                    If UCase(sAPDNetDebugging) = "TRUE" Then
                        sXslHash.Add("RunTime", Now.Subtract(dtStart).Milliseconds & " msec")
                    Else
                        sXslHash.Add("RunTime", Chr(149))
                    End If

                    '---------------------------------
                    ' Update Session
                    '---------------------------------
                    Dim XMLNodeValidate As XmlNode = uspXMLXDoc.SelectSingleNode("Root/@Context")
                    If XMLNodeValidate Is Nothing Then
                    Else
                        iRC = wsAPDFoundation.UpdateSessionVar(sSessionKey, "Context-" & sWindowID, XMLNodeValidate.InnerText)
                    End If

                    sHTML = APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sParams, sXslHash)

                    Response.Write(sHTML)
                End If

                'uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)
            End If

            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "APDJournal.aspx - Finished: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls/parameter not found or page pre-transformation failed.", sError & "  Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID)

            Response.Write(sError)
            Response.End()
            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        End Try

    End Sub
End Class