﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddVehDACoverage.aspx.vb" Inherits="APDNet.AddVehDACoverage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Apply Coverage</title> 
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />

    <!-- ============== JQuery - Loader =============== -->
    <script type="text/javascript" src="js/jQuery/jquery-1.11.1.js"></script>

    <link rel="stylesheet" href="css/jquery-ui.css" />
    <%--    <script type="text/javascript" src="Scripts/ClaimTabs.js"></script>
    <script type="text/javascript" src="Scripts/APDData.js"></script>--%>

    <script type="text/javascript">
        function OnClose() {
            //alert("Here");
            if (window.opener != null && !window.opener.closed) {
                window.opener.HideModalDiv();
            }
            window.returnValue = "false";
            window.close();
        }

        function doCancel() {
            OnClose();
            //window.returnValue = "false";
            //window.close();
        }

        //function checkLimit(senderID) {
        //    var strCoverageLimit = $("#txtCoverageLimit").val();
        //    if ($.isNumeric($("#txtCoverageLimit").val())) {
        //        if ((parseFloat(strCoverageLimit) > 0 && parseFloat(strCoverageLimit)) <= 10000) {
        //            window.opener.top.displayCoverageLimitWarining(true);
        //        }  else {
        //            window.opener.top.displayCoverageLimitWarining(false);
        //        }
        //    } else {
        //        return;
        //    }

        //}

        function getTotalDeductible() {
            alert("DDLChanged...");
            $("#txtTotalDeductible").prop('disabled', false);
            $("#txtTotalDedApplied").prop('disabled', false);

            var iTotalDeductible = 0;
            var iTotalDeductibleApplied = 0;

            //var oCoverage = xmlCoveragesApplied.selectSingleNode("/Root/CoverageApplied[@ClaimCoverageID='" + selCoverage.value + "']")

            //if (oCoverage)
            //    blnAddMode = false;
            //else
            //    blnAddMode = true;

            //var oClaimCoverage = xmlReference.selectSingleNode("/Root/Reference[@List='ClaimCoverage' and @ReferenceID='" + selCoverage.value + "']");
            //if (oClaimCoverage) {

            $("#txtTotalDeductible").val("100.00");
            //    txtTotalDeductible.value = oClaimCoverage.getAttribute("DeductibleAmt");

            //    iTotalDeductible = parseFloat(oClaimCoverage.getAttribute("DeductibleAmt"));
            //    iTotalLimit = parseFloat(oClaimCoverage.getAttribute("LimitAmt"));
            //    var iDedApplied = 0;

            //    //var oAppliedNodes = xmlCoveragesApplied.selectNodes("/Root/CoverageApplied[@ClaimCoverageID='" + selCoverage.value + "']");
            //    var oAppliedNodes = xmlClaimCoverages.selectNodes("/Root/ClaimCoveragesApplied[@ClaimCoverageID='" + selCoverage.value + "' and @ClaimAspectServiceChannelID != '" + gsClaimAspectServiceChannelID + "']");
            //    if (oAppliedNodes && oAppliedNodes.length > 0) {
            //        for (var i = 0; i < oAppliedNodes.length; i++) {
            //            iDedApplied = oAppliedNodes[i].getAttribute("DeductibleAppliedAmt");
            //            iTotalDeductibleApplied += (isNaN(iDedApplied) ? 0 : parseFloat(iDedApplied));
            //        }
            //    }

            //    if (blnAddMode == true) {
            //        txtDeductible.value = iTotalDeductible - iTotalDeductibleApplied;
            //        txtDeductibleApplied.value = iTotalDeductibleApplied;
            //    } else {
            //        if (oCoverage) {
            //            txtDeductible.value = oCoverage.getAttribute("DeductibleAppliedAmt");
            //            txtDeductibleApplied.value = iTotalDeductibleApplied;
            //        }
            //    }
            //} else {
            //    txtTotalDeductible.value = "";
            //    txtDeductibleApplied.value = "";
            //}
            //txtTotalDeductible.CCDisabled = true;
            //txtDeductibleApplied.CCDisabled = true;
        }
    </script>
</head>
<body>
    <form id="frmAddCoverage" runat="server">
        <div id="diaAddCoverage" title="Add Coverage">
            <table class="topTable" style="border-spacing: 0px; width: 400px;">
                <tr style="vertical-align: top; ">
                    <td style="width: 200px;">
                        LynxID:
                    </td>
                    <td>
                        &nbsp;&nbsp;<asp:TextBox ID="txtLynxID" runat="server" CssClass="inputField" Enabled="false" />
                    </td>
                </tr>         
                <tr style="vertical-align: top;">
                    <td style="width: 200px;">Coverage:
                    </td>
                    <td>
                        &nbsp;&nbsp;<asp:DropDownList ID="ddlCoverage" runat="server" CssClass="inputField" Width="175px" AutoPostBack="True"></asp:DropDownList>
<%--                        &nbsp;&nbsp;<asp:DropDownList ID="ddlCoverage" runat="server" CssClass="inputField" Width="175px" AutoPostBack="false" onchange="getTotalDeductible();"></asp:DropDownList>--%>
<%--                        <asp:RequiredFieldValidator ID="ddlCoverageDescriptionValidator" runat="server" ControlToValidate="ddlCoverageDescription" ErrorMessage="*Required" InitialValue=""></asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 200px;">Total Deductible: 
                    </td>
                    <td>
                        <b>$</b><asp:TextBox ID="txtTotalDeductible" runat="server" CssClass="inputField" Width="100px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 200px;">Total Ded. Applied:  
                    </td>
                    <td>
                        <b>$</b><asp:TextBox ID="txtTotalDedApplied" runat="server" CssClass="inputField" Width="100px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 200px;">Ded. applied for this channel:
                    </td>
                    <td>
                        <b>$</b><asp:TextBox ID="txtDedAppliedChannel" runat="server" CssClass="inputField" Width="100px" Enabled="true"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="txtDedAppliedChannelValidator" runat="server" ControlToValidate="txtDedAppliedChannel" ErrorMessage="Numeric only" ForeColor="Red" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 200px;">Overage of Limits:  
                    </td>
                    <td>
                        <b>$</b><asp:TextBox ID="txtOverageLimits" runat="server" CssClass="inputField" Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="txtCoverageDeductibleNotBlankValidator" runat="server" ErrorMessage="*Required" ControlToValidate="txtOverageLimits" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="txtOverageLimitsValidator" runat="server" ControlToValidate="txtOverageLimits" ErrorMessage="Numeric only" ForeColor="Red" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 200px;">
                        <asp:CheckBox ID="chkPartialCoverage" runat="server" CssClass="inputField" Text="Partial Coverage" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 200px;">&nbsp;
                        <asp:Button ID="btnCoverageAddSave" runat="server" Text="Save" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:doCancel(); return false;" />
                        <asp:Label ID="lblMsg" runat="server" ForeColor="Red" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
