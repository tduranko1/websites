﻿Imports System.Xml

Public Class SavCoverge
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    Dim sCRUD As String = ""
    Dim iLynxID As Integer = 0
    Dim iInsuranceCompanyID As Integer = 0
    Dim iSelectedCoverageTypeID As Integer
    Dim sUserID As String = ""
    Dim sClaimCoverageID As String
    Dim sSelectedCoverageTypeID As String = ""
    Dim sSelectedClaimCoverageID As String = ""
    Dim sCoverageSysLastUpdatedDate As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim iClientCoverageType As Integer = 0
        Dim DTCoverage As New DataTable
        Dim DCCoverage As New DataColumn
        Dim sDDLParams As String = ""
        Dim sSelectedClientCoverageTypeID As String = ""
        Dim sQueryParams As String = ""
        Dim aQueryParams As Array = Nothing

        Try
            '-------------------------------
            ' Check Session Data
            '-------------------------------
            iLynxID = Request.QueryString("LynxID")
            iInsuranceCompanyID = Request.QueryString("InscCompID")
            sUserID = Request.QueryString("UserID")

            '-------------------------------
            ' Process Validation
            '-------------------------------
            '??? Verify we got good params

            '-------------------------------
            ' Apply CRUD
            '-------------------------------

            '-------------------------------
            ' Process Main Code
            '-------------------------------
            '-------------------------------
            ' Database access
            '-------------------------------  
            Dim sStoredProcedure As String = "uspClaimCondGetDetailWSXML"
            Dim sParams As String = iLynxID & "," & iInsuranceCompanyID

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '---------------------------------------
            ' Load Current Coverages
            '---------------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("Claim/Coverage")

            DTCoverage.Columns.Add(New DataColumn("Description", GetType(String)))
            DTCoverage.Columns.Add(New DataColumn("CoverageTypeCD", GetType(String)))

            For Each oXMLNode In oXMLNodeList
                Dim DRCoverage As DataRow = DTCoverage.NewRow
                DRCoverage("Description") = oXMLNode.Attributes("CoverageTypeCD").InnerText & " - " & oXMLNode.Attributes("Description").InnerText
                DRCoverage("CoverageTypeCD") = oXMLNode.Attributes("CoverageTypeCD").InnerText
                DTCoverage.Rows.Add(DRCoverage)
            Next

            '-------------------------------
            ' Load Reference data
            '-------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("Reference")
            iClientCoverageType = 0

            '--------------------------------
            ' Populate Coverage Add/Edit
            ' Description Dropdown
            '--------------------------------
            For Each oXMLNode In oXMLNodeList
                '---------------------------------------
                ' Load Reference data - AssignmentType
                '---------------------------------------
                'If iClientCoverageType = 0 Then
                '    ddlCoverageDescription.Items.Add("")
                '    iClientCoverageType += 1
                'End If

                If oXMLNode.Attributes("List").InnerText = "ClientCoverageType" Then
                    If DTCoverage.Select("Description='" & oXMLNode.Attributes("ReferenceID").Value & " - " & oXMLNode.Attributes("Name").Value & "'").Length = 0 Then
                        sDDLParams = oXMLNode.Attributes("ReferenceID").Value & "|" & oXMLNode.Attributes("Name").Value & "|" & oXMLNode.Attributes("ClientCoverageTypeID").Value & "|" & oXMLNode.Attributes("AddtlCoverageFlag").Value & "|" & oXMLNode.Attributes("AddtlCoverageFlag").Value
                        ddlCoverageDescription.Items.Insert(iClientCoverageType, New ListItem(oXMLNode.Attributes("ReferenceID").Value & " - " & oXMLNode.Attributes("Name").InnerText, sDDLParams))
                        iClientCoverageType += 1
                    End If
                End If
            Next

            ddlCoverageDescription.SelectedItem.Text = Request.QueryString("ClientCode") + " - " + Request.QueryString("Description")
            sClaimCoverageID = Request.QueryString("SelectedClaimCoverageID")
            sSelectedCoverageTypeID = Request.QueryString("ClientCoverageTypeID")
            sCoverageSysLastUpdatedDate = Request.QueryString("CoverageSysLastUpdatedDate")
            If Not IsPostBack Then
                txtLynxID.Text = iLynxID
                txtCoverageClientCode.Text = Request.QueryString("ClientCode")
                txtCoverageType.Text = Request.QueryString("Description").Replace(Request.QueryString("ClientCode") + " - ", "")
            End If


            '' If the value return from "parms"
            sQueryParams = Request.QueryString("Params")
            If sQueryParams <> "" Or sQueryParams <> Nothing Then
                aQueryParams = sQueryParams.Split("|")
                ddlCoverageDescription.SelectedItem.Text = aQueryParams(3)
                sClaimCoverageID = aQueryParams(1)
                sSelectedCoverageTypeID = aQueryParams(0)
                sCoverageSysLastUpdatedDate = aQueryParams(2)

                If Not IsPostBack Then
                    txtCoverageClientCode.Text = aQueryParams(4)
                    txtCoverageType.Text = aQueryParams(4)
                End If

                ddlCoverageDescription.SelectedValue = sSelectedClientCoverageTypeID
            End If
            '-------------------------------
            ' Clean-up
            '-------------------------------
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDCoverage", "ERROR", "Coverage data not found or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
            'Return ""
        Finally
            'oXMLNode = Nothing
            'oReturnXML = Nothing
            'oXMLNodeList = Nothing
        End Try
    End Sub

    Protected Sub btnCoverageSave_Click(sender As Object, e As EventArgs) Handles btnCoverageSave.Click
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim sLimitDailyAmt As String = 0
        Dim sMaximumDays As String = 0
        Dim aCoverageParams As Array = Nothing
        Dim sDiscription As String = ""
        Dim sDeductibleAmt As String = 0
        Dim sLimitAmt As String = 0

        Try
            '-------------------------------
            ' Validate the web page data
            '-------------------------------  
            '??????????? CODE THIS ????????????????????
            'lblClaimSaved.Text = ""

            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim sStoredProcedure As String = "uspClaimCoverageUpdDetailWSXML"

            If UCase(txtCoverageAdditionalFlag.Text) = "YES" Then
                sSelectedClaimCoverageID = "1"
            Else
                sSelectedClaimCoverageID = "0"
            End If
            sDiscription = Request.QueryString("Description").Replace(Request.QueryString("ClientCode") + " - ", "")

            If (txtCoverageDailyLimit.Text).Trim() = "" Or txtCoverageDailyLimit.Text = Nothing Then
                sLimitDailyAmt = 0
            Else
                sLimitDailyAmt = txtCoverageDailyLimit.Text
            End If
            If (txtCoverageMaxDays.Text).Trim() = "" Or txtCoverageMaxDays.Text = Nothing Then
                sMaximumDays = 0
            Else
                sMaximumDays = txtCoverageMaxDays.Text
            End If

            If (txtCoverageDeductible.Text).Trim() = "" Or txtCoverageDeductible.Text = Nothing Then
                sDeductibleAmt = 0
            Else
                sDeductibleAmt = txtCoverageDeductible.Text
            End If
            If (txtCoverageLimit.Text).Trim() = "" Or txtCoverageLimit.Text = Nothing Then
                sLimitAmt = 0
            Else
                sLimitAmt = txtCoverageLimit.Text
            End If

            Dim sParams As String = "@ClaimCoverageID = " & sClaimCoverageID _
                & ", @ClientCoverageTypeID = " & CStr(sSelectedCoverageTypeID) _
                & ", @LynxID = " & CStr(iLynxID) _
                & ", @InsuranceCompanyID = " & CStr(iInsuranceCompanyID) _
                & ", @AddtlCoverageFlag = " & sSelectedClaimCoverageID _
                & ", @CoverageTypeCD = '" & txtCoverageClientCode.Text & "'" _
                & ", @Description = '" & sDiscription & "'" _
                & ", @DeductibleAmt = " & sDeductibleAmt _
                & ", @LimitAmt = " & sLimitAmt _
                & ", @LimitDailyAmt = " & sLimitDailyAmt _
                & ", @MaximumDays = " & sMaximumDays _
                & ", @UserID = " & sUserID _
                & ", @SysLastUpdatedDate = '" & sCoverageSysLastUpdatedDate & "'"

            '?????? Code This ?????
            '    oNode.setAttribute("AddlCoverageFlagInd", (txtAdditionalFlag.value == "Yes" ? "/images/tick.gif" : "/images/spacer.gif"));

            'Debug.Print(sParams)

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "OnClose('Save');", True)
            '???? check for success and save new Coverage syslastupdateddate ????

            'Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("CoverageAddSaveDetails", "ERROR", "Add Coverage Save not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
        End Try
    End Sub

    'Protected Sub ddlCoverageDescription_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCoverageDescription.SelectedIndexChanged
    '    Dim aCoverageData As Array = Nothing
    '    Dim iCoverageTypeID As Integer = 0

    '    aCoverageData = ddlCoverageDescription.SelectedItem.Value.Split("|")
    '    txtCoverageClientCode.Text = aCoverageData(0)
    '    txtCoverageType.Text = aCoverageData(1)
    '    iCoverageTypeID = CInt(aCoverageData(2))
    '    txtCoverageAdditionalFlag.Text = aCoverageData(3)
    '    txtCoverageDailyLimit.Text = 0
    '    txtCoverageDeductible.Text = 0
    '    txtCoverageLimit.Text = 0
    '    txtCoverageMaxDays.Text = 0
    '    iSelectedCoverageTypeID = iCoverageTypeID

    '    '-------------------------
    '    ' Additional Flag 
    '    '-------------------------
    '    Select Case txtCoverageAdditionalFlag.Text
    '        Case "0"
    '            txtCoverageAdditionalFlag.Text = "No"
    '        Case "1"
    '            txtCoverageAdditionalFlag.Text = "Yes"
    '    End Select

    '    '-------------------------
    '    ' Rental 
    '    '-------------------------
    '    If UCase(txtCoverageClientCode.Text) = "RENT" Then
    '        lblRentalMax.Visible = True
    '        lblRentalLimit.Visible = True
    '        txtCoverageMaxDays.Visible = True
    '        txtCoverageDailyLimit.Visible = True
    '    Else
    '        lblRentalMax.Visible = False
    '        lblRentalLimit.Visible = False
    '        txtCoverageMaxDays.Visible = False
    '        txtCoverageDailyLimit.Visible = False
    '    End If
    'End Sub

End Class