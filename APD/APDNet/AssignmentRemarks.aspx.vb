﻿Imports System.Xml
Imports System.Xml.Xsl
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager



Public Class AssignmentRemarks
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    Dim oReturnXML As XmlElement = Nothing

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strInsuranceCompanyID As String = "", strClaimAspectServiceChannelID As String = ""


        Try
            strInsuranceCompanyID = Request.QueryString("InsuranceCompanyID")
            strClaimAspectServiceChannelID = Request.QueryString("ClaimAspectServiceChannelID")


            GetVehicleAssignmentData(strClaimAspectServiceChannelID, strInsuranceCompanyID)

            If Not IsPostBack Then
                Populatedropdown()
            End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDShopAssignment", "Error", "Shop Assignment Remarks data Not found Or data Error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
        End Try


    End Sub

    Protected Function Populatedropdown()
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim iCounter As Integer

        '-------------------------------
        ' Load Reference data
        '-------------------------------
        oXMLNodeList = oReturnXML.SelectNodes("Reference")
        iCounter = 0
        For Each oXMLNode In oXMLNodeList

            '--------------------------------
            ' Populate ShopRemarkTemplates Dropdown
            '--------------------------------GetXMLNodeattrbuteValue(oXMLNode, "List") 
            If oXMLNode.Attributes("List").InnerText = "ShopRemarkTemplates" Then
                ddlselShopRemarksTemplate.Items.Add(GetXMLNodeattrbuteValue(oXMLNode, "Name").Split("|")(0))
                ddlselShopRemarksTemplate.Items.Item(iCounter).Value = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")
                iCounter += 1
            End If
        Next

    End Function

    Protected Function GetVehicleAssignmentData(ByVal iClaimAspectServiceChannelID As Integer, iInsuranceCompanyID As Integer)

        '-------------------------------
        ' Session/Local Variables
        '-------------------------------  
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim sStoredProcedure As String = "uspVehicleAssignGetDetailwsXML"
        Dim sParams As String = iClaimAspectServiceChannelID & "," & iInsuranceCompanyID

        Try
            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDShopAssignment", "Error", "Shop Assignment Remarks data Not found Or data Error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        End Try


    End Function

    Protected Sub ddlselShopRemarksTemplate_SelectedIndexChanged(sender As Object, e As EventArgs)
        GetTransformedMessage()
    End Sub


    Protected Function GetTransformedMessage()
        Try

            Dim objXML As XmlDocument = New XmlDocument()
            Dim oXMLNode As XmlNode = Nothing
            Dim oXMLNodeList As XmlNodeList = Nothing

            Dim oXMLMsgNode As XmlNode = Nothing
            Dim oAttribute As XmlAttribute
            Dim strMessageTemplate As String = ""

            Dim oXSLT As New XslCompiledTransform()

            oXMLNodeList = oReturnXML.SelectNodes("Reference")

            For Each oXMLNode In oXMLNodeList

                '--------------------------------
                ' Populate ShopRemarkTemplates Dropdown
                '--------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "ShopRemarkTemplates" And GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID") = ddlselShopRemarksTemplate.SelectedValue Then
                    strMessageTemplate = oXMLNode.Attributes("Name").InnerText
                End If
            Next

            ''Construct xml for the message template
            oXMLMsgNode = objXML.CreateElement("Claim")

            oAttribute = objXML.CreateAttribute("rental")
            oAttribute.Value = ddlselRentalCompany.SelectedItem.Text
            oXMLMsgNode.Attributes.Append(oAttribute)

            'From Vehicle node 
            oXMLNode = oReturnXML.SelectSingleNode("Vehicle")
            oAttribute = objXML.CreateAttribute("InsuranceCompanyName")
            oAttribute.Value = GetXMLNodeattrbuteValue(oXMLNode, "InsuranceCompanyName")
            oXMLMsgNode.Attributes.Append(oAttribute)

            oAttribute = objXML.CreateAttribute("InsuredClaimant")
            oAttribute.Value = GetXMLNodeattrbuteValue(oXMLNode, "InsuredClaimantName")
            oXMLMsgNode.Attributes.Append(oAttribute)

            ''From Root node 
            oXMLNode = oReturnXML.SelectSingleNode("/")
            oAttribute = objXML.CreateAttribute("ClaimOwnerName")
            oAttribute.Value = GetXMLNodeattrbuteValue(oXMLNode, "ClaimOwnerName")
            oXMLMsgNode.Attributes.Append(oAttribute)

            oAttribute = objXML.CreateAttribute("ClaimOwnerPhone")
            oAttribute.Value = GetXMLNodeattrbuteValue(oXMLNode, "ClaimOwnerPhone")
            oXMLMsgNode.Attributes.Append(oAttribute)

            objXML.AppendChild(oXMLMsgNode)

            oXSLT.Load(Server.MapPath("~/" + strMessageTemplate.Split("|")(1)))

            Using writer = New StringWriter()

                oXSLT.Transform(objXML, Nothing, writer)

                txtAssignmentRemarksMore.Text = writer.ToString()
            End Using
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDShopAssignment", "Error", "Shop Assignment Remarks data Not found Or data Error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
        End Try

    End Function

    ''' <summary>
    ''' To check and get the xmlnode attribute text value
    ''' </summary>
    ''' <param name="XMLnode"></param>
    ''' <param name="strAttributes"></param>
    ''' <returns></returns>
    Protected Function GetXMLNodeattrbuteValue(ByVal XMLnode As XmlNode, strAttributes As String) As String
        Dim strRetVal As String = ""
        If Not XMLnode Is Nothing Then
            If Not XMLnode.Attributes(strAttributes) Is Nothing Then
                strRetVal = XMLnode.Attributes(strAttributes).InnerText
            Else
                strRetVal = ""
            End If
        Else
            strRetVal = ""
        End If
        Return strRetVal
    End Function
End Class