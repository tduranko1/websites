﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SavCoverage.aspx.vb" Inherits="APDNet.SavCoverge" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Claim Save Coverage</title>
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />

    <!-- ============== JQuery - Loader =============== -->
    <script type="text/javascript" src="js/jQuery/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="js/jQuery/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jQuery/ClaimTabs.js"></script>

    <link rel="stylesheet" href="css/jquery-ui.css" />
    <%--    <script type="text/javascript" src="Scripts/ClaimTabs.js"></script>
    <script type="text/javascript" src="Scripts/APDData.js"></script>--%>

    <script type="text/javascript">

        function rentalFldEnable(){
            if ($("#txtCoverageClientCode").val() == "RENT") {
                $("#divRentalMax").css("visibility", "visible");
                $("#divRentalLimit").css("visibility", "visible");
                $("#txtCoverageAdditionalFlag")[0].value = "YES";
            }
            else {
                $("#divRentalMax").css("display", "none");
                $("#divRentalLimit").css("display", "none");
                $("#txtCoverageAdditionalFlag")[0].value = "NO";
            }
            $("#txtCoverageDeductible")[0].value = getUrlParameter("Deductable");
            $("#txtCoverageLimit")[0].value = getUrlParameter("Limit");
            $("#txtCoverageMaxDays")[0].value = getUrlParameter("MaxDays");
            $("#txtCoverageDailyLimit")[0].value = getUrlParameter("DayLimit");
        }

        function getUrlParameter(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }

        function OnClose(strSource) {
            //alert(window.opener.location);
            if (window.opener != null && !window.opener.closed) {
                window.opener.HideModalDiv();
            }
            if (strSource == "Save") {
                window.opener.location.href = window.opener.location.href;
                window.close();
            }
            else {
                window.close();
            }
        }

       </script>
</head>
<body onload="rentalFldEnable()">
    <form id="frmSavCoverage" runat="server">
        <div id="diaSavCoverage" title="Save Coverage">
            <!-- Title Bar -->
<%--            <table style="padding: 0; width: 300px; border: 0;">
                <tr>
                    <td style="text-align: left; vertical-align: top; width: 8px; background-image: url(images/PanelLeft.png); background-repeat: no-repeat;" />
                    <td style="text-align: left; vertical-align: top; background-image: url(images/PanelMiddle.png); background-repeat: repeat;" class="Header">
                        <a name="InvoiceInfo" style="color: White; font-weight: bold;">LynxID:
                            <asp:Label ID="lblLynxID" runat="server"></asp:Label>
                        </a>
                    </td>
                    <td style="text-align: left; vertical-align: top; width: 8px; background-image: url(images/PanelRight.png); background-repeat: no-repeat;" />
                </tr>
            </table>--%>
            <!-- /Title Bar -->

            <table class="topTable" style="border-spacing: 0px; width: 350px;">
                <tr style="vertical-align: top; ">
                    <td style="width: 95px;">
                        <b>LynxID:</b>
                    </td>
                    <td>
                        <asp:TextBox ID="txtLynxID" runat="server" CssClass="inputField" Enabled="false" />
                    </td>
                </tr>         
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Client Code:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageClientCode" runat="server" CssClass="inputField" Width="50px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Description:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCoverageDescription" runat="server" CssClass="inputField" Width="175px" Enabled="false"></asp:DropDownList>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Type:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageType" runat="server" CssClass="inputField" Width="50px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Additional Flag:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageAdditionalFlag" runat="server" CssClass="inputField" Width="175px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Deductible:  $
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageDeductible" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">Limit:  $
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageLimit" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                    </td>
                </tr>

                <tr id="divRentalMax" style="vertical-align: top; visibility:hidden">
                    <td style="width: 85px;">
                       Maximum Days:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageMaxDays" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                    </td>
                </tr>
                <tr id="divRentalLimit" style="vertical-align: top;  visibility:hidden">
                    <td style="width: 85px;">Rental Limit: $
                    </td>
                    <td>
                        <asp:TextBox ID="txtCoverageDailyLimit" runat="server" CssClass="inputField" Width="50px"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td>&nbsp;</td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnCoverageSave" runat="server" Text="Save" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:OnClose('cancel'); return false;" />
                    </td>
                </tr>
            </table>

            <!-- Footer Bar -->
<%--            <table style="padding: 0; width: 300px; border: 0;">
                <tr>
                    <td style="text-align: left; vertical-align: top; width: 8px; background-image: url(images/PanelBotLeft.png); background-repeat: no-repeat;" />
                    <td style="text-align: left; vertical-align: top; background-image: url(images/PanelMiddle.png); background-repeat: repeat;" class="Header">&nbsp;
                    </td>
                    <td style="text-align: left; vertical-align: top; width: 8px; background-image: url(images/PanelBotRight.png); background-repeat: no-repeat;" />
                </tr>
            </table>--%>
            <!-- /Footer Bar -->
        </div>
    </form>
</body>
</html>