﻿Imports System.Xml
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Public Class UpdateCarrierRep
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    Dim sCRUD As String = ""
    Dim sInsuranceCompanyID As String = ""
    Dim sLynxID As String = ""
    Dim sUserID As String = ""
    Dim sCarrierRepSysLastUpdatedDate As String = ""
    Dim sAPDNetDebugging As String = ""

    'Dim iSelectedCoverageTypeID As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sCurrentCarrierRepUserID As String = ""
        Dim sCurrentOfficeID As String = ""
        Dim sRC As String = ""

        '---------------------------------
        ' Debugging Setup
        '---------------------------------
        If UCase(AppSettings("Debug")) = "TRUE" Then
            sAPDNetDebugging = "TRUE"
        End If

        '-------------------------------
        ' Check Session Data
        '-------------------------------
        sLynxID = Request.QueryString("LynxID")
        txtLynxID.Text = sLynxID
        sUserID = Request.QueryString("UserID")
        sInsuranceCompanyID = Request.QueryString("InscCompID")
        sCurrentOfficeID = Request.QueryString("CurrentOfficeID")
        sCurrentCarrierRepUserID = Request.QueryString("CarrierRepUserID")
        sCarrierRepSysLastUpdatedDate = Request.QueryString("CarrierRepSysLastUpdatedDate")

        '---------------------------------
        ' Debugging
        '---------------------------------
        If sAPDNetDebugging = "TRUE" Then
            wsAPDFoundation.LogEvent("APDNet-ClaimXP", "START", "UpdateCarrierRep.aspx - Starting: " & Date.Now, "Session Data and Param Variables: LynxID=" & sLynxID, "Vars: LynxID=" & sLynxID & ", sUserID=" & sUserID & ", sInsuranceCompanyID=" & sInsuranceCompanyID & ", sCurrentOfficeID=" & sCurrentOfficeID & ", sCurrentCarrierRepUserID=" & sCurrentCarrierRepUserID & ", sCarrierRepSysLastUpdatedDate=" & sCarrierRepSysLastUpdatedDate)
        End If

        If IsPostBack Then
        Else
            sRC = GetClaimCarrierRepData(sInsuranceCompanyID, sCurrentOfficeID, sCurrentCarrierRepUserID)

            '---------------------------------
            ' Debugging
            '---------------------------------
            If sRC = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNet-ClaimXP", "ERROR", "UpdateCarrierRep.aspx - Error occured getting the Carrier Rep Data: " & Date.Now, "", "Error=" & sRC)
            End If

        End If

        '---------------------------------
        ' Debugging
        '---------------------------------
        If sAPDNetDebugging = "TRUE" Then
            wsAPDFoundation.LogEvent("APDNet-ClaimXP", "END", "UpdateCarrierRep.aspx - Ended: " & Date.Now, "", "")
        End If

    End Sub

    Protected Function GetClaimCarrierRepData(ByVal iInscCompanyID As Integer, ByVal sCurrentOfficeID As String, ByVal sCarrierRepUserID As String) As String
        Dim oReturnXML As XmlElement = Nothing
        Dim oOfficeNodeList As XmlNodeList = Nothing
        Dim iCnt As Integer = 0
        Dim sDDLParams As String = ""
        Dim sRC As String = ""

        '-------------------------------
        ' Database access
        '-------------------------------  
        Dim sStoredProcedure As String = "uspClaimCarrierRepGetListWSXML"
        Dim sParams As String = iInscCompanyID

        '---------------------------------
        ' Debugging
        '---------------------------------
        If sAPDNetDebugging = "TRUE" Then
            wsAPDFoundation.LogEvent("APDNet-ClaimXP", "EXECSQL", "GetClaimCarrierRepData.aspx: " & Date.Now, "Updating claim carrier rep and office.", "EXEC: " & sStoredProcedure & " " & sParams)
        End If

        Try
            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '---------------------------------
            ' Debugging
            '---------------------------------
            If sAPDNetDebugging = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNet-ClaimXP", "EXECSQL", "GetClaimCarrierRepData.aspx: " & Date.Now, "StoredProc response: ", "Resp: " & oReturnXML.InnerXml)
            End If

            oOfficeNodeList = oReturnXML.SelectNodes("Office")

            For Each oXMLNode In oOfficeNodeList
                sDDLParams = oXMLNode.Attributes("OfficeID").value
                ddlCarrierOffice.Items.Insert(iCnt, New ListItem(oXMLNode.Attributes("OfficeName").value, sDDLParams))

                iCnt += 1
            Next

            ddlCarrierOffice.SelectedValue = sCurrentOfficeID
            sRC = GetCarrierRepUsersByOffice(iInscCompanyID, ddlCarrierOffice.SelectedValue, sCarrierRepUserID)

            Return sRC
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error:   {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDNet-ClaimXP", "ERROR", "Claim Carrier Rep List not processed or data error occured.", oExcept.Message, sError)

            Return oExcept.ToString
        Finally
            oReturnXML = Nothing
            oOfficeNodeList = Nothing
        End Try
    End Function

    Protected Function GetCarrierRepUsersByOffice(ByVal iInsuranceCompanyID As Integer, ByVal iOfficeID As Integer, ByVal sCarrierRepUserID As String)
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim oOfficeUserNodeList As XmlNodeList = Nothing
        Dim oOfficeNodeClaimRepList As XmlNodeList = Nothing
        Dim oOffice As Office = Nothing
        Dim iCnt As Integer = 0
        Dim iCnt1 As Integer = 0
        Dim sDDLParams As String = ""
        '-------------------------------
        ' Database access
        '-------------------------------  
        Dim sStoredProcedure As String = "uspClaimCarrierRepGetListByOfficeWSXML"
        Dim sParams As String = "@InsuranceCompanyID=" & iInsuranceCompanyID & ", @OfficeID=" & iOfficeID

        Try
            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            oOfficeUserNodeList = oReturnXML.SelectNodes("Office/OfficeUser")

            For Each oXMLNode In oOfficeUserNodeList
                ddlCarrierRep.Items.Insert(iCnt, New ListItem(oXMLNode.Attributes("NameLast").Value & ", " & oXMLNode.Attributes("NameFirst").Value, oXMLNode.Attributes("UserID").Value))
                iCnt += 1
            Next

            If sCarrierRepUserID <> "" Then
                ddlCarrierRep.SelectedValue = sCarrierRepUserID
            End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error:  {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("GetCarrierRepUsersByOffice", "ERROR", "Claim Carrier Rep User List not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
        Finally
            'oXMLNode = Nothing
            'oReturnXML = Nothing
            'oXMLNodeList = Nothing
        End Try
    End Function

    Protected Sub ddlCarrierOffice_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCarrierOffice.SelectedIndexChanged
        Dim iRC As Integer = 0

        ddlCarrierRep.Items.Clear()

        iRC = GetCarrierRepUsersByOffice(sInsuranceCompanyID, ddlCarrierOffice.SelectedValue, "")
    End Sub

    Protected Sub btnCarrierRepOK_Click(sender As Object, e As EventArgs) Handles btnCarrierRepOK.Click
        Dim sReturnData As String = ""

        Try
            '-------------------------------
            ' Validate the web page data
            '-------------------------------  
            '??????????? CODE THIS ????????????????????

            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim sStoredProcedure As String = "uspClaimCarrierUpdDetail"
            Dim sParams As String = "@LynxID = " & sLynxID _
                & ", @CarrierRepUserID = " & ddlCarrierRep.Text _
                & ", @UserID = " & sUserID _
                & ", @SysLastUpdatedDate = '" & sCarrierRepSysLastUpdatedDate & "'"

            'Debug.Print(sParams)

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            sReturnData = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

            '???? check for success ????

            'Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)

            '--------------------------------- 
            ' Close Window
            '---------------------------------
            'Dim sScript As String = ""
            'sScript = "window.opener.HideModalDiv(); window.returnValue = 'False'; window.close(); "
            'sScript = "window.opener.reload(true); window.returnValue = 'False'; window.close(); "

            'ClientScript.RegisterStartupScript(GetType(Page), "closePage", sScript, True)
            Page.ClientScript.RegisterStartupScript(Me.GetType, "myCloseScript", "window.opener.location.reload(true); window.close();", True)

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("CarrierRepOK", "ERROR", "Carrier Rep Update not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
        Finally
        End Try
    End Sub
End Class