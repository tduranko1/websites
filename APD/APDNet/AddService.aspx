﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddService.aspx.vb" Inherits="APDNet.AddService" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Vehicle Add Service Channel</title>
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />

    <!-- ============== JQuery - Loader =============== -->
    <script type="text/javascript" src="js/jQuery/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="js/jQuery/jquery-ui.js"></script>

    <link rel="stylesheet" href="css/jquery-ui.css" />
    <%--    <script type="text/javascript" src="Scripts/ClaimTabs.js"></script>
    <script type="text/javascript" src="Scripts/APDData.js"></script>--%>

    <script type="text/javascript">
        $(document).ready(function () {
            var optionValues = [];
            $('#ddlNewServiceChannel option').each(function () {
                optionValues.push($(this).val());
            });
            if(optionValues[1] == "" || optionValues[1] == undefined)
            {
                alert("The Services are full");
                OnClose();
            }
        });
 $(window).on("beforeunload", function () {
            if (window.opener != null && !window.opener.closed) {
                if (typeof window.opener.HideModalDiv == 'function' || typeof window.opener.HideModalDiv == 'object') {
                    window.opener.HideModalDiv();
                }
            }
        });

        function OnClose() {
            if (window.opener != null && !window.opener.closed) {
                window.opener.HideModalDiv();
            }
            window.returnValue = "false";
            window.close();
        }

        function doCancel() {
            OnClose();
        }

        function ServiceValidation() {
            var strServiceChannel = document.getElementById('ddlNewServiceChannel').value;
            if (strServiceChannel == "" || strServiceChannel == undefined)
            {
                alert("Please select the service");
                return false;
            }
        }
        
    </script>
</head>
<body>
    <form id="frmAddServiceChannel" runat="server">
        <!-- ==================== New Service Channel - Modal Dialog ============================== -->    
        <div id="diaNewServiceChannel" title="Add New Service Channel">
            <table class="topTable" style="border-spacing: 0px; width: 350px;" >
                <tr style="vertical-align: top;">
                    <td style="width: 85px;">
                        <asp:DropDownList ID="ddlNewServiceChannel" runat="server" CssClass="inputField" Width="275px" ></asp:DropDownList>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td>
                        <asp:CheckBox ID="chkNewServiceChannelPrimary" runat="server" Text="Primary" />
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 185px;">
                        <asp:Button runat="server" ID="btnNewServiceChannel_ADD" OnClientClick="ServiceValidation();" CssClass="formbutton" Text="Add" />
                        <asp:Button runat="server" ID="btnNewServiceChannel_CANCEL" CssClass="formbutton" Text="Cancel" OnClientClick="javascript:doCancel(); return false;" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>