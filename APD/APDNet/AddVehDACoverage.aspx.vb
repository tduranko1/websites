﻿Imports System.Xml

Public Class AddVehDACoverage
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    'Dim sCRUD As String = ""
    Dim sLynxID As String = ""
    Dim iInsuranceCompanyID As Integer = 0
    'Dim iSelectedCoverageTypeID As Integer
    Dim sUserID As String = ""
    Dim hCoverageApplied As Hashtable = New Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim oReturnXML As XmlElement = Nothing
        'Dim oXMLNode As XmlNode = Nothing
        'Dim oXMLNodeList As XmlNodeList = Nothing
        Dim iClaimAspectServiceChannelID As Integer = 0
        Dim iRC As Integer = 0
        'Dim DTCoverage As New DataTable
        'Dim DCCoverage As New DataColumn
        'Dim sDDLParams As String = ""

        Try
            '-------------------------------
            ' Check Session Data
            '-------------------------------
            sLynxID = Request.QueryString("LynxID")
            iInsuranceCompanyID = Request.QueryString("InscCompID")
            iClaimAspectServiceChannelID = Request.QueryString("ClaimAspectServiceChannelID")
            sUserID = Request.QueryString("UserID")

            '-------------------------------
            ' Get the Coverages data from DB
            '-------------------------------
            iRC = GetCoverages(iInsuranceCompanyID, iClaimAspectServiceChannelID, sLynxID)

            '    txtLynxID.Text = CStr(iLynxID)

            '    '-------------------------------
            '    ' Process Validation
            '    '-------------------------------
            '    '??? Verify we got good params

            '    '-------------------------------
            '    ' Apply CRUD
            '    '-------------------------------

            '    '-------------------------------
            '    ' Process Main Code
            '    '-------------------------------
            '    '-------------------------------
            '    ' Database access
            '    '-------------------------------  
            '    Dim sStoredProcedure As String = "uspClaimCondGetDetailWSXML"
            '    Dim sParams As String = iLynxID & "," & iInsuranceCompanyID

            '    '-------------------------------
            '    ' Call WS and get data
            '    '-------------------------------
            '    oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '    '---------------------------------------
            '    ' Load Current Coverages
            '    '---------------------------------------
            '    oXMLNodeList = oReturnXML.SelectNodes("Claim/Coverage")

            '    DTCoverage.Columns.Add(New DataColumn("Description", GetType(String)))
            '    DTCoverage.Columns.Add(New DataColumn("CoverageTypeCD", GetType(String)))

            '    For Each oXMLNode In oXMLNodeList
            '        Dim DRCoverage As DataRow = DTCoverage.NewRow
            '        DRCoverage("Description") = oXMLNode.Attributes("CoverageTypeCD").InnerText & " - " & oXMLNode.Attributes("Description").InnerText
            '        DRCoverage("CoverageTypeCD") = oXMLNode.Attributes("CoverageTypeCD").InnerText
            '        DTCoverage.Rows.Add(DRCoverage)
            '    Next

            '    '-------------------------------
            '    ' Load Reference data
            '    '-------------------------------
            '    oXMLNodeList = oReturnXML.SelectNodes("Reference")
            '    iClientCoverageType = 0

            '    '--------------------------------
            '    ' Populate Coverage Add/Edit
            '    ' Description Dropdown
            '    '--------------------------------
            '    For Each oXMLNode In oXMLNodeList
            '        '---------------------------------------
            '        ' Load Reference data - AssignmentType
            '        '---------------------------------------
            '        If iClientCoverageType = 0 Then
            '            ddlCoverageDescription.Items.Add("")
            '            iClientCoverageType += 1
            '        End If

            '        If oXMLNode.Attributes("List").InnerText = "ClientCoverageType" Then
            '            If DTCoverage.Select("Description='" & oXMLNode.Attributes("ReferenceID").Value & " - " & oXMLNode.Attributes("Name").Value & "'").Length = 0 Then
            '                sDDLParams = oXMLNode.Attributes("ReferenceID").Value & "|" & oXMLNode.Attributes("Name").Value & "|" & oXMLNode.Attributes("ClientCoverageTypeID").Value & "|" & oXMLNode.Attributes("AddtlCoverageFlag").Value & "|" & oXMLNode.Attributes("AddtlCoverageFlag").Value
            '                ddlCoverageDescription.Items.Insert(iClientCoverageType, New ListItem(oXMLNode.Attributes("ReferenceID").Value & " - " & oXMLNode.Attributes("Name").InnerText, sDDLParams))
            '                iClientCoverageType += 1
            '            End If
            '        End If
            '    Next

            '    '-------------------------------
            '    ' Clean-up
            '    '-------------------------------
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("AddVehDACoverage", "ERROR", "Vehicle Coverage data not found or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
            'Return ""
        Finally
            'oXMLNode = Nothing
            'oReturnXML = Nothing
            'oXMLNodeList = Nothing
        End Try
    End Sub

    Protected Function GetVehicleData(ByVal iInsuranceCompanyID As Integer, ByVal iClaimAspectServiceChannelID As Integer, ByVal sLynxID As String)
        '-------------------------------
        ' Session/Local Variables
        '-------------------------------  
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing

        '-------------------------------
        ' Database access
        '-------------------------------  
        Dim sStoredProcedure As String = "uspClaimVehicleGetDetailwsXML"
        Dim sParams As String = iClaimAspectServiceChannelID & "," & iInsuranceCompanyID

        Try
            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '---------------------------------------
            ' Load Vehicle data 
            '---------------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("Vehicle/ClaimAspectServiceChannel/Coverage")

            If oXMLNodeList.Count > 0 Then

            End If
            'oVehicleShopAssignment.LDAUID = GetXMLNodeattrbuteValue(oXMLNode, "LDAUID")

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("GetVehicleData", "ERROR", "Vehicle Data Not found Or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
            oXMLNodeList = Nothing
        End Try
    End Function

    Protected Function GetCoverages(ByVal iInsuranceCompanyID As Integer, ByVal iClaimAspectServiceChannelID As Integer, ByVal sLynxID As String)
        '-------------------------------
        ' Session/Local Variables
        '-------------------------------  
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim oVehicleShopAssignment As ShopAssignment = Nothing
        Dim objReferenceDisposition As ReferenceDisposition = Nothing
        Dim objReferenceStateList As ReferenceStateList = Nothing
        Dim sServiceChannel As String = ""
        Dim hCommunicationMethodProfile As New Hashtable()
        Dim hEstimatePackageProfile As New Hashtable()
        Dim oCRUDXML As XmlElement = Nothing
        Dim sShopLocationID As String = ""
        Dim iCnt As Integer = 0

        '-------------------------------
        ' Database access
        '-------------------------------  
        Dim sStoredProcedure As String = "uspVehicleAssignGetDetailwsXML"
        Dim sParams As String = iClaimAspectServiceChannelID & "," & iInsuranceCompanyID

        Try
            'oVehicleShopAssignment = New ShopAssignment()

            '-------------------------------
            ' Get CRUD Data
            '-------------------------------
            oCRUDXML = wsAPDFoundation.GetCRUD("Reference", sUserID)

            If oCRUDXML.InnerXml.Contains("ERROR") Then
                Throw New SystemException("CRUDError: Could not get the CRUD for entity: Reference=" & "Reference, UserID=" & sUserID)
            End If

            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            'sCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            txtLynxID.Text = sLynxID

            '---------------------------------------
            ' Load Vehicle data - ServiceChannel
            '---------------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("ServiceChannel/CoverageApplied")

            'ddlCoverage.Items.Add("")
            'iCnt += 1

            For Each oXMLNode In oXMLNodeList
                hCoverageApplied.Add(GetXMLNodeattrbuteValue(oXMLNode, "ClaimCoverageID"), GetXMLNodeattrbuteValue(oXMLNode, "DeductibleAppliedAmt") & "|" & GetXMLNodeattrbuteValue(oXMLNode, "LimitAppliedAmt") & "|" & GetXMLNodeattrbuteValue(oXMLNode, "PartialCoverageFlag") & "|" & GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate"))
            Next

            'hCoverageApplied.ContainsKey("258340")
            'hCoverageApplied("258340")


            'oVehicleShopAssignment.CurEstGrossRepairTotal = GetXMLNodeattrbuteValue(oXMLNode, "CurEstGrossRepairTotal")
            'oVehicleShopAssignment.CurEstDeductiblesApplied = GetXMLNodeattrbuteValue(oXMLNode, "CurEstDeductiblesApplied")
            'oVehicleShopAssignment.CurEstNetRepairTotal = GetXMLNodeattrbuteValue(oXMLNode, "CurEstNetRepairTotal")
            'oVehicleShopAssignment.CurEstLimitsEffect = GetXMLNodeattrbuteValue(oXMLNode, "CurEstLimitsEffect")
            'oVehicleShopAssignment.ClaimAspectServiceChannelID = GetXMLNodeattrbuteValue(oXMLNode, "ClaimAspectServiceChannelID")
            'oVehicleShopAssignment.JobID = GetXMLNodeattrbuteValue(oXMLNode, "JobID")
            'oVehicleShopAssignment.JobStatus = GetXMLNodeattrbuteValue(oXMLNode, "JobStatus")

            'If GetXMLNodeattrbuteValue(oXMLNode, "WorkStartConfirmFlag") = 0 Then
            '    oVehicleShopAssignment.WorkStartConfirmFlag = "display:none"
            'Else
            '    oVehicleShopAssignment.WorkStartConfirmFlag = "display:inline"
            'End If

            'If GetXMLNodeattrbuteValue(oXMLNode, "WorkEndConfirmFlag") = 0 Then
            '    oVehicleShopAssignment.WorkEndConfirmFlag = "display:none"
            'Else
            '    oVehicleShopAssignment.WorkEndConfirmFlag = "display:inline"
            'End If

            'oVehicleShopAssignment.ServiceChannelCD = GetXMLNodeattrbuteValue(oXMLNode, "ServiceChannelCD") 'oXMLNode.Attributes("ServiceChannelCD").InnerText
            ''hidClaimServiceChannelCD.Value = oVehicleShopAssignment.ServiceChannelCD

            'If oXMLNode.Attributes("WorkStartDate").Value <> "" Then
            '    oVehicleShopAssignment.WorkStartDate = Format(CDate(oXMLNode.Attributes("WorkStartDate").Value), "MM/dd/yyyy")
            'End If

            'If oXMLNode.Attributes("WorkEndDate").Value <> "" Then
            '    oVehicleShopAssignment.WorkEndDate = Format(CDate(oXMLNode.Attributes("WorkEndDate").Value), "MM/dd/yyyy")
            'End If

            'If (oXMLNode.Attributes("AppraiserInvoiceDate").Value <> "") Then
            '    oVehicleShopAssignment.AppraiserInvoiceDate = Format(CDate(oXMLNode.Attributes("AppraiserInvoiceDate").Value), "MM/dd/yyyy")
            'End If

            'If oXMLNode.Attributes("CashOutDate").Value <> "" Then
            '    oVehicleShopAssignment.CashOutDate = Format(CDate(oXMLNode.Attributes("CashOutDate").Value), "MM/dd/yyyy")
            'Else
            '    oVehicleShopAssignment.CashOutDate = ""
            'End If

            'sServiceChannel = GetXMLNodeattrbuteValue(oXMLNode, "ServiceChannelCD") 'oXMLNode.Attributes("ServiceChannelCD").InnerText
            'oVehicleShopAssignment.ServiceChannelCD = sServiceChannel
            'oVehicleShopAssignment.CurrentDisposition = GetXMLNodeattrbuteValue(oXMLNode, "DispositionTypeCD") 'oXMLNode.Attributes("DispositionTypeCD").InnerText

            '-------------------------------
            ' Load Reference data
            '-------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("Reference")

            'oVehicleShopAssignment.DispositionCollection = New List(Of ReferenceDisposition)
            'objReferenceDisposition = New ReferenceDisposition()
            'objReferenceDisposition.ReferenceID = "0"
            'objReferenceDisposition.DispositionName = " "
            'oVehicleShopAssignment.DispositionCollection.Add(objReferenceDisposition)

            'oVehicleShopAssignment.StateListCollection = New List(Of ReferenceStateList)
            'objReferenceStateList = New ReferenceStateList()
            'objReferenceStateList.ReferenceID = ""
            'objReferenceStateList.StateListName = " "
            'oVehicleShopAssignment.StateListCollection.Add(objReferenceStateList)


            'hCommunicationMethodProfile.Clear()
            'hEstimatePackageProfile.Clear()

            ddlCoverage.Items.Add("")
            iCnt += 1
            For Each oXMLNode In oXMLNodeList
                '---------------------------------------
                ' Load Reference data - Disposition
                '---------------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "ClaimCoverage" Then
                    'ddlCoverage.Items.Add(oXMLNode.Attributes("Name").Value)
                    ddlCoverage.Items.Insert(iCnt, New ListItem(oXMLNode.Attributes("Name").InnerText, oXMLNode.Attributes("ReferenceID").Value & "|" & oXMLNode.Attributes("DeductibleAmt").Value & "|" & oXMLNode.Attributes("LimitAmt").Value))
                    iCnt += 1
                End If
            Next

            ''---------------------------------------
            '' Shop Assignment Data 
            ''---------------------------------------
            'oXMLNode = oReturnXML.SelectSingleNode("ServiceChannel/Assignment")

            ''--------------------------------------------
            '' Check if Assignment Exists
            ''--------------------------------------------
            'If Not (oXMLNode) Is Nothing Then
            '    oVehicleShopAssignment.AssignmentID = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentID")
            '    oVehicleShopAssignment.AssignmentElecStatus = GetXMLNodeattrbuteValue(oXMLNode, "VANAssignmentStatusName")
            '    oVehicleShopAssignment.AssignmentStatusID = GetXMLNodeattrbuteValue(oXMLNode, "VANAssignmentStatusID")
            '    oVehicleShopAssignment.AssignmentFaxStatus = GetXMLNodeattrbuteValue(oXMLNode, "FaxAssignmentStatusName")
            '    oVehicleShopAssignment.AssignmentFaxStatusID = GetXMLNodeattrbuteValue(oXMLNode, "FaxAssignmentStatusID")
            '    oVehicleShopAssignment.AssignmentWorkStatus = GetXMLNodeattrbuteValue(oXMLNode, "WorkStatus")
            '    oVehicleShopAssignment.AssignmentDeductSent = GetXMLNodeattrbuteValue(oXMLNode, "EffectiveDeductibleSentAmt")
            '    oVehicleShopAssignment.AssignmentRemarks = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentRemarks")
            '    oVehicleShopAssignment.AssignmentTypeCD = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentTypeCD")
            '    oVehicleShopAssignment.ShopLocationID = GetXMLNodeattrbuteValue(oXMLNode, "ShopLocationID")
            '    oVehicleShopAssignment.ShopReferenceID = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")
            '    oVehicleShopAssignment.CurrentAssignmentTypeID = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentTypeID")
            '    oVehicleShopAssignment.AssignmentSequenceNumber = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentSequenceNumber")
            '    oVehicleShopAssignment.AssignmentSysLastUpdatedDate = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate")

            '    oXMLNode = oReturnXML.SelectSingleNode("ServiceChannel/Assignment/Shop")
            '    If oXMLNode IsNot Nothing Then
            '        oVehicleShopAssignment.AssignmentAssignTo = GetXMLNodeattrbuteValue(oXMLNode, "Name")
            '        oVehicleShopAssignment.AssignmentShopAddress1 = GetXMLNodeattrbuteValue(oXMLNode, "Address1")
            '        oVehicleShopAssignment.AssignmentShopAddress2 = GetXMLNodeattrbuteValue(oXMLNode, "Address2")
            '        oVehicleShopAssignment.AssignmentShopAddressCity = GetXMLNodeattrbuteValue(oXMLNode, "AddressCity")
            '        oVehicleShopAssignment.AssignmentShopAddressState = GetXMLNodeattrbuteValue(oXMLNode, "AddressState")
            '        oVehicleShopAssignment.AssignmentShopAddressZip = GetXMLNodeattrbuteValue(oXMLNode, "AddressZip")
            '        oVehicleShopAssignment.AssignmentShopMSACode = GetXMLNodeattrbuteValue(oXMLNode, "MSACode")
            '        oVehicleShopAssignment.AssignmentShopAddressCounty = GetXMLNodeattrbuteValue(oXMLNode, "AddressCounty")
            '        oVehicleShopAssignment.AssignmentShopPhone = GetXMLNodeattrbuteValue(oXMLNode, "PhoneAreaCode") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "PhoneExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "PhoneUnitNumber")
            '        oVehicleShopAssignment.AssignmentShopFax = GetXMLNodeattrbuteValue(oXMLNode, "FaxAreaCode") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "FaxExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "FaxUnitNumber")
            '        oVehicleShopAssignment.AssignmentShopContactEmail = GetXMLNodeattrbuteValue(oXMLNode, "EmailAddress")
            '        oVehicleShopAssignment.AssignmentPreferredCommunicationMethodID = GetXMLNodeattrbuteValue(oXMLNode, "PreferredCommunicationMethodID")
            '        oVehicleShopAssignment.AssignmentPreferredEstimatePackageID = GetXMLNodeattrbuteValue(oXMLNode, "PreferredEstimatePackageID")
            '        oVehicleShopAssignment.AssignmentPreferredCommunicationMethodName = hCommunicationMethodProfile(oVehicleShopAssignment.AssignmentPreferredCommunicationMethodID)
            '        oVehicleShopAssignment.AssignmentPreferredEstimatePackageName = hEstimatePackageProfile(oVehicleShopAssignment.AssignmentPreferredEstimatePackageID)
            '        oVehicleShopAssignment.ShopID = GetXMLNodeattrbuteValue(oXMLNode, "ShopID")
            '        oVehicleShopAssignment.BusinessID = GetXMLNodeattrbuteValue(oXMLNode, "BusinessID")

            '        If (Not IsNothing(oVehicleShopAssignment.AssignmentPrevDate)) Then
            '            oVehicleShopAssignment.AssignmentPrevDate = Format(CDate(oXMLNode.Attributes("LastAssignedDate").Value), "MM/dd/yyyy")
            '        End If

            '        '-----------------------------------------------------
            '        ' Load Vehicle Assignment data - Communication Method
            '        '-----------------------------------------------------
            '        'oVehicleShopAssignment.AssignmentPreferredCommunicationMethodID = oXMLNode.Attributes("PreferredCommunicationMethodID").InnerText
            '        'oVehicleShopAssignment.AssignmentPreferredEstimatePackageID = oXMLNode.Attributes("PreferredEstimatePackageID").InnerText

            '        '--------------------------------------------
            '        ' Load Vehicle Assignment data - ProgramType
            '        '--------------------------------------------
            '        If Not (oReturnXML.SelectSingleNode("ServiceChannel/Assignment/Shop/ShopLocationID") Is Nothing) Then
            '            sShopLocationID = GetXMLNodeattrbuteValue(oXMLNode, "ShopLocationID")
            '        Else
            '            sShopLocationID = ""
            '        End If

            '        If UCase(GetXMLNodeattrbuteValue(oXMLNode, "ProgramType")) = "LS" Then
            '            oVehicleShopAssignment.AssignmentProgramType = "Yes"
            '        ElseIf (UCase(GetXMLNodeattrbuteValue(oXMLNode, "ProgramType")) <> "LS" And sShopLocationID = "") Then
            '            oVehicleShopAssignment.AssignmentProgramType = ""
            '        ElseIf UCase(GetXMLNodeattrbuteValue(oXMLNode, "ProgramType")) <> "CEI" Then
            '            oVehicleShopAssignment.AssignmentProgramType = "CEI"
            '        Else
            '            oVehicleShopAssignment.AssignmentProgramType = "No"
            '        End If

            '        '---------------------------------------
            '        ' Shop Assignment Data 
            '        '---------------------------------------
            '        oXMLNode = oReturnXML.SelectSingleNode("ServiceChannel/Assignment/Appraiser")
            '        If oXMLNode IsNot Nothing Then
            '            oVehicleShopAssignment.AppraiserName = GetXMLNodeattrbuteValue(oXMLNode, "Name")
            '        End If

            '        '--------------------------------------------
            '        ' Load Vehicle Assignment data - ShopContact
            '        '--------------------------------------------
            '        oXMLNode = oReturnXML.SelectSingleNode("ServiceChannel/Assignment/Shop/ShopContact")
            '        oVehicleShopAssignment.AssignmentShopContact = GetXMLNodeattrbuteValue(oXMLNode, "Name") 'oXMLNode.Attributes("Name").InnerText
            '        oVehicleShopAssignment.AssignmentShopContactPhone = GetXMLNodeattrbuteValue(oXMLNode, "PhoneAreaCode") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "PhoneExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "PhoneUnitNumber")
            '        'oXMLNode.Attributes("PhoneAreaCode").InnerText & "-" & oXMLNode.Attributes("PhoneExchangeNumber").InnerText & "-" & oXMLNode.Attributes("PhoneUnitNumber").InnerText
            '    End If
            'Else
            '    '------------------------------------
            '    ' No Assignment set Vehicle Section
            '    ' ReadOnly
            '    '------------------------------------
            '    bPageReadOnly_Vehicle = True
            '    oVehicleShopAssignment.AssignmentStatus = "NoShopSelected"

            '    oVehicleShopAssignment.AssignmentID = ""
            '    oVehicleShopAssignment.AssignmentElecStatus = ""
            '    oVehicleShopAssignment.AssignmentStatusID = ""
            '    oVehicleShopAssignment.AssignmentFaxStatus = ""
            '    oVehicleShopAssignment.AssignmentFaxStatusID = ""
            '    oVehicleShopAssignment.AssignmentWorkStatus = ""
            '    oVehicleShopAssignment.AssignmentDeductSent = ""
            '    oVehicleShopAssignment.AssignmentRemarks = ""
            '    oVehicleShopAssignment.ShopLocationID = ""
            '    oVehicleShopAssignment.ShopReferenceID = ""
            '    oVehicleShopAssignment.CurrentAssignmentTypeID = ""
            '    oVehicleShopAssignment.AssignmentSequenceNumber = ""
            '    oVehicleShopAssignment.AssignmentSysLastUpdatedDate = ""

            '    oVehicleShopAssignment.AssignmentAssignTo = ""
            '    oVehicleShopAssignment.AssignmentShopAddress1 = ""
            '    oVehicleShopAssignment.AssignmentShopAddress2 = ""
            '    oVehicleShopAssignment.AssignmentShopAddressCity = ""
            '    oVehicleShopAssignment.AssignmentShopAddressState = ""
            '    oVehicleShopAssignment.AssignmentShopAddressZip = ""
            '    oVehicleShopAssignment.AssignmentShopPhone = ""
            '    oVehicleShopAssignment.AssignmentShopFax = ""
            '    oVehicleShopAssignment.AssignmentShopMSACode = ""
            '    oVehicleShopAssignment.AssignmentShopContactEmail = ""
            '    oVehicleShopAssignment.AssignmentPreferredCommunicationMethodID = ""
            '    oVehicleShopAssignment.AssignmentPreferredEstimatePackageID = ""

            '    oVehicleShopAssignment.AssignmentPrevDate = ""
            '    oVehicleShopAssignment.AssignmentProgramType = ""
            '    oVehicleShopAssignment.AssignmentShopContact = ""
            '    oVehicleShopAssignment.AssignmentShopContactPhone = ""

            '    oVehicleShopAssignment.CurrentDisposition = ""
            'End If

            ''------------------------------------------------------
            '' Shop Assignment History based on Multiple Assignments
            ''------------------------------------------------------
            'oXMLNodeList = oReturnXML.SelectNodes("ServiceChannel/Assignments")

            'oVehicleShopAssignment.DTAssignmentHistory = New DataTable()

            'oVehicleShopAssignment.DTAssignmentHistory.Columns.Add(New DataColumn("AssignmentID", GetType(String)))
            'oVehicleShopAssignment.DTAssignmentHistory.Columns.Add(New DataColumn("AssignmentTypeCD", GetType(String)))
            'oVehicleShopAssignment.DTAssignmentHistory.Columns.Add(New DataColumn("AppraiserName", GetType(String)))
            'oVehicleShopAssignment.DTAssignmentHistory.Columns.Add(New DataColumn("AssignmentDate", GetType(String)))
            'oVehicleShopAssignment.DTAssignmentHistory.Columns.Add(New DataColumn("CancellationDate", GetType(String)))
            'oVehicleShopAssignment.DTAssignmentHistory.Columns.Add(New DataColumn("SelectionDate", GetType(String)))

            'Dim DRAssignmentHistory As DataRow

            'If (oXMLNodeList.Count <> 0) Then
            '    For Each oXMLNode In oXMLNodeList
            '        DRAssignmentHistory = oVehicleShopAssignment.DTAssignmentHistory.NewRow

            '        DRAssignmentHistory("AssignmentID") = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentID")
            '        DRAssignmentHistory("AssignmentTypeCD") = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentTypeCD")
            '        DRAssignmentHistory("AppraiserName") = GetXMLNodeattrbuteValue(oXMLNode, "AppraiserName")
            '        DRAssignmentHistory("AssignmentDate") = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentDate")
            '        DRAssignmentHistory("CancellationDate") = GetXMLNodeattrbuteValue(oXMLNode, "CancellationDate")
            '        DRAssignmentHistory("SelectionDate") = GetXMLNodeattrbuteValue(oXMLNode, "SelectionDate")

            '        oVehicleShopAssignment.DTAssignmentHistory.Rows.Add(DRAssignmentHistory)
            '    Next
            'Else
            '    DRAssignmentHistory = oVehicleShopAssignment.DTAssignmentHistory.NewRow
            '    DRAssignmentHistory("AssignmentDate") = "No Records To Display"
            '    oVehicleShopAssignment.DTAssignmentHistory.Rows.Add(DRAssignmentHistory)

            'End If

            ''---------------------------------------------
            '' Load Vehicle Assignment data - Coverage
            ''---------------------------------------------
            'oXMLNode = oReturnXML.SelectSingleNode("ClaimCoveragesApplied")
            'oVehicleShopAssignment.ServiceChannelName = GetXMLNodeattrbuteValue(oXMLNode, "ServiceChannelName")
            ''oVehicleShopAssignment.DeductibleAppliedAmt = GetXMLNodeattrbuteValue(oXMLNode, "DeductibleAppliedAmt")

            ''---------------------------------------------
            '' Load Vehicle data - CommunicationMethod Key
            ''---------------------------------------------
            ''For Each sCommunicationMethodKey In hCommunicationMethodProfile
            ''    If sCommunicationMethodKey.key = oVehicleShopAssignment.AssignmentPreferredCommunicationMethodID Then
            ''        oVehicleShopAssignment.AssignmentPreferredCommunicationMethodID = sCommunicationMethodKey.value
            ''    End If
            ''Next

            '''----------------------------------------------------
            ''' Load Vehicle data - PreferredEstimatePackage Key
            '''----------------------------------------------------
            ''For Each sEstimatePackageKey In hEstimatePackageProfile
            ''    If sEstimatePackageKey.key = oVehicleShopAssignment.AssignmentPreferredEstimatePackageID Then
            ''        oVehicleShopAssignment.AssignmentPreferredEstimatePackageID = sEstimatePackageKey.value
            ''    End If
            ''Next

            '''-------------------------------
            ''' CRUD Permissions 
            '''-------------------------------
            ''Select Case sCRUD
            ''    Case "0000"
            ''        txtPolicyNumber.Enabled = False
            ''        txtClientClaimNumber.Enabled = False
            ''        txtLossDate.Enabled = False
            ''        ddlState.Enabled = False
            ''        txtLossDescription.Enabled = False
            ''        txtSubmittedBy.Enabled = False
            ''        txtIntakeFinishDate.Enabled = False
            ''        txtRemarks.Enabled = False
            ''    Case "1111"
            ''        txtPolicyNumber.Enabled = True
            ''        txtClientClaimNumber.Enabled = True
            ''        txtLossDate.Enabled = False
            ''        ddlState.Enabled = True
            ''        txtLossDescription.Enabled = True
            ''        txtSubmittedBy.Enabled = False
            ''        txtIntakeFinishDate.Enabled = False
            ''        txtRemarks.Enabled = True
            ''    Case Else
            ''        txtPolicyNumber.Enabled = False
            ''        txtClientClaimNumber.Enabled = False
            ''        txtLossDate.Enabled = False
            ''        ddlState.Enabled = False
            ''        txtLossDescription.Enabled = False
            ''        txtSubmittedBy.Enabled = False
            ''        txtIntakeFinishDate.Enabled = False
            ''        txtRemarks.Enabled = False
            ''End Select

            'Return oVehicleShopAssignment

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDShopAssignment", "ERROR", "Shop Assignment data Not found Or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
            oXMLNodeList = Nothing
        End Try
    End Function

    Protected Sub btnCoverageAddSave_Click(sender As Object, e As EventArgs) 'Handles btnCoverageAddSave.Click
        '    Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        '    Dim sLimitDailyAmt As String = 0
        '    Dim sMaximumDays As String = 0
        '    Dim aCoverageParams As Array = Nothing
        '    Dim sClaimCoverageID As String = ""
        '    Dim iAdditionalCoverageFlag As Integer = 0

        Try
            '        '-------------------------------
            '        ' Validate the web page data
            '        '-------------------------------  
            '        'If ddlCoverageDescription.SelectedValue = "" Then
            '        '    ' Nothing selected to add
            '        '    lblMsg.Text = "Select a coverage"
            '        'Else
            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim sStoredProcedure As String = "uspClaimAspectServiceChannelCoverageUpdDetail"

            '        sClaimCoverageID = "0"
            '        If UCase(txtCoverageAdditionalFlag.Text) = "YES" Then
            '            iAdditionalCoverageFlag = 1
            '        Else
            '            iAdditionalCoverageFlag = 0
            '        End If

            'Dim sParams As String = "@ClaimAspectServiceChannelID = " & sClaimAspectServiceChannelID _
            '             & ", @ClaimCoverageID = " & sClaimCoverageID _
            '             & ", @DocumentID = " & sDocumentID _
            '             & ", @DeductibleAppliedAmt = " & txtDeductible.value
            '             & ", @LimitAppliedAmt = " & txtLimit.value _
            '             & ", @PartialCoverageFlag = " & chkPartialCoverageFlag.value 
            '             & ", @UserID = " & sUserID _
            '             & ", @SysLastUpdatedDate = " & SysLastUpdatedDate

            '        '?????? Code This ?????
            '        '    oNode.setAttribute("AddlCoverageFlagInd", (txtAdditionalFlag.value == "Yes" ? "/images/tick.gif" : "/images/spacer.gif"));

            '        'Debug.Print(sParams)

            '        '-------------------------------
            '        ' Call WS and get data
            '        '-------------------------------
            '        oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '        '???? check for success and save new Coverage syslastupdateddate ????

            '        'response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)

            '        Page.ClientScript.RegisterStartupScript(Me.GetType, "myCloseScript", "window.opener.location.reload(true); window.close();", True)
            '        'End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("AddVehDACoverage_Save", "ERROR", "Add Coverage Save not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
        End Try
    End Sub

    'Protected Sub ddlCoverageDescription_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCoverageDescription.SelectedIndexChanged
    '    Dim aCoverageData As Array = Nothing
    '    Dim iCoverageTypeID As Integer = 0

    '    aCoverageData = ddlCoverageDescription.SelectedItem.Value.Split("|")
    '    txtCoverageClientCode.Text = aCoverageData(0)
    '    txtCoverageType.Text = aCoverageData(1)
    '    iCoverageTypeID = CInt(aCoverageData(2))
    '    txtCoverageAdditionalFlag.Text = aCoverageData(3)
    '    txtCoverageDailyLimit.Text = 0
    '    txtCoverageDeductible.Text = 0
    '    txtCoverageLimit.Text = 0
    '    txtCoverageMaxDays.Text = 0
    '    iSelectedCoverageTypeID = iCoverageTypeID

    '    '-------------------------
    '    ' Additional Flag 
    '    '-------------------------
    '    Select Case txtCoverageAdditionalFlag.Text
    '        Case "0"
    '            txtCoverageAdditionalFlag.Text = "No"
    '        Case "1"
    '            txtCoverageAdditionalFlag.Text = "Yes"
    '    End Select

    '    '-------------------------
    '    ' Rental 
    '    '-------------------------
    '    If UCase(txtCoverageClientCode.Text) = "RENT" Then
    '        lblRentalMax.Visible = True
    '        lblRentalLimit.Visible = True
    '        txtCoverageMaxDays.Visible = True
    '        txtCoverageDailyLimit.Visible = True
    '    Else
    '        lblRentalMax.Visible = False
    '        lblRentalLimit.Visible = False
    '        txtCoverageMaxDays.Visible = False
    '        txtCoverageDailyLimit.Visible = False
    '    End If
    'End Sub

    Protected Function GetXMLNodeattrbuteValue(ByVal XMLnode As XmlNode, strAttributes As String) As String
        Try
            Dim strRetVal As String = ""
            If Not XMLnode.Attributes(strAttributes) Is Nothing Then
                strRetVal = XMLnode.Attributes(strAttributes).InnerText
            Else
                strRetVal = ""
            End If
            Return strRetVal
        Catch ex As Exception
            wsAPDFoundation.LogEvent("GetXMLNodeattrbuteValue", "XMLERROR", "GetXMLNodeattrbuteValue - Parse: " & Date.Now, "ERROR: Parsing XMLNode Attribute.", "XMLNodeAttribute = " & strAttributes)
            Return ex.ToString
        End Try
    End Function

    Protected Sub ddlCoverage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCoverage.SelectedIndexChanged
        Dim sCoverageID As String = ""
        Dim aCoverageApplied() As String = Nothing

        sCoverageID = ddlCoverage.SelectedValue.Replace("|", "")
        If hCoverageApplied.ContainsKey(sCoverageID) Then
            aCoverageApplied = hCoverageApplied(sCoverageID).split("|")
            txtTotalDeductible.Text = aCoverageApplied(0)
        End If

    End Sub
End Class