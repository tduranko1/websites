﻿'--------------------------------------------------------------
' Program: VehicleShopAssignment
' Author:  Thomas Duranko
' Date:    May 28, 2013
' Version: 1.0
'
' Description:  This site provides functions for APD replacing the 
'               existing COM based classic asp functions. 
'--------------------------------------------------------------
Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Public Class VehicleShopAssignment
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    'Dim APDDBUtils As New APDDBUtilities

    '--------------------------------------------------------------
    ' Page Load
    '--------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '-------------------------------
        ' Globals
        '-------------------------------
        Dim sUserID As String = ""
        Dim sLynxID As String = ""
        Dim sInsuranceCompanyID As String = ""
        Dim sParams As String = ""
        Dim sClaimAspectServiceChannelID As String = ""
        Dim sClaimStatus As String = ""
        Dim sPageReadOnly As String = ""
        Dim sEvtVeicleAssignmentSentToShop As String = ""
        Dim sEvtAppraiserAssignmentSent As String = ""
        Dim sAssignmentSequenceNumber As String = ""
        Dim oSession As Object
        Dim XMLReturnElem As XmlElement
        Dim sXslHash As New Hashtable

        '-------------------------------
        ' Session/Local Variables
        '-------------------------------
        Dim sStoredProcedure As String = "uspVehicleAssignGetDetailWSXML"
        Dim sXSLProcedure As String = "VehicleShopAssignment.xsl"

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        Dim sSessionKey = Request.QueryString("SessionKey")
        Dim sWindowID = Request.QueryString("WindowID")

        Try
            If sSessionKey = "" Then
                Throw New SystemException("SessionValidationError: No session information received from classic ASP page.")
            Else
                '---------------------------------
                ' Get configuration data
                '---------------------------------
                XMLReturnElem = wsAPDFoundation.GetConfigXMLData("Root/Application/WorkFlow/EventCodes/Code[@name='VehicleAssignmentSentToShop']")
                If XMLReturnElem.InnerXml.Contains("ERROR:") Then
                    Throw New SystemException("ConfigXMLError: Could not find the VehicleAssignmentSentToShop configuration variable.")
                Else
                    sEvtVeicleAssignmentSentToShop = XMLReturnElem.InnerText
                End If

                XMLReturnElem = wsAPDFoundation.GetConfigXMLData("Root/Application/WorkFlow/EventCodes/Code[@name='AppraiserAssignmentSent']")
                If XMLReturnElem.InnerXml.Contains("ERROR:") Then
                    Throw New SystemException("ConfigXMLError: Could not find the AppraiserAssignmentSent configuration variable.")
                Else
                    sEvtAppraiserAssignmentSent = XMLReturnElem.InnerText
                End If

                '---------------------------------
                ' Get the session data
                '---------------------------------
                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "UserID-" & sWindowID)
                sUserID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "LynxID-" & sWindowID)
                sLynxID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "InsuranceCompanyID-" & sWindowID)
                sInsuranceCompanyID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "pageReadOnly-" & sWindowID)
                sPageReadOnly = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "ClaimAspectServiceChannelID-" & sWindowID)
                sClaimAspectServiceChannelID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "ClaimStatus-" & sWindowID)
                sClaimStatus = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "AssignmentSequenceNumber-" & sWindowID)
                sAssignmentSequenceNumber = oSession(0).SessionValue

                '-----------------------------------
                ' Add config variables as XslParams
                '-----------------------------------
                sXslHash.Add("EventVehicleAssignmentSentToShopID", sEvtVeicleAssignmentSentToShop)
                sXslHash.Add("EventAppraiserAssignmentSent", sEvtAppraiserAssignmentSent)
                sXslHash.Add("LynxID", sLynxID)
                sXslHash.Add("UserId", sUserID)
                sXslHash.Add("WindowID", sWindowID)
                sXslHash.Add("pageReadOnly", sPageReadOnly)
                sXslHash.Add("AssignmentSequenceNumber", sAssignmentSequenceNumber)
                sXslHash.Add("ClaimStatus", sClaimStatus)

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(AppSettings("Debug")) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls.", "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID)
                End If

                '---------------------------------
                ' XSL Transformation
                '---------------------------------
                sParams = sClaimAspectServiceChannelID
                sParams += ", " & sInsuranceCompanyID

                'Response.Write(APDDBUtils.DataPresenterNet(sStoredProcedure, sXSLProcedure, sParams, sXslHash))
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "APDNet Failed: Session controls not found or expired (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls not found or expired.", "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID)

            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        End Try
    End Sub

End Class