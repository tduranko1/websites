﻿Imports System.Xml

Public Class AddService
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    'Dim sCRUD As String = ""
    'Dim iLynxID As Integer = 0
    Dim sInsuranceCompanyID As String = ""
    Dim sClaimAspectID As String = ""
    'Dim iSelectedCoverageTypeID As Integer
    Dim sUserID As String = ""
    Dim sServiceChannels As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim oXMLParentNodeList As XmlNodeList = Nothing
        Dim oXMLParentNode As XmlNode = Nothing
        Dim sDDLParams As String = ""
        Dim iClientServiceChannel As Integer = 0
        Dim isServiceExsist As Boolean = False
        'Dim iClientCoverageType As Integer = 0
        'Dim DTCoverage As New DataTable
        'Dim DCCoverage As New DataColumn
        'Dim sDDLParams As String = ""

        Try
            '-------------------------------
            ' Check Session Data
            '-------------------------------
            sClaimAspectID = Request.QueryString("ClaimAspectID")
            sInsuranceCompanyID = Request.QueryString("InscCompID")
            sUserID = Request.QueryString("UserID")
            sServiceChannels = Request.QueryString("ServiceChannels")

            '    'txtLynxID.Text = CStr(iLynxID)

            '    '-------------------------------
            '    ' Process Validation
            '    '-------------------------------
            '    '??? Verify we got good params

            '    '-------------------------------
            '    ' Apply CRUD
            '    '-------------------------------

            '    '-------------------------------
            '    ' Process Main Code
            '    '-------------------------------
            '-------------------------------
            ' Database access
            '-------------------------------  
            Dim sStoredProcedure As String = "uspClaimVehicleGetDetailwsXML"
            Dim sParams As String = "@ClaimAspectID=" & sClaimAspectID & ", @InsuranceCompanyID=" & sInsuranceCompanyID

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '---------------------------------------
            ' Load Current Coverages
            '---------------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("Reference")
            oXMLParentNodeList = oReturnXML.SelectNodes("Vehicle/ClaimAspectServiceChannel")
            '--------------------------------
            ' Populate Coverage Add/Edit
            ' Description Dropdown
            '--------------------------------
            If Not IsPostBack Then
                For Each oXMLNode In oXMLNodeList
                    '---------------------------------------
                    ' Load Reference data - AssignmentType
                    '---------------------------------------
                    If iClientServiceChannel = 0 Then
                        ddlNewServiceChannel.Items.Add("")
                        iClientServiceChannel += 1
                    End If

                    If oXMLNode.Attributes("List").InnerText = "ClientServiceChannels" Then
                        For Each oXMLParentNode In oXMLParentNodeList
                            '-- Remove Already Added ServiceChannels
                            If oXMLParentNode.Attributes("ServiceChannelCD").Value = oXMLNode.Attributes("ReferenceID").Value Then
                                isServiceExsist = True
                            End If
                        Next
                        If isServiceExsist <> True Then
                            sDDLParams = oXMLNode.Attributes("ReferenceID").Value & "|" & oXMLNode.Attributes("Name").Value
                            ddlNewServiceChannel.Items.Insert(iClientServiceChannel, New ListItem(oXMLNode.Attributes("ReferenceID").Value & " - " & oXMLNode.Attributes("Name").InnerText, sDDLParams))
                            iClientServiceChannel += 1
                        End If
                        isServiceExsist = False
                    End If
                Next
            End If


        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDServiceChannel", "ERROR", "Service Channel data not found or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
            'Return ""
        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
            oXMLNodeList = Nothing
        End Try
    End Sub

    Protected Sub btnNewServiceChannel_ADD_Click(sender As Object, e As EventArgs) Handles btnNewServiceChannel_ADD.Click
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim aServiceChannelParams As Array = Nothing
        Dim sServiceChannelID As String = ""
        Dim iPrimaryFlag As Boolean = 0
        Dim iPrimary As String = ""

        Try
            iPrimaryFlag = chkNewServiceChannelPrimary.Checked
            If iPrimaryFlag = True Then
                iPrimary = 1
            Else
                iPrimary = 0
            End If
            aServiceChannelParams = ddlNewServiceChannel.SelectedValue.ToString().Split("|")
            sServiceChannelID = aServiceChannelParams(0)
            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim sStoredProcedure As String = "uspClaimVehicleAddServiceChannel"

            Dim sParams As String = "@ClaimAspectID = " & sClaimAspectID _
                & ", @ServiceChannelCD = '" & sServiceChannelID & "'" _
                & ", @PrimaryFlag = " & iPrimary _
                & ", @UserID = " & sUserID _

            '?????? Code This ?????
            '    oNode.setAttribute("AddlCoverageFlagInd", (txtAdditionalFlag.value == "Yes" ? "/images/tick.gif" : "/images/spacer.gif"));

            'Debug.Print(sParams)

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '???? check for success and save new Coverage syslastupdateddate ????

            'response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)

            Page.ClientScript.RegisterStartupScript(Me.GetType, "myCloseScript", "window.opener.location.reload(true); window.close();", True)
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("CoverageAddSaveDetails", "ERROR", "Add Coverage Save not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
        End Try
    End Sub

    'Protected Sub ddlCoverageDescription_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCoverageDescription.SelectedIndexChanged
    '    Dim aCoverageData As Array = Nothing
    '    Dim iCoverageTypeID As Integer = 0

    '    aCoverageData = ddlCoverageDescription.SelectedItem.Value.Split("|")
    '    txtCoverageClientCode.Text = aCoverageData(0)
    '    txtCoverageType.Text = aCoverageData(1)
    '    iCoverageTypeID = CInt(aCoverageData(2))
    '    txtCoverageAdditionalFlag.Text = aCoverageData(3)
    '    txtCoverageDailyLimit.Text = 0
    '    txtCoverageDeductible.Text = 0
    '    txtCoverageLimit.Text = 0
    '    txtCoverageMaxDays.Text = 0
    '    iSelectedCoverageTypeID = iCoverageTypeID

    '    '-------------------------
    '    ' Additional Flag 
    '    '-------------------------
    '    Select Case txtCoverageAdditionalFlag.Text
    '        Case "0"
    '            txtCoverageAdditionalFlag.Text = "No"
    '        Case "1"
    '            txtCoverageAdditionalFlag.Text = "Yes"
    '    End Select

    '    '-------------------------
    '    ' Rental 
    '    '-------------------------
    '    If UCase(txtCoverageClientCode.Text) = "RENT" Then
    '        lblRentalMax.Visible = True
    '        lblRentalLimit.Visible = True
    '        txtCoverageMaxDays.Visible = True
    '        txtCoverageDailyLimit.Visible = True
    '    Else
    '        lblRentalMax.Visible = False
    '        lblRentalLimit.Visible = False
    '        txtCoverageMaxDays.Visible = False
    '        txtCoverageDailyLimit.Visible = False
    '    End If
    'End Sub

End Class