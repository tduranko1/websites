﻿'--------------------------------------------------------------
' Program: APD Document Upload
' Author:  Thomas Duranko
' Date:    Oct 2, 2013
' Version: 1.0
'
' Description:  This site provides the Document Upload for APD
'--------------------------------------------------------------
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Public Class DocumentUpload
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    Public sLynxID As String = ""
    Public sUserID As String = ""
    Public sInsuranceCompanyID As String = ""
    Public sSessionKey As String = ""
    Public sWindowID As String = ""
    Public sParams As String = ""

    '--------------------------------------------------------------
    ' Page Load
    '--------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '-------------------------------
        ' Globals
        '-------------------------------
        Dim sXslHash As New Hashtable
        Dim sASPParams = Request.QueryString("Params")
        Dim oSession As Object
        Dim sAPDNetDebugging As String = ""
        Dim dtStart As Date
        Dim APDDBUtils As APDDBUtilities = Nothing

        'btnViewDoc.Attributes.Add("onclick", "viewDoc()")

        '-------------------------------
        ' Session/Local Variables
        '-------------------------------
        Dim sStoredProcedure As String = ""
        Dim sXSLProcedure As String = ""

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        sSessionKey = Request.QueryString("SessionKey")
        sWindowID = Request.QueryString("WindowID")
        sParams = Request.QueryString("Params")

        Try
            If sSessionKey = "" Then
                Throw New SystemException("SessionValidationError: No session information received from classic ASP page.")
            Else
                '---------------------------------
                ' New Session Data
                '---------------------------------
                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "LynxID-" & sWindowID)
                sLynxID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "UserID-" & sWindowID)
                sUserID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "APDNetDebugging-" & sWindowID)
                sAPDNetDebugging = ""
                If UCase(oSession(0).SessionValue) = "TRUE" Or UCase(AppSettings("Debug")) = "TRUE" Then
                    sAPDNetDebugging = "TRUE"
                End If

                APDDBUtils = New APDDBUtilities()
                dtStart = Date.Now

                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls.", " LynxID: " & sLynxID & "UserID: " & sUserID & "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
                End If

                '------------------------------------
                ' If ASPParms, parse and add to hash
                '------------------------------------
                If sASPParams <> "" Then
                    Dim aParams As Array
                    aParams = Split(sASPParams, "|")
                    For Each sASPParam In aParams
                        Dim aValues As Array
                        aValues = Split(sASPParam, ":")
                        sXslHash.Add(aValues(0), aValues(1))
                        'Response.Write(aValues(0) & " - " & aValues(1) & "<br/>")
                    Next
                End If

                '-------------------------------
                ' Get Session Controls from
                ' Classic ASP page
                '-------------------------------
                sLynxID = sXslHash("LynxID")
                sUserID = sXslHash("UserID")
                sInsuranceCompanyID = sXslHash("InsuranceCompanyID")

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls/Parameters." _
                                             , "Session controls for Window: " & sWindowID _
                                             & " and SessionKey: " & sSessionKey _
                                             , "Vars: StoredProc = " & sStoredProcedure _
                                             & ", XSLPage = " & sXSLProcedure _
                                             & ", UserID(Session) = " & sUserID _
                                             & ", LynxID(Session) = " & sLynxID _
                                             & ", InsuranceCompanyID = " & sInsuranceCompanyID _
                                             & ", sASPParams = " & sASPParams _
                                             )
                End If

                'Response.Write(sSessionKey)
                'Response.Write("<br/>")
                'Response.Write(sWindowID)
                'Response.Write("<br/>")
                'Response.Write(sASPParams)
                'Response.Write("<br/>")
                'Response.Write(sLynxID)
                'Response.Write("<br/>")
                'Response.Write(sUserID)
                'Response.Write("<br/>")
                'Response.Write(sInsuranceCompanyID)
                'Response.Write("<br/>")
                'Response.Write(sParams)
                'Response.Write("<br/>")
                'Response.End()

            End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            'wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls/parameter not found or page pre-transformation failed.", "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID)

            Response.Write(sError)
            Response.End()
            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        End Try
    End Sub

End Class