﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UpdateCarrierRep.aspx.vb" Inherits="APDNet.UpdateCarrierRep" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Update Carrier Rep</title>
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />

    <!-- ============== JQuery - Loader =============== -->      
    <script type="text/javascript" src="js/jQuery/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="js/jQuery/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jQuery/ClaimTabs.js"></script>

    <script type="text/javascript">
  $(document).ready(function () {
                if ($("#ddlCarrierRep").val() == "" || $("#ddlCarrierRep").val() == null) {
                    $("#btnCarrierRepOK").attr("disabled", "disabled");
                }
                else {
                    $("#btnCarrierRepOK").removeAttr("disabled", "disabled");
                }
 if (window.opener != null && !window.opener.closed) {
                    if (typeof window.opener.LoadModalDiv == 'function' || typeof window.opener.HideModalDiv == 'object') {
                        window.opener.LoadModalDiv();
                    }
                }
            });
 $(window).on("beforeunload", function () {
            if (window.opener != null && !window.opener.closed) {
                if (typeof window.opener.HideModalDiv == 'function' || typeof window.opener.HideModalDiv == 'object') {
                    window.opener.HideModalDiv();
                }
            }
});
        function OnClose() {
            //alert("Here");
            if (window.opener != null && !window.opener.closed) {
                window.opener.HideModalDiv();
            }
            window.returnValue = "false";
            window.close();
        }

        function doCancel() {
            OnClose();
            //window.returnValue = "false";
            //window.close();
        }
    </script>
    <link rel="stylesheet" href="css/jquery-ui.css" />
</head>
<body>
    <form id="frmUpdateCarrierRep" runat="server">
    <div id="diaTitleBar" title="Carrier Rep" >
        <!-- ==================== Carrier Rep - Modal Dialog ============================== -->    
        <div id="diaCarrierRepx" title="Carrier Representative" >
            <table style="border-spacing: 0px; width: 360px;">
                <tr style="vertical-align: top; ">
                    <td style="width: 95px;">
                        <b>LynxID:</b>
                    </td>
                    <td>
                        <asp:TextBox ID="txtLynxID" runat="server" CssClass="inputField" Enabled="false" />
                    </td>
                </tr>                
                <tr style="vertical-align: top;">
                    <td style="width: 95px;">
                        <b>Carrier Office:</b>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCarrierOffice" runat="server" CssClass="inputField" Width="275px" AutoPostBack="True" ></asp:DropDownList>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="width: 95px;">
                        <b>Carrier Rep:</b>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCarrierRep" runat="server" CssClass="inputField" Width="275px" ></asp:DropDownList>
                    </td>
                </tr>
            </table>
                    
            <asp:Button runat="server" ID="btnCarrierRepOK" Text="OK" class="formbutton"/>
            <asp:Button runat="server" ID="btnCarrierRepCancel" Text="Cancel" class="formbutton" OnClientClick="javascript:doCancel(); return false;" />
        </div>
    </div>
    </form>
</body>
</html>