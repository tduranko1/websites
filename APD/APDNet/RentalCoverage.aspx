﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RentalCoverage.aspx.vb" Inherits="APDNet.RentalCoverage" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rental Coverage</title> 
    <base target="_self" />
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />

    <!-- ============== JQuery - Loader =============== -->
    <script type="text/javascript" src="js/jQuery/jquery-1.11.1.js"></script>
<script type="text/javascript" src="js/jQuery/jquery-ui.js"></script>
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <%--    <script type="text/javascript" src="Scripts/ClaimTabs.js"></script>
    <script type="text/javascript" src="Scripts/APDData.js"></script>--%>

    <script type="text/javascript">
    $(document).ready(function () {
$('#txtRentalAuthPickup').datepicker();
$('#txtRentalStartDate').datepicker();
$('#txtRentalEndDate').datepicker();
});
 $( window ).unload(function() {
        window.opener.HideModalDiv();
    });
        function OnClose() {
            //alert("Here");
            if (window.opener != null && !window.opener.closed) {
                window.opener.HideModalDiv();
            }
            window.returnValue = "false";
            window.close();
        }

        function doCancel() {
            OnClose();
            //window.returnValue = "false";
            //window.close();
        }

        function dateDiffInDays(startDate, endDate) {
            var date1 = new Date(startDate);
            var date2 = new Date(endDate);
            var diffDays = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));

            return(diffDays);
        }

        //--- ReCalculate Rentals ---//
        function reCalculateRentals() {
            var rentalRate = parseFloat(Number($("#txtRentalRate").val()).toFixed(2));
            var taxRate = parseFloat($("#txtRentalTaxRate").val() / 100);
            taxRate = parseFloat((taxRate * rentalRate));
            var taxedRate = Number(parseFloat(taxRate + rentalRate)).toFixed(2);
            var rentalDollars = Number($("#txtRentalNoOfDays").val() * $("#txtRentalTaxedRate").val()).toFixed(2)

            var rentalStartDate = $("#txtRentalStartDate").val();
            var rentalEndDate = $("#txtRentalEndDate").val();

            $("#txtRentalTaxedRate").val(taxedRate);
            $("#txtRentalNoOfDays").val(dateDiffInDays(rentalStartDate, rentalEndDate) + 1);
            $("#txtRentalDollars").val(rentalDollars);

            if ($("#txtRentalNoOfDays").val() === "NaN") {
                $("#txtRentalNoOfDays").val("0");
            }

            if ($("#txtRentalDollars").val() === "NaN" || $("#txtRentalDollars").val() < 0 && $("#txtRentalTaxedRate").val() === "NaN") {
                $("#txtRentalDollars").val("0");
            } else {
                $("#txtRentalDollars").val($("#txtRentalNoOfDays").val() * $("#txtRentalTaxedRate").val());
                var dRentalDollars;
                dRentalDollars = (Math.round($("#txtRentalDollars").val() * 100) / 100).toString();
                $("#txtRentalDollars").val(dRentalDollars);
            }

            if ($("#txtRentalRepairStart").val() === "" || $("#txtRentalRepairEnd").val() === "") {
            } else {
                $("#txtRentalRepairCycle").val(dateDiffInDays($("#txtRentalRepairStart").val(), $("#txtRentalRepairEnd").val()));
            }


            if (parseInt($("#txtRentalNoOfDays").val()) > parseInt($("#txtRentalRepairCycle").val())) {
                //alert("Rental Days cannot exceed the number of Repair Cycle days.");
                $("#txtRentalNoOfDays").css('color', 'red')
                $("#lblMsg").text("* Rental Days cannot exceed the number of Repair Cycle days.");
                $("#btnRentalSave").prop('disabled', true);
            } 

            if (parseInt(dateDiffInDays(rentalStartDate, rentalEndDate) + 1) < 0) {
                $("#txtRentalNoOfDays").css('color', 'red')
                $("#lblMsg").text("* Rental End date cannot be before the start date.");
                $("#btnRentalSave").prop('disabled', true);
            }

            if (parseInt(dateDiffInDays(rentalStartDate, rentalEndDate) + 1) > 0 && parseInt($("#txtRentalNoOfDays").val()) <= parseInt($("#txtRentalRepairCycle").val())) {
                $("#txtRentalNoOfDays").css('color', 'black')
                $("#lblMsg").text("");
            }

            //if (parseInt(dateDiffInDays(rentalStartDate, rentalEndDate) + 1) > 0 && parseInt($("#txtRentalNoOfDays").val()) <= parseInt($("#txtRentalRepairCycle").val())) {
            if (parseInt($("#txtRentalNoOfDays").val()) > 0 && parseInt($("#txtRentalNoOfDays").val()) > 0) {
                $("#btnRentalSave").prop('disabled', false);
            }

            '--- Populate hidden fields ---'
            $("#hidtxtRentalDollars").val($("#txtRentalDollars").val());
            $("#hidtxtRentalTaxedRate").val($("#txtRentalTaxedRate").val());
            $("#hidtxtRentalNoOfDays").val($("#txtRentalNoOfDays").val());
        }

        //-----------------------------//
        // On change of Rental Rate    //
        // or Tax Rate recalculate     //
        // the Taxed Rate and Rental   //
        //-----------------------------//
        $(function () {
            $("#txtRentalRate").change(function () {
                reCalculateRentals();
            });
        });

        $(function () {
            $("#txtRentalTaxRate").change(function () {
                reCalculateRentals();
            });
        });

        //-----------------------------//
        // Calculate Rental Days       //
        //-----------------------------//
        $(function () {
            $("#txtRentalStartDate").change(function () {
                reCalculateRentals();
            });
        })

        $(function () {
            $("#txtRentalEndDate").change(function () {
                reCalculateRentals();
            });
        })

        $(function () {
            $("#btnRentalUpdate").click(function () {
                reCalculateRentals();
                reCalculateRentals();
            });
        })

        function getTotalDeductible() {
            $("#txtTotalDeductible").prop('disabled', false);
            $("#txtTotalDedApplied").prop('disabled', false);

            var iTotalDeductible = 0;
            var iTotalDeductibleApplied = 0;

        }

        //-----------------------------//
        // Warranty Claim              //
        //-----------------------------//
        $(function () {
            $("#chkWarranty").click(function () {
                if (this.checked) {
                    //-- Set Repair Start/End to Warranty Date
                    $("#txtRentalRepairStart").val($("#hidWarrantyStart").val());
                    $("#txtRentalRepairEnd").val($("#hidWarrantyEnd").val());
                    reCalculateRentals();
                } else {
                    //-- Set Repair Start/End to Repair Date
                    $("#txtRentalRepairStart").val($("#hidRepairStart").val());
                    $("#txtRentalRepairEnd").val($("#hidRepairEnd").val());
                    reCalculateRentals();
                }
            });
        })


        //$(function () {
        //    $("#gvRentals").click(function () {
        //        reCalculateRentals();
        //        reCalculateRentals();
        //    });
        //})

    </script>

<style type="text/css">
  .hiddencol
  {
    display: none;
  }
</style>

</head>
<body>
    <form id="frmRentalCoverage" runat="server">
        <div id="diaRentalCoverage" title="Rental Coverage">

                    <table class="topTable" style="border-spacing: 0px; width: 800px;">
                        <tr style="vertical-align: top; ">
                            <td style="width: 200px; font-size: medium; padding-bottom: 5px;">
                                Rental Management
                            </td>
                        </tr>         
                    </table>
        <table class="topTable" style="border-spacing: 0px; width: 600px;">
            <tr style="vertical-align: top; ">
                <td>
                    <table class="topTable" style="border-spacing: 0px; width: 440px;">
                        <tr style="vertical-align: top; ">
                            <td>
                                <hr />
                            </td>
                        </tr>         
                    </table>
                    <table class="topTable" style="border-spacing: 0px; width: 333px;">
                        <tr style="vertical-align: top; ">
                            <td style="width: 100px;">
                                LynxID:
                            </td>
                            <td style="width: 100px;">
                                <asp:TextBox ID="txtRentalLynxID" runat="server" CssClass="inputField" Enabled="false" Width="60px" MaxLength="5" />
                            </td>
                            <td style="width: 100px;">
                                Party:
                            </td>
                            <td style="width: 100px;">
                                <asp:TextBox ID="txtRentalParty" runat="server" CssClass="inputField" Enabled="false" Width="25px" MaxLength="5" />
                            </td>
                            <td style="width: 100px;">
                                Coverage:
                            </td>
                            <td style="width: 100px;">
                                <asp:TextBox ID="txtRentalCoverage" runat="server" CssClass="inputField" Enabled="false" Width="60px" MaxLength="5" />
                            </td>
                        </tr>         
                    </table>
                    <table class="topTable" style="border-spacing: 0px; width: 341px;">
                        <tr style="vertical-align: top; ">
                            <td style="width: 47px;">
                                Insured:
                            </td>
                            <td style="width: 300px;">
                                <asp:TextBox ID="txtRentalInsured" runat="server" CssClass="inputField" Enabled="false" Width="274px" MaxLength="5" />
                            </td>
                        </tr>         
                        <tr style="vertical-align: top; ">
                            <td style="width: 47px;">
                                Renter:
                            </td>
                            <td style="width: 300px;">
                                <asp:TextBox ID="txtRentalClaimant" runat="server" CssClass="inputField" Enabled="false" Width="274px" MaxLength="5" />
                            </td>
                        </tr>         
                    </table>
                    <table class="topTable" style="border-spacing: 0px; width: 440px;">
                        <tr style="vertical-align: top; ">
                            <td>
                                <hr />
                            </td>
                        </tr>         
                    </table>
                    <table class="topTable" style="border-spacing: 0px; ">
                        <tr style="vertical-align: top; ">
                            <td style="width: 45px;">
                                Year:
                            </td>
                            <td style="width: 120px;">
                                <asp:TextBox ID="txtRentalYear" runat="server" CssClass="inputField" Enabled="false" Width="30px" MaxLength="5" />
                            </td>
                            <td style="width: 80px;">
                                Daily $ Max:
                            </td>
                            <td style="width: 100px;">
                                <asp:TextBox ID="txtRentalDailyMax" runat="server" CssClass="inputField" Enabled="false" Width="60px" MaxLength="5" />
                            </td>
                        </tr>
                        <tr style="vertical-align: top; ">
                            <td style="width: 45px;">
                                Make: 
                            </td>
                            <td style="width: 120px;">
                                <asp:TextBox ID="txtRentalMake" runat="server" CssClass="inputField" Enabled="false" Width="100px" MaxLength="5" />
                            </td>
                            <td style="width: 80px;">
                                Rental $ Max:
                            </td>
                            <td style="width: 100px;">
                                <asp:TextBox ID="txtRentalMax" runat="server" CssClass="inputField" Enabled="false" Width="60px" MaxLength="5" />
                            </td>
                        </tr>         
                        <tr style="vertical-align: top; ">
                            <td style="width: 45px;">
                                Model:
                            </td>
                            <td style="width: 120px;">
                                <asp:TextBox ID="txtRentalModel" runat="server" CssClass="inputField" Enabled="false" Width="100px" MaxLength="5" />
                            </td>
                            <td style="width: 80px;">
                                Days Max:
                            </td>
                            <td style="width: 100px;">
                                <asp:TextBox ID="txtDaysMax" runat="server" CssClass="inputField" Enabled="false" Width="30px" MaxLength="5" />
                            </td>
                        </tr>         
                    </table>

                    <table class="topTable" style="border-spacing: 0px; width: 440px;">
                        <tr style="vertical-align: top; ">
                            <td>
                                <hr />
                            </td>
                        </tr>         
                    </table>

                    <table class="topTable" style="border-spacing: 0px; width: 350px;">
                        <tr style="vertical-align: top; ">
                            <td>
                                <table class="topTable" style="border-spacing: 0px; width: 356px;">
                                    <tr style="vertical-align: top; ">
                                        <td style="width: 200px;">
                                            RENTAL INSTRUCTIONS:
                                        </td>
                                        <td style="width: 90px;">
                                            Days Authorized:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtRentalDaysAuthorized" runat="server" CssClass="inputField" Enabled="false" Width="25px" MaxLength="5" />
                                        </td>
                                    </tr>         
                                </table>
                                <table class="topTable" style="border-spacing: 0px;">
                                    <tr style="vertical-align: top; ">
                                        <td style="width: 430px;">
                                            <asp:TextBox ID="txtRentalInstructions" runat="server" CssClass="inputField" Enabled="false" Width="100%" MaxLength="250" Height="60px" TextMode="MultiLine" />
                                        </td>
                                    </tr>         
                                </table>
                                <table class="topTable" style="border-spacing: 0px;">
                                    <tr style="vertical-align: top; ">
                                        <td style="width: 87px;">
                                            Date of Loss:
                                        </td>
                                        <td style="width: 105px;">
                                            <asp:TextBox ID="txtRentalDateOfLoss" runat="server" CssClass="inputField" Enabled="false" Width="75px" MaxLength="5" />
                                        </td>
                                        <td style="width: 50px;">
                                            Drivable:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtRentalDrivable" runat="server" CssClass="inputField" Enabled="false" Width="25px" MaxLength="5" />
                                        </td>
                                        <td style="width: 60px;">
                                            &nbsp; Warranty:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtWarranty" runat="server" CssClass="inputField" Enabled="false" Width="25px" MaxLength="5" />
                                        </td>
                                    </tr>         
                                </table>
                                <table class="topTable" style="border-spacing: 0px; width: 440px;">
                                    <tr style="vertical-align: top; ">
                                        <td>
                                            <hr />
                                        </td>
                                    </tr>         
                                </table>
                                <table class="topTable" style="border-spacing: 0px;">
                                    <tr style="vertical-align: top; ">
                                        <td style="width: 70px;">
                                            Max Rental:
                                        </td>
                                        <td style="width: 5px;">
                                            <b>$</b>
                                        </td>
                                        <td style="width: 85px;">
                                            <asp:TextBox ID="txtRentalMaxRentals" runat="server" CssClass="inputField" Enabled="false" Width="75px" MaxLength="5" />
                                        </td>
                                        <td style="width: 80px;">
                                            Max Days:
                                        </td>
                                        <td style="width: 35px;">
                                            <asp:TextBox ID="txtRentalMaxDays" runat="server" CssClass="inputField" Enabled="false" Width="25px" MaxLength="5" />
                                        </td>
                                        <td style="width: 110px;">
                                            Total Repair Cycle:
                                        </td>
                                        <td style="width: 40px;">
                                            <asp:TextBox ID="txtRentalTotalRepairCycle" runat="server" CssClass="inputField" Enabled="false" Width="25px" MaxLength="5" />
                                        </td>
                                    </tr>         
                                    <tr style="vertical-align: top; ">
                                        <td style="width: 70px;">
                                            Total Rental:
                                        </td>
                                        <td style="width: 5px;">
                                            <b>$</b>
                                        </td>
                                        <td style="width: 85px;">
                                            <asp:TextBox ID="txtRentalTotal" runat="server" CssClass="inputField" Enabled="false" Width="75px" MaxLength="5" />
                                        </td>
                                        <td style="width: 80px;">
                                            Rental Days:
                                        </td>
                                        <td style="width: 35px;">
                                            <asp:TextBox ID="txtRentalDays" runat="server" CssClass="inputField" Enabled="false" Width="25px" MaxLength="5" />
                                        </td>
                                        <td style="width: 110px;">
                                            Total Rental Days:
                                        </td>
                                        <td style="width: 40px;">
                                            <asp:TextBox ID="txtRentalTotalRentalDays" runat="server" CssClass="inputField" Enabled="false" Width="25px" MaxLength="5" />
                                        </td>
                                    </tr>         
                                    <tr style="vertical-align: top; ">
                                        <td style="width: 75px;">
                                            Available +/- :
                                        </td>
                                        <td style="width: 5px;">
                                            <b>$</b>
                                        </td>
                                        <td style="width: 85px;">
                                            <asp:TextBox ID="txtRentalAvailablePlusMinus" runat="server" CssClass="inputField" Enabled="false" Width="75px" MaxLength="5" />
                                        </td>
                                        <td style="width: 80px;">
                                            Available Days:
                                        </td>
                                        <td style="width: 35px;">
                                            <asp:TextBox ID="txtRentalAvailableDays" runat="server" CssClass="inputField" Enabled="false" Width="25px" MaxLength="5" />
                                        </td>
                                        <td style="width: 110px;">
                                            Saved Sync +/- Days:
                                        </td>
                                        <td style="width: 40px;">
                                            <asp:TextBox ID="txtRentalSavedSync" runat="server" CssClass="inputField" Enabled="false" Width="25px" MaxLength="5" />
                                        </td>
                                    </tr>         
                                </table>
                                <table class="topTable" style="border-spacing: 0px; width: 440px;">
                                    <tr style="vertical-align: top; ">
                                        <td>
                                            <hr />
                                        </td>
                                    </tr>         
                                </table>
                            </td>
                        </tr>         
                    </table>
                </td>

                <!----------------- 2nd Half --------------------------->

                <td>
                 <div id="divscrollRentalCovarage" style="width: 520px; height: 80px; overflow-y: auto; border: 1px solid black;">
                    <table class="topTable" style="border-spacing: 0px; width: 400px;">
                        <tr style="vertical-align: top; ">
                            <td style="width: 500px;">
                                <asp:GridView ID="gvRentals" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnRowDataBound="gvRentals_RowDataBound" OnSelectedIndexChanged="gvRentals_SelectedIndexChanged">
                                    <AlternatingRowStyle BackColor="#F7F7F7" />
                                    <Columns>
                                        <asp:BoundField DataField="RentalID" HeaderText="RentalID" SortExpression="RentalID" HeaderStyle-Width="5px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="RentalAgency" HeaderText="Agency" SortExpression="RentalAgency" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="ResConfNumber" HeaderText="Res/Conf" SortExpression="ResConfNumber" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="RateTypeCD" HeaderText="Rate Type" SortExpression="RateTypeCD" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="RentalStatus" HeaderText="Rental Status" SortExpression="RentalStatus" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="Warranty" HeaderText="Warranty" SortExpression="Warranty" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="ClaimAspectID" HeaderText="ClaimAspectID" SortExpression="ClaimAspectID" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="VehicleClass" HeaderText="VehicleClass" SortExpression="VehicleClass" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="Rate" HeaderText="Rate" SortExpression="Rate" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="TaxRate" HeaderText="TaxRate" SortExpression="TaxRate" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="TaxedRate" HeaderText="TaxedRate" SortExpression="TaxedRate" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="Rental" HeaderText="Rental" SortExpression="Rental" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="RentalNoOfDays" HeaderText="RentalNoOfDays" SortExpression="RentalNoOfDays" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="AuthPickupDate" HeaderText="AuthPickupDate" SortExpression="AuthPickupDate" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="StartDate" HeaderText="StartDate" SortExpression="StartDate" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="EndDate" HeaderText="EndDate" SortExpression="EndDate" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="RentalAgencyAddress1" HeaderText="RentalAgencyAddress1" SortExpression="RentalAgencyAddress1" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="RentalAgencyCity" HeaderText="RentalAgencyCity" SortExpression="RentalAgencyCity" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="RentalAgencyState" HeaderText="RentalAgencyState" SortExpression="RentalAgencyState" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="RentalAgencyZip" HeaderText="RentalAgencyZip" SortExpression="RentalAgencyZip" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                        <asp:BoundField DataField="RentalAgencyPhone" HeaderText="RentalAgencyPhone" SortExpression="RentalAgencyPhone" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                    </Columns>
                                    <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                    <HeaderStyle BackColor="#629ad6" Font-Bold="True" ForeColor="#F7F7F7" />
                                    <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                    <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                </asp:GridView>
                            </td>
                        </tr>         
                    </table>
</div>
                    <table class="topTable" style="border-spacing: 0px; width: 400px;">
                        <tr style="vertical-align: top; ">
                            <td>
                                <hr />
                            </td>
                        </tr>         
                    </table>  
                    <table class="topTable" style="border-spacing: 0px;">
                        <tr style="vertical-align: top; ">
                            <td style="width: 170px;">
                                <asp:DropDownList ID="ddlRentalAgency" runat="server" CssClass="inputField" Width="150px">
                                    <asp:ListItem Value="" Text="" />
                                    <asp:ListItem Value="Enterprise" Text="Enterprise" />
                                    <asp:ListItem Value="Hertz" Text="Hertz" />
                                    <asp:ListItem Value="Other" Text="Other" />
                                </asp:DropDownList>
                                <asp:Label ID ="lblRentalAgency" runat="server" Text="*" />
                            </td>
                            <td style="width: 55px;">
                                Res/Conf:
                            </td>
                            <td style="width: 105px;">
                                <asp:TextBox ID="txtRentalResConf" runat="server" CssClass="inputField" Width="85px" MaxLength="15" />
                                <asp:Label ID ="lblRentalResConf" runat="server" Text="*" />
                            </td>
                        </tr>         
                    </table>
                    <table class="topTable" style="border-spacing: 0px; width: 400px;">
                        <tr style="vertical-align: top; ">
                            <td>
                                <hr />
                            </td>
                        </tr>         
                    </table>  
                                          
                    <table class="topTable" style="border-spacing: 0px;">
                        <tr style="vertical-align: top; ">
                            <td style="width: 70px;">
                                Vehicle Class:
                            </td>
                            <td style="width: 120px;">
                                <asp:DropDownList ID="ddlRentalVehicleClass" runat="server" CssClass="inputField" Width="100px">
                                    <asp:ListItem Value="" Text="" />
                                    <asp:ListItem Value="Compact" Text="Compact" />
                                    <asp:ListItem Value="Economy" Text="Economy" />
                                    <asp:ListItem Value="Full Size" Text="Full Size" />
                                    <asp:ListItem Value="Intermediate" Text="Intermediate" />
                                    <asp:ListItem Value="Large Pickup" Text="Large Pickup" />
                                    <asp:ListItem Value="Luxury" Text="Luxury" />
                                    <asp:ListItem Value="Midsize SUV" Text="Midsize SUV" />
                                    <asp:ListItem Value="Mini Van" Text="Mini Van" />
                                    <asp:ListItem Value="Premium" Text="Premium" />
                                    <asp:ListItem Value="Premium SUV" Text="Premium SUV" />
                                    <asp:ListItem Value="Standard" Text="Standard" />
                                    <asp:ListItem Value="SUV" Text="SUV" />
                                </asp:DropDownList>
                                <asp:Label ID ="lblRentalVehicleClass" runat="server" Text="*" />
                            </td>
                            <td style="width: 25px;">
                                Status:
                            </td>
                            <td style="width: 130px;">
                                <asp:DropDownList ID="ddlRentalStatus" runat="server" CssClass="inputField" Width="120px">
                                    <asp:ListItem Value="" Text="" />
                                    <asp:ListItem Value="Cancelled" Text="Cancelled" />
                                    <asp:ListItem Value="Closed" Text="Closed" />
                                    <asp:ListItem Value="Closed & Billed" Text="Closed & Billed" />
                                    <asp:ListItem Value="New" Text="New" />
                                    <asp:ListItem Value="Open" Text="Open" />
                                    <asp:ListItem Value="Reservation" Text="Reservation" />
                                </asp:DropDownList>
                                <asp:Label ID ="lblRentalStatus" runat="server" Text="*" />
                            </td>
                        </tr>         
                        <tr style="vertical-align: top; ">
                            <td style="width: 70px;">
                                Rate Type:
                            </td>
                            <td style="width: 110px;">
                                <asp:DropDownList ID="ddlRentalRateType" runat="server" CssClass="inputField" Width="100px">
                                    <asp:ListItem Value="" Text="" />
                                    <asp:ListItem Value="Daily" Text="Daily" />
                                    <asp:ListItem Value="Weekly" Text="Weekly" />
                                </asp:DropDownList>
                                <asp:Label ID ="lblRentalRateType" runat="server" Text="*" />
                            </td>
                            <td style="width: 65px;">
                                <asp:CheckBox ID="chkWarranty" runat="server" />Warranty
                            </td>
                            <td style="width: 50px;">
                                &nbsp;
                            </td>
                        </tr>         
                        <tr style="vertical-align: top; ">
                            <td style="width: 70px;">
                                Rate:
                            </td>
                            <td style="width: 85px;">
                                <asp:TextBox ID="txtRentalRate" runat="server" CssClass="inputField" Width="50px" />
                                &nbsp;<asp:Label ID ="lblRentalRate" runat="server" Text="*" />
                            </td>
                            <td style="width: 65px;">
                                Auth Pickup:
                            </td>
                            <td style="width: 95px;">
                                <asp:TextBox ID="txtRentalAuthPickup" runat="server" CssClass="inputField" Width="75px" MaxLength="10" />
                                <asp:Label ID ="lblRentalAuthPickup" runat="server" Text="*" />
                            </td>
                        </tr> 
                    </table>  

                    <table class="topTable" style="border-spacing: 0px; width: 550px;">
                        <tr style="vertical-align: top; ">
                            <td>
                                <hr />
                            </td>
                        </tr>         
                    </table> 
                                                              
                    <table class="topTable" style="border-spacing: 0px; ">
                        <tr style="vertical-align: top; ">
                            <td style="width: 75px;">
                                Tax Rate:
                            </td>
                            <td style="width: 160px;">
                                <b>%</b>
                                <asp:TextBox ID="txtRentalTaxRate" runat="server" CssClass="inputField" Width="100px" />
                                &nbsp;
                                <asp:Label ID ="lblRentalTaxRate" runat="server" Text="*" />
                            </td>
                            <td style="width: 125px;">
                                Rental Start Date:
                            </td>
                            <td style="width: 105px;">
                                <asp:TextBox ID="txtRentalStartDate" runat="server" CssClass="inputField" Width="75px" MaxLength="10" />
                                <asp:HiddenField ID="hidRepairStart" runat="server" />
                                <asp:HiddenField ID="hidWarrantyStart" runat="server" />
                                <asp:Label ID ="lblRentalStartDate" runat="server" Text="*" />
                            </td>
                            <td style="width: 85px;">
                                Repair Start:
                            </td>
                            <td style="width: 95px;">
                                <asp:TextBox ID="txtRentalRepairStart" runat="server" CssClass="inputField" Width="75px" Enabled="false" />
                            </td>
                        </tr> 
                        <tr style="vertical-align: top; ">
                            <td style="width: 75px;">
                                Taxed Rate:
                            </td>
                            <td style="width: 155px;">
                                <b>&nbsp;&nbsp;$</b>
                                <asp:TextBox ID="txtRentalTaxedRate" runat="server" CssClass="inputField" Width="100px" Enabled="false" />
                                <asp:HiddenField ID="hidtxtRentalTaxedRate" runat="server" />
                                <asp:Label ID ="lblRentalTaxedRate" runat="server" Text="*" />
                            </td>
                            <td style="width: 125px;">
                                Rental End Date:
                            </td>
                            <td style="width: 105px;">
                                <asp:TextBox ID="txtRentalEndDate" runat="server" CssClass="inputField" Width="75px" MaxLength="10" />
                                <asp:HiddenField ID="hidRepairEnd" runat="server" />
                                <asp:HiddenField ID="hidWarrantyEnd" runat="server" />
                                <asp:Label ID ="lblRentalEndDate" runat="server" Text="*" />
                            </td>
                            <td style="width: 85px;">
                                Repair End:
                            </td>
                            <td style="width: 50px;">
                                <asp:TextBox ID="txtRentalRepairEnd" runat="server" CssClass="inputField" Width="75px" Enabled="false" />
                            </td>
                        </tr>         
                        <tr style="vertical-align: top; ">
                            <td style="width: 75px;">
                                Rental:
                            </td>
                            <td style="width: 155px;">
                                <b>&nbsp;&nbsp;$</b>
                                <asp:TextBox ID="txtRentalDollars" runat="server" CssClass="inputField" Width="100px" Enabled="false" />
                                <asp:HiddenField ID="hidtxtRentalDollars" runat="server" />
                                <asp:Label ID ="lblRentalDollars" runat="server" Text="*" />
                            </td>
                            <td style="width: 125px;">
                                Rental Days:
                            </td>
                            <td style="width: 105px;">
                                <asp:TextBox ID="txtRentalNoOfDays" runat="server" CssClass="inputField" Width="25px" Enabled="false" />
                                <asp:HiddenField ID="hidtxtRentalNoOfDays" runat="server" />
                            </td>
                            <td style="width: 85px;">
                                Repair Cycle:
                            </td>
                            <td style="width: 70px;">
                                <asp:TextBox ID="txtRentalRepairCycle" runat="server" CssClass="inputField" Width="25px" Enabled="false" />
                                <asp:Label ID ="lblLossDateIndicator" runat="server" ForeColor="Red" ToolTip="Rental Start Date from Loss Date..." />
                            </td>
                        </tr>         
                        <tr style="vertical-align: top; ">
                            <td style="width: 70px;">
                                &nbsp;
                            </td>
                            <td style="width: 50px;">
                                &nbsp;
                            </td>
                        </tr>         
                    </table>

                    <table class="topTable" style="border-spacing: 0px;">
                        <tr style="vertical-align: top; ">
                            <td style="width: 200px;">
                                &nbsp;
                            </td>
                            <td style="width: 325px;">
                                <asp:RegularExpressionValidator ID="txtRentalStartDateValidator" ControlToValidate = "txtRentalStartDate"  ValidationExpression = "^([0-9]|0[1-9]|1[012])\/([0-9]|0[1-9]|[12][0-9]|3[01])\/(19|20)\d\d$" runat="server" ErrorMessage="Rental Start Date must be formatted (MM/DD/YYYY)"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="txtRentalEndDateValidator" ControlToValidate = "txtRentalEndDate"  ValidationExpression = "^([0-9]|0[1-9]|1[012])\/([0-9]|0[1-9]|[12][0-9]|3[01])\/(19|20)\d\d$" runat="server" ErrorMessage="Rental End Date must be formatted (MM/DD/YYYY)"></asp:RegularExpressionValidator>
                                <asp:Label ID ="lblMsg" runat="server" ForeColor="Red" Width="320" />
                            </td>
                        </tr>         
                    </table>

                    <table class="topTable" style="border-spacing: 0px;">
                        <tr style="vertical-align: top; ">
                            <td style="width: 85px;">
                                Rental Phone:
                            </td>
                            <td style="width: 95px;">
                                <asp:TextBox ID="txtRentalPhone" runat="server" CssClass="inputField" Width="80px" MaxLength="12" />
                                <asp:Label ID ="lblRentalPhone" runat="server" Text="*" />
                                <asp:RegularExpressionValidator ID="txtRentalPhoneFieldValidator" runat="server" ControlToValidate="txtRentalPhone" ErrorMessage="Format (nnn)nnn-nnnn" ForeColor="Red" ValidationExpression="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>         
                        <tr style="vertical-align: top; ">
                            <td style="width: 85px;">
                                Rental Address:
                            </td>
                            <td style="width: 145px;">
                                <asp:TextBox ID="txtRentalAddress" runat="server" CssClass="inputField" Width="130px" MaxLength="100" />
                                <asp:Label ID ="lblRentalAddress" runat="server" Text="*" />
                            </td>
                        </tr>         
                        <tr style="vertical-align: top; ">
                            <td style="width: 85px;">
                                Rental City:
                            </td>
                            <td style="width: 145px;">
                                <asp:TextBox ID="txtRentalCity" runat="server" CssClass="inputField" Width="130px" MaxLength="100" />
                                <asp:Label ID ="lblRentalCity" runat="server" Text="*" />
                            </td>
                        </tr>         
                    </table>

                    <table class="topTable" style="border-spacing: 0px;">
                        <tr style="vertical-align: top; ">
                            <td style="width: 85px;">
                                Rental State:
                            </td>
                            <td style="width: 60px;">
                                <asp:TextBox ID="txtRentalState" runat="server" CssClass="inputField" Width="30px" MaxLength="2" />
                                <asp:Label ID ="lblRentalState" runat="server" Text="*" />
                            </td>
                            <td style="width: 60px;">
                                Rental Zip:
                            </td>
                            <td style="width: 105px;">
                                <asp:TextBox ID="txtRentalZip" runat="server" CssClass="inputField"  Width="70px" MaxLength="10" />
                                <asp:Label ID ="lblRentalZip" runat="server" Text="*" />
                            </td>
                            <td style="width: 50px;">
                                &nbsp;
                            </td>
                            <td style="width: 50px;">
                                <asp:Button runat="server" ID="btnRentalAdd" CssClass="formbutton" Text="Add" />
                            </td>
                            <td style="width: 50px;">
                                <asp:Button runat="server" ID="btnRentalUpdate" CssClass="formbutton" Text="Update" />
                            </td>
                            <td style="width: 50px;">
                                <asp:Button runat="server" ID="btnRentalSave" CssClass="formbutton" Text="Save" />
                            </td>
                        </tr>         
                    </table>            

                    <table class="topTable" style="border-spacing: 0px;">
                        <tr style="vertical-align: top;">
                            <td style=" width: 350px;">
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblRentalAddSaved" ForeColor="Blue" />
                            </td>
                        </tr> 
                    </table>            

                    <table class="topTable" style="border-spacing: 0px; width: 550px;">
                        <tr style="vertical-align: top; ">
                            <td>
                                <hr />
                            </td>
                        </tr>         
                    </table>  
                </td>
            </tr>
        </table>

        </div>
    </form>
</body>
</html>