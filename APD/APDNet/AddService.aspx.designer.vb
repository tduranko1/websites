﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AddService
    
    '''<summary>
    '''frmAddServiceChannel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents frmAddServiceChannel As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''ddlNewServiceChannel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlNewServiceChannel As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''chkNewServiceChannelPrimary control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkNewServiceChannelPrimary As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''btnNewServiceChannel_ADD control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnNewServiceChannel_ADD As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btnNewServiceChannel_CANCEL control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnNewServiceChannel_CANCEL As Global.System.Web.UI.WebControls.Button
End Class
