﻿'--------------------------------------------------------------
' Program: ClaimXP
' Author:  Thomas Duranko
' Date:    Aug 06, 2013
' Version: 1.0
'
' Description:  This site provides the ClaimXP for APD replacing the 
'               existing COM based classic asp functions. 
'--------------------------------------------------------------
Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Imports System.Web.Caching
Imports System.Web.Caching.Cache

Public Class ClaimXP
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '-------------------------------
        ' Globals
        '-------------------------------
        Dim sLynxID As String = ""
        Dim sUserID As String = ""
        Dim sShowAll As String = ""
        Dim sWindowState As String = ""
        Dim sInsuranceCompanyID As String = ""
        Dim sClaimClaimAspectID As String = ""
        Dim sParams As String = ""
        Dim sASPParam As Object
        Dim oSession As Object
        Dim sXslHash As New Hashtable
        Dim XMLCRUD As XmlElement
        Dim sVehClaimAspectID = ""
        Dim sVehCount = ""
        Dim sNoteCRUD = ""
        Dim sTaskCRUD = ""
        Dim uspXMLRaw As XmlElement
        Dim uspClaimXMLRaw As XmlElement
        Dim uspVehicleXMLRaw As XmlElement
        Dim uspXMLXDoc As New XmlDocument
        Dim sAPDNetDebugging As String = ""
        Dim dtStart As Date
        Dim APDDBUtils As APDDBUtilities = Nothing
        Dim XMLVehNode As XmlElement
        Dim sRowsAffected As String = ""
        Dim oConfigXML As XmlElement
        Dim sImageRootDir As String = ""

        '-------------------------------
        ' Session/Local Variables
        '-------------------------------
        Dim sClaimDataStoredProcedure As String = "uspSessionGetClaimDetailWSXML"
        Dim sVehicleDataStoredProcedure As String = "uspClaimVehicleGetListWSXML"
        Dim sStoredProcedure As String = "uspClaimCondGetDetailWSXML"
        Dim sWorkflowStoredProcedure As String = "uspWorkflowSetEntityAsRead"
        Dim sXSLProcedure As String = "ClaimXP.xsl"
        Dim sVehNum As String = ""

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        Dim sSessionKey = Request.QueryString("SessionKey")
        Dim sWindowID = Request.QueryString("WindowID")
        Dim sASPParams = Request.QueryString("Params")

        'sSessionKey = "{F061E217-1CBB-4D79-9FF3-C0A001E12B00}"
        'sWindowID = 10

        Try
            If sSessionKey = "" Then
                Throw New SystemException("SessionValidationError: No session information received from classic ASP page.")
            Else
                '---------------------------------
                ' New Session Data
                '---------------------------------
                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "LynxID-" & sWindowID)
                sLynxID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "UserID-" & sWindowID)
                sUserID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "Claim_ClaimAspectID-" & sWindowID)
                sClaimClaimAspectID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "InsuranceCompanyID-" & sWindowID)
                sInsuranceCompanyID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "APDNetDebugging-" & sWindowID)
                sAPDNetDebugging = ""
                If UCase(oSession(0).SessionValue) = "TRUE" Or UCase(AppSettings("Debug")) = "TRUE" Then
                    sAPDNetDebugging = "TRUE"
                End If

                APDDBUtils = New APDDBUtilities()
                dtStart = Date.Now

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "ClaimXP.aspx - Start: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", ASPParam = " & sASPParams)
                End If

                '---------------------------------
                ' Get configuration data
                '---------------------------------
                'wsAPDFoundation.InitializeEx()
                oConfigXML = wsAPDFoundation.GetConfigXMLData("Document/RootDirectory")
                If oConfigXML.InnerXml.Contains("ERROR:") Then
                    Throw New SystemException("ConfigXMLError: Could not find the Document/RootDirectory configuration variable.")
                Else
                    sImageRootDir = oConfigXML.InnerText
                End If

                '---------------------------------
                ' Get the session data
                '---------------------------------
                If Not Request("UserID") Then
                    'sUserID = APDDBUtils.UserID
                Else
                    sUserID = Request("UserID")
                End If

                If Not Request("LynxID") Then
                    'sLynxID = APDDBUtils.LynxID
                Else
                    sLynxID = Request("LynxID")
                End If

                '------------------------------------
                ' If ASPParms, parse and add to hash
                '------------------------------------
                If sASPParams <> "" Then
                    Dim aParams As Array
                    aParams = Split(sASPParams, "|")
                    For Each sASPParam In aParams
                        Dim aValues As Array
                        aValues = Split(sASPParam, ":")
                        sXslHash.Add(aValues(0), aValues(1))

                        ' Debugging only
                        Response.Write(aValues(0) & " - " & aValues(1) & "<br/>")
                    Next
                End If

                '--------------------------------------------
                ' Get the vehicle number from the asp params
                '--------------------------------------------
                sVehNum = sXslHash.Item("VehNum")
                If sVehNum Is Nothing Then
                    Throw New SystemException("RequestValidationError: No vehicle number received from classic ASP page.")
                End If

                '---------------------------------
                ' Claim Parameters
                '---------------------------------
                sParams = sLynxID

                '---------------------------------
                ' Exec StoredProc to get the 
                ' Claim Details
                '---------------------------------
                uspClaimXMLRaw = wsAPDFoundation.ExecuteSpAsXML(sClaimDataStoredProcedure, sParams)
                If uspClaimXMLRaw.OuterXml = Nothing Then
                    ' Throw exception
                Else
                    uspXMLXDoc.LoadXml(uspClaimXMLRaw.OuterXml)
                    'sXslHash.Add("RunTime", Now.Subtract(dtStart).Milliseconds & " msec")

                    'Response.Write(uspClaimXMLRaw.OuterXml)
                    'Response.Write(APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sParams, sXslHash))
                End If

                '---------------------------------
                ' Claim Parameters
                '---------------------------------
                sParams += ", " & sInsuranceCompanyID

                '---------------------------------
                ' Exec StoredProc to get the 
                ' Vehicle Details
                '---------------------------------
                uspVehicleXMLRaw = wsAPDFoundation.ExecuteSpAsXML(sVehicleDataStoredProcedure, sParams)
                If uspVehicleXMLRaw.OuterXml = Nothing Then
                    ' Throw exception
                Else
                    uspXMLXDoc.LoadXml(uspVehicleXMLRaw.OuterXml)
                    XMLVehNode = uspXMLXDoc.SelectSingleNode("/Root[@LynxID='" & sLynxID & "']/Vehicle[@VehicleNumber='" + sVehNum + "']")

                    If Not XMLVehNode Is Nothing Then
                        sVehClaimAspectID = XMLVehNode.GetAttribute("ClaimAspectID")
                        If sVehClaimAspectID Is Nothing Then
                            Throw New SystemException("XMLValidationError: No VehClaimAspectID received in the XML.")
                        Else
                            ' Entity has been touched!  Kick the Workflow
                            sParams = sVehClaimAspectID
                            sParams += ", " & sUserID
                            sRowsAffected = wsAPDFoundation.ExecuteSp(sWorkflowStoredProcedure, sParams)
                            If sRowsAffected Is Nothing Then
                                Throw New SystemException("RequestValidationError: Workflow event didn't update successfully.")
                            End If
                        End If
                    End If

                    uspXMLXDoc = Nothing
                    XMLVehNode = Nothing

                    'sXslHash.Add("RunTime", Now.Subtract(dtStart).Milliseconds & " msec")
                    'Response.Write(uspVehicleXMLRaw.OuterXml)
                    'Response.Write(APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sParams, sXslHash))
                End If

                'strVehicleListXML = objExe.ExecuteSpAsXML("uspClaimVehicleGetListXML", "", strLynxID, strInsuranceCompanyID)


                '-----------------------------------
                ' Add config variables as XslParams
                '-----------------------------------
                sXslHash.Add("LynxID", sLynxID)
                sXslHash.Add("UserID", sUserID)
                sXslHash.Add("ClaimClaimAspectID", sClaimClaimAspectID)
                sXslHash.Add("InsuranceCompanyID", sInsuranceCompanyID)
                'sXslHash.Add("SubordinateUserID", Request("SubordinateUserID"))
                'sXslHash.Add("SubordinatesFlag", Request("SubordinatesFlag"))
                'sXslHash.Add("VehClaimAspectID", sVehClaimAspectID)
                'sXslHash.Add("VehCount", sVehCount)
                'sXslHash.Add("Note", sNoteCRUD)
                'sXslHash.Add("Task", sTaskCRUD)

                Response.Write("sLynxID = " & sLynxID & "<br/>")
                Response.Write("sUserID = " & sUserID & "<br/>")
                Response.Write("sAPDNetDebugging = " & sAPDNetDebugging & "<br/>")
                Response.Write("sClaimClaimAspectID = " & sClaimClaimAspectID & "<br/>")
                Response.Write("sInsuranceCompanyID = " & sInsuranceCompanyID & "<br/>")
                Response.Write("sSessionKey = " & sSessionKey & "<br/>")
                Response.Write("sWindowID = " & sWindowID & "<br/>")
                Response.Write("sImageRootDir = " & sImageRootDir & "<br/>")
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls/parameter not found or page pre-transformation failed.", "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID & ", ASPParam = " & sASPParams)

            Response.Write(oExcept.Message)
            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        Finally
            '-----------------------------------
            ' Clean-up the mess
            '-----------------------------------
            wsAPDFoundation.Dispose()
            APDDBUtils = Nothing
            sASPParam = Nothing
            oSession = Nothing
            sXslHash = Nothing
            XMLCRUD = Nothing
            uspXMLRaw = Nothing
            uspXMLXDoc = Nothing
            'APDDBUtils = Nothing
            GC.Collect()
            GC.SuppressFinalize(Me)
        End Try
    End Sub

End Class