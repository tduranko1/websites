﻿'--------------------------------------------------------------
' Program: APDNotes
' Author:  Thomas Duranko
' Date:    Jun 06, 2013
' Version: 1.0
'
' Description:  This site provides the APDNotes for APD replacing the 
'               existing COM based classic asp functions. 
'--------------------------------------------------------------
Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Public Class APDNotes
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    Dim sHostingServer As String = ""
    Dim sUserComputer As String = ""

    '--------------------------------------------------------------
    ' Page Load
    '--------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '-------------------------------
        ' Globals
        '-------------------------------
        Dim sLynxID As String = ""
        Dim sUserID As String = ""
        Dim sShowAll As String = ""
        Dim sWindowState As String = ""
        Dim sInsuranceCompanyID As String = ""
        Dim sParams As String = ""
        Dim sASPParam As Object = Nothing
        Dim oSession As Object = Nothing
        Dim sXslHash As New Hashtable
        Dim XMLCRUD As XmlElement = Nothing
        Dim sVehClaimAspectID = ""
        Dim sVehCount = ""
        Dim sNoteCRUD = ""
        Dim sTaskCRUD = ""
        Dim iRC As Integer = 0
        Dim sSessionVariable As String = ""
        Dim sSessionVariableValue As String = ""
        Dim uspXMLRaw As XmlElement = Nothing
        Dim uspXMLXDoc As New XmlDocument
        Dim XMLSessionNode As XmlNode = Nothing
        Dim sHTML As String = ""
        Dim sAPDNetDebugging As String = ""
        Dim dtStart As Date = Nothing
        Dim APDDBUtils As APDDBUtilities = Nothing
        'Dim iTime As Decimal = 25000 ' Wait sec

        '----------------------------------
        ' Debug Params - Processing Server
        '----------------------------------
        sHostingServer = Request.ServerVariables("LOCAL_ADDR")
        sUserComputer = Request.ServerVariables("REMOTE_ADDR")

        '-------------------------------
        ' Local Variables
        '-------------------------------
        Dim sStoredProcedure As String = "uspNoteGetDetailWSXML"
        Dim sXSLProcedure As String = "APDNotes.xsl"

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        Dim sSessionKey = Request.QueryString("SessionKey")
        Dim sWindowID = Request.QueryString("WindowID")
        Dim sASPParams = Request.QueryString("Params")

        'sSessionKey = "3F301617-4513-4F3D-8EB6-5E72A2CA869C"
        'sWindowID = ""
        'sASPParams = "ShowAllNotes:|winState:"
        'Dim APDDBUtils As New APDDBUtilities

        'sASPParams = "showAll:|ShowAllNotes:|LynxID:4819979|UserID:7372|InsuranceCompanyID:184|VehCount:1|VehClaimAspectID:1279584"
        'sSessionKey = "{74CB1FBC-FEE6-4AF6-AEE6-8CF00918BAE2}"
        'sWindowID = 1

        '?SessionKey={74CB1FBC-FEE6-4AF6-AEE6-8CF00918BAE2}&WindowID=1&Params=winState:|ShowAllNotes:|LynxID:4819979|UserID:7372|InsuranceCompanyID:184|VehCount:1|VehClaimAspectID:1279584


        Try
            '---------------------------------
            ' Make sure we got the WindowID
            '---------------------------------
            If sWindowID Is Nothing Or sWindowID = "" Then
                wsAPDFoundation.LogEvent("APDNet", "CriticalSessionError", "APDNotes - Session data not passed: " & Date.Now, "Session controls for Window: sWindowsID=" & sWindowID & " and SessionKey: " & sSessionKey & " and ASPParam = " & sASPParams & " and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, "")
            End If

            If sSessionKey = "" Then
                Throw New SystemException("SessionValidationError: No session information received from classic ASP page.")
            Else
                '------------------------------------
                ' If ASPParms, parse and add to hash
                '------------------------------------
                If sASPParams <> "" Then
                    Dim aParams As Array
                    aParams = Split(sASPParams, "|")
                    For Each sASPParam In aParams
                        Dim aValues As Array
                        aValues = Split(sASPParam, ":")
                        sXslHash.Add(aValues(0), aValues(1))
                        'Response.Write(aValues(0) & ":" & aValues(1))
                    Next
                End If

                sLynxID = sXslHash.Item("LynxID")
                sUserID = sXslHash.Item("UserID")
                sInsuranceCompanyID = sXslHash.Item("InsuranceCompanyID")
                sVehCount = sXslHash.Item("VehCount")
                sVehClaimAspectID = sXslHash.Item("VehClaimAspectID")
                sWindowState = sXslHash.Item("winState")
                sShowAll = sXslHash.Item("ShowAllNotes")

                'Response.Write("sLynxID: " & sLynxID)
                'Response.Write("<br/>sUserID: " & sUserID)
                'Response.Write("<br/>sInsuranceCompanyID: " & sInsuranceCompanyID)
                'Response.Write("<br/>sVehCount: " & sVehCount)
                'Response.Write("<br/>sVehClaimAspectID: " & sVehClaimAspectID)
                'Response.Write("<br/>sWindowState: " & sWindowState)
                'Response.Write("<br/>sShowAll: " & sShowAll)
                'Response.End()

                '---------------------------------
                ' Get the current window state
                '---------------------------------
                'If sShowAll = "true" Or sLynxID = "" Then sLynxID = "0"

                '---------------------------------------
                ' Make sure we got the key session vars
                '---------------------------------------
                If sLynxID Is Nothing Or sLynxID = "" Or sUserID Is Nothing Or sUserID = "" Or sInsuranceCompanyID Is Nothing Or sInsuranceCompanyID = "" Then
                    wsAPDFoundation.LogEvent("APDNet", "CriticalSessionError", "APDNotes - Session data not passed: ", "Session controls for Window: sWindowsID=" & sWindowID & " and SessionKey: " & sSessionKey & " and ASPParam = " & sASPParams & " and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, "")
                End If

                '---------------------------------
                ' Turn debugging on or off
                '---------------------------------
                sAPDNetDebugging = ""
                If UCase(AppSettings("Debug")) = "TRUE" Then
                    sAPDNetDebugging = "TRUE"
                End If

                APDDBUtils = New APDDBUtilities()
                dtStart = Date.Now

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "APDNotes - Start: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey & " and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", ASPParam = " & sASPParams)
                End If

                '---------------------------------
                ' Get configuration data
                '---------------------------------
                'XMLReturnElem = wsAPDFoundation.GetConfigXMLData("Root/Application/WorkFlow/EventCodes/Code[@name='VehicleAssignmentSentToShop']")
                'If XMLReturnElem.InnerXml.Contains("ERROR:") Then
                '    Throw New SystemException("ConfigXMLError: Could not find the VehicleAssignmentSentToShop configuration variable.")
                'Else
                '    sEvtVeicleAssignmentSentToShop = XMLReturnElem.InnerText
                'End If

                '---------------------------------
                ' Get the permission data
                '---------------------------------
                XMLCRUD = wsAPDFoundation.ExecuteSpAsXML("uspCRUDGetDetailWSXML", "Note, " & sUserID)
                sNoteCRUD = APDDBUtils.GetCrud(XMLCRUD)

                '-------------------------------------
                ' If CRUD is blank, default it to CRU_
                '-------------------------------------
                If sNoteCRUD = "" Then
                    sNoteCRUD = "CRU_"
                    wsAPDFoundation.LogEvent("APDNet", "CRUD_ERROR", "Note CRUD set to default value CRU_", "XMLCRUD: and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, XMLCRUD.InnerXml)
                End If

                '-----------------------------------
                ' Add config variables as XslParams
                '-----------------------------------
                sXslHash.Add("SessionKey", sSessionKey)
                sXslHash.Add("WindowID", sWindowID)
                sXslHash.Add("NoteCRUD", sNoteCRUD)
                sXslHash.Add("SubordinateUserID", Request("SubordinateUserID"))
                sXslHash.Add("SubordinatesFlag", Request("SubordinatesFlag"))
                sXslHash.Add("Task", sTaskCRUD)

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls/Parameters." _
                                             , "Session controls for Window: " & sWindowID _
                                             & " and SessionKey: " & sSessionKey _
                                             & " and HostingServer: " & sHostingServer _
                                             & " and UserAddr: " & sUserComputer _
                                             , "Vars: StoredProc = " & sStoredProcedure _
                                             & ", XSLPage = " & sXSLProcedure _
                                             & ", UserID(Session) = " & sUserID _
                                             & ", LynxID(Session) = " & sLynxID _
                                             & ", winState = " & sWindowState _
                                             & ", showAll = " & sShowAll _
                                             & ", SubordinateUserID = " & Request("SubordinateUserID") _
                                             & ", SubordinatesFlag = " & Request("SubordinatesFlag") _
                                             & ", ShowAllTasks = " & Request("ShowAllTasks") _
                                             & ", NoteCRUD = " & sNoteCRUD _
                                             )
                End If

                '---------------------------------
                ' StoredProc Params
                '---------------------------------
                sParams = sLynxID

                '---------------------------------
                ' StoredProc XML Data
                '---------------------------------
                uspXMLRaw = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)
                If uspXMLRaw.OuterXml = Nothing Then
                    ' Throw exception
                Else
                    uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)

                    XMLSessionNode = uspXMLXDoc.SelectSingleNode("/Root/Reference[@List='PertainsTo']")
                    sSessionVariableValue = XMLSessionNode.OuterXml

                    XMLSessionNode = uspXMLXDoc.SelectSingleNode("/Root/Reference[@List='ServiceChannel']")
                    sSessionVariableValue += XMLSessionNode.OuterXml

                    XMLSessionNode = uspXMLXDoc.SelectSingleNode("/Root/Reference[@List='Status']")
                    sSessionVariableValue += XMLSessionNode.OuterXml

                    '---------------------------------
                    ' Update session data from the XML
                    '---------------------------------
                    'sSessionVariable = "NotePertainsTo-" & sWindowID
                    'iRC = wsAPDFoundation.UpdateSessionVar(sSessionKey, sSessionVariable, sSessionVariableValue)
                    'If iRC <> -1 Then
                    ' Throw New SystemException("SessionUpdateError: Session variable " & sSessionVariable & " was not updated to " & sSessionVariableValue)
                    'End If

                    sSessionVariableValue = ""
                    XMLSessionNode = uspXMLXDoc.SelectSingleNode("/Root/Metadata")
                    sSessionVariableValue = XMLSessionNode.OuterXml

                    'sSessionVariable = "NoteMetadata-" & sWindowID
                    'iRC = wsAPDFoundation.UpdateSessionVar(sSessionKey, sSessionVariable, sSessionVariableValue)
                    'If iRC <> -1 Then
                    ' Throw New SystemException("SessionUpdateError: Session variable " & sSessionVariable & " was not updated to " & sSessionVariableValue)
                    'End If

                    If UCase(sAPDNetDebugging) = "TRUE" Then
                        sXslHash.Add("RunTime", Now.Subtract(dtStart).Milliseconds & " msec")
                    Else
                        sXslHash.Add("RunTime", Chr(149))
                    End If

                    sHTML = APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sParams, sXslHash)
                    Response.Write(sHTML)

                    '---------------------------------
                    ' Extract the reference data
                    '---------------------------------
                    'Dim sStart As String = "<xml id=""Reference"">"
                    'Dim sEnd As String = "</xml>"
                    'Dim iStart, iEnd As Integer

                    'iStart = sHTML.Contains(sStart)
                    'If iStart > 1 Then
                    '    iStart = iStart + Len(sStart)
                    '    iEnd = sHTML.Contains(sEnd)
                    '    If iEnd > iStart Then

                    '    End If
                    'End If

                End If

                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "APDNotes.aspx - Finished: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey & " and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
                End If

                '-----------------------------------
                ' Clean-up the mess
                '-----------------------------------
                'APDDBUtils = Nothing
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            'sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNet", "ERROR-" & sHostingServer, "Session controls/parameter not found or page pre-transformation failed.", "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey & " and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID & ", ASPParam = " & sASPParams & sError)

            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        Finally
            '-----------------------------------
            ' Clean-up the mess
            '-----------------------------------
            wsAPDFoundation.Dispose()
            sASPParam = Nothing
            oSession = Nothing
            sXslHash = Nothing
            XMLCRUD = Nothing
            uspXMLRaw = Nothing
            uspXMLXDoc = Nothing
            XMLSessionNode = Nothing
            APDDBUtils = Nothing
            GC.Collect()
            GC.SuppressFinalize(Me)
        End Try
    End Sub
End Class