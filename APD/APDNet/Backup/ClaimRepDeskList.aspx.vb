﻿'--------------------------------------------------------------
' Program: ClaimRepDeskList
' Author:  Thomas Duranko
' Date:    Jun 14, 2013
' Version: 1.0
'
' Description:  This site provides the Claim Rep Destop list of 
'               assignments for APD replacing the 
'               existing COM based classic asp functions. 
'--------------------------------------------------------------
Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Public Class ClaimRepDeskList
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService

    '--------------------------------------------------------------
    ' Page Load
    '--------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '-------------------------------
        ' Globals
        '-------------------------------
        Dim sLynxID As String = ""
        Dim sUserID As String = ""
        'Dim sShowAll As String = ""
        'Dim sWindowState As String = ""
        Dim sInsuranceCompanyID As String = ""
        Dim sParams As String = ""
        Dim oSession As Object
        Dim sXslHash As New Hashtable
        Dim XMLCRUD As XmlElement
        'Dim sVehClaimAspectID = ""
        'Dim sVehCount = ""
        'Dim sNoteCRUD = ""
        'Dim sTaskCRUD = ""
        Dim iRC As Integer = 0
        Dim sSessionVariable As String = ""
        Dim sSessionVariableValue As String = ""
        Dim uspXMLRaw As XmlElement
        Dim uspXMLXDoc As New XmlDocument
        Dim XMLSessionNode As XmlNode
        Dim sHTML As String = ""
        Dim sAPDNetDebugging As String = ""
        Dim sSupervisorFlag As String = ""
        Dim sReassignExposureCRUD As String = ""

        '-------------------------------
        ' Local Variables
        '-------------------------------
        Dim sStoredProcedure As String = "uspClaimRepDeskGetDetailWSXML"
        Dim sXSLProcedure As String = "ClaimRepDeskList.xsl"

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        Dim sSessionKey = Request.QueryString("SessionKey")
        Dim sWindowID = Request.QueryString("WindowID")

        'sSessionKey = "{657EC6DE-985A-4264-8BC7-31E485C1BFF7}"
        'sWindowID = "0"

        Try
            If sSessionKey = "" Then
                Throw New SystemException("SessionValidationError: No session information received from classic ASP page.")
            Else
                '---------------------------------
                ' New Session Data
                '---------------------------------
                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "LynxID-" & sWindowID)
                sLynxID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "UserID-" & sWindowID)
                sUserID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "APDNetDebugging-" & sWindowID)
                sAPDNetDebugging = ""
                If UCase(oSession(0).SessionValue) = "TRUE" Or UCase(AppSettings("Debug")) = "TRUE" Then
                    sAPDNetDebugging = "TRUE"
                End If

                Dim APDDBUtils As New APDDBUtilities()
                'Dim APDDBUtils As New APDDBUtilities(sSessionKey, sWindowID)

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "APDNotes - Start: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
                End If

                '---------------------------------
                ' Get configuration data
                '---------------------------------
                'XMLReturnElem = wsAPDFoundation.GetConfigXMLData("Root/Application/WorkFlow/EventCodes/Code[@name='VehicleAssignmentSentToShop']")
                'If XMLReturnElem.InnerXml.Contains("ERROR:") Then
                '    Throw New SystemException("ConfigXMLError: Could not find the VehicleAssignmentSentToShop configuration variable.")
                'Else
                '    sEvtVeicleAssignmentSentToShop = XMLReturnElem.InnerText
                'End If

                '---------------------------------
                ' Get the session data
                '---------------------------------
                If Not Request("LynxID") Then
                    'sLynxID = APDDBUtils.LynxID
                Else
                    sLynxID = Request("LynxID")
                End If

                If Not Request("UserID") Then
                    'sUserID = APDDBUtils.UserID
                Else
                    sUserID = Request("UserID")
                End If

                '---------------------------------
                ' Get the current window state
                '---------------------------------
                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "InsuranceCompanyID-" & sWindowID)
                sInsuranceCompanyID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "SupervisorFlag-" & sWindowID)
                sSupervisorFlag = oSession(0).SessionValue

                '---------------------------------
                ' Get the permission data
                '---------------------------------
                XMLCRUD = wsAPDFoundation.ExecuteSpAsXML("uspCRUDGetDetailWSXML", "'Reassign Exposure', " & sUserID)
                'sReassignExposureCRUD = APDDBUtils.GetCrud(XMLCRUD)

                '-----------------------------------
                ' Add config variables as XslParams
                '-----------------------------------
                'sXslHash.Add("LynxID", sLynxID)
                sXslHash.Add("UserID", sUserID)
                sXslHash.Add("SessionKey", sSessionKey)
                sXslHash.Add("WindowID", sWindowID)
                sXslHash.Add("SupervisorFlag", sSupervisorFlag)
                sXslHash.Add("ReassignExposureCRUD", sReassignExposureCRUD)

                'sXslHash.Add("ShowAllNotes", sShowAll)
                'sXslHash.Add("NoteCRUD", sNoteCRUD)
                'sXslHash.Add("VehClaimAspectID", sVehClaimAspectID)
                'sXslHash.Add("VehCount", sVehCount)



                'sXslHash.Add("SubordinateUserID", Request("SubordinateUserID"))
                'sXslHash.Add("SubordinatesFlag", Request("SubordinatesFlag"))
                'sXslHash.Add("Task", sTaskCRUD)

                '---------------------------------
                ' Debug Data
                '---------------------------------
                'If UCase(APDDBUtils.APDNetDebugging) = "TRUE" Then
                '    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls/Parameters." _
                '                             , "Session controls for Window: " & sWindowID _
                '                             & " and SessionKey: " & sSessionKey _
                '                             , "Vars: StoredProc = " & sStoredProcedure _
                '                             & ", XSLPage = " & sXSLProcedure _
                '                             & ", UserID(Session) = " & sUserID _
                '                             & ", LynxID(Session) = " & sLynxID _
                '                             & ", winState = " & sWindowState _
                '                             & ", showAll = " & sShowAll _
                '                             & ", SubordinateUserID = " & Request("SubordinateUserID") _
                '                             & ", SubordinatesFlag = " & Request("SubordinatesFlag") _
                '                             & ", ShowAllTasks = " & Request("ShowAllTasks") _
                '                             & ", NoteCRUD = " & sNoteCRUD _
                '                             )
                'End If

                '---------------------------------
                ' StoredProc Params
                '---------------------------------
                sParams = sUserID
                'sParams = sLynxID
                'sParams += ", " & sSupervisorFlag
                'sParams += ", " & IIf(Request("SubordinateUserID") Is Nothing, "NULL", Request("SubordinateUserID"))
                'sParams += ", " & IIf(Request("SubordinatesFlag") Is Nothing, "NULL", Request("SubordinatesFlag"))

                '---------------------------------
                ' StoredProc XML Data
                '---------------------------------
                uspXMLRaw = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)
                If uspXMLRaw.OuterXml = Nothing Then
                    ' Throw exception
                Else
                    uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)

                    'XMLSessionNode = uspXMLXDoc.SelectSingleNode("/Root/SupervisorFlag")
                    'sSessionVariableValue = XMLSessionNode.OuterXml

                    'XMLSessionNode = uspXMLXDoc.SelectSingleNode("/Root/Reference[@List='ServiceChannel']")
                    'sSessionVariableValue += XMLSessionNode.OuterXml

                    'XMLSessionNode = uspXMLXDoc.SelectSingleNode("/Root/Reference[@List='Status']")
                    'sSessionVariableValue += XMLSessionNode.OuterXml

                    '---------------------------------
                    ' Update session data from the XML
                    '---------------------------------
                    'sSessionVariable = "NotePertainsTo-" & sWindowID
                    'iRC = wsAPDFoundation.UpdateSessionVar(sSessionKey, sSessionVariable, sSessionVariableValue)
                    'If iRC <> -1 Then
                    '    Throw New SystemException("SessionUpdateError: Session variable " & sSessionVariable & " was not updated to " & sSessionVariableValue)
                    'End If

                    'sSessionVariableValue = ""
                    'XMLSessionNode = uspXMLXDoc.SelectSingleNode("/Root/Metadata")
                    'sSessionVariableValue = XMLSessionNode.OuterXml

                    'sSessionVariable = "NoteMetadata-" & sWindowID
                    'iRC = wsAPDFoundation.UpdateSessionVar(sSessionKey, sSessionVariable, sSessionVariableValue)
                    'If iRC <> -1 Then
                    '    Throw New SystemException("SessionUpdateError: Session variable " & sSessionVariable & " was not updated to " & sSessionVariableValue)
                    'End If

                    sHTML = APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sParams, sXslHash)
                    Response.Write(sHTML)

                    '---------------------------------
                    ' Extract the reference data
                    '---------------------------------
                    'Dim sStart As String = "<xml id=""Reference"">"
                    'Dim sEnd As String = "</xml>"
                    'Dim iStart, iEnd As Integer

                    'iStart = sHTML.Contains(sStart)
                    'If iStart > 1 Then
                    '    iStart = iStart + Len(sStart)
                    '    iEnd = sHTML.Contains(sEnd)
                    '    If iEnd > iStart Then

                    '    End If
                    'End If

                End If

                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "APDNotes.aspx - Finished: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
                End If

            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls/parameter not found or page pre-transformation failed.", "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID)

            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        End Try
    End Sub

End Class