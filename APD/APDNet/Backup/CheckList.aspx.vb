﻿'--------------------------------------------------------------
' Program: APD Custom Forms
' Author:  Thomas Duranko
' Date:    Sept 25, 2013
' Version: 1.0
'
' Description:  This site provides the Custom Forms for APD replacing the 
'               existing COM based classic asp functions. 
'--------------------------------------------------------------
Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Public Class CheckList
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService

    '--------------------------------------------------------------
    ' Page Load
    '--------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '-------------------------------
        ' Globals
        '-------------------------------
        Dim sParams As String = ""
        Dim sXslHash As New Hashtable
        Dim uspXMLRaw As XmlElement
        Dim uspXMLXDoc As New XmlDocument
        Dim sHTML As String = ""
        Dim sAPDNetDebugging As String = ""
        Dim dtStart As Date
        Dim APDDBUtils As APDDBUtilities = Nothing
        Dim sUserID As String = ""
        Dim sLynxID As String = ""
        Dim sVehNum As String = ""
        Dim sTaskID As String = ""
        Dim sInsCoId As String = ""
        Dim sTaskCRUD As String = ""
        Dim XMLCRUD As XmlElement

        '-------------------------------
        ' Session/Local Variables
        '-------------------------------
        Dim sStoredProcedure As String = "uspDiaryGetDetailWSXML"
        Dim sXSLProcedure As String = ""

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        Dim sSessionKey = Request.QueryString("SessionKey")
        Dim sWindowID = Request.QueryString("WindowID")
        Dim sASPParams = Request.QueryString("Params")

        Try
            '---------------------------------
            ' New Session Data
            '---------------------------------
            If UCase(AppSettings("Debug")) = "TRUE" Then
                sAPDNetDebugging = "TRUE"
            End If

            '---------------------------------
            ' Debugging
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNet", "START", "CheckList.aspx - Starting: " & Date.Now, "Session controls for Window: " & "" & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure)
            End If

            APDDBUtils = New APDDBUtilities()
            dtStart = Date.Now

            '------------------------------------
            ' If ASPParms, parse and add to hash
            '------------------------------------
            If sASPParams <> "" Then
                Dim aParams As Array
                aParams = Split(sASPParams, "|")
                For Each sASPParam In aParams
                    Dim aValues As Array
                    aValues = Split(sASPParam, ":")
                    sXslHash.Add(aValues(0), aValues(1))

                    '-------------------------------------
                    ' Debugging
                    '-------------------------------------
                    'Response.Write(aValues(0) & " - " & aValues(1) & "<br/>")
                Next
            End If

            sXSLProcedure = sXslHash("Entity")
            sLynxID = sXslHash("LynxId")
            sUserID = sXslHash("UserId")
            sTaskID = sXslHash("TaskID")

            '---------------------------------
            ' Get the permission data
            '---------------------------------
            XMLCRUD = wsAPDFoundation.ExecuteSpAsXML("uspCRUDGetDetailWSXML", "Task, " & sUserID)
            sTaskCRUD = APDDBUtils.GetCrud(XMLCRUD)
            '-------------------------------------
            ' If CRUD is blank, default it to CRU_
            '-------------------------------------
            If sTaskCRUD = "" Then
                sTaskCRUD = "CRU_"
                wsAPDFoundation.LogEvent("APDNet", "CRUD_ERROR", "Diary CRUD set to default value CRU_", "XMLCRUD: ", XMLCRUD.InnerXml)
            End If

            '-----------------------------------
            ' Add config variables as XslParams
            '-----------------------------------
            sXslHash.Add("Task", sTaskCRUD)

            '-------------------------------------
            ' Debugging
            '-------------------------------------
            'Response.Write("sSessionKey" & " - " & sSessionKey & "<br/>")
            'Response.Write("sWindowID" & " - " & sWindowID & "<br/>")
            'Response.End()

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls/Parameters." _
                                         , "Session controls for Window: " & "" _
                                         & " and SessionKey: " & sSessionKey _
                                         , "Vars: StoredProc = " & sStoredProcedure _
                                         & ", XSLPage = " & sXSLProcedure _
                                         & ", UserID = " & sUserID _
                                         & ", LynxID = " & sLynxID _
                                         & ", TaskID = " & sTaskID _
                                         & ", sASPParams = " & sASPParams _
                                         )
            End If

            '---------------------------------
            ' XSL Transformation
            '---------------------------------
            sParams = sTaskID
            sParams += "," & sLynxID
            sParams += "," & sUserID

            '---------------------------------
            ' StoredProc XML Data
            '---------------------------------
            uspXMLRaw = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)
            If uspXMLRaw.OuterXml = Nothing Then
                ' Throw exception
            Else
                uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)

                If UCase(sAPDNetDebugging) = "TRUE" Then
                    sXslHash.Add("RunTime", Now.Subtract(dtStart).Milliseconds & " msec")
                Else
                    sXslHash.Add("RunTime", Chr(149))
                End If

                '---------------------------------
                ' Call Data Presenter
                '---------------------------------
                sHTML = APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sParams, sXslHash)
                Response.Write(sHTML)
            End If

            'uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)

            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNet", "END", "CheckList.aspx - Finished: " & Date.Now, "Session controls for Window: " & "" & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls/parameter not found or page pre-transformation failed.", "Session controls for Window: " & "" & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID)

            Response.Write(sError)
            Response.End()
            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        End Try
    End Sub
End Class