﻿'--------------------------------------------------------------
' Program: ClaimDocument
' Author:  Thomas Duranko
' Date:    Sep 24, 2013
' Version: 1.0
'
' Description:  This site provides the APD Claim Document for APD replacing the 
'               existing COM based classic asp functions. 
'--------------------------------------------------------------
Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Public Class ClaimDocument
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService

    '--------------------------------------------------------------
    ' Page Load
    '--------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '-------------------------------
        ' Globals
        '-------------------------------
        Dim sLynxID As String = ""
        Dim sUserID As String = ""
        Dim sInsuranceCompanyID As String = ""
        Dim sParams As String = ""
        'Dim sASPParam As Object
        Dim oSession As Object
        Dim sXslHash As New Hashtable
        'Dim XMLCRUD As XmlElement
        Dim sVehClaimAspectID = ""
        Dim sVehCount = ""
        Dim iRC As Integer = 0
        Dim sSessionVariable As String = ""
        Dim sSessionVariableValue As String = ""
        Dim uspXMLRaw As XmlElement
        Dim uspXMLXDoc As New XmlDocument
        Dim XMLSessionNode As XmlNode
        Dim sHTML As String = ""
        Dim sAPDNetDebugging As String = ""
        Dim dtStart As Date
        Dim APDDBUtils As APDDBUtilities = Nothing
        Dim sReloadTab As String = ""
        Dim sClaim_ClaimAspectID As String = ""

        '-------------------------------
        ' Local Variables
        '-------------------------------
        Dim sStoredProcedure As String = "uspDocumentGetDetailWSXML"
        Dim sXSLProcedure As String = "ClaimDocument.xsl"

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        Dim sSessionKey = Request.QueryString("SessionKey")
        Dim sWindowID = Request.QueryString("WindowID")
        Dim sASPParams = Request.QueryString("Params")

        'sSessionKey = "{F061E217-1CBB-4D79-9FF3-C0A001E12B00}"
        'sWindowID = "1"
        'sASPParams = "ShowAllNotes:|winState:"
        'Dim APDDBUtils As New APDDBUtilities

        Try
            If sSessionKey = "" Then
                Throw New SystemException("SessionValidationError: No session information received from classic ASP page.")
            Else
                '---------------------------------
                ' New Session Data
                '---------------------------------
                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "LynxID-" & sWindowID)
                sLynxID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "UserID-" & sWindowID)
                sUserID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "APDNetDebugging-" & sWindowID)
                sAPDNetDebugging = ""
                If UCase(oSession(0).SessionValue) = "TRUE" Or UCase(AppSettings("Debug")) = "TRUE" Then
                    sAPDNetDebugging = "TRUE"
                End If

                APDDBUtils = New APDDBUtilities()
                dtStart = Date.Now

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "ClaimDocument - Start: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", ASPParam = " & sASPParams)
                End If

                '---------------------------------
                ' Get the session data
                '---------------------------------
                If Not Request("LynxID") Then
                    'sLynxID = APDDBUtils.LynxID
                Else
                    sLynxID = Request("LynxID")
                End If

                If Not Request("UserID") Then
                    'sUserID = APDDBUtils.UserID
                Else
                    sUserID = Request("UserID")
                End If

                '---------------------------------
                ' Get the current window state
                '---------------------------------
                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "InsuranceCompanyID-" & sWindowID)
                sInsuranceCompanyID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "VehCount-" & sWindowID)
                sVehCount = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "VehClaimAspectID-" & sWindowID)
                sVehClaimAspectID = oSession(0).SessionValue

                '-----------------------------------
                ' This code is relient on a vehicle
                ' so loop until one is available
                '-----------------------------------
                'If sVehClaimAspectID = Nothing Then
                '    wsAPDFoundation.LogEvent("APDNet", "SESSION_ERROR", "Note - Waiting on Vehicle section to load", "Note - VehClaimAspectID not available from the session variables because the vehicle section isn't loaded yet.  Looping ", "VehClaimAspectID: " & sVehClaimAspectID)

                '    Do While sVehClaimAspectID = Nothing
                '        oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "VehClaimAspectID-" & sWindowID)
                '        sVehClaimAspectID = oSession(0).SessionValue

                '        System.Threading.Thread.Sleep(iTime)
                '        wsAPDFoundation.LogEvent("APDNet", "TIMELOOP", "TIMELOOP", "LOOP" & Now, "VehClaimAspectID: " & sVehClaimAspectID)
                '    Loop
                'End If

                '---------------------------------
                ' Get the permission data
                '---------------------------------
                'XMLCRUD = wsAPDFoundation.ExecuteSpAsXML("uspCRUDGetDetailWSXML", "Note, " & sUserID)
                'sNoteCRUD = APDDBUtils.GetCrud(XMLCRUD)

                '-------------------------------------
                ' If CRUD is blank, default it to CRU_
                '-------------------------------------
                'If sNoteCRUD = "" Then
                ' sNoteCRUD = "CRU_"
                ' wsAPDFoundation.LogEvent("APDNet", "CRUD_ERROR", "Note CRUD set to default value CRU_", "XMLCRUD: ", XMLCRUD.InnerXml)
                'End If

                '-----------------------------------
                ' Add config variables as XslParams
                '-----------------------------------
                sXslHash.Add("LynxID", sLynxID)
                sXslHash.Add("UserID", sUserID)
                sXslHash.Add("SessionKey", sSessionKey)
                sXslHash.Add("WindowID", sWindowID)
                'sXslHash.Add("NoteCRUD", sNoteCRUD)
                sXslHash.Add("VehClaimAspectID", sVehClaimAspectID)
                sXslHash.Add("VehCount", sVehCount)

                '------------------------------------
                ' If ASPParms, parse and add to hash
                '------------------------------------
                If sASPParams <> "" Then
                    Dim aParams As Array
                    aParams = Split(sASPParams, "|")
                    For Each sASPParam In aParams
                        Dim aValues As Array
                        aValues = Split(sASPParam, ":")
                        sXslHash.Add(aValues(0), aValues(1))
                    Next
                End If

                sReloadTab = sXslHash("ReloadTab")
                sClaim_ClaimAspectID = sXslHash("Claim_ClaimAspectID")

                '-------------------------------------
                ' Debugging
                '-------------------------------------
                'Response.Write("sSessionKey" & " - " & sSessionKey & "<br/>")
                'Response.Write("sWindowID" & " - " & sWindowID & "<br/>")
                'Response.Write("sUserID" & " - " & sUserID & "<br/>")
                'Response.Write("sLynxID" & " - " & sLynxID & "<br/>")
                'Response.Write("sReloadTab" & " - " & sReloadTab & "<br/>")
                'Response.Write("sClaim_ClaimAspectID" & " - " & sClaim_ClaimAspectID & "<br/>")
                'Response.Write("sInsuranceCompanyID" & " - " & sInsuranceCompanyID & "<br/>")
                'Response.Write("sASPParams" & " - " & sASPParams & "<br/>")
                'Response.End()

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls/Parameters." _
                                             , "Session controls for Window: " & sWindowID _
                                             & " and SessionKey: " & sSessionKey _
                                             , "Vars: StoredProc = " & sStoredProcedure _
                                             & ", XSLPage = " & sXSLProcedure _
                                             & ", UserID(Session) = " & sUserID _
                                             & ", LynxID(Session) = " & sLynxID _
                                             & ", sReloadTab = " & sReloadTab _
                                             & ", sClaim_ClaimAspectID = " & sClaim_ClaimAspectID _
                                             )
                End If

                '---------------------------------
                ' StoredProc Params
                '---------------------------------
                sParams = sLynxID
                sParams += ", " & sVehClaimAspectID
                sParams += "," & "null"
                sParams += "," & "null"
                sParams += ", " & sInsuranceCompanyID

                '---------------------------------
                ' StoredProc XML Data
                '---------------------------------
                uspXMLRaw = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)
                If uspXMLRaw.OuterXml = Nothing Then
                    ' Throw exception
                Else
                    uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)

                    'XMLSessionNode = uspXMLXDoc.SelectSingleNode("/Root/Reference[@List='PertainsTo']")
                    'sSessionVariableValue = XMLSessionNode.OuterXml

                    'XMLSessionNode = uspXMLXDoc.SelectSingleNode("/Root/Reference[@List='ServiceChannel']")
                    'sSessionVariableValue += XMLSessionNode.OuterXml

                    'XMLSessionNode = uspXMLXDoc.SelectSingleNode("/Root/Reference[@List='Status']")
                    'sSessionVariableValue += XMLSessionNode.OuterXml

                    '---------------------------------
                    ' Update session data from the XML
                    '---------------------------------
                    'sSessionVariable = "NotePertainsTo-" & sWindowID
                    'iRC = wsAPDFoundation.UpdateSessionVar(sSessionKey, sSessionVariable, sSessionVariableValue)
                    'If iRC <> -1 Then
                    '    Throw New SystemException("SessionUpdateError: Session variable " & sSessionVariable & " was not updated to " & sSessionVariableValue)
                    'End If

                    'sSessionVariableValue = ""
                    'XMLSessionNode = uspXMLXDoc.SelectSingleNode("/Root/Metadata")
                    'sSessionVariableValue = XMLSessionNode.OuterXml

                    'sSessionVariable = "NoteMetadata-" & sWindowID
                    'iRC = wsAPDFoundation.UpdateSessionVar(sSessionKey, sSessionVariable, sSessionVariableValue)
                    'If iRC <> -1 Then
                    '    Throw New SystemException("SessionUpdateError: Session variable " & sSessionVariable & " was not updated to " & sSessionVariableValue)
                    'End If

                    If UCase(sAPDNetDebugging) = "TRUE" Then
                        sXslHash.Add("RunTime", Now.Subtract(dtStart).Milliseconds & " msec")
                    Else
                        sXslHash.Add("RunTime", Chr(149))
                    End If

                    sHTML = APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sParams, sXslHash)
                    Response.Write(sHTML)

                    '---------------------------------
                    ' Extract the reference data
                    '---------------------------------
                    'Dim sStart As String = "<xml id=""Reference"">"
                    'Dim sEnd As String = "</xml>"
                    'Dim iStart, iEnd As Integer

                    'iStart = sHTML.Contains(sStart)
                    'If iStart > 1 Then
                    '    iStart = iStart + Len(sStart)
                    '    iEnd = sHTML.Contains(sEnd)
                    '    If iEnd > iStart Then

                    '    End If
                    'End If

                End If

                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "ClaimDocument.aspx - Finished: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
                End If

                '-----------------------------------
                ' Clean-up the mess
                '-----------------------------------
                'APDDBUtils = Nothing
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            'sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls/parameter not found or page pre-transformation failed.", "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID & ", ASPParam = " & sASPParams & sError)

            Response.Write(sError)
            Response.End()

            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        Finally
            '-----------------------------------
            ' Clean-up the mess
            '-----------------------------------
            wsAPDFoundation.Dispose()
            oSession = Nothing
            sXslHash = Nothing
            uspXMLRaw = Nothing
            uspXMLXDoc = Nothing
            XMLSessionNode = Nothing
            APDDBUtils = Nothing
            GC.Collect()
            GC.SuppressFinalize(Me)
        End Try
    End Sub

End Class