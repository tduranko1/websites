﻿Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager
Imports System.Net
Imports System.Web.Caching

Public Class APDDBUtilities
    Dim wsAPDFoundation As New PGWAPDFoundation.APDService

    '--------------------------------------------------------------
    ' DataPresenterNet Function
    '--------------------------------------------------------------
    Public Function DataPresenterNet(ByVal StoredProcedure As String, ByVal uspXMLXDoc As XmlDocument, ByVal xslPage As String, ByVal Parameters As String, ByVal XslParams As Hashtable) As String
        '--------------------------------------
        ' Global Variables
        '--------------------------------------
        Dim docXML As New XmlDocument
        Dim XSLTransform As New Xsl.XslCompiledTransform(True)
        Dim uspXMLRaw As XmlElement = Nothing
        Dim HTMLData As New StringWriter
        Dim oXslParams As Object = Nothing
        Dim xslSettings As New System.Xml.Xsl.XsltSettings
        Dim xslResolv As New System.Xml.XmlUrlResolver
        Dim xslPathFile As String = ""
        Dim xslArg As New System.Xml.Xsl.XsltArgumentList()

        Try
            '------------------------------------------
            ' Get XML data from the WebService and the
            ' uspVehicleAssignGetDetailTVD
            '------------------------------------------
            uspXMLRaw = wsAPDFoundation.ExecuteSpAsXML(StoredProcedure, Parameters)
            If uspXMLRaw.OuterXml = Nothing Then
                ' Throw exception
            Else
                uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)
            End If

            Dim XMLNewElem As XmlNode
            For Each oXslParams In XslParams
                XMLNewElem = uspXMLXDoc.CreateElement(oXslParams.key)
                XMLNewElem.InnerText = oXslParams.value
                uspXMLXDoc.DocumentElement.AppendChild(XMLNewElem)

                '--------------------------------------
                ' Add XslParams to the <xsl:Params tag
                '--------------------------------------
                'xslArg.AddExtensionObject("xmlns:session", "http://lynx.apd/session-manager")
                'xslArg.AddExtensionObject(oXslParams.key, oXslParams.value)
                xslArg.AddParam(oXslParams.key, "", IIf(oXslParams.value Is Nothing, "", oXslParams.value))

                If UCase(AppSettings("Debug")) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "XML New Elements", "XMLElement: " & oXslParams.key & " = " & oXslParams.value & " Added...", "")
                End If
            Next

            Dim oSession As New SessionMgrClass
            xslArg.AddExtensionObject("urn:session", oSession)

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(AppSettings("Debug")) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "XSL Transformation", "Transformation of StoredProcedure: " & StoredProcedure & " and XSLPage: " & xslPage & " with Parameters: " & Parameters, "SQLXML: " & uspXMLXDoc.InnerXml)
            End If

            '------------------------------------------
            ' XSLT Setting - Allow Scripting
            '------------------------------------------
            xslSettings.EnableScript = True

            'xslSettings.EnableDocumentFunction = True

            '------------------------------------------
            ' Transform the files
            '------------------------------------------
            '------------------------------------------
            ' The 1st time a new xslt page is loaded, 
            ' Cache that page.  After the initial load
            ' just used the cached xslt page and pass
            ' the XML data to it.  This will fix the issue
            ' in dot net where javascripts in the xslt 
            ' are compiled and not cleaned up correctly
            '------------------------------------------
            xslPathFile = AppSettings("xslPath") & xslPage

            If HttpContext.Current.Cache.Get(xslPathFile) IsNot Nothing Then
                If UCase(AppSettings("Debug")) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "XSLT Cache", "Page " & xslPathFile & " already cached...", "")
                End If
                XSLTransform = CType(HttpContext.Current.Cache.Get(xslPathFile), Xsl.XslCompiledTransform)
            Else
                If UCase(AppSettings("Debug")) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "XSLT Cache", "Caching XSLPage: " & xslPathFile, "")
                End If

                XSLTransform.Load(xslPathFile, xslSettings, xslResolv)
                'XSLTransform.Load(xslPathFile, xslSettings, Nothing)
                HttpContext.Current.Cache.Insert(xslPathFile, XSLTransform, New CacheDependency(xslPathFile), Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, Nothing)
            End If

            'XSLTransform.Transform(uspXMLXDoc, Nothing, HTMLData)
            XSLTransform.Transform(uspXMLXDoc, xslArg, HTMLData)

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(AppSettings("Debug")) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Transformed HTML", "Transformed HTML after XML combined with xsl: " & StoredProcedure & " and XSLPage: " & xslPage & " with Parameters: " & Parameters, HTMLData.ToString)
            End If

            Return HTMLData.ToString
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "APDNet Utilities Failed: XSL Transformation (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNetUtilities", "ERROR", "XSL Transformation", "Transformation of StoredProcedure: " & StoredProcedure & " and XSLPage: " & xslPage & " with Parameters: " & Parameters, "SQLXML: " & uspXMLXDoc.InnerXml)

            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")

            '---------------------------------
            ' Clean-up
            '---------------------------------
            sError = Nothing
            sBody = Nothing
            FunctionName = Nothing

            Return oExcept.Message
        Finally
            docXML = Nothing
            XSLTransform = Nothing
            uspXMLRaw = Nothing
            HTMLData.Dispose()
            oXslParams = Nothing
            xslSettings = Nothing
            xslResolv = Nothing
            GC.Collect()
            GC.SuppressFinalize(Me)
        End Try
    End Function

    '--------------------------------------------------------------
    ' GetCrud Function
    '--------------------------------------------------------------
    Public Function GetCrud(ByVal XMLCRUD As XmlElement) As String
        Dim sCRUD = ""

        'If XMLCRUD.FirstChild.Attributes.ItemOf("Entity").Value = "Client Billing" Then
        '    wsAPDFoundation.LogEvent("APDNetUtilities", "DEBUG-->", "Found Client Billing", "CRUD XML = " & XMLCRUD.InnerXml, "")
        'End If

        'Return "CRUD"

        'wsAPDFoundation.LogEvent("APDNetUtilities", "Permissions", "CRUD XML Data", "CRUD XML = " & XMLCRUD.InnerXml, "")

        Try
            'wsAPDFoundation.LogEvent("APDNetUtilities", "DEBUG", "Checking Create", "CRUD Value = " & XMLCRUD.FirstChild.Attributes.ItemOf("Insert").Value, "")
            If XMLCRUD.FirstChild.Attributes.ItemOf("Insert").Value = "1" Then
                sCRUD = "C"
            Else
                sCRUD = "_"
            End If
            'wsAPDFoundation.LogEvent("APDNetUtilities", "DEBUG", "Checking Select", "CRUD Value = " & XMLCRUD.FirstChild.Attributes.ItemOf("Select").Value, "")
            If XMLCRUD.FirstChild.Attributes.ItemOf("Select").Value = "1" Then
                sCRUD += "R"
            Else
                sCRUD += "_"
            End If
            'wsAPDFoundation.LogEvent("APDNetUtilities", "DEBUG", "Checking Update", "CRUD Value = " & XMLCRUD.FirstChild.Attributes.ItemOf("Update").Value, "")
            If XMLCRUD.FirstChild.Attributes.ItemOf("Update").Value = "1" Then
                sCRUD += "U"
            Else
                sCRUD += "_"
            End If
            'wsAPDFoundation.LogEvent("APDNetUtilities", "DEBUG", "Checking Delete", "CRUD Value = " & XMLCRUD.FirstChild.Attributes.ItemOf("Delete").Value, "")
            If XMLCRUD.FirstChild.Attributes.ItemOf("Delete").Value = "1" Then
                sCRUD += "D"
            Else
                sCRUD += "_"
            End If

            'wsAPDFoundation.LogEvent("APDNetUtilities", "DEBUG", "Return Value", "CRUD = " & sCRUD, "")

            Return sCRUD
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "APDNet Utilities Failed: CRUD creation failed  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNetUtilities", "ERROR", "CRUD creation failed", "CRUD failed to retrieve permission data for object: CRUD: " & sCRUD, sError)

            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")

            '---------------------------------
            ' Clean-up
            '---------------------------------
            sError = Nothing
            sBody = Nothing
            FunctionName = Nothing

            Return oExcept.Message
        Finally
            sCRUD = Nothing
            XMLCRUD = Nothing
            GC.Collect()
            GC.SuppressFinalize(Me)
        End Try
    End Function

    '--------------------------------------------------------------
    ' RequestServerVar Function
    '--------------------------------------------------------------
    'Public Function RequestServerVar(ByVal sVariable As String) As String
    '    Dim oHttp As HttpRequest = Nothing

    '    Try
    '        Return oHttp.ServerVariables(sVariable)
    '    Catch oExcept As Exception
    '        '---------------------------------
    '        ' Error handler and notifications
    '        '---------------------------------
    '        Dim sError As String = ""
    '        Dim sBody As String = ""
    '        Dim FunctionName As New System.Diagnostics.StackFrame

    '        sError = String.Format("Error: {0}", "APDNet Utilities Failed: Request Server Variable failed  " & oExcept.ToString)

    '        wsAPDFoundation.LogEvent("APDNetUtilities", "ERROR", "Request Server Variable failed", "Server variable " & sVariable & " was not retrieved successfully.", "")

    '        '---------------------------------
    '        ' Email notifications
    '        '---------------------------------
    '        'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")

    '        '---------------------------------
    '        ' Clean-up
    '        '---------------------------------
    '        sError = Nothing
    '        sBody = Nothing
    '        FunctionName = Nothing

    '        Return sError
    '    Finally
    '        oHttp = Nothing
    '    End Try
    'End Function

End Class
