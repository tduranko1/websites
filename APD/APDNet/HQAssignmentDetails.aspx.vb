﻿Imports System.Xml
Imports System.Configuration.ConfigurationManager

Public Class HQAssignmentDetails
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService

    '---------------------------------
    ' Globals
    '---------------------------------
    Dim APDFoundation As New PGWAPDFoundation.APDService
    Public sErrorMessage As String
    Public sLynxID As String = ""
    Public sServer As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim oReturnXML As XmlElement = Nothing
        Dim iClaimAspectServiceChannelID As Integer = 0
        Dim iEventSeq As Integer = 1
        Dim bDebugMode As Boolean = False
        Dim bError As Boolean = False

        Try
            '----------------------------------------
            ' Debug Init 
            '----------------------------------------
            If UCase(AppSettings("Debug")) = "TRUE" Then
                bDebugMode = True
                sServer = Request.ServerVariables("server_name")
            End If

            '---------------------------
            ' Read query string values
            '---------------------------
            sLynxID = Request.QueryString("LynxID")
            iClaimAspectServiceChannelID = Request.QueryString("ClaimAspectServiceChannelID")
            lblLynxID.Text = sLynxID

            '-------------------------------
            ' Database Calls
            '-------------------------------  
            Dim sStoredProcedure As String = "uspHyperquestGetAssignmentDetailsWSXML"
            Dim sParams As String = iClaimAspectServiceChannelID

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If bDebugMode Then
                wsAPDFoundation.LogEvent("APDNet", "HQAssignmentDetails - " & sServer, "HQAssignmentDetails.aspx - Started: " & Date.Now & ", SEQ: " & CStr(iEventSeq), "==> Requesting HQ Assignment Job Status Details: ", "Vars: StoredProc = " & sStoredProcedure & ", Params = " & sParams)
                iEventSeq = iEventSeq + 1
            End If

            If Not IsPostBack Then
                '-------------------------------
                ' Call WS and get data
                '-------------------------------
                oReturnXML = APDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

                '----------------------------------------
                ' Debugging
                '----------------------------------------
                If bDebugMode = True Then
                    wsAPDFoundation.LogEvent("APDNet", "HQAssignmentDetails - " & sServer, "HQAssignmentDetails.aspx - Exec Stored Proc: " & Date.Now & ", SEQ: " & CStr(iEventSeq), "====> Executing Store Procedure to get Job details.  Results in EventXML", oReturnXML.InnerXml)
                    iEventSeq = iEventSeq + 1
                End If

                '-------------------------------
                ' Check the returned XML Data
                '-------------------------------
                Try
                    If oReturnXML.SelectSingleNode("/@ClaimAspectServiceChannelID").InnerText = CStr(iClaimAspectServiceChannelID) Then
                        txtJobStatus.Text = oReturnXML.SelectSingleNode("/Assignment/@JobStatus").InnerText
                        txtJobStatusDetails.Text = oReturnXML.SelectSingleNode("/Assignment/@JobStatusDetails").InnerText
                        Try
                            txtJobSenttoHQDate.Text = oReturnXML.SelectSingleNode("/Assignment/@JobTransmittedDate").InnerText
                        Catch ex As Exception
                            '----------------------------------------
                            ' Don't do anything, we just didn't get 
                            ' one of these values.
                            '----------------------------------------
                        End Try
                        Try
                            txtHQMessages.Text = oReturnXML.SelectSingleNode("/Assignment/@ErrorMessage").InnerText
                        Catch ex As Exception
                            '----------------------------------------
                            ' Don't do anything, we just didn't get 
                            ' one of these values.
                            '----------------------------------------
                        End Try

                        Select Case UCase(txtJobStatus.Text)
                            Case "ERROR"
                                If txtJobSenttoHQDate.Text = "" Then
                                    ' -- Add override if failed before being sent to hq
                                End If
                            Case "UNPROCESSED"
                                txtHQMessages.Text = Date.Now & " - Waiting for HQ Service to process the job to HQ."
                            Case Else

                        End Select

                        'If bDebug = True Then
                        ' APDPartnerFoundation.LogHyperquestPartnerEvent(sProcessingServer, sEventTransactionID, "HyperquestService(DBXMLEXEC" & CStr(iEventSeq) & ")", CStr(iEventSeq), "DBXMLEXEC", "====> Vehicle number details: " & Date.Now, "Vehicle number details XML from the database.", "XMLVehicleNumber: " & sVehicleID)
                        ' iEventSeq = iEventSeq + 1
                    Else
                        'Throw exception bad xml
                        bError = True
                        Throw New SystemException("ERROR: ReturnXML did not load/validate correctly.  LynxID returned didn't match Job LynxID.  XML Data formatting errors or database call failed.")
                    End If

                    bError = False
                Catch oExcept As Exception
                    'Throw exception bad xml
                    bError = True
                    Throw New SystemException("<==== No Job data returned or not yet assigned.")
                End Try

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If bDebugMode Then
                    wsAPDFoundation.LogEvent("APDNet", "HQAssignmentDetails - " & sServer, "HQAssignmentDetails.aspx - Ended: " & Date.Now & ", SEQ: " & CStr(iEventSeq), "====> Requesting HQ Assignment Job Status Details: ", "")
                    iEventSeq = iEventSeq + 1
                End If
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDNet", "HQAssignmentDetails - " & sServer, "HQAssignmentDetails.aspx - (ERROR): " & Date.Now & ", SEQ: " & CStr(iEventSeq), "<==== ERROR Occured while processing HQAssignmentDetails.aspx.  Error details in EventXML", sError)
            iEventSeq = iEventSeq + 1

            bError = True
        End Try

    End Sub

End Class