﻿Public Class RentalMgt
    Property RentalID As String
    Property ClaimAspectID As String
    Property RentalAgency As String
    Property RentalAgencyAddress1 As String
    Property RentalAgencyCity As String
    Property RentalAgencyState As String
    Property RentalAgencyZip As String
    Property RentalAgencyPhone As String
    Property ResConfNumber As String
    Property VehicleClass As String
    Property RentalStatus As String
    Property WarrantyFlag As String
    Property RateTypeCD As String
    Property Rate As String
    Property TaxRate As String
    Property TaxedRate As String
    Property Rental As String
    Property AuthPickupDate As String
    Property StartDate As String
    Property EndDate As String
    Property SysLastUserID As String
    Property SysLastUpdatedDate As String
End Class
