﻿Public Class ShopAssignment
    '------------------------------
    ' Root data
    '------------------------------
    Property AssignmentStatus As String
    Property ClaimAspectServiceChannelID As String
    Property InsuranceCompanyID As String

    '------------------------------
    ' ShopAssignment data
    '------------------------------
    Property AssignmentID As String
    Property AssignmentAssignTo As String
    Property AssignmentShopAddress1 As String
    Property AssignmentShopAddress2 As String
    Property AssignmentShopAddressCity As String
    Property AssignmentShopAddressState As String
    Property AssignmentShopAddressZip As String
    Property AssignmentShopAddressCounty As String

    Property AssignmentShopPhone As String
    Property AssignmentShopMSACode As String
    Property AssignmentShopFax As String
    Property AssignmentShopContact As String
    Property AssignmentShopContactPhone As String
    Property AssignmentShopContactEmail As String
    Property AssignmentProgramType As String
    Property AssignmentPreferredCommunicationMethodID As String
    Property AssignmentPreferredCommunicationMethodName As String
    Property AssignmentPreferredEstimatePackageName As String
    Property AssignmentPreferredEstimatePackageID As String
    Property AssignmentContact As String
    Property AssignmentContactPhone As String
    Property AssignmentContactFax As String
    Property AssignmentElecStatus As String
    Property AssignmentFaxStatus As String
    Property AssignmentStatusID As String
    Property AssignmentFaxStatusID As String
    Property AssignmentPrevDate As String
    Property AssignmentAssignDate As String
    Property AssignmentWorkStatus As String
    Property AssignmentDeductSent As String
    Property AssignmentRemarks As String
    Property CurEstGrossRepairTotal As String
    Property CurEstDeductiblesApplied As String
    Property CurEstNetRepairTotal As String
    Property CurEstLimitsEffect As String
    Property WorkStartDateConfirmFlag As String
    Property WorkEndDateConfirmFlag As String
    Property WorkStartDate As String
    Property WorkEndDate As String
    Property AssignmentTypeCD As String
    Property ShopLocationID As String
    Property ServiceChannelCD As String
    Property ShopReferenceID As String
    Property LDAUID As String
    Property AppraiserInvoiceDate As String
    Property CashOutDate As String
    Property CurrentAssignmentTypeID As String
    Property AssignmentSysLastUpdatedDate As String
    Property ClaimServiceChannelCD As String
    Property RepairScheduleChangeReasonID As String
    Property RepairScheduleChangeReason As String
    Property AssignmentSequenceNumber As String
    Property ServiceChannelName As String
    Property ShopLocationCity As String
    Property ShopLocationCounty As String
    Property ShopLocationState As String
    Property InsuranceCompanyName As String
    Property WorkStartConfirmFlag As String
    Property WorkEndConfirmFlag As String
    Property AppraiserName As String
    Property DeductibleAppliedAmt As String
    Property JobID As String
    Property JobStatus As String
    Property ShopID As String
    Property BusinessID As String
    Property RepairLocationCity As String

    Property DTAssignmentHistory As DataTable

    '-------------------------------------
    ' Disposition Data
    '-------------------------------------
    Property DispositionCollection As List(Of ReferenceDisposition)
    Property CurrentDisposition As String

    '-------------------------------------
    ' ReferenceStateList
    '-------------------------------------
    Property StateListCollection As List(Of ReferenceStateList)
    Property CurrentStateList As String

    'Property DTAssignmentCoverage As DataTable

End Class
Public Class ReferenceDisposition
    Property DispositionName As String
    Property ReferenceID As String
End Class

Public Class ReferenceStateList
    Property StateListName As String
    Property ReferenceID As String
End Class