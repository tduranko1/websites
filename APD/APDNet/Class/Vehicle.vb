﻿Imports System.Collections

Public Class Vehicle
    Property ClaimAspectServiceChannelID As String
    Property Vehnumber As Int32
    Property Businessname As String
    Property ClaimAspectID As Int64
    Property VehicleYear As String
    Property Make As String
    Property Model As String
    Property VIN As String
    Property NameFirst As String
    Property NameLast As String
    Property StatusID As Int16
    Property Status As String
    Property ExposureCD As Int16
    Property StatusExposureCD As Int16
    Property CoverageProfileCD As String
    Property OwnerUserFirstName As String
    Property OwnerUserLastName As String
    Property OwnerUserEmail As String
    Property OwnerUserAreaCode As String
    Property OwnerUserExchangeNumber As String
    Property BodyStyle As String
    Property Color As String
    Property ColorEstimateVIN As String
    Property LicensePlateNumber As String
    Property Mileage As String
    Property BookValueAmt As String
    Property RentalDaysAuthorized As String
    Property Remarks As String
    Property InitialAssignmentTypeID As String
    Property LicensePlateStateCollection As List(Of ReferenceState)
    Property LocationStateCollection As List(Of ReferenceState)
    Property ImpactCollection As List(Of ReferenceImpactPoints)
    Property EstimateVIN As String
    Property RentalInstructions As String
    Property DTCoverage As DataTable
    Property ActiveServiceChannel As String
    Property ImpactPoints As String
    Property PriorImpactPoints As String
    Property InitialAssignmentType As String
    Property CurrentLicensePlateState As String
    Property VehicleRemarks As String
    Property CurrentAssignmentTypeID As String
    Property ImpactSpeed As String
    Property PostedSpeed As String
    Property ClientCoverageTypeID As String
    Property PermissionToDriveCD As String
    Property SysLastUpdatedDate As String
    Property FinalEstDate As String
    Property CODate As String
    Property ServiceChannelCD As String
    Property VehDisplayName As String
    Property LynxID As String
    Property UserID As String
    Property InsuranceCompanyID As String
    Property ServiceChannels As String
    Property WarrantyExistsFlag As String


    '-------------------------------------
    ' ImpactPoints
    '-------------------------------------
    Property DTImpactPoints As DataTable
    Property DTPriorImpactPoints As DataTable
    Property StrImpactPoints As String
    Property StrPriorImpactPoints As String

    '-------------------------------------
    ' Location Data
    '-------------------------------------
    Property LocationName As String
    Property LocationAddress1 As String
    Property LocationAddress2 As String
    Property LocationCity As String
    Property LocationState As String
    Property LocationZip As String
    Property LocationPhone As String
    Property CurrentLocationState As String

    '-------------------------------------
    ' Contact Data
    '-------------------------------------
    Property ContactNameTitle As String
    Property ContactNameFirst As String
    Property ContactNameLast As String
    Property ContactAddress1 As String
    Property ContactAddress2 As String
    Property ContactCity As String
    Property ContactState As String
    Property ContactZip As String
    Property ContactEmailAddress As String
    Property AlternateAreaCode As String
    Property AlternateExchangeNumber As String
    Property AlternateExtensionNumber As String
    Property AlternateUnitNumber As String
    Property BestContactPhoneCD As String
    Property BestContactTime As String
    Property DayAreaCode As String
    Property DayExchangeNumber As String
    Property DayExtensionNumber As String
    Property DayUnitNumber As String
    Property NightAreaCode As String
    Property NightExchangeNumber As String
    Property NightExtensionNumber As String
    Property NightUnitNumber As String
    Property InsuredRelationID As String
    Property ContactSysLastUpdatedDate As String
    Property ContactDay As String
    Property ContactNight As String
    Property ContactAlternate As String
    Property ContactCell As String

    Property BestContactTabTime As String

    Property ContactInsuredRelationCollection As List(Of ReferenceInsuredRelation)
    Property BestContactTabPhoneCDCollection As List(Of ReferenceContactTabPhone)

    Property CurContactInsuredRelation As String
    Property CurBestContactTabPhoneCD As String

    Property ContactStateCollection As List(Of ReferenceState)

    '-------------------------------------
    ' Communication Method Data
    '-------------------------------------
    'Property CommunicationMethodCollection As List(Of ReferenceCommunicationMethod)

    '-------------------------------------
    ' Exposure Data
    '-------------------------------------
    Property ExposureCollection As List(Of ReferenceExposure)
    Property CurrentExposure As String

    '-------------------------------------
    ' Involved Data
    '-------------------------------------
    Property InvolvedID As String
    Property InvolvedStateCollection As List(Of ReferenceState)
    Property Involveddata As VehicleInvolved
    Property Involveddatalist As List(Of VehicleInvolvedlst)
    Property InvolvedInsuredCount As Int16
    Property involvedClaimantCount As Int16


    '-------------------------------------
    ' Shop Assignment Data
    '-------------------------------------
    Property ShopAssignmentData As ShopAssignment
    Property DTAssignmentCoverage As DataTable
    Property InspectionDate As String
    Property OriginalEstimateDate As String

    '--------------------------------------
    ' Document Data
    '--------------------------------------
    Public Property Documents As List(Of Documents)
    Property DTDocumentEstimate As DataTable

    '-------------------------------------
    ' Concession Data
    '-------------------------------------
    Property DTConcession As DataTable
    Property ConcessionTotal As Double
    'Property ConcLastUpdated As String
    'Property ConType As String
    'Property ConcReason As String
    'Property ConcAmount As String

    '-------------------------------------
    ' Coverage Data
    '-------------------------------------
    Property CoverageCollection As List(Of ReferenceCoverage)
    Property InitialCoverageProfile As String

    '-------------------------------------
    ' Drivable Data
    '-------------------------------------
    Property DriveableFlag As String

    '-------------------------------------
    ' Warranty Data
    '-------------------------------------
    Property Warrantylst As List(Of Warranty)
    Property WarrantyDocuments As List(Of Documents)
    Property DTWarDocumentEstimate As DataTable

End Class

Public Class Vehicles
    Property vehicleCollection As List(Of Vehicle)
End Class

Public Class ReferenceState
    Property statename As String
    Property ReferenceID As String
End Class

Public Class ReferenceInsuredRelation
    Property Name As String
    Property ReferenceID As String
End Class

Public Class ReferenceContactTabPhone
    Property Name As String
    Property ReferenceID As String
End Class

Public Class ReferenceExposure
    Property ExposureName As String
    Property ReferenceID As String
End Class

Public Class ReferenceCoverage
    Property CoverageName As String
    Property ReferenceID As String
End Class

Public Class ReferenceCommunicationMethod
    Property CommunicationMethodName As String
    Property ReferenceID As String
End Class

Public Class ReferenceImpactPoints
    Property ImpactName As String
    Property ReferenceID As String
End Class