﻿Public Class CarrierRep
    Property OfficeID As Integer
    Property OfficeName As String
    Property Title As String
    Property NameFirst As String
    Property NameLast As String
    Property UserID As Integer
    Property EmailAddress As String
    Property PhoneAreaCode As String
    Property PhoneExchangeNumber As String
    Property PhoneExtensionNumber As String
    Property PhoneUnitNumber As String
    Property FaxAreaCode As String
    Property FaxExchangeNumber As String
    Property FaxExtensionNumber As String
    Property FaxUnitNumber As String
    Property SysLastUpdatedDate As String
End Class
