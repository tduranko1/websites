﻿Public Class VehicleInvolved
    Property InvolvedNameTitle As String
    Property InvolvedNameFirst As String
    Property InvolvedNameLast As String
    Property InvolvedAddress1 As String
    Property InvolvedAddress2 As String
    Property InvolvedBusinessName As String
    Property InvolvedCity As String
    Property InvolvedState As String
    Property InvolvedZip As String
    Property InvolvedEmailAddress As String
    Property InvolvedTypes As List(Of InvolvedType)
    Property InvolvedGenderCD As String
    Property InvolvedFedTaxId As String
    Property InvolvedAge As String
    Property InvolvedBirthDate As String
    Property InvolvedBestContactPhoneCD As String
    Property InvolvedPhoneDay As String
    Property InvolvedPhoneNight As String
    Property InvolvedPhoneAlt As String
    Property InvolvedPhoneCell As String
    Property InvolvedSysLastUpdatedDate As String
    ''collection for loading reference datalist
    Property InvolvedBusinessType As List(Of ReferenceType)
    Property InvolvedGender As List(Of ReferenceType)
    Property InvolvedBestContactPhone As List(Of ReferenceType)
    Property InvolvedBusinessTypeCD As String
    Property InvolvedBestTimeToCall As String


End Class
Public Class ReferenceType
    Property Name As String
    Property ReferenceID As String
End Class

Public Class InvolvedType
    Property InvolvedTypeID As String
    Property InvolvedTypeName As String
End Class

'This class used for data retrive from uspClaimVehicleGetDetailWSXML
Public Class VehicleInvolvedlst
    Property InvolvedID As String
    Property InvolvedNameTitle As String
    Property InvolvedNameFirst As String
    Property InvolvedNameLast As String
    Property InvolvedBusinessName As String
    Property InvolvedCity As String
    Property InvolvedState As String
    Property InvolvedZip As String
    Property InvolvedTypes As String
    Property InvolvedDisplayName As String
    Property InvolvedGender As String
    Property InvolvedBestContactPhoneCD As String
    Property InvolvedBusinessTypeCD As String
    Property InvolvedEmailAddress As String
End Class