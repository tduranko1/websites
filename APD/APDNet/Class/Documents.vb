﻿Public Class Documents
    Public Property DocumentID As String
    Public Property DocumentSourceName As String
    Public Property DocumentTypeName As String
    Public Property CreatedDate As String
    Public Property ReceivedDate As String
    Public Property ImageLocation As String
    Public Property ImageLocationRaw As String
    Public Property ImageLocationThumbnail As String
    Public Property SupplementSeqNumber As String
    Public Property FullSummaryExistsFlag As String
    Public Property AgreedPriceMetCD As String
    Public Property GrossEstimateAmt As String
    Public Property NetEstimateAmt As String
    Public Property EstimateTypeFlag As String
    Public Property DirectionToPayFlag As String
    Public Property FinalEstimateFlag As String
    Public Property DuplicateFlag As String
    Public Property EstimateTypeCD As String
    Public Property SysLastUpdatedDateEstimate As String
    Public Property ImageViewable As String
    Public Property ImageSize As String
    Public Property ImageInfo As String
    Public Property ImageInfolbl As String
    Public Property VanFlag As String
    Public Property ApprovedFlag As String
    Public Property ServiceChannelCD As String
    Public Property SysLastUpdatedDateDocument As String

    '--------------------------------------
    ' Objects Array
    '--------------------------------------
    'Public Property Documents As Object
End Class