﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Data
Imports System.Xml

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class APDFoundationWrapper
    Inherits System.Web.Services.WebService

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService

    '-----------------------------------------------------------
    ' APD: ExecuteSQL
    ' This APD function returns information from the database by
    ' executing a stored procedure without parameters and returning 
    ' a Dataset that represents the data.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns information from the database by executing a stored procedure without parameters and returning a Dataset that represents the data.")>
    Public Function ExecuteSQL(ByVal StoredProcedure As String) As DataSet
        Dim dsReturnData As New DataSet

        Try

            dsReturnData = wsAPDFoundation.ExecuteSQL(StoredProcedure)

            Return dsReturnData
        Catch oExcept As Exception

            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDNet", "ERROR", "APDFoundataionWrapper webservice error.", oExcept.Message, sError)

            Return dsReturnData
        Finally
            dsReturnData = Nothing

        End Try


    End Function



    '-----------------------------------------------------------
    ' APD: ExecuteSp
    ' This APD function executes the specified stored procedure
    ' and returns the number of records modified
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function executes the specified stored procedure and returns the number of records modified")>
    Public Function ExecuteSp(ByVal StoredProcedure As String, ByVal Parameters As String) As String
        Dim sRecordsAffected As String = ""
        Try
            sRecordsAffected = wsAPDFoundation.ExecuteSp(StoredProcedure, Parameters)

            Return sRecordsAffected
        Catch oExcept As Exception

            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDNet", "ERROR", "APDFoundataionWrapper webservice error.", oExcept.Message, sError)

            Return sRecordsAffected
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: ExecuteSpAsString
    ' This APD function executes the specified stored procedure
    ' and returns the results as a string
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function executes the specified stored procedure and returns the results as string")>
    Public Function ExecuteSpAsString(ByVal StoredProcedure As String, ByVal Parameters As String) As String
        Dim sReturnData As String = ""

        Try
            sReturnData = wsAPDFoundation.ExecuteSpAsString(StoredProcedure, Parameters)

            Return sReturnData
        Catch oExcept As Exception

            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDNet", "ERROR", "APDFoundataionWrapper webservice error.", oExcept.Message, sError)

            Return sReturnData
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: ExecuteSpAsXML
    ' This APD function executes the specified WSXML stored procedure
    ' and returns the raw XML
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function executes the specified WSXML stored procedure and returns the raw XML")>
    Public Function ExecuteSpAsXML(ByVal StoredProcedure As String, ByVal Parameters As String) As XmlDocument
        Dim xdoc As New XmlDocument
        Dim xnode As XmlNode

        Try
            xnode = wsAPDFoundation.ExecuteSpAsXML(StoredProcedure, Parameters)

            xdoc.LoadXml(xnode.OuterXml)

            Return xdoc
        Catch oExcept As Exception

            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDNet", "ERROR", "APDFoundataionWrapper webservice error.", oExcept.Message, sError)

            Return xdoc
        End Try
    End Function

    '-------------------------------------------------------------------
    ' APD: XslGetSession
    ' This APD function gets a specific veriable value from the session
    ' control database by SessionKey.
    '-------------------------------------------------------------------
    <WebMethod(Description:="This APD function gets a specific veriable value from the session control database by SessionKey.")>
    Public Function XslGetSession(ByVal SessionKey As String, ByVal SessionVariable As String) As String
        Dim sReturnData As String = ""

        Try
            sReturnData = wsAPDFoundation.XslGetSession(SessionKey, SessionVariable)

        Catch oExcept As Exception

            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDNet", "ERROR", "APDFoundataionWrapper webservice error.", oExcept.Message, sError)
        End Try

        Return sReturnData
    End Function

    '-----------------------------------------------------------
    ' APD: UpdateSessionVar
    ' This APD function updates a APD session variable 
    ' by GUID and WindowID.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function updates a APD session variable by GUID and WindowID.")>
    Public Function UpdateSessionVar(ByVal SessionID As String, ByVal SessionVariable As String, ByVal SessionValue As String) As Integer
        Dim iReturnData As Integer = 0

        Try
            iReturnData = wsAPDFoundation.UpdateSessionVar(SessionID, SessionVariable, SessionValue)

        Catch oExcept As Exception

            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDNet", "ERROR", "APDFoundataionWrapper webservice error.", oExcept.Message, sError)
        End Try

        Return iReturnData

    End Function
End Class