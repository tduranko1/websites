﻿Imports System.Xml
Imports System.Configuration.ConfigurationManager

Public Class RentalCoverage
    Inherits System.Web.UI.Page

    Public sAPDNetDebugging As String = ""

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    Dim sLynxID As String = ""
    Dim iInsuranceCompanyID As Integer = 0
    'Dim iSelectedCoverageTypeID As Integer
    Dim sUserID As String
    Dim hCoverageApplied As Hashtable = New Hashtable
    Dim oRentalMgt As RentalMgt
    Dim DTRentalData As New DataTable
    Dim iClaimAspectID As Integer = 0
    Dim bWarranty As Boolean = False


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sRC As String = ""

        '---------------------------------
        ' Turn debugging on or off
        '---------------------------------
        sAPDNetDebugging = ""
        If UCase(AppSettings("Debug")) = "TRUE" Then
            sAPDNetDebugging = "TRUE"
        End If

        Try
            '-------------------------------
            ' Check Session Data
            '-------------------------------
            sLynxID = Request.QueryString("LynxID")
            iInsuranceCompanyID = Request.QueryString("InscCompID")
            iClaimAspectID = Request.QueryString("ClaimAspectID")
            sUserID = Request.QueryString("UserID")

            txtRentalLynxID.Text = sLynxID

            '-------------------------------
            ' Get the Vehicle XML and update 
            ' fields.
            '-------------------------------

            '-------------------------------
            ' Clean-up
            '-------------------------------
            If Not IsPostBack Then
                sRC = GetRentalData(iInsuranceCompanyID, iClaimAspectID, sLynxID)
                '-------------------------------
                ' Hide Required Indicator
                '-------------------------------
                lblRentalAgency.Visible = False
                lblRentalVehicleClass.Visible = False
                lblRentalAgency.Visible = False
                lblRentalResConf.Visible = False
                lblRentalRateType.Visible = False
                lblRentalRate.Visible = False
                lblRentalStatus.Visible = False
                lblRentalAuthPickup.Visible = False
                lblRentalVehicleClass.Visible = False
                lblRentalTaxRate.Visible = False
                lblRentalTaxedRate.Visible = False
                lblRentalDollars.Visible = False
                lblRentalStartDate.Visible = False
                lblRentalEndDate.Visible = False
                lblRentalPhone.Visible = False
                lblRentalAddress.Visible = False
                lblRentalCity.Visible = False
                lblRentalState.Visible = False
                lblRentalZip.Visible = False

                ddlRentalAgency.Enabled = False
                txtRentalResConf.Enabled = False
                ddlRentalVehicleClass.Enabled = False
                ddlRentalStatus.Enabled = False
                ddlRentalRateType.Enabled = False
                chkWarranty.Enabled = False
                txtRentalRate.Enabled = False
                txtRentalAuthPickup.Enabled = False
                txtRentalTaxRate.Enabled = False
                txtRentalStartDate.Enabled = False
                txtRentalEndDate.Enabled = False
                txtRentalPhone.Enabled = False
                txtRentalAddress.Enabled = False
                txtRentalCity.Enabled = False
                txtRentalState.Enabled = False
                txtRentalZip.Enabled = False

                btnRentalSave.Visible = False
                btnRentalUpdate.Visible = False
            End If


        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("RentalCoverage", "ERROR", "Rental Coverage data not found or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
        Finally
        End Try
    End Sub

    Protected Function GetRentalData(ByVal iInsuranceCompanyID As Integer, ByVal iClaimAspectID As Integer, ByVal sLynxID As String) As String
        '-------------------------------
        ' Session/Local Variables
        '-------------------------------  
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim iGrandTotalRepairCycle As Integer = 0
        Dim iGrandTotalRental As Decimal = 0
        Dim iGrandTotalRentalDays As Integer = 0
        Dim sDrivable As String = ""
        Dim sLossDate As String = ""
        Dim iWarrantyRepairCycle As Long = 0
        Dim iRepairRepairCycle As Long = 0
        Dim sWarrantyStartDate As String = ""
        Dim sWarrantyEndDate As String = ""
        Dim sRepairStartDate As String = ""
        Dim sRepairEndDate As String = ""


        '-------------------------------
        ' Database access
        '-------------------------------  
        Dim sStoredProcedure As String = "uspRentalMgtGetDetailsWSXML"
        Dim sParams As String = sLynxID & "," & iClaimAspectID & "," & iInsuranceCompanyID

        '---------------------------------
        ' Debug Data
        '---------------------------------
        If UCase(sAPDNetDebugging) = "TRUE" Then
            wsAPDFoundation.LogEvent("APDNET-RentalCoverage", "GetRentalData()", "GetRentalData - Started: " & Date.Now, "", "")
        End If

        Try
            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-RentalCoverage", "GetRentalData(DBEXEC)", "GetRentalData: " & Date.Now, "StoredProc: " & sStoredProcedure & " Params: " & sParams, "Results: " & oReturnXML.InnerXml)
            End If

            '---------------------------------------
            ' Load Rental data 
            '---------------------------------------
            oXMLNode = oReturnXML.SelectSingleNode("ClaimAspect")

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-RentalCoverage", "GetRentalData(XMLParse)", "GetRentalData: " & Date.Now, "ClaimAspect Node: ", "Results: " & oXMLNode.OuterXml)
            End If

            If Not (oXMLNode) Is Nothing Then
                txtRentalParty.Text = GetXMLNodeattrbuteValue(oXMLNode, "ExposureCD")
                txtRentalCoverage.Text = GetXMLNodeattrbuteValue(oXMLNode, "CoverageProfileCD")

                txtRentalYear.Text = GetXMLNodeattrbuteValue(oXMLNode, "VehicleYear")
                txtRentalMake.Text = GetXMLNodeattrbuteValue(oXMLNode, "Make")
                txtRentalModel.Text = GetXMLNodeattrbuteValue(oXMLNode, "Model")
                txtRentalDaysAuthorized.Text = GetXMLNodeattrbuteValue(oXMLNode, "RentalDaysAuthorized")
                txtRentalDays.Text = GetXMLNodeattrbuteValue(oXMLNode, "RentalDaysAuthorized")
                txtRentalInstructions.Text = GetXMLNodeattrbuteValue(oXMLNode, "RentalInstructions")

                If GetXMLNodeattrbuteValue(oXMLNode, "WarrantyStartDate") <> "" Then
                    hidWarrantyStart.Value = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "WarrantyStartDate")), "MM/dd/yyyy")
                End If
                If GetXMLNodeattrbuteValue(oXMLNode, "WarrantyEndDate") <> "" Then
                    hidWarrantyEnd.Value = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "WarrantyEndDate")), "MM/dd/yyyy")
                End If

                If chkWarranty.Checked Then
                    If GetXMLNodeattrbuteValue(oXMLNode, "WarrantyStartDate") <> "" Then
                        txtRentalRepairStart.Text = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "WarrantyStartDate")), "MM/dd/yyyy")
                        'hidWarrantyStart.Value = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "WarrantyStartDate")), "MM/dd/yyyy")
                        txtRentalRepairEnd.Text = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "WarrantyEndDate")), "MM/dd/yyyy")
                        'hidWarrantyEnd.Value = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "WarrantyEndDate")), "MM/dd/yyyy")
                        txtRentalRepairCycle.Text = DateDiff("d", txtRentalRepairStart.Text, txtRentalRepairEnd.Text)
                    Else
                        txtRentalRepairStart.Text = ""
                        'hidWarrantyStart.Value = ""
                        txtRentalRepairEnd.Text = ""
                        'hidWarrantyEnd.Value = ""
                    End If
                Else
                    If GetXMLNodeattrbuteValue(oXMLNode, "RepairStartDate") <> "" Then
                        txtRentalRepairStart.Text = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "RepairStartDate")), "MM/dd/yyyy")
                        hidRepairStart.Value = txtRentalRepairStart.Text
                        txtRentalRepairEnd.Text = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "RepairEndDate")), "MM/dd/yyyy")
                        hidRepairEnd.Value = txtRentalRepairEnd.Text
                        txtRentalRepairCycle.Text = DateDiff("d", txtRentalRepairStart.Text, txtRentalRepairEnd.Text)
                    Else
                        txtRentalRepairStart.Text = ""
                        hidRepairStart.Value = ""
                        txtRentalRepairEnd.Text = ""
                        hidRepairEnd.Value = ""
                    End If
                End If

                txtRentalDateOfLoss.Text = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "LossDate")), "MM/dd/yyyy")
                sDrivable = GetXMLNodeattrbuteValue(oXMLNode, "DrivableFlag")
                sLossDate = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "LossDate")), "MM/dd/yyyy")

                Select Case GetXMLNodeattrbuteValue(oXMLNode, "DrivableFlag")
                    Case 1
                        txtRentalDrivable.Text = "Yes"
                        If GetXMLNodeattrbuteValue(oXMLNode, "RepairStartDate") <> "" Then
                            txtRentalNoOfDays.Text = DateDiff("d", txtRentalRepairStart.Text, txtRentalRepairEnd.Text) + 1
                            If GetXMLNodeattrbuteValue(oXMLNode, "RentalStartDate") <> "" Then
                                txtRentalStartDate.Text = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "RentalStartDate")), "MM/dd/yyyy")
                                txtRentalEndDate.Text = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "RentalEndDate")), "MM/dd/yyyy")
                            End If
                        End If
                        lblLossDateIndicator.Text = ""
                    Case Else
                        txtRentalDrivable.Text = "No"

                        txtRentalStartDate.Text = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "LossDate")), "MM/dd/yyyy")
                        txtRentalNoOfDays.Text = 0

                        If txtDaysMax.Text <> "" And txtRentalDays.Text <> "" Then
                            txtRentalRepairCycle.Text = txtRentalRepairCycle.Text + 1
                        End If
                        lblLossDateIndicator.Text = "* Loss"
                End Select

                '------------------------------
                ' Check is Warranty Assignment
                ' has been processed
                '------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "WarrantyAssignment") = "0" Then
                    txtWarranty.Text = "No"
                    bWarranty = False
                Else
                    txtWarranty.Text = "Yes"
                    bWarranty = True
                End If

                '------------------------------
                ' Calculate Total Repair Cycle
                '------------------------------
                sWarrantyStartDate = GetXMLNodeattrbuteValue(oXMLNode, "WarrantyStartDate")
                sWarrantyEndDate = GetXMLNodeattrbuteValue(oXMLNode, "WarrantyEndDate")
                sRepairStartDate = GetXMLNodeattrbuteValue(oXMLNode, "RepairStartDate")
                sRepairEndDate = GetXMLNodeattrbuteValue(oXMLNode, "RepairEndDate")

                If sWarrantyStartDate <> "" And sWarrantyEndDate <> "" Then
                    iWarrantyRepairCycle = DateDiff(DateInterval.Day, CDate(GetXMLNodeattrbuteValue(oXMLNode, "WarrantyStartDate")), CDate(GetXMLNodeattrbuteValue(oXMLNode, "WarrantyEndDate")))
                Else
                    iWarrantyRepairCycle = 0
                End If

                If sRepairStartDate <> "" And sRepairEndDate <> "" Then
                    iRepairRepairCycle = DateDiff(DateInterval.Day, CDate(GetXMLNodeattrbuteValue(oXMLNode, "RepairStartDate")), CDate(GetXMLNodeattrbuteValue(oXMLNode, "RepairEndDate")))
                Else
                    iRepairRepairCycle = 0
                End If

                iGrandTotalRepairCycle += iWarrantyRepairCycle + iRepairRepairCycle
            End If

            oXMLNode = oReturnXML.SelectSingleNode("Contact")

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-RentalCoverage", "GetRentalData(XMLParse)", "GetRentalData: " & Date.Now, "Contact Node: ", "Results: " & oXMLNode.OuterXml)
            End If

            If Not (oXMLNode) Is Nothing Then
                txtRentalInsured.Text = GetXMLNodeattrbuteValue(oXMLNode, "InsuredName")
                txtRentalClaimant.Text = GetXMLNodeattrbuteValue(oXMLNode, "RenterName")
            End If

            '---------------------------------
            ' Rental Mgt Level
            '---------------------------------
            'oXMLNode = oReturnXML.SelectSingleNode("RentalMgt")
            'If Not (oXMLNode) Is Nothing Then
            '    '---------------------------------
            '    ' Debug Data
            '    '---------------------------------
            '    If UCase(sAPDNetDebugging) = "TRUE" Then
            '        wsAPDFoundation.LogEvent("APDNET-RentalMgt", "GetRentalData(XMLParse)", "GetRentalData: " & Date.Now, "RemtalMgt Node: ", "Results: " & oXMLNode.OuterXml)
            '    End If

            '    '---------------------------------
            '    ' Get RentalMgt - Warrant Tag
            '    '---------------------------------
            '    chkWarranty.Checked = GetXMLNodeattrbuteValue(oXMLNode, "Warranty")
            'End If


            oXMLNode = oReturnXML.SelectSingleNode("Coverage")

            If Not (oXMLNode) Is Nothing Then
                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNET-RentalCoverage", "GetRentalData(XMLParse)", "GetRentalData: " & Date.Now, "Coverage Node: ", "Results: " & oXMLNode.OuterXml)
                End If

                '----- Check 1st party vs 3rd and adjust max
                If txtRentalParty.Text = "1" Then
                    txtRentalDailyMax.Text = FormatNumber(Val(GetXMLNodeattrbuteValue(oXMLNode, "LimitDailyAmt")), 2)
                    txtDaysMax.Text = GetXMLNodeattrbuteValue(oXMLNode, "MaximumDays")

                    If txtRentalDailyMax.Text <> "" And txtDaysMax.Text <> "" Then
                        txtRentalMax.Text = FormatNumber(Val(txtRentalDailyMax.Text * txtDaysMax.Text), 2)
                    End If
                Else
                    txtRentalDailyMax.Text = "0"
                    txtRentalMax.Text = "0"
                    txtDaysMax.Text = "0"
                    txtRentalMaxDays.Text = "0"
                End If

                txtRentalMaxRentals.Text = txtRentalMax.Text
                txtRentalMaxDays.Text = txtDaysMax.Text

                If txtDaysMax.Text <> "" And txtRentalDays.Text <> "" Then
                    txtRentalAvailableDays.Text = txtDaysMax.Text - txtRentalDays.Text
                End If
            End If

            '---------------------------------------
            ' Load Rental data 
            '---------------------------------------
            Dim DCRentalData As New DataColumn

            DTRentalData.Columns.Add(New DataColumn("RentalID", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("RentalAgency", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("ResConfNumber", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("RateTypeCD", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("RentalStatus", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("ClaimAspectID", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("VehicleClass", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("Warranty", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("Rate", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("TaxRate", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("TaxedRate", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("Rental", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("RentalNoOfDays", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("AuthPickupDate", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("StartDate", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("EndDate", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("RentalAgencyAddress1", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("RentalAgencyCity", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("RentalAgencyState", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("RentalAgencyZip", GetType(String)))
            DTRentalData.Columns.Add(New DataColumn("RentalAgencyPhone", GetType(String)))

            oXMLNodeList = oReturnXML.SelectNodes("RentalMgt")
            If oXMLNodeList.Count > 0 Then
                For Each oXMLNode In oXMLNodeList
                    '---------------------------------
                    ' Debug Data
                    '---------------------------------
                    If UCase(sAPDNetDebugging) = "TRUE" Then
                        wsAPDFoundation.LogEvent("APDNET-RentalCoverage", "GetRentalData(XMLParse)", "GetRentalData: " & Date.Now, "RentalMgt Node: ", "Results: " & oXMLNode.OuterXml)
                    End If

                    Dim DRRentalData As DataRow = DTRentalData.NewRow
                    Try
                        DRRentalData("RentalAgency") = GetXMLNodeattrbuteValue(oXMLNode, "RentalAgency")
                        DRRentalData("ResConfNumber") = GetXMLNodeattrbuteValue(oXMLNode, "ResConfNumber")
                        DRRentalData("RateTypeCD") = GetXMLNodeattrbuteValue(oXMLNode, "RateTypeCD")
                        DRRentalData("RentalStatus") = GetXMLNodeattrbuteValue(oXMLNode, "RentalStatus")

                        DRRentalData("RentalID") = GetXMLNodeattrbuteValue(oXMLNode, "RentalID")

                        DRRentalData("ClaimAspectID") = GetXMLNodeattrbuteValue(oXMLNode, "ClaimAspectID")

                        DRRentalData("VehicleClass") = GetXMLNodeattrbuteValue(oXMLNode, "VehicleClass")
                        DRRentalData("Warranty") = GetXMLNodeattrbuteValue(oXMLNode, "Warranty")

                        DRRentalData("Rate") = FormatNumber(Val(GetXMLNodeattrbuteValue(oXMLNode, "Rate")), 2)
                        DRRentalData("TaxRate") = FormatNumber(Val(GetXMLNodeattrbuteValue(oXMLNode, "TaxRate")), 2)
                        DRRentalData("TaxedRate") = FormatNumber(Val(GetXMLNodeattrbuteValue(oXMLNode, "TaxedRate")), 2)
                        DRRentalData("Rental") = FormatNumber(Val(GetXMLNodeattrbuteValue(oXMLNode, "Rental")), 2)

                        DRRentalData("RentalNoOfDays") = GetXMLNodeattrbuteValue(oXMLNode, "RentalNoOfDays")
                        If GetXMLNodeattrbuteValue(oXMLNode, "AuthPickupDate") <> "" Then
                            DRRentalData("AuthPickupDate") = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "AuthPickupDate")), "MM/dd/yyyy")
                        Else
                            DRRentalData("AuthPickupDate") = ""
                        End If
                        If GetXMLNodeattrbuteValue(oXMLNode, "RentalStartDate") <> "" Then
                            DRRentalData("StartDate") = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "RentalStartDate")), "MM/dd/yyyy")
                        Else
                            DRRentalData("StartDate") = ""
                        End If
                        If GetXMLNodeattrbuteValue(oXMLNode, "RentalEndDate") <> "" Then
                            DRRentalData("EndDate") = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "RentalEndDate")), "MM/dd/yyyy")
                        Else
                            DRRentalData("EndDate") = ""
                        End If
                        'DRRentalData("StartDate") = Format(GetXMLNodeattrbuteValue(oXMLNode, "RentalStartDate"), "MM/dd/yyyy")
                        'DRRentalData("EndDate") = Format(GetXMLNodeattrbuteValue(oXMLNode, "RentalEndDate"), "MM/dd/yyyy")
                        DRRentalData("RentalAgencyAddress1") = GetXMLNodeattrbuteValue(oXMLNode, "RentalAgencyAddress1")
                        DRRentalData("RentalAgencyCity") = GetXMLNodeattrbuteValue(oXMLNode, "RentalAgencyCity")
                        DRRentalData("RentalAgencyState") = GetXMLNodeattrbuteValue(oXMLNode, "RentalAgencyState")
                        DRRentalData("RentalAgencyZip") = GetXMLNodeattrbuteValue(oXMLNode, "RentalAgencyZip")
                        DRRentalData("RentalAgencyPhone") = GetXMLNodeattrbuteValue(oXMLNode, "RentalAgencyPhone")

                        DTRentalData.Rows.Add(DRRentalData)
                    Catch ex As Exception
                        Throw New SystemException("RentalMgt Node Exception: One or more XML values not found.  Exception: " & ex.ToString)
                    End Try

                    '---------------------------------
                    ' Debug Data
                    '---------------------------------
                    If UCase(sAPDNetDebugging) = "TRUE" Then
                        wsAPDFoundation.LogEvent("APDNET-RentalCoverage", "GetRentalData(Calculations)", "GetRentalData: " & Date.Now, "RentalMgt Calculations: ", "")
                    End If

                    '---------------------------
                    ' All Rental Totals Calcs
                    '---------------------------
                    Try
                        iGrandTotalRentalDays += CInt(DRRentalData("RentalNoOfDays"))
                        txtRentalTotalRentalDays.Text = CStr(iGrandTotalRentalDays)
                        txtRentalDays.Text = txtRentalTotalRentalDays.Text

                        txtRentalTotalRepairCycle.Text = iGrandTotalRepairCycle
                        txtRentalAvailableDays.Text = (txtRentalMaxDays.Text - txtRentalDays.Text)

                        If (txtRentalTotalRepairCycle.Text <> "" And txtRentalTotalRentalDays.Text <> "") Then
                            txtRentalSavedSync.Text = txtRentalTotalRepairCycle.Text - txtRentalTotalRentalDays.Text
                        End If

                        'If DRRentalData("Rental") <> "" Then
                        iGrandTotalRental += CDbl(DRRentalData("Rental"))
                        txtRentalTotal.Text = FormatNumber(Val(CDbl(iGrandTotalRental)), 2)
                        'End If

                        If (txtRentalMaxRentals.Text <> "" And txtRentalTotal.Text <> "") Then
                            txtRentalAvailablePlusMinus.Text = FormatNumber(Val(txtRentalMaxRentals.Text - txtRentalTotal.Text), 2)
                        End If
                    Catch ex As Exception
                        Throw New SystemException("RentalMgt Calculation Exception: One or more of the Calculations failed.  Exception: " & ex.ToString)
                    End Try

                    '---------------------------------
                    ' Debug Data
                    '---------------------------------
                    If UCase(sAPDNetDebugging) = "TRUE" Then
                        wsAPDFoundation.LogEvent("APDNET-RentalCoverage", "GetRentalData(Class)", "GetRentalData: " & Date.Now, "RentalMgt Class object update", "")
                    End If

                    '-----------------------------------
                    ' Populate the RentalMgt Class
                    '-----------------------------------
                    oRentalMgt = New RentalMgt
                    oRentalMgt.RentalID = GetXMLNodeattrbuteValue(oXMLNode, "RentalID")
                    oRentalMgt.ClaimAspectID = GetXMLNodeattrbuteValue(oXMLNode, "ClaimAspectID")
                    oRentalMgt.RentalAgency = GetXMLNodeattrbuteValue(oXMLNode, "RentalAgency")
                    oRentalMgt.RentalAgencyAddress1 = GetXMLNodeattrbuteValue(oXMLNode, "RentalAgencyAddress1")
                    oRentalMgt.RentalAgencyCity = GetXMLNodeattrbuteValue(oXMLNode, "RentalAgencyCity")
                    oRentalMgt.RentalAgencyState = GetXMLNodeattrbuteValue(oXMLNode, "RentalAgencyState")
                    oRentalMgt.RentalAgencyZip = GetXMLNodeattrbuteValue(oXMLNode, "RentalAgencyZip")
                    oRentalMgt.RentalAgencyPhone = GetXMLNodeattrbuteValue(oXMLNode, "RentalAgencyPhone")
                    oRentalMgt.ResConfNumber = GetXMLNodeattrbuteValue(oXMLNode, "ResConfNumber")
                    oRentalMgt.VehicleClass = GetXMLNodeattrbuteValue(oXMLNode, "VehicleClass")
                    oRentalMgt.RentalStatus = GetXMLNodeattrbuteValue(oXMLNode, "RentalStatus")
                    oRentalMgt.WarrantyFlag = GetXMLNodeattrbuteValue(oXMLNode, "Warranty")
                    oRentalMgt.RateTypeCD = GetXMLNodeattrbuteValue(oXMLNode, "RateTypeCD")
                    oRentalMgt.Rate = GetXMLNodeattrbuteValue(oXMLNode, "Rate")
                    oRentalMgt.TaxRate = GetXMLNodeattrbuteValue(oXMLNode, "TaxRate")
                    oRentalMgt.TaxedRate = GetXMLNodeattrbuteValue(oXMLNode, "Rental")
                    oRentalMgt.Rental = GetXMLNodeattrbuteValue(oXMLNode, "Rental")
                    oRentalMgt.AuthPickupDate = GetXMLNodeattrbuteValue(oXMLNode, "AuthPickupDate")
                    oRentalMgt.StartDate = GetXMLNodeattrbuteValue(oXMLNode, "StartDate")
                    oRentalMgt.EndDate = GetXMLNodeattrbuteValue(oXMLNode, "EndDate")
                    oRentalMgt.SysLastUserID = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUserID")
                    oRentalMgt.SysLastUpdatedDate = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate")

                    'sCoverageTypeCD = GetXMLNodeattrbuteValue(oXMLNode, "ClaimCoverageID")
                    'If UCase(sCoverageTypeCD) = "RENT" Then
                    '    txtRentalDailyMax.Text = GetXMLNodeattrbuteValue(oXMLNode, "LimitDailyAmt")
                    '    txtRentalMaxDays.Text = GetXMLNodeattrbuteValue(oXMLNode, "MaximumDays")
                    '    txtRentalTotal.Text = GetXMLNodeattrbuteValue(oXMLNode, "LimitAmt")

                    'End If
                Next
            Else
                Dim DRRentalData As DataRow = DTRentalData.NewRow
                DRRentalData("RentalAgency") = "No Data..."
                DTRentalData.Rows.Add(DRRentalData)
            End If

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-RentalCoverage", "GetRentalData(gvRentals)", "Gridview hide: " & Date.Now, "Hiding columns in the gvRentals GridView", "")
            End If

            '---------------------------------
            ' Service Channel Grid
            '---------------------------------
            If Not DTRentalData Is Nothing Then
                gvRentals.DataSource = DTRentalData
                gvRentals.DataBind()
                gvRentals.Columns(0).Visible = True
                'gvRentals.Columns(5).Visible = True
                'gvRentals.Columns(6).Visible = True
                'gvRentals.Columns(7).Visible = True
                'gvRentals.Columns(8).Visible = True
                'gvRentals.Columns(9).Visible = True
                'gvRentals.Columns(10).Visible = True
                'gvRentals.Columns(11).Visible = True
                'gvRentals.Columns(12).Visible = True
                'gvRentals.Columns(13).Visible = True
                'gvRentals.Columns(14).Visible = True
                'gvRentals.Columns(15).Visible = True
                'gvRentals.Columns(16).Visible = True
                'gvRentals.Columns(17).Visible = True
                'gvRentals.Columns(18).Visible = True
                'gvRentals.Columns(19).Visible = True
                'gvRentals.Columns(20).Visible = True
                gvRentals.Visible = True
            End If

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-RentalCoverage", "GetRentalData()", "GetRentalData - Ended: " & Date.Now, "", "")
            End If

            Return ""
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("GetRentalData", "ERROR", "Rental Data Not found Or data error occured.", oExcept.Message, sError)

            Return oExcept.ToString
        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
            oXMLNodeList = Nothing
        End Try
    End Function

    'Protected Function GetCoverageData(ByVal sLynxID As String, ByVal iInsuranceCompanyID As Integer)
    '    '-------------------------------
    '    ' Session/Local Variables
    '    '-------------------------------  
    '    Dim oReturnXML As XmlElement = Nothing
    '    Dim oXMLNode As XmlNode = Nothing
    '    Dim oXMLNodeList As XmlNodeList = Nothing
    '    Dim sCoverageTypeCD As String = ""

    '    '-------------------------------
    '    ' Database access
    '    '-------------------------------  
    '    Dim sStoredProcedure As String = "uspClaimCoverageGetDetailsWSXML"
    '    Dim sParams As String = sLynxID & "," & iInsuranceCompanyID

    '    Try
    '        '-------------------------------
    '        ' Call WS and get data
    '        '-------------------------------
    '        oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

    '        '---------------------------------------
    '        ' Load Vehicle data 
    '        '---------------------------------------
    '        'oXMLNode = oReturnXML.SelectSingleNode("Coverage/CoverageTypeCD='RENT'")

    '        oXMLNodeList = oReturnXML.SelectNodes("Coverage")
    '        If oXMLNodeList.Count > 0 Then
    '            For Each oXMLNode In oXMLNodeList
    '                sCoverageTypeCD = GetXMLNodeattrbuteValue(oXMLNode, "ClaimCoverageID")
    '                If UCase(sCoverageTypeCD) = "RENT" Then
    '                    txtRentalDailyMax.Text = GetXMLNodeattrbuteValue(oXMLNode, "LimitDailyAmt")
    '                    txtRentalMaxDays.Text = GetXMLNodeattrbuteValue(oXMLNode, "MaximumDays")

    '                End If
    '            Next
    '        End If

    '        'If Not (oXMLNode) Is Nothing Then
    '        'txtRentalDailyMax.Text = GetXMLNodeattrbuteValue(oXMLNode, "LimitDailyAmt")

    '        'Select Case GetXMLNodeattrbuteValue(oXMLNode, "DriveableFlag")
    '        '    Case 1
    '        '        txtRentalDrivable.Text = "Yes"
    '        '    Case Else
    '        '        txtRentalDrivable.Text = "No"
    '        'End Select
    '        'End If

    '    Catch oExcept As Exception
    '        '---------------------------------
    '        ' Error handler and notifications
    '        '---------------------------------
    '        Dim sError As String = ""
    '        Dim sBody As String = ""
    '        Dim FunctionName As New System.Diagnostics.StackFrame

    '        sError = String.Format("Error {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
    '        wsAPDFoundation.LogEvent("GetVehicleData", "ERROR", "Vehicle Data Not found Or data error occured.", oExcept.Message, sError)

    '        Response.Write(oExcept.ToString)
    '    Finally
    '        oXMLNode = Nothing
    '        oReturnXML = Nothing
    '        oXMLNodeList = Nothing
    '    End Try
    'End Function

    Protected Function GetXMLNodeattrbuteValue(ByVal XMLnode As XmlNode, strAttributes As String) As String
        Try
            Dim strRetVal As String = ""
            If Not XMLnode.Attributes(strAttributes) Is Nothing Then
                strRetVal = XMLnode.Attributes(strAttributes).InnerText
            Else
                strRetVal = ""
            End If
            Return strRetVal
        Catch ex As Exception
            wsAPDFoundation.LogEvent("GetXMLNodeattrbuteValue", "XMLERROR", "GetXMLNodeattrbuteValue - Parse: " & Date.Now, "ERROR: Parsing XMLNode Attribute.", "XMLNodeAttribute = " & strAttributes)
            Return ex.ToString
        End Try
    End Function

    Protected Sub gvRentals_RowDataBound(ByVal sender As Object, ByVal e As EventArgs)
        'If gvRentals.SelectedIndex = -1 Then
        '    gvRentals.SelectedIndex = 1
        'End If
    End Sub

    Protected Sub gvRentals_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim iSelectedRowIndex As Integer = 0
        Dim sSelectedRowType As String = ""

        '---------------------------------
        ' Debug Data
        '---------------------------------
        If UCase(sAPDNetDebugging) = "TRUE" Then
            wsAPDFoundation.LogEvent("APDNET-RentalCoverage", "gvRentals_SelectedIndexChanged()", "ReIndex fields - Started: " & Date.Now, "SelectedIndex", "SelectedIndex: " & gvRentals.SelectedIndex)
        End If

        Try
            iSelectedRowIndex = gvRentals.SelectedIndex

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                'wsAPDFoundation.LogEvent("APDNET-RentalCoverage", "gvRentals_SelectedIndexChanged()", "DTRentalData values" & Date.Now, "Return Data in EventXML", "Data: " & DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).ToString)
            End If

            ddlRentalAgency.SelectedValue = gvRentals.SelectedRow.Cells(1).Text
            txtRentalResConf.Text = gvRentals.SelectedRow.Cells(2).Text
            ddlRentalRateType.SelectedValue = gvRentals.SelectedRow.Cells(3).Text
            ddlRentalStatus.SelectedValue = gvRentals.SelectedRow.Cells(4).Text
            ddlRentalVehicleClass.SelectedValue = gvRentals.SelectedRow.Cells(7).Text
            If gvRentals.SelectedRow.Cells(5).Text = Nothing Or gvRentals.SelectedRow.Cells(5).Text = "" Or gvRentals.SelectedRow.Cells(5).Text = "&nbsp;" Or gvRentals.SelectedRow.Cells(5).Text = "0" Then
                chkWarranty.Checked = False
            Else
                chkWarranty.Checked = True
            End If
            txtRentalRate.Text = gvRentals.SelectedRow.Cells(8).Text
            txtRentalTaxRate.Text = gvRentals.SelectedRow.Cells(9).Text
            txtRentalTaxedRate.Text = gvRentals.SelectedRow.Cells(10).Text
            txtRentalDollars.Text = gvRentals.SelectedRow.Cells(11).Text
            txtRentalAuthPickup.Text = gvRentals.SelectedRow.Cells(13).Text
            If gvRentals.SelectedRow.Cells(14).Text = "&nbsp;" Then
                txtRentalStartDate.Text = ""
            Else
                txtRentalStartDate.Text = gvRentals.SelectedRow.Cells(14).Text
            End If
            If gvRentals.SelectedRow.Cells(15).Text = "&nbsp;" Then
                txtRentalEndDate.Text = ""
            Else
                txtRentalEndDate.Text = gvRentals.SelectedRow.Cells(15).Text
            End If

            If gvRentals.SelectedRow.Cells(13).Text = "&nbsp;" Then gvRentals.SelectedRow.Cells(14).Text = ""
            If gvRentals.SelectedRow.Cells(14).Text = "&nbsp;" Then gvRentals.SelectedRow.Cells(15).Text = ""
            If gvRentals.SelectedRow.Cells(13).Text <> "" And gvRentals.SelectedRow.Cells(15).Text <> "" Then
                txtRentalNoOfDays.Text = DateDiff("d", gvRentals.SelectedRow.Cells(14).Text, gvRentals.SelectedRow.Cells(15).Text) + 1
            Else
                txtRentalNoOfDays.Text = ""
            End If

            If gvRentals.SelectedRow.Cells(16).Text = "&nbsp;" Then gvRentals.SelectedRow.Cells(16).Text = ""
            If gvRentals.SelectedRow.Cells(17).Text = "&nbsp;" Then gvRentals.SelectedRow.Cells(17).Text = ""
            If gvRentals.SelectedRow.Cells(18).Text = "&nbsp;" Then gvRentals.SelectedRow.Cells(18).Text = ""
            If gvRentals.SelectedRow.Cells(19).Text = "&nbsp;" Then gvRentals.SelectedRow.Cells(19).Text = ""
            If gvRentals.SelectedRow.Cells(20).Text = "&nbsp;" Then gvRentals.SelectedRow.Cells(20).Text = ""

            txtRentalAddress.Text = gvRentals.SelectedRow.Cells(16).Text
            txtRentalCity.Text = gvRentals.SelectedRow.Cells(17).Text
            txtRentalState.Text = gvRentals.SelectedRow.Cells(18).Text
            txtRentalZip.Text = gvRentals.SelectedRow.Cells(19).Text
            txtRentalPhone.Text = gvRentals.SelectedRow.Cells(20).Text

            'ddlRentalAgency.SelectedValue = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(1)
            'txtRentalResConf.Text = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(2)
            'ddlRentalRateType.SelectedValue = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(3)
            'ddlRentalStatus.SelectedValue = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(4)
            'ddlRentalVehicleClass.SelectedValue = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(6)
            'chkWarranty.Checked = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(7)
            'txtRentalRate.Text = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(8)
            'txtRentalTaxRate.Text = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(9)
            'txtRentalTaxedRate.Text = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(10)
            'txtRentalDollars.Text = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(11)
            'txtRentalAuthPickup.Text = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(13)
            'txtRentalStartDate.Text = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(14)
            'txtRentalEndDate.Text = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(15)
            'txtRentalNoOfDays.Text = DateDiff("d", DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(14), DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(15)) + 1
            'txtRentalAddress.Text = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(16)
            'txtRentalCity.Text = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(17)
            'txtRentalState.Text = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(18)
            'txtRentalZip.Text = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(19)
            ''txtRentalPhone.Text = DTRentalData(iSelectedRowIndex).Table.Rows(iSelectedRowIndex).Item(20)

            '-------------------------------
            ' Warranty
            '-------------------------------
            'If bWarranty Then
            If UCase(txtWarranty.Text) = "YES" Then
                chkWarranty.Enabled = True
            Else
                chkWarranty.Enabled = False
            End If

            '------------------------------
            ' If checkbox is checked, 
            ' change repair start/end
            ' to warranty start/end
            '------------------------------
            If chkWarranty.Checked Then
                If hidWarrantyStart.Value <> "" Then
                    txtRentalRepairStart.Text = Format(CDate(hidWarrantyStart.Value), "MM/dd/yyyy")
                Else
                    txtRentalRepairStart.Text = ""
                    hidWarrantyStart.Value = ""
                    txtRentalRepairCycle.Text = "0"
                End If

                If hidWarrantyEnd.Value <> "" Then
                    txtRentalRepairEnd.Text = Format(CDate(hidWarrantyEnd.Value), "MM/dd/yyyy")
                Else
                    txtRentalRepairEnd.Text = ""
                    hidWarrantyEnd.Value = ""
                    txtRentalRepairCycle.Text = "0"
                End If
            Else
                If gvRentals.SelectedRow.Cells(14).Text = "" Or gvRentals.SelectedRow.Cells(14).Text = "&nbsp;" Then
                    txtRentalRepairStart.Text = ""
                    txtRentalRepairCycle.Text = "0"
                Else
                    'txtRentalRepairStart.Text = Format(CDate(gvRentals.SelectedRow.Cells(14).Text), "MM/dd/yyyy")
                    txtRentalRepairStart.Text = Format(CDate(hidRepairStart.Value), "MM/dd/yyyy")
                End If

                If gvRentals.SelectedRow.Cells(15).Text = "" Or gvRentals.SelectedRow.Cells(15).Text = "&nbsp;" Then
                    txtRentalRepairEnd.Text = ""
                    txtRentalRepairCycle.Text = "0"
                Else
                    'txtRentalRepairEnd.Text = Format(CDate(gvRentals.SelectedRow.Cells(15).Text), "MM/dd/yyyy")
                    txtRentalRepairEnd.Text = Format(CDate(hidRepairEnd.Value), "MM/dd/yyyy")
                End If
            End If

            '-------------------------------
            ' Enable all textboxes
            '-------------------------------
            ShowHide(True)
            btnRentalSave.Visible = False
            btnRentalUpdate.Visible = True
            btnRentalAdd.Visible = True

            Page.ClientScript.RegisterStartupScript(Me.GetType, "reCalc", "reCalculateRentals();", True)

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-RentalCoverage", "gvRentals_SelectedIndexChanged()", "ReIndex fields - Ended: " & Date.Now, "", "")
            End If


        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("gvRentals_SelectedIndexChanged", "ERROR", "Update objects failed...", oExcept.Message, sError)
        Finally
        End Try
    End Sub

    Protected Sub gvRentals_RowCreated(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRentals.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Me.Page.ClientScript.GetPostBackEventReference(Me.gvRentals, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Sub ShowHide(ByVal bState As Boolean)
        '-------------------------------
        ' Enable all textboxes
        '-------------------------------
        ddlRentalAgency.Enabled = bState
        txtRentalResConf.Enabled = bState
        ddlRentalVehicleClass.Enabled = bState
        ddlRentalStatus.Enabled = bState
        ddlRentalRateType.Enabled = bState
        'chkWarranty.Enabled = bState
        txtRentalRate.Enabled = bState
        txtRentalAuthPickup.Enabled = bState
        txtRentalTaxRate.Enabled = bState
        txtRentalStartDate.Enabled = bState
        txtRentalEndDate.Enabled = bState
        txtRentalPhone.Enabled = bState
        txtRentalAddress.Enabled = bState
        txtRentalCity.Enabled = bState
        txtRentalState.Enabled = bState
        txtRentalZip.Enabled = bState
    End Sub

    Protected Sub btnRentalAdd_Click(sender As Object, e As EventArgs) Handles btnRentalAdd.Click
        '-------------------------------
        ' Clear all textboxes
        '-------------------------------
        ddlRentalAgency.SelectedValue = ""
        txtRentalResConf.Text = ""
        ddlRentalRateType.SelectedValue = ""
        txtRentalRate.Text = ""
        ddlRentalStatus.SelectedValue = ""
        txtRentalAuthPickup.Text = ""
        ddlRentalVehicleClass.SelectedValue = ""

        If bWarranty Then
            chkWarranty.Enabled = True
        Else
            chkWarranty.Enabled = False
        End If

        txtRentalTaxRate.Text = ""
        txtRentalTaxedRate.Text = ""
        txtRentalDollars.Text = ""
        'txtRentalStartDate.Text = ""
        'txtRentalEndDate.Text = ""
        txtRentalNoOfDays.Text = ""
        txtRentalPhone.Text = ""
        txtRentalAddress.Text = ""
        txtRentalCity.Text = ""
        txtRentalState.Text = ""
        txtRentalZip.Text = ""

        '-------------------------------
        ' Enable all textboxes
        '-------------------------------
        ShowHide(True)

        '-------------------------------
        ' Show Required Indicator
        '-------------------------------
        lblRentalAgency.Visible = True
        lblRentalVehicleClass.Visible = True
        lblRentalAgency.Visible = True
        lblRentalResConf.Visible = True
        lblRentalRateType.Visible = True
        lblRentalRate.Visible = True
        lblRentalStatus.Visible = True
        lblRentalAuthPickup.Visible = True
        lblRentalVehicleClass.Visible = True
        lblRentalTaxRate.Visible = True
        lblRentalTaxedRate.Visible = True
        lblRentalDollars.Visible = True
        lblRentalStartDate.Visible = True
        lblRentalEndDate.Visible = True
        lblRentalPhone.Visible = True
        lblRentalAddress.Visible = True
        lblRentalCity.Visible = True
        lblRentalState.Visible = True
        lblRentalZip.Visible = True

        btnRentalSave.Visible = True
        btnRentalSave.Enabled = False
        btnRentalUpdate.Visible = False

        btnRentalAdd.Visible = False
    End Sub

    Protected Sub btnRentalSave_Click(sender As Object, e As EventArgs) Handles btnRentalSave.Click
        UpdateRental("SAVE", "")
    End Sub

    Protected Sub btnRentalUpdate_Click(sender As Object, e As EventArgs) Handles btnRentalUpdate.Click
        UpdateRental("UPDATE", gvRentals.SelectedRow.Cells(0).Text)
    End Sub

    Protected Sub UpdateRental(ByVal sType As String, ByVal sRentalID As String)
        Dim bRequire As Boolean = False
        Dim sReturnData As String = ""

        If sRentalID = "" Then sRentalID = 0

        '-------------------------------
        ' Check Required Fields
        '-------------------------------
        If ddlRentalAgency.SelectedValue = "" Then
            lblRentalAgency.ForeColor = Drawing.Color.Red
            bRequire = True
        Else
            lblRentalAgency.ForeColor = Drawing.Color.Black
        End If
        If txtRentalResConf.Text = "" Then
            lblRentalResConf.ForeColor = Drawing.Color.Red
            bRequire = True
        Else
            lblRentalResConf.ForeColor = Drawing.Color.Black
        End If
        If ddlRentalRateType.SelectedValue = "" Then
            lblRentalRateType.ForeColor = Drawing.Color.Red
            bRequire = True
        Else
            lblRentalRateType.ForeColor = Drawing.Color.Black
        End If
        If txtRentalRate.Text = "" Then
            lblRentalRate.ForeColor = Drawing.Color.Red
            bRequire = True
        Else
            lblRentalRate.ForeColor = Drawing.Color.Black
        End If
        If ddlRentalStatus.SelectedValue = "" Then
            lblRentalStatus.ForeColor = Drawing.Color.Red
            bRequire = True
        Else
            lblRentalStatus.ForeColor = Drawing.Color.Black
        End If
        If txtRentalAuthPickup.Text = "" Then
            lblRentalAuthPickup.ForeColor = Drawing.Color.Red
            bRequire = True
        Else
            lblRentalAuthPickup.ForeColor = Drawing.Color.Black
        End If
        If ddlRentalVehicleClass.SelectedValue = "" Then
            lblRentalVehicleClass.ForeColor = Drawing.Color.Red
            bRequire = True
        Else
            lblRentalVehicleClass.ForeColor = Drawing.Color.Black
        End If
        If hidtxtRentalDollars.Value = "" Then
            lblRentalTaxedRate.ForeColor = Drawing.Color.Red
            bRequire = True
        Else
            lblRentalTaxedRate.ForeColor = Drawing.Color.Black
        End If
        If hidtxtRentalTaxedRate.Value = "" Then
            lblRentalTaxedRate.ForeColor = Drawing.Color.Red
            bRequire = True
        Else
            lblRentalTaxedRate.ForeColor = Drawing.Color.Black
        End If
        If txtRentalStartDate.Text = "" Then
            lblRentalStartDate.ForeColor = Drawing.Color.Red
            bRequire = True
        Else
            lblRentalStartDate.ForeColor = Drawing.Color.Black
        End If
        If txtRentalEndDate.Text = "" Then
            lblRentalEndDate.ForeColor = Drawing.Color.Red
            bRequire = True
        Else
            lblRentalEndDate.ForeColor = Drawing.Color.Black
        End If
        If txtRentalPhone.Text = "" Then
            lblRentalPhone.ForeColor = Drawing.Color.Red
            bRequire = True
        Else
            lblRentalPhone.ForeColor = Drawing.Color.Black
        End If

        'If txtRentalAddress.Text = "" Then
        '    lblRentalAddress.ForeColor = Drawing.Color.Red
        '    bRequire = True
        'Else
        '    lblRentalAddress.ForeColor = Drawing.Color.Black
        'End If
        'If txtRentalCity.Text = "" Then
        '    lblRentalCity.ForeColor = Drawing.Color.Red
        '    bRequire = True
        'Else
        '    lblRentalCity.ForeColor = Drawing.Color.Black
        'End If
        'If txtRentalState.Text = "" Then
        '    lblRentalState.ForeColor = Drawing.Color.Red
        '    bRequire = True
        'Else
        '    lblRentalState.ForeColor = Drawing.Color.Black
        'End If
        'If txtRentalZip.Text = "" Then
        '    lblRentalZip.ForeColor = Drawing.Color.Red
        '    bRequire = True
        'Else
        '    lblRentalZip.ForeColor = Drawing.Color.Black
        'End If

        If bRequire = True Then
            lblRentalAddSaved.ForeColor = Drawing.Color.Red
            lblRentalAddSaved.Text = "*** Required field need completed ***"
        End If

        '-------------------------------
        ' Call DB and Save the record
        '-------------------------------
        If bRequire = False Then
            ' --- Save
            Try
                '-------------------------------
                ' Database access
                '-------------------------------  
                Dim sStoredProcedure As String = "uspRentalMgtUpdDetails"
                Dim sParams As String = "@LynxID=" & sLynxID
                sParams += ", @ClaimAspectID=" & iClaimAspectID
                sParams += ", @InsuranceCompanyID=" & iInsuranceCompanyID
                sParams += ", @RentalAgency='" & ddlRentalAgency.SelectedValue & "'"
                sParams += ", @RentalAgencyAddress1='" & txtRentalAddress.Text & "'"
                sParams += ", @RentalAgencyCity='" & txtRentalCity.Text & "'"
                sParams += ", @RentalAgencyState='" & txtRentalState.Text & "'"
                sParams += ", @RentalAgencyZip='" & txtRentalZip.Text & "'"
                sParams += ", @RentalAgencyPhone='" & txtRentalPhone.Text & "'"
                sParams += ", @ResConfNumber='" & txtRentalResConf.Text & "'"
                sParams += ", @VehicleClass='" & ddlRentalVehicleClass.SelectedValue & "'"
                sParams += ", @RentalStatus='" & ddlRentalStatus.SelectedValue & "'"
                sParams += ", @WarrantyFlag=" & chkWarranty.Checked
                sParams += ", @RateTypeCD='" & ddlRentalRateType.SelectedValue & "'"
                sParams += ", @Rate=" & txtRentalRate.Text
                sParams += ", @TaxRate=" & txtRentalTaxRate.Text
                sParams += ", @TaxedRate=" & hidtxtRentalTaxedRate.Value
                sParams += ", @Rental=" & hidtxtRentalDollars.Value
                sParams += ", @RentalNoOfDays=" & hidtxtRentalNoOfDays.Value
                sParams += ", @AuthPickupDate='" & txtRentalAuthPickup.Text & "'"
                sParams += ", @StartDate='" & txtRentalStartDate.Text & "'"
                sParams += ", @EndDate='" & txtRentalEndDate.Text & "'"
                sParams += ", @SysLastUserID=" & sUserID
                sParams += ", @Type='" & sType & "'"
                sParams += ", @RentalID=" & sRentalID

                '-------------------------------
                ' Call WS and get data
                '-------------------------------
                sReturnData = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

                If sReturnData.Contains("ERROR") Then
                    lblRentalAddSaved.ForeColor = Drawing.Color.Red
                    lblRentalAddSaved.Text = "*** Error occured while saving... ***"

                    wsAPDFoundation.LogEvent("btnRentalSave", "ERROR", "Rental Add failed...", "Error occured.  Error in EventXML", sReturnData)
                Else
                    lblRentalAddSaved.ForeColor = Drawing.Color.Blue
                    lblRentalAddSaved.Text = "Saved..."
                    Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
                End If

            Catch oExcept As Exception
                '---------------------------------
                ' Error handler and notifications
                '---------------------------------
                Dim sError As String = ""
                Dim sBody As String = ""
                Dim FunctionName As New System.Diagnostics.StackFrame

                sError = String.Format("Error {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

                '-------------------------------
                ' Refresh the page
                '-------------------------------
                wsAPDFoundation.LogEvent("btnRentalSave", "ERROR", "Rental Add failed...", oExcept.Message, sError)
            Finally
            End Try
        End If
    End Sub

End Class