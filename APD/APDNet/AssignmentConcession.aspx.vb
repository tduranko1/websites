﻿Imports System.Xml
Imports System.Xml.Xsl
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Public Class AssignmentConcession
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    Dim oReturnXML As XmlElement = Nothing
    Dim strClaimAspectServiceChannelID As String = "", strClaimAspectServiceChannelConcessionID As String = "", strUserid As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim strClaimAspectID As String = "", strInsCompanyID As String = ""

        strClaimAspectServiceChannelID = Request.QueryString("ClaimAspectServiceChannelID")
        strClaimAspectServiceChannelConcessionID = Request.QueryString("ClaimAspectServiceChannelConcessionID")
        strUserid = Request.QueryString("UserID")
        strClaimAspectID = Request.QueryString("ClaimAspectID")
        strInsCompanyID = Request.QueryString("InsuranceCompanyID")

        If strClaimAspectServiceChannelConcessionID = "" Then
            strClaimAspectServiceChannelConcessionID = "null"
        End If

        GetConcessionRefData(strClaimAspectServiceChannelID)

        If Not IsPostBack Then
            Populatedropdown()
            GetReasontype()
            If strClaimAspectID <> "" And strInsCompanyID <> "" Then

                GetPrepopulatedata(strClaimAspectID, strInsCompanyID)
            End If
        End If

    End Sub

    Private Sub GetPrepopulatedata(strClaimAspectID As String, strInscCompanyID As String)
        Dim oReturnVehXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing

        '-------------------------------
        ' Database access
        '-------------------------------  
        Dim sStoredProcedure As String = "uspClaimVehicleGetDetailWSXML"
        Dim sParams As String = strClaimAspectID & "," & strInscCompanyID

        Try
            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnVehXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            oXMLNode = oReturnVehXML.SelectSingleNode("Vehicle/ClaimAspectServiceChannel/Concession[@ClaimAspectServiceChannelConcessionID='" + strClaimAspectServiceChannelConcessionID + "']") '(ClaimAspectServiceChannelConcessionID = "98"

            '    '--------------------------------
            '    ' Populate Data for edit screen
            '    '--------------------------------
            If Not IsNothing(oXMLNode) Then

                ddlType.ClearSelection()

                ddlType.SelectedIndex = ddlType.Items.IndexOf(ddlType.Items.FindByText(GetXMLNodeattrbuteValue(oXMLNode, "TypeDescription")))
                GetReasontype()
                ddlReason.SelectedValue = GetXMLNodeattrbuteValue(oXMLNode, "ConcessionReasonID")
                txtConcessionAmt.Text = GetXMLNodeattrbuteValue(oXMLNode, "Amount")
                txtComment.Text = GetXMLNodeattrbuteValue(oXMLNode, "Comments")
            End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("AssignmentConcession", "ERROR", "Vehicle data not found or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
            oReturnVehXML = Nothing
        End Try

    End Sub

    Protected Sub Populatedropdown()
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim iCounter As Integer

        '-------------------------------
        ' Load Reference data
        '-------------------------------
        oXMLNodeList = oReturnXML.SelectNodes("Reference")
        ddlType.Items.Add("")
        ddlType.Items.Item(0).Value = "0"

        iCounter = 1
        For Each oXMLNode In oXMLNodeList
            '--------------------------------
            ' Populate ShopRemarkTemplates Dropdown
            '--------------------------------
            If GetXMLNodeattrbuteValue(oXMLNode, "ListName") = "ConcessionType" Then
                ddlType.Items.Add(GetXMLNodeattrbuteValue(oXMLNode, "Description"))
                ddlType.Items.Item(iCounter).Value = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceCD")
                iCounter += 1
            End If
        Next

    End Sub

    Private Sub GetConcessionRefData(ByVal iClaimAspectServiceChannelID As Integer)

        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim sStoredProcedure As String = "uspConcessionGetRefDataWSXML"
        Dim sParams As String = iClaimAspectServiceChannelID

        Try
            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)
        Catch ex As Exception

        End Try
    End Sub

    Protected Function GetXMLNodeattrbuteValue(ByVal XMLnode As XmlNode, strAttributes As String) As String
        Dim strRetVal As String = ""
        If Not XMLnode Is Nothing Then
            If Not XMLnode.Attributes(strAttributes) Is Nothing Then
                strRetVal = XMLnode.Attributes(strAttributes).InnerText
            Else
                strRetVal = ""
            End If
        Else
            strRetVal = ""
        End If
        Return strRetVal
    End Function

    Protected Sub ddlType_SelectedIndexChanged(sender As Object, e As EventArgs)

        GetReasontype()

    End Sub

    Private Sub GetReasontype()
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim iCounter As Integer

        ddlReason.Items.Clear()
        '-------------------------------
        oXMLNodeList = oReturnXML.SelectNodes("Reference")
        ddlReason.Items.Add("")
        ddlReason.Items.Item(0).Value = "0"

        iCounter = 1
        For Each oXMLNode In oXMLNodeList

            '--------------------------------
            ' Populate ShopRemarkTemplates Dropdown
            '--------------------------------
            If GetXMLNodeattrbuteValue(oXMLNode, "ListName") = "ReasonType" And GetXMLNodeattrbuteValue(oXMLNode, "ConcessionReasonTypeCD") = ddlType.SelectedValue Then
                ddlReason.Items.Add(GetXMLNodeattrbuteValue(oXMLNode, "Description"))
                ddlReason.Items.Item(iCounter).Value = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceCD")
                iCounter += 1
            End If
        Next
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs)

        Dim sStoredProcedure As String = "", sParams As String = "", sReturnData As String = ""

        sStoredProcedure = "uspConcessionUpdDetail"

        sParams = "@ClaimAspectServiceChannelConcessionID = " & strClaimAspectServiceChannelConcessionID _
                   & " , @ClaimAspectServiceChannelID = " & strClaimAspectServiceChannelID _
                   & " , @ConcessionReasonID = " & ddlReason.SelectedValue _
                   & " , @ConcessionAmount = " & txtConcessionAmt.Text _
                   & " , @Comments = '" + txtComment.Text + "'" _
                   & " , @UserID = " & strUserid

        '-------------------------------
        ' Call WS and get data
        '-------------------------------
        sReturnData = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

        ClientScript.RegisterStartupScript(Me.GetType, "Closing", "javascript:doCancel('Save');", True)

    End Sub

End Class