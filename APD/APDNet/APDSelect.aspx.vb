﻿'--------------------------------------------------------------
' Program: APDSelect
' Author:  Thomas Duranko
' Date:    Aug 21, 2013
' Version: 1.0
'
' Description:  This site provides the APDSelect for APD replacing the 
'               existing COM based classic asp functions. 
'--------------------------------------------------------------
Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Imports System.Web.Caching
Imports System.Web.Caching.Cache

Public Class APDSelect
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '-------------------------------
        ' Globals
        '-------------------------------
        Dim sLynxID As String = ""
        Dim sUserID As String = ""
        Dim oSession As Object = ""
        Dim sAPDNetDebugging As String = ""
        Dim sValidatedUserID As String = ""
        Dim sParams As String = ""
        Dim dtStart As Date
        Dim APDDBUtils As APDDBUtilities = Nothing
        Dim XMLUserDetails As XmlElement = Nothing
        Dim uspXMLXDoc As New XmlDocument
        Dim XMLSessionNode As XmlNode = Nothing
        Dim sXslHash As New Hashtable
        Dim uspXMLRaw As XmlElement = Nothing

        '-------------------------------
        ' Session/Local Variables
        '-------------------------------
        Dim sStoredProcedure As String = "uspSessionGetUserDetailWSXML"
        Dim sXSLProcedure As String = "ApdSelect.xsl"

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        Dim sCSRID As String = ""

        Dim sSessionKey = Request.QueryString("SessionKey")
        Dim sWindowID = Request.QueryString("WindowID")
        Dim sASPParams = Request.QueryString("Params")

        'sSessionKey = "{42BF0DA7-FCBD-47EC-AB3B-6025C16275E0}"
        'sWindowID = 0

        Try
            APDDBUtils = New APDDBUtilities()

            '---------------------------------
            ' Make sure we got the session
            ' key from classic asp
            '---------------------------------
            If sSessionKey = "" Then
                Throw New SystemException("SessionValidationError: No session information received from classic ASP page.")
            Else
                '---------------------------------
                ' New Session Data
                '---------------------------------
                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "LynxID-" & sWindowID)
                sLynxID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "UserID-" & sWindowID)
                sUserID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "APDNetDebugging-" & sWindowID)
                sAPDNetDebugging = ""
                If UCase(oSession(0).SessionValue) = "TRUE" Or UCase(AppSettings("Debug")) = "TRUE" Then
                    sAPDNetDebugging = "TRUE"
                End If

                dtStart = Date.Now

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "APDSelect.aspx - Start: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", ASPParam = " & sASPParams)
                End If

                '---------------------------------
                ' Make sure that a valid basic
                ' authenticated user was logged in
                ' ** This is already handled in the 
                ' classic side **
                '---------------------------------
                'sCSRID = Request.ServerVariables("LOGON_USER")

                'Dim iStart As Integer
                'Dim iLen As Integer
                'iStart = sCSRID.IndexOf("\") + 1
                'iLen = sCSRID.Length

                'If sCSRID = "" Then
                '    Throw New SystemException("AccessPermissionValidationError: No valid logged in user exists.")
                'Else
                '    '---------------------------------
                '    ' Strip down the CSR ID
                '    '---------------------------------
                '    If sCSRID.Contains("\") Then
                '        sCSRID = sCSRID.Substring(iStart, iLen - iStart)
                '    End If
                'End If

                '---------------------------------
                ' Debug Data
                '---------------------------------
                'If UCase(sAPDNetDebugging) = "TRUE" Then
                '    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Access Permissions - CSRID Validation", "User: " & sCSRID & " received from the Server Vars LOGON_USER", "CSRID: " & sCSRID)
                'End If

                '---------------------------------
                ' Get the App Path
                '---------------------------------
                'Response.Write("===> " & Request.ServerVariables("APPL_PHYSICAL_PATH"))

                '------------------------------------
                ' If ASPParms, parse and add to hash
                '------------------------------------
                If sASPParams <> "" Then
                    Dim aParams As Array
                    aParams = Split(sASPParams, "|")
                    For Each sASPParam In aParams
                        Dim aValues As Array
                        aValues = Split(sASPParam, ":")
                        sXslHash.Add(aValues(0), aValues(1))
                    Next
                End If

                'Dim oHash As New Hashtable
                'For Each oHash In sXslHash
                '    Response.Write(oHash.Keys)
                '    Response.Write(" - ")
                '    Response.Write(oHash.Values)
                '    Response.Write("<br/>")
                'Next

                'Response.Write("Debugging...")
                'Dim oXslParams As Object = Nothing
                'For Each oXslParams In sXslHash
                '    Response.Write(oXslParams.key)
                '    Response.Write(" - ")
                '    Response.Write(oXslParams.value)
                '    Response.Write("<br/>")
                'Next
                'Response.End()

                '------------------------------------
                ' Check if this is a new session
                ' ** This is already handled in the 
                ' classic side **
                '------------------------------------
                'If sXslHash("NewSession") = "1" Or sLynxID = "" Then
                '    sWindowID = "0"
                '    'sSessionKey = Request.Cookies("APDSession").ToString
                'End If

                '---------------------------------
                ' Call the get user details and 
                ' pass it the csrID and make sure
                ' we get back a good userid
                '---------------------------------
                'XMLUserDetails = wsAPDFoundation.ExecuteSpAsXML("uspSessionGetUserDetailWSXML", sCSRID)

                'If XMLUserDetails.OuterXml = Nothing Then
                '    Throw New SystemException("AccessPermissionValidationError: No userid exists for CSR " & sCSRID)
                'Else
                '    uspXMLXDoc.LoadXml(XMLUserDetails.OuterXml)

                '    XMLSessionNode = uspXMLXDoc.SelectSingleNode("/Root/User/@UserID")
                '    If XMLSessionNode.InnerText = Nothing Then
                '        Throw New SystemException("AccessPermissionValidationError: No userid exists for CSR " & sCSRID)
                '    Else
                '        sValidatedUserID = XMLSessionNode.InnerText
                '    End If


                '    '---------------------------------
                '    ' Debug Data
                '    '---------------------------------
                '    If UCase(sAPDNetDebugging) = "TRUE" Then
                '        wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Access Permissions - CSRID to UserID Validation", "CSR: " & sCSRID & " returned the user: " & sCSRID & " from the database", XMLUserDetails.OuterXml)
                '    End If

                '    Response.Write(XMLUserDetails.OuterXml)
                '    'Response.Write(APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sParams, sXslHash))
                'End If

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls/Parameters." _
                                             , "Session controls for Window: " & sWindowID _
                                             & " and SessionKey: " & sSessionKey _
                                             , "Vars: StoredProc = " & sStoredProcedure _
                                             & ", XSLPage = " & sXSLProcedure _
                                             & ", UserID(Session) = " & sUserID _
                                             & ", LynxID(Session) = " & sLynxID _
                                             & ", ASPParms = " & sASPParams _
                                             )
                End If

                '---------------------------------
                ' XSL Transformation
                '---------------------------------
                sParams = LCase(sXslHash("CSRID"))

                '---------------------------------
                ' StoredProc XML Data
                '---------------------------------
                uspXMLRaw = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

                If uspXMLRaw.OuterXml = Nothing Then
                    Throw New SystemException("WSDatabaseAccessError: No data returned from the StoredPRoc: " & sStoredProcedure & " with Params: " & sParams)
                Else
                    uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)

                    If UCase(sAPDNetDebugging) = "TRUE" Then
                        sXslHash.Add("RunTime", Now.Subtract(dtStart).Milliseconds & " msec")
                    Else
                        sXslHash.Add("RunTime", Chr(149))
                    End If

                    Response.Write(APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sParams, sXslHash))
                End If

                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "APDSelect.aspx - Finished: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
                End If

                'Response.Write("sSessionKey = " & sSessionKey & "<br/>")
                'Response.Write("sWindowID = " & sWindowID & "<br/>")
                'Response.Write("sASPParams = " & sASPParams & "<br/>")
                'Response.Write("sLynxID = " & sLynxID & "<br/>")
                'Response.Write("sUserID = " & sUserID & "<br/>")
                'Response.Write("sAPDNetDebugging = " & sAPDNetDebugging & "<br/>")
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls/parameter not found or page pre-transformation failed.", "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID & ", ASPParam = " & sASPParams)

            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        Finally
            '-----------------------------------
            ' Clean-up the mess
            '-----------------------------------
            wsAPDFoundation.Dispose()
            APDDBUtils = Nothing
            oSession = Nothing
            sXslHash = Nothing
            uspXMLRaw = Nothing
            uspXMLXDoc = Nothing
            APDDBUtils = Nothing
            GC.Collect()
            GC.SuppressFinalize(Me)
        End Try

    End Sub

End Class