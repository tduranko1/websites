﻿'--------------------------------------------------------------
' Program: ClaimXP
' Author:  Thomas Duranko
' Date:    Aug 06, 2013
' Version: 1.0
'
' Description:  This site provides the ClaimXP for APD replacing the 
'               existing COM based classic asp functions. 
'--------------------------------------------------------------
Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Imports System.Web.Caching
Imports System.Web.Caching.Cache

Imports System.Data
Imports System.Drawing
Imports System.ComponentModel
Imports System.Net
Imports APDNet
Imports System.Globalization

Public Class ClaimXP
    Inherits System.Web.UI.Page

    Public sLynxID As String = ""
    Public sInscCompany As String = ""
    Public sClaimAspectServiceChannelID As String = ""
    Public sUserID As String = ""
    Public sClaimSysLastUpdatedDate As String = ""
    Public sCoverageSysLastUpdatedDate As String = ""
    Public sClaimCoverageID As String = ""
    Public sClientCoverageID As String = ""
    Public sClientCoverageTypeID As String = ""
    Public sSelectedClaimCoverageID As String = ""
    Public sSelectedClientCoverageTypeID As String = ""
    Public sCarrierRepUserID As String = ""
    Public sCurrentOfficeID As String = ""
    Public sCarrierRepSysLastUpdatedDate As String = ""
    Public sContactSysLastUpdatedDate As String = ""
    Public sVehicleClaimAspectID As String = ""
    Public sVehicleSysLastUpdatedDate As String
    Public sClaimServiceChannelCD As String = ""
    Public sParams As String = ""
    Public bPageReadOnly As Boolean = False
    Public bPageReadOnly_Vehicle As Boolean = False
    Public sClaimStatus As String = ""
    Public sClaimOpen As String = ""
    Public sDemoFlag As String = ""
    Public sInsuranceCompanyName As String = ""
    Public sClaimClaimAspectID As String = ""
    Public sAPDNetDebugging As String = ""
    Public iVehicleCount As Integer = 0

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    Dim sCRUD As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '-------------------------------
        ' Globals
        '-------------------------------
        Dim sErrorMessage As String = ""
        Dim oSession As Object
        Dim dtStart As Date
        Dim APDDBUtils As APDDBUtilities = Nothing


        '---------------------------------
        ' Turn debugging on or off
        '---------------------------------
        sAPDNetDebugging = ""
        If UCase(AppSettings("Debug")) = "TRUE" Then
            sAPDNetDebugging = "TRUE"
        End If

        ClientScript.GetPostBackEventReference(Me, String.Empty)

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        Dim sSessionKey = Request.QueryString("SessionKey")
        Dim sWindowID = Request.QueryString("WindowID")
        Dim sASPParams = Request.QueryString("Params")

        '---------------------------------
        ' Debug Data
        '---------------------------------
        If UCase(sAPDNetDebugging) = "TRUE" Then
            wsAPDFoundation.LogEvent("APDNET-ClaimXP", "STARTING", "ClaimXP Load - Start: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey & " Session Vars: " & sASPParams, "")
        End If

        hidWindowID.Value = sWindowID

        'sSessionKey = "{927413F5-F5B7-4D60-8C02-D4E9DA062EFA}"
        'sWindowID = 2

        Try
            If sSessionKey = "" Then
                Throw New SystemException("SessionValidationError: No session information received from classic ASP page.")
            Else
                '---------------------------------
                ' New Session Data
                '---------------------------------
                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "LynxID-" & sWindowID)
                sLynxID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "UserID-" & sWindowID)
                sUserID = oSession(0).SessionValue
                hidUserID.Value = sUserID

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "Claim_ClaimAspectID-" & sWindowID)
                sClaimClaimAspectID = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "InsuranceCompanyID-" & sWindowID)
                sInscCompany = oSession(0).SessionValue
                hidInsuranceCompanyID.Value = sInscCompany

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "APDNetDebugging-" & sWindowID)
                sAPDNetDebugging = ""
                If UCase(oSession(0).SessionValue) = "TRUE" Or UCase(AppSettings("Debug")) = "TRUE" Then
                    sAPDNetDebugging = "TRUE"
                End If

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "ClaimStatus-" & sWindowID)
                sClaimStatus = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "ClaimOpen-" & sWindowID)
                sClaimOpen = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "DemoFlag-" & sWindowID)
                sDemoFlag = oSession(0).SessionValue

                oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "InsuranceCompanyName-" & sWindowID)
                sInsuranceCompanyName = oSession(0).SessionValue

            End If

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "DEBUGGING", "ClaimXP Load - Session: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Session Vars: LynxID:" & sLynxID & ", UserID:" & sUserID & ", ClaimClaimAspectID:" & sClaimClaimAspectID & ", InscCompany:" & sInscCompany & ", ClaimStatus:" & sClaimStatus & ", ClaimOpen:" & sClaimOpen & ", DemoFlag:" & sDemoFlag & ", InsuranceCompanyName:" & sInsuranceCompanyName)
            End If

            APDDBUtils = New APDDBUtilities()
            dtStart = Date.Now

            '        ---------------------------------
            '         Debug Data
            '        ---------------------------------
            '        If UCase(sAPDNetDebugging) = "TRUE" Then
            '            wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "ClaimXP.aspx - Start: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", ASPParam = " & sASPParams)
            '        End If

            '        ---------------------------------
            '         Get configuration data
            '        ---------------------------------
            '        wsAPDFoundation.InitializeEx()
            '        oConfigXML = wsAPDFoundation.GetConfigXMLData("Document/RootDirectory")
            '        If oConfigXML.InnerXml.Contains("ERROR:") Then
            '            Throw New SystemException("ConfigXMLError: Could not find the Document/RootDirectory configuration variable.")
            '        Else
            '            sImageRootDir = oConfigXML.InnerText
            '        End If

            '---------------------------------
            ' Get the session data
            '---------------------------------
            If Not Request("UserID") Then
                'sUserID = APDDBUtils.UserID
            Else
                sUserID = Request("UserID")
            End If

            If Not IsPostBack Then
                hidCoverageSelected.Value = "0"
            End If


        Catch oExcept As Exception
            '---------------------------------
            ' Error handler And notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls/parameter not found or page pre-transformation failed.", "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID & ", ClaimClaimAspectID(Session) = " & sClaimClaimAspectID & ",  InscCompany(Session) = " & sInscCompany & ", ClaimStatus(Session) = " & sClaimStatus & ", ClaimOpen(Session) = " & sClaimOpen & ", InsuranceCompanyName(Session) = " & sInsuranceCompanyName & ",      ASPParam = " & sASPParams)

            'Response.Write(oExcept.Message)
            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
            'Finally
            '    -----------------------------------
            '     Clean-up the mess
            '    -----------------------------------
            '    wsAPDFoundation.Dispose()
            '    APDDBUtils = Nothing
            '    sASPParam = Nothing
            '    oSession = Nothing
            '    sXslHash = Nothing
            '    XMLCRUD = Nothing
            '    uspXMLRaw = Nothing
            '    uspXMLXDoc = Nothing
            '    APDDBUtils = Nothing
            '    GC.Collect()
            '    GC.SuppressFinalize(Me)
        End Try

        '-----------------------------------------------
        ' New GetClaimData
        '-----------------------------------------------
        If Not IsPostBack Then
            sErrorMessage = GetClaimData(CInt(sLynxID), CInt(sInscCompany))
        End If

        '-----------------------------------------------
        ' New GetVehicleData
        '-----------------------------------------------
        '??? TEMP ClaimAspect ???
        ' sErrorMessage = GetVehicleData(CInt(sClaimClaimAspectID + 1), CInt(sInscCompany))


    End Sub

    Protected Function GetPhoneData(ByVal SPhonenumber As String, sType As String) As String
        Dim sPData As String = ""
        Try
            If SPhonenumber <> "" And SPhonenumber.Length >= 13 Then
                Select Case sType
                    Case "AreaCode"
                        sPData = SPhonenumber.Substring(1, 3)
                    Case "ExchangeNumber"
                        sPData = SPhonenumber.Substring(5, 3)
                    Case "UnitNumber"
                        sPData = SPhonenumber.Substring(9, 4)
                    Case "ExtensionNumber"
                        sPData = ""
                End Select
            End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDClaim", "ERROR", "Claim data not found or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        End Try

        Return sPData

    End Function

    Protected Function GetClaimData(ByVal iLynxID As Integer, iInsuranceCompanyID As Integer) As String
        '-------------------------------
        ' Session/Local Variables
        '-------------------------------  
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing

        Dim oXMLvehNodeList As XmlNodeList = Nothing
        Dim oXMLvehNode As XmlNode = Nothing

        Dim lstVeh As Vehicles = Nothing

        Dim iStateID As Integer = 0
        Dim iContactPhoneID As Integer = 0
        Dim iClientCoverageType As Integer = 0
        Dim sCRUD As String = ""
        Dim oCRUDXML As XmlElement = Nothing
        Dim sClaimCRUD As String = ""
        Dim sCarrierCRUD As String = ""
        Dim sInsuredCRUD As String = ""
        Dim sCoverageCRUD As String = ""
        Dim sDocumentCRUD As String = ""
        Dim sCoverageFlag As String = ""

        Dim sContactPhoneCD As String = ""
        Dim sSelectedLossState As String = ""
        Dim sDDLParams As String = ""
        Dim sVehicleDisplayname As String = ""
        Dim sVehicleList As String = ""
        Dim sVehOwner As String = ""

        '-------------------------------
        ' Database access
        '-------------------------------  
        Dim sStoredProcedure As String = "uspClaimCondGetDetailWSXML"
        Dim sParams As String = iLynxID & "," & iInsuranceCompanyID

        Try
            '-------------------------------
            ' Get Session Data
            '-------------------------------
            ' *** ADD THIS CODE ***
            'objExe.InitializeEx Request.ServerVariables("APPL_PHYSICAL_PATH"), sSessionKey, sWindowID, strUserID
            ' Place claim information in session manager.
            'StuffClaimSessionVars objExe.ExecuteSpAsXML("uspSessionGetClaimDetailXML", "", strLynxID), sSessionKey            
            'sSessionData = GetSessionData("", strLynxID)

            '-------------------------------
            ' Build ALL CRUD 
            '-------------------------------
            oCRUDXML = wsAPDFoundation.GetCRUD("Claim", sUserID)
            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            sClaimCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")

            oCRUDXML = wsAPDFoundation.GetCRUD("Data:Carrier", sUserID)
            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            sCarrierCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")

            oCRUDXML = wsAPDFoundation.GetCRUD("Insured", sUserID)
            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            sInsuredCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")

            oCRUDXML = wsAPDFoundation.GetCRUD("Coverage", sUserID)
            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            sClaimCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")
            hidClaimCRUD.Value = SetCRUD(sClaimCRUD)

            oCRUDXML = wsAPDFoundation.GetCRUD("Document", sUserID)
            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            sDocumentCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")
            hidDocumentCRUD.Value = SetCRUD(sDocumentCRUD)


            '-------------------------------
            ' Build ALL CRUD For Parent tab updation -- Feb 04
            '-------------------------------
            oCRUDXML = wsAPDFoundation.GetCRUD("Action:Reopen Exposure", sUserID)
            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            sDocumentCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")
            hidReOpenExpCRUD.Value = SetCRUD(sDocumentCRUD)

            oCRUDXML = wsAPDFoundation.GetCRUD("Action:Close Exposure", sUserID)
            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            sDocumentCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")
            hidCloseExpCRUD.Value = SetCRUD(sDocumentCRUD)

            oCRUDXML = wsAPDFoundation.GetCRUD("Transfer Claim Exposure", sUserID)
            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            sDocumentCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")
            hidTransferClaimExpCRUD.Value = SetCRUD(sDocumentCRUD)

            oCRUDXML = wsAPDFoundation.GetCRUD("Reassign Exposure", sUserID)
            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            sDocumentCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")
            hidReassignExpCRUD.Value = SetCRUD(sDocumentCRUD)

            oCRUDXML = wsAPDFoundation.GetCRUD("Cancel Exposure", sUserID)
            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            sDocumentCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")
            hidCancelExpCRUD.Value = SetCRUD(sDocumentCRUD)

            oCRUDXML = wsAPDFoundation.GetCRUD("Void Exposure", sUserID)
            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            sDocumentCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")
            hidVoidExpCRUD.Value = SetCRUD(sDocumentCRUD)

            oCRUDXML = wsAPDFoundation.GetCRUD("Service Channel", sUserID)
            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            sDocumentCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")
            hidServiceChannelCRUD.Value = SetCRUD(sDocumentCRUD)


            '----------------------------------
            ' CRUD Permissions - Claim Section
            '----------------------------------
            Select Case sClaimCRUD
                Case "0000"
                    txtPolicyNumber.Enabled = False
                    txtClientClaimNumber.Enabled = False
                    txtLossDate.Enabled = False
                    ddlState.Enabled = False
                    txtLossDescription.Enabled = False
                    txtSubmittedBy.Enabled = False
                    txtIntakeFinishDate.Enabled = False
                    txtRemarks.Enabled = False
                Case "1111"
                    txtPolicyNumber.Enabled = True
                    txtClientClaimNumber.Enabled = True
                    txtLossDate.Enabled = False
                    ddlState.Enabled = True
                    txtLossDescription.Enabled = True
                    txtSubmittedBy.Enabled = False
                    txtIntakeFinishDate.Enabled = False
                    txtRemarks.Enabled = True
                Case Else
                    txtPolicyNumber.Enabled = False
                    txtClientClaimNumber.Enabled = False
                    txtLossDate.Enabled = False
                    ddlState.Enabled = False
                    txtLossDescription.Enabled = False
                    txtSubmittedBy.Enabled = False
                    txtIntakeFinishDate.Enabled = False
                    txtRemarks.Enabled = False
            End Select

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '---------------------------------------
            ' Load Root level claim data 
            '---------------------------------------
            '--- Claim level
            oXMLNode = oReturnXML.SelectSingleNode("Claim")
            sClaimSysLastUpdatedDate = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate")
            hidClaimSysLastUpdatedDate.Value = sClaimSysLastUpdatedDate
            hidLynxID.Value = GetXMLNodeattrbuteValue(oXMLNode, "LynxID")
            hidClaimID.Value = GetXMLNodeattrbuteValue(oXMLNode, "ClientClaimNumber")
            hidClaimIDSquished.Value = GetXMLNodeattrbuteValue(oXMLNode, "ClientClaimNumberSquished")
            hidLoss.Value = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "LossDate")), "MM/dd/yyyy")
            hidInsCo.Value = sInsuranceCompanyName
            hidRestrictedFlag.Value = GetXMLNodeattrbuteValue(oXMLNode, "RestrictedFlag")
            hidClaim_ClaimAspectID.Value = sClaimClaimAspectID
            sSelectedLossState = GetXMLNodeattrbuteValue(oXMLNode, "LossState")

            '--- Claim/Carrier level
            oXMLNode = oReturnXML.SelectSingleNode("Claim/Carrier")
            hidRepFname.Value = GetXMLNodeattrbuteValue(oXMLNode, "NameFirst")
            hidRepLname.Value = GetXMLNodeattrbuteValue(oXMLNode, "NameLast")
            hidRepActiveFlag.Value = GetXMLNodeattrbuteValue(oXMLNode, "ActiveFlag")

            '--- Claim/Insured level
            oXMLNode = oReturnXML.SelectSingleNode("Claim/Insured")
            hidInsFname.Value = GetXMLNodeattrbuteValue(oXMLNode, "NameFirst")
            hidInsLname.Value = GetXMLNodeattrbuteValue(oXMLNode, "NameLast")
            hidInsInvolvedID.Value = GetXMLNodeattrbuteValue(oXMLNode, "InvolvedID")
            hidInsFedTaxID.Value = GetXMLNodeattrbuteValue(oXMLNode, "FedTaxID")
            hidInsSysLastUpdatedDate.Value = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate")

            '--- Claim/Coverage level
            oXMLNode = oReturnXML.SelectSingleNode("Claim/Coverage")
            sClaimCoverageID = GetXMLNodeattrbuteValue(oXMLNode, "ClaimCoverageID")
            sClientCoverageTypeID = GetXMLNodeattrbuteValue(oXMLNode, "ClientCoverageTypeID")
            sCoverageSysLastUpdatedDate = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate")

            '--- Claim Open/Status/Demo
            hidClaimOpen.Value = sClaimOpen
            hidClaimStatus.Value = sClaimStatus
            hidDemoFlag.Value = sDemoFlag

            '---------------------------------------
            ' Load Root level claim data - Coverage
            '---------------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("Claim/Coverage")

            Dim DTCoverage As New DataTable
            Dim DCCoverage As New DataColumn

            DTCoverage.Columns.Add(New DataColumn("ClaimCoverageID", GetType(String)))
            DTCoverage.Columns.Add(New DataColumn("ClientCoverageTypeID", GetType(String)))
            DTCoverage.Columns.Add(New DataColumn("CoverageSysLastUpdatedDate", GetType(String)))
            DTCoverage.Columns.Add(New DataColumn("Description", GetType(String)))
            DTCoverage.Columns.Add(New DataColumn("CoverageTypeCD", GetType(String)))
            DTCoverage.Columns.Add(New DataColumn("AddtlCoverageFlag", GetType(String)))
            DTCoverage.Columns.Add(New DataColumn("DeductibleAmt", GetType(String)))
            DTCoverage.Columns.Add(New DataColumn("LimitAmt", GetType(String)))
            DTCoverage.Columns.Add(New DataColumn("MaximumDays", GetType(String)))
            DTCoverage.Columns.Add(New DataColumn("LimitDailyAmt", GetType(String)))

            For Each oXMLNode In oXMLNodeList
                Dim DRCoverage As DataRow = DTCoverage.NewRow
                ' Hidden
                DRCoverage("ClaimCoverageID") = GetXMLNodeattrbuteValue(oXMLNode, "ClaimCoverageID")
                DRCoverage("ClientCoverageTypeID") = GetXMLNodeattrbuteValue(oXMLNode, "ClientCoverageTypeID")
                DRCoverage("CoverageSysLastUpdatedDate") = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate")
                ' Viewable
                'DRCoverage("Description") = oXMLNode.Attributes("CoverageTypeCD").InnerText & " - " & oXMLNode.Attributes("Description").InnerText
                DRCoverage("Description") = GetXMLNodeattrbuteValue(oXMLNode, "Description")
                DRCoverage("CoverageTypeCD") = GetXMLNodeattrbuteValue(oXMLNode, "CoverageTypeCD")
                'DRCoverage("AddtlCoverageFlag") = GetXMLNodeattrbuteValue(oXMLNode, "AddtlCoverageFlag")

                sCoverageFlag = GetXMLNodeattrbuteValue(oXMLNode, "AddtlCoverageFlag")
                Select Case sCoverageFlag
                    Case "1"
                        DRCoverage("AddtlCoverageFlag") = ResolveUrl("images/tick.gif") '"images/tick.gif"
                    Case Else
                        DRCoverage("AddtlCoverageFlag") = ""
                End Select

                'DRCoverage("AddtlCoverageFlag") = sCoverageFlag

                DRCoverage("DeductibleAmt") = GetXMLNodeattrbuteValue(oXMLNode, "DeductibleAmt")
                DRCoverage("LimitAmt") = GetXMLNodeattrbuteValue(oXMLNode, "LimitAmt")
                DRCoverage("MaximumDays") = GetXMLNodeattrbuteValue(oXMLNode, "MaximumDays")
                DRCoverage("LimitDailyAmt") = GetXMLNodeattrbuteValue(oXMLNode, "LimitDailyAmt")

                DTCoverage.Rows.Add(DRCoverage)
            Next

            '---------------------------------------
            ' Coverage GridView
            '---------------------------------------
            gvCoverage.DataSource = DTCoverage
            gvCoverage.DataBind()

            gvCoverage.Columns(0).Visible = False
            gvCoverage.Columns(1).Visible = False
            gvCoverage.Columns(2).Visible = False
            gvCoverage.Visible = True

            '---------------------------------------
            ' Load Root level claim data - Carrier
            '---------------------------------------
            oXMLNode = oReturnXML.SelectSingleNode("Claim/Carrier")

            Dim oCarrierRep As New CarrierRep
            oCarrierRep.OfficeID = CInt(GetXMLNodeattrbuteValue(oXMLNode, "OfficeID"))
            oCarrierRep.UserID = CInt(GetXMLNodeattrbuteValue(oXMLNode, "UserID"))
            oCarrierRep.EmailAddress = GetXMLNodeattrbuteValue(oXMLNode, "EmailAddress")
            oCarrierRep.PhoneAreaCode = GetXMLNodeattrbuteValue(oXMLNode, "PhoneAreaCode")
            oCarrierRep.PhoneExchangeNumber = GetXMLNodeattrbuteValue(oXMLNode, "PhoneExchangeNumber")
            oCarrierRep.PhoneExtensionNumber = GetXMLNodeattrbuteValue(oXMLNode, "PhoneExtensionNumber")
            oCarrierRep.PhoneUnitNumber = GetXMLNodeattrbuteValue(oXMLNode, "PhoneUnitNumber")
            oCarrierRep.FaxAreaCode = GetXMLNodeattrbuteValue(oXMLNode, "FaxAreaCode")
            oCarrierRep.FaxExchangeNumber = GetXMLNodeattrbuteValue(oXMLNode, "FaxExchangeNumber")
            oCarrierRep.FaxExtensionNumber = GetXMLNodeattrbuteValue(oXMLNode, "FaxExtensionNumber")
            oCarrierRep.FaxUnitNumber = GetXMLNodeattrbuteValue(oXMLNode, "FaxUnitNumber")
            oCarrierRep.Title = GetXMLNodeattrbuteValue(oXMLNode, "NameTitle")
            oCarrierRep.NameFirst = GetXMLNodeattrbuteValue(oXMLNode, "NameFirst")
            oCarrierRep.NameLast = GetXMLNodeattrbuteValue(oXMLNode, "NameLast")
            oCarrierRep.OfficeName = GetXMLNodeattrbuteValue(oXMLNode, "OfficeName")
            oCarrierRep.SysLastUpdatedDate = GetXMLNodeattrbuteValue(oXMLNode, "OfficeName")

            sCurrentOfficeID = oCarrierRep.OfficeID
            sCarrierRepUserID = oCarrierRep.UserID
            sCarrierRepSysLastUpdatedDate = sClaimSysLastUpdatedDate 'oCarrierRep.SysLastUpdatedDate

            txtCarrierNameTitle.Text = oCarrierRep.Title
            txtCarrierNameFirst.Text = oCarrierRep.NameFirst
            txtCarrierNameLast.Text = oCarrierRep.NameLast
            txtCarrierOfficeName.Text = oCarrierRep.OfficeName
            txtCarrierEmailAddress.Text = oCarrierRep.EmailAddress
            txtCarrierPhone.Text = "(" & oCarrierRep.PhoneAreaCode & ") " & oCarrierRep.PhoneExchangeNumber & "-" & oCarrierRep.PhoneUnitNumber & " X" & oCarrierRep.PhoneExtensionNumber
            txtCarrierFax.Text = "(" & oCarrierRep.FaxAreaCode & ") " & oCarrierRep.FaxExchangeNumber & "-" & oCarrierRep.FaxUnitNumber

            '------------------------------------
            ' Load Root level claim data - Claim
            '------------------------------------
            Dim veh As Vehicle
            oXMLvehNodeList = oReturnXML.SelectNodes("Vehicle")

            'Vehicle count for checking the validation for Choice shop assignment 
            iVehicleCount = oXMLvehNodeList.Count
            hidVehCount.Value = iVehicleCount

            lstVeh = New Vehicles()
            lstVeh.vehicleCollection = New List(Of Vehicle)

            For Each oXMLvehNode In oXMLvehNodeList
                veh = New Vehicle()
                veh.Vehnumber = Convert.ToInt32(oXMLvehNode.Attributes("VehicleNumber").InnerText)
                veh.Businessname = oXMLvehNode.Attributes("BusinessName").InnerText
                veh.ClaimAspectID = Convert.ToUInt64(oXMLvehNode.Attributes("ClaimAspectID").InnerText)
                'veh.SysLastUpdatedDate = Convert.ToUInt64(oXMLvehNode.Attributes("SysLastUpdatedDate").InnerText)

                '------------------------
                ' Build Vehicle List
                '------------------------
                If oXMLvehNode.Attributes("BusinessName").InnerText <> "" Then
                    sVehOwner = oXMLvehNode.Attributes("BusinessName").InnerText
                ElseIf oXMLvehNode.Attributes("NameFirst").InnerText <> "" Then
                    sVehOwner = (oXMLvehNode.Attributes("NameFirst").InnerText).Substring(1, 1) & ". " & oXMLvehNode.Attributes("NameLast").InnerText
                Else
                    sVehOwner = oXMLvehNode.Attributes("NameLast").InnerText
                End If
                sVehicleList += oXMLvehNode.Attributes("VehicleNumber").InnerText & "|" & oXMLvehNode.Attributes("VehicleNumber").InnerText & ": " & sVehOwner

                ''Constructed Vehicle tabs name displayed 
                sVehicleDisplayname = oXMLvehNode.Attributes("VehicleNumber").InnerText + ": "

                If (oXMLvehNode.Attributes("NameFirst").InnerText <> "") Then 'NameFirst
                    sVehicleDisplayname = sVehicleDisplayname + oXMLvehNode.Attributes("NameFirst").InnerText.Substring(0, 1)
                End If
                sVehicleDisplayname = sVehicleDisplayname + ". "

                sVehicleDisplayname = sVehicleDisplayname + oXMLvehNode.Attributes("NameLast").InnerText

                veh.VehDisplayName = sVehicleDisplayname

                GetVehicleData(veh.ClaimAspectID, veh)

                lstVeh.vehicleCollection.Add(veh)
            Next
            hidVehicleList.Value = sVehicleList

            RepeaterVehTab.DataSource = lstVeh.vehicleCollection
            RepeaterVehTab.DataBind()

            RepeaterVehdetTab.DataSource = lstVeh.vehicleCollection
            RepeaterVehdetTab.DataBind()

            oXMLNode = oReturnXML.SelectSingleNode("Claim")
            txtPolicyNumber.Text = GetXMLNodeattrbuteValue(oXMLNode, "PolicyNumber")
            txtClientClaimNumber.Text = GetXMLNodeattrbuteValue(oXMLNode, "ClientClaimNumber")
            txtLossDate.Text = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "LossDate")), "MM/dd/yyyy hh:mm:ss")
            ddlState.SelectedValue = GetXMLNodeattrbuteValue(oXMLNode, "LossState")
            txtLossDescription.Text = GetXMLNodeattrbuteValue(oXMLNode, "LossDescription")
            txtIntakeFinishDate.Text = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "IntakeFinishDate")), "MM/dd/yyyy hh:mm:ss")
            txtSubmittedBy.Text = GetXMLNodeattrbuteValue(oXMLNode, "IntakeUserFirstName") & " " & GetXMLNodeattrbuteValue(oXMLNode, "IntakeUserLastName") & " (" & GetXMLNodeattrbuteValue(oXMLNode, "IntakeUserCompany") & ")"
            txtRemarks.Text = GetXMLNodeattrbuteValue(oXMLNode, "Remarks")

            '-------------------------------
            ' Load Reference data
            '-------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("Reference")
            iStateID = 0
            iClientCoverageType = 0

            ''Add empty data for dropdownlsit for claim and insured section 
            ddlState.Items.Add("")
            ddlState.Items.Item(iStateID).Value = "0"
            ddlAddressState.Items.Add("")
            ddlAddressState.Items.Item(iStateID).Value = "0"

            For Each oXMLNode In oXMLNodeList
                '--------------------------------
                ' Populate State Dropdown
                '--------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "State" Then
                    iStateID += 1
                    ddlState.Items.Add(GetXMLNodeattrbuteValue(oXMLNode, "Name"))
                    ddlState.Items.Item(iStateID).Value = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")
                    ddlAddressState.Items.Add(GetXMLNodeattrbuteValue(oXMLNode, "Name"))
                    ddlAddressState.Items.Item(iStateID).Value = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")
                End If

                '--------------------------------
                ' Populate Contact Phone
                '--------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "BestContactPhone" Then
                    ddlBestContactPhoneCD.Items.Add(GetXMLNodeattrbuteValue(oXMLNode, "Name"))
                    ddlBestContactPhoneCD.Items.Item(iContactPhoneID).Value = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")
                    iContactPhoneID += 1
                End If

                '--------------------------------
                ' Populate Coverage Add/Edit
                ' Description Dropdown
                '--------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "ClientCoverageType" Then
                    If iClientCoverageType = 0 Then
                        ddlCoverageDescription.Items.Add("")
                        ddlCoverageDescription.Items.Item(iClientCoverageType).Value = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")
                        iClientCoverageType += 1
                    End If

                    If DTCoverage.Select("Description='" & oXMLNode.Attributes("ReferenceID").Value & " - " & oXMLNode.Attributes("Name").Value & "'").Length = 0 Then
                        sDDLParams = oXMLNode.Attributes("ReferenceID").Value & "|" & oXMLNode.Attributes("Name").Value & "|" & oXMLNode.Attributes("ClientCoverageTypeID").Value & "|" & oXMLNode.Attributes("AddtlCoverageFlag").Value
                        ddlCoverageDescription.Items.Insert(iClientCoverageType, New ListItem(oXMLNode.Attributes("ReferenceID").Value & " - " & GetXMLNodeattrbuteValue(oXMLNode, "Name"), sDDLParams))
                        iClientCoverageType += 1
                    End If
                End If
            Next

            ddlState.SelectedValue = sSelectedLossState

            '---------------------------------------
            ' Load Root level claim data - Insured
            '---------------------------------------
            oXMLNode = oReturnXML.SelectSingleNode("Claim/Insured")
            txtNameTitle.Text = GetXMLNodeattrbuteValue(oXMLNode, "NameTitle")
            txtNameFirst.Text = GetXMLNodeattrbuteValue(oXMLNode, "NameFirst")
            txtNameLast.Text = GetXMLNodeattrbuteValue(oXMLNode, "NameLast")
            txtBusinessName.Text = GetXMLNodeattrbuteValue(oXMLNode, "BusinessName")
            txtAddress1.Text = GetXMLNodeattrbuteValue(oXMLNode, "Address1")
            txtAddress2.Text = GetXMLNodeattrbuteValue(oXMLNode, "Address2")
            txtAddressCity.Text = GetXMLNodeattrbuteValue(oXMLNode, "AddressCity")
            ddlAddressState.SelectedValue = GetXMLNodeattrbuteValue(oXMLNode, "AddressState")
            txtAddressZip.Text = GetXMLNodeattrbuteValue(oXMLNode, "AddressZip")
            txtEmailAddress.Text = GetXMLNodeattrbuteValue(oXMLNode, "EmailAddress")
            txtFedTaxID.Text = GetXMLNodeattrbuteValue(oXMLNode, "FedTaxId")
            ddlBestContactPhoneCD.SelectedValue = GetXMLNodeattrbuteValue(oXMLNode, "BestContactPhoneCD")
            txtBestContactTime.Text = GetXMLNodeattrbuteValue(oXMLNode, "BestContactTime")
            txtDay.Text = "( " & GetXMLNodeattrbuteValue(oXMLNode, "DayAreaCode") & " ) " & GetXMLNodeattrbuteValue(oXMLNode, "DayExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "DayUnitNumber")
            txtNight.Text = "( " & GetXMLNodeattrbuteValue(oXMLNode, "NightAreaCode") & " ) " & GetXMLNodeattrbuteValue(oXMLNode, "NightExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "NightUnitNumber")
            txtAlternate.Text = "( " & GetXMLNodeattrbuteValue(oXMLNode, "AlternateAreaCode") & " ) " & GetXMLNodeattrbuteValue(oXMLNode, "AlternateExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "AlternateUnitNumber")

            'ddlCoverageDescription.SelectedIndex = 0

            '-------------------------------
            ' Initialize Add Dialog 
            '-------------------------------
            ddlCoverageDescription.SelectedIndex = 0
            txtCoverageMaxDays.Text = 0
            txtCoverageDailyLimit.Text = 0

            '-- ReOrder State
            'Dim liStates As List(Of String) = ddlState.Items.Cast(Of ListItem)().Select(Function(item) item.Text).ToList()
            'liStates.Sort(Function(a, b) String.Compare(a, b))
            'ddlState.DataSource = liStates
            'ddlState.DataBind()

            '-- ReOrder Coverage Description
            'Dim liCoverageDescription As List(Of String) = ddlCoverageDescription.Items.Cast(Of ListItem)().Select(Function(item) item.Text).ToList()
            'liCoverageDescription.Sort(Function(a, b) String.Compare(a, b))
            'ddlCoverageDescription.DataSource = liCoverageDescription
            'ddlCoverageDescription.DataBind()

            txtCoverageDeductible.Text = 0
            txtCoverageLimit.Text = 0

            Return ""
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDClaim", "ERROR", "Claim data not found or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
            Return ""
        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
            oXMLNodeList = Nothing
        End Try
    End Function

    Protected Function GetVehicleData(ByVal iClaimAspectID As Integer, veh As Vehicle) As Vehicle
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim iStateID As Integer = 0
        Dim sInitialAssignmentTypeID As String = ""
        Dim hAssignmentType As New Hashtable()
        Dim hCoverageProfile As New Hashtable()
        Dim hImpact As New Hashtable()
        Dim sErrorMessage As String = ""
        Dim oCRUDXML As XmlElement = Nothing
        Dim sVehicleCRUD As String = ""
        Dim dtReferences As DataTable = New DataTable
        Dim drReferences As DataRow = Nothing
        sVehicleClaimAspectID = iClaimAspectID
        Dim strImpact As String = ""
        Dim strPriorImpact As String = ""

        'Dim sClaimAspectServiceChannelID As String = ""

        '-------------------------------
        ' Session/Local Variables
        '-------------------------------  
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLContactNode As XmlNode = Nothing
        Dim objReferenceState As ReferenceState = Nothing
        Dim objReferenceExposure As ReferenceExposure = Nothing
        Dim objReferenceImpactPoints As ReferenceImpactPoints = Nothing
        Dim objReferenceInsuredRelation As ReferenceInsuredRelation = Nothing
        Dim objReferenceContactTabPhone As ReferenceContactTabPhone = Nothing
        'Dim objReferenceCommunicationMethod As ReferenceCommunicationMethod = Nothing

        '-----------------------------------------
        'Vehicle Impact collection 
        '-----------------------------------------
        Dim oXMLImpactNodeList As XmlNodeList = Nothing
        Dim oXMLImpactNode As XmlNode = Nothing

        '-------------------------------
        ' Database access
        '-------------------------------  
        Dim sStoredProcedure As String = "uspClaimVehicleGetDetailWSXML"
        Dim sParams As String = iClaimAspectID & "," & sInscCompany

        Try
            '-------------------------------
            ' Get CRUD Data
            '-------------------------------
            oCRUDXML = wsAPDFoundation.GetCRUD("Vehicle", sUserID)

            If oCRUDXML.InnerXml.Contains("ERROR") Then
                Throw New SystemException("CRUDError: Could not get the Vehicle CRUD for entity: Reference=" & "Reference, UserID=" & sUserID)
            End If

            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            sVehicleCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")

            '-------------------------------
            ' CRUD Permissions 
            '-------------------------------
            Select Case sVehicleCRUD
                Case "0000"
                    'txtPolicyNumber.Enabled = False
                    'txtClientClaimNumber.Enabled = False
                    'txtLossDate.Enabled = False
                    'ddlState.Enabled = False
                    'txtLossDescription.Enabled = False
                    'txtSubmittedBy.Enabled = False
                    'txtIntakeFinishDate.Enabled = False
                    'txtRemarks.Enabled = False
                Case "1111"
                    'txtPolicyNumber.Enabled = True
                    'txtClientClaimNumber.Enabled = True
                    'txtLossDate.Enabled = False
                    'ddlState.Enabled = True
                    'txtLossDescription.Enabled = True
                    'txtSubmittedBy.Enabled = False
                    'txtIntakeFinishDate.Enabled = False
                    'txtRemarks.Enabled = True
                Case Else
                    'txtPolicyNumber.Enabled = False
                    'txtClientClaimNumber.Enabled = False
                    'txtLossDate.Enabled = False
                    'ddlState.Enabled = False
                    'txtLossDescription.Enabled = False
                    'txtSubmittedBy.Enabled = False
                    'txtIntakeFinishDate.Enabled = False
                    'txtRemarks.Enabled = False
            End Select

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "EXECSP", "GetVehicleData - Start: " & Date.Now, "EXECSP: " & sStoredProcedure & " Params: " & sParams, "ReturnXML = " & oReturnXML.InnerXml)
            End If

            '------------------------------------
            ' Load Root level vehicle data 
            '------------------------------------
            Try
                veh.LynxID = sLynxID
                veh.UserID = sUserID
                veh.InsuranceCompanyID = sInscCompany

                oXMLNode = oReturnXML.SelectSingleNode("Vehicle")
                veh.VehicleYear = GetXMLNodeattrbuteValue(oXMLNode, "VehicleYear")
                veh.Make = GetXMLNodeattrbuteValue(oXMLNode, "Make")
                veh.Model = GetXMLNodeattrbuteValue(oXMLNode, "Model")
                veh.BodyStyle = GetXMLNodeattrbuteValue(oXMLNode, "BodyStyle")
                veh.Color = GetXMLNodeattrbuteValue(oXMLNode, "Color")
                veh.VIN = GetXMLNodeattrbuteValue(oXMLNode, "VIN")
                veh.EstimateVIN = GetXMLNodeattrbuteValue(oXMLNode, "EstimateVIN")
                veh.LicensePlateNumber = GetXMLNodeattrbuteValue(oXMLNode, "LicensePlateNumber")
                veh.Mileage = GetXMLNodeattrbuteValue(oXMLNode, "Mileage")
                veh.BookValueAmt = GetXMLNodeattrbuteValue(oXMLNode, "BookValueAmt")
                veh.RentalDaysAuthorized = GetXMLNodeattrbuteValue(oXMLNode, "RentalDaysAuthorized")
                veh.RentalInstructions = UCase(GetXMLNodeattrbuteValue(oXMLNode, "RentalInstructions"))
                veh.CurrentExposure = GetXMLNodeattrbuteValue(oXMLNode, "ExposureCD")
                If (GetXMLNodeattrbuteValue(oXMLNode, "CurrentAssignmentTypeID") <> "") Then
                    veh.CurrentAssignmentTypeID = GetXMLNodeattrbuteValue(oXMLNode, "CurrentAssignmentTypeID")
                Else
                    veh.CurrentAssignmentTypeID = GetXMLNodeattrbuteValue(oXMLNode, "InitialAssignmentTypeID")
                End If
                veh.VehicleRemarks = GetXMLNodeattrbuteValue(oXMLNode, "Remarks")
                veh.Status = GetXMLNodeattrbuteValue(oXMLNode, "Status")
                veh.ClaimAspectID = GetXMLNodeattrbuteValue(oXMLNode, "ClaimAspectID")
                veh.CoverageProfileCD = GetXMLNodeattrbuteValue(oXMLNode, "CoverageProfileCD")
                veh.WarrantyExistsFlag = GetXMLNodeattrbuteValue(oXMLNode, "WarrantyExistsFlag")

                veh.Warrantylst = New List(Of Warranty)

                If veh.WarrantyExistsFlag = "1" Then
                    GetVehicleWarrantyData(veh.ClaimAspectID, veh)
                End If

                veh.ImpactSpeed = GetXMLNodeattrbuteValue(oXMLNode, "ImpactSpeed")
                veh.PostedSpeed = GetXMLNodeattrbuteValue(oXMLNode, "PostedSpeed")
                veh.InspectionDate = GetXMLNodeattrbuteValue(oXMLNode, "InspectionDate")
                veh.ClientCoverageTypeID = GetXMLNodeattrbuteValue(oXMLNode, "ClientCoverageTypeID")
                veh.PermissionToDriveCD = GetXMLNodeattrbuteValue(oXMLNode, "PermissionToDriveCD")

                veh.SysLastUpdatedDate = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate")
                sVehicleSysLastUpdatedDate = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate")
                'hidVehicleClaimAspectID.value = veh.ClaimAspectID
            Catch ex As Exception
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLDataERROR", "GetVehicleData - Parse Vehicle: " & Date.Now, "ERROR: Parsing Vehicle XMLNodes", "ReturnXML = " & veh.ToString)
            End Try

            '---------------------------------
            ' Exposure drives insured update
            '---------------------------------
            ' check the exposure.  1st party not saveable, 3rd yes
            If veh.CurrentExposure = "3" And txtNameLast.Enabled = False Then
                InsuredEnableDisable(True)
            Else
                InsuredEnableDisable(False)
            End If

            Try
                veh.CurrentLicensePlateState = UCase(GetXMLNodeattrbuteValue(oXMLNode, "LicensePlateState"))
            Catch ex As Exception
            End Try

            '--------------------------------------
            ' Check Page Readonly based on Status
            '--------------------------------------
            If UCase(veh.Status) = "VEHICLE CANCELLED" Or UCase(veh.Status) = "VEHICLE VOIDED" Then
                bPageReadOnly = True
            End If

            'txtImpactPoints.Text = oXMLNode.Attributes("Model").InnerText
            'txtPriorDamagePoints.Text = oXMLNode.Attributes("Model").InnerText
            veh.Remarks = GetXMLNodeattrbuteValue(oXMLNode, "Remarks")
            veh.InitialAssignmentTypeID = GetXMLNodeattrbuteValue(oXMLNode, "InitialAssignmentTypeID")

            '-------------------------------
            ' Load Reference data
            '-------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("Reference")

            veh.LicensePlateStateCollection = New List(Of ReferenceState)
            veh.LocationStateCollection = New List(Of ReferenceState)
            veh.ContactStateCollection = New List(Of ReferenceState)
            veh.InvolvedStateCollection = New List(Of ReferenceState)
            veh.ExposureCollection = New List(Of ReferenceExposure)
            veh.ImpactCollection = New List(Of ReferenceImpactPoints)
            veh.CoverageCollection = New List(Of ReferenceCoverage)

            veh.ContactInsuredRelationCollection = New List(Of ReferenceInsuredRelation)
            veh.BestContactTabPhoneCDCollection = New List(Of ReferenceContactTabPhone)

            'veh.CommunicationMethodCollection = New List(Of ReferenceCommunicationMethod)

            objReferenceState = New ReferenceState()
            objReferenceState.ReferenceID = ""
            objReferenceState.statename = "0"

            veh.LicensePlateStateCollection.Add(objReferenceState)
            veh.LocationStateCollection.Add(objReferenceState)
            veh.ContactStateCollection.Add(objReferenceState)
            veh.InvolvedStateCollection.Add(objReferenceState)
            'veh.CommunicationMethodCollection.Add(objReferenceCommunicationMethod)

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLDataParse", "GetVehicleData - Parse References: " & Date.Now, "Parsing Vehicle XMLNodes for References", "")
            End If

            'Define table structure
            dtReferences.Columns.Add(New DataColumn("List", GetType(System.String)))
            dtReferences.Columns.Add(New DataColumn("ReferenceID", GetType(System.String)))
            dtReferences.Columns.Add(New DataColumn("Name", GetType(System.String)))

            For Each oXMLNode In oXMLNodeList
                '----------------------------
                ' Create Full Reference Hash
                '----------------------------
                drReferences = dtReferences.NewRow
                'add values to each rows
                drReferences("List") = GetXMLNodeattrbuteValue(oXMLNode, "List")
                drReferences("ReferenceID") = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")
                drReferences("Name") = GetXMLNodeattrbuteValue(oXMLNode, "Name")
                'add the row to DataTable
                dtReferences.Rows.Add(drReferences)


                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "State" Then
                    objReferenceState = New ReferenceState()
                    objReferenceState.ReferenceID = GetXMLNodeattrbuteValue(oXMLNode, "Name")
                    objReferenceState.statename = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")

                    veh.LicensePlateStateCollection.Add(objReferenceState)
                    veh.LocationStateCollection.Add(objReferenceState)
                    veh.ContactStateCollection.Add(objReferenceState)
                    veh.InvolvedStateCollection.Add(objReferenceState)
                End If

                '---------------------------------------
                ' Load Reference data - Exposure
                '---------------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "Exposure" Then
                    objReferenceExposure = New ReferenceExposure()
                    objReferenceExposure.ReferenceID = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")
                    objReferenceExposure.ExposureName = GetXMLNodeattrbuteValue(oXMLNode, "Name")

                    If objReferenceExposure.ExposureName = veh.CurrentExposure Then
                        veh.CurrentExposure = objReferenceExposure.ReferenceID
                    End If

                    veh.ExposureCollection.Add(objReferenceExposure)
                End If


                '---------------------------------------
                ' Load Reference data - ContactInsuredRelationCollection
                '---------------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "ContactRelationToInsured" Then
                    objReferenceInsuredRelation = New ReferenceInsuredRelation()
                    objReferenceInsuredRelation.ReferenceID = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")
                    objReferenceInsuredRelation.Name = GetXMLNodeattrbuteValue(oXMLNode, "Name")
                    veh.ContactInsuredRelationCollection.Add(objReferenceInsuredRelation)
                End If

                '---------------------------------------
                ' Load Reference data - BestContactTabPhoneCDCollection
                '---------------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "BestContactPhone" Then
                    objReferenceContactTabPhone = New ReferenceContactTabPhone()
                    objReferenceContactTabPhone.ReferenceID = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")
                    objReferenceContactTabPhone.Name = GetXMLNodeattrbuteValue(oXMLNode, "Name")
                    veh.BestContactTabPhoneCDCollection.Add(objReferenceContactTabPhone)
                End If


                '---------------------------------------
                ' Load Reference data - AssignmentType
                '---------------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "AssignmentType" Then
                    hAssignmentType.Add(GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID"), GetXMLNodeattrbuteValue(oXMLNode, "Name"))
                End If

                '---------------------------------------
                ' Load Reference data - Impact
                '---------------------------------------
                ''Get Impacts

                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "Impact" Then
                    oXMLImpactNodeList = oReturnXML.SelectNodes("Vehicle/Impact")
                    For Each oXMLImpactNode In oXMLImpactNodeList
                        If GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID") = GetXMLNodeattrbuteValue(oXMLImpactNode, "ImpactID") Then
                            'impact
                            If GetXMLNodeattrbuteValue(oXMLImpactNode, "CurrentImpactFlag") = 1 And GetXMLNodeattrbuteValue(oXMLImpactNode, "PrimaryImpactFlag") = 1 Then
                                If strImpact = "" Then
                                    strImpact = GetXMLNodeattrbuteValue(oXMLNode, "Name") + " (Primary)"
                                Else
                                    strImpact = strImpact + "," + GetXMLNodeattrbuteValue(oXMLNode, "Name") + " (Primary)"
                                End If
                            ElseIf GetXMLNodeattrbuteValue(oXMLImpactNode, "CurrentImpactFlag") = 1 Then
                                If strImpact = "" Then
                                    strImpact = GetXMLNodeattrbuteValue(oXMLNode, "Name")
                                Else
                                    strImpact = strImpact + "," + GetXMLNodeattrbuteValue(oXMLNode, "Name")
                                End If
                            End If
                            'priorImpact
                            If GetXMLNodeattrbuteValue(oXMLImpactNode, "PriorImpactFlag") = 1 Then
                                If strPriorImpact = "" Then
                                    strPriorImpact = GetXMLNodeattrbuteValue(oXMLNode, "Name")
                                Else
                                    strPriorImpact = strPriorImpact + "," + GetXMLNodeattrbuteValue(oXMLNode, "Name")
                                End If
                            End If
                        End If
                    Next



                    'hImpact.Add(GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID"), GetXMLNodeattrbuteValue(oXMLNode, "Name"))
                    'objReferenceImpactPoints = New ReferenceImpactPoints()
                    'objReferenceImpactPoints.ReferenceID = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")
                    'objReferenceImpactPoints.ImpactName = GetXMLNodeattrbuteValue(oXMLNode, "Name")
                    'veh.ImpactCollection.Add(objReferenceImpactPoints)
                End If

                '---------------------------------------
                ' Load Reference data - Coverage
                '---------------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "CoverageProfile" Then
                    hCoverageProfile.Add(GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID"), GetXMLNodeattrbuteValue(oXMLNode, "Name"))
                End If
            Next

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLDataParse", "GetVehicleData - Parse XMLNodes for Vehicle/ClaimAspectServiceChannel: " & Date.Now, "", "")
            End If

            '---------------------------------------
            ' Load Vehicle data - ClaimAspectServiceChannel
            '---------------------------------------
            oXMLNode = oReturnXML.SelectSingleNode("Vehicle/ClaimAspectServiceChannel")
            veh.ServiceChannelCD = GetXMLNodeattrbuteValue(oXMLNode, "ServiceChannelCD")

            '---------------------------------------
            ' Load Vehicle data - Location
            '---------------------------------------
            oXMLNode = oReturnXML.SelectSingleNode("Vehicle")
            veh.LocationName = GetXMLNodeattrbuteValue(oXMLNode, "LocationName")
            veh.LocationAddress1 = GetXMLNodeattrbuteValue(oXMLNode, "LocationAddress1")
            veh.LocationAddress2 = GetXMLNodeattrbuteValue(oXMLNode, "LocationAddress2")
            veh.LocationCity = GetXMLNodeattrbuteValue(oXMLNode, "LocationCity")
            veh.LocationState = GetXMLNodeattrbuteValue(oXMLNode, "LocationState")
            veh.LocationZip = GetXMLNodeattrbuteValue(oXMLNode, "LocationZip")
            veh.LocationPhone = "(" & GetXMLNodeattrbuteValue(oXMLNode, "LocationAreaCode") & ") " & GetXMLNodeattrbuteValue(oXMLNode, "LocationExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "LocationUnitNumber")
            veh.InitialCoverageProfile = GetXMLNodeattrbuteValue(oXMLNode, "CoverageProfileCD")
            veh.DriveableFlag = GetXMLNodeattrbuteValue(oXMLNode, "DriveableFlag")
            veh.StrImpactPoints = strImpact
            veh.StrPriorImpactPoints = strPriorImpact
            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLDataParse", "GetVehicleData - Parse XMLNodes for Vehicle/Contact: " & Date.Now, "", "")
            End If

            '---------------------------------------
            ' Load Vehicle data - Contact
            '---------------------------------------
            oXMLNode = oReturnXML.SelectSingleNode("Vehicle/Contact")
            veh.ContactNameTitle = GetXMLNodeattrbuteValue(oXMLNode, "NameTitle")
            veh.ContactNameFirst = GetXMLNodeattrbuteValue(oXMLNode, "NameFirst")
            veh.ContactNameLast = GetXMLNodeattrbuteValue(oXMLNode, "NameLast")
            veh.ContactAddress1 = GetXMLNodeattrbuteValue(oXMLNode, "Address1")
            veh.ContactAddress2 = GetXMLNodeattrbuteValue(oXMLNode, "Address2")
            veh.ContactCity = GetXMLNodeattrbuteValue(oXMLNode, "AddressCity")
            veh.ContactState = GetXMLNodeattrbuteValue(oXMLNode, "AddressState")  'oXMLNode.Attributes("AddressState").InnerText
            veh.ContactZip = GetXMLNodeattrbuteValue(oXMLNode, "AddressZip")
            veh.ContactEmailAddress = GetXMLNodeattrbuteValue(oXMLNode, "EmailAddress")

            veh.AlternateAreaCode = GetXMLNodeattrbuteValue(oXMLNode, "AlternateAreaCode")
            veh.AlternateExchangeNumber = GetXMLNodeattrbuteValue(oXMLNode, "AlternateExchangeNumber")
            veh.AlternateExtensionNumber = GetXMLNodeattrbuteValue(oXMLNode, "AlternateExtensionNumber")
            veh.AlternateUnitNumber = GetXMLNodeattrbuteValue(oXMLNode, "AlternateUnitNumber")

            veh.BestContactPhoneCD = GetXMLNodeattrbuteValue(oXMLNode, "BestContactPhoneCD")
            veh.BestContactTime = GetXMLNodeattrbuteValue(oXMLNode, "BestContactTime")
            veh.DayAreaCode = GetXMLNodeattrbuteValue(oXMLNode, "DayAreaCode")
            veh.DayExchangeNumber = GetXMLNodeattrbuteValue(oXMLNode, "DayExchangeNumber")
            veh.DayExtensionNumber = GetXMLNodeattrbuteValue(oXMLNode, "DayExtensionNumber")
            veh.DayUnitNumber = GetXMLNodeattrbuteValue(oXMLNode, "DayUnitNumber")

            veh.NightAreaCode = GetXMLNodeattrbuteValue(oXMLNode, "NightAreaCode")
            veh.NightExchangeNumber = GetXMLNodeattrbuteValue(oXMLNode, "NightExchangeNumber")
            veh.NightExtensionNumber = GetXMLNodeattrbuteValue(oXMLNode, "NightExtensionNumber")
            veh.NightUnitNumber = GetXMLNodeattrbuteValue(oXMLNode, "NightUnitNumber")

            veh.CurContactInsuredRelation = GetXMLNodeattrbuteValue(oXMLNode, "InsuredRelationID")
            veh.CurBestContactTabPhoneCD = GetXMLNodeattrbuteValue(oXMLNode, "BestContactPhoneCD")

            veh.ContactSysLastUpdatedDate = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate")

            'Missing fields in contact tab
            veh.InsuredRelationID = GetXMLNodeattrbuteValue(oXMLNode, "InsuredRelationID")
            veh.BestContactPhoneCD = GetXMLNodeattrbuteValue(oXMLNode, "BestContactPhoneCD")
            veh.BestContactTabTime = GetXMLNodeattrbuteValue(oXMLNode, "BestContactTime")
            veh.ContactDay = "( " & GetXMLNodeattrbuteValue(oXMLNode, "DayAreaCode") & " ) " & GetXMLNodeattrbuteValue(oXMLNode, "DayExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "DayUnitNumber")
            veh.ContactNight = "( " & GetXMLNodeattrbuteValue(oXMLNode, "NightAreaCode") & " ) " & GetXMLNodeattrbuteValue(oXMLNode, "NightExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "NightUnitNumber")
            veh.ContactAlternate = "( " & GetXMLNodeattrbuteValue(oXMLNode, "AlternateAreaCode") & " ) " & GetXMLNodeattrbuteValue(oXMLNode, "AlternateExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "AlternateUnitNumber")
            veh.ContactCell = "( " & GetXMLNodeattrbuteValue(oXMLNode, "CellAreaCode") & " ) " & GetXMLNodeattrbuteValue(oXMLNode, "CellExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "CellUnitNumber")

            veh.ContactSysLastUpdatedDate = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate")

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLDataParse", "GetVehicleData - Parse XMLNodes for Vehicle/Involved: " & Date.Now, "", "")
            End If

            '---------------------------------------------------------------------
            ' Load Vehicle data - Involved List from uspClaimVehicleGetDetailWSXML
            ' This code used for populate the dropdown list 
            '----------------------------------------------------------------------
            Dim oXMLInvolvedNodeList As XmlNodeList = Nothing
            oXMLInvolvedNodeList = oReturnXML.SelectNodes("Vehicle/Involved")

            veh.Involveddatalist = New List(Of VehicleInvolvedlst)
            veh.InvolvedInsuredCount = 0
            veh.involvedClaimantCount = 0

            For Each oXMLNode In oXMLInvolvedNodeList

                Dim oVehicleInvolvedlst As New VehicleInvolvedlst
                Dim oxmlChildNode As XmlNode = Nothing

                oVehicleInvolvedlst.InvolvedID = GetXMLNodeattrbuteValue(oXMLNode, "InvolvedID")
                oVehicleInvolvedlst.InvolvedNameFirst = GetXMLNodeattrbuteValue(oXMLNode, "NameFirst")
                oVehicleInvolvedlst.InvolvedNameLast = GetXMLNodeattrbuteValue(oXMLNode, "NameLast")
                oVehicleInvolvedlst.InvolvedCity = GetXMLNodeattrbuteValue(oXMLNode, "AddressCity")
                oVehicleInvolvedlst.InvolvedZip = GetXMLNodeattrbuteValue(oXMLNode, "AddressZip")
                oVehicleInvolvedlst.InvolvedBusinessName = GetXMLNodeattrbuteValue(oXMLNode, "BusinessName")

                If oVehicleInvolvedlst.InvolvedID <> "" Then
                    Dim oReturnInvolvedXML As XmlElement = Nothing
                    Dim oxmlInvolvedNode As XmlNode = Nothing
                    Dim sInvolvedStoredProcedure As String = "uspVehicleInvolvedGetDetailWSXML"
                    Dim sInvolvedParams As String = oVehicleInvolvedlst.InvolvedID & "," & sInscCompany
                    oReturnInvolvedXML = wsAPDFoundation.ExecuteSpAsXML(sInvolvedStoredProcedure, sInvolvedParams)
                    oxmlInvolvedNode = oReturnInvolvedXML.SelectSingleNode("Involved")
                    oVehicleInvolvedlst.InvolvedNameTitle = GetXMLNodeattrbuteValue(oxmlInvolvedNode, "NameTitle")
                    oVehicleInvolvedlst.InvolvedState = GetXMLNodeattrbuteValue(oxmlInvolvedNode, "AddressState")
                    oVehicleInvolvedlst.InvolvedGender = GetXMLNodeattrbuteValue(oxmlInvolvedNode, "GenderCD")
                    oVehicleInvolvedlst.InvolvedBestContactPhoneCD = GetXMLNodeattrbuteValue(oxmlInvolvedNode, "BestContactPhoneCD")
                    oVehicleInvolvedlst.InvolvedBusinessTypeCD = GetXMLNodeattrbuteValue(oxmlInvolvedNode, "BusinessTypeCD")
                    oVehicleInvolvedlst.InvolvedEmailAddress = GetXMLNodeattrbuteValue(oxmlInvolvedNode, "EmailAddress")
                End If

                For Each oxmlChildNode In oXMLNode.ChildNodes
                    oVehicleInvolvedlst.InvolvedTypes = oVehicleInvolvedlst.InvolvedTypes + oxmlChildNode.Attributes("InvolvedTypeName").InnerText + ","

                    If (oxmlChildNode.Attributes("InvolvedTypeName").InnerText = "Insured") Then
                        veh.InvolvedInsuredCount = veh.InvolvedInsuredCount + 1
                    End If

                    If (oxmlChildNode.Attributes("InvolvedTypeName").InnerText = "Claimant") Then
                        veh.involvedClaimantCount = veh.involvedClaimantCount + 1
                    End If
                Next

                oVehicleInvolvedlst.InvolvedTypes = oVehicleInvolvedlst.InvolvedTypes.Substring(0, oVehicleInvolvedlst.InvolvedTypes.Length - 1)

                oVehicleInvolvedlst.InvolvedDisplayName = GetXMLNodeattrbuteValue(oXMLNode, "NameFirst") + " " + GetXMLNodeattrbuteValue(oXMLNode, "NameLast") + "[" + oVehicleInvolvedlst.InvolvedTypes + "]"

                veh.Involveddatalist.Add(oVehicleInvolvedlst)
            Next

            '---------------------------------------
            ' Load Vehicle data - Involved
            '---------------------------------------
            oXMLNode = oReturnXML.SelectSingleNode("Vehicle/Involved")
            veh.InvolvedID = GetXMLNodeattrbuteValue(oXMLNode, "InvolvedID")
            veh.Involveddata = GetVehicleInvolvedData(veh.InvolvedID)

            ' Involved Checkboxes
            oXMLNodeList = oReturnXML.SelectNodes("Vehicle/Involved/InvolvedType/@InvolvedTypeName")
            For Each oNode In oXMLNodeList
                Select Case UCase(oXMLNodeList.Item(0).Value)
                    Case "INSURED"
                    Case "Claimant"
                        'cbInsured.checked = True
                    Case "OWNER"
                    Case "DRIVER"
                    Case "PASSENGER"
                End Select

            Next

            '---------------------------------------
            ' Load Vehicle data - Assignment Key
            '---------------------------------------
            For Each sAssignmentKey In hAssignmentType
                If sAssignmentKey.key = veh.InitialAssignmentTypeID Then
                    veh.InitialAssignmentType = sAssignmentKey.value
                End If
            Next

            '---------------------------------------
            ' Load Vehicle data - Covrage Key
            '---------------------------------------
            For Each sCoverageKey In hCoverageProfile
                If sCoverageKey.key = veh.InitialCoverageProfile Then
                    veh.InitialCoverageProfile = sCoverageKey.value
                End If
            Next

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLDataParse", "GetVehicleData - Parse XMLNodes for Vehicle/Impact: " & Date.Now, "", "")
            End If

            '---------------------------------------
            ' Load Primary Impact Points
            '---------------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("Vehicle/Impact")
            veh.DTImpactPoints = New DataTable

            veh.DTImpactPoints.Columns.Add(New DataColumn("ReferenceID", GetType(String)))
            veh.DTImpactPoints.Columns.Add(New DataColumn("ImpactName", GetType(String)))

            For Each oXMLNode In oXMLNodeList
                For Each simpact In hImpact
                    If simpact.key = GetXMLNodeattrbuteValue(oXMLNode, "ImpactID") And
                    GetXMLNodeattrbuteValue(oXMLNode, "PrimaryImpactFlag") = 1 And
                    GetXMLNodeattrbuteValue(oXMLNode, "CurrentImpactFlag") = 1 Then

                        Dim DRImpactPoints As DataRow = veh.DTImpactPoints.NewRow
                        DRImpactPoints("ReferenceID") = simpact.Key
                        DRImpactPoints("ImpactName") = simpact.Value

                        veh.ImpactPoints = simpact.value + "(Primary) ,"
                        veh.DTImpactPoints.Rows.Add(DRImpactPoints)
                    End If
                Next
            Next

            '---------------------------------------
            ' Load Previous Impact Points
            '---------------------------------------
            'oXMLNodeList = oReturnXML.SelectNodes("Vehicle/Impact")
            veh.DTPriorImpactPoints = New DataTable

            veh.DTPriorImpactPoints.Columns.Add(New DataColumn("ReferenceID", GetType(String)))
            veh.DTPriorImpactPoints.Columns.Add(New DataColumn("ImpactName", GetType(String)))

            For Each oXMLNode In oXMLNodeList
                For Each simpact In hImpact
                    If simpact.key = GetXMLNodeattrbuteValue(oXMLNode, "ImpactID") And
                    GetXMLNodeattrbuteValue(oXMLNode, "PrimaryImpactFlag") = 0 And
                    GetXMLNodeattrbuteValue(oXMLNode, "CurrentImpactFlag") = 1 Then

                        Dim DRPriorImpactPoints As DataRow = veh.DTPriorImpactPoints.NewRow
                        DRPriorImpactPoints("ReferenceID") = simpact.Key
                        DRPriorImpactPoints("ImpactName") = simpact.Value

                        veh.PriorImpactPoints = simpact.value + "(Prior) ,"
                        veh.DTPriorImpactPoints.Rows.Add(DRPriorImpactPoints)
                    End If
                Next
            Next

            '    For Each oXMLNode In oXMLNodeList
            '    For Each simpact In hImpact
            '        If simpact.key = GetXMLNodeattrbuteValue(oXMLNode, "ImpactID") And
            '            GetXMLNodeattrbuteValue(oXMLNode, "PrimaryImpactFlag") = 0 And
            '            GetXMLNodeattrbuteValue(oXMLNode, "CurrentImpactFlag") = 1 Then
            '            veh.PriorImpactPoints = veh.ImpactPoints + simpact.value
            '        End If
            '    Next
            'Next

            Try
                'ddlLicensePlateState.SelectedValue = GetXMLNodeattrbuteValue(oXMLNode, "LicensePlateState")
            Catch ex As Exception
                'ddlLicensePlateState.SelectedIndex = 0
            End Try

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLDataParse", "GetVehicleData - Parse XMLNodes for Vehicle/ClaimAspectServiceChannel: " & Date.Now, "", "")
            End If

            '---------------------------------------
            ' Load Root level claim data - Coverage
            '---------------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("Vehicle/ClaimAspectServiceChannel")

            veh.DTCoverage = New DataTable
            Dim DCCoverage As New DataColumn

            veh.DTCoverage.Columns.Add(New DataColumn("Channel", GetType(String)))
            veh.DTCoverage.Columns.Add(New DataColumn("Status", GetType(String)))
            veh.DTCoverage.Columns.Add(New DataColumn("EstAmount", GetType(String)))
            veh.DTCoverage.Columns.Add(New DataColumn("Coverage", GetType(String)))
            veh.DTCoverage.Columns.Add(New DataColumn("Deductable", GetType(String)))
            veh.DTCoverage.Columns.Add(New DataColumn("OverageAndLmt", GetType(String)))

            For Each oXMLNode In oXMLNodeList
                Dim DRCoverage As DataRow = veh.DTCoverage.NewRow
                DRCoverage("Channel") = GetXMLNodeattrbuteValue(oXMLNode, "ServiceChannelName")
                DRCoverage("Status") = GetXMLNodeattrbuteValue(oXMLNode, "StatusName")
                DRCoverage("EstAmount") = GetXMLNodeattrbuteValue(oXMLNode, "CurEstGrossRepairTotal")
                DRCoverage("Coverage") = GetXMLNodeattrbuteValue(oXMLNode.SelectSingleNode("Coverage"), "ClientCode")
                DRCoverage("Deductable") = GetXMLNodeattrbuteValue(oXMLNode.SelectSingleNode("Coverage"), "DeductibleAppliedAmt") + "/" + GetXMLNodeattrbuteValue(oXMLNode.SelectSingleNode("Coverage"), "DeductibleAmt")
                DRCoverage("OverageAndLmt") = GetXMLNodeattrbuteValue(oXMLNode.SelectSingleNode("Coverage"), "Overage") + "/" + GetXMLNodeattrbuteValue(oXMLNode.SelectSingleNode("Coverage"), "LimitAmt")


                'veh.ServiceChannels += oXMLNode.Attributes("ServiceChannelName").InnerText & "|"

                veh.DTCoverage.Rows.Add(DRCoverage)
                If GetXMLNodeattrbuteValue(oXMLNode, "StatusName") = "Active" Then
                    veh.ActiveServiceChannel = GetXMLNodeattrbuteValue(oXMLNode, "ServiceChannelName")
                End If
                veh.ServiceChannels = GetXMLNodeattrbuteValue(oXMLNode, "ServiceChannelName")
            Next

            If (Not IsNothing(veh.InspectionDate) And oXMLNode.Attributes("InspectionDate").Value <> "") Then
                veh.InspectionDate = Format(CDate(oXMLNode.Attributes("InspectionDate").Value), "MM/dd/yyyy")
            End If

            If (Not IsNothing(veh.OriginalEstimateDate) And oXMLNode.Attributes("OriginalEstimateDate").Value <> "") Then
                veh.OriginalEstimateDate = Format(CDate(oXMLNode.Attributes("OriginalEstimateDate").Value), "MM/dd/yyyy")
            End If


            'gvServiceChannel.DataSource = DTCoverage
            'gvServiceChannel.DataBind()
            'gvServiceChannel.Visible = True

            'gvAssignmentCoverage.visible = True

            '---------------------------------------
            ' ImpactPoints GridView
            '---------------------------------------
            'veh.DTImpactPoints = New DataTable
            'Dim DCImpactPoints As New DataColumn

            'veh.DTImpactPoints.Columns.Add(New DataColumn("ImpactID", GetType(String)))
            'veh.DTImpactPoints.Columns.Add(New DataColumn("ImpactPoint", GetType(String)))

            'oXMLNodeList = oReturnXML.SelectNodes("Reference")

            'For Each oXMLNode In oXMLNodeList
            '    If oXMLNode.Attributes("List").Value = "Impact" Then
            '        Dim DRImpactPoints As DataRow = veh.DTImpactPoints.NewRow
            '        DRImpactPoints("ImpactID") = oXMLNode.Attributes("ReferenceID").InnerText
            '        DRImpactPoints("ImpactPoint") = oXMLNode.Attributes("Name").InnerText

            '        veh.DTImpactPoints.Rows.Add(DRImpactPoints)
            '    End If
            'Next

            'gvImpactPoints.DataSource = DTImpactPoints
            'gvImpactPoints.DataBind()

            'gvImpactPoints.Columns(0).Visible = True

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLDataParse", "GetVehicleData - Parse XMLNodes for Vehicle/ClaimAspectServiceChannel/Coverage: " & Date.Now, "", "")
            End If

            '-----------------------------------------------------
            ' Load vehicle assignment data - Assignment Coverage
            '-----------------------------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("Vehicle/ClaimAspectServiceChannel/Coverage")

            veh.DTAssignmentCoverage = New DataTable
            Dim DCAssignmentCoverage As New DataColumn

            veh.DTAssignmentCoverage.Columns.Add(New DataColumn("Coverage", GetType(String)))
            veh.DTAssignmentCoverage.Columns.Add(New DataColumn("Deductable", GetType(String)))
            veh.DTAssignmentCoverage.Columns.Add(New DataColumn("Overage", GetType(String)))
            veh.DTAssignmentCoverage.Columns.Add(New DataColumn("Partial", GetType(String)))

            Dim result() As DataRow

            If oXMLNodeList.Count > 0 Then

                For Each oXMLNode In oXMLNodeList
                    Dim DRAssignmentCoverage As DataRow = veh.DTAssignmentCoverage.NewRow
                    result = dtReferences.Select("List = 'CoverageProfile'AND ReferenceID = '" & GetXMLNodeattrbuteValue(oXMLNode, "CoverageTypeCD") & "'")
                    DRAssignmentCoverage("Coverage") = result(0).Item("Name")
                    DRAssignmentCoverage("Deductable") = GetXMLNodeattrbuteValue(oXMLNode, "DeductibleAppliedAmt")
                    DRAssignmentCoverage("Overage") = GetXMLNodeattrbuteValue(oXMLNode, "DeductibleAmt") & "/" & GetXMLNodeattrbuteValue(oXMLNode, "LimitAmt")
                    DRAssignmentCoverage("Partial") = GetXMLNodeattrbuteValue(oXMLNode, "LimitAppliedAmt")

                    veh.DTAssignmentCoverage.Rows.Add(DRAssignmentCoverage)
                    'If oXMLNode.Attributes("StatusName").InnerText = "Active" Then
                    'veh.ActiveServiceChannel = oXMLNode.Attributes("ServiceChannelName").InnerText
                    'End If
                Next
            Else
                Dim DRAssignmentCoverage As DataRow = veh.DTAssignmentCoverage.NewRow
                DRAssignmentCoverage("Deductable") = "No Records To Display"
                veh.DTAssignmentCoverage.Rows.Add(DRAssignmentCoverage)
            End If

            '-----------------------------------
            ' Estimate Summary
            '-----------------------------------
            veh.DTDocumentEstimate = New DataTable
            Dim DCDocumentEstimate As New DataColumn

            veh.DTDocumentEstimate.Columns.Add(New DataColumn("EstRecvDate", GetType(DateTime)))
            veh.DTDocumentEstimate.Columns.Add(New DataColumn("EstSummary", GetType(String)))
            veh.DTDocumentEstimate.Columns.Add(New DataColumn("EstSeq", GetType(String)))
            veh.DTDocumentEstimate.Columns.Add(New DataColumn("EstSource", GetType(String)))
            veh.DTDocumentEstimate.Columns.Add(New DataColumn("EstGross", GetType(String)))
            veh.DTDocumentEstimate.Columns.Add(New DataColumn("EstNet", GetType(String)))
            veh.DTDocumentEstimate.Columns.Add(New DataColumn("EstOrgAud", GetType(String)))
            veh.DTDocumentEstimate.Columns.Add(New DataColumn("EstDup", GetType(String)))
            veh.DTDocumentEstimate.Columns.Add(New DataColumn("EstAgreed", GetType(String)))
            veh.DTDocumentEstimate.Columns.Add(New DataColumn("DocumentID", GetType(String)))
            veh.DTDocumentEstimate.Columns.Add(New DataColumn("FullSummaryExistsFlag", GetType(String)))
            veh.DTDocumentEstimate.Columns.Add(New DataColumn("ImageLocation", GetType(String)))


            Dim sDocumentLocation As String = ""
            Dim oXMLDocumentNodeList As XmlNodeList = Nothing

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLDataParse", "GetVehicleData - Parse XMLNodes for Vehicle/Document: " & Date.Now, "", "")
            End If

            '----------------------------------
            ' Test and see if document exist
            '----------------------------------
            oXMLDocumentNodeList = oReturnXML.SelectNodes("Vehicle/Document")
            Dim oDocumentRows As New List(Of Documents)()
            Dim oXMLDocumentNode As XmlNode
            oDocumentRows.Clear()

            For Each oXMLDocumentNode In oXMLDocumentNodeList
                If oXMLDocumentNode.Attributes("DocumentID").Value <> 0 Then
                    'Dim DRDocumentDocuments As DataRow = veh.DTDocumentDocuments.NewRow
                    'DRDocumentDocuments("ImageLocationRaw") = "Some Data" 'oXMLNode.Attributes("ServiceChannelName").InnerText

                    '----------------------------
                    ' Document Details
                    '----------------------------
                    Dim oDocuments As New Documents

                    oDocuments.DocumentID = GetXMLNodeattrbuteValue(oXMLDocumentNode, "DocumentID")
                    oDocuments.DocumentSourceName = GetXMLNodeattrbuteValue(oXMLDocumentNode, "DocumentSourceName")
                    oDocuments.DocumentTypeName = GetXMLNodeattrbuteValue(oXMLDocumentNode, "DocumentTypeName")
                    oDocuments.CreatedDate = GetXMLNodeattrbuteValue(oXMLDocumentNode, "CreatedDate")

                    oDocuments.ReceivedDate = Format(CDate(GetXMLNodeattrbuteValue(oXMLDocumentNode, "ReceivedDate")), "MM/dd/yyyy")

                    '----------------------------
                    ' Document Image Details
                    '----------------------------
                    sDocumentLocation = GetXMLNodeattrbuteValue(oXMLDocumentNode, "ImageLocation")
                    oDocuments.ImageLocationRaw = AppSettings("ImagePath").Replace("\", "\\") & sDocumentLocation
                    oDocuments.ImageLocation = AppSettings("ImagePath") & sDocumentLocation.Replace("\\", "\")
                    oDocuments.ImageLocationThumbnail = AppSettings("ImageThumbnailPath") & sDocumentLocation.Replace("\\", "\")

                    Select Case UCase(Right(oDocuments.ImageLocation, 3))
                        Case "JPG", "GIF", "PNG"
                            oDocuments.ImageViewable = 1
                            oDocuments.ImageLocationThumbnail = "Thumbnail.asp?Doc=" & oDocuments.ImageLocationThumbnail
                        Case "TIF"
                            oDocuments.ImageViewable = 1
                            oDocuments.ImageLocationThumbnail = "images/tif.png"
                        Case "PDF"
                            oDocuments.ImageViewable = 0
                            oDocuments.ImageLocationThumbnail = "images/PDFDefault.png"
                        Case "DOC"
                            oDocuments.ImageViewable = 0
                            oDocuments.ImageLocationThumbnail = "images/WordDoc.png"
                        Case Else
                            oDocuments.ImageViewable = 0
                            oDocuments.ImageLocationThumbnail = "images/blank.png"
                    End Select

                    If oDocuments.ImageLocation <> AppSettings("ImagePath") Then
                        Dim fs As New System.IO.FileInfo(oDocuments.ImageLocation)
                        If fs.Exists Then
                            oDocuments.ImageSize = Convert.ToInt32(fs.Length / 1000)
                            oDocuments.ImageInfo = oDocuments.DocumentTypeName & Chr(10) & oDocuments.ReceivedDate & Chr(10) & oDocuments.ImageSize & " kb"
                            If oDocuments.DocumentTypeName.Length >= 16 Then
                                oDocuments.ImageInfolbl = oDocuments.DocumentTypeName.Substring(0, 13) + "..." & Chr(10) & oDocuments.ReceivedDate & "<br/>" & Chr(13) + Chr(10) & oDocuments.ImageSize & " kb"
                            Else
                                oDocuments.ImageInfolbl = oDocuments.DocumentTypeName & Chr(10) & oDocuments.ReceivedDate & "<br/>" & Chr(13) + Chr(10) & oDocuments.ImageSize & " kb"
                            End If

                        Else
                            oDocuments.ImageViewable = 0
                            oDocuments.ImageInfo = sDocumentLocation.Replace("\\", "\")
                            oDocuments.ImageLocationThumbnail = "images/NoPhoto.png"
                        End If
                    End If

                    '----------------------------
                    ' Document Estimate Details
                    '----------------------------
                    'oDocuments.VanFlag = oXMLDocumentNode.Attributes("VanFlag").value
                    wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLDataParse", "DEBUG: " & Date.Now, "After 1A", "")
                    oDocuments.ApprovedFlag = GetXMLNodeattrbuteValue(oXMLDocumentNode, "ApprovedFlag")
                    oDocuments.SupplementSeqNumber = GetXMLNodeattrbuteValue(oXMLDocumentNode, "SupplementSeqNumber")
                    oDocuments.FullSummaryExistsFlag = GetXMLNodeattrbuteValue(oXMLDocumentNode, "FullSummaryExistsFlag")
                    oDocuments.AgreedPriceMetCD = GetXMLNodeattrbuteValue(oXMLDocumentNode, "AgreedPriceMetCD")
                    oDocuments.GrossEstimateAmt = GetXMLNodeattrbuteValue(oXMLDocumentNode, "GrossEstimateAmt")
                    oDocuments.NetEstimateAmt = GetXMLNodeattrbuteValue(oXMLDocumentNode, "NetEstimateAmt")
                    oDocuments.EstimateTypeFlag = GetXMLNodeattrbuteValue(oXMLDocumentNode, "EstimateTypeFlag")
                    oDocuments.DirectionToPayFlag = GetXMLNodeattrbuteValue(oXMLDocumentNode, "DirectionToPayFlag")
                    oDocuments.FinalEstimateFlag = GetXMLNodeattrbuteValue(oXMLDocumentNode, "FinalEstimateFlag")
                    oDocuments.DuplicateFlag = GetXMLNodeattrbuteValue(oXMLDocumentNode, "DuplicateFlag")
                    oDocuments.EstimateTypeCD = GetXMLNodeattrbuteValue(oXMLDocumentNode, "EstimateTypeCD")
                    oDocuments.ServiceChannelCD = GetXMLNodeattrbuteValue(oXMLDocumentNode, "ServiceChannelCD")
                    oDocuments.SysLastUpdatedDateDocument = GetXMLNodeattrbuteValue(oXMLDocumentNode, "SysLastUpdatedDateDocument")
                    oDocuments.SysLastUpdatedDateEstimate = GetXMLNodeattrbuteValue(oXMLDocumentNode, "SysLastUpdatedDateEstimate")

                    '---------------------------------
                    ' Debug Data
                    '---------------------------------
                    If UCase(sAPDNetDebugging) = "TRUE" Then
                        wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLDataParse", "DEBUG: " & Date.Now, "After 1", "")
                    End If

                    Dim DRDocumentEstimate As DataRow = veh.DTDocumentEstimate.NewRow
                    DRDocumentEstimate("DocumentID") = oDocuments.DocumentID
                    DRDocumentEstimate("FullSummaryExistsFlag") = oDocuments.FullSummaryExistsFlag
                    DRDocumentEstimate("ImageLocation") = oDocuments.ImageLocation

                    ' DRDocumentEstimate("EstRecvDate") = oDocuments.ReceivedDate

                    If oDocuments.ReceivedDate <> "" Then
                        Dim oDate As DateTime = DateTime.Parse(CDate(GetXMLNodeattrbuteValue(oXMLDocumentNode, "ReceivedDate")))
                        DRDocumentEstimate("EstRecvDate") = oDate
                    End If


                    DRDocumentEstimate("EstSeq") = oDocuments.SupplementSeqNumber + 1
                    DRDocumentEstimate("EstSource") = oDocuments.DocumentSourceName
                    DRDocumentEstimate("EstGross") = oDocuments.GrossEstimateAmt
                    DRDocumentEstimate("EstNet") = oDocuments.NetEstimateAmt

                    Select Case oDocuments.EstimateTypeCD
                        Case "O"
                            DRDocumentEstimate("EstOrgAud") = "Original"
                        Case "A"
                            DRDocumentEstimate("EstOrgAud") = "Audited"
                    End Select
                    Select Case oDocuments.DuplicateFlag
                        Case "0"
                            DRDocumentEstimate("EstDup") = "No"
                        Case "1"
                            DRDocumentEstimate("EstDup") = "Yes"
                    End Select
                    Select Case UCase(oDocuments.AgreedPriceMetCD)
                        Case "N"
                            DRDocumentEstimate("EstAgreed") = "No"
                        Case "Y"
                            DRDocumentEstimate("EstAgreed") = "Yes"
                    End Select

                    '---------------------------------------
                    ' Estimate/Supplement add to Grid
                    '---------------------------------------
                    Select Case UCase(oDocuments.DocumentTypeName)
                        Case "ESTIMATE"
                            DRDocumentEstimate("EstSeq") = "E0" & DRDocumentEstimate("EstSeq")
                        Case "SUPPLEMENT"
                            DRDocumentEstimate("EstSeq") = "S0" & DRDocumentEstimate("EstSeq")
                    End Select

                    If oDocuments.FullSummaryExistsFlag = 1 Then
                        DRDocumentEstimate("EstSummary") = "images/ManualEst.gif"
                    Else
                        DRDocumentEstimate("EstSummary") = "images/ElectronicEst.gif"
                    End If

                    If (UCase(oDocuments.DocumentTypeName) = "ESTIMATE" Or UCase(oDocuments.DocumentTypeName) = "SUPPLEMENT") Then
                        veh.DTDocumentEstimate.Rows.Add(DRDocumentEstimate)
                    End If
                    oDocumentRows.Add(oDocuments)

                End If
            Next

            '-------------------------------
            ' Bind the Data
            '-------------------------------
            If oDocumentRows.Count > 0 Then
                veh.Documents = oDocumentRows
            End If

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLDataParse", "GetVehicleData - Parse XMLNodes for Vehicle/ClaimAspectServiceChannel/Concession: " & Date.Now, "", "")
            End If

            '-----------------------------------
            ' Concessions
            '-----------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("Vehicle/ClaimAspectServiceChannel/Concession")

            veh.DTConcession = New DataTable
            Dim DCConcession As New DataColumn
            Dim DRConcession As DataRow
            Dim dconcessionTotal As Double = 0.0

            veh.DTConcession.Columns.Add(New DataColumn("ClaimAspectServiceChannelConcessionID", GetType(String)))
            veh.DTConcession.Columns.Add(New DataColumn("ConcessionReasonID", GetType(String)))
            veh.DTConcession.Columns.Add(New DataColumn("TypeDescription", GetType(String)))
            veh.DTConcession.Columns.Add(New DataColumn("ReasonDescription", GetType(String)))
            veh.DTConcession.Columns.Add(New DataColumn("Amount", GetType(String)))
            veh.DTConcession.Columns.Add(New DataColumn("CreatedDate", GetType(String)))
            veh.DTConcession.Columns.Add(New DataColumn("Comments", GetType(String)))

            dconcessionTotal = 0
            If (oXMLNodeList.Count <> 0) Then
                For Each oXMLNode In oXMLNodeList
                    DRConcession = veh.DTConcession.NewRow
                    DRConcession("ClaimAspectServiceChannelConcessionID") = GetXMLNodeattrbuteValue(oXMLNode, "ClaimAspectServiceChannelConcessionID")
                    DRConcession("ConcessionReasonID") = GetXMLNodeattrbuteValue(oXMLNode, "ConcessionReasonID")
                    DRConcession("TypeDescription") = GetXMLNodeattrbuteValue(oXMLNode, "TypeDescription")
                    DRConcession("ReasonDescription") = GetXMLNodeattrbuteValue(oXMLNode, "ReasonDescription")
                    DRConcession("Amount") = GetXMLNodeattrbuteValue(oXMLNode, "Amount")

                    If (Not IsNothing(GetXMLNodeattrbuteValue(oXMLNode, "CreatedDate"))) Then
                        DRConcession("CreatedDate") = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "CreatedDate")), "MM/dd/yyyy")
                    End If

                    DRConcession("Comments") = GetXMLNodeattrbuteValue(oXMLNode, "Comments")

                    veh.DTConcession.Rows.Add(DRConcession)

                    'updateConcessionTotals
                    If (GetXMLNodeattrbuteValue(oXMLNode, "TypeDescription") = "Indemnity") Then
                        dconcessionTotal = dconcessionTotal + CDbl(GetXMLNodeattrbuteValue(oXMLNode, "Amount"))
                    End If

                Next
            Else
                DRConcession = veh.DTConcession.NewRow
                DRConcession("TypeDescription") = "No Records To Display"
                veh.DTConcession.Rows.Add(DRConcession)
            End If


            veh.ConcessionTotal = dconcessionTotal

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLDataParse", "GetVehicleData - Parse XMLNodes for Vehicle/ClaimAspectServiceChannel: " & Date.Now, "", "")
            End If

            '-----------------------------------------------
            ' New GetClaimAspectData
            '-----------------------------------------------
            oXMLNode = oReturnXML.SelectSingleNode("Vehicle/ClaimAspectServiceChannel")
            sClaimAspectServiceChannelID = GetXMLNodeattrbuteValue(oXMLNode, "ClaimAspectServiceChannelID")

            veh.ShopAssignmentData = GetVehicleAssignmentData(CInt(sClaimAspectServiceChannelID), CInt(sInscCompany))

            ' GetRepairReasonCode()

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDNET-ClaimXP", "ERROR", "Vehicle data not found or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
            oXMLNodeList = Nothing
        End Try
        Return veh
    End Function

    Private Sub GetVehicleWarrantyData(ByVal iClaimAspectID As Integer, ByRef veh As Vehicle)

        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing

        Dim oWarrantylst As New List(Of Warranty)
        Dim oWarranty As Warranty = Nothing

        '-------------------------------
        ' Database access
        '-------------------------------  
        Dim sStoredProcedure As String = "uspWarrantyAssignmentGetDetailWSXML"
        Dim sParams As String = iClaimAspectID

        Try

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            oXMLNodeList = oReturnXML.SelectNodes("Assignment")
            For Each oXMLNode In oXMLNodeList
                oWarranty = New Warranty()

                oWarranty.AssignmentID = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentID")
                oWarranty.ShopName = GetXMLNodeattrbuteValue(oXMLNode, "ShopName")
                oWarranty.ShopContact = GetXMLNodeattrbuteValue(oXMLNode, "ShopContact")
                oWarranty.ShopPhone = GetXMLNodeattrbuteValue(oXMLNode, "ShopPhone")
                oWarranty.ShopFax = GetXMLNodeattrbuteValue(oXMLNode, "ShopFax")
                oWarranty.AssignmentStatus = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentStatus")
                oWarranty.AssignmentDate = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentDate")
                oWarranty.Remarks = GetXMLNodeattrbuteValue(oXMLNode, "Remarks")
                oWarranty.WarrantyStartDate = GetXMLNodeattrbuteValue(oXMLNode, "WarrantyStartDate")
                oWarranty.WarrantyStartConfirmFlag = GetXMLNodeattrbuteValue(oXMLNode, "WarrantyStartConfirmFlag")
                oWarranty.WarrantyEndDate = GetXMLNodeattrbuteValue(oXMLNode, "WarrantyEndDate")
                oWarranty.WarrantyEndConfirmFlag = GetXMLNodeattrbuteValue(oXMLNode, "WarrantyEndConfirmFlag")

                'massageAssignmentData logic

                If oWarranty.AssignmentStatus IsNot Nothing Then
                    Dim strstatus As String() = oWarranty.AssignmentStatus.Split(";")
                    oWarranty.FaxStatus = strstatus(0).Substring(2)
                    oWarranty.ElecStatus = strstatus(1).Substring(2)
                    oWarranty.Disposition = strstatus(2).Substring(2)
                End If

                If oWarranty.FaxStatus = "Not Sent" Or oWarranty.FaxStatus = "" Then
                    oWarranty.workstatus = "Selected"
                End If

                If oWarranty.FaxStatus = "Sent" Then
                    oWarranty.workstatus = "Active"
                End If

                If oWarranty.FaxStatus = "Sent" And oWarranty.WarrantyStartDate IsNot Nothing Then
                    oWarranty.workstatus = "Active"
                End If

                If oWarranty.FaxStatus = "Sent" And oWarranty.WarrantyStartConfirmFlag = "1" Then
                    oWarranty.workstatus = "In Progress"
                End If

                If oWarranty.FaxStatus = "Sent" And oWarranty.WarrantyEndConfirmFlag = "1" Then
                    oWarranty.workstatus = "Complete"
                End If

                If oWarranty.FaxStatus <> "" Then
                    oWarranty.FaxStatus = "Not Sent"
                End If

                veh.Warrantylst.Add(oWarranty)
            Next

            '-----------------------------------
            ' Estimate Summary
            '-----------------------------------
            veh.WarrantyDocuments = New List(Of Documents)
            veh.DTWarDocumentEstimate = New DataTable


            veh.DTWarDocumentEstimate.Columns.Add(New DataColumn("EstRecvDate", GetType(String)))
            veh.DTWarDocumentEstimate.Columns.Add(New DataColumn("EstSummary", GetType(String)))
            veh.DTWarDocumentEstimate.Columns.Add(New DataColumn("EstSeq", GetType(String)))
            veh.DTWarDocumentEstimate.Columns.Add(New DataColumn("EstSource", GetType(String)))
            veh.DTWarDocumentEstimate.Columns.Add(New DataColumn("EstGross", GetType(String)))
            veh.DTWarDocumentEstimate.Columns.Add(New DataColumn("EstNet", GetType(String)))
            veh.DTWarDocumentEstimate.Columns.Add(New DataColumn("EstOrgAud", GetType(String)))
            veh.DTWarDocumentEstimate.Columns.Add(New DataColumn("EstDup", GetType(String)))
            veh.DTWarDocumentEstimate.Columns.Add(New DataColumn("EstAgreed", GetType(String)))
            veh.DTWarDocumentEstimate.Columns.Add(New DataColumn("DocumentID", GetType(String)))
            veh.DTWarDocumentEstimate.Columns.Add(New DataColumn("FullSummaryExistsFlag", GetType(String)))
            veh.DTWarDocumentEstimate.Columns.Add(New DataColumn("ImageLocation", GetType(String)))

            Dim sDocumentLocation As String = ""
            Dim oXMLDocumentNodeList As XmlNodeList = Nothing


            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLDataParse", "GetVehicleData - Parse XMLNodes for Vehicle/Document: " & Date.Now, "", "")
            End If

            '----------------------------------
            ' Test and see if document exist
            '----------------------------------
            oXMLDocumentNodeList = oReturnXML.SelectNodes("Document")
            Dim oXMLDocumentNode As XmlNode

            For Each oXMLDocumentNode In oXMLDocumentNodeList
                If oXMLDocumentNode.Attributes("DocumentID").Value <> 0 Then
                    'Dim DRDocumentDocuments As DataRow = veh.DTDocumentDocuments.NewRow
                    'DRDocumentDocuments("ImageLocationRaw") = "Some Data" 'oXMLNode.Attributes("ServiceChannelName").InnerText

                    '----------------------------
                    ' Document Details
                    '----------------------------
                    Dim oDocuments As New Documents

                    oDocuments.DocumentID = GetXMLNodeattrbuteValue(oXMLDocumentNode, "DocumentID")
                    oDocuments.DocumentSourceName = GetXMLNodeattrbuteValue(oXMLDocumentNode, "DocumentSourceName")
                    oDocuments.DocumentTypeName = GetXMLNodeattrbuteValue(oXMLDocumentNode, "DocumentTypeName")
                    oDocuments.CreatedDate = GetXMLNodeattrbuteValue(oXMLDocumentNode, "CreatedDate")

                    oDocuments.ReceivedDate = Format(CDate(GetXMLNodeattrbuteValue(oXMLDocumentNode, "ReceivedDate")), "MM/dd/yyyy hh: mm:ss")

                    '----------------------------
                    ' Document Image Details
                    '----------------------------
                    sDocumentLocation = GetXMLNodeattrbuteValue(oXMLDocumentNode, "ImageLocation")
                    oDocuments.ImageLocationRaw = AppSettings("ImagePath").Replace("\", "\\") & sDocumentLocation
                    oDocuments.ImageLocation = AppSettings("ImagePath") & sDocumentLocation.Replace("\\", "\")
                    oDocuments.ImageLocationThumbnail = AppSettings("ImageThumbnailPath") & sDocumentLocation.Replace("\\", "\")

                    Select Case UCase(Right(oDocuments.ImageLocation, 3))
                        Case "JPG", "GIF", "PNG"
                            oDocuments.ImageViewable = 1
                            oDocuments.ImageLocationThumbnail = "Thumbnail.asp?Doc=" & oDocuments.ImageLocationThumbnail
                        Case "TIF"
                            oDocuments.ImageViewable = 1
                            oDocuments.ImageLocationThumbnail = "images/tif.png"
                        Case "PDF"
                            oDocuments.ImageViewable = 0
                            oDocuments.ImageLocationThumbnail = "images/PDFDefault.png"
                        Case "DOC"
                            oDocuments.ImageViewable = 0
                            oDocuments.ImageLocationThumbnail = "images/WordDoc.png"
                        Case Else
                            oDocuments.ImageViewable = 0
                            oDocuments.ImageLocationThumbnail = "images/blank.png"
                    End Select

                    If oDocuments.ImageLocation <> AppSettings("ImagePath") Then
                        Dim fs As New System.IO.FileInfo(oDocuments.ImageLocation)
                        If fs.Exists Then
                            oDocuments.ImageSize = Convert.ToInt32(fs.Length / 1000)
                            oDocuments.ImageInfo = oDocuments.DocumentTypeName & Chr(10) & oDocuments.ReceivedDate & Chr(10) & oDocuments.ImageSize & " kb"
                            If oDocuments.DocumentTypeName.Length >= 16 Then
                                oDocuments.ImageInfolbl = oDocuments.DocumentTypeName.Substring(0, 13) + "..." & Chr(10) & oDocuments.ReceivedDate & "<br/>" & Chr(13) + Chr(10) & oDocuments.ImageSize & " kb"
                            Else
                                oDocuments.ImageInfolbl = oDocuments.DocumentTypeName & Chr(10) & oDocuments.ReceivedDate & "<br/>" & Chr(13) + Chr(10) & oDocuments.ImageSize & " kb"
                            End If
                        Else
                            oDocuments.ImageViewable = 0
                            oDocuments.ImageInfo = sDocumentLocation.Replace("\\", "\")
                            oDocuments.ImageLocationThumbnail = "images/NoPhoto.png"
                        End If
                    End If

                    '----------------------------
                    ' Document Estimate Details
                    '----------------------------
                    'oDocuments.VanFlag = oXMLDocumentNode.Attributes("VanFlag").value
                    oDocuments.ApprovedFlag = GetXMLNodeattrbuteValue(oXMLDocumentNode, "ApprovedFlag")

                    If (oDocuments.ApprovedFlag = "") Then
                        oDocuments.ApprovedFlag = "0"
                    End If

                    oDocuments.SupplementSeqNumber = GetXMLNodeattrbuteValue(oXMLDocumentNode, "SupplementSeqNumber")
                    oDocuments.FullSummaryExistsFlag = GetXMLNodeattrbuteValue(oXMLDocumentNode, "FullSummaryExistsFlag")
                    oDocuments.AgreedPriceMetCD = GetXMLNodeattrbuteValue(oXMLDocumentNode, "AgreedPriceMetCD")
                    oDocuments.GrossEstimateAmt = GetXMLNodeattrbuteValue(oXMLDocumentNode, "GrossEstimateAmt")
                    oDocuments.NetEstimateAmt = GetXMLNodeattrbuteValue(oXMLDocumentNode, "NetEstimateAmt")
                    oDocuments.EstimateTypeFlag = GetXMLNodeattrbuteValue(oXMLDocumentNode, "EstimateTypeFlag")
                    oDocuments.DirectionToPayFlag = GetXMLNodeattrbuteValue(oXMLDocumentNode, "DirectionToPayFlag")
                    oDocuments.FinalEstimateFlag = GetXMLNodeattrbuteValue(oXMLDocumentNode, "FinalEstimateFlag")
                    oDocuments.DuplicateFlag = GetXMLNodeattrbuteValue(oXMLDocumentNode, "DuplicateFlag")
                    oDocuments.EstimateTypeCD = GetXMLNodeattrbuteValue(oXMLDocumentNode, "EstimateTypeCD")
                    oDocuments.ServiceChannelCD = GetXMLNodeattrbuteValue(oXMLDocumentNode, "ServiceChannelCD")
                    oDocuments.SysLastUpdatedDateDocument = GetXMLNodeattrbuteValue(oXMLDocumentNode, "SysLastUpdatedDateDocument")
                    oDocuments.SysLastUpdatedDateEstimate = GetXMLNodeattrbuteValue(oXMLDocumentNode, "SysLastUpdatedDateEstimate")

                    '---------------------------------
                    ' Debug Data
                    '---------------------------------
                    If UCase(sAPDNetDebugging) = "TRUE" Then
                        wsAPDFoundation.LogEvent("APDNET-ClaimXmP", "XMLDataParse", "DEBUG: " & Date.Now, "After 1", "")
                    End If

                    Dim DRDocumentEstimate As DataRow = veh.DTWarDocumentEstimate.NewRow
                    DRDocumentEstimate("DocumentID") = oDocuments.DocumentID
                    DRDocumentEstimate("FullSummaryExistsFlag") = oDocuments.FullSummaryExistsFlag
                    DRDocumentEstimate("ImageLocation") = oDocuments.ImageLocation

                    DRDocumentEstimate("EstRecvDate") = oDocuments.ReceivedDate
                    DRDocumentEstimate("EstSeq") = oDocuments.SupplementSeqNumber + 1
                    DRDocumentEstimate("EstSource") = oDocuments.DocumentSourceName
                    DRDocumentEstimate("EstGross") = oDocuments.GrossEstimateAmt
                    DRDocumentEstimate("EstNet") = oDocuments.NetEstimateAmt

                    Select Case oDocuments.EstimateTypeCD
                        Case "O"
                            DRDocumentEstimate("EstOrgAud") = "Original"
                        Case "A"
                            DRDocumentEstimate("EstOrgAud") = "Audited"
                    End Select
                    Select Case oDocuments.DuplicateFlag
                        Case "0"
                            DRDocumentEstimate("EstDup") = "No"
                        Case "1"
                            DRDocumentEstimate("EstDup") = "Yes"
                    End Select
                    Select Case UCase(oDocuments.AgreedPriceMetCD)
                        Case "N"
                            DRDocumentEstimate("EstAgreed") = "No"
                        Case "Y"
                            DRDocumentEstimate("EstAgreed") = "Yes"
                    End Select

                    '---------------------------------------
                    ' Estimate/Supplement add to Grid
                    '---------------------------------------
                    Select Case UCase(oDocuments.DocumentTypeName)
                        Case "ESTIMATE"
                            DRDocumentEstimate("EstSeq") = "E0" & DRDocumentEstimate("EstSeq")
                        Case "SUPPLEMENT"
                            DRDocumentEstimate("EstSeq") = "S0" & DRDocumentEstimate("EstSeq")
                    End Select

                    If oDocuments.FullSummaryExistsFlag = 1 Then
                        DRDocumentEstimate("EstSummary") = "images/ManualEst.gif"
                    Else
                        DRDocumentEstimate("EstSummary") = "images/ElectronicEst.gif"
                    End If

                    If (UCase(oDocuments.DocumentTypeName) = "ESTIMATE" Or UCase(oDocuments.DocumentTypeName) = "SUPPLEMENT") Then
                        veh.DTWarDocumentEstimate.Rows.Add(DRDocumentEstimate)
                    End If
                    veh.WarrantyDocuments.Add(oDocuments)

                End If
            Next


        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDNET-ClaimXP", "ERROR", "Vehicle data not found or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
            oXMLNodeList = Nothing
        End Try

    End Sub

    'Protected Function GetXMLReferenceData(ByVal sList As String, ByVal sReference As String, ByVal sName As String) As String
    '    Try
    '        'Return strRetVal
    '    Catch oExcept As Exception
    '        '---------------------------------
    '        ' Error handler and notifications
    '        '---------------------------------
    '        Dim sError As String = ""
    '        Dim sBody As String = ""
    '        Dim FunctionName As New System.Diagnostics.StackFrame

    '        sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
    '        wsAPDFoundation.LogEvent("APDNET-ClaimXP", "ERROR", "Vehicle data not found or data error occured.", oExcept.Message, sError)

    '        'Response.Write(oExcept.ToString)
    '    End Try
    'End Function

    ''' <summary>
    ''' To check and get the xmlnode attribute text value
    ''' </summary>
    ''' <param name="XMLnode"></param>
    ''' <param name="strAttributes"></param>
    ''' <returns></returns>
    Protected Function GetXMLNodeattrbuteValue(ByVal XMLnode As XmlNode, strAttributes As String) As String
        Try
            Dim strRetVal As String = ""
            If Not XMLnode Is Nothing Then
                If Not XMLnode.Attributes(strAttributes) Is Nothing Then
                    strRetVal = XMLnode.Attributes(strAttributes).InnerText
                End If
            End If

            Return strRetVal
        Catch ex As Exception
            wsAPDFoundation.LogEvent("APDNET-ClaimXP", "XMLERROR", "GetXMLNodeattrbuteValue - Parse: " & Date.Now, "ERROR: Parsing XMLNode Attribute.", "XMLNodeAttribute = " & strAttributes)
            Return ex.ToString
        End Try
    End Function
    ''' <summary>
    ''' Get Vehicle Involved data
    ''' </summary>
    ''' <param name="iVehicleInvolvedID">InvolvedID</param>
    ''' <returns>VehicleInvolved Object</returns>
    ''' <remarks></remarks>
    Protected Function GetVehicleInvolvedData(ByVal iVehicleInvolvedID As Integer) As VehicleInvolved

        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing

        Dim oVehicleInvolved As VehicleInvolved = Nothing
        Dim oInvolvedType As InvolvedType = Nothing
        Dim oReferenceType As ReferenceType = Nothing
        Dim iBusinessType As Integer = 0

        '-------------------------------
        ' Database access
        '-------------------------------  
        Dim sStoredProcedure As String = "uspVehicleInvolvedGetDetailwsXML"
        Dim sParams As String = iVehicleInvolvedID & "," & sInscCompany


        Try
            oVehicleInvolved = New VehicleInvolved()

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '------------------------------------
            ' Load Root level claim data - Claim
            '------------------------------------
            oXMLNode = oReturnXML.SelectSingleNode("Involved")

            oVehicleInvolved.InvolvedNameTitle = GetXMLNodeattrbuteValue(oXMLNode, "NameTitle")
            oVehicleInvolved.InvolvedNameFirst = GetXMLNodeattrbuteValue(oXMLNode, "NameFirst")
            oVehicleInvolved.InvolvedNameLast = GetXMLNodeattrbuteValue(oXMLNode, "NameLast")
            oVehicleInvolved.InvolvedBusinessName = GetXMLNodeattrbuteValue(oXMLNode, "BusinessName")
            oVehicleInvolved.InvolvedAddress1 = GetXMLNodeattrbuteValue(oXMLNode, "Address1")
            oVehicleInvolved.InvolvedAddress2 = GetXMLNodeattrbuteValue(oXMLNode, "Address2")
            oVehicleInvolved.InvolvedCity = GetXMLNodeattrbuteValue(oXMLNode, "AddressCity")
            oVehicleInvolved.InvolvedState = GetXMLNodeattrbuteValue(oXMLNode, "AddressState")
            oVehicleInvolved.InvolvedZip = GetXMLNodeattrbuteValue(oXMLNode, "AddressZip")
            oVehicleInvolved.InvolvedEmailAddress = GetXMLNodeattrbuteValue(oXMLNode, "EmailAddress")
            oVehicleInvolved.InvolvedGenderCD = GetXMLNodeattrbuteValue(oXMLNode, "GenderCD")
            oVehicleInvolved.InvolvedFedTaxId = GetXMLNodeattrbuteValue(oXMLNode, "FedTaxId")
            oVehicleInvolved.InvolvedBusinessTypeCD = GetXMLNodeattrbuteValue(oXMLNode, "BusinessTypeCD")
            oVehicleInvolved.InvolvedBestTimeToCall = GetXMLNodeattrbuteValue(oXMLNode, "BestContactTime")

            If GetXMLNodeattrbuteValue(oXMLNode, "BirthDate") <> "" Then
                oVehicleInvolved.InvolvedBirthDate = Format(CDate(GetXMLNodeattrbuteValue(oXMLNode, "BirthDate")), "MM/dd/yyyy")
            End If

            oVehicleInvolved.InvolvedAge = GetXMLNodeattrbuteValue(oXMLNode, "Age")

            oVehicleInvolved.InvolvedBestContactPhoneCD = GetXMLNodeattrbuteValue(oXMLNode, "BestContactPhoneCD")
            If GetXMLNodeattrbuteValue(oXMLNode, "DayAreaCode").Trim() <> "" And GetXMLNodeattrbuteValue(oXMLNode, "DayExchangeNumber").Trim() <> "" And GetXMLNodeattrbuteValue(oXMLNode, "DayUnitNumber").Trim() <> "" Then
                oVehicleInvolved.InvolvedPhoneDay = "(" & GetXMLNodeattrbuteValue(oXMLNode, "DayAreaCode") & ") " & GetXMLNodeattrbuteValue(oXMLNode, "DayExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "DayUnitNumber")
            End If
            If GetXMLNodeattrbuteValue(oXMLNode, "NightAreaCode").Trim() <> "" And GetXMLNodeattrbuteValue(oXMLNode, "NightExchangeNumber").Trim() <> "" And GetXMLNodeattrbuteValue(oXMLNode, "NightUnitNumber").Trim() <> "" Then
                oVehicleInvolved.InvolvedPhoneNight = "(" & GetXMLNodeattrbuteValue(oXMLNode, "NightAreaCode") & ") " & GetXMLNodeattrbuteValue(oXMLNode, "NightExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "NightUnitNumber")
            End If
            If GetXMLNodeattrbuteValue(oXMLNode, "AlternateAreaCode").Trim() <> "" And GetXMLNodeattrbuteValue(oXMLNode, "AlternateExchangeNumber").Trim() <> "" And GetXMLNodeattrbuteValue(oXMLNode, "AlternateUnitNumber").Trim() <> "" Then
                oVehicleInvolved.InvolvedPhoneAlt = "(" & GetXMLNodeattrbuteValue(oXMLNode, "AlternateAreaCode") & ") " & GetXMLNodeattrbuteValue(oXMLNode, "AlternateExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "AlternateUnitNumber")
            End If
            oVehicleInvolved.InvolvedSysLastUpdatedDate = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate")


            ''Get Reference data 
            oVehicleInvolved.InvolvedBusinessType = New List(Of ReferenceType)
            oVehicleInvolved.InvolvedGender = New List(Of ReferenceType)
            oVehicleInvolved.InvolvedBestContactPhone = New List(Of ReferenceType)

            oXMLNodeList = oReturnXML.SelectNodes("Reference")
            For Each oXMLNode In oXMLNodeList

                ''Add business type collection 
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "BusinessType" Then
                    If iBusinessType = 0 Then
                        oReferenceType = New ReferenceType()
                        oReferenceType.ReferenceID = ""
                        oReferenceType.Name = ""
                        oVehicleInvolved.InvolvedBusinessType.Add(oReferenceType)
                        iBusinessType = 1
                    End If
                    oReferenceType = New ReferenceType()
                    oReferenceType.ReferenceID = GetXMLNodeattrbuteValue(oXMLNode, "Name")
                    oReferenceType.Name = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")
                    oVehicleInvolved.InvolvedBusinessType.Add(oReferenceType)
                End If

                ''Add Gender collection 
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "GenderCD" Then
                    oReferenceType = New ReferenceType()
                    oReferenceType.ReferenceID = GetXMLNodeattrbuteValue(oXMLNode, "Name")
                    oReferenceType.Name = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")

                    oVehicleInvolved.InvolvedGender.Add(oReferenceType)
                End If

                ''Add Best Number to call collection 
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "BestContactPhoneCD" Then
                    oReferenceType = New ReferenceType()
                    oReferenceType.ReferenceID = GetXMLNodeattrbuteValue(oXMLNode, "Name")
                    oReferenceType.Name = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")

                    oVehicleInvolved.InvolvedBestContactPhone.Add(oReferenceType)
                End If
            Next

            ''Add InvolvedType collection 
            oVehicleInvolved.InvolvedTypes = New List(Of InvolvedType)
            oXMLNodeList = oReturnXML.SelectNodes("Involved/InvolvedType")
            For Each oXMLNode In oXMLNodeList
                oInvolvedType = New InvolvedType()
                If (oXMLNode.Attributes.Count > 0) Then
                    oInvolvedType.InvolvedTypeID = GetXMLNodeattrbuteValue(oXMLNode, "InvolvedTypeID")
                    oInvolvedType.InvolvedTypeName = GetXMLNodeattrbuteValue(oXMLNode, "InvolvedTypeName")

                    oVehicleInvolved.InvolvedTypes.Add(oInvolvedType)
                End If
            Next

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDClaim", "ERROR", "Vehicle Involved data not found or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
        End Try

        Return oVehicleInvolved

    End Function

    Protected Function GetVehicleAssignmentData(ByVal iClaimAspectServiceChannelID As Integer, iInsuranceCompanyID As Integer) As ShopAssignment
        '-------------------------------
        ' Session/Local Variables
        '-------------------------------  
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim oVehicleShopAssignment As ShopAssignment = Nothing
        Dim objReferenceDisposition As ReferenceDisposition = Nothing
        Dim objReferenceStateList As ReferenceStateList = Nothing
        Dim sServiceChannel As String = ""
        Dim hCommunicationMethodProfile As New Hashtable()
        Dim hEstimatePackageProfile As New Hashtable()
        Dim oCRUDXML As XmlElement = Nothing
        Dim sShopLocationID As String = ""

        '-------------------------------
        ' Database access
        '-------------------------------  
        Dim sStoredProcedure As String = "uspVehicleAssignGetDetailwsXML"
        Dim sParams As String = iClaimAspectServiceChannelID & "," & iInsuranceCompanyID

        Try
            oVehicleShopAssignment = New ShopAssignment()

            '-------------------------------
            ' Get CRUD Data
            '-------------------------------
            'sCRUD = GetCRUD("Reference", sUserID)
            oCRUDXML = wsAPDFoundation.GetCRUD("Reference", sUserID)

            If oCRUDXML.InnerXml.Contains("ERROR") Then
                Throw New SystemException("CRUDError: Could not get the CRUD for entity: Reference=" & "Reference, UserID=" & sUserID)
            End If

            oXMLNode = oCRUDXML.SelectSingleNode("Crud")
            sCRUD = GetXMLNodeattrbuteValue(oXMLNode, "Insert") & GetXMLNodeattrbuteValue(oXMLNode, "Select") & GetXMLNodeattrbuteValue(oXMLNode, "Update") & GetXMLNodeattrbuteValue(oXMLNode, "Delete")

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '---------------------------------------
            ' Load Vehicle data - Root
            '---------------------------------------
            oXMLNode = oReturnXML.SelectSingleNode("/")
            oVehicleShopAssignment.LDAUID = GetXMLNodeattrbuteValue(oXMLNode, "LDAUID")
            oVehicleShopAssignment.ClaimAspectServiceChannelID = CStr(iClaimAspectServiceChannelID)
            oVehicleShopAssignment.InsuranceCompanyID = CStr(iInsuranceCompanyID)
            oVehicleShopAssignment.ShopLocationCity = GetXMLNodeattrbuteValue(oXMLNode, "RepairLocationCity")
            oVehicleShopAssignment.ShopLocationCounty = GetXMLNodeattrbuteValue(oXMLNode, "RepairLocationCounty")
            oVehicleShopAssignment.ShopLocationState = GetXMLNodeattrbuteValue(oXMLNode, "RepairLocationState")
            oVehicleShopAssignment.InsuranceCompanyName = GetXMLNodeattrbuteValue(oXMLNode, "InsuranceCompanyName")
            oVehicleShopAssignment.RepairLocationCity = GetXMLNodeattrbuteValue(oXMLNode, "RepairLocationCity")
            oVehicleShopAssignment.CurrentStateList = GetXMLNodeattrbuteValue(oXMLNode, "RepairLocationState")

            '---------------------------------------
            ' Load Vehicle data - ServiceChannel
            '---------------------------------------
            oXMLNode = oReturnXML.SelectSingleNode("ServiceChannel")
            oVehicleShopAssignment.CurEstGrossRepairTotal = GetXMLNodeattrbuteValue(oXMLNode, "CurEstGrossRepairTotal")
            oVehicleShopAssignment.CurEstDeductiblesApplied = GetXMLNodeattrbuteValue(oXMLNode, "CurEstDeductiblesApplied")
            oVehicleShopAssignment.CurEstNetRepairTotal = GetXMLNodeattrbuteValue(oXMLNode, "CurEstNetRepairTotal")
            oVehicleShopAssignment.CurEstLimitsEffect = GetXMLNodeattrbuteValue(oXMLNode, "CurEstLimitsEffect")
            oVehicleShopAssignment.ClaimAspectServiceChannelID = GetXMLNodeattrbuteValue(oXMLNode, "ClaimAspectServiceChannelID")
            oVehicleShopAssignment.JobID = GetXMLNodeattrbuteValue(oXMLNode, "JobID")
            oVehicleShopAssignment.JobStatus = GetXMLNodeattrbuteValue(oXMLNode, "JobStatus")



            oVehicleShopAssignment.ServiceChannelCD = GetXMLNodeattrbuteValue(oXMLNode, "ServiceChannelCD") 'oXMLNode.Attributes("ServiceChannelCD").InnerText
            'hidClaimServiceChannelCD.Value = oVehicleShopAssignment.ServiceChannelCD

            oVehicleShopAssignment.WorkStartConfirmFlag = "display:none"
            oVehicleShopAssignment.WorkEndConfirmFlag = "display:none"
            If oXMLNode.Attributes("WorkStartDate").Value <> "" Then
                oVehicleShopAssignment.WorkStartDate = Format(CDate(oXMLNode.Attributes("WorkStartDate").Value), "MM/dd/yyyy")
                If DateTime.Compare(CDate(oXMLNode.Attributes("WorkStartDate").Value), DateTime.Now) > 0 Or GetXMLNodeattrbuteValue(oXMLNode, "WorkStartConfirmFlag") = 1 Then
                    oVehicleShopAssignment.WorkStartConfirmFlag = "display:none"
                Else
                    oVehicleShopAssignment.WorkStartConfirmFlag = "display:inline"
                End If
            End If
            oVehicleShopAssignment.WorkStartDateConfirmFlag = GetXMLNodeattrbuteValue(oXMLNode, "WorkStartConfirmFlag")

            If oXMLNode.Attributes("WorkEndDate").Value <> "" Then
                oVehicleShopAssignment.WorkEndDate = Format(CDate(oXMLNode.Attributes("WorkEndDate").Value), "MM/dd/yyyy")
                If DateTime.Compare(CDate(oXMLNode.Attributes("WorkEndDate").Value), DateTime.Now) > 0 Or GetXMLNodeattrbuteValue(oXMLNode, "WorkEndConfirmFlag") = 1 Then
                    oVehicleShopAssignment.WorkEndConfirmFlag = "display:none"
                ElseIf oVehicleShopAssignment.WorkStartDateConfirmFlag = "1" Then
                    oVehicleShopAssignment.WorkEndConfirmFlag = "display:inline"
                End If
            End If
            oVehicleShopAssignment.WorkEndDateConfirmFlag = GetXMLNodeattrbuteValue(oXMLNode, "WorkEndConfirmFlag")


            If (oXMLNode.Attributes("AppraiserInvoiceDate").Value <> "") Then
                oVehicleShopAssignment.AppraiserInvoiceDate = Format(CDate(oXMLNode.Attributes("AppraiserInvoiceDate").Value), "MM/dd/yyyy")
            End If

            If oXMLNode.Attributes("CashOutDate").Value <> "" Then
                oVehicleShopAssignment.CashOutDate = Format(CDate(oXMLNode.Attributes("CashOutDate").Value), "MM/dd/yyyy")
            Else
                oVehicleShopAssignment.CashOutDate = ""
            End If

            sServiceChannel = GetXMLNodeattrbuteValue(oXMLNode, "ServiceChannelCD") 'oXMLNode.Attributes("ServiceChannelCD").InnerText
            oVehicleShopAssignment.ServiceChannelCD = sServiceChannel
            oVehicleShopAssignment.CurrentDisposition = GetXMLNodeattrbuteValue(oXMLNode, "DispositionTypeCD") 'oXMLNode.Attributes("DispositionTypeCD").InnerText

            '-------------------------------
            ' Load Reference data
            '-------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("Reference")

            oVehicleShopAssignment.DispositionCollection = New List(Of ReferenceDisposition)
            objReferenceDisposition = New ReferenceDisposition()
            objReferenceDisposition.ReferenceID = "0"
            objReferenceDisposition.DispositionName = " "
            oVehicleShopAssignment.DispositionCollection.Add(objReferenceDisposition)

            oVehicleShopAssignment.StateListCollection = New List(Of ReferenceStateList)
            objReferenceStateList = New ReferenceStateList()
            objReferenceStateList.ReferenceID = ""
            objReferenceStateList.StateListName = " "
            oVehicleShopAssignment.StateListCollection.Add(objReferenceStateList)


            hCommunicationMethodProfile.Clear()
            hEstimatePackageProfile.Clear()

            For Each oXMLNode In oXMLNodeList
                '---------------------------------------
                ' Load Reference data - Disposition
                '---------------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "DispositionCD" Then
                    objReferenceDisposition = New ReferenceDisposition()
                    objReferenceDisposition.ReferenceID = UCase(GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")) 'oXMLNode.Attributes("ReferenceID").InnerText
                    objReferenceDisposition.DispositionName = GetXMLNodeattrbuteValue(oXMLNode, "Name") 'oXMLNode.Attributes("Name").InnerText

                    If UCase(sServiceChannel) = "PS" Then
                        If (objReferenceDisposition.ReferenceID = "RC" Or objReferenceDisposition.ReferenceID = "RNC" Or objReferenceDisposition.ReferenceID = "ST" Or objReferenceDisposition.ReferenceID = "TL" Or objReferenceDisposition.ReferenceID = "VD") Then
                            oVehicleShopAssignment.DispositionCollection.Add(objReferenceDisposition)
                        End If
                    End If

                    If UCase(sServiceChannel) = "DA" Or UCase(sServiceChannel) = "DR" Then
                        If (objReferenceDisposition.ReferenceID = "CA" Or objReferenceDisposition.ReferenceID = "CO" Or objReferenceDisposition.ReferenceID = "IC" Or objReferenceDisposition.ReferenceID = "IPD" Or objReferenceDisposition.ReferenceID = "IME") Then
                            oVehicleShopAssignment.DispositionCollection.Add(objReferenceDisposition)
                        End If
                    End If
                End If

                '---------------------------------------
                ' Load Reference data - StateList
                '---------------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "StateList" Then
                    objReferenceStateList = New ReferenceStateList()
                    objReferenceStateList.ReferenceID = UCase(GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID"))
                    objReferenceStateList.StateListName = GetXMLNodeattrbuteValue(oXMLNode, "Name")

                    If UCase(sServiceChannel) = "DA" Then
                        'If (objReferenceDisposition.ReferenceID = "RC" Or objReferenceDisposition.ReferenceID = "RNC" Or objReferenceDisposition.ReferenceID = "ST" Or objReferenceDisposition.ReferenceID = "TL" Or objReferenceDisposition.ReferenceID = "VD") Then
                        oVehicleShopAssignment.StateListCollection.Add(objReferenceStateList)
                        'End If
                    End If
                End If

                '--------------------------------------------
                ' Load Reference data - CommunicationMethod
                '--------------------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "CommunicationMethod" Then
                    hCommunicationMethodProfile.Add(GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID"), GetXMLNodeattrbuteValue(oXMLNode, "Name"))
                End If

                '--------------------------------------------
                ' Load Reference data - EstimatePackage
                '--------------------------------------------
                If GetXMLNodeattrbuteValue(oXMLNode, "List") = "EstimatePackage" Then
                    hEstimatePackageProfile.Add(GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID"), GetXMLNodeattrbuteValue(oXMLNode, "Name"))
                End If
            Next

            '---------------------------------------
            ' Shop Assignment Data 
            '---------------------------------------
            oXMLNode = oReturnXML.SelectSingleNode("ServiceChannel/Assignment")

            '--------------------------------------------
            ' Check if Assignment Exists
            '--------------------------------------------
            If Not (oXMLNode) Is Nothing Then
                oVehicleShopAssignment.AssignmentID = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentID")
                oVehicleShopAssignment.AssignmentElecStatus = GetXMLNodeattrbuteValue(oXMLNode, "VANAssignmentStatusName")
                oVehicleShopAssignment.AssignmentStatusID = GetXMLNodeattrbuteValue(oXMLNode, "VANAssignmentStatusID")
                oVehicleShopAssignment.AssignmentFaxStatus = GetXMLNodeattrbuteValue(oXMLNode, "FaxAssignmentStatusName")
                oVehicleShopAssignment.AssignmentFaxStatusID = GetXMLNodeattrbuteValue(oXMLNode, "FaxAssignmentStatusID")
                oVehicleShopAssignment.AssignmentWorkStatus = GetXMLNodeattrbuteValue(oXMLNode, "WorkStatus")
                oVehicleShopAssignment.AssignmentDeductSent = GetXMLNodeattrbuteValue(oXMLNode, "EffectiveDeductibleSentAmt")
                oVehicleShopAssignment.AssignmentRemarks = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentRemarks")
                oVehicleShopAssignment.AssignmentTypeCD = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentTypeCD")
                oVehicleShopAssignment.ShopLocationID = GetXMLNodeattrbuteValue(oXMLNode, "ShopLocationID")
                oVehicleShopAssignment.ShopReferenceID = GetXMLNodeattrbuteValue(oXMLNode, "ReferenceID")
                oVehicleShopAssignment.CurrentAssignmentTypeID = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentTypeID")
                oVehicleShopAssignment.AssignmentSequenceNumber = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentSequenceNumber")
                oVehicleShopAssignment.AssignmentSysLastUpdatedDate = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate")

                oXMLNode = oReturnXML.SelectSingleNode("ServiceChannel/Assignment/Shop")
                If oXMLNode IsNot Nothing Then
                    oVehicleShopAssignment.AssignmentAssignTo = GetXMLNodeattrbuteValue(oXMLNode, "Name")
                    oVehicleShopAssignment.AssignmentShopAddress1 = GetXMLNodeattrbuteValue(oXMLNode, "Address1")
                    oVehicleShopAssignment.AssignmentShopAddress2 = GetXMLNodeattrbuteValue(oXMLNode, "Address2")
                    oVehicleShopAssignment.AssignmentShopAddressCity = GetXMLNodeattrbuteValue(oXMLNode, "AddressCity")
                    oVehicleShopAssignment.AssignmentShopAddressState = GetXMLNodeattrbuteValue(oXMLNode, "AddressState")
                    oVehicleShopAssignment.AssignmentShopAddressZip = GetXMLNodeattrbuteValue(oXMLNode, "AddressZip")
                    oVehicleShopAssignment.AssignmentShopMSACode = GetXMLNodeattrbuteValue(oXMLNode, "MSACode")
                    oVehicleShopAssignment.AssignmentShopAddressCounty = GetXMLNodeattrbuteValue(oXMLNode, "AddressCounty")
                    oVehicleShopAssignment.AssignmentShopPhone = GetXMLNodeattrbuteValue(oXMLNode, "PhoneAreaCode") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "PhoneExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "PhoneUnitNumber")
                    oVehicleShopAssignment.AssignmentShopFax = GetXMLNodeattrbuteValue(oXMLNode, "FaxAreaCode") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "FaxExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "FaxUnitNumber")
                    oVehicleShopAssignment.AssignmentShopContactEmail = GetXMLNodeattrbuteValue(oXMLNode, "EmailAddress")
                    oVehicleShopAssignment.AssignmentPreferredCommunicationMethodID = GetXMLNodeattrbuteValue(oXMLNode, "PreferredCommunicationMethodID")
                    oVehicleShopAssignment.AssignmentPreferredEstimatePackageID = GetXMLNodeattrbuteValue(oXMLNode, "PreferredEstimatePackageID")
                    oVehicleShopAssignment.AssignmentPreferredCommunicationMethodName = hCommunicationMethodProfile(oVehicleShopAssignment.AssignmentPreferredCommunicationMethodID)
                    oVehicleShopAssignment.AssignmentPreferredEstimatePackageName = hEstimatePackageProfile(oVehicleShopAssignment.AssignmentPreferredEstimatePackageID)
                    oVehicleShopAssignment.ShopID = GetXMLNodeattrbuteValue(oXMLNode, "ShopID")
                    oVehicleShopAssignment.BusinessID = GetXMLNodeattrbuteValue(oXMLNode, "BusinessID")

                    If (Not IsNothing(oVehicleShopAssignment.AssignmentPrevDate)) Then
                        oVehicleShopAssignment.AssignmentPrevDate = Format(CDate(oXMLNode.Attributes("LastAssignedDate").Value), "MM/dd/yyyy")
                    End If

                    '-----------------------------------------------------
                    ' Load Vehicle Assignment data - Communication Method
                    '-----------------------------------------------------
                    'oVehicleShopAssignment.AssignmentPreferredCommunicationMethodID = oXMLNode.Attributes("PreferredCommunicationMethodID").InnerText
                    'oVehicleShopAssignment.AssignmentPreferredEstimatePackageID = oXMLNode.Attributes("PreferredEstimatePackageID").InnerText

                    '--------------------------------------------
                    ' Load Vehicle Assignment data - ProgramType
                    '--------------------------------------------
                    If Not (oReturnXML.SelectSingleNode("ServiceChannel/Assignment/Shop/ShopLocationID") Is Nothing) Then
                        sShopLocationID = GetXMLNodeattrbuteValue(oXMLNode, "ShopLocationID")
                    Else
                        sShopLocationID = ""
                    End If

                    If UCase(GetXMLNodeattrbuteValue(oXMLNode, "ProgramType")) = "LS" Then
                        oVehicleShopAssignment.AssignmentProgramType = "Yes"
                    ElseIf (UCase(GetXMLNodeattrbuteValue(oXMLNode, "ProgramType")) <> "LS" And sShopLocationID = "") Then
                        oVehicleShopAssignment.AssignmentProgramType = ""
                    ElseIf UCase(GetXMLNodeattrbuteValue(oXMLNode, "ProgramType")) <> "CEI" Then
                        oVehicleShopAssignment.AssignmentProgramType = "CEI"
                    Else
                        oVehicleShopAssignment.AssignmentProgramType = "No"
                    End If

                    '---------------------------------------
                    ' Shop Assignment Data 
                    '---------------------------------------
                    oXMLNode = oReturnXML.SelectSingleNode("ServiceChannel/Assignment/Appraiser")
                    If oXMLNode IsNot Nothing Then
                        oVehicleShopAssignment.AppraiserName = GetXMLNodeattrbuteValue(oXMLNode, "Name")
                    End If

                    '--------------------------------------------
                    ' Load Vehicle Assignment data - ShopContact
                    '--------------------------------------------
                    oXMLNode = oReturnXML.SelectSingleNode("ServiceChannel/Assignment/Shop/ShopContact")
                    oVehicleShopAssignment.AssignmentShopContact = GetXMLNodeattrbuteValue(oXMLNode, "Name") 'oXMLNode.Attributes("Name").InnerText
                    oVehicleShopAssignment.AssignmentShopContactPhone = GetXMLNodeattrbuteValue(oXMLNode, "PhoneAreaCode") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "PhoneExchangeNumber") & "-" & GetXMLNodeattrbuteValue(oXMLNode, "PhoneUnitNumber")
                    'oXMLNode.Attributes("PhoneAreaCode").InnerText & "-" & oXMLNode.Attributes("PhoneExchangeNumber").InnerText & "-" & oXMLNode.Attributes("PhoneUnitNumber").InnerText
                End If
            Else
                '------------------------------------
                ' No Assignment set Vehicle Section
                ' ReadOnly
                '------------------------------------
                bPageReadOnly_Vehicle = True
                oVehicleShopAssignment.AssignmentStatus = "NoShopSelected"

                oVehicleShopAssignment.AssignmentID = ""
                oVehicleShopAssignment.AssignmentElecStatus = ""
                oVehicleShopAssignment.AssignmentStatusID = ""
                oVehicleShopAssignment.AssignmentFaxStatus = ""
                oVehicleShopAssignment.AssignmentFaxStatusID = ""
                oVehicleShopAssignment.AssignmentWorkStatus = ""
                oVehicleShopAssignment.AssignmentDeductSent = ""
                oVehicleShopAssignment.AssignmentRemarks = ""
                oVehicleShopAssignment.ShopLocationID = ""
                oVehicleShopAssignment.ShopReferenceID = ""
                oVehicleShopAssignment.CurrentAssignmentTypeID = ""
                oVehicleShopAssignment.AssignmentSequenceNumber = ""
                oVehicleShopAssignment.AssignmentSysLastUpdatedDate = ""

                oVehicleShopAssignment.AssignmentAssignTo = ""
                oVehicleShopAssignment.AssignmentShopAddress1 = ""
                oVehicleShopAssignment.AssignmentShopAddress2 = ""
                oVehicleShopAssignment.AssignmentShopAddressCity = ""
                oVehicleShopAssignment.AssignmentShopAddressState = ""
                oVehicleShopAssignment.AssignmentShopAddressZip = ""
                oVehicleShopAssignment.AssignmentShopPhone = ""
                oVehicleShopAssignment.AssignmentShopFax = ""
                oVehicleShopAssignment.AssignmentShopMSACode = ""
                oVehicleShopAssignment.AssignmentShopContactEmail = ""
                oVehicleShopAssignment.AssignmentPreferredCommunicationMethodID = ""
                oVehicleShopAssignment.AssignmentPreferredEstimatePackageID = ""

                oVehicleShopAssignment.AssignmentPrevDate = ""
                oVehicleShopAssignment.AssignmentProgramType = ""
                oVehicleShopAssignment.AssignmentShopContact = ""
                oVehicleShopAssignment.AssignmentShopContactPhone = ""

                oVehicleShopAssignment.CurrentDisposition = ""
            End If

            '------------------------------------------------------
            ' Shop Assignment History based on Multiple Assignments
            '------------------------------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("ServiceChannel/Assignments")

            oVehicleShopAssignment.DTAssignmentHistory = New DataTable()

            oVehicleShopAssignment.DTAssignmentHistory.Columns.Add(New DataColumn("AssignmentID", GetType(String)))
            oVehicleShopAssignment.DTAssignmentHistory.Columns.Add(New DataColumn("AssignmentTypeCD", GetType(String)))
            oVehicleShopAssignment.DTAssignmentHistory.Columns.Add(New DataColumn("AppraiserName", GetType(String)))
            oVehicleShopAssignment.DTAssignmentHistory.Columns.Add(New DataColumn("AssignmentDate", GetType(String)))
            oVehicleShopAssignment.DTAssignmentHistory.Columns.Add(New DataColumn("CancellationDate", GetType(String)))
            oVehicleShopAssignment.DTAssignmentHistory.Columns.Add(New DataColumn("SelectionDate", GetType(String)))

            Dim DRAssignmentHistory As DataRow

            If (oXMLNodeList.Count <> 0) Then
                For Each oXMLNode In oXMLNodeList
                    DRAssignmentHistory = oVehicleShopAssignment.DTAssignmentHistory.NewRow

                    DRAssignmentHistory("AssignmentID") = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentID")
                    DRAssignmentHistory("AssignmentTypeCD") = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentTypeCD")
                    DRAssignmentHistory("AppraiserName") = GetXMLNodeattrbuteValue(oXMLNode, "AppraiserName")
                    DRAssignmentHistory("AssignmentDate") = GetXMLNodeattrbuteValue(oXMLNode, "AssignmentDate")
                    DRAssignmentHistory("CancellationDate") = GetXMLNodeattrbuteValue(oXMLNode, "CancellationDate")
                    DRAssignmentHistory("SelectionDate") = GetXMLNodeattrbuteValue(oXMLNode, "SelectionDate")

                    oVehicleShopAssignment.DTAssignmentHistory.Rows.Add(DRAssignmentHistory)
                Next
            Else
                DRAssignmentHistory = oVehicleShopAssignment.DTAssignmentHistory.NewRow
                DRAssignmentHistory("AssignmentDate") = "No Records To Display"
                oVehicleShopAssignment.DTAssignmentHistory.Rows.Add(DRAssignmentHistory)

            End If

            '---------------------------------------------
            ' Load Vehicle Assignment data - Coverage
            '---------------------------------------------
            oXMLNode = oReturnXML.SelectSingleNode("ClaimCoveragesApplied")
            oVehicleShopAssignment.ServiceChannelName = GetXMLNodeattrbuteValue(oXMLNode, "ServiceChannelName")
            'oVehicleShopAssignment.DeductibleAppliedAmt = GetXMLNodeattrbuteValue(oXMLNode, "DeductibleAppliedAmt")

            '---------------------------------------------
            ' Load Vehicle data - CommunicationMethod Key
            '---------------------------------------------
            'For Each sCommunicationMethodKey In hCommunicationMethodProfile
            '    If sCommunicationMethodKey.key = oVehicleShopAssignment.AssignmentPreferredCommunicationMethodID Then
            '        oVehicleShopAssignment.AssignmentPreferredCommunicationMethodID = sCommunicationMethodKey.value
            '    End If
            'Next

            ''----------------------------------------------------
            '' Load Vehicle data - PreferredEstimatePackage Key
            ''----------------------------------------------------
            'For Each sEstimatePackageKey In hEstimatePackageProfile
            '    If sEstimatePackageKey.key = oVehicleShopAssignment.AssignmentPreferredEstimatePackageID Then
            '        oVehicleShopAssignment.AssignmentPreferredEstimatePackageID = sEstimatePackageKey.value
            '    End If
            'Next

            ''-------------------------------
            '' CRUD Permissions 
            ''-------------------------------
            'Select Case sCRUD
            '    Case "0000"
            '        txtPolicyNumber.Enabled = False
            '        txtClientClaimNumber.Enabled = False
            '        txtLossDate.Enabled = False
            '        ddlState.Enabled = False
            '        txtLossDescription.Enabled = False
            '        txtSubmittedBy.Enabled = False
            '        txtIntakeFinishDate.Enabled = False
            '        txtRemarks.Enabled = False
            '    Case "1111"
            '        txtPolicyNumber.Enabled = True
            '        txtClientClaimNumber.Enabled = True
            '        txtLossDate.Enabled = False
            '        ddlState.Enabled = True
            '        txtLossDescription.Enabled = True
            '        txtSubmittedBy.Enabled = False
            '        txtIntakeFinishDate.Enabled = False
            '        txtRemarks.Enabled = True
            '    Case Else
            '        txtPolicyNumber.Enabled = False
            '        txtClientClaimNumber.Enabled = False
            '        txtLossDate.Enabled = False
            '        ddlState.Enabled = False
            '        txtLossDescription.Enabled = False
            '        txtSubmittedBy.Enabled = False
            '        txtIntakeFinishDate.Enabled = False
            '        txtRemarks.Enabled = False
            'End Select

            Return oVehicleShopAssignment

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("APDShopAssignment", "ERROR", "Shop Assignment data Not found Or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
            oXMLNodeList = Nothing
        End Try
    End Function

    Protected Sub RepeaterVehdetTab_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim ddlLicensePlateState As DropDownList = CType(e.Item.FindControl("ddlLicensePlateState"), DropDownList)
            Dim ddlLocationState As DropDownList = CType(e.Item.FindControl("ddlLocationState"), DropDownList)
            Dim ddlContactState As DropDownList = CType(e.Item.FindControl("ddlContactState"), DropDownList)
            Dim ddlContactInsuredRelation As DropDownList = CType(e.Item.FindControl("ddlContactInsuredRelation"), DropDownList)
            Dim ddlBestContactTabPhoneCD As DropDownList = CType(e.Item.FindControl("ddlBestContactTabPhoneCD"), DropDownList)
            Dim gvServiceChannel As GridView = CType(e.Item.FindControl("gvServiceChannel"), GridView)
            Dim gvWarAssignment As GridView = CType(e.Item.FindControl("gvWarAssignment"), GridView)
            Dim gvEstimate As GridView = CType(e.Item.FindControl("gvEstimate"), GridView)
            Dim gvConcessions As GridView = CType(e.Item.FindControl("gvConcessions"), GridView)
            Dim repDocuments As Repeater = CType(e.Item.FindControl("repDocuments"), Repeater)
            Dim gvAssignmentCoverage As GridView = CType(e.Item.FindControl("gvAssignmentCoverage"), GridView)
            Dim ddlDisposition As DropDownList = CType(e.Item.FindControl("ddlDisposition"), DropDownList)

            Dim ddlInvolvedstate As DropDownList = CType(e.Item.FindControl("ddlInvolvedstate"), DropDownList)
            Dim ddlInvolvedBusinessType As DropDownList = CType(e.Item.FindControl("ddlInvolvedBusinessType"), DropDownList)
            Dim ddlInvolvedGender As DropDownList = CType(e.Item.FindControl("ddlInvolvedGender"), DropDownList)
            Dim ddlBestNumberCall As DropDownList = CType(e.Item.FindControl("ddlBestNumberCall"), DropDownList)
            Dim ddlInvolved As DropDownList = CType(e.Item.FindControl("ddlInvolved"), DropDownList)
            ''Mahes added for Checkbox
            Dim oInvolvedType As InvolvedType = Nothing
            Dim cbInsured As CheckBox = CType(e.Item.FindControl("cbInsured"), CheckBox)
            Dim cbClaimant As CheckBox = CType(e.Item.FindControl("cbClaimant"), CheckBox)
            Dim cbOwner As CheckBox = CType(e.Item.FindControl("cbOwner"), CheckBox)
            Dim cbDriver As CheckBox = CType(e.Item.FindControl("cbDriver"), CheckBox)
            Dim cbPassenger As CheckBox = CType(e.Item.FindControl("cbPassenger"), CheckBox)

            Dim txtParty As Label = CType(e.Item.FindControl("txtParty"), Label)
            Dim chkDriveableFlag As CheckBox = CType(e.Item.FindControl("chkDriveableFlag"), CheckBox)

            Dim btnSelectShop As Button = CType(e.Item.FindControl("btnSelectShop"), Button)
            Dim btnSendAssignment As Button = CType(e.Item.FindControl("btnSendAssignment"), Button)
            Dim btnResendAssignment As Button = CType(e.Item.FindControl("btnResendAssignment"), Button)
            Dim btnCancelAssignment As Button = CType(e.Item.FindControl("btnCancelAssignment"), Button)

            Dim imgVehAssignmentStatus As WebControls.Image = CType(e.Item.FindControl("imgVehAssignmentStatus"), WebControls.Image)
            Dim gvAssignmentHistory As GridView = CType(e.Item.FindControl("gvAssignmentHistory"), GridView)
            Dim lblConcessionTotal As Label = CType(e.Item.FindControl("lblConcessionTotal"), Label)

            Dim FAXAssignmentStatusName As TextBox = CType(e.Item.FindControl("FAXAssignmentStatusName"), TextBox)

            Dim imgbtnVINEdit As ImageButton = CType(e.Item.FindControl("imgbtnVINEdit"), ImageButton)

            '-------------------------------
            ' PS Fields
            '-------------------------------
            Dim txtWorkStartDate As TextBox = CType(e.Item.FindControl("txtWorkStartDate"), TextBox)
            Dim txtWorkEndDate As TextBox = CType(e.Item.FindControl("txtWorkEndDate"), TextBox)
            'Dim btnRepairStart As ImageButton = CType(e.Item.FindControl("btnRepairStart"), ImageButton)
            'Dim btnRepairEnd As ImageButton = CType(e.Item.FindControl("btnRepairEnd"), ImageButton)

            '---------------------------------
            ' Vehicle Warranty section 
            '---------------------------------
            Dim gvWarEstimate As GridView = CType(e.Item.FindControl("gvWarEstimate"), GridView)
            Dim repWarDocuments As Repeater = CType(e.Item.FindControl("repWarDocuments"), Repeater)

            Dim txtInspectionDate As TextBox = CType(e.Item.FindControl("txtInspectionDate"), TextBox)
            Dim txtOriginalEstimateDate As TextBox = CType(e.Item.FindControl("txtOriginalEstimateDate"), TextBox)
            Dim txtFinalEstDate As TextBox = CType(e.Item.FindControl("txtFinalEstDate"), TextBox)
            Dim txtCODate As TextBox = CType(e.Item.FindControl("txtCODate"), TextBox)

            Dim lblInspection As Label = CType(e.Item.FindControl("lblInspection"), Label)
            Dim lblOrigEstDate As Label = CType(e.Item.FindControl("lblOrigEstDate"), Label)
            Dim lblRepairStartDate As Label = CType(e.Item.FindControl("lblRepairStartDate"), Label)
            Dim lblRepairEndDate As Label = CType(e.Item.FindControl("lblRepairEndDate"), Label)
            Dim lblFinalEstDate As Label = CType(e.Item.FindControl("lblFinalEstDate"), Label)
            Dim lblCODate As Label = CType(e.Item.FindControl("lblCODate"), Label)

            '-------------------------------
            ' DA Fields
            '-------------------------------
            Dim txtRepairZip As TextBox = CType(e.Item.FindControl("txtRepairZip"), TextBox)
            Dim txtRepairCity As TextBox = CType(e.Item.FindControl("txtRepairCity"), TextBox)
            Dim ddlRepairState As DropDownList = CType(e.Item.FindControl("ddlRepairState"), DropDownList)
            Dim ddlRepairCounty As DropDownList = CType(e.Item.FindControl("ddlRepairCounty"), DropDownList)

            Dim lblRepairZip As Label = CType(e.Item.FindControl("lblRepairZip"), Label)
            Dim lblRepairCity As Label = CType(e.Item.FindControl("lblRepairCity"), Label)
            Dim lblRepairState As Label = CType(e.Item.FindControl("lblRepairState"), Label)
            Dim lblRepairCounty As Label = CType(e.Item.FindControl("lblRepairCounty"), Label)

            Dim btnVehAddCoverage As Button = CType(e.Item.FindControl("btnVehAddCoverage"), Button)
            Dim divDADRDetail As HtmlGenericControl = CType(e.Item.FindControl("divDADRDetail"), HtmlGenericControl)

            '---------------------------------
            ' Vehicle Repeater
            '---------------------------------
            Dim lstVeh As Vehicle = New Vehicle()
            lstVeh = CType(e.Item.DataItem, Vehicle)

            '---------------------------------
            ' License Plate State
            '---------------------------------
            If Not ddlLicensePlateState Is Nothing Then
                ddlLicensePlateState.DataValueField = "statename"
                ddlLicensePlateState.DataTextField = "ReferenceID"
                ddlLicensePlateState.DataSource = lstVeh.LicensePlateStateCollection
                ddlLicensePlateState.DataBind()
            End If

            '---------------------------------
            ' Location State
            '---------------------------------
            If Not ddlLocationState Is Nothing Then
                ddlLocationState.DataValueField = "statename"
                ddlLocationState.DataTextField = "ReferenceID"
                ddlLocationState.DataSource = lstVeh.ContactStateCollection
                ddlLocationState.DataBind()
            End If

            '---------------------------------
            ' Contact State
            '---------------------------------
            If Not ddlContactState Is Nothing Then
                ddlContactState.DataValueField = "statename"
                ddlContactState.DataTextField = "ReferenceID"
                ddlContactState.DataSource = lstVeh.LicensePlateStateCollection
                ddlContactState.DataBind()
            End If

            '---------------------------------
            ' Contact ddlContactInsuredRelation
            '---------------------------------
            If Not ddlContactInsuredRelation Is Nothing Then
                ddlContactInsuredRelation.DataValueField = "ReferenceID"
                ddlContactInsuredRelation.DataTextField = "Name"
                ddlContactInsuredRelation.DataSource = lstVeh.ContactInsuredRelationCollection
                ddlContactInsuredRelation.DataBind()
            End If

            '---------------------------------
            ' Contact ddlBestContactTabPhoneCD
            '---------------------------------
            If Not ddlBestContactTabPhoneCD Is Nothing Then
                ddlBestContactTabPhoneCD.DataValueField = "ReferenceID"
                ddlBestContactTabPhoneCD.DataTextField = "Name"
                ddlBestContactTabPhoneCD.DataSource = lstVeh.BestContactTabPhoneCDCollection
                ddlBestContactTabPhoneCD.DataBind()
            End If



            '---------------------------------
            ' Service Channel Grid
            '---------------------------------
            If Not gvServiceChannel Is Nothing Then
                gvServiceChannel.DataSource = lstVeh.DTCoverage
                gvServiceChannel.DataBind()
                gvServiceChannel.Visible = True
            End If

            '---------------------------------
            ' Assignment Coverage Grid
            '---------------------------------
            If Not gvAssignmentCoverage Is Nothing Then
                gvAssignmentCoverage.DataSource = lstVeh.DTAssignmentCoverage
                gvAssignmentCoverage.DataBind()
                gvAssignmentCoverage.Visible = True
            End If

            '---------------------------------
            ' Assignment History Grid
            '---------------------------------
            If Not gvAssignmentHistory Is Nothing Then
                gvAssignmentHistory.DataSource = lstVeh.ShopAssignmentData.DTAssignmentHistory
                gvAssignmentHistory.DataBind()
                gvAssignmentHistory.Visible = True
            End If


            '---------------------------------
            ' Service Channel - Estimate Grid
            '---------------------------------
            If Not gvEstimate Is Nothing Then
                lstVeh.DTDocumentEstimate.DefaultView.Sort = "EstRecvDate DESC"
                gvEstimate.DataSource = lstVeh.DTDocumentEstimate
                gvEstimate.DataBind()

                gvEstimate.Visible = True
            End If

            '---------------------------------
            ' Waranty Asignment Grid
            '---------------------------------
            If Not gvWarAssignment Is Nothing Then
                gvWarAssignment.DataSource = lstVeh.Warrantylst
                gvWarAssignment.DataBind()

                gvWarAssignment.Visible = True
            End If

            '---------------------------------
            ' Service Channel - Estimate Grid
            '---------------------------------
            If Not gvWarEstimate Is Nothing Then
                gvWarEstimate.DataSource = lstVeh.DTWarDocumentEstimate
                gvWarEstimate.DataBind()

                gvWarEstimate.Visible = True
            End If

            '---------------------------------
            ' Service Channel - Concession
            '---------------------------------
            If Not gvConcessions Is Nothing Then
                gvConcessions.DataSource = lstVeh.DTConcession
                gvConcessions.DataBind()
                gvConcessions.Visible = True

                gvConcessions.Columns(0).Visible = False
            End If

            If Not lblConcessionTotal Is Nothing Then
                lblConcessionTotal.Text = String.Format("{0:C}", lstVeh.ConcessionTotal)
            End If

            If Not imgbtnVINEdit Is Nothing Then
                imgbtnVINEdit.Attributes.Add("onmouseout", "makeHighlight(this,1,60)")
                imgbtnVINEdit.Attributes.Add("onmouseover", "makeHighlight(this,1,100)")
            End If


            '---------------------------------
            ' Involved Drop down
            '---------------------------------
            'If Not ddlInvolved Is Nothing Then
            ddlInvolved.DataValueField = "InvolvedID"
            ddlInvolved.DataTextField = "InvolvedDisplayName"
            ddlInvolved.DataSource = lstVeh.Involveddatalist
            ddlInvolved.DataBind()
            ddlInvolved.Items.Add(New ListItem("--Add New--", "-1"))
            '' ddlInvolved.Items.Add()
            'End If


            '---------------------------------
            ' Involved State
            '---------------------------------
            If Not ddlInvolvedstate Is Nothing Then
                ddlInvolvedstate.DataValueField = "statename"
                ddlInvolvedstate.DataTextField = "ReferenceID"
                ddlInvolvedstate.DataSource = lstVeh.InvolvedStateCollection
                ddlInvolvedstate.DataBind()
            End If

            '---------------------------------
            ' populate Involved Business Type
            '---------------------------------
            If Not ddlInvolvedBusinessType Is Nothing Then
                ddlInvolvedBusinessType.DataValueField = "Name"
                ddlInvolvedBusinessType.DataTextField = "ReferenceID"
                ddlInvolvedBusinessType.DataSource = lstVeh.Involveddata.InvolvedBusinessType
                ddlInvolvedBusinessType.DataBind()
            End If

            '---------------------------------
            ' populate Involved Gender 
            '---------------------------------
            If Not ddlInvolvedGender Is Nothing Then
                ddlInvolvedGender.DataValueField = "Name"
                ddlInvolvedGender.DataTextField = "ReferenceID"
                ddlInvolvedGender.DataSource = lstVeh.Involveddata.InvolvedGender
                ddlInvolvedGender.DataBind()
            End If

            '--------------------------------
            ' Check if Involved exists
            '--------------------------------
            'Try
            For Each oInvolvedType In lstVeh.Involveddata.InvolvedTypes
                Select Case oInvolvedType.InvolvedTypeName
                    Case "Insured"
                        If Not cbInsured Is Nothing Then
                            cbInsured.Checked = True
                        End If
                    Case "Claimant"
                        If Not cbClaimant Is Nothing Then
                            cbClaimant.Checked = True
                        End If
                    Case "Owner"
                        If Not cbOwner Is Nothing Then
                            cbOwner.Checked = True
                        End If
                    Case "Driver"
                        If Not cbDriver Is Nothing Then
                            cbDriver.Checked = True
                        End If
                    Case "Passenger"
                        If Not cbPassenger Is Nothing Then
                            cbPassenger.Checked = True
                        End If
                End Select
            Next

            'This is for validation  __pageInit from VehicleInvolvedCond.xsl

            If (Not cbInsured Is Nothing) Then
                If (lstVeh.CurrentExposure = "1" And lstVeh.InvolvedInsuredCount > 0) Then
                    cbInsured.Enabled = False
                End If
            End If

            If (Not cbClaimant Is Nothing) Then
                If (lstVeh.CurrentExposure = "3" And lstVeh.involvedClaimantCount > 0) Then
                    cbClaimant.Enabled = False
                End If
            End If


            'Catch ex As Exception
            ' Doesn't exist
            'End Try

            '---------------------------------
            ' populate Business number to call
            '---------------------------------
            If Not ddlBestNumberCall Is Nothing Then
                ddlBestNumberCall.DataValueField = "Name"
                ddlBestNumberCall.DataTextField = "ReferenceID"
                ddlBestNumberCall.DataSource = lstVeh.Involveddata.InvolvedBestContactPhone
                ddlBestNumberCall.DataBind()
            End If


            '---------------------------------
            ' Shop Assignment status image 
            '---------------------------------
            If Not imgVehAssignmentStatus Is Nothing Then
                Select Case (lstVeh.Status)
                    Case "Vehicle Closed"
                        imgVehAssignmentStatus.ImageUrl = "images/closedExposure.gif"
                    Case "Vehicle Cancelled"
                        imgVehAssignmentStatus.ImageUrl = "images/cancelled.gif"
                    Case "Vehicle Voided"
                        imgVehAssignmentStatus.ImageUrl = "images/voided.gif"
                    Case "Open"
                        imgVehAssignmentStatus.ImageUrl = "images/blank-sml.gif"
                    Case Else
                        imgVehAssignmentStatus.ImageUrl = "images/blank-sml.gif"
                End Select
            End If

            '---------------------------------
            ' Shop Assignment - Disposition
            '---------------------------------
            Try
                If Not ddlDisposition Is Nothing Then
                    ddlDisposition.DataValueField = "ReferenceID"
                    ddlDisposition.DataTextField = "DispositionName"
                    ddlDisposition.DataSource = lstVeh.ShopAssignmentData.DispositionCollection
                    ddlDisposition.DataBind()
                End If
            Catch ex As Exception
                ' Doesn't exist
            End Try

            '---------------------------------
            ' Shop Assignment - StateList
            '---------------------------------
            Try
                If Not ddlRepairState Is Nothing Then
                    ddlRepairState.DataValueField = "ReferenceID"
                    ddlRepairState.DataTextField = "StateListName"
                    ddlRepairState.DataSource = lstVeh.ShopAssignmentData.StateListCollection
                    ddlRepairState.DataBind()
                End If
            Catch ex As Exception
                ' Doesn't exist
            End Try

            '---------------------------------
            ' Set Selected Values
            '---------------------------------
            Try
                ddlDisposition.SelectedValue = lstVeh.ShopAssignmentData.CurrentDisposition
                ddlRepairState.SelectedValue = lstVeh.ShopAssignmentData.CurrentStateList
                ddlLicensePlateState.SelectedValue = lstVeh.CurrentLicensePlateState
                ddlLocationState.SelectedValue = lstVeh.LocationState
                ddlContactState.SelectedValue = lstVeh.ContactState
                ddlContactInsuredRelation.SelectedValue = lstVeh.CurContactInsuredRelation
                ddlBestContactTabPhoneCD.SelectedValue = lstVeh.CurBestContactTabPhoneCD

                ddlInvolvedstate.SelectedValue = lstVeh.Involveddata.InvolvedState
                ddlInvolvedBusinessType.SelectedValue = lstVeh.Involveddata.InvolvedBusinessTypeCD
                ddlInvolvedGender.SelectedValue = lstVeh.Involveddata.InvolvedGenderCD
                ddlBestNumberCall.SelectedValue = lstVeh.Involveddata.InvolvedBestContactPhoneCD

                ddlRepairCounty.Items.Add(lstVeh.ShopAssignmentData.ShopLocationCounty)
                ddlRepairCounty.SelectedValue = lstVeh.ShopAssignmentData.ShopLocationCounty
                If (lstVeh.CurrentExposure = "1") Then
                    txtParty.Text = lstVeh.CurrentExposure + "st Party"
                ElseIf (lstVeh.CurrentExposure = "3") Then
                    txtParty.Text = lstVeh.CurrentExposure + "rd Party"
                Else
                    txtParty.Text = lstVeh.CurrentExposure + " Party"
                End If


            Catch ex As Exception
                ' Doesn't exist
            End Try

            '---------------------------------------
            ' Load Vehicle data - Drivable
            '---------------------------------------
            If lstVeh.DriveableFlag = "1" Then
                chkDriveableFlag.Checked = True
            Else
                chkDriveableFlag.Checked = False
            End If

            '-------------------------------
            ' Assignment Send/ReSend/Cancel 
            '-------------------------------
            Select Case UCase(lstVeh.ShopAssignmentData.AssignmentStatus)
                Case = "NOSHOPSELECTED"
                    btnSelectShop.Visible = True
                    btnSendAssignment.Visible = False
                    btnResendAssignment.Visible = False
                    btnCancelAssignment.Visible = False
                Case Else
                    btnSendAssignment.Visible = True
                    btnResendAssignment.Visible = True
                    btnCancelAssignment.Visible = True
            End Select

            '-------------------------------
            ' Show/Hide fields based on
            ' service channel
            '-------------------------------
            Select Case UCase(lstVeh.ShopAssignmentData.ServiceChannelCD)
                Case = "PS"
                    txtWorkStartDate.Visible = True
                    txtWorkEndDate.Visible = True
                    txtInspectionDate.Visible = True
                    txtOriginalEstimateDate.Visible = True
                    txtFinalEstDate.Visible = True
                    txtCODate.Visible = True

                    lblInspection.Visible = True
                    lblOrigEstDate.Visible = True
                    lblRepairStartDate.Visible = True
                    lblRepairEndDate.Visible = True
                    lblFinalEstDate.Visible = True
                    lblCODate.Visible = True

                    lblRepairZip.Visible = False
                    lblRepairCity.Visible = False
                    lblRepairState.Visible = False
                    lblRepairCounty.Visible = False

                    txtRepairZip.Visible = False
                    txtRepairCity.Visible = False
                    ddlRepairState.Visible = False
                    ddlRepairCounty.Visible = False

                    'divDADRDetail.Visible = False
                Case = "DA", "DR"
                    txtWorkStartDate.Visible = False
                    txtWorkEndDate.Visible = False
                    txtInspectionDate.Visible = False
                    txtOriginalEstimateDate.Visible = False
                    txtFinalEstDate.Visible = False
                    txtCODate.Visible = False

                    lblInspection.Visible = False
                    lblOrigEstDate.Visible = False
                    lblRepairStartDate.Visible = False
                    lblRepairEndDate.Visible = False
                    lblFinalEstDate.Visible = False
                    lblCODate.Visible = False

                    lblRepairZip.Visible = True
                    lblRepairCity.Visible = True
                    lblRepairState.Visible = True
                    lblRepairCounty.Visible = True

                    txtRepairZip.Visible = True
                    txtRepairCity.Visible = True
                    ddlRepairState.Visible = True
                    ddlRepairCounty.Visible = True

                    divDADRDetail.Visible = True
                Case Else
            End Select

            '---------------------------------------------
            ' Update Assignment Button based on current
            ' Assignment Status
            '---------------------------------------------
            Try
                ' Check if assignment exists
                If lstVeh.ShopAssignmentData.AssignmentSequenceNumber >= "1" And lstVeh.ShopAssignmentData.AssignmentID <> "" Then
                    ' Assignment Exists
                    Select Case lstVeh.ShopAssignmentData.ServiceChannelName
                        Case "GL"
                            btnSendAssignment.Visible = False
                        'Case "DA"
                        '    If lstVeh.ShopAssignmentData.AssignmentStatusID <> 0 Then
                        '        btnSelectShop.Visible = False
                        '    Else
                        '        btnSelectShop.Visible = True
                        '    End If
                        Case "PS", "DA", "CS", "RRP"
                            If lstVeh.ShopAssignmentData.AssignmentStatusID <> 0 Then
                                If lstVeh.ShopAssignmentData.AssignmentStatusID = "11" And lstVeh.ShopAssignmentData.AssignmentFaxStatusID = "22" Then
                                    btnSelectShop.Visible = False
                                    btnSendAssignment.Visible = True
                                    btnResendAssignment.Visible = False
                                    btnCancelAssignment.Visible = False
                                Else
                                    btnSelectShop.Visible = False
                                    btnSendAssignment.Visible = False
                                    btnResendAssignment.Visible = True
                                    btnCancelAssignment.Visible = True
                                End If
                            Else
                                btnSelectShop.Visible = True
                            End If



                    End Select

                    'If lstVeh.ShopAssignmentData.ServiceChannelName = "GL" Then
                    '    btnSendAssignment.Visible = False
                    'Else
                    '    btnSendAssignment.Visible = True
                    'End If

                    'If (lstVeh.ShopAssignmentData.AssignmentStatusID = "11" And lstVeh.ShopAssignmentData.AssignmentFaxStatusID = "22") Then
                    '    btnSendAssignment.Visible = True
                    'Else
                    '    btnSendAssignment.Visible = False
                    '    btnResendAssignment.Visible = True
                    'End If
                Else
                    'Shop is not selected show buttons based on that
                    btnSelectShop.Visible = True

                    If UCase(lstVeh.ShopAssignmentData.AssignmentFaxStatus) = "CANCELLATION SENT" Then
                        FAXAssignmentStatusName.Text = lstVeh.ShopAssignmentData.AssignmentFaxStatus & " to " & lstVeh.ShopAssignmentData.AppraiserName
                    End If
                End If

                If lstVeh.ShopAssignmentData.WorkStartDateConfirmFlag = "1" Then
                    txtWorkStartDate.Enabled = False
                End If
                If lstVeh.ShopAssignmentData.WorkEndDateConfirmFlag = "1" Then
                    txtWorkEndDate.Enabled = False
                End If

                '-----------------------------------//
                ' Change Fax Status 
                '-----------------------------------//

                'If lstVeh.ShopAssignmentData.ShopLocationID = 0 Or (lstVeh.ShopAssignmentData.ServiceChannelCD = "GL" And lstVeh.ShopAssignmentData.ShopReferenceID = "") Then
                '    btnSelectShop.Visible = True
                'Else
                '    btnSelectShop.Visible = False
                'End If

                If UCase(lstVeh.ShopAssignmentData.AssignmentTypeCD) = "SHOP" And lstVeh.ShopAssignmentData.ShopLocationID <> "" Or
                (UCase(lstVeh.ShopAssignmentData.AssignmentTypeCD) = "LDAU" And lstVeh.ShopAssignmentData.LDAUID = lstVeh.ShopAssignmentData.ShopLocationID) Or
                UCase(lstVeh.ShopAssignmentData.AssignmentTypeCD) = "IA" Then
                    btnCancelAssignment.Visible = True
                End If

                If lstVeh.InitialAssignmentType = "Choice Shop Assignment" And iVehicleCount = 1 Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "CSAlert", " alert('Please note this vehicle is assigned to a Choice Shop and is being processed in Hyperquest.\nPlease do not attempt to work this vehicle in APD.');", True)
                End If

            Catch ex As Exception
                '    ' Doesn't exist
            End Try

            '--------------------------------
            ' Vehicle Section make Read Only
            '--------------------------------
            'If bPageReadOnly = True Then
            If UCase(lstVeh.Status) = "VEHICLE CANCELLED" Or UCase(lstVeh.Status) = "VEHICLE VOIDED" Then
                For Each ctrl In e.Item.Controls
                    '--------------------------------
                    ' Disable Inputs
                    '--------------------------------
                    If ctrl.GetType.Name = "TextBox" Or ctrl.GetType.Name = "DropDownList" Or ctrl.GetType.Name = "Button" Or ctrl.GetType.Name = "CheckBox" Then
                        ctrl.enabled = False
                        ctrl.Attributes.Add("onclick", "")
                    End If

                    'If ctrl.ID = "btnImpact" Then
                    Debug.Print(ctrl.ID & " - " & ctrl.GetType.Name)
                    'End If

                    '--------------------------------
                    ' Disable Modal Launch's
                    '--------------------------------
                    If ctrl.GetType.Name = "ImageButton" Then
                        ctrl.enabled = False
                        ctrl.Attributes.Add("onclick", "")
                    End If
                Next
            End If
        End If

        '--------------------------------
        ' Vehicle Assignment Section 
        ' make Read Only
        '--------------------------------
        If bPageReadOnly_Vehicle = True Then
        End If

    End Sub

    Protected Sub RepeaterVehdetTab_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles RepeaterVehdetTab.ItemCommand
        Dim iRC As Integer = 0
        Dim sClaimAspectServiceChannelID As String = ""
        Dim sInscCompanyID As String = ""
        Dim aParams As Array = Nothing
        Dim bResendFlag As Boolean = False
        Dim bAssignmentFlag As Boolean = False

        If e.CommandSource.id = "btnSendAssignment" Then
            aParams = e.CommandArgument.ToString().Split(",")
            sClaimAspectServiceChannelID = aParams(0)
            sInscCompanyID = aParams(1)
            bResendFlag = False
            bAssignmentFlag = True

            'iRC = SendShopAssignment(CInt(sClaimAspectServiceChannelID), CInt(sInscCompanyID), bResendFlag, bAssignmentFlag)
        End If
    End Sub

    Protected Sub btnSendAssignment_Click(sender As Object, e As EventArgs)
        Dim sAction As String = ""
        Dim sJobID As String = ""
        Dim sReturnString As String = ""

        'Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)

        'Dim sShopRemarks As String = ""
        'Dim sClaimAspectServiceChannelID As String = ""
        'Dim sVehNum As String = ""
        'Dim sShopLocationID As String = ""
        'Dim sAppraiserID As String = ""
        'Dim aRC As Array = Nothing

        Try
            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim btn As Button = TryCast(sender, Button)
            If btn IsNot Nothing Then
                '--------------------------
                ' Access the repeater data
                '--------------------------
                Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)
                sAction = TryCast(item.FindControl("hidAssignmentAction"), HiddenField).Value
                Dim aAction As Array = sAction.Split("|")

                If sAction <> "" And aAction.Length > 0 Then

                    sAction = aAction(0)
                    sJobID = aAction(1)

                    '        sShopRemarks = TryCast(item.FindControl("txtAssignmentRemarks"), TextBox).Text
                    '        sClaimAspectServiceChannelID = TryCast(item.FindControl("hidClaimAspectServiceChannelID"), HiddenField).Value
                    '        sVehNum = TryCast(item.FindControl("hidVehNumber"), HiddenField).Value
                    '        sShopLocationID = TryCast(item.FindControl("hidShopLocationID"), HiddenField).Value
                    '        sAppraiserID = TryCast(item.FindControl("hidLDAUID"), HiddenField).Value

                    '-------------------------------
                    ' Create DB Params
                    '-------------------------------  
                    sParams = "@vJobID='" & sJobID & "', @vJobStatus='UnProcessed'"

                    '-------------------------------
                    ' Process DB Call
                    '-------------------------------  
                    Dim sStoredProcedure As String = "uspHyperquestUpdateJobDetails"

                    '-------------------------------
                    ' Call WS and get data
                    '-------------------------------
                    sReturnString = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

                    '-------------------------------
                    ' Check if update was successful
                    '-------------------------------
                    If UCase(sReturnString) <> "UPDATED" Then
                        Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + sReturnString + " ');", True)
                    Else
                        Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
                    End If
                End If

            End If
            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("SendAssignment", "ERROR", "Send Assignment not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            '    'oXMLNode = Nothing
            '    'oReturnXML = Nothing
            '    'lblClaimSaved.Text = "Saved..."
        End Try
    End Sub

    Protected Sub SendHQAssignment(ByVal sJobID As String)
        Dim sReturnString As String = ""
        Dim aRC As Array = Nothing

        '-------------------------------
        ' Database access
        '-------------------------------  
        Dim sStoredProcedure As String = "uspHyperquestUpdateJobDetails"
        Dim sParams As String = "@vJobID=" & sJobID & ", @vJobStatus='UnProcessed'"

        Try
            If sJobID <> "" Then
                '-------------------------------
                ' Call WS and get data
                '-------------------------------
                sReturnString = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

                '-------------------------------
                ' Check if update was successful
                '-------------------------------
                If sReturnString.Contains("ERROR") And sReturnString.Contains("|") Then
                    aRC = sReturnString.Split("|")
                    Throw New SystemException("Error: " & aRC(0))
                    'Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + aRC(1) + " ');", True)
                End If
            Else
                Throw New SystemException("Error: No JobID found.  JobID=" & sJobID)
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("SendHQAssignment", "ERROR", "Send HQ Assignment not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
        End Try
    End Sub

    Protected Sub SendFaxAssignment()
        Dim sReturnString As String = ""
        Dim aRC As Array = Nothing

        '-------------------------------
        ' Database access
        '-------------------------------  
        'Dim sStoredProcedure As String = "uspHyperquestUpdateJobDetails"
        'Dim sParams As String = "@vJobID=" & sJobID & ", @vJobStatus='UnProcessed'"

        'sRequest = "<Root><Assignment assignmentID=\"" + gsAssignmentID + " \ " userID=\"" + gsUserID + " \ " staffAppraiser=\""+ strStaffAppraiser +" \ " hqAssignment=\"" + false + " \ "/></Root>"

        'Try
        '    If sJobID <> "" Then
        '        '-------------------------------
        '        ' Call WS and get data
        '        '-------------------------------
        '        sReturnString = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

        '        '-------------------------------
        '        ' Check if update was successful
        '        '-------------------------------
        '        If sReturnString.Contains("ERROR") And sReturnString.Contains("|") Then
        '            aRC = sReturnString.Split("|")
        '            Throw New SystemException("Error: " & aRC(0))
        '            'Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + aRC(1) + " ');", True)
        '        End If
        '    Else
        '        Throw New SystemException("Error: No JobID found.  JobID=" & sJobID)
        '    End If
        'Catch oExcept As Exception
        '    '---------------------------------
        '    ' Error handler and notifications
        '    '---------------------------------
        '    Dim sError As String = ""
        '    Dim sBody As String = ""
        '    Dim FunctionName As New System.Diagnostics.StackFrame

        '    sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
        '    wsAPDFoundation.LogEvent("SendHQAssignment", "ERROR", "Send HQ Assignment not processed or data error occured.", oExcept.Message, sError)

        '    Response.Write(oExcept.ToString)

        'Finally
        'End Try
    End Sub

    Protected Sub WorkFlowSelectShop(ByVal sClaimAspectServiceChannelID As String, ByVal sSelectOperationCD As String, ByVal sAppraiserID As String, ByVal sShopLocationID As String, ByVal sAssignmentRemarks As String)
        Dim sReturnString As String = ""
        Dim aRC As Array = Nothing

        '-------------------------------
        ' Validation
        '-------------------------------  
        If sAppraiserID = "" Then sAppraiserID = "NULL"

        '-------------------------------
        ' Database access
        '-------------------------------  
        Dim sStoredProcedure As String = "uspWorkFlowSelectShop"
        Dim sParams As String = "@ClaimAspectServiceChannelID=" & sClaimAspectServiceChannelID & ", @SelectOperationCD='" & sSelectOperationCD & "', @ShopLocationID=" & sShopLocationID & ", @AppraiserID=" & sAppraiserID & ", @AssignmentRemarks='" & sAssignmentRemarks & "', @UserID=" & sUserID & ", @NotifyEvent= 1"

        Try
            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            sReturnString = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

            '-------------------------------
            ' Check if update was successful
            '-------------------------------
            If sReturnString.Contains("Error") And sReturnString.Contains("|") Then
                aRC = sReturnString.Split("|")
                Throw New SystemException("Error " & aRC(0))
                'Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + aRC(1) + " ');", True)
            End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("WorkFlowSelectShop", "ERROR", "Workflow Select Shop not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
        End Try
    End Sub

    Protected Sub btnVehicleDescSave_Click(sender As Object, e As EventArgs)
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim iDrivable As Integer = 0
        Dim sInspectionDate As String = ""
        Dim sStoredProcedure As String = ""
        Dim sParams As String = ""
        Dim oImpactPoints As GridView = Nothing
        Dim sImpactPoints As String = ""
        Dim oPriorImpactPoints As GridView = Nothing
        Dim sPriorImpactPoints As String = ""
        Dim sImpactSpeed As String = ""
        Dim sPostedSpeed As String = ""
        Dim sBookValueAmt As String = ""
        Dim sMileage As String = ""
        Dim olblVehicleDescSaved As Label = Nothing
        Dim sHidRepairChangeReason As String = ""
        Dim aRC As Array = Nothing
        Dim iCnt = 0

        Try
            '-------------------------------
            ' Validate the web page data
            '-------------------------------  
            '??????????? CODE THIS ????????????????????
            'lblVehicleDescSaved.Text = ""

            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim btn As Button = TryCast(sender, Button)
            If btn IsNot Nothing Then
                '--------------------------
                ' Access the repeater data
                '--------------------------
                Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)

                sVehicleSysLastUpdatedDate = TryCast(item.FindControl("hidVehicleSysLastUpdatedDate"), HiddenField).Value
                sVehicleClaimAspectID = TryCast(item.FindControl("hidVehicleClaimAspectID"), HiddenField).Value

                'sHidRepairChangeReason = TryCast(item.FindControl("hidRepairScheduleChangeReason"), HiddenField).Value

                sInspectionDate = TryCast(item.FindControl("txtInspectionDate"), TextBox).Text
                If Not sInspectionDate <> "" Then
                    sInspectionDate = ""
                End If

                If TryCast(item.FindControl("chkDriveableFlag"), CheckBox).Checked = "True" Then
                    iDrivable = 1
                Else
                    iDrivable = 0
                End If

                sImpactPoints = TryCast(item.FindControl("hidImpactPoints"), HiddenField).Value
                sPriorImpactPoints = TryCast(item.FindControl("hidPriorImpactPoints"), HiddenField).Value

                If (item.FindControl("txtImpactSpeed") Is Nothing) Then
                    sImpactSpeed = ""
                Else
                    sImpactSpeed = TryCast(item.FindControl("txtImpactSpeed"), TextBox).Text
                    If sImpactSpeed = "" Then sImpactSpeed = 0
                End If

                If (item.FindControl("txtPostedSpeed") Is Nothing) Then
                    sPostedSpeed = ""
                Else
                    sPostedSpeed = TryCast(item.FindControl("txtPostedSpeed"), TextBox).Text
                    If sPostedSpeed = "" Then sPostedSpeed = 0
                End If

                sBookValueAmt = TryCast(item.FindControl("txtBookValueAmt"), TextBox).Text
                If sBookValueAmt = "" Then sBookValueAmt = 0
                sMileage = TryCast(item.FindControl("txtMileage"), TextBox).Text
                If sMileage = "" Then sMileage = 0

                olblVehicleDescSaved = item.FindControl("lblVehicleDescSaved")

                sStoredProcedure = "uspVehicleDescriptionUpdDetail"
                sParams = "@ClaimAspectID=" & sVehicleClaimAspectID _
            & ", @AssignmentTypeID=" & TryCast(item.FindControl("hidCurrentAssignmentTypeID"), HiddenField).Value _
            & ", @BodyStyle='" & TryCast(item.FindControl("txtBodyStyle"), TextBox).Text & "'" _
            & ", @BookValueAmt=" & sBookValueAmt _
            & ", @Color='" & TryCast(item.FindControl("txtColor"), TextBox).Text & "'" _
            & ", @CoverageProfileCD='" & TryCast(item.FindControl("hidCoverageProfileCD"), HiddenField).Value & "'" _
            & ", @ClientCoverageTypeID=" & TryCast(item.FindControl("hidClientCoverageTypeID"), HiddenField).Value _
            & ", @DriveableFlag=" & iDrivable _
            & ", @ImpactPoints='" & sImpactPoints & "'" _
            & ", @ImpactSpeed=" & sImpactSpeed _
            & ", @LicensePlateNumber='" & TryCast(item.FindControl("txtLicensePlateNumber"), TextBox).Text & "'" _
            & ", @LicensePlateState='" & TryCast(item.FindControl("ddlLicensePlateState"), DropDownList).SelectedItem.Value & "'" _
            & ", @InspectionDate=NULL" _
            & ", @Make='" & TryCast(item.FindControl("txtMake"), TextBox).Text & "'" _
            & ", @Mileage=" & sMileage _
            & ", @Model='" & TryCast(item.FindControl("txtModel"), TextBox).Text & "'" _
            & ", @PermissionToDriveCD='" & TryCast(item.FindControl("hidPermissionToDriveCD"), HiddenField).Value & "'" _
            & ", @PostedSpeed=" & sPostedSpeed _
            & ", @PriorImpactPoints='" & sPriorImpactPoints & "'" _
            & ", @Remarks='" & TryCast(item.FindControl("txtVehicleRemarks"), TextBox).Text & "'" _
            & ", @RepairEndDate=NULL" _
            & ", @RepairStartDate=NULL" _
            & ", @RepairScheduleChangeReason=''" _
            & ", @VehicleYear='" & TryCast(item.FindControl("txtVehicleYear"), TextBox).Text & "'" _
            & ", @Vin='" & TryCast(item.FindControl("txtVIN"), TextBox).Text & "'" _
            & ", @SafetyDevicesDeployed=''" _
            & ", @UserID=" & sUserID _
            & ", @SysLastUpdatedDate='" & TryCast(item.FindControl("hidVehicleSysLastUpdatedDate"), HiddenField).Value & "'"

            End If

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            olblVehicleDescSaved.Text = "Saved..."

            ''-------------------------------
            '' Check if update was successful
            ''-------------------------------
            'If oReturnXML.OuterXml.Contains("ERROR") Then
            '    aRC = (oReturnXML.InnerText).Split("|")
            '    Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + aRC(1) + " ');", True)
            'Else
            '    olblVehicleDescSaved.Text = "Saved..."
            'End If

            'Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame
            olblVehicleDescSaved.Text = "Not Saved..."
            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("VehicleSaveDetails", "ERROR", "Vehicle Description Save Details not processed or data error occured.", oExcept.Message, sError)
            olblVehicleDescSaved.Text = "Not Saved..."
            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
        End Try
    End Sub

    Protected Sub btnVehicleLocationSave_Click(sender As Object, e As EventArgs)
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim sPhone As String = ""
        Dim sStoredProcedure As String = ""
        Dim sParams As String = ""
        Dim sRC As String = ""
        Dim oStatusLabel As Label = Nothing
        Dim veh As Vehicle = New Vehicle
        Dim sVehicleSParams As String
        Dim sVehicleStoreProcedure As String

        Try
            '-------------------------------
            ' Validate the web page data
            '-------------------------------  
            '??????????? CODE THIS ????????????????????

            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim btn As Button = TryCast(sender, Button)
            If btn IsNot Nothing Then
                '--------------------------
                ' Access the repeater data
                '--------------------------
                Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)

                '--------------------------
                ' Clean up phone
                '--------------------------
                sPhone = TryCast(item.FindControl("txtLocationPhone"), TextBox).Text
                sVehicleClaimAspectID = TryCast(item.FindControl("hidVehicleClaimAspectID"), HiddenField).Value

                'Get system last update
                sVehicleStoreProcedure = "uspClaimVehicleGetDetailWSXML"
                sVehicleSParams = sVehicleClaimAspectID & "," & sInscCompany
                oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sVehicleStoreProcedure, sVehicleSParams)
                oXMLNode = oReturnXML.SelectSingleNode("Vehicle")
                sVehicleSysLastUpdatedDate = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate")

                oStatusLabel = item.FindControl("lblVehicleLocationSave")
                oStatusLabel.Text = ""

                sStoredProcedure = "uspVehicleLocationUpdDetail"
                sParams = "@ClaimAspectID=" & sVehicleClaimAspectID _
                    & ", @LocationAddress1='" & TryCast(item.FindControl("txtLocationAddress1"), TextBox).Text & "'" _
                    & ", @LocationAddress2='" & TryCast(item.FindControl("txtLocationAddress2"), TextBox).Text & "'" _
                    & ", @LocationCity='" & TryCast(item.FindControl("txtLocationCity"), TextBox).Text & "'" _
                    & ", @LocationAreaCode='" & GetPhoneData(sPhone, "AreaCode") & "'" _
                    & ", @LocationExchangeNumber='" & GetPhoneData(sPhone, "ExchangeNumber") & "'" _
                    & ", @LocationExtensionNumber=''" _
                    & ", @LocationUnitNumber='" & GetPhoneData(sPhone, "UnitNumber") & "'" _
                    & ", @LocationName='" & TryCast(item.FindControl("txtLocationName"), TextBox).Text & "'" _
                    & ", @LocationState='" & TryCast(item.FindControl("ddlLocationState"), DropDownList).SelectedItem.Value & "'" _
                    & ", @LocationZip='" & TryCast(item.FindControl("txtLocationZip"), TextBox).Text & "'" _
                    & ", @UserID=" & sUserID _
                    & ", @SysLastUpdatedDate='" & sVehicleSysLastUpdatedDate & "'"
            End If

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            sRC = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)

            '-------------------------------
            ' Check if update was successful
            '-------------------------------
            If sRC = "1" Then
                oStatusLabel.Text = "Saved..."
                'Page.ClientScript.RegisterStartupScript(Me.GetType, "myCloseScript", "window.opener.location.reload(true);", True)
            Else
                oStatusLabel.Text = "Failed to Save..."
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("VehicleSaveDetails", "ERROR", "Vehicle Location Save Details not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
        End Try
    End Sub

    Protected Sub btnVehicleContactSave_Click(sender As Object, e As EventArgs)
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim sStoredProcedure As String = ""
        Dim sParams As String = ""
        Dim sRC As String = ""
        Dim oStatusLabel As Label = Nothing
        Dim sContactSParams As String
        Dim sContactStoreProcedure As String

        Dim sDayPhoneArea As String = ""
        Dim sDayPhoneExch As String = ""
        Dim sDayPhoneExt As String = ""
        Dim sDayPhoneUnit As String = ""
        Dim sNightPhoneArea As String = ""
        Dim sNightPhoneExch As String = ""
        Dim sNightPhoneExt As String = ""
        Dim sNightPhoneUnit As String = ""
        Dim sAltPhoneArea As String = ""
        Dim sAltPhoneExch As String = ""
        Dim sAltPhoneExt As String = ""
        Dim sAltPhoneUnit As String = ""
        Dim sCellPhoneArea As String = ""
        Dim sCellPhoneExch As String = ""
        Dim sCellPhoneExt As String = ""
        Dim sCellPhoneUnit As String = ""

        Dim strContactCell As String = ""

        Try
            '-------------------------------
            ' Validate the web page data
            '-------------------------------  
            '??????????? CODE THIS ????????????????????

            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim btn As Button = TryCast(sender, Button)
            If btn IsNot Nothing Then
                '--------------------------
                ' Access the repeater data
                '--------------------------
                Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)

                sDayPhoneArea = GetPhoneData(TryCast(item.FindControl("txtContactDay"), TextBox).Text, "AreaCode")
                sDayPhoneExch = GetPhoneData(TryCast(item.FindControl("txtContactDay"), TextBox).Text, "ExchangeNumber")
                sDayPhoneUnit = GetPhoneData(TryCast(item.FindControl("txtContactDay"), TextBox).Text, "UnitNumber")

                sNightPhoneArea = GetPhoneData(TryCast(item.FindControl("txtContactNight"), TextBox).Text, "AreaCode")
                sNightPhoneExch = GetPhoneData(TryCast(item.FindControl("txtContactNight"), TextBox).Text, "ExchangeNumber")
                sNightPhoneUnit = GetPhoneData(TryCast(item.FindControl("txtContactNight"), TextBox).Text, "UnitNumber")

                sAltPhoneArea = GetPhoneData(TryCast(item.FindControl("txtContactAlternate"), TextBox).Text, "AreaCode")
                sAltPhoneExch = GetPhoneData(TryCast(item.FindControl("txtContactAlternate"), TextBox).Text, "ExchangeNumber")
                sAltPhoneUnit = GetPhoneData(TryCast(item.FindControl("txtContactAlternate"), TextBox).Text, "UnitNumber")

                sCellPhoneArea = GetPhoneData(TryCast(item.FindControl("txtContactCell"), TextBox).Text, "AreaCode")
                sCellPhoneExch = GetPhoneData(TryCast(item.FindControl("txtContactCell"), TextBox).Text, "ExchangeNumber")
                sCellPhoneUnit = GetPhoneData(TryCast(item.FindControl("txtContactCell"), TextBox).Text, "UnitNumber")

                '--------------------------
                ' Clean up phone
                '--------------------------
                'sContactSysLastUpdatedDate = TryCast(item.FindControl("hidContactSysLastUpdatedDate"), HiddenField).Value
                sVehicleClaimAspectID = TryCast(item.FindControl("hidVehicleClaimAspectID"), HiddenField).Value
                'Get system last update
                sContactStoreProcedure = "uspClaimVehicleGetDetailWSXML"
                sContactSParams = sVehicleClaimAspectID & "," & sInscCompany
                oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sContactStoreProcedure, sContactSParams)
                oXMLNode = oReturnXML.SelectSingleNode("Vehicle/Contact")
                sContactSysLastUpdatedDate = GetXMLNodeattrbuteValue(oXMLNode, "SysLastUpdatedDate")

                oStatusLabel = item.FindControl("lblVehicleContactSave")
                oStatusLabel.Text = ""

                sStoredProcedure = "uspvehicleContactUpdDetail"
                sParams = "@ClaimAspectID=" & sVehicleClaimAspectID _
                    & ", @InsuredRelationID='" & TryCast(item.FindControl("hidInsuredRelationID"), HiddenField).Value & "'" _
                    & ", @Address1='" & TryCast(item.FindControl("txtContactAddress1"), TextBox).Text & "'" _
                    & ", @Address2='" & TryCast(item.FindControl("txtContactAddress2"), TextBox).Text & "'" _
                    & ", @AddressCity='" & TryCast(item.FindControl("txtContactCity"), TextBox).Text & "'" _
                    & ", @AddressState='" & TryCast(item.FindControl("ddlContactState"), DropDownList).SelectedItem.Value & "'" _
                    & ", @AddressZip='" & TryCast(item.FindControl("txtContactZip"), TextBox).Text & "'" _
                    & ", @AlternateAreaCode='" & sAltPhoneArea & "'" _
                    & ", @AlternateExchangeNumber='" & sAltPhoneExch & "'" _
                    & ", @AlternateExtensionNumber='" & TryCast(item.FindControl("hidAlternateExtensionNumber"), HiddenField).Value & "'" _
                    & ", @AlternateUnitNumber='" & sAltPhoneUnit & "'" _
                    & ", @BestContactPhoneCD='" & TryCast(item.FindControl("hidBestContactPhoneCD"), HiddenField).Value & "'" _
                    & ", @BestContactTime='" & TryCast(item.FindControl("hidBestContactTime"), HiddenField).Value & "'" _
                    & ", @DayAreaCode='" & sDayPhoneArea & "'" _
                    & ", @DayExchangeNumber='" & sDayPhoneExch & "'" _
                    & ", @DayExtensionNumber='" & TryCast(item.FindControl("hidDayExtensionNumber"), HiddenField).Value & "'" _
                    & ", @DayUnitNumber='" & sDayPhoneUnit & "'" _
                    & ", @EmailAddress='" & TryCast(item.FindControl("txtContactEmailAddress"), TextBox).Text & "'" _
                    & ", @NameFirst='" & TryCast(item.FindControl("txtContactNameFirst"), TextBox).Text & "'" _
                    & ", @NameLast='" & TryCast(item.FindControl("txtContactNameLast"), TextBox).Text & "'" _
                    & ", @NameTitle='" & TryCast(item.FindControl("txtContactNameTitle"), TextBox).Text & "'" _
                    & ", @NightAreaCode='" & sNightPhoneArea & "'" _
                    & ", @NightExchangeNumber='" & sNightPhoneExch & "'" _
                    & ", @NightExtensionNumber='" & TryCast(item.FindControl("hidNightExtensionNumber"), HiddenField).Value & "'" _
                    & ", @NightUnitNumber='" & sNightPhoneUnit & "'" _
                    & ", @UserID=" & sUserID _
                    & ", @SysLastUpdatedDate='" & sContactSysLastUpdatedDate & "'" _
                    & ", @PrefMethodUpd=null" _
                    & ", @CellPhoneCarrier=null" _
                    & ", @CellAreaCode='" & sCellPhoneArea & "'" _
                    & ", @CellExchangeNumber='" & sCellPhoneExch & "'" _
                    & ", @CellUnitNumber='" & sCellPhoneUnit & "'"
            End If

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            sRC = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

            '-------------------------------
            ' Check if update was successful
            '-------------------------------
            If sRC = "1" Then
                oStatusLabel.Text = "Saved..."
            Else
                oStatusLabel.Text = "Failed to Save..."
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("VehicleSaveDetails", "ERROR", "Vehicle Location Save Details not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
        End Try
    End Sub

    Protected Sub btnSaveAssignment_Click(sender As Object, e As EventArgs)
        Dim sReturnData As String = ""
        Dim oXMLNode As XmlNode = Nothing
        Dim sShopInfo As String = ""
        Dim sParams As String = ""
        Dim txtInspectionDate As TextBox = Nothing
        Dim stxtInspectionDate As String = ""
        Dim txtWorkEndDate As TextBox = Nothing
        Dim stxtWorkEndDate As String = ""
        Dim txtWorkStartDate As TextBox = Nothing
        Dim stxtWorkStartDate As String = ""
        Dim txtCODate As TextBox = Nothing
        Dim stxtCODate As String = ""
        Dim aRC As Array = Nothing
        Dim olblAssignmentSaved As Label = Nothing
        Dim txtFinalEstDate As TextBox = Nothing
        Dim stxtFinalEstDate As String = ""
        Dim txtAppraiserInvoiceDate As TextBox = Nothing
        Dim stxtAppraiserInvoiceDate As String = ""
        Dim hidDispositionTypeCD As HiddenField = Nothing
        Dim sRC As String = ""

        Try
            '-------------------------------
            ' Repair Change Reason
            '-------------------------------  
            'sRC = RepairChangeReason.aspx

            '-------------------------------
            ' Process DB Call
            '------------------------------- 
            Dim btn
            If TryCast(sender, Button) Is Nothing Then
                btn = TryCast(sender, ImageButton)
            Else
                btn = TryCast(sender, Button)
            End If

            If btn IsNot Nothing Then
                '--------------------------
                ' Access the repeater data
                '--------------------------
                Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)
                Dim txtRepairLocationCity = TryCast(item.FindControl("txtRepairLocationCity"), TextBox)
                Dim txtRepairLocationCounty = TryCast(item.FindControl("txtRepairLocationCounty"), TextBox)
                Dim ddlRepairLocationState = TryCast(item.FindControl("ddlRepairLocationState"), DropDownList)

                Dim sServiceChannelCD As String = TryCast(item.FindControl("hidServiceChannelCD"), HiddenField).Value
                Dim sCurrentAssignmentTypeID As String = TryCast(item.FindControl("hidCurrentAssignmentTypeID"), HiddenField).Value

                olblAssignmentSaved = TryCast(item.FindControl("lblSaveAssignment"), Label)
                olblAssignmentSaved.Text = ""

                '--------------------------------
                ' Determine the service channel
                ' and build SP Params
                '--------------------------------
                sParams = "@AssignmentID=" & TryCast(item.FindControl("hidAssignmentID"), HiddenField).Value
                sParams += ", @AssignmentRemarks='" & TryCast(item.FindControl("txtAssignmentRemarks"), TextBox).Text & "'"
                If TryCast(item.FindControl("txtAssignmentDeductSent"), TextBox).Text = "" Then
                    sParams += ", @EffectiveDeductibleSentAmt=0"
                Else
                    sParams += ", @EffectiveDeductibleSentAmt=" & TryCast(item.FindControl("txtAssignmentDeductSent"), TextBox).Text
                End If

                hidDispositionTypeCD = TryCast(item.FindControl("hidCurrentDisposition"), HiddenField)
                If hidDispositionTypeCD.Value <> "" Then
                    sParams += ", @DispositionTypeCD='" & TryCast(item.FindControl("hidCurrentDisposition"), HiddenField).Value & "'"
                End If

                txtInspectionDate = item.FindControl("txtInspectionDate")
                If txtInspectionDate.Text <> "" Then
                    stxtInspectionDate = Date.ParseExact((TryCast(item.FindControl("txtInspectionDate"), TextBox).Text), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")
                End If

                txtWorkStartDate = item.FindControl("txtWorkStartDate")
                If txtWorkStartDate.Text <> "" Then
                    stxtWorkStartDate = Date.ParseExact((TryCast(item.FindControl("txtWorkStartDate"), TextBox).Text), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")
                End If

                txtWorkEndDate = item.FindControl("txtWorkEndDate")
                If txtWorkEndDate.Text <> "" Then
                    stxtWorkEndDate = Date.ParseExact((TryCast(item.FindControl("txtWorkEndDate"), TextBox).Text), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")
                End If

                txtCODate = item.FindControl("txtCODate")
                If txtCODate.Text <> "" Then
                    stxtCODate = Date.ParseExact((TryCast(item.FindControl("txtCODate"), TextBox).Text), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")
                End If

                txtFinalEstDate = item.FindControl("txtFinalEstDate")
                If txtFinalEstDate.Text <> "" Then
                    stxtFinalEstDate = Date.ParseExact((TryCast(item.FindControl("txtFinalEstDate"), TextBox).Text), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")
                End If

                txtAppraiserInvoiceDate = item.FindControl("txtFinalEstDate")
                If txtAppraiserInvoiceDate.Text <> "" Then
                    stxtAppraiserInvoiceDate = Date.ParseExact((TryCast(item.FindControl("txtFinalEstDate"), TextBox).Text), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")
                End If


                Select Case UCase(sServiceChannelCD)
                    Case "DA"
                        sParams += ", @RepairLocationCity=" & TryCast(item.FindControl("txtRepairLocationCity"), TextBox).Text
                        sParams += ", @RepairLocationCounty=" & TryCast(item.FindControl("txtRepairLocationCounty"), TextBox).Text
                        sParams += ", @RepairLocationState=" & TryCast(item.FindControl("txtRepairLocationState"), TextBox).Text

                        '-- Validation
                        'If txtRepairLocationCity.Text = "" Or ddlRepairLocationState.SelectedIndex = -1 Or txtRepairLocationCounty.Text = "" Then
                        '    Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert('Repair City, State and County are required.');", True)
                        'End If

                        '-- Persuit Audit    
                        If sCurrentAssignmentTypeID = "15" Then
                            sParams = TryCast(item.FindControl("txtRepairShopName"), TextBox).Text


                            'sShopInfo = TryCast(item.FindControl("txtRepairShopName"), TextBox).Text & " " _
                            '    & TryCast(item.FindControl("txtRepairShopAddress"), TextBox).Text & " " _
                            '    & TryCast(item.FindControl("txtRepairLocationZip"), TextBox).Text & " " _
                            '    & TryCast(item.FindControl("txtRepairLocationCity"), TextBox).Text & " " _
                            '    & TryCast(item.FindControl("txtRepairLocationState"), TextBox).Text & " " _
                            '    & TryCast(item.FindControl("txtRepairLocationCounty"), TextBox).Text & " " _
                            '    & TryCast(item.FindControl("txtRepairShopPhone"), TextBox).Text & " " _
                            '    & TryCast(item.FindControl("txtRepairShopEmailAddress"), TextBox).Text & " " _
                            '    & TryCast(item.FindControl("txtPursuitInspectionDate"), TextBox).Text

                            'sRequest = "&InspectionDate=" & TryCast(item.FindControl("txtPursuitInspectionDate"), TextBox).Text & "&SourceApplicationPassThruData=" & HttpUtility.HtmlEncode(sShopInfo)
                        End If

                    Case "DR"
                        sParams += ", @RepairLocationCity=" & TryCast(item.FindControl("txtRepairLocationCity"), TextBox).Text
                        sParams += ", @RepairLocationCounty=" & TryCast(item.FindControl("txtRepairLocationCounty"), TextBox).Text
                        sParams += ", @RepairLocationState=" & TryCast(item.FindControl("txtRepairLocationState"), TextBox).Text
                        sParams += ", @RepairScheduleChangeReasonID='" & TryCast(item.FindControl("hidRepairScheduleChangeReasonID"), HiddenField).Value & "'"
                        sParams += ", @RepairScheduleChangeReason='" & TryCast(item.FindControl("hidRepairScheduleChangeReason"), HiddenField).Value & "'"
                    Case "GL"
                        If txtFinalEstDate.Text <> "" Then
                            sParams += ", @AppraiserInvoiceDate='" & stxtAppraiserInvoiceDate & "'"
                        Else
                            sParams += ", @AppraiserInvoiceDate=NULL"
                        End If
                        If txtCODate.Text <> "" Then
                            sParams += ", @CashOutDate='" & stxtCODate & "'"
                        Else
                            sParams += ", @CashOutDate=NULL"
                        End If

                        If stxtWorkEndDate <> "" Then
                            sParams += ", @WorkEndDate='" & stxtWorkEndDate & "'"
                        Else
                            sParams += ", @WorkEndDate=NULL"
                        End If

                        If stxtWorkStartDate <> "" Then
                            sParams += ", @WorkStartDate='" & stxtWorkStartDate & "'"
                        Else
                            sParams += ", @WorkStartDate=NULL"
                        End If


                    Case "ME"
                        If txtFinalEstDate.Text <> "" Then
                            sParams += ", @AppraiserInvoiceDate='" & stxtAppraiserInvoiceDate & "'"
                        Else
                            sParams += ", @AppraiserInvoiceDate=NULL"
                        End If
                        If txtCODate.Text <> "" Then
                            sParams += ", @CashOutDate='" & stxtCODate & "'"
                        Else
                            sParams += ", @CashOutDate=NULL"
                        End If

                        If stxtWorkEndDate <> "" Then
                            sParams += ", @WorkEndDate='" & stxtWorkEndDate & "'"
                        Else
                            sParams += ", @WorkEndDate=NULL"
                        End If

                        If stxtWorkStartDate <> "" Then
                            sParams += ", @WorkStartDate='" & stxtWorkStartDate & "'"
                        Else
                            sParams += ", @WorkStartDate=NULL"
                        End If
                        sParams += ", @RepairScheduleChangeReasonID='" & TryCast(item.FindControl("hidRepairScheduleChangeReasonID"), HiddenField).Value & "'"
                        sParams += ", @RepairScheduleChangeReason='" & TryCast(item.FindControl("hidRepairScheduleChangeReason"), HiddenField).Value & "'"
                    Case "PS"
                        If txtFinalEstDate.Text <> "" Then
                            sParams += ", @AppraiserInvoiceDate='" & stxtAppraiserInvoiceDate & "'"
                        Else
                            sParams += ", @AppraiserInvoiceDate=NULL"
                        End If
                        If txtCODate.Text <> "" Then
                            sParams += ", @CashOutDate='" & stxtCODate & "'"
                        Else
                            sParams += ", @CashOutDate=NULL"
                        End If

                        If stxtWorkEndDate <> "" Then
                            sParams += ", @WorkEndDate='" & stxtWorkEndDate & "'"
                        Else
                            sParams += ", @WorkEndDate=NULL"
                        End If

                        If stxtWorkStartDate <> "" Then
                            sParams += ", @WorkStartDate='" & stxtWorkStartDate & "'"
                        Else
                            sParams += ", @WorkStartDate=NULL"
                        End If
                        ' sParams += ", @WorkStartDate='" & stxtWorkStartDate & "'"
                        sParams += ", @RepairScheduleChangeReasonID='" & TryCast(item.FindControl("hidRepairScheduleChangeReasonID"), HiddenField).Value & "'"
                        sParams += ", @RepairScheduleChangeReason='" & TryCast(item.FindControl("hidRepairScheduleChangeReason"), HiddenField).Value & "'"
                    Case "CS"
                        If txtFinalEstDate.Text <> "" Then
                            sParams += ", @AppraiserInvoiceDate='" & stxtAppraiserInvoiceDate & "'"
                        Else
                            sParams += ", @AppraiserInvoiceDate=NULL"
                        End If
                        If txtCODate.Text <> "" Then
                            sParams += ", @CashOutDate='" & stxtCODate & "'"
                        Else
                            sParams += ", @CashOutDate=NULL"
                        End If

                        If stxtWorkEndDate <> "" Then
                            sParams += ", @WorkEndDate='" & stxtWorkEndDate & "'"
                        Else
                            sParams += ", @WorkEndDate=NULL"
                        End If

                        If stxtWorkStartDate <> "" Then
                            sParams += ", @WorkStartDate='" & stxtWorkStartDate & "'"
                        Else
                            sParams += ", @WorkStartDate=NULL"
                        End If

                        If stxtWorkStartDate <> "" Then
                            sParams += ", @InspectionDate='" & stxtInspectionDate & "'"
                        Else
                            sParams += ", @InspectionDate=NULL"
                        End If

                        sParams += ", @RepairScheduleChangeReasonID='" & TryCast(item.FindControl("hidRepairScheduleChangeReasonID"), HiddenField).Value & "'"
                        sParams += ", @RepairScheduleChangeReason='" & TryCast(item.FindControl("hidRepairScheduleChangeReason"), HiddenField).Value & "'"
                    Case "RRP"
                        If stxtWorkStartDate <> "" Then
                            sParams += ", @InspectionDate='" & stxtInspectionDate & "'"
                        Else
                            sParams += ", @InspectionDate=NULL"
                        End If
                        sParams += ", @RepairScheduleChangeReasonID='" & TryCast(item.FindControl("hidRepairScheduleChangeReasonID"), HiddenField).Value & "'"
                        sParams += ", @RepairScheduleChangeReason='" & TryCast(item.FindControl("hidRepairScheduleChangeReason"), HiddenField).Value & "'"
                        sParams += ", @SourceApplicationPassThruData=" & TryCast(item.FindControl("txtSourceApplicationPassThruData"), TextBox).Text
                End Select

                sParams += ", @UserID=" & sUserID
                sParams += ", @SysLastUpdatedDate='" & TryCast(item.FindControl("hidAssignmentSysLastUpdatedDate"), HiddenField).Value & "'"

                '-------------------------------
                ' Process DB Call
                '-------------------------------  
                Dim sStoredProcedure As String = "uspAssignmentUpdRemarks"

                '-------------------------------
                ' Call WS and get data
                '-------------------------------
                sReturnData = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

                '-------------------------------
                ' Check if update was successful
                '-------------------------------
                If sReturnData.Contains("Error") And sReturnData.Contains("|") Then
                    aRC = sReturnData.Split("|")
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + aRC(1) + " ');", True)
                    'Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
                Else
                    If (sender.id = "btnRepairStart") Or (sender.id = "btnRepairEnd") Then
                    Else
                        olblAssignmentSaved.Text = "Saved..."
                        Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
                    End If
                    Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
                End If
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("AssignmentUpdRemarks", "ERROR", "Assignment Remarks Detail not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
            ' lblClaimSaved.Text = "Saved..."
        End Try

        'lstVeh.ShopAssignmentData.ServiceChannelCD
    End Sub

    Protected Sub btnInsuredSave_Click(sender As Object, e As EventArgs)
        Dim sReturnData As String = ""
        Dim oXMLNode As XmlNode = Nothing
        Dim aRC As Array = Nothing
        Dim sDayPhoneArea As String = ""
        Dim sDayPhoneExch As String = ""
        Dim sDayPhoneExt As String = ""
        Dim sDayPhoneUnit As String = ""
        Dim sNightPhoneArea As String = ""
        Dim sNightPhoneExch As String = ""
        Dim sNightPhoneExt As String = ""
        Dim sNightPhoneUnit As String = ""
        Dim sAltPhoneArea As String = ""
        Dim sAltPhoneExch As String = ""
        Dim sAltPhoneExt As String = ""
        Dim sAltPhoneUnit As String = ""
        Dim sParams As String = ""
        Dim sContactPhoneCD As String = ""

        Try
            '-------------------------------
            ' Validate the web page data
            '-------------------------------  
            '??????????? CODE THIS ????????????????????
            lblInsuredSave.Text = ""

            sDayPhoneArea = GetPhoneData(txtDay.Text, "AreaCode")
            sDayPhoneExch = GetPhoneData(txtDay.Text, "ExchangeNumber")
            sDayPhoneUnit = GetPhoneData(txtDay.Text, "UnitNumber")

            sNightPhoneArea = GetPhoneData(txtNight.Text, "AreaCode")
            sNightPhoneExch = GetPhoneData(txtNight.Text, "ExchangeNumber")
            sNightPhoneUnit = GetPhoneData(txtNight.Text, "UnitNumber")

            sAltPhoneArea = GetPhoneData(txtAlternate.Text, "AreaCode")
            sAltPhoneExch = GetPhoneData(txtAlternate.Text, "ExchangeNumber")
            sAltPhoneUnit = GetPhoneData(txtAlternate.Text, "UnitNumber")

            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim sStoredProcedure As String = "uspClaimInsuredUpdDetail"

            sParams = "@InvolvedID=" & hidInsInvolvedID.Value _
            & ", @Address1='" & txtAddress1.Text & "'" _
            & ", @Address2='" & txtAddress2.Text & "'" _
            & ", @AddressCity='" & txtAddressCity.Text & "'" _
            & ", @AddressState='" & ddlAddressState.SelectedValue & "'" _
            & ", @AddressZip='" & txtAddressZip.Text & "'" _
            & ", @AlternateAreaCode='" & sAltPhoneArea & "'" _
            & ", @AlternateExchangeNumber='" & sAltPhoneExch & "'" _
            & ", @AlternateExtensionNumber='" & sAltPhoneExt & "'" _
            & ", @AlternateUnitNumber='" & sAltPhoneUnit & "'" _
            & ", @BestContactPhoneCD='" & ddlBestContactPhoneCD.SelectedValue & "'" _
            & ", @BestContactTime='" & txtBestContactTime.Text & "'" _
            & ", @BusinessName='" & txtBusinessName.Text & "'" _
            & ", @DayAreaCode='" & sDayPhoneArea & "'" _
            & ", @DayExchangeNumber='" & sDayPhoneExch & "'" _
            & ", @DayExtensionNumber='" & sDayPhoneExt & "'" _
            & ", @DayUnitNumber='" & sDayPhoneUnit & "'" _
            & ", @EmailAddress='" & txtEmailAddress.Text & "'" _
            & ", @NameFirst='" & txtNameFirst.Text & "'" _
            & ", @NameLast='" & txtNameLast.Text & "'" _
            & ", @NameTitle='" & txtNameTitle.Text & "'" _
            & ", @NightAreaCode='" & sNightPhoneArea & "'" _
            & ", @NightExchangeNumber='" & sNightPhoneExch & "'" _
            & ", @NightExtensionNumber='" & sNightPhoneExt & "'" _
            & ", @NightUnitNumber='" & sNightPhoneUnit & "'" _
            & ", @FedTaxID='" & hidInsFedTaxID.Value & "'" _
            & ", @UserID=" & sUserID _
            & ", @SysLastUpdatedDate='" & hidInsSysLastUpdatedDate.Value & "'"

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            sReturnData = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

            '-------------------------------
            ' Check if update was successful
            '-------------------------------
            If sReturnData.Contains("ERROR") And sReturnData.Contains("|") Then
                aRC = sReturnData.Split("|")
                Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + aRC(1) + " ');", True)
            Else
                lblInsuredSave.Text = "Saved..."
                'Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
            End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("ClaimInsuredSaveDetails", "ERROR", "Claim Insured Save Details not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
        End Try
    End Sub

    Protected Sub btnClaimSave_Click(sender As Object, e As EventArgs) Handles btnClaimSave.Click
        Dim sReturnData As String = ""
        Dim oXMLNode As XmlNode = Nothing
        Dim aRC As Array = Nothing

        Try
            '-------------------------------
            ' Validate the web page data
            '-------------------------------  
            '??????????? CODE THIS ????????????????????
            lblClaimSaved.Text = ""

            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim sStoredProcedure As String = "uspClaimCondUpdDetail"

            Dim sParams As String = sLynxID & ",'" & txtPolicyNumber.Text & "','" & txtClientClaimNumber.Text & "','" & txtClientClaimNumber.Text & "','" & ddlState.SelectedValue & "','" & txtLossDescription.Text & "','" & txtRemarks.Text & "'," & sUserID & ",'" & hidClaimSysLastUpdatedDate.Value & "'"
            'Dim sParams As String = "1606985,'Policy','12345601-83','12345601-83','GA','Loss','Remarks',5849,'2018-04-10 14:10:10'"

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "EXECSP", "btnClaimSave_Click - Start: " & Date.Now, "StoredProc: " & sStoredProcedure & " Params: " & sParams, "")
            End If

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            sReturnData = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

            '-------------------------------
            ' Check if update was successful
            '-------------------------------
            If sReturnData.Contains("ERROR") And sReturnData.Contains("|") Then
                aRC = sReturnData.Split("|")
                Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + aRC(1) + " ');", True)
            Else
                If (sender.id = "btnRepairStart") Or (sender.id = "btnRepairEnd") Then
                Else
                    lblClaimSaved.Text = "Saved..."
                    Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
                End If
            End If

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNET-ClaimXP", "EXECSP", "btnClaimSave_Click - Ended: " & Date.Now, "Result: " & sReturnData, "")
            End If

            '-------------------------------
            ' Check if update was successful
            '-------------------------------
            '???????????? Add this code ????????????????
            'oXMLNode = oReturnXML.SelectSingleNode("Crud")

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("ClaimSaveDetails", "ERROR", "Claim Save Details not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
            lblClaimSaved.Text = "Saved..."
        End Try

    End Sub

    Protected Sub btnCoverageAddSave_Click(sender As Object, e As EventArgs) Handles btnCoverageAddSave.Click
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim sLimitDailyAmt As String = 0
        Dim sMaximumDays As String = 0
        Dim aCoverageParams As Array = Nothing

        Try
            '-------------------------------
            ' Validate the web page data
            '-------------------------------  
            '??????????? CODE THIS ????????????????????
            lblClaimSaved.Text = ""

            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim sStoredProcedure As String = "uspClaimCoverageUpdDetail"

            sClaimCoverageID = "0"
            aCoverageParams = HIDCoverageParams.Value.Split("|")

            Dim sParams As String = "@ClaimCoverageID = " & sClaimCoverageID _
                & ", @ClientCoverageTypeID = " & aCoverageParams(2) _
                & ", @LynxID = " & sLynxID _
                & ", @InsuranceCompanyID = " & sInscCompany _
                & ", @AddtlCoverageFlag = " & aCoverageParams(3) _
                & ", @CoverageTypeCD = '" & aCoverageParams(0) & "'" _
                & ", @Description = '" & aCoverageParams(1) & "'" _
                & ", @DeductibleAmt = " & txtCoverageDeductible.Text _
                & ", @LimitAmt = " & txtCoverageLimit.Text _
                & ", @LimitDailyAmt = " & txtCoverageDailyLimit.Text _
                & ", @MaximumDays = " & txtCoverageMaxDays.Text _
                & ", @UserID = " & sUserID _
                & ", @SysLastUpdatedDate = " & "''"

            '?????? Code This ?????
            '    oNode.setAttribute("AddlCoverageFlagInd", (txtAdditionalFlag.value == "Yes" ? "/images/tick.gif" : "/images/spacer.gif"));

            'Debug.Print(sParams)

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '???? check for success ????

            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("CoverageAddSaveDetails", "ERROR", "Add Coverage Save not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
        End Try
    End Sub

    Protected Sub btnShopSelect_Click(sender As Object, e As EventArgs) Handles btnCoverageAddSave.Click
        Debug.Print("Ready")
        'Dim oReturnXML As XmlElement = Nothing
        'Dim oXMLNode As XmlNode = Nothing
        'Dim sLimitDailyAmt As String = 0
        'Dim sMaximumDays As String = 0
        'Dim aCoverageParams As Array = Nothing

        'Try
        '    '-------------------------------
        '    ' Validate the web page data
        '    '-------------------------------  
        '    '??????????? CODE THIS ????????????????????
        '    lblClaimSaved.Text = ""

        '    '-------------------------------
        '    ' Process DB Call
        '    '-------------------------------  
        '    Dim sStoredProcedure As String = "uspClaimCoverageUpdDetail"

        '    sClaimCoverageID = "0"
        '    aCoverageParams = HIDCoverageParams.Value.Split("|")

        '    Dim sParams As String = "@ClaimCoverageID = " & sClaimCoverageID _
        '        & ", @ClientCoverageTypeID = " & aCoverageParams(2) _
        '        & ", @LynxID = " & sLynxID _
        '        & ", @InsuranceCompanyID = " & sInscCompany _
        '        & ", @AddtlCoverageFlag = " & aCoverageParams(3) _
        '        & ", @CoverageTypeCD = '" & aCoverageParams(0) & "'" _
        '        & ", @Description = '" & aCoverageParams(1) & "'" _
        '        & ", @DeductibleAmt = " & txtCoverageDeductible.Text _
        '        & ", @LimitAmt = " & txtCoverageLimit.Text _
        '        & ", @LimitDailyAmt = " & txtCoverageDailyLimit.Text _
        '        & ", @MaximumDays = " & txtCoverageMaxDays.Text _
        '        & ", @UserID = " & sUserID _
        '        & ", @SysLastUpdatedDate = " & "''"

        '    '?????? Code This ?????
        '    '    oNode.setAttribute("AddlCoverageFlagInd", (txtAdditionalFlag.value == "Yes" ? "/images/tick.gif" : "/images/spacer.gif"));

        '    'Debug.Print(sParams)

        '    '-------------------------------
        '    ' Call WS and get data
        '    '-------------------------------
        '    oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

        '    '???? check for success ????

        '    Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)

        'Catch oExcept As Exception
        '    '---------------------------------
        '    ' Error handler and notifications
        '    '---------------------------------
        '    Dim sError As String = ""
        '    Dim sBody As String = ""
        '    Dim FunctionName As New System.Diagnostics.StackFrame

        '    sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
        '    wsAPDFoundation.LogEvent("CoverageAddSaveDetails", "ERROR", "Add Coverage Save not processed or data error occured.", oExcept.Message, sError)

        '    Response.Write(oExcept.ToString)

        'Finally
        '    oXMLNode = Nothing
        'End Try
    End Sub

    Protected Sub btnShopCancel_Click(sender As Object, e As EventArgs)
        Dim sReturnString As String = ""
        Dim sParams As String = ""
        Dim sShopRemarks As String = ""
        Dim sClaimAspectServiceChannelID As String = ""
        Dim sVehNum As String = ""
        Dim sShopLocationID As String = ""
        Dim aRC As Array = Nothing

        Try
            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim btn As Button = TryCast(sender, Button)
            If btn IsNot Nothing Then
                '--------------------------
                ' Access the repeater data
                '--------------------------
                Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)
                sShopRemarks = TryCast(item.FindControl("txtAssignmentRemarks"), TextBox).Text
                sClaimAspectServiceChannelID = TryCast(item.FindControl("hidClaimAspectServiceChannelID"), HiddenField).Value
                sVehNum = TryCast(item.FindControl("hidVehNumber"), HiddenField).Value
                sShopLocationID = TryCast(item.FindControl("hidShopLocationID"), HiddenField).Value
            End If

            '----------------------------
            ' Validate Assignment State
            ' And send workflow notify
            '----------------------------
            If sClaimAspectServiceChannelID <> "" Then
                'do Not send the vehicle number Or the LynxID when the claimAspectID Is available
                sParams = "@ClaimAspectServiceChannelID=" & sClaimAspectServiceChannelID & ", @SelectOperationCD='C', @UserId=" & sUserID & ", @AssignmentRemarks='" & sShopRemarks & "', @NotifyEvent=1"
                sParams += ", @AppraiserID=NULL"
            Else
                'when claimAspectID Is Not available then send in the LynxID And the vehicle Number.
                sParams = "@LynxId=" & sLynxID & ", @VehicleNumber=" & sVehNum & ", @ClaimAspectServiceChannelID=" & sClaimAspectServiceChannelID & ", @SelectOperationCD='C', @UserId=" & sUserID & ", @AssignmentRemarks='" & sShopRemarks & "', @NotifyEvent=1"
                sParams += ", @ShopLocationID=" & sShopLocationID
            End If

            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim sStoredProcedure As String = "uspWorkFlowSelectShop"

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            sReturnString = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

            '-------------------------------
            ' Check if update was successful
            '-------------------------------
            If sReturnString.Contains("ERROR") And sReturnString.Contains("|") Then
                aRC = (sReturnString).Split("|")
                Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + aRC(1) + " ');", True)
            Else
                'olblAssignmentSaved.Text = "Saved..."
                Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
            End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("btnShopCancel_Click", "ERROR", "Cancel Assignment Detail not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            lblClaimSaved.Text = "Cancelled..."
        End Try
    End Sub

    Protected Sub InsuredEnableDisable(ByVal bEnableInsuredEdit As Boolean)
        txtNameTitle.Enabled = bEnableInsuredEdit
        txtNameFirst.Enabled = bEnableInsuredEdit
        txtNameLast.Enabled = bEnableInsuredEdit
        txtBusinessName.Enabled = bEnableInsuredEdit
        txtAddress1.Enabled = bEnableInsuredEdit
        txtAddress2.Enabled = bEnableInsuredEdit
        txtAddressCity.Enabled = bEnableInsuredEdit
        ddlAddressState.Enabled = bEnableInsuredEdit
        txtAddressZip.Enabled = bEnableInsuredEdit
        txtEmailAddress.Enabled = bEnableInsuredEdit
        txtFedTaxID.Enabled = bEnableInsuredEdit
        ddlBestContactPhoneCD.Enabled = bEnableInsuredEdit
        txtBestContactTime.Enabled = bEnableInsuredEdit
        txtDay.Enabled = bEnableInsuredEdit
        txtNight.Enabled = bEnableInsuredEdit
        txtAlternate.Enabled = bEnableInsuredEdit

        If bEnableInsuredEdit = True Then
            btnInsuredSave.Visible = True
        Else
            btnInsuredSave.Visible = False
        End If
    End Sub

    Protected Sub OnRowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            'e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(gvCoverage, "Select$" & e.Row.RowIndex)
            'e.Row.ToolTip = "Click to select this row."
        End If
    End Sub

    'Protected Sub OnSelectedIndexChanged(sender As Object, e As EventArgs)
    '    For Each row As GridViewRow In gvCoverage.Rows
    '        If row.RowIndex = gvCoverage.SelectedIndex Then
    '            row.BackColor = ColorTranslator.FromHtml("#A1DCF2")
    '            row.ToolTip = String.Empty
    '        Else
    '            row.BackColor = ColorTranslator.FromHtml("#FFFFFF")
    '            row.ToolTip = "Click to select this row."
    '        End If
    '    Next
    'End Sub

    'Private Sub gvCoverage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvCoverage.SelectedIndexChanged
    '    Debug.Print("ok")
    'End Sub

    Protected Sub gvCoverage_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        Dim sOriginalBackgroundColor As String = ""
        Dim sCoverageDescription As String = ""
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            'sOriginalBackgroundColor = gvCoverage.BackColor.Name
            'e.Row.Attributes("onmouseover") = "this.style.backgroundColor='aquamarine';"
            ' e.Row.Attributes("onmouseout") = "this.style.backgroundColor='" & sOriginalBackgroundColor & "';"
            'e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(gvCoverage, "Select$" + e.Row.RowIndex.ToString()))
            Dim gvcoverage As GridView = TryCast(sender, GridView)
            If gvcoverage IsNot Nothing Then
                If (e.Row.RowType = DataControlRowType.DataRow) Then
                    sCoverageDescription = TryCast(e.Row.FindControl("hidCoverageDescription"), HiddenField).Value
                    e.Row.Attributes.Add("onClick", "return setCoverageValue(this,'" + gvcoverage.ClientID + "'," + (e.Row.Cells(0)).Text + ",'" + (e.Row.Cells(2)).Text + "','" + (e.Row.Cells(1)).Text + "')")
                    e.Row.Attributes.Add("ondblclick", "return dblClicksetCoverageValue(this,'" + gvcoverage.ClientID + "','" + (e.Row.Cells(0)).Text + "','" + (e.Row.Cells(1)).Text + "','" + (e.Row.Cells(2)).Text + "','" + sCoverageDescription + "','" + (e.Row.Cells(4)).Text + "','" + (e.Row.Cells(5)).Text + "','" + (e.Row.Cells(6)).Text + "','" + (e.Row.Cells(7)).Text + "','" + (e.Row.Cells(8)).Text + "','" + (e.Row.Cells(9)).Text + "')")
                End If
            End If
            'Dim lbDoubleClickButton As LinkButton = CType(e.Row.Cells(1).Controls(0), LinkButton)
            ' Get the javascript which is assigned to this LinkButton
            'Dim _jsDouble As String = ClientScript.GetPostBackClientHyperlink(lbDoubleClickButton, "")
            ' Add this JavaScript to the ondblclick Attribute of the row
            'e.Row.Attributes("ondblclick") = _jsDouble
        End If
    End Sub

    'Protected Sub gvCoverage_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim sSelectedRowType As String = ""
    '    Dim sCRUD As String = ""

    '    hidCoverageSelected.Value = "1"

    '    ' Get the row selected
    '    sSelectedRowType = gvCoverage.SelectedRow.Cells(1).Text
    '    sSelectedClaimCoverageID = gvCoverage.SelectedRow.Cells(1).Text
    '    sSelectedClientCoverageTypeID = gvCoverage.SelectedRow.Cells(1).Text
    '    sCoverageSysLastUpdatedDate = gvCoverage.SelectedRow.Cells(1).Text

    '    sParams = gvCoverage.SelectedRow.Cells(0).Text & "|" & gvCoverage.SelectedRow.Cells(1).Text & "|" & gvCoverage.SelectedRow.Cells(2).Text & "|" & gvCoverage.SelectedRow.Cells(3).Text & "|" & gvCoverage.SelectedRow.Cells(4).Text & "|" & gvCoverage.SelectedRow.Cells(5).Text & "|" & gvCoverage.SelectedRow.Cells(6).Text & "|" & gvCoverage.SelectedRow.Cells(7).Text & "|" & gvCoverage.SelectedRow.Cells(8).Text

    '    'gvCoverage.SelectedRow.Cells(1).BackColor = Color.FromName("yellow")
    '    gvCoverage.SelectedRowStyle.BackColor = Color.FromName("yellow")
    '    'gvCoverage.RowStyle.BackColor = Color.FromName("yellow")
    '    gvCoverage.SelectedRowStyle.ForeColor = Color.FromName("red")

    '    '-------------------------------
    '    ' Populate Coverage Edit Dialog 
    '    '-------------------------------
    '    txtEditCoverageClientCode.Text = gvCoverage.SelectedRow.Cells(1).Text
    '    txtEditCoverageName.Text = gvCoverage.SelectedRow.Cells(0).Text
    '    txtEditCoverageType.Text = gvCoverage.SelectedRow.Cells(1).Text
    '    txtEditCoverageAdditionalFlag.Text = gvCoverage.SelectedRow.Cells(2).Text
    '    txtEditCoverageDeductible.Text = gvCoverage.SelectedRow.Cells(3).Text
    '    txtEditCoverageLimit.Text = gvCoverage.SelectedRow.Cells(4).Text

    '    ' Get CRUD Data
    '    '-------------------------------
    '    'sCRUD = GetCRUD("Coverage", sUserID)

    '    '-------------------------------
    '    ' CRUD Permissions 
    '    '-------------------------------
    '    'If sCRUD.Substring(4, 1) = 0 Then

    '    '    'ClientWarning("You do not have permission to delete coverage. Please contact your supervisor.");
    '    'End If



    '    '---------------------------------------
    '    ' Validations
    '    '---------------------------------------
    '    'If (oXML.getAttribute("CoverageApplied") == "1") Then{
    '    '          ClientWarning("Deductible/Limit has been applied to this coverage. Cannot delete.");
    '    '          Return;
    '    '       }

    '    ''---------------------------------------
    '    '' Execute
    '    ''---------------------------------------

    '    'var sProc = "uspClaimCoverageDel";
    '    '          strClaimCoverageID = oXML.getAttribute("ClaimCoverageID");
    '    '          sRequest = "ClaimCoverageID=" + strClaimCoverageID +
    '    '                     "&ClientCoverageTypeID=" + oXML.getAttribute("ClientCoverageTypeID") +
    '    '                     "&LynxID=" + gsLynxID +
    '    '                     "&UserID=" + gsUserID +
    '    '                     "&SysLastUpdatedDate=" + oXML.getAttribute("SysLastUpdatedDate");
    '    '          //alert(sRequest);



    'End Sub

    Protected Sub btnSaveInvolved_Click(sender As Object, e As EventArgs)

        Dim strClaimAspectID As String = "", strinvolvedID As String = "", strNameTitle As String = "", strNameFirst As String = "", strNameLast As String = "", strBusinessName As String = "", strBusinessType As String = "", strAddress1 As String = "", strAddress2 As String = ""
        Dim strAddressCity As String = "", strAddressZip As String = "", strState As String = "", strEmailAddress As String = "", strcbInsured As String = "", strcbOwner As String = "", strcbClaimant As String = "", strcbDriver As String = "", strcbPassenger As String = ""
        Dim strGender As String = "", strFedTaxId As String = "", strBirthDate As String = "", strAge As String = "", strBestNumberCall As String = "", strBestTimeToCall As String = "", strPhone As String = ""
        Dim strPhoneDayAreaCode As String = "", strPhoneDayExchangeNumber As String = "", strPhoneDayExtensionNumber As String = "", strPhoneDayUnitNumber As String = ""
        Dim strPhoneNightAreaCode As String = "", strPhoneNightExchangeNumber As String = "", strPhoneNightExtensionNumber As String = "", strPhoneNightUnitNumber As String = ""
        Dim strAltAreaCode As String = "", strAltExchangeNumber As String = "", strAltExtensionNumber As String = "", strAltUnitNumber As String = ""

        Dim sParams As String, strInvolvedTypeList As String = "", struserID As String = "", StrSysLastUpdatedDate As String = "", sStoredProcedure As String = ""

        Dim sRC As XmlNode
        Dim btn As Button = TryCast(sender, Button)
        If btn IsNot Nothing Then
            Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)
            'Now you can use FindControl to get to the controls in the found item, for example a Label with ID: Label1

            strClaimAspectID = TryCast(item.FindControl("hidClaimAspectID"), HiddenField).Value
            strinvolvedID = TryCast(item.FindControl("ddlInvolved"), DropDownList).SelectedValue
            strNameTitle = TryCast(item.FindControl("txtInvolvedNameTitle"), TextBox).Text
            strNameFirst = TryCast(item.FindControl("txtInvolvedNameFirst"), TextBox).Text
            strNameLast = TryCast(item.FindControl("txtInvolvedNameLast"), TextBox).Text
            strBusinessName = TryCast(item.FindControl("txtInvolvedBusinessName"), TextBox).Text
            strBusinessType = TryCast(item.FindControl("ddlInvolvedBusinessType"), DropDownList).SelectedValue
            strAddress1 = TryCast(item.FindControl("txtInvolvedAddress1"), TextBox).Text
            strAddress2 = TryCast(item.FindControl("txtInvolvedAddress2"), TextBox).Text
            strAddressCity = TryCast(item.FindControl("txtInvolvedAddressCity"), TextBox).Text
            strAddressZip = TryCast(item.FindControl("txtInvolvedAddressZip"), TextBox).Text
            strState = TryCast(item.FindControl("ddlInvolvedState"), DropDownList).SelectedValue
            strEmailAddress = TryCast(item.FindControl("txtInvolvedEmailAddress"), TextBox).Text

            If (TryCast(item.FindControl("cbInsured"), CheckBox).Enabled) Then
                If TryCast(item.FindControl("cbInsured"), CheckBox).Checked Then
                    strInvolvedTypeList = strInvolvedTypeList + "3,"
                End If
            End If

            If (TryCast(item.FindControl("cbClaimant"), CheckBox).Enabled) Then
                If TryCast(item.FindControl("cbClaimant"), CheckBox).Checked Then
                    strInvolvedTypeList = strInvolvedTypeList + "6,"
                End If
            End If

            If TryCast(item.FindControl("cbOwner"), CheckBox).Checked Then
                strInvolvedTypeList = strInvolvedTypeList + "4,"
            End If

            If TryCast(item.FindControl("cbDriver"), CheckBox).Checked Then
                strInvolvedTypeList = strInvolvedTypeList + "1,"
            End If

            If TryCast(item.FindControl("cbPassenger"), CheckBox).Checked Then
                strInvolvedTypeList = strInvolvedTypeList + "5,"
            End If

            If strInvolvedTypeList <> "" Then
                strInvolvedTypeList = strInvolvedTypeList.Substring(0, strInvolvedTypeList.LastIndexOf(","))
            End If


            strGender = TryCast(item.FindControl("ddlInvolvedGender"), DropDownList).SelectedValue
            strFedTaxId = TryCast(item.FindControl("txtInvolvedFedTaxId"), TextBox).Text
            strBirthDate = TryCast(item.FindControl("txtInvolvedBirthDate"), TextBox).Text
            strAge = TryCast(item.FindControl("txtInvolvedAge"), TextBox).Text
            strBestNumberCall = TryCast(item.FindControl("ddlBestNumberCall"), DropDownList).SelectedValue
            strBestTimeToCall = TryCast(item.FindControl("txtInvolvedBestTimeToCall"), TextBox).Text

            strPhoneDayAreaCode = GetPhoneData(TryCast(item.FindControl("txtInvolvedPhoneDay"), TextBox).Text, "AreaCode")
            strPhoneDayExchangeNumber = GetPhoneData(TryCast(item.FindControl("txtInvolvedPhoneDay"), TextBox).Text, "ExchangeNumber")
            strPhoneDayUnitNumber = GetPhoneData(TryCast(item.FindControl("txtInvolvedPhoneDay"), TextBox).Text, "UnitNumber")
            strPhoneDayExtensionNumber = GetPhoneData(TryCast(item.FindControl("txtInvolvedPhoneDay"), TextBox).Text, "ExtensionNumber")

            strPhoneNightAreaCode = GetPhoneData(TryCast(item.FindControl("txtInvolvedPhoneNight"), TextBox).Text, "AreaCode")
            strPhoneNightExchangeNumber = GetPhoneData(TryCast(item.FindControl("txtInvolvedPhoneNight"), TextBox).Text, "ExchangeNumber")
            strPhoneNightUnitNumber = GetPhoneData(TryCast(item.FindControl("txtInvolvedPhoneNight"), TextBox).Text, "UnitNumber")
            strPhoneNightExtensionNumber = GetPhoneData(TryCast(item.FindControl("txtInvolvedPhoneNight"), TextBox).Text, "ExtensionNumber")

            strAltAreaCode = GetPhoneData(TryCast(item.FindControl("txtAlt"), TextBox).Text, "AreaCode")
            strAltExchangeNumber = GetPhoneData(TryCast(item.FindControl("txtAlt"), TextBox).Text, "ExchangeNumber")
            strAltUnitNumber = GetPhoneData(TryCast(item.FindControl("txtAlt"), TextBox).Text, "UnitNumber")
            strAltExtensionNumber = GetPhoneData(TryCast(item.FindControl("txtAlt"), TextBox).Text, "ExtensionNumber")


            StrSysLastUpdatedDate = TryCast(item.FindControl("hidInvolvedSysLastUpdatedDate"), HiddenField).Value

            If strinvolvedID = "-1" Then
                sStoredProcedure = "uspCondVehInvolvedInsDetail"

                sParams = "@ClaimAspectID = {0},@Address1 = '{1}',@Address2 = '{2}',@AddressCity = '{3}',@AddressState = '{4}',@AddressZip = '{5}',
                         @AlternateAreaCode = '{6}',@AlternateExchangeNumber = '{7}',@AlternateExtensionNumber = '{8}',@AlternateUnitNumber = '{9}',@BestContactPhoneCD = '{10}',
		                 @BestContactTime = '{11}',@BirthDate ='{12}',@Age = '{13}',@BusinessName ='{14}',@BusinessTypeCD = '{15}',@DayAreaCode = '{16}',@DayExchangeNumber = '{17}',
		                 @DayExtensionNumber ='{18}',@DayUnitNumber = '{19}',@EmailAddress = '{20}',@FedTaxID = '{21}',@GenderCD = '{22}',@InvolvedTypeList ='{23}',@NameFirst = '{24}',
		                 @NameLast = '{25}',@NameTitle = '{26}',@NightAreaCode = '{27}',@NightExchangeNumber = '{28}',@NightExtensionNumber = '{29}',@NightUnitNumber = '{30}',@UserID = '{31}'"


                sParams = String.Format(sParams, strClaimAspectID, strAddress1, strAddress2, strAddressCity, strState, strAddressZip,
                                     strAltAreaCode, strAltExchangeNumber, strAltExtensionNumber, strAltUnitNumber, strBestNumberCall, strBestTimeToCall, strBirthDate, strAge, strBusinessName, strBusinessType,
                                     strPhoneDayAreaCode, strPhoneDayExchangeNumber, strPhoneDayExtensionNumber, strPhoneDayUnitNumber, strEmailAddress, strFedTaxId, strGender, strInvolvedTypeList, strNameFirst,
                                     strNameLast, strNameTitle, strPhoneNightAreaCode, strPhoneNightExchangeNumber, strPhoneNightExtensionNumber, strPhoneNightUnitNumber, sUserID)

            Else
                sStoredProcedure = "uspCondVehInvolvedUpdDetail"

                sParams = "@ClaimAspectID = {0},@InvolvedID = {1},@Address1 = '{2}',@Address2 = '{3}',@AddressCity = '{4}',@AddressState = '{5}',@AddressZip = '{6}',
                         @AlternateAreaCode = '{7}',@AlternateExchangeNumber = '{8}',@AlternateExtensionNumber = '{9}',@AlternateUnitNumber = '{10}',@BestContactPhoneCD = '{11}',
		                 @BestContactTime = '{12}',@BirthDate ='{13}',@Age = '{14}',@BusinessName ='{15}',@BusinessTypeCD = '{16}',@DayAreaCode = '{17}',@DayExchangeNumber = '{18}',
		                 @DayExtensionNumber ='{19}',@DayUnitNumber = '{20}',@EmailAddress = '{21}',@FedTaxID = '{22}',@GenderCD = '{23}',@InvolvedTypeList ='{24}',@NameFirst = '{25}',
		                 @NameLast = '{26}',@NameTitle = '{27}',@NightAreaCode = '{28}',@NightExchangeNumber = '{29}',@NightExtensionNumber = '{30}',@NightUnitNumber = '{31}',@UserID = '{32}',
                         @SysLastUpdatedDate = '{33}'"

                sParams = String.Format(sParams, strClaimAspectID, strinvolvedID, strAddress1, strAddress2, strAddressCity, strState, strAddressZip,
                                     strAltAreaCode, strAltExchangeNumber, strAltExtensionNumber, strAltUnitNumber, strBestNumberCall, strBestTimeToCall, strBirthDate, strAge, strBusinessName, strBusinessType,
                                     strPhoneDayAreaCode, strPhoneDayExchangeNumber, strPhoneDayExtensionNumber, strPhoneDayUnitNumber, strEmailAddress, strFedTaxId, strGender, strInvolvedTypeList, strNameFirst,
                                     strNameLast, strNameTitle, strPhoneNightAreaCode, strPhoneNightExchangeNumber, strPhoneNightExtensionNumber, strPhoneNightUnitNumber, sUserID, StrSysLastUpdatedDate)

            End If

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            sRC = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '-------------------------------
            ' Check if update was successful
            '-------------------------------
            'If sRC = "1" Then
            '    oStatusLabel.Text = "Saved..."
            'Else
            '    oStatusLabel.Text = "Failed to Save..."
            'End If
            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)

        End If

    End Sub

    Protected Sub RepeaterVehTab_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim imgVehStatus As WebControls.Image = CType(e.Item.FindControl("imgVehStatus"), WebControls.Image)

            Dim lstVeh As Vehicle = New Vehicle()
            lstVeh = CType(e.Item.DataItem, Vehicle)

            Select Case (lstVeh.Status)
                Case "Vehicle Closed"
                    imgVehStatus.ImageUrl = "images/closedExposure.gif"
                Case "Vehicle Cancelled"
                    imgVehStatus.ImageUrl = "images/cancelled.gif"
                Case "Vehicle Voided"
                    imgVehStatus.ImageUrl = "images/voided.gif"
                Case "Open"
                    imgVehStatus.ImageUrl = "images/blank-sml.gif"
                Case Else
                    imgVehStatus.ImageUrl = "images/blank-sml.gif"
            End Select
        End If

    End Sub

    ''' <summary>
    ''' Delete involved 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub btnDeleteInvolved_Click(sender As Object, e As EventArgs)
        Dim strClaimAspectID As String = "", strInvolvedID As String = "", strSysLastUpdatedDate As String = ""
        Dim sParams As String, sStoredProcedure As String = "", sret As String = ""
        Dim btn As Button = TryCast(sender, Button)

        sStoredProcedure = "uspVehicleInvolvedDel"

        If btn IsNot Nothing Then
            Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)
            'Now you can use FindControl to get to the controls in the found item, for example a Label with ID: Label1

            strClaimAspectID = TryCast(item.FindControl("hidClaimAspectID"), HiddenField).Value
            strInvolvedID = TryCast(item.FindControl("ddlInvolved"), DropDownList).SelectedValue
            strSysLastUpdatedDate = TryCast(item.FindControl("hidInvolvedSysLastUpdatedDate"), HiddenField).Value


            sParams = "@InvolvedID=" + strInvolvedID + ", @ClaimAspectID=" + strClaimAspectID + " ,@UserID=" + hidUserID.Value + " ,@SysLastUpdatedDate='" + strSysLastUpdatedDate + "'"

            sret = wsAPDFoundation.ExecuteSp(sStoredProcedure, sParams)

            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)

        End If
    End Sub

    Protected Sub btnRepairStart_Click(sender As Object, e As EventArgs)
        Dim sParams As String = ""
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim sRC As String = ""
        Dim aRC As Array = Nothing
        Dim txtWorkStartDate = ""
        Dim txtWorkEndDate = ""

        Try
            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim btn As ImageButton = TryCast(sender, ImageButton)
            If btn IsNot Nothing Then
                '--------------------------
                ' Access the repeater data
                '--------------------------
                Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)

                '-------------------------------
                ' Process DB Call
                '-------------------------------  
                Dim sStoredProcedure As String = "uspRepairConfirmUpdDetail"

                '-------------------------------
                ' Save Assignment Data before
                ' processing
                '-------------------------------  
                txtWorkStartDate = TryCast(item.FindControl("txtWorkStartDate"), TextBox).Text
                txtWorkEndDate = TryCast(item.FindControl("txtWorkEndDate"), TextBox).Text

                If txtWorkStartDate <> "" And txtWorkEndDate <> "" Then
                    Call btnSaveAssignment_Click(TryCast(item.FindControl("btnRepairStart"), ImageButton), e)
                End If

                '--------------------------------
                ' Determine the service channel
                ' and build SP Params
                '--------------------------------
                sParams = "@ClaimAspectServiceChannelID=" & TryCast(item.FindControl("hidClaimAspectServiceChannelID"), HiddenField).Value
                sParams += ", @RepairDateCD='S' "
                sParams += ", @UserID=" & TryCast(item.FindControl("hidUserID"), HiddenField).Value

                '-------------------------------
                ' Call WS and get data
                '-------------------------------
                sRC = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

                '-------------------------------
                ' Check if update was successful
                '-------------------------------
                If sRC.Contains("Error") And sRC.Contains("|") Then
                    aRC = sRC.Split("|")
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + aRC(1) + " ');", True)
                Else
                    'olblAssignmentSaved.Text = "Saved..."
                    Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
                End If


            End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("bntRepairStart", "ERROR", "Repair Start not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
            lblClaimSaved.Text = "Saved..."
        End Try
    End Sub

    Protected Sub btnRepairEnd_Click(sender As Object, e As EventArgs)
        Dim sParams As String = ""
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim sRC As String = ""
        Dim aRC As Array = Nothing
        Dim txtWorkStartDate = ""
        Dim txtWorkEndDate = ""

        Try
            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim btn As ImageButton = TryCast(sender, ImageButton)
            If btn IsNot Nothing Then
                '--------------------------
                ' Access the repeater data
                '--------------------------
                Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)

                '-------------------------------
                ' Process DB Call
                '-------------------------------  
                Dim sStoredProcedure As String = "uspRepairConfirmUpdDetail"

                '-------------------------------
                ' Save Assignment Data before
                ' processing
                '-------------------------------  
                txtWorkStartDate = TryCast(item.FindControl("txtWorkStartDate"), TextBox).Text
                txtWorkEndDate = TryCast(item.FindControl("txtWorkEndDate"), TextBox).Text

                If txtWorkStartDate <> "" And txtWorkEndDate <> "" Then
                    Call btnSaveAssignment_Click(TryCast(item.FindControl("btnRepairEnd"), ImageButton), e)
                End If

                '--------------------------------
                ' Determine the service channel
                ' and build SP Params
                '--------------------------------
                sParams = "@ClaimAspectServiceChannelID=" & TryCast(item.FindControl("hidClaimAspectServiceChannelID"), HiddenField).Value
                sParams += ", @RepairDateCD='E' "
                sParams += ", @UserID=" & TryCast(item.FindControl("hidUserID"), HiddenField).Value

                '-------------------------------
                ' Call WS and get data
                '-------------------------------
                sRC = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

                '-------------------------------
                ' Check if update was successful
                '-------------------------------
                If sRC.Contains("Error") And sRC.Contains("|") Then
                    aRC = sRC.Split("|")
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + aRC(1) + " ');", True)
                Else
                    'olblAssignmentSaved.Text = "Saved..."
                    Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
                End If


            End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("bntRepairStart", "ERROR", "Repair Start not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
            lblClaimSaved.Text = "Saved..."
        End Try
    End Sub

    Protected Sub btnWarRepairStart_Click(sender As Object, e As EventArgs)
        Dim sParams As String = ""
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim sRC As String = ""
        Dim aRC As Array = Nothing
        Dim txtWorkStartDate = ""
        Dim txtWorkEndDate = ""

        Try
            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim btn As ImageButton = TryCast(sender, ImageButton)
            If btn IsNot Nothing Then
                '--------------------------
                ' Access the repeater data
                '--------------------------
                Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)

                '-------------------------------
                ' Process DB Call
                '-------------------------------  
                Dim sStoredProcedure As String = "uspRepairConfirmUpdDetail"

                '-------------------------------
                ' Save Assignment Data before
                ' processing
                '-------------------------------  
                txtWorkStartDate = TryCast(item.FindControl("txtWarRepairStartDate"), TextBox).Text
                txtWorkEndDate = TryCast(item.FindControl("txtWarRepairEndDate"), TextBox).Text

                If txtWorkStartDate <> "" And txtWorkEndDate <> "" Then
                    Call btnSaveAssignment_Click(TryCast(item.FindControl("btnWarRepairStart"), ImageButton), e)
                End If

                '--------------------------------
                ' Determine the service channel
                ' and build SP Params
                '--------------------------------
                sParams = "@ClaimAspectServiceChannelID=" & TryCast(item.FindControl("hidClaimAspectServiceChannelID"), HiddenField).Value
                sParams += ", @RepairDateCD='S' "
                sParams += ", @UserID=" & TryCast(item.FindControl("hidUserID"), HiddenField).Value

                '-------------------------------
                ' Call WS and get data
                '-------------------------------
                sRC = wsAPDFoundation.ExecuteSp(sStoredProcedure, sParams)

                '-------------------------------
                ' Check if update was successful
                '-------------------------------
                If sRC.Contains("Error") And sRC.Contains("|") Then
                    aRC = sRC.Split("|")
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + aRC(1) + " ');", True)
                Else
                    'olblAssignmentSaved.Text = "Saved..."
                    Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
                End If


            End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("btnWarRepairStart", "ERROR", "Repair Start not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
            lblClaimSaved.Text = "Saved..."
        End Try
    End Sub
    Protected Sub btnWarRepairEnd_Click(sender As Object, e As EventArgs)
        Dim sParams As String = ""
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim sRC As String = ""
        Dim aRC As Array = Nothing
        Dim txtWorkStartDate = ""
        Dim txtWorkEndDate = ""

        Try
            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim btn As ImageButton = TryCast(sender, ImageButton)
            If btn IsNot Nothing Then
                '--------------------------
                ' Access the repeater data
                '--------------------------
                Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)

                '-------------------------------
                ' Process DB Call
                '-------------------------------  
                Dim sStoredProcedure As String = "uspRepairConfirmUpdDetail"

                '-------------------------------
                ' Save Assignment Data before
                ' processing
                '-------------------------------  
                txtWorkStartDate = TryCast(item.FindControl("txtWarRepairStartDate"), TextBox).Text
                txtWorkEndDate = TryCast(item.FindControl("txtWarRepairEndDate"), TextBox).Text

                If txtWorkStartDate <> "" And txtWorkEndDate <> "" Then
                    Call btnSaveAssignment_Click(TryCast(item.FindControl("btnWarRepairStart"), ImageButton), e)
                End If

                '--------------------------------
                ' Determine the service channel
                ' and build SP Params
                '--------------------------------
                sParams = "@ClaimAspectServiceChannelID=" & TryCast(item.FindControl("hidClaimAspectServiceChannelID"), HiddenField).Value
                sParams += ", @RepairDateCD='E' "
                sParams += ", @UserID=" & TryCast(item.FindControl("hidUserID"), HiddenField).Value

                '-------------------------------
                ' Call WS and get data
                '-------------------------------
                sRC = wsAPDFoundation.ExecuteSp(sStoredProcedure, sParams)

                '-------------------------------
                ' Check if update was successful
                '-------------------------------
                If sRC.Contains("Error") And sRC.Contains("|") Then
                    aRC = sRC.Split("|")
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + aRC(1) + " ');", True)
                Else
                    'olblAssignmentSaved.Text = "Saved..."
                    Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
                End If


            End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("bntRepairStart", "ERROR", "Repair Start not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            oXMLNode = Nothing
            oReturnXML = Nothing
            lblClaimSaved.Text = "Saved..."
        End Try
    End Sub

    Protected Function SetCRUD(ByVal sCRUD As String) As String
        Dim sCRUDNow As String = ""

        If sCRUD.Substring(0, 1) = 1 Then sCRUDNow = "C" Else sCRUDNow = "_"
        If sCRUD.Substring(1, 1) = 1 Then sCRUDNow += "R" Else sCRUDNow += "_"
        If sCRUD.Substring(2, 1) = 1 Then sCRUDNow += "U" Else sCRUDNow += "_"
        If sCRUD.Substring(3, 1) = 1 Then sCRUDNow += "D" Else sCRUDNow += "_"

        Return sCRUDNow
    End Function

    Protected Sub gvConcessions_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Dim sOriginalBackgroundColor As String = ""
        Dim strClaimAspectServiceChannelID As String = ""
        Dim strClaimAspectID As String = ""

        Dim gvconcession As GridView = TryCast(sender, GridView)

        If gvconcession IsNot Nothing Then
            Dim item As RepeaterItem = TryCast(gvconcession.NamingContainer, RepeaterItem)
            'Now you can use FindControl to get to the controls in the found item, for example a Label with ID: Label1

            strClaimAspectServiceChannelID = TryCast(item.FindControl("hidClaimAspectServiceChannelID"), HiddenField).Value
            strClaimAspectID = TryCast(item.FindControl("hidClaimAspectID"), HiddenField).Value

            If (e.Row.RowType = DataControlRowType.DataRow) Then
                e.Row.Attributes.Add("onclick", "return setEditConcession(this,'" + item.ClientID + "'," + (e.Row.Cells(0)).Text + ")")
                e.Row.Attributes.Add("ondblclick", "return dblClickEditConcession(this,'" + item.ClientID + "'," + (e.Row.Cells(0)).Text + "," + strClaimAspectServiceChannelID + "," + strClaimAspectID + "," + sUserID + "," + sInscCompany + ")")
            End If
        End If
    End Sub

    Protected Sub btnDelConcession_Click(sender As Object, e As EventArgs)
        Dim strClaimAspectServiceChannelConcessionID As String = ""
        Dim sParams As String, sStoredProcedure As String = "", sret As String = ""
        Dim btn As Button = TryCast(sender, Button)

        sStoredProcedure = "uspConcessionDelDetail"

        If btn IsNot Nothing Then
            Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)
            'Now you can use FindControl to get to the controls in the found item, for example a Label with ID: Label1

            strClaimAspectServiceChannelConcessionID = TryCast(item.FindControl("hidClaimAspectServiceChannelConcessionID"), HiddenField).Value


            sParams = "@ClaimAspectServiceChannelConcessionID=" + strClaimAspectServiceChannelConcessionID + " ,@UserID=" + hidUserID.Value

            sret = wsAPDFoundation.ExecuteSp(sStoredProcedure, sParams)
            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
        End If
    End Sub

    Protected Sub gvEstimate_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Dim strClaimAspectID As String = ""
        Dim imgEstimate As WebControls.Image

        Dim gvEstimate As GridView = TryCast(sender, GridView)

        If gvEstimate IsNot Nothing Then
            Dim item As RepeaterItem = TryCast(gvEstimate.NamingContainer, RepeaterItem)
            'Now you can use FindControl to get to the controls in the found item, for example a Label with ID: Label1

            If (e.Row.RowType = DataControlRowType.DataRow) Then
                ' e.Row.DataItem.AssignmentID
                imgEstimate = e.Row.Cells(1).Controls(0)

                If (e.Row.DataItem(10) <> "0") Then
                    imgEstimate.Attributes.Add("onClick", "return ShowEstimate('" + e.Row.DataItem(9) + "',1)")
                Else
                    strClaimAspectID = TryCast(item.FindControl("hidClaimAspectID"), HiddenField).Value
                    imgEstimate.Attributes.Add("onClick", "return ShowShortEstimate('" + e.Row.DataItem(9) + "','" + e.Row.DataItem(11).Replace("\", "\\") + "','" + strClaimAspectID + "','" + sLynxID + "','" + sInscCompany + "','" + sUserID + "')")
                End If

                imgEstimate.Attributes.Add("Style", "'cursor: pointer;'")

            End If
        End If

    End Sub

    Protected Sub gvWarAssignment_RowDataBound(sender As Object, e As GridViewRowEventArgs)

        Dim gvWarAssignment As GridView = TryCast(sender, GridView)

        If gvWarAssignment IsNot Nothing Then
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim sParams As String = "'" & e.Row.DataItem.AssignmentID & "'" _
                                        & ",'" & (e.Row.DataItem.ShopName).Replace("'", "\'") & "'" _
                                        & ",'" & (e.Row.DataItem.ShopContact).Replace("'", "\'") & "'" _
                                        & ",'" & e.Row.DataItem.ShopPhone & "'" _
                                        & ",'" & e.Row.DataItem.ShopFax & "'" _
                                        & ",'" & e.Row.DataItem.AssignmentStatus & "'" _
                                        & ",'" & e.Row.DataItem.FaxStatus & "'" _
                                        & ",'" & e.Row.DataItem.ElecStatus & "'" _
                                        & ",'" & e.Row.DataItem.Disposition & "'" _
                                        & ",'" & e.Row.DataItem.workstatus & "'" _
                                        & ",'" & e.Row.DataItem.AssignmentDate & "'" _
                                        & ",'" & (e.Row.DataItem.Remarks).Replace("'", "\'") & "'" _
                                        & ",'" & e.Row.DataItem.WarrantyStartDate & "'" _
                                        & ",'" & e.Row.DataItem.WarrantyStartConfirmFlag & "'" _
                                        & ",'" & e.Row.DataItem.WarrantyEndDate & "'" _
                                        & ",'" & e.Row.DataItem.WarrantyEndConfirmFlag & "'"

                e.Row.Attributes.Add("onClick", "return showWarrantyDetails(this,'" + gvWarAssignment.ClientID + "'," + sParams + ",1)")
            End If

        End If

    End Sub

    'Handled this on ClientSide 06Feb2019
    'Protected Sub GetRepairReasonCode()
    '    Dim oReturnXML As XmlElement = Nothing
    '    Dim oXMLNodeList As XmlNodeList = Nothing
    '    Dim iCnt As Integer = 0

    '    Try
    '        If IsPostBack Then
    '        Else
    '            '-------------------------------
    '            ' Check Session Data
    '            '-------------------------------
    '            lblLynxID.Text = sLynxID

    '            '-------------------------------
    '            ' Database access
    '            '-------------------------------  
    '            Dim sStoredProcedure As String = "uspRepairSchReasonGetListWSXML"
    '            Dim sParams As String = sClaimAspectServiceChannelID

    '            '-------------------------------
    '            ' Call WS and get data
    '            '-------------------------------
    '            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

    '            '---------------------------------------
    '            ' Load Current Reasons
    '            '---------------------------------------
    '            oXMLNodeList = oReturnXML.SelectNodes("Reason")

    '            '    iClientCoverageType = 0

    '            '--------------------------------
    '            ' Populate Reason Dropdown
    '            '--------------------------------
    '            For Each oXMLNode In oXMLNodeList
    '                '---------------------------------------
    '                ' Load Reason
    '                '---------------------------------------
    '                If iCnt = 0 Then
    '                    ddlRepairReason.Items.Add("")
    '                    iCnt += 1
    '                End If

    '                ddlRepairReason.Items.Add(oXMLNode.Attributes("Name").Value)
    '                ddlRepairReason.Items.Item(iCnt).Value = oXMLNode.Attributes("ReasonID").Value
    '                iCnt += 1
    '            Next
    '        End If

    '        '-------------------------------
    '        ' Clean-up
    '        '-------------------------------
    '    Catch oExcept As Exception
    '        '---------------------------------
    '        ' Error handler and notifications
    '        '---------------------------------
    '        Dim sError As String = ""
    '        Dim sBody As String = ""
    '        Dim FunctionName As New System.Diagnostics.StackFrame

    '        sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
    '        wsAPDFoundation.LogEvent("RepairChangeReason", "ERROR", "Repair Change Reason data not found or data error occured.", oExcept.Message, sError)

    '        Response.Write(oExcept.ToString)
    '        'Return ""
    '    Finally
    '        oReturnXML = Nothing
    '    End Try
    'End Sub

    Protected Sub repDocuments_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim chkShare As CheckBox = CType(e.Item.FindControl("chkShare"), CheckBox)

            Dim lstdoc As Documents = New Documents()
            lstdoc = CType(e.Item.DataItem, Documents)

            If (lstdoc.DocumentTypeName = "Photograph") Then
                chkShare.Visible = True
                chkShare.Attributes.Add("onClick", "return shareDocument(this,'chkShare','" + lstdoc.DocumentID + "')")
                chkShare.Checked = lstdoc.ApprovedFlag
            Else
                chkShare.Visible = False
            End If


        End If
    End Sub

    Protected Sub btnWarCancelAssignment_click(sender As Object, e As EventArgs)
        Dim sClaimAspectID As String = ""
        Dim stxtWarRepairStartDate As String = ""
        Dim sUserID As String = ""
        Dim sReturnString As String = ""
        Dim sShopRemarks As String = ""
        Dim sVehNum As String = ""
        Dim sShopLocationID As String = ""
        Dim strWarAssignmentID As String = ""
        Dim aRC As Array = Nothing
        Dim sParams As String = ""

        Try
            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim btn As Button = TryCast(sender, Button)
            If btn IsNot Nothing Then
                '--------------------------
                ' Access the repeater data
                '--------------------------
                Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)
                stxtWarRepairStartDate = TryCast(item.FindControl("txtWarRepairStartDate"), TextBox).Text
                strWarAssignmentID = TryCast(item.FindControl("hidWarAssignmentID"), HiddenField).Value
                sClaimAspectID = TryCast(item.FindControl("hidClaimAspectID"), HiddenField).Value
                sUserID = TryCast(item.FindControl("hidUserID"), HiddenField).Value
                sShopRemarks = TryCast(item.FindControl("txtAssignmentRemarks"), TextBox).Text
                sVehNum = TryCast(item.FindControl("hidVehNumber"), HiddenField).Value
                ' sShopLocationID = TryCast(item.FindControl("hidShopLocationID"), HiddenField).Value
            End If

            '----------------------------
            ' Validate Assignment State
            ' And send workflow notify
            '----------------------------
            If sClaimAspectID <> "" Then
                'do Not send the vehicle number Or the LynxID when the claimAspectID Is available
                sParams = "@ClaimAspectID=" & sClaimAspectID & ", @SelectOperationCD='C', @UserId=" & sUserID & ", @AssignmentRemarks='" & sShopRemarks & "', @NotifyEvent=1, @WarrantyAssignmentID=" & strWarAssignmentID
                'sParams += ", @AppraiserID=NULL"
            Else
                'when claimAspectID Is Not available then send in the LynxID And the vehicle Number.
                sParams = "@LynxId=" & sLynxID & ", @VehicleNumber=" & sVehNum & ", @ClaimAspectServiceChannelID=" & sClaimAspectServiceChannelID & ", @SelectOperationCD='C', @UserId=" & sUserID & ", @AssignmentRemarks='" & sShopRemarks & "', @NotifyEvent=1"
                sParams += ", @ShopLocationID=" & sShopLocationID
            End If

            '-------------------------------
            ' Process DB Call
            '-------------------------------  
            Dim sStoredProcedure As String = "uspWorkflowWarrantySelectShop"

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            sReturnString = wsAPDFoundation.ExecuteSp(sStoredProcedure, sParams)

            '-------------------------------
            ' Check if update was successful
            '-------------------------------
            If sReturnString.Contains("ERROR") And sReturnString.Contains("|") Then
                aRC = (sReturnString).Split("|")
                Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + aRC(1) + " ');", True)
            Else
                'olblAssignmentSaved.Text = "Saved..."
                Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)
            End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("btnWarCancelAssignment_click", "ERROR", "Cancel Assignment Detail not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            'lblClaimSaved.Text = "Cancelled..."
        End Try
    End Sub

    'Protected Function GetDocuments(ByVal oXMLNodeList As XmlNodeList, ByVal sReference As String, ByVal iUserid As Integer) As String
    '    Dim oXMLDocumentNodeList As XmlNodeList = Nothing
    '    Dim oDocumentReturnXML As XmlElement = Nothing

    '    '-------------------------------
    '    ' Load the Reference Data
    '    '-------------------------------


    '    oXMLDocumentNodeList = oDocumentReturnXML.SelectNodes("Vehicle[@ClaimAspectID=" & oClaimRepDesktopClaimAspect.ClaimAspectID & "]/Document")

    '    Dim oClaimRepDesktopDocumentRows As New List(Of ClaimRepDesktopDocuments)()
    '    oClaimRepDesktopDocumentRows.Clear()
    '    For Each oXMLDocumentNode In oXMLDocumentNodeList
    '        Dim oClaimRepDesktopDocuments As New ClaimRepDesktopDocuments

    '        oClaimRepDesktopDocuments.DocumentID = oXMLDocumentNode.Attributes("DocumentID").value
    '        oClaimRepDesktopDocuments.DocumentSourceName = oXMLDocumentNode.Attributes("DocumentSourceName").value
    '        oClaimRepDesktopDocuments.DocumentTypeName = oXMLDocumentNode.Attributes("DocumentTypeName").value
    '        oClaimRepDesktopDocuments.CreatedDate = oXMLDocumentNode.Attributes("CreatedDate").value
    '        oClaimRepDesktopDocuments.ReceivedDate = Format(CDate(oXMLDocumentNode.Attributes("ReceivedDate").value), "MM/dd/yyyy")

    '        sDocumentLocation = oXMLDocumentNode.Attributes("ImageLocation").value
    '        oClaimRepDesktopDocuments.ImageLocationRaw = AppSettings("ImagePath").Replace("\", "\\") & sDocumentLocation
    '        oClaimRepDesktopDocuments.ImageLocation = AppSettings("ImagePath") & sDocumentLocation.Replace("\\", "\")
    '        oClaimRepDesktopDocuments.ImageLocationThumbnail = AppSettings("ImagePath") & sDocumentLocation.Replace("\\", "\")

    '        oClaimRepDesktopDocuments.SupplementSeqNumber = oXMLDocumentNode.Attributes("SupplementSeqNumber").value
    '        oClaimRepDesktopDocuments.FullSummaryExistsFlag = oXMLDocumentNode.Attributes("FullSummaryExistsFlag").value
    '        oClaimRepDesktopDocuments.AgreedPriceMetCD = oXMLDocumentNode.Attributes("AgreedPriceMetCD").value
    '        oClaimRepDesktopDocuments.GrossEstimateAmt = oXMLDocumentNode.Attributes("GrossEstimateAmt").value
    '        oClaimRepDesktopDocuments.NetEstimateAmt = oXMLDocumentNode.Attributes("NetEstimateAmt").value
    '        oClaimRepDesktopDocuments.EstimateTypeFlag = oXMLDocumentNode.Attributes("EstimateTypeFlag").value
    '        oClaimRepDesktopDocuments.DirectionToPayFlag = oXMLDocumentNode.Attributes("DirectionToPayFlag").value
    '        oClaimRepDesktopDocuments.FinalEstimateFlag = oXMLDocumentNode.Attributes("FinalEstimateFlag").value
    '        oClaimRepDesktopDocuments.DuplicateFlag = oXMLDocumentNode.Attributes("DuplicateFlag").value
    '        oClaimRepDesktopDocuments.EstimateTypeCD = oXMLDocumentNode.Attributes("EstimateTypeCD").value
    '        oClaimRepDesktopDocuments.SysLastUpdatedDateEstimate = oXMLDocumentNode.Attributes("SysLastUpdatedDateEstimate").value

    '        'If oClaimRepDesktopDocuments.ImageLocation <> AppSettings("ImagePath") Then
    '        Select Case UCase(Right(oClaimRepDesktopDocuments.ImageLocation, 3))
    '            Case "JPG", "GIF", "PNG"
    '                oClaimRepDesktopDocuments.ImageViewable = 1
    '            Case "TIF"
    '                oClaimRepDesktopDocuments.ImageViewable = 1
    '                oClaimRepDesktopDocuments.ImageLocationThumbnail = "image/tif.png"
    '            Case "PDF"
    '                oClaimRepDesktopDocuments.ImageViewable = 0
    '                oClaimRepDesktopDocuments.ImageLocationThumbnail = "image/PDFDefault.png"
    '            Case "DOC"
    '                oClaimRepDesktopDocuments.ImageViewable = 0
    '                oClaimRepDesktopDocuments.ImageLocationThumbnail = "image/WordDoc.png"
    '            Case Else
    '                oClaimRepDesktopDocuments.ImageViewable = 0
    '                oClaimRepDesktopDocuments.ImageLocationThumbnail = "image/blank.png"
    '        End Select

    '        If oClaimRepDesktopDocuments.ImageLocation <> AppSettings("ImagePath") Then
    '            Dim fs As New System.IO.FileInfo(oClaimRepDesktopDocuments.ImageLocation)
    '            If fs.Exists Then
    '                oClaimRepDesktopDocuments.ImageSize = Convert.ToInt32(fs.Length / 1000)
    '                oClaimRepDesktopDocuments.ImageInfo = oClaimRepDesktopDocuments.DocumentTypeName & Chr(10) & oClaimRepDesktopDocuments.ReceivedDate & Chr(10) & oClaimRepDesktopDocuments.ImageSize & " kb"
    '            Else
    '                oClaimRepDesktopDocuments.ImageViewable = 0
    '                oClaimRepDesktopDocuments.ImageInfo = sDocumentLocation.Replace("\\", "\")
    '                oClaimRepDesktopDocuments.ImageLocationThumbnail = "image/NoPhoto.png"
    '            End If
    '        End If
    '        oClaimRepDesktopDocumentRows.Add(oClaimRepDesktopDocuments)
    '        oClaimRepDesktopClaimAspect.Documents = oClaimRepDesktopDocumentRows
    '    Next
    'End Function



    'Protected Function GetCRUD(ByVal sReference As String, ByVal iUserid As Integer) As String
    '    Dim oReturnXML As XmlElement = Nothing
    '    Dim oXMLNode As XmlNode = Nothing
    '    Dim oXMLNodeList As XmlNodeList = Nothing
    '    Dim sCRUD As String = ""

    '    Try
    '        '-------------------------------
    '        ' Database access
    '        '-------------------------------  
    '        Dim sStoredProcedure As String = "uspCRUDGetDetailWSXML"
    '        Dim sParams As String = "'" & sReference & "', " & iUserid

    '        '-------------------------------
    '        ' Call WS and get data
    '        '-------------------------------
    '        oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

    '        '-------------------------------
    '        ' Load Root level claim data
    '        '-------------------------------
    '        oXMLNode = oReturnXML.SelectSingleNode("Crud")
    '        sCRUD = oXMLNode.Attributes("Insert").InnerText & oXMLNode.Attributes("Select").InnerText & oXMLNode.Attributes("Update").InnerText & oXMLNode.Attributes("Delete").InnerText

    '        Return sCRUD
    '    Catch oExcept As Exception
    '        '---------------------------------
    '        ' Error handler and notifications
    '        '---------------------------------
    '        Dim sError As String = ""
    '        Dim sBody As String = ""
    '        Dim FunctionName As New System.Diagnostics.StackFrame

    '        sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
    '        wsAPDFoundation.LogEvent("APDClaim", "ERROR", "CRUD data not found or data error occured.", oExcept.Message, sError)

    '        Response.Write(oExcept.ToString)
    '        Return ""
    '    Finally
    '        oXMLNode = Nothing
    '        oReturnXML = Nothing
    '        oXMLNodeList = Nothing
    '    End Try
    'End Function

    'Protected Function SendShopAssignment(ByVal sServiceChannelCD As String, ByVal ResendFlag As Boolean, ByVal sAssignmentTypeCD As String, ByVal sAssignmentID As String)
    '    Dim sStoredProcedure As String = ""
    '    Dim sParams As String = ""
    '    Dim oReturnXML As XmlElement = Nothing
    '    Dim oXMLNode As XmlNode = Nothing
    '    Dim bStaffAppraiser As Boolean = False
    '    Dim sRequest As String = ""

    '    '-----------------------------------
    '    ' Check if we h
    '    '// Check for changes in Shop Remarks
    '    'If (txtAssignmentRemarks.isDirty) Then {

    '    '    ClientWarning("Assignment data has changed. Please save the data and try again.");
    '    '    disableButtons(False);
    '    '    Return;
    '    '    //saveData();
    '    '}

    '    '    If (parseFloat(txtEffDedSent.value) > 1000) Then {
    '    '    If (YesNoMessage("Confirm Effective Deductible", "Effective Deductible Sent value is greater than $1000. Is this value correct?") != "Yes") Then {
    '    '        disableButtons(False);
    '    '        Return;
    '    '    }
    '    '}

    '    If ResendFlag = False Then
    '        '-------------------------------
    '        ' Database access
    '        '-------------------------------  
    '        sStoredProcedure = "uspWorkflowAssignShop"

    '        If sAssignmentTypeCD = "LDAU" Then
    '            sParams = "@AssignmentID=" & sAssignmentID
    '        End If

    '        oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

    '    End If

    '    '---------------------------
    '    ' Check if Staff Appraiser
    '    '---------------------------
    '    bStaffAppraiser = False
    '    If (sServiceChannelCD = "DA" Or sServiceChannelCD = "DR" Or (sServiceChannelCD = "RRP" And sAssignmentTypeCD = "LDAU")) Then
    '        bStaffAppraiser = True
    '    End If

    '    'If (sAssignmentFlag = "LDAU") Then sAssignmentID = sAssignmentID

    '    sRequest = "<Root><Assignment assignmentID='" & sAssignmentID & "' userID='" & sUserID & "' staffAppraiser='True' /></Root>"

    '    Call HTTPPost("rs/AssignShop.aspx", sRequest)

    '    'Dim request As System.Web.HttpResponse
    '    'request
    '    '= WebRequest.Create("http://www.contoso.com/")

    '    'Dim url As String = "AssignShop.asp"
    '    'Dim s As String = "window.open('" & url + "', 'popup_window', 'width=300,height=100,left=100,top=100,resizable=yes');"
    '    'ClientScript.RegisterStartupScript(Me.GetType(), "script", s, True)

    '    'Dim myReq As HttpWebRequest = WebRequest.Create("AssignShop.asp")

    '    'Dim req As WebRequest = Nothing
    '    'Dim rsp As WebResponse = Nothing
    '    'Try
    '    '    Dim uri As String = "http://localhost:51790/AssignShop.asp"
    '    '    req = WebRequest.Create(uri)
    '    '    req.Method = "POST"
    '    '    req.ContentType = "text/xml"
    '    '    Dim writer As StreamWriter = New StreamWriter(req.GetRequestStream)
    '    '    writer.WriteLine("<Root>")
    '    '    writer.Close()
    '    '    ' Send the data to the webserver
    '    '    rsp = req.GetResponse
    '    'Catch webEx As WebException

    '    'Catch ex As Exception

    '    'Finally
    '    '    If (Not (req) Is Nothing) Then
    '    '        req.GetRequestStream.Close()
    '    '    End If

    '    '    If (Not (rsp) Is Nothing) Then
    '    '        rsp.GetResponseStream.Close()
    '    '    End If

    '    'End Try

    '    'var objRet = XMLServerExecute("AssignShop.asp", sRequest);
    '    ''-------------------------------
    '    '' Database access
    '    ''-------------------------------  
    '    'Dim sStoredProcedure As String = "uspVehicleAssignGetDetailWSXML"
    '    ''Dim sParams As String = iClaimAspectServiceChannelID & "," & iInscCompanyID

    '    'Try
    '    '    Call WorkFlowSelectShop(sClaimAspectServiceChannelID, "S", sAppraiserID, sShopLocationID, sAssignmentRemarks)
    '    '    '-------------------------------
    '    '    ' Call WS and get data
    '    '    '-------------------------------
    '    '    '????????? Lots of code needs added here for this process ???????????

    '    '    'oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

    '    '    '------------------------------------
    '    '    ' Load XML data 
    '    '    '------------------------------------
    '    '    'oXMLNode = oReturnXML.SelectSingleNode("Vehicle")
    '    '    'veh.VehicleYear = oXMLNode.Attributes("VehicleYear").InnerText

    '    'Catch oExcept As Exception
    '    '    '---------------------------------
    '    '    ' Error handler and notifications
    '    '    '---------------------------------
    '    '    Dim sError As String = ""
    '    '    Dim sBody As String = ""
    '    '    Dim FunctionName As New System.Diagnostics.StackFrame

    '    '    sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
    '    '    wsAPDFoundation.LogEvent("ShopAssignment", "ERROR", "Shop Assignment not processed or data error occured.", oExcept.Message, sError)

    '    '    Response.Write(oExcept.ToString)

    '    'Finally
    '    '    'oXMLNode = Nothing
    '    '    'oReturnXML = Nothing
    '    '    'oXMLNodeList = Nothing
    '    'End Try
    'End Function

    'Protected Sub btnSendFaxAssignment_Click(sender As Object, e As EventArgs)
    '    'Protected Sub SendFaxAssignment(ByVal sServiceChannelCD As String, ByVal ResendFlag As Boolean, ByVal sAssignmentTypeCD As String, ByVal sAssignmentID As String)
    '    Dim bStaffAppraiser As Boolean = False
    '    Dim sRequest As String = ""
    '    Dim btn As Button = TryCast(sender, Button)
    '    Dim hidServiceChannelCD As HiddenField = Nothing
    '    Dim hidAssignmentID As HiddenField = Nothing

    '    '-------------------------------
    '    ' Get Vars from repeater
    '    '-------------------------------  
    '    If btn IsNot Nothing Then
    '        Dim item As RepeaterItem = TryCast(btn.NamingContainer, RepeaterItem)
    '        hidServiceChannelCD = TryCast(item.FindControl("hidServiceChannelCD"), HiddenField)
    '        hidAssignmentID = TryCast(item.FindControl("hidAssignmentID"), HiddenField)
    '    End If
    '    '---------------------------
    '    ' Check if Staff Appraiser
    '    '---------------------------
    '    bStaffAppraiser = False
    '    If (hidServiceChannelCD.Value = "DA" Or hidServiceChannelCD.Value = "DR" Or (hidServiceChannelCD.Value = "RRP" And hidServiceChannelCD.Value = "LDAU")) Then
    '        bStaffAppraiser = True
    '    End If

    '    sRequest = "<Root><Assignment assignmentID='" & hidAssignmentID.Value & "' userID='" & sUserID & "' staffAppraiser='True' hqAssignment='false' /></Root>"

    '    Call HTTPPost("rs/AssignShop.aspx", sRequest)
    'End Sub

    'Protected Sub HTTPPost(ByVal sFunctionName As String, ByVal sXMLData As String)
    '    Dim byteArray() As Byte = Encoding.UTF8.GetBytes(sXMLData)
    '    Dim sURL As String = ""

    '    sURL = "http://" & HttpContext.Current.Request.Url.Host
    '    If HttpContext.Current.Request.Url.Port > 0 Then
    '        'sURL += ":" & HttpContext.Current.Request.Url.Port
    '    Else
    '    End If

    '    ''sURL = "rs/AssignShop.asp"
    '    sURL += "/" & sFunctionName

    '    '???? Test code
    '    Dim request As HttpWebRequest = CType(WebRequest.Create(sURL), HttpWebRequest)

    '    'request.MaximumAutomaticRedirections = 4
    '    'request.MaximumResponseHeadersLength = 4

    '    '' Set credentials to use for this request.
    '    'request.Credentials = CredentialCache.DefaultCredentials

    '    Dim response As HttpWebResponse = CType(Request.GetResponse(), HttpWebResponse)

    '    'Console.WriteLine("Content length is {0}", response.ContentLength)
    '    'Console.WriteLine("Content type is {0}", response.ContentType)

    '    ' Get the stream associated with the response.
    '    Dim receiveStream As Stream = Response.GetResponseStream()

    '    ' Pipes the stream to a higher level stream reader with the required encoding format. 
    '    Dim readStream As New StreamReader(receiveStream, Encoding.UTF8)

    '    Console.WriteLine("Response stream received.")
    '    Console.WriteLine(readStream.ReadToEnd())
    '    Response.Close()
    '    readStream.Close()




    '    '??? different test

    '    'Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + sURL + " ');", True)
    '    Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + sXMLData + " ');", True)

    '    'Debug.Print(HttpContext.Current.Request.Url.Host & ":" & HttpContext.Current.Request.Url.Port)

    '    'Dim xmlData As String = "<?xml version='1.0'?><!DOCTYPE svc_init SYSTEM 'ABCD.DTD'><svc_init ver='3.3.0'><hdr ver='3.3.0'><cli" &
    '    '"ent><id>xxx</id><pwd>xxx</pwd></client></hdr><aaaa ver='3.3.0'><trans_id>1</trans_id><request_type t" &
    '    '"ype='2'/><l_hor_acc type='HIGH'/></aaaa></svc_init>"

    '    'Dim request As WebRequest = WebRequest.Create(sURL)
    '    'request.Method = "POST"
    '    'request.ContentType = "application/x-www-form-urlencoded"
    '    'request.ContentLength = byteArray.Length
    '    'request.UseDefaultCredentials = True
    '    ''request.PreAuthenticate = True
    '    'request.Credentials = CredentialCache.DefaultCredentials

    '    'Page.ClientScript.RegisterStartupScript(Me.GetType, "Alert", "alert(' " + request.Credentials + " ');", True)


    '    'Dim dataStream As Stream = request.GetRequestStream
    '    'dataStream.Write(byteArray, 0, byteArray.Length)
    '    'dataStream.Close()

    '    'Dim response As WebResponse = request.GetResponse
    '    'dataStream = response.GetResponseStream

    '    'Dim reader As StreamReader = New StreamReader(dataStream)
    '    'Dim responseFromServer As String = reader.ReadToEnd

    '    'reader.Close()
    '    'dataStream.Close()
    '    'response.Close()
    'End Sub

End Class