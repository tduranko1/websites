﻿Imports System.Xml

Public Class RepairChangeReason
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    Dim sCRUD As String = ""
    Dim sLynxID As String = ""
    Dim iClaimAspectServiceChannelID As Integer = 0
    Dim sUserID As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim oReturnXML As XmlElement = Nothing
        'Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim iCnt As Integer = 0
        'Dim iClientCoverageType As Integer = 0
        'Dim DTCoverage As New DataTable
        'Dim DCCoverage As New DataColumn
        'Dim sDDLParams As String = ""

        Try
            If IsPostBack Then
            Else
                '-------------------------------
                ' Check Session Data
                '-------------------------------
                sLynxID = Request.QueryString("LynxID")
                lblLynxID.Text = sLynxID
                iClaimAspectServiceChannelID = Request.QueryString("ClaimAspectServiceChannelID")
                '    sUserID = Request.QueryString("UserID")

                'txtLynxID.Text = CStr(iLynxID)

                '-------------------------------
                ' Database access
                '-------------------------------  
                Dim sStoredProcedure As String = "uspRepairSchReasonGetListWSXML"
                Dim sParams As String = iClaimAspectServiceChannelID

                '-------------------------------
                ' Call WS and get data
                '-------------------------------
                oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

                '---------------------------------------
                ' Load Current Reasons
                '---------------------------------------
                oXMLNodeList = oReturnXML.SelectNodes("Reason")

                '    iClientCoverageType = 0

                '--------------------------------
                ' Populate Reason Dropdown
                '--------------------------------
                For Each oXMLNode In oXMLNodeList
                    '---------------------------------------
                    ' Load Reason
                    '---------------------------------------
                    If iCnt = 0 Then
                        ddlRepairReason.Items.Add("")
                        iCnt += 1
                    End If

                    ddlRepairReason.Items.Add(oXMLNode.Attributes("Name").Value)
                    ddlRepairReason.Items.Item(iCnt).Value = oXMLNode.Attributes("ReasonID").Value
                    iCnt += 1
                Next
            End If

            '-------------------------------
            ' Clean-up
            '-------------------------------
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("RepairChangeReason", "ERROR", "Repair Change Reason data not found or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
            'Return ""
        Finally
            'oXMLNode = Nothing
            oReturnXML = Nothing
            'oXMLNodeList = Nothing
        End Try
    End Sub

    Protected Sub btnReasonOk_Click(sender As Object, e As EventArgs) Handles btnReasonOk.Click
        Try
            'ddlRepairReason.SelectedValue

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("btnReasonOk_Click", "ERROR", "btnReasonOk_Click not processed or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)

        Finally
            'oXMLNode = Nothing
        End Try
    End Sub

    'Protected Sub btnCoverageAddSave_Click(sender As Object, e As EventArgs) Handles btnCoverageAddSave.Click
    '    Dim oReturnXML As XmlElement = Nothing
    '    Dim oXMLNode As XmlNode = Nothing
    '    Dim sLimitDailyAmt As String = 0
    '    Dim sMaximumDays As String = 0
    '    Dim aCoverageParams As Array = Nothing
    '    Dim sClaimCoverageID As String = ""
    '    Dim iAdditionalCoverageFlag As Integer = 0

    '    Try
    '        '-------------------------------
    '        ' Validate the web page data
    '        '-------------------------------  
    '        '??????????? CODE THIS ????????????????????
    '        'lblClaimSaved.Text = ""

    '        '-------------------------------
    '        ' Process DB Call
    '        '-------------------------------  
    '        Dim sStoredProcedure As String = "uspClaimCoverageUpdDetailWSXML"

    '        sClaimCoverageID = "0"
    '        If UCase(txtCoverageAdditionalFlag.Text) = "YES" Then
    '            iAdditionalCoverageFlag = 1
    '        Else
    '            iAdditionalCoverageFlag = 0
    '        End If


    '        Dim sParams As String = "@ClaimCoverageID = " & sClaimCoverageID _
    '            & ", @ClientCoverageTypeID = " & CStr(iSelectedCoverageTypeID) _
    '            & ", @LynxID = " & CStr(iLynxID) _
    '            & ", @InsuranceCompanyID = " & CStr(iInsuranceCompanyID) _
    '            & ", @AddtlCoverageFlag = " & iAdditionalCoverageFlag _
    '            & ", @CoverageTypeCD = '" & txtCoverageClientCode.Text & "'" _
    '            & ", @Description = '" & ddlCoverageDescription.SelectedItem.Text & "'" _
    '            & ", @DeductibleAmt = " & txtCoverageDeductible.Text _
    '            & ", @LimitAmt = " & txtCoverageLimit.Text _
    '            & ", @LimitDailyAmt = " & txtCoverageDailyLimit.Text _
    '            & ", @MaximumDays = " & txtCoverageMaxDays.Text _
    '            & ", @UserID = " & sUserID _
    '            & ", @SysLastUpdatedDate = " & "''"

    '        '?????? Code This ?????
    '        '    oNode.setAttribute("AddlCoverageFlagInd", (txtAdditionalFlag.value == "Yes" ? "/images/tick.gif" : "/images/spacer.gif"));

    '        'Debug.Print(sParams)

    '        '-------------------------------
    '        ' Call WS and get data
    '        '-------------------------------
    '        oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

    '        '???? check for success and save new Coverage syslastupdateddate ????

    '        'response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)

    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "myCloseScript", "window.opener.location.reload(true); window.close();", True)
    '    Catch oExcept As Exception
    '        '---------------------------------
    '        ' Error handler and notifications
    '        '---------------------------------
    '        Dim sError As String = ""
    '        Dim sBody As String = ""
    '        Dim FunctionName As New System.Diagnostics.StackFrame

    '        sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
    '        wsAPDFoundation.LogEvent("CoverageAddSaveDetails", "ERROR", "Add Coverage Save not processed or data error occured.", oExcept.Message, sError)

    '        Response.Write(oExcept.ToString)

    '    Finally
    '        oXMLNode = Nothing
    '    End Try
    'End Sub

    'Protected Sub ddlCoverageDescription_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCoverageDescription.SelectedIndexChanged
    '    Dim aCoverageData As Array = Nothing
    '    Dim iCoverageTypeID As Integer = 0

    '    aCoverageData = ddlCoverageDescription.SelectedItem.Value.Split("|")
    '    txtCoverageClientCode.Text = aCoverageData(0)
    '    txtCoverageType.Text = aCoverageData(1)
    '    iCoverageTypeID = CInt(aCoverageData(2))
    '    txtCoverageAdditionalFlag.Text = aCoverageData(3)
    '    txtCoverageDailyLimit.Text = 0
    '    txtCoverageDeductible.Text = 0
    '    txtCoverageLimit.Text = 0
    '    txtCoverageMaxDays.Text = 0
    '    iSelectedCoverageTypeID = iCoverageTypeID

    '    '-------------------------
    '    ' Additional Flag 
    '    '-------------------------
    '    Select Case txtCoverageAdditionalFlag.Text
    '        Case "0"
    '            txtCoverageAdditionalFlag.Text = "No"
    '        Case "1"
    '            txtCoverageAdditionalFlag.Text = "Yes"
    '    End Select

    '    '-------------------------
    '    ' Rental 
    '    '-------------------------
    '    If UCase(txtCoverageClientCode.Text) = "RENT" Then
    '        lblRentalMax.Visible = True
    '        lblRentalLimit.Visible = True
    '        txtCoverageMaxDays.Visible = True
    '        txtCoverageDailyLimit.Visible = True
    '    Else
    '        lblRentalMax.Visible = False
    '        lblRentalLimit.Visible = False
    '        txtCoverageMaxDays.Visible = False
    '        txtCoverageDailyLimit.Visible = False
    '    End If
    'End Sub

End Class