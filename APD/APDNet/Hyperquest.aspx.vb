﻿Imports System.Xml

Public Class Hyperquest
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    Dim bError As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim iDocumentID As Integer
        Dim iInscCompID As Integer

        '-----------------------------------
        ' Get passed params and process
        '-----------------------------------
        iInscCompID = Request.QueryString("InscCompID")
        iDocumentID = Request.QueryString("DocumentID")

        bError = GetDocumentDetails(iInscCompID, iDocumentID)
        'bError = GetDocumentDetails(259, 4955902)

        If bError Then
            lblMessage.Text = "Error occured sending new Hyperquest document."
            lblMessage.ForeColor = Drawing.Color.Red
            'Response.Write("Error occured sending new Hyperquest document.")
        Else
            lblMessage.Text = "Hyperquest document processed successfully."
            lblMessage.ForeColor = Drawing.Color.Blue
            'Response.Write("Hyperquest document processed successfully.")
        End If
    End Sub

    Private Function GetDocumentDetails(ByVal iInscCompID As Integer, ByVal iDocumentID As Integer) As Boolean
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLJobNodeList As XmlNodeList = Nothing
        Dim oXMLJobNode As XmlNode = Nothing
        Dim sLynxID As String = ""
        Dim sJobID As String = ""
        Dim sJobStatus As String = ""
        Dim sClaimNumber As String = ""
        Dim sDocumentID As String = ""
        Dim sAssignmentID As String = ""
        Dim sReceivedDate As String = ""
        Dim sDocumentTypeID As String = ""
        Dim sDocumentTypeName As String = ""
        Dim sDocumentSource As String = ""
        Dim sDocumentImageType As String = ""
        Dim sDocumentImageLocation As String = ""
        Dim sClaimAspectServiceChannelID As String = ""
        Dim sClaimAspectID As String = ""
        Dim sJobXSLFile As String = ""
        Dim sJobStatusDetails As String = ""
        Dim sJobPriority As String = ""
        Dim sCreatedUserID As String = ""
        Dim sJobCreatedDate As String = ""
        Dim sJobXML As String = ""

        '-------------------------------
        ' Database Calls
        '-------------------------------  
        Dim sStoredProcedure As String = "uspHyperquestGetDADocumentDetailsWSXML"
        Dim sParams As String = "@iDocumentID=" & iDocumentID

        Try
            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '-------------------------------
            ' Get References for later use
            '-------------------------------
            'oXMLJobNodeList = oReturnXML.SelectNodes("Root")

            Try
                If oReturnXML.SelectSingleNode("@JobID").Value = "" Then
                    '------------------------------
                    ' Error no data for Document                                                                        
                    '------------------------------
                    wsAPDFoundation.LogEvent("Hyperquest", "ERROR", "Failed to retrieve XML Document data.", "XML Data returned from the uspHyperquestGetDADocumentDetailsWSXML didn't contain a @JobID.  ReturnXML in EventXML", oReturnXML.OuterXml)
                    bError = True
                End If
            Catch ex As Exception
                '------------------------------
                ' Error no data for Document                                                                        
                '------------------------------
                wsAPDFoundation.LogEvent("Hyperquest", "ERROR", "No XML returned from the database.", "No XML document details were returned from the uspHyperquestGetDADocumentDetailsWSXML.", ex.Message)
                bError = True
            End Try

            If bError Then
            Else
                If oReturnXML.SelectSingleNode("@JobID").Value <> "" Then
                    '    '------------------------------
                    '    ' Error no data for Document                                                                        
                    '    '------------------------------
                    '    wsAPDFoundation.LogEvent("Hyperquest", "ERROR", "Failed to create Hyperquest Job.", "Failed to create Hyperquest Job in utb_hyperquestsvc_jobs table.  ReturnXML in EventXML", oReturnXML.OuterXml)
                    '    bError = True
                    'Else
                    '------------------------------
                    ' Parse the XML to vars                                                                        
                    '------------------------------
                    sJobID = oReturnXML.SelectSingleNode("@JobID").InnerText
                    sJobStatus = oReturnXML.SelectSingleNode("@JobStatus").InnerText
                    sLynxID = oReturnXML.SelectSingleNode("@LynxID").InnerText
                    sClaimNumber = oReturnXML.SelectSingleNode("@ClientClaimNumber").InnerText
                    sDocumentID = oReturnXML.SelectSingleNode("@DocumentID").InnerText
                    Try
                        sAssignmentID = oReturnXML.SelectSingleNode("@AssignmentID").InnerText
                    Catch ex As Exception
                        sAssignmentID = "0"
                    End Try
                    sReceivedDate = oReturnXML.SelectSingleNode("@ReceivedDate").InnerText
                    sDocumentTypeID = oReturnXML.SelectSingleNode("@DocumentTypeID").InnerText
                    sDocumentTypeName = oReturnXML.SelectSingleNode("@DocumentTypeName").InnerText
                    sDocumentSource = oReturnXML.SelectSingleNode("@DocumentSource").InnerText
                    sDocumentImageType = oReturnXML.SelectSingleNode("@DocumentImageType").InnerText
                    sDocumentImageLocation = oReturnXML.SelectSingleNode("@DocumentImageLocation").InnerText
                    sClaimAspectServiceChannelID = oReturnXML.SelectSingleNode("@ClaimAspectServiceChannelID").InnerText
                    sClaimAspectID = oReturnXML.SelectSingleNode("@ClaimAspectID").InnerText
                    sJobXSLFile = ""
                    sJobStatus = oReturnXML.SelectSingleNode("@JobStatus").InnerText
                    sJobStatusDetails = oReturnXML.SelectSingleNode("@JobStatusDetails").InnerText
                    sJobPriority = oReturnXML.SelectSingleNode("@JobPriority").InnerText
                    sCreatedUserID = oReturnXML.SelectSingleNode("@CreatedUserID").InnerText
                    sJobCreatedDate = oReturnXML.SelectSingleNode("@JobCreatedDate").InnerText
                    sJobXML = oReturnXML.SelectSingleNode("@JobXML").InnerText
                End If

                bError = CreateHyperquestJob(sJobID, iInscCompID, sLynxID, sClaimNumber, sDocumentID, sAssignmentID, "0", "0", "0", sReceivedDate, sDocumentTypeID, sDocumentTypeName, sDocumentSource, sDocumentImageType, sDocumentImageLocation, sClaimAspectServiceChannelID, sClaimAspectID, sJobXSLFile, sJobStatus, sJobStatusDetails, sJobPriority, sCreatedUserID, sJobCreatedDate, sJobXML)
            End If

            Return bError
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("Hyperquest", "ERROR(999)", "Failed while parsing XML data from the database.", "Failed parsing return XML from uspHyperquestGetDADocumentDetailsWSXML.  ReturnXML in EventXML", " StoredProc: " & sStoredProcedure & " Params: " & sParams & "  ReturnXML: " & oReturnXML.OuterXml)

            Return True
        Finally
        End Try
    End Function

    Private Function CreateHyperquestJob(ByVal sJobID As String _
        , ByVal iInscCompID As Integer _
        , ByVal sLynxID As String _
        , ByVal sClaimNumber As String _
        , ByVal sDocumentID As String _
        , ByVal sAssignmentID As String _
        , ByVal sVehicleID As String _
        , ByVal sShopLocationID As String _
        , ByVal sSeqID As String _
        , ByVal sReceivedDate As String _
        , ByVal sDocumentTypeID As String _
        , ByVal sDocumentTypeName As String _
        , ByVal sDocumentSource As String _
        , ByVal sDocumentImageType As String _
        , ByVal sDocumentImageLocation As String _
        , ByVal sClaimAspectServiceChannelID As String _
        , ByVal sClaimAspectID As String _
        , ByVal sJobXSLFile As String _
        , ByVal sJobStatus As String _
        , ByVal sJobStatusDetails As String _
        , ByVal sJobPriority As String _
        , ByVal sSysLastUserID As String _
        , ByVal sJobCreatedDate As String _
        , ByVal sJobXML As String _
        ) As Boolean

        Dim sDBError As String = ""

        '-------------------------------
        ' Database Calls
        '-------------------------------  
        Dim sStoredProcedure As String = "uspHyperquestInsDAJobDocument"
        Dim sParams As String = "@vJobID='" & sJobID & "'" _
            & ",@iInscCompID=" & iInscCompID _
            & ",@iLynxID=" & CInt(sLynxID) _
            & ",@vClaimNumber='" & sClaimNumber & "'" _
            & ",@iDocumentID=" & CInt(sDocumentID) _
            & ",@iAssignmentID=" & CInt(sAssignmentID) _
            & ",@iVehicleID=" & CInt(sAssignmentID) _
            & ",@iShopLocationID=" & CInt(sShopLocationID) _
            & ",@iSeqID=" & CInt(sSeqID) _
            & ",@dtReceivedDate='" & sReceivedDate & "'" _
            & ",@iDocumentTypeID=" & CInt(sDocumentTypeID) _
            & ",@vDocumentTypeName='" & sDocumentTypeName & "'" _
            & ",@vDocumentSource='" & sDocumentSource & "'" _
            & ",@vDocumentImageType='" & sDocumentImageType & "'" _
            & ",@vDocumentImageLocation='" & sDocumentImageLocation & "'" _
            & ",@iClaimAspectServiceChannelID=" & CInt(sClaimAspectServiceChannelID) _
            & ",@iClaimAspectID=" & CInt(sClaimAspectID) _
            & ",@vJobXSLFile='" & sJobXSLFile & "'" _
            & ",@vJobStatus='" & sJobStatus & "'" _
            & ",@vJobStatusDetails='" & sJobStatusDetails & "'" _
            & ",@iJobPriority=" & CInt(sJobPriority) _
            & ",@dtJobCreatedDate='" & sJobCreatedDate & "'" _
            & ",@bEnabledFlag=1" _
            & ",@xJobXML='" & sJobXML & "'" _
            & ",@iSysLastUserID=" & CInt(sSysLastUserID)

        Try
            '----------------------------------------
            ' Debugging
            '----------------------------------------
            'If bDebug = True Then
            'APDPartnerFoundation.LogHyperquestPartnerEvent(sProcessingServer, sEventTransactionID, "HyperquestService(DBXMLEXEC" & CStr(iEventSeq) & ")", CStr(iEventSeq), "DBEXEC", "<====== Calling " & sStoredProcedure & " to update the job details", "Params passed in EventXML.", "Params: " & sParams)
            'iEventSeq = iEventSeq + 1
            'End If

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            sDBError = wsAPDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

            '----------------------------------------
            ' Debugging
            '----------------------------------------
            'If bDebug = True Then
            '    APDPartnerFoundation.LogHyperquestPartnerEvent(sProcessingServer, sEventTransactionID, "HyperquestService(DBXMLEXEC" & CStr(iEventSeq) & ")", CStr(iEventSeq), "DBEXEC", "<====== String Returned from " & sStoredProcedure & " to update the job details", "String in EventXML: ", sReturnData)
            '    iEventSeq = iEventSeq + 1
            'End If

            If sDBError = "0" Then
                Return False
            Else
                Return True
            End If

            'Catch oSqlEx As SqlException
            '    '---------------------------------
            '    ' SQL Error handler and notifications
            '    '---------------------------------
            '    Dim sError As String = ""
            '    Dim sBody As String = ""
            '    Dim FunctionName As New System.Diagnostics.StackFrame

            '    sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oSqlEx.ToString)
            '    APDPartnerFoundation.LogHyperquestPartnerEvent(sProcessingServer, sEventTransactionID, "HyperquestService(ERROR" & iErrorEventSeq.ToString & ")", CStr(iErrorEventSeq), "ERROR", "<== Failed to update the JobStatus in the database: " & sStoredProcedure, oSqlEx.Message, sError)
            '    iErrorEventSeq = iErrorEventSeq + 1
            '    Return False

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("Hyperquest", "ERROR(999)", "Failed to insert job data into the database.", "Failed to insert job data into the utb_hyperquestsvc_jobs table.  Details in EventXML", " StoredProc: " & sStoredProcedure & " Params: " & sParams & "  Details: " & sError)
            Return True
        Finally
        End Try
    End Function

End Class