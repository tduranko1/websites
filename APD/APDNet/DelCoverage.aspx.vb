﻿Imports System.Xml

Public Class DelCoverge
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    Dim sCRUD As String = ""
    'Dim iInsuranceCompanyID As Integer = 0
    'Dim iSelectedCoverageTypeID As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnDelCoverageYes_Click(sender As Object, e As EventArgs) Handles btnDelCoverageYes.Click
        Dim sLynxID As Integer = 0
        Dim sUserID As String = ""
        Dim oReturnXML As XmlElement = Nothing
        Dim sSelectedClientCoverageTypeID As String = ""
        Dim sCoverageSysLastUpdatedDate As String = ""
        Dim sSelectedClaimCoverageID As String = ""

        Try
            '-------------------------------
            ' Check Session Data
            '-------------------------------
            sLynxID = Request.QueryString("LynxID")
            'txtLynxID.Text = sLynxID
            sUserID = Request.QueryString("UserID")

            sSelectedClaimCoverageID = Request.QueryString("SelectedClientCoverageTypeID")
            sSelectedClientCoverageTypeID = Request.QueryString("SelectedClaimCoverageID")
            sCoverageSysLastUpdatedDate = Request.QueryString("CoverageSysLastUpdatedDate")

            '    iInsuranceCompanyID = Request.QueryString("InscCompID")
            '    sUserID = Request.QueryString("UserID")

            '-------------------------------
            ' Process Validation
            '-------------------------------
            '??? Verify we got good params

            '-------------------------------
            ' Apply CRUD
            '-------------------------------

            '-------------------------------
            ' Process Main Code
            '-------------------------------
            '-------------------------------
            ' Database access
            '-------------------------------  
            Dim sStoredProcedure As String = "uspClaimCoverageDelWSXML"
            Dim sParams As String = "@ClaimCoverageID=" & sSelectedClaimCoverageID & ", @ClientCoverageTypeID=" & sSelectedClientCoverageTypeID & ", @LynxID=" & sLynxID & ", @UserID=" & sUserID & ", @SysLastUpdatedDate='" & sCoverageSysLastUpdatedDate & "'"

            '-------------------------------
            ' Call WS and get data
            '-------------------------------
            oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "OnClose('Save');", True)
            '-------------------------------
            ' Check Results
            '-------------------------------
            ' ??? Check oReturnXML

            '-------------------------------
            ' Clean-up
            '-------------------------------
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            wsAPDFoundation.LogEvent("DelCoverage", "ERROR", "Coverage Delete data not found or data error occured.", oExcept.Message, sError)

            Response.Write(oExcept.ToString)
            'Return ""
        Finally
            oReturnXML = Nothing
        End Try
    End Sub

    'Protected Sub btnCoverageAddSave_Click(sender As Object, e As EventArgs) Handles btnCoverageDelSave.Click
    '    Dim oReturnXML As XmlElement = Nothing
    '    Dim oXMLNode As XmlNode = Nothing
    '    Dim sLimitDailyAmt As String = 0
    '    Dim sMaximumDays As String = 0
    '    Dim aCoverageParams As Array = Nothing
    '    Dim sClaimCoverageID As String = ""
    '    Dim iAdditionalCoverageFlag As Integer = 0

    '    Try
    '        '-------------------------------
    '        ' Validate the web page data
    '        '-------------------------------  
    '        '??????????? CODE THIS ????????????????????
    '        'lblClaimSaved.Text = ""

    '        '-------------------------------
    '        ' Process DB Call
    '        '-------------------------------  
    '        Dim sStoredProcedure As String = "uspClaimCoverageUpdDetailWSXML"

    '        sClaimCoverageID = "0"
    '        If UCase(txtCoverageAdditionalFlag.Text) = "YES" Then
    '            iAdditionalCoverageFlag = 1
    '        Else
    '            iAdditionalCoverageFlag = 0
    '        End If


    '        Dim sParams As String = "@ClaimCoverageID = " & sClaimCoverageID _
    '            & ", @ClientCoverageTypeID = " & CStr(iSelectedCoverageTypeID) _
    '            & ", @LynxID = " & CStr(iLynxID) _
    '            & ", @InsuranceCompanyID = " & CStr(iInsuranceCompanyID) _
    '            & ", @AddtlCoverageFlag = " & iAdditionalCoverageFlag _
    '            & ", @CoverageTypeCD = '" & txtCoverageClientCode.Text & "'" _
    '            & ", @Description = '" & ddlCoverageDescription.SelectedItem.Text & "'" _
    '            & ", @DeductibleAmt = " & txtCoverageDeductible.Text _
    '            & ", @LimitAmt = " & txtCoverageLimit.Text _
    '            & ", @LimitDailyAmt = " & txtCoverageDailyLimit.Text _
    '            & ", @MaximumDays = " & txtCoverageMaxDays.Text _
    '            & ", @UserID = " & sUserID _
    '            & ", @SysLastUpdatedDate = " & "''"

    '        '?????? Code This ?????
    '        '    oNode.setAttribute("AddlCoverageFlagInd", (txtAdditionalFlag.value == "Yes" ? "/images/tick.gif" : "/images/spacer.gif"));

    '        'Debug.Print(sParams)

    '        '-------------------------------
    '        ' Call WS and get data
    '        '-------------------------------
    '        oReturnXML = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

    '        '???? check for success and save new Coverage syslastupdateddate ????

    '        Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri)

    '    Catch oExcept As Exception
    '        '---------------------------------
    '        ' Error handler and notifications
    '        '---------------------------------
    '        Dim sError As String = ""
    '        Dim sBody As String = ""
    '        Dim FunctionName As New System.Diagnostics.StackFrame

    '        sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
    '        wsAPDFoundation.LogEvent("CoverageAddSaveDetails", "ERROR", "Add Coverage Save not processed or data error occured.", oExcept.Message, sError)

    '        Response.Write(oExcept.ToString)

    '    Finally
    '        oXMLNode = Nothing
    '    End Try
    'End Sub

    'Protected Sub ddlCoverageDescription_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCoverageDescription.SelectedIndexChanged
    '    Dim aCoverageData As Array = Nothing
    '    Dim iCoverageTypeID As Integer = 0

    '    aCoverageData = ddlCoverageDescription.SelectedItem.Value.Split("|")
    '    txtCoverageClientCode.Text = aCoverageData(0)
    '    txtCoverageType.Text = aCoverageData(1)
    '    iCoverageTypeID = CInt(aCoverageData(2))
    '    txtCoverageAdditionalFlag.Text = aCoverageData(3)
    '    txtCoverageDailyLimit.Text = 0
    '    txtCoverageDeductible.Text = 0
    '    txtCoverageLimit.Text = 0
    '    txtCoverageMaxDays.Text = 0
    '    iSelectedCoverageTypeID = iCoverageTypeID

    '    '-------------------------
    '    ' Additional Flag 
    '    '-------------------------
    '    Select Case txtCoverageAdditionalFlag.Text
    '        Case "0"
    '            txtCoverageAdditionalFlag.Text = "No"
    '        Case "1"
    '            txtCoverageAdditionalFlag.Text = "Yes"
    '    End Select

    '    '-------------------------
    '    ' Rental 
    '    '-------------------------
    '    If UCase(txtCoverageClientCode.Text) = "RENT" Then
    '        lblRentalMax.Visible = True
    '        lblRentalLimit.Visible = True
    '        txtCoverageMaxDays.Visible = True
    '        txtCoverageDailyLimit.Visible = True
    '    Else
    '        lblRentalMax.Visible = False
    '        lblRentalLimit.Visible = False
    '        txtCoverageMaxDays.Visible = False
    '        txtCoverageDailyLimit.Visible = False
    '    End If
    'End Sub

End Class