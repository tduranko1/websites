﻿'--------------------------------------------------------------
' Program: VehicleInvolvedCond
' Author:  Mahes
' Date:    Jul 31, 2013
' Version: 1.0
'
' Description:  This site provides the VehicleInvolvedCond for APD replacing the 
'               existing COM based classic asp functions. 
'--------------------------------------------------------------

Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Public Class VehicleInvolvedCond
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As PGWAPDFoundation.APDService

    'Page Load'
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sLynxID As String = ""
        Dim sUserID As String = ""
        Dim sInvolvedCRUD As String = ""
        Dim sInsuranceCompanyID As String = ""
        Dim oSession As Object
        Dim XMLCRUD As XmlElement
        Dim sXslHash As New Hashtable
        Dim sparams As String = ""
        Dim uspXMLRaw As XmlElement
        Dim uspXMLXDoc As New XmlDocument

        '-------------------------------
        ' Session/Local Variables
        '-------------------------------
        Dim sStoredProcedure As String = "uspVehicleInvolvedGetDetailXML"
        Dim sXSLProcedure As String = "VehicleInvolvedCond.xsl"

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        Dim sSessionKey As String = Request.QueryString("SessionKey")
        Dim sInvolvedID As String = Request.QueryString("InvolvedID")
        Dim sWindowID As String = Request.QueryString("WindowID")
        Dim sASPParams As String = Request.QueryString("Params")

        Try
            'If sSessionKey = "" Then
            '    Throw New SystemException("SessionValidationError: No session information received from classic ASP page.")
            'Else
            '    'Dim APDDBUtils As New APDDBUtilities(sSessionKey, sWindowID)

            '    '---------------------------------
            '    ' Debug Data
            '    '---------------------------------
            '    If UCase(APDDBUtils.APDNetDebugging) = "TRUE" Then
            '        wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "VehicleInvolvedCond.aspx - Start: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", ASPParam = " & sASPParams)

            '        '---------------------------------
            '        ' Get the session data
            '        '---------------------------------
            '        If Not Request("UserID") Then
            '            sUserID = APDDBUtils.UserID
            '        Else
            '            sUserID = Request("UserID")
            '        End If

            '        If Not Request("LynxID") Then
            '            sLynxID = APDDBUtils.LynxID
            '        Else
            '            sUserID = Request("LynxID")
            '        End If

            '        oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "InsuranceCompanyID-" & sWindowID)
            '        sInsuranceCompanyID = oSession(0).Sessionvalue


            '        '---------------------------------
            '        ' Get the permission data
            '        '---------------------------------
            '        XMLCRUD = wsAPDFoundation.ExecuteSpAsXML("uspCRUDGetDetailWSXML", "Involved" & sUserID)
            '        sInvolvedCRUD = APDDBUtils.GetCrud(XMLCRUD)

            '        '-----------------------------------
            '        ' Add config variables as XslParams
            '        '-----------------------------------

            '        sXslHash.Add("WindowID", sWindowID)
            '        sXslHash.Add("UserId", sUserID)
            '        sXslHash.Add("LynxId", sLynxID)
            '        sXslHash.Add("Involved", sInvolvedCRUD)

            '        '------------------------------------
            '        ' If ASPParms, parse and add to hash
            '        '------------------------------------
            '        If sASPParams <> "" Then
            '            Dim aParams As Array
            '            aParams = Split(sASPParams, "|")
            '            For Each sASPParam In aParams
            '                Dim aValues As Array
            '                aValues = Split(sASPParam, ":")
            '                sXslHash.Add(aValues(0), aValues(1))
            '            Next
            '        End If


            '        'Need to TODO - Check
            '        '---------------------------------
            '        ' Debug Data
            '        '---------------------------------
            '        If UCase(APDDBUtils.APDNetDebugging) = "TRUE" Then
            '            wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls/Parameters." _
            '                                     , "Session controls for Window: " & sWindowID _
            '                                     & " and SessionKey: " & sSessionKey _
            '                                     , "Vars: StoredProc = " & sStoredProcedure _
            '                                     & ", XSLPage = " & sXSLProcedure _
            '                                     & ", UserID(Session) = " & sUserID _
            '                                     & ", LynxID(Session) = " & sLynxID _
            '                                     & ", Involved = " & sInvolvedID _
            '                                     & ", "
            '                                     )
            '        End If

            '        '---------------------------------
            '        ' XSL Transformation
            '        '---------------------------------
            '        sparams = sInvolvedID
            '        sparams += "," & sInsuranceCompanyID

            '        '---------------------------------
            '        ' StoredProc XML Data
            '        '---------------------------------
            '        uspXMLRaw = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sparams)
            '        If uspXMLRaw.OuterXml = Nothing Then
            '            'Throw Exception
            '        Else
            '            uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)

            '            Response.Write(APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sparams, sXslHash))
            '        End If

            '        If UCase(APDDBUtils.APDNetDebugging) = "TRUE" Then
            '            wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "APDDiary.aspx - Finished: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
            '        End If

            '    End If
            'End If

        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls/parameter not found or page pre-transformation failed.", "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID & ", ASPParam = " & sASPParams)

            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        End Try

    End Sub

End Class