﻿'--------------------------------------------------------------
' Program: APD Custom Forms
' Author:  Thomas Duranko
' Date:    Sept 25, 2013
' Version: 1.0
'
' Description:  This site provides the Custom Forms for APD replacing the 
'               existing COM based classic asp functions. 
'--------------------------------------------------------------
Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Public Class CustomForms
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService

    '--------------------------------------------------------------
    ' Page Load
    '--------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '-------------------------------
        ' Globals
        '-------------------------------
        Dim sParams As String = ""
        'Dim oSession As Object
        Dim sXslHash As New Hashtable
        'Dim sVehCount = ""
        'Dim iRC As Integer = 0
        'Dim sSessionVariable As String = ""
        'Dim sSessionVariableValue As String = ""
        Dim uspXMLRaw As XmlElement
        Dim uspXMLXDoc As New XmlDocument
        'Dim XMLSessionNode As XmlNode
        Dim sHTML As String = ""
        Dim sAPDNetDebugging As String = ""
        Dim dtStart As Date
        Dim APDDBUtils As APDDBUtilities = Nothing
        'Dim sInsuranceCompanyID As String = ""
        'Dim sContext As String = ""
        'Dim sClaimStatus As String = ""
        'Dim XMLCRUD As XmlElement = Nothing
        'Dim sInfoCRUD As String = ""
        'Dim sAction As String = ""
        'Dim sClaimAspectID As String = ""

        Dim sUserID As String = ""
        Dim sLynxID As String = ""
        Dim sVehNum As String = ""
        Dim sInsCoId As String = ""

        '-------------------------------
        ' Session/Local Variables
        '-------------------------------
        Dim sStoredProcedure As String = "uspCustomFormsGetListWSXML"
        Dim sXSLProcedure As String = "CustomForms.xsl"

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        Dim sSessionKey = Request.QueryString("SessionKey")
        Dim sASPParams = Request.QueryString("Params")

        Try
            '---------------------------------
            ' New Session Data
            '---------------------------------
            If UCase(AppSettings("Debug")) = "TRUE" Then
                sAPDNetDebugging = "TRUE"
            End If

            APDDBUtils = New APDDBUtilities()
            dtStart = Date.Now

            '---------------------------------
            ' Get the permission data
            '---------------------------------
            'XMLCRUD = wsAPDFoundation.ExecuteSpAsXML("uspCRUDGetDetailWSXML", "'Client Billing', " & sUserID)
            'sInfoCRUD = APDDBUtils.GetCrud(XMLCRUD)

            '-----------------------------------
            ' Add config variables as XslParams
            '-----------------------------------
            'sXslHash.Add("LynxID", sLynxID)
            'sXslHash.Add("UserID", sUserID)
            'sXslHash.Add("VehicleNum", sVehCount)
            'sXslHash.Add("WindowID", sWindowID)
            'sXslHash.Add("ClaimStatus", sClaimStatus)
            'sXslHash.Add("InfoCRUD", sInfoCRUD)
            'sXslHash.Add("Context", sContext)
            'sXslHash.Add("InsuranceCompanyID", sInsuranceCompanyID)

            '------------------------------------
            ' If ASPParms, parse and add to hash
            '------------------------------------
            If sASPParams <> "" Then
                Dim aParams As Array
                aParams = Split(sASPParams, "|")
                For Each sASPParam In aParams
                    Dim aValues As Array
                    aValues = Split(sASPParam, ":")
                    sXslHash.Add(aValues(0), aValues(1))
                Next
            End If

            sLynxID = sXslHash("LynxId")
            sUserID = sXslHash("UserId")
            sVehNum = sXslHash("VehNum")
            sInsCoId = sXslHash("InsCoId")

            '-------------------------------------
            ' Debugging
            '-------------------------------------
            'Response.Write("sUserID" & " - " & sUserID & "<br/>")
            'Response.Write("sLynxID" & " - " & sLynxID & "<br/>")
            'Response.Write("sVehNum" & " - " & sVehNum & "<br/>")
            'Response.Write("sInsCoId" & " - " & sInsCoId & "<br/>")
            'Response.Write("sASPParams" & " - " & sASPParams & "<br/>")
            'Response.End()

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "Session controls/Parameters." _
                                         , "Session controls for Window: " & "" _
                                         & " and SessionKey: " & sSessionKey _
                                         , "Vars: StoredProc = " & sStoredProcedure _
                                         & ", XSLPage = " & sXSLProcedure _
                                         & ", UserID = " & sUserID _
                                         & ", LynxID = " & sLynxID _
                                         & ", VehNum = " & sVehNum _
                                         & ", InsCoId = " & sInsCoId _
                                         & ", sASPParams = " & sASPParams _
                                         )
            End If

            '---------------------------------
            ' XSL Transformation
            '---------------------------------
            sParams = sLynxID
            sParams += "," & sUserID

            '---------------------------------
            ' StoredProc XML Data
            '---------------------------------
            uspXMLRaw = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)
            If uspXMLRaw.OuterXml = Nothing Then
                ' Throw exception
            Else
                uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)

                If UCase(sAPDNetDebugging) = "TRUE" Then
                    sXslHash.Add("RunTime", Now.Subtract(dtStart).Milliseconds & " msec")
                Else
                    sXslHash.Add("RunTime", Chr(149))
                End If

                '---------------------------------
                ' Call Data Presenter
                '---------------------------------
                sHTML = APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sParams, sXslHash)
                Response.Write(sHTML)
            End If

            'uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)

            If UCase(sAPDNetDebugging) = "TRUE" Then
                wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "CustomForms.aspx - Finished: " & Date.Now, "Session controls for Window: " & "" & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls/parameter not found or page pre-transformation failed.", "Session controls for Window: " & "" & " and SessionKey: " & sSessionKey, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID)

            Response.Write(sError)
            Response.End()
            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        End Try
    End Sub
End Class