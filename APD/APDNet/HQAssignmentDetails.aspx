﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="HQAssignmentDetails.aspx.vb" Inherits="APDNet.HQAssignmentDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>HQ Assignment Detials</title>
    <link href="css/APD2.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmHQAssignmentDetails" runat="server">
    <div id="Main" style="position: absolute; z-index: 1; top: 5px; left: 5px;">
        <!-- Shop Assignment Title Bar -->
        <table cellpadding="0" cellspacing="0" width="675" border="0">
            <tr>
                <td align="left" valign="top" width="8" style="background-image:url(images/PanelLeft.png); background-repeat:no-repeat;">
                </td>
                <td align="left" class="Header"style="background-image:url(images/PanelMiddle.png); background-repeat:repeat;">
                    <a name="ShopSearch" style="color: White; font-weight: bold;" >
                        HQ Assignment Status: <asp:Label ID="lblLynxID" runat="server"></asp:Label>
                    </a>
                </td>
                <td align="left" valign="top" width="8" style="background-image:url(images/PanelRight.png); background-repeat:no-repeat;">
                </td>
            </tr>
        </table> 

        <!-- HQ Status Details -->
        <asp:Panel ID="pnlHQDetails" runat="server" BorderWidth="1" Style="padding: 5px 5px 5px 5px;">
            <table border="0" cellspacing="0" cellpadding="1" style="width: 675;">
                <tr>
                    <td align="right" style="white-space: nowrap;">
                        Job Status:
                    </td>
                    <td align="left" colspan="3">
                        <div>
                            <asp:TextBox ID="txtJobStatus" Width="150" CssClass="APDInputInActive" runat="server"
                                Visible="true" Enabled="false"></asp:TextBox>
                         </div>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="white-space: nowrap;">
                        Job Status Details:
                    </td>
                    <td align="left" colspan="3">
                        <div>
                            <asp:TextBox ID="txtJobStatusDetails" Width="300" CssClass="APDInputInActive" runat="server"
                                Visible="true" Enabled="false"></asp:TextBox>
                         </div>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="white-space: nowrap;">
                        Job Sent to HQ Date:
                    </td>
                    <td align="left" colspan="3">
                        <div>
                            <asp:TextBox ID="txtJobSenttoHQDate" Width="150" CssClass="APDInputInActive" runat="server"
                                Visible="true" Enabled="false"></asp:TextBox>
                         </div>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top" style="white-space: nowrap;">
                        HQ Messages:
                    </td>
                    <td align="left" colspan="3">
                        <div>
                            <asp:TextBox ID="txtHQMessages" Width="500" Height="60" CssClass="APDInputInActive" runat="server"
                                Visible="true" Enabled="false"></asp:TextBox>
                         </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <!-- Shop Assignment Footer Bar -->
        <table cellpadding="0" cellspacing="0" width="675" border="0">
            <tr>
                <td align="left" valign="top" width="8" style="background-image:url(images/PanelBotLeft.png); background-repeat:no-repeat;">
                </td>
                <td align="left" class="Header"style="background-image:url(images/PanelMiddle.png); background-repeat:repeat;">
                    &nbsp;
                </td>
                <td align="left" valign="top" width="8" style="background-image:url(images/PanelBotRight.png); background-repeat:no-repeat;">
                </td>
            </tr>
        </table> 
    
    </div>
    </form>
</body>
</html>
