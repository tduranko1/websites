﻿'--------------------------------------------------------------
' Program: APDDiary
' Author:  Thomas Duranko
' Date:    Jun 03, 2013
' Version: 1.0
'
' Description:  This site provides the APDDiary for APD replacing the 
'               existing COM based classic asp functions. 
'
' Updates:
'           23Jun2014 - Updated the LogEvent code to add the 
'                       server IP so we know where it came from
'--------------------------------------------------------------
Imports System.Xml
Imports System.IO
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager

Imports System.Web.Caching
Imports System.Web.Caching.Cache

Public Class APDDiary
    Inherits System.Web.UI.Page

    Dim wsAPDFoundation As New PGWAPDFoundation.APDService
    Dim sHostingServer As String = ""
    Dim sUserComputer As String = ""

    '--------------------------------------------------------------
    ' Page Load
    '--------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '-------------------------------
        ' Globals
        '-------------------------------
        Dim sLynxID As String = ""
        Dim sUserID As String = ""
        Dim sShowAll As String = ""
        Dim sWindowState As String = ""
        Dim sInsuranceCompanyID As String = ""
        Dim sParams As String = ""
        Dim sASPParam As Object = Nothing
        Dim oSession As Object = Nothing
        Dim sXslHash As New Hashtable
        Dim XMLCRUD As XmlElement = Nothing
        Dim sVehClaimAspectID = ""
        Dim sVehCount = ""
        Dim sNoteCRUD = ""
        Dim sTaskCRUD = ""
        Dim uspXMLRaw As XmlElement = Nothing
        Dim uspXMLXDoc As New XmlDocument
        Dim sAPDNetDebugging As String = ""
        Dim dtStart As Date = Nothing
        Dim APDDBUtils As APDDBUtilities = Nothing
        'Dim iTime As Decimal = 25000 ' Wait sec

        '----------------------------------
        ' Debug Params - Processing Server
        '----------------------------------
        sHostingServer = Request.ServerVariables("LOCAL_ADDR")
        sUserComputer = Request.ServerVariables("REMOTE_ADDR")

        '-------------------------------
        ' Session/Local Variables
        '-------------------------------
        Dim sStoredProcedure As String = "uspDiaryGetListWSXML"
        Dim sXSLProcedure As String = "APDDiary.xsl"

        '-------------------------------
        ' Get Session Controls from
        ' Classic ASP page
        '-------------------------------
        Dim sSessionKey = Request.QueryString("SessionKey")
        Dim sWindowID = Request.QueryString("WindowID")
        Dim sASPParams = Request.QueryString("Params")

        'sASPParams = "showAll:|ShowAllTasks:|winState:"
        'sASPParams = "showAll:|ShowAllNotes:|LynxID:4819979|UserID:7372|InsuranceCompanyID:184|VehCount:1|VehClaimAspectID:1279584"
        'sSessionKey = "{4730C0D9-75F6-4C92-8DE0-C2A51157FD6D}"
        'sSessionKey = "{D1B5711E-813F-4198-82D0-26BEDDFB257B}"
        'sSessionKey = "{7E8F1898-73A8-4FEA-A570-F9DEB880CA74}"
        'sSessionKey = "{74CB1FBC-FEE6-4AF6-AEE6-8CF00918BAE2}"
        'sWindowID = 1

        '?SessionKey={74CB1FBC-FEE6-4AF6-AEE6-8CF00918BAE2}&WindowID=1&Params=winState:|ShowAllNotes:|LynxID:4819979|UserID:7372|InsuranceCompanyID:184|VehCount:1|VehClaimAspectID:1279584

        'Dim APDDBUtils As New APDDBUtilities

        '---------------------------------
        ' Turn debugging on or off
        '---------------------------------
        sAPDNetDebugging = ""
        If UCase(AppSettings("Debug")) = "TRUE" Then
            sAPDNetDebugging = "TRUE"
        End If

        '---------------------------------
        ' Debug Data
        '---------------------------------
        If UCase(sAPDNetDebugging) = "TRUE" Then
            wsAPDFoundation.LogEvent("APDNET-APDDiary", "Start", "APDDiary Load - Start: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey & " Session Vars: " & sASPParams, "")
        End If

        Try
            '---------------------------------
            ' Make sure we go the session vars
            '---------------------------------
            If sWindowID Is Nothing Or sWindowID = "" Then
                wsAPDFoundation.LogEvent("APDNet-APDDiary", "CriticalSessionError", "APDDiary - Session data not passed: " & Date.Now, "Session controls for Window: sWindowsID=" & sWindowID & " and SessionKey: " & sSessionKey & " and ASPParam = " & sASPParams & " and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, "")
            End If

            If sSessionKey = "" Then
                Throw New SystemException("SessionValidationError: No session information received from classic ASP page.")
            Else
                '------------------------------------
                ' If ASPParms, parse and add to hash
                '------------------------------------
                If sASPParams <> "" Then
                    Dim aParams As Array
                    aParams = Split(sASPParams, "|")
                    For Each sASPParam In aParams
                        Dim aValues As Array
                        aValues = Split(sASPParam, ":")
                        sXslHash.Add(aValues(0), aValues(1))
                        'Response.Write(aValues(0) & ":" & aValues(1))
                    Next
                End If

                sLynxID = sXslHash.Item("LynxID")
                sUserID = sXslHash.Item("UserID")
                sInsuranceCompanyID = sXslHash.Item("InsuranceCompanyID")
                sVehCount = sXslHash.Item("VehCount")
                sVehClaimAspectID = sXslHash.Item("VehClaimAspectID")
                sWindowState = sXslHash.Item("winState")
                sShowAll = sXslHash.Item("ShowAllNotes")

                '-------------------------------
                ' If Session Then controls Not passed
                ' go get them from the session DB
                '-------------------------------
                Try
                    If (sLynxID = "" Or sUserID = "" Or sInsuranceCompanyID = "" Or sInsuranceCompanyID = "" Or sVehClaimAspectID = "") Then
                        '---------------------------------
                        ' New Session Data
                        '---------------------------------
                        oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "LynxID-" & sWindowID)
                        sLynxID = oSession(0).SessionValue

                        oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "UserID-" & sWindowID)
                        sUserID = oSession(0).SessionValue

                        oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "InsuranceCompanyID-" & sWindowID)
                        sInsuranceCompanyID = oSession(0).SessionValue

                        oSession = wsAPDFoundation.GetClassicSessionVars(sSessionKey, "VehClaimAspectID-" & sWindowID)
                        sVehClaimAspectID = oSession(0).SessionValue
                    End If
                Catch ex As Exception
                    wsAPDFoundation.LogEvent("APDNET-APDDiary", "TryCatch", "APDDiary - Session data: " & Date.Now, " Session controls for Window: sWindowsID =" & sWindowID & " and SessionKey: " & sSessionKey & " and ASPParam LynxID= " & sLynxID & ", UserID= " & sUserID & " , InsuranceCompanyID= " & sInsuranceCompanyID & " , VehCount= " & sVehCount & " , VehClaimAspectID= " & sVehClaimAspectID & " , WindowState= " & sWindowState & ", ShowAll= " & sShowAll & " and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, "")
                End Try

                '---------------------------------------
                ' Check the Session Controls
                '---------------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNET-APDDiary", "Session Vars 2", "APDDiary - Session data passed in: " & Date.Now, " Session controls for Window: sWindowsID =" & sWindowID & " and SessionKey: " & sSessionKey & " and ASPParam LynxID= " & sLynxID & ", UserID= " & sUserID & " , InsuranceCompanyID= " & sInsuranceCompanyID & " , VehCount= " & sVehCount & " , VehClaimAspectID= " & sVehClaimAspectID & " , WindowState= " & sWindowState & ", ShowAll= " & sShowAll & " and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, "")
                End If

                'Response.Write("sLynxID: " & sLynxID)
                'Response.Write("<br/>sUserID: " & sUserID)
                'Response.Write("<br/>sInsuranceCompanyID: " & sInsuranceCompanyID)
                'Response.Write("<br/>sVehCount: " & sVehCount)
                'Response.Write("<br/>sVehClaimAspectID: " & sVehClaimAspectID)
                'Response.Write("<br/>sWindowState: " & sWindowState)
                'Response.Write("<br/>sShowAll: " & sShowAll)
                'Response.End()

                '---------------------------------------
                ' Log the Session Controls
                '---------------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNET-APDDiary", "Session Vars", "APDDiary - Session data passed in: " & Date.Now, " Session controls for Window: sWindowsID =" & sWindowID & " and SessionKey: " & sSessionKey & " and ASPParam LynxID= " & sLynxID & ", UserID= " & sUserID & " , InsuranceCompanyID= " & sInsuranceCompanyID & " , VehCount= " & sVehCount & " , VehClaimAspectID= " & sVehClaimAspectID & " , WindowState= " & sWindowState & ", ShowAll= " & sShowAll & " and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, "")
                End If

                '---------------------------------------
                ' Make sure we got the key session vars
                '---------------------------------------
                If sLynxID Is Nothing Or sLynxID = "" Or sUserID Is Nothing Or sUserID = "" Or sInsuranceCompanyID Is Nothing Or sInsuranceCompanyID = "" Then
                    wsAPDFoundation.LogEvent("APDNet", "CriticalSessionError", "APDDiary - Session data not passed: ", "Session controls for Window: sWindowsID=" & sWindowID & " and SessionKey: " & sSessionKey & " and ASPParam = " & sASPParams & " and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, "")
                End If

                APDDBUtils = New APDDBUtilities()
                dtStart = Date.Now

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "APDDiary.aspx - Start: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey & " and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", ASPParam = " & sASPParams)
                End If

                '---------------------------------
                ' Get configuration data
                '---------------------------------
                'XMLReturnElem = wsAPDFoundation.GetConfigXMLData("Root/Application/WorkFlow/EventCodes/Code[@name='VehicleAssignmentSentToShop']")
                'If XMLReturnElem.InnerXml.Contains("ERROR:") Then
                ' Throw New SystemException("ConfigXMLError: Could not find the VehicleAssignmentSentToShop configuration variable.")
                'Else
                'sEvtVeicleAssignmentSentToShop = XMLReturnElem.InnerText
                'End If

                '---------------------------------
                ' Get the permission data
                '---------------------------------
                'XMLCRUD = wsAPDFoundation.ExecuteSpAsXML("uspCRUDGetDetailWSXML", "Note, " & sUserID)
                'sNoteCRUD = APDDBUtils.GetCrud(XMLCRUD)

                '-------------------------------------
                ' If CRUD is blank, default it to CRU_
                '-------------------------------------
                'If sNoteCRUD = "" Then
                ' sNoteCRUD = "CRU_"
                ' wsAPDFoundation.LogEvent("APDNet", "CRUD_ERROR", "Note CRUD set to default value CRU_", "XMLCRUD: and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, XMLCRUD.InnerXml)
                'End If

                'XMLCRUD.InnerXml = ""
                XMLCRUD = wsAPDFoundation.ExecuteSpAsXML("uspCRUDGetDetailWSXML", "'Task', " & sUserID)
                sTaskCRUD = APDDBUtils.GetCrud(XMLCRUD)

                '-------------------------------------
                ' If CRUD is blank, default it to CRU_
                '-------------------------------------
                If sTaskCRUD = "" Then
                    sTaskCRUD = "CRU_"
                    wsAPDFoundation.LogEvent("APDNet", "CRUD_ERROR", "Diary CRUD set to default value CRU_", "XMLCRUD: and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, XMLCRUD.InnerXml)
                End If

                '-----------------------------------
                ' Add config variables as XslParams
                '-----------------------------------
                sXslHash.Add("WindowID", sWindowID)
                sXslHash.Add("windowState", sWindowState)
                'sXslHash.Add("Note", sNoteCRUD)
                sXslHash.Add("Task", sTaskCRUD)

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING-" & sHostingServer, "Session controls/Parameters." _
                                             , "Session controls for Window: " & sWindowID _
                                             & " and SessionKey: " & sSessionKey _
                                             & " and HostingServer: " & sHostingServer _
                                             & " and UserAddr: " & sUserComputer _
                                             , "Vars: StoredProc = " & sStoredProcedure _
                                             & ", XSLPage = " & sXSLProcedure _
                                             & ", UserID(Session) = " & sUserID _
                                             & ", LynxID(Session) = " & sLynxID _
                                             & ", winState = " & sWindowState _
                                             & ", showAll = " & sShowAll _
                                             & ", SubordinateUserID = " & Request("SubordinateUserID") _
                                             & ", SubordinatesFlag = " & Request("SubordinatesFlag") _
                                             & ", ShowAllTasks = " & Request("ShowAllTasks") _
                                             & ", NoteCRUD = " & sNoteCRUD _
                                             & ", TaskCRUD = " & sTaskCRUD _
                                             )
                End If

                '---------------------------------
                ' XSL Transformation
                '---------------------------------
                sParams = sLynxID
                sParams += ", " & sUserID
                sParams += ", " & IIf(Request("SubordinateUserID") Is Nothing, "NULL", Request("SubordinateUserID"))
                sParams += ", " & IIf(Request("SubordinatesFlag") Is Nothing, "NULL", Request("SubordinatesFlag"))

                '---------------------------------
                ' StoredProc XML Data
                '---------------------------------
                uspXMLRaw = wsAPDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)
                If uspXMLRaw.OuterXml = Nothing Then
                    ' Throw exception
                Else
                    uspXMLXDoc.LoadXml(uspXMLRaw.OuterXml)

                    If UCase(sAPDNetDebugging) = "TRUE" Then
                        sXslHash.Add("RunTime", Now.Subtract(dtStart).Milliseconds & " msec")
                    Else
                        sXslHash.Add("RunTime", Chr(149))
                    End If

                    Response.Write(APDDBUtils.DataPresenterNet(sStoredProcedure, uspXMLXDoc, sXSLProcedure, sParams, sXslHash))
                End If

                If UCase(sAPDNetDebugging) = "TRUE" Then
                    wsAPDFoundation.LogEvent("APDNet", "DEBUGGING", "APDDiary.aspx - Finished: " & Date.Now, "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey & " and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure)
                End If

                '-----------------------------------
                ' Clean-up the mess
                '-----------------------------------
                'APDDBUtils = Nothing
            End If
        Catch oExcept As Exception
            '---------------------------------
            ' Error handler and notifications
            '---------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            'sError = String.Format("Error: {0}", "APDNet Failed: Session controls/parameter not found or page pre-transformation failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            sError = String.Format("Error: {0}", " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            wsAPDFoundation.LogEvent("APDNet", "ERROR", "Session controls/parameter not found or page pre-transformation failed.", "Session controls for Window: " & sWindowID & " and SessionKey: " & sSessionKey & " and HostingServer: " & sHostingServer & " and UserAddr: " & sUserComputer, "Vars: StoredProc = " & sStoredProcedure & ", XSLPage = " & sXSLProcedure & ", UserID(Session) = " & sUserID & ", LynxID(Session) = " & sLynxID & ", ASPParam = " & sASPParams & sError)

            '---------------------------------
            ' Email notifications
            '---------------------------------
            'wsAPDFoundation.SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "Data", "Vars " & StoredProcedure & ", " & xslPage & ", " & Parameters, "rocSMTP01.pgw.local")
        Finally
            '-----------------------------------
            ' Clean-up the mess
            '-----------------------------------
            wsAPDFoundation.Dispose()
            APDDBUtils = Nothing
            sASPParam = Nothing
            oSession = Nothing
            sXslHash = Nothing
            XMLCRUD = Nothing
            uspXMLRaw = Nothing
            uspXMLXDoc = Nothing
            'APDDBUtils = Nothing
            GC.Collect()
            GC.SuppressFinalize(Me)
        End Try
    End Sub
End Class