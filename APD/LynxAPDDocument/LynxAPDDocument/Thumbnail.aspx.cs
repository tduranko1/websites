﻿using System;
using System.Web;
using System.Net;
using System.Xml;
using System.IO;

namespace LynxAPDDocument
{
    public partial class Thumbnail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //------------------------------------------------
            // TVD - 26Apr2017
            // Adding security to the page tied to ClaimPoint
            //------------------------------------------------
            try
            {
                String sCookie;
                sCookie = Request.Cookies["CPSession"]["authUser"];
                //Response.Write(sCookie.Length);

                if (sCookie.Length > 0)
                {
                    string strFilePath = string.Empty, strHeight = string.Empty, strWidth = string.Empty,strArchived = string.Empty;
                    string responseString = string.Empty,
                strServiceURL = string.Empty, strReportData = string.Empty;
                    byte[] Base64 = null;
                    string strHostName = string.Empty;
                    XmlDocument xmlDocument = null;
                    try
                    {

                        if (Request.Cookies["lsdocPath"] != null)
                            strFilePath = Request.Cookies["lsdocPath"].Value;

                        strArchived = null;

                        if (Request.Cookies["lsimgHeight"] != null)
                            strHeight = Request.Cookies["lsimgHeight"].Value;

                        if (Request.Cookies["lsimgWidth"] != null)
                            strWidth = Request.Cookies["lsimgWidth"].Value;

                        strHostName = HttpContext.Current.Request.Url.Host;
                        //Response.Write(strHostName);
                        //Response.Write(strFilePath);

                        if (strHostName == "dapd.pgw.local")
                            strServiceURL = "http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc";
                        else if (strHostName == "sapd.pgw.local")
                            strServiceURL = "http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc";
                        else if (strHostName == "apd.pgw.local")
                            strServiceURL = "";
                        else
                            strServiceURL = "http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc";  

                        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(strServiceURL);
                        string xmlParameters = "<?xml version=\"1.0\" encoding=\"utf-8\"?><s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Header></s:Header><s:Body><GetAPDThumbnail xmlns=\"http://LynxAPDComponentServices\"><strParamXML>" + Server.UrlDecode(strFilePath) + "</strParamXML><Height>" + strHeight + "</Height><Width>" + strWidth + "</Width><Archived>" + strArchived + "</Archived></GetAPDThumbnail></s:Body></s:Envelope>";
                        xmlDocument = new XmlDocument();
                        xmlDocument.LoadXml(xmlParameters);
                        webRequest.Method = "POST";
                        webRequest.ContentType = "text/xml;charset=utf-8";
                        webRequest.KeepAlive = false;
                        webRequest.ProtocolVersion = HttpVersion.Version11;
                        webRequest.Headers.Add("SOAPAction", "http://LynxAPDComponentServices/IEcadAccessor/GetAPDThumbnail");
                        Stream responseStream = webRequest.GetRequestStream();
                        xmlDocument.Save(responseStream);
                        responseStream.Close();
                        WebResponse response = webRequest.GetResponse();
                        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                        {
                            responseString = Convert.ToString(streamReader.ReadToEnd());
                        }
                        xmlDocument.LoadXml(responseString);
                        strReportData = ((xmlDocument).DocumentElement).InnerText;
                        Base64 = Convert.FromBase64String(strReportData);
                        Response.Clear();
                        Response.ContentType = "image/png";
                        Response.AddHeader("Content-Disposition", "inline;thumbNail.png");
                        Response.BinaryWrite(Base64);
                    }
                    catch (Exception ex)
                    {
                        responseString = ex.Message.ToString();
                    }
                    finally
                    {
                        Request.Cookies["lsdocPath"].Expires = DateTime.Now.AddDays(-1);
                        Request.Cookies["lsimgHeight"].Expires = DateTime.Now.AddDays(-1);
                        Request.Cookies["lsimgWidth"].Expires = DateTime.Now.AddDays(-1);
                    }
                }
            }
            catch
            {
                //Response.Write("No Session...");
                Response.Redirect("frmLogout.asp?NL=1");
            }

        }
    }
}