﻿using System;
using System.Web;
using System.Net;
using System.Xml;
using System.IO;

namespace LynxAPDDocument
{
    public partial class frmThumbnail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //------------------------------------------------
            // TVD - 26Apr2017
            // Adding security to the page tied to ClaimPoint
            //------------------------------------------------
            try
            {
                String sCookie;
                sCookie = Request.Cookies["CPSession"]["authUser"];
                //Response.Write(sCookie.Length);

                if (sCookie.Length > 0)
                {
                    string strFilePath = string.Empty,
                        strArchived = string.Empty;
                    string responseString = string.Empty,
                strServiceURL = string.Empty, strHeight = string.Empty, strWidth = string.Empty;
                    byte[] Base64 = null;
                    string strHostName = string.Empty, strReportData = string.Empty;
                    XmlDocument xmlDocument = null;
                    try
                    {
                        strFilePath = Request.QueryString["doc"];
                        strArchived = Request.QueryString["Arc"];
                        strHostName = HttpContext.Current.Request.Url.Host;

                        if (strHostName == "apdlynxservices.pgw.local")
                            strServiceURL = "http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc";
                        else if (strHostName == "sapd.lynxservices.com")
                            strServiceURL = "http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc";
                        else if (strHostName == "apd.lynxservices.com")
                            strServiceURL = "http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc";
                        else
                            strServiceURL = "http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc";

                        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(strServiceURL);
                        string xmlParameters = "<?xml version=\"1.0\" encoding=\"utf-8\"?><s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Header></s:Header><s:Body><GetAPDThumbnail xmlns=\"http://LynxAPDComponentServices\"><strParamXML>" + strFilePath + "</strParamXML><Height>" + strHeight + "</Height><Width>" + strWidth + "</Width><Archived>" + strArchived + "</Archived></GetAPDThumbnail></s:Body></s:Envelope>";
                        xmlDocument = new XmlDocument();
                        xmlDocument.LoadXml(xmlParameters);
                        webRequest.Method = "POST";
                        webRequest.ContentType = "text/xml;charset=utf-8";
                        webRequest.KeepAlive = false;
                        webRequest.ProtocolVersion = HttpVersion.Version11;
                        webRequest.Headers.Add("SOAPAction", "http://LynxAPDComponentServices/IEcadAccessor/GetAPDThumbnail");
                        Stream responseStream = webRequest.GetRequestStream();
                        xmlDocument.Save(responseStream);
                        responseStream.Close();
                        WebResponse response = webRequest.GetResponse();
                        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                        {
                            responseString = Convert.ToString(streamReader.ReadToEnd());
                        }
                        xmlDocument.LoadXml(responseString);
                        strReportData = ((xmlDocument).DocumentElement).InnerText;
                        Base64 = Convert.FromBase64String(strReportData);
                        Response.Clear();
                        Response.ContentType = "Image/jpeg";
                        Response.BinaryWrite(Base64);
                    }
                    catch (Exception ex)
                    {
                        responseString = ex.Message.ToString();
                    }
                }
            }
            catch
            {
                //Response.Write("No Session...");
                Response.Redirect("frmLogout.asp?NL=1");
            }
        
        }
    }
}