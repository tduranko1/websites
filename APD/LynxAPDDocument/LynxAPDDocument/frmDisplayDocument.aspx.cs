﻿using System;
using System.Web;
using System.Net;
using System.Xml;
using System.IO;

namespace LynxAPDDocument
{
    public partial class frmDisplayDocument : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string strFilePath = string.Empty,
                strArchived = string.Empty;
            string responseString = string.Empty,
        strFormat = string.Empty,
            strServiceURL = string.Empty;
            byte[] Base64 = null;
            string strHostName = string.Empty, strReportData = string.Empty;
            try
            {
                strFilePath = Request.QueryString["doc"];
                strArchived = Request.QueryString["Arc"];
                strFormat = Request.QueryString["format"];
                strHostName = HttpContext.Current.Request.Url.Host;

                strFilePath = string.Concat("<Document ImageLocation='", strFilePath, "' Archived='", strArchived, "' />");

                if (strHostName == "apdlynxservices.pgw.local")
                    strServiceURL = "http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc";
                else if (strHostName == "sapd.lynxservices.com")
                    strServiceURL = "http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc";
                else if (strHostName == "apd.lynxservices.com")
                    strServiceURL = "http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc";
                else
                    strServiceURL = "http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc";


                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(strServiceURL);
                string xmlParameters = "<?xml version=\"1.0\" encoding=\"utf-8\"?><s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Header></s:Header><s:Body><GetDocument xmlns=\"http://LynxAPDComponentServices\"><strParamXML>" + "<![CDATA[" + strFilePath + "]]>" + "</strParamXML></GetDocument></s:Body></s:Envelope>";
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(xmlParameters);
                webRequest.Method = "POST";
                webRequest.ContentType = "text/xml;charset=utf-8";
                webRequest.KeepAlive = false;
                webRequest.ProtocolVersion = HttpVersion.Version11;
                webRequest.Headers.Add("SOAPAction", "http://LynxAPDComponentServices/IEcadAccessor/GetDocument");
                Stream responseStream = webRequest.GetRequestStream();
                xmlDocument.Save(responseStream);
                responseStream.Close();
                WebResponse response = webRequest.GetResponse();
                using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                {
                    responseString = Convert.ToString(streamReader.ReadToEnd());
                }
                xmlDocument.LoadXml(responseString);
                strReportData = ((xmlDocument).DocumentElement).InnerText;

                Base64 = Convert.FromBase64String(strReportData);
                Response.ContentType = strFormat;
                Response.BinaryWrite(Base64);
            }
            catch (Exception ex)
            {
                responseString = ex.Message.ToString();
            }
        }
    }
}