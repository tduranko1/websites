﻿using System;
using System.Web;
using System.Net;
using System.Xml;
using System.IO;

namespace LynxAPDDocument
{
    public partial class frmGetReportPDF : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //------------------------------------------------
            // TVD - 26Apr2017
            // Adding security to the page tied to ClaimPoint
            //------------------------------------------------
            try
            {
                String sCookie;
                sCookie = Request.Cookies["CPSession"]["authUser"];
                //Response.Write(sCookie.Length);

                if (sCookie.Length > 0)
                {
                    string strReportFormat = string.Empty, strReportData = string.Empty;
                    byte[] Base64 = null;
                    string responseString = "", strHostName = string.Empty, strServiceURL = string.Empty;
                    string strReportFileName = string.Empty, strStoredProc = string.Empty, strReportParams = string.Empty,
                    strReportStaticParams = string.Empty, strPrintFormat = string.Empty, strRequestXML = string.Empty;
                    try
                    {

                        strHostName = HttpContext.Current.Request.Url.Host;

                        if (strHostName == "apdlynxservices.pgw.local")
                            strServiceURL = "http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc";
                        else if (strHostName == "sapd.lynxservices.com")
                            strServiceURL = "http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc";
                        else if (strHostName == "apd.lynxservices.com")
                            strServiceURL = "http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc";
                        else
                            strServiceURL = "http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc";


                        if (Request.Cookies["ReportFileName"] != null)
                            strReportFileName = Request.Cookies["ReportFileName"].Value;

                        if (Request.Cookies["StoredProc"] != null)
                            strStoredProc = Request.Cookies["StoredProc"].Value;

                        if (Request.Cookies["ReportParams"] != null)
                            strReportParams = Request.Cookies["ReportParams"].Value;

                        if (Request.Cookies["ReportStaticParams"] != null)
                            strReportStaticParams = Request.Cookies["ReportStaticParams"].Value;

                        if (Request.Cookies["ReportFormat"] != null)
                            strReportFormat = Request.Cookies["ReportFormat"].Value;

                        if (Request.Cookies["PrintFormat"] != null)
                            strPrintFormat = Request.Cookies["PrintFormat"].Value;


                        strRequestXML = string.Concat("<Report", " sReportFileName='", Server.UrlDecode(strReportFileName), "'", " sStoredProc='", Server.UrlDecode(strStoredProc), "'", " sReportParams='", Server.UrlDecode(strReportParams), "'", " sReportStaticParams='", Server.UrlDecode(strReportStaticParams), "'", " sReportFormat='", Server.UrlDecode(strReportFormat), "'", " />");

                        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(strServiceURL);
                        string xmlParameters = "<?xml version=\"1.0\" encoding=\"utf-8\"?><s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Header></s:Header><s:Body><GetReportDocument xmlns=\"http://LynxAPDComponentServices\"><strParamXML>" + "<![CDATA[" + strRequestXML + "]]>" + "</strParamXML></GetReportDocument></s:Body></s:Envelope>";
                        XmlDocument xmlDocument = new XmlDocument();
                        xmlDocument.LoadXml(xmlParameters);
                        webRequest.Method = "POST";
                        webRequest.ContentType = "text/xml;charset=utf-8";
                        webRequest.KeepAlive = false;
                        webRequest.ProtocolVersion = HttpVersion.Version11;
                        webRequest.Headers.Add("SOAPAction", "http://LynxAPDComponentServices/IEcadAccessor/GetReportDocument");
                        Stream responseStream = webRequest.GetRequestStream();
                        xmlDocument.Save(responseStream);
                        responseStream.Close();
                        WebResponse response = webRequest.GetResponse();
                        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                        {
                            responseString = Convert.ToString(streamReader.ReadToEnd());
                        }
                        xmlDocument.LoadXml(responseString);
                        strReportData = ((xmlDocument).DocumentElement).InnerText;

                        Base64 = Convert.FromBase64String(strReportData);
                        Response.ContentType = Server.UrlDecode(strPrintFormat);
                        Response.BinaryWrite(Base64);


                    }
                    catch (Exception ex)
                    {
                        responseString = ex.Message.ToString();
                    }
                    finally
                    {
                        Request.Cookies["ReportFileName"].Expires = DateTime.Now;
                        Request.Cookies["StoredProc"].Expires = DateTime.Now;
                        Request.Cookies["ReportParams"].Expires = DateTime.Now;
                        Request.Cookies["ReportStaticParams"].Expires = DateTime.Now;
                        Request.Cookies["ReportFormat"].Expires = DateTime.Now;
                        Request.Cookies["PrintFormat"].Expires = DateTime.Now;
                    }
                }
            }
            catch
            {
                //Response.Write("No Session...");
                Response.Redirect("frmLogout.asp?NL=1");
            }

        }
    }
}