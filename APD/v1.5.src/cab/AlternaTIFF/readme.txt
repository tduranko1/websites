Making CAB files for deployment of OCX over the web

There are several utilities to make a CAB file. Visual Studio has an inbuilt project to do this. When making CAB project with Visual Studio, a .osd file is automatically added to the output. When deployed over the web using <OBJECT> tag, IE looks into this .osd file for information on what to install. OSD definition is very restrictive for licensed activeX controls. The other option is to use .inf file. Though .inf files are used to deploy device drivers for hardware, we can use their format to deploy anything (I guess). To package this .inf file into the CAB file, we must use an utility that is straight forward. I have included an utility Chippy.exe which works fine in our case. You are free to use any other utility.

Procedure to make a CAB file using Chippy.exe
1. Create a new CAB file "alttiff.cab"
2. Add the following files to it...
    a. alttiff.ocx
    b. license.dat
    c. AlternaTIF.inf
3. Make the CAB file and deploy (copy) to "\install" folder on the web site.

On the client machine, the security setting has to be the following
1. Download signed ActiveX controls - Enable
2. Run ActiveX controls and plug-ins - Enable
3. Script ActiveX controls marked safe for scripting - Enable


Also, for the TIF viewer to load the document (which is a network path in APD), change the following setting

1. Access data sources across domains - Enable


This procedure should silently install the ocx when not found on the client's machine.
