// XslCache.cpp : Implementation of CXslCache
#include "stdafx.h"
#include "BaseUtilities.h"
#include "XslCache.h"

/////////////////////////////////////////////////////////////////////////////
// Destructor
/////////////////////////////////////////////////////////////////////////////
CXslCache::~CXslCache()
{
}

/////////////////////////////////////////////////////////////////////////////
// interface ISupportsErrorInfo

STDMETHODIMP CXslCache::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IXslCache
	};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (IsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// interface IObjectControl

/////////////////////////////////////////////////////////////////////////////
// COM+ tells us when we have been activated. We need to setup our
//		 object for this context
/////////////////////////////////////////////////////////////////////////////
HRESULT CXslCache::Activate()
{
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////
// COM+ tells us when we are being deactivated. Remove any context state
//		 we have. We don't close the file here since it's state is maintained
//		 througout all contexts
/////////////////////////////////////////////////////////////////////////////
void CXslCache::Deactivate()
{
}

/////////////////////////////////////////////////////////////////////////////
// COM+ asks us if our object can be pooled
/////////////////////////////////////////////////////////////////////////////
BOOL CXslCache::CanBePooled()
{
	return m_bCanBePooled; // we can be pooled
}

/////////////////////////////////////////////////////////////////////////////
// interface IXslCache

/////////////////////////////////////////////////////////////////////////////
// Returns any thrown error description.
/////////////////////////////////////////////////////////////////////////////
STDMETHODIMP CXslCache::GetErrorDescription( BSTR *pstrDescription )
{
	*pstrDescription = m_strErrorDescription.copy();
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////
// Returns a comma delimited string of CRUD params for the style sheet.
/////////////////////////////////////////////////////////////////////////////
STDMETHODIMP CXslCache::GetCrudList( BSTR strFilePath, BSTR *pstrCrud )
{
	Method( L"CXslCache::GetCrudList()" );

	try
	{
		*pstrCrud = m_arXsl.GetFile( strFilePath ).m_strCrud.copy();
	}	  

	// Standard error handling.
	catch ( _com_error e ) { return HandleError( e.Description() ); }
	catch ( CBstr str ) { return HandleError( str ); }
	catch ( ... ) { return HandleError(); }
	return Success();
}

/////////////////////////////////////////////////////////////////////////////
// Returns a comma delimited string of non-CRUD params for the style sheet
/////////////////////////////////////////////////////////////////////////////
STDMETHODIMP CXslCache::GetParamList( BSTR strFilePath, BSTR *pstrParams )
{
	Method( L"CXslCache::GetParamList()" );

	try
	{
		*pstrParams = m_arXsl.GetFile( strFilePath ).m_strParams.copy();
	}	  

	// Standard error handling.
	catch ( _com_error e ) { return HandleError( e.Description() ); }
	catch ( CBstr str ) { return HandleError( str ); }
	catch ( ... ) { return HandleError(); }
	return Success();
}

/////////////////////////////////////////////////////////////////////////////
// Returns a new IXSLProcessor object for the style sheet template.
/////////////////////////////////////////////////////////////////////////////
STDMETHODIMP CXslCache::CreateProcessor( BSTR strFilePath, IDispatch **ppXslProcessor )
{
	Method( L"CXslCache::CreateProcessor()" );

	try
	{
		IDispatchPtr pDispatch = 
			m_arXsl.GetFile( strFilePath ).m_pTemplate->createProcessor();

		*ppXslProcessor = pDispatch.Detach();
	}	  

	// Standard error handling.
	catch ( _com_error e ) { return HandleError( e.Description() ); }
	catch ( CBstr str ) { return HandleError( str ); }
	catch ( ... ) { return HandleError(); }
	return Success();
}

/////////////////////////////////////////////////////////////////////////////
// Returns true if the file has been cached.
/////////////////////////////////////////////////////////////////////////////
STDMETHODIMP CXslCache::GetIsCached( BSTR strFilePath, int* pbIsCached )
{
	Method( L"CXslCache::GetIsCached()" );

	try
	{
		*pbIsCached = (int) m_arXsl.IsInList( strFilePath );
	}	  

	// Standard error handling.
	catch ( _com_error e ) { return HandleError( e.Description() ); }
	catch ( CBstr str ) { return HandleError( str ); }
	catch ( ... ) { return HandleError(); }
	return Success();
}

/////////////////////////////////////////////////////////////////////////////
// Returns true if the style sheet requires a session object.
/////////////////////////////////////////////////////////////////////////////
STDMETHODIMP CXslCache::GetNeedsSession( BSTR strFilePath, int *pbNeedsSession )
{
	Method( L"CXslCache::GetIsCached()" );

	try
	{
		if ( m_arXsl.GetFile( strFilePath ).m_bNeedsSession )
			*pbNeedsSession = 1;
		else
			*pbNeedsSession = 0;
	}	  

	// Standard error handling.
	catch ( _com_error e ) { return HandleError( e.Description() ); }
	catch ( CBstr str ) { return HandleError( str ); }
	catch ( ... ) { return HandleError(); }
	return Success();
}
