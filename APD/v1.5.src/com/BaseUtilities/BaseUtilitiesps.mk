
BaseUtilitiesps.dll: dlldata.obj BaseUtilities_p.obj BaseUtilities_i.obj
	link /dll /out:BaseUtilitiesps.dll /def:BaseUtilitiesps.def /entry:DllMain dlldata.obj BaseUtilities_p.obj BaseUtilities_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del BaseUtilitiesps.dll
	@del BaseUtilitiesps.lib
	@del BaseUtilitiesps.exp
	@del dlldata.obj
	@del BaseUtilities_p.obj
	@del BaseUtilities_i.obj
