/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Thu Mar 04 15:28:49 2004
 */
/* Compiler settings for C:\websites\apd\v1.3.src\com\BaseUtilities\BaseUtilities.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __BaseUtilities_h__
#define __BaseUtilities_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __IXmlCache_FWD_DEFINED__
#define __IXmlCache_FWD_DEFINED__
typedef interface IXmlCache IXmlCache;
#endif 	/* __IXmlCache_FWD_DEFINED__ */


#ifndef __IXslCache_FWD_DEFINED__
#define __IXslCache_FWD_DEFINED__
typedef interface IXslCache IXslCache;
#endif 	/* __IXslCache_FWD_DEFINED__ */


#ifndef __XmlCache_FWD_DEFINED__
#define __XmlCache_FWD_DEFINED__

#ifdef __cplusplus
typedef class XmlCache XmlCache;
#else
typedef struct XmlCache XmlCache;
#endif /* __cplusplus */

#endif 	/* __XmlCache_FWD_DEFINED__ */


#ifndef __XslCache_FWD_DEFINED__
#define __XslCache_FWD_DEFINED__

#ifdef __cplusplus
typedef class XslCache XslCache;
#else
typedef struct XslCache XslCache;
#endif /* __cplusplus */

#endif 	/* __XslCache_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 


#ifndef __BaseUtilitiesLib_LIBRARY_DEFINED__
#define __BaseUtilitiesLib_LIBRARY_DEFINED__

/* library BaseUtilitiesLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_BaseUtilitiesLib;

#ifndef __IXmlCache_INTERFACE_DEFINED__
#define __IXmlCache_INTERFACE_DEFINED__

/* interface IXmlCache */
/* [unique][oleautomation][nonextensible][dual][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IXmlCache;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("43109C47-2438-47BA-8E0B-1B243FD1986F")
    IXmlCache : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetErrorDescription( 
            /* [retval][out] */ BSTR __RPC_FAR *pstrDescription) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetDom( 
            /* [in] */ BSTR strFilePath,
            /* [retval][out] */ IDispatch __RPC_FAR *__RPC_FAR *ppDOMDocument) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetIsCached( 
            /* [in] */ BSTR strFilePath,
            /* [retval][out] */ int __RPC_FAR *pbIsCached) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IXmlCacheVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IXmlCache __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IXmlCache __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IXmlCache __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IXmlCache __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IXmlCache __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IXmlCache __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IXmlCache __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetErrorDescription )( 
            IXmlCache __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pstrDescription);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetDom )( 
            IXmlCache __RPC_FAR * This,
            /* [in] */ BSTR strFilePath,
            /* [retval][out] */ IDispatch __RPC_FAR *__RPC_FAR *ppDOMDocument);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIsCached )( 
            IXmlCache __RPC_FAR * This,
            /* [in] */ BSTR strFilePath,
            /* [retval][out] */ int __RPC_FAR *pbIsCached);
        
        END_INTERFACE
    } IXmlCacheVtbl;

    interface IXmlCache
    {
        CONST_VTBL struct IXmlCacheVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IXmlCache_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IXmlCache_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IXmlCache_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IXmlCache_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IXmlCache_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IXmlCache_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IXmlCache_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IXmlCache_GetErrorDescription(This,pstrDescription)	\
    (This)->lpVtbl -> GetErrorDescription(This,pstrDescription)

#define IXmlCache_GetDom(This,strFilePath,ppDOMDocument)	\
    (This)->lpVtbl -> GetDom(This,strFilePath,ppDOMDocument)

#define IXmlCache_GetIsCached(This,strFilePath,pbIsCached)	\
    (This)->lpVtbl -> GetIsCached(This,strFilePath,pbIsCached)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXmlCache_GetErrorDescription_Proxy( 
    IXmlCache __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pstrDescription);


void __RPC_STUB IXmlCache_GetErrorDescription_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXmlCache_GetDom_Proxy( 
    IXmlCache __RPC_FAR * This,
    /* [in] */ BSTR strFilePath,
    /* [retval][out] */ IDispatch __RPC_FAR *__RPC_FAR *ppDOMDocument);


void __RPC_STUB IXmlCache_GetDom_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXmlCache_GetIsCached_Proxy( 
    IXmlCache __RPC_FAR * This,
    /* [in] */ BSTR strFilePath,
    /* [retval][out] */ int __RPC_FAR *pbIsCached);


void __RPC_STUB IXmlCache_GetIsCached_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IXmlCache_INTERFACE_DEFINED__ */


#ifndef __IXslCache_INTERFACE_DEFINED__
#define __IXslCache_INTERFACE_DEFINED__

/* interface IXslCache */
/* [unique][oleautomation][nonextensible][dual][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IXslCache;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("5EECFA34-5332-4121-B6CD-F60963302EED")
    IXslCache : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetErrorDescription( 
            /* [retval][out] */ BSTR __RPC_FAR *pstrDescription) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCrudList( 
            /* [in] */ BSTR strFilePath,
            /* [retval][out] */ BSTR __RPC_FAR *pstrCrud) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetParamList( 
            /* [in] */ BSTR strFilePath,
            /* [retval][out] */ BSTR __RPC_FAR *pstrParams) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CreateProcessor( 
            /* [in] */ BSTR strFilePath,
            /* [retval][out] */ IDispatch __RPC_FAR *__RPC_FAR *ppXslProcessor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetIsCached( 
            /* [in] */ BSTR strFilePath,
            /* [retval][out] */ int __RPC_FAR *pbIsCached) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNeedsSession( 
            /* [in] */ BSTR strFilePath,
            /* [retval][out] */ int __RPC_FAR *pbNeedsSession) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IXslCacheVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IXslCache __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IXslCache __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IXslCache __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IXslCache __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IXslCache __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IXslCache __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IXslCache __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetErrorDescription )( 
            IXslCache __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pstrDescription);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCrudList )( 
            IXslCache __RPC_FAR * This,
            /* [in] */ BSTR strFilePath,
            /* [retval][out] */ BSTR __RPC_FAR *pstrCrud);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetParamList )( 
            IXslCache __RPC_FAR * This,
            /* [in] */ BSTR strFilePath,
            /* [retval][out] */ BSTR __RPC_FAR *pstrParams);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CreateProcessor )( 
            IXslCache __RPC_FAR * This,
            /* [in] */ BSTR strFilePath,
            /* [retval][out] */ IDispatch __RPC_FAR *__RPC_FAR *ppXslProcessor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIsCached )( 
            IXslCache __RPC_FAR * This,
            /* [in] */ BSTR strFilePath,
            /* [retval][out] */ int __RPC_FAR *pbIsCached);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNeedsSession )( 
            IXslCache __RPC_FAR * This,
            /* [in] */ BSTR strFilePath,
            /* [retval][out] */ int __RPC_FAR *pbNeedsSession);
        
        END_INTERFACE
    } IXslCacheVtbl;

    interface IXslCache
    {
        CONST_VTBL struct IXslCacheVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IXslCache_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IXslCache_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IXslCache_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IXslCache_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IXslCache_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IXslCache_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IXslCache_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IXslCache_GetErrorDescription(This,pstrDescription)	\
    (This)->lpVtbl -> GetErrorDescription(This,pstrDescription)

#define IXslCache_GetCrudList(This,strFilePath,pstrCrud)	\
    (This)->lpVtbl -> GetCrudList(This,strFilePath,pstrCrud)

#define IXslCache_GetParamList(This,strFilePath,pstrParams)	\
    (This)->lpVtbl -> GetParamList(This,strFilePath,pstrParams)

#define IXslCache_CreateProcessor(This,strFilePath,ppXslProcessor)	\
    (This)->lpVtbl -> CreateProcessor(This,strFilePath,ppXslProcessor)

#define IXslCache_GetIsCached(This,strFilePath,pbIsCached)	\
    (This)->lpVtbl -> GetIsCached(This,strFilePath,pbIsCached)

#define IXslCache_GetNeedsSession(This,strFilePath,pbNeedsSession)	\
    (This)->lpVtbl -> GetNeedsSession(This,strFilePath,pbNeedsSession)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXslCache_GetErrorDescription_Proxy( 
    IXslCache __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pstrDescription);


void __RPC_STUB IXslCache_GetErrorDescription_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXslCache_GetCrudList_Proxy( 
    IXslCache __RPC_FAR * This,
    /* [in] */ BSTR strFilePath,
    /* [retval][out] */ BSTR __RPC_FAR *pstrCrud);


void __RPC_STUB IXslCache_GetCrudList_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXslCache_GetParamList_Proxy( 
    IXslCache __RPC_FAR * This,
    /* [in] */ BSTR strFilePath,
    /* [retval][out] */ BSTR __RPC_FAR *pstrParams);


void __RPC_STUB IXslCache_GetParamList_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXslCache_CreateProcessor_Proxy( 
    IXslCache __RPC_FAR * This,
    /* [in] */ BSTR strFilePath,
    /* [retval][out] */ IDispatch __RPC_FAR *__RPC_FAR *ppXslProcessor);


void __RPC_STUB IXslCache_CreateProcessor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXslCache_GetIsCached_Proxy( 
    IXslCache __RPC_FAR * This,
    /* [in] */ BSTR strFilePath,
    /* [retval][out] */ int __RPC_FAR *pbIsCached);


void __RPC_STUB IXslCache_GetIsCached_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IXslCache_GetNeedsSession_Proxy( 
    IXslCache __RPC_FAR * This,
    /* [in] */ BSTR strFilePath,
    /* [retval][out] */ int __RPC_FAR *pbNeedsSession);


void __RPC_STUB IXslCache_GetNeedsSession_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IXslCache_INTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_XmlCache;

#ifdef __cplusplus

class DECLSPEC_UUID("0EFDC160-39B1-4FE3-B6C3-24548D757D20")
XmlCache;
#endif

EXTERN_C const CLSID CLSID_XslCache;

#ifdef __cplusplus

class DECLSPEC_UUID("CA629D13-91EA-479E-82B6-4B635AD28A88")
XslCache;
#endif
#endif /* __BaseUtilitiesLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
