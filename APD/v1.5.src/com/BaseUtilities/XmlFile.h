/////////////////////////////////////////////////////////////////////////////
//
// XmlFile.h: interface for the CXmlFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(XML_FILE_H)
#define XML_FILE_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FileBase.h"

//////////////////////////////////////////////////////////////////////
// Wrapper class for a single cached XML file.
//////////////////////////////////////////////////////////////////////
class CXmlFile : public CFileBase
{
public:

	// Constructors
	CXmlFile();
	CXmlFile( CBstr strFilePath );
	CXmlFile( const CXmlFile &rFrom );

	// Destructor
	virtual ~CXmlFile();

	// operator=
	const CXmlFile& operator=( const CXmlFile& oSrc );

private:
	
	// Utility function used to centralize some common code.
	virtual void InternalLoadFile();

public:

	// An XML DOM document of the file contents.
	MSXML2::IXMLDOMDocumentPtr m_pDom;

};

// A vector of CXmlFiles
typedef CFileList<CXmlFile> CXmlFileList;

// An iterator of CXmlFiles
typedef CXmlFileList::iterator CXmlFileIterator;

#endif // !defined(XML_FILE_H)
