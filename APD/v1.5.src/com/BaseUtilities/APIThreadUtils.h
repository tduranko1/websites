//////////////////////////////////////////////////////////////////////
// APDThreadUtils.h
//
// Multithreading utility classes that use the C/Win32 API
//
// Note that you must use the multithreaded run-time libs for the
// CThread class to compile.
//////////////////////////////////////////////////////////////////////
#ifndef THREAD_UTIL_H
#define THREAD_UTIL_H

//#include "SystemUtils.h"
#include <windows.h>
#include <process.h>

#define Assert( exp )

//////////////////////////////////////////////////////////////////////
// Thread Utils Namespace
//////////////////////////////////////////////////////////////////////
namespace APIThreadUtils
{
	//////////////////////////////////////////////////////////////////////
	// CSynchBase
	//
	// Base class for a set of synchronization classes
	//////////////////////////////////////////////////////////////////////
	class CSynchBase
	{
	public:

		CSynchBase()
		{
			m_bLocked = FALSE;
		}

		virtual ~CSynchBase()
		{
		}

		BOOL SynchLock( DWORD dwTimeout = 250 )
		{
			m_bLocked = DoSynchLock( dwTimeout );
			return m_bLocked;
		}

		virtual void SynchUnlock()
		{
			DoSynchUnlock();
			m_bLocked = FALSE;
		}

		virtual BOOL IsSynchLocked()
		{
			return m_bLocked;
		}

	protected:

		virtual BOOL DoSynchLock( DWORD dwTimeout ) = 0;
		virtual void DoSynchUnlock() = 0;

	private:
		BOOL m_bLocked;
	};

	//////////////////////////////////////////////////////////////////////
	// CTransient
	//
	// Wrapper that locks in constructor and unlocks in destructor. With
	// this class you do not have to think about unlocking. Just create
	// it on the stack (not with new).
	//////////////////////////////////////////////////////////////////////
	class CTransient
	{
	private:

		CTransient()
		{
		}

	public:

		CTransient( CSynchBase *pSynchro, DWORD dwTimeout = 250 )
		{
			Assert( pSynchro != NULL );
			m_pSynchro = pSynchro;
			m_pSynchro->SynchLock( dwTimeout );
		}

		virtual ~CTransient()
		{
			m_pSynchro->SynchUnlock();
		}

		BOOL IsLocked(void)
		{
			return m_pSynchro->IsSynchLocked();
		}

	private:

		CSynchBase *m_pSynchro;

	};

	//////////////////////////////////////////////////////////////////////
	// class CEvent
	//
	// Simple event class.
	//////////////////////////////////////////////////////////////////////
	class CEvent
	{
	public:

		// CEvent Constructor.
		//
		// bInitialState - Specifies the initial state of the event object.
		//	If TRUE, the initial state is signaled; otherwise, it is nonsignaled.
		//
		// bManualReset - Specifies whether a manual-reset or auto-reset
		//	event object is created. If TRUE, then you must use the
		//	ResetEvent function to manually reset the state to nonsignaled.
		//	If FALSE, the system automatically resets the state to
		//	nonsignaled after a single waiting thread has been released.
		//
		// szName - specifies the name of the event object. The name is
		//	limited to MAX_PATH characters. Name comparison is case sensitive.
		//
		CEvent( BOOL bInitialState = FALSE, BOOL bManualReset = FALSE, LPCTSTR szName = NULL )
		{
			m_hEvent = CreateEvent( NULL, bManualReset, bInitialState, szName );
		}

		// CEvent Destructor.
		virtual ~CEvent()
		{
			::CloseHandle( m_hEvent );
		}

		// Signals the event.
		void Signal()
		{
			SetEvent( m_hEvent );
		}

		// Sets the event to non-signalled.
		void Reset()
		{
			ResetEvent( m_hEvent );
		}

		// Waits dwMilliSeconds until the event is signalled.
		void WaitForSignal( DWORD dwMilliSeconds = INFINITE )
		{
			WaitForSingleObject( m_hEvent, dwMilliSeconds );
		}

		// Returns true if the state has been signalled.
		BOOL IsSignaled()
		{
			return ( WAIT_OBJECT_0 == WaitForSingleObject( m_hEvent, 0 ) );
		}

	private:

		HANDLE m_hEvent;

	};

	//////////////////////////////////////////////////////////////////////
	// CMutex
	//
	// Can be owned by only one thread at a time, enabling threads to
	// coordinate mutually exclusive access to a shared resource.
	//////////////////////////////////////////////////////////////////////
	class CMutex : public CSynchBase
	{
	public:

		// CMutex Constructor.
		//
		// bIsOwned - Specifies the initial owner of the mutex object.
		//	If this value is TRUE and the caller created the mutex,
		//	the calling thread obtains ownership of the mutex object.
		//	Otherwise, the calling thread does not obtain ownership.
		//
		// szName - specifies the name of the event object. The name is
		//	limited to MAX_PATH characters. Name comparison is case sensitive.
		//
		// lpSecAttr - determines whether the returned handle can be
		//	inherited by child processes. If lpMutexAttributes is NULL,
		//	the handle cannot be inherited.
		//
		CMutex( BOOL bIsOwned = FALSE, LPCTSTR szName = NULL, LPSECURITY_ATTRIBUTES lpSecAttr = NULL )
		{
			m_hMutex = ::CreateMutex( lpSecAttr, bIsOwned, szName );
		}

		// CMutex Destructor.
		virtual ~CMutex()
		{
			::CloseHandle( m_hMutex );
		}

	private:

		virtual BOOL DoSynchLock( DWORD dwTimeout )
		{
			DWORD dwCode = ::WaitForSingleObject( m_hMutex, dwTimeout );
			return ( dwCode == WAIT_OBJECT_0 ) ? TRUE : FALSE;
		}

		virtual void DoSynchUnlock(void)
		{
			::ReleaseMutex( m_hMutex );
		}

	private:

		HANDLE m_hMutex;

	};

	//////////////////////////////////////////////////////////////////////
	// class CSemaphore
	//
	// Maintains a count between zero and some maximum value, limiting
	// the number of threads that are simultaneously accessing a shared
	// resource.
	//////////////////////////////////////////////////////////////////////
	class CSemaphore
	{
	public:

		// CSemaphore Constructor.
		//
		// lInitCount - Specifies an initial count for the semaphore object.
		//	This value must be greater than or equal to zero and less than
		//	or equal to lMaximumCount. The state of a semaphore is signaled
		//	when its count is greater than zero and nonsignaled when it is
		//	zero. The count is decreased by one whenever a wait function
		//	releases a thread that was waiting for the semaphore. The count
		//	is increased by a specified amount by calling Release().
		//
		// lMaxCount - Specifies the maximum count for the semaphore object.
		//	This value must be greater than zero.
		//
		// szName - specifies the name of the event object. The name is
		//	limited to MAX_PATH characters. Name comparison is case sensitive.
		//
		CSemaphore( long lInitCount = 0, long lMaxCount = 0x7FFFFFFF, LPCTSTR szName = NULL )
		{
			m_hSemaphore = CreateSemaphore ( NULL, lInitCount, lMaxCount, szName );

			if ( m_hSemaphore == NULL )
			{
				// Semaphore already created - try to open it.
				if ( GetLastError() == ERROR_ALREADY_EXISTS )
				{
					m_hSemaphore = OpenSemaphore( SEMAPHORE_ALL_ACCESS | SYNCHRONIZE, FALSE, szName );
				}
			}
		}

		// CSemaphore Destructor.
		virtual ~CSemaphore()
		{
			CloseHandle( m_hSemaphore );
		}

		// Releases the semaphore by incrementing the semaphore count.
		// Returns FALSE if too many posts have occurred.
		bool Release()
		{
			return ( ERROR_TOO_MANY_POSTS != ReleaseSemaphore( m_hSemaphore, 1, NULL ) );
		}

		// Acquires the semaphore by attempting to decrement the count.  If
		// the semaphore count is zero, the current thread blocks until another
		// thread increments the count.
		void Acquire()
		{
			WaitForSingleObject( m_hSemaphore, INFINITE );
		}

		// Acquires the semaphore by attempting to decrement the count.  If
		// the semaphore count is zero, the current thread blocks until another
		// thread increments the count or the specified amount of time expires.
		// If the time has expired, the method returns FALSE.  Otherwise, the
		// count is decremented and the thread is allowed to proceed.
		bool Acquire( DWORD dwMilliseconds = 0 )
		{
			return ( WAIT_TIMEOUT != WaitForSingleObject( m_hSemaphore, dwMilliseconds ) );
		}

	private:

		HANDLE m_hSemaphore;

	};


	//////////////////////////////////////////////////////////////////////
	// CCriticalSection
	//
	// The threads of a single process can use a critical section
	// object for mutual-exclusion synchronization. There is no
	// guarantee about the order in which threads will obtain ownership
	// of the critical section, however, the system will be fair to all
	// threads.
	//////////////////////////////////////////////////////////////////////
	class CCriticalSection : public CSynchBase
	{

	public:

		// CCriticalSection Constructor
		//
		// bBlock - Set to TRUE if you want this thread to wait until
		// it can take ownership of the critical section.
		//
		CCriticalSection( BOOL bBlock = TRUE )
		{
			m_bBlock = bBlock;
			::InitializeCriticalSection( &m_Section );
		}

		// CCriticalSection Destructor
		virtual ~CCriticalSection()
		{
			::DeleteCriticalSection( &m_Section );
		}

		virtual BOOL DoSynchLock( DWORD dwTimeout = 250 )
		{
			UNREFERENCED_PARAMETER( dwTimeout );

			if ( m_bBlock )
			{
				::EnterCriticalSection( &m_Section );
				return TRUE;
			}
			else
			{
				#if (_WIN32_WINNT >= 0x0400)
					return ::TryEnterCriticalSection( &m_Section );
				#endif
			}
			return FALSE;
		}

		virtual void DoSynchUnlock(void)
		{
			::LeaveCriticalSection( &m_Section );
		}

	private:

		BOOL m_bBlock;
		CRITICAL_SECTION m_Section;

	};

	//////////////////////////////////////////////////////////////////////
	// CCriticalSectionLock
	//
	// Locks the critical section upon construction, and unlocks it
	// upon destruction.
	//////////////////////////////////////////////////////////////////////
	class CCriticalSectionLock
	{

	public:

		CCriticalSectionLock( CCriticalSection &rCrit ) :
			m_rCrit( rCrit )
		{
			m_rCrit.DoSynchLock();
		}

		virtual ~CCriticalSectionLock()
		{
			m_rCrit.DoSynchUnlock();
		}

	private:

		CCriticalSection &m_rCrit;

	};

	//////////////////////////////////////////////////////////////////////
	// CThread
	//
	// Threading wrapper class.  Note that the pData variable can be
	// set in either the constructor or the BeginThread function.
	//////////////////////////////////////////////////////////////////////
	class CThread : protected CMutex, protected CEvent
	{
	public:

		CThread( void* pData = NULL ) : CEvent( TRUE, TRUE ), m_pData( pData )
		{
		}

		// Starts the thread.  Returns TRUE if successful.
		BOOL BeginThread( void* pData = NULL )
		{
			CTransient Guard( this );

			WaitForSignal();
			Reset();

			// Use passed pointer if non-NULL.
			if ( pData != NULL )
			{
				m_pData = pData;
			}

			// Start the thread.
			return ( -1 != _beginthread( &HelperThreadFunction, 0, (void*) this ) );
		}

		// Returns TRUE if the thread is running.
		BOOL IsRunning()
		{
			return !IsSignaled();
		}

		// Waits until the thread has ended.
		void WaitUntilFinished()
		{
			WaitForSignal();
		}

		DWORD GetThreadId()
		{
			return m_dwThreadId;
		}

	protected:

		// Entry point for worker thread.
		virtual void ThreadFunction( void* pData ) = 0;

	private:

		static void __cdecl HelperThreadFunction( void* pThis )
		{
			CThread* pThread = (CThread*) pThis;

			// Use a local data pointer so that ThreadFunction
			// doesn't have to worry about locking.
			pThread->SynchLock();
			void* pData = pThread->m_pData;
			pThread->m_dwThreadId = GetCurrentThreadId();
			pThread->SynchUnlock();

			// Call the derived thread function.
			pThread->ThreadFunction( pData );

			// Clean up and end thread.
			CTransient Guard( pThread );
			pThread->Signal();
			_endthread();
		}

	private:

		void* m_pData;
		DWORD m_dwThreadId;

	};

};	// APIThreadUtils

#endif
