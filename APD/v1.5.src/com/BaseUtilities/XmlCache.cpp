/////////////////////////////////////////////////////////////////////////////
//
// XmlCache.cpp : Implementation of CXmlCache
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "BaseUtilities.h"
#include "XmlCache.h"

/////////////////////////////////////////////////////////////////////////////
// Destructor
/////////////////////////////////////////////////////////////////////////////
CXmlCache::~CXmlCache()
{
}

/////////////////////////////////////////////////////////////////////////////
// interface ISupportsErrorInfo

STDMETHODIMP CXmlCache::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IXmlCache
	};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (IsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// interface IObjectControl

/////////////////////////////////////////////////////////////////////////////
// COM+ tells us when we have been activated. We need to setup our
//		 object for this context
/////////////////////////////////////////////////////////////////////////////
HRESULT CXmlCache::Activate()
{
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////
// COM+ tells us when we are being deactivated. Remove any context state
//		 we have. We don't close the file here since it's state is maintained
//		 througout all contexts
/////////////////////////////////////////////////////////////////////////////
void CXmlCache::Deactivate()
{
}

/////////////////////////////////////////////////////////////////////////////
// COM+ asks us if our object can be pooled
/////////////////////////////////////////////////////////////////////////////
BOOL CXmlCache::CanBePooled()
{
	return m_bCanBePooled; // we can be pooled
}

/////////////////////////////////////////////////////////////////////////////
// interface IXmlCache

/////////////////////////////////////////////////////////////////////////////
// Returns any thrown error description.
/////////////////////////////////////////////////////////////////////////////
STDMETHODIMP CXmlCache::GetErrorDescription(BSTR *pstrDescription)
{
	*pstrDescription = m_strErrorDescription.copy();
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////
// Returns a reference to a cached DOM object.
/////////////////////////////////////////////////////////////////////////////
STDMETHODIMP CXmlCache::GetDom(BSTR strFilePath, IDispatch **ppDOMDocument)
{
	Method( L"CXslCache::GetDom()" );

	try
	{
		IDispatchPtr pDispatch = 
			m_arXml.GetFile( strFilePath ).m_pDom;

		*ppDOMDocument = pDispatch.Detach();
	}	  

	// Standard error handling.
	catch ( _com_error e ) { return HandleError( e.Description() ); }
	catch ( CBstr str ) { return HandleError( str ); }
	catch ( ... ) { return HandleError(); }
	return Success();
}

/////////////////////////////////////////////////////////////////////////////
// Returns true if the file has been cached.
/////////////////////////////////////////////////////////////////////////////
STDMETHODIMP CXmlCache::GetIsCached( BSTR strFilePath, int* pbIsCached )
{
	Method( L"CXslCache::GetIsCached()" );

	try
	{
		*pbIsCached = (int) m_arXml.IsInList( strFilePath );
	}	  

	// Standard error handling.
	catch ( _com_error e ) { return HandleError( e.Description() ); }
	catch ( CBstr str ) { return HandleError( str ); }
	catch ( ... ) { return HandleError(); }
	return Success();
}
