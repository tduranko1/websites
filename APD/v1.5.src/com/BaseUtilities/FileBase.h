/////////////////////////////////////////////////////////////////////////////
//
// FileBase.h: Interface for CFileBase class
//
//////////////////////////////////////////////////////////////////////
#if !defined(FILE_BASE_H)
#define FILE_BASE_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>
#include "CBstrImpl.h"

//////////////////////////////////////////////////////////////////////
// Base class for file storage classes.
//////////////////////////////////////////////////////////////////////
class CFileBase  
{
public:

	// Constructors
	CFileBase();
	CFileBase( const CBstr& strFilePath );
	CFileBase( const CFileBase &rFrom );

	// operator=
	const CFileBase& operator=( const CFileBase& oSrc );

	// Destructor
	virtual ~CFileBase();

	// Loads the XML file.
	void LoadFile();

	// Checks the file date and reloads the xml if the date has changed.
	// Returns true if a reload was required.
	bool LoadFileChanges();

	// Returns true if this object has been initialized with a non-blank file path.
	bool HasFile() const;

	// Compares the passed path to the internal file path.
	bool ComparePath( const CBstr& strFilePath ) const;

protected:
	
	// Fills a filetime struct for the member file.
	void GetDate( FILETIME *pTime );

	// Utility function used to centralize some common code.
	virtual void InternalLoadFile() = 0;

protected:

	// The XML file path and name.
	CBstr m_strFilePath;

	// The last read file date.
	FILETIME m_stFileDate;

};

//////////////////////////////////////////////////////////////////////
// Templated wrapper list class for CFileBase
//////////////////////////////////////////////////////////////////////
template < class FILESTORE >
class CFileList : public std::vector< FILESTORE >
{
public:

	// Constructor
	CFileList() {}

	// Destructor
	virtual ~CFileList() { }

	// Returns a FILESTORE object for the passed file path.
	// Will go get the file and add it to the list if it doesn't exist.
	FILESTORE& GetFile( CBstr strFilePath )
	{
		FILESTORE& objFile = FindFile( strFilePath );

		if ( !objFile.HasFile() )
		{
			push_back( FILESTORE( strFilePath ) );
			return back();
		}
		else
		{
			objFile.LoadFileChanges();
			return objFile;
		}
	}

	// Returns true if the passed file path is in the vector.
	bool IsInList( CBstr strFilePath )
	{
		FILESTORE& objFile = FindFile( strFilePath );
		return objFile.HasFile();
	}

private:

	// Searches for the passed file in the list.
	// Returns NULL if not found.
	FILESTORE& FindFile( CBstr& strFilePath )
	{
		std::vector< FILESTORE >::iterator iT = begin();
		for ( ; iT != end(); iT++ )
		{
			if ( iT->ComparePath( strFilePath ) )
				return *iT;
		}
		return m_Blank;
	}

	FILESTORE m_Blank;

};

#endif // !defined(FILE_BASE_H)
