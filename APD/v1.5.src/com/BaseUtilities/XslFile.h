/////////////////////////////////////////////////////////////////////////////
//
// XslFile.h: interface for the CXslFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(XSL_FILE_H)
#define XSL_FILE_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FileBase.h"

//////////////////////////////////////////////////////////////////////
// Wrapper class for a single cached XML file.
//////////////////////////////////////////////////////////////////////
class CXslFile : public CFileBase
{
public:

	// Constructors
	CXslFile();
	CXslFile( CBstr strFilePath );
	CXslFile( const CXslFile &rFrom );

	// Destructor
	virtual ~CXslFile();

	// operator=
	const CXslFile& operator=( const CXslFile& oSrc );

private:
	
	// Utility function used to centralize some common code.
	virtual void InternalLoadFile();

public:

	// An XSL template of the XSL file contents.
	MSXML2::IXSLTemplatePtr m_pTemplate;

	// A comma delimited list of CRUD params.
	CBstr m_strCrud;

	// A comma delimited list of non-CRUD params.
	CBstr m_strParams;

	// Boolean used to track whether this style sheet needs a session object.
	bool m_bNeedsSession;

};

// A vector of CXslFiles
typedef CFileList<CXslFile> CXslFileList;

// An iterator of CXslFiles
typedef CXslFileList::iterator CXslFileIterator;

#endif // !defined(XSL_FILE_H)
