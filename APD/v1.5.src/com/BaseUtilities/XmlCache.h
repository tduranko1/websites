/////////////////////////////////////////////////////////////////////////////
//	
// XmlCache.h : Declaration of the CXmlCache
//
/////////////////////////////////////////////////////////////////////////////
#ifndef __XMLCACHE_H_
#define __XMLCACHE_H_

#include "resource.h"       // main symbols
#include "XmlFile.h"
#include "ErrorInfoBase.h"

/////////////////////////////////////////////////////////////////////////////
// CXmlCache
class ATL_NO_VTABLE CXmlCache : 
	//public CComObjectRoot,
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<CXmlCache, &CLSID_XmlCache>,
	public IDispatchImpl<IXmlCache, &IID_IXmlCache, &LIBID_BaseUtilitiesLib>,
	public ISupportErrorInfo,
	public IObjectControl,
	public CErrorInfoBase
{
public:

	CXmlCache()
	{
		m_pUnkMarshaler = NULL;
	}

	virtual ~CXmlCache();

	DECLARE_REGISTRY_RESOURCEID(IDR_XMLCACHE)
	DECLARE_GET_CONTROLLING_UNKNOWN()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	BEGIN_COM_MAP(CXmlCache)
		COM_INTERFACE_ENTRY(IDispatch)
		COM_INTERFACE_ENTRY(IObjectControl)
		COM_INTERFACE_ENTRY(IXmlCache)
		COM_INTERFACE_ENTRY(ISupportErrorInfo)
	//	COM_INTERFACE_ENTRY_AGGREGATE(IID_IMarshal, m_pUnkMarshaler.p)
	END_COM_MAP()

	HRESULT FinalConstruct()
	{
		return CoCreateFreeThreadedMarshaler(
			GetControllingUnknown(), &m_pUnkMarshaler.p);
	}

	void FinalRelease()
	{
		m_pUnkMarshaler.Release();
	}

	CComPtr<IUnknown> m_pUnkMarshaler;

private:

	// Holds the cached XML data.
	CXmlFileList m_arXml;

public:

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)( REFIID riid );

// IObjectControl
	STDMETHOD(Activate)();
	STDMETHOD_(VOID, Deactivate)();
	STDMETHOD_(BOOL, CanBePooled)();

// IXmlCache
	STDMETHOD(GetErrorDescription)( /*[out,retval]*/ BSTR* pstrDescription );
	STDMETHOD(GetDom)( /*[in]*/ BSTR strFilePath, /*[out,retval]*/ IDispatch** ppDOMDocument );
	STDMETHOD(GetIsCached)( /*[in]*/ BSTR strFilePath, /*[out,retval]*/ int* pbIsCached );

};

#endif //__XMLCACHE_H_
