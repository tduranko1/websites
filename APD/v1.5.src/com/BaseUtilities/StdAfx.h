// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently

#if !defined(AFX_STDAFX_H__324CB9EA_6F6D_48EA_95C7_82153D4A1783__INCLUDED_)
#define AFX_STDAFX_H__324CB9EA_6F6D_48EA_95C7_82153D4A1783__INCLUDED_

// we only work in UNICODE
#ifndef _UNICODE
#error This module must be build in UNICODE
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define STRICT
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0400
#endif

//default: _ATL_MULTI_THREADED
//optional: _ATL_SINGLE_THREADED
//#define _ATL_APARTMENT_THREADED

#include <atlbase.h>
//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
extern CComModule _Module;

#include <atlcom.h>
#include <comdef.h>
#include <comsvcs.h>

#import "msxml4.dll"

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__324CB9EA_6F6D_48EA_95C7_82153D4A1783__INCLUDED)
