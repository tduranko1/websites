///////////////////////////////////////////////////////////////////////////
//	
//	CBStr : BSTR wrapper class with manipulation routines
//
///////////////////////////////////////////////////////////////////////////
#ifndef _CBSTR_H_D130E080_1B3E_11d2_955A_00805FC3DFB4
#define _CBSTR_H_D130E080_1B3E_11d2_955A_00805FC3DFB4

///////////////////////////////////////////////////////////////////////////
// CBstr class
///////////////////////////////////////////////////////////////////////////
class CBstr : public _bstr_t
{
public:	 
      // Constructors
      CBstr();
      CBstr( const CBstr& strInput);
      CBstr( const _bstr_t& strInput);
      CBstr( const TCHAR* strInput);
      // _bstr_t mimic constructors
      CBstr( const _variant_t& varinput );
      CBstr( BSTR strinput, bool bcopy );

//      CBstr(TCHAR ch, int nRepeat = 1);

      //string population
      bool LoadString(HINSTANCE hInstance, UINT uID);

	  // The string as an array
//      int GetLength() const;
      bool IsEmpty() const;
      void Empty();
      TCHAR GetAt(int nIndex) const;
      void SetAt(int nIndex, TCHAR ch);

      // Comparison
      int Compare(const TCHAR* psz) ;//const;
      int Compare(const CBstr& str) const;
      int CompareNoCase(const TCHAR* psz) const;
      int CompareNoCase(const CBstr& str) const;
      int Collate(const TCHAR* psz) const;
      int Collate(const CBstr& str) const;

      // Extraction
//      CBstr Mid(int nFirst) const;
      CBstr Mid(int nFirst, int nCount) const;
      CBstr Left(int nCount) const;
      CBstr Right(int nCount) const;
      CBstr SpanIncluding(const TCHAR* pszCharSet) const;
      CBstr SpanExcluding(const TCHAR* pszCharSet) const;

      // Other Conversions
      void MakeUpper();
      void MakeLower();
      void MakeReverse();
      void TrimLeft();
      void TrimRight();
      void Format(const TCHAR* pszFormat, ... );

      // Searching - THESE DONT WORK?!
      //int Find(const TCHAR& ch) const;
      //int Find(const TCHAR* psz) const;
      //int ReverseFind(const TCHAR& ch) const;
      //int FindOneOf(const TCHAR* pszCharSet) const;

		// Searching - NEW VERSION
      int Find(const TCHAR* psz) const;

      // Operators
      TCHAR operator[](int nIndex) const;
      operator const TCHAR*() const;

};

#endif // _CBSTR_H_D130E080_1B3E_11d2_955A_00805FC3DFB4
