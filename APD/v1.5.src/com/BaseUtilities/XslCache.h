/////////////////////////////////////////////////////////////////////////////
//	
// XslCache.h : Declaration of the CXslCache
//
/////////////////////////////////////////////////////////////////////////////
#ifndef __XSLCACHE_H_
#define __XSLCACHE_H_

#include "resource.h"       // main symbols
#include "XslFile.h"
#include "ErrorInfoBase.h"

/////////////////////////////////////////////////////////////////////////////
// CXslCache
class ATL_NO_VTABLE CXslCache : 
	//public CComObjectRoot,
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<CXslCache, &CLSID_XslCache>,
	public IDispatchImpl<IXslCache, &IID_IXslCache, &LIBID_BaseUtilitiesLib>,
	public ISupportErrorInfo,
	public IObjectControl,
	public CErrorInfoBase
{
public:

	CXslCache()
	{
		m_pUnkMarshaler = NULL;
	}

	virtual ~CXslCache();

	DECLARE_REGISTRY_RESOURCEID(IDR_XSLCACHE)
	DECLARE_GET_CONTROLLING_UNKNOWN()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	BEGIN_COM_MAP(CXslCache)
		COM_INTERFACE_ENTRY(IDispatch)
		COM_INTERFACE_ENTRY(IObjectControl)
		COM_INTERFACE_ENTRY(IXslCache)
		COM_INTERFACE_ENTRY(ISupportErrorInfo)
	//	COM_INTERFACE_ENTRY_AGGREGATE(IID_IMarshal, m_pUnkMarshaler.p)
	END_COM_MAP()

	HRESULT FinalConstruct()
	{
		return CoCreateFreeThreadedMarshaler(
			GetControllingUnknown(), &m_pUnkMarshaler.p);
	}

	void FinalRelease()
	{
		m_pUnkMarshaler.Release();
	}

	CComPtr<IUnknown> m_pUnkMarshaler;

private:

	// Holds the cached XML data.
	CXslFileList m_arXsl;

public:
	STDMETHOD(GetNeedsSession)(/*[in]*/ BSTR strFilePath, /*[out,retval]*/ int *pbNeedsSession);

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)( REFIID riid );

// IObjectControl
	STDMETHOD(Activate)();
	STDMETHOD_(VOID, Deactivate)();
	STDMETHOD_(BOOL, CanBePooled)();

// IXslCache
	STDMETHOD(GetErrorDescription)( /*[out,retval]*/ BSTR* pstrDescription );
	STDMETHOD(GetCrudList)( /*[in]*/ BSTR strFilePath, /*[out,retval]*/ BSTR* pstrCrud );
	STDMETHOD(GetParamList)( /*[in]*/ BSTR strFilePath, /*[out,retval]*/ BSTR* pstrParams );
	STDMETHOD(CreateProcessor)( /*[in]*/ BSTR strFilePath, /*[out,retval]*/ IDispatch** ppXslProcessor );
	STDMETHOD(GetIsCached)( /*[in]*/ BSTR strFilePath, /*[out,retval]*/ int* pbIsCached );

};

#endif //__XSLCACHE_H_
