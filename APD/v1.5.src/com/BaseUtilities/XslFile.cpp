//////////////////////////////////////////////////////////////////////
//
// XslFile.cpp: implementation of the CXslFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "XslFile.h"

//////////////////////////////////////////////////////////////////////
// Default Construction
//////////////////////////////////////////////////////////////////////
CXslFile::CXslFile() :
	CFileBase()
{
}

//////////////////////////////////////////////////////////////////////
// Construction
//////////////////////////////////////////////////////////////////////
CXslFile::CXslFile( CBstr strFilePath ) :
	CFileBase( strFilePath )
{
	if ( strFilePath.length() != 0 )
		LoadFile();
}

//////////////////////////////////////////////////////////////////////
// Copy Constructor
//////////////////////////////////////////////////////////////////////
CXslFile::CXslFile( const CXslFile &rFrom )
{
	*this = rFrom;
}

//////////////////////////////////////////////////////////////////////
// Destruction
//////////////////////////////////////////////////////////////////////
CXslFile::~CXslFile()
{
}

//////////////////////////////////////////////////////////////////////
// Operator =
//////////////////////////////////////////////////////////////////////
const CXslFile& CXslFile::operator=( const CXslFile& oSrc )
{
	if ( this != &oSrc )
	{
		( * (CFileBase*) this ) = * (CFileBase*) &oSrc;
		m_pTemplate = oSrc.m_pTemplate;
		m_strCrud = oSrc.m_strCrud.copy();
		m_strParams = oSrc.m_strParams.copy();
		m_bNeedsSession = oSrc.m_bNeedsSession;
	}
	return *this;
}

//////////////////////////////////////////////////////////////////////
// Utility function used to centralize some common code.
//////////////////////////////////////////////////////////////////////
void CXslFile::InternalLoadFile()
{
	// First Template object creation if needed.
	if ( m_pTemplate == NULL )
		m_pTemplate.CreateInstance( __uuidof( MSXML2::XSLTemplate40 ) );

	// Clear out our param string vars.
	m_strCrud.Empty();
	m_strParams.Empty();

	// Create a temp DOM to load XSL with.
	MSXML2::IXMLDOMDocument2Ptr pDom;
	pDom.CreateInstance( __uuidof( MSXML2::FreeThreadedDOMDocument40 ) );

	// Set up the DOM for asynchronous transforms.
	pDom->async = VARIANT_FALSE;
	pDom->resolveExternals = VARIANT_FALSE;
	
	// Load the XSL from disk.
	if ( VARIANT_FALSE == pDom->load( m_strFilePath ) )
		throw CBstr( L"Could not load file '" + m_strFilePath + "'" );

	// Throw any parse errors back to caller.
	if ( pDom->parseError->errorCode != 0 )
	   throw CBstr( pDom->parseError->reason );

	// Determine whether this style sheet needs a session object.
	// We do so by looking for the session namespace.

	MSXML2::IXMLDOMSchemaCollectionPtr pSchemaCol( pDom->namespaces );

	m_bNeedsSession = false;

	for ( int nA = 0; nA < pSchemaCol->Getlength(); nA++ )
	{
		CBstr strUrn = pSchemaCol->namespaceURI[ nA ];
		if ( strUrn.Find( L"session-manager" ) >= 0 )
			m_bNeedsSession = true;
	}

	// Set the "xsl" namespace for the XSL dom.
	// This allows us to XPath into individual XSL tags.
	pDom->setProperty( L"SelectionNamespaces", L"xmlns:xsl='http://www.w3.org/1999/XSL/Transform'" );
	pDom->setProperty( L"SelectionLanguage", L"XPath" );

	// Extract CRUD param list and cache it.
	// Extract non-CRUD param list and cache it.

	MSXML2::IXMLDOMNodeListPtr pList;
	MSXML2::IXMLDOMNodePtr pNode;
	MSXML2::IXMLDOMAttributePtr pAtt;
	CBstr strEntityList;

	// Get a list of params at the style sheet root.
	pList = pDom->selectNodes( L"//xsl:stylesheet/xsl:param" );

	// Loop through all the nodes.
	pNode = pList->nextNode();
	while ( pNode != NULL )
	{
		// Get the parameter name attribute.
		pAtt = pNode->attributes->getNamedItem( L"name" );

		if ( pAtt == NULL )
			throw CBstr( L"Style Sheet xsl:param was missing a name attribute." );

		// Get the actual param name from the attribute.
		CBstr strName = pAtt->value;

		// Is this a CRUD param?
		if ( strName.Right( 4 ) == L"CRUD" )
		{
			pAtt = pNode->attributes->getNamedItem( L"select" );

			if ( pAtt == NULL )
				throw CBstr( L"Style Sheet xsl:param was missing a select attribute." );

			// Get the param permission entity from the attribute.
			CBstr strEntity = pAtt->value;

			if ( strEntity.IsEmpty() )
				throw CBstr( L"Style Sheet xsl:param select attribute was blank." );

			// Now clear the CRUD select value to avoid namespace errors
			// that arise from select values such as "Action:Reopen Exposure".
			pAtt->value = L"removed";

			// This logic prevents a comma from appearing at the end.
			if ( m_strCrud.IsEmpty() )
			{
				m_strCrud = strName;
				strEntityList = strEntity;
			}
			else
			{
				m_strCrud += L"," + strName;
				strEntityList += L"," + strEntity;
			}
		}
		// Otherwise this is a non-CRUD param.
		else
		{
			// Leave the comma on the end for non-CRUD params to assist
			// the parameter search at the caller.
			m_strParams += strName + L",";
		}

		pNode = pList->nextNode();
	}

	// Tack the entity list onto the CRUD param string.
	if ( m_strCrud.length() != 0 )
		m_strCrud += L"|" + strEntityList;

	//MessageBox( NULL, pDom->xml, L"XSL", 0 );

	// Create a Template for the file.
	try {
		m_pTemplate->PutRefstylesheet( pDom );
	}
	catch( _com_error e ) {
		throw CBstr( L"Error putting '" + m_strFilePath + L"' into XSLTemplate\n" + e.Description() );
	}
	catch (...) {
		throw CBstr( L"Error putting '" + m_strFilePath + L"' into XSLTemplate." );
	}

}
