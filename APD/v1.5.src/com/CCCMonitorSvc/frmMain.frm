VERSION 5.00
Object = "{E7BC34A0-BA86-11CF-84B1-CBC2DA68BF6C}#1.0#0"; "NTSVC.ocx"
Begin VB.Form frmMain 
   Caption         =   "CCC Monitor"
   ClientHeight    =   2100
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   2100
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer tmrCleanup 
      Interval        =   1000
      Left            =   3600
      Top             =   1620
   End
   Begin VB.ListBox lstFiles 
      Height          =   450
      Left            =   660
      TabIndex        =   4
      Top             =   780
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Timer tmrStartService 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   4200
      Top             =   1140
   End
   Begin NTService.NTService CCCMonitorSvc 
      Left            =   4200
      Top             =   780
      _Version        =   65536
      _ExtentX        =   741
      _ExtentY        =   741
      _StockProps     =   0
      DisplayName     =   "CCC Pathways Export Monitor Service"
      ServiceName     =   "CCCMonitorSvc"
      StartMode       =   2
   End
   Begin VB.CommandButton cmdFullRefresh 
      Caption         =   "Full Refresh"
      Height          =   495
      Left            =   2640
      TabIndex        =   3
      Top             =   1560
      Width           =   1215
   End
   Begin VB.Timer tmrFullRefresh 
      Enabled         =   0   'False
      Interval        =   15000
      Left            =   4200
      Top             =   1620
   End
   Begin VB.TextBox txtFileName 
      Height          =   1395
      Left            =   60
      TabIndex        =   2
      Top             =   60
      Width           =   4575
   End
   Begin VB.CommandButton cmdStop 
      Caption         =   "Stop Monitor"
      Height          =   495
      Left            =   1380
      TabIndex        =   1
      Top             =   1560
      Width           =   1215
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start Monitor"
      Height          =   495
      Left            =   60
      TabIndex        =   0
      Top             =   1560
      Width           =   1215
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Const MODULE_NAME As String = "CCCMonitorSvc.frmMain."

'***************************************************************************************
' Procedure : cmdFullRefresh_Click
' DateTime  : 11/5/2004 14:07
' Author    : Ramesh Vishegu
' Purpose   : Full Refresh command button click handler
'***************************************************************************************
'
Private Sub cmdFullRefresh_Click()
10        doFullRefresh
End Sub

'***************************************************************************************
' Procedure : cmdStart_Click
' DateTime  : 11/5/2004 14:08
' Author    : Ramesh Vishegu
' Purpose   : Start command button click handler
'***************************************************************************************
'
Private Sub cmdStart_Click()
    doMonitor
End Sub

'***************************************************************************************
' Procedure : cmdStop_Click
' DateTime  : 11/5/2004 14:08
' Author    : Ramesh Vishegu
' Purpose   : Stop command button click handler
'***************************************************************************************
'
Private Sub cmdStop_Click()
10        tmrFullRefresh.Enabled = False
20        cleanup
End Sub

'***************************************************************************************
' Procedure : Form_Load
' DateTime  : 11/5/2004 14:08
' Author    : Ramesh Vishegu
' Purpose   : Form load handler. This is the entry point for the service. Handle
'             service commands (install, uninstall etc)
'***************************************************************************************
'
Private Sub Form_Load()
          'StartService
10        Select Case UCase(Command)
              Case "-I", "/I", "/INSTALL", "-INSTALL"
20                If CCCMonitorSvc.Install Then
                      'CCCMonitorSvc.LogEvent eLogEventTypeInformation, 0, CCCMonitorSvc.DisplayName & " installed successfully."
30                    MsgBox CCCMonitorSvc.DisplayName & " installed successfully.", vbInformation
40                Else
                      'CCCMonitorSvc.LogEvent eLogEventTypeError, 0, CCCMonitorSvc.DisplayName & " failed to install."
50                    MsgBox CCCMonitorSvc.DisplayName & " failed to install.", vbCritical
60                End If
70                Unload Me
80                End
90            Case "-U", "/U", "/UNINSTALL", "-UNINSTALL"
100               If CCCMonitorSvc.Uninstall Then
110                   MsgBox CCCMonitorSvc.DisplayName & " uninstalled successfully.", vbInformation
120               Else
130                   MsgBox CCCMonitorSvc.DisplayName & " failed to uninstall.", vbCritical
140               End If
150               Unload Me
160               End
170           Case "-DEBUG", "/DEBUG"
180               CCCMonitorSvc.Interactive = True
190               CCCMonitorSvc.Debug = True
200           Case ""
                  'default pass-through
210           Case Else
220               MsgBox "Invalid command option!" & vbCrLf & "try -install, -uninstall, or -debug.", vbInformation
                  'Unload Me
230               End
240       End Select
          
          'Allow the service to accept start/stop and shutdown control events.
          'This must be done before the service is started
250       CCCMonitorSvc.ControlsAccepted = svcCtrlStartStop + svcCtrlShutdown
          
260       CCCMonitorSvc.StartService
          
270       tmrStartService.Enabled = True
          
          'cmdStart_Click
          'End
End Sub

'***************************************************************************************
' Procedure : Form_Unload
' DateTime  : 11/5/2004 14:13
' Author    : Ramesh Vishegu
' Purpose   : Application unload handler. Clean up any objects created
'***************************************************************************************
'
Private Sub Form_Unload(Cancel As Integer)
10        cleanup
End Sub

'***************************************************************************************
' Procedure : CCCMonitorSvc_Continue
' DateTime  : 11/5/2004 14:13
' Author    : Ramesh Vishegu
' Purpose   : Handles the service continue request
'***************************************************************************************
'
Private Sub CCCMonitorSvc_Continue(Success As Boolean)
10        On Error GoTo errHandler
          
20        StatusMessage "Running..."
          
30        tmrStartService.Enabled = True
          
40        Success = True
50        Call CCCMonitorSvc.LogEvent(svcEventInformation, svcMessageInfo, "Service continued")
          
60        Exit Sub
errHandler:
70        StatusMessage "Error: " & Err.Description
End Sub

'***************************************************************************************
' Procedure : CCCMonitorSvc_Pause
' DateTime  : 11/5/2004 14:14
' Author    : Ramesh Vishegu
' Purpose   : Handles the service pause request
'***************************************************************************************
'
Private Sub CCCMonitorSvc_Pause(Success As Boolean)
10        On Error GoTo errHandler
          
20        cmdStop_Click

30        StatusMessage "Paused"
40        Call CCCMonitorSvc.LogEvent(svcEventError, svcMessageError, "Service paused")
50        Success = True
          
60        Exit Sub
errHandler:
70        StatusMessage "Error: " & Err.Description
End Sub

'***************************************************************************************
' Procedure : CCCMonitorSvc_Start
' DateTime  : 11/5/2004 14:14
' Author    : Ramesh Vishegu
' Purpose   : Handles the service start request.
'***************************************************************************************
'
Private Sub CCCMonitorSvc_Start(Success As Boolean)
10        On Error GoTo errHandler
          
20        Call CCCMonitorSvc.LogEvent(svcEventInformation, svcMessageInfo, "starting Service...")
30        StatusMessage "Running..."
          
40        tmrStartService.Enabled = True

50        Call CCCMonitorSvc.LogEvent(svcEventInformation, svcMessageInfo, "Service started")
          
60        Success = True
          
70        Exit Sub
errHandler:
80        Call CCCMonitorSvc.LogEvent(svcEventError, svcMessageInfo, "Error Starting Service...")
90        StatusMessage "Error: " & Err.Description
End Sub

'***************************************************************************************
' Procedure : CCCMonitorSvc_Stop
' DateTime  : 11/5/2004 14:14
' Author    : Ramesh Vishegu
' Purpose   : Handles the service stop request
'***************************************************************************************
'
Private Sub CCCMonitorSvc_Stop()
10        On Error GoTo errHandler
          
20        cmdStop_Click
          
30        Call CCCMonitorSvc.LogEvent(svcEventInformation, svcMessageInfo, "Service stopped")
          
40        StatusMessage "Stopped"
50        Unload Me
          
60        Exit Sub
errHandler:
70        StatusMessage "Error: " & Err.Description
End Sub

'***************************************************************************************
' Procedure : DoExtract
' DateTime  : 11/5/2004 14:14
' Author    : Ramesh Vishegu
' Purpose   : Extract the data from the list of file to process
'***************************************************************************************
'
Public Sub DoExtract()
          Const PROC_NAME As String = "DoExtract()"
          Dim i As Long
          Dim sEnvFile As String
          Dim sAd1File As String
          Dim sTtlFile As String
          
10        Trace "    Need to process " & CStr(lstFiles.ListCount), PROC_NAME
20        If lstFiles.ListCount > 0 Then
30            For i = lstFiles.ListCount - 1 To 0 Step -1
                  'Debug.Print "   Files to process:" & CStr(lstFiles.ListCount)
40                sEnvFile = lstFiles.List(i)
50                If sEnvFile <> "" Then
60                    sAd1File = Dir(g_objFSO.GetParentFolderName(sEnvFile) & "\" & g_objFSO.GetBaseName(sEnvFile) & "*.ad1")
70                    If sAd1File = "" Then
80                        sAd1File = g_objFSO.BuildPath(g_objFSO.GetParentFolderName(sEnvFile), g_objFSO.GetBaseName(sEnvFile) & "A.ad1")
90                    Else
100                       If g_objFSO.GetParentFolderName(sAd1File) = "" Then
110                           sAd1File = g_objFSO.BuildPath(g_objFSO.GetParentFolderName(sEnvFile), sAd1File)
120                       End If
130                   End If
                      
140                   sTtlFile = g_objFSO.BuildPath(g_objFSO.GetParentFolderName(sEnvFile), g_objFSO.GetBaseName(sEnvFile) & ".ttl")
150                   If g_objFSO.FileExists(sEnvFile) And _
                         g_objFSO.FileExists(sAd1File) And _
                         g_objFSO.FileExists(sTtlFile) Then
160                      ExtractData sEnvFile, sAd1File, sTtlFile
170                      DoEvents
                         'lstProcessed.AddItem lstFiles.List(i)
180                      If Not g_objDictionary.Exists(g_objFSO.GetBaseName(sEnvFile)) Then g_objDictionary.Add g_objFSO.GetBaseName(sEnvFile), sEnvFile
190                      lstFiles.RemoveItem i
200                   Else
                          'there are missing file. move it to the last so that we can
                          '  wait for some time till the other files appear.
210                       Debug.Print "   ENV file not found [" & sEnvFile & "]. Moving it to the end."
                          'If Not g_objFSO.FileExists(sAd1File) Then Trace "      Missing " & sAd1File & " for " & sEnvFile, PROC_NAME
                          'If Not g_objFSO.FileExists(sTtlFile) Then Trace "      Missing " & sTtlFile & " for " & sEnvFile, PROC_NAME
                          'lstFiles.AddItem sEnvFile
                          'lstFiles.RemoveItem i
220                   End If
230               End If
240           Next
250       Else
260           Debug.Print "    No Files to process."
              'Trace "    No Files to process.", PROC_NAME
270       End If
280       tmrFullRefresh.Enabled = True
End Sub

'***************************************************************************************
' Procedure : tmrCleanup_Timer
' DateTime  : 11/5/2004 14:15
' Author    : Ramesh Vishegu
' Purpose   : Timer to check when the File cleanup to begin.
'***************************************************************************************
'
Private Sub tmrCleanup_Timer()
10        If DateDiff("d", g_dteLastCleanup, Now) > 0 Then
20            doFileCleanup
30        End If
End Sub

'***************************************************************************************
' Procedure : tmrFullRefresh_Timer
' DateTime  : 11/5/2004 14:17
' Author    : Ramesh Vishegu
' Purpose   : Timer event to do the full refresh of the export directory
'***************************************************************************************
'
Private Sub tmrFullRefresh_Timer()
    doFullRefresh
End Sub

Private Sub StatusMessage(strMsg As String)
10        If CCCMonitorSvc.Debug = True Then
20            'txtFileName.Text = txtFileName.Text + strMsg + vbCrLf
30        End If
End Sub

'***************************************************************************************
' Procedure : tmrStartService_Timer
' DateTime  : 11/5/2004 14:18
' Author    : Ramesh Vishegu
' Purpose   : A simple timer event that will start the service. Need to implement this way
'             because when we start the service from the Service MMC, the MMC waits for the
'             the application to respond. If we have anything big doing during the start the
'             MMC will timeout.
'***************************************************************************************
'
Private Sub tmrStartService_Timer()
10        tmrStartService.Enabled = False
20        cmdStart_Click
End Sub

'***************************************************************************************
' Procedure : ExtractData
' DateTime  : 11/5/2004 14:05
' Author    : Ramesh Vishegu
' Purpose   : Extract the CCC Pathways data and put it into this service database. This
'             service database and the CCC Pathways exported files are dBase files.
'***************************************************************************************
'
Private Function ExtractData(ByVal sEnvFile As String, ByVal sAd1File As String, ByVal sTtlFile As String) As Boolean
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "ExtractData() "
          Dim con As ADODB.Connection
          Dim rs As ADODB.Recordset
          Dim rsPathwaysData As ADODB.Recordset
          Dim sLynxID As String
          Dim iSupplementNo As Integer
          Dim sClientClaimNumber As String
          Dim cur_Deductible As Currency
          Dim cur_GrossTotal As Currency
          Dim cur_GrossBetterment As Currency
          Dim cur_GrossDeductible As Currency
          Dim cur_OtherAdjustments As Currency
          Dim cur_NetTotal As Currency
          Dim cur_GrossRelatedPriorDamage As Currency
          Dim cur_GrossUnrelatedPriorDamage As Currency
          Dim cur_GrossAppearanceAllowance As Currency
          Dim sTempFolderPath As String
          Dim sMemoFileName As String
          
20        Trace "    ExtractData('" & sEnvFile & "', '" & sAd1File & "', '" & sTtlFile & "')", PROC_NAME
          'check if the temporary location to copy the data files exist
30        sTempFolderPath = g_objFSO.BuildPath(App.Path, "temp")
40        If Not g_objFSO.FolderExists(sTempFolderPath) Then
50            g_objFSO.CreateFolder sTempFolderPath
              'Trace "    Creating a temporary folder: " & sTempFolderPath, PROC_NAME
60        End If
          
          'create the connection object
70        Set con = CreateObject("ADODB.Connection")
80        con.Open "Driver={Microsoft dBASE Driver (*.dbf)};" & _
                      "Dbq=" & sTempFolderPath
          
          'copy the ad1 file to a temporary location and rename it to dbf
90        g_objFSO.CopyFile sAd1File, sTempFolderPath & "\Data.dbf", True
          
          'copy the corresponding memo file if exists
100       sMemoFileName = g_objFSO.BuildPath(g_objFSO.GetParentFolderName(sAd1File), g_objFSO.GetBaseName(sAd1File) & ".dbt")
110       If g_objFSO.FileExists(sMemoFileName) Then
120           g_objFSO.CopyFile sMemoFileName, sTempFolderPath & "\Data.dbt", True
130       End If
          
          'extract the data from the ad1 table
140       Set rsPathwaysData = CreateObject("ADODB.Recordset")
150       rsPathwaysData.Open "Select * from Data.dbf", con, , , adCmdText
          
160       If rsPathwaysData.State = adStateOpen Then
170           If IsNull(rsPathwaysData.fields("POLICY_NO").Value) Then
                  ' current file does not lynx id. skip it.
180               Set rs = CreateObject("ADODB.Recordset")
190               rs.Open g_sDBPathwaysEMS, , adOpenDynamic, adLockOptimistic
                  
                  'check if the record exists
200               rs.Filter = "FileName='" & g_objFSO.GetBaseName(sEnvFile) & "'"
                  
210               If rs.EOF = True Then
                      'reset the filter
220                   rs.Filter = ""
                      
                      'add the new record
230                   rs.AddNew
240                   rs!FileName = g_objFSO.GetBaseName(sEnvFile)
250                   rs!DateCreated = Now
260                   rs.Update
                      
270                   rs.save
280                   Exit Function
290               End If
300           End If
310           Debug.Print "LYNX ID: " & getLYNXID(rsPathwaysData.fields("CLM_NO").Value)
              'sLynxID = rsPathwaysData.fields("POLICY_NO").Value
320           sLynxID = getLYNXID(rsPathwaysData.fields("CLM_NO").Value)
330           If Not IsNull(rsPathwaysData.fields("ASGN_NO").Value) Then
340               sClientClaimNumber = rsPathwaysData.fields("ASGN_NO").Value
350           End If
360       End If
          
370       rsPathwaysData.Close
          
          'delete all temporary files
380       Kill sTempFolderPath & "\*.*"
          
390       DoEvents
          
          'copy the env file to a temporary location and rename it to dbf
400       g_objFSO.CopyFile sEnvFile, sTempFolderPath & "\Data.dbf", True
          
          'copy the corresponding memo file if exists
410       sMemoFileName = g_objFSO.BuildPath(g_objFSO.GetParentFolderName(sEnvFile), g_objFSO.GetBaseName(sEnvFile) & ".dbt")
420       If g_objFSO.FileExists(sMemoFileName) Then
430           g_objFSO.CopyFile sMemoFileName, sTempFolderPath & "\Data.dbt", True
440       End If
          
          
          'extract the data from the env table
450       rsPathwaysData.Open "Select * from Data.dbf", con, , , adCmdText
          
460       If rsPathwaysData.State = adStateOpen Then
470           If IsNumeric(rsPathwaysData.fields("SUPP_NO").Value) Then
480               iSupplementNo = rsPathwaysData.fields("SUPP_NO").Value
490           Else
500               iSupplementNo = 0
510           End If
520       End If
          
530       rsPathwaysData.Close
          
          'delete all temporary files
540       Kill sTempFolderPath & "\*.*"
          
550       DoEvents
          
          'copy the ttl file to a temporary location and rename it to dbf
560       g_objFSO.CopyFile sTtlFile, sTempFolderPath & "\Data.dbf", True
          
          'copy the corresponding memo file if exists
570       sMemoFileName = g_objFSO.BuildPath(g_objFSO.GetParentFolderName(sTtlFile), g_objFSO.GetBaseName(sTtlFile) & ".dbt")
580       If g_objFSO.FileExists(sMemoFileName) Then
590           g_objFSO.CopyFile sMemoFileName, sTempFolderPath & "\Data.dbt", True
600       End If
          
          'extract the data from the env table
610       rsPathwaysData.Open "Select * from Data.dbf", con, , , adCmdText
          
620       If rsPathwaysData.State = adStateOpen Then
630           cur_GrossTotal = rsPathwaysData.fields("G_TTL_AMT").Value
640           cur_GrossBetterment = rsPathwaysData.fields("G_BETT_AMT").Value
650           cur_Deductible = rsPathwaysData.fields("G_DED_AMT").Value
660           cur_NetTotal = rsPathwaysData.fields("N_TTL_AMT").Value
670           cur_GrossRelatedPriorDamage = rsPathwaysData.fields("G_RPD_AMT").Value
680           cur_GrossUnrelatedPriorDamage = rsPathwaysData.fields("G_UPD_AMT").Value
690           cur_GrossAppearanceAllowance = rsPathwaysData.fields("G_AA_AMT").Value
          
700           cur_OtherAdjustments = _
                  cur_GrossRelatedPriorDamage + _
                  cur_GrossUnrelatedPriorDamage - _
                  cur_GrossAppearanceAllowance
710       End If
          
720       rsPathwaysData.Close
          
          'delete all temporary files
730       Kill sTempFolderPath & "\*.*"
          
740       DoEvents
          
750       If Trim(sLynxID) <> "" Then
760           Set rs = CreateObject("ADODB.Recordset")
770           rs.Open g_sDBPathwaysEMS, , adOpenDynamic, adLockOptimistic
              
              'check if the record exists
780           rs.Filter = "FileName='" & g_objFSO.GetBaseName(sEnvFile) & "'"
              
790           If rs.EOF = True Then
                  'reset the filter
800               rs.Filter = ""
                  
                  'add the new record
810               rs.AddNew
820               rs!FileName = g_objFSO.GetBaseName(sEnvFile)
830               rs!LynxID = sLynxID
840               rs!ClientClaimNumber = sClientClaimNumber
850               rs!SupplementNo = iSupplementNo
860               rs!Deductible = cur_Deductible
870               rs!RepairTotal = cur_GrossTotal
880               rs!NetTotal = cur_NetTotal
890               rs!Betterment = cur_GrossBetterment
900               rs!OtherAdj = cur_OtherAdjustments
910               rs!GrossRPD = cur_GrossRelatedPriorDamage
920               rs!GrossUPD = cur_GrossUnrelatedPriorDamage
930               rs!GrossAA = cur_GrossAppearanceAllowance
940               rs!DateCreated = Now
                  
950               rs.Update
                  
960               rs.save
                  
970               Debug.Print "   Number of records: " & CStr(rs.RecordCount)
                  
980               Debug.Print "   Created record for LynxID: " & sLynxID
                  'Trace "   Created record for LynxID: " & sLynxID, PROC_NAME
                  
990           End If
              
1000          rs.Close
1010          Set rs = Nothing
1020      End If
          
errHandler:
1030      If Err.Number <> 0 Then
1040          Trace Err.Description & " at Line # " & CStr(Erl), PROC_NAME
1050      End If
End Function

Private Function getLYNXID(sStr As String) As String
          Const PROC_NAME As String = MODULE_NAME & "getLYNXID()"
10        On Error GoTo errHandler
          Dim oMatch As Object
          Dim re As RegExp
          
          Dim objMatches As MatchCollection
          Dim iSubMatchCount As Integer
          Dim i As Integer
          
20        Set re = New RegExp
30        re.IgnoreCase = True
40        re.Global = True
          
50        re.Pattern = "LYNX[ ID|]{0,}\s{0,}[#|-|=|:]{0,}\s{0,}(\d{6,})-{0,1}(\d{1,}){0,}"
          
60        Set objMatches = re.Execute(sStr)
70        getLYNXID = ""
          
80        If objMatches.Count = 1 Then
90            Set oMatch = objMatches(0)
100           getLYNXID = oMatch.SubMatches(0)
110       End If
errHandler:
120       If Err.Number <> 0 Then
130           Trace Err.Description & vbCrLf & _
                            "      Error Description: " & CStr(Err.Description) & _
                            "at Line: " & Erl, PROC_NAME
140       End If
End Function


