Attribute VB_Name = "modCCCMonitor"
Option Explicit
Option Compare Text

Private Const MODULE_NAME As String = "CCCMonitorSvc.modCCCMonitor."

'Public g_objDataAccessor As Object
'Public g_objEvents As Object
Public g_sCOMUserName As String

Public g_sEnvironment As String
Public g_sCOMMachineName As String
Public g_blnDebugMode As Boolean
Public g_objDictionary As Scripting.Dictionary
Public g_iCurrentDay As Integer
Public g_objFSO As Scripting.FileSystemObject
Public g_sExportPath As String
Public g_sDBPathwaysEMS As String
Public g_dteLastCleanup As Date
Public g_lAgeInDaysAllowed As Long
Public g_sUserName As String
Public g_sMachineName As String
Public g_sLogFolder As String
Public g_bDebug As Boolean

Private Declare Function GetUserNameAPI Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

'***************************************************************************************
' Procedure : doMonitor
' DateTime  : 11/5/2004 14:21
' Author    : Ramesh Vishegu
' Purpose   : The service initializing routine. Initializes the COM objects and retrieves
'             application settings from the config file.
'***************************************************************************************
'
Public Sub doMonitor()
      Const PROC_NAME As String = MODULE_NAME & "doMonitor()"
          Dim rs As ADODB.Recordset
          Dim strDaysAllowed As String
          Dim oXML As MSXML2.DOMDocument
          'Dim oNode As MSXML2.IXMLDOMNode
          Dim oNode As MSXML2.IXMLDOMElement
          Dim sDebug As String
10        On Error GoTo errHandler

          'CreateDataAccessor
          
20        Set g_objFSO = CreateObjectEx("Scripting.FileSystemObject")
          
30        g_sUserName = GetUserName()
40        g_sMachineName = GetWinComputerName()
          
50        g_sUserName = UCase(g_sUserName)
60        g_sMachineName = UCase(g_sMachineName)
          
70        Set oXML = CreateObject("MSXML2.DOMDocument")
          
80        oXML.async = False
          
90        If Not g_objFSO.FileExists(App.Path & "\config.xml") Then
              'missing config file
100           LocalTrace "Missing config file at " & App.Path, PROC_NAME
110           End
120       End If
          
130       oXML.Load App.Path & "\config.xml"
          
140       If oXML.parseError.errorCode <> 0 Then
              'error reading config file
150           LocalTrace "Error parsing the config.xml" & vbCrLf & _
                          "  XML Error: " & oXML.parseError.reason & vbCrLf & _
                          "  at Line: " & oXML.parseError.Line, PROC_NAME
160           End
170       End If
          
180       Set oNode = oXML.selectSingleNode("/CCCMonitorSvc/Machine[@Name='" & g_sMachineName & "']")
          
190       If oNode Is Nothing Then
              'No information for the current machine in the config
200           LocalTrace "Current machine is not present in the config file", PROC_NAME
210           End
220       End If
          
          'read config settings
230       g_sExportPath = oNode.selectSingleNode("Pathways/ExportPath").Text
240       g_sDBPathwaysEMS = oNode.selectSingleNode("Pathways/DBFileName").Text
250       strDaysAllowed = oNode.selectSingleNode("Pathways/AgeInDaysAllowed").Text
260       sDebug = oNode.selectSingleNode("Debug").Text
270       If sDebug = "True" Then
280           g_bDebug = True
290       Else
300           g_bDebug = False
310       End If
320       g_sLogFolder = oNode.selectSingleNode("Debug/@logdir").Text
          
      '    g_blnDebugMode = g_objEvents.IsDebugMode
          
      '    g_sExportPath = getSetting("EstimatingPackages/Pathways/ExportPath")
      '    g_sDBPathwaysEMS = getSetting("EstimatingPackages/Pathways/DBFileName")
      '    strDaysAllowed = getSetting("EstimatingPackages/Pathways/AgeInDaysAllowed")
330       If IsNumeric(strDaysAllowed) Then
340           g_lAgeInDaysAllowed = CLng(strDaysAllowed)
350       Else
360           g_lAgeInDaysAllowed = 30
370       End If
              
      '    g_sCOMMachineName = g_objEvents.mSettings.Machine
          
380       Trace "Starting the CCC Monitor service." & vbCrLf & _
                  "  Application settings:" & vbCrLf & _
                  "     Export Monitor path: " & g_sExportPath & vbCrLf & _
                  "     Pathways EMS database: " & g_sDBPathwaysEMS & vbCrLf & _
                  "     Age in Days Allowed: " & CStr(g_lAgeInDaysAllowed) & _
                  "  Service running at: " & g_sMachineName & vbCrLf & vbCrLf, PROC_NAME
          
390       Set g_objDictionary = CreateObjectEx("Scripting.Dictionary")
400       g_objDictionary.CompareMode = TextCompare
          
          'check the existance of the export path
410       If Not g_objFSO.FolderExists(g_sExportPath) Then
420           Trace "Export Path " & g_sExportPath & " does not exist.", PROC_NAME
430           End
440       End If
          
          'the Pathways EMS data extracted database file will located in the Export Path
450       g_sDBPathwaysEMS = g_objFSO.BuildPath(g_sExportPath, g_sDBPathwaysEMS)
          
          'frmMain.Show
          
460       If Not g_objFSO.FileExists(g_sDBPathwaysEMS) Then
470           createRS
480       Else
              'load the processed files from the database
490           Set rs = CreateObject("ADODB.Recordset")
500           rs.Open g_sDBPathwaysEMS, , adOpenDynamic, adLockOptimistic
              
510           While Not rs.EOF
520               If Not g_objDictionary.Exists(CStr(rs!FileName)) Then g_objDictionary.Add CStr(rs!FileName), ""
530               rs.MoveNext
540           Wend
              
550           Set rs = Nothing
560       End If
          
          'clean up old files
570       doFileCleanup
          
580       frmMain.tmrFullRefresh.Enabled = True
          
590       g_dteLastCleanup = CDate(Format(Now, "mm/dd/yyyy"))
          
600       Exit Sub
errHandler:
610       If Err.Number <> 0 Then
              Dim sMsg As String
620           sMsg = "An Error Occurred in CCC Pathways Export Monitor service." & vbCrLf & _
                         "  Location: " & PROC_NAME & vbCrLf & _
                         "  Error Description: " & Err.Description & vbCrLf & _
                         "  Error Number: " & CStr(Err.Number) & vbCrLf & _
                         "  Inputs: (none)" & vbCrLf & _
                         "  Line: " & Erl
                         
630           mDebugInfo sMsg, PROC_NAME, vbLogEventTypeError
640           End
650       End If
End Sub

'***************************************************************************************
' Procedure : cleanup
' DateTime  : 11/5/2004 14:22
' Author    : Ramesh Vishegu
' Purpose   : Service cleanup routine. Used when the service is shutdown
'***************************************************************************************
'
Public Sub cleanup()
Const PROC_NAME As String = MODULE_NAME & "cleanup()"
    Trace "Exiting the CCC Pathways Export Monitor service.", PROC_NAME
End Sub

'********************************************************************************
'* Creates the object from the class string specified and returns it
'********************************************************************************
Public Function CreateObjectEx(strClassString As String) As Object
      Const PROC_NAME As String = MODULE_NAME & "CreateObjectEx()"
10        On Error GoTo errHandler
          Dim strMsg As String
          
20        Set CreateObjectEx = CreateObject(strClassString)
          
30        Exit Function
errHandler:
40        strMsg = "    Error creating """ + strClassString + """ class object"

50        If Err.Number <> 0 Then
60            mDebugInfo "An Error Occurred in CCC Pathways Export Monitor service." & vbCrLf & _
                         "  Location: " & PROC_NAME & vbCrLf & _
                         "  Error Description: " & Err.Description & vbCrLf & _
                         "  Error Number: " & CStr(Err.Number) & vbCrLf & _
                         "  Inputs: " & vbCrLf & _
                         "    strClassString: " & strClassString & vbCrLf & _
                         "  Line: " & Erl, PROC_NAME, vbLogEventTypeError
70        End If
End Function

'***************************************************************************************
' Procedure : mDebugInfo
' DateTime  : 11/5/2004 14:24
' Author    : Ramesh Vishegu
' Purpose   : Debug message handler. Will email the notification group/person and log the
'             event.
'***************************************************************************************
'
Public Function mDebugInfo(sDebugInfo As String, sProc As String, severity As LogEventTypeConstants)
    On Error Resume Next
    If InStr(1, LCase(Err.Description), "automation error", vbTextCompare) > 0 Then
        sDebugInfo = sDebugInfo & vbCrLf & _
                    "      API Error info: " & FormatApiErrorMessage(Err.LastDllError)
    End If
    Trace sProc & vbCrLf & sDebugInfo, "mDebugInfo:"
End Function

'***************************************************************************************
' Procedure : addDocument
' DateTime  : 11/5/2004 14:24
' Author    : Ramesh Vishegu
' Purpose   : Add a document to the files to extract data from
'***************************************************************************************
'
Public Function addDocument(ByVal sFileName As String) As Boolean
          Const PROC_NAME As String = MODULE_NAME & "addDocument() "
10        On Error GoTo errHandler
          Dim dteFileCreated As Date
          Dim sFileFullPath As String
          Dim oFile As File

          'build the full path to the file name
20        sFileFullPath = g_objFSO.BuildPath(g_sExportPath, sFileName)


30        If g_objFSO.FileExists(sFileFullPath) Then
40            If InQueue(sFileFullPath) = False Then
50                frmMain.lstFiles.AddItem sFileFullPath
                  'trace out the event to the log file
60                Trace "      Added document : " & sFileFullPath, PROC_NAME
70            End If
80        End If
90        addDocument = True
100       Exit Function
errHandler:
110       If Err.Number <> 0 Then
120           addDocument = False
130           mDebugInfo "An Error Occurred in CCC Pathways Export Monitor service." & vbCrLf & _
                         "  Location: " & PROC_NAME & vbCrLf & _
                         "  Error Description: " & Err.Description & vbCrLf & _
                         "  Error Number: " & CStr(Err.Number) & vbCrLf & _
                         "  Inputs: " & vbCrLf & _
                         "     sFileName: " & sFileName & vbCrLf & _
                         "  Line: " & Erl, PROC_NAME, vbLogEventTypeError
140       End If
End Function

'***************************************************************************************
' Procedure : InQueue
' DateTime  : 11/5/2004 14:25
' Author    : Ramesh Vishegu
' Purpose   : Determines if a file was processed (data extracted)
'***************************************************************************************
'
Public Function InQueue(ByVal sFileName As String) As Long
10        InQueue = g_objDictionary.Exists(g_objFSO.GetBaseName(sFileName))
End Function

'***************************************************************************************
' Procedure : Trace
' DateTime  : 11/5/2004 14:25
' Author    : Ramesh Vishegu
' Purpose   : Traces the message to the service log folder as defined in the config
'***************************************************************************************
'
Public Function Trace(sMsg As String, sProcName As String)
    Dim sLogFile As String
    Dim sFolders() As String
    Dim sCurrentFolder As String
    Dim i As Integer
    Dim oTxtFile As TextStream
    
    Debug.Print sMsg
    If g_bDebug Then
        If g_sLogFolder = "" Then
            If Not g_objFSO.FolderExists(App.Path & "\Log") Then g_objFSO.CreateFolder (App.Path & "\Log")
            sLogFile = App.Path & "\Log\" & Format(Now, "mm-dd-yyyy") & " CCC Monitor Service v" & App.Major & "." & App.Minor & ".log"
        Else
            If Not g_objFSO.FolderExists(g_sLogFolder) Then
                'create the logging path
                sFolders = Split(g_sLogFolder, "\")
                
                For i = 0 To UBound(sFolders)
                    If InStr(1, sFolders(i), ":") = 0 Then
                        If i > 0 Then sCurrentFolder = sCurrentFolder & "\"
                        sCurrentFolder = sCurrentFolder & sFolders(i)
                        
                        If Not g_objFSO.FolderExists(sCurrentFolder) Then g_objFSO.CreateFolder (sCurrentFolder)
                    Else
                        sCurrentFolder = sCurrentFolder & sFolders(i)
                    End If
                Next
            Else
                sCurrentFolder = g_sLogFolder
            End If
            sLogFile = sCurrentFolder & "\" & Format(Now, "yyyy-mm-dd") & " CCC Monitor Service v" & App.Major & "." & App.Minor & ".log"
        End If
        
        Set oTxtFile = g_objFSO.OpenTextFile(sLogFile, ForAppending, True)
        oTxtFile.WriteLine sProcName & vbCrLf & sMsg
        oTxtFile.Close
        Set oTxtFile = Nothing
    End If
End Function

'***************************************************************************************
' Procedure : LocalTrace
' DateTime  : 11/5/2004 14:25
' Author    : Ramesh Vishegu
' Purpose   : Traces the message to the service log folder as defined in the config
'***************************************************************************************
'
Public Function LocalTrace(sMsg As String, sProcName As String)
          Dim sLogFile As String
          Dim oTxtFile As TextStream
10        Debug.Print sMsg
20        If Not g_objFSO.FolderExists(App.Path & "\Log") Then g_objFSO.CreateFolder (App.Path & "\Log")
30        sLogFile = App.Path & "\Log\" & Format(Now, "yyyy-mm-dd") & " CCC Monitor Service v" & App.Major & "." & App.Minor & ".log"
          
40        Set oTxtFile = g_objFSO.OpenTextFile(sLogFile, ForAppending, True)
50        oTxtFile.WriteLine sProcName & vbCrLf & sMsg
60        oTxtFile.Close
70        Set oTxtFile = Nothing
End Function

'***************************************************************************************
' Procedure : createRS
' DateTime  : 11/5/2004 14:25
' Author    : Ramesh Vishegu
' Purpose   : Create a blank DBase file. The schema will be used to store the extracted
'             data.
'***************************************************************************************
'
Private Function createRS() As Boolean
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "createRS() "
          Dim rs As ADODB.Recordset
          
20        Set rs = CreateObject("ADODB.Recordset")
          
          'create the fields
30        With rs.fields
40            .Append "FileName", adVarChar, 25
50            .Append "LynxID", adVarChar, 10
60            .Append "ClientClaimNumber", adVarChar, 25
70            .Append "SupplementNo", adInteger
80            .Append "Deductible", adCurrency
90            .Append "RepairTotal", adCurrency
100           .Append "NetTotal", adCurrency
110           .Append "Betterment", adCurrency
120           .Append "OtherAdj", adCurrency
130           .Append "GrossRPD", adCurrency
140           .Append "GrossUPD", adCurrency
150           .Append "GrossAA", adCurrency
160           .Append "DateCreated", adDate
170       End With
          'open the empty recordset
180       rs.Open
          
          'save the empty recordset to the export path
190       rs.save g_sDBPathwaysEMS, adPersistADTG
          
200       Set rs = Nothing
          'Trace "Created a blank database at " & g_sDBPathwaysEMS, PROC_NAME
          
210       createRS = True
          
errHandler:
220       If Err.Number <> 0 Then
230           createRS = False
240       End If
End Function

Public Function GetUserName() As String
          Dim strName As String
          Dim lngSize As Long
10        Call GetUserNameAPI(strName, lngSize)
20        strName = Space$(lngSize)
30        Call GetUserNameAPI(strName, lngSize)
40        strName = Left$(strName, Len(strName) - 1)

50        GetUserName = strName
End Function

'********************************************************************
'* Syntax:  GetWinComputerName
'* Params:  None
'* Purpose: This procedure retrieves a NetBIOS name
'*              associated with the local computer.
'* Returns: An string - the name.
'********************************************************************
Public Function GetWinComputerName() As String
          Const PROC_NAME As String = MODULE_NAME & "GetWinComputerName()"
10        On Error GoTo errHandler
          Dim strName As String
          Dim lngCnt As Long
          
20        strName = Space(255)
30        lngCnt = GetComputerName(strName, 255)
          
40        If lngCnt <> 0 Then
50            strName = Left(strName, InStr(strName, Chr$(0)) - 1)
60        End If
          
70        GetWinComputerName = strName
80        Exit Function
errHandler:
90        If Err.Number <> 0 Then
100           mDebugInfo "An Error Occurred in Glass Imaging application." & vbCrLf & _
                          "      Location: " & PROC_NAME & vbCrLf & _
                          "      Error Description: " & Err.Description & vbCrLf & _
                          "      Error Number: " & CStr(Err.Number) & vbCrLf & _
                          "      Inputs: (none)" & vbCrLf & _
                          "      Line: " & Erl, PROC_NAME, vbLogEventTypeError
110           GetWinComputerName = ""
120       End If
End Function


