Attribute VB_Name = "modReadDirChanges"
Option Explicit
Option Compare Text

Private Const MODULE_NAME As String = "GlassQueueMonitorSvc.modReadDirChanges."

Private Const TIME_OUT = &H102
Private Const FILE_SHARE_DELETE = &H4
Private Const FILE_SHARE_READ = &H1
Private Const FILE_SHARE_WRITE = &H2
Private Const FILE_ALL_ACCESS = &H1FF

Private Const FILE_LIST_DIRECTORY = &H1
Private Const OPEN_EXISTING = &H3
Private Const FILE_FLAG_BACKUP_SEMANTICS = &H2000000
Private Const FILE_FLAG_OVERLAPPED = &H40000000
Private Const FILE_ATTRIBUTE_NORMAL = &H80

Private Enum FILE_NOTIFY_CHANGE
    FILE_NOTIFY_CHANGE_FILE_NAME = &H1
    FILE_NOTIFY_CHANGE_DIR_NAME = &H2
    FILE_NOTIFY_CHANGE_ATTRIBUTES = &H4
    FILE_NOTIFY_CHANGE_SIZE = &H8
    FILE_NOTIFY_CHANGE_LAST_WRITE = &H10
    FILE_NOTIFY_CHANGE_LAST_ACCESS = &H20
    FILE_NOTIFY_CHANGE_CREATION = &H40
    FILE_NOTIFY_CHANGE_SECURITY = &H100
End Enum

Private Enum FILE_ACTION
    FILE_ACTION_ADDED = &H1
    FILE_ACTION_REMOVED = &H2
    FILE_ACTION_MODIFIED = &H3
    FILE_ACTION_RENAMED_OLD_NAME = &H4
    FILE_ACTION_RENAMED_NEW_NAME = &H5
End Enum


'Private Type SECURITY_ATTRIBUTES
'        nLength As Long
'        lpSecurityDescriptor As Long
'        bInheritHandle As Long
'End Type

Private Type OVERLAPPED
        Internal As Long
        InternalHigh As Long
        offset As Long
        OffsetHigh As Long
        hEvent As Long
End Type

Private Type FILE_NOTIFY_INFORMATION
    dwNextEntryOffset As Long
    dwAction As FILE_ACTION
    dwFileNameLength As Long
    wcFileName(1024 - 1) As Byte 'Assigned byte array buffer of 1024 bytes
End Type

Private Declare Function ResetEvent Lib "kernel32" (ByVal hEvent As Long) As Long

Private Declare Function WaitForSingleObject Lib "kernel32" _
   (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long


Private Declare Function CreateEvent Lib "kernel32" Alias "CreateEventA" _
                (ByVal lpEventAttributes As Long, _
                ByVal bManualReset As Long, _
                ByVal bInitialState As Long, _
                ByVal lpName As String) As Long

Private Declare Function GetOverlappedResult Lib "kernel32" _
               (ByVal hFile As Long, lpOverlapped As OVERLAPPED, _
               lpNumberOfBytesTransferred As Long, _
               ByVal bWait As Long) As Long

Public Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long


Private Declare Function CreateFile Lib "kernel32" Alias "CreateFileA" _
  (ByVal lpFileName As String, ByVal dwDesiredAccess As Long, _
  ByVal dwShareMode As Long, ByVal lpSecurityAttributes As Long, _
  ByVal dwCreationDisposition As Long, _
  ByVal dwFlagsAndAttributes As Long, _
  ByVal hTemplateFile As Long) As Long


Private Declare Function ReadDirectoryChangesW Lib "kernel32.dll" _
    (ByVal hDirectory As Long, ByVal lpBuffer As Long, ByVal nBufferLength As Long, _
    ByVal bWatchSubtree As Boolean, ByVal dwNotifyFilter As FILE_NOTIFY_CHANGE, lpBytesReturned As Long, _
    ByVal lpOverlapped As Long, ByVal lpCompletionRoutine As Long) As Long



Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" _
    (ByVal Destination As Long, _
    ByVal Source As Long, _
    ByVal Length As Long)


'Flag to stop mointoring
Public fstop As Boolean

'Flag to indicate the monitoring started
Public fStarted As Boolean
Public hDir As Long                 'Directory Handler
Public hEvent As Long               'Event Handler

Public Sub startMonitor(sPath As String)
    Const PROC_NAME As String = MODULE_NAME & "startMonitor: "
    On Error GoTo errHandler
    Dim sFileName As String
    
    If fStarted = True Then
        Exit Sub
    End If
    
    fStarted = True
    
    fstop = False
    
    Trace "   Entering the ReadChangeNotification " & sPath & " ...", PROC_NAME
    
    'Create the handle to mointor "c:\test"
    hDir = CreateFile(sPath, _
                      FILE_LIST_DIRECTORY, _
                      FILE_SHARE_READ Or FILE_SHARE_DELETE Or FILE_SHARE_WRITE, _
                      0&, _
                      OPEN_EXISTING, _
                      FILE_FLAG_BACKUP_SEMANTICS Or FILE_FLAG_OVERLAPPED, _
                      0&)
    
    'Documenting CreateEvent
    'Create an Event for Async mode in ReadDirectoryChangesW
    'hEvent = CreateEvent( _
    '        0&, _              'No Security Attribute
    '        True, _            'Manual Reset
    '        True, _            'Initial State is Set
    '        "DirEvent")        'Unique name of Event
    
    hEvent = CreateEvent( _
            0&, _
            True, _
            True, _
            "DirEvent")
            
    'Create an OverLapped structure for Async mode
    Dim oLap As OVERLAPPED
    'Assign the event created to the Overlapped structure
    With oLap
     .hEvent = hEvent
    End With
    
    
    'Buffer for ReadDirectoryChangesW
    Dim buf(0 To 1024 * 5 - 1) As Byte
    
    'structure to retreive file information
    Dim dirBuf As FILE_NOTIFY_INFORMATION
    
    Dim nUsed As Long 'used only for sync mode
    
    'Documenting ReadDirectoryChangesW call
    'ReadDirectoryChangesW in Asyn mode
    'Call ReadDirectoryChangesW(hDir, _                             ' Directory Handler
    '                          dirBuf, _                            ' Buffer
    '                          LenB(dirBuf), _                      ' Byte Length of Buffer
    '                          True, _                              ' Watch sub tree
    '                          FILE_NOTIFY_CHANGE_FILE_NAME, _      ' Notification Filter
    '                          nUsed, _
    '                          VarPtr(oLap), _                      ' Pointer to OverLapped
    '                          0&)
    
    Call ReadDirectoryChangesW(hDir, _
                              VarPtr(buf(0)), _
                              UBound(buf) + 1, _
                              False, _
                              FILE_NOTIFY_CHANGE_FILE_NAME, _
                              nUsed, _
                              VarPtr(oLap), _
                              0&)
    
    'Just for completeness, if you want to use Sync mode
    'Sync mode is called this way
    'Call ReadDirectoryChangesW(hDir, _
    '                          VarPtr(buf(0)), _
    '                          UBound(buf) + 1, _
    '                          False, _
    '                          FILE_NOTIFY_CHANGE_FILE_NAME, _
    '                          nUsed, _
    '                          0&, _
    '                          0&)
                                
                                
    Dim bstr As String  'string for display of filename
    Dim pos As Long     'location of file entry in buffer buf
    Dim ret As Long     'return value of wait operation
    Dim sMsg As String
    
    Do
     pos = 0
     'wait for hEvent to be unset
     ret = WaitForSingleObject(hEvent, 100)
     If ret <> TIME_OUT Then
        'get the first entry in buf
        CopyMemory VarPtr(dirBuf), VarPtr(buf(pos)), Len(dirBuf)
        
        'uncomment if you want to see details of dirBuf
        'Debug.Print dirBuf.dwFileNameLength & " " & dirBuf.dwNextEntryOffset
        
        Select Case dirBuf.dwAction
            Case FILE_ACTION.FILE_ACTION_ADDED:
               'sMsg = "ADDED "
               sFileName = getAddedFileName(dirBuf)
               'Debug.Print sFileName & " was added."
               If sFileName <> "" And g_objFSO.GetExtensionName(sFileName) = "ENV" Then addDocument sFileName
            'Not mointoring File modification
            'Case FILE_ACTION.FILE_ACTION_MODIFIED: 'Debug.Print "MODIFIED ";
            Case FILE_ACTION.FILE_ACTION_REMOVED: 'Debug.Print "REMOVED";
            Case FILE_ACTION.FILE_ACTION_RENAMED_NEW_NAME: 'Debug.Print "RENAME NEW ";
            Case FILE_ACTION.FILE_ACTION_RENAMED_OLD_NAME: 'Debug.Print "RENAME OLD ";
        End Select
        
        While dirBuf.dwNextEntryOffset <> 0 ' 0 this indicate last entry
          'get next entry in buf
          pos = pos + dirBuf.dwNextEntryOffset
          CopyMemory VarPtr(dirBuf), VarPtr(buf(pos)), Len(dirBuf)
          
          'uncomment if you want to see details of dirBuf
          'Debug.Print dirBuf.dwFileNameLength & " " & dirBuf.dwNextEntryOffset
          
            Select Case dirBuf.dwAction
                Case FILE_ACTION.FILE_ACTION_ADDED:
                     'sMsg = "ADDED "
                    sFileName = getAddedFileName(dirBuf)
                    'Debug.Print sFileName & " was added."
                    If sFileName <> "" And g_objFSO.GetExtensionName(sFileName) = "ENV" Then addDocument sFileName
                'Not mointoring File modification
                'Case FILE_ACTION.FILE_ACTION_MODIFIED: Debug.Print "MODIFIED ";
                Case FILE_ACTION.FILE_ACTION_REMOVED: 'Debug.Print "REMOVED ";
                Case FILE_ACTION.FILE_ACTION_RENAMED_NEW_NAME: 'Debug.Print "RENAME NEW ";
                Case FILE_ACTION.FILE_ACTION_RENAMED_OLD_NAME: 'Debug.Print "RENAME OLD ";
            End Select
            'sMsg = sMsg & sFileName & vbCrLf 'getAddedFileName(dirBuf)
            'frmMain.txtFileName.Text = sMsg
        Wend
        
        'reser the event mointering and repeat the whole process
        ResetEvent hEvent
        Call ReadDirectoryChangesW(hDir, VarPtr(buf(0)), UBound(buf) + 1, True, _
                                  FILE_NOTIFY_CHANGE_FILE_NAME, _
                                  nUsed, VarPtr(oLap), 0&)
     End If
     DoEvents
    Loop While Not fstop ' if fstop is true exit loop
    
errHandler:

    'close all the handles
    If hEvent <> 0 Then CloseHandle hEvent
    If hDir <> 0 Then CloseHandle hDir
    fStarted = False
    
    Trace "   Closing the ReadChangeNotification...", PROC_NAME
    
    If Err.Number <> 0 Then
        mDebugInfo "An Error Occurred in Glass Imaging Queue Monitor service." & vbCrLf & _
                   "  Location: " & PROC_NAME & vbCrLf & _
                   "  Error Description: " & Err.Description & vbCrLf & _
                   "  Error Number: " & CStr(Err.Number) & vbCrLf & _
                   "  Inputs: " & vbCrLf & _
                   "    sPath: " & sPath & vbCrLf & _
                   "  Line: " & Erl, PROC_NAME, vbLogEventTypeError
    End If
End Sub

Public Function getAddedFileName(dirBuf As FILE_NOTIFY_INFORMATION) As String
          Const PROC_NAME As String = MODULE_NAME & "getAddedFileName: "
10        On Error GoTo errHandler
          Dim bstr As String  'string for display of filename
20        bstr = dirBuf.wcFileName
30        bstr = Left(bstr, dirBuf.dwFileNameLength / 2)
          
40        getAddedFileName = bstr
50        Exit Function
errHandler:
60        If Err.Number <> 0 Then
70            getAddedFileName = ""
80            mDebugInfo "An Error Occurred in Glass Imaging Queue Monitor service." & vbCrLf & _
                         "  Location: " & PROC_NAME & vbCrLf & _
                         "  Error Description: " & Err.Description & vbCrLf & _
                         "  Error Number: " & CStr(Err.Number) & vbCrLf & _
                         "  Inputs: " & vbCrLf & _
                         "    dirBuf.dwAction: " & CStr(dirBuf.dwAction) & vbCrLf & _
                         "    dirBuf.dwFileNameLength: " & CStr(dirBuf.dwFileNameLength) & vbCrLf & _
                         "    dirBuf.dwNextEntryOffset: " & CStr(dirBuf.dwNextEntryOffset) & vbCrLf & _
                         "    dirBuf.wcFileName: " & CStr(dirBuf.wcFileName) & vbCrLf & _
                         "  Line: " & Erl, PROC_NAME, vbLogEventTypeError
90        End If
End Function
