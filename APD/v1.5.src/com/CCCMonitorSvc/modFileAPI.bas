Attribute VB_Name = "modFileAPI"
Option Explicit
Option Compare Text

Private Const MODULE_NAME As String = "CCCMonitorSvc.modFileAPI."

Private Type FILETIME
        dwLowDateTime As Long
        dwHighDateTime As Long
End Type

Private Type WIN32_FIND_DATA
    dwFileAttributes As Long
    ftCreationTime As FILETIME
    ftLastAccessTime As FILETIME
    ftLastWriteTime As FILETIME
    nFileSizeHigh As Long
    nFileSizeLow As Long
    dwReserved0 As Long
    dwReserved1 As Long
    cFileName As String * 260
    cAlternate As String * 14
End Type


Private Type SYSTEMTIME
        wYear As Integer
        wMonth As Integer
        wDayOfWeek As Integer
        wDay As Integer
        wHour As Integer
        wMinute As Integer
        wSecond As Integer
        wMilliseconds As Integer
End Type

Private Const FILE_ATTRIBUTE_DIRECTORY = &H10

Private Declare Function FindFirstFile Lib "kernel32" _
    Alias "FindFirstFileA" (ByVal lpFileName As String, _
    lpFindFileData As WIN32_FIND_DATA) As Long
Private Declare Function FindNextFile Lib "kernel32" _
    Alias "FindNextFileA" (ByVal hFindFile As Long, _
    lpFindFileData As WIN32_FIND_DATA) As Long
Private Declare Function FindClose Lib "kernel32" _
    (ByVal hFindFile As Long) As Long
Private Declare Function FileTimeToSystemTime Lib "kernel32" (lpFileTime As FILETIME, lpSystemTime As SYSTEMTIME) As Long
Private Declare Function FileTimeToLocalFileTime Lib "kernel32" (lpFileTime As FILETIME, lpLocalFileTime As FILETIME) As Long
Private Const ERROR_NO_MORE_FILES As Long = 18&

'***************************************************************************************
' Procedure : doFullRefresh
' DateTime  : 11/5/2004 14:26
' Author    : Ramesh Vishegu
' Purpose   : Populates the list of files found in the CCC Pathways export folder.
'***************************************************************************************
'
Public Sub doFullRefresh()
      Const PROC_NAME As String = MODULE_NAME & "doFullRefresh()"
10        On Error GoTo errHandler
          Dim hFind As Long
          Dim sPath As String, oldPath As String
          Dim wf As WIN32_FIND_DATA
          Dim objXML As MSXML2.DOMDocument
          Dim objDocs As Object
          Dim objDoc As Object
          Dim sXML As String
          Dim sFileName As String

          'frmMain.tmrDoFullRefresh.Enabled = False
20        frmMain.tmrFullRefresh.Enabled = False
          
30        If g_objFSO.FolderExists(g_sExportPath) Then

              'Trace "   Doing a full refresh " & FormatDateTime(Now, vbGeneralDate) & "...", "doFullRefresh()"
          
40            sPath = g_sExportPath & "\*.env"
          
50            hFind = FindFirstFile(sPath, wf)
              
60            Do
                  'Get the filename, if any.
70                If InStr(wf.cFileName, Chr$(0)) > 0 Then
80                    sPath = Left$(wf.cFileName, _
                          InStr(wf.cFileName, Chr$(0)) - 1)
90                Else
100                   sPath = wf.cFileName
110               End If
120               If Len(sPath) = 0 Or StrComp(sPath, oldPath) = 0 Then
                      'No files found?
130                   Exit Do
140               ElseIf sPath = "." Or sPath = ".." Then
150                   GoTo Iterate
160               End If
          
                  'check if the file is a env file.
170               If g_objFSO.GetExtensionName(wf.cFileName) = "env" Then
180                   sFileName = Left$(wf.cFileName, InStr(wf.cFileName, Chr$(0)) - 1)
          
                      'Add file with correct image, file or folder
190                   If (wf.dwFileAttributes And FILE_ATTRIBUTE_DIRECTORY) <> FILE_ATTRIBUTE_DIRECTORY Then
200                       Debug.Print "Found file " & sFileName
210                       addDocument sFileName
220                   End If
230               End If
Iterate:
240               FindNextFile hFind, wf
          
          
250               If Err.LastDllError = ERROR_NO_MORE_FILES Then
                      'Trace "   No more files to loop...", "doFullRefresh()"
260                   Exit Do
270               End If
          
280               oldPath = sPath
          
290           Loop
          
300           FindClose hFind
310           frmMain.DoExtract
              
              'Trace "   Completed full refresh ...", PROC_NAME
320       Else
330           Trace "Export Path " & g_sExportPath & " does not exist.", PROC_NAME
340       End If
350       Exit Sub
errHandler:
360       FindClose hFind
370       If Err.Number <> 0 Then
380           mDebugInfo "An Error Occurred in CCC Pathways Export Monitor service." & vbCrLf & _
                         "  Location: " & PROC_NAME & vbCrLf & _
                         "  Error Description: " & Err.Description & vbCrLf & _
                         "  Error Number: " & CStr(Err.Number) & vbCrLf & _
                         "  Inputs: (none)" & vbCrLf & _
                         "  UnprocessedQueuePath: " & g_sExportPath & vbCrLf & _
                         "  Line: " & Erl, PROC_NAME, vbLogEventTypeError
390       End If
End Sub

'***************************************************************************************
' Procedure : doFileCleanup
' DateTime  : 11/5/2004 14:27
' Author    : Ramesh Vishegu
' Purpose   : Clean up those files whose age is older than the number of days allowed
'             setting (in config file).
'***************************************************************************************
'
Public Function doFileCleanup()
          Dim i As Integer
          Dim rs As ADODB.Recordset
          Dim sFileName As String
          
10        frmMain.tmrFullRefresh.Enabled = False
          
          'create the connection object
          'Set con = CreateObject("ADODB.Connection")
          'con.Open "Driver={Microsoft dBASE Driver (*.dbf)};" & _
                      "Dbq=" & sTempFolderPath
          
20        Set rs = CreateObject("ADODB.Recordset")
30        rs.Open g_sDBPathwaysEMS, , adOpenDynamic, adLockOptimistic
          
40        rs.Filter = "dateCreated < #" & Format(Now, "mm/dd/yyyy") & "#"
          'rs.Filter = "dateCreated < #11/10/2004#"
          
50        While Not rs.EOF
          
60            sFileName = g_objFSO.BuildPath(g_objFSO.GetParentFolderName(g_sDBPathwaysEMS), rs!FileName) & "*.*"
              
70            If Dir(sFileName) <> "" Then
80                Kill sFileName
90                If g_objDictionary.Exists(CStr(rs!FileName)) Then g_objDictionary.Remove CStr(rs!FileName)
100               rs.Delete
110           End If
          
120           rs.MoveNext
130       Wend
140       rs.save
150       frmMain.tmrFullRefresh.Enabled = True
End Function

