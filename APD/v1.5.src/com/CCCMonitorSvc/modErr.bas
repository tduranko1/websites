Attribute VB_Name = "modErr"
Option Explicit

Public Enum enmWinVersions
   WinNt40 = 1
   Win95 = 2
   Win98 = 3
   Win2000 = 4
End Enum

'Windows API Constants
Private Const VER_PLATFORM_WIN32_NT = 2
Private Const VER_PLATFORM_WIN32_WINDOWS = 1
Private Const VER_PLATFORM_WIN32s = 0

'Windows API Declarations
Private Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (lpVersionInformation As OSVERSIONINFO) As Long
Private Declare Function FormatMessageAPI Lib "kernel32" Alias "FormatMessageA" (ByVal dwFlags As Long, lpSource As Any, ByVal dwMessageId As Long, ByVal dwLanguageId As Long, ByVal lpBuffer As String, ByVal nSize As Long, Arguments As Long) As Long

Type OSVERSIONINFO 'for GetVersionEx API call
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As Long
    szCSDVersion As String * 128
End Type

'********************************************************************
'* Util methods to prevent calls to this module from modifying
'* any error information stored in the Err object.
'********************************************************************
Type typError
    Number As Long
    Description As String
    Source As String
End Type

'Loggable event types
Public Enum enmNTEventTypes
    eLogEventTypeError = vbLogEventTypeError    '1
    eLogEventTypeWarning = vbLogEventTypeWarning    '2
    eLogEventTypeInformation = vbLogEventTypeInformation    '4
    eLogEventTypeNull = 6   'Picked six so could do numeric compares of these
End Enum


'********************************************************************
'* Syntax:  LogNTEvent strError, eLogEventTypeError
'* Params:  strMsg - detailed description of the error
'*          intEventType - event type (error, warning, info)
'* Purpose: Logs an event to the local machine NT log.
'* Returns: Nothing
'* NOTE:    When debugging SiteUtilities in VB, events that come
'*          through here do not actually make it to the event log.
'********************************************************************
Public Sub LogNTEvent(ByVal strMsg As String, ByVal intEventType As Integer)

          Dim Error As typError
10        PushErr Error
              
20        On Error Resume Next
          
          Dim intWinVersion As Integer
30        intWinVersion = GetWinVersion
         
40        Select Case intWinVersion
              Case WinNt40, Win2000
50                App.StartLogging "", vbLogToNT
60                App.LogEvent strMsg, intEventType
70        End Select
          
80        PopErr Error
          
End Sub


Public Sub PushErr(ByRef Error As typError)
10        Error.Number = Err.Number
20        Error.Description = Err.Description
30        Error.Source = Err.Source
End Sub

Public Sub PopErr(ByRef Error As typError)
10        Err.Number = Error.Number
20        Err.Description = Error.Description
30        Err.Source = Error.Source
End Sub


'********************************************************************
'* Syntax:  GetWinVersion
'* Params:  None
'* Purpose: This procedure retrieves the windows version
'*              information via an API call.
'* Returns: An integer representing the windows version.
'*              The versions are represented in an enum.
'********************************************************************
Public Function GetWinVersion() As enmWinVersions
          Dim osvi As OSVERSIONINFO
          Dim strCSDVersion As String
          
10        osvi.dwOSVersionInfoSize = Len(osvi)
          
20        If GetVersionEx(osvi) = 0 Then
30            Exit Function
40        End If
          
50        Select Case osvi.dwPlatformId
              'NT 4.0
              Case VER_PLATFORM_WIN32_NT
60                Select Case osvi.dwMajorVersion
                      Case 4
70                        GetWinVersion = WinNt40
80                    Case 5
90                        GetWinVersion = Win2000
100               End Select
              'Windows 95
110           Case VER_PLATFORM_WIN32_WINDOWS
120               If osvi.dwMajorVersion > 4 Or (osvi.dwMajorVersion = 4 And osvi.dwMinorVersion > 0) Then
130                   GetWinVersion = Win98
140               Else
150                   GetWinVersion = Win95
160               End If
170       End Select
          
End Function



'********************************************************************
'* Syntax:  FormatApiErrorMessage
'* Params:  lngErrNumber - The API error number
'* Purpose: Retrieves and formats API error messages
'* Returns: An string - the formatted error.
'********************************************************************
Public Function FormatApiErrorMessage(ByVal lngErrNumber As Long) As String
          Dim strBuffer As String * 512, strMsg As String
10        On Error GoTo ErrorHandler
          
20        FormatMessageAPI &H1000, Null, lngErrNumber, 0, strBuffer, 512, 0
          
30        strMsg = strBuffer
          'Strange but necessary manipulations
40        strMsg = Replace(strMsg, vbNewLine, "")
50        strMsg = Replace(strMsg, Chr(0), "")
          
60        FormatApiErrorMessage = strMsg
ErrorHandler:
End Function

