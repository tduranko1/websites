Attribute VB_Name = "MRulesEngine"
Option Explicit

Public Const APP_NAME As String = "RulesEngine."
Public Const RulesEngine_FirstError As Long = &H80090000

Public Enum ErrorCodes
    eXMLParseError = RulesEngine_FirstError
    eXMLNodeNotFound
End Enum

Public g_objEvents As SiteUtilities.CEvents
Public g_objDataAccessor As DataAccessor.CDataAccessor

Public g_strSupportDocPath As String
Public g_blnDebugMode As Boolean

'********************************************************************************
'* Initializes global objects and variables
'********************************************************************************
Public Sub InitializeGlobals()

          'Create DataAccessor.
10        Set g_objDataAccessor = New DataAccessor.CDataAccessor
          
          'Share DataAccessor's events object.
20        Set g_objEvents = g_objDataAccessor.mEvents
          
          'Get path to support document directory
30        g_strSupportDocPath = App.Path & "\" & Left(APP_NAME, Len(APP_NAME) - 1)
          
          'This will give partner data its own log file.
40        g_objEvents.ComponentInstance = "Rules Engine"
          
          'Initialize our member components first, so we have logging set up.
50        g_objDataAccessor.InitEvents App.Path & "\..\config\config.xml"
          
          'Get config debug mode status.
60        g_blnDebugMode = g_objEvents.IsDebugMode
          

End Sub

'********************************************************************************
'* Terminates global objects and variables
'********************************************************************************
Public Sub TerminateGlobals()
10        Set g_objEvents = Nothing
20        Set g_objDataAccessor = Nothing
End Sub

