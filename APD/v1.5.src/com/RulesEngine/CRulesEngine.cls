VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRulesEngine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Const MODULE_NAME As String = "CRulesEngine:"

Private Sub Class_Initialize()
    'initialize the component
    InitializeGlobals
End Sub

Private Sub Class_Terminate()
    'destroy this instance of the component
    TerminateGlobals
End Sub

'***************************************************************************************
' Procedure : EngineExecute
' DateTime  : 2/9/2006 13:35
' Author    : Ramesh Vishegu
' Purpose   : The main Rules Engine execution. This procedure will make a Rules Package
'             and transform using the RulesEngine.
' Parameters: strDataXML - The data XML for which we want to execute the rules against
'             strRulesDefinitionXML - Rules Definition for the above data.
'             strReferenceXML - Optional. Reference data for the data. If the Rules
'               definition contains Reference macros then this parameter is required.
'               Else the engine will error out.
'***************************************************************************************
'
Public Function EngineExecute(ByVal strDataXML As String, _
                                ByVal strRuleDefinitionXML As String, _
                                Optional ByVal strReferenceXML As String) As String
          Const PROC_NAME As String = MODULE_NAME & "Execute()"
10        On Error GoTo errHandler
          
          Dim strEngineXSLPath As String
          Dim strReturn As String
          
          'variables that will need reset
          Dim objRulesPkgXML As MSXML2.DOMDocument
          Dim objRulesEngineXSL As MSXML2.DOMDocument
          Dim objFSO As Scripting.FileSystemObject
          
          'validate the input parameters
20        g_objEvents.Assert strDataXML <> "", "The Data XML was blank."
30        g_objEvents.Assert strRuleDefinitionXML <> "", "The Rules definition XML was blank."
          
          'initialize the XML objects
40        Set objRulesPkgXML = New MSXML2.DOMDocument
50        objRulesPkgXML.async = False
          
60        Set objRulesEngineXSL = New MSXML2.DOMDocument
70        objRulesEngineXSL.async = False
          
80        Set objFSO = New Scripting.FileSystemObject
          
90        strEngineXSLPath = objFSO.BuildPath(g_strSupportDocPath, "RulesEngine.xsl")
          
100       g_objEvents.Assert objFSO.FileExists(strEngineXSLPath) = True, "Missing Rules Engine transformation file. [" & strEngineXSLPath & "]"
          
110       If g_blnDebugMode Then g_objEvents.Trace "Loading Rules Engine transformation..."
          
          'load the rules engine transformation
120       objRulesEngineXSL.Load strEngineXSLPath
          
130       If objRulesEngineXSL.parseError.errorCode <> 0 Then
140           Err.Raise eXMLParseError, PROC_NAME, "Error parsing Rules Engine XSL." & vbCrLf & _
                                                   "Parse Reason:" & vbCrLf & _
                                                   objRulesEngineXSL.parseError.reason
150       End If
          
          'create the Rules package XML. This will of the following format
          '<RulesPackage>
          '   <Data>
          '       <DataXML>
          '   </Data>
          '   <Reference>
          '       <ReferenceXML/>
          '   </Reference>
          '   <RulesDefinitionXML/>
          '</RulesPackage>
          
160       If g_blnDebugMode Then g_objEvents.Trace "Building the Rules Package..."
          
170       objRulesPkgXML.loadXML "<RulesPackage><Data/><Reference/></RulesPackage>"
          
180       appendXMLFragment objRulesPkgXML, strDataXML, "/RulesPackage/Data"
          
          'load the reference data xml
190       If strReferenceXML <> "" Then
200           appendXMLFragment objRulesPkgXML, strReferenceXML, "/RulesPackage/Reference"
210       End If
          
          'load the rule definitions
220       appendXMLFragment objRulesPkgXML, strRuleDefinitionXML, "/RulesPackage"
          
230       If g_blnDebugMode Then g_objEvents.Trace "Performing transformation..."
          
          'now do the crunching.
240       strReturn = objRulesPkgXML.transformNode(objRulesEngineXSL)
          
250       If g_blnDebugMode Then
260           g_objEvents.Trace "Transformation complete. " & vbCrLf & _
                                  "Result = '" & strReturn & "'"
270       End If
          
280       EngineExecute = strReturn
          
errHandler:
290       Set objRulesEngineXSL = Nothing
300       Set objRulesPkgXML = Nothing
310       Set objFSO = Nothing
          
320       If Err.Number <> 0 Then
330           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                        "Data XML = '" & strDataXML & "'" & vbCrLf & _
                        "Reference XML ='" & strReferenceXML & "'" & vbCrLf & _
                        "Rules XML = '" & strRuleDefinitionXML & "'"
340       End If
End Function

'***************************************************************************************
' Procedure : appendXMLFragment
' DateTime  : 2/9/2006 13:36
' Author    : Ramesh Vishegu
' Purpose   : This procedure will add a document fragment to the source XML to a
'               particular node
' Parameters: objXMLSource - the source XML object to which the fragment will be added
'             strXMLFragment - the XML fragment
'             strNode - an existing node in the objXMLSource to which the strXMLFragment
'               will be added as a child.
'***************************************************************************************
'
Private Sub appendXMLFragment(ByRef objXMLSource As MSXML2.DOMDocument, _
                              ByVal strXMLFragment As String, _
                              ByVal strNode As String)
          Const PROC_NAME As String = "appendXMLFragment()"
10        On Error GoTo errHandler
          Dim objFragmentXML As MSXML2.DOMDocument
          Dim objAppendNode As IXMLDOMNode

20        Set objFragmentXML = New MSXML2.DOMDocument
30        objFragmentXML.async = False
          
          'load the Data XML
40        objFragmentXML.loadXML strXMLFragment
          
          'check for error
50        If objFragmentXML.parseError.errorCode <> 0 Then
60            Err.Raise eXMLParseError, PROC_NAME, "Error parsing Data XML." & vbCrLf & _
                                                   "Parse Reason:" & vbCrLf & _
                                                   objFragmentXML.parseError.reason
70        End If
          
          'get the node to which the fragment will be appended
80        Set objAppendNode = objXMLSource.selectSingleNode(strNode)
          
90        If Not objAppendNode Is Nothing Then
              'append the fragment
100           objAppendNode.appendChild objFragmentXML.documentElement
110       Else
120           Err.Raise eXMLNodeNotFound, PROC_NAME, strNode & " not found."
130       End If
          
errHandler:
140       If Err.Number <> 0 Then
150           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                        "Param XML = '" & objXMLSource.xml & "'" & vbCrLf & _
                        "Fragment XML ='" & strXMLFragment & "'" & vbCrLf & _
                        "Append to node = '" & strNode & "'", "", True
160       End If
End Sub

