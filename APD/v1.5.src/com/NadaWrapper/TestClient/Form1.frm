VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Form1 
   Caption         =   "Nada Wrapper Test Client"
   ClientHeight    =   7545
   ClientLeft      =   4905
   ClientTop       =   2970
   ClientWidth     =   11595
   LinkTopic       =   "Form1"
   ScaleHeight     =   7545
   ScaleWidth      =   11595
   Begin VB.TextBox txtAdjRetail 
      Height          =   375
      Left            =   7200
      TabIndex        =   40
      Top             =   5160
      Width           =   1935
   End
   Begin VB.TextBox txtAdjTrade 
      Height          =   375
      Left            =   3120
      TabIndex        =   39
      Top             =   5160
      Width           =   1935
   End
   Begin VB.TextBox txtAdjLoan 
      Height          =   375
      Left            =   5160
      TabIndex        =   38
      Top             =   5160
      Width           =   1935
   End
   Begin VB.CommandButton GetAdjValues 
      Caption         =   "Get Adjusted Values"
      Height          =   495
      Left            =   360
      TabIndex        =   37
      Top             =   4920
      Width           =   1815
   End
   Begin VB.TextBox txtWeight 
      Height          =   375
      Left            =   9600
      TabIndex        =   35
      Top             =   3480
      Width           =   1935
   End
   Begin VB.TextBox txtMileage 
      Height          =   375
      Left            =   6840
      TabIndex        =   33
      Top             =   3480
      Width           =   1935
   End
   Begin VB.TextBox txtAccsyAdj 
      Height          =   375
      Left            =   9600
      TabIndex        =   30
      Top             =   3960
      Width           =   1935
   End
   Begin MSComctlLib.ListView lvwAccessory 
      Height          =   1815
      Left            =   120
      TabIndex        =   15
      Top             =   2520
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   3201
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Status"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Accessory"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Value"
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Code"
         Object.Width           =   1235
      EndProperty
   End
   Begin VB.ListBox lstVehicles 
      Height          =   840
      Left            =   120
      TabIndex        =   21
      Top             =   600
      Width           =   7335
   End
   Begin VB.TextBox txtMsrp 
      Height          =   375
      Left            =   9240
      TabIndex        =   20
      Top             =   4680
      Width           =   1935
   End
   Begin VB.TextBox txtRetail 
      Height          =   375
      Left            =   7200
      TabIndex        =   19
      Top             =   4680
      Width           =   1935
   End
   Begin VB.TextBox txtLoan 
      Height          =   375
      Left            =   5160
      TabIndex        =   18
      Top             =   4680
      Width           =   1935
   End
   Begin VB.TextBox txtTrade 
      Height          =   375
      Left            =   3120
      TabIndex        =   17
      Top             =   4680
      Width           =   1935
   End
   Begin VB.TextBox txtBody 
      Height          =   375
      Left            =   6840
      TabIndex        =   16
      Top             =   3000
      Width           =   1935
   End
   Begin VB.TextBox txtModel 
      Height          =   375
      Left            =   9600
      TabIndex        =   14
      Top             =   3000
      Width           =   1935
   End
   Begin VB.TextBox txtMake 
      Height          =   375
      Left            =   6840
      TabIndex        =   13
      Top             =   2520
      Width           =   1935
   End
   Begin VB.TextBox txtYear 
      Height          =   375
      Left            =   9600
      TabIndex        =   12
      Top             =   2520
      Width           =   1935
   End
   Begin VB.ComboBox cboRegions 
      Height          =   315
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   11
      Top             =   1560
      Width           =   1815
   End
   Begin VB.CommandButton cmdUid 
      Caption         =   "Decode Uid"
      Height          =   375
      Left            =   2880
      TabIndex        =   10
      Top             =   2040
      Width           =   1215
   End
   Begin VB.TextBox txtUid 
      Height          =   375
      Left            =   600
      TabIndex        =   8
      Top             =   2040
      Width           =   2175
   End
   Begin VB.TextBox txtVin 
      Height          =   375
      Left            =   600
      TabIndex        =   6
      Top             =   120
      Width           =   2175
   End
   Begin VB.ComboBox cboBodies 
      Height          =   315
      Left            =   7800
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   1560
      Width           =   1815
   End
   Begin VB.TextBox txtXml 
      Height          =   1695
      Left            =   720
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Text            =   "Form1.frx":0000
      Top             =   5760
      Width           =   10815
   End
   Begin VB.ComboBox cboModels 
      Height          =   315
      Left            =   5880
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1560
      Width           =   1815
   End
   Begin VB.ComboBox cboMakes 
      Height          =   315
      Left            =   3960
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   1560
      Width           =   1815
   End
   Begin VB.ComboBox cboYear 
      Height          =   315
      Left            =   2040
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1560
      Width           =   1815
   End
   Begin VB.CommandButton cmdVin 
      Caption         =   "Decode Vin"
      Height          =   375
      Left            =   2880
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label Label14 
      Alignment       =   1  'Right Justify
      Caption         =   "Adjusted"
      Height          =   255
      Left            =   2280
      TabIndex        =   42
      Top             =   5280
      Width           =   735
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Base"
      Height          =   255
      Left            =   2400
      TabIndex        =   41
      Top             =   4800
      Width           =   615
   End
   Begin VB.Label Label13 
      Alignment       =   1  'Right Justify
      Caption         =   "Weight"
      Height          =   255
      Left            =   8880
      TabIndex        =   36
      Top             =   3600
      Width           =   615
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "Mileage"
      Height          =   255
      Left            =   5880
      TabIndex        =   34
      Top             =   3600
      Width           =   855
   End
   Begin VB.Label Label11 
      Caption         =   "XML"
      Height          =   375
      Left            =   120
      TabIndex        =   32
      Top             =   5760
      Width           =   495
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "Accessory Adjustment"
      Height          =   255
      Left            =   7680
      TabIndex        =   31
      Top             =   4080
      Width           =   1815
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      Caption         =   "Retail"
      Height          =   255
      Left            =   8520
      TabIndex        =   29
      Top             =   4440
      Width           =   615
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      Caption         =   "Trade"
      Height          =   255
      Left            =   4440
      TabIndex        =   28
      Top             =   4440
      Width           =   615
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "Model"
      Height          =   255
      Left            =   8880
      TabIndex        =   27
      Top             =   3120
      Width           =   615
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Year"
      Height          =   255
      Left            =   8880
      TabIndex        =   26
      Top             =   2640
      Width           =   615
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "MSRP"
      Height          =   255
      Left            =   10320
      TabIndex        =   25
      Top             =   4440
      Width           =   855
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Caption         =   "Loan"
      Height          =   255
      Left            =   6240
      TabIndex        =   24
      Top             =   4440
      Width           =   855
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Body"
      Height          =   255
      Left            =   5880
      TabIndex        =   23
      Top             =   3120
      Width           =   855
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Make"
      Height          =   255
      Left            =   5880
      TabIndex        =   22
      Top             =   2640
      Width           =   855
   End
   Begin VB.Label lblId 
      AutoSize        =   -1  'True
      Caption         =   "UID"
      Height          =   195
      Left            =   120
      TabIndex        =   9
      Top             =   2160
      Width           =   285
   End
   Begin VB.Label lblVin 
      AutoSize        =   -1  'True
      Caption         =   "VIN"
      Height          =   195
      Left            =   120
      TabIndex        =   7
      Top             =   240
      Width           =   270
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Private objNada As NadaWrapper.CNadaWrapper
Private objNada As Object
Private blnLoading As Boolean

'Private objTidy As TidyCOM.TidyObject
Private varVinData As Variant
Private varAcc As Variant

Private Sub Form_Load()
    Dim varData As Variant
    Dim intCounter As Integer
    Dim strdata As String
    
    'This object just used for displaying XML in an eye-friendly manner.
    'Set objTidy = CreateObject("TidyCOM.TidyObject")
    'objTidy.Options.Doctype = "strict"
    'objTidy.Options.DropFontTags = False
    'objTidy.Options.OutputXhtml = True
    'objTidy.Options.InputXml = True
    'objTidy.Options.OutputXml = True
    'objTidy.Options.Indent = 2 'AutoIndent
    'objTidy.Options.TabSize = 8
        
    blnLoading = True
    
    Set objNada = CreateObject("NadaWrapper.CNadaWrapper")
    'Set objNada = New NadaWrapper.CNadaWrapper
    
    varData = objNada.GetRegions
    cboRegions.Clear
    
    For intCounter = LBound(varData) To UBound(varData)
        If Not IsEmpty(varData(intCounter, 0)) Then
            cboRegions.AddItem varData(intCounter, 0)
            cboRegions.ItemData(cboRegions.NewIndex) = varData(intCounter, 1)
        End If
    Next intCounter
    cboRegions.ListIndex = 0
    
    'txtXml.Text = objTidy.TidyMemToMem(objNada.GetRegionsXML)
    txtXml.Text = objNada.GetRegionsXML
    
    txtVin.Text = "1ftcr10x4rpc08263"
    
    varData = objNada.GetYears
    cboYear.Clear
    
    For intCounter = LBound(varData) To UBound(varData)
        cboYear.AddItem varData(intCounter)
        cboYear.ItemData(cboYear.NewIndex) = varData(intCounter)
    Next intCounter
    
    'txtXml.Text = objTidy.TidyMemToMem(objNada.GetYearsXML)
    txtXml.Text = objNada.GetYearsXML
    
    cboYear.ListIndex = 0
    blnLoading = False
End Sub

Private Sub cboBodies_Click()
    Dim lngRetVal As Long
    
    txtUid.Text = objNada.GetVehicleId(cboYear.Text, cboMakes.ItemData(cboMakes.ListIndex), cboModels.ItemData(cboModels.ListIndex), _
                cboBodies.ItemData(cboBodies.ListIndex))
End Sub

Private Sub cboMakes_Click()
    Dim varData As Variant
    Dim strdata As String
    Dim intCounter As Integer
    
    If blnLoading Then Exit Sub
    blnLoading = True
    
    varData = objNada.GetModels(cboRegions.ItemData(cboRegions.ListIndex), cboYear.Text, cboMakes.ItemData(cboMakes.ListIndex))
    
    cboModels.Clear
    
    For intCounter = LBound(varData) To UBound(varData)
        cboModels.AddItem varData(intCounter, 0)
        cboModels.ItemData(cboModels.NewIndex) = varData(intCounter, 1)
    Next intCounter
    cboModels.ListIndex = 0
    
    'txtXml.Text = objTidy.TidyMemToMem(objNada.GetModelsXML(cboRegions.ItemData(cboRegions.ListIndex), cboYear.Text, cboMakes.ItemData(cboMakes.ListIndex)))
    txtXml.Text = objNada.GetModelsXML(cboRegions.ItemData(cboRegions.ListIndex), cboYear.Text, cboMakes.ItemData(cboMakes.ListIndex))
    
    blnLoading = False
End Sub

Private Sub cboModels_Click()
    Dim varData As Variant
    Dim strdata As String
    Dim intCounter As Integer
    
    If blnLoading Then Exit Sub
    blnLoading = True
    
    varData = objNada.GetBodies(cboRegions.ItemData(cboRegions.ListIndex), cboYear.Text, cboMakes.ItemData(cboMakes.ListIndex), cboModels.ItemData(cboModels.ListIndex))
    
    cboBodies.Clear
    
    For intCounter = LBound(varData) To UBound(varData)
        cboBodies.AddItem varData(intCounter, 0)
        cboBodies.ItemData(cboBodies.NewIndex) = varData(intCounter, 1)
    Next intCounter
    cboBodies.ListIndex = 0
    
    'txtXml.Text = objTidy.TidyMemToMem(objNada.GetBodiesXML(cboRegions.ItemData(cboRegions.ListIndex), cboYear.Text, cboMakes.ItemData(cboMakes.ListIndex), cboModels.ItemData(cboModels.ListIndex)))
    txtXml.Text = objNada.GetBodiesXML(cboRegions.ItemData(cboRegions.ListIndex), cboYear.Text, cboMakes.ItemData(cboMakes.ListIndex), cboModels.ItemData(cboModels.ListIndex))
    
    blnLoading = False
End Sub

Private Sub cboRegions_Click()
    Dim strdata As String
    
    'txtXml.Text = objTidy.TidyMemToMem(objNada.GetRegionsXML)
    txtXml.Text = objNada.GetRegionsXML
End Sub

Private Sub cboYear_Click()
    Dim varData As Variant
    Dim strdata As String
    Dim intCounter As Integer
    
    If blnLoading Then Exit Sub
    blnLoading = True
     
    varData = objNada.GetMakes(cboRegions.ItemData(cboRegions.ListIndex), cboYear.Text)
    
    cboMakes.Clear
    
    For intCounter = LBound(varData) To UBound(varData)
        cboMakes.AddItem varData(intCounter, 0)
        cboMakes.ItemData(cboMakes.NewIndex) = varData(intCounter, 1)
    Next intCounter
    cboMakes.ListIndex = 0
    
    'txtXml.Text = objTidy.TidyMemToMem(objNada.GetMakesXML(cboRegions.ItemData(cboRegions.ListIndex), cboYear.Text))
    txtXml.Text = objNada.GetMakesXML(cboRegions.ItemData(cboRegions.ListIndex), cboYear.Text)
    
    blnLoading = False
    
End Sub

Private Sub cmdUid_Click()
    Dim varVehIdData As Variant

    varVehIdData = objNada.DecodeVehicleId(txtUid.Text, cboRegions.ItemData(cboRegions.ListIndex))
    
    txtXml = ""
    
    txtYear = varVehIdData(eYear)
    txtModel = varVehIdData(eSeries)
    txtMake = varVehIdData(eMake)
    txtBody = varVehIdData(eBody)
    
    txtLoan = varVehIdData(eLoan)
    txtRetail = varVehIdData(eRetail)
    txtMsrp = varVehIdData(eMSRP)
    txtTrade = varVehIdData(eTrade)
    
    txtWeight = varVehIdData(eWeight)
    txtMileage = varVehIdData(eMileage)
    
    varAcc = objNada.GetAccessories(txtUid.Text, cboRegions.ItemData(cboRegions.ListIndex), txtVin.Text)
    
    Dim Item As ListItem
    Dim i As Integer
    
    lvwAccessory.ListItems.Clear
    lvwAccessory.View = lvwReport
    lvwAccessory.Visible = True

    For i = 0 To varAcc(eAccessoryCount) - 1
        Set Item = lvwAccessory.ListItems.Add(, , varAcc(eAccessorySet)(i, eAccStatus))
        Item.SubItems(eAccDescription) = varAcc(eAccessorySet)(i, eAccDescription)
        Item.SubItems(eAccValue) = CStr(varAcc(eAccessorySet)(i, eAccValue))
        Item.SubItems(eAccCode) = CStr(varAcc(eAccessorySet)(i, eAccCode))
    Next i
    
    txtAccsyAdj.Text = varAcc(eAccessoryAdjustment)
    
    'txtXml.Text = objTidy.TidyMemToMem(objNada.GetAccessoriesXML(txtUid.Text, cboRegions.ItemData(cboRegions.ListIndex), txtVin.Text))
    'txtXml.Text = txtXml.Text + objTidy.TidyMemToMem(objNada.DecodeVehicleIdXML(txtUid.Text, cboRegions.ItemData(cboRegions.ListIndex)))
    txtXml.Text = objNada.GetAccessoriesXML(txtUid.Text, cboRegions.ItemData(cboRegions.ListIndex), txtVin.Text)
    txtXml.Text = txtXml.Text + objNada.DecodeVehicleIdXML(txtUid.Text, cboRegions.ItemData(cboRegions.ListIndex))
    
End Sub

Private Sub cmdVin_Click()
    varVinData = objNada.DecodeVin(txtVin.Text, cboRegions.ItemData(cboRegions.ListIndex))
    
    txtYear = varVinData(0, eYear)
    txtModel = varVinData(0, eSeries)
    txtMake = varVinData(0, eMake)
    txtBody = varVinData(0, eBody)
    
    txtLoan = varVinData(0, eLoan)
    txtRetail = varVinData(0, eRetail)
    txtMsrp = varVinData(0, eMSRP)
    txtTrade = varVinData(0, eTrade)
    
    txtWeight = varVinData(0, eWeight)
    txtMileage = varVinData(0, eMileage)
    
    txtUid.Text = varVinData(0, eUID)

    LoadVehiclesList
    'txtXml.Text = objTidy.TidyMemToMem(objNada.DecodeVinXML(txtVin.Text, cboRegions.ItemData(cboRegions.ListIndex)))
    txtXml.Text = objNada.DecodeVinXML(txtVin.Text, cboRegions.ItemData(cboRegions.ListIndex))
End Sub

Private Sub LoadVehiclesList()
    Dim intCounter As Integer
    lstVehicles.Clear
        
    For intCounter = LBound(varVinData) To UBound(varVinData)
        If Not IsEmpty(varVinData(intCounter, eUID)) Then
            lstVehicles.AddItem varVinData(intCounter, eYear) & "-" & _
                varVinData(intCounter, eMake) & "-" & _
                varVinData(intCounter, eSeries) & "-" & _
                varVinData(intCounter, eBody)
            lstVehicles.ItemData(lstVehicles.NewIndex) = intCounter
        End If
    Next intCounter
End Sub

Private Sub lstVehicles_Click()
    If lstVehicles.ListIndex <> -1 Then
    
        Dim intSelIdx As Integer
        Dim intItemIdx As Integer
    
        intSelIdx = lstVehicles.ListIndex
        intItemIdx = lstVehicles.ItemData(intSelIdx)
    
        txtUid.Text = varVinData(intItemIdx, eUID)
        
        'FindComboItem cboYear, varVinData(intItemIdx, eYear)
        'FindComboItem cboMakes, varVinData(intItemIdx, eMake)
        'FindComboItem cboModels, varVinData(intItemIdx, eSeries)
        'FindComboItem cboBodies, varVinData(intItemIdx, eBody)
        
    End If
End Sub

Private Sub FindComboItem(ByRef cbo As ComboBox, ByVal str As String)
    Dim i As Integer
    For i = 0 To cbo.ListCount - 1
        If cbo.List(i) = str Then
            cbo.ListIndex = i
        End If
    Next i
End Sub

Private Sub lvwAccessory_ItemClick(ByVal Item As MSComctlLib.ListItem)
    objNada.ToggleAccessory Item.Index - 1, varAcc

    Dim CurItem As ListItem
    Dim i As Integer
    
    For i = 0 To varAcc(eAccessoryCount) - 1
        Set CurItem = lvwAccessory.ListItems.Item(i + 1)
        CurItem.Text = varAcc(eAccessorySet)(i, eAccStatus)
        CurItem.SubItems(eAccDescription) = varAcc(eAccessorySet)(i, eAccDescription)
        CurItem.SubItems(eAccValue) = CStr(varAcc(eAccessorySet)(i, eAccValue))
        CurItem.SubItems(eAccCode) = CStr(varAcc(eAccessorySet)(i, eAccCode))
    Next i
    
    txtAccsyAdj.Text = varAcc(eAccessoryAdjustment)
End Sub

Private Sub GetAdjValues_Click()
    Dim varAdjVal As Variant
    varAdjVal = objNada.GetAdjustedNADAValues(Val(txtTrade.Text), Val(txtLoan.Text), Val(txtRetail.Text), Val(txtMileage.Text), varAcc)
    
    txtAdjTrade.Text = varAdjVal(eAdjTrade)
    txtAdjLoan.Text = varAdjVal(eAdjLoan)
    txtAdjRetail.Text = varAdjVal(eAdjRetail)
End Sub


