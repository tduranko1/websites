VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnitTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Class CUnitTest
'*
'* This purpose of this class is to establish a standard unit test interface.
'*
'* A client test application will will instantiate and call this interface in
'* turn for all components that implement it.  Results for each component will
'* be displayed to the tester.
'*
'* To implement for your component, include a copy of this file into your project.
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & ".CUnitTest."

'The number of tests called from this CUnitTest implementation.
Private Const mcintNumberOfTests As Integer = 1

Private mstrResults As String
Private mintErrors As String

'********************************************************************************
'* Returns a count of the number of tests that can be run for this application.
'********************************************************************************
Public Function NumTests() As Integer
10        NumTests = mcintNumberOfTests
End Function

'********************************************************************************
'* Returns the indexed test description.
'********************************************************************************
Public Function TestDesc(ByVal intIndex As Integer) As String
          Const PROC_NAME As String = MODULE_NAME & "TestDesc: "
          
10        Select Case intIndex
          
              'CLS modules
              Case 1: TestDesc = APP_NAME & "CNadaWrapper"
              
20            Case Else:
30                Err.Raise vbObjectError + 1, PROC_NAME, "Passed test index out of range!"
40        End Select
          
End Function

'********************************************************************************
'* Runs the indexed test.
'* Returns the results as a giant formatted string.
'* Returns an error count through intErrors.
'********************************************************************************
Public Function RunTest(ByVal intIndex As Integer, ByRef intErrors As Integer) As String
          Const PROC_NAME As String = MODULE_NAME & "RunTest: "

10        Init intIndex 'Initialize results counters.
          
          'Run the test.
20        Select Case intIndex
          
              'CLS modules
              Case 1: RunTest_CNadaWrapper (intErrors)
              
30            Case Else:
40                Err.Raise vbObjectError + 1, PROC_NAME, "Passed test index out of range!"
50        End Select
          
60        RunTest = Done(intErrors)
End Function

'********************************************************************************
'* Private results formatting helpers
'********************************************************************************

'Initialize results
Private Sub Init(ByVal intIndex As Integer)
10        mstrResults = vbCrLf & "TESTING : " & TestDesc(intIndex) & vbCrLf
20        mintErrors = 0
End Sub

Private Sub Success(ByVal strLine As String)
10        mstrResults = mstrResults & "SUCCESS : " & strLine & vbCrLf
End Sub

Private Sub Verify(ByVal strLine As String)
10        mstrResults = mstrResults & "VERIFY : " & strLine & "  Does this look right?" & vbCrLf
End Sub

Private Sub Failure(ByVal strLine As String)
10        mintErrors = mintErrors + 1
20        mstrResults = mstrResults & "FAILURE : " & strLine & vbCrLf
End Sub

'Add new results line.
Private Function Done(ByRef intErrors As Integer) As String
10        intErrors = mintErrors
20        Done = mstrResults
End Function

'********************************************************************************
'* ADD PRIVATE TEST METHODS BELOW THIS LINE.
'********************************************************************************

'********************************************************************************
'* Tests Class Module CNadaWrapper
'********************************************************************************
Private Function RunTest_CNadaWrapper(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CNadaWrapper
20        If obj Is Nothing Then
30            Failure "Could not create CNadaWrapper"
40        Else
50            Success "Created CNadaWrapper object."
60        End If
          
          'Other CNadaWrapper specific tests:
          
70        Set obj = Nothing
End Function

