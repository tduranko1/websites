
Imports System.Xml
Imports System.Web.Services.Protocols
Imports System.Net.Mail
Imports System.Net

Public Class NadaLoginHelper
    'Dim _strUserId As String = Nothing
    'Dim _strPassword As String = Nothing
    'Dim _strNadaWsLoginUrl As String = Nothing
    Dim _wsLogin As NadaLoginWs.SecureLogin
    'Dim _wsLogin As PRDNadaLogin.SecureLogin



    Public Function GetToken() As String

        Dim strToken As String = ""
        Try
            InitializeWebService()

            Dim loginReq As New NadaLoginWs.GetTokenRequest
            'Dim loginReq As New PRDNadaLogin.GetTokenRequest

            loginReq.Username = My.Settings("LynxNadaWS_UserId").ToString
            loginReq.Password = My.Settings("LynxNadaWS_Password").ToString

            strToken = _wsLogin.getToken(loginReq)

            If Not IsValid(strToken) Then
                Throw New Exception("Login Failed")
            End If

        Catch ex As Exception
            Throw New ApplicationException("Exception Occured")
        End Try

        Return strToken
    End Function

    Private Sub InitializeWebService()
        Try
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12

            'If Not IsValid(_strUserId) Then _strUserId = My.Settings("LynxNadaWS_UserId").ToString
            'If Not IsValid(_strPassword) Then _strPassword = My.Settings("LynxNadaWS_Password").ToString
            'Try
            _wsLogin = New NadaLoginWs.SecureLogin
            '_wsLogin = New PRDNadaLogin.SecureLogin
            'Catch ex As Exception

            'End Try

            _wsLogin.Url = GetSecureLoginURL() 'My.Settings("LynxNadaWS_NadaLoginWs_SecureLogin").ToString

        Catch ex As Exception
            Throw New ApplicationException("Exception Occured")
        End Try
    End Sub
    Public Function GetSecureLoginURL() As String
        Dim str As String = String.Empty
        str = My.Settings("LynxNadaWS_NadaLoginWs_SecureLogin").ToString
        'str = My.Settings("LynxNadaWS_PRDNadaLogin_SecureLogin").ToString

        Select Case PGW.Business.Environment.getEnvironment.Trim.ToUpper
            Case "PRD"
                str = My.Settings("LynxNadaWS_PRDNadaLogin_SecureLogin").ToString
            Case "PROD"
                str = My.Settings("LynxNadaWS_PRDNadaLogin_SecureLogin").ToString
            Case ""
                str = My.Settings("LynxNadaWS_NadaLoginWs_SecureLogin").ToString
        End Select
        Return str
    End Function
    Public Function PingToken(ByVal strToken As String) As Boolean
        'Try
        Return New NadaVehicleWs.Vehicle().ping(strToken)
        'Catch ex As Exception
        ' Throw
        'End Try
    End Function

    Private Function IsValid(ByVal strIn As String) As Boolean
        'Try
        If strIn Is Nothing Then
            Return False
        ElseIf strIn = "" Then
            Return False
        End If

        'Catch ex As Exception

        'End Try
        Return True

    End Function
End Class

Public Enum NadaErrorCodes As Integer
    InvalidToken = 2010

End Enum

Public Class NadaVehicleHelper
    'Dim _strNadaWsVehicleUrl As String = Nothing
    Dim _wsVehicle As New NadaVehicleWs.Vehicle

    Dim _NadaVehicleType As NadaVehicleWs.VehicleTypes
    Dim _NadaPeriod As Integer



    Public Sub New()


        _NadaPeriod = My.Settings("NadaPeriod")
        _NadaVehicleType = NadaVehicleWs.VehicleTypes.UsedCar

        ''''
        _wsVehicle.Url = GetVehicleURL()


    End Sub

    Public Property Vehicletype() As NadaVehicleWs.VehicleTypes
        Get
            Return _NadaVehicleType
        End Get
        Set(ByVal value As NadaVehicleWs.VehicleTypes)

            _NadaVehicleType = value
        End Set
    End Property


    Public Function GetRegions(ByVal strToken As String) As String
        Dim xmlOut As String
        Try
            Dim regionRequest As New NadaVehicleWs.GetRegionsRequest
            regionRequest.Period = _NadaPeriod
            regionRequest.Token = strToken
            regionRequest.VehicleType = _NadaVehicleType

            Dim result() As NadaVehicleWs.Lookup_Struc
            result = _wsVehicle.getRegions(regionRequest)
            xmlOut = ConvertLookupStrucToXML(result)

        Catch ex As Exception
            Throw
        End Try
        Return xmlOut

    End Function
    Public Function GetRegionsArray(ByVal strToken As String) As String
        Dim xmlOut As String
        Try
            Dim regionRequest As New NadaVehicleWs.GetRegionsRequest
            regionRequest.Period = _NadaPeriod
            regionRequest.Token = strToken
            regionRequest.VehicleType = _NadaVehicleType

            Dim result() As NadaVehicleWs.Lookup_Struc
            result = _wsVehicle.getRegions(regionRequest)
            xmlOut = ConvertLookupStrucToStringArray(result)

        Catch ex As Exception
            Throw
        End Try
        Return xmlOut

    End Function
    Public Function GetVehiclesByVin(ByVal strToken As String, ByVal strVin As String) As String
        Dim xmlOut As String
        Try
            Dim vinRequest As New NadaVehicleWs.GetVehiclesByVinRequest
            vinRequest.Token = strToken
            vinRequest.Period = _NadaPeriod
            vinRequest.VehicleType = _NadaVehicleType
            vinRequest.Vin = strVin

            Dim result() As NadaVehicleWs.Vehicle_Struc
            result = _wsVehicle.getVehicles(vinRequest)
            xmlOut = ConvertVehicleStrucToXML(result)

            'Catch ex As Exception
        Catch soapEx As SoapException
            If soapEx.Message.Contains("No vehicle found") Then
                Return ""
            End If
            ' Throw
        End Try
        Return xmlOut
    End Function
    Public Function GetVehiclesByVinArray(ByVal strToken As String, ByVal strVin As String) As String
        Dim xmlOut As String
        Try
            Dim vinRequest As New NadaVehicleWs.GetVehiclesByVinRequest
            vinRequest.Token = strToken
            vinRequest.Period = _NadaPeriod
            vinRequest.VehicleType = _NadaVehicleType
            vinRequest.Vin = strVin

            Dim result() As NadaVehicleWs.Vehicle_Struc
            result = _wsVehicle.getVehicles(vinRequest)
            xmlOut = ConvertVehicleStrucToArray(result)

        Catch soapEx As SoapException
            If soapEx.Message.Contains("No vehicle found") Then
                Return ""
            End If
            ' Throw
        End Try

        Return xmlOut
    End Function

    Public Function GetVehicleByUid(ByVal strToken As String, ByVal vehicleUid As String) As String
        Dim xmlOut As String
        Try
            Dim webRequest As New NadaVehicleWs.GetVehicleByUidRequest
            webRequest.Token = strToken
            webRequest.Period = _NadaPeriod
            webRequest.VehicleType = _NadaVehicleType
            webRequest.Uid = vehicleUid

            Dim webResponse As NadaVehicleWs.Vehicle_Struc
            webResponse = _wsVehicle.getVehicle(webRequest)

            xmlOut = ConvertSingleVehicleStrucToXML(webResponse)

        Catch ex As Exception
            Throw
        End Try
        Return xmlOut
    End Function

    Public Function getVehicleAndValueByUid(ByVal strToken As String, ByVal vehicleUid As String, ByVal Region As Integer, ByVal Mileage As Integer) As String
        Dim xmlOut As String
        Try
            Dim webRequest As New NadaVehicleWs.GetVehicleValuesByUidRequest
            webRequest.Token = strToken
            webRequest.Period = _NadaPeriod
            webRequest.VehicleType = _NadaVehicleType
            webRequest.Uid = vehicleUid
            webRequest.Region = Region
            webRequest.Mileage = Mileage

            Dim webResponse As NadaVehicleWs.VehicleValue_Struc
            webResponse = _wsVehicle.getVehicleAndValueByUid(webRequest)

            xmlOut = ConvertVehicleValueStrucToXML(webResponse)

        Catch ex As Exception
            Throw
        End Try
        Return xmlOut
    End Function
    Public Function getVehicleByVic(ByVal strToken As String, ByVal vehicleVic As String) As String
        Dim xmlOut As String
        Try
            Dim webRequest As New NadaVehicleWs.GetVehicleByVicRequest
            webRequest.Token = strToken
            webRequest.Period = _NadaPeriod
            webRequest.VehicleType = _NadaVehicleType
            webRequest.Vic = vehicleVic

            Dim webResponse As NadaVehicleWs.Vehicle_Struc
            webResponse = _wsVehicle.getVehicleByVic(webRequest)

            xmlOut = ConvertSingleVehicleStrucToXML(webResponse)

        Catch ex As Exception
            Throw
        End Try
        Return xmlOut
    End Function



    Public Function GetModels(ByVal strToken As String, ByVal intYear As Integer, ByVal intMakeCode As Integer) As String
        Dim xmlOut As String

        Try
            Dim seriesRequest As New NadaVehicleWs.GetSeriesRequest
            seriesRequest.Token = strToken
            seriesRequest.Period = _NadaPeriod
            seriesRequest.VehicleType = _NadaVehicleType
            seriesRequest.Year = intYear
            seriesRequest.MakeCode = intMakeCode

            Dim result() As NadaVehicleWs.Lookup_Struc
            result = _wsVehicle.getSeries(seriesRequest)

            xmlOut = ConvertLookupStrucToXML(result)

        Catch ex As Exception
            Throw
        End Try

        Return xmlOut

    End Function
    Public Function GetModelsArray(ByVal strToken As String, ByVal intYear As Integer, ByVal intMakeCode As Integer) As String
        Dim xmlOut As String

        Try
            Dim seriesRequest As New NadaVehicleWs.GetSeriesRequest
            seriesRequest.Token = strToken
            seriesRequest.Period = _NadaPeriod
            seriesRequest.VehicleType = _NadaVehicleType
            seriesRequest.Year = intYear
            seriesRequest.MakeCode = intMakeCode

            Dim result() As NadaVehicleWs.Lookup_Struc
            result = _wsVehicle.getSeries(seriesRequest)

            xmlOut = ConvertLookupStrucToStringArray(result)

        Catch ex As Exception
            Throw
        End Try

        Return xmlOut

    End Function
    Public Function GetMakes(ByVal strToken As String, ByVal intYear As Integer) As String
        Dim xmlOut As String

        Try
            Dim makesRequest As New NadaVehicleWs.GetMakesRequest
            makesRequest.Token = strToken
            makesRequest.Period = _NadaVehicleType
            makesRequest.VehicleType = _NadaVehicleType
            makesRequest.Year = intYear

            Dim result() As NadaVehicleWs.Lookup_Struc
            result = _wsVehicle.getMakes(makesRequest)

            xmlOut = ConvertLookupStrucToXML(result)

        Catch ex As Exception
            Throw
        End Try

        Return xmlOut

    End Function

    Public Function GetMakesArray(ByVal strToken As String, ByVal intYear As Integer) As String
        Dim xmlOut As String

        Try
            Dim makesRequest As New NadaVehicleWs.GetMakesRequest
            makesRequest.Token = strToken
            makesRequest.Period = _NadaVehicleType
            makesRequest.VehicleType = _NadaVehicleType
            makesRequest.Year = intYear

            Dim result() As NadaVehicleWs.Lookup_Struc
            result = _wsVehicle.getMakes(makesRequest)

            xmlOut = ConvertLookupStrucToStringArray(result)

        Catch ex As Exception
            Throw
        End Try

        Return xmlOut

    End Function
    Public Function GetBodies(ByVal strToken As String, ByVal intYear As Integer, ByVal intMakeCode As Integer, ByVal intSeriesCode As Integer) As String
        Dim xmlOut As String

        Try
            Dim bodyRequest As New NadaVehicleWs.GetBodyUidsRequest
            bodyRequest.Token = strToken
            bodyRequest.Period = _NadaPeriod
            bodyRequest.VehicleType = _NadaVehicleType
            bodyRequest.Year = intYear
            bodyRequest.MakeCode = intMakeCode
            bodyRequest.SeriesCode = intSeriesCode

            Dim result() As NadaVehicleWs.Lookup_Struc
            result = _wsVehicle.getBodyUids(bodyRequest)

            xmlOut = ConvertLookupStrucToXML(result)

        Catch ex As Exception
            Throw
        End Try

        Return xmlOut

    End Function
    Public Function GetBodiesArray(ByVal strToken As String, ByVal intYear As Integer, ByVal intMakeCode As Integer, ByVal intSeriesCode As Integer) As String
        Dim xmlOut As String

        Try
            Dim bodyRequest As New NadaVehicleWs.GetBodyUidsRequest
            bodyRequest.Token = strToken
            bodyRequest.Period = _NadaPeriod
            bodyRequest.VehicleType = _NadaVehicleType
            bodyRequest.Year = intYear
            bodyRequest.MakeCode = intMakeCode
            bodyRequest.SeriesCode = intSeriesCode

            Dim result() As NadaVehicleWs.Lookup_Struc
            result = _wsVehicle.getBodyUids(bodyRequest)

            xmlOut = ConvertLookupStrucToStringArray(result)

        Catch ex As Exception
            Throw
        End Try

        Return xmlOut

    End Function
    Public Function GetYears(ByVal strToken As String) As String
        Dim xmlOut As String
        Try

            Dim yearsRequest As New NadaVehicleWs.GetYearsRequest
            yearsRequest.Token = strToken
            yearsRequest.Period = _NadaPeriod
            yearsRequest.VehicleType = _NadaVehicleType

            Dim result() As NadaVehicleWs.Lookup_Struc
            result = _wsVehicle.getYears(yearsRequest)

            xmlOut = ConvertLookupStrucToXML(result)

        Catch ex As Exception
            Throw
        End Try

        Return xmlOut

    End Function
    Public Function GetDefaultvehicleValyeByVin(ByVal strToken As String, ByVal VinValue As String, ByVal Region As Integer, ByVal Mileage As Integer) As String
        Dim xmlOut As String

        Try
            Dim defaultVehValue As New NadaVehicleWs.GetVehicleValuesByVinRequest
            defaultVehValue.Token = strToken
            defaultVehValue.Period = _NadaVehicleType
            defaultVehValue.VehicleType = _NadaVehicleType
            defaultVehValue.Vin = VinValue
            defaultVehValue.Region = Region
            defaultVehValue.Mileage = Mileage

            Dim result As NadaVehicleWs.VehicleValue_Struc
            result = _wsVehicle.getDefaultVehicleAndValueByVin(defaultVehValue)

            xmlOut = ConvertVehicleValueStrucToXML(result)

        Catch ex As SoapException
            Throw
        Catch ex As Exception
            Throw
        End Try

        Return xmlOut

    End Function

    Public Function GetAccessories(ByVal strToken As String, ByVal intUid As Integer, ByVal strVin As String, ByVal intRegion As Integer) As String
        Dim xmlOut As String = ""

        Try
            Dim seriesRequest As New NadaVehicleWs.GetAccessoriesRequest
            Dim strctAccessory() As NadaVehicleWs.Accessory_Struc
            seriesRequest.Token = strToken
            seriesRequest.Period = _NadaPeriod
            seriesRequest.VehicleType = _NadaVehicleType
            seriesRequest.Uid = intUid
            seriesRequest.Vin = strVin
            seriesRequest.Region = intRegion

            'Dim result() As NadaVehicleWs.Lookup_Struc
            strctAccessory = _wsVehicle.getAccessories(seriesRequest)

            If Not strctAccessory Is Nothing Then
                xmlOut = ConvertVehicleAccessoriesStrucToXML(strctAccessory)
            End If

        Catch ex As SoapException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

        Return xmlOut

    End Function

    Public Function GetBaseVehicleValueByUid(ByVal strToken As String, ByVal intUid As Integer, ByVal intRegion As Integer, ByVal intMileage As Integer) As String
        Dim xmlOut As String = ""

        Try
            Dim seriesRequest As New NadaVehicleWs.GetVehicleValuesByUidRequest
            Dim strctAccessory As NadaVehicleWs.StandardValue_Struc
            seriesRequest.Token = strToken
            seriesRequest.Period = _NadaPeriod
            seriesRequest.VehicleType = _NadaVehicleType
            seriesRequest.Uid = intUid
            seriesRequest.Mileage = intMileage
            seriesRequest.Region = intRegion

            strctAccessory = _wsVehicle.getBaseVehicleValueByUid(seriesRequest)

            If Not strctAccessory Is Nothing Then
                xmlOut = ConvertBaseVehicleValueStrucToXML(strctAccessory)
            End If

        Catch ex As SoapException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

        Return xmlOut

    End Function

    Public Function GetVehicleValueByUid(ByVal strToken As String, ByVal intUid As Integer, ByVal intRegion As Integer, ByVal intMileage As Integer) As String
        Dim xmlOut As String = ""

        Try
            Dim seriesRequest As New NadaVehicleWs.GetVehicleValuesByUidRequest
            Dim strctAccessory As NadaVehicleWs.Value_Struc
            seriesRequest.Token = strToken
            seriesRequest.Period = _NadaPeriod
            seriesRequest.VehicleType = _NadaVehicleType
            seriesRequest.Uid = intUid
            seriesRequest.Mileage = intMileage
            seriesRequest.Region = intRegion

            strctAccessory = _wsVehicle.getVehicleValueByUid(seriesRequest)

            If Not strctAccessory Is Nothing Then
                xmlOut = ConvertVehicleByUidValueStrucToXML(strctAccessory)
            End If

        Catch ex As SoapException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

        Return xmlOut

    End Function
    Public Function getHighVehicleAndValueByVin(ByVal strToken As String, ByVal Vin As String, ByVal Region As Integer, ByVal Mileage As Integer) As String
        Dim xmlOut As String = ""

        Try
            Dim webRequest As New NadaVehicleWs.GetVehicleValuesByVinRequest
            Dim webResponse As NadaVehicleWs.VehicleValue_Struc
            webRequest.Token = strToken
            webRequest.Period = _NadaPeriod
            webRequest.VehicleType = _NadaVehicleType
            webRequest.Vin = Vin
            webRequest.Mileage = Mileage
            webRequest.Region = Region

            webResponse = _wsVehicle.getHighVehicleAndValueByVin(webRequest)

            If Not webResponse Is Nothing Then
                xmlOut = ConvertVehicleValueStrucToXML(webResponse)
            End If

        Catch ex As SoapException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

        Return xmlOut

    End Function
    Public Function getLowVehicleAndValueByVin(ByVal strToken As String, ByVal Vin As String, ByVal Region As Integer, ByVal Mileage As Integer) As String
        Dim xmlOut As String = ""

        Try
            Dim webRequest As New NadaVehicleWs.GetVehicleValuesByVinRequest
            Dim webResponse As NadaVehicleWs.VehicleValue_Struc
            webRequest.Token = strToken
            webRequest.Period = _NadaPeriod
            webRequest.VehicleType = _NadaVehicleType
            webRequest.Vin = Vin
            webRequest.Mileage = Mileage
            webRequest.Region = Region

            webResponse = _wsVehicle.getLowVehicleAndValueByVin(webRequest)

            If Not webResponse Is Nothing Then
                xmlOut = ConvertVehicleValueStrucToXML(webResponse)
            End If

        Catch ex As SoapException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

        Return xmlOut

    End Function

    Public Function getMsrpVehicleAndValueByVin(ByVal strToken As String, ByVal Vin As String, ByVal Region As Integer, ByVal Mileage As Integer, ByVal Msrp As Integer) As String
        Dim xmlOut As String = ""

        Try
            Dim webRequest As New NadaVehicleWs.GetVehicleValuesByMsrpRequest
            Dim webResponse As NadaVehicleWs.VehicleValue_Struc
            webRequest.Token = strToken
            webRequest.Period = _NadaPeriod
            webRequest.VehicleType = _NadaVehicleType
            webRequest.Vin = Vin
            webRequest.Mileage = Mileage
            webRequest.Region = Region
            webRequest.Msrp = Msrp

            webResponse = _wsVehicle.getMsrpVehicleAndValueByVin(webRequest)

            If Not webResponse Is Nothing Then
                xmlOut = ConvertVehicleValueStrucToXML(webResponse)
            End If

        Catch soapEx As SoapException
            If soapEx.Message.Contains("No vehicle found") Then
                Return ""
            End If
            ' Throw
        End Try
        'karthi
        Return xmlOut

    End Function
    Public Function getMileageAdj(ByVal strToken As String, ByVal Uid As Integer, ByVal Region As Integer) As String
        Dim xmlOut As String = ""

        Try
            Dim webRequest As New NadaVehicleWs.GetMileageAdjByUidRequest
            Dim webResponse As NadaVehicleWs.Mileage_Struc
            webRequest.Token = strToken
            webRequest.Period = _NadaPeriod
            webRequest.VehicleType = _NadaVehicleType
            webRequest.Uid = Uid
            webRequest.Region = Region

            webResponse = _wsVehicle.getMileageAdj(webRequest)

            If Not webResponse Is Nothing Then
                xmlOut = ConvertMileageStrucToXML(webResponse)
            End If

        Catch ex As SoapException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

        Return xmlOut

    End Function
    Public Function getTotalAdjFloorValues(ByVal strToken As String, ByVal Uid As Integer, ByVal Region As Integer, ByVal Mileage As Integer) As String
        Dim xmlOut As String = ""

        Try
            Dim webRequest As New NadaVehicleWs.GetVehicleValuesByUidRequest
            Dim webResponse As NadaVehicleWs.FloorValue_Struc
            webRequest.Token = strToken
            webRequest.Period = _NadaPeriod
            webRequest.VehicleType = _NadaVehicleType
            webRequest.Uid = Uid
            webRequest.Region = Region
            webRequest.Mileage = Mileage

            webResponse = _wsVehicle.getTotalAdjFloorValues(webRequest)

            If Not webResponse Is Nothing Then
                xmlOut = ConvertTotalAdjFloorStrucToXML(webResponse)
            End If

        Catch ex As SoapException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

        Return xmlOut

    End Function


    Public Function getInclusiveAccessories(ByVal strToken As String, ByVal vehicleUid As Integer) As String
        Dim xmlOut As String = ""

        Try
            Dim webRequest As New NadaVehicleWs.GetMutualAccessoriesRequest
            Dim webResponse() As NadaVehicleWs.AccessoryPakgIncl_Struc
            webRequest.Token = strToken
            webRequest.Period = _NadaPeriod
            webRequest.VehicleType = _NadaVehicleType
            webRequest.Uid = vehicleUid

            webResponse = _wsVehicle.getInclusiveAccessories(webRequest)


            If Not webResponse Is Nothing Then
                xmlOut = ConvertInclusiveAccessStrucToXML(webResponse)
            End If

        Catch ex As SoapException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

        Return xmlOut

    End Function

    Public Function getExclusiveAccessories(ByVal strToken As String, ByVal intUid As Integer) As String
        Dim xmlOut As String = ""

        Try
            Dim seriesRequest As New NadaVehicleWs.GetMutualAccessoriesRequest
            Dim strctAccessory() As NadaVehicleWs.AccessoryMultExcl_Struc
            seriesRequest.Token = strToken
            seriesRequest.Period = _NadaPeriod
            seriesRequest.VehicleType = _NadaVehicleType
            seriesRequest.Uid = intUid


            strctAccessory = _wsVehicle.getExclusiveAccessories(seriesRequest)

            If Not strctAccessory Is Nothing Then
                xmlOut = ConvertExcAccessStrucToXML(strctAccessory)
            End If

        Catch ex As SoapException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

        Return xmlOut

    End Function

    Public Function getExperianAutoCheckReport(ByVal strToken As String, ByVal Vin As String, ByVal ExperianAccountNumber As String, ByVal ReportType As String) As String
        Dim xmlOut As String = ""

        Try
            Dim seriesRequest As New NadaVehicleWs.GetExperianAutoCheckRequest
            ' Dim strctAccessory As String
            seriesRequest.Token = strToken
            seriesRequest.Vin = Vin
            If ReportType.Trim.ToUpper = "SUMMARY" Then
                seriesRequest.ReportType = NadaVehicleWs.ExperianAutoCheckReportTypes.Summary
            Else
                seriesRequest.ReportType = NadaVehicleWs.ExperianAutoCheckReportTypes.VehicleHistory
            End If

            seriesRequest.ExperianAccountNumber = ExperianAccountNumber


            xmlOut = _wsVehicle.getExperianAutoCheckReport(seriesRequest)



        Catch ex As SoapException
            Throw ex
        Catch ex As Exception
            Throw ex
        End Try

        Return xmlOut

    End Function


    Private Function ConvertMileageStrucToXML(ByVal VehicleAccessStrucIn As NadaVehicleWs.Mileage_Struc) As String
        Dim strXmlOut As String = ""
        Try

            strXmlOut = "<Xml><getMileageAdjResult>"
            strXmlOut = strXmlOut + "<MaxAmount><![CDATA[" + VehicleAccessStrucIn.MaxAmount.ToString + "]]></MaxAmount>"
            strXmlOut = strXmlOut + "<MinAmount><![CDATA[" + VehicleAccessStrucIn.MinAmount.ToString + "]]></MinAmount>"
            strXmlOut = strXmlOut + "<AverageMileage><![CDATA[" + VehicleAccessStrucIn.AverageMileage.ToString + "]]></AverageMileage>"
            strXmlOut = strXmlOut + "<Count><![CDATA[" + VehicleAccessStrucIn.Count.ToString + "]]></Count>"
            strXmlOut = strXmlOut + "<MileageTable>"

            If Not VehicleAccessStrucIn.MileageTable Is Nothing Then
                For iLoopVar = 0 To VehicleAccessStrucIn.MileageTable.Length - 1
                    strXmlOut = strXmlOut + "<MileageTable_Struc "
                    strXmlOut = strXmlOut + "base='" + VehicleAccessStrucIn.MileageTable(iLoopVar).AdjAmount.ToString + "' "
                    strXmlOut = strXmlOut + "LowLevel='" + VehicleAccessStrucIn.MileageTable(iLoopVar).LowLevel.ToString + "' "
                    strXmlOut = strXmlOut + "mile='" + VehicleAccessStrucIn.MileageTable(iLoopVar).HighLevel.ToString + "' "
                    strXmlOut = strXmlOut + "variable='" + VehicleAccessStrucIn.MileageTable(iLoopVar).RatePerMile.ToString + "' />"
                    'strXmlOut = strXmlOut + "</MileageTable_Struc>"
                Next
            End If

            strXmlOut = strXmlOut + "</MileageTable>"
            strXmlOut = strXmlOut + "</getMileageAdjResult></Xml>"

        Catch ex As Exception
            Throw
        End Try

        Return strXmlOut
    End Function



    Private Function ConvertInclusiveAccessStrucToXML(ByVal VehicleAccessStrucIn() As NadaVehicleWs.AccessoryPakgIncl_Struc) As String
        Dim strXmlOut As String = ""
        Try
            Dim dtOut As New DataTable("InclusiveAccessStruct")

            Dim col1 As New DataColumn("AccCode", System.Type.GetType("System.String"))
            Dim col2 As New DataColumn("AccPakgInclCode", System.Type.GetType("System.String"))

            dtOut.Columns.Add(col1)
            dtOut.Columns.Add(col2)

            For i As Integer = 0 To VehicleAccessStrucIn.Count - 1

                Dim newRow As DataRow = dtOut.NewRow
                newRow("AccCode") = VehicleAccessStrucIn(i).AccCode
                newRow("AccPakgInclCode") = VehicleAccessStrucIn(i).AccPakgInclCode
                dtOut.Rows.Add(newRow)
            Next

            Dim dsOut As New DataSet
            dsOut.Tables.Add(dtOut)

            strXmlOut = dsOut.GetXml

        Catch ex As Exception
            Throw
        End Try

        Return strXmlOut
    End Function

    Private Function ConvertVehicleAccessoriesStrucToXML(ByVal VehicleAccessStrucIn() As NadaVehicleWs.Accessory_Struc) As String
        Dim strXmlOut As String = ""
        Try
            Dim dtOut As New DataTable("VehicleAccessStruc")

            Dim col1 As New DataColumn("Uid", System.Type.GetType("System.String"))
            Dim col2 As New DataColumn("AccDesc", System.Type.GetType("System.String"))
            Dim col3 As New DataColumn("AccCode", System.Type.GetType("System.String"))
            Dim col4 As New DataColumn("TradeIn", System.Type.GetType("System.Decimal"))
            Dim col5 As New DataColumn("Retail", System.Type.GetType("System.Decimal"))
            Dim col6 As New DataColumn("Loan", System.Type.GetType("System.Decimal"))
            Dim col7 As New DataColumn("IsIncluded", System.Type.GetType("System.String"))
            Dim col8 As New DataColumn("IsAdded", System.Type.GetType("System.String"))

            dtOut.Columns.Add(col1)
            dtOut.Columns.Add(col2)
            dtOut.Columns.Add(col3)
            dtOut.Columns.Add(col4)
            dtOut.Columns.Add(col5)
            dtOut.Columns.Add(col6)
            dtOut.Columns.Add(col7)
            dtOut.Columns.Add(col8)



            For i As Integer = 0 To VehicleAccessStrucIn.Count - 1

                Dim newRow As DataRow = dtOut.NewRow

                newRow("Uid") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn(i).Uid)
                newRow("AccDesc") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn(i).AccDesc)
                newRow("AccCode") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn(i).AccCode)
                newRow("TradeIn") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn(i).TradeIn)
                newRow("Retail") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn(i).Retail)
                newRow("Loan") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn(i).Loan)
                newRow("IsIncluded") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn(i).IsIncluded)
                newRow("IsAdded") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn(i).IsAdded)
                dtOut.Rows.Add(newRow)

            Next


            Dim dsOut As New DataSet
            dsOut.Tables.Add(dtOut)

            strXmlOut = dsOut.GetXml

        Catch ex As Exception
            Throw
        End Try

        Return strXmlOut
    End Function
    Private Function ConvertExcAccessStrucToXML(ByVal VehicleAccessStrucIn() As NadaVehicleWs.AccessoryMultExcl_Struc) As String
        Dim strXmlOut As String = ""
        Try
            Dim dtOut As New DataTable("ExclusiveAccessStruc")

            Dim col1 As New DataColumn("AccCode", System.Type.GetType("System.String"))
            Dim col2 As New DataColumn("AccMultExclCode", System.Type.GetType("System.String"))

            dtOut.Columns.Add(col1)
            dtOut.Columns.Add(col2)

            For i As Integer = 0 To VehicleAccessStrucIn.Count - 1

                Dim newRow As DataRow = dtOut.NewRow

                newRow("AccCode") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn(i).AccCode)
                newRow("AccMultExclCode") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn(i).AccMultExclCode)

                dtOut.Rows.Add(newRow)
            Next

            Dim dsOut As New DataSet
            dsOut.Tables.Add(dtOut)

            strXmlOut = dsOut.GetXml

        Catch ex As Exception
            Throw
        End Try

        Return strXmlOut
    End Function

    Private Function ConvertVehicleByUidValueStrucToXML(ByVal VehicleAccessStrucIn As NadaVehicleWs.Value_Struc) As String
        Dim strXmlOut As String = ""
        Try
            Dim dtOut As New DataTable("VehicleByUidStruc")

            Dim col1 As New DataColumn("Uid", System.Type.GetType("System.String"))
            Dim col2 As New DataColumn("Msrp", System.Type.GetType("System.Decimal"))
            Dim col3 As New DataColumn("AvgMileage", System.Type.GetType("System.Int32"))
            Dim col4 As New DataColumn("MileageAdj", System.Type.GetType("System.Decimal"))
            Dim col5 As New DataColumn("Retail", System.Type.GetType("System.Decimal"))
            Dim col6 As New DataColumn("TradeIn", System.Type.GetType("System.Decimal"))
            Dim col7 As New DataColumn("Loan", System.Type.GetType("System.Decimal"))
            Dim col8 As New DataColumn("MileAdjRetail", System.Type.GetType("System.Decimal"))
            Dim col9 As New DataColumn("MileAdjTradeIn", System.Type.GetType("System.Decimal"))
            Dim col10 As New DataColumn("MileAdjLoan", System.Type.GetType("System.Decimal"))
            Dim col11 As New DataColumn("AvgTradeIn", System.Type.GetType("System.Decimal"))

            Dim col12 As New DataColumn("RoughTradeIn", System.Type.GetType("System.Decimal"))
            Dim col13 As New DataColumn("MileAdjAvgTradeIn", System.Type.GetType("System.Decimal"))
            Dim col14 As New DataColumn("MileAdjRoughTradeIn", System.Type.GetType("System.Decimal"))


            dtOut.Columns.Add(col1)
            dtOut.Columns.Add(col2)
            dtOut.Columns.Add(col3)
            dtOut.Columns.Add(col4)
            dtOut.Columns.Add(col5)
            dtOut.Columns.Add(col6)
            dtOut.Columns.Add(col7)
            dtOut.Columns.Add(col8)
            dtOut.Columns.Add(col9)
            dtOut.Columns.Add(col10)
            dtOut.Columns.Add(col11)
            dtOut.Columns.Add(col12)
            dtOut.Columns.Add(col13)
            dtOut.Columns.Add(col14)

            Dim newRow As DataRow = dtOut.NewRow

            newRow("Uid") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.Uid)
            newRow("Msrp") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.Msrp)
            newRow("AvgMileage") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.AveMileage)
            newRow("MileageAdj") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.MileageAdj)
            newRow("Retail") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.Retail)
            newRow("TradeIn") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.TradeIn)
            newRow("Loan") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.Loan)
            newRow("MileAdjRetail") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.MileAdjRetail)
            newRow("MileAdjTradeIn") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.MileAdjTradeIn)
            newRow("MileAdjLoan") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.MileAdjLoan)
            newRow("AvgTradeIn") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.AvgTradeIn)
            newRow("RoughTradeIn") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.RoughTradeIn)
            newRow("MileAdjAvgTradeIn") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.MileAdjAvgTradeIn)
            newRow("MileAdjRoughTradeIn") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.MileAdjRoughTradeIn)
            dtOut.Rows.Add(newRow)

            ' Next


            Dim dsOut As New DataSet
            dsOut.Tables.Add(dtOut)

            strXmlOut = dsOut.GetXml

        Catch ex As Exception
            Throw
        End Try

        Return strXmlOut
    End Function
    Private Function ConvertBaseVehicleValueStrucToXML(ByVal VehicleAccessStrucIn As NadaVehicleWs.StandardValue_Struc) As String
        Dim strXmlOut As String = ""
        Try
            Dim dtOut As New DataTable("VehicleAccessStruc")

            Dim col1 As New DataColumn("Uid", System.Type.GetType("System.String"))
            Dim col2 As New DataColumn("Retail", System.Type.GetType("System.Decimal"))
            Dim col3 As New DataColumn("TradeIn", System.Type.GetType("System.Decimal"))
            Dim col4 As New DataColumn("Loan", System.Type.GetType("System.Decimal"))
            Dim col5 As New DataColumn("RoughTradeIn", System.Type.GetType("System.Decimal"))
            Dim col6 As New DataColumn("AvgTradeIn", System.Type.GetType("System.Decimal"))


            dtOut.Columns.Add(col1)
            dtOut.Columns.Add(col2)
            dtOut.Columns.Add(col3)
            dtOut.Columns.Add(col4)
            dtOut.Columns.Add(col5)
            dtOut.Columns.Add(col6)

            Dim newRow As DataRow = dtOut.NewRow

            newRow("Uid") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.Uid)
            newRow("Retail") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.Retail)
            newRow("TradeIn") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.TradeIn)
            newRow("Loan") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.Loan)
            newRow("RoughTradeIn") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.RoughTradeIn)
            newRow("AvgTradeIn") = System.Security.SecurityElement.Escape(VehicleAccessStrucIn.AvgTradeIn)

            dtOut.Rows.Add(newRow)

            ' Next


            Dim dsOut As New DataSet
            dsOut.Tables.Add(dtOut)

            strXmlOut = dsOut.GetXml

        Catch ex As Exception
            Throw
        End Try

        Return strXmlOut
    End Function



    Private Function ConvertVehicleValueStrucToXML(ByVal VehicleValueStrucIn As NadaVehicleWs.VehicleValue_Struc) As String
        Dim strXmlOut As String = ""
        Try
            Dim dtOut As New DataTable("VehicleValueStruc")

            Dim col1 As New DataColumn("Uid", System.Type.GetType("System.String"))
            Dim col2 As New DataColumn("VehicleYear", System.Type.GetType("System.Int32"))
            Dim col3 As New DataColumn("MakeCode", System.Type.GetType("System.Int32"))
            Dim col4 As New DataColumn("MakeDescr", System.Type.GetType("System.String"))
            Dim col5 As New DataColumn("SeriesCode", System.Type.GetType("System.Int32"))
            Dim col6 As New DataColumn("SeriesDescr", System.Type.GetType("System.String"))
            Dim col7 As New DataColumn("BodyCode", System.Type.GetType("System.Int32"))
            Dim col8 As New DataColumn("BodyDescr", System.Type.GetType("System.String"))
            Dim col9 As New DataColumn("Weight", System.Type.GetType("System.Int32"))
            Dim col10 As New DataColumn("Msrp", System.Type.GetType("System.Decimal"))
            Dim col11 As New DataColumn("AvgMileage", System.Type.GetType("System.Int32"))
            Dim col12 As New DataColumn("MileageAdj", System.Type.GetType("System.Decimal"))
            Dim col13 As New DataColumn("TradeIn", System.Type.GetType("System.Decimal"))
            Dim col14 As New DataColumn("TradeInPlusVinAcc", System.Type.GetType("System.Decimal"))
            Dim col15 As New DataColumn("TradeInPlusVinAccMileage", System.Type.GetType("System.Decimal"))
            Dim col16 As New DataColumn("Loan", System.Type.GetType("System.Decimal"))
            Dim col17 As New DataColumn("LoanPlusVinAcc", System.Type.GetType("System.Decimal"))
            Dim col18 As New DataColumn("LoanPlusVinAccMileage", System.Type.GetType("System.Decimal"))
            Dim col19 As New DataColumn("Retail", System.Type.GetType("System.Decimal"))
            Dim col20 As New DataColumn("RetailPlusVinAcc", System.Type.GetType("System.Decimal"))
            Dim col21 As New DataColumn("RetailPlusVinAccMileage", System.Type.GetType("System.Decimal"))
            Dim col22 As New DataColumn("AvgTradeIn", System.Type.GetType("System.Decimal"))
            Dim col23 As New DataColumn("AvgTradeInPlusVinAcc", System.Type.GetType("System.Decimal"))
            Dim col24 As New DataColumn("AvgTradeInPlusVinAccMileage", System.Type.GetType("System.Decimal"))
            Dim col25 As New DataColumn("RoughTradeIn", System.Type.GetType("System.Decimal"))
            Dim col26 As New DataColumn("RoughTradeInPlusVinAcc", System.Type.GetType("System.Decimal"))
            Dim col27 As New DataColumn("RoughTradeInPlusVinAccMileage", System.Type.GetType("System.Decimal"))

            dtOut.Columns.Add(col1)
            dtOut.Columns.Add(col2)
            dtOut.Columns.Add(col3)
            dtOut.Columns.Add(col4)
            dtOut.Columns.Add(col5)
            dtOut.Columns.Add(col6)
            dtOut.Columns.Add(col7)
            dtOut.Columns.Add(col8)
            dtOut.Columns.Add(col9)
            dtOut.Columns.Add(col10)
            dtOut.Columns.Add(col11)
            dtOut.Columns.Add(col12)
            dtOut.Columns.Add(col13)
            dtOut.Columns.Add(col14)
            dtOut.Columns.Add(col15)
            dtOut.Columns.Add(col16)
            dtOut.Columns.Add(col17)
            dtOut.Columns.Add(col18)
            dtOut.Columns.Add(col19)
            dtOut.Columns.Add(col20)
            dtOut.Columns.Add(col21)
            dtOut.Columns.Add(col22)
            dtOut.Columns.Add(col23)
            dtOut.Columns.Add(col24)
            dtOut.Columns.Add(col25)
            dtOut.Columns.Add(col26)
            dtOut.Columns.Add(col27)

            ' For i As Integer = 0 To VehicleValueStrucIn.Count - 1

            Dim newRow As DataRow = dtOut.NewRow

            newRow("Uid") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.Uid)
            newRow("VehicleYear") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.VehicleYear)
            newRow("MakeCode") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.MakeCode)
            newRow("MakeDescr") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.MakeDescr)
            newRow("SeriesCode") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.SeriesCode)
            newRow("SeriesDescr") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.SeriesDescr)
            newRow("BodyCode") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.BodyCode)
            newRow("BodyDescr") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.BodyDescr)
            newRow("Weight") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.Weight)
            newRow("Msrp") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.Msrp)
            newRow("AvgMileage") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.AveMileage)
            newRow("MileageAdj") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.MileageAdj)
            newRow("TradeIn") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.TradeIn)
            newRow("TradeInPlusVinAcc") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.TradeInPlusVinAcc)
            newRow("TradeInPlusVinAccMileage") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.TradeInPlusVinAccMileage)
            newRow("Loan") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.Loan)
            newRow("LoanPlusVinAcc") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.LoanPlusVinAcc)
            newRow("LoanPlusVinAccMileage") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.LoanPlusVinAccMileage)
            newRow("Retail") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.Retail)
            newRow("RetailPlusVinAcc") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.RetailPlusVinAcc)
            newRow("RetailPlusVinAccMileage") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.RetailPlusVinAccMileage)
            newRow("AvgTradein") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.AvgTradeIn)
            newRow("AvgTradeinPlusVinAcc") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.AvgTradeInPlusVinAcc)
            newRow("AvgTradeinPlusVinAccMileage") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.AvgTradeInPlusVinAccMileage)
            newRow("RoughTradeIn") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.RoughTradeIn)
            newRow("RoughTradeInPlusVinAcc") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.RoughTradeInPlusVinAcc)
            newRow("RoughTradeInPlusVinAccMileage") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.RoughTradeInPlusVinAccMileage)

            dtOut.Rows.Add(newRow)

            ' Next


            Dim dsOut As New DataSet
            dsOut.Tables.Add(dtOut)

            strXmlOut = dsOut.GetXml

        Catch ex As Exception
            Throw
        End Try

        Return strXmlOut
    End Function

    Private Function ConvertVehicleStrucToXML(ByVal VehicleStrucIn() As NadaVehicleWs.Vehicle_Struc) As String
        Dim strXmlOut As String = ""
        Try
            Dim dtOut As New DataTable("VehicleStruc")

            Dim col1 As New DataColumn("Uid", System.Type.GetType("System.String"))
            Dim col2 As New DataColumn("Year", System.Type.GetType("System.Int32"))
            Dim col3 As New DataColumn("MakeCode", System.Type.GetType("System.Int32"))
            Dim col4 As New DataColumn("MakeDescr", System.Type.GetType("System.String"))
            Dim col5 As New DataColumn("SeriesCode", System.Type.GetType("System.Int32"))
            Dim col6 As New DataColumn("SeriesDescr", System.Type.GetType("System.String"))
            Dim col7 As New DataColumn("BodyCode", System.Type.GetType("System.Int32"))
            Dim col8 As New DataColumn("BodyDescr", System.Type.GetType("System.String"))
            Dim col9 As New DataColumn("Weight", System.Type.GetType("System.Int32"))
            Dim col10 As New DataColumn("MileageClass", System.Type.GetType("System.Int32"))
            dtOut.Columns.Add(col1)
            dtOut.Columns.Add(col2)
            dtOut.Columns.Add(col3)
            dtOut.Columns.Add(col4)
            dtOut.Columns.Add(col5)
            dtOut.Columns.Add(col6)
            dtOut.Columns.Add(col7)
            dtOut.Columns.Add(col8)
            dtOut.Columns.Add(col9)
            dtOut.Columns.Add(col10)


            For i As Integer = 0 To VehicleStrucIn.Count - 1

                Dim newRow As DataRow = dtOut.NewRow

                newRow("Uid") = System.Security.SecurityElement.Escape(VehicleStrucIn(i).Uid)
                newRow("Year") = System.Security.SecurityElement.Escape(VehicleStrucIn(i).Year)
                newRow("MakeCode") = System.Security.SecurityElement.Escape(VehicleStrucIn(i).MakeCode)
                newRow("MakeDescr") = System.Security.SecurityElement.Escape(VehicleStrucIn(i).MakeDescr)
                newRow("SeriesCode") = System.Security.SecurityElement.Escape(VehicleStrucIn(i).SeriesCode)
                newRow("SeriesDescr") = System.Security.SecurityElement.Escape(VehicleStrucIn(i).SeriesDescr)
                newRow("BodyCode") = System.Security.SecurityElement.Escape(VehicleStrucIn(i).BodyCode)
                newRow("BodyDescr") = System.Security.SecurityElement.Escape(VehicleStrucIn(i).BodyDescr)
                newRow("Weight") = System.Security.SecurityElement.Escape(VehicleStrucIn(i).Weight)
                newRow("MileageClass") = System.Security.SecurityElement.Escape(VehicleStrucIn(i).MileageClass)

                dtOut.Rows.Add(newRow)

            Next


            Dim dsOut As New DataSet
            dsOut.Tables.Add(dtOut)

            strXmlOut = dsOut.GetXml

        Catch ex As Exception
            Throw
        End Try

        Return strXmlOut
    End Function
    Private Function ConvertVehicleStrucToArray(ByVal VehicleStrucIn() As NadaVehicleWs.Vehicle_Struc) As String
        Dim strXmlOut As String = ""
        Dim returnString As New StringBuilder
        Try
            For i As Integer = 0 To VehicleStrucIn.Count - 1

                If returnString.Length > 0 Then
                    returnString.Append(",")
                End If
                returnString.Append(VehicleStrucIn(i).Uid)
                returnString.Append(",")
                returnString.Append(VehicleStrucIn(i).Year)
                returnString.Append(",")
                returnString.Append(String.Concat(VehicleStrucIn(i).MakeDescr, "|", VehicleStrucIn(i).MakeCode))
                returnString.Append(",")
                returnString.Append(String.Concat(VehicleStrucIn(i).SeriesDescr, "|", VehicleStrucIn(i).SeriesCode))
                returnString.Append(",")
                returnString.Append(String.Concat(VehicleStrucIn(i).BodyDescr, "|", VehicleStrucIn(i).BodyCode))
                returnString.Append(",")

            Next
            Return returnString.ToString




        Catch ex As Exception
            Throw
        End Try

        Return strXmlOut
    End Function
    Private Function ConvertSingleVehicleStrucToXML(ByVal VehicleStrucIn As NadaVehicleWs.Vehicle_Struc) As String
        Dim strXmlOut As String = ""
        Try
            Dim dtOut As New DataTable("VehicleStruc")

            Dim col1 As New DataColumn("Uid", System.Type.GetType("System.String"))
            Dim col2 As New DataColumn("Year", System.Type.GetType("System.Int32"))
            Dim col3 As New DataColumn("MakeCode", System.Type.GetType("System.Int32"))
            Dim col4 As New DataColumn("MakeDescr", System.Type.GetType("System.String"))
            Dim col5 As New DataColumn("SeriesCode", System.Type.GetType("System.Int32"))
            Dim col6 As New DataColumn("SeriesDescr", System.Type.GetType("System.String"))
            Dim col7 As New DataColumn("BodyCode", System.Type.GetType("System.Int32"))
            Dim col8 As New DataColumn("BodyDescr", System.Type.GetType("System.String"))
            Dim col9 As New DataColumn("Weight", System.Type.GetType("System.Int32"))
            Dim col10 As New DataColumn("MileageClass", System.Type.GetType("System.Int32"))
            Dim col11 As New DataColumn("Gvw", System.Type.GetType("System.Int32"))
            Dim col12 As New DataColumn("Gcw", System.Type.GetType("System.Int32"))

            dtOut.Columns.Add(col1)
            dtOut.Columns.Add(col2)
            dtOut.Columns.Add(col3)
            dtOut.Columns.Add(col4)
            dtOut.Columns.Add(col5)
            dtOut.Columns.Add(col6)
            dtOut.Columns.Add(col7)
            dtOut.Columns.Add(col8)
            dtOut.Columns.Add(col9)
            dtOut.Columns.Add(col10)
            dtOut.Columns.Add(col11)
            dtOut.Columns.Add(col12)


            ' For i As Integer = 0 To VehicleStrucIn.Count - 1

            Dim newRow As DataRow = dtOut.NewRow

            newRow("Uid") = System.Security.SecurityElement.Escape(VehicleStrucIn.Uid)
            newRow("Year") = System.Security.SecurityElement.Escape(VehicleStrucIn.Year)
            newRow("MakeCode") = System.Security.SecurityElement.Escape(VehicleStrucIn.MakeCode)
            newRow("MakeDescr") = System.Security.SecurityElement.Escape(VehicleStrucIn.MakeDescr)
            newRow("SeriesCode") = System.Security.SecurityElement.Escape(VehicleStrucIn.SeriesCode)
            newRow("SeriesDescr") = System.Security.SecurityElement.Escape(VehicleStrucIn.SeriesDescr)
            newRow("BodyCode") = System.Security.SecurityElement.Escape(VehicleStrucIn.BodyCode)
            newRow("BodyDescr") = System.Security.SecurityElement.Escape(VehicleStrucIn.BodyDescr)
            newRow("Weight") = System.Security.SecurityElement.Escape(VehicleStrucIn.Weight)
            newRow("MileageClass") = System.Security.SecurityElement.Escape(VehicleStrucIn.MileageClass)
            newRow("Gvw") = System.Security.SecurityElement.Escape(VehicleStrucIn.Gvw)
            newRow("Gcw") = System.Security.SecurityElement.Escape(VehicleStrucIn.Gcw)

            dtOut.Rows.Add(newRow)

            'Next


            Dim dsOut As New DataSet
            dsOut.Tables.Add(dtOut)

            strXmlOut = dsOut.GetXml

        Catch ex As Exception
            Throw
        End Try

        Return strXmlOut
    End Function

    Private Function ConvertTotalAdjFloorStrucToXML(ByVal VehicleValueStrucIn As NadaVehicleWs.FloorValue_Struc) As String
        Dim strXmlOut As String = ""
        Try
            Dim dtOut As New DataTable("FloorValueStruc")

            Dim col1 As New DataColumn("Uid", System.Type.GetType("System.String"))
            Dim col2 As New DataColumn("AdjustedRetail", System.Type.GetType("System.Decimal"))
            Dim col3 As New DataColumn("AdjustedTradeIn", System.Type.GetType("System.Decimal"))
            Dim col4 As New DataColumn("AdjustedLoan", System.Type.GetType("System.Decimal"))
            Dim col5 As New DataColumn("AdjustedRoughTradeIn", System.Type.GetType("System.Decimal"))
            Dim col6 As New DataColumn("AdjustedAvgTradeIn", System.Type.GetType("System.Decimal"))


            dtOut.Columns.Add(col1)
            dtOut.Columns.Add(col2)
            dtOut.Columns.Add(col3)
            dtOut.Columns.Add(col4)
            dtOut.Columns.Add(col5)
            dtOut.Columns.Add(col6)

            ' For i As Integer = 0 To VehicleValueStrucIn.Count - 1

            Dim newRow As DataRow = dtOut.NewRow

            newRow("Uid") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.Uid)
            newRow("AdjustedRetail") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.AdjustedRetail)
            newRow("AdjustedTradeIn") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.AdjustedTradeIn)
            newRow("AdjustedLoan") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.AdjustedLoan)
            newRow("AdjustedRoughTradeIn") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.AdjustedRoughTradeIn)
            newRow("AdjustedAvgTradeIn") = System.Security.SecurityElement.Escape(VehicleValueStrucIn.AdjustedAvgTradeIn)

            dtOut.Rows.Add(newRow)

            Dim dsOut As New DataSet
            dsOut.Tables.Add(dtOut)

            strXmlOut = dsOut.GetXml

        Catch ex As Exception
            Throw
        End Try

        Return strXmlOut
    End Function

    Private Function ConvertLookupStrucToXML(ByVal LookupStrucIn() As NadaVehicleWs.Lookup_Struc) As String
        Dim strXmlOut As String = ""
        Try
            Dim dtOut As New DataTable("LookupStruc")

            Dim colCode As New DataColumn("Code", System.Type.GetType("System.Int32"))
            Dim colDescr As New DataColumn("Description", System.Type.GetType("System.String"))
            dtOut.Columns.Add(colCode)
            dtOut.Columns.Add(colDescr)

            For i As Integer = 0 To LookupStrucIn.Count - 1

                Dim newRow As DataRow = dtOut.NewRow

                newRow("Code") = System.Security.SecurityElement.Escape(LookupStrucIn(i).Code)
                newRow("Description") = System.Security.SecurityElement.Escape(LookupStrucIn(i).Descr)

                dtOut.Rows.Add(newRow)

            Next


            Dim dsOut As New DataSet
            dsOut.Tables.Add(dtOut)

            strXmlOut = dsOut.GetXml

        Catch ex As Exception
            Throw
        End Try

        Return strXmlOut
    End Function

    Private Function ConvertLookupStrucToStringArray(ByVal LookupStrucIn() As NadaVehicleWs.Lookup_Struc) As String
        Dim strXmlOut As String = ""
        Dim returnString As New StringBuilder


        For i As Integer = 0 To LookupStrucIn.Count - 1

            If returnString.Length > 0 Then
                returnString.Append("," + LookupStrucIn(i).Descr + "|" + CStr(LookupStrucIn(i).Code))
            Else
                returnString.Append(LookupStrucIn(i).Descr + "|" + CStr(LookupStrucIn(i).Code))
            End If

        Next
        Return returnString.ToString

        ' Return strXmlOut
    End Function

    Private Sub InitializeVehicleWebService()
        'Try
        _wsVehicle = New NadaVehicleWs.Vehicle
        'Catch ex As Exception

        'End Try

    End Sub


    Public Function GetAllAsXML(ByVal strTokenId As String, ByVal strRegion As String, ByVal strRegionDesc As String, ByVal strYear As String, _
                            ByVal strMake As String, ByVal strModel As String, ByVal strBody As String, _
                            ByVal strMileage As String, ByVal strVIN As String, ByVal strVehId As String, _
                            ByVal strAction As String, ByVal strZip As String, ByVal strReadOnly As String, ByVal strRegionListArray As String, _
                            Optional ByVal strVehicleDBDate As String = "", Optional ByVal strPeriod As String = "", _
                            Optional ByVal blnXformOPtoHTML As Boolean = True) As String

        ' Const PROC_NAME As String = MODULE_NAME & "GetAllAsXML"

        'On Error GoTo errHandler
        Try
            'Dim varVics As Object
            'Dim varRegion As Object
            'Dim vYear As Object
            'Dim vMake As Object
            'Dim vModel As Object
            'Dim vBody As Object
            'Dim bInvalidVIN As Boolean
            'Dim blnDebugMode As Boolean
            Dim bRegionFound As Boolean
            Dim bYearFound As Boolean
            Dim bMakeFound As Boolean
            Dim bModelFound As Boolean
            Dim bBodyFound As Boolean
            Dim strStateAbbrev As String
            Dim strCounty As String
            'Dim lngRegion As Long

            Dim lookUpYear As String = ""
            Dim lookUpMake As String = ""
            Dim lookUpModel As String = ""
            Dim lookUpBody As String = ""
            'Dim i As Integer
            Dim strXml As New StringBuilder
            Dim strID As String = strVehId
            'Dim varData As Object

            Dim lookUpMSRP As String = ""
            Dim lookUpTrade As String = ""
            Dim lookUpLoan As String = ""
            Dim lookUpRetail As String = ""
            Dim lookUpWeight As String = ""
            Dim regionValue As Integer = 0

            ' Initialize booleans.
            bRegionFound = False
            bYearFound = False
            bMakeFound = False
            bModelFound = False
            bBodyFound = False

            ' Dim bgetaccessories As Boolean
            strXml.Append("<Root Action=""" & strAction & """ ReadOnly=""")
            'If mblnReadOnly Then
            'strXml & "true"""
            'Else
            strXml.Append("false""")
            'End If
            If strVIN.Length > 0 Then
                strXml.Append(" VIN=""" & strVIN & """")
            End If

            ' If strAction = "LookupVIN" Then
            'strXml.Append(" VIN=""" & strVIN & """")
            'End If

            strXml.Append(">")

            '----------------------------- Built Region List Begin---------------------------------------
            Dim regionXml As String = ""
            regionXml = regionXml & "<RegionList>"
            'Get the region list 
            'strRegionListArray

            If strRegionListArray.Trim.Length > 1 Then
                strRegionListArray = strRegionListArray.Replace("amp;", "&")
                For iloopvar As Integer = 0 To strRegionListArray.Split(",").Length - 1
                    regionXml = regionXml & "<Region code='" & strRegionListArray.Split(",")(iloopvar).Split("|")(1) & "'"
                    regionXml = regionXml & ">" & strRegionListArray.Split(",")(iloopvar).Split("|")(0) & "</Region>"
                Next
            Else
                Dim strRegionList As String
                Dim dsRegionList As New DataSet
                strRegionList = GetRegions(strTokenId)
                Dim xmlSR As System.IO.StringReader = New System.IO.StringReader(strRegionList)

                dsRegionList.ReadXml(xmlSR)

                For iloopvar As Integer = 0 To dsRegionList.Tables(0).Rows.Count - 1
                    regionXml = regionXml & "<Region code='" & dsRegionList.Tables(0).Rows(iloopvar).Item("Code").ToString & "'"
                    If strRegionDesc.Trim.Length > 0 Then
                        If String.Compare(dsRegionList.Tables(0).Rows(iloopvar).Item("Description").ToString.Trim.ToUpper, strRegionDesc.Trim.ToUpper) = 0 Then
                            regionXml = regionXml & " selected=''"
                            bRegionFound = True
                            If dsRegionList.Tables(0).Rows(iloopvar).Item("Code").ToString.Trim.Length > 0 Then
                                regionValue = Integer.Parse(dsRegionList.Tables(0).Rows(iloopvar).Item("Code").ToString.Trim)
                            End If
                        End If
                    End If
                    regionXml = regionXml & ">" & dsRegionList.Tables(0).Rows(iloopvar).Item("Description").ToString & "</Region>"
                Next
            End If
            regionXml = regionXml & "</RegionList>"

            '--------------------------------------Built Region List end ------------------------------------------
            ' Vehicle specifics
            '-----------------------------------------------------------------


            strXml.Append("<Vehicle>")
            strXml.Append("<Id>" & strID & "</Id>")
            strXml.Append("<VIN>" & strVIN & "</VIN>")

            ' newRow("Uid") = VehicleValueStrucIn.Uid
            ' dsVehicleDtlsList.Tables(0).Rows(0).Item("Msrp").ToString 


            strXml.Append("<Year>")
            'If vYear Then
            ' strXml & mobjNadaSession.Text(nfYear)
            strXml.Append(strYear)
            'End If

            strXml.Append("</Year>")

            strXml.Append("<Make>")
            ' If vMake Then
            strXml.Append(strMake)
            'End If
            strXml.Append("</Make>")

            strXml.Append("<Model>")
            ' If vModel Then
            strXml.Append(strModel)
            'End If
            strXml.Append("</Model>")

            strXml.Append("<Body>")
            'If vBody Then
            strXml.Append(strBody)
            'End If
            strXml.Append("</Body>")



            strXml.Append("<MSRP>" & lookUpMSRP & "</MSRP>")
            strXml.Append("<Trade>" & lookUpTrade & "</Trade>")
            strXml.Append("<Loan>" & lookUpLoan & "</Loan>")
            strXml.Append("<Retail>" & lookUpRetail & "</Retail>")
            strXml.Append("<Weight>" & lookUpWeight & "</Weight>")
            strXml.Append("<Mileage>" & strMileage & "</Mileage>") 'CalculateMileage
            strXml.Append("</Vehicle>")

            strXml.Append("<NADALossData>")
            strXml.Append("<ZipCode>" & strZip & "</ZipCode>")
            strXml.Append("<State>" & strStateAbbrev & "</State>")
            strXml.Append("<County>" & strCounty & "</County>")
            'Hidden field in Form so commented )
            strXml.Append("<DBDate></DBDate>")
            'In case of Database error - No need in Webservice OR check for webservice not available
            strXml.Append("<DBAlertMsg></DBAlertMsg>")
            strXml.Append("</NADALossData>")


            'Add Region Xml here
            strXml.Append(regionXml)


            ' Year List
            '----------------------------- Built Year List Begin---------------------------------------
            Dim strYearList As String
            Dim dsYearList As New DataSet
            Dim currentYear As String

            Dim DateValue As Date
            DateValue = Now()
            currentYear = Year(DateValue)


            ' Year List
            strXml.Append("<YearList>")

            For iloopvar As Integer = 0 To 40
                strXml.Append("<Year code='" & CInt(currentYear) - iloopvar & "'")
                If strYear.Trim.Length > 0 Then
                    If String.Compare(CInt(currentYear) - iloopvar, strYear.Trim) = 0 Then
                        strXml.Append(" selected=''")
                        bYearFound = True
                    End If
                End If
                strXml.Append(">" & CInt(currentYear) - iloopvar & "</Year>")
            Next


            strXml.Append("</YearList>")

            '--------------------------------------Built Year List end ------------------------------------------
            ' Make List
            '----------------------------- Built Make List Begin---------------------------------------
            Dim strMakeList As String
            Dim dsMakeList As New DataSet
            Dim MakeCode As Integer = 0
            ' Year List
            strXml.Append("<MakeList>")
            '''' Using JQuery to get Makes on change of Model drop down 

            '' ''strMakeList = GetMakes(strTokenId, strYear)

            '' ''Dim xmlMakeSR As System.IO.StringReader = New System.IO.StringReader(strMakeList)

            '' ''dsMakeList.ReadXml(xmlMakeSR)

            '' ''For iloopvar As Integer = 0 To dsMakeList.Tables(0).Rows.Count - 1
            '' ''    strXml.Append("<Make code='" & dsMakeList.Tables(0).Rows(iloopvar).Item("Code").ToString & "'")
            '' ''    If strMake.Trim.Length > 0 Then
            '' ''        If dsMakeList.Tables(0).Rows(iloopvar).Item("Description").ToString.Trim.ToUpper.Contains(strMake.Trim.ToUpper) Then
            '' ''            strXml.Append(" selected=''")
            '' ''            bMakeFound = True
            '' ''            If dsMakeList.Tables(0).Rows(iloopvar).Item("Code").ToString.Trim.Length > 0 Then
            '' ''                MakeCode = Integer.Parse(dsMakeList.Tables(0).Rows(iloopvar).Item("Code"))
            '' ''            End If

            '' ''        End If
            '' ''    End If
            '' ''    strXml.Append(">" & dsMakeList.Tables(0).Rows(iloopvar).Item("Description").ToString & "</Make>")
            '' ''Next

            strXml.Append("</MakeList>")



            '--------------------------------------Built Make List end ------------------------------------------

            '----------------------------- Built Model List Begin---------------------------------------
            ' Model List
            '  strXml = strXml & "<ModelList>"


            Dim strModelList As String
            Dim dsModelList As New DataSet
            Dim ModelCode As Integer = 0
            ' Year List
            strXml.Append("<ModelList>")
            '''' Using JQuery to get Models on change of Make drop down 

            '' ''strModelList = GetModels(strTokenId, strYear, MakeCode)

            '' ''Dim xmlModelSR As System.IO.StringReader = New System.IO.StringReader(strModelList)

            '' ''dsModelList.ReadXml(xmlModelSR)

            '' ''For iloopvar As Integer = 0 To dsModelList.Tables(0).Rows.Count - 1
            '' ''    strXml.Append("<Model code='" & dsModelList.Tables(0).Rows(iloopvar).Item("Code").ToString & "'")
            '' ''    If strModel.Trim.Length > 0 Then
            '' ''        If dsModelList.Tables(0).Rows(iloopvar).Item("Description").ToString.Trim.ToUpper.Contains(strModel.Trim.ToUpper) Then
            '' ''            strXml.Append(" selected=''")
            '' ''            bMakeFound = True
            '' ''            If dsModelList.Tables(0).Rows(iloopvar).Item("Code").ToString.Trim.Length > 0 Then
            '' ''                ModelCode = Integer.Parse(dsModelList.Tables(0).Rows(iloopvar).Item("Code").ToString)
            '' ''            End If
            '' ''        End If
            '' ''    End If
            '' ''    strXml.Append(">" & dsModelList.Tables(0).Rows(iloopvar).Item("Description").ToString & "</Model>")
            '' ''Next

            strXml.Append("</ModelList>")


            '--------------------------------------Built Model List end ------------------------------------------

            ' Body List
            '----------------------------- Built Body List Begin---------------------------------------



            Dim strBodyList As String
            Dim dsBodyList As New DataSet
            ' Year List
            strXml.Append("<BodyList>")
            '''' Using JQuery to get Body on change of Model drop down
            strXml.Append("</BodyList>")
            '--------------------------------------Built Body List end ------------------------------------------


            'Dim varAccsy As Object
            'Dim intCount As Integer

            'Dim strAccessoryList As String
            Dim dsAccessoryList As New DataSet

            strXml.Append("</Root>")

            'strXml.Replace("&", "&amp;")

            Dim oTrasform As New Xsl.XslTransform
            Dim Resp As System.IO.Stream

            Dim myDoc As New System.Xml.XmlDocument
            Dim args As New Xsl.XsltArgumentList

            myDoc.LoadXml(strXml.ToString.Replace("&", "&amp;"))

            Dim txt As New System.IO.MemoryStream
            Dim output As New System.Xml.XmlTextWriter(txt, System.Text.Encoding.UTF8)

            'HttpContext.Current.Server.MapPath("VehicleEvaluator.xslt")

            oTrasform.Load(HttpContext.Current.Server.MapPath("VehicleEvaluator.xslt"))

            oTrasform.Transform(myDoc, args, output, Nothing)

            output.Flush()
            txt.Position = 0

            Dim sr As New IO.StreamReader(txt)
            Return sr.ReadToEnd()
            'textbox1.text = sr.ReadToEnd()

            'Dim xslTransfrom As New Xsl.XslCompiledTransform
            'xslTransfrom.Load(

            '' ''errHandler:
            '' ''        If Err.Number <> 0 Then
            '' ''            mobjEvents.HandleEvent(Err.Number, "[Line " & Erl() & "] " & PROC_NAME & Err.Source, Err.Description, _
            '' ''                    PROC_NAME & " XML=" & strXml & " XSLSheet=" & strXSLSheet)
            '' ''        End If
        Catch ex As Exception
            Return "Exception" & ex.Message & "RegionDesc: " & strRegionListArray
        End Try

    End Function

    Public Function GetAccessoriesList(ByVal strTokenId As String, ByVal strID As String, ByVal strVIN As String, ByVal regionValue As String) As String
        Dim strXml As New StringBuilder
        Dim strAccessoryList As String
        Dim dsAccessoryList As New DataSet


            'strXml.Append("<Root Action="" ReadOnly=""")
            'If mblnReadOnly Then
            'strXml & "true"""
            'Else
            ' strXml.Append("false""")
            'End If

            'strXml.Append(">")

            Dim strVehiclePriceDtlsList As String
            Dim dsVehiclePriceDtlsList As New DataSet


            strVehiclePriceDtlsList = getVehicleAndValueByUid(strTokenId, strID, regionValue, 0) ' CHeck for passing 0 value in Msrp .... 
            Dim xmlVehiclePriceDtls As System.IO.StringReader = New System.IO.StringReader(strVehiclePriceDtlsList)

            dsVehiclePriceDtlsList.ReadXml(xmlVehiclePriceDtls)
            strXml.Append("<Root>")
            strXml.Append("<Vehicle>")
            strXml.Append("<MSRP>" & dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("Msrp").ToString & "</MSRP>")
            strXml.Append("<Trade>" & dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("TradeIn").ToString & "</Trade>")
            strXml.Append("<Loan>" & dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("Loan").ToString & "</Loan>")
            strXml.Append("<Retail>" & dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("Retail").ToString & "</Retail>")
            strXml.Append("<Weight>" & dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("Weight").ToString & "</Weight>")
            strXml.Append("<Mileage>" & dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("AvgMileage").ToString & "</Mileage>")
            strXml.Append("</Vehicle>")


            strXml.Append("<AccessoryData>")

            strAccessoryList = GetAccessories(strTokenId, strID, strVIN, regionValue)


            Dim xmlAccessorySR As System.IO.StringReader = New System.IO.StringReader(strAccessoryList)

            dsAccessoryList.ReadXml(xmlAccessorySR)



            If dsAccessoryList.Tables.Count > 0 Then
                If dsAccessoryList.Tables(0).Rows.Count > 0 Then

                Dim accesories = From e1 In dsAccessoryList.Tables(0) Select status = e1.Item("IsIncluded").ToString, name = e1.Item("AccDesc").ToString, _
                                  TradeValue = e1.Item("TradeIn").ToString, LoanValue = e1.Item("Loan").ToString, Value = e1.Item("Retail").ToString, _
                                      Code = e1.Item("AccCode").ToString Order By name


                    strXml.Append("<AccessoryList count='" & dsAccessoryList.Tables(0).Rows.Count - 1 & "'>")

                    For Each accesory In accesories
                        strXml.Append("<Accessory status='" & accesory.status)
                        strXml.Append("' name='" & accesory.name)
                        strXml.Append("' tradeValue='" & accesory.TradeValue)
                        strXml.Append("' loanValue='" & accesory.LoanValue)
                        strXml.Append("' value='" & accesory.Value)
                        strXml.Append("' code='" & accesory.Code & "'/>")
                    Next

                    'For iloopvar As Integer = 0 To dsAccessoryList.Tables(0).Rows.Count - 1
                    '    strXml.Append("<Accessory status='" & dsAccessoryList.Tables(0).Rows(iloopvar).Item("IsIncluded").ToString)
                    '    strXml.Append("' name='" & dsAccessoryList.Tables(0).Rows(iloopvar).Item("AccDesc").ToString)
                    '    strXml.Append("' tradeValue='" & dsAccessoryList.Tables(0).Rows(iloopvar).Item("TradeIn").ToString)
                    '    strXml.Append("' loanValue='" & dsAccessoryList.Tables(0).Rows(iloopvar).Item("Loan").ToString)
                    '    strXml.Append("' value='" & dsAccessoryList.Tables(0).Rows(iloopvar).Item("Retail").ToString)
                    '    strXml.Append("' code='" & dsAccessoryList.Tables(0).Rows(iloopvar).Item("AccCode").ToString & "'/>")
                    '    'strXml.Append("' code='" & dsAccessoryList.Tables(0).Rows(iloopvar).Item("AccCode").ToString & "'/>")
                    'Next
                    strXml.Append("</AccessoryList>")
                Else
                    strXml.Append("<AccessoryList count=''/>")
                End If
            Else
                strXml.Append("<AccessoryList count=''/>")
            End If



            ' Get the accessory collection.
            ' get accesories values from NADA
            ' varData = varAccsy(eAccessorySet)
            ' intCount = varAccsy(eAccessoryCount)

            '  If Not IsEmpty(varData) Then
            '      strXml = strXml & "<AccessoryList count='" & intCount & "'>"
            'For i =  To intCount - 
            '          strXml = strXml & "<Accessory status='" & CStr(varData(i, eAccStatus))
            '          strXml = strXml & "' name='" & Trim(varData(i, eAccDescription))
            '          strXml = strXml & "' tradeValue='" & CStr(varData(i, iAccTradeValue))
            '          strXml = strXml & "' loanValue='" & CStr(varData(i, iAccLoanValue))
            '          strXml = strXml & "' value='" & CStr(varData(i, iAccValue))
            '          strXml = strXml & "' code='" & CStr(varData(i, iAccCode)) & "'/>"
            '      Next i
            '      strXml = strXml & "</AccessoryList>"
            '  Else
            '      strXml = strXml & "<AccessoryList count=''/>"
            '  End If


            '--------------------------------------Built Accessoires Data End ------------------------------------------
            'Get the Inclusive collection.
            'Get inclusive accessories values --- from nada service 

            ' ----------------- get Inclusive accessories -------------------------------

            Dim strIncAccessList As String
            Dim dsIncAccessList As New DataSet

            'strXml = strXml & "<BodyList>"
            strIncAccessList = getInclusiveAccessories(strTokenId, strID)
            ' karthi 

            Dim xmlIncAccesorySR As System.IO.StringReader = New System.IO.StringReader(strIncAccessList)

            dsIncAccessList.ReadXml(xmlIncAccesorySR)

            If dsIncAccessList.Tables.Count > 0 Then
                If dsIncAccessList.Tables(0).Rows.Count > 0 Then
                    strXml.Append("<PackageInclusive count='" & dsIncAccessList.Tables(0).Rows.Count & "'>")
                    For iLoopvar As Integer = 0 To dsIncAccessList.Tables(0).Rows.Count - 1
                        strXml.Append("<PI other='" & dsIncAccessList.Tables(0).Rows(iLoopvar).Item("AccCode").ToString & "' this='" & dsIncAccessList.Tables(0).Rows(iLoopvar).Item("AccPakgInclCode").ToString & "'/>")
                    Next
                    strXml.Append("</PackageInclusive>")
                Else
                    strXml.Append("<PackageInclusive count=''/>")

                End If
            Else
                strXml.Append("<PackageInclusive count=''/>")
            End If


            ' ----------------- End Inclusive accessories -------------------------------

            ' ----------------- Begin Mutually Exclusive accessories -------------------------------
            '    
            'Get the Exclusive collection.
            'Get exclusive accessories vaues from NADA service

            Dim strExcAccessList As String
            Dim dsExcAccessList As New DataSet

            'strXml & "<BodyList>"
            strExcAccessList = getExclusiveAccessories(strTokenId, strID)

            Dim xmlExcAccesorySR As System.IO.StringReader = New System.IO.StringReader(strExcAccessList)

            dsExcAccessList.ReadXml(xmlExcAccesorySR)

            If dsExcAccessList.Tables.Count > 0 Then
                If dsExcAccessList.Tables(0).Rows.Count > 0 Then
                    strXml.Append("<MutuallyExclusive count='" & dsExcAccessList.Tables(0).Rows.Count & "'>")
                    For iLoopvar As Integer = 0 To dsExcAccessList.Tables(0).Rows.Count - 1
                        strXml.Append("<ME other='" & dsExcAccessList.Tables(0).Rows(iLoopvar).Item("AccCode").ToString & "' this='" & dsExcAccessList.Tables(0).Rows(iLoopvar).Item("AccMultExclCode").ToString & "'/>")
                    Next
                    strXml.Append("</MutuallyExclusive>")
                Else
                    strXml.Append("<MutuallyExclusive count=''/>")

                End If
            Else
                strXml.Append("<MutuallyExclusive count=''/>")
            End If

            ' ----------------- End Mutually Exclusive accessories ---------------------------------- 

            'Get the Mileage range information.
            ' ----------------- Start Mileage range Information ---------------------------------- 

            ' varData = varAccsy(eMileageSet)
            'intCount = varAccsy(eMileageCount)
            Dim strMileageRange As String
            Dim xmlMileage As XElement
            'Dim xmlDoc As New XDocument

            strMileageRange = getMileageAdj(strTokenId, strID, 1) '            regionValue()
            'xmlDoc.Load(strMileageRange)
            Dim xmlDoc As XDocument = XDocument.Parse(strMileageRange)
            'xmlMileage.Parse(strMileageRange)
            'xmlMileage.Load(strMileageRange)

        Dim xml = (From C In xmlDoc.Root.Elements.Elements.Elements _
                       Select C)
            'Select LowLevel = C.Element("LowLevel").ToString, mile = C.Element("HighLevel").ToString, base = C.Element("AdjAmount").ToString, variable = C.Element("RatePerMile").ToString)

            If xml.Count > 0 Then
                strXml.Append("<MileageRange count='" & xml.Count & "'>")
                For Each xmlValues In xml
                    strXml.Append("<MR " + xmlValues.ToString.Replace("<MileageTable_Struc", "").Replace("/>", "").Replace("""", "'") + "></MR>") 'mile='" & xmlValues.mile.ToString & "' base='" & xmlValues.base & "' variable='" & xmlValues.variable.ToString & "'></MR>")
                    'xmlValues.ToString.Replace("<MileageTable_Struc", "").Replace("/>", "").Replace("""", "'")
                Next
                strXml.Append("</MileageRange>")
            Else
                strXml.Append("<MileageRange count=''></MileageRange>")
            End If

            ' ----------------- End Mileage range Information ---------------------------------- 

            strXml.Append("</AccessoryData>")
            strXml.Append("</Root>")
            'Return strXml.ToString


            Return String.Concat(GetHTMLfromXSLT("Accessories.xslt", strXml.ToString.Replace("&", "&amp;")), " ", GetHTMLfromXSLT("VehicleXML.xslt", strXml.ToString.Replace("&", "&amp;")))

    End Function

    Public Function GetHTMLfromXSLT(ByVal xsltFile As String, ByVal xmlText As String) As String

        Dim oTrasform As New Xsl.XslTransform
        Dim Resp As System.IO.Stream

        Dim myDoc As New System.Xml.XmlDocument
        Dim args As New Xsl.XsltArgumentList

        myDoc.LoadXml(xmlText)


        Dim txt As New System.IO.MemoryStream
        Dim output As New System.Xml.XmlTextWriter(txt, System.Text.Encoding.UTF8)

        'HttpContext.Current.Server.MapPath("VehicleEvaluator.xslt")

        oTrasform.Load(HttpContext.Current.Server.MapPath(xsltFile))
        'oTrasform.Load(HttpContext.Current.Server.MapPath("VehicleEvaluator.xslt"))

        oTrasform.Transform(myDoc, args, output, Nothing)

        output.Flush()
        txt.Position = 0

        Dim sr As New IO.StreamReader(txt)
        Dim htmlVal As String = sr.ReadToEnd()
        'sr = Nothing
        Return htmlVal
    End Function

    'Returns Vehicle information for FNOL application
    Public Function FNOLVehicleInfo(ByVal TokenId As String, ByVal FNOLXml As String) As String
        'FNOLXml = "<NADAValuation LossDate='01/15/2012' Region='' LossState='TX' Year='' Make='' Model='' Body='' VIN='1G2NE52F52C197193' Mileage='' Zip='' NADAVehicleId='' Action='LookupVIN' ReadOnly='true' />"
        '1GBHC34T4BV114075
        Dim xmlDoc As XElement
        Dim returnXml As New StringBuilder
        Dim strVehicleID As String = String.Empty
        Dim strYear As String = String.Empty
        Dim strMake As String = String.Empty
        Dim strModel As String = String.Empty
        Dim strBody As String = String.Empty
        Dim strMSRP As String = String.Empty
        Dim strTrade As String = String.Empty
        Dim strLoan As String = String.Empty
        Dim strRetail As String = String.Empty
        Dim strWeight As String = String.Empty
        Dim strMileage As String = String.Empty

        xmlDoc = XElement.Parse(FNOLXml)

        Dim strVehiclePriceDtlsList As String = String.Empty

        Dim dsVehiclePriceDtlsList As New DataSet

        strVehiclePriceDtlsList = getMsrpVehicleAndValueByVin(TokenId, xmlDoc.Attribute("VIN").Value, 1, 0, 0)

        If strVehiclePriceDtlsList.Length > 0 Then
            Dim xmlVehiclePriceDtls As System.IO.StringReader = New System.IO.StringReader(strVehiclePriceDtlsList)
            dsVehiclePriceDtlsList.ReadXml(xmlVehiclePriceDtls)

            If dsVehiclePriceDtlsList.Tables.Count > 0 Then
                If dsVehiclePriceDtlsList.Tables(0).Rows.Count > 0 Then

                    strVehicleID = dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("Uid").ToString
                    strYear = dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("VehicleYear").ToString
                    strMake = dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("MakeDescr").ToString
                    strModel = dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("SeriesDescr").ToString
                    strBody = dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("BodyDescr").ToString
                    strMSRP = dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("Msrp").ToString
                    strTrade = dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("TradeIn").ToString
                    strLoan = dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("Loan").ToString
                    strRetail = dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("Retail").ToString
                    strWeight = dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("Weight").ToString
                    strMileage = dsVehiclePriceDtlsList.Tables(0).Rows(0).Item("AvgMileage").ToString
                End If
            End If
        End If

        returnXml.Append(String.Concat("<Root Action='", xmlDoc.Attribute("Action").Value, "'"))
        returnXml.Append(String.Concat(" ReadOnly='", xmlDoc.Attribute("ReadOnly").Value, "'"))
        returnXml.Append(String.Concat(" VIN='", xmlDoc.Attribute("VIN").Value, "'"))
        returnXml.Append(">")
        returnXml.Append("<Vehicle>")
        returnXml.Append("<Id>")
        returnXml.Append(strVehicleID)
        returnXml.Append("</Id>")
        returnXml.Append("<VIN>")
        returnXml.Append(xmlDoc.Attribute("VIN").Value)
        returnXml.Append("</VIN>")
        returnXml.Append("<Year>")
        returnXml.Append(strYear)
        returnXml.Append("</Year>")
        returnXml.Append("<Make>")
        returnXml.Append(strMake)
        returnXml.Append("</Make>")
        returnXml.Append("<Model>")
        returnXml.Append(strModel)
        returnXml.Append("</Model>")
        returnXml.Append("<Body>")
        returnXml.Append(strBody)
        returnXml.Append("</Body>")
        returnXml.Append("<MSRP>")
        returnXml.Append(strMSRP)
        returnXml.Append("</MSRP>")
        returnXml.Append("<Trade>")
        returnXml.Append(strTrade)
        returnXml.Append("</Trade>")
        returnXml.Append("<Loan>")
        returnXml.Append(strLoan)
        returnXml.Append("</Loan>")
        returnXml.Append("<Retail>")
        returnXml.Append(strRetail)
        returnXml.Append("</Retail>")
        returnXml.Append("<Weight>")
        returnXml.Append(strWeight)
        returnXml.Append("</Weight>")
        returnXml.Append("<Mileage>")
        returnXml.Append(strMileage)
        returnXml.Append("</Mileage>")
        returnXml.Append("</Vehicle></Root>")

        Return returnXml.ToString
    End Function


    Public Function GetVehicleURL() As String
        Dim str As String = String.Empty
        str = My.Settings("LynxNadaWS_NadaVehicleWs_Vehicle").ToString

        Select Case PGW.Business.Environment.getEnvironment.Trim.ToUpper
            Case "PRD"
                str = My.Settings("LynxNadaWS_PRDVehicleWs_Vehicle").ToString
            Case "PROD"
                str = My.Settings("LynxNadaWS_PRDVehicleWs_Vehicle").ToString
            Case ""
                str = My.Settings("LynxNadaWS_NadaVehicleWs_Vehicle").ToString
        End Select
        Return str
    End Function

    Public Function FrameMailBody(ByVal NADAURL As String, ByVal strErrorText As String, ByVal strParamINfo As String) As String
        Dim stringBuilder As New Text.StringBuilder()

        Try
            stringBuilder.Append("<span style='font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>")
            stringBuilder.Append("NADA Service Summary")
            stringBuilder.Append("</span>")

            'Environment 
            stringBuilder.Append("<table>")
            stringBuilder.Append("<tr style='vertical-align:top;text-align:left;'>")
            stringBuilder.Append("<td style='width:20%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>")
            stringBuilder.Append("Running Environment")
            stringBuilder.Append("</td>")
            stringBuilder.Append("<td style='width:80%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>")
            stringBuilder.Append(PGW.Business.Environment.getEnvironment())
            stringBuilder.Append("</td>")
            stringBuilder.Append("</tr>")



            stringBuilder.Append("<tr style='vertical-align:top;text-align:left;'>")
            stringBuilder.Append("<td style='width:20%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>")
            stringBuilder.Append("NADA URL")
            stringBuilder.Append("</td>")
            stringBuilder.Append("<td style='width:80%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>")
            stringBuilder.Append(NADAURL)
            stringBuilder.Append("</td>")
            stringBuilder.Append("</tr>")

            'Program Name
            stringBuilder.Append("<tr style='vertical-align:top;text-align:left;'>")
            stringBuilder.Append("<td style='width:20%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>")
            stringBuilder.Append("Program Name")
            stringBuilder.Append("</td>")
            stringBuilder.Append("<td style='width:80%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>")
            stringBuilder.Append("PGW NADA SERVICE")
            stringBuilder.Append("</td>")
            stringBuilder.Append("</tr>")

            stringBuilder.Append("<tr style='vertical-align:top;text-align:left;'>")
            stringBuilder.Append("<td style='width:20%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>")
            stringBuilder.Append("Start Time")
            stringBuilder.Append("</td>")
            stringBuilder.Append("<td style='width:80%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>")
            stringBuilder.Append(Now.ToString)
            stringBuilder.Append("</td>")
            stringBuilder.Append("</tr>")

            stringBuilder.Append("<tr style='vertical-align:top;text-align:left;'>")
            stringBuilder.Append("<td style='width:20%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>")
            stringBuilder.Append("Parameter Info")
            stringBuilder.Append("</td>")
            stringBuilder.Append("<td style='width:80%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>")
            stringBuilder.Append(strParamINfo)
            stringBuilder.Append("</td>")
            stringBuilder.Append("</tr>")

            stringBuilder.Append("<tr style='vertical-align:top;text-align:left;'>")
            stringBuilder.Append("<td style='width:20%; background-color:#dcdcdc;font-family:Verdana;font-size:8pt;font-weight:bold;color:#07519a;'>")
            stringBuilder.Append("Error Text")
            stringBuilder.Append("</td>")
            stringBuilder.Append("<td style='width:80%; background-color:#fffacd;font-family:Verdana;font-size:8pt;font-weight:bold;color:black;'>")
            stringBuilder.Append(HttpContext.Current.Server.HtmlEncode(strErrorText))
            stringBuilder.Append("</td>")
            stringBuilder.Append("</tr>")

            stringBuilder.Append("<tr style='vertical-align:top;text-align:left;'>")
            stringBuilder.Append("</tr>")

            stringBuilder.Append("</table>")

            Return stringBuilder.ToString
        Catch ex As Exception

        End Try
    End Function

    'Not used
    Public Function SendMail(ByVal mailSubject As String, ByVal mailBody As String) As String
        Dim promoteMail As MailMessage
        Dim mailSuccess As String = String.Empty
        Dim environment As String = String.Empty
        Dim smtpServer As String = String.Empty

        Try
            'Checking the environment and getting mail server information from web.config file
            'environment = "SmtpServer_" & PGW.Business.Environment.getEnvironment().ToUpper()

            'Get the server information from web.config file
            smtpServer = "rocSMTP01.pgw.local" 'ConfigurationManager.AppSettings("smtpServer")
            promoteMail = New MailMessage
            promoteMail.To.Add("")

            Dim fromAddress As MailAddress
            fromAddress = New MailAddress("")

            promoteMail.From = fromAddress
            promoteMail.Subject = mailSubject
            promoteMail.Body = mailBody
            promoteMail.IsBodyHtml = True

            promoteMail.Priority = MailPriority.Normal

            Dim smtp As New SmtpClient(smtpServer)

            smtp.Send(promoteMail)

            Return mailSuccess
        Catch ex As Exception
            mailSuccess = ex.Message
            'Throw ex
        Finally
            promoteMail = Nothing
        End Try
    End Function

End Class
