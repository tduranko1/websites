﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:import href="msxsl/generic-template-library.xsl"/>
  <xsl:import href="msxsl/msjs-client-library.js"/>
  <xsl:output method="html" indent="yes" encoding="UTF-8" />

  <xsl:template match="/">
    <html>
      <head>
        <title>Vehicle Evaluator</title>
        <LINK rel="stylesheet" href="/css/apdMain.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/apdSelect.css" type="text/css"/>
        <LINK rel="stylesheet" href="/css/custom.css" type="text/css"/>
        <STYLE type="text/css">
          .chkbox {border:0px;background-color:#FFFFFF;background-image:url('')}
        </STYLE>

        <SCRIPT language="JavaScript" src="/js/formvalid.js"></SCRIPT>
        <script type="text/javascript" language="JavaScript1.2"  src="/webhelp/RoboHelp_CSH.js"></script>
        <script type="text/javascript">
          document.onhelp=function(){
          RH_ShowHelp(0,"WebHelp/APD_ClaimsSys_Help.htm",HH_HELP_CONTEXT,5);
          event.returnValue=false;
          };
        </script>
        <xsl:call-template name="HeaderJavaScript"/>
      </head>

      <body class="bodyAPDSubSub" onload="NadaWrapper_onLoad()" onunload="NadaWrapper_unLoad()">
        <form name="form1" method="post" action="/VehicleEvaluator.asp" target="thisWin">
          <xsl:apply-templates select="Root"/>
        </form>


        <xml id="xmlNada" name="xmlNada" ondatasetcomplete="xmlNada.setProperty('SelectionLanguage', 'XPath');">
          <xsl:copy-of select="Root/AccessoryData"/>
        </xml>

      </body>
    </html>
  </xsl:template>

  <xsl:template name="HeaderJavaScript">
    <script language="JavaScript">

      window.name = "thisWin";
      var gsInvalidVIN;
      gsVIN = '<xsl:value-of select="/Root/@VIN"/>';

      function ToNumber( val )
      {
      if ( isNaN( val ) )
      return 0;
      return Number( val );
      }

      function NadaWrapper_onLoad(){

      try{
      var sDBAlert;

      // client message in case of invalid vin
      <xsl:if test="Root/Errors/Error[@name='InvalidVIN']">
        gsInvalidVIN = '<xsl:value-of select="Root/Errors/Error/@VIN"/>';
      </xsl:if>

      // client message in case of bad data
      <xsl:if test="Root/NADALossData/DBAlertMsg">
        sDBAlert = '<xsl:value-of select="Root/NADALossData/DBAlertMsg"/>';
      </xsl:if>

      if (sDBAlert.length &gt; 0){
      var sContinue = YesNoMessage("Vehicle Evaluator", sDBAlert, 140)
      if (sContinue == "No"){
      btnCancel_onclick();
      return false;
      }
      }

      if (form1.selRegion.selectedIndex &gt; 0 &amp;&amp;
      form1.selYear.selectedIndex &gt; 0 &amp;&amp;
      form1.selMake.selectedIndex &gt; 0 &amp;&amp;
      form1.selModel.selectedIndex &gt; 0 &amp;&amp;
      form1.selBody.selectedIndex &gt; 0)
      {
      form1.btnFind.disabled = false;
      form1.btnFind.style.cursor = 'hand';
      }
      else
      {
      form1.btnFind.disabled = true;
      form1.btnFind.style.cursor = 'default';
      }

      if (gsInvalidVIN)
      AlertInvalidVIN(gsInvalidVIN);
      }
      catch(e){
      ClientError(BuildErrMsg("NadaWrapper_onLoad") + "Error: " + e.message);
      }
      }

      function NadaWrapper_unLoad(){
      // this code causes the dialog to maintain its current onscreen position when reloaded
      // due to form submission.

      window.dialogLeft = event.screenX - event.clientX - 3;
      window.dialogTop = event.screenY - event.clientY - 22;
      }

      function AlertInvalidVIN(sInvalidVin){
      var vinMsg = "The VIN, " + sInvalidVin + ", is invalid or resolves to a vehicle more than 20 years old.  ";
      vinMsg += "This may cause incomplete or incorrect data.  To avoid this: \r\n  ";
      vinMsg += "1) Enter a correct VIN or remove all characters from the 'VIN:' entry box. \r\n  ";
      vinMsg += "2) Click 'Decode>>'.";
      ClientWarning(vinMsg);
      }

      function ValidateVIN(){
      var sVIN = form1.txtVIN.value;

      <!-- All valid VINs are 17 characters long -->

      if (sVIN.length != 17){
      AlertInvalidVIN(sVIN);
      return false;
      }

      <!-- Prevent NADAComServer from taking repetitive hits on a single VIN -->
      if (gsVIN == sVIN){
      if (gsInvalidVIN) AlertInvalidVIN(sVIN);
      return false;
      }
      return true;
      }

      function SubmitAction( strAction ){
      if (strAction == "LookupVIN")
      if (!ValidateVIN())
      return;

      try{
      form1.strAction.value = strAction;
      form1.txtRegion.value = form1.selRegion.value;
      form1.txtRegionDesc.value = form1.selRegion.options[form1.selRegion.selectedIndex].text;
      form1.txtYear.value = form1.selYear.value;
      form1.txtMake.value = form1.selMake.value;
      form1.txtModel.value = form1.selModel.value;
      form1.txtBody.value = form1.selBody.value;
      form1.btnFind.disabled = true;
      form1.btnMileage.disabled = true;
      form1.btnOK.disabled = true;
      form1.btnDecode.disabled = true;
      form1.selRegion.disabled = true;
      form1.selYear.disabled = true;
      form1.selMake.disabled = true;
      form1.selModel.disabled = true;
      form1.selBody.disabled = true;
      form1.submit();
      }
      catch(e){
      ClientError(BuildErrMsg("SubmitAction") + e.message);
      }
      }

      //LJCPCBLCX11000237
      // Updates the value fields on the client side
      // for clicks to the Accessory list check-boxes.
      //

      function chkBox_onclick(){
      var i, j
      var bOn;
      var nAccTradeValue;
      var iAccCode=0;
      var iThis;
      var oTrade, oInc, oExc;

      try{
      if (!xmlNada) throw "Data island does not exist";

      iAccCode = window.event.srcElement.name

      oTrade = xmlNada.selectSingleNode("/AccessoryData/AccessoryList/Accessory[@code='" + iAccCode + "']");
      if (oTrade)
      nAccTradeValue = oTrade.getAttribute("tradeValue");
      else
      nAccTradeValue = 0;

      if (window.event.srcElement.checked){ //user checked an accessory

      bOn = true;
      // calc accessory total
      document.all.txtAccTotalRetail.innerText = ToNumber(document.all.txtAccTotalRetail.innerText) + ToNumber(window.event.srcElement.value);
      document.all.txtAccTotalTrade.innerText = ToNumber(document.all.txtAccTotalTrade.innerText) + ToNumber(nAccTradeValue);

      // Adjust Trade, Loan and Retail values
      if (document.all.txtAdjTrade)
      document.all.txtAdjTrade.innerText = ToNumber(document.all.txtAdjTrade.innerText) + ToNumber(nAccTradeValue);

      if (document.all.txtAdjLoan)
      document.all.txtAdjLoan.innerText = ToNumber(document.all.txtAdjLoan.innerText) + ToNumber(window.event.srcElement.value);

      if (document.all.txtAdjRetail)
      document.all.txtAdjRetail.innerText = ToNumber(document.all.txtAdjRetail.innerText) + ToNumber(window.event.srcElement.value);
      }
      else{ //user unchecked an accessory

      bOn = false;
      // calc accessory total
      if (document.all.txtAccTotalRetail)
      document.all.txtAccTotalRetail.innerText = ToNumber(document.all.txtAccTotalRetail.innerText) - ToNumber(window.event.srcElement.value);

      if (document.all.txtAccTotalTrade)
      document.all.txtAccTotalTrade.innerText = ToNumber(document.all.txtAccTotalTrade.innerText) - ToNumber(nAccTradeValue);


      // Adjust Trade, Loan and Retail values
      if (document.all.txtAdjTrade)
      document.all.txtAdjTrade.innerText = ToNumber(document.all.txtAdjTrade.innerText) - ToNumber(nAccTradeValue);

      if (document.all.txtAdjLoan)
      document.all.txtAdjLoan.innerText = ToNumber(document.all.txtAdjLoan.innerText) - ToNumber(window.event.srcElement.value);

      if (document.all.txtAdjRetail)
      document.all.txtAdjRetail.innerText = ToNumber(document.all.txtAdjRetail.innerText) - ToNumber(window.event.srcElement.value);
      }


      // Accessories - Package Includes
      oInc = xmlNada.selectSingleNode("/AccessoryData/PackageInclusive/PI[@other='" + iAccCode + "']");
      if (oInc){
      iThis = "" + oInc.getAttribute("this");
      document.all(iThis).checked = bOn;
      document.all(iThis).disabled = bOn;
      }

      // Accessories - Mutually Exclusives
      oExc = xmlNada.selectSingleNode("/AccessoryData/MutuallyExclusive/ME[@other='" + iAccCode + "']");
      if (oExc){
      iThis = "" + oExc.getAttribute("this");
      document.all(iThis).disabled = bOn;
      }
      }
      catch(e){
      ClientError("Src: " + iAccCode + "\n" + BuildErrMsg("chkBox_onclick") + e.message);
      }
      }

      //
      //  Closes the NADA  dialog and returns required values to calling page.
      //

      function btnOk_onclick(){
      try{
      var bookValue = '';
      var sDescInfo = '';

      if (form1.chkSaveVehicle.checked == true){
      sDescInfo += 'year--' + form1.selYear.options[form1.selYear.selectedIndex].text + '||';
      sDescInfo += 'make--' + form1.selMake.options[form1.selMake.selectedIndex].text + '||';
      sDescInfo += 'model--' + form1.selModel.options[form1.selModel.selectedIndex].text + '||';
      sDescInfo += 'body--' + form1.selBody.options[form1.selBody.selectedIndex].text + '||';
      }
      if (form1.chkSaveVIN.checked == true)
      sDescInfo += 'vin--' + form1.txtVIN.value + '||';
      if (form1.chkSaveRetailValue.checked == true){
      bookValue = document.all.tdAdjRetail.innerText;
      if (bookValue.indexOf('.') &gt; -1)
      bookValue = bookValue.slice(0, bookValue.indexOf('.'));
      bookValue += '.00';
      sDescInfo += 'value--' + bookValue + '||';
      }
      if (form1.chkSaveTradeValue.checked == true){
      bookValue = document.all.tdAdjTrade.innerText;
      if (bookValue.indexOf('.') &gt; -1)
      bookValue = bookValue.slice(0, bookValue.indexOf('.'));
      bookValue += '.00';
      sDescInfo += 'value--' + bookValue + '||';
      }
      if (form1.chkSaveMileage.checked == true)
      sDescInfo += 'miles--' + form1.txtMileage.value + '||';

      //alert(sDescInfo + "; " + sDescInfo.substr(0, sDescInfo.length - 2)); return;
      window.returnValue = sDescInfo.substr(0, sDescInfo.length - 2);
      window.close();
      }
      catch(e){
      ClientError(BuildErrMsg("btnOk_onclick") + e.message);
      }
      }

      function btnFind_onClick(){
      try{
      var doc = document.all;
      doc.divAccessories.style.visibility = 'visible';
      doc.tdMSRP.style.visibility = 'visible';
      doc.tdRetail.style.visibility = 'visible';
      doc.tdTrade.style.visibility = 'visible';
      doc.tdAdjMileageRetail.style.visibility = 'visible';
      doc.tdAdjMileageTrade.style.visibility = 'visible';
      doc.tdAccessoriesRetail.style.visibility = 'visible';
      doc.tdAccessoriesTrade.style.visibility = 'visible';
      doc.tdAdjRetail.style.visibility = 'visible';
      doc.tdAdjTrade.style.visibility = 'visible';
      form1.btnOK.disabled = false;
      form1.btnOK.style.cursor = 'hand';
      form1.btnMileage.disabled = false;
      form1.btnMileage.style.cursor = 'hand';

      // add the default selected accessories
      if (divAccessories.style.visibility == "visible"){
      var oAccessories = divAccessories.getElementsByTagName("INPUT");
      if (oAccessories){
      var nAccTradeValue, oTrade;
      var iAccCode=0;
      document.all.txtAdjTrade.innerText = 0;
      document.all.txtAdjLoan.innerText = 0;
      document.all.txtAdjRetail.innerText = 0;

      document.all.txtAccTotalRetail.innerText = 0;
      document.all.txtAccTotalTrade.innerText = 0;

      for (var i = 0; i &lt; oAccessories.length; i++){
      if (oAccessories[i].checked){
      iAccCode = oAccessories[i].name;

      oTrade = xmlNada.selectSingleNode("/AccessoryData/AccessoryList/Accessory[@code='" + iAccCode + "']");
      if (oTrade)
      nAccTradeValue = oTrade.getAttribute("tradeValue");
      else
      nAccTradeValue = 0;

      // calc accessory total
      document.all.txtAccTotalRetail.innerText = ToNumber(document.all.txtAccTotalRetail.innerText) + ToNumber(oAccessories[i].value);
      document.all.txtAccTotalTrade.innerText = ToNumber(document.all.txtAccTotalTrade.innerText) + ToNumber(nAccTradeValue);

      // Adjust Trade, Loan and Retail values
      if (document.all.txtAdjTrade)
      document.all.txtAdjTrade.innerText = ToNumber(document.all.txtAdjTrade.innerText) + ToNumber(nAccTradeValue);

      if (document.all.txtAdjLoan)
      document.all.txtAdjLoan.innerText = ToNumber(document.all.txtAdjLoan.innerText) + ToNumber(oAccessories[i].value);

      if (document.all.txtAdjRetail)
      document.all.txtAdjRetail.innerText = ToNumber(document.all.txtAdjRetail.innerText) + ToNumber(oAccessories[i].value);
      }
      }
      }
      }

      btnMileage_onclick();
      }
      catch(e){
      ClientError(BuildErrMsg("btnFind_onClick") + e.message);
      }
      }


      function AlphaNumericOnly(){
      //alert(event.keyCode);
      if (!(event.keyCode &gt;= 65 &amp;&amp; event.keyCode &lt;= 90) &amp;&amp;
      !(event.keyCode &gt;= 97 &amp;&amp; event.keyCode &lt;= 122) &amp;&amp;
      !(event.keyCode &gt;= 48 &amp;&amp; event.keyCode &lt;= 57))
      event.returnValue = false;
      }

      //
      // Closes NADA dialog with a return value of 'cancel'.
      //
      function btnCancel_onclick()
      {
      window.returnValue = 'cancel';
      window.close();
      }

      //
      // Updates the value fields with any changes to the vehicle mileage.
      //
      function btnMileage_onclick(){

      var lMileage = 0;
      var lMileAdjustment = 0;
      var lTrade = 0;
      var i;
      var dFiftyCents = 0;
      var Retail = 0;
      var Trade = 0;
      var AdjustedRetail= 0;
      var AdjustedTrade = 0;
      var AdjustedLoan = 0;
      var obj;
      var lVariable = 0;

      try{
      if (!xmlNada) throw "Data island does not exist";

      if ( isNaN( form1.txtMileage.value ) ) {
      ClientInfo( "Please enter a numeric value for Mileage." );
      return;
      }

      lMileage = Number( form1.txtMileage.value );

      if ( 0 > lMileage ) {
      ClientInfo( "Please enter a positive numeric value for Mileage." );
      return;
      }

      form1.txtMileage.value = lMileage;

      if (document.all.txtTrade){
      lTrade = ToNumber(document.all.txtTrade.innerText);

      //if txtTrade value is 0 or mileage is 0, return 0
      if ((lTrade &lt;= 0 ) || (lMileage == 0))	{
      lMileAdjustment = 0;
      }
      else{
      var obj2 = xmlNada.selectNodes("/AccessoryData/MileageRange/MR[@mile &lt;= " + lMileage + "]");
      if (obj2 &amp;&amp; obj2.length > 0)
      obj = obj2.item(obj2.length - 1); //Get the last xml node that is closest to the mileage input

      if (obj) { // if a node was found, get the adjustment and variable values.
      lMileAdjustment = ToNumber(obj.getAttribute("base"));
      lVariable = ToNumber(obj.getAttribute("variable"));
      }

      if (lVariable &gt; 0)
      dFiftyCents = 0.5;
      else if(lVariable == 0)
      dFiftyCents = 0;
      else if(lVariable &lt; 0)
      dFiftyCents = -0.5;

      lMileAdjustment = lMileAdjustment + lTrade * (lVariable * 0.01) + dFiftyCents;

      //NADA MILEAGE Rule
      //- Mileage additions cannot exceed 50% of base veh txtTrade in value
      //- Mileage deductions cannot exceed 40% of base veh txtTrade in value

      if (lTrade &gt; 0)
      {
      if (lMileAdjustment &gt; 0)
      {
      if (lMileAdjustment &gt; (lTrade * 0.5))
      lMileAdjustment = lTrade * 0.5;
      }
      else if (lMileAdjustment &lt; 0)
      {
      if (Math.abs(lMileAdjustment) &gt; (lTrade * 0.4))
      lMileAdjustment = 0 - lTrade * 0.4;
      }
      }
      }
      }

      if (!isNaN(lMileAdjustment)) {//if the adjustment was found then update the UI.
      document.all.txtAdjMileageRetail.innerText = lMileAdjustment;
      document.all.txtAdjMileageTrade.innerText = lMileAdjustment;
      }

      //adjusted txtTrade-in should be &gt;= 120
      if (document.all.txtTrade){
      Trade = ToNumber(document.all.txtTrade.innerText);
      AdjustedTrade = ToNumber(Trade) + lMileAdjustment + ToNumber(document.all.txtAccTotalTrade.innerText);
      }
      else
      Trade = document.all.tdRetail.innerText;


      if (AdjustedTrade &gt; 120){
      document.all.txtAdjTrade.innerText = AdjustedTrade;
      }
      else{
      if (ToNumber(Trade))
      document.all.txtAdjTrade.innerText = 120;
      else{
      document.all.tdAdjTrade.innerText = "Not Avail";
      document.all.chkSaveTradeValue.checked = false;
      document.all.chkSaveTradeValue.disabled = true;
      document.all.chkSaveTradeValue.style.cursor = 'default';
      }
      }

      //adjusted txtLoan must be &gt;= 0
      if (!isNaN(document.all.txtLoan.innerText))
      AdjustedLoan = ToNumber(document.all.txtLoan.innerText) + lMileAdjustment + ToNumber(document.all.txtAccTotalRetail.innerText);
      else
      AdjustedLoad = 0;

      if (AdjustedLoan &gt; 0)
      document.all.txtAdjLoan.innerText = AdjustedLoan;
      else
      document.all.txtAdjLoan.innerText = 0;

      //adjusted txtRetail should be &gt;= 420
      if (document.all.txtRetail){
      Retail = ToNumber(document.all.txtRetail.innerText);
      AdjustedRetail = ToNumber(Retail) + lMileAdjustment + ToNumber(document.all.txtAccTotalRetail.innerText);
      }
      else
      Retail = document.all.tdRetail.innerText;

      if (AdjustedRetail &gt; 420)
      document.all.txtAdjRetail.innerText = AdjustedRetail;
      else{
      if (ToNumber(Retail))
      document.all.txtAdjRetail.innerText = 420;
      else{
      document.all.tdAdjRetail.innerText = "Not Avail";
      document.all.chkSaveRetailValue.checked = false;
      document.all.chkSaveRetailValue.disabled = true;
      document.all.chkSaveRetailValue.style.cursor = 'default';
      }
      }
      }
      catch(e){
      ClientError(BuildErrMsg("btnMileage_onclick") + e.message);
      }
      }

      function CheckValueToSave(){
      if (event.srcElement.id == "chkSaveRetailValue"){
      if (document.all.chkSaveRetailValue.checked == true) document.all.chkSaveTradeValue.checked = false;
      }
      else{
      if (document.all.chkSaveTradeValue.checked == true) document.all.chkSaveRetailValue.checked = false;
      }
      }

      function BuildErrMsg(FunctionName){
      var sErrMsg = "Function: " + FunctionName + "\n";
      sErrMsg += "Region: " + form1.selRegion.value + "-" + form1.selRegion.options[form1.selRegion.selectedIndex].text
      "\nYear: " + form1.selYear.value +
      "\nMake: " + form1.selMake.value +
      "\nModel: " + form1.selModel.value +
      "\nBody: " + form1.selBody.value +
      "\nMileage: " + form1.txtMileage.value +
      "\nError: ";
      return sErrMsg;
      }

    </script>
  </xsl:template>

  <xsl:template match="Root">

    <TABLE cellspacing="0" cellpadding="0" border="0" width="725" height="100%">
      <COLGROUP>
        <COL width="355" valign="top"/>
        <COL width="15" valign="top"/>
        <COL width="355" valign="top"/>
      </COLGROUP>
      <TR>
        <!-- Region, Insured State/County -->
        <TD colspan="3" align="center" valign="center">
          <table cellspacing="0" cellpadding="0" border="0" width="725">
            <tr>
              <td nowrap="nowrap">
                Region:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                <select id="selRegion" onChange="SubmitAction('SelectVehicle')" tabindex="1">
                  <option/>
                  <xsl:for-each select="RegionList/Region">
                    <option>
                      <xsl:attribute name="value">
                        <xsl:value-of select="@code"/>
                      </xsl:attribute>
                      <xsl:if test="@selected">
                        <xsl:attribute name="selected"/>
                      </xsl:if>
                      <xsl:value-of select="."/>
                    </option>
                  </xsl:for-each>
                </select>
                <!--<input type="hidden" name="strRegion2"/>-->
                <input type="hidden" name="strAction"/>
                <input type="hidden" id="txtAction">
                  <xsl:attribute name="value">
                    <xsl:value-of select="@Action"/>
                  </xsl:attribute>
                </input>
                <input type="hidden" name="strZip">
                  <xsl:attribute name="value">
                    <xsl:value-of select="NADALossData/ZipCode"/>
                  </xsl:attribute>
                </input>
                <input type="hidden" name="strDBDate">
                  <xsl:attribute name="value">
                    <xsl:value-of select="NADALossData/DBDate"/>
                  </xsl:attribute>
                </input>
                <input type="hidden" name="strReadOnly">
                  <xsl:attribute name="value">
                    <xsl:value-of select="@ReadOnly"/>
                  </xsl:attribute>
                </input>
                <input type="hidden" id="txtRegion" name="strRegion"/>
                <input type="hidden" id="txtRegionDesc" name="strRegionDesc"/>
                <input type="hidden" id="txtYear" name="strYear"/>
                <input type="hidden" id="txtMake" name="strMake"/>
                <input type="hidden" id="txtModel" name="strModel"/>
                <input type="hidden" id="txtBody" name="strBody"/>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              </td>
              <td>
                Insured State/County:<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                <input type="text" id="txtInsStateCounty" size="11" readonly="true" tabindex="2">
                  <xsl:variable name="NADAState" select="NADALossData/State"/>
                  <xsl:variable name="NADACounty" select="NADALossData/County"/>
                  <xsl:if test="$NADAState != '' and $NADACounty != ''">
                    <xsl:attribute name="value">
                      <xsl:value-of select="NADALossData/State"/>/<xsl:value-of select="NADALossData/County"/>
                    </xsl:attribute>
                  </xsl:if>
                </input>
              </td>
            </tr>
            <tr>
              <td colspan="3">
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              </td>
            </tr>
          </table>
        </TD>
      </TR>
      <TR>
        <TD valign="top">
          <!-- VIN Decode -->
          <table cellSpacing="0" cellPadding="4" border="0" width="355">
            <tr>
              <td>
                VIN:
                <input id="txtVIN" name="strVIN" tabindex="3" maxlength="17" onkeypress="AlphaNumericOnly()">
                  <xsl:attribute name="value">
                    <xsl:value-of select="Vehicle/VIN"/>
                  </xsl:attribute>
                </input>
              </td>
              <td align="right">
                <input id="btnDecode" name="butLookupVIN" type="button" class="formbutton" onClick="SubmitAction('LookupVIN')" value="Decode >>" tabindex="4"/>
              </td>
            </tr>
          </table>

          <table cellSpacing="0" cellPadding="0" border="0" style="display:none">
            <!-- Invisible table with VIN Resolution errors -->
            <tr>
              <td>
                <xsl:choose>
                  <xsl:when test="VINResolutionList/Error!=''">
                    <td colspan="2">
                      <font color="red">
                        <b>
                          <xsl:value-of select="VINResolutionList/Error"/>
                        </b>
                      </font>
                    </td>
                  </xsl:when>
                  <xsl:when test="count(VINResolutionList/Vehicle[@uid &gt; 0]) = 0">
                    <td colspan="2">
                      <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td colspan="2">
                      <br />
                      VIN Resolution:
                      <table cellSpacing="0" cellPadding="0" border="0">
                        <tr>
                          <td vAlign="top">
                            <select name="strVehId" onChange="SubmitAction('ResolveVIN')">
                              <xsl:for-each select="VINResolutionList/Vehicle">
                                <option>
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="@uid"/>
                                  </xsl:attribute>
                                  <xsl:if test="@selected">
                                    <xsl:attribute name="selected"/>
                                  </xsl:if>
                                  <xsl:value-of select="."/>
                                </option>
                              </xsl:for-each>
                            </select>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
          </table>
        </TD>
        <TD></TD>
        <!-- spacer -->
        <TD>
          <!-- Year, Make, Model, Body -->
          <table cellSpacing="0" cellPadding="4" border="0">
            <tr>
              <td>Year</td>
              <td>
                <select id="selYear" onChange="SubmitAction('SelectVehicle')" tabindex="5">
                  <option/>
                  <xsl:for-each select="YearList/Year">
                    <option>
                      <xsl:attribute name="value">
                        <xsl:value-of select="."/>
                      </xsl:attribute>
                      <xsl:if test="@selected">
                        <xsl:attribute name="selected"/>
                      </xsl:if>
                      <xsl:value-of select="."/>
                    </option>
                  </xsl:for-each>
                </select>
              </td>
            </tr>
            <tr>
              <td>Make</td>
              <td>
                <select id="selMake" onChange="SubmitAction('SelectVehicle')" tabindex="6">
                  <option/>
                  <xsl:for-each select="MakeList/Make">
                    <option>
                      <xsl:attribute name="value">
                        <xsl:value-of select="."/>
                      </xsl:attribute>
                      <xsl:if test="@selected">
                        <xsl:attribute name="selected"/>
                      </xsl:if>
                      <xsl:value-of select="."/>
                    </option>
                  </xsl:for-each>
                </select>
              </td>
            </tr>
            <tr>
              <td>Model</td>
              <td>
                <select id="selModel" onChange="SubmitAction('SelectVehicle')" tabindex="7">
                  <option/>
                  <xsl:for-each select="ModelList/Model">
                    <option>
                      <xsl:attribute name="value">
                        <xsl:value-of select="."/>
                      </xsl:attribute>
                      <xsl:if test="@selected">
                        <xsl:attribute name="selected"/>
                      </xsl:if>
                      <xsl:value-of select="."/>
                    </option>
                  </xsl:for-each>
                </select>
              </td>
            </tr>
            <tr>
              <td>Body</td>
              <td>
                <select id="selBody" onChange="SubmitAction('SelectVehicle')" tabindex="8">
                  <option/>
                  <xsl:for-each select="BodyList/Body">
                    <option>
                      <xsl:attribute name="value">
                        <xsl:value-of select="."/>
                      </xsl:attribute>
                      <xsl:if test="@selected">
                        <xsl:attribute name="selected"/>
                      </xsl:if>
                      <xsl:value-of select="."/>
                    </option>
                  </xsl:for-each>
                </select>
              </td>
            </tr>
            <tr>
              <td colspan="2" align="center">
                <input name="btnFind" type="button" class="formbutton" onclick="btnFind_onClick()" value="Find" tabindex="9"/>
              </td>
            </tr>
          </table>
        </TD>
      </TR>
      <TR>
        <TD>
          <table border="0" width="337">
            <tr>
              <td width="257">
                <strong>Accessories:</strong>
              </td>
              <td width="45">
                <strong>Retail</strong>
              </td>
              <td width="35">
                <strong>Trade</strong>
              </td>
            </tr>
          </table>

          <DIV id="divAccessories" unselectable="on" class="autoflowDiv" style="height:140px; width:355px; visibility:hidden;" tabindex="10">
            <xsl:variable name="cnt">
              <xsl:value-of select="floor( ( count( AccessoryData/AccessoryList/Accessory ) + 1 ) div 2 )"/>
            </xsl:variable>

            <table cellSpacing="0" cellPadding="0" border="0" width="337">
              <colgroup>
                <col width="7"/>
                <col width="30"/>
                <col width="200"/>
                <col align="right" width="50"/>
                <col align="right" width="50"/>
              </colgroup>
              <tr>
                <td>
                  <xsl:for-each select="AccessoryData/AccessoryList/Accessory[ $cnt >= position() ]">
                    <xsl:apply-templates select="."/>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="AccessoryData/AccessoryList/Accessory[ position() > $cnt ]">
                    <xsl:apply-templates select="."/>
                  </xsl:for-each>
                </td>
              </tr>
            </table>
          </DIV>
        </TD>
        <TD></TD>
        <TD valign="top" nowrap="nowrap">
          <table border="0" cellpadding="4" cellspacing="0">
            <colgroup>
              <col width="200"/>
              <col width="60" align="right"/>
              <col width="60" align="right"/>
            </colgroup>
            <tr>
              <td></td>
              <td>
                <strong>Retail</strong>
              </td>
              <td>
                <strong>Trade</strong>
              </td>
            </tr>
            <tr>
              <td>MSRP</td>
              <td id="tdMSRP" align="right" style="visibility:hidden">
                $<xsl:value-of select="Vehicle/MSRP"/>
              </td>
              <td id="tdMSRP2" align="right" style="visibility:hidden">
                $<xsl:value-of select="Vehicle/MSRP"/>
              </td>
            </tr>
            <tr>
              <td>Base</td>
              <td id="tdRetail" style="visibility:hidden">
                <xsl:choose>
                  <xsl:when test="Vehicle/Retail = 0">Not Avail</xsl:when>
                  <xsl:otherwise>
                    $<span id="txtRetail">
                      <xsl:value-of select="Vehicle/Retail"/>
                    </span>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <td id="tdTrade" style="visibility:hidden">
                <xsl:choose>
                  <xsl:when test="Vehicle/Trade = 0">Not Avail</xsl:when>
                  <xsl:otherwise>
                    $<span id="txtTrade">
                      <xsl:value-of select="Vehicle/Trade"/>
                    </span>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
            <tr>
              <td nowrap="nowrap">
                Miles
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                <input id="txtMileage" name="txtMileage" size="6" maxlength="8" tabindex="11">
                  <xsl:attribute name="value">
                    <xsl:value-of select="Vehicle/Mileage"/>
                  </xsl:attribute>
                </input>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                <input name="btnMileage" id="btnMileage" type="button" class="formbutton" disabled="true" style="cursor:default;" onClick="btnMileage_onclick()" value="Adjust" tabindex="12"/>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              </td>
              <td id="tdAdjMileageRetail" align="right" style="visibility:hidden">
                $<span id="txtAdjMileageRetail">0</span>
              </td>
              <td id="tdAdjMileageTrade" align="right" style="visibility:hidden">
                $<span id="txtAdjMileageTrade">0</span>
              </td>
            </tr>
            <tr>
              <td>Accessories</td>
              <td id="tdAccessoriesRetail" align="right" style="visibility:hidden">
                $<span id="txtAccTotalRetail">0</span>
              </td>
              <td id="tdAccessoriesTrade" align="right" style="visibility:hidden">
                $<span id="txtAccTotalTrade">0</span>
              </td>
            </tr>
            <tr>
              <td>Adjusted</td>
              <td id="tdAdjRetail" align="right" style="visibility:hidden">
                <strong>
                  $<span id="txtAdjRetail">
                    <xsl:value-of select="Vehicle/Retail"/>
                  </span>
                </strong>
              </td>
              <td id="tdAdjTrade" align="right" style="visibility:hidden">
                <strong>
                  $<span id="txtAdjTrade">
                    <xsl:value-of select="Vehicle/Trade"/>
                  </span>
                </strong>
              </td>
            </tr>
          </table>
        </TD>
      </TR>
      <TR>
        <TD colspan="3">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
      </TR>
      <TR>
        <!-- Save Options -->
        <TD colspan="3" align="center">
          <table border="0" cellspacing="0" cellpadding="4" width="100%">
            <tr>
              <td>
                <strong>Save:</strong>
              </td>
            </tr>
            <tr>
              <td align="center" nowrap="nowrap">
                <input class="chkbox" type="checkbox" name="chkSaveVIN" id="chkSaveVIN" checked="true" value=""  tabindex="13"/>
                VIN
              </td>
              <td align="center" nowrap="nowrap">
                <input class="chkbox" type="checkbox" name="chkSaveVehicle" id="chkSaveVehicle" checked="true" value=""  tabindex="14"/>
                Year/Make/Model/Body
              </td>
              <td>
                <input class="chkbox" type="checkbox" name="chkSaveMileage" id="chkSaveMileage" checked="true" value=""  tabindex="15"/>
                Mileage
              </td>
              <td align="center" nowrap="nowrap">
                <input class="chkbox" type="checkbox" name="chkSaveRetailValue" id="chkSaveRetailValue" checked="true" value="" tabindex="16" onclick="CheckValueToSave()"/>
                Adjusted Retail
              </td>
              <td align="center" nowrap="nowrap">
                <input class="chkbox" type="checkbox" name="chkSaveTradeValue" id="chkSaveTradeValue" value=""  tabindex="16" onclick="CheckValueToSave()"/>
                Adjusted Trade
              </td>
            </tr>
            <tr>
              <td colspan="5" height="10"></td>
            </tr>
            <tr>
              <td colspan="5" align="center" nowrap="nowrap">
                <input type="button" name="txtbtnOK" id="btnOK" class="formbutton" disabled="true" onClick="btnOk_onclick()" value="OK"  tabindex="17">
                  <xsl:attribute name="style">
                    cursor:default;width:80;<xsl:if test="@ReadOnly='true'">visibility:hidden;</xsl:if>
                  </xsl:attribute>
                </input>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                <input type="button" name="txtbtnCancel" id="btnCancel" class="formbutton" onClick ="btnCancel_onclick()" style="width:80" value="Cancel"  tabindex="18"/>
              </td>
            </tr>
          </table>
        </TD>
      </TR>
      <TR>
        <!-- invisible data -->
        <TD colspan="3">
          <table border="0" cellspacing="0" cellpadding="0" style="display:none;">
            <tr>
              <td></td>
              <td class="nadaHeader">Loan</td>
            </tr>
            <tr>
              <td>
                <strong>Base</strong>
              </td>
              <td>
                $<span id="txtLoan">
                  <xsl:choose>
                    <xsl:when test="Vehicle/Loan != ''">
                      <xsl:value-of select="Vehicle/Loan"/>
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                  </xsl:choose>
                </span>
              </td>
            </tr>
            <tr>
              <td>
                <strong>Adjusted</strong>
              </td>
              <td>
                $<span id="txtAdjLoan">
                  <xsl:value-of select="Vehicle/Loan"/>
                </span>
              </td>
            </tr>
          </table>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

  <!-- Builds the accessories check-box list -->
  <xsl:template match="Accessory">
    <tr>
      <xsl:choose>
        <xsl:when test="@status='UnCheck'">
          <td>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </td>
          <td align="center">
            <input class="chkbox" type='checkbox' onclick='chkBox_onclick()'>
              <xsl:attribute name='name'>
                <xsl:value-of select="@code"/>
              </xsl:attribute>
              <xsl:attribute name='value'>
                <xsl:value-of select="@value"/>
              </xsl:attribute>
            </input>
          </td>
          <td>
            <xsl:value-of select="@name"/>
          </td>
          <td>
            <xsl:call-template name="AccessoryValue">
              <xsl:with-param name="value">retail</xsl:with-param>
            </xsl:call-template>
          </td>
          <td align="right">
            <xsl:call-template name="AccessoryValue">
              <xsl:with-param name="value">trade</xsl:with-param>
            </xsl:call-template>
          </td>
        </xsl:when>
        <xsl:when test="@status='Incl.'">
          <td>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </td>
          <td align="center">
            <input class="chkbox" type='checkbox' onclick='chkBox_onclick()' checked='1' disabled='1'>
              <xsl:attribute name='name'>
                <xsl:value-of select="@code"/>
              </xsl:attribute>
              <xsl:attribute name='value'>
                <xsl:value-of select="@value"/>
              </xsl:attribute>
            </input>
          </td>
          <td>
            <xsl:value-of select="@name"/>
          </td>
          <td>
            <xsl:call-template name="AccessoryValue">
              <xsl:with-param name="value">retail</xsl:with-param>
            </xsl:call-template>
          </td>
          <td align="right">
            <xsl:call-template name="AccessoryValue">
              <xsl:with-param name="value">trade</xsl:with-param>
            </xsl:call-template>
          </td>
        </xsl:when>
        <xsl:otherwise>
          <!-- @status='Excl.' -->
          <td>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </td>
          <td align="center">
            <input class="chkbox" type='checkbox' onclick='chkBox_onclick()' checked='1'>
              <xsl:attribute name='name'>
                <xsl:value-of select="@code"/>
              </xsl:attribute>
              <xsl:attribute name='value'>
                <xsl:value-of select="@value"/>
              </xsl:attribute>
            </input>
          </td>
          <td>
            <xsl:value-of select="@name"/>
          </td>
          <td>
            <xsl:call-template name="AccessoryValue">
              <xsl:with-param name="value">retail</xsl:with-param>
            </xsl:call-template>
          </td>
          <td align="right">
            <xsl:call-template name="AccessoryValue">
              <xsl:with-param name="value">trade</xsl:with-param>
            </xsl:call-template>
          </td>
        </xsl:otherwise>
      </xsl:choose>
    </tr>
  </xsl:template>

  <xsl:template name="AccessoryValue" >
    <xsl:param name="value"/>

    <xsl:choose>
      <xsl:when test="$value='retail'">
        <xsl:choose>
          <xsl:when test="@value>=0">
            <font color="green">
              <b>
                $<xsl:value-of select="@value"/>
              </b>
            </font>
          </xsl:when>
          <xsl:otherwise>
            <font color="red">
              <b>
                $<xsl:value-of select="@value"/>
              </b>
            </font>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="@value>=0">
            <font color="green">
              <b>
                $<xsl:value-of select="@tradeValue"/>
              </b>
            </font>
          </xsl:when>
          <xsl:otherwise>
            <font color="red">
              <b>
                $<xsl:value-of select="@tradeValue"/>
              </b>
            </font>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

</xsl:stylesheet>

