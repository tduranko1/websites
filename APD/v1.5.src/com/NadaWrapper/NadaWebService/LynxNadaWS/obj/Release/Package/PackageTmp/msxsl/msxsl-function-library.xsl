<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace">

<msxsl:script language="JScript" implements-prefix="user">

    // Creates a JavaScript Date object compatible string from a SQL datetime string.
    function extractSQLDateTime( strDate )
    {
        var strConv =
             strDate.substr( 5, 2 ) + "-"
           + strDate.substr( 8, 2 ) + "-"
           + strDate.substr( 0, 4 ) + " "
           + strDate.substr( 11, 8 );
        return strConv;
    }

    var i = 0;

    function RowPosition()
    {
        i = i + 1;
        return i-1;
    }
	
	function UTCConvertDate(vDate)
  	{
    var vYear = vDate.substr(0,4);
    var vMonth = vDate.substr(5,2);
    var vDay = vDate.substr(8,2);
    vDate = vMonth + '/' + vDay  + '/' + vYear
    return vDate;
  	}
	
    function UTCConvertDateAndTimeByNodeType(nodelist, N, T)
    {
        try
        {
            if ( nodelist == null | N == null | T == null )
                return '';

            var vNext = nodelist.nextNode();
            if ( vNext == null )
                return '';

            var vDate;
            if ( T=='A' | T=='a' )
                vDate = vNext.getAttribute(N);
            if ( T=='E' | T=='e' )
                vDate = vNext.selectSingleNode(N).text;
            if ( vDate == null )
                return '';

            var vYear = vDate.substr(0,4);
            var vMonth = vDate.substr(5,2);
            var vDay = vDate.substr(8,2);
            var vFormedDate = vMonth + '/' + vDay  + '/' + vYear
            var ampm = 'AM'
            var hh = vDate.substr(11,2)

            if (hh >= 12)
            {
				if (hh > 12)
				{
                    hh = hh-12;
				}
                ampm = 'PM';
            }

            return vFormedDate + '  ' + hh + ':' + vDate.substr(14,2)  + ':' + vDate.substr(17,2)  + ' ' + ampm;
        }
        catch(e)
        {
            return e.message;
        }
    }

    function UTCConvertDateAndTimeByNodeTypeShort(nodelist, N, T)
    {
        try
        {
            if ( nodelist == null | N == null | T == null )
                return '';

            var vNext = nodelist.nextNode();
            if ( vNext == null )
                return '';

            var vDate;
            if ( T=='A' | T=='a' )
                vDate = vNext.getAttribute(N);
            if ( T=='E' | T=='e' )
                vDate = vNext.selectSingleNode(N).text;
            if ( vDate == null )
                return '';

            var vYear = vDate.substr(2,2);
            var vMonth = vDate.substr(5,2);
            var vDay = vDate.substr(8,2);
            var vFormedDate = vMonth + '/' + vDay  + '/' + vYear
            var ampm = 'AM'
            var hh = vDate.substr(11,2)

            if (hh >= 12)
            {
				if (hh > 12)
				{
                    hh = hh-12;
				}
                ampm = 'PM';
            }

            //return vFormedDate + '  ' + hh + ':' + vDate.substr(14,2)  + ':' + vDate.substr(17,2)  + ' ' + ampm;
            return vFormedDate + '  ' + hh + ':' + vDate.substr(14,2)  + ' ' + ampm;
        }
        catch(e)
        {
            return e.message;
        }
    }

    function UTCConvertDateByNodeType(nodelist,N , T)
    {
        try
        {
            if ( nodelist == null )
                return '';

            var vNext = nodelist.nextNode();
            if ( vNext == null )
                return '';

            var vDate;
            if ( T=='A' | T=='a' )
                vDate = vNext.getAttribute(N);
            if ( T=='E' | T=='e' )
                vDate = vNext.selectSingleNode(N).text;
            if ( vDate == null )
                return '';

            var vYear = vDate.substr(0,4);
            var vMonth = vDate.substr(5,2);
            var vDay = vDate.substr(8,2);
            vDate = vMonth + '/' + vDay  + '/' + vYear
            return vDate;
        }
        catch(e)
        {
            return e.message;
        }
    }

    function UTCGetTimeAMPMByNodeType(nodelist,N, T )
		{
        try
        {
            if ( nodelist == null )
                return '';

            var vNext = nodelist.nextNode();
            if ( vNext == null )
                return '';

            var vDate;
            if ( T=='A' | T=='a' )
                vDate = vNext.getAttribute(N);
            if ( T=='E' | T=='e' )
                vDate = vNext.selectSingleNode(N).text;
            if ( vDate == null )
                return '';

            var ampm = 'AM'
            var hh = vDate.substr(11,2)
            if (hh >= 12)
            {
                if(hh > 12) hh = hh-12;
                ampm = 'PM';
            }
            return hh + ':' + vDate.substr(14,2)  + ':' + vDate.substr(17,2)  + '&#xa0;' + ampm;
        }
        catch(e)
        {
            return e.message;
        }
		}


    function ConvertMinutesToDaysHoursMinutesByNodeType(nodelist, N, T )
    {
        try
        {
            if ( nodelist == null )
                return '';

            var vNext = nodelist.nextNode();
            if ( vNext == null )
                return '';

            var vDate;
            if ( T=='A' | T=='a' )
                vDate = vNext.getAttribute(N);
            if ( T=='E' | T=='e' )
                vDate = vNext.selectSingleNode(N).text;
            if ( vDate == null )
                return '';

            var tDays, tHours, tMinutes;
            var fMins = parseFloat(Math.abs(vDate ));
            tDays = parseInt(fMins/1440);
            tHours = parseInt((fMins % 1440)/60);
            tMinutes = (fMins % 1440) % 60;
            return tDays + " days "  + tHours + " hr "   + tMinutes + ' mins';
        }
        catch(e)
        {
            return e.message;
        }
    }

    function chooseBackgroundColor(position, color1, color2)
    {
        // This XSL-IF block creates the alternating colors for the grid
        if (position % 2 == 0)
            return color1;
        else
            return color2;
    }


    // GetNodeListNames
    // Used internally to build XPath names for input boxes.
    function GetNodeListNames(nodelist)
    {
        try
        {
            if ( nodelist == null )
                return '';

            var vPath = '';
            var vNext = nodelist.nextNode();

            while ( vNext != null )
            {
                vPath += vNext.nodeName;
                vNext = nodelist.nextNode();
                if ( vNext != null )
                    vPath += '_';
            }

            return vPath;
        }
        catch(e)
        {
            return e.message;
        }
    }

    // FormatCurrency
    // Used to format currencies, i.e. add a decimal
    //    and two zeroes at the end (if not already present).
    function FormatCurrency(nodelist)
    {
        try
        {
            if ( nodelist == null )
                return '';

            var vNext = nodelist.nextNode();
            if ( vNext != null )
            {
                var strValue = vNext.text;

                if (strValue == '')
                    return '0.00';
                else if (strValue.indexOf('.') == -1)
                    return strValue + '.00';
                else if (strValue.indexOf('.') == strValue.length - 2)
                    return strValue + '0';
                else
                    return strValue.substr(0, strValue.indexOf('.') + 3);
            }
            return "0.00";
        }
        catch(e)
        {
            return e.message;
        }
    }

    // FormatPersonName
    // Desc: Returns passed first and last name as 'Last, F.'
    function FormatPersonName( firstNameNodeList, lastNameNodeList )
    {
        try
        {
            if ( firstNameNodeList == null || lastNameNodeList == null )
                return '';

            var vFirst = firstNameNodeList.nextNode();
            var vLast = lastNameNodeList.nextNode();
            if ( ( vFirst != null ) &amp;&amp; ( vLast != null ) )
            {
                var str = vLast.text;
                if (vFirst.text.length > 0)
                  str += ', ' + vFirst.text.substr(0,1) + '.';

                return str;
            }
            return '';
        }
        catch(e)
        {
            return e.message;
        }
    }

</msxsl:script>
</xsl:stylesheet>
