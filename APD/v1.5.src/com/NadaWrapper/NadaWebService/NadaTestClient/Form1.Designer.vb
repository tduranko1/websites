﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtOutput = New System.Windows.Forms.TextBox
        Me.btnGetYears = New System.Windows.Forms.Button
        Me.cboVehicleYear = New System.Windows.Forms.ComboBox
        Me.cboVehicleModel = New System.Windows.Forms.ComboBox
        Me.cboVehicleMake = New System.Windows.Forms.ComboBox
        Me.btnGetLoginToken = New System.Windows.Forms.Button
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtLynxWsUrl = New System.Windows.Forms.TextBox
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.btnGetAllVehicleData = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.cboRegion = New System.Windows.Forms.ComboBox
        Me.cboVehicleBody = New System.Windows.Forms.ComboBox
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnUidLookup = New System.Windows.Forms.Button
        Me.txtUid = New System.Windows.Forms.TextBox
        Me.btnVinLookup = New System.Windows.Forms.Button
        Me.txtVin = New System.Windows.Forms.TextBox
        Me.dgResults = New System.Windows.Forms.DataGridView
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.dgResults, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtOutput
        '
        Me.txtOutput.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtOutput.Location = New System.Drawing.Point(0, 0)
        Me.txtOutput.Multiline = True
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.ReadOnly = True
        Me.txtOutput.Size = New System.Drawing.Size(703, 172)
        Me.txtOutput.TabIndex = 1
        '
        'btnGetYears
        '
        Me.btnGetYears.Location = New System.Drawing.Point(15, 4)
        Me.btnGetYears.Name = "btnGetYears"
        Me.btnGetYears.Size = New System.Drawing.Size(75, 23)
        Me.btnGetYears.TabIndex = 2
        Me.btnGetYears.Text = "Clear"
        Me.btnGetYears.UseVisualStyleBackColor = True
        '
        'cboVehicleYear
        '
        Me.cboVehicleYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVehicleYear.FormattingEnabled = True
        Me.cboVehicleYear.Location = New System.Drawing.Point(96, 6)
        Me.cboVehicleYear.Name = "cboVehicleYear"
        Me.cboVehicleYear.Size = New System.Drawing.Size(218, 21)
        Me.cboVehicleYear.TabIndex = 3
        '
        'cboVehicleModel
        '
        Me.cboVehicleModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVehicleModel.FormattingEnabled = True
        Me.cboVehicleModel.Location = New System.Drawing.Point(96, 60)
        Me.cboVehicleModel.Name = "cboVehicleModel"
        Me.cboVehicleModel.Size = New System.Drawing.Size(218, 21)
        Me.cboVehicleModel.TabIndex = 5
        '
        'cboVehicleMake
        '
        Me.cboVehicleMake.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVehicleMake.FormattingEnabled = True
        Me.cboVehicleMake.Location = New System.Drawing.Point(96, 33)
        Me.cboVehicleMake.Name = "cboVehicleMake"
        Me.cboVehicleMake.Size = New System.Drawing.Size(218, 21)
        Me.cboVehicleMake.TabIndex = 7
        '
        'btnGetLoginToken
        '
        Me.btnGetLoginToken.Location = New System.Drawing.Point(28, 32)
        Me.btnGetLoginToken.Name = "btnGetLoginToken"
        Me.btnGetLoginToken.Size = New System.Drawing.Size(108, 23)
        Me.btnGetLoginToken.TabIndex = 10
        Me.btnGetLoginToken.Text = "GetLoginToken"
        Me.btnGetLoginToken.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(27, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(662, 170)
        Me.TabControl1.TabIndex = 13
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.txtLynxWsUrl)
        Me.TabPage1.Controls.Add(Me.btnGetLoginToken)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(654, 144)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Setup"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(28, 76)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Lynx ws URL"
        '
        'txtLynxWsUrl
        '
        Me.txtLynxWsUrl.Location = New System.Drawing.Point(135, 73)
        Me.txtLynxWsUrl.Name = "txtLynxWsUrl"
        Me.txtLynxWsUrl.Size = New System.Drawing.Size(278, 20)
        Me.txtLynxWsUrl.TabIndex = 13
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnGetAllVehicleData)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.cboRegion)
        Me.TabPage2.Controls.Add(Me.cboVehicleBody)
        Me.TabPage2.Controls.Add(Me.cboVehicleYear)
        Me.TabPage2.Controls.Add(Me.btnGetYears)
        Me.TabPage2.Controls.Add(Me.cboVehicleMake)
        Me.TabPage2.Controls.Add(Me.cboVehicleModel)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(654, 144)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Vehicle Lookup"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnGetAllVehicleData
        '
        Me.btnGetAllVehicleData.Location = New System.Drawing.Point(464, 112)
        Me.btnGetAllVehicleData.Name = "btnGetAllVehicleData"
        Me.btnGetAllVehicleData.Size = New System.Drawing.Size(149, 23)
        Me.btnGetAllVehicleData.TabIndex = 18
        Me.btnGetAllVehicleData.Text = "GetAllVehicleData"
        Me.btnGetAllVehicleData.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(346, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 13)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Region"
        '
        'cboRegion
        '
        Me.cboRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRegion.FormattingEnabled = True
        Me.cboRegion.Location = New System.Drawing.Point(406, 6)
        Me.cboRegion.Name = "cboRegion"
        Me.cboRegion.Size = New System.Drawing.Size(218, 21)
        Me.cboRegion.TabIndex = 11
        '
        'cboVehicleBody
        '
        Me.cboVehicleBody.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVehicleBody.FormattingEnabled = True
        Me.cboVehicleBody.Location = New System.Drawing.Point(96, 87)
        Me.cboVehicleBody.Name = "cboVehicleBody"
        Me.cboVehicleBody.Size = New System.Drawing.Size(218, 21)
        Me.cboVehicleBody.TabIndex = 10
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Label2)
        Me.TabPage3.Controls.Add(Me.Label1)
        Me.TabPage3.Controls.Add(Me.btnUidLookup)
        Me.TabPage3.Controls.Add(Me.txtUid)
        Me.TabPage3.Controls.Add(Me.btnVinLookup)
        Me.TabPage3.Controls.Add(Me.txtVin)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(654, 144)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Vin Lookup"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 89)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(23, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Uid"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(22, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Vin"
        '
        'btnUidLookup
        '
        Me.btnUidLookup.Location = New System.Drawing.Point(68, 112)
        Me.btnUidLookup.Name = "btnUidLookup"
        Me.btnUidLookup.Size = New System.Drawing.Size(75, 23)
        Me.btnUidLookup.TabIndex = 3
        Me.btnUidLookup.Text = "Lookup"
        Me.btnUidLookup.UseVisualStyleBackColor = True
        '
        'txtUid
        '
        Me.txtUid.Location = New System.Drawing.Point(68, 86)
        Me.txtUid.Name = "txtUid"
        Me.txtUid.Size = New System.Drawing.Size(321, 20)
        Me.txtUid.TabIndex = 2
        '
        'btnVinLookup
        '
        Me.btnVinLookup.Location = New System.Drawing.Point(68, 38)
        Me.btnVinLookup.Name = "btnVinLookup"
        Me.btnVinLookup.Size = New System.Drawing.Size(75, 23)
        Me.btnVinLookup.TabIndex = 1
        Me.btnVinLookup.Text = "Lookup"
        Me.btnVinLookup.UseVisualStyleBackColor = True
        '
        'txtVin
        '
        Me.txtVin.Location = New System.Drawing.Point(68, 12)
        Me.txtVin.Name = "txtVin"
        Me.txtVin.Size = New System.Drawing.Size(321, 20)
        Me.txtVin.TabIndex = 0
        '
        'dgResults
        '
        Me.dgResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgResults.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgResults.Location = New System.Drawing.Point(0, 0)
        Me.dgResults.Name = "dgResults"
        Me.dgResults.Size = New System.Drawing.Size(703, 176)
        Me.dgResults.TabIndex = 2
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.Location = New System.Drawing.Point(12, 188)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.dgResults)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtOutput)
        Me.SplitContainer1.Size = New System.Drawing.Size(703, 352)
        Me.SplitContainer1.SplitterDistance = 176
        Me.SplitContainer1.TabIndex = 14
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(723, 552)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "Form1"
        Me.Text = "Lookup"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.dgResults, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtOutput As System.Windows.Forms.TextBox
    Friend WithEvents btnGetYears As System.Windows.Forms.Button
    Friend WithEvents cboVehicleYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboVehicleModel As System.Windows.Forms.ComboBox
    Friend WithEvents cboVehicleMake As System.Windows.Forms.ComboBox
    Friend WithEvents btnGetLoginToken As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtLynxWsUrl As System.Windows.Forms.TextBox
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents cboVehicleBody As System.Windows.Forms.ComboBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents btnVinLookup As System.Windows.Forms.Button
    Friend WithEvents txtVin As System.Windows.Forms.TextBox
    Friend WithEvents dgResults As System.Windows.Forms.DataGridView
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnUidLookup As System.Windows.Forms.Button
    Friend WithEvents txtUid As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboRegion As System.Windows.Forms.ComboBox
    Friend WithEvents btnGetAllVehicleData As System.Windows.Forms.Button

End Class
