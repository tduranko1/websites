﻿Imports System.Data
Imports System.Reflection
Imports System.Xml
Imports System.IO


Public Class Form1

    Dim _NadaLoginToken As String = ""
    'Dim _objLynxNadaDll As LynxNada.LynxNada

    Dim _wsNadaUsername As String = ""
    Dim _wsNadaPassword As String = ""
    Dim _wsLynxNadaURL As String = ""
    Dim _wsNadaLoginURL As String = ""
    Dim _wsNadaVehicleURL As String = ""

    Dim _wsLynxNadaWs As New LynxNadaWs.Service

    Dim _bFormLoading As Boolean = False


    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            _bFormLoading = True
            'txtUsername.Text = My.Settings("LynxNadaWS_UserId").ToString
            'txtPassword.Text = My.Settings("LynxNadaWS_Password").ToString
            txtLynxWsUrl.Text = My.Settings("NadaTestClient_LynxNadaService_Service").ToString
            'txtNadaLoginUrl.Text = My.Settings("LynxNada_NadaLoginWS_SecureLogin").ToString
            'txtNadaVehicleUrl.Text = My.Settings("LynxNada_NadaVehicleWS_Vehicle").ToString

            '_objLynxNadaDll = New LynxNada.LynxNada(txtLynxWsUrl.Text, _
            '                                              txtNadaVehicleUrl.Text, _
            '                                              txtUsername.Text, _
            '                                              txtPassword.Text)

            GetYears()
            GetRegions()




        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            _bFormLoading = False
        End Try
    End Sub

    Private Sub InitializeWebService()
        _wsLynxNadaWs.Url = txtLynxWsUrl.Text

    End Sub
    Private Sub btnGetLoginToken_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetLoginToken.Click

        Try
            InitializeWebService()

            Dim strTemp As String = _wsLynxNadaWs.GetToken()
            Write(strTemp)


        Catch ex As Exception
            MsgBox(ex.Message)

        End Try


    End Sub

    'Private Sub LoginToNadaWs()

    '    Try

    '        '_NadaLoginToken = _objLynxNadaDll.GetLoginToken()

    '        _NadaLoginToken = _wsLynxNada.gettoken
    '        Write("New Token received: " & _NadaLoginToken)

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub
    Private Sub Write(ByVal strOutput As String)
        Try

            txtOutput.Text = DateTime.Now & ": " & strOutput & Environment.NewLine & txtOutput.Text

        Catch ex As Exception

        End Try

    End Sub


    Private Sub btnGetYears_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetYears.Click


    End Sub

    Private Sub btnGetMakes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnGetModels_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub btnGetBodies_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub
    Private Sub GetRegions()
        Cursor = Cursors.WaitCursor
        Try
            InitializeWebService()

            Dim xmlRegions As String
            xmlRegions = _wsLynxNadaWs.GetRegions()

            BindXMLToDropDown(xmlRegions, cboRegion)

            Write(xmlRegions)


        Catch ex As Exception
            Throw
        Finally
            Cursor = Cursors.Arrow
        End Try
    End Sub
    Private Sub GetYears()
        Cursor = Cursors.WaitCursor

        Try

            cboVehicleMake.DataSource = Nothing
            cboVehicleModel.DataSource = Nothing
            cboVehicleBody.DataSource = Nothing

            InitializeWebService()

            Dim xmlYears As String
            xmlYears = _wsLynxNadaWs.GetYears()

            BindXMLToDropDown(xmlYears, cboVehicleYear)

            Write(xmlYears)



        Catch ex As Exception
            MsgBox(ex.Message)
            Write(ex.StackTrace)
        Finally
            Cursor = Cursors.Arrow
        End Try
    End Sub
    Private Sub GetMakes()
        Cursor = Cursors.WaitCursor
        Try

            cboVehicleModel.DataSource = Nothing
            cboVehicleBody.DataSource = Nothing

            InitializeWebService()

            Dim makesXML As String = _wsLynxNadaWs.GetMakes(cboVehicleYear.SelectedValue)

            BindXMLToDropDown(makesXML, cboVehicleMake)

            Write(makesXML)

          

        Catch ex As Exception
            MsgBox(ex.Message)
            Write(ex.StackTrace)
        Finally
            Cursor = Cursors.Arrow
        End Try
    End Sub
    Private Sub GetModels()
        Cursor = Cursors.WaitCursor
        Try

            'If Not IsNumeric(cboVehicleYear.SelectedValue) Then
            '    MsgBox("Please select a year first.")
            '    Exit Sub
            'End If
            'If Not IsNumeric(cboVehicleMake.SelectedValue) Then
            '    MsgBox("Please select a year first.")
            '    Exit Sub
            'End If
            cboVehicleBody.DataSource = Nothing

            InitializeWebService()

            Dim modelsXML As String = _wsLynxNadaWs.GetModels(cboVehicleYear.SelectedValue, cboVehicleMake.SelectedValue)


            BindXMLToDropDown(modelsXML, cboVehicleModel)


            Write(modelsXML)



        Catch ex As Exception
            MsgBox(ex.Message)
            Write(ex.StackTrace)
        Finally
            Cursor = Cursors.Arrow
        End Try
    End Sub
    Private Sub GetBodies()
        Cursor = Cursors.WaitCursor
        Try

            'If Not IsNumeric(cboVehicleYear.SelectedValue) Then
            '    MsgBox("Please select a year first.")
            '    Exit Sub
            'End If
            'If Not IsNumeric(cboVehicleMake.SelectedValue) Then
            '    MsgBox("Please select a Make first.")
            '    Exit Sub
            'End If
            'If Not IsNumeric(cboVehicleBody.SelectedValue) Then
            '    MsgBox("Please select a Model first.")
            '    Exit Sub
            'End If

            InitializeWebService()

            Dim bodiesXML As String = _wsLynxNadaWs.GetBodies(cboVehicleYear.SelectedValue, cboVehicleMake.SelectedValue, cboVehicleModel.SelectedValue)

            BindXMLToDropDown(bodiesXML, cboVehicleBody)

            Write(bodiesXML)

        Catch ex As Exception
            Throw New ApplicationException("Exception Occured")
        Finally
            Cursor = Cursors.Arrow
        End Try
    End Sub
    Private Sub BindXMLToDropDown(ByVal strXML As String, ByRef cboTarget As ComboBox)
        Try
            _bFormLoading = True
            Dim dsTemp As DataSet = GetDatasetFromXMLString(strXML)

            cboTarget.DataSource = dsTemp.Tables(0)
            cboTarget.ValueMember = "Code"
            cboTarget.DisplayMember = "Description"



        Catch ex As Exception
            Throw
        Finally
            _bFormLoading = False
        End Try


    End Sub

    Private Function GetDatasetFromXMLString(ByVal strXMLIn As String, Optional ByVal bindToDataGrid As Boolean = False) As DataSet
        Try
            Dim xmlResult As Xml.XmlDocument = New Xml.XmlDocument
            xmlResult.LoadXml(strXMLIn)
            Dim dsResult As New DataSet()
            dsResult.ReadXml(New XmlNodeReader(xmlResult))

            If bindToDataGrid Then
                dgResults.DataSource = dsResult.Tables(0)
            End If

            Return dsResult

        Catch ex As Exception
            Throw
        End Try

        Return Nothing

    End Function



  
    Private Sub cboVehicleYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVehicleYear.SelectedIndexChanged

        Try
            If _bFormLoading Then Exit Sub

            GetMakes()

        Catch ex As Exception
            MsgBox(ex.Message)
            Write(ex.StackTrace)
        End Try


    End Sub

    Private Sub cboVehicleMake_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVehicleMake.SelectedIndexChanged
        Try
            If _bFormLoading Then Exit Sub

            GetModels()

        Catch ex As Exception
            MsgBox(ex.Message)
            Write(ex.StackTrace)
        End Try


    End Sub

    Private Sub cboVehicleModel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVehicleModel.SelectedIndexChanged
        Try
            If _bFormLoading Then Exit Sub

            GetBodies()

        Catch ex As Exception
            MsgBox(ex.Message)
            Write(ex.StackTrace)
        End Try

    End Sub

    Private Sub btnVinLookup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVinLookup.Click

        Try

            If txtVin.Text = "" Then
                MsgBox("Enter Vin")
                Exit Sub

            End If
            InitializeWebService()

            Dim strXML As String = _wsLynxNadaWs.VinLookup(txtVin.Text)

            Dim dsTemp As DataSet = GetDatasetFromXMLString(strXML, True)

            dgResults.DataSource = dsTemp.Tables(0)



        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub

   
    Private Sub btnUidLookup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUidLookup.Click
        Try

            If txtUid.Text = "" Then
                MsgBox("Enter Unit Id")
                Exit Sub

            End If
            InitializeWebService()

            Dim strXML As String = _wsLynxNadaWs.VinLookup(txtVin.Text)

            Dim dsTemp As DataSet = GetDatasetFromXMLString(strXML, True)

            dgResults.DataSource = dsTemp.Tables(0)



        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub


    Private Sub btnGetAllVehicleData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetAllVehicleData.Click
        Try



        Catch ex As Exception

        End Try
    End Sub
End Class
