﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.IO

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://LynxServices.com/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.None)> _
<ToolboxItem(False)> _
Public Class Service
    Inherits System.Web.Services.WebService

    Dim strSessionToken As String = Nothing

    Private Const CACHE_LOGIN_TOKEN_KEY As String = "LoginToken"

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"

    End Function
    <WebMethod()> _
    Public Function GetYears() As String
        Try

            Return New NadaVehicleHelper().GetYears(GetToken())

        Catch ex As Exception
            Throw

        End Try
    End Function

    <WebMethod()> _
        Public Function GetMakes(ByVal intYear As String) As String
        Try

            Return New NadaVehicleHelper().GetMakes(GetToken(), CInt(intYear))

        Catch ex As Exception
            Throw

        End Try
    End Function

    <WebMethod()> _
       Public Function GetMakesStringArray(ByVal intYear As String) As String
        Try
            Return New NadaVehicleHelper().GetMakesArray(GetToken(), CInt(intYear))
        Catch ex As Exception
            Throw
        End Try
    End Function
    <WebMethod()> _
     Public Function GetModels(ByVal intYear As Integer, ByVal intMakeCode As Integer) As String
        Try

            Return New NadaVehicleHelper().GetModels(GetToken(), intYear, intMakeCode)

        Catch ex As Exception
            Throw

        End Try
    End Function

    <WebMethod()> _
     Public Function GetModelsStringArray(ByVal intYear As Integer, ByVal intMakeCode As Integer) As String
        Try
            Return New NadaVehicleHelper().GetModelsArray(GetToken(), intYear, intMakeCode)
        Catch ex As Exception
            Throw
        End Try
    End Function

    <WebMethod()> _
      Public Function GetBodies(ByVal intYear As Integer, ByVal intMakeCode As Integer, ByVal intSeriesCode As Integer) As String
        Try
            Return New NadaVehicleHelper().GetBodies(GetToken(), intYear, intMakeCode, intSeriesCode)
        Catch ex As Exception
            Throw
        End Try
    End Function
    <WebMethod()> _
     Public Function GetBodiesStringArray(ByVal intYear As Integer, ByVal intMakeCode As Integer, ByVal intSeriesCode As Integer) As String
        Try
            Return New NadaVehicleHelper().GetBodiesArray(GetToken(), intYear, intMakeCode, intSeriesCode)
        Catch ex As Exception
            Throw
        End Try
    End Function
    <WebMethod()> _
  Public Function GetDefaultvehicleValyeByVin(ByVal VinValue As String, ByVal region As Integer, ByVal Mileage As Integer) As String
        Try

            Return New NadaVehicleHelper().GetDefaultvehicleValyeByVin(GetToken(), VinValue, region, Mileage)

        Catch ex As Exception
            Throw

        End Try
    End Function
    <WebMethod()> _
  Public Function getMsrpVehicleAndValueByVin(ByVal VinValue As String, ByVal region As Integer, ByVal Mileage As Integer, ByVal Msrp As Integer) As String
        Try

            Return New NadaVehicleHelper().getMsrpVehicleAndValueByVin(GetToken(), VinValue, region, Mileage, Msrp)

        Catch ex As Exception
            Throw

        End Try
    End Function


    <WebMethod()> _
Public Function getLowVehicleAndValueByVin(ByVal VinValue As String, ByVal region As Integer, ByVal Mileage As Integer) As String
        Try

            Return New NadaVehicleHelper().getLowVehicleAndValueByVin(GetToken(), VinValue, region, Mileage)

        Catch ex As Exception
            Throw

        End Try
    End Function
    <WebMethod()> _
Public Function getMileageAdj(ByVal vehicleUid As Integer, ByVal region As Integer) As String
        Try

            Return New NadaVehicleHelper().getMileageAdj(GetToken(), vehicleUid, region)

        Catch ex As Exception
            Throw

        End Try
    End Function

    <WebMethod()> _
Public Function getExclusiveAccessories(ByVal vehicleUid As Integer) As String
        Try

            Return New NadaVehicleHelper().getExclusiveAccessories(GetToken(), vehicleUid)

        Catch ex As Exception
            Throw

        End Try
    End Function

    <WebMethod()> _
Public Function getInclusiveAccessories(ByVal vehicleUid As Integer) As String
        Try

            Return New NadaVehicleHelper().getInclusiveAccessories(GetToken(), vehicleUid)

        Catch ex As Exception
            Throw

        End Try
    End Function


    <WebMethod()> _
 Public Function getHighVehicleAndValueByVin(ByVal VinValue As String, ByVal region As Integer, ByVal Mileage As Integer) As String
        Try

            Return New NadaVehicleHelper().getHighVehicleAndValueByVin(GetToken(), VinValue, region, Mileage)

        Catch ex As Exception
            Throw

        End Try
    End Function

    <WebMethod()> _
Public Function getExperianAutoCheckReport(ByVal Vin As String, ByVal ExperianAccountNumber As String, ByVal reportType As String) As String
        Try

            Return New NadaVehicleHelper().getExperianAutoCheckReport(GetToken(), Vin, ExperianAccountNumber, reportType)

        Catch ex As Exception
            Throw

        End Try
    End Function




    <WebMethod()> _
    Public Function VinLookup(ByVal strVin As String) As String
        Try

            Return New NadaVehicleHelper().GetVehiclesByVin(GetToken(), strVin)

        Catch ex As Exception
            Throw

        End Try
    End Function
    <WebMethod()> _
    Public Function VinLookupArray(ByVal strVin As String) As String
        Try

            Return New NadaVehicleHelper().GetVehiclesByVinArray(GetToken(), strVin)

        Catch ex As Exception
            Throw

        End Try
    End Function

    <WebMethod()> _
   Public Function GetRegions() As String
        Try

            Return New NadaVehicleHelper().GetRegions(GetToken())

        Catch ex As Exception
            Throw

        End Try
    End Function
    <WebMethod()> _
   Public Function GetRegionsArray() As String
        Try

            Return New NadaVehicleHelper().GetRegionsArray(GetToken())

        Catch ex As Exception
            Throw

        End Try
    End Function
    <WebMethod()> _
    Public Function GetAccessories(ByVal intUid As Integer, ByVal strVin As String, ByVal intRegion As Integer) As String
        Try

            Return New NadaVehicleHelper().GetAccessories(GetToken(), intUid, strVin, intRegion)

        Catch ex As Exception
            Throw

        End Try
    End Function

    <WebMethod()> _
    Public Function GetBaseVehicleValueByUid(ByVal intUid As Integer, ByVal intRegion As Integer, ByVal intMileage As Integer) As String
        Try

            Return New NadaVehicleHelper().GetBaseVehicleValueByUid(GetToken(), intUid, intRegion, intMileage)

        Catch ex As Exception
            Throw

        End Try
    End Function
    <WebMethod()> _
   Public Function GetVehicleValueByUid(ByVal intUid As Integer, ByVal intRegion As Integer, ByVal intMileage As Integer) As String
        Try

            Return New NadaVehicleHelper().GetVehicleValueByUid(GetToken(), intUid, intRegion, intMileage)

        Catch ex As Exception
            Throw

        End Try
    End Function
    <WebMethod()> _
    Public Function GetVehicleByUid(ByVal intUid As Integer) As String
        Try

            Return New NadaVehicleHelper().GetVehicleByUid(GetToken(), intUid)

        Catch ex As Exception
            Throw

        End Try
    End Function
    <WebMethod()> _
    Public Function getVehicleAndValueByUid(ByVal intUid As Integer, ByVal Region As Integer, ByVal Mileage As Integer) As String
        Try

            Return New NadaVehicleHelper().getVehicleAndValueByUid(GetToken(), intUid, Region, Mileage)

        Catch ex As Exception
            Throw

        End Try
    End Function
    <WebMethod()> _
    Public Function getVehicleByVic(ByVal vehicleVic As String) As String
        Try

            Return New NadaVehicleHelper().getVehicleByVic(GetToken(), vehicleVic)

        Catch ex As Exception
            Throw

        End Try
    End Function


    <WebMethod()> _
    Public Function getTotalAdjFloorValues(ByVal intUid As Integer, ByVal intRegion As Integer, ByVal intMileage As Integer) As String
        Try

            Return New NadaVehicleHelper().getTotalAdjFloorValues(GetToken(), intUid, intRegion, intMileage)

        Catch ex As Exception
            Throw

        End Try
    End Function



    <WebMethod()> _
    Public Function GetToken() As String
        'This should not be exposed as a web method, and it should be private
        Dim strToken As String = ""
        Try
            If Context.Cache.Item(CACHE_LOGIN_TOKEN_KEY) Is Nothing Then
                strToken = New NadaLoginHelper().GetToken()

                Context.Cache.Add(CACHE_LOGIN_TOKEN_KEY, strToken, Nothing, DateTime.MaxValue, _
                                  System.TimeSpan.FromMinutes(30), CacheItemPriority.High, Nothing)
            Else
                strToken = Context.Cache.Item(CACHE_LOGIN_TOKEN_KEY).ToString

            End If

            'If Context.Cache.it Then
            'If Context.Cache.Get("

        Catch ex As Exception
            Throw
        End Try

        Return strToken

    End Function

    <WebMethod()> _
    Public Function GetAllAsXML(ByVal strTokenId As String, ByVal strRegion As String, ByVal strRegionDesc As String, ByVal strYear As String, _
                            ByVal strMake As String, ByVal strModel As String, ByVal strBody As String, _
                            ByVal strMileage As String, ByVal strVIN As String, ByVal strVehId As String, _
                            ByVal strAction As String, ByVal strZip As String, ByVal strReadOnly As String, ByVal strRegionList As String) As String

        'Dim FILENAME As String = "C:\IntranetSites\APD\webroot\test2.txt"
        'Dim objStreamWriter As StreamWriter
        'objStreamWriter = File.AppendText(FILENAME)
        'objStreamWriter.WriteLine(strRegionList)
        'objStreamWriter.WriteLine("---------------")
        'objStreamWriter.Close()


        ', objFile, strFilePath
        '   strFilePath = "C:\IntranetSites\APD\webroot\test1.txt"

        '   objFSO = Server.CreateObject("Scripting.FileSystemObject")
        '   objFile = objFSO.OpenTextFile(strFilePath, 8, True)
        Dim vehHelper As New NadaVehicleHelper
        Dim strReturnValue As String

        Try
            strReturnValue = vehHelper.GetAllAsXML(GetToken(), strRegion, strRegionDesc, strYear, strMake, strModel, strBody, strMileage, strVIN, strVehId, strAction, strZip, strReadOnly, strRegionList)
            'vehHelper.SendMail("NADA Service", vehHelper.FrameMailBody("", strReturnValue, String.Concat("strYear:", strYear, ",strMake:", strMake, ",strModel:", strModel, ",strBody:", strBody, ",strRegionList:", strRegionList)))
            Return strReturnValue
            'Return New NadaVehicleHelper().GetAllAsXML(GetToken(), strRegion, strRegionDesc, strYear, strMake, strModel, strBody, strMileage, strVIN, strVehId, strAction, strZip, strReadOnly, strRegionList)
        Catch ex As Exception
            'vehHelper.SendMail("NADA Service", vehHelper.FrameMailBody("", ex.Message, String.Concat("strYear:", strYear, ",strMake:", strMake, ",strModel:", strModel, ",strBody:", strBody, ",strRegionList:", strRegionList)))
            Return "Exception" & ex.Message & "RegionDesc: " & strRegionList
        End Try
    End Function

    <WebMethod()> _
    Public Function TEstXMl(ByVal value1 As String) As String

        ''Dim MyCompanyInfo
        ''Dim MyCompanyInfo_numRows
        ''On Error Resume Next
        ''MyCompanyInfo = Server.CreateObject("ADODB.Recordset")
        ' ''Response.Write(TypeName(MyCompanyInfo))
        ' ''Response.Write(Err.Description)
        ''MyCompanyInfo.ActiveConnection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\MDB\Main_Database.mdb;Persist Security Info=False"
        ''MyCompanyInfo.Source = "SELECT * FROM MyCompanyInformation"
        ''MyCompanyInfo.CursorType = 0
        ''MyCompanyInfo.CursorLocation = 2
        ''MyCompanyInfo.LockType = 1
        ''MyCompanyInfo.Open()
        ' ''response.write("<br>" & MM_Connect_File_STRING & "<br>")
        ' ''response.write("error" & Err.Description & "<br>")
        ''MyCompanyInfo_numRows = 0
        ' ''MyCompanyInfo.Fields.Item("CompanyName").Value
    End Function

    <WebMethod()> _
   Public Function GetFNOLXML(ByVal FNOLXml As String) As String
        Return New NadaVehicleHelper().FNOLVehicleInfo(GetToken(), FNOLXml)
    End Function

    <WebMethod()> _
    Public Function GetAccessoriesList(ByVal intVehicleID As String, ByVal strVin As String, ByVal intRegion As String) As String
        Return New NadaVehicleHelper().GetAccessoriesList(GetToken(), intVehicleID, strVin, intRegion)
    End Function
End Class