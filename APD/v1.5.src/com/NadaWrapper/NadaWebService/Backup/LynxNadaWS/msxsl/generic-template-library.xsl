<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:msxsl="urn:schemas-microsoft-com:xslt"  xmlns:user="http://mycompany.com/mynamespace">
<xsl:import href="msxsl-function-library.xsl"/>
  <!-- Required
    Desc: Adds 'field required' graphic next to controls.
    Params: Nullable - "YES" or "No"
  -->
  <xsl:template name="Required">
    <xsl:param name="IsNullable"/>

    <xsl:choose>
      <xsl:when test="starts-with($IsNullable,'YES')">
        <img height="11" width="10" src="/images/blank.gif"/>
      </xsl:when>
      <xsl:otherwise>
        <img height="11" width="10" src="/images/requiredW.gif"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- RequiredLookup
    Desc: Adds 'field required' graphic next to controls.
    Params: Name - field name to do Column lookup with.
  -->
  <xsl:template name="RequiredLookup">
    <xsl:param name="Name"/>

    <xsl:choose>
      <xsl:when test="Column[@Name=$Name]/@Nullable='YES'">
        <img height="11" width="10" src="/images/blank.gif"/>
      </xsl:when>
      <xsl:when test="Column[@Name=$Name]/@Nullable='No '">
        <img height="11" width="10" src="/images/requiredW.gif"/>
      </xsl:when>
      <xsl:when test="preceding-sibling::Column[@Name=$Name]/@Nullable='YES'">
        <img height="11" width="10" src="/images/blank.gif"/>
      </xsl:when>
      <xsl:otherwise>
        <img height="11" width="10" src="/images/requiredW.gif"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- NotRequired
    Desc: Adds blank graphic next to controls.  Used to keep spacing with
      other controls that use the Required template above.
  -->
  <xsl:template name="NotRequired">
    <img height="11" width="10" src="/images/blank.gif"/>
  </xsl:template>

  <!-- Input
    Desc: Adds an input box and stuffs attributes automatically from the
      database schema information.  Assumes that the value is in an attribute
      of the current context, and that the Column information is stored either
      as a child element of the current context or as a preceding sibling of
      the current context.  Will name the input box automatically using the
      XML XPath, separating ancestor names by '|'.
    Params: Name (Required) - The field name to use for selects.
      Size (Optional) - The input box size.  The default is extracted from
        the MaxLength attribute of the Column data.
      Type (Optional) - The Input box data type.
      Style (Optional) - The Input box style.
      Class (Optional) - The Input box class.
  -->
  <xsl:template name="Input">
    <xsl:param name="Name"/>
    <xsl:param name="Size"/>
    <xsl:param name="Type">text</xsl:param>
    <xsl:param name="Style">text-align: left</xsl:param>
    <xsl:param name="Class">fieldInput</xsl:param>

    <xsl:variable name="ParentPath" select="user:GetNodeListNames(ancestor-or-self::*)"/>
    <xsl:variable name="Path" select="concat($ParentPath,concat('_',$Name))"/>

    <input>
      <xsl:choose>
        <xsl:when test="Column[@Name=$Name]/@DataType='varchar'">
          <xsl:attribute name="maxlength"><xsl:value-of select="Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
          <xsl:attribute name="size"><xsl:value-of select="Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
        </xsl:when>
        <xsl:when test="Column[@Name=$Name]/@DataType='char'">
          <xsl:attribute name="maxlength"><xsl:value-of select="Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
          <xsl:attribute name="size"><xsl:value-of select="Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
        </xsl:when>
        <xsl:when test="preceding-sibling::Column[@Name=$Name]/@DataType='varchar'">
          <xsl:attribute name="maxlength"><xsl:value-of select="preceding-sibling::Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
          <xsl:attribute name="size"><xsl:value-of select="preceding-sibling::Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
        </xsl:when>
        <xsl:when test="preceding-sibling::Column[@Name=$Name]/@DataType='char'">
          <xsl:attribute name="maxlength"><xsl:value-of select="preceding-sibling::Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
          <xsl:attribute name="size"><xsl:value-of select="preceding-sibling::Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
        </xsl:when>
      </xsl:choose>

      <xsl:if test="$Size!=''">
        <xsl:attribute name="size"><xsl:value-of select="$Size"/></xsl:attribute>
      </xsl:if>

      <xsl:attribute name="class"><xsl:value-of select="$Class"/></xsl:attribute>
      <xsl:attribute name="style"><xsl:value-of select="$Style"/></xsl:attribute>
      <xsl:attribute name="name"><xsl:value-of select="$Path"/></xsl:attribute>
      <xsl:attribute name="id"><xsl:value-of select="$Path"/></xsl:attribute>
      <xsl:attribute name="type"><xsl:value-of select="$Type"/></xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="@*[name()=$Name]"/></xsl:attribute>
    </input>
  </xsl:template>

  <!-- RequiredInput
    Same as Input, but handles putting the "Required" graphic in front.
  -->
  <xsl:template name="RequiredInput">
    <xsl:param name="Name"/>
    <xsl:param name="Size"/>
    <xsl:param name="Type">text</xsl:param>
    <xsl:param name="Style">text-align: left</xsl:param>
    <xsl:param name="Class">fieldInput</xsl:param>

    <xsl:variable name="ParentPath" select="user:GetNodeListNames(ancestor-or-self::*)"/>
    <xsl:variable name="Path" select="concat($ParentPath,concat('_',$Name))"/>

    <xsl:choose>
      <xsl:when test="Column[@Name=$Name]/@Nullable='YES'">
        <img height="11" width="10" src="/images/blank.gif"/>
      </xsl:when>
      <xsl:when test="preceding-sibling::Column[@Name=$Name]/@Nullable='YES'">
        <img height="11" width="10" src="/images/blank.gif"/>
      </xsl:when>
      <xsl:otherwise>
        <img height="11" width="10" src="/images/requiredW.gif"/>
      </xsl:otherwise>
    </xsl:choose>

    <input>
      <xsl:choose>
        <xsl:when test="Column[@Name=$Name]/@DataType='varchar'">
          <xsl:attribute name="maxlength"><xsl:value-of select="Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
          <xsl:attribute name="size"><xsl:value-of select="Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
        </xsl:when>
        <xsl:when test="Column[@Name=$Name]/@DataType='char'">
          <xsl:attribute name="maxlength"><xsl:value-of select="Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
          <xsl:attribute name="size"><xsl:value-of select="Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
        </xsl:when>
        <xsl:when test="preceding-sibling::Column[@Name=$Name]/@DataType='varchar'">
          <xsl:attribute name="maxlength"><xsl:value-of select="preceding-sibling::Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
          <xsl:attribute name="size"><xsl:value-of select="preceding-sibling::Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
        </xsl:when>
        <xsl:when test="preceding-sibling::Column[@Name=$Name]/@DataType='char'">
          <xsl:attribute name="maxlength"><xsl:value-of select="preceding-sibling::Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
          <xsl:attribute name="size"><xsl:value-of select="preceding-sibling::Column[@Name=$Name]/@MaxLength"/></xsl:attribute>
        </xsl:when>
      </xsl:choose>

      <xsl:if test="$Size!=''">
        <xsl:attribute name="size"><xsl:value-of select="$Size"/></xsl:attribute>
      </xsl:if>

      <xsl:attribute name="class"><xsl:value-of select="$Class"/></xsl:attribute>
      <xsl:attribute name="style"><xsl:value-of select="$Style"/></xsl:attribute>
      <xsl:attribute name="name"><xsl:value-of select="$Path"/></xsl:attribute>
      <xsl:attribute name="id"><xsl:value-of select="$Path"/></xsl:attribute>
      <xsl:attribute name="type"><xsl:value-of select="$Type"/></xsl:attribute>
      <xsl:attribute name="value"><xsl:value-of select="@*[name()=$Name]"/></xsl:attribute>
    </input>
  </xsl:template>

  <!-- SetPathName
    Desc: Sets the name and id attributes to be equal to the current context
      path plus the passed name.
  -->
  <xsl:template name="SetPathName">
    <xsl:param name="Name"/>

    <xsl:variable name="ParentPath" select="user:GetNodeListNames(ancestor-or-self::*)"/>
    <xsl:variable name="Path" select="concat($ParentPath,concat('_',$Name))"/>

    <xsl:attribute name="name"><xsl:value-of select="$Path"/></xsl:attribute>
    <xsl:attribute name="id"><xsl:value-of select="$Path"/></xsl:attribute>
  </xsl:template>

  <!-- YesNoRadioButtons
    Desc: Creates a set of Yes/No Radio buttons.
  -->
  <xsl:template name="YesNoRadioButtons">
    <xsl:param name="Name"/>

    <xsl:variable name="ParentPath" select="user:GetNodeListNames(ancestor-or-self::*)"/>
    <xsl:variable name="Path" select="concat($ParentPath,concat('_',$Name))"/>

    <input type="radio" value="1" class="fieldinput">
      <xsl:if test="@*[name()=$Name]='1'"><xsl:attribute name="checked"/></xsl:if>
      <xsl:attribute name="name"><xsl:value-of select="$Path"/></xsl:attribute>
      <xsl:attribute name="id"><xsl:value-of select="$Path"/></xsl:attribute>
    </input>Yes

    <input type="radio" value="0" class="fieldinput">
      <xsl:if test="@*[name()=$Name]='0'"><xsl:attribute name="checked"/></xsl:if>
      <xsl:attribute name="name"><xsl:value-of select="$Path"/></xsl:attribute>
      <xsl:attribute name="id"><xsl:value-of select="$Path"/></xsl:attribute>
    </input>No
  </xsl:template>

  <!-- States
    Desc: Adds state options to a select.  This template only contains U.S.
      states - see StatesPlus below for additional regions.
    Params: selectedState - State value to be selected.
  -->
  <xsl:template name="States">
    <xsl:param name="selectedState"></xsl:param>
    <option value=""></option>
    <option value="AK">
      <xsl:if test="$selectedState='AK'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>AK</option>
    <option value="AL">
      <xsl:if test="$selectedState='AL'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>AL</option>
    <option value="AR">
      <xsl:if test="$selectedState='AR'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>AR</option>
    <option value="AZ">
      <xsl:if test="$selectedState='AZ'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>AZ</option>
    <option value="CA">
      <xsl:if test="$selectedState='CA'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>CA</option>
    <option value="CO">
      <xsl:if test="$selectedState='CO'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>CO</option>
    <option value="CT">
      <xsl:if test="$selectedState='CT'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>CT</option>
    <option value="DC">
      <xsl:if test="$selectedState='DC'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>DC</option>
    <option value="DE">
      <xsl:if test="$selectedState='DE'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>DE</option>
    <option value="FL">
      <xsl:if test="$selectedState='FL'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>FL</option>
    <option value="GA">
      <xsl:if test="$selectedState='GA'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>GA</option>
    <option value="HI">
      <xsl:if test="$selectedState= 'HI'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>HI</option>
    <option value="IA">
      <xsl:if test="$selectedState= 'IA'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>IA</option>
    <option value="ID">
      <xsl:if test="$selectedState= 'ID'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>ID</option>
    <option value="IL">
      <xsl:if test="$selectedState= 'IL'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      IL</option>
    <option value="IN">
      <xsl:if test="$selectedState= 'IN'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      IN</option>
    <option value="KS">
      <xsl:if test="$selectedState= 'KS'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      KS</option>
    <option value="KY">
      <xsl:if test="$selectedState= 'KY'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      KY</option>
    <option value="LA">
      <xsl:if test="$selectedState= 'LA'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      LA</option>
    <option value="MA">
      <xsl:if test="$selectedState= 'MA'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      MA</option>
    <option value="MD">
      <xsl:if test="$selectedState= 'MD'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      MD</option>
    <option value="ME">
      <xsl:if test="$selectedState= 'ME'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      ME</option>
    <option value="MI">
      <xsl:if test="$selectedState= 'MI'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      MI</option>
    <option value="MN">
      <xsl:if test="$selectedState= 'MN'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      MN</option>
    <option value="MO">
      <xsl:if test="$selectedState= 'MO'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      MO</option>
    <option value="MS">
      <xsl:if test="$selectedState= 'MS'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      MS</option>
    <option value="MT">
      <xsl:if test="$selectedState= 'MT'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      MT</option>
    <option value="NC">
      <xsl:if test="$selectedState= 'NC'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      NC</option>
    <option value="ND">
      <xsl:if test="$selectedState= 'ND'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      ND</option>
    <option value="NE">
      <xsl:if test="$selectedState= 'NE'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      NE</option>
    <option value="NH">
      <xsl:if test="$selectedState= 'NH'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      NH</option>
    <option value="NJ">
      <xsl:if test="$selectedState= 'NJ'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      NJ</option>
    <option value="NM">
      <xsl:if test="$selectedState= 'NM'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      NM</option>
    <option value="NV">
      <xsl:if test="$selectedState= 'NV'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      NV</option>
    <option value="NY">
      <xsl:if test="$selectedState= 'NY'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      NY</option>
    <option value="OH">
      <xsl:if test="$selectedState= 'OH'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      OH</option>
    <option value="OK">
      <xsl:if test="$selectedState= 'OK'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      OK</option>
    <option value="OR">
      <xsl:if test="$selectedState= 'OR'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      OR</option>
    <option value="PA">
      <xsl:if test="$selectedState= 'PA'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      PA</option>
    <option value="PR">
      <xsl:if test="$selectedState= 'PR'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      PR</option>
    <option value="RI">
      <xsl:if test="$selectedState= 'RI'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      RI</option>
    <option value="SC">
      <xsl:if test="$selectedState= 'SC'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      SC</option>
    <option value="SD">
      <xsl:if test="$selectedState= 'SD'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      SD</option>
    <option value="TN">
      <xsl:if test="$selectedState= 'TN'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      TN</option>
    <option value="TX">
      <xsl:if test="$selectedState= 'TX'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      TX</option>
    <option value="UT">
      <xsl:if test="$selectedState= 'UT'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      UT</option>
    <option value="VA">
      <xsl:if test="$selectedState= 'VA'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      VA</option>
    <option value="VT">
      <xsl:if test="$selectedState= 'VT'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      VT</option>
    <option value="WA">
      <xsl:if test="$selectedState= 'WA'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      WA</option>
    <option value="WI">
      <xsl:if test="$selectedState= 'WI'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      WI</option>
    <option value="WV">
      <xsl:if test="$selectedState= 'WV'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      WV</option>
    <option value="WY">
      <xsl:if test="$selectedState= 'WY'">
        <xsl:attribute name="selected"></xsl:attribute>
      </xsl:if>
      WY</option>
  </xsl:template>

  <!-- StatesPlus
    Desc: Adds state options to a select.  Includes additional regions.
    Params: selectedState - State value to be selected.
  -->
  <xsl:template name="StatesPlus">
    <xsl:param name="selectedState"/>
    <option value=""/>
    <option value="'XX'">
      <xsl:if test="$selectedState='XX'">
        <xsl:attribute name="selected"/>
      </xsl:if>Outside US / Canada</option>
    <option value="AL">
      <xsl:if test="$selectedState='AL'">
        <xsl:attribute name="selected"/>
      </xsl:if>Alabama</option>
    <option value="AK">
      <xsl:if test="$selectedState='AK'">
        <xsl:attribute name="selected"/>
      </xsl:if>Alaska</option>
    <option value="AB">
      <xsl:if test="$selectedState='AB'">
        <xsl:attribute name="selected"/>
      </xsl:if>Alberta</option>
    <option value="AS">
      <xsl:if test="$selectedState='AS'">
        <xsl:attribute name="selected"/>
      </xsl:if>American Samoa</option>
    <option value="AZ">
      <xsl:if test="$selectedState='AZ'">
        <xsl:attribute name="selected"/>
      </xsl:if>Arizona</option>
    <option value="AR">
      <xsl:if test="$selectedState='AR'">
        <xsl:attribute name="selected"/>
      </xsl:if>Arkansas</option>
    <option value="AA">
      <xsl:if test="$selectedState='AA'">
        <xsl:attribute name="selected"/>
      </xsl:if>Armed Forces Americas</option>
    <option value="AE">
      <xsl:if test="$selectedState='AE'">
        <xsl:attribute name="selected"/>
      </xsl:if>Armed Forces Europe</option>
    <option value="AP">
      <xsl:if test="$selectedState='AP'">
        <xsl:attribute name="selected"/>
      </xsl:if>Armed Forces Pacific</option>
    <option value="BC">
      <xsl:if test="$selectedState='BC'">
        <xsl:attribute name="selected"/>
      </xsl:if>British Columbia</option>
    <option value="CA">
      <xsl:if test="$selectedState='CA'">
        <xsl:attribute name="selected"/>
      </xsl:if>California</option>
    <option value="CO">
      <xsl:if test="$selectedState='CO'">
        <xsl:attribute name="selected"/>
      </xsl:if>Colorado</option>
    <option value="CT">
      <xsl:if test="$selectedState='CT'">
        <xsl:attribute name="selected"/>
      </xsl:if>Connecticut</option>
    <option value="DE">
      <xsl:if test="$selectedState='DE'">
        <xsl:attribute name="selected"/>
      </xsl:if>Deleaware</option>
    <option value="DC">
      <xsl:if test="$selectedState='DC'">
        <xsl:attribute name="selected"/>
      </xsl:if>District of Columbia</option>
    <option value="FL">
      <xsl:if test="$selectedState='FL'">
        <xsl:attribute name="selected"/>
      </xsl:if>Florida</option>
    <option value="GA">
      <xsl:if test="$selectedState='GA'">
        <xsl:attribute name="selected"/>
      </xsl:if>Georgia</option>
    <option value="GU">
      <xsl:if test="$selectedState= 'GU'">
        <xsl:attribute name="selected"/>
      </xsl:if>Guam</option>
    <option value="HI">
      <xsl:if test="$selectedState= 'HI'">
        <xsl:attribute name="selected"/>
      </xsl:if>Hawaii</option>
    <option value="ID">
      <xsl:if test="$selectedState= 'ID'">
        <xsl:attribute name="selected"/>
      </xsl:if>Idaho</option>
    <option value="IL">
      <xsl:if test="$selectedState= 'IL'">
        <xsl:attribute name="selected"/>
      </xsl:if>Illinois</option>
    <option value="IN">
      <xsl:if test="$selectedState= 'IN'">
        <xsl:attribute name="selected"/>
      </xsl:if>Indiana</option>
    <option value="IA">
      <xsl:if test="$selectedState= 'IA'">
        <xsl:attribute name="selected"/>
      </xsl:if>Iowa</option>
    <option value="KS">
      <xsl:if test="$selectedState= 'KS'">
        <xsl:attribute name="selected"/>
      </xsl:if>Kansas</option>
    <option value="KY">
      <xsl:if test="$selectedState= 'KY'">
        <xsl:attribute name="selected"/>
      </xsl:if>Kentucky</option>
    <option value="LA">
      <xsl:if test="$selectedState= 'LA'">
        <xsl:attribute name="selected"/>
      </xsl:if>Louisiana</option>
    <option value="ME">
      <xsl:if test="$selectedState= 'ME'">
        <xsl:attribute name="selected"/>
      </xsl:if>Maine</option>
    <option value="MB">
      <xsl:if test="$selectedState= 'MB'">
        <xsl:attribute name="selected"/>
      </xsl:if>Manitoba</option>
    <option value="MD">
      <xsl:if test="$selectedState= 'MD'">
        <xsl:attribute name="selected"/>
      </xsl:if>Maryland</option>
    <option value="MA">
      <xsl:if test="$selectedState= 'MA'">
        <xsl:attribute name="selected"/>
      </xsl:if>Massachusetts</option>
    <option value="MI">
      <xsl:if test="$selectedState= 'MI'">
        <xsl:attribute name="selected"/>
      </xsl:if>Michigan</option>
    <option value="MN">
      <xsl:if test="$selectedState= 'MN'">
        <xsl:attribute name="selected"/>
      </xsl:if>Minnesota</option>
    <option value="MS">
      <xsl:if test="$selectedState= 'MS'">
        <xsl:attribute name="selected"/>
      </xsl:if>Mississippi</option>
    <option value="MO">
      <xsl:if test="$selectedState= 'MO'">
        <xsl:attribute name="selected"/>
      </xsl:if>
      Missouri</option>
    <option value="MT">
      <xsl:if test="$selectedState= 'MT'">
        <xsl:attribute name="selected"/>
      </xsl:if>
      Montana</option>
    <option value="NE">
      <xsl:if test="$selectedState= 'NE'">
        <xsl:attribute name="selected"/>
      </xsl:if>
      Nebraska</option>
    <option value="NV">
      <xsl:if test="$selectedState= 'NV'">
        <xsl:attribute name="selected"/>
      </xsl:if>
      Nevada</option>
    <option value="NB">
      <xsl:if test="$selectedState= 'NB'">
        <xsl:attribute name="selected"/>
      </xsl:if>New Brunswick</option>
    <option value="NH">
      <xsl:if test="$selectedState= 'NH'">
        <xsl:attribute name="selected"/>
      </xsl:if>New Hampshire</option>
    <option value="NJ">
      <xsl:if test="$selectedState= 'NJ'">
        <xsl:attribute name="selected"/>
      </xsl:if>New Jersey</option>
    <option value="NM">
      <xsl:if test="$selectedState= 'NM'">
        <xsl:attribute name="selected"/>
      </xsl:if>New Mexico</option>
    <option value="NY">
      <xsl:if test="$selectedState= 'NY'">
        <xsl:attribute name="selected"/>
      </xsl:if>New York</option>
    <option value="NF">
      <xsl:if test="$selectedState= 'NF'">
        <xsl:attribute name="selected"/>
      </xsl:if>Newfoundland</option>
    <option value="NC">
      <xsl:if test="$selectedState= 'NC'">
        <xsl:attribute name="selected"/>
      </xsl:if>North Carolina</option>
    <option value="ND">
      <xsl:if test="$selectedState= 'ND'">
        <xsl:attribute name="selected"/>
      </xsl:if>North Dakota</option>
    <option value="MP">
      <xsl:if test="$selectedState= 'MP'">
        <xsl:attribute name="selected"/>
      </xsl:if>Northern Mariana Is</option>
    <option value="NT">
      <xsl:if test="$selectedState= 'NT'">
        <xsl:attribute name="selected"/>
      </xsl:if>Northwest Territories</option>
    <option value="NS">
      <xsl:if test="$selectedState= 'NS'">
        <xsl:attribute name="selected"/>
      </xsl:if>Nova Scotia</option>
    <option value="OH">
      <xsl:if test="$selectedState= 'OH'">
        <xsl:attribute name="selected"/>
      </xsl:if>Ohio</option>
    <option value="OK">
      <xsl:if test="$selectedState= 'OK'">
        <xsl:attribute name="selected"/>
      </xsl:if>Oklahoma</option>
    <option value="ON">
      <xsl:if test="$selectedState= 'ON'">
        <xsl:attribute name="selected"/>
      </xsl:if>Ontario</option>
    <option value="OR">
      <xsl:if test="$selectedState= 'OR'">
        <xsl:attribute name="selected"/>
      </xsl:if>Oregon</option>
    <option value="PW">
      <xsl:if test="$selectedState= 'PW'">
        <xsl:attribute name="selected"/>
      </xsl:if>Palau</option>
    <option value="PA">
      <xsl:if test="$selectedState= 'PA'">
        <xsl:attribute name="selected"/>
      </xsl:if>Pennsylvania</option>
    <option value="PE">
      <xsl:if test="$selectedState= 'PE'">
        <xsl:attribute name="selected"/>
      </xsl:if>Prince Edward Island</option>
    <option value="PQ">
      <xsl:if test="$selectedState= 'PQ'">
        <xsl:attribute name="selected"/>
      </xsl:if>Province du Quebec</option>
    <option value="PR">
      <xsl:if test="$selectedState= 'PR'">
        <xsl:attribute name="selected"/>
      </xsl:if>
      Puerto Rico</option>
    <option value="RI">
      <xsl:if test="$selectedState= 'RI'">
        <xsl:attribute name="selected"/>
      </xsl:if>Rhode Island</option>
    <option value="SK">
      <xsl:if test="$selectedState= 'SK'">
        <xsl:attribute name="selected"/>
      </xsl:if>Saskatchewan</option>
    <option value="SC">
      <xsl:if test="$selectedState= 'SC'">
        <xsl:attribute name="selected"/>
      </xsl:if>South Carolina</option>
    <option value="SD">
      <xsl:if test="$selectedState= 'SD'">
        <xsl:attribute name="selected"/>
      </xsl:if>South Dakota</option>
    <option value="TN">
      <xsl:if test="$selectedState= 'TN'">
        <xsl:attribute name="selected"/>
      </xsl:if>Tennessee</option>
    <option value="TX">
      <xsl:if test="$selectedState= 'TX'">
        <xsl:attribute name="selected"/>
      </xsl:if>Texas</option>
    <option value="UT">
      <xsl:if test="$selectedState= 'UT'">
        <xsl:attribute name="selected"/>
      </xsl:if>Utah</option>
    <option value="VT">
      <xsl:if test="$selectedState= 'VT'">
        <xsl:attribute name="selected"/>
      </xsl:if>Vermont</option>
    <option value="VI">
      <xsl:if test="$selectedState= 'VI'">
        <xsl:attribute name="selected"/>
      </xsl:if>Virgin Islands</option>
    <option value="VA">
      <xsl:if test="$selectedState= 'VA'">
        <xsl:attribute name="selected"/>
      </xsl:if>Virginia</option>
    <option value="WA">
      <xsl:if test="$selectedState= 'WA'">
        <xsl:attribute name="selected"/>
      </xsl:if>Washington</option>
    <option value="WV">
      <xsl:if test="$selectedState= 'WV'">
        <xsl:attribute name="selected"/>
      </xsl:if>West Virginia</option>
    <option value="WI">
      <xsl:if test="$selectedState= 'WI'">
        <xsl:attribute name="selected"/>
      </xsl:if>Wisconsin</option>
    <option value="WY">
      <xsl:if test="$selectedState= 'WY'">
        <xsl:attribute name="selected"/>
      </xsl:if>Wyoming</option>
    <option value="YT">
      <xsl:if test="$selectedState= 'YT'">
        <xsl:attribute name="selected"/>
      </xsl:if>Yukon Territory</option>
  </xsl:template>
  
  <!-- Build an option list for a Select box from xml reference data.  Set the contact by wrapping call to
    this template in a for-each loop.  The parameter 'current' represents the option that should be 
	currently selected.  If you need a blank option, add it manually. -->	   
  <xsl:template name="BuildSelectOptions">
	<xsl:param name="current"/>
	<option>
		<xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
		
		<xsl:if test="@ReferenceID = $current">
			<xsl:attribute name="selected"/>
		</xsl:if>
		
		<xsl:value-of select="@Name"/>	
	</option>
  </xsl:template>
  
  <!-- Builds a set of phone number input boxes.  The ContactMethod parameter should be sent one of the following values: 
     'Phone', 'Fax', 'Cell', 'Pager'.  The parameter will be used to dynamically build the 'name' and 'id' attributes of the 
	 input box as well as the field name to be passed to the 'phoneAutoTab function.  Lastly the parameter will determine what value
	 from the XML will be used to populate the input box.  -->
	 
<xsl:template name="ContactNumbers">
  <xsl:param name="ContactMethod"/>
  <xsl:param name="ContactNumber"/> <!-- Integer to add to the end of the id/names when multiple sets of contact info exist within a page -->

  <input maxlength="3" size="2" onbeforedeactivate="checkAreaCode(this);" class="inputFld">
	<xsl:attribute name="id">txt<xsl:value-of select="$ContactMethod"/>AreaCode<xsl:value-of select="$ContactNumber"/></xsl:attribute>  
	<xsl:attribute name="name"><xsl:value-of select="$ContactMethod"/>AreaCode<xsl:value-of select="$ContactNumber"/></xsl:attribute>
	<xsl:attribute name="onkeypress">NumbersOnly(event);phoneAutoTab(this,txt<xsl:value-of select="$ContactMethod"/>ExchangeNumber<xsl:value-of select="$ContactNumber"/>,'A');</xsl:attribute>
	<xsl:attribute name="value">
	  <xsl:choose>
	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="@PhoneAreaCode"/></xsl:when>
		  <xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="@FaxAreaCode"/></xsl:when>
		  <xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="@CellAreaCode"/></xsl:when>
		  <xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="@PagerAreaCode"/></xsl:when>
	  </xsl:choose>
	</xsl:attribute>
  </input> -
         
  <input maxlength="3" size="2" onbeforedeactivate="checkPhoneExchange(this);" class="inputFld">
	<xsl:attribute name="id">txt<xsl:value-of select="$ContactMethod"/>ExchangeNumber<xsl:value-of select="$ContactNumber"/></xsl:attribute>  
	<xsl:attribute name="name"><xsl:value-of select="$ContactMethod"/>ExchangeNumber<xsl:value-of select="$ContactNumber"/></xsl:attribute>
	<xsl:attribute name="onkeypress">NumbersOnly(event);phoneAutoTab(this,txt<xsl:value-of select="$ContactMethod"/>UnitNumber<xsl:value-of select="$ContactNumber"/>,'E');</xsl:attribute>
	<xsl:attribute name="value">
	  <xsl:choose>
	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="@PhoneExchangeNumber"/></xsl:when>
		  <xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="@FaxExchangeNumber"/></xsl:when>
		  <xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="@CellExchangeNumber"/></xsl:when>
		  <xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="@PagerExchangeNumber"/></xsl:when>
	  </xsl:choose>
	</xsl:attribute>  
  </input> -
		 
  <input maxlength="4" size="3" onbeforedeactivate="checkPhoneNumber(this);" class="inputFld">
    <xsl:attribute name="id">txt<xsl:value-of select="$ContactMethod"/>UnitNumber<xsl:value-of select="$ContactNumber"/></xsl:attribute>  
	<xsl:attribute name="name"><xsl:value-of select="$ContactMethod"/>UnitNumber<xsl:value-of select="$ContactNumber"/></xsl:attribute>
	<xsl:attribute name="onkeypress">NumbersOnly(event);phoneAutoTab(this,txt<xsl:value-of select="$ContactMethod"/>ExtensionNumber<xsl:value-of select="$ContactNumber"/>,'U');</xsl:attribute>
	<xsl:attribute name="value">
	  <xsl:choose>
	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="@PhoneUnitNumber"/></xsl:when>
		  <xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="@FaxUnitNumber"/></xsl:when>
		  <xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="@CellUnitNumber"/></xsl:when>
		  <xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="@PagerUnitNumber"/></xsl:when>
	  </xsl:choose>
	</xsl:attribute>
  </input>
  
  <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
         
  <input maxlength="5" size="5" onkeypress="NumbersOnly(window.event)" class="inputFld">
    <xsl:attribute name="id">txt<xsl:value-of select="$ContactMethod"/>ExtensionNumber<xsl:value-of select="$ContactNumber"/></xsl:attribute>  
	<xsl:attribute name="name"><xsl:value-of select="$ContactMethod"/>ExtensionNumber<xsl:value-of select="$ContactNumber"/></xsl:attribute>
	<xsl:attribute name="value">
	  <xsl:choose>
	    <xsl:when test="$ContactMethod='Phone'"><xsl:value-of select="@PhoneExtensionNumber"/></xsl:when>
		  <xsl:when test="$ContactMethod='Fax'"><xsl:value-of select="@FaxExtensionNumber"/></xsl:when>
		  <xsl:when test="$ContactMethod='Cell'"><xsl:value-of select="@CellExtensionNumber"/></xsl:when>
		  <xsl:when test="$ContactMethod='Pager'"><xsl:value-of select="@PagerExtensionNumber"/></xsl:when>
	  </xsl:choose>
	</xsl:attribute>
  </input>
</xsl:template>
  
</xsl:stylesheet>

