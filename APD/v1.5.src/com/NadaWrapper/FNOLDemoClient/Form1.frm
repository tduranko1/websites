VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Nada Wrapper Test Client"
   ClientHeight    =   6240
   ClientLeft      =   4905
   ClientTop       =   2970
   ClientWidth     =   7155
   LinkTopic       =   "Form1"
   ScaleHeight     =   6240
   ScaleWidth      =   7155
   Begin VB.ListBox lstVehicles 
      Height          =   840
      Left            =   240
      TabIndex        =   15
      Top             =   2400
      Width           =   4095
   End
   Begin VB.TextBox txtBody 
      Height          =   375
      Left            =   1560
      TabIndex        =   14
      Top             =   5520
      Width           =   1935
   End
   Begin VB.TextBox txtModel 
      Height          =   375
      Left            =   4320
      TabIndex        =   13
      Top             =   5520
      Width           =   1935
   End
   Begin VB.TextBox txtMake 
      Height          =   375
      Left            =   1560
      TabIndex        =   12
      Top             =   5040
      Width           =   1935
   End
   Begin VB.TextBox txtYear 
      Height          =   375
      Left            =   4320
      TabIndex        =   11
      Top             =   5040
      Width           =   1935
   End
   Begin VB.ComboBox cboRegions 
      Height          =   315
      Left            =   2520
      Style           =   2  'Dropdown List
      TabIndex        =   10
      Top             =   480
      Width           =   1815
   End
   Begin VB.CommandButton cmdUid 
      Caption         =   "Decode Uid"
      Height          =   375
      Left            =   3720
      TabIndex        =   9
      Top             =   4560
      Width           =   1215
   End
   Begin VB.TextBox txtUid 
      Height          =   375
      Left            =   3000
      TabIndex        =   7
      Top             =   3720
      Width           =   2175
   End
   Begin VB.TextBox txtVin 
      Height          =   375
      Left            =   720
      TabIndex        =   5
      Top             =   1680
      Width           =   2175
   End
   Begin VB.ComboBox cboBodies 
      Height          =   315
      Left            =   5040
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   2880
      Width           =   1815
   End
   Begin VB.ComboBox cboModels 
      Height          =   315
      Left            =   5040
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   2520
      Width           =   1815
   End
   Begin VB.ComboBox cboMakes 
      Height          =   315
      Left            =   5040
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   2160
      Width           =   1815
   End
   Begin VB.ComboBox cboYear 
      Height          =   315
      Left            =   5040
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1800
      Width           =   1815
   End
   Begin VB.CommandButton cmdVin 
      Caption         =   "Decode Vin"
      Height          =   375
      Left            =   3000
      TabIndex        =   0
      Top             =   1680
      Width           =   1215
   End
   Begin VB.Label Label9 
      Caption         =   "Pick one..."
      Height          =   255
      Left            =   240
      TabIndex        =   24
      Top             =   2160
      Width           =   1815
   End
   Begin VB.Label Label8 
      Caption         =   "Otherwise, complete the following...."
      Height          =   495
      Left            =   5040
      TabIndex        =   23
      Top             =   1200
      Width           =   1695
   End
   Begin VB.Label Label5 
      Caption         =   "If you have the VIN, enter it below"
      Height          =   255
      Left            =   240
      TabIndex        =   22
      Top             =   1320
      Width           =   3615
   End
   Begin VB.Label Label4 
      Caption         =   "Start With Region..."
      Height          =   255
      Left            =   2760
      TabIndex        =   21
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "To Verify...."
      Height          =   255
      Left            =   2640
      TabIndex        =   20
      Top             =   4680
      Width           =   975
   End
   Begin VB.Label lblVin 
      AutoSize        =   -1  'True
      Caption         =   "VIN"
      Height          =   195
      Left            =   360
      TabIndex        =   6
      Top             =   1800
      Width           =   270
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "Model"
      Height          =   255
      Left            =   3600
      TabIndex        =   19
      Top             =   5640
      Width           =   615
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Year"
      Height          =   255
      Left            =   3600
      TabIndex        =   18
      Top             =   5160
      Width           =   615
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Body"
      Height          =   255
      Left            =   600
      TabIndex        =   17
      Top             =   5640
      Width           =   855
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Make"
      Height          =   255
      Left            =   600
      TabIndex        =   16
      Top             =   5160
      Width           =   855
   End
   Begin VB.Label lblId 
      AutoSize        =   -1  'True
      Caption         =   "Resulting UID"
      Height          =   195
      Left            =   1920
      TabIndex        =   8
      Top             =   3840
      Width           =   990
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private objNada As Object
'Private objNada As CNadaWrapper


Private blnLoading As Boolean
Private varVinData As Variant

Private Sub Form_Load()
    blnLoading = True
    
    'Set objNada = CreateObject("NadaWrapper.CNadaWrapper", "sftmfnolweb1")
    Set objNada = CreateObject("NadaWrapper.CNadaWrapper")
    'Set objNada = New CNadaWrapper
    
    txtVin.Text = "1ftcr10x4rpc08263"
    
    Fill_Regions
    Fill_Years
    
    
    blnLoading = False
End Sub

Private Sub Fill_Years()
    Dim varData As Variant
    Dim intCounter As Integer

    varData = objNada.GetYears
    cboYear.Clear
    
    For intCounter = LBound(varData) To UBound(varData)
        cboYear.AddItem varData(intCounter)
        cboYear.ItemData(cboYear.NewIndex) = varData(intCounter)
    Next intCounter
    
    cboYear.ListIndex = 0
End Sub

Private Sub Fill_Regions()
    Dim varData As Variant
    Dim intCounter As Integer

    varData = objNada.GetRegions
    cboRegions.Clear
    
    For intCounter = LBound(varData) To UBound(varData)
        If Not IsEmpty(varData(intCounter, 0)) Then
            cboRegions.AddItem varData(intCounter, 0)
            cboRegions.ItemData(cboRegions.NewIndex) = varData(intCounter, 1)
        End If
    Next intCounter
    
    cboRegions.ListIndex = 0
End Sub

Private Sub cboYear_Click()
    Dim varData As Variant
    Dim intCounter As Integer
    
    If blnLoading Then Exit Sub
    blnLoading = True
     
    varData = objNada.GetMakes(cboRegions.ItemData(cboRegions.ListIndex), cboYear.Text)
    
    cboMakes.Clear
    
    For intCounter = LBound(varData) To UBound(varData)
        cboMakes.AddItem varData(intCounter, 0)
        cboMakes.ItemData(cboMakes.NewIndex) = varData(intCounter, 1)
    Next intCounter
    cboMakes.ListIndex = 0
    
    blnLoading = False
End Sub

Private Sub cboMakes_Click()
    Dim varData As Variant
    Dim intCounter As Integer
    
    If blnLoading Then Exit Sub
    blnLoading = True
    
    varData = objNada.GetModels(cboRegions.ItemData(cboRegions.ListIndex), cboYear.Text, cboMakes.ItemData(cboMakes.ListIndex))
    
    cboModels.Clear
    
    For intCounter = LBound(varData) To UBound(varData)
        cboModels.AddItem varData(intCounter, 0)
        cboModels.ItemData(cboModels.NewIndex) = varData(intCounter, 1)
    Next intCounter
    cboModels.ListIndex = 0
    
    blnLoading = False
End Sub

Private Sub cboModels_Click()
    Dim varData As Variant
    Dim intCounter As Integer
    
    If blnLoading Then Exit Sub
    blnLoading = True
    
    varData = objNada.GetBodies(cboRegions.ItemData(cboRegions.ListIndex), cboYear.Text, cboMakes.ItemData(cboMakes.ListIndex), cboModels.ItemData(cboModels.ListIndex))
    
    cboBodies.Clear
    
    For intCounter = LBound(varData) To UBound(varData)
        cboBodies.AddItem varData(intCounter, 0)
        cboBodies.ItemData(cboBodies.NewIndex) = varData(intCounter, 1)
    Next intCounter
    cboBodies.ListIndex = 0
    
    blnLoading = False
End Sub

Private Sub cboBodies_Click()
    Dim lngRetVal As Long
    
    txtUid.Text = objNada.GetVehicleId(cboYear.Text, cboMakes.ItemData(cboMakes.ListIndex), cboModels.ItemData(cboModels.ListIndex), _
                cboBodies.ItemData(cboBodies.ListIndex))
End Sub

Private Sub cmdUid_Click()
    Dim varVehIdData As Variant

    varVehIdData = objNada.DecodeVehicleId(txtUid.Text, cboRegions.ItemData(cboRegions.ListIndex))
    
    txtYear = varVehIdData(eYear)
    txtModel = varVehIdData(eSeries)
    txtMake = varVehIdData(eMake)
    txtBody = varVehIdData(eBody)
End Sub

Private Sub cmdVin_Click()
    varVinData = objNada.DecodeVin(txtVin.Text, cboRegions.ItemData(cboRegions.ListIndex))
    
    Dim intCounter As Integer
    lstVehicles.Clear
        
    For intCounter = LBound(varVinData) To UBound(varVinData)
        If Not IsEmpty(varVinData(intCounter, eUID)) Then
            lstVehicles.AddItem varVinData(intCounter, eYear) & "-" & _
                varVinData(intCounter, eMake) & "-" & _
                varVinData(intCounter, eSeries) & "-" & _
                varVinData(intCounter, eBody)
            lstVehicles.ItemData(lstVehicles.NewIndex) = intCounter
        End If
    Next intCounter
End Sub

Private Sub lstVehicles_Click()
    If lstVehicles.ListIndex <> -1 Then
        txtUid.Text = varVinData(lstVehicles.ItemData(lstVehicles.ListIndex), eUID)
    End If
End Sub

