﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:local="http://local.com/mynamespace"
	  xmlns:js="urn:the-xml-files:xslt"
		id="NadaWrapper">


  <xsl:output method="html" indent="yes"  encoding="UTF-8"/>


  <xsl:template match="Root">

    <TABLE cellspacing="0" cellpadding="0" border="0" width="725" height="100%">
      <COLGROUP>
        <COL width="355" valign="top"/>
        <COL width="15" valign="top"/>
        <COL width="355" valign="top"/>
      </COLGROUP>
      <TR>
        <TD>
          <table border="0" width="337">
            <tr>
              <td width="257">
                <strong>Accessories:</strong>
              </td>
              <td width="45">
                <strong>Retail</strong>
              </td>
              <td width="35">
                <strong>Trade</strong>
              </td>
            </tr>
          </table>

          <DIV id="divAccessories" unselectable="on" class="autoflowDiv" style="height:140px; width:355px; visibility:;" tabindex="10">
            <xsl:variable name="cnt">
              <xsl:value-of select="floor( ( count( AccessoryData/AccessoryList/Accessory ) + 1 ) div 2 )"/>
            </xsl:variable>

            <table cellSpacing="0" cellPadding="0" border="0" width="337">
              <colgroup>
                <col width="7"/>
                <col width="30"/>
                <col width="200"/>
                <col align="right" width="50"/>
                <col align="right" width="50"/>
              </colgroup>
              <tr>
                <td>
                  <xsl:for-each select="AccessoryData/AccessoryList/Accessory[ $cnt >= position() ]">
                    <xsl:apply-templates select="."/>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="AccessoryData/AccessoryList/Accessory[ position() > $cnt ]">
                    <xsl:apply-templates select="."/>
                  </xsl:for-each>
                </td>
              </tr>
            </table>
          </DIV>
        </TD>
        <TD></TD>
        <TD valign="top" nowrap="nowrap">
          <table border="0" cellpadding="4" cellspacing="0">
            <colgroup>
              <col width="200"/>
              <col width="60" align="right"/>
              <col width="60" align="right"/>
            </colgroup>
            <tr>
              <td></td>
              <td>
                <strong>Retail</strong>
              </td>
              <td>
                <strong>Trade</strong>
              </td>
            </tr>
            <tr>
              <td>MSRP</td>
              <td id="tdMSRP" align="right" style="visibility:">
                $<xsl:value-of select="Vehicle/MSRP"/>
              </td>
              <td id="tdMSRP2" align="right" style="visibility:">
                $<xsl:value-of select="Vehicle/MSRP"/>
              </td>
            </tr>
            <tr>
              <td>Base</td>
              <td id="tdRetail" style="visibility:">
                <xsl:choose>
                  <xsl:when test="Vehicle/Retail = 0">Not Avail</xsl:when>
                  <xsl:otherwise>
                    $<span id="txtRetail">
                      <xsl:value-of select="Vehicle/Retail"/>
                    </span>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <td id="tdTrade" style="visibility:">
                <xsl:choose>
                  <xsl:when test="Vehicle/Trade = 0">Not Avail</xsl:when>
                  <xsl:otherwise>
                    $<span id="txtTrade">
                      <xsl:value-of select="Vehicle/Trade"/>
                    </span>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
            <tr>
              <td nowrap="nowrap">
                Miles
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                <input  id="txtMileage" name="txtMileage" size="6" maxlength="8" tabindex="11" >
                  <xsl:attribute name="value">
                    <xsl:value-of select="Vehicle/Mileage"/>
                  </xsl:attribute>
                </input>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
                <input name="btnMileage" id="btnMileage" type="button" class="formbutton" disabled="true" style="cursor:default;" onClick="btnMileage_onclick()" value="Adjust" tabindex="12"/>
                <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
              </td>
              <td id="tdAdjMileageRetail" align="right" style="visibility:">
                $<span id="txtAdjMileageRetail">0</span>
              </td>
              <td id="tdAdjMileageTrade" align="right" style="visibility:">
                $<span id="txtAdjMileageTrade">0</span>
              </td>
            </tr>
            <tr>
              <td>Accessories</td>
              <td id="tdAccessoriesRetail" align="right" style="visibility:">
                $<span id="txtAccTotalRetail">0</span>
              </td>
              <td id="tdAccessoriesTrade" align="right" style="visibility:">
                $<span id="txtAccTotalTrade">0</span>
              </td>
            </tr>
            <tr>
              <td>Adjusted</td>
              <td id="tdAdjRetail" align="right" style="visibility:">
                <strong>
                  $<span id="txtAdjRetail">
                    <xsl:value-of select="Vehicle/Retail"/>
                  </span>
                </strong>
              </td>
              <td id="tdAdjTrade" align="right" style="visibility:">
                <strong>
                  $<span id="txtAdjTrade">
                    <xsl:value-of select="Vehicle/Trade"/>
                  </span>
                </strong>
              </td>
            </tr>
          </table>
        </TD>
      </TR>
      <TR>
        <TD colspan="3">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
        </TD>
      </TR>
      <TR>
        <!-- invisible data -->
        <TD colspan="3">
          <table border="0" cellspacing="0" cellpadding="0" style="display:none;">
            <tr>
              <td></td>
              <td class="nadaHeader">Loan</td>
            </tr>
            <tr>
              <td>
                <strong>Base</strong>
              </td>
              <td>
                $<span id="txtLoan">
                  <xsl:choose>
                    <xsl:when test="Vehicle/Loan != ''">
                      <xsl:value-of select="Vehicle/Loan"/>
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                  </xsl:choose>
                </span>
              </td>
            </tr>
            <tr>
              <td>
                <strong>Adjusted</strong>
              </td>
              <td>
                $<span id="txtAdjLoan">
                  <xsl:value-of select="Vehicle/Loan"/>
                </span>
              </td>
            </tr>
          </table>
        </TD>
      </TR>
    </TABLE>
  </xsl:template>

  <!-- Builds the accessories check-box list -->
  <xsl:template match="Accessory">
    <tr>
      <xsl:choose>
        <xsl:when test="@status='False'">
          <td>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </td>
          <td align="center">
            <input class="chkbox" type='checkbox' onclick='chkBox_onclick()'>
              <xsl:attribute name='name'>
                <xsl:value-of select="@code"/>
              </xsl:attribute>
              <xsl:attribute name='value'>
                <xsl:value-of select="@value"/>
              </xsl:attribute>
            </input>
          </td>
          <td>
            <xsl:value-of select="@name"/>
          </td>
          <td>
            <xsl:call-template name="AccessoryValue">
              <xsl:with-param name="value">retail</xsl:with-param>
            </xsl:call-template>
          </td>
          <td align="right">
            <xsl:call-template name="AccessoryValue">
              <xsl:with-param name="value">trade</xsl:with-param>
            </xsl:call-template>
          </td>
        </xsl:when>
        <xsl:when test="@status='True'">
          <td>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </td>
          <td align="center">
            <input class="chkbox" type='checkbox' onclick='chkBox_onclick()' checked='1' disabled='1'>
              <xsl:attribute name='name'>
                <xsl:value-of select="@code"/>
              </xsl:attribute>
              <xsl:attribute name='value'>
                <xsl:value-of select="@value"/>
              </xsl:attribute>
            </input>
          </td>
          <td>
            <xsl:value-of select="@name"/>
          </td>
          <td>
            <xsl:call-template name="AccessoryValue">
              <xsl:with-param name="value">retail</xsl:with-param>
            </xsl:call-template>
          </td>
          <td align="right">
            <xsl:call-template name="AccessoryValue">
              <xsl:with-param name="value">trade</xsl:with-param>
            </xsl:call-template>
          </td>
        </xsl:when>
        <xsl:otherwise>
          <!-- @status='Excl.' -->
          <td>
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;<xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          </td>
          <td align="center">
            <input class="chkbox" type='checkbox' onclick='chkBox_onclick()' checked='1'>
              <xsl:attribute name='name'>
                <xsl:value-of select="@code"/>
              </xsl:attribute>
              <xsl:attribute name='value'>
                <xsl:value-of select="@value"/>
              </xsl:attribute>
            </input>
          </td>
          <td>
            <xsl:value-of select="@name"/>
          </td>
          <td>
            <xsl:call-template name="AccessoryValue">
              <xsl:with-param name="value">retail</xsl:with-param>
            </xsl:call-template>
          </td>
          <td align="right">
            <xsl:call-template name="AccessoryValue">
              <xsl:with-param name="value">trade</xsl:with-param>
            </xsl:call-template>
          </td>
        </xsl:otherwise>
      </xsl:choose>
    </tr>
  </xsl:template>

  <xsl:template name="AccessoryValue" >
    <xsl:param name="value"/>

    <xsl:choose>
      <xsl:when test="$value='retail'">
        <xsl:choose>
          <xsl:when test="@value>=0">
            <font color="green">
              <b>
                $<xsl:value-of select="@value"/>
              </b>
            </font>
          </xsl:when>
          <xsl:otherwise>
            <font color="red">
              <b>
                $<xsl:value-of select="@value"/>
              </b>
            </font>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="@value>=0">
            <font color="green">
              <b>
                $<xsl:value-of select="@tradeValue"/>
              </b>
            </font>
          </xsl:when>
          <xsl:otherwise>
            <font color="red">
              <b>
                $<xsl:value-of select="@tradeValue"/>
              </b>
            </font>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>


</xsl:stylesheet>
