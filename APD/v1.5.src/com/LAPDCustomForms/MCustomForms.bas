Attribute VB_Name = "MCustomForms"
Option Explicit

Public Const APP_NAME As String = "LAPDCustomForms."
Public Const CustomForms_FirstError As Long = &H80091000

Public Enum ErrorCodes
    eXMLParseError = CustomForms_FirstError
    eXMLNodeNotFound
    eFormDoesNotExist
    eFormXSLDoesNotExist
    eFormFilledDoesNotExist
    eActiveXCreate
    ePDFTKMissing
    eNoOutput
    eMissingDocumentRoot
    eMissingSaveFormSP
    eFileNotFound
End Enum

Public g_objEvents As SiteUtilities.CEvents
Public g_objDataAccessor As DataAccessor.CDataAccessor

Public g_strSupportDocPath As String
Public g_strPDFTKPath As String
Public g_strGhostscriptPath As String
Public g_blnDebugMode As Boolean
Public g_strDocumentRoot As String
Public g_strLYNXConnStringStd As String
Public g_intDEFAULT_SLEEP As Integer
Public g_strPDFMetaDataFile As String

Public Const SHELL_TIMEOUT As Long = 60000 '1 minute

'********************************************************************************
'* Initializes global objects and variables
'********************************************************************************
Public Sub InitializeGlobals()

          'Create DataAccessor.
10        Set g_objDataAccessor = New DataAccessor.CDataAccessor
          
          'Share DataAccessor's events object.
20        Set g_objEvents = g_objDataAccessor.mEvents
          
          'Get path to support document directory
30        g_strSupportDocPath = App.Path & "\" & Left(APP_NAME, Len(APP_NAME) - 1)
          
          'This will give partner data its own log file.
40        g_objEvents.ComponentInstance = "Custom Forms"
          
          'Initialize our member components first, so we have logging set up.
50        g_objDataAccessor.InitEvents App.Path & "\..\config\config.xml"
          
          'Get config debug mode status.
60        g_blnDebugMode = g_objEvents.IsDebugMode
          
          'Get config PDFTK path setting
70        g_strPDFTKPath = GetConfig("CustomForms/PDFTKPath")
          
          'get config Ghostscript path setting
80        g_strGhostscriptPath = GetConfig("CustomForms/GhostScriptPath")
          
          'Get the APD Document Root directory
90        g_strDocumentRoot = Replace(GetConfig("Document/RootDirectory"), "\\", "\")
          
          'Get the standard APD connection string
100       g_strLYNXConnStringStd = GetConfig("ConnectionStringStd")
          
          'Get the default sleep for the component to wait when a shell program is executed
110       g_intDEFAULT_SLEEP = GetConfig("CustomForms/DefaultSleep")
          
          'Get the PDF meta file name so we can stamp the pdf
120       g_strPDFMetaDataFile = GetConfig("CustomForms/PDFMetaFile")
End Sub

'********************************************************************************
'* Terminates global objects and variables
'********************************************************************************
Public Sub TerminateGlobals()
10        Set g_objEvents = Nothing
20        Set g_objDataAccessor = Nothing
End Sub


Public Function GetConfig(ByVal strSetting As String)
10        GetConfig = g_objEvents.mSettings.GetParsedSetting(strSetting)
End Function


Public Function getSafeFileName(strFileName As String) As String
          Dim iCount As Integer
          Dim strFolderPath As String
          Dim strFileBaseName As String
          Dim strFileExt As String
          Dim strRet As String
          Dim objFSO As New Scripting.FileSystemObject
          
10        If strFileName <> "" Then
20            iCount = 1
30            strFolderPath = objFSO.GetParentFolderName(strFileName)
40            strFileBaseName = objFSO.GetBaseName(strFileName)
50            strFileExt = objFSO.GetExtensionName(strFileName)
60            strRet = strFileName
70            While objFSO.FileExists(strRet)
80                strRet = objFSO.BuildPath(strFolderPath, strFileBaseName & "_" & CStr(iCount))
90                If strFileExt <> "" Then
100                   strRet = strRet & "." & strFileExt
110               End If
120               iCount = iCount + 1
130           Wend
140       End If
150       getSafeFileName = strRet
160       Set objFSO = Nothing
End Function

Public Function PDF2Tif(strFileName As String) As String
          Const PROC_NAME As String = "PDF2Tif()"
10        On Error GoTo errHandler
          Dim objFSO As Scripting.FileSystemObject
          Dim bRet As Boolean
          Dim strRet As String
          Dim strOutputFileName As String
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Dim strPDFFilledFile As String
          
20        strRet = "" 'convertedFileExists(strFileName, "pdf")
          
30        Set objFSO = New Scripting.FileSystemObject
          
40        If objFSO.FileExists(strFileName) Then
50            strOutputFileName = getSafeFileName(objFSO.BuildPath(objFSO.GetParentFolderName(strFileName), objFSO.GetBaseName(strFileName) & ".tif"))
60            strBatchFile = objFSO.BuildPath(objFSO.GetParentFolderName(strFileName), "totif.bat")
              
70            Set oBatFile = objFSO.OpenTextFile(strBatchFile, ForWriting, True)
80            oBatFile.WriteLine """" & g_strGhostscriptPath & """ -dNOPAUSE -dBATCH -q -sDEVICE=tiffg4 -sPAPERSIZE=letter -r200 -sOutputFile=""" & strOutputFileName & """ """ & strFileName & """"
90            oBatFile.Close
100           Set oBatFile = Nothing
              
              'execute the batch file
110           bRet = SuperShell(strBatchFile, objFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
              
120           DoEvents
              'Sleep 1000
              
              'cleanup
130           objFSO.DeleteFile strBatchFile
              
140           If objFSO.FileExists(strOutputFileName) Then
150               strRet = strOutputFileName
160           Else
170               strRet = ""
180           End If
190       Else
200           bRet = False
210       End If
          
          'if bDeleteOriginal Then safeDelete strFileName
          
errHandler:
220       Set objFSO = Nothing
230       If Err.Number <> 0 Then
240           If g_blnDebugMode Then g_objEvents.Trace _
                              "Cannot convert pdf file to tif " & strFileName & vbCrLf, PROC_NAME
250           strRet = ""
260       End If
270       PDF2Tif = strRet
End Function
