<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="no" method="text"/>
  <msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
		//IMPORTANT: This function must be used for all cdata or free-form text so that we
		//           can safely escape the paranthesis.
        function cleanData(str){
		   str = str.replace(/[(]/g, '\\(');
		   str = str.replace(/[)]/g, '\\)');
		   return str;
		}
		function crlf(){
			return String.fromCharCode(13, 10);
		}
		function toLowerCase(str){
		   if (str != "")
		      return str.toLowerCase();
           else
              return str;
		}
        function formatNumber(intNum, intZeroPadLength) {
          var strNum = intNum + "";
          var strRet = "";
          for (var i = 0; i < intZeroPadLength - strNum.length; i++)
              strRet += "0";
          strRet += strNum;
          return strRet;
        }
        function getDate(){
          var strRet = "";
          var dt = new Date();
          strRet = formatNumber((dt.getMonth() + 1), 2) + "/" +
                   formatNumber(dt.getDate(), 2) + "/" +
                   dt.getFullYear() + " " +
                   formatNumber(dt.getHours(), 2) + ":" +
                   formatNumber(dt.getMinutes(), 2) + ":" +
                   formatNumber(dt.getSeconds(), 2);
          return strRet;
        }
  ]]>
  </msxsl:script>
  <xsl:template match="/">%FDF-1.2
1 0 obj
&lt;&lt;
/FDF
&lt;&lt;
/Fields [
    <!--Read-only fields-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtOriginalEstAmount'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtOriginalEstimate"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAuditedDifference'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtAuditedDifference"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAuditedEstAmount'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtAuditedEstimate"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtDeductible'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtDeductible"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtNetAuditedEstimate'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtNetAuditedEstimate"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtReportDate'"/>
      <xsl:with-param name="fieldValue" select="user:getDate()"/>
    </xsl:call-template>
    <!--Alternate Parts Located-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbAlternatePartsLocated'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkAltParts"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <!--LKQ-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbLKQ'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkLKQ"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtLKQComments'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtLKQComments"/>
    </xsl:call-template>
    <!--After Market-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbAftermarket'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkAftermarket"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAfterMarketComments'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtAfterMarketComments"/>
    </xsl:call-template>
    <!--Reconditioned/Remanufactured-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbReconditioned'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkReconditioned"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtRecComments'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtRecComments"/>
    </xsl:call-template>
    <!--Surplus OEM-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbSurplus'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkSurplusOEM"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtSurplusOEMComments'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtSurplusOEMComments"/>
    </xsl:call-template>
    <!--Alternate Parts Located consolidated-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAltPartsLocatedItemsChecked'"/>
      <xsl:with-param name="fieldValue" select="//Data/AltPartsLocatedItemsChecked"/>
    </xsl:call-template>
    <!--Correction to Labor Operations-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbCorrection'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkCorrection"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtCorrlaborOpComments'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtCorrComments"/>
    </xsl:call-template>
    <!--Estimating Guidelines Applied-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbEstimatingGuidelines'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkEstimateGuidelines"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtEstGuidelineComments'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtEstGuidelinesComments"/>
    </xsl:call-template>
    <!--Rate Correction to Prevailing/Supplied Rates-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbRateCorrection'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkRateCorrection"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtRateCorrComments'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtRateCorrComments"/>
    </xsl:call-template>
    <!--Other-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbOther'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkOther"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtOther'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtOther"/>
    </xsl:call-template>
    <!--Price Not Agreed-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbPriceNotAgreed'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkPriceNotAgreed"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbAgreedPriceObtainedWith'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkAgreedPrice"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAgreedPriceObtainedWith'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtShopContact"/>
    </xsl:call-template>
    <!--Rate Difference-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbRateDifferences'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkRateDiff"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtRateDiffComments'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtRateDiffComments"/>
    </xsl:call-template>
    <!--System Differences-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbSystemDifferences'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkSystemDiff"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtSysDiffComments'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtSysDiffComments"/>
    </xsl:call-template>
    <!--Price Not Agreed - Other-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbOtherPriceNotAgreed'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkPriceNotAgreedOther"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtOtherPriceNotAgreed'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtPriceNotAgreedOther"/>
    </xsl:call-template>
    <!--Price not agreed items consolidated-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtPriceNotAgreedItemsChecked'"/>
      <xsl:with-param name="fieldValue" select="//Data/PriceNotAgreedItemsChecked"/>
    </xsl:call-template>
    <!-- No Shop Contact -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'cbNoAttemptToContactShop'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkNoAttemptToContactShop"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <!--Auditor Comments-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAuditorComments'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtAuditorComments"/>
    </xsl:call-template>
    <!--System fields-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtLynxID'"/>
      <xsl:with-param name="fieldValue" select="//Data/LynxID"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtInsuranceCompany'"/>
      <xsl:with-param name="fieldValue" select="//Data/InsuranceCompany"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtClaimNumber'"/>
      <xsl:with-param name="fieldValue" select="//Data/ClaimNumber"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtClaimRep'"/>
      <xsl:with-param name="fieldValue" select="//Data/ClaimRepName"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtRepName'"/>
      <xsl:with-param name="fieldValue" select="//Data/AuditorName"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtRepTitle'"/>
      <xsl:with-param name="fieldValue" select="//Data/AuditorTitle"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtRepPhone'"/>
      <xsl:with-param name="fieldValue" select="//Data/AuditorPhone"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtRepEmail'"/>
      <xsl:with-param name="fieldValue" select="//Data/AuditorEmail"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtRepairState'"/>
      <xsl:with-param name="fieldValue" select="//Data/RepairState"/>
    </xsl:call-template>
    <xsl:variable name="faciltyRegistered">
      <xsl:choose>
        <xsl:when test="//Data/rbgRegistered = '1'">YES</xsl:when>
        <xsl:otherwise>NO</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtRepairFacilityRegistered'"/>
      <xsl:with-param name="fieldValue" select="$faciltyRegistered"/>
    </xsl:call-template>]
/F () &gt;&gt;
 &gt;&gt;
endobj
trailer
&lt;&lt;  /Root 1 0 R  &gt;&gt;
%%EOF
  </xsl:template>
  <xsl:template name="field">
    <xsl:param name="fieldName"/>
    <xsl:param name="fieldValue"/>
    <xsl:param name="fieldType"/>
    <xsl:variable name="LCaseFieldValue" select="user:toLowerCase(string($fieldValue))"/>
    <xsl:text>&lt;&lt; /T (</xsl:text>
    <xsl:value-of select="$fieldName"/>
    <xsl:text>) /V (</xsl:text>
    <xsl:choose>
      <xsl:when test="$fieldType = 'boolean'">
        <xsl:choose>
          <xsl:when test="$LCaseFieldValue = 'yes' or $LCaseFieldValue = 'y' or $LCaseFieldValue = '1' or $LCaseFieldValue = 'true' or $LCaseFieldValue = 't'">Yes</xsl:when>
          <xsl:otherwise>No</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="user:cleanData(string($fieldValue))"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>) &gt;&gt;</xsl:text>
    <xsl:value-of select="user:crlf()"/>
  </xsl:template>
</xsl:stylesheet>
