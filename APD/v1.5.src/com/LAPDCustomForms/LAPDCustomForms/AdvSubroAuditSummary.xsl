<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="no" method="text"/>
  <msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
		//IMPORTANT: This function must be used for all cdata or free-form text so that we
		//           can safely escape the paranthesis.
        function cleanData(str){
		   str = str.replace(/[(]/g, '\\(');
		   str = str.replace(/[)]/g, '\\)');
		   return str;
		}
		function crlf(){
			return String.fromCharCode(13, 10);
		}
		function toLowerCase(str){
		   if (str != "")
		      return str.toLowerCase();
           else
              return str;
		}
  ]]>
  </msxsl:script>
  <xsl:template match="/">%FDF-1.2
1 0 obj
&lt;&lt;
/FDF
&lt;&lt;
/Fields [
    <!-- Total or Partial Supplement -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtOriginal'"/>
      <xsl:with-param name="fieldValue" select="//Data/OriginalSel"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAudited'"/>
      <xsl:with-param name="fieldValue" select="//Data/AuditSel"/>
    </xsl:call-template>
    <!-- Estimate Row -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtOEstimate'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtEstimateOriginal"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAPEstimate'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtEstimateAnalysis"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAEstimate'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtEstimateAudited"/>
    </xsl:call-template>
    <xsl:choose>
      <xsl:when test="//Data/txtEstimateOriginal != '' and //Data/txtEstimateAudited != '' and //Data/txtEstimateOriginal != //Data/txtEstimateAudited">
        <xsl:call-template name="field">
          <xsl:with-param name="fieldName" select="'txtADEstimate'"/>
          <xsl:with-param name="fieldValue" select="//Data/txtEstimateDiff"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
          <xsl:if test="//Data/txtEstimateOriginal != '' and //Data/txtEstimateAudited != ''">
            <xsl:call-template name="field">
              <xsl:with-param name="fieldName" select="'txtADEstimate'"/>
              <xsl:with-param name="fieldValue" select="'$0.00'"/>
            </xsl:call-template>
          </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtDSREstimate'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtEstimateSummary"/>
    </xsl:call-template>
    <!-- Loss of Use / Rental row -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtOLossOfUse'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtLossUseOriginal"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAPLossOfUse'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtLossUseAnalysis"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtALossOfUse'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtLossUseAudited"/>
    </xsl:call-template>
    <xsl:choose>
      <xsl:when test="//Data/txtLossUseOriginal != '' and //Data/txtLossUseAudited != '' and //Data/txtLossUseOriginal != //Data/txtLossUseAudited">
        <xsl:call-template name="field">
          <xsl:with-param name="fieldName" select="'txtADLossOfUse'"/>
          <xsl:with-param name="fieldValue" select="//Data/txtLossUseDiff"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
          <xsl:if test="//Data/txtLossUseOriginal != '' and //Data/txtLossUseAudited != ''">
            <xsl:call-template name="field">
              <xsl:with-param name="fieldName" select="'txtADLossOfUse'"/>
              <xsl:with-param name="fieldValue" select="'$0.00'"/>
            </xsl:call-template>
          </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtDSRLossOfUse'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtLossUseSummary"/>
    </xsl:call-template>
    <!-- Other row -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtOOther'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtOtherOriginal"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAPOther'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtOtherAnalysis"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAOther'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtOtherAudited"/>
    </xsl:call-template>
    <xsl:choose>
      <xsl:when test="//Data/txtOtherOriginal != '' and //Data/txtOtherAudited != '' and //Data/txtOtherOriginal != //Data/txtOtherAudited">
        <xsl:call-template name="field">
          <xsl:with-param name="fieldName" select="'txtADOther'"/>
          <xsl:with-param name="fieldValue" select="//Data/txtOtherDiff"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
          <xsl:if test="//Data/txtOtherOriginal != '' and //Data/txtOtherAudited != ''">
            <xsl:call-template name="field">
              <xsl:with-param name="fieldName" select="'txtADOther'"/>
              <xsl:with-param name="fieldValue" select="'$0.00'"/>
            </xsl:call-template>
          </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtDSROther'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtOtherSummary"/>
    </xsl:call-template>
    <!-- Vehicle Settlement -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtOVehSettlement'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtVehicleSetOriginal"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAPVehSettlement'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtVehicleSetAnalysis"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAVehSettlement'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtVehicleSetAudited"/>
    </xsl:call-template>
    <xsl:choose>
      <xsl:when test="//Data/txtVehicleSetOriginal != '' and //Data/txtVehicleSetAudited != '' and //Data/txtVehicleSetOriginal != //Data/txtVehicleSetAudited">
        <xsl:call-template name="field">
          <xsl:with-param name="fieldName" select="'txtADVehSettlement'"/>
          <xsl:with-param name="fieldValue" select="//Data/txtVehicleSetDiff"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
          <xsl:if test="//Data/txtVehicleSetOriginal != '' and //Data/txtVehicleSetAudited != ''">
            <xsl:call-template name="field">
              <xsl:with-param name="fieldName" select="'txtADVehSettlement'"/>
              <xsl:with-param name="fieldValue" select="'$0.00'"/>
            </xsl:call-template>
          </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtDSRVehSettlement'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtVehicleSetSummary"/>
    </xsl:call-template>
    <!-- Salvage -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtOSalvage'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtSalvageOriginal"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAPSalvage'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtSalvageAnalysis"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtASalvage'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtSalvageAudited"/>
    </xsl:call-template>
    <xsl:choose>
      <xsl:when test="//Data/txtSalvageOriginal != '' and //Data/txtSalvageAudited != '' and //Data/txtSalvageOriginal != //Data/txtSalvageAudited">
        <xsl:call-template name="field">
          <xsl:with-param name="fieldName" select="'txtADSalvage'"/>
          <xsl:with-param name="fieldValue" select="//Data/txtSalvageDiff"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
          <xsl:if test="//Data/txtSalvageOriginal != '' and //Data/txtSalvageAudited != ''">
            <xsl:call-template name="field">
              <xsl:with-param name="fieldName" select="'txtADSalvage'"/>
              <xsl:with-param name="fieldValue" select="'$0.00'"/>
            </xsl:call-template>
          </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtDSRSalvage'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtSalvageSummary"/>
    </xsl:call-template>
    <!-- Total Subrogation Demand -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtOTotalSubroDemand'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtTotalSubroOriginalTotal"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtATotalSubroDemand'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtTotalSubroAuditedTotal"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtADTotalSubroDemand'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtTotalSubroDiffTotal"/>
    </xsl:call-template>
    <!-- Summary row -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtPercentAuditSavings'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtAuditSavings"/>
    </xsl:call-template>
    <!--Parts Exception-->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkAltNew'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkAltVsNew"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkRepairReplace'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkReprVsRepl"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkPartsPrice'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkPartsPrice"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkIncorrectMarkup'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkIncInaMarkup"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkOtherPartsException'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkOtherPartsExc"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <!-- Labor Exceptions -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkExcessiveLabor'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkExcLaborHrs"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkIncorrectLaborHours'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkIncLaborHrsOp"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkExcessiveFrameHours'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkExcFrameHrs"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkSetup'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkSMIncLaborCat"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkOtherLaborException'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkOtherLaborExc"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <!-- Calculation/Rate Exceptions -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkExcessiveLaborRates'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkExcLaborRates"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkExcessiveRefinishRate'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkExcRefMatRates"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkShopMaterialsEstimate'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkShopMatEst"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkTaxRateError'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkTaxRateError"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkPaintMaterialExcessive'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkPaintMatExc"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkDedutibleNotApplied'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkDedNotApplied"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkOtherCalcException'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkOtherCalExc"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <!-- Other Exceptions -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkSubletAmountExcessive'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkSubAmtExc"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkSubletAmountNotDocumented'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkSubAmtNoDoc"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkBettermentNotApplied'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkBettNotApplied"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkDamageUnrelated'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkDamUnrelatedLoss"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkDuplicateEntry'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkDupEntryEst"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkVehMisidentifiedVINError'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkVehVINError"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkTowing'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkTowStorExc"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkEstimateGuideline'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkEstGuideline"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkOtherEstimateExceptions'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkOtherEstExc"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <!-- Loss of Use / Rental Analysis -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtTotalDays'"/>
      <xsl:with-param name="fieldValue" select="//Data/txtTotalDays"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkLossOfUseRentalExcessive'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkLossUseExc"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkRentalRate'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkRentalRateExc"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkRentalOptions'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkRentalOptionsExc"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkOtherLossOfUseException'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkOtherLossUse"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <!-- Total Loss Analysis -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkVehMisidentified'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkVehMisIdentified"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkSettlementCalc'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkSettCalInc"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkSalvageReturn'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkSalRetNotInc"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkTotalLossHandlingTime'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkTotalLossHandleTime"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'chkOtherTotalLossException'"/>
      <xsl:with-param name="fieldValue" select="//Data/chkOtherTotalLoss"/>
      <xsl:with-param name="fieldType" select="'boolean'"/>
    </xsl:call-template>
    <!-- Auditor Comments -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtComments'"/>
      <xsl:with-param name="fieldValue" select="//Data/AuditorComments"/>
    </xsl:call-template>
    <!-- System Fields -->
    <!-- Assignment Information -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtLynxID'"/>
      <xsl:with-param name="fieldValue" select="//Data/LynxID"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtClientName'"/>
      <xsl:with-param name="fieldValue" select="//Data/InsuranceCompany"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtClaimNumber'"/>
      <xsl:with-param name="fieldValue" select="//Data/ClaimNumber"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtYear'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleYear"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtMake'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleMake"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtModel'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleModel"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtReportDate'"/>
      <xsl:with-param name="fieldValue" select="//Data/ReportDate"/>
    </xsl:call-template>
    <!-- Auditor Information -->
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtAuditorName'"/>
      <xsl:with-param name="fieldValue" select="//Data/AuditorName"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtTitle'"/>
      <xsl:with-param name="fieldValue" select="//Data/AuditorTitle"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtPhone'"/>
      <xsl:with-param name="fieldValue" select="//Data/AuditorPhone"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'txtEmail'"/>
      <xsl:with-param name="fieldValue" select="//Data/AuditorEmail"/>
    </xsl:call-template>]
/F () &gt;&gt;
 &gt;&gt;
endobj
trailer
&lt;&lt;  /Root 1 0 R  &gt;&gt;
%%EOF
  </xsl:template>
  <xsl:template name="field">
    <xsl:param name="fieldName"/>
    <xsl:param name="fieldValue"/>
    <xsl:param name="fieldType"/>
    <xsl:variable name="LCaseFieldValue" select="user:toLowerCase(string($fieldValue))"/>
    <xsl:text>&lt;&lt; /T (</xsl:text>
    <xsl:value-of select="$fieldName"/>
    <xsl:text>) /V (</xsl:text>
    <xsl:choose>
      <xsl:when test="$fieldType = 'boolean'">
        <xsl:choose>
          <xsl:when test="$LCaseFieldValue = 'yes' or $LCaseFieldValue = 'y' or $LCaseFieldValue = '1' or $LCaseFieldValue = 'true' or $LCaseFieldValue = 't'">Yes</xsl:when>
          <xsl:otherwise>No</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="user:cleanData(string($fieldValue))"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>) &gt;&gt;</xsl:text>
    <xsl:value-of select="user:crlf()"/>
  </xsl:template>
</xsl:stylesheet>
