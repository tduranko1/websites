<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="no" method="text"/>
  <msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
		//IMPORTANT: This function must be used for all cdata or free-form text so that we
		//           can safely escape the paranthesis.
        function cleanData(str){
		   str = str.replace(/[(]/g, '\\(');
		   str = str.replace(/[)]/g, '\\)');
		   return str;
		}
		function crlf(){
			return String.fromCharCode(13, 10);
		}
		function toLowerCase(str){
		   if (str != "")
		      return str.toLowerCase();
           else
              return str;
		}
  ]]>
  </msxsl:script>
  <xsl:template match="/">%FDF-1.2
1 0 obj
&lt;&lt;
/FDF
&lt;&lt;
/Fields [
<xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'InsuranceCompany'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/Claim/@InsuranceCompany"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'LynxIDVehNum'"/>
         <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/@LynxId, '-', //Data/Assignment/@VehicleNumber)"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'ClaimNumber'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/Coverage/@ClaimNumber"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'RepPhone'"/>
         <xsl:with-param name="fieldValue" select="'(800) 490 1935'"/>
      </xsl:call-template>

      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'RepEmailAddress'"/>
         <xsl:with-param name="fieldValue" select="'lynxapd@lynxservices.com'"/>
      </xsl:call-template>

      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'OwnerName'"/>
         <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/Owner/@OwnerFirstName, ' ', //Data/Assignment/Owner/@OwnerLastName)"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'OwnerName2'"/>
         <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/Owner/@OwnerFirstName, ' ', //Data/Assignment/Owner/@OwnerLastName)"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'OwnerName3'"/>
         <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/Owner/@OwnerFirstName, ' ', //Data/Assignment/Owner/@OwnerLastName)"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'ShopName1'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/Shop/@ShopName"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'ShopName2'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/Shop/@ShopName"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'Vin'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@Vin"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'VehicleYear'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@VehicleYear"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'Color'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@Color"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'MakeModel'"/>
         <xsl:with-param name="fieldValue" select="concat(//Data/Assignment/Vehicle/@Make, ' ', //Data/Assignment/Vehicle/@Model)"/>
      </xsl:call-template>
      
      <xsl:call-template name="field">
         <xsl:with-param name="fieldName" select="'LicensePlateNo'"/>
         <xsl:with-param name="fieldValue" select="//Data/Assignment/Vehicle/@LicensePlateNo"/>
      </xsl:call-template>]
/F () &gt;&gt;
 &gt;&gt;
endobj
trailer
&lt;&lt;  /Root 1 0 R  &gt;&gt;
%%EOF
  </xsl:template>
  <xsl:template name="field">
    <xsl:param name="fieldName"/>
    <xsl:param name="fieldValue"/>
    <xsl:param name="fieldType"/>
    <xsl:variable name="LCaseFieldValue" select="user:toLowerCase(string($fieldValue))"/>
    <xsl:text>&lt;&lt; /T (</xsl:text>
    <xsl:value-of select="$fieldName"/>
    <xsl:text>) /V (</xsl:text>
    <xsl:choose>
      <xsl:when test="$fieldType = 'boolean'">
        <xsl:choose>
          <xsl:when test="$LCaseFieldValue = 'yes' or $LCaseFieldValue = 'y' or $LCaseFieldValue = '1' or $LCaseFieldValue = 'true' or $LCaseFieldValue = 't'">Yes</xsl:when>
          <xsl:otherwise>No</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="user:cleanData(string($fieldValue))"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>) &gt;&gt;</xsl:text>
    <xsl:value-of select="user:crlf()"/>
  </xsl:template>
</xsl:stylesheet>
