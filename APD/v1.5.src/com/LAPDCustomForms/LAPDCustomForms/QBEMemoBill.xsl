<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="no" method="text"/>
  <msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
		//IMPORTANT: This function must be used for all cdata or free-form text so that we
		//           can safely escape the paranthesis.
        function cleanData(str){
		   str = str.replace(/[(]/g, '\\(');
		   str = str.replace(/[)]/g, '\\)');
		   return str;
		}
		function crlf(){
			return String.fromCharCode(13, 10);
		}
		function toLowerCase(str){
		   if (str != "")
		      return str.toLowerCase();
           else
              return str;
		}
		function reverseString(str){
		   if (str != ""){
		      var strReverse = "";
		      for (var i = str.length - 1; i >= 0 ; i--)
		          strReverse += str.substr(i, 1);
              return strReverse;
		   } else
		      return "";
		}
		function toUpper(str){
		   return str.toUpperCase();
		}
  ]]>
  </msxsl:script>
  <xsl:template match="/">%FDF-1.2
1 0 obj
&lt;&lt;
/FDF
&lt;&lt;
/Fields [
	<xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LynxID'"/>
      <xsl:with-param name="fieldValue" select="//Data/LynxID"/>
    </xsl:call-template>

    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Adjuster'"/>
      <xsl:with-param name="fieldValue" select="//Data/CarrierRepName"/>
    </xsl:call-template>

    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'InsuredName'"/>
      <xsl:with-param name="fieldValue" select="//Data/ClaimantName"/>
    </xsl:call-template>
    
	<xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Address1'"/>
      <xsl:with-param name="fieldValue" select="//Data/Address1"/>
    </xsl:call-template>
	
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Address2'"/>
      <xsl:with-param name="fieldValue" select="//Data/Address2"/>
    </xsl:call-template>
    
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'CityStateZip'"/>
      <xsl:with-param name="fieldValue" select="//Data/Address3"/>
    </xsl:call-template>
	
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ClaimNum'"/>
      <xsl:with-param name="fieldValue" select="//Data/ClaimNumber"/>
    </xsl:call-template>
    
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'PolicyNum'"/>
      <xsl:with-param name="fieldValue" select="//Data/PolicyNumber"/>
    </xsl:call-template>
    
	<xsl:variable name="LossDate">
      <xsl:call-template name="formatSQLDate">
        <xsl:with-param name="fieldValue" select="//Data/LossDate"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LossDate'"/>
      <xsl:with-param name="fieldValue" select="$LossDate"/>
    </xsl:call-template>

	<xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ClaimantName'"/>
      <xsl:with-param name="fieldValue" select="//Data/ClaimantName"/>
    </xsl:call-template>
    
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'BusinessName'"/>
      <xsl:with-param name="fieldValue" select="//Data/BusinessName"/>
    </xsl:call-template>
    
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Vehicle'"/>
      <xsl:with-param name="fieldValue" select="//Data/Vehicle"/>
    </xsl:call-template>
    
	<xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'FeeAmount'"/>
      <xsl:with-param name="fieldValue" select="//Data/TotalFees"/>
    </xsl:call-template>
    
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'TotalAmount'"/>
      <xsl:with-param name="fieldValue" select="//Data/TotalFees"/>
    </xsl:call-template>

    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'SvcChannel'"/>
      <xsl:with-param name="fieldValue" select="//Data/SvcChannel"/>
    </xsl:call-template>
    
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'MemoBillNum'"/>
      <xsl:with-param name="fieldValue" select="//Data/MemoBillNum"/>
    </xsl:call-template>
    
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'MemoBillDate'"/>
      <xsl:with-param name="fieldValue" select="//Data/MemoBillDate"/>
    </xsl:call-template>]
/F () &gt;&gt;
 &gt;&gt;
endobj
trailer
&lt;&lt;  /Root 1 0 R  &gt;&gt;
%%EOF
  </xsl:template>
  <xsl:template name="field">
    <xsl:param name="fieldName"/>
    <xsl:param name="fieldValue"/>
    <xsl:param name="fieldType"/>
    <xsl:variable name="LCaseFieldValue" select="user:toLowerCase(string($fieldValue))"/>
    <xsl:text>&lt;&lt; /T (</xsl:text>
    <xsl:value-of select="$fieldName"/>
    <xsl:text>) /V (</xsl:text>
    <xsl:choose>
      <xsl:when test="$fieldType = 'boolean'">
        <xsl:choose>
          <xsl:when test="$LCaseFieldValue = 'yes' or $LCaseFieldValue = 'y' or $LCaseFieldValue = '1' or $LCaseFieldValue = 'true' or $LCaseFieldValue = 't'">Yes</xsl:when>
          <xsl:otherwise>No</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="user:cleanData(string($fieldValue))"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>) &gt;&gt;</xsl:text>
    <xsl:value-of select="user:crlf()"/>
  </xsl:template>
  
  <xsl:template name="formatSQLDate">
    <xsl:param name="fieldValue"/>
    <xsl:choose>
      <xsl:when test="contains($fieldValue, 'T') = boolean('true')">
        <xsl:variable name="year" select="substring($fieldValue, 1, 4)"/>
        <xsl:variable name="month" select="substring($fieldValue, 6, 2)"/>
        <xsl:variable name="date" select="substring($fieldValue, 9, 2)"/>
        <xsl:value-of select="concat($month, '/', $date, '/', $year)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$fieldValue"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>
