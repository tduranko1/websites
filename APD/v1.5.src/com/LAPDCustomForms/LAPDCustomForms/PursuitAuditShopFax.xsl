<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output omit-xml-declaration="no" method="text"/>
  <msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
		//IMPORTANT: This function must be used for all cdata or free-form text so that we
		//           can safely escape the paranthesis.
        function cleanData(str){
		   str = str.replace(/[(]/g, '\\(');
		   str = str.replace(/[)]/g, '\\)');
		   return str;
		}
		function crlf(){
			return String.fromCharCode(13, 10);
		}
		function toLowerCase(str){
		   if (str != "")
		      return str.toLowerCase();
           else
              return str;
		}
  ]]>
  </msxsl:script>
  <xsl:template match="/">%FDF-1.2
1 0 obj
&lt;&lt;
/FDF
&lt;&lt;
/Fields [
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'RequestDate'"/>
      <xsl:with-param name="fieldValue" select="//Data/RequestDate"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ShopName'"/>
      <xsl:with-param name="fieldValue" select="//Data/ShopName"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ShopAddress1'"/>
      <xsl:with-param name="fieldValue" select="//Data/ShopAddress"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ShopCityStateZip'"/>
      <xsl:with-param name="fieldValue" select="concat(//Data/ShopAddressCity, ' ', //Data/ShopAddressState, ' ', //Data/ShopAddressZip)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ShopPhone'"/>
      <xsl:with-param name="fieldValue" select="//Data/ShopPhoneNumber"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ShopFax'"/>
      <xsl:with-param name="fieldValue" select="//Data/ShopFaxNumber"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ShopEmail'"/>
      <xsl:with-param name="fieldValue" select="//Data/ShopEmailAddress"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'LynxID'"/>
      <xsl:with-param name="fieldValue" select="//Data/LynxId"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ClaimNumber'"/>
      <xsl:with-param name="fieldValue" select="//Data/ClaimNumber"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'DateOfLoss'"/>
      <xsl:with-param name="fieldValue" select="//Data/LossDate"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Party'"/>
      <xsl:with-param name="fieldValue" select="//Data/Party"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Deductible'"/>
      <xsl:with-param name="fieldValue" select="//Data/Deductible"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VehicleOwner'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleOwnerName"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerAddress1'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleOwnerAddress"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerCityStateZip'"/>
      <xsl:with-param name="fieldValue" select="concat(//Data/VehicleOwnerAddressCity, ' ', //Data/VehicleOwnerAddressState, ' ', //Data/VehicleOwnerAddressZip)"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'OwnerPhone'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleOwnerPhone"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VehicleMake'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleMake"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VehicleModel'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleModel"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VIN'"/>
      <xsl:with-param name="fieldValue" select="//Data/VIN"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VehicleTag'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleTag"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'Mileage'"/>
      <xsl:with-param name="fieldValue" select="//Data/Mileage"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'VehicleYear'"/>
      <xsl:with-param name="fieldValue" select="//Data/VehicleYear"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'InspectionDate'"/>
      <xsl:with-param name="fieldValue" select="//Data/InspectionDate"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'UploadLink'"/>
      <xsl:with-param name="fieldValue" select="//Data/UploadLink"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ClaimOwnerName'"/>
      <xsl:with-param name="fieldValue" select="//Data/ClaimOwnerName"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ClaimOwnerPhone'"/>
      <xsl:with-param name="fieldValue" select="//Data/ClaimOwnerPhone"/>
    </xsl:call-template>
    <xsl:call-template name="field">
      <xsl:with-param name="fieldName" select="'ClaimOwnerEmail'"/>
      <xsl:with-param name="fieldValue" select="//Data/ClaimOwnerEmail"/>
    </xsl:call-template>]
/F () &gt;&gt;
 &gt;&gt;
endobj
trailer
&lt;&lt;  /Root 1 0 R  &gt;&gt;
%%EOF
  </xsl:template>
  <xsl:template name="field">
    <xsl:param name="fieldName"/>
    <xsl:param name="fieldValue"/>
    <xsl:param name="fieldType"/>
    <xsl:variable name="LCaseFieldValue" select="user:toLowerCase(string($fieldValue))"/>
    <xsl:text>&lt;&lt; /T (</xsl:text>
    <xsl:value-of select="$fieldName"/>
    <xsl:text>) /V (</xsl:text>
    <xsl:choose>
      <xsl:when test="$fieldType = 'boolean'">
        <xsl:choose>
          <xsl:when test="$LCaseFieldValue = 'yes' or $LCaseFieldValue = 'y' or $LCaseFieldValue = '1' or $LCaseFieldValue = 'true' or $LCaseFieldValue = 't'">Yes</xsl:when>
          <xsl:otherwise>No</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="user:cleanData(string($fieldValue))"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>) &gt;&gt;</xsl:text>
    <xsl:value-of select="user:crlf()"/>
  </xsl:template>
</xsl:stylesheet>

