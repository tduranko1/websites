Attribute VB_Name = "modShell"
Option Explicit
Option Compare Text

Private Const INFINITE = &HFFFF
Private Const STARTF_USESHOWWINDOW = &H1

Private Const STATUS_ABANDONED_WAIT_0 As Long = &H80
Private Const STATUS_WAIT_0 As Long = &H0
Private Const STATUS_USER_APC As Long = &HC0

Private Const WAIT_ABANDONED As Long = (STATUS_ABANDONED_WAIT_0 + 0)
Private Const WAIT_ABANDONED_0 As Long = (STATUS_ABANDONED_WAIT_0 + 0)
Private Const WAIT_FAILED As Long = &HFFFFFFFF
Private Const WAIT_IO_COMPLETION As Long = STATUS_USER_APC
Private Const WAIT_OBJECT_0 As Long = (STATUS_WAIT_0 + 0)
Private Const WAIT_TIMEOUT As Long = 258&


Public Enum enSW
    SW_HIDE = 0
    SW_NORMAL = 1
    SW_MAXIMIZE = 3
    SW_MINIMIZE = 6
End Enum

Public Type PROCESS_INFORMATION
    hProcess As Long
    hThread As Long
    dwProcessId As Long
    dwThreadId As Long
End Type

Public Type STARTUPINFO
    cb As Long
    lpReserved As String
    lpDesktop As String
    lpTitle As String
    dwX As Long
    dwY As Long
    dwXSize As Long
    dwYSize As Long
    dwXCountChars As Long
    dwYCountChars As Long
    dwFillAttribute As Long
    dwFlags As Long
    wShowWindow As Integer
    cbReserved2 As Integer
    lpReserved2 As Byte
    hStdInput As Long
    hStdOutput As Long
    hStdError As Long
End Type

Public Type SECURITY_ATTRIBUTES
    nLength As Long
    lpSecurityDescriptor As Long
    bInheritHandle As Long
End Type

Public Enum enPriority_Class
    NORMAL_PRIORITY_CLASS = &H20
    IDLE_PRIORITY_CLASS = &H40
    HIGH_PRIORITY_CLASS = &H80
End Enum

Private Declare Function CreateProcess Lib "kernel32" Alias "CreateProcessA" (ByVal lpApplicationName As String, ByVal lpCommandLine As String, lpProcessAttributes As SECURITY_ATTRIBUTES, lpThreadAttributes As SECURITY_ATTRIBUTES, ByVal bInheritHandles As Long, ByVal dwCreationFlags As Long, lpEnvironment As Any, ByVal lpCurrentDriectory As String, lpStartupInfo As STARTUPINFO, lpProcessInformation As PROCESS_INFORMATION) As Long
Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

'usage:     SuperShell filefullpath, filepath, 0, SW_NORMAL, HIGH_PRIORITY_CLASS
Public Function SuperShell(ByVal App As String, ByVal WorkDir As String, dwMilliseconds As Long, ByVal start_size As enSW, ByVal Priority_Class As enPriority_Class) As Boolean
          Dim pclass As Long
          Dim sinfo As STARTUPINFO
          Dim pinfo As PROCESS_INFORMATION
          'Not used, but needed
          Dim sec1 As SECURITY_ATTRIBUTES
          Dim sec2 As SECURITY_ATTRIBUTES
          'Set the structure size
10        sec1.nLength = Len(sec1)
20        sec2.nLength = Len(sec2)
30        sinfo.cb = Len(sinfo)
          'Set the flags
40        sinfo.dwFlags = STARTF_USESHOWWINDOW
          'Set the window's startup position
50        sinfo.wShowWindow = start_size
          'Set the priority class
60        pclass = Priority_Class
          'Start the program
70        If CreateProcess(vbNullString, App, sec1, sec2, False, pclass, 0&, WorkDir, sinfo, pinfo) Then
              'Wait
80            WaitForSingleObject pinfo.hProcess, dwMilliseconds
90            SuperShell = True
100       Else
110           SuperShell = False
120       End If
End Function


