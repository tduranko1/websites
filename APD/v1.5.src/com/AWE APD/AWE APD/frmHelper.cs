﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;
using System.Xml;

namespace AWE_APD
{
    public partial class frmHelper : Form
    {
        public string FilePath
        {
            get;
            set;
        }

        public string strAPDUrl
        {
            get;
            set;
        }
        public string strFileName
        {
            get;
            set;
        }
        public string InboxPath
        {
            get;
            set;
        }
        public string OutboxPath 
        {
            get;
            set;
        }
        public string lynxID
        {
            get;
            set;
        }
        public string supplementID
        {
            get;
            set;
        }
        public frmHelper()
        {
            InitializeComponent();
        }
        private void frmHelper_Load(object sender, EventArgs e)
        {
            lblFileName.Text = FilePath;
           GetLynxID(FilePath);
        }
        private void frmHelper_Load1(object sender, EventArgs e)
        {
            if (strFileName != null && strFileName != "")
            {
                lblFileName.Text = lblFileName.Text + strFileName;

                strFileName = strFileName.Replace(Path.GetExtension(strFileName), ".AD1");
                GetLynxID(strFileName);
                //txtFilePath.Text = strFileName;

                //move all the file to destination folder 
                string FileName;
                string FilePath;

                if (OutboxPath.Trim().Length > 0)
                {
                    if (Directory.Exists(OutboxPath.Trim()))
                    {
                        FileName = Path.GetFileNameWithoutExtension(strFileName);
                        FilePath = Path.GetDirectoryName(strFileName);


                        foreach (var file in Directory.GetFiles(FilePath))
                        {
                            if (string.Compare(Path.GetFileNameWithoutExtension(file).ToUpper(), FileName.ToUpper()) == 0)
                            {
                                File.Copy(file, OutboxPath +  lynxID +  supplementID + Path.GetExtension(file));
                                File.Delete(file);
                            }
                        }

                        notifyIcon1.ShowBalloonTip(2000, "APD Pathways Helper",  "Files moved",  ToolTipIcon.Info);
                    }
                    else
                    {
                        MessageBox.Show("Destination Directory doesnot exists");
                    }
                }
                else
                {
                    MessageBox.Show("Enter the Destination directory path");
                }



                //string sourcePath = txtFilePath.Text;

                ////Get the list of files from Source path
                //foreach (var files in Directory.GetFiles(sourcePath))
                //{
                //    File.Copy(files, txtDestinationPath.Text + Path.GetFileName(files));
                //}

            }
                     
        }
        //private void btnGet_Click(object sender, EventArgs e)
        //{

        //    //string LynxID;

        //   //GetLynxID(txtFilePath.Text);

        //}

        public  void GetLynxID(string sourcefilePath)  
        {
            string connStr;
            string query;
            string strTmpPath = "";
            string strTmpadfPath = "";
            string strTmpenvPath = "";
            string sourceEnvPath = "";
            string LynxID;

            strTmpPath = Path.GetTempPath();  

            strTmpadfPath = strTmpPath + "ad1.dbf";
            strTmpenvPath = strTmpPath + "env.dbf";


            if (File.Exists(strTmpadfPath))
            {
                File.Delete(strTmpadfPath);
            }

            File.Copy(sourcefilePath, strTmpadfPath);

            sourceEnvPath =  sourcefilePath; //Path.GetFullPath(sourcefilePath) + Path.GetFileNameWithoutExtension(sourcefilePath);
            sourceEnvPath = sourceEnvPath.ToUpper().Replace("AD1", "ENV");

            if (File.Exists(strTmpenvPath))
            {
                File.Delete(strTmpenvPath);
            }
            File.Copy(sourceEnvPath, strTmpenvPath);
            
            
            connStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strTmpPath + ";Extended Properties=dBASE IV;";  //, null, null, 0);
            //
            query = "Select * from ad1.dbf";
            DataSet ds = new DataSet();
            
            ds = ExecuteDbase(connStr, query);
            //  ADODB.Connection conn = new ADODB.Connection();
            // ADODB.Recordset recset = new ADODB.Recordset();

            //connStr = "Driver={Microsoft dBASE Driver (*.dbf)};Dbq=" + strTmpPath ;  //, null, null, 0);
            //OleDbConnection conn = new OleDbConnection(connStr);
            //conn.Open();
            //OleDbDataAdapter adapter = new OleDbDataAdapter(query, conn);
            //DataSet ds = new DataSet();
            //adapter.Fill(ds);

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //Read the LynxID 
                    //Check for error if a file other than dbf file is read !! 
                    lynxID = ds.Tables[0].Rows[0]["CLM_NO"].ToString();
                    txtLynxID.Text = lynxID.Split(' ')[2].Split('-')[0];
                    txtVehicleId.Text =   lynxID.Split('-')[1];
                    //LYNX ID 1749584-2
                }
                else
                {
                    //Couldnt read dbf file or dbf file is empty
                }

            }
            else
            {
                //Couldnt read dbf file or dbf file is empty
            }

            ds = new DataSet();
            connStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strTmpPath + ";Extended Properties=dBASE IV;";  //, null, null, 0);

            query = "Select * from env.dbf";
            ds = ExecuteDbase(connStr, query);

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //Read the LynxID 
                    //Check for error if a file other than dbf file is read !! 
                     supplementID = ds.Tables[0].Rows[0]["SUPP_NO"].ToString();
                     txtEstimate.Text = supplementID;
                }
                else
                {
                    //Couldnt read dbf file or dbf file is empty
                }

            }
            else
            {
                //Couldnt read dbf file or dbf file is empty
            }
           

           
        }

        public DataSet ExecuteDbase(string ConnectionString, string Query)
        {
            DataSet ds = new DataSet();
            OleDbConnection conn = new OleDbConnection(ConnectionString);

            conn.Open();
            OleDbDataAdapter adapter = new OleDbDataAdapter(Query, conn);
            ds = new DataSet();
            adapter.Fill(ds);
            conn.Close();
            return ds;
        }

      

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    using (OpenFileDialog dlg = new OpenFileDialog())
        //    {
        //        dlg.CheckFileExists = true;
        //        dlg.CheckPathExists = true;
        //        //dlg.DefaultExt = "awf";
        //       // dlg.Filter = "Pathways WPF File|*.awf";
        //        dlg.Multiselect = false;
        //        dlg.ShowDialog();
        //        txtFilePath.Text = dlg.FileName;
        //        //if (txtFilePath.Text != "" && File.Exists(txtFilePath.Text))
        //        //{
        //        //    lblFileName.Text = strFileName;
        //        //    buttonSave.Enabled = true;
        //        //}
        //    }
        //}

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
            //Hide();
        }

        //private void button3_Click(object sender, EventArgs e)
        //{
        //    //move all the file to destination folder 
        //    string FileName;
        //    string FilePath;

        //    if (txtDestinationPath.Text.Trim().Length > 0)
        //    {
        //        if( Directory.Exists(txtDestinationPath.Text.Trim()))
        //        {
        //            FileName = Path.GetFileNameWithoutExtension(strFileName);
        //            FilePath = Path.GetDirectoryName(strFileName);


        //            foreach (var file in Directory.GetFiles(FilePath))
        //            {
        //                if (string.Compare(Path.GetFileNameWithoutExtension(file).ToUpper(), FileName.ToUpper()) == 0)
        //                {
        //                    File.Copy(file, txtDestinationPath.Text + txtLynxID.Text + txtVehicleNO.Text + Path.GetExtension(file) );
        //                    File.Delete(file);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            MessageBox.Show("Destination Directory doesnot exists");
        //        }
        //    }
        //    else{
        //        MessageBox.Show("Enter the Destination directory path");
        //    }

           

        //    //string sourcePath = txtFilePath.Text;

        //    ////Get the list of files from Source path
        //    //foreach (var files in Directory.GetFiles(sourcePath))
        //    //{
        //    //    File.Copy(files, txtDestinationPath.Text + Path.GetFileName(files));
        //    //}
        //}
               
        private void btnDelete_Click(object sender, EventArgs e)
        {
            FileDelete(strFileName);
           //strFileName
            //string FileName;
            //string FilePath;

            //FileName = Path.GetFileNameWithoutExtension(strFileName);
            //FilePath = Path.GetDirectoryName(strFileName);


            //foreach (var file in Directory.GetFiles(FilePath))
            //{
            //    if (string.Compare(Path.GetFileNameWithoutExtension(file).ToUpper(), FileName.ToUpper()) == 0)
            //    {
            //        File.Delete(file);
            //    }
            //}
            //resetting FileName variable 
            this.Close();
            //strFileName = "";
            ////frmHelper frmValue = new frmHelper();            
            ////frmValue.strFileName = "";
          

            //lblFileName.Text="";
            //txtFilePath.Text = "";
            //btnDelete.Enabled = false;
            //txtLynxID.Text="";
            //txtVehicleNO.Text = "";
          
        }

        private void FileDelete(string strFilePath)
        {
            string FileName;
            string FilePath;

            FileName = Path.GetFileNameWithoutExtension(strFilePath);
            FilePath = Path.GetDirectoryName(strFilePath);


            foreach (var file in Directory.GetFiles(FilePath))
            {
                if (string.Compare(Path.GetFileNameWithoutExtension(file).ToUpper(), FileName.ToUpper()) == 0)
                {
                    File.Delete(file);
                }
            }
        }

      
        private void btnSave_Click_1(object sender, EventArgs e)
        {
            //add a note to the claim. Also, create a task for the MDS.
            string strRequest = "<Root>";
            string strData = "";

            strData = "LynxID=" + txtLynxID.Text;
            strData += "&VehNo=" + txtVehicleId.Text;
           // strData += "&Estimate=" + txtEstimate.Text;
            strData += "&SeqNo=0";
            strData += "&Note=" + txtNotes.Text;
            strData += "&LogonID=" + Environment.UserName;


            strRequest += "<StoredProc name='uspCCCHelperUpdate' execType='executespnp'><Data><![CDATA[" + strData + "]]></Data></StoredProc>";
            strRequest += "</Root>";

            clsWeb request = new clsWeb(strAPDUrl);
            //<Root><StoredProc name='uspCCCHelperUpdate' execType='executespnp'><Data><![CDATA[LynxID=1749584&VehNo=2&SeqNo=0&Note=1749584 testing 123&LogonID=saty162]]></Data></StoredProc></Root>
            string strResponse = request.GetApdData("/rs/apdsave.asp", strRequest);
            request = null;

            XmlDocument xmlResponse = new XmlDocument();
            xmlResponse.LoadXml(strResponse);

            XmlNodeList results = xmlResponse.SelectNodes("//StoredProc");

            if (results != null)
            {
                XmlElement result;
                string errorMessage = "";
                for (int i = 0; i < results.Count; i++)
                {
                    result = (XmlElement)results[i];
                    if (result.GetAttribute("returnCode") != "0")
                    {
                        switch (result.GetAttribute("name"))
                        {
                            case "uspNoteInsDetail":
                                errorMessage += "Unable to create notes to the claim. Details: " + System.Uri.UnescapeDataString(result.InnerText);
                                break;
                            case "uspDiaryInsDetail":
                                errorMessage += "Unable to create diary task to the claim. Details: " + System.Uri.UnescapeDataString(result.InnerText);
                                break;
                            case "uspCCCHelperUpdate":
                                errorMessage += "Error in executing the procedure. Details: " + System.Uri.UnescapeDataString(result.InnerText);
                                break;
                        }
                    }
                    else
                    {
                        //delete the ems file set 
                        FileDelete(FilePath);
                    }
                    
                }
                if (errorMessage != "")
                {
                    MessageBox.Show("Unable to record information to the claim.\n\nError information:" + errorMessage, "APD CCC Helper", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(" Unable to record information to the claim.", "APD CCC Helper", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            //close the window.
            this.Close();
        }    
    }
}
