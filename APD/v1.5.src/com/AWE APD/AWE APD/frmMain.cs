﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Data.OleDb;
using System.Threading;

namespace AWE_APD
{
   public partial class frmMain : Form
   {
      #region Variables
      frmSettings frmSetting = null;

      XmlDocument xmlConfig = null;
      string UserType = "";
      string strWPLInPath = "";
      string strWPLOutPath = "";
      string strAPDArchivePath = "";
      string strUserArchivePath = "";
      string strAPDURL = "";
      string strUserLogin = "";
      string strUserID = "";
      string strEnvironment = "";
      string strPathwaysDrive = "";
      string strAssignmentCopyStatus = "";
      int intAssignmentsCopied = 0;
      int intAWFCopied = 0;
      string lynxID = "";
      string supplementID = "";

      List<string> awfFiles = new List<string>();
      List<string> strAnalysts = new List<string>();

      System.Collections.Hashtable myWatchers = new System.Collections.Hashtable();

      #endregion

      #region Form Methods
      public frmMain()
      {
         InitializeComponent();
      }

      private void frmMain_Shown(object sender, EventArgs e)
      {
         WindowState = FormWindowState.Minimized;
      }

      private void Form1_Load(object sender, EventArgs e)
      {
          bool blnSettingsValid = false;
          readConfig();

          if (strWPLInPath == "" || Directory.Exists(strWPLInPath) == false)
          {
              MessageBox.Show("Pathways WPL In Folder is missing. Update the settings.");
          }
          else
          {
              blnSettingsValid = true;
          }
          if (strWPLOutPath == "" || Directory.Exists(strWPLOutPath) == false)
          {
              MessageBox.Show("Pathways WPL Out Folder is missing. Update the settings.");
          }
          else
          {
              blnSettingsValid = true;
          }

          if (strAPDArchivePath == "" || Directory.Exists(strAPDArchivePath) == false)
          {
              MessageBox.Show("Pathways APD Archive Folder is missing. Update the settings.");
          }
          else
          {
              //strAPDArchivePath = strAPDArchivePath + @"\" + Environment.UserName;
              strUserArchivePath = strAPDArchivePath + @"\" + Environment.UserName;
              if (Directory.Exists(strUserArchivePath) == false)
              {
                  Directory.CreateDirectory(strUserArchivePath);
              }
              blnSettingsValid = true;
          }

          if (strAPDURL == "" ||
              (strAPDURL.StartsWith("http://", StringComparison.OrdinalIgnoreCase) == false &&
                 strAPDURL.StartsWith("https://", StringComparison.OrdinalIgnoreCase) == false))
          {
              MessageBox.Show("APD URL missing. Update the settings.");
          }
          else
          {
              if (strAPDURL.EndsWith("/") == true)
                  strAPDURL = strAPDURL.Substring(0, strAPDURL.Length - 1);

              blnSettingsValid = true;
          }

          if (blnSettingsValid)
          {
              if (strWPLInPath != "")
              {
                  //CreateWatcher();
                  startWatching();
              }
          }
      }

      private void Form1_Resize(object sender, EventArgs e)
      {
         if (FormWindowState.Minimized == WindowState)
            Hide();
      }

      private void notifyIcon1_DoubleClick(object sender, EventArgs e)
      {
         Show();
         WindowState = FormWindowState.Normal;
      }

      private void button4_Click(object sender, EventArgs e)
      {
         Hide();
      }

      private void exportFileToAPDToolStripMenuItem_Click(object sender, EventArgs e)
      {
         buttonExport_Click(sender, e);
      }

      private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
      {
         buttonSettings_Click(sender, e);
      }

      private void importFileToPathwaysToolStripMenuItem_Click(object sender, EventArgs e)
      {
         buttonImport_Click(sender, e);
      }

      private void exitToolStripMenuItem_Click(object sender, EventArgs e)
      {
         Application.Exit();
      }

      private void buttonSettings_Click(object sender, EventArgs e)
      {
         if (frmSetting == null || frmSetting.IsDisposed)
         {
            frmSetting = new frmSettings();
         }
         frmSetting.ShowDialog();
         xmlConfig = null;
         initConfig();
         readConfig();
      }

      private void buttonExport_Click(object sender, EventArgs e)
      {
         frmExport exp = new frmExport();
         exp.strArchivePath = strUserArchivePath;
         exp.strAPDUrl = strAPDURL;
         exp.strUserID = strUserID;
         exp.strPathwaysDrive = strPathwaysDrive;
         exp.strAnalysts = strAnalysts;
         exp.Show();
      }

      private void buttonImport_Click(object sender, EventArgs e)
      {
         frmImport imp = new frmImport();
         imp.strArchivePath = strAPDArchivePath;
         imp.strInboxPath = strWPLInPath;
         imp.strPathwaysDrive = strPathwaysDrive;
         imp.ShowDialog();
      }

      private void buttonInstall_Click(object sender, EventArgs e)
      {
         frmInstall ins = new frmInstall();
         ins.ShowDialog();
      }

      private void buttonTest_Click(object sender, EventArgs e)
      {
         //MessageBox.Show(Screen.PrimaryScreen.Bounds.Height.ToString());
      }


      private void buttonSubscribe_Click(object sender, EventArgs e)
      {
         //notifyIcon1.ShowBalloonTip(1000, "APD Pathways Helper", "Tooltip text", ToolTipIcon.Info);
         frmSubscribe frm = new frmSubscribe();
         frm.strPathwaysDrive = strPathwaysDrive;
         frm.ShowDialog();

         //reload file system watcher
      }

      private void timer1_Tick(object sender, EventArgs e)
      {
         timer1.Enabled = false;

         strAssignmentCopyStatus = "";

         try
         {
            for (int i = 0; i < strAnalysts.Count; i++)
            {
               intAssignmentsCopied = 0;
               intAWFCopied = 0;
               
               copyAssignments(strPathwaysDrive + strAnalysts[i], strPathwaysDrive + strUserLogin + @"\ASG");
               copyAWE(strPathwaysDrive + strAnalysts[i] + @"\WFLOUT\", strPathwaysDrive + strUserLogin + @"\WFLIN\");

               if (intAssignmentsCopied > 0 || intAWFCopied > 0)
               {
                  strAssignmentCopyStatus += string.Format("* Moved {0} assignments and imported {1} work file from {2}\n", intAssignmentsCopied, intAWFCopied, strAnalysts[i]);
               }
            }
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message, "APD Pathways Helper", MessageBoxButtons.OK, MessageBoxIcon.Error);
         }
         finally
         {
            if (strAssignmentCopyStatus != "")
            {
               //notify user about new assignment moved to his queue.
               notifyIcon1.ShowBalloonTip(20, "APD Pathways Helper", strAssignmentCopyStatus, ToolTipIcon.Info);
            }
            timer1.Enabled = true;
         }
      }

      private void timer2_Tick(object sender, EventArgs e)
      {
          long copyFileCount=0;
         timer2.Enabled = false;
         try
         {
             string[] strExportFiles = Directory.GetFiles(strWPLOutPath);
            
            if (strExportFiles.Count() > 0)
            {

                if (UserType == "Supporter")
                {
                    //waiting for 5 seconds for all the emf files to be copied 
                    Thread.Sleep(5000);

                    //Supporter Part
                    foreach (var file in Directory.GetFiles(strWPLOutPath))
                    {
                        //Chk for the .ad1 file ... 
                        if (string.Compare(Path.GetExtension(file).ToUpper(), ".AD1") == 0)
                        {
                            //frmTest exp = new frmTest();
                            ////frmSupporterHelper1 exp = new frmSupporterHelper1();
                            ////exp.FilePath = file;
                            ////exp.strFileName = strExportFiles[i];
                            //////exp.strArchivePath = strUserArchivePath;
                            //////exp.strAPDUrl = strAPDURL;
                            //////exp.strUserID = strUserID;
                            //////exp.strPathwaysDrive = strPathwaysDrive;
                            //////exp.strAnalysts = strAnalysts;
                            ////exp.InboxPath = strWPLInPath;
                            ////exp.OutboxPath = strWPLOutPath;
                            //exp.ShowDialog();
                            //exp = null;

                            frmHelper exp1 = new frmHelper();
                            //exp.strFileName = strExportFiles[i];
                            //exp.strArchivePath = strUserArchivePath;
                            exp1.strAPDUrl = strAPDURL;
                            //exp.strUserID = strUserID;
                            //exp.strPathwaysDrive = strPathwaysDrive;
                            //exp.strAnalysts = strAnalysts;
                            exp1.FilePath = file;
                            exp1.InboxPath = strWPLInPath;
                            exp1.OutboxPath = strWPLOutPath;
                            exp1.ShowDialog();
                            exp1 = null;

                        }
                    }
                }

                if (UserType == "Analyst")
                {
                    //Analyst Part
                    Thread.Sleep(5000);
                    foreach (var file in Directory.GetFiles(strWPLOutPath))
                    // for(int i = 0; i < strExportFiles.Count(); i++)
                    {
                        if (File.Exists(file))
                        {
                            GetLynxID(file);

                            //move all the file to destination folder 
                            string FileName;
                            string FilePath;

                            if (strWPLOutPath.Trim().Length > 0)
                            {
                                if (Directory.Exists(strWPLOutPath.Trim()))
                                {
                                    FileName = Path.GetFileNameWithoutExtension(file);
                                    FilePath = Path.GetDirectoryName(file);


                                    foreach (var file1 in Directory.GetFiles(FilePath))
                                    {
                                        if (string.Compare(Path.GetFileNameWithoutExtension(file1).ToUpper(), FileName.ToUpper()) == 0)
                                        {
                                            File.Copy(file1, strWPLInPath + lynxID + ' ' + supplementID + Path.GetExtension(file1));
                                            File.Delete(file1);
                                            copyFileCount += 1;
                                        }
                                    }


                                }
                                else
                                {
                                    MessageBox.Show("Destination Directory doesnot exists");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Enter the Destination directory path");
                            }
                        }

                    }
                    notifyIcon1.ShowBalloonTip(3500, "APD Pathways Helper", string.Format("{0} Files moved", copyFileCount), ToolTipIcon.Info);
                }
                
                               
            }

            //for (int i = 0; i < strExportFiles.Count(); i++)
            //{
                //Files dropped ... 
                //Copy the files to network folder after renaming it with lynxid 

                 

              

                //frmExport exp = new frmExport();
                //exp.strFileName = strExportFiles[i];
                //exp.strArchivePath = strUserArchivePath;
                //exp.strAPDUrl = strAPDURL;
                //exp.strUserID = strUserID;
                //exp.strPathwaysDrive = strPathwaysDrive;
                //exp.strAnalysts = strAnalysts;
                //exp.ShowDialog();
                //exp = null;
            //}
            //notifyIcon1.ShowBalloonTip(2000, "APD Pathways Helper", "Files moved", ToolTipIcon.Info);
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message, "APD Pathways Helper", MessageBoxButtons.OK, MessageBoxIcon.Error);
         }
         finally
         {
            timer2.Enabled = true;
         }
      }

      public void GetLynxID(string sourcefilePath)
      {
          string connStr;
          string query;
          string strTmpPath = "";
          string strTmpadfPath = "";
          string strTmpenvPath = "";
          string sourceEnvPath = "";
          string LynxID;

          strTmpPath = Path.GetTempPath();

          strTmpadfPath = strTmpPath + "ad1.dbf";
          strTmpenvPath = strTmpPath + "env.dbf";


          if (File.Exists(strTmpadfPath))
          {
              File.Delete(strTmpadfPath);
          }

          File.Copy(sourcefilePath, strTmpadfPath);

          sourceEnvPath = sourcefilePath; //Path.GetFullPath(sourcefilePath) + Path.GetFileNameWithoutExtension(sourcefilePath);
          sourceEnvPath = sourceEnvPath.ToUpper().Replace("AD1", "ENV");

          if (File.Exists(strTmpenvPath))
          {
              File.Delete(strTmpenvPath);
          }
          File.Copy(sourceEnvPath, strTmpenvPath);


          connStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strTmpPath + ";Extended Properties=dBASE IV;";  //, null, null, 0);
          //
          query = "Select * from ad1.dbf";
          DataSet ds = new DataSet();

          ds = ExecuteDbase(connStr, query);
          //  ADODB.Connection conn = new ADODB.Connection();
          // ADODB.Recordset recset = new ADODB.Recordset();

          //connStr = "Driver={Microsoft dBASE Driver (*.dbf)};Dbq=" + strTmpPath ;  //, null, null, 0);
          //OleDbConnection conn = new OleDbConnection(connStr);
          //conn.Open();
          //OleDbDataAdapter adapter = new OleDbDataAdapter(query, conn);
          //DataSet ds = new DataSet();
          //adapter.Fill(ds);

          if (ds.Tables.Count > 0)
          {
              if (ds.Tables[0].Rows.Count > 0)
              {
                  //Read the LynxID 
                  //Check for error if a file other than dbf file is read !! 
                  lynxID = ds.Tables[0].Rows[0]["CLM_NO"].ToString();
                  //txtLynxID.Text = LynxID;
              }
              else
              {
                  //Couldnt read dbf file or dbf file is empty
              }

          }
          else
          {
              //Couldnt read dbf file or dbf file is empty
          }

          ds = new DataSet();
          connStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strTmpPath + ";Extended Properties=dBASE IV;";  //, null, null, 0);

          query = "Select * from env.dbf";
          ds = ExecuteDbase(connStr, query);

          if (ds.Tables.Count > 0)
          {
              if (ds.Tables[0].Rows.Count > 0)
              {
                  //Read the LynxID 
                  //Check for error if a file other than dbf file is read !! 
                  supplementID = ds.Tables[0].Rows[0]["SUPP_NO"].ToString();
                  //txtLynxID.Text = LynxID;
              }
              else
              {
                  //Couldnt read dbf file or dbf file is empty
              }

          }
          else
          {
              //Couldnt read dbf file or dbf file is empty
          }

          
      }
      public DataSet ExecuteDbase(string ConnectionString, string Query)
      {
          DataSet ds = new DataSet();
          OleDbConnection conn = new OleDbConnection(ConnectionString);

          conn.Open();
          OleDbDataAdapter adapter = new OleDbDataAdapter(Query, conn);
          ds = new DataSet();
          adapter.Fill(ds);
          conn.Close();
          return ds;
      }

      #endregion

      #region Configuration Handler Code

      /// <summary>
      /// Read the configuration settings
      /// </summary>
      private void readConfig()
      {
          UserType = getSetting("//UserType");
         strWPLInPath = getSetting("//Inbox");
         strWPLOutPath = getSetting("//Outbox");
         strAPDArchivePath = getSetting("//Archive");
         strPathwaysDrive = getSetting("//PathwaysFolder");
         strEnvironment = getSetting("//Environment");
         if (strEnvironment == "") strEnvironment = "Development";
         strAPDURL = getSetting("//ApdUrl[@environment='" + strEnvironment +"']");
         strUserLogin = Environment.UserName;

         if (Directory.Exists(strPathwaysDrive + strUserLogin) == false)
         {
            Directory.CreateDirectory(strPathwaysDrive + strUserLogin);
         }

         if (Directory.Exists(strPathwaysDrive + strUserLogin + @"\WFLIN") == false)
         {
            Directory.CreateDirectory(strPathwaysDrive + strUserLogin + @"\WFLIN");
         }

         if (Directory.Exists(strPathwaysDrive + strUserLogin + @"\WFLOUT") == false)
         {
            Directory.CreateDirectory(strPathwaysDrive + strUserLogin + @"\WFLOUT");
         }

         if (Directory.Exists(strPathwaysDrive + strUserLogin + @"\ASG") == false)
         {
            Directory.CreateDirectory(strPathwaysDrive + strUserLogin + @"\ASG");
         }

         //read the analysts
         strAnalysts.Clear();
         XmlNodeList nodeAnalysts = xmlConfig.SelectNodes("//Analyst");
         for (int i = 0; i < nodeAnalysts.Count; i++)
         {
            strAnalysts.Add(nodeAnalysts[i].InnerText);
         }
         nodeAnalysts = null;

         strUserID = getSetting("//UserID[@login='" + strUserLogin + "']");

         if (strUserID == "")
         {
            //get the APD user id.
            string strRequest = "<Root><StoredProc name='uspSessionGetUserDetailXML' execType='executespnpasxml'><Data><![CDATA[Login=" + strUserLogin + "]]></Data></StoredProc></Root>";
            clsWeb request = new clsWeb(strAPDURL);

            string strResponse = request.GetApdData("/rs/apdsave.asp", strRequest);
            request = null;

            if (strResponse != "")
            {
               XmlDocument xmlDoc = new XmlDocument();
               xmlDoc.LoadXml(strResponse);


               XmlElement xmlResult = (XmlElement)xmlDoc.SelectSingleNode("//User");
               if (xmlResult != null)
               {
                  strUserID = xmlResult.GetAttribute("UserID");

                  setSetting("/Root/UserID", "login='" + strUserLogin + "'", strUserID);
               }

               if (strUserID == "" || strUserID == "0")
               {
                  MessageBox.Show("Invalid APD User " + strUserLogin + ". Please add user to APD first.", "APD Pathways Helper");
                  Application.Exit();
               }
            }

         }
      }

      /// <summary>
      /// Utility function to get the configuration setting
      /// </summary>
      /// <param name="strNode"></param>
      /// <returns></returns>
      private string getSetting(string strNode)
      {
         string strRet = "";
         XmlElement objNode = null;
         if (strNode != "")
         {
            if (xmlConfig == null)
            {
               initConfig();
            }

            objNode = (XmlElement)xmlConfig.SelectSingleNode(strNode);
            if (objNode != null)
            {
               strRet = objNode.InnerText;
            }
         }
         return strRet;
      }

      private void setSetting(string strNode, string strAttrib, string strValue)
      {
         XmlElement objNode = null;
         if (strNode != "")
         {
            if (xmlConfig == null)
            {
               initConfig();
            }

            objNode = (XmlElement)xmlConfig.SelectSingleNode(strNode);
            if (objNode != null)
            {
               objNode.InnerText = strValue;
            }
            else
            {
               string strNodeName = strNode.Substring(strNode.LastIndexOf("/") + 1);
               objNode = xmlConfig.CreateElement(strNodeName);
               if (strAttrib != "")
               {
                  string strAttribName = strAttrib;
                  string strAttribValue = "";
                  if (strAttrib.IndexOf("=") != -1)
                  {
                     string[] strSeparator = new string[] { "=" };

                     strAttribName = strAttrib.Split(strSeparator, StringSplitOptions.None)[0];
                     strAttribValue = strAttrib.Split(strSeparator, StringSplitOptions.None)[1];
                     strAttribValue = strAttribValue.Replace("'", "");
                  }

                  objNode.SetAttribute(strAttribName, strAttribValue);
               }
               objNode.InnerText = strValue;
               xmlConfig.DocumentElement.AppendChild(objNode);
            }

            xmlConfig.Save(Environment.CurrentDirectory + @"\config.xml");
         }
      }

      private void initConfig()
      {
         xmlConfig = new XmlDocument();
         xmlConfig.Load(Environment.CurrentDirectory + @"\config.xml");
      }
      #endregion

      #region File System Watcher

      public void startWatching()
      {
         timer1.Interval = 5000;
         timer1.Enabled = true;

         timer2.Interval = 2000;
         timer2.Enabled = true;
      }

      public void CreateWatcher()
      {
         watcher.Filter = "*.awf";
         watcher.Created += new FileSystemEventHandler(watcher_FileCreated);
         watcher.Path = strWPLOutPath;
         watcher.EnableRaisingEvents = true;
      }

      void watcher_FileCreated(object sender, FileSystemEventArgs e)
      {         
         if (awfFiles.IndexOf(e.Name) == -1)
         {
            frmExport exp = new frmExport();
            exp.strFileName = e.FullPath;
            exp.strArchivePath = strUserArchivePath;
            exp.strAPDUrl = strAPDURL;
            exp.strUserID = strUserID;
            exp.Show(this);
            awfFiles.Add(e.Name);

            if (awfFiles.Count > 10)
            {
               awfFiles.RemoveRange(0, awfFiles.Count - 10);
            }
         }
      }

      private void copyAssignments(string strSourcePath, string strDestinationPath)
      {
         if (Directory.Exists(strSourcePath) && Directory.Exists(strDestinationPath))
         {
            string[] strAssignmentFiles = Directory.GetFiles(strSourcePath, "*.asg");
            FileInfo fi = null;
            DateTime dt = DateTime.Now.AddMinutes(-2);
            for (int i = 0; i < strAssignmentFiles.Count(); i++)
            {
               fi = new FileInfo(strAssignmentFiles[i]);
               if (fi.LastWriteTime < dt)
               {
                  File.Move(strAssignmentFiles[i], strDestinationPath + @"\" + fi.Name);
                  intAssignmentsCopied++;
               }
            }
         }
      }

      private void copyAWE(string strSourcePath, string strDestinationPath)
      {
         if (Directory.Exists(strSourcePath) && Directory.Exists(strDestinationPath))
         {
            string[] strAssignmentFiles = Directory.GetFiles(strSourcePath, "*.awf");
            FileInfo fi = null;
            DateTime dt = DateTime.Now.AddMinutes(-2);
            for (int i = 0; i < strAssignmentFiles.Count(); i++)
            {
               fi = new FileInfo(strAssignmentFiles[i]);
               if (fi.LastWriteTime < dt)
               {
                  File.Move(strAssignmentFiles[i], strDestinationPath + @"\" + fi.Name);
                  intAWFCopied++;
               }
            }
         }
      }

      #endregion

      private void button1_Click(object sender, EventArgs e)
      {
          frmHelper help = new frmHelper();
          help.ShowDialog();          
      }


   }
}
