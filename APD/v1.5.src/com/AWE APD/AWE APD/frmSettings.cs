﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace AWE_APD
{
   public partial class frmSettings : Form
   {
      public frmSettings()
      {
         InitializeComponent();
      }

      private void buttonSave_Click(object sender, EventArgs e)
      {
         if (validateData() == true)
         {

            XmlDocument xmlConfig = new XmlDocument();
            xmlConfig.Load(Environment.CurrentDirectory + @"\config.xml");

            XmlNode node = xmlConfig.SelectSingleNode("//Inbox");
            node.InnerText = txtInbox.Text;

            node = xmlConfig.SelectSingleNode("//Outbox");
            node.InnerText = txtOutbox.Text;

            node = xmlConfig.SelectSingleNode("//Archive");
            node.InnerText = txtArchiveFolder.Text;

            node = xmlConfig.SelectSingleNode("//Environment");
            node.InnerText = cmbEnvironment.SelectedItem.ToString();

            node = xmlConfig.SelectSingleNode("//PathwaysFolder");
            node.InnerText = textBoxPathways.Text.Trim();

            xmlConfig.Save(Environment.CurrentDirectory + @"\config.xml");

            node = null;
            xmlConfig = null;

            this.Close();
         }
      }

      private void frmSettings_Load(object sender, EventArgs e)
      {
         XmlDocument xmlConfig = new XmlDocument();
         xmlConfig.Load(Environment.CurrentDirectory + @"\config.xml");

         XmlNode node = xmlConfig.SelectSingleNode("//Inbox");
         txtInbox.Text = node.InnerText;

         node = xmlConfig.SelectSingleNode("//Outbox");
         txtOutbox.Text = node.InnerText;

         node = xmlConfig.SelectSingleNode("//Archive");
         txtArchiveFolder.Text = node.InnerText;

         node = xmlConfig.SelectSingleNode("//PathwaysFolder");
         textBoxPathways.Text = node.InnerText;

         node = xmlConfig.SelectSingleNode("//Environment");
         cmbEnvironment.SelectedItem = node.InnerText;

         xmlConfig = null;
      }

      private Boolean validateData()
      {
         //if (txtInbox.Text.Trim() == "")
         //{
         //   MessageBox.Show("Inbox Path cannot be empty. Please point to the local Pathways WPFIN folder.", "APD Pathways Helper");
         //   txtInbox.Focus();
         //   return false;
         //}

         //if (txtOutbox.Text.Trim() == "")
         //{
         //   MessageBox.Show("Outbox Path cannot be empty. Please point to the local Pathways WPFIN folder.", "APD Pathways Helper");
         //   txtOutbox.Focus();
         //   return false;
         //}

         //if (txtArchiveFolder.Text.Trim() == "")
         //{
         //   MessageBox.Show("Archive Path cannot be empty. Please point to the local Pathways WPFIN folder.", "APD Pathways Helper");
         //   txtArchiveFolder.Focus();
         //   return false;
         //}

         if (textBoxPathways.Text.Trim() == "")
         {
            MessageBox.Show("Pathways Assignment Folder cannot be empty. Please point to the Pathways drive.", "APD Pathways Helper");
            textBoxPathways.Focus();
            return false;
         }

         if (System.IO.Directory.Exists(textBoxPathways.Text.Trim() + @"\" + Environment.UserName) == false)
         {
            MessageBox.Show("Invalid Pathways Assignment folder. Please point to the Pathways drive.", "APD Pathways Helper");
            textBoxPathways.Focus();
            return false;
         }

         return true;
      }

      private void buttonAnalysts_Click(object sender, EventArgs e)
      {
         frmSubscribe frm = new frmSubscribe();
         frm.strPathwaysDrive = textBoxPathways.Text.Trim();
         frm.ShowDialog();
      }

      private void buttonCancel_Click(object sender, EventArgs e)
      {
         this.Close();
      }

   }
}
