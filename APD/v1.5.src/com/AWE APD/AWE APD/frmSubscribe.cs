﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace AWE_APD
{
   public partial class frmSubscribe : Form
   {
      //private string strPathwaysDrive = @"D:\APD\AWE\"; //@"L:\";
      public string strPathwaysDrive
      {
         get;
         set;
      }

      public frmSubscribe()
      {
         InitializeComponent();
      }

      private void frmSubscribe_Load(object sender, EventArgs e)
      {
         //load settings
         loadAnalysts();
         textBoxCSRNo.Focus();
      }

      private void buttonSave_Click(object sender, EventArgs e)
      {
         saveAnalysts();
         this.Close();
      }

      private void buttonCancel_Click(object sender, EventArgs e)
      {
         this.Close();
      }

      private void buttonAdd_Click(object sender, EventArgs e)
      {
         if (textBoxCSRNo.Text.Trim() != "")
         {
            if (Directory.Exists(strPathwaysDrive + textBoxCSRNo.Text.Trim()) == true)
            {
               string strCSRNo = textBoxCSRNo.Text.Trim();

               //int CSRNumber;
               //bool isNumber;
               //isNumber = int.TryParse(strCSRNo, out CSRNumber);

               //if (strCSRNo.Length == 4 && isNumber == true) strCSRNo = "CSR" + strCSRNo;

               if (listBoxAnalysts.Items.IndexOf(strCSRNo) == -1)
               {
                  listBoxAnalysts.Items.Add(strCSRNo);
               }
               //else
               //{
               //   if (strCSRNo.Length != 7)
               //   {
               //      MessageBox.Show("Analyst CSR# must be in the following format 'CSR####' where # is a number.", "APD Pathways Helper");
               //   }

               //   if (listBoxAnalysts.Items.IndexOf(strCSRNo) == -1)
               //   {
               //      MessageBox.Show("You are already subscribing to this Analyst.", "APD Pathways Helper");
               //   }
               //}
            }
            else
            {
               MessageBox.Show("Invalid Analyst CSR#.", "APD Pathways Helper");
            }
         }
      }

      private void buttonDelete_Click(object sender, EventArgs e)
      {
         if (listBoxAnalysts.SelectedIndex != -1)
         {
            listBoxAnalysts.Items.RemoveAt(listBoxAnalysts.SelectedIndex);
         }
      }
      private void loadAnalysts()
      {
         XmlDocument xmlConfig = new XmlDocument();
         xmlConfig.Load(Environment.CurrentDirectory + @"\config.xml");

         XmlNodeList nodeAnalysts = xmlConfig.SelectNodes("//Analyst");
         for (int i = 0; i < nodeAnalysts.Count; i++ )
         {
            XmlElement nodeAnalyst = (XmlElement)nodeAnalysts[i];
            listBoxAnalysts.Items.Add(nodeAnalyst.InnerText);
         }

         nodeAnalysts = null;
         xmlConfig = null;
      }

      private void saveAnalysts()
      {
         XmlDocument xmlConfig = new XmlDocument();
         xmlConfig.Load(Environment.CurrentDirectory + @"\config.xml");

         XmlNodeList nodeAnalysts = xmlConfig.SelectNodes("//Analyst");
         for (int i = nodeAnalysts.Count - 1; i >= 0; i--)
         {
            xmlConfig.DocumentElement.RemoveChild(nodeAnalysts[i]);
         }

         for (int i = 0; i < listBoxAnalysts.Items.Count; i++)
         {
            XmlNode nodeAnalyst = (XmlNode)xmlConfig.CreateElement("Analyst");
            nodeAnalyst.InnerText = listBoxAnalysts.Items[i].ToString();
            xmlConfig.DocumentElement.AppendChild(nodeAnalyst);
         }

         xmlConfig.Save(Environment.CurrentDirectory + @"\config.xml");

         nodeAnalysts = null;
         xmlConfig = null;
      }

   }
}
