﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace AWE_APD
{
   class clsWeb
   {
      public string _baseURL = "";

      public clsWeb(string strURL)
      {
         if (strURL != "" && strURL != null)
         {
            strURL = strURL.Trim();
            if (strURL.StartsWith("http://", StringComparison.OrdinalIgnoreCase) == false &&
                strURL.StartsWith("https://", StringComparison.OrdinalIgnoreCase) == false)
               new Exception("URL does not start with http: or https:");

            if (strURL.EndsWith("/") == true)
               strURL = strURL.Substring(0, strURL.Length - 1);
            _baseURL = strURL;
         }
         else
         {
            new Exception("Invalid URL");
         }
      }

      public string GetApdData(string strURL, string strRequest)
      {
         string strRet = "";

         ASCIIEncoding encoder = new ASCIIEncoding();

         HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_baseURL + "/rs/apdsave.asp");
         byte[] requestBuffer = encoder.GetBytes(strRequest);
         
         request.Method = "POST";
         request.ContentType = "application/x-www-form-urlencoded";
         request.ContentLength = strRequest.Length;

         Stream PostData = request.GetRequestStream();
         PostData.Write(requestBuffer, 0, requestBuffer.Length);
         PostData.Close();

         HttpWebResponse response = (HttpWebResponse)request.GetResponse();
         
         StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.ASCII);
         strRet = reader.ReadToEnd();
         reader.Close();

         response.Close();


         return strRet;
      }
   }
}
