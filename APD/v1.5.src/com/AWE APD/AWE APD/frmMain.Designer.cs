﻿namespace AWE_APD
{
   partial class frmMain
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.components = new System.ComponentModel.Container();
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
          this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
          this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
          this.exportFileToAPDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.importFileToPathwaysToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
          this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.label1 = new System.Windows.Forms.Label();
          this.buttonExport = new System.Windows.Forms.Button();
          this.buttonImport = new System.Windows.Forms.Button();
          this.buttonInstall = new System.Windows.Forms.Button();
          this.buttonClose = new System.Windows.Forms.Button();
          this.buttonSettings = new System.Windows.Forms.Button();
          this.watcher = new System.IO.FileSystemWatcher();
          this.buttonSubscribe = new System.Windows.Forms.Button();
          this.timer1 = new System.Windows.Forms.Timer(this.components);
          this.timer2 = new System.Windows.Forms.Timer(this.components);
          this.button1 = new System.Windows.Forms.Button();
          this.contextMenuStrip1.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.watcher)).BeginInit();
          this.SuspendLayout();
          // 
          // notifyIcon1
          // 
          this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
          this.notifyIcon1.BalloonTipText = "APD Pathways Helper";
          this.notifyIcon1.BalloonTipTitle = "APD Pathways";
          this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
          this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
          this.notifyIcon1.Text = "APD Pathways Helper";
          this.notifyIcon1.Visible = true;
          this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
          // 
          // contextMenuStrip1
          // 
          this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportFileToAPDToolStripMenuItem,
            this.importFileToPathwaysToolStripMenuItem,
            this.toolStripMenuItem1,
            this.settingsToolStripMenuItem,
            this.exitToolStripMenuItem});
          this.contextMenuStrip1.Name = "contextMenuStrip1";
          this.contextMenuStrip1.Size = new System.Drawing.Size(187, 98);
          // 
          // exportFileToAPDToolStripMenuItem
          // 
          this.exportFileToAPDToolStripMenuItem.Name = "exportFileToAPDToolStripMenuItem";
          this.exportFileToAPDToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
          this.exportFileToAPDToolStripMenuItem.Text = "Export File to APD";
          this.exportFileToAPDToolStripMenuItem.Click += new System.EventHandler(this.exportFileToAPDToolStripMenuItem_Click);
          // 
          // importFileToPathwaysToolStripMenuItem
          // 
          this.importFileToPathwaysToolStripMenuItem.Name = "importFileToPathwaysToolStripMenuItem";
          this.importFileToPathwaysToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
          this.importFileToPathwaysToolStripMenuItem.Text = "Re-export File to APD";
          this.importFileToPathwaysToolStripMenuItem.Click += new System.EventHandler(this.importFileToPathwaysToolStripMenuItem_Click);
          // 
          // toolStripMenuItem1
          // 
          this.toolStripMenuItem1.Name = "toolStripMenuItem1";
          this.toolStripMenuItem1.Size = new System.Drawing.Size(183, 6);
          // 
          // settingsToolStripMenuItem
          // 
          this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
          this.settingsToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
          this.settingsToolStripMenuItem.Text = "Settings";
          this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
          // 
          // exitToolStripMenuItem
          // 
          this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
          this.exitToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
          this.exitToolStripMenuItem.Text = "Exit";
          this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(13, 11);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(166, 13);
          this.label1.TabIndex = 0;
          this.label1.Text = "APD Pathways Helper application";
          // 
          // buttonExport
          // 
          this.buttonExport.Location = new System.Drawing.Point(16, 44);
          this.buttonExport.Name = "buttonExport";
          this.buttonExport.Size = new System.Drawing.Size(75, 23);
          this.buttonExport.TabIndex = 1;
          this.buttonExport.Text = "Export";
          this.buttonExport.UseVisualStyleBackColor = true;
          this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
          // 
          // buttonImport
          // 
          this.buttonImport.Enabled = false;
          this.buttonImport.Location = new System.Drawing.Point(16, 73);
          this.buttonImport.Name = "buttonImport";
          this.buttonImport.Size = new System.Drawing.Size(75, 23);
          this.buttonImport.TabIndex = 2;
          this.buttonImport.Text = "Import";
          this.buttonImport.UseVisualStyleBackColor = true;
          this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
          // 
          // buttonInstall
          // 
          this.buttonInstall.Location = new System.Drawing.Point(150, 73);
          this.buttonInstall.Name = "buttonInstall";
          this.buttonInstall.Size = new System.Drawing.Size(75, 23);
          this.buttonInstall.TabIndex = 3;
          this.buttonInstall.Text = "Install";
          this.buttonInstall.UseVisualStyleBackColor = true;
          this.buttonInstall.Click += new System.EventHandler(this.buttonInstall_Click);
          // 
          // buttonClose
          // 
          this.buttonClose.Location = new System.Drawing.Point(150, 138);
          this.buttonClose.Name = "buttonClose";
          this.buttonClose.Size = new System.Drawing.Size(75, 23);
          this.buttonClose.TabIndex = 4;
          this.buttonClose.Text = "Close";
          this.buttonClose.UseVisualStyleBackColor = true;
          this.buttonClose.Click += new System.EventHandler(this.button4_Click);
          // 
          // buttonSettings
          // 
          this.buttonSettings.Location = new System.Drawing.Point(150, 44);
          this.buttonSettings.Name = "buttonSettings";
          this.buttonSettings.Size = new System.Drawing.Size(75, 23);
          this.buttonSettings.TabIndex = 5;
          this.buttonSettings.Text = "Settings";
          this.buttonSettings.UseVisualStyleBackColor = true;
          this.buttonSettings.Click += new System.EventHandler(this.buttonSettings_Click);
          // 
          // watcher
          // 
          this.watcher.EnableRaisingEvents = true;
          this.watcher.SynchronizingObject = this;
          // 
          // buttonSubscribe
          // 
          this.buttonSubscribe.Location = new System.Drawing.Point(16, 102);
          this.buttonSubscribe.Name = "buttonSubscribe";
          this.buttonSubscribe.Size = new System.Drawing.Size(75, 23);
          this.buttonSubscribe.TabIndex = 6;
          this.buttonSubscribe.Text = "Subscribe";
          this.buttonSubscribe.UseVisualStyleBackColor = true;
          this.buttonSubscribe.Click += new System.EventHandler(this.buttonSubscribe_Click);
          // 
          // timer1
          // 
          this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
          // 
          // timer2
          // 
          this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
          // 
          // button1
          // 
          this.button1.Location = new System.Drawing.Point(16, 131);
          this.button1.Name = "button1";
          this.button1.Size = new System.Drawing.Size(75, 23);
          this.button1.TabIndex = 7;
          this.button1.Text = "Helper";
          this.button1.UseVisualStyleBackColor = true;
          this.button1.Click += new System.EventHandler(this.button1_Click);
          // 
          // frmMain
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(240, 173);
          this.ControlBox = false;
          this.Controls.Add(this.button1);
          this.Controls.Add(this.buttonSubscribe);
          this.Controls.Add(this.buttonSettings);
          this.Controls.Add(this.buttonClose);
          this.Controls.Add(this.buttonInstall);
          this.Controls.Add(this.buttonImport);
          this.Controls.Add(this.buttonExport);
          this.Controls.Add(this.label1);
          this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
          this.MaximizeBox = false;
          this.MinimizeBox = false;
          this.Name = "frmMain";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
          this.Text = "APD Pathways Helper";
          this.Load += new System.EventHandler(this.Form1_Load);
          this.Shown += new System.EventHandler(this.frmMain_Shown);
          this.Resize += new System.EventHandler(this.Form1_Resize);
          this.contextMenuStrip1.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.watcher)).EndInit();
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.NotifyIcon notifyIcon1;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Button buttonExport;
      private System.Windows.Forms.Button buttonImport;
      private System.Windows.Forms.Button buttonInstall;
      private System.Windows.Forms.Button buttonClose;
      private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
      private System.Windows.Forms.ToolStripMenuItem exportFileToAPDToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem importFileToPathwaysToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
      private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
      private System.Windows.Forms.Button buttonSettings;
      private System.IO.FileSystemWatcher watcher;
      private System.Windows.Forms.Button buttonSubscribe;
      private System.Windows.Forms.Timer timer1;
      private System.Windows.Forms.Timer timer2;
      private System.Windows.Forms.Button button1;
   }
}

