﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using System.IO;
using IWshRuntimeLibrary;

namespace AWE_APD
{
   public partial class frmInstall : Form
   {
      public frmInstall()
      {
         InitializeComponent();
      }

      private void frmInstall_Load(object sender, EventArgs e)
      {
         
      }

      private void InstallApp()
      {
         labelStartupProgress.ForeColor = Color.Goldenrod;
         labelStartupProgress.Text = "3";
         labelStartupProgress.Refresh();

         if (CreateAppShortcut("Startup") == true)
         {
            labelStartupProgress.ForeColor = Color.ForestGreen;
            labelStartupProgress.Text = "a";
            labelStartupProgress.Refresh();
         }
         else
         {
            labelStartupProgress.ForeColor = Color.Red;
            labelStartupProgress.Text = "r";
            labelStartupProgress.Refresh();
         }

         labelDesktopProgress.ForeColor = Color.Goldenrod;
         labelDesktopProgress.Text = "3";
         labelDesktopProgress.Refresh();

         if (CreateAppShortcut("Desktop") == true)
         {
            labelDesktopProgress.ForeColor = Color.ForestGreen;
            labelDesktopProgress.Text = "a";
            labelDesktopProgress.Refresh();
         }
         else
         {
            labelDesktopProgress.ForeColor = Color.Red;
            labelDesktopProgress.Text = "r";
            labelDesktopProgress.Refresh();
         }

         this.Close();
      }

      private bool CreateAppShortcut(string strLocation)
      {
         bool blnRet = false;
         string strShortCutDir = "";

         switch (strLocation)
         {
            case "Startup":
               strShortCutDir = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
               break;
            case "Desktop":
               strShortCutDir = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
               break;
         }

         if (strShortCutDir != "")
         {
            try
            {
               //using (StreamWriter writer = new StreamWriter(strShortCutDir + "\\APD Pathways Helper.url"))
               //{
               //   string app = System.Reflection.Assembly.GetExecutingAssembly().Location;
               //   writer.WriteLine("[InternetShortcut]");
               //   writer.WriteLine("URL=" + app);
               //   writer.WriteLine("IconIndex=0");
               //   writer.WriteLine("IconFile=" + app);
               //   writer.Flush();

               //   blnRet = true;
               //}

               WshShellClass wshShell = new WshShellClass();
               IWshRuntimeLibrary.IWshShortcut MyShortcut = (IWshRuntimeLibrary.IWshShortcut) wshShell.CreateShortcut(strShortCutDir + @"\APD Pathways Helper.lnk");
               MyShortcut.TargetPath = Application.ExecutablePath;
               MyShortcut.WorkingDirectory = Application.StartupPath;
               MyShortcut.Description = "APD Pathways Helper";
               MyShortcut.IconLocation = Application.StartupPath + @"\app.ico";
               MyShortcut.Save();

               blnRet = true;
            }
            catch (Exception ex)
            {
               MessageBox.Show("An Error occured. " + ex.Message, "APD Pathways Helper", MessageBoxButtons.OK, MessageBoxIcon.Error);
               blnRet = false;
            }
         }

         return blnRet;
      }

      private void timer1_Tick(object sender, EventArgs e)
      {
         timer1.Enabled = false;
         InstallApp();
      }

      private void button1_Click(object sender, EventArgs e)
      {
         this.Close();
      }
   }
}
