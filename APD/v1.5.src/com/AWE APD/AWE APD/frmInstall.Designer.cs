﻿namespace AWE_APD
{
   partial class frmInstall
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.components = new System.ComponentModel.Container();
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInstall));
         this.label1 = new System.Windows.Forms.Label();
         this.labelStartupProgress = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.label4 = new System.Windows.Forms.Label();
         this.labelDesktopProgress = new System.Windows.Forms.Label();
         this.button1 = new System.Windows.Forms.Button();
         this.timer1 = new System.Windows.Forms.Timer(this.components);
         this.SuspendLayout();
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label1.Location = new System.Drawing.Point(12, 9);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(186, 13);
         this.label1.TabIndex = 0;
         this.label1.Text = "Installing APD Pathways Helper";
         // 
         // labelStartupProgress
         // 
         this.labelStartupProgress.AutoSize = true;
         this.labelStartupProgress.Font = new System.Drawing.Font("Webdings", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
         this.labelStartupProgress.ForeColor = System.Drawing.Color.ForestGreen;
         this.labelStartupProgress.Location = new System.Drawing.Point(14, 36);
         this.labelStartupProgress.Name = "labelStartupProgress";
         this.labelStartupProgress.Size = new System.Drawing.Size(0, 30);
         this.labelStartupProgress.TabIndex = 1;
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(49, 45);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(116, 13);
         this.label3.TabIndex = 2;
         this.label3.Text = "Create Startup shortcut";
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(49, 81);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(149, 13);
         this.label4.TabIndex = 4;
         this.label4.Text = "Create User Desktop Shortcut";
         // 
         // labelDesktopProgress
         // 
         this.labelDesktopProgress.AutoSize = true;
         this.labelDesktopProgress.Font = new System.Drawing.Font("Webdings", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
         this.labelDesktopProgress.ForeColor = System.Drawing.Color.ForestGreen;
         this.labelDesktopProgress.Location = new System.Drawing.Point(14, 72);
         this.labelDesktopProgress.Name = "labelDesktopProgress";
         this.labelDesktopProgress.Size = new System.Drawing.Size(0, 30);
         this.labelDesktopProgress.TabIndex = 3;
         // 
         // button1
         // 
         this.button1.Location = new System.Drawing.Point(137, 130);
         this.button1.Name = "button1";
         this.button1.Size = new System.Drawing.Size(75, 23);
         this.button1.TabIndex = 5;
         this.button1.Text = "Cancel";
         this.button1.UseVisualStyleBackColor = true;
         this.button1.Click += new System.EventHandler(this.button1_Click);
         // 
         // timer1
         // 
         this.timer1.Enabled = true;
         this.timer1.Interval = 1000;
         this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
         // 
         // frmInstall
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(235, 173);
         this.Controls.Add(this.button1);
         this.Controls.Add(this.label4);
         this.Controls.Add(this.labelDesktopProgress);
         this.Controls.Add(this.label3);
         this.Controls.Add(this.labelStartupProgress);
         this.Controls.Add(this.label1);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "frmInstall";
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
         this.Text = "Installing Application";
         this.Load += new System.EventHandler(this.frmInstall_Load);
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label labelStartupProgress;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label labelDesktopProgress;
      private System.Windows.Forms.Button button1;
      private System.Windows.Forms.Timer timer1;
   }
}