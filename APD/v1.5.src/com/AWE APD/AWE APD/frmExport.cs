﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using System.Timers;

namespace AWE_APD
{
   public partial class frmExport : Form
   {
      private XmlDocument xmlDoc = null;
      private string strClaimAspectID = "";
      private string strClaimAspectServiceChannelID = "";
      private string strStatusID = "";
      private string strAnalystLogonID = "";
      //private string strPathwaysDrive = @"D:\APD\AWE\"; //@"L:\";

      public string strFileName
      {
         get;
         set;
      }

      public string strArchivePath
      {
         get;
         set;
      }

      public string strAPDUrl
      {
         get;
         set;
      }

      public string strUserID
      {
         get;
         set;
      }

      public string strPathwaysDrive
      {
         get;
         set;
      }

      public List<string> strAnalysts
      {
         get;
         set;
      }

      public frmExport()
      {
         InitializeComponent();
      }

      private void buttonCancel_Click(object sender, EventArgs e)
      {
         this.Close();
      }

      private void frmExport_Load(object sender, EventArgs e)
      {
         Int32 intFrmTop = Screen.PrimaryScreen.WorkingArea.Height - this.Height;
         Int32 intFrmLeft = Screen.PrimaryScreen.WorkingArea.Width - this.Width;

         this.Top = intFrmTop;
         this.Left = intFrmLeft;

         if (strFileName != null && strFileName != "")
         {
            // form was created by the automatic file creation.
            lblFileName.Text = strFileName;
            buttonOpen.Enabled = false;
            txtLynxID.Focus();
            labelHeader.Text = "A new file was added to the Pathways WFL Out folder.";
            buttonCancel.Enabled = false;

            buttonSave.Enabled = true;

            timer1.Enabled = true;
         }
         else
         {
            //manual export
            labelHeader.Text = "Open the file you want to export.";
            lblFileName.Text = "";

            buttonDelete.Enabled = false;
            buttonSave.Enabled = false;
         }
      }

      private void decodeFileName()
      {
         FileInfo fi = new FileInfo(lblFileName.Text);
         string strFileBaseName = fi.Name;
         fi = null;

         Regex reg = new Regex(@"(\d{7,})v(\d{1,})[ ]([E|S])(\d{1,})");
         Match m = reg.Match(strFileBaseName);
         if (m.Success)
         {
            if (m.Groups.Count == 5)
            {
               txtLynxID.Text = m.Groups[1].Value;
               
               ValidateLynxID();

               cmbVehicleNumber.Text = "Vehicle " + m.Groups[2].Value;
               if (m.Groups[3].Value == "E")
               {
                  cmbEstimateType.Text = "Estimate";
               }
               else
               {
                  cmbEstimateType.Text = "Supplement";
                  txtSequenceNumber.Text = m.Groups[4].Value;
               }


            }
         }
      }

      private void cmbEstimateType_SelectedIndexChanged(object sender, EventArgs e)
      {
         string strSelectedItem = cmbEstimateType.SelectedItem.ToString();
         if (strSelectedItem == "Estimate")
         {
            txtSequenceNumber.Enabled = false;
            txtSequenceNumber.Text = "";
         }
         else
         {
            txtSequenceNumber.Enabled = true;
            txtSequenceNumber.Text = "01";
         }
      }

      private void buttonSave_Click(object sender, EventArgs e)
      {
         bool blnAnalystPathFixed = false;

         if (validateData() == true)
         {
            //check if the archive path exists
            if (Directory.Exists(strArchivePath) == true)
            {
               string strDestinationFile = "";
               //get the original file extension.
               FileInfo fi = new FileInfo(strFileName);
               string strFileExt = fi.Extension;
               fi = null;

               //build the base file name
               string strVehicleNumber = cmbVehicleNumber.Text.Replace("Vehicle ", "");
               string strFileBaseName = txtLynxID.Text.Trim() + "v" + strVehicleNumber + " ";
               if (cmbEstimateType.SelectedItem.ToString() == "Estimate")
               {
                  strFileBaseName += "E1";
               } 
               else 
               {
                  strFileBaseName += "S" +  Convert.ToInt32(txtSequenceNumber.Text.Trim()).ToString();
               }

               strFileBaseName += strFileExt;

               //set the destination file path
               strDestinationFile = strArchivePath + @"\" + strFileBaseName;

               //check if the file already exists. if it does, then find a unique number
               int intDupCount = 1;
               if (File.Exists(strDestinationFile) == true)
               {
                  strDestinationFile = strArchivePath + @"\" + strFileBaseName.Replace(".", "_" + intDupCount.ToString() + ".");
                  intDupCount++;
               }
               
               //copy the file to the archive
               File.Copy(strFileName, strDestinationFile);

               //move the file to the analyst's inbox
               if (strAnalystLogonID != "")
               {
                  if (Directory.Exists(strPathwaysDrive + strAnalystLogonID) == false)
                  {
                     Directory.CreateDirectory(strPathwaysDrive + strAnalystLogonID + @"\WFLIN");
                     Directory.CreateDirectory(strPathwaysDrive + strAnalystLogonID + @"\WFLOUT");
                     blnAnalystPathFixed = true;
                  }
                  else
                  {
                     if (Directory.Exists(strPathwaysDrive + strAnalystLogonID + @"\WFLIN") == false)
                     {
                        Directory.CreateDirectory(strPathwaysDrive + strAnalystLogonID + @"\WFLIN");
                        blnAnalystPathFixed = true;
                     }

                     if (Directory.Exists(strPathwaysDrive + strAnalystLogonID + @"\WFLOUT") == false)
                     {
                        Directory.CreateDirectory(strPathwaysDrive + strAnalystLogonID + @"\WFLOUT");
                        blnAnalystPathFixed = true;
                     }
                  }

                  strDestinationFile = strPathwaysDrive + strAnalystLogonID + @"\WFLIN\" + strFileBaseName;

                  if (File.Exists(strDestinationFile) == true)
                  {
                     File.Delete(strDestinationFile);
                  }

                  File.Move(strFileName, strDestinationFile);
               }

               string strDescription = System.Uri.EscapeDataString(txtDescription.Text);

               if (strDescription == "") strDescription = "AWE File is available for review.";

               //add a note to the claim. Also, create a task for the MDS.
               string strRequest = "<Root>";
               string strData = "";
               strData = "ClaimAspectID=" + strClaimAspectID;
               strData += "&NoteTypeID=1";
               strData += "&StatusID=" + strStatusID;
               strData += "&Note=" + strDescription;
               strData += "&UserID=" + strUserID;
               strData += "&PrivateFlag=1";
               strData += "&ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID;

               strRequest += "<StoredProc name='uspNoteInsDetail' execType='executespnp'><Data><![CDATA[" + strData + "]]></Data></StoredProc>";

               strData = "ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID;
               strData += "&TaskID=73";
               strData += "&AlarmDate=" + DateTime.Now.AddHours(1);
               strData += "&UserTaskDescription=" + strDescription;
               strData += "&UserID=" + strUserID;

               strRequest += "<StoredProc name='uspDiaryInsDetail' execType='executespnp'><Data><![CDATA[" + strData + "]]></Data></StoredProc>";
               strRequest += "</Root>";

               clsWeb request = new clsWeb(strAPDUrl);
               string strResponse = request.GetApdData("/rs/apdsave.asp", strRequest);
               request = null;

               XmlDocument xmlResponse = new XmlDocument();
               xmlResponse.LoadXml(strResponse);

               XmlNodeList results = xmlResponse.SelectNodes("//StoredProc");
               if (results != null)
               {
                  XmlElement result;
                  string errorMessage = "";
                  for (int i = 0; i < results.Count; i++)
                  {
                     result = (XmlElement)results[i];
                     if (result.GetAttribute("returnCode") != "0")
                     {
                        switch (result.GetAttribute("name"))
                        {
                           case "uspNoteInsDetail":
                              errorMessage += "Unable to create notes to the claim. Details: " + System.Uri.UnescapeDataString(result.InnerText);
                              break;
                           case "uspDiaryInsDetail":
                              errorMessage += "Unable to create diary task to the claim. Details: " + System.Uri.UnescapeDataString(result.InnerText);
                              break;
                        }
                     }
                  }
                  if (errorMessage != "")
                  {
                     MessageBox.Show("File was successully saved to APD Archive. Unable to record information to the claim.\n\nError information:" + errorMessage, "APD Pathways Helper", MessageBoxButtons.OK, MessageBoxIcon.Error);
                  }
               }
               else
               {
                  MessageBox.Show("File was successully saved to APD Archive. Unable to record information to the claim.", "APD Pathways Helper", MessageBoxButtons.OK, MessageBoxIcon.Error);
               }

               if (blnAnalystPathFixed == true)
               {
                  MessageBox.Show("Analyst WFL IN/OUT folder were missing and has been created. Please update their Pathways profile or contact your supervisor.", "APD Pathways Helper", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
               }

               //close the window.
               this.Close();
            }
         }
      }

      private bool validateData()
      {
         if (txtLynxID.Text.Trim() == "")
         {
            MessageBox.Show("Please type the Lynx ID.", "APD Pathways Helper");
            txtLynxID.Focus();
            return false;
         }

         if (cmbVehicleNumber.Text.Trim() == "")
         {
            MessageBox.Show("Please select the Vehicle from the list.", "APD Pathways Helper");
            cmbVehicleNumber.Focus();
            return false;
         }

         if (cmbEstimateType.SelectedText == "Supplement")
         {
            if (txtSequenceNumber.Text.Trim() == "")
            {
               MessageBox.Show("Please type the sequence number.", "APD Pathways Helper");
               txtSequenceNumber.Focus();
               return false;
            }
         }

         if (xmlDoc == null)
         {
            return ValidateLynxID();
         }

         if (strClaimAspectID == "")
         {
            MessageBox.Show("Unable to determine the Vehicle's ID in the system.", "APD Pathways Helper");
            return false;
         }

         if (strStatusID == "")
         {
            MessageBox.Show("Unable to determine the Vehicle's status id in the system or the Lynx id does not have a Desk Audit service channel.", "APD Pathways Helper");
            return false;
         }

         return true;
      }

      private bool ValidateLynxID()
      {
         bool blnRet = false;
         string strRequest = "";
         string strResponse = "";

         this.Cursor = Cursors.WaitCursor;

         picProgress.Visible = true;
         picProgress.Refresh();
         cmbVehicleNumber.Items.Clear();
         strRequest = "<Root><StoredProc name='uspClaimExposureGetListXML' execType='executespnpasxml'><Data><![CDATA[LynxID=" + txtLynxID.Text.Trim() + "]]></Data></StoredProc></Root>";

         clsWeb request = new clsWeb(strAPDUrl);
         strResponse = request.GetApdData("/rs/apdsave.asp", strRequest);
         request = null;

         if (strResponse != "")
         {
            if (xmlDoc == null)
               xmlDoc = new XmlDocument();

            xmlDoc.LoadXml(strResponse);


            XmlElement xmlResult = (XmlElement)xmlDoc.SelectSingleNode("//Exposure");
            if (xmlResult != null)
            {
               XmlNodeList exposures = xmlDoc.SelectNodes("//Exposure");
               XmlElement exposure;
               for (int i = 0; i < exposures.Count; i++)
               {
                  exposure = (XmlElement)exposures[i];
                  //MessageBox.Show(exposure.GetAttribute("Name"));
                  cmbVehicleNumber.Items.Add(exposure.GetAttribute("Name"));
               }

               if (exposures.Count == 1)
               {
                  cmbVehicleNumber.SelectedIndex = 0;
               }
               blnRet = true;
            }
            else
            {
               MessageBox.Show("Invalid Lynx ID.", "APD Pathways Helper");
               xmlDoc = null;
            }

         }

         picProgress.Visible = false;
         this.Cursor = Cursors.Default;
         return blnRet;
      }

      private void buttonDelete_Click(object sender, EventArgs e)
      {
         if (File.Exists(strFileName)) 
            File.Delete(strFileName);
         this.Close();
      }

      private void buttonOpen_Click(object sender, EventArgs e)
      {
         //string strFileName = "";
         using (OpenFileDialog dlg = new OpenFileDialog())
         {
            dlg.CheckFileExists = true;
            dlg.CheckPathExists = true;
            dlg.DefaultExt = "awf";
            dlg.Filter = "Pathways WPF File|*.awf";
            dlg.Multiselect = false;
            dlg.ShowDialog();
            strFileName = dlg.FileName;
            if (strFileName != "" && File.Exists(strFileName))
            {
               lblFileName.Text = strFileName;
               buttonSave.Enabled = true;
            }
         }
      }

      private void txtLynxID_Leave(object sender, EventArgs e)
      {
         if (txtLynxID.Text.Trim() != "")
         {
            ValidateLynxID();
         }
      }

      private void cmbVehicleNumber_TextChanged(object sender, EventArgs e)
      {
         bool blnAnalystPathFixed = false;

         if (xmlDoc != null)
         {
            XmlElement exposure = (XmlElement)xmlDoc.SelectSingleNode("//Exposure[@Name='" + cmbVehicleNumber.SelectedItem + "']");
            if (exposure != null)
            {
               strAnalystLogonID = "";
               strClaimAspectID = exposure.GetAttribute("ClaimAspectID");
               strAnalystLogonID = exposure.GetAttribute("FAAPDLogonID");
               string strAnalystActive = exposure.GetAttribute("FAIsActive");
               string strAnalystName = exposure.GetAttribute("FAUserNameFirst") + " " + exposure.GetAttribute("FAUserNameLast");

               if (strAnalystActive != "1")
               {
                  MessageBox.Show(string.Format("Analyst {0} on {1} is not active. Cannot export this estimate.", strAnalystName, cmbVehicleNumber.SelectedText), "APD Pathways Helper", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                  buttonSave.Enabled = false;
               }
               else
               {
                  // check if the analyst is your subscription
                  if (strAnalysts.IndexOf(strAnalystLogonID) == -1)
                  {
                     string strSubscribedAnalysts = "";
                     for (int i = 0; i < strAnalysts.Count; i++)
                        strSubscribedAnalysts += strAnalysts[i].ToString() + ", ";
                     MessageBox.Show(string.Format("Analyst {0} is not in your subscription. Cannot export this estimate.\nYou are subscribed to the following Analysts: {1}", strAnalystName, strSubscribedAnalysts), "APD Pathways Helper", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                     buttonSave.Enabled = false;
                  }
                  else
                  {
                     buttonSave.Enabled = true;

                     if (Directory.Exists(strPathwaysDrive + strAnalystLogonID) == false)
                     {
                        Directory.CreateDirectory(strPathwaysDrive + strAnalystLogonID + @"\WFLIN");
                        Directory.CreateDirectory(strPathwaysDrive + strAnalystLogonID + @"\WFLOUT");
                        blnAnalystPathFixed = true;
                     }
                     else
                     {
                        if (Directory.Exists(strPathwaysDrive + strAnalystLogonID + @"\WFLIN") == false)
                        {
                           Directory.CreateDirectory(strPathwaysDrive + strAnalystLogonID + @"\WFLIN");
                           blnAnalystPathFixed = true;
                        }

                        if (Directory.Exists(strPathwaysDrive + strAnalystLogonID + @"\WFLOUT") == false)
                        {
                           Directory.CreateDirectory(strPathwaysDrive + strAnalystLogonID + @"\WFLOUT");
                           blnAnalystPathFixed = true;
                        }
                     }

                     if (blnAnalystPathFixed == true)
                     {
                        MessageBox.Show(string.Format("Analyst {0} WFL IN/OUT folder were missing and has been created. Please update their Pathways profile or contact your supervisor.", strAnalystName), "APD Pathways Helper", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                     }

                     cmbServiceChannel.Items.Clear();
                     XmlNodeList serviceChannels = exposure.SelectNodes("ServiceChannel");
                     XmlElement serviceChannel = null;
                     for (int i = 0; i < serviceChannels.Count; i++)
                     {
                        serviceChannel = (XmlElement)serviceChannels[i];
                        cmbServiceChannel.Items.Add(serviceChannel.GetAttribute("ServiceChannelName"));
                     }
                     if (serviceChannels.Count == 1)
                     {
                        cmbServiceChannel.SelectedIndex = 0;
                     }
                  }
               }
            }
            else
            {
               strClaimAspectID = "";
            }
         }
      }

      private void cmbServiceChannel_TextChanged(object sender, EventArgs e)
      {
         if (xmlDoc != null)
         {
            XmlElement serviceChannel = (XmlElement)xmlDoc.SelectSingleNode("//ServiceChannel[@ClaimAspectID='" + strClaimAspectID + "' and @ServiceChannelName='" + cmbServiceChannel.SelectedItem + "']");
            if (serviceChannel != null)
            {
               strStatusID = serviceChannel.GetAttribute("ServiceChannelStatusID");
               strClaimAspectServiceChannelID = serviceChannel.GetAttribute("ClaimAspectServiceChannelID");
            }
            else
            {
               strStatusID = "";
               strClaimAspectServiceChannelID = "";
            }
         }
      }

      private void txtLynxID_Enter(object sender, EventArgs e)
      {
         txtLynxID.SelectAll();
      }

      private void txtSequenceNumber_Enter(object sender, EventArgs e)
      {
         txtSequenceNumber.SelectAll();
      }

      private void cmbVehicleNumber_SelectedIndexChanged(object sender, EventArgs e)
      {

      }

      private void timer1_Tick(object sender, EventArgs e)
      {
         timer1.Enabled = false;
         decodeFileName();
      }

   }
}
