﻿namespace AWE_APD
{
   partial class frmImport
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmImport));
         this.label1 = new System.Windows.Forms.Label();
         this.txtVehicleNumber = new System.Windows.Forms.MaskedTextBox();
         this.label2 = new System.Windows.Forms.Label();
         this.txtSequenceNumber = new System.Windows.Forms.MaskedTextBox();
         this.label3 = new System.Windows.Forms.Label();
         this.cmbEstimateType = new System.Windows.Forms.ComboBox();
         this.lstResults = new System.Windows.Forms.ListBox();
         this.label4 = new System.Windows.Forms.Label();
         this.buttonSearch = new System.Windows.Forms.Button();
         this.buttonImport = new System.Windows.Forms.Button();
         this.buttonClose = new System.Windows.Forms.Button();
         this.labelSearchProgress = new System.Windows.Forms.Label();
         this.txtLynxID = new System.Windows.Forms.TextBox();
         this.SuspendLayout();
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(13, 13);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(46, 13);
         this.label1.TabIndex = 0;
         this.label1.Text = "Lynx ID:";
         // 
         // txtVehicleNumber
         // 
         this.txtVehicleNumber.Location = new System.Drawing.Point(89, 36);
         this.txtVehicleNumber.Mask = "00";
         this.txtVehicleNumber.Name = "txtVehicleNumber";
         this.txtVehicleNumber.Size = new System.Drawing.Size(100, 20);
         this.txtVehicleNumber.TabIndex = 2;
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(13, 39);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(55, 13);
         this.label2.TabIndex = 2;
         this.label2.Text = "Vehicle #:";
         // 
         // txtSequenceNumber
         // 
         this.txtSequenceNumber.Location = new System.Drawing.Point(246, 63);
         this.txtSequenceNumber.Mask = "00";
         this.txtSequenceNumber.Name = "txtSequenceNumber";
         this.txtSequenceNumber.Size = new System.Drawing.Size(34, 20);
         this.txtSequenceNumber.TabIndex = 4;
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(13, 65);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(69, 13);
         this.label3.TabIndex = 4;
         this.label3.Text = "Sequence #:";
         // 
         // cmbEstimateType
         // 
         this.cmbEstimateType.FormattingEnabled = true;
         this.cmbEstimateType.Items.AddRange(new object[] {
            "Estimate",
            "Supplement"});
         this.cmbEstimateType.Location = new System.Drawing.Point(89, 63);
         this.cmbEstimateType.Name = "cmbEstimateType";
         this.cmbEstimateType.Size = new System.Drawing.Size(151, 21);
         this.cmbEstimateType.TabIndex = 3;
         this.cmbEstimateType.SelectedIndexChanged += new System.EventHandler(this.cmbEstimateType_SelectedIndexChanged);
         // 
         // lstResults
         // 
         this.lstResults.FormattingEnabled = true;
         this.lstResults.Location = new System.Drawing.Point(16, 152);
         this.lstResults.Name = "lstResults";
         this.lstResults.Size = new System.Drawing.Size(264, 95);
         this.lstResults.TabIndex = 6;
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(13, 136);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(77, 13);
         this.label4.TabIndex = 8;
         this.label4.Text = "Search Result:";
         // 
         // buttonSearch
         // 
         this.buttonSearch.Location = new System.Drawing.Point(205, 90);
         this.buttonSearch.Name = "buttonSearch";
         this.buttonSearch.Size = new System.Drawing.Size(75, 23);
         this.buttonSearch.TabIndex = 5;
         this.buttonSearch.Text = "Search";
         this.buttonSearch.UseVisualStyleBackColor = true;
         this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
         // 
         // buttonImport
         // 
         this.buttonImport.Location = new System.Drawing.Point(124, 253);
         this.buttonImport.Name = "buttonImport";
         this.buttonImport.Size = new System.Drawing.Size(75, 23);
         this.buttonImport.TabIndex = 7;
         this.buttonImport.Text = "Import";
         this.buttonImport.UseVisualStyleBackColor = true;
         this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
         // 
         // buttonClose
         // 
         this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
         this.buttonClose.Location = new System.Drawing.Point(205, 253);
         this.buttonClose.Name = "buttonClose";
         this.buttonClose.Size = new System.Drawing.Size(75, 23);
         this.buttonClose.TabIndex = 8;
         this.buttonClose.Text = "Close";
         this.buttonClose.UseVisualStyleBackColor = true;
         this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
         // 
         // labelSearchProgress
         // 
         this.labelSearchProgress.AutoEllipsis = true;
         this.labelSearchProgress.Location = new System.Drawing.Point(16, 90);
         this.labelSearchProgress.Name = "labelSearchProgress";
         this.labelSearchProgress.Size = new System.Drawing.Size(173, 27);
         this.labelSearchProgress.TabIndex = 9;
         // 
         // txtLynxID
         // 
         this.txtLynxID.Location = new System.Drawing.Point(89, 10);
         this.txtLynxID.Name = "txtLynxID";
         this.txtLynxID.Size = new System.Drawing.Size(100, 20);
         this.txtLynxID.TabIndex = 1;
         // 
         // frmImport
         // 
         this.AcceptButton = this.buttonSearch;
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.CancelButton = this.buttonClose;
         this.ClientSize = new System.Drawing.Size(292, 288);
         this.Controls.Add(this.txtLynxID);
         this.Controls.Add(this.labelSearchProgress);
         this.Controls.Add(this.buttonClose);
         this.Controls.Add(this.buttonImport);
         this.Controls.Add(this.buttonSearch);
         this.Controls.Add(this.label4);
         this.Controls.Add(this.lstResults);
         this.Controls.Add(this.cmbEstimateType);
         this.Controls.Add(this.txtSequenceNumber);
         this.Controls.Add(this.label3);
         this.Controls.Add(this.txtVehicleNumber);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.label1);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "frmImport";
         this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
         this.Text = "Import Pathways File";
         this.Load += new System.EventHandler(this.frmImport_Load);
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.MaskedTextBox txtVehicleNumber;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.MaskedTextBox txtSequenceNumber;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.ComboBox cmbEstimateType;
      private System.Windows.Forms.ListBox lstResults;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Button buttonSearch;
      private System.Windows.Forms.Button buttonImport;
      private System.Windows.Forms.Button buttonClose;
      private System.Windows.Forms.Label labelSearchProgress;
      private System.Windows.Forms.TextBox txtLynxID;
   }
}