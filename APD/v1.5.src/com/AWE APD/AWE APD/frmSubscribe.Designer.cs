﻿namespace AWE_APD
{
   partial class frmSubscribe
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSubscribe));
         this.label1 = new System.Windows.Forms.Label();
         this.listBoxAnalysts = new System.Windows.Forms.ListBox();
         this.label2 = new System.Windows.Forms.Label();
         this.textBoxCSRNo = new System.Windows.Forms.TextBox();
         this.buttonAdd = new System.Windows.Forms.Button();
         this.buttonDelete = new System.Windows.Forms.Button();
         this.buttonSave = new System.Windows.Forms.Button();
         this.buttonCancel = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(9, 13);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(155, 13);
         this.label1.TabIndex = 1;
         this.label1.Text = "Analysts you are subscribing to:";
         // 
         // listBoxAnalysts
         // 
         this.listBoxAnalysts.FormattingEnabled = true;
         this.listBoxAnalysts.HorizontalScrollbar = true;
         this.listBoxAnalysts.Location = new System.Drawing.Point(12, 29);
         this.listBoxAnalysts.Name = "listBoxAnalysts";
         this.listBoxAnalysts.Size = new System.Drawing.Size(183, 186);
         this.listBoxAnalysts.TabIndex = 1;
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(9, 227);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(68, 13);
         this.label2.TabIndex = 3;
         this.label2.Text = "Analyst csr#:";
         // 
         // textBoxCSRNo
         // 
         this.textBoxCSRNo.Location = new System.Drawing.Point(83, 224);
         this.textBoxCSRNo.Name = "textBoxCSRNo";
         this.textBoxCSRNo.Size = new System.Drawing.Size(112, 20);
         this.textBoxCSRNo.TabIndex = 2;
         // 
         // buttonAdd
         // 
         this.buttonAdd.Location = new System.Drawing.Point(205, 224);
         this.buttonAdd.Name = "buttonAdd";
         this.buttonAdd.Size = new System.Drawing.Size(75, 23);
         this.buttonAdd.TabIndex = 3;
         this.buttonAdd.Text = "Add";
         this.buttonAdd.UseVisualStyleBackColor = true;
         this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
         // 
         // buttonDelete
         // 
         this.buttonDelete.Location = new System.Drawing.Point(205, 192);
         this.buttonDelete.Name = "buttonDelete";
         this.buttonDelete.Size = new System.Drawing.Size(75, 23);
         this.buttonDelete.TabIndex = 5;
         this.buttonDelete.Text = "Delete";
         this.buttonDelete.UseVisualStyleBackColor = true;
         this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
         // 
         // buttonSave
         // 
         this.buttonSave.Location = new System.Drawing.Point(205, 29);
         this.buttonSave.Name = "buttonSave";
         this.buttonSave.Size = new System.Drawing.Size(75, 23);
         this.buttonSave.TabIndex = 4;
         this.buttonSave.Text = "Save";
         this.buttonSave.UseVisualStyleBackColor = true;
         this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
         // 
         // buttonCancel
         // 
         this.buttonCancel.Location = new System.Drawing.Point(205, 58);
         this.buttonCancel.Name = "buttonCancel";
         this.buttonCancel.Size = new System.Drawing.Size(75, 23);
         this.buttonCancel.TabIndex = 6;
         this.buttonCancel.Text = "Cancel";
         this.buttonCancel.UseVisualStyleBackColor = true;
         this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
         // 
         // frmSubscribe
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(292, 262);
         this.Controls.Add(this.buttonCancel);
         this.Controls.Add(this.buttonSave);
         this.Controls.Add(this.buttonDelete);
         this.Controls.Add(this.buttonAdd);
         this.Controls.Add(this.textBoxCSRNo);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.listBoxAnalysts);
         this.Controls.Add(this.label1);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "frmSubscribe";
         this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
         this.Text = "Subscriptions";
         this.Load += new System.EventHandler(this.frmSubscribe_Load);
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.ListBox listBoxAnalysts;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.TextBox textBoxCSRNo;
      private System.Windows.Forms.Button buttonAdd;
      private System.Windows.Forms.Button buttonDelete;
      private System.Windows.Forms.Button buttonSave;
      private System.Windows.Forms.Button buttonCancel;
   }
}