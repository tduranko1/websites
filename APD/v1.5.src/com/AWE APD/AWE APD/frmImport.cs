﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AWE_APD
{
   public partial class frmImport : Form
   {
      List<string> searchResult = null;

      public string strArchivePath
      {
         get;
         set;
      }

      public string strInboxPath
      {
         get;
         set;
      }

      public string strPathwaysDrive
      {
         get;
         set;
      }

      public frmImport()
      {
         InitializeComponent();
      }

      private void buttonClose_Click(object sender, EventArgs e)
      {
         this.Close();
      }

      private void cmbEstimateType_SelectedIndexChanged(object sender, EventArgs e)
      {
         string strSelectedItem = cmbEstimateType.SelectedItem.ToString();
         if (strSelectedItem == "Estimate")
         {
            txtSequenceNumber.Enabled = false;
            txtSequenceNumber.Text = "";
         }
         else
         {
            txtSequenceNumber.Enabled = true;
            txtSequenceNumber.Text = "01";
         }
      }

      private void buttonSearch_Click(object sender, EventArgs e)
      {
         if (txtLynxID.Text.Trim() == "")
         {
            MessageBox.Show("Please type the Lynx ID.", "APD Pathways Helper");
            txtLynxID.Focus();
         }

         try
         {
            if (Convert.ToInt64(txtLynxID.Text.Trim()) > 0)
            {
               if (txtLynxID.Text != "")
               {
                  string searchMask = txtLynxID.Text.Trim() + "*.awf";
                  labelSearchProgress.Text = "Searching...";
                  labelSearchProgress.Refresh();
                  searchFile(strArchivePath, searchMask);
               }
            }
         }
         catch (Exception ex)
         {
            MessageBox.Show("Please type a numeric Lynx ID.", "APD Pathways Helper");
         }
      }

      private void searchFile(string strArchivePath, string searchMask)
      {
         if (Directory.Exists(strArchivePath))
         {
            lstResults.Items.Clear();
            if (searchResult == null)
               searchResult = new List<string>();
            searchResult.Clear();
            DirSearch(strArchivePath, searchMask);
            labelSearchProgress.Text = "Done.";
         }
      }

      private void DirSearch(string sDir, string sPattern)
      {
         try
         {
            foreach (string d in Directory.GetDirectories(sDir))
            {
               labelSearchProgress.Text = "Searching " + d;
               foreach (string f in Directory.GetFiles(d, sPattern))
               {
                  lstResults.Items.Add(f.Replace(strArchivePath, ""));

                  searchResult.Add(f);
               }

               DirSearch(d, sPattern);
            }
         }
         catch (System.Exception excpt)
         {
            labelSearchProgress.Text = excpt.Message;
         }
      }

      private void buttonImport_Click(object sender, EventArgs e)
      {
         if (lstResults.SelectedIndex != -1)
         {
            //MessageBox.Show(searchResult[lstResults.SelectedIndex]);
            string strDestinationFile = "";
            string strSourceFile = searchResult[lstResults.SelectedIndex];

            if (File.Exists(strSourceFile))
            {
               FileInfo fi = new FileInfo(strSourceFile);
               strDestinationFile = strPathwaysDrive + Environment.UserName + @"\WFLOUT\" + fi.Name;
               fi = null;

               if (File.Exists(strDestinationFile))
                  File.Delete(strDestinationFile);

               File.Copy(strSourceFile, strDestinationFile);

               //MessageBox.Show("File copied to Pathways Inbox.", "APD Pathways Helper", MessageBoxButtons.OK, MessageBoxIcon.Information);

               this.Close();
            }
            
         }
      }

      private void frmImport_Load(object sender, EventArgs e)
      {
         Int32 intFrmTop = Screen.PrimaryScreen.WorkingArea.Height - this.Height;
         Int32 intFrmLeft = Screen.PrimaryScreen.WorkingArea.Width - this.Width;

         this.Top = intFrmTop;
         this.Left = intFrmLeft;
      }

   }
}
