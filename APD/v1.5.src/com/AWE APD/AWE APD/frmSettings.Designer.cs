﻿namespace AWE_APD
{
   partial class frmSettings
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSettings));
         this.label1 = new System.Windows.Forms.Label();
         this.txtOutbox = new System.Windows.Forms.TextBox();
         this.txtInbox = new System.Windows.Forms.TextBox();
         this.label2 = new System.Windows.Forms.Label();
         this.txtArchiveFolder = new System.Windows.Forms.TextBox();
         this.label3 = new System.Windows.Forms.Label();
         this.buttonSave = new System.Windows.Forms.Button();
         this.label5 = new System.Windows.Forms.Label();
         this.cmbEnvironment = new System.Windows.Forms.ComboBox();
         this.buttonAnalysts = new System.Windows.Forms.Button();
         this.buttonCancel = new System.Windows.Forms.Button();
         this.textBoxPathways = new System.Windows.Forms.TextBox();
         this.label4 = new System.Windows.Forms.Label();
         this.SuspendLayout();
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(21, 258);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(132, 13);
         this.label1.TabIndex = 0;
         this.label1.Text = "Pathways WPL Out Folder";
         // 
         // txtOutbox
         // 
         this.txtOutbox.Location = new System.Drawing.Point(24, 274);
         this.txtOutbox.Name = "txtOutbox";
         this.txtOutbox.Size = new System.Drawing.Size(268, 20);
         this.txtOutbox.TabIndex = 1;
         // 
         // txtInbox
         // 
         this.txtInbox.Location = new System.Drawing.Point(24, 320);
         this.txtInbox.Name = "txtInbox";
         this.txtInbox.Size = new System.Drawing.Size(268, 20);
         this.txtInbox.TabIndex = 2;
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(21, 304);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(124, 13);
         this.label2.TabIndex = 2;
         this.label2.Text = "Pathways WPL In Folder";
         // 
         // txtArchiveFolder
         // 
         this.txtArchiveFolder.Location = new System.Drawing.Point(12, 79);
         this.txtArchiveFolder.Name = "txtArchiveFolder";
         this.txtArchiveFolder.Size = new System.Drawing.Size(268, 20);
         this.txtArchiveFolder.TabIndex = 3;
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(9, 63);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(103, 13);
         this.label3.TabIndex = 4;
         this.label3.Text = "APD Archive Folder:";
         // 
         // buttonSave
         // 
         this.buttonSave.Location = new System.Drawing.Point(124, 175);
         this.buttonSave.Name = "buttonSave";
         this.buttonSave.Size = new System.Drawing.Size(75, 23);
         this.buttonSave.TabIndex = 6;
         this.buttonSave.Text = "Save";
         this.buttonSave.UseVisualStyleBackColor = true;
         this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.Location = new System.Drawing.Point(9, 115);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(66, 13);
         this.label5.TabIndex = 9;
         this.label5.Text = "Environment";
         // 
         // cmbEnvironment
         // 
         this.cmbEnvironment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.cmbEnvironment.FormattingEnabled = true;
         this.cmbEnvironment.Items.AddRange(new object[] {
            "Development",
            "Staging",
            "Production"});
         this.cmbEnvironment.Location = new System.Drawing.Point(12, 131);
         this.cmbEnvironment.Name = "cmbEnvironment";
         this.cmbEnvironment.Size = new System.Drawing.Size(204, 21);
         this.cmbEnvironment.TabIndex = 5;
         // 
         // buttonAnalysts
         // 
         this.buttonAnalysts.Location = new System.Drawing.Point(12, 175);
         this.buttonAnalysts.Name = "buttonAnalysts";
         this.buttonAnalysts.Size = new System.Drawing.Size(75, 23);
         this.buttonAnalysts.TabIndex = 10;
         this.buttonAnalysts.Text = "My Analysts";
         this.buttonAnalysts.UseVisualStyleBackColor = true;
         this.buttonAnalysts.Click += new System.EventHandler(this.buttonAnalysts_Click);
         // 
         // buttonCancel
         // 
         this.buttonCancel.Location = new System.Drawing.Point(205, 175);
         this.buttonCancel.Name = "buttonCancel";
         this.buttonCancel.Size = new System.Drawing.Size(75, 23);
         this.buttonCancel.TabIndex = 11;
         this.buttonCancel.Text = "Cancel";
         this.buttonCancel.UseVisualStyleBackColor = true;
         this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
         // 
         // textBoxPathways
         // 
         this.textBoxPathways.Location = new System.Drawing.Point(12, 27);
         this.textBoxPathways.Name = "textBoxPathways";
         this.textBoxPathways.Size = new System.Drawing.Size(268, 20);
         this.textBoxPathways.TabIndex = 12;
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(9, 11);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(145, 13);
         this.label4.TabIndex = 13;
         this.label4.Text = "Pathways Assignment Folder:";
         // 
         // frmSettings
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(292, 211);
         this.Controls.Add(this.textBoxPathways);
         this.Controls.Add(this.label4);
         this.Controls.Add(this.buttonCancel);
         this.Controls.Add(this.buttonAnalysts);
         this.Controls.Add(this.cmbEnvironment);
         this.Controls.Add(this.label5);
         this.Controls.Add(this.buttonSave);
         this.Controls.Add(this.txtArchiveFolder);
         this.Controls.Add(this.label3);
         this.Controls.Add(this.txtInbox);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.txtOutbox);
         this.Controls.Add(this.label1);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "frmSettings";
         this.ShowInTaskbar = false;
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
         this.Text = "Settings";
         this.Load += new System.EventHandler(this.frmSettings_Load);
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox txtOutbox;
      private System.Windows.Forms.TextBox txtInbox;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.TextBox txtArchiveFolder;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Button buttonSave;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.ComboBox cmbEnvironment;
      private System.Windows.Forms.Button buttonAnalysts;
      private System.Windows.Forms.Button buttonCancel;
      private System.Windows.Forms.TextBox textBoxPathways;
      private System.Windows.Forms.Label label4;
   }
}