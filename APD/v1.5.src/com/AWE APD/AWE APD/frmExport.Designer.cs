﻿namespace AWE_APD
{
   partial class frmExport
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.components = new System.ComponentModel.Container();
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExport));
         this.cmbEstimateType = new System.Windows.Forms.ComboBox();
         this.label5 = new System.Windows.Forms.Label();
         this.labelHeader = new System.Windows.Forms.Label();
         this.buttonSave = new System.Windows.Forms.Button();
         this.label3 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.lblFileName = new System.Windows.Forms.Label();
         this.buttonDelete = new System.Windows.Forms.Button();
         this.buttonCancel = new System.Windows.Forms.Button();
         this.buttonOpen = new System.Windows.Forms.Button();
         this.txtSequenceNumber = new System.Windows.Forms.MaskedTextBox();
         this.label4 = new System.Windows.Forms.Label();
         this.txtDescription = new System.Windows.Forms.TextBox();
         this.cmbVehicleNumber = new System.Windows.Forms.ComboBox();
         this.picProgress = new System.Windows.Forms.PictureBox();
         this.cmbServiceChannel = new System.Windows.Forms.ComboBox();
         this.label6 = new System.Windows.Forms.Label();
         this.txtLynxID = new System.Windows.Forms.TextBox();
         this.timer1 = new System.Windows.Forms.Timer(this.components);
         ((System.ComponentModel.ISupportInitialize)(this.picProgress)).BeginInit();
         this.SuspendLayout();
         // 
         // cmbEstimateType
         // 
         this.cmbEstimateType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.cmbEstimateType.FormattingEnabled = true;
         this.cmbEstimateType.Items.AddRange(new object[] {
            "Estimate",
            "Supplement"});
         this.cmbEstimateType.Location = new System.Drawing.Point(92, 214);
         this.cmbEstimateType.Name = "cmbEstimateType";
         this.cmbEstimateType.Size = new System.Drawing.Size(92, 21);
         this.cmbEstimateType.TabIndex = 4;
         this.cmbEstimateType.SelectedIndexChanged += new System.EventHandler(this.cmbEstimateType_SelectedIndexChanged);
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.Location = new System.Drawing.Point(12, 110);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(126, 13);
         this.label5.TabIndex = 18;
         this.label5.Text = "Export this file to APD as:";
         // 
         // labelHeader
         // 
         this.labelHeader.AutoEllipsis = true;
         this.labelHeader.Location = new System.Drawing.Point(12, 9);
         this.labelHeader.Name = "labelHeader";
         this.labelHeader.Size = new System.Drawing.Size(265, 13);
         this.labelHeader.TabIndex = 17;
         this.labelHeader.Text = "A new file was added to the Pathways WFL Out folder.";
         // 
         // buttonSave
         // 
         this.buttonSave.Enabled = false;
         this.buttonSave.Location = new System.Drawing.Point(76, 341);
         this.buttonSave.Name = "buttonSave";
         this.buttonSave.Size = new System.Drawing.Size(75, 23);
         this.buttonSave.TabIndex = 7;
         this.buttonSave.Text = "Save";
         this.buttonSave.UseVisualStyleBackColor = true;
         this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(12, 218);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(69, 13);
         this.label3.TabIndex = 12;
         this.label3.Text = "Sequence #:";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(12, 160);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(55, 13);
         this.label2.TabIndex = 11;
         this.label2.Text = "Vehicle #:";
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(12, 134);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(50, 13);
         this.label1.TabIndex = 10;
         this.label1.Text = "LYNX Id:";
         // 
         // lblFileName
         // 
         this.lblFileName.AutoEllipsis = true;
         this.lblFileName.Location = new System.Drawing.Point(12, 34);
         this.lblFileName.Name = "lblFileName";
         this.lblFileName.Size = new System.Drawing.Size(257, 13);
         this.lblFileName.TabIndex = 20;
         this.lblFileName.Text = "New File Name";
         // 
         // buttonDelete
         // 
         this.buttonDelete.Location = new System.Drawing.Point(157, 60);
         this.buttonDelete.Name = "buttonDelete";
         this.buttonDelete.Size = new System.Drawing.Size(75, 23);
         this.buttonDelete.TabIndex = 9;
         this.buttonDelete.Text = "Delete";
         this.buttonDelete.UseVisualStyleBackColor = true;
         this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
         // 
         // buttonCancel
         // 
         this.buttonCancel.Location = new System.Drawing.Point(157, 341);
         this.buttonCancel.Name = "buttonCancel";
         this.buttonCancel.Size = new System.Drawing.Size(75, 23);
         this.buttonCancel.TabIndex = 8;
         this.buttonCancel.Text = "Cancel";
         this.buttonCancel.UseVisualStyleBackColor = true;
         this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
         // 
         // buttonOpen
         // 
         this.buttonOpen.Location = new System.Drawing.Point(76, 60);
         this.buttonOpen.Name = "buttonOpen";
         this.buttonOpen.Size = new System.Drawing.Size(75, 23);
         this.buttonOpen.TabIndex = 1;
         this.buttonOpen.Text = "Open";
         this.buttonOpen.UseVisualStyleBackColor = true;
         this.buttonOpen.Click += new System.EventHandler(this.buttonOpen_Click);
         // 
         // txtSequenceNumber
         // 
         this.txtSequenceNumber.Location = new System.Drawing.Point(190, 214);
         this.txtSequenceNumber.Mask = "00";
         this.txtSequenceNumber.Name = "txtSequenceNumber";
         this.txtSequenceNumber.PromptChar = ' ';
         this.txtSequenceNumber.Size = new System.Drawing.Size(42, 20);
         this.txtSequenceNumber.TabIndex = 5;
         this.txtSequenceNumber.Enter += new System.EventHandler(this.txtSequenceNumber_Enter);
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(12, 242);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(80, 13);
         this.label4.TabIndex = 21;
         this.label4.Text = "Note Comment:";
         // 
         // txtDescription
         // 
         this.txtDescription.Location = new System.Drawing.Point(92, 239);
         this.txtDescription.MaxLength = 1000;
         this.txtDescription.Multiline = true;
         this.txtDescription.Name = "txtDescription";
         this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
         this.txtDescription.Size = new System.Drawing.Size(187, 96);
         this.txtDescription.TabIndex = 6;
         // 
         // cmbVehicleNumber
         // 
         this.cmbVehicleNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.cmbVehicleNumber.FormattingEnabled = true;
         this.cmbVehicleNumber.Location = new System.Drawing.Point(92, 156);
         this.cmbVehicleNumber.Name = "cmbVehicleNumber";
         this.cmbVehicleNumber.Size = new System.Drawing.Size(121, 21);
         this.cmbVehicleNumber.TabIndex = 2;
         this.cmbVehicleNumber.SelectedIndexChanged += new System.EventHandler(this.cmbVehicleNumber_SelectedIndexChanged);
         this.cmbVehicleNumber.TextChanged += new System.EventHandler(this.cmbVehicleNumber_TextChanged);
         // 
         // picProgress
         // 
         this.picProgress.Image = ((System.Drawing.Image)(resources.GetObject("picProgress.Image")));
         this.picProgress.Location = new System.Drawing.Point(190, 135);
         this.picProgress.Name = "picProgress";
         this.picProgress.Size = new System.Drawing.Size(87, 12);
         this.picProgress.TabIndex = 22;
         this.picProgress.TabStop = false;
         this.picProgress.Visible = false;
         // 
         // cmbServiceChannel
         // 
         this.cmbServiceChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.cmbServiceChannel.FormattingEnabled = true;
         this.cmbServiceChannel.Location = new System.Drawing.Point(92, 183);
         this.cmbServiceChannel.Name = "cmbServiceChannel";
         this.cmbServiceChannel.Size = new System.Drawing.Size(185, 21);
         this.cmbServiceChannel.TabIndex = 3;
         this.cmbServiceChannel.TextChanged += new System.EventHandler(this.cmbServiceChannel_TextChanged);
         // 
         // label6
         // 
         this.label6.AutoSize = true;
         this.label6.Location = new System.Drawing.Point(12, 187);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(74, 13);
         this.label6.TabIndex = 24;
         this.label6.Text = "Svc. Channel:";
         // 
         // txtLynxID
         // 
         this.txtLynxID.Location = new System.Drawing.Point(92, 131);
         this.txtLynxID.MaxLength = 10;
         this.txtLynxID.Name = "txtLynxID";
         this.txtLynxID.Size = new System.Drawing.Size(92, 20);
         this.txtLynxID.TabIndex = 1;
         this.txtLynxID.Leave += new System.EventHandler(this.txtLynxID_Leave);
         this.txtLynxID.Enter += new System.EventHandler(this.txtLynxID_Enter);
         // 
         // timer1
         // 
         this.timer1.Interval = 500;
         this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
         // 
         // frmExport
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(291, 373);
         this.ControlBox = false;
         this.Controls.Add(this.txtLynxID);
         this.Controls.Add(this.cmbServiceChannel);
         this.Controls.Add(this.label6);
         this.Controls.Add(this.picProgress);
         this.Controls.Add(this.cmbVehicleNumber);
         this.Controls.Add(this.txtDescription);
         this.Controls.Add(this.label4);
         this.Controls.Add(this.txtSequenceNumber);
         this.Controls.Add(this.buttonOpen);
         this.Controls.Add(this.buttonCancel);
         this.Controls.Add(this.buttonDelete);
         this.Controls.Add(this.lblFileName);
         this.Controls.Add(this.cmbEstimateType);
         this.Controls.Add(this.label5);
         this.Controls.Add(this.labelHeader);
         this.Controls.Add(this.buttonSave);
         this.Controls.Add(this.label3);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.label1);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.KeyPreview = true;
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "frmExport";
         this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
         this.Text = "Export New File to APD";
         this.TopMost = true;
         this.Load += new System.EventHandler(this.frmExport_Load);
         ((System.ComponentModel.ISupportInitialize)(this.picProgress)).EndInit();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.ComboBox cmbEstimateType;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label labelHeader;
      private System.Windows.Forms.Button buttonSave;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label lblFileName;
      private System.Windows.Forms.Button buttonDelete;
      private System.Windows.Forms.Button buttonCancel;
      private System.Windows.Forms.Button buttonOpen;
      private System.Windows.Forms.MaskedTextBox txtSequenceNumber;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.TextBox txtDescription;
      private System.Windows.Forms.ComboBox cmbVehicleNumber;
      private System.Windows.Forms.PictureBox picProgress;
      private System.Windows.Forms.ComboBox cmbServiceChannel;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.TextBox txtLynxID;
      private System.Windows.Forms.Timer timer1;
   }
}