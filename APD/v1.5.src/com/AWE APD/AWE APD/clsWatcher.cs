﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace AWE_APD
{
   class clsWatcher
   {
      private string sourcePath = "";
      private string destinationPath = "";

      private FileSystemWatcher watcher = new FileSystemWatcher();
      private System.Timers.Timer tmr = new System.Timers.Timer(1000);

      private List<string> asgFiles = new List<string>();

      public clsWatcher(string strSourcePath, string strDestinationPath)
      {
         tmr.Enabled = false;
         tmr.Interval = 5000;
         tmr.Elapsed += new System.Timers.ElapsedEventHandler(tmr_Elapsed);

         if (strSourcePath != "" && Directory.Exists(strSourcePath) == true)
         {
            if (strSourcePath.EndsWith(@"\") == false)
            {
               strSourcePath += @"\";
            }
            sourcePath = strSourcePath;
         }

         if (strDestinationPath != "" && Directory.Exists(strDestinationPath) == true)
         {
            if (strDestinationPath.EndsWith(@"\") == false)
            {
               strDestinationPath += @"\";
            }
            destinationPath = strDestinationPath;
         }

         if (strSourcePath != "" && strDestinationPath != "")
         {
            CreateWatcher();
         }
      }

      void tmr_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
      {
         tmr.Enabled = false;
         string strDestinationFileName = "";
         string strSourceFileName = "";
         for (int i = asgFiles.Count - 1; i >= 0; i--)
         {
            strSourceFileName = asgFiles[i];
            FileInfo fi = new FileInfo(strSourceFileName);
            strDestinationFileName = destinationPath + fi.Name;
            File.Move(asgFiles[i], strDestinationFileName);
            asgFiles.RemoveAt(asgFiles.IndexOf(strSourceFileName));
         }
      }

      public void CreateWatcher()
      {
         watcher.Filter = "*.asg";
         watcher.Created += new FileSystemEventHandler(watcher_FileCreated);
         watcher.Path = sourcePath;
         watcher.EnableRaisingEvents = true;
      }

      void watcher_FileCreated(object sender, FileSystemEventArgs e)
      {
         if (asgFiles.IndexOf(e.Name) == -1)
         {
            asgFiles.Add(e.Name);
            tmr.Enabled = true;
         }
      }

      
   }
}
