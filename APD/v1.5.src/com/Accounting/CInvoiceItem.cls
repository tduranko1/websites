VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CInvoiceItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : CInvoiceItem
' DateTime  : 3/21/2005 14:25
' Author    : Dan Price
' Purpose   : This object represents the aggregate of APD Invoice records that comprise
'             a Dispatch in Ingres, as well as a line item on an electronic invoice, both
'             of which include the Handling Fee along with the totaled indemnity payments
'             to the last assigned shop.  For a paper invoice, the Handling Fee will be
'             broken out to constitute a separate line item.  This object maintains all
'             properties required by Ingres or one of the invoicing methods.  This object
'             maintains a collection of InvoiceIDs that represent the APD Invoice records
'             that constitute the InvoiceItem.  These IDs are used to sync the APD database
'             to Ingres with the Dispatch number returned from Ingres.  Lastly this object
'             provides functionality to return an XML node representation of itself tailored
'             to the specific invoicing methods, currently electronic or paper.  This XML
'             is used to alter the source XML document before firing off one of the supported
'             invoicing methods.
'---------------------------------------------------------------------------------------

Option Explicit

Private Const MODULE_NAME = "CInvoiceItem."

Private mcolInvoiceIDs As Collection

Private mintInvoiceItemID As Integer
Private mlngClaimAspectID As Long
Private mlngInvoiceID As Long
Private mstrAuthorizingClientUserID As String
Private mstrAmount As String
Private mstrClientFeeCode As String
Private mstrDispatchNumber As String
Private mstrDeductible As String
Private mstrInitialPayment As String
Private mstrInvoiceDate As String
Private mstrInvoiceDescription As String
Private mstrItemType As String
Private mstrPayeeID As String
Private mstrPayeeName As String
Private mstrPayeeCity As String
Private mstrPayeeState As String
Private mstrPayeeType As String
Private mstrPayment As String
Private mstrFee As String
Private mstrAdminFee As String
Private mstrTaxTotalAmt As String
Private mstrLynxID As String    ' LynxID-AspectNumber
Private mstrCarrierClaimNumber As String
Private mstrComment As String    ' Vehicle Desc + VIN
Private mstrCarrierRepNameFirst As String
Private mstrCarrierRepnameLast As String
Private mstrInsuredNameFirst As String
Private mstrInsuredNameLast As String
Private mstrInsuredBusinessName As String
Private mstrClaimantNameFirst As String
Private mstrClaimantNameLast As String
Private mstrClaimantBusinessName As String
Private mstrServiceChannel As String
Private mblnFirstIndemnityAssignment As Boolean


Public Enum AmountType
  ePayment
  eFee
End Enum

' Properties
Friend Property Get InvoiceItemID() As Integer
10      InvoiceItemID = mintInvoiceItemID
End Property

Friend Property Let InvoiceItemID(intInvoiceItemID As Integer)
10      mintInvoiceItemID = intInvoiceItemID
End Property


Friend Property Get InvoiceIDs() As Collection
10      Set InvoiceIDs = mcolInvoiceIDs
End Property

Friend Property Get Claimant() As String
10      If Len(mstrClaimantBusinessName) > 0 Then
20        Claimant = mstrClaimantBusinessName
30      Else
40        Claimant = Trim(mstrClaimantNameFirst) & " " & Trim(mstrClaimantNameLast)
50      End If
End Property

Friend Property Get AuthorizingClientUserID() As String
10      AuthorizingClientUserID = mstrAuthorizingClientUserID
End Property

Friend Property Let AuthorizingClientUserID(strAuthorizingClientUserID As String)
10      mstrAuthorizingClientUserID = strAuthorizingClientUserID
End Property

Friend Property Get Amount() As String
10      Amount = mstrAmount
End Property

Friend Property Let Amount(strAmount As String)
10      mstrAmount = strAmount
End Property

Friend Property Get ClientFeeCode() As String
10      ClientFeeCode = mstrClientFeeCode
End Property

Friend Property Let ClientFeeCode(strClientFeeCode As String)
10      mstrClientFeeCode = strClientFeeCode
End Property

Friend Property Get InvoiceDate() As String
10      InvoiceDate = mstrInvoiceDate
End Property

Friend Property Let InvoiceDate(strInvoiceDate As String)
10      mstrInvoiceDate = strInvoiceDate
End Property

Friend Property Get InvoiceDescription() As String
10      If mstrItemType = "Fee" Or Len(mstrPayeeName) = 0 Then
20        InvoiceDescription = mstrInvoiceDescription
30      Else
40        InvoiceDescription = mstrPayeeName & " - " & mstrInvoiceDescription & " Claim"
50      End If
End Property

Friend Property Let InvoiceDescription(strInvoiceDescription As String)
10      mstrInvoiceDescription = strInvoiceDescription
End Property

Friend Property Get LynxID() As String
10      LynxID = mstrLynxID
End Property

Friend Property Let LynxID(strLynxID As String)
10      mstrLynxID = strLynxID
End Property

Friend Property Get PayeeID() As String
10      PayeeID = mstrPayeeID
End Property

Friend Property Let PayeeID(strPayeeID As String)
10      mstrPayeeID = strPayeeID
End Property

Friend Property Get Payment() As String
10      Payment = mstrPayment
End Property

Friend Property Let Payment(strPayment As String)
10      mstrPayment = strPayment
End Property

Friend Property Get ClaimNumber() As String
10      ClaimNumber = mstrCarrierClaimNumber
End Property

Friend Property Let ClaimNumber(strClaimNumber As String)
10      mstrCarrierClaimNumber = strClaimNumber
End Property

Friend Property Get Comment() As String
10      Comment = mstrComment
End Property

Friend Property Let Comment(strComment As String)
10      mstrComment = strComment
End Property

Friend Property Get PayeeName() As String
10      PayeeName = mstrPayeeName
End Property

Friend Property Let PayeeName(strPayeeName As String)
10      mstrPayeeName = strPayeeName
End Property

Friend Property Get PayeeCity() As String
10      PayeeCity = mstrPayeeCity
End Property

Friend Property Let PayeeCity(strPayeeCity As String)
10      mstrPayeeCity = strPayeeCity
End Property

Friend Property Get PayeeState() As String
10      PayeeState = mstrPayeeState
End Property

Friend Property Let PayeeType(strPayeeType As String)
10      mstrPayeeType = strPayeeType
End Property

Friend Property Get PayeeType() As String
10      PayeeType = mstrPayeeType
End Property

Friend Property Let PayeeState(strPayeeState As String)
10      mstrPayeeState = strPayeeState
End Property

Friend Property Let Fee(strFee As String)
10      mstrFee = strFee
End Property

Friend Property Get Fee() As String
10      Fee = mstrFee
End Property

Friend Property Let adminFee(strFee As String)
10      mstrAdminFee = strFee
End Property

Friend Property Get adminFee() As String
10      adminFee = mstrAdminFee
End Property

Friend Property Get DeductibleAmt() As String
10      DeductibleAmt = mstrDeductible
End Property

Friend Property Let TaxTotalAmt(strTaxTotalAmt As String)
10      mstrTaxTotalAmt = strTaxTotalAmt
End Property

Friend Property Get TaxTotalAmt() As String
10      TaxTotalAmt = mstrTaxTotalAmt
End Property

Friend Property Get InsuredNameFirst() As String
10      InsuredNameFirst = mstrInsuredNameFirst
End Property

Friend Property Let InsuredNameFirst(strInsuredNameFirst As String)
10      mstrInsuredNameFirst = strInsuredNameFirst
End Property

Friend Property Get InsuredNameLast() As String
10      InsuredNameLast = mstrInsuredNameLast
End Property

Friend Property Let InsuredNameLast(strInsuredNameLast As String)
10      mstrInsuredNameLast = strInsuredNameLast
End Property

Friend Property Get InsuredBusinessName() As String
10      InsuredBusinessName = mstrInsuredBusinessName
End Property

Friend Property Let InsuredBusinessName(strInsuredBusinessName As String)
10      mstrInsuredBusinessName = strInsuredBusinessName
End Property

Friend Property Get CarrierRepNameFirst() As String
10      CarrierRepNameFirst = mstrCarrierRepNameFirst
End Property

Friend Property Let CarrierRepNameFirst(strCarrierRepNameFirst As String)
10      mstrCarrierRepNameFirst = strCarrierRepNameFirst
End Property

Friend Property Get CarrierRepNameLast() As String
10      CarrierRepNameLast = mstrCarrierRepnameLast
End Property

Friend Property Let CarrierRepNameLast(strCarrierRepNameLast As String)
10      mstrCarrierRepnameLast = strCarrierRepNameLast
End Property

Friend Property Get DispatchNumber() As String
10      DispatchNumber = mstrDispatchNumber
End Property

Friend Property Let DispatchNumber(strDispatchNumber As String)
10      mstrDispatchNumber = strDispatchNumber
End Property

' This property is useful in cases such as an InvoiceItem that represents a Handling Fee. In this case there should only ever be one
' InvoiceID in the collection.
Friend Property Get FirstInvoiceID() As Long
10      FirstInvoiceID = mcolInvoiceIDs.item(1)
End Property

Friend Property Let AddInvoiceID(lngInvoiceID As Long)
10      mcolInvoiceIDs.Add lngInvoiceID
End Property

Friend Property Get ClaimAspectID() As Long
10      ClaimAspectID = mlngClaimAspectID
End Property

Friend Property Let ClaimAspectID(lngClaimAspectID As Long)
10      mlngClaimAspectID = lngClaimAspectID
End Property


Friend Property Get HandlingFeeGoesHere() As Boolean
10      HandlingFeeGoesHere = mblnFirstIndemnityAssignment
End Property


Private Sub Class_Initialize()
10      Set mcolInvoiceIDs = New Collection
End Sub

Private Sub Class_Terminate()
10      Set mcolInvoiceIDs = Nothing
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Initialize
' DateTime  : 3/8/2005 21:18
' Author    : Dan Price
' Purpose   : Store the initial property values for the InvoiceItem.
'---------------------------------------------------------------------------------------
'
Friend Sub Initialize(intInvoiceItemID As Integer, lngClaimAspectID As Long, strItemType As String, strLynxID As String, strCarrierClaimNumber As String, _
                      strCarrierRepNameFirst As String, strCarrierRepNameLast As String, strComment As String, strDeductible As String, _
                      strInsuredNameFirst As String, strInsuredNameLast As String, strInsuredBusinessName As String, _
                      strClaimantNameFirst As String, strClaimantNameLast As String, strClaimantBusinessName As String, _
                      strAuthorizingClientUserID As String, strAmount As String, strAdminAmount As String, strPayeeID As String, strPayeeName As String, _
                      strPayeeCity As String, strPayeeState As String, strDispatchNumber As String, strInvoiceDescription As String, _
                      lngInvoiceID As Long, strClientFeeCode As String, strInvoiceDate As String, strPayeeType As String, strServiceChannel As String, _
                      strInitialPayment As String, strTaxTotalAmt As String, Optional blnLatestIndemnityAssignment As Boolean = False)
                        
  
  Const PROC_NAME = "Initialize"
  
  On Error GoTo ErrorHandler
      
  ' Populate property values from parameters.
  mintInvoiceItemID = intInvoiceItemID
  mlngClaimAspectID = lngClaimAspectID
  mlngInvoiceID = lngInvoiceID
  mstrAuthorizingClientUserID = strAuthorizingClientUserID
  mstrAmount = strAmount
  mstrDeductible = strDeductible
  mstrDispatchNumber = strDispatchNumber
  mstrLynxID = strLynxID
  mstrCarrierClaimNumber = strCarrierClaimNumber
  mstrComment = strComment
  mstrInsuredNameFirst = strInsuredNameFirst
  mstrInsuredNameLast = strInsuredNameLast
  mstrInsuredBusinessName = strInsuredBusinessName
  mstrClaimantNameLast = strClaimantNameLast
  mstrClaimantNameFirst = strClaimantNameFirst
  mstrClaimantBusinessName = strClaimantBusinessName
  mstrCarrierRepNameFirst = strCarrierRepNameFirst
  mstrCarrierRepnameLast = strCarrierRepNameLast
  mstrInvoiceDescription = strInvoiceDescription
  mstrClientFeeCode = strClientFeeCode
  mblnFirstIndemnityAssignment = blnLatestIndemnityAssignment
  mstrPayeeID = strPayeeID
  mstrInvoiceDate = strInvoiceDate
  mstrItemType = strItemType
  mstrPayeeType = strPayeeType
  mstrServiceChannel = strServiceChannel
  mstrTaxTotalAmt = strTaxTotalAmt
  
  ' Deductible is applied on initial payment only.  This is a fix for WAG but should apply to all other clients as well.
  mstrInitialPayment = strInitialPayment
      
  If strItemType = "Fee" Then
    mstrPayment = "0.00"
    mstrFee = strAmount
    mstrAdminFee = strAdminAmount
  Else
    mstrFee = "0.00"
    mstrAdminFee = "0.00"
    mstrPayment = strAmount
    mstrPayeeName = strPayeeName
    mstrPayeeCity = strPayeeCity
    mstrPayeeState = strPayeeState
  End If
    
  mcolInvoiceIDs.Add lngInvoiceID
  
  
ErrorHandler:
  
  If Err.Number <> 0 Then
    g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & " " & Err.Source, Err.Description
  End If

End Sub


'---------------------------------------------------------------------------------------
' Procedure : Add
' DateTime  : 3/18/2005 13:12
' Author    : Dan Price
' Purpose   : Add the passed amount to this InvoiceItem and add the InvoiceID to the
'             collection.
'---------------------------------------------------------------------------------------
'
Friend Function Add(strAmount As String, strAdminAmount As String, strDeductibleAmt As String, strTaxTotalAmt As String, sType As AmountType, lngInvoiceID As Long, strInitialPayment As String) As Boolean

  ' Initialize return value.
  Add = False
  
  ' Set the Fee or Payment amount.
  If sType = AmountType.eFee Then
    mstrFee = strAmount
    mstrAdminFee = strAdminAmount
    mstrDeductible = strDeductibleAmt
    mstrTaxTotalAmt = strTaxTotalAmt
  Else
    mstrPayment = CStr(CDbl(mstrPayment) + CDbl(strAmount))
    
    ' Format the Payment amount for display.
    Select Case InStr(mstrPayment, ".")
      Case 0
        mstrPayment = mstrPayment & ".00"
      Case Len(mstrPayment) - 1
        mstrPayment = mstrPayment & "0"
      Case Else
            
    End Select
        
  End If
  
  ' Only change this value if the parameter is "true".  Any "true" on any item should result in a "true".  This is because only one payment
  ' will be marked as 'initial' for each claim aspect.  However, multiple payments may be accumulated into one invoice line item.  If any one of them
  ' is marked as 'inital', the deductible applies.  Only if no payment that comprises a line item was the 'initial' is the deductible not applied.
  If strInitialPayment = "true" Then
      mstrInitialPayment = True
  End If
  
  ' Redundant value used by some functions.
  mstrAmount = mstrPayment
     
  ' Add the passed invoice id to the collection.
  mcolInvoiceIDs.Add lngInvoiceID
  
  ' Set the return to success.
  Add = True
  
End Function


'---------------------------------------------------------------------------------------
' Procedure : GetBillingFileXML
' DateTime  : 3/18/2005 13:13
' Author    : Dan Price
' Purpose   : Construct an XML node from this InvoiceItem's property values appopriate for
'             use in the XML source that will be sent to GLAXIS for electronic billng.
'---------------------------------------------------------------------------------------
'
Friend Function GetBillingFileXML() As String
  
  Dim strXML As String
  Dim oXML As New MSXML2.DOMDocument
  Dim oInvNode As IXMLDOMElement
  
  oXML.async = False
  oXML.LoadXml "<Invoice/>"

  strXML = ""
  
  Set oInvNode = oXML.SelectSingleNode("/Invoice")
  If Not (oInvNode Is Nothing) Then
    oInvNode.setAttribute "AuthorizingClientUserID", mstrAuthorizingClientUserID
    oInvNode.setAttribute "AmountFee", mstrFee
    oInvNode.setAttribute "AdminAmountFee", mstrAdminFee
    oInvNode.setAttribute "AmountPayment", mstrPayment
    oInvNode.setAttribute "ClientFeeCode", mstrClientFeeCode
    oInvNode.setAttribute "Deductible", mstrDeductible
    oInvNode.setAttribute "TaxTotalAmt", mstrTaxTotalAmt
    oInvNode.setAttribute "InitialPayment", mstrInitialPayment
    oInvNode.setAttribute "DispatchNumber", mstrDispatchNumber
    oInvNode.setAttribute "InvoiceDate", CStr(Date)
    oInvNode.setAttribute "InvoiceDescription", Me.InvoiceDescription
    oInvNode.setAttribute "PayeeName", mstrPayeeName
    oInvNode.setAttribute "PayeeCity", mstrPayeeCity
    oInvNode.setAttribute "PayeeState", mstrPayeeState
    oInvNode.setAttribute "ServiceChannel", mstrServiceChannel
    
    strXML = oXML.XML
  End If
  
  Set oXML = Nothing
  Set oInvNode = Nothing
  GetBillingFileXML = strXML
End Function


'---------------------------------------------------------------------------------------
' Procedure : GetInvoiceDocumentXML
' DateTime  : 3/18/2005 13:14
' Author    : Dan Price
' Purpose   : Construct an XML node from this InvoiceItem's property values appopriate for
'             creating a paper Invoice document.
'---------------------------------------------------------------------------------------
'
Friend Function GetInvoiceDocumentXML() As String
        
        Dim strXML As String
        Dim oXML As New MSXML2.DOMDocument
        Dim oInvNode As IXMLDOMElement
        
10      oXML.async = False
20      oXML.LoadXml "<Invoice/>"
30      strXML = ""
        
40      Set oInvNode = oXML.SelectSingleNode("/Invoice")
50      If Not (oInvNode Is Nothing) Then
60        oInvNode.setAttribute "AuthorizingClientUserID", mstrAuthorizingClientUserID
70        oInvNode.setAttribute "Amount", mstrAmount
80        oInvNode.setAttribute "ClientFeeCode", mstrClientFeeCode
90        oInvNode.setAttribute "DispatchNumber", mstrDispatchNumber
100       oInvNode.setAttribute "InvoiceDate", CStr(Date)
110       oInvNode.setAttribute "InvoiceDescription", Me.InvoiceDescription
120       oInvNode.setAttribute "PayeeName", mstrPayeeName
130       oInvNode.setAttribute "PayeeCity", mstrPayeeCity
140       oInvNode.setAttribute "PayeeState", mstrPayeeState
          
150       strXML = oXML.XML
160     End If
        
170     Set oXML = Nothing
180     Set oInvNode = Nothing
        
190     GetInvoiceDocumentXML = strXML
End Function


