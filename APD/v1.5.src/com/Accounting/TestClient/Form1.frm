VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   1980
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4605
   LinkTopic       =   "Form1"
   ScaleHeight     =   1980
   ScaleWidth      =   4605
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "Billing"
      Height          =   735
      Left            =   240
      TabIndex        =   1
      Top             =   1080
      Width           =   4095
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Payment"
      Height          =   735
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   4095
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()

    Dim obj As Object
    Dim str As String
    
    On Error Resume Next
    
    Set obj = CreateObject("LAPDAccounting.CPayment")
    
    str = obj.MakePayment( _
        3923, 241, _
        "0", "3929", _
        "Description", _
        "InsuredFirst", "InsuredLast", _
        "Insured Business Name", _
        "1998", "Ford", "Mustang V8 Cobra", "Convertible", _
        "PayeeAddr1", "PayeeAddr2", _
        "Payee City", "FL", _
        "33907", "Payee Shop of Horrors", _
        "S", _
        "1500.25", "80.45", _
        "I", 0, _
        6, "John Carpenter", _
        148656, "383-23423-22", _
        145, 632, False, "Carrier Rep")

    Set obj = Nothing

    If Err.Number <> 0 Then
        MsgBox Err.Description
    Else
        MsgBox "Success: " & str
    End If
    
End Sub

Private Sub Command2_Click()
    Dim obj As Object
    Dim str As String
    
    On Error Resume Next
    
    Set obj = CreateObject("LAPDAccounting.CBilling")
    
    str = obj.BillAndCreateInvoice("134531", 145, 148656, 6)

    Set obj = Nothing

    If Err.Number <> 0 Then
        MsgBox Err.Description
    Else
        MsgBox "Success: " & str
    End If

End Sub
