Attribute VB_Name = "MLAPDAccounting"
'********************************************************************************
'* Component LAPDAccounting : Module MLAPDAccounting
'*
'* Global Consts and Utility Methods
'*
'********************************************************************************
Option Explicit

Public Const APP_NAME As String = "LAPDAccounting."

Private Const MODULE_NAME As String = APP_NAME & "MLAPDAccounting."

Public Const LAPDAccounting_FirstError As Long = &H80066800

'Error codes specific to LAPDAccounting
Public Enum LAPDAccounting_ErrorCodes

    'General error codes applicable to all modules of this component
    eCreateObjectExError = LAPDAccounting_FirstError
    eIngresReturnedUnknownError = LAPDAccounting_FirstError + &H50
    eIngresRequiresVehicleDesc = LAPDAccounting_FirstError + &H60
    eIngresRequiresIngresAccountingID = LAPDAccounting_FirstError + &H70
        
    'Module specific error ranges.
    CBilling_CLS_ErrorCodes = LAPDAccounting_FirstError + &H100
    eHandlingFeeError = LAPDAccounting_FirstError + &H110
    
    MRecordsetUtils_BAS_ErrorCodes = LAPDAccounting_FirstError + &H180
    

End Enum

Public Enum EDatabase
    eAPD_Recordset
    eAPD_XML
    eIngres_Recordset
End Enum

'********************************************************************************
'* Global events and data access objects.
'********************************************************************************
Public g_objEvents As Object
Public g_strSupportDocPath As String
Public g_blnDebugMode As Boolean

'I used multiple DataAccessor components here because we have two separate
'databases to hit, one with two different providers.  All of this has to be
'wrapped in the same transaction.  You cannot switch provider or database
'in the middle of a transaction.  Ugly, ugly.
Public g_objApdRsAccessor As Object
Public g_objApdXmlAccessor As Object
'Public g_objIngresAccessor As Object

Public g_strIngresConnect As String

'API declares
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

'********************************************************************************
'* Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
'* Parameters:  strObjectName = The object to call CreateObject for.
'* Purpose:     Wraps CreateObject() with better debug information.
'* Returns:     The Object created or Nothing.
'********************************************************************************
Public Function CreateObjectEx(strObjectName) As Object
          Dim obj As Object
          
10        On Error GoTo ErrorHandler
          
20        Set CreateObjectEx = Nothing
          
30        Set obj = CreateObject(strObjectName)
          
40        If obj Is Nothing Then
50            Err.Raise eCreateObjectExError, "", "CreateObject('" & strObjectName & "') returned Nothing."
60        End If
          
70        Set CreateObjectEx = obj
          
ErrorHandler:

80        Set obj = Nothing
          
90        If Err.Number <> 0 Then
100           Err.Raise Err.Number, "CreateObjectEx('" & strObjectName & "') " & Err.Source, Err.Description & " " & strObjectName
110       End If
          
End Function

'********************************************************************************
'* Initializes global objects and variables
'********************************************************************************
Public Sub InitializeGlobals()

          'Create DataAccessor.
10        Set g_objApdRsAccessor = CreateObjectEx("DataAccessor.CDataAccessor")
20        Set g_objApdXmlAccessor = CreateObjectEx("DataAccessor.CDataAccessor")
'30        Set g_objIngresAccessor = CreateObjectEx("DataAccessor.CDataAccessor")
          
          'Share DataAccessor's events object.
30        Set g_objEvents = g_objApdRsAccessor.mEvents

          'Get path to support document directory
40        g_strSupportDocPath = App.Path & "\" & Left(APP_NAME, Len(APP_NAME) - 1)

          'This will give LAPDAccounting its own log file.
50        g_objEvents.ComponentInstance = "Accounting"
60        g_objApdXmlAccessor.mEvents.ComponentInstance = "Accounting"
'80        g_objIngresAccessor.mEvents.ComponentInstance = "Accounting"

          'Initialize our member components first, so we have logging set up.
70        g_objApdRsAccessor.InitEvents App.Path & "\..\config\config.xml", "Debug", True
80        g_objApdXmlAccessor.InitEvents App.Path & "\..\config\config.xml", "Debug", False
'110       g_objIngresAccessor.InitEvents App.Path & "\..\config\config.xml", "Debug", False
          
90        g_strIngresConnect = GetConfig("Accounting/ConnectionString")
100       g_objEvents.Assert Len(g_strIngresConnect) > 0, "Blank connection string returned from config file: " & g_strIngresConnect
              
          
          'Get config debug mode status.
110       g_blnDebugMode = g_objEvents.IsDebugMode

End Sub

'********************************************************************************
'* Terminates global objects and variables
'********************************************************************************
Public Sub TerminateGlobals()
10        Set g_objApdRsAccessor = Nothing
20        Set g_objEvents = Nothing
'30        Set g_objIngresAccessor = Nothing
30        Set g_objApdXmlAccessor = Nothing
End Sub

'********************************************************************************
'* Syntax:      strSqlDateTime = ConstructSqlDateTime("12032003","013425")
'* Parameters:  strXmlDate = Date in format "mmddyyyy"
'*              strXmlTime = Time in format "hhmmss"
'*              blnNowAsDefault = If true, will use Now if no values passed.
'* Purpose:     Builds a specifically formatted time string, according to our
'*              SQL specs, as "yyyy-mm-ddThh:mm:ss"
'* Returns:     The date-time string.
'********************************************************************************
Public Function ConstructSqlDateTime( _
    Optional ByVal strXmlDate As String = "", _
    Optional ByVal strXmlTime As String = "", _
    Optional ByVal blnNowAsDefault As Boolean = True) As String
              
          Dim strDateTime As String
          Dim strTemp As String
          
          'Should be in format 'mmddyyyy' as per spec.
10        If Len(strXmlDate) = 8 Then
20            strDateTime = Right(strXmlDate, 4) & "-" _
                  & Left(strXmlDate, 2) & "-" _
                  & Mid(strXmlDate, 3, 2)
30        ElseIf blnNowAsDefault Then
40            strDateTime = Format(Now, "yyyy-mm-dd")
50        End If
              
          'Dont bother with time if we dont have date.
60        If Len(strDateTime) > 0 Then
          
              'Should be in format 'hhmmss' as per spec.
70            If Len(strXmlTime) = 6 Then
80                strDateTime = strDateTime & "T" _
                      & Left(strXmlTime, 2) & ":" _
                      & Mid(strXmlTime, 3, 2) & ":" _
                      & Right(strXmlTime, 2)
              'If date was passed but no time then use default of 0 time.
90            ElseIf Len(strXmlDate) > 0 Then
100               strDateTime = strDateTime & "T00:00:00"
              'If no date or time were passed then use now if allowed.
110           ElseIf blnNowAsDefault Then
120               strDateTime = strDateTime & Format(Now, "Thh:mm:ss")
130           End If
              
140       End If
              
          ConstructSqlDateTime = strDateTime

End Function

'********************************************************************************
'* Returns the requested configuration setting.
'********************************************************************************
Public Function GetConfig(ByVal strSetting As String)
10        GetConfig = g_objEvents.mSettings.GetParsedSetting(strSetting)
End Function

'********************************************************************************
'* Points the global data accessor object at the proper database.
'********************************************************************************
Public Sub ConnectToDB(ByVal eDB As EDatabase)
          Dim strConnect As String
          Dim strConfig As String
          
          'Get configuration settings for FNOL database connect string.
10        Select Case eDB
              Case eIngres_Recordset
20                strConfig = "Accounting/ConnectionString"
30            Case eAPD_Recordset
40                strConfig = "ConnectionStringStd"
50            Case eAPD_XML
60                strConfig = "ConnectionStringXml"
70        End Select
          
80        strConnect = GetConfig(strConfig)
90        g_objEvents.Assert Len(strConnect) > 0, "Blank connection string returned from config file: " & strConfig

          'Set the connect string.
100       Select Case eDB
              Case eAPD_Recordset
110               g_objApdRsAccessor.SetConnectString strConnect
120           Case eAPD_XML
130               g_objApdXmlAccessor.SetConnectString strConnect
140       End Select

End Sub

'********************************************************************************
'* Construct for the invoice the actual filename as it will be stored on disk.
'********************************************************************************
Public Function BuildFileName(ByVal lngLynxID As Long) As String
          Dim x As Integer
          Dim strImageFolder As String
          Dim strLynxID As String
          Dim strDateTime As String

          'Use the current date time to make the file name unique.
10        strDateTime = Format(Now, "yyyymmddhhmmss")

          'Get the last 4 digits of the lynx id for the file folder hierarchy.
20        strLynxID = Right(CStr(lngLynxID), 4)
          
30        For x = 1 To 4
40            strImageFolder = strImageFolder & "\" & Mid(strLynxID, x, 1)
50        Next
60        strImageFolder = strImageFolder & "\"

          'Put it all together now...
70        BuildFileName = strImageFolder & "INV" & strDateTime & "LYNXID" & CStr(lngLynxID) & "Doc.doc"

End Function

'********************************************************************************
'* Builds the year/make/model/body style string
'********************************************************************************
Public Function BuildVehicleString( _
    ByVal strYear As String, _
    ByVal strMake As String, _
    ByVal strModel As String, _
    ByVal strBody As String) As String

          Dim strTemp As String
          
10        strTemp = strYear
20        If strMake <> "" Then strTemp = strTemp & " " & strMake
30        If strModel <> "" Then strTemp = strTemp & " " & strModel
40        If strBody <> "" Then strTemp = strTemp & " " & strBody
          
50        BuildVehicleString = UCase(strTemp)

End Function

'********************************************************************************
'* Syntax:      DoSleep( 500 )
'* Parameters:  lngMS - milliseconds to sleep.
'* Purpose:     Wraps Sleep() with calls to VB DoEvents().
'* Returns:     Nothing.
'********************************************************************************
Public Sub DoSleep(ByVal lngMS As Long)
          Dim lCount As Long
10        For lCount = 1 To lngMS \ 10
20            Sleep 10
30            DoEvents
40        Next
End Sub

