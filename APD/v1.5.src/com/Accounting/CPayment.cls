VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPayment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Component LAPD: Class CPayment
'*
'* This component handles calls from the APD payment page to stimulate
'* creation of payment records in the Ingres DB for estimates.
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CPayment."

'Internal error codes for this module.
Private Enum EventCodes
    eNull = CPayment_CLS_ErrorCodes
End Enum

'********************************************************************************
'* Backup global termination
'********************************************************************************
Private Sub Class_Terminate()
10        TerminateGlobals
End Sub

'********************************************************************************
'* Make Payment
'*
'* This function attempts to:
'*
'* 1) Insert/Update the Ingres DB with the payment info.
'* 2) Insert/Update the APD DB with the payment info.
'* 3) Send Email to Accounting for CEI Shops
'*
'* All of the above is as transactional as it can be.
'********************************************************************************
Public Function MakePayment( _
    ByVal lngClaimAspectID As Long, ByVal lngPayeeID As Long, _
    ByVal strCurrentInvoiceNbr As String, ByVal strOriginalInvoiceNbr As String, _
    ByVal strDescription As String, _
    ByVal strInsuredFirstName As String, ByVal strInsuredLastName As String, _
    ByVal strInsuredBusinessName As String, ByVal strVehicleYear As String, _
    ByVal strVehicleMake As String, ByVal strVehicleModel As String, _
    ByVal strVehicleBody As String, _
    ByVal strPayeeAddress1 As String, ByVal strPayeeAddress2 As String, _
    ByVal strPayeeCity As String, ByVal strPayeeState As String, _
    ByVal strPayeeZip As String, ByVal strPayeeName As String, _
    ByVal strPayeeTypeCD As String, _
    ByVal strClaimAmount As String, ByVal strFeeAmount As String, _
    ByVal strPaymentTypeCD As String, ByVal intRentalDays As Integer, _
    ByVal intUserID As Integer, ByVal strClaimRepName As String, _
    ByVal lngLynxID As Long, ByVal strCarrierClaimNumber As String, _
    ByVal lngInsuranceCompanyID As Long, ByVal lngIngresAccountingID As Long, _
    ByVal blnIsCeiShop As Boolean, ByVal strCarrierRepName _
) As String

          Const PROC_NAME As String = MODULE_NAME & "MakePayment: "

          Dim blnIngresEnabled As Boolean
          Dim strComment As String
          Dim strVehicleDesc As String
          Dim strMessageInd As String
          
10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Trace out everything... yeach...
30        If g_blnDebugMode Then g_objEvents.Trace _
              "lngClaimAspectID = " & lngClaimAspectID & vbCrLf & _
              "  Lynx ID = " & lngLynxID & " Carrier Claim Number = " & strCarrierClaimNumber & vbCrLf & _
              "  Ins. Co. ID = " & lngInsuranceCompanyID & vbCrLf & " Ingres Accounting ID = " & lngIngresAccountingID & _
              "  Ingres Accounting ID = " & lngIngresAccountingID & " CEI = " & blnIsCeiShop & vbCrLf & _
              "  Payee ID = " & lngPayeeID & vbCrLf & _
              "  CurrentInvoiceNbr = " & strCurrentInvoiceNbr & vbCrLf & _
              "  OriginalInvoiceNbr = " & strOriginalInvoiceNbr & vbCrLf & _
              "  Description = " & strDescription & vbCrLf & _
              "  Insured Name = " & strInsuredFirstName & " " & strInsuredLastName & vbCrLf & _
              "  Insured Business Name = " & strInsuredBusinessName & vbCrLf & _
              "  Vehicle = " & strVehicleYear & " " & strVehicleMake & " " & strVehicleModel & " " & strVehicleBody & vbCrLf & _
              "  Payee Name = " & strPayeeName & vbCrLf & _
              "  Payee Address 1 = " & strPayeeAddress1 & vbCrLf & _
              "  Payee Address 2 = " & strPayeeAddress2 & vbCrLf & _
              "  Payee City/st/zip = " & strPayeeCity & ", " & strPayeeState & "  " & strPayeeZip & vbCrLf & _
              "  Payee Type CD = " & strPayeeTypeCD & vbCrLf & _
              "  CLAIM AMOUNT = " & strClaimAmount & "  FEE AMOUNT = " & strFeeAmount & vbCrLf & _
              "  Payment Type CD = " & strPaymentTypeCD & "  Rental Days = " & intRentalDays & vbCrLf & _
              "  User ID = " & intUserID & " Claim Rep Name = " & strClaimRepName & " Carrier Rep Name = " & strCarrierRepName & vbCrLf _
              , PROC_NAME & "Started"
          
          'Check passed parameters.
40        g_objEvents.Assert lngClaimAspectID > 0, "Nothing passed for Claim Aspect ID."
50        g_objEvents.Assert Len(strPaymentTypeCD) > 0, "Nothing passed for Payment Type Code."
60        g_objEvents.Assert intUserID > 0, "Nothing passed for User ID."
70        g_objEvents.Assert Len(strClaimRepName) > 0, "Nothing passed for Claim Rep Name."
80        g_objEvents.Assert lngLynxID > 0, "Nothing passed for Lynx ID."
90        g_objEvents.Assert Len(strPayeeName) > 0, "Nothing passed for Payee Name."
100       g_objEvents.Assert lngIngresAccountingID > 0, "Nothing passed for Ingres Accounting ID."
110       g_objEvents.Assert Len(strCarrierRepName) > 0, "Nothing passed for Carrier Rep Name."
          
          'Are the Ingres DB calls enabled?
120       blnIngresEnabled = IIf(GetConfig("Accounting/IngresEnabled") = "True", True, False)
          
          'If no payment company ID was passed then use the default from the config file.
          'This is a hack to save major changes to the DB in the near term.
          'This hack has been tur-muh-na-ted as the major changes to the DB have been implemented.
          'If lngPaymentCompanyID = 0 Then
          '    lngPaymentCompanyID = CLng(GetConfig("Accounting/DefaultPaymentCompanyID"))
          'End If
          
          ' If we have a business name for the insured, use it versus first/last
130       If Len(strInsuredBusinessName) > 0 Then
140           strInsuredLastName = strInsuredBusinessName
150           strInsuredFirstName = ""
160       End If
          
170       If Len(strCurrentInvoiceNbr) = 0 Then
180           strCurrentInvoiceNbr = "0"
190       End If
          
          '* The comment we insert into Ingres is the vehicle description with the
          '* original dispatch number for this exposure(if there is one)
200       strVehicleDesc = BuildVehicleString(strVehicleYear, strVehicleMake, strVehicleModel, strVehicleBody)
210       If strVehicleDesc = "" Then
220           strVehicleDesc = "No vehicle description available"
230       End If
240       strComment = strVehicleDesc
              
250       If Len(strOriginalInvoiceNbr) > 0 Then
260           strComment = strComment & " : Original dispatch #" & strOriginalInvoiceNbr
270       End If
          
280       If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Comment = |" & strComment & "|"
          
          '*
          '* Insert/Update the Ingres DB with the payment info.
          '*
          
290       If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Insert/Update Ingres Database"

          'For CEI shops, use the shop ID from the config file (999998)
300       If blnIsCeiShop Then
310           lngPayeeID = CLng(GetConfig("Accounting/Payment/CEI/@ShopID"))
320       End If
          
          Dim strReturnValue As String
          Dim strDispatchNumber As String
          Dim strMessageText As String
          
          'This boolean is used to disable ingres calls for non-production environments.
330       If blnIngresEnabled Then
          
              'Enforce presence of a vehicle description.
              'Ingres will thrown an unintelligible error message otherwise.
340           If strVehicleDesc = "" Then
350               Err.Raise eIngresRequiresVehicleDesc, "Call to Ingres"
360           End If

370           ConnectToDB eIngres_Recordset
          
380           With g_objIngresAccessor
390               .BeginTransaction
400               .FpCreateCommand GetConfig("Accounting/IngressBillingSP")
              
410               .FpAddParam "retval", adInteger, adParamReturnValue, 4, 0
420               .FpAddParam "h_claim_amt", adDouble, adParamInput, -1, strClaimAmount
430               .FpAddParam "h_claim_no", adChar, adParamInput, 20, Left(strCarrierClaimNumber, 20)
440               .FpAddParam "h_comment", adChar, adParamInput, 80, Left(strComment, 80)
450               .FpAddParam "h_comment_ind", adChar, adParamInput, 1, ""
460               .FpAddParam "h_create_dt", adDBTimeStamp, adParamInput, 0, Format(Now, "mm/dd/yyyy")
470               .FpAddParam "h_dispatch_no", adInteger, adParamInputOutput, 4, CLng(strCurrentInvoiceNbr)
480               .FpAddParam "h_fee_amt", adDouble, adParamInput, -1, strFeeAmount
490               .FpAddParam "h_insrd_first_nm", adChar, adParamInput, 20, Left(strInsuredFirstName, 20)
500               .FpAddParam "h_insrd_last_nm", adChar, adParamInput, 20, Left(strInsuredLastName, 20)
510               .FpAddParam "h_ins_co_uid_no", adInteger, adParamInput, 4, lngInsuranceCompanyID
520               .FpAddParam "h_ins_pc_uid_no", adInteger, adParamInput, 4, lngIngresAccountingID
530               .FpAddParam "h_lcmt_id", adInteger, adParamInput, 4, 0
540               .FpAddParam "h_lynx_id", adChar, adParamInput, 10, CStr(lngLynxID)
550               .FpAddParam "h_msg_ind", adChar, adParamInputOutput, 1, ""
560               .FpAddParam "h_msg_text", adVarChar, adParamInputOutput, 200, strCarrierRepName
570               .FpAddParam "h_shop_location_id", adInteger, adParamInput, 4, lngPayeeID
580               .FpAddParam "h_tries", adInteger, adParamInput, 4, 1
590               .FpAddParam "h_user_id_no", adVarChar, adParamInput, 32, CStr(intUserID)
600               .FpAddParam "h_user_session_no", adVarChar, adParamInput, 32, ""
                  
610               strReturnValue = .FpExecute()
620               strDispatchNumber = .FpGetParameter("h_dispatch_no")
630               strMessageText = .FpGetParameter("h_msg_text")
640               strMessageInd = .FpGetParameter("h_msg_ind") 'No longer used
                  
650               .FpCleanUp
                  
660           End With
670       End If
          
680       MakePayment = strDispatchNumber
          
          'Verify what we got back from Ingres.
'690       If strReturnValue = "-1" Then
'700           Err.Raise eIngresReturnedExternalLogicalError, "Ingres Billing SP", strReturnValue & "|" & strMessageText
'710       ElseIf strReturnValue = "-2" Then
'720           Err.Raise eIngresReturnedInternalLogicalError, "Ingres Billing SP", strReturnValue & "|" & strMessageText
'730       ElseIf strReturnValue <> "0" Then
'740           Err.Raise eIngresReturnedErrorCode, "IngresBillingSP", strReturnValue & "|" & strMessageText
'750       End If
          
          'Verify the returned dispatch number.
'760       If strDispatchNumber = "0" Or strDispatchNumber = "" Then
'770           Err.Raise eIngresReturnedBadDispatchNumber, "Ingres Billing SP", strDispatchNumber
'780       End If
          
          '*
          '* Insert/Update the APD DB with the payment info.
          '*
          
790       If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Insert/Update APD Database"

800       ConnectToDB eAPD_Recordset
          
810       g_objApdRsAccessor.BeginTransaction
820       g_objApdRsAccessor.ExecuteSpNamedParams _
              GetConfig("Accounting/Payment/InsertDetailSP"), _
              "@ClaimAspectID", lngClaimAspectID, _
              "@PayeeID", lngPayeeID, _
              "@Description", strDescription, _
              "@DispatchNumber", strDispatchNumber, _
              "@PayeeAddress1", strPayeeAddress1, _
              "@PayeeAddress2", strPayeeAddress2, _
              "@PayeeCity", strPayeeCity, _
              "@PayeeState", strPayeeState, _
              "@PayeeZip", strPayeeZip, _
              "@PayeeTypeCD", strPayeeTypeCD, _
              "@PayeeName", strPayeeName, _
              "@Amount", strClaimAmount, _
              "@PaymentTypeCD", strPaymentTypeCD, _
              "@RentalDays", intRentalDays, _
              "@UserID", intUserID
              
          '*
          '* Send Email to Accounting for CEI Shops
          '*
          
          'If blnIsCeiShop Then
          
              Dim strSubject As String, strBody As String, strFrom As String, strTo As String
              
830           strTo = GetConfig("Accounting/Payment/CEI/@ToEmail")
              
840           If strTo <> "" Then
              
850               strFrom = GetConfig("Accounting/Payment/CEI/@FromEmail")
860               strSubject = "APD Payment of $" & strClaimAmount & " for Dispatch #" & strDispatchNumber
                  
870               strBody = _
                      "Dispatch Number: " & strDispatchNumber & vbCrLf & _
                      "Payment Amount: $" & strClaimAmount & vbCrLf & _
                      "Shop Name: " & strPayeeName & vbCrLf & _
                      "APD LYNX ID: " & lngLynxID & vbCrLf & _
                      "Claims Representative: " & strClaimRepName & vbCrLf
          
880               If g_blnDebugMode Then
890                   g_objEvents.Trace strSubject & vbCrLf & strBody, _
                          PROC_NAME & "Sending email to " & strTo
900               End If
          
910               g_objEvents.SendEmail strFrom, strTo, strSubject, strBody, False, 2
                  
920           End If
              
          'End If

930       If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Successful Finish!"
          
ErrorHandler:

          Dim lngNum As Long, strSrc As String, strDesc As String
          
          'Any error conditions found in all that crap above?
940       If Err.Number <> 0 Then
          
              ' Copy the error info so that it doesnt get overwritten
              ' by any problems in the transaction commit/abort.
950           lngNum = Err.Number
960           strSrc = "[Line " & Erl & "] " & PROC_NAME & Err.Source
970           strDesc = Err.Description

              'Attempt to rollback transactions
980           On Error Resume Next
990           If blnIngresEnabled Then g_objIngresAccessor.RollbackTransaction
1000          g_objApdRsAccessor.RollbackTransaction
1010          On Error GoTo 0
              
1020          g_objEvents.Env "INGRES: Return Value = '" & strReturnValue _
                  & "' Dispatch Num = '" & strDispatchNumber _
                  & "' Message Text = '" & strMessageText _
                  & "' Message Indicator = '" & strMessageInd & "'"
                  
1030          g_objEvents.HandleEvent lngNum, strSrc, strDesc
              
          'Otherwise, we are clean.  Commit the transactions.
1040      Else
          
              'Attempt to commit transactions.
1050          On Error Resume Next
1060          If blnIngresEnabled Then g_objIngresAccessor.CommitTransaction
1070          g_objApdRsAccessor.CommitTransaction

              'Problems committing?
1080          If Err.Number = 0 Then
1090              If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Transactions Committed Successfully."
1100          Else
                  ' Now throw the commit error back to the caller.
1110              lngNum = Err.Number
1120              strSrc = "[Line " & Erl & "] " & PROC_NAME & Err.Source
1130              strDesc = Err.Description

1140              On Error GoTo 0
1150              g_objEvents.HandleEvent lngNum, strSrc, "Error Committing Transaction: " & strDesc
1160          End If

1170      End If

1180      TerminateGlobals
          
End Function

