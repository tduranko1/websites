VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CWordDocument"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'********************************************************************************
'* Class CWordDocument
'*
'* Generic MSWord Utility class.
'* Requires MDomUtils.bas.
'* Requires reference to Microsoft Word 9.0 Library.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CWordDocument."

Private mobjWord As Word.Application
Private mobjDoc As Word.Document

'********************************************************************************
'* Class clean-up.
'********************************************************************************
Private Sub Class_Terminate()
10        CloseDocument
End Sub

'********************************************************************************
'* Returns the MS Word application object.
'********************************************************************************
Public Property Get Document() As Word.Document
10        If Not mobjWord Is Nothing Then
20            mobjDoc.Activate
30            Set Document = mobjDoc
40        End If
End Property

'********************************************************************************
'* Creates and initializes a MS Word application object.
'********************************************************************************
Public Sub LoadFromTemplate(ByVal strTemplateFile As String)
          Const PROC_NAME As String = MODULE_NAME & "LoadFromTemplate: "
          
10        On Error GoTo ErrorHandler
          
20        If g_blnDebugMode Then g_objEvents.Trace strTemplateFile, PROC_NAME & "Loading Template"
          
          'Close any existing document.
30        CloseDocument
          
          'Create the MS Word object.
40        Set mobjWord = New Word.Application
          
          'Initialize the MS Word object.
50        mobjWord.DisplayAlerts = wdAlertsNone
60        mobjWord.FeatureInstall = 0 'msoFeatureInstallNone
70        mobjWord.PrintPreview = False
80        mobjWord.Visible = False
          
90        mobjWord.Application.DisplayAlerts = wdAlertsNone
100       mobjWord.Application.FeatureInstall = 0 'msoFeatureInstallNone
110       mobjWord.Application.PrintPreview = False
120       mobjWord.Application.Visible = False
          
130       Set mobjDoc = mobjWord.Documents.Open( _
              FileName:=strTemplateFile, _
              ConfirmConversions:=False, _
              ReadOnly:=True, _
              AddToRecentFiles:=False, _
              PasswordDocument:="", _
              PasswordTemplate:="", _
              Revert:=True)
      '        Visible:=False) dont use this - for some reason document is not 'opened'
              
140       If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Template Loaded"
          
ErrorHandler:
150       If Err.Number <> 0 Then
160           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, strTemplateFile
170       End If
End Sub

'********************************************************************************
'* Prints the word document out to a file.
'********************************************************************************
Public Sub PrintToFile(ByVal strFaxPrinter As String, ByVal strFile As String)
          Const PROC_NAME As String = MODULE_NAME & "PrintToFile: "
          
10        If g_blnDebugMode Then g_objEvents.Trace strFaxPrinter & " : " & strFile, PROC_NAME & "Begin Print"

20        On Error Resume Next
          
30        Kill strFile    ' Kill it if it already exists
          
40        On Error GoTo ErrorHandler
          
50        mobjWord.ActivePrinter = strFaxPrinter

60        mobjDoc.Activate
          
70        mobjDoc.PrintOut _
              Background:=False, _
              Append:=False, _
              OutputFileName:=strFile, _
              PrintToFile:=True
              
80        If g_blnDebugMode Then g_objEvents.Trace strFaxPrinter & " : " & strFile, PROC_NAME & "Print Complete"
          
ErrorHandler:
90        If Err.Number <> 0 Then
100           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, strFaxPrinter & " : " & strFile
110       End If
End Sub

'********************************************************************************
'* Prints the word document out to a file.
'********************************************************************************
Public Sub SaveToFile(ByVal strFile As String)
          Const PROC_NAME As String = MODULE_NAME & "SaveToFile: "

10        On Error GoTo ErrorHandler
          
20        mobjDoc.SaveAs strFile
          
ErrorHandler:
30        If Err.Number <> 0 Then
40            g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, strFile
50        End If
End Sub

'********************************************************************************
'* Turns on document protection.
'********************************************************************************
Public Sub ProtectDocument()
          Const PROC_NAME As String = MODULE_NAME & "ProtectDocument: "
          
10        On Error GoTo ErrorHandler
          
20        mobjDoc.Protect wdAllowOnlyFormFields
          
ErrorHandler:
30        If Err.Number <> 0 Then
40            g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
50        End If
End Sub

'********************************************************************************
'* Closes up the word document
'********************************************************************************
Private Sub CloseDocument()
          Const PROC_NAME As String = MODULE_NAME & "CloseDocument: "
          
10        On Error GoTo ErrorHandler
          
20        If Not mobjDoc Is Nothing Then
          
30            mobjDoc.Close wdDoNotSaveChanges
40            Set mobjDoc = Nothing
              
50        End If
          
60        If Not mobjWord Is Nothing Then
          
70            mobjWord.Quit wdDoNotSaveChanges
80            Set mobjWord = Nothing
              
90        End If

ErrorHandler:
100       If Err.Number <> 0 Then
110           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
120       End If
End Sub

'********************************************************************************
'* Loop through every bookmark and insert values from the converted
'* XML that match up with the bookmark name.
'********************************************************************************
Public Sub LoadBookmarksFromXML( _
    ByRef objXmlDoc As MSXML2.DOMDocument40, _
    Optional ByVal blnForceMatch As Boolean = True)

          Const PROC_NAME As String = MODULE_NAME & "LoadBookmarksFromXML: "

10        On Error GoTo ErrorHandler
          
          Dim objBM As Bookmark
          Dim strName As String
          Dim intIdx As Integer

20        For Each objBM In mobjWord.ActiveDocument.Bookmarks
              Dim blnFound As Boolean
              
30            objBM.Select
40            strName = objBM.Name
50            blnFound = False
              
              'Double underscores indicate copies of a data
60            intIdx = InStr(1, strName, "__", vbTextCompare)
70            If intIdx > 0 Then
                  'remove anything after the double underscore.
80                strName = Left(strName, intIdx - 1)
90            End If
              
              'Underscores allow parent specification, i.e. Carrier_FirstName ==> //Carrier/@FirstName
100           If InStr(1, strName, "_", vbTextCompare) > 0 Then
110               For intIdx = Len(strName) To 1 Step -1
120                   If Mid(strName, intIdx, 1) = "_" Then
130                       blnFound = True
140                       strName = Left(strName, intIdx - 1) & Replace(strName, "_", "/@", intIdx)
150                       strName = "//" & Replace(strName, "_", "/")
160                   End If
170               Next
180           End If
              
              'Or just find any matching attribute, i.e. InsuredFirstName ==> //@InsuredFirstName
190           If Not blnFound Then
200               strName = "//@" & strName
210           End If

220           mobjWord.Selection.InsertAfter GetChildNodeText(objXmlDoc, strName, blnForceMatch)
230       Next
          
240       Set objBM = Nothing

ErrorHandler:
250       If Err.Number <> 0 Then
260           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, objXmlDoc.xml
270       End If
End Sub



'********************************************************************************
'* Syntax:      CreateLineItemsFromXML (objDataDom, "<items item1='DetailNumber'.../>", "/Root/Reinspect/ReinspectDetail", 3)
'* Parameters:  objDataDom - An XML document that contains the data used to populate the Word document.
'*              sCellsXml - An XML string whose attributes indicate the NodeList attributes that will populate each table cells.
'*              strCellsXml - A string representing the XPath to the nodes that will be used to create the line item table rows.
'*              intLineItemTable - An integer representing the position of the line item table in the tables collection of the word document.
'*                                 This will not currently handle nested tables and may have to be re-thought at some point.
'* Purpose:     Adds rows to a pre-defined table and populates the cells with attribute valuse from the indicated nodelist.
'* Returns:
'********************************************************************************
 
Public Sub CreateLineItemsFromXML(objDataDom As MSXML2.DOMDocument40, strCellsXml As String, strLineItemXPath As String, intLineItemTable As Integer)
        Const PROC_NAME As String = MODULE_NAME & "CreateLineItemsFromXML: "
       
10      On Error GoTo ErrorHandler
        
        Dim objCell As Word.Cell
        Dim objXMLCells As MSXML2.DOMDocument40
        Dim objNode As MSXML2.IXMLDOMNode
        
        'Add parameters to trace data.
20        If g_blnDebugMode Then g_objEvents.Trace "Params: " & vbCrLf & "strCellsXml=" & strCellsXml & vbCrLf & "strLineItemXPath = " & _
                                                   strLineItemXPath & vbCrLf & "intLineItemTable = " & intLineItemTable, PROC_NAME & "Started"
        
        'Validate our parameters.
30        g_objEvents.Assert CBool(strCellsXml <> ""), "No XML passed."
40        g_objEvents.Assert CBool(strLineItemXPath <> ""), "No XPath passed."
50        g_objEvents.Assert CBool(intLineItemTable > 0), "Invalid table number passed."
          
        
60      Set objXMLCells = New MSXML2.DOMDocument40
        
70      LoadXml objXMLCells, strCellsXml, PROC_NAME, "Table Column Headings"
        
        'Loop through each node in the node list indicated by the strLineItemXPath param.
80      For Each objNode In objDataDom.selectNodes(strLineItemXPath)
        
          ' Add a new row to the end of the line item table
90        mobjWord.Documents(1).Tables(intLineItemTable).Rows.Add
          
          ' Loop through each cell in the newly added row and populate the cell with the proper attribute value from the line item node
100       For Each objCell In mobjWord.Documents(1).Tables(intLineItemTable).Rows(mobjWord.Documents(1).Tables(intLineItemTable).Rows.Count).Cells
110         objCell.Select
120         mobjWord.Selection.InsertAfter GetChildNodeText(objNode, "@" & objXMLCells.documentElement.Attributes(objCell.ColumnIndex - 1).Text)
130       Next
        
140     Next
       
ErrorHandler:
150     Set objCell = Nothing
160     Set objXMLCells = Nothing
170     Set objNode = Nothing
       
180     If Err.Number <> 0 Then
190       g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
200     End If
End Sub
