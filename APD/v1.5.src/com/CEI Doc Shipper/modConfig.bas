Attribute VB_Name = "modConfig"
Option Explicit

Private Const MODULE_NAME As String = "modConfig"

Private m_oConfigXML As MSXML2.DOMDocument
Private m_blnConfigLoaded As Boolean

'***************************************************************************************
' Procedure : loadConfig
' DateTime  : 4/19/2006 16:41
' Author    : Ramesh Vishegu
' Purpose   : Load the config XML
'***************************************************************************************
'
Public Function loadConfig() As Boolean
          Const PROC_NAME As String = MODULE_NAME & ":loadConfig()"

10        Set m_oConfigXML = New MSXML2.DOMDocument
          
20        m_oConfigXML.async = False
           
30        If g_objFSO.FileExists(g_objFSO.BuildPath(App.Path, "config.xml")) = False Then
40            Err.Raise vbError + 100, "loadConfig", "Missing config.xml in application folder"
50        End If
          
60        m_oConfigXML.Load g_objFSO.BuildPath(App.Path, "config.xml")
70        If m_oConfigXML.parseError.errorCode <> 0 Then
80            Err.Raise vbError + 101, MODULE_NAME, "Malformed config.xml file. Reason: " & m_oConfigXML.parseError.reason
90        End If
          
100       m_blnConfigLoaded = True
End Function

'***************************************************************************************
' Procedure : getConfigSetting
' DateTime  : 4/19/2006 16:41
' Author    : Ramesh Vishegu
' Purpose   : Retrieve a setting irrespective of the environment
'***************************************************************************************
'
Public Function getConfigSetting(strPath As String, strAttribute As String) As String
          Const PROC_NAME As String = MODULE_NAME & ":getConfigSetting()"
          
          Dim oNode As IXMLDOMElement
          Dim strValue As String
          
10        strValue = ""
          
20        If strPath <> "" And m_blnConfigLoaded = True Then
30            Set oNode = getConfigElement(strPath)
              
40            If Not oNode Is Nothing Then
50                If strAttribute <> "" Then
60                    strValue = oNode.getAttribute(strValue)
70                Else
80                    If oNode.nodeType = NODE_TEXT Then
90                        strValue = oNode.Text
100                   End If
110               End If
120           End If
130       End If
          
140       getConfigSetting = strValue
End Function

'***************************************************************************************
' Procedure : getConfigElement
' DateTime  : 4/19/2006 16:42
' Author    : Ramesh Vishegu
' Purpose   : Retrieve a XML element irrespective of the environment
'***************************************************************************************
'
Public Function getConfigElement(strPath As String) As IXMLDOMElement
          Const PROC_NAME As String = MODULE_NAME & ":getConfigElement()"
          
          Dim oNode As IXMLDOMElement
10        If strPath <> "" And m_blnConfigLoaded = True Then
20            Set oNode = m_oConfigXML.selectSingleNode(strPath)
30        End If
40        Set getConfigElement = oNode
End Function

'***************************************************************************************
' Procedure : getConfigNodes
' DateTime  : 4/19/2006 16:42
' Author    : Ramesh Vishegu
' Purpose   : Retrieve a set of XML nodes irrespective of the environment
'***************************************************************************************
'
Public Function getConfigNodes(strPath As String) As IXMLDOMNodeList
          Const PROC_NAME As String = MODULE_NAME & ":getConfigNodes()"
          
          Dim oNodeList As IXMLDOMNodeList
10        If strPath <> "" And m_blnConfigLoaded = True Then
20            Set oNodeList = m_oConfigXML.selectNodes(strPath)
30        End If
40        Set getConfigNodes = oNodeList
End Function

'***************************************************************************************
' Procedure : getEnvSetting
' DateTime  : 4/19/2006 16:42
' Author    : Ramesh Vishegu
' Purpose   : Retrieve a setting for a particular environment
'***************************************************************************************
'
Public Function getEnvSetting(strPath As String, strAttribute As String) As String
          Const PROC_NAME As String = MODULE_NAME & ":getEnvSetting()"
          
          Dim oNode As IXMLDOMElement
          Dim strValue As String
          
10        strValue = ""
          
20        If strPath <> "" And m_blnConfigLoaded = True Then
30            Set oNode = getEnvElement(strPath)
              
40            If Not oNode Is Nothing Then
50                If strAttribute <> "" Then
60                    strValue = IIf(IsNull(oNode.getAttribute(strAttribute)), "", oNode.getAttribute(strAttribute))
70                Else
80                    If oNode.nodeType = NODE_ELEMENT Then
90                        strValue = oNode.Text
100                   End If
110               End If
120           End If
130       End If
          
140       getEnvSetting = strValue
End Function

'***************************************************************************************
' Procedure : getEnvElement
' DateTime  : 4/19/2006 16:43
' Author    : Ramesh Vishegu
' Purpose   : Retrieve a XML element for a particular environment
'***************************************************************************************
'
Public Function getEnvElement(strPath As String) As IXMLDOMElement
          Const PROC_NAME As String = MODULE_NAME & ":getConfigElement()"
          
          Dim oNode As IXMLDOMElement
10        If strPath <> "" And m_blnConfigLoaded = True Then
20            Set oNode = g_objEnv.selectSingleNode(strPath)
30        End If
40        Set getEnvElement = oNode
End Function

'***************************************************************************************
' Procedure : getEnvNodes
' DateTime  : 4/19/2006 16:43
' Author    : Ramesh Vishegu
' Purpose   : Retrieve a set of XML nodes for a particular environment
'***************************************************************************************
'
Public Function getEnvNodes(strPath As String) As IXMLDOMNodeList
          Const PROC_NAME As String = MODULE_NAME & ":getConfigNodes()"
          
          Dim oNodeList As IXMLDOMNodeList
10        If strPath <> "" And m_blnConfigLoaded = True Then
20            Set oNodeList = g_objEnv.selectNodes(strPath)
30        End If
40        Set getEnvNodes = oNodeList
End Function
