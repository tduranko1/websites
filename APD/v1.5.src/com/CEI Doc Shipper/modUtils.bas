Attribute VB_Name = "modUtils"
Option Explicit
Option Compare Text

Private Const MODULE_NAME As String = "modUtils"

Private strDefaultPrinter As String

Public Enum tagREADYSTATE
    READYSTATE_UNINITIALIZED = 0
    READYSTATE_LOADING
    READYSTATE_LOADED
    READYSTATE_INTERACTIVE
    READYSTATE_COMPLETE
End Enum

Private Type SHELLEXECUTEINFO
    cbSize As Long
    fMask As Long
    hwnd As Long
    lpVerb As String
    lpFile As String
    lpParameters As String
    lpDirectory As String
    nShow As Long
    hInstApp As Long
    lpIDList As Long
    lpClass As String
    hkeyClass As Long
    dwHotKey As Long
    hIcon As Long
    hProcess As Long
End Type

Public Const SW_MINIMIZE = 6

Public Const PDFCreator_Uninstall_GUID = "{0001B4FD-9EA3-4D90-A79E-FD14BA3AB01D}"
Public Const g_sAPPName As String = "CEIDocShipper"

Public g_objFSO As New Scripting.FileSystemObject
Public g_sTempPath As String
Public g_sAttachmentsPath As String
Public g_sLogsPath As String
Public g_sDailyLogFile As String
Public g_sJournalLogFile As String
Public g_sAPPIniFile As String
Public g_blnAutoRun As Boolean
Public g_objEnv As IXMLDOMElement

Public g_objEvents As Object
Public g_blnDebugMode As Boolean         'Are we in debug mode?? -- SiteUtilities
Public g_sAPDConfigPath As String

'Ghostscript settings
Public g_sGSExePath As String
Public g_sGSLibPath As String
Public g_sGSFontsPath As String

'PDF Printer settings
Public g_sPDFPrinterName As String
Public g_sPDFOutputFolder As String
Public g_sPSPrinterName As String

'SMTP settings
Public g_sSMTPServerName As String


Public g_objWordApp As Word.Application


Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Function GetUserNameAPI Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Public Declare Function SetDefaultPrinter Lib "winspool.drv" Alias "SetDefaultPrinterA" (ByVal Name As String) As Long
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

'***************************************************************************************
' Procedure : Word2PDF
' DateTime  : 4/19/2006 16:46
' Author    : Ramesh Vishegu
' Purpose   : Word Document to PDF converter. Document is printer to the PDF printer and
'             exists when the print job is complete
'***************************************************************************************
'
Public Function Word2PDF(strFileName As String) As Boolean
10        On Error GoTo errHandler
          Dim objWordDoc As Word.Document
          Dim strOutputPSFileName As String
          Dim strActivePrinter As String
          Dim iFileCheckCounter As Integer
          
          'check the default printer. If the default is not the pdf printer then make it.
          'If Printer.DeviceName <> g_sPDFPrinterName Then SetDefaultPrinter g_sPDFPrinterName
          
20        strOutputPSFileName = g_objFSO.BuildPath(g_sPDFOutputFolder, g_objFSO.GetBaseName(strFileName) & ".pdf")
          
30        Trace "     Converting " & strFileName & " to pdf..."
          
40        iFileCheckCounter = 10
          
50        If g_objWordApp Is Nothing Then
              'create the word application
60            Set g_objWordApp = CreateObject("Word.Application")
              
70            g_objWordApp.DisplayAlerts = wdAlertsNone
80            g_objWordApp.FeatureInstall = 0 'msoFeatureInstallNone
90            g_objWordApp.PrintPreview = False
100           g_objWordApp.Visible = False
                        
110           g_objWordApp.Application.DisplayAlerts = wdAlertsNone
120           g_objWordApp.Application.FeatureInstall = 0 'msoFeatureInstallNone
130           g_objWordApp.Application.PrintPreview = False
140           g_objWordApp.Application.Visible = False
              'g_objWordApp.Application.ActivePrinter = "Virtual PostScript Printer"
150       End If
          
          'open the word document
160       Set objWordDoc = g_objWordApp.Documents.Open( _
                              FileName:=strFileName, _
                              ConfirmConversions:=False, _
                              ReadOnly:=True, _
                              AddToRecentFiles:=False, _
                              PasswordDocument:="", _
                              PasswordTemplate:="", _
                              Revert:=True)
                              
170       strDefaultPrinter = g_objWordApp.ActivePrinter
180       SetPrinter g_sPDFPrinterName
          
190       With frmMain.PDFCreator1
200           .cOption("UseAutosave") = 1
210           .cOption("UseAutosaveDirectory") = 1
220           .cOption("AutosaveDirectory") = g_sPDFOutputFolder
230           .cOption("AutosaveFilename") = g_objFSO.GetBaseName(strFileName) & ".pdf"
240           .cOption("AutosaveFormat") = 0                            ' 0 = PDF
250           .cClearCache
260           DoEvents
270           objWordDoc.PrintOut Background:=False
280           DoEvents
290           .cPrinterStop = False
300       End With
                              
310       DoEvents
          
320       objWordDoc.Close wdDoNotSaveChanges
          
330       Trace "       Done."
          
          'destroy the word document
340       Set objWordDoc = Nothing
          
350       Word2PDF = True
errHandler:
360       If Err.Number <> 0 Then
370           Trace "      ERROR in Word2PDF [Line # " & Erl & "] " & Err.Description
380           g_objFSO.CopyFile strFileName, g_objFSO.BuildPath(App.Path, g_objFSO.GetBaseName(strFileName) & "." & g_objFSO.GetExtensionName(strFileName))
390           If Not objWordDoc Is Nothing Then
400               objWordDoc.Close wdDoNotSaveChanges
410               Set objWordDoc = Nothing
420           End If
430           Word2PDF = False
440       End If
End Function

Private Sub SetPrinter(Printername As String)
    With Dialogs(wdDialogFilePrintSetup)
        .Printer = Printername
        .DoNotSetAsSysDefault = True
        .Execute
    End With
End Sub


'***************************************************************************************
' Procedure : Image2PDF
' DateTime  : 4/19/2006 16:46
' Author    : Ramesh Vishegu
' Purpose   : Image to PDF converter.
'***************************************************************************************
'
Public Function Image2PDF(strFileName As String) As Boolean
    On Error GoTo errHandler
    Dim strPDFFileName As String
    
    strPDFFileName = g_objFSO.BuildPath(g_sPDFOutputFolder, g_objFSO.GetBaseName(strFileName) & ".pdf")
    
    With frmMain.imgFile
        'load the image into the ImageBasic control
        .LoadFile strFileName
        
        'save the document as a pdf document
        .OutputFileFormat = PixelFileOutputFormat_PDF_G4
        .OutputFileAppend = False
        .SaveDocument strPDFFileName
        
        'release the document
        .InputFileName = ""
        
    End With
    
    Image2PDF = True
    
errHandler:
    frmMain.imgFile.InputFileName = ""
    frmMain.imgFile.Clear
    
    If Err.Number <> 0 Then
        Image2PDF = False
    End If

End Function

'***************************************************************************************
' Procedure : ReduceImageSize
' DateTime  : 5/17/2006 09:51
' Author    : Ramesh Vishegu
' Purpose   : Reduces an image to 640x480 with aspect ratio preserved
'***************************************************************************************
'
Public Function ReduceImageSize(strFileName As String) As Boolean
10        On Error GoTo errHandler
          Dim strOutputFileName As String
          Dim strFileExtension As String
20        strFileExtension = g_objFSO.GetExtensionName(strFileName)
          
30        If strFileExtension = "jpg" Or strFileExtension = "jpeg" Or strFileExtension = "bmp" Or strFileExtension = "gif" Then
40            strOutputFileName = g_objFSO.BuildPath(g_objFSO.GetParentFolderName(strFileName), g_objFSO.GetBaseName(strFileName) & ".tmp")
50            With frmMain.imgFile
60                .LoadFile strFileName
70                If .PageWidth > 640 Or .PageHeight > 640 Then
80                    .OutputFileFormat = PixelFileOutputFormat_JPEG
90                    .OutputBitsPerSample = PixelFileBitsPerSample_8
100                   .OutputSamplesPerPixel = PixelFileSamplesPerPixel_3
110                   .OutputPhotoInterp = PixelFilePhotoInterp_RGB
120                   .OutputScalingNum = 640
130                   .OutputScalingDen = .PageWidth
                      
140                   .SavePage strOutputFileName
                      
150                   .InputFileName = ""
                      
                      'delete the original file
160                   g_objFSO.DeleteFile strFileName
                      
                      'move the converted image to the original file
170                   g_objFSO.MoveFile strOutputFileName, strFileName
180               End If
190               .InputFileName = ""
200           End With
210       End If
220       ReduceImageSize = True
errHandler:
230       If Err.Number > 0 Then
240           ReduceImageSize = False
250           frmMain.imgFile.InputFileName = ""
260       End If
End Function

'***************************************************************************************
' Procedure : XML2PDF
' DateTime  : 4/19/2006 16:47
' Author    : Ramesh Vishegu
' Purpose   : XML to PDF converter
'***************************************************************************************
'
Public Function XML2PDF(strFileName As String) As Boolean
10        On Error GoTo errHandler
          Dim oXML As MSXML2.DOMDocument
          Dim oXSL As MSXML2.DOMDocument
          Dim oXMLOutput As MSXML2.DOMDocument
          Dim strParentNode As String
          Dim strXSLFileName As String
          Dim strXSLOutputType As String
          Dim strTransformedFileName As String
          Dim oTxtStream As TextStream
          
20        Set oXML = New MSXML2.DOMDocument
30        oXML.async = False
40        oXML.Load strFileName
          
50        If oXML.parseError.errorCode <> 0 Then
60            Err.Raise vbError + 105, "XML2PDF", strFileName & " XML malformed. Reason: " & oXML.parseError.reason
70        End If
          
          'get the parent node name.
80        strParentNode = oXML.FirstChild.nodeName
          
90        Trace "    Converting " & strFileName & " to pdf..."
          
          'now see if we have a converter xsl template for the current parent node.
100       strXSLFileName = getEnvSetting("DocumentConverter/XMLPrintTemplates/XMLTransform[@root='" & strParentNode & "']", "XSL")
          
110       If strXSLFileName <> "" Then
              'get the type of output we have after the transformation. htm? or what. usually it is htm
120           strXSLOutputType = getEnvSetting("DocumentConverter/XMLPrintTemplates/XMLTransform[@root='" & strParentNode & "']", "outputtype")
130           strXSLFileName = g_objFSO.BuildPath(App.Path, strXSLFileName)
              
140           Trace "      XSL: " & strXSLFileName
          
              'load the XSL stylesheet into the DOM
150           Set oXSL = New MSXML2.DOMDocument
160           oXSL.async = False
170           oXSL.Load strXSLFileName
              
180           If oXSL.parseError.errorCode <> 0 Then
190               Err.Raise vbError + 105, "XML2PDF", strXSLFileName & " XSL malformed. Reason: " & oXSL.parseError.reason
200           End If
              
210           Set oXMLOutput = New MSXML2.DOMDocument
              
              'determine the output file name
220           strTransformedFileName = g_objFSO.BuildPath(g_objFSO.GetParentFolderName(strFileName), g_objFSO.GetBaseName(strFileName) & "." & strXSLOutputType)
              
230           Trace "      Transformation done."
              
              'transform the XML and save it to the output
240           Set oTxtStream = g_objFSO.OpenTextFile(strTransformedFileName, ForWriting, True)
250           oTxtStream.Write oXML.transformNode(oXSL)
260           oTxtStream.Close
270           Set oTxtStream = Nothing
              
280           Trace "      Saving to " & strTransformedFileName
              
              'now convert the transformed file to pdf
290           XML2PDF = Word2PDF(strTransformedFileName)
300       Else
              'no transformation stylesheet exist. Do the basic conversion of loading it into word and printing
310           XML2PDF = Word2PDF(strTransformedFileName)
320       End If
          
errHandler:
330       Set oXML = Nothing
340       Set oXSL = Nothing
350       Set oXMLOutput = Nothing
360       If Err.Number <> 0 Then
370           XML2PDF = False
380       End If
End Function

'***************************************************************************************
' Procedure : HTM2PDF
' DateTime  : 4/19/2006 16:52
' Author    : Ramesh Vishegu
' Purpose   : HTML to PDF conversion handled by Word
'***************************************************************************************
'
Public Function HTM2PDF(strFileName As String) As Boolean
          Const PROC_NAME As String = MODULE_NAME & ":htmToPDF()"
          
10        HTM2PDF = Word2PDF(strFileName)

End Function

'***************************************************************************************
' Procedure : PS2PDF
' DateTime  : 4/19/2006 16:53
' Author    : Ramesh Vishegu
' Purpose   : Postscript to PDF conversion using Ghostscript. For non-interactive mode,
'              make sure the user id under which the application is running is the admin
'              on the box for the shellExecute to work
'***************************************************************************************
'
Private Function PS2PDF(strFileName As String) As Boolean
          Dim oBATFile As TextStream
          Dim strBatFileName As String
          Dim strPDFFileName As String
          Dim iWaitCounter As Integer
          
10        strBatFileName = g_objFSO.BuildPath(g_objFSO.GetParentFolderName(strFileName), "myPS2PDF.bat")
20        strPDFFileName = g_objFSO.BuildPath(g_objFSO.GetParentFolderName(strFileName), g_objFSO.GetBaseName(strFileName) & ".pdf")
          
30        Set oBATFile = g_objFSO.OpenTextFile(strBatFileName, ForWriting, True)
40        oBATFile.WriteLine """" & g_sGSExePath & """ -I""" & g_sGSLibPath & """;""" & g_sGSFontsPath & """ -q -dNOPAUSE -dSAFER -dBATCH -sDEVICE=pdfwrite -sOutputFile=""" & strPDFFileName & """ -dUCRRandBGInfo=/Preserve -dUseFlatCompression=true -dOptmize=true -dDetectBlends=true -f """ & strFileName & """"
50        oBATFile.Close
60        Set oBATFile = Nothing
          
70        iWaitCounter = 20
          
          'run the batch file
80        ShellExecute 0&, vbNullString, strBatFileName, vbNullString, App.Path & "\temp", SW_MINIMIZE
          
90        While iWaitCounter > 0 And g_objFSO.FileExists(strPDFFileName) = False
100           Sleep 250
110           iWaitCounter = iWaitCounter - 1
120       Wend
          
130       PS2PDF = g_objFSO.FileExists(strPDFFileName)
          
End Function

'***************************************************************************************
' Procedure : ForceBuildPath
' DateTime  : 4/19/2006 16:54
' Author    : Ramesh Vishegu
' Purpose   : Build a folder path recursively
'***************************************************************************************
'
Public Function ForceBuildPath(sPath As String) As Boolean
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & ":BuildPath()"
          Dim sFolders() As String
          Dim sCurrentFolder As String
          Dim i As Integer
          Dim bUNCPath As Boolean
          
20        bUNCPath = (Left(sPath, 2) = "\\")
              
30        sCurrentFolder = ""
          
40        If bUNCPath = False Then
              'create the logging path
50            sFolders = Split(sPath, "\")
60        Else
70            sCurrentFolder = Mid(sPath, 1, InStr(4, sPath, "\") - 1)
80            sFolders = Split(Mid(sPath, InStr(4, sPath, "\") + 1), "\")
90        End If
          
100       For i = 0 To UBound(sFolders)
110           If InStr(1, sFolders(i), ":") = 0 Then
120               If i > 0 Then sCurrentFolder = sCurrentFolder & "\"
130               sCurrentFolder = g_objFSO.BuildPath(sCurrentFolder, sFolders(i))
                  
140               If Not g_objFSO.FolderExists(sCurrentFolder) Then g_objFSO.CreateFolder (sCurrentFolder)
150           Else
160               sCurrentFolder = sCurrentFolder & sFolders(i)
170           End If
180       Next
190       ForceBuildPath = True
errHandler:
200       If Err.Number <> 0 Then
210           ForceBuildPath = False
220       End If
End Function

'***************************************************************************************
' Procedure : GetWinComputerName
' DateTime  : 4/19/2006 16:54
' Author    : Ramesh Vishegu
' Purpose   : Retrieves the computer name
'***************************************************************************************
'
Public Function GetWinComputerName() As String
          Const PROC_NAME As String = MODULE_NAME & ":GetWinComputerName()"
10        On Error GoTo errHandler
          Dim strName As String
          Dim lngCnt As Long
          
20        strName = Space(255)
30        lngCnt = GetComputerName(strName, 255)
          
40        If lngCnt <> 0 Then
50            strName = Left(strName, InStr(strName, Chr$(0)) - 1)
60        End If
          
70        GetWinComputerName = strName
80        Exit Function
errHandler:
90        If Err.Number <> 0 Then
100           GetWinComputerName = ""
110       End If
End Function

'********************************************************************************
'* Creates the object from the class string specified and returns it
'********************************************************************************
Public Function CreateObjectEx(strClassString As String) As Object
      Const PROC_NAME As String = "CreateObjectEx()"
10        On Error GoTo errHandler
          Dim strMsg As String
          
20        Set CreateObjectEx = CreateObject(strClassString)
          
30        Exit Function
errHandler:
40        strMsg = "    Error creating """ + strClassString + """ class object"

50        g_objEvents.HandleEvent Err.Number, PROC_NAME & Err.Source, Err.Description, _
              PROC_NAME & vbCrLf & strMsg
          
End Function

'***************************************************************************************
' Procedure : Trace
' DateTime  : 4/19/2006 16:55
' Author    : Ramesh Vishegu
' Purpose   : Write log entry to the DataPresenter log file (if exist) or writes to
'             a log file under the application directory
'***************************************************************************************
'
Public Sub Trace(strMsg As String)
10        If Not g_objEvents Is Nothing Then
20            g_objEvents.Trace strMsg
30        Else
              Dim oErrFile As TextStream
              Dim sErrFileName As String
              
40            sErrFileName = g_objFSO.BuildPath(App.Path, "Error Log " & Format(Now, "yyyy-mm-dd") & ".log")
              
50            Set oErrFile = g_objFSO.OpenTextFile(sErrFileName, ForAppending, True)
60            oErrFile.WriteLine strMsg
70            oErrFile.Close
80            Set oErrFile = Nothing
90        End If
End Sub
