Attribute VB_Name = "modIniUtils"
Option Explicit

Const MODULE_NAME As String = "IMPSEmailSvc:modRegistry"

'ini functions
Public Declare Function WritePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpString As String, ByVal lpFileName As String) As Long
Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long
Public Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Long, ByVal lpFileName As String) As Long
Public Declare Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function FormatMessageAPI Lib "kernel32" Alias "FormatMessageA" (ByVal dwFlags As Long, lpSource As Any, ByVal dwMessageId As Long, ByVal dwLanguageId As Long, ByVal lpBuffer As String, ByVal nSize As Long, Arguments As Long) As Long

'***************************************************************************************
' Procedure : getProfileString
' DateTime  : 4/19/2006 16:43
' Author    : Ramesh Vishegu
' Purpose   : Retrieve a profile string from the ini file
'***************************************************************************************
'
Public Function getProfileString(sAppName As String, sKeyName As String, sIniFileName As String) As String
      Const PROC_NAME As String = MODULE_NAME & "getProfileString() "
10    On Error GoTo errHandler
          Dim sRet As String * 255
          Dim bSuccess As Boolean
20        bSuccess = GetPrivateProfileString(sAppName, sKeyName, "", sRet, CLng(Len(sRet)), sIniFileName)
30        If bSuccess Then
40            sRet = Trim(Replace(sRet, Chr(0), ""))
50            getProfileString = Trim(sRet)
60        Else
70            GoTo errHandler
80        End If
90        Exit Function
100       getProfileString = vbNull
errHandler:
110       If Err.Number <> 0 Then
120           getProfileString = vbNull
130       End If
End Function

'***************************************************************************************
' Procedure : getProfileInt
' DateTime  : 4/19/2006 16:44
' Author    : Ramesh Vishegu
' Purpose   : Retrieve a integer profile value from the ini file
'***************************************************************************************
'
Public Function getProfileInt(sAppName As String, sKeyName As String, sIniFileName) As Long
      Const PROC_NAME As String = MODULE_NAME & "getProfileInt() "
10    On Error GoTo errHandler
20        getProfileInt = GetPrivateProfileInt(sAppName, sKeyName, 0, sIniFileName)
30        Exit Function
errHandler:
40        If Err.Number <> 0 Then
50            getProfileInt = 0
60        End If
End Function

'***************************************************************************************
' Procedure : getProfileSection
' DateTime  : 4/19/2006 16:44
' Author    : Ramesh Vishegu
' Purpose   : Retrieve the full profile section from the ini file
'***************************************************************************************
'
Public Function getProfileSection(sAppName As String, sIniFileName As String) As String()
      Const PROC_NAME As String = MODULE_NAME & "getProfileSection() "
10    On Error GoTo errHandler
          Dim sSections As String * 32767
          Dim sRet() As String
          Dim bSuccess As Boolean
20        bSuccess = GetPrivateProfileSection(sAppName, sSections, CLng(Len(sSections)), sIniFileName)
30        If bSuccess Then
40            sSections = Replace(sSections, Chr(0) & Chr(0), "")
50            sRet = Split(sSections, Chr(0))
60            getProfileSection = sRet
70        Else
80            ReDim sRet(1)
90            sRet(0) = "0"
100           GoTo errHandler
              'getProfileSection = sRet
110       End If
120       Exit Function
errHandler:
130       If Err.Number <> 0 Then
140           getProfileSection = sRet
150       End If
End Function

'***************************************************************************************
' Procedure : setProfileString
' DateTime  : 4/19/2006 16:44
' Author    : Ramesh Vishegu
' Purpose   : Write a profile string to the ini file
'***************************************************************************************
'
Public Function setProfileString(sAppName As String, sKeyName As String, sValue As String, sIniFileName As String) As Boolean
      Const PROC_NAME As String = MODULE_NAME & "setProfileString() "
10    On Error GoTo errHandler
          Dim sRet As Long
          Dim oFile As File
20        If g_objFSO.FileExists(sIniFileName) Then
30            Set oFile = g_objFSO.GetFile(sIniFileName)
40            If (oFile.Attributes And ReadOnly) = ReadOnly Then
50                oFile.Attributes = Normal
60            End If
70            Set oFile = Nothing
80        End If
90        sRet = WritePrivateProfileString(sAppName, sKeyName, sValue, sIniFileName)
100       If sRet <> 0 Then
110           setProfileString = True
120       Else
130           GoTo errHandler
140       End If
150       Exit Function
errHandler:
160       If Err.Number <> 0 Then
170           setProfileString = False
180           FormatApiErrorMessage Err.LastDllError
190       End If
End Function

'********************************************************************
'* Syntax:  FormatApiErrorMessage
'* Params:  lngErrNumber - The API error number
'* Purpose: Retrieves and formats API error messages
'* Returns: An string - the formatted error.
'********************************************************************
Public Function FormatApiErrorMessage(ByVal lngErrNumber As Long) As String
          Dim strBuffer As String * 512, strMsg As String
10        On Error GoTo ErrorHandler
          
20        FormatMessageAPI &H1000, Null, lngErrNumber, 0, strBuffer, 512, 0
          
30        strMsg = strBuffer
          'Strange but necessary manipulations
40        strMsg = Replace(strMsg, vbNewLine, "")
50        strMsg = Replace(strMsg, Chr(0), "")
          
60        FormatApiErrorMessage = strMsg
ErrorHandler:
End Function


