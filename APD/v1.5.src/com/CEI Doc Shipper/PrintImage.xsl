<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace">

<xsl:output method="xml" indent="no" encoding="UTF-8"/>

<msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
        /*********************************************************
        *  This function will process the next node and return the
        *  value of the node in a suitable
        **********************************************************/

        function getPageInfo( nodeList )
        {
            //var ndeType = nde.dataType;
            var nde = nodeList.nextNode().firstChild;
            var ndeType = nde.nodeType;
            if (ndeType == 4) { //CDATA
                var retVal = "\n" + stripEmptyLines(nde.nodeTypedValue);
                return retVal;
            }
            //else
            //    return nde.nodeTypedValue;
        }

        /*********************************************************
        *  This function will chop the top and bottom empty lines
        *  so that the page break does not extend to the next page.
        **********************************************************/
        function stripEmptyLines(str){
            var tmpArr;
            var tmpLine = "                                                                                ";

            str = str.replace(/\t/g, "");
            tmpArr = str.split("\n");

            var arrLen = tmpArr.length;

            //chop the top empty lines
            for (var i = 0; i < arrLen; i++) {
                if (tmpLine.indexOf(tmpArr[0]) != -1)
                    tmpArr.splice(0, 1);
                else
                    break;
            }

            //chop the bottom empty lines
            for (var i = tmpArr.length - 1; i >= 0; i--) {
                if (tmpLine.indexOf(tmpArr[tmpArr.length-1]) != -1)
                    tmpArr.splice(tmpArr.length-1, 1);
                else
                    break;
            }

            return tmpArr.join("\n");
        }

  ]]>
</msxsl:script>
<xsl:template match="/PrintImage">
    <html>
        <head>
        <style>
            @page Section1
            	{size:8.5in 11.0in;
            	margin:1in .75in 1in .75in;
            	vertical-align:middle;
            	mso-header-margin:.5in;
            	mso-footer-margin:.5in;
            	mso-paper-source:0;}
            div.Section1
            	{page:Section1;}
        </style>
        </head>
        <body topmargin="0" bottommargin="0" rightmargin="0" leftmargin="0" margintop="0" marginleft="0">
		<xsl:for-each select="Page">
		<div class="Section1">
			<pre style="font:10pt Lucida console,courier new">
				<xsl:value-of select="user:getPageInfo(.)"/>
			</pre>
			<xsl:if test="position() != last()">
			<br clear="all" style="mso-special-character:line-break;page-break-before:always"/>
			</xsl:if>
		</div>
		</xsl:for-each>
        </body>
    </html>
</xsl:template>
    
</xsl:stylesheet>
