Attribute VB_Name = "modPrinter"
Option Explicit

Const MODULE_NAME As String = "IMPSEmailSvc:modPrinter:"

Public Declare Function lstrcpy Lib "kernel32" _
   Alias "lstrcpyA" _
   (ByVal lpString1 As String, _
   ByVal lpString2 As String) _
   As Long

Public Declare Function OpenPrinter Lib "winspool.drv" _
   Alias "OpenPrinterA" _
   (ByVal pPrinterName As String, _
   phPrinter As Long, _
   pDefault As PRINTER_DEFAULTS) _
   As Long

Public Declare Function GetPrinter Lib "winspool.drv" Alias "GetPrinterA" _
   (ByVal hPrinter As Long, _
   ByVal Level As Long, _
   pPrinter As Byte, _
   ByVal cbBuf As Long, _
   pcbNeeded As Long) _
   As Long

Public Declare Function ClosePrinter Lib "winspool.drv" _
   (ByVal hPrinter As Long) _
   As Long

Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" _
   (Destination As Any, _
   Source As Any, _
   ByVal Length As Long)

Public Declare Function EnumJobs Lib "winspool.drv" Alias "EnumJobsA" _
   (ByVal hPrinter As Long, _
   ByVal FirstJob As Long, _
   ByVal NoJobs As Long, _
   ByVal Level As Long, _
   pJob As Byte, _
   ByVal cdBuf As Long, _
   pcbNeeded As Long, _
   pcReturned As Long) _
   As Long
   
' constants for PRINTER_DEFAULTS structure
Public Const PRINTER_ACCESS_USE = &H8
Public Const PRINTER_ACCESS_ADMINISTER = &H4

' constants for DEVMODE structure
Public Const CCHDEVICENAME = 32
Public Const CCHFORMNAME = 32

Public Type PRINTER_DEFAULTS
   pDatatype As String
   pDevMode As Long
   DesiredAccess As Long
End Type

Public Type DEVMODE
   dmDeviceName As String * CCHDEVICENAME
   dmSpecVersion As Integer
   dmDriverVersion As Integer
   dmSize As Integer
   dmDriverExtra As Integer
   dmFields As Long
   dmOrientation As Integer
   dmPaperSize As Integer
   dmPaperLength As Integer
   dmPaperWidth As Integer
   dmScale As Integer
   dmCopies As Integer
   dmDefaultSource As Integer
   dmPrintQuality As Integer
   dmColor As Integer
   dmDuplex As Integer
   dmYResolution As Integer
   dmTTOption As Integer
   dmCollate As Integer
   dmFormName As String * CCHFORMNAME
   dmLogPixels As Integer
   dmBitsPerPel As Long
   dmPelsWidth As Long
   dmPelsHeight As Long
   dmDisplayFlags As Long
   dmDisplayFrequency As Long
End Type

Type SYSTEMTIME
   wYear As Integer
   wMonth As Integer
   wDayOfWeek As Integer
   wDay As Integer
   wHour As Integer
   wMinute As Integer
   wSecond As Integer
   wMilliseconds As Integer
End Type

Type JOB_INFO_2
   JobId As Long
   pPrinterName As Long
   pMachineName As Long
   pUserName As Long
   pDocument As Long
   pNotifyName As Long
   pDatatype As Long
   pPrintProcessor As Long
   pParameters As Long
   pDriverName As Long
   pDevMode As Long
   pStatus As Long
   pSecurityDescriptor As Long
   Status As Long
   Priority As Long
   Position As Long
   StartTime As Long
   UntilTime As Long
   TotalPages As Long
   Size As Long
   Submitted As SYSTEMTIME
   time As Long
   PagesPrinted As Long
End Type

Type PRINTER_INFO_2
   pServerName As Long
   pPrinterName As Long
   pShareName As Long
   pPortName As Long
   pDriverName As Long
   pComment As Long
   pLocation As Long
   pDevMode As Long
   pSepFile As Long
   pPrintProcessor As Long
   pDatatype As Long
   pParameters As Long
   pSecurityDescriptor As Long
   Attributes As Long
   Priority As Long
   DefaultPriority As Long
   StartTime As Long
   UntilTime As Long
   Status As Long
   cJobs As Long
   AveragePPM As Long
End Type

Public Const ERROR_INSUFFICIENT_BUFFER = 122
Public Const PRINTER_STATUS_BUSY = &H200
Public Const PRINTER_STATUS_DOOR_OPEN = &H400000
Public Const PRINTER_STATUS_ERROR = &H2
Public Const PRINTER_STATUS_INITIALIZING = &H8000
Public Const PRINTER_STATUS_IO_ACTIVE = &H100
Public Const PRINTER_STATUS_MANUAL_FEED = &H20
Public Const PRINTER_STATUS_NO_TONER = &H40000
Public Const PRINTER_STATUS_NOT_AVAILABLE = &H1000
Public Const PRINTER_STATUS_OFFLINE = &H80
Public Const PRINTER_STATUS_OUT_OF_MEMORY = &H200000
Public Const PRINTER_STATUS_OUTPUT_BIN_FULL = &H800
Public Const PRINTER_STATUS_PAGE_PUNT = &H80000
Public Const PRINTER_STATUS_PAPER_JAM = &H8
Public Const PRINTER_STATUS_PAPER_OUT = &H10
Public Const PRINTER_STATUS_PAPER_PROBLEM = &H40
Public Const PRINTER_STATUS_PAUSED = &H1
Public Const PRINTER_STATUS_PENDING_DELETION = &H4
Public Const PRINTER_STATUS_PRINTING = &H400
Public Const PRINTER_STATUS_PROCESSING = &H4000
Public Const PRINTER_STATUS_TONER_LOW = &H20000
Public Const PRINTER_STATUS_USER_INTERVENTION = &H100000
Public Const PRINTER_STATUS_WAITING = &H2000
Public Const PRINTER_STATUS_WARMING_UP = &H10000
Public Const JOB_STATUS_PAUSED = &H1
Public Const JOB_STATUS_ERROR = &H2
Public Const JOB_STATUS_DELETING = &H4
Public Const JOB_STATUS_SPOOLING = &H8
Public Const JOB_STATUS_PRINTING = &H10
Public Const JOB_STATUS_OFFLINE = &H20
Public Const JOB_STATUS_PAPEROUT = &H40
Public Const JOB_STATUS_PRINTED = &H80
Public Const JOB_STATUS_DELETED = &H100
Public Const JOB_STATUS_BLOCKED_DEVQ = &H200
Public Const JOB_STATUS_USER_INTERVENTION = &H400
Public Const JOB_STATUS_RESTART = &H800

Public Function GetString(ByVal PtrStr As Long) As String
         Dim StrBuff As String * 256
         
         'Check for zero address
10       If PtrStr = 0 Then
20          GetString = " "
30          Exit Function
40       End If
         
         'Copy data from PtrStr to buffer.
50       CopyMemory ByVal StrBuff, ByVal PtrStr, 256
         
         'Strip any trailing nulls from string.
60       GetString = StripNulls(StrBuff)
End Function

Public Function StripNulls(OriginalStr As String) As String
         'Strip any trailing nulls from input string.
10       If (InStr(OriginalStr, Chr(0)) > 0) Then
20          OriginalStr = Left(OriginalStr, InStr(OriginalStr, Chr(0)) - 1)
30       End If

         'Return modified string.
40       StripNulls = OriginalStr
End Function

Public Function PtrCtoVbString(Add As Long) As String
          Dim sTemp As String * 512
          Dim x As Long

10        x = lstrcpy(sTemp, Add)
20        If (InStr(1, sTemp, Chr(0)) = 0) Then
30             PtrCtoVbString = ""
40        Else
50             PtrCtoVbString = Left(sTemp, InStr(1, sTemp, Chr(0)) - 1)
60        End If
End Function

Public Function CheckPrinterStatus(PI2Status As Long) As String
         Dim tempStr As String
         
10       If PI2Status = 0 Then   ' Return "Ready"
20          CheckPrinterStatus = "Printer Status = Ready" & vbCrLf
30       Else
40          tempStr = ""   ' Clear
50          If (PI2Status And PRINTER_STATUS_BUSY) Then
60             tempStr = tempStr & "Busy  "
70          End If
            
80          If (PI2Status And PRINTER_STATUS_DOOR_OPEN) Then
90             tempStr = tempStr & "Printer Door Open  "
100         End If
            
110         If (PI2Status And PRINTER_STATUS_ERROR) Then
120            tempStr = tempStr & "Printer Error  "
130         End If
            
140         If (PI2Status And PRINTER_STATUS_INITIALIZING) Then
150            tempStr = tempStr & "Initializing  "
160         End If
            
170         If (PI2Status And PRINTER_STATUS_IO_ACTIVE) Then
180            tempStr = tempStr & "I/O Active  "
190         End If

200         If (PI2Status And PRINTER_STATUS_MANUAL_FEED) Then
210            tempStr = tempStr & "Manual Feed  "
220         End If
            
230         If (PI2Status And PRINTER_STATUS_NO_TONER) Then
240            tempStr = tempStr & "No Toner  "
250         End If
            
260         If (PI2Status And PRINTER_STATUS_NOT_AVAILABLE) Then
270            tempStr = tempStr & "Not Available  "
280         End If
            
290         If (PI2Status And PRINTER_STATUS_OFFLINE) Then
300            tempStr = tempStr & "Off Line  "
310         End If
            
320         If (PI2Status And PRINTER_STATUS_OUT_OF_MEMORY) Then
330            tempStr = tempStr & "Out of Memory  "
340         End If
            
350         If (PI2Status And PRINTER_STATUS_OUTPUT_BIN_FULL) Then
360            tempStr = tempStr & "Output Bin Full  "
370         End If
            
380         If (PI2Status And PRINTER_STATUS_PAGE_PUNT) Then
390            tempStr = tempStr & "Page Punt  "
400         End If
            
410         If (PI2Status And PRINTER_STATUS_PAPER_JAM) Then
420            tempStr = tempStr & "Paper Jam  "
430         End If

440         If (PI2Status And PRINTER_STATUS_PAPER_OUT) Then
450            tempStr = tempStr & "Paper Out  "
460         End If
            
470         If (PI2Status And PRINTER_STATUS_OUTPUT_BIN_FULL) Then
480            tempStr = tempStr & "Output Bin Full  "
490         End If
            
500         If (PI2Status And PRINTER_STATUS_PAPER_PROBLEM) Then
510            tempStr = tempStr & "Page Problem  "
520         End If
            
530         If (PI2Status And PRINTER_STATUS_PAUSED) Then
540            tempStr = tempStr & "Paused  "
550         End If

560         If (PI2Status And PRINTER_STATUS_PENDING_DELETION) Then
570            tempStr = tempStr & "Pending Deletion  "
580         End If
            
590         If (PI2Status And PRINTER_STATUS_PRINTING) Then
600            tempStr = tempStr & "Printing  "
610         End If
            
620         If (PI2Status And PRINTER_STATUS_PROCESSING) Then
630            tempStr = tempStr & "Processing  "
640         End If
            
650         If (PI2Status And PRINTER_STATUS_TONER_LOW) Then
660            tempStr = tempStr & "Toner Low  "
670         End If

680         If (PI2Status And PRINTER_STATUS_USER_INTERVENTION) Then
690            tempStr = tempStr & "User Intervention  "
700         End If
            
710         If (PI2Status And PRINTER_STATUS_WAITING) Then
720            tempStr = tempStr & "Waiting  "
730         End If
            
740         If (PI2Status And PRINTER_STATUS_WARMING_UP) Then
750            tempStr = tempStr & "Warming Up  "
760         End If
            
            'Did you find a known status?
770         If Len(tempStr) = 0 Then
780            tempStr = "Unknown Status of " & PI2Status
790         End If
            
            'Return the Status
800         CheckPrinterStatus = "Printer Status = " & tempStr & vbCrLf
810      End If
End Function

'***************************************************************************************
' Procedure : waitForPrinter
' DateTime  : 4/19/2006 16:45
' Author    : Ramesh Vishegu
' Purpose   : Wait for the print job to complete
'***************************************************************************************
'
Public Function waitForPrinter(sPrinterName As String) As Boolean
          Dim NumJI2 As Long
          Dim result As Long
          Dim hPrinter As Long
          Dim pDefaults As PRINTER_DEFAULTS
          Dim BytesNeeded As Long
          Dim iCount As Long
          
          'Call API to get a handle to the printer.
10        result = OpenPrinter(sPrinterName, hPrinter, pDefaults)
20        If result = 0 Then
             'If an error occurred, display an error and exit sub.
30           waitForPrinter = False
40           Exit Function
50        End If
          
          'Init BytesNeeded
60        BytesNeeded = 0
          
70        result = EnumJobs(hPrinter, 0&, &HFFFFFFFF, 2, ByVal 0&, 0&, _
             BytesNeeded, NumJI2)
          
80        iCount = 4 * 60 ' 1 minute
90        While BytesNeeded > 0 And iCount > 0
100           result = EnumJobs(hPrinter, 0&, &HFFFFFFFF, 2, ByVal 0&, 0&, _
                 BytesNeeded, NumJI2)
110           DoEvents
120           Sleep 250
130           iCount = iCount - 1
140       Wend
          
150       result = ClosePrinter(hPrinter)
          
160       If iCount <= 0 Then
170           waitForPrinter = False
180       Else
190           waitForPrinter = True
200       End If
End Function

'***************************************************************************************
' Procedure : waitForPrinterPickup
' DateTime  : 4/19/2006 16:45
' Author    : Ramesh Vishegu
' Purpose   : wait for the print job to appear in the printer queue
'***************************************************************************************
'
Public Function waitForPrinterPickup(sPrinterName As String) As Boolean
          Dim NumJI2 As Long
          Dim result As Long
          Dim hPrinter As Long
          Dim pDefaults As PRINTER_DEFAULTS
          Dim BytesNeeded As Long
          Dim iCount As Long
          
          'Call API to get a handle to the printer.
10        result = OpenPrinter(sPrinterName, hPrinter, pDefaults)
20        If result = 0 Then
             'If an error occurred, display an error and exit sub.
30           waitForPrinterPickup = False
40           Exit Function
50        End If
          
          'Init BytesNeeded
60        BytesNeeded = 0
          
70        result = EnumJobs(hPrinter, 0&, &HFFFFFFFF, 2, ByVal 0&, 0&, _
             BytesNeeded, NumJI2)
          
80        iCount = 4 * 60 '250ms * 4 * 60 = 1 minute
90        While BytesNeeded = 0 And iCount > 0
100           result = EnumJobs(hPrinter, 0&, &HFFFFFFFF, 2, ByVal 0&, 0&, _
                 BytesNeeded, NumJI2)
110           DoEvents
120           Sleep 250
130           iCount = iCount - 1
140       Wend
          
150       result = ClosePrinter(hPrinter)
160       If iCount <= 0 Then
170           waitForPrinterPickup = False
180       Else
190           waitForPrinterPickup = True
200       End If
End Function


