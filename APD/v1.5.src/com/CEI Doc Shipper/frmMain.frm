VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "shdocvw.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{10000000-1024-11CF-A19E-0020AF333BD8}#1.0#0"; "PIXFIL32.OCX"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CEI Document Packager"
   ClientHeight    =   4755
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8850
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4755
   ScaleWidth      =   8850
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkSendAgain 
      Caption         =   "Send again"
      Height          =   195
      Left            =   7560
      TabIndex        =   20
      Top             =   2220
      Width           =   1215
   End
   Begin VB.OptionButton opLynxID 
      Caption         =   "This LYNX id:"
      Height          =   315
      Left            =   60
      TabIndex        =   19
      Top             =   2100
      Width           =   1695
   End
   Begin VB.TextBox txtLynxID 
      Enabled         =   0   'False
      Height          =   315
      Left            =   2400
      TabIndex        =   18
      Top             =   2100
      Width           =   2055
   End
   Begin PIXFILELib.PixFile imgFile 
      Left            =   6000
      Top             =   60
      _Version        =   131075
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
      Active          =   -1  'True
      LinkID          =   "12%0\>/*!"
      Reserved1       =   1
      Reserved2       =   $"frmMain.frx":0000
      ImageDataSource =   "<None>"
      InputFileName   =   ""
      OutputBitsPerSample=   1
      OutputFileAppend=   0   'False
      OutputFileFormat=   24
      OutputPhotoInterp=   0
      OutputSamplesPerPixel=   1
      OutputScalingNum=   0
      OutputScalingDen=   0
      PageIndex       =   0
      JPEGCompressionFactor=   0
      OutputRotation  =   0
      OutputScalingYNum=   0
      OutputScalingYDen=   0
   End
   Begin MSComctlLib.ListView lvDocuments 
      Height          =   1335
      Left            =   60
      TabIndex        =   17
      Top             =   2520
      Width           =   8715
      _ExtentX        =   15372
      _ExtentY        =   2355
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Appearance      =   1
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "DocumentID"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Insurance Co."
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Created Date"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Image Location"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Document Type"
         Object.Width           =   2540
      EndProperty
   End
   Begin SHDocVwCtl.WebBrowser wb 
      Height          =   915
      Left            =   2820
      TabIndex        =   16
      Top             =   2760
      Width           =   1635
      ExtentX         =   2884
      ExtentY         =   1614
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
   Begin VB.Timer tmrStart 
      Interval        =   1000
      Left            =   7740
      Top             =   3840
   End
   Begin VB.ComboBox cmbInsuranceCo 
      Height          =   315
      Left            =   1200
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   15
      Top             =   480
      Width           =   3975
   End
   Begin VB.CommandButton cmdProcess 
      Caption         =   "Ship 'em"
      Height          =   495
      Left            =   7620
      TabIndex        =   13
      Top             =   1200
      Width           =   1155
   End
   Begin VB.TextBox txtTo 
      Height          =   315
      Left            =   5220
      TabIndex        =   11
      Top             =   1260
      Width           =   2055
   End
   Begin VB.TextBox txtFrom 
      Height          =   315
      Left            =   2400
      TabIndex        =   9
      Top             =   1260
      Width           =   2055
   End
   Begin VB.OptionButton opDateRange 
      Caption         =   "This Date Range:"
      Height          =   315
      Left            =   60
      TabIndex        =   8
      Top             =   1260
      Value           =   -1  'True
      Width           =   1575
   End
   Begin VB.TextBox txtDocumentID 
      Enabled         =   0   'False
      Height          =   315
      Left            =   2400
      TabIndex        =   6
      Top             =   1680
      Width           =   2055
   End
   Begin VB.OptionButton opDocumentID 
      Caption         =   "This DocumentID:"
      Height          =   315
      Left            =   60
      TabIndex        =   5
      Top             =   1680
      Width           =   1695
   End
   Begin VB.ComboBox cmbEnvironment 
      Height          =   315
      Left            =   1200
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   60
      Width           =   3975
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   495
      Left            =   7620
      TabIndex        =   2
      Top             =   120
      Width           =   1155
   End
   Begin VB.Label Label6 
      Caption         =   "Insurance Co:"
      Height          =   195
      Left            =   60
      TabIndex        =   14
      Top             =   540
      Width           =   1095
   End
   Begin VB.Label Label5 
      Caption         =   "To:"
      Height          =   195
      Left            =   4800
      TabIndex        =   12
      Top             =   1320
      Width           =   315
   End
   Begin VB.Label Label4 
      Caption         =   "From:"
      Height          =   195
      Left            =   1800
      TabIndex        =   10
      Top             =   1320
      Width           =   435
   End
   Begin VB.Label Label3 
      Caption         =   "What documents to process?"
      Height          =   195
      Left            =   60
      TabIndex        =   7
      Top             =   960
      Width           =   2295
   End
   Begin VB.Label Label2 
      Caption         =   "Environment:"
      Height          =   195
      Left            =   60
      TabIndex        =   3
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label lblStatus 
      Height          =   435
      Left            =   180
      TabIndex        =   1
      Top             =   4200
      Width           =   8535
   End
   Begin VB.Label Label1 
      Caption         =   "Status:"
      Height          =   195
      Left            =   180
      TabIndex        =   0
      Top             =   3960
      Width           =   1155
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Compare Text

Private Const MODULE_NAME As String = "frmMain"
Private m_sImageBasePath As String
Private m_objDataPresenter As Object
Private m_sSPName As String
Private m_oRSDocuments As Recordset
Private m_sExceptions As String
Private m_sErrorEmailFrom As String
Private m_oErrorEmailTo As IXMLDOMNodeList
Private m_sErrorEmailSubject As String
Private m_sMachineName As String
Private m_sCommandInsCoID As String
Private m_sCommandEnv As String
Private m_oDailyAppLogXML As New MSXML2.DOMDocument
Private m_oJournalAppLogXML As New MSXML2.DOMDocument
Private m_sSessionID As String
Private m_oSessionNode As IXMLDOMElement
Private m_sBaseLynxID As String
Private m_bError As Boolean
Private m_oDestinations As IXMLDOMNodeList

Public WithEvents PDFCreator1 As PDFCreator.clsPDFCreator
Attribute PDFCreator1.VB_VarHelpID = -1

Private ReadyState As Boolean, DefaultPrinter As String


'***************************************************************************************
' Procedure : cmbEnvironment_Click
' DateTime  : 4/19/2006 15:11
' Author    : Ramesh Vishegu
' Purpose   : Handles the stuff needed to do when the Environment combo is changed
'***************************************************************************************
'
Private Sub cmbEnvironment_Click()
          Const PROC_NAME As String = MODULE_NAME & ":cmbEnvironment_Click()"
10        On Error GoTo errHandler
          Dim aEnvs() As String
          Dim oEnv As IXMLDOMElement
20        If cmbEnvironment.Text <> "" Then
30            aEnvs = Split(cmbEnvironment.Text, ",")
              
40            Set oEnv = getConfigElement("/CEIWildCat/Environment[@id='" & Trim(aEnvs(1)) & "']")
              
50            If Not oEnv Is Nothing Then
60                Set g_objEnv = oEnv
70                g_sAPDConfigPath = getEnvSetting("APDConfigPath", "")
80                m_sImageBasePath = getEnvSetting("ImageBasePath", "")
90                m_sSPName = getEnvSetting("SPName", "")
100               m_sBaseLynxID = getEnvSetting("BaseLynxID", "")
                  
110               g_sGSExePath = getEnvSetting("GhostScript/EXEPath", "")
120               g_sGSLibPath = getEnvSetting("GhostScript/LibPath", "")
130               g_sGSFontsPath = getEnvSetting("GhostScript/FontsPath", "")
                  
                  'PDF Printer settings
140               g_sPDFPrinterName = getEnvSetting("PDFPrinter/PrinterName", "")
150               g_sPDFOutputFolder = getEnvSetting("PDFPrinter/OutputFolder", "")
                  
160               g_sPSPrinterName = getEnvSetting("PSPrinter", "")
                  
170               m_sErrorEmailFrom = getEnvSetting("Error/Email/From", "")
180               Set m_oErrorEmailTo = getEnvNodes("Error/Email/To")
190               m_sErrorEmailSubject = getEnvSetting("Error/Email/Subject", "")
                  
200               Set m_oDestinations = getEnvNodes("CEIDestination")
                  
                  'g_sSMTPServerName = getEnvSetting("CEIDestination/Email", "SMTPServer")
                  'If g_sSMTPServerName = "" Then g_sSMTPServerName = m_sMachineName

                  'load the insurance companies defined in the config
210               cmbInsuranceCo.Clear
220               loadConfigInsurance
230           End If
240       End If
errHandler:
250       Set oEnv = Nothing
260       If Err.Number <> 0 Then
270           handleError "An error occured in " & PROC_NAME & " [Line#" & CStr(Erl) & " " & Err.Description
280       End If
End Sub

Private Sub cmdClose_Click()
10        Unload Me
End Sub

'***************************************************************************************
' Procedure : cmdProcess_Click
' DateTime  : 4/19/2006 15:11
' Author    : Ramesh Vishegu
' Purpose   : Handles the stuff needed to do when the Process (Ship'em) button is clicked
'             This will start getting the documents and package them per LynxID and send
'             to CEI.
'***************************************************************************************
'
Private Sub cmdProcess_Click()
          Const PROC_NAME As String = MODULE_NAME & ":cmdProcess_Click()"
10        On Error GoTo errHandler
          
          'do validation of data
20        If opDateRange.Value = True Then
30            If IsDate(txtFrom.Text) = False Then
40                handleError "Invalid From date."
50                Exit Sub
60            End If
              
70            If IsDate(txtTo.Text) = False Then
80                handleError "Invalid To date."
90                Exit Sub
100           End If
110       ElseIf opDocumentID.Value = True Then
120           If txtDocumentID.Text = "" Or IsNumeric(txtDocumentID.Text) = False Then
130               handleError "Invalid/Missing Document ID."
140               Exit Sub
150           End If
160       ElseIf opLynxID.Value = True Then
170           If txtLynxID.Text = "" Or IsNumeric(txtLynxID.Text) = False Then
180               handleError "Invalid/Missing Lynx ID."
190               Exit Sub
200           End If
210       End If
          
220       If m_sSPName = "" Then
230           handleError "Invalid/Missing Stored proc name"
240           Exit Sub
250       End If
          
260       showStatus "Creating DataPresenter..."
          
270       enableControls False
          
280       Me.MousePointer = vbHourglass
          
          'Get the document list
290       GetDocuments
          
300       If Not m_oRSDocuments Is Nothing Then
310           If m_oRSDocuments.State = adStateOpen Then
320               If m_oRSDocuments.EOF = False Then
                      'Now package and ship the documents to CEI
330                   ShipDocuments
340               Else
350                   Trace "No documents to send."
360               End If
370           Else
380               Trace "Error retrieving the documents."
390           End If
400       Else
410           Trace "Error retrieving the documents."
420       End If
          
          'Do cleanup
430       Set g_objEvents = Nothing
440       Set m_objDataPresenter = Nothing
          
450       If opDateRange.Value = True And m_bError = False And g_blnAutoRun = True Then
              'record the last run only when the date range was selected. This will be the case when run from the
              ' windows scheduler.
460           setProfileString g_sAPPName, "LastRun", txtTo.Text, g_sAPPIniFile
470       End If
          
          'clean up all the temp files created
480       If g_objFSO.GetFolder(g_sTempPath).Files.Count > 0 Then Kill g_objFSO.BuildPath(g_sTempPath, "*.*")
490       If g_objFSO.GetFolder(g_sAttachmentsPath).Files.Count > 0 Then Kill g_objFSO.BuildPath(g_sAttachmentsPath, "*.*")
          
500       saveAppLog
          
510       showStatus "Done."
errHandler:
520       enableControls True
530       Me.MousePointer = vbDefault
540       If Err.Number <> 0 Then
550           handleError "An error occured in " & PROC_NAME & " [Line#" & CStr(Erl) & " " & Err.Description
560       End If
          
End Sub

Private Sub Form_Unload(Cancel As Integer)
10        PDFCreator1.cClose
20        Set PDFCreator1 = Nothing
30        Sleep 250
40        DoEvents
50        If Not g_objWordApp Is Nothing Then
              'quit word application without saving any changes.
60            g_objWordApp.Quit SaveChanges:=False
70        End If
80        Set g_objWordApp = Nothing
End Sub

Private Sub imgFile_Error(ByVal Number As Long, ByVal Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Integer)
10        CancelDisplay = True
End Sub

'***************************************************************************************
' Procedure : opDateRange_Click
' DateTime  : 4/19/2006 15:16
' Author    : Ramesh Vishegu
' Purpose   : Date range requires the from and to date. From date is defaulted to the
'             last run of the application and is stored in the application ini located in the
'             application path. The To date will be defaulted to the current datetime.
'***************************************************************************************
'
Private Sub opDateRange_Click()
          Dim bEnabled As Boolean
10        bEnabled = (opDateRange.Value = vbTrue)
20        txtFrom.Enabled = bEnabled
30        txtTo.Enabled = bEnabled
40        txtLynxID.Enabled = bEnabled
50        txtDocumentID.Enabled = Not bEnabled
60        txtFrom.SetFocus
End Sub

'***************************************************************************************
' Procedure : opDocumentID_Click
' DateTime  : 4/19/2006 15:17
' Author    : Ramesh Vishegu
' Purpose   : Document ID option requires the entry of DocumentID!!
'***************************************************************************************
'
Private Sub opDocumentID_Click()
          Dim bEnabled As Boolean
10        bEnabled = (opDocumentID.Value = vbTrue)
20        txtDocumentID.Enabled = bEnabled
30        txtFrom.Enabled = Not bEnabled
40        txtTo.Enabled = Not bEnabled
50        txtLynxID.Enabled = Not bEnabled
60        txtDocumentID.SetFocus
End Sub

'***************************************************************************************
' Procedure : opLynxID_Click
' DateTime  : 4/19/2006 15:18
' Author    : Ramesh Vishegu
' Purpose   : LynxID option requires a value for LynxID text box!!
'***************************************************************************************
'
Private Sub opLynxID_Click()
          Dim bEnabled As Boolean
10        bEnabled = (opLynxID.Value = vbTrue)
20        txtLynxID.Enabled = bEnabled
30        txtFrom.Enabled = bEnabled
40        txtTo.Enabled = bEnabled
50        txtDocumentID.Enabled = Not bEnabled
60        txtLynxID.SetFocus
End Sub

'***************************************************************************************
' Procedure : tmrStart_Timer
' DateTime  : 4/19/2006 15:18
' Author    : Ramesh Vishegu
' Purpose   : This timer allows the application to run and refresh itself before doing
'             anything else. Initialize the app. Also this prepares the required folders:
'             AppPath\temp contains the documents for pdf conversion
'             AppPath\attachments contains the documents for a particular LynxID and these
'               will be packaged into one email.
'             These files will be cleared as needed.
'***************************************************************************************
'
Private Sub tmrStart_Timer()
10        On Error GoTo errHandler
          Const PROC_NAME As String = "tmrStart_Timer()"
          'disable the timer
20        tmrStart.Enabled = False
30        g_sTempPath = g_objFSO.BuildPath(App.Path, "temp")
40        g_sAttachmentsPath = g_objFSO.BuildPath(App.Path, "attachments")
50        g_sLogsPath = g_objFSO.BuildPath(App.Path, "logs")
          
60        If g_objFSO.FolderExists(g_sTempPath) = False Then
70            If ForceBuildPath(g_sTempPath) = False Then
80                g_sTempPath = App.Path
90            End If
100       End If
          
110       If g_objFSO.FolderExists(g_sAttachmentsPath) = False Then
120           If ForceBuildPath(g_sAttachmentsPath) = False Then
130               g_sAttachmentsPath = App.Path
140           End If
150       End If
          
160       If g_objFSO.FolderExists(g_sLogsPath) = False Then
170           If ForceBuildPath(g_sLogsPath) = False Then
180               g_sLogsPath = App.Path
190           End If
200       End If
          
210       g_sDailyLogFile = g_objFSO.BuildPath(g_sLogsPath, "Daily Transmission " & Format(Now, "yyyy-mm-dd") & ".xml")
220       g_sJournalLogFile = g_objFSO.BuildPath(g_sLogsPath, "CEI Wildcat Transmission Journal.xml")
          
230       m_sSessionID = Format(Now, "yyyymmddhhnn")
          
240       g_sAPPIniFile = g_objFSO.BuildPath(App.Path, g_sAPPName & ".ini")

250       loadConfig
          
260       initApp
          
270       If g_blnAutoRun = True Then
280           cmdProcess_Click
290           Unload Me
300       End If
errHandler:
310       If Err.Number <> 0 Then
320           handleError "An error occured in " & PROC_NAME & " [Line#" & CStr(Erl) & " " & Err.Description
330       End If
                
End Sub

'***************************************************************************************
' Procedure : initApp
' DateTime  : 4/19/2006 15:24
' Author    : Ramesh Vishegu
' Purpose   : Initialize the application
'***************************************************************************************
'
Private Sub initApp()
          Const PROC_NAME As String = MODULE_NAME & ":initApp()"
          
10        On Error GoTo errHandler
          
          Dim oEnvs As IXMLDOMNodeList
          Dim oEnv As IXMLDOMElement
          Dim sPrevRunDateTime As String
          Dim dtPrevRun As Date
          Dim sCommands As String
          Dim aCmdParms() As String
          Dim i As Integer
          Dim iCommandEnvListIdx As Integer
          
20        sCommands = Command()
          
30        If sCommands <> "" Then
              'parse the command line and extract info
40            aCmdParms = Split(LCase(sCommands), " ")
50            For i = 0 To UBound(aCmdParms)
60                Select Case Left(aCmdParms(i), 2)
                      Case "-e":
70                        m_sCommandEnv = Mid(aCmdParms(i), 3)
80                    Case "-i":
90                        m_sCommandInsCoID = Mid(aCmdParms(i), 3)
100               End Select
110           Next
120       End If
          
          'Now preselect the environment based on the machine name
130       m_sMachineName = GetWinComputerName()
          
140       iCommandEnvListIdx = -1
          'load the environments
150       cmbEnvironment.Clear
160       Set oEnvs = getConfigNodes("/CEIWildCat/Environment")
170       If Not oEnvs Is Nothing Then
180           For Each oEnv In oEnvs
190               cmbEnvironment.AddItem oEnv.getAttribute("name") & ", " & oEnv.getAttribute("id"), cmbEnvironment.ListCount
200               If m_sCommandEnv <> "" Then
210                   If oEnv.getAttribute("id") = m_sCommandEnv Then
                          'the last added item
220                       iCommandEnvListIdx = cmbEnvironment.ListCount - 1
230                   End If
240               End If
250           Next
260       End If
          
270       If m_sCommandEnv <> "" Then
              'select the environment that was sent from the command line
280           If iCommandEnvListIdx > -1 Then cmbEnvironment.ListIndex = iCommandEnvListIdx
290       Else
300           Set oEnv = getConfigElement("/CEIWildCat/Environment[Machine/@name='" & m_sMachineName & "']")
              
310           If Not oEnv Is Nothing Then
320               selectComboItem cmbEnvironment, oEnv.getAttribute("name") & ", " & oEnv.getAttribute("id")
330           End If
340       End If
          
          'load the previous run information
350       sPrevRunDateTime = getProfileString(g_sAPPName, "LastRun", g_sAPPIniFile)
          
360       If sPrevRunDateTime = "" Then
370           dtPrevRun = DateAdd("h", -1, Now)
380       Else
390           dtPrevRun = CDate(sPrevRunDateTime)
400       End If
          
410       txtFrom.Text = FormatDateTime(dtPrevRun, vbGeneralDate)
420       txtTo.Text = FormatDateTime(Now, vbGeneralDate)

          'initialize the transmission logs
430       m_oDailyAppLogXML.async = False
440       m_oJournalAppLogXML.async = False
          
          'Create the DataPresenter object
450       Set m_objDataPresenter = CreateObjectEx("DataPresenter.CExecute")
460       If m_objDataPresenter Is Nothing Then
470           handleError Err.Description
480           End
490       End If
          
          'initialize the data presenter
500       Set g_objEvents = m_objDataPresenter.mToolkit.mEvents
510       g_objEvents.ComponentInstance = "CEI Document Shipper"
520       m_objDataPresenter.Initialize g_sAPDConfigPath
530       g_blnDebugMode = g_objEvents.IsDebugMode
          
540       Me.Caption = Me.Caption & " [Proxy:" & g_objEvents.mSettings.GetParsedSetting("@EnvironmentCode") & "]"
          
550       loadAppLog
          
          'init PDFCreator
560       Set PDFCreator1 = New clsPDFCreator
570       With PDFCreator1
580           If .cStart("/NoProcessingAtStartup") = False Then
                  'Exit Sub
590           End If
600           .cVisible = False
610       End With
          
620       If sCommands <> "" And cmbEnvironment.ListIndex > -1 And cmbInsuranceCo.ListIndex > -1 Then
630           g_blnAutoRun = True
640       End If
errHandler:
650       Set oEnv = Nothing
660       Set oEnvs = Nothing
670       If Err.Number <> 0 Then
680           handleError "An error occured in " & PROC_NAME & " [Line#" & CStr(Erl) & " " & Err.Description
690       End If
End Sub

'***************************************************************************************
' Procedure : loadConfigInsurance
' DateTime  : 4/19/2006 15:24
' Author    : Ramesh Vishegu
' Purpose   : Load the insurance companies from the application config
'***************************************************************************************
'
Private Sub loadConfigInsurance()
          Const PROC_NAME As String = MODULE_NAME & ":loadConfigInsurance()"
10        On Error GoTo errHandler
          Dim oInsList As IXMLDOMNodeList
          Dim oIns As IXMLDOMElement
          Dim iCommandInsIdx As String
20        If Not g_objEnv Is Nothing Then
30            iCommandInsIdx = -1
40            Set oInsList = getEnvNodes("Insurance") 'g_objEnv.selectNodes("Insurance")
              
50            If Not oInsList Is Nothing Then
60                cmbInsuranceCo.AddItem "All"
                  
70                For Each oIns In oInsList
80                    cmbInsuranceCo.AddItem oIns.getAttribute("name") & ", " & oIns.getAttribute("id")
90                    If oIns.getAttribute("id") = m_sCommandInsCoID Then iCommandInsIdx = cmbInsuranceCo.ListCount - 1
100               Next
                  
110               If m_sCommandInsCoID <> "" Then
120                   If iCommandInsIdx > -1 Then cmbInsuranceCo.ListIndex = iCommandInsIdx
130               Else
                      'default select the "All"
140                   cmbInsuranceCo.ListIndex = 0
150               End If
160           End If
170       End If
errHandler:
180       Set oIns = Nothing
190       Set oInsList = Nothing
200       If Err.Number <> 0 Then
210           handleError "An error occured in " & PROC_NAME & " [Line#" & CStr(Erl) & " " & Err.Description
220       End If
End Sub

'***************************************************************************************
' Procedure : selectComboItem
' DateTime  : 4/19/2006 15:24
' Author    : Ramesh Vishegu
' Purpose   : General utility to select an item in the combobox based on the text
'***************************************************************************************
'
Private Sub selectComboItem(ByRef cmbObject As ComboBox, ByVal strListItem As String)
          Dim i As Integer
          
10        For i = 0 To cmbObject.ListCount - 1
20            If cmbObject.List(i) = strListItem Then
30                cmbObject.ListIndex = i
40                Exit For
50            End If
60        Next
End Sub

'***************************************************************************************
' Procedure : GetDocuments
' DateTime  : 4/19/2006 15:27
' Author    : Ramesh Vishegu
' Purpose   : Retrieve the documents based on the user input
'***************************************************************************************
'
Private Sub GetDocuments()
          Const PROC_NAME As String = MODULE_NAME & ":GetDocuments()"
10        On Error GoTo errHandler
          Dim sDocumentTypes As String
          Dim sInsuranceCompanyIDList As String
          Dim aInsCoText() As String
          Dim i As Integer
          Dim sParams As String
          Dim itmx As ListItem
          Dim dtFrom As Date
          Dim dtTo As Date
          Dim oDestination As IXMLDOMElement
          
20        showStatus "Retrieving documents..."
          
          'read the document types we need from the config
30        sDocumentTypes = ""
40        For Each oDestination In m_oDestinations
50            sDocumentTypes = sDocumentTypes & "," & oDestination.getAttribute("documentIDs")
60        Next
          'sDocumentTypes = getEnvSetting("DocumentIDs", "")
          
70        Trace "Environment: " & cmbEnvironment.Text & vbCrLf & _
                "  Insurance:   " & cmbInsuranceCo.Text
          
          'prepare the list of insurance companies (comma delimited) we are going to look at
80        If cmbInsuranceCo.Text = "All" Then
              'make a comma delimited Ins. Co. id
90            For i = 1 To cmbInsuranceCo.ListCount - 1
100               aInsCoText = Split(cmbInsuranceCo.List(i), ",")
110               sInsuranceCompanyIDList = sInsuranceCompanyIDList & Trim(aInsCoText(1)) & ","
120           Next
130       Else
140           aInsCoText = Split(cmbInsuranceCo.Text, ",")
150           sInsuranceCompanyIDList = Trim(aInsCoText(1))
160       End If
          
170       dtFrom = CDate(txtFrom.Text)
180       dtTo = CDate(txtTo.Text)
          
          'prepare the stored proc parameters based on user selection
190       If opDateRange.Value = True Then
200           sParams = "InsuranceCompanyIDList=" & sInsuranceCompanyIDList & _
                        "&FromDate=" & Format(dtFrom, "mm/dd/yyyy hh:nn:ss") & "&ToDate=" & Format(dtTo, "mm/dd/yyyy hh:nn:ss") & _
                        "&DocumentTypeList=" & sDocumentTypes & _
                        "&BaseLynxID=" & m_sBaseLynxID
                        
210           Trace "From Date: " & txtFrom.Text & vbCrLf & _
                    "  To date: " & txtTo.Text & vbCrLf & _
                    "   Params: " & sParams
                    
220           If Not m_oSessionNode Is Nothing Then m_oSessionNode.setAttribute "runAs", "range: " & Format(dtFrom, "mm-dd-yyyy hh:nn:ss") & " to " & Format(dtTo, "mm-dd-yyyy hh:nn:ss")
230       ElseIf opDocumentID.Value = True Then
240           sParams = "DocumentID=" & txtDocumentID.Text & _
                        "&BaseLynxID=" & m_sBaseLynxID
              
250           Trace "DocumentID: " & txtDocumentID.Text & vbCrLf & _
                    "   Params: " & sParams
                    
260           If Not m_oSessionNode Is Nothing Then m_oSessionNode.setAttribute "runAs", "DocumentID:" & txtDocumentID.Text
                    
270       ElseIf opLynxID.Value = True Then
280           sParams = "LynxID=" & txtLynxID.Text & _
                        "&DocumentTypeList=" & sDocumentTypes & _
                        "&BaseLynxID=" & m_sBaseLynxID

290           Trace "LynxID: " & txtLynxID.Text & vbCrLf & _
                    "   Params: " & sParams
                    
300           If Not m_oSessionNode Is Nothing Then m_oSessionNode.setAttribute "runAs", "LynxID:" & txtLynxID.Text
                    
310       End If
          
          'now get the data from the database
320       Set m_oRSDocuments = m_objDataPresenter.ExecuteSpNpAsRS(m_sSPName, sParams)
          
          'initialize listview
330       lvDocuments.ListItems.Clear
          
340       If m_oRSDocuments.State = adStateOpen Then
              'default sort order
350           m_oRSDocuments.Sort = "InsuranceCompanyID desc, LynxID asc, CreatedDate asc"
              
360           If g_blnAutoRun = False Then 'interactive mode. so load the documents onto the listview
370               With lvDocuments.ListItems
380                   While Not m_oRSDocuments.EOF
390                       If IsNull(m_oRSDocuments.Fields("ImageLocation").Value) = False Then
400                           Set itmx = .Add(, , m_oRSDocuments.Fields("DocumentID").Value)
410                           itmx.SubItems(1) = m_oRSDocuments.Fields("InsuranceCompanyName").Value
420                           itmx.SubItems(2) = FormatDateTime(m_oRSDocuments.Fields("CreatedDate").Value, vbGeneralDate)
430                           itmx.SubItems(3) = g_objFSO.BuildPath(m_sImageBasePath, Replace(m_oRSDocuments.Fields("ImageLocation").Value, "\\", "\"))
440                           itmx.SubItems(4) = m_oRSDocuments.Fields("DocumentName").Value
450                       End If
460                       m_oRSDocuments.MoveNext
470                   Wend
                      
480                   If m_oRSDocuments.RecordCount > 0 Then m_oRSDocuments.MoveFirst
490               End With
500           End If
510       End If
errHandler:
520       Set itmx = Nothing
530       If Err.Number <> 0 Then
540           handleError "An error occured in " & PROC_NAME & " [Line#" & CStr(Erl) & " " & Err.Description
550       End If
End Sub

'***************************************************************************************
' Procedure : showStatus
' DateTime  : 4/19/2006 15:33
' Author    : Ramesh Vishegu
' Purpose   : Show status messages
'***************************************************************************************
'
Private Sub showStatus(strStatus As String)
          'If g_blnAutoRun = False Then 'interactive mode. so show the application status
10            lblStatus.Caption = strStatus
20            lblStatus.Refresh
          'End If
          
30        DoEvents
End Sub

'***************************************************************************************
' Procedure : ShipDocuments
' DateTime  : 4/19/2006 15:33
' Author    : Ramesh Vishegu
' Purpose   : Package the documents per LynxID and email to CEI
'***************************************************************************************
'
Private Sub ShipDocuments()
          Const PROC_NAME As String = MODULE_NAME & ":ShipDocuments()"
10        On Error GoTo errHandler
          Dim lDocumentID As Long
          Dim dtCreatedDate As Date
          Dim sImageLocation As String
          Dim iSupplementSeqNumber As Integer
          Dim lLynxID As Long
          Dim sClaimNumber As String
          
          Dim i As Integer
          Dim iAttachCount As Integer
          Dim iImageCount As Integer
          
          Dim aAttachments() As String
          
          Dim blnEmail As Boolean
          Dim sEmailFrom As String
          Dim oRecipients As IXMLDOMNodeList
          Dim oRecipient As IXMLDOMNode
          Dim sEmailReplyTo As String
          Dim sEmailSubject As String
          Dim sEmailSubjectResolved As String
          Dim sEmailImportance As String
          Dim sEmailBody As String
          Dim sEmailMaxSize As String
          
          Dim oDistinctLynxIDs As New Scripting.Dictionary
          Dim sFileExt As String
          Dim blnConvert As Boolean
          Dim sTempImageLocation As String
          
          Dim oXML As New MSXML2.DOMDocument
          Dim oTxtFile As TextStream
          Dim sPDFOutputFile As String
          Dim oPDFOutputFolder As Scripting.Folder
          Dim sDocumentDesc As String
          
          Dim oDestination As IXMLDOMElement
          Dim sDestinationDocTypeIDs As String
          Dim aDocumentTypeID() As String
          Dim oDocumentTypeID As New Scripting.Dictionary
          Dim sFilter As String
          Dim iDocCount As Integer
          Dim iDocumentTypeID As Integer
          
          Const MAX_PRINT_WAIT As Integer = 20
          Const MAX_DELETE_WAIT As Integer = 20
          Dim iWaitCounter As Integer
          Dim bPDFPrintOut As Boolean
          
20        If Not m_oRSDocuments Is Nothing Then
30              If m_oRSDocuments.State = adStateOpen Then
                
                    'move to the first record
40                  m_oRSDocuments.MoveFirst
                    
                    'load the distinct LynxIDs
50                  oDistinctLynxIDs.RemoveAll
60                  oDistinctLynxIDs.CompareMode = TextCompare
                    
70                  While Not m_oRSDocuments.EOF
80                      lLynxID = m_oRSDocuments.Fields("LynxID").Value
                        
90                      If Not oDistinctLynxIDs.Exists(lLynxID) Then oDistinctLynxIDs.Add lLynxID, CStr(lLynxID)
                        
100                     m_oRSDocuments.MoveNext
110                 Wend
                    
120                 oXML.async = False
                    
130                 m_sExceptions = ""
                    
140                 Trace "Number of distinct LynxIDs to process: " & CStr(oDistinctLynxIDs.Count)
                    
150                 For i = 0 To oDistinctLynxIDs.Count - 1
160                     For Each oDestination In m_oDestinations
170                         sDestinationDocTypeIDs = oDestination.getAttribute("documentIDs")
180                         aDocumentTypeID = Split(sDestinationDocTypeIDs, ",")
190                         oDocumentTypeID.RemoveAll
200                         For iDocCount = LBound(aDocumentTypeID) To UBound(aDocumentTypeID)
210                           If oDocumentTypeID.Exists(aDocumentTypeID(iDocCount)) = False Then
220                               If Trim(aDocumentTypeID(iDocCount)) <> "" And IsNumeric(aDocumentTypeID(iDocCount)) Then
230                                   oDocumentTypeID.Add CInt(aDocumentTypeID(iDocCount)), CInt(aDocumentTypeID(iDocCount))
240                               End If
250                           End If
260                         Next
                            
                            'delete all temp files from the previous temporary files
270                         If g_objFSO.GetFolder(g_sTempPath).Files.Count > 0 Then Kill g_objFSO.BuildPath(g_sTempPath, "*.*")
280                         If g_objFSO.GetFolder(g_sAttachmentsPath).Files.Count > 0 Then Kill g_objFSO.BuildPath(g_sAttachmentsPath, "*.*")
                            
290                         lLynxID = oDistinctLynxIDs.Items(i)
                            
                            'filter the documents for the current LynxID
300                         m_oRSDocuments.Filter = "LynxID = " & CStr(lLynxID)
                            
                            'initialize the run for current LynxID
310                         ReDim aAttachments(m_oRSDocuments.RecordCount)
320                         iAttachCount = 0
330                         iImageCount = 0
                            
                            'load the email settings
340                         blnEmail = (oDestination.selectSingleNode("Email/@enabled").Text = "true")
350                         sEmailFrom = oDestination.selectSingleNode("Email/From").Text
360                         Set oRecipients = oDestination.selectNodes("Email/Recipients/Recipient")
370                         sEmailReplyTo = oDestination.selectSingleNode("Email/ReplyTo").Text
380                         sEmailSubject = oDestination.selectSingleNode("Email/Subject").Text
390                         sEmailImportance = oDestination.selectSingleNode("Email/Importance").Text
400                         sEmailBody = oDestination.selectSingleNode("Email/Body").Text
410                         sEmailMaxSize = oDestination.selectSingleNode("Email/@maxSize").Text
                              
420                         g_sSMTPServerName = oDestination.selectSingleNode("Email/@SMTPserver").Text
430                         If g_sSMTPServerName = "" Then g_sSMTPServerName = m_sMachineName
                              
440                         Trace "Ship by Email: " & CStr(blnEmail)
                              
450                         If blnEmail Then
460                             Trace "Email Settings" & vbCrLf & _
                                      "  ------------------" & vbCrLf & _
                                      "  Server: " & g_sSMTPServerName & vbCrLf & _
                                      "  From: " & sEmailFrom & vbCrLf & _
                                      "  Reply to: " & sEmailReplyTo & vbCrLf & _
                                      "  Subject: " & sEmailSubject & vbCrLf & _
                                      "  Body: " & sEmailBody
470                         End If
                              
                            'Trace "Processing LynxID: " & CStr(lLynxID) & ". Number of attachments: " & CStr(m_oRSDocuments.RecordCount)
                            
480                         While Not m_oRSDocuments.EOF
490                             If IsNull(m_oRSDocuments.Fields("ImageLocation").Value) = True Then GoTo nextDoc
          
500                             lDocumentID = m_oRSDocuments.Fields("DocumentID").Value
510                             iDocumentTypeID = m_oRSDocuments.Fields("DocumentTypeID").Value
                                
                                'if the current document does not correspond to the current destination then skip it. It will be handled in
                                '  subsequent destination loop
520                             If oDocumentTypeID.Exists(iDocumentTypeID) = False Then GoTo nextDoc
                                
530                             sImageLocation = g_objFSO.BuildPath(m_sImageBasePath, Replace(m_oRSDocuments.Fields("ImageLocation").Value, "\\", "\"))
540                             If IsNull(m_oRSDocuments.Fields("SupplementSeqNumber").Value) = False Then
550                               iSupplementSeqNumber = m_oRSDocuments.Fields("SupplementSeqNumber").Value
560                             Else
570                               iSupplementSeqNumber = 0
580                             End If
590                             sClaimNumber = m_oRSDocuments.Fields("ClaimNumber").Value
                                
                                'check the journal if this document was already sent
600                             If chkSendAgain.Value = vbUnchecked Then
610                                 If Not m_oJournalAppLogXML.selectSingleNode("//Document[@documentID='" & CStr(lDocumentID) & "']") Is Nothing Then
620                                     Trace "DocumentID: " & CStr(lDocumentID) & " already sent. Skipping this file."
630                                     GoTo nextDoc
640                                 End If
650                             End If
                                
                                
660                             sFileExt = LCase(g_objFSO.GetExtensionName(sImageLocation))
670                             iWaitCounter = MAX_DELETE_WAIT
                                
                                'delete all temp files from the previous session
680                             On Error GoTo errDelete
690                             If g_objFSO.GetFolder(g_sPDFOutputFolder).Files.Count > 0 Then Kill g_objFSO.BuildPath(g_sPDFOutputFolder, "*.*")
errDelete:
700                             If Err.Number <> 0 Then
710                               If iWaitCounter > 0 Then
720                                   Sleep 1000
730                                   iWaitCounter = iWaitCounter - 1
740                                   Resume
750                               Else
760                                   GoTo errHandler
770                               End If
780                             End If
                                
                                'check for the existance of the document
790                             If g_objFSO.FileExists(sImageLocation) = False Then
800                                 m_sExceptions = m_sExceptions & "Document " & sImageLocation & " does not exist. Document id: " & CStr(lDocumentID) & vbCrLf
810                                 Trace "Document " & sImageLocation & " does not exist."
820                                 GoTo nextDoc
830                             End If
                                
                                'copy the document from the APD storage to the temp location
840                             sTempImageLocation = g_objFSO.BuildPath(g_sTempPath, g_objFSO.GetBaseName(sImageLocation) & "." & sFileExt)
                                
850                             g_objFSO.CopyFile sImageLocation, sTempImageLocation
                                
860                             sImageLocation = sTempImageLocation
                                
                                'read from the config if the current file type need conversion to pdf
870                             blnConvert = (getEnvSetting("DocumentConverter/FileExtension[@extension='" & sFileExt & "']", "convert") = "true")
                                
880                             bPDFPrintOut = False
                                
890                             If blnConvert Then 'need to convert to pdf
900                                 showStatus "Converting " & sImageLocation & " to pdf ..."
910                                 Select Case sFileExt
                                        Case "doc":
920                                         bPDFPrintOut = True
930                                         If Word2PDF(sImageLocation) = False Then
                                                'Err.Raise vbError + 106, PROC_NAME, "Error converting Word document to pdf"
940                                             m_sExceptions = m_sExceptions & "Error converting document to pdf. " & g_objFSO.BuildPath(m_sImageBasePath, Replace(m_oRSDocuments.Fields("ImageLocation").Value, "\\", "\")) & ". DocumentID:" & CStr(lDocumentID) & vbCrLf
950                                             Trace "Error converting document to pdf. " & g_objFSO.BuildPath(m_sImageBasePath, Replace(m_oRSDocuments.Fields("ImageLocation").Value, "\\", "\")) & ". DocumentID:" & CStr(lDocumentID)
960                                             GoTo nextDoc
970                                         End If
980                                     Case "xml":
990                                         bPDFPrintOut = True
1000                                        If XML2PDF(sImageLocation) = False Then
1010                                            m_sExceptions = m_sExceptions & "Error converting document to pdf. " & g_objFSO.BuildPath(m_sImageBasePath, Replace(m_oRSDocuments.Fields("ImageLocation").Value, "\\", "\")) & ". DocumentID:" & CStr(lDocumentID) & vbCrLf
1020                                            Trace "Error converting document to pdf. " & g_objFSO.BuildPath(m_sImageBasePath, Replace(m_oRSDocuments.Fields("ImageLocation").Value, "\\", "\")) & ". DocumentID:" & CStr(lDocumentID)
1030                                            GoTo nextDoc
1040                                        End If
1050                                    Case "jpg", "bmp", "tif", "gif":
1060                                        iImageCount = iImageCount + 1
1070                                        If Image2PDF(sImageLocation) = False Then
1080                                            m_sExceptions = m_sExceptions & "Error converting document to pdf. " & g_objFSO.BuildPath(m_sImageBasePath, Replace(m_oRSDocuments.Fields("ImageLocation").Value, "\\", "\")) & ". DocumentID:" & CStr(lDocumentID) & vbCrLf
1090                                            Trace "Error converting document to pdf. " & g_objFSO.BuildPath(m_sImageBasePath, Replace(m_oRSDocuments.Fields("ImageLocation").Value, "\\", "\")) & ". DocumentID:" & CStr(lDocumentID)
1100                                            GoTo nextDoc
1110                                        End If
1120                                    Case Else:
                                            'file type not supportted. Need to send as-is
1130                                        Trace "Document conversion not supported. " & g_objFSO.BuildPath(m_sImageBasePath, Replace(m_oRSDocuments.Fields("ImageLocation").Value, "\\", "\")) & ". DocumentID:" & CStr(lDocumentID)
1140                                        showStatus "Document [" & sImageLocation & "] not supported. Skipping convertion to pdf ..."
1150                                        GoTo noConvert
1160                                  End Select
                                    
1170                                  If bPDFPrintOut = True Then
1180                                      With PDFCreator1
1190                                         While .cPrinterStop = False Or .cCountOfPrintjobs > 0
1200                                            Sleep 1000
1210                                            DoEvents
1220                                         Wend
1230                                      End With
1240                                  End If
                                    
1250                                Set oPDFOutputFolder = g_objFSO.GetFolder(g_sPDFOutputFolder)
                                    
                                    ' the document was printed to the PDF Printer. We expect only one file in the output folder
1260                                If oPDFOutputFolder.Files.Count = 1 Then
1270                                    sPDFOutputFile = g_objFSO.BuildPath(g_sPDFOutputFolder, Dir(g_objFSO.BuildPath(g_sPDFOutputFolder, "*.pdf")))
1280                                    If g_objFSO.FileExists(sPDFOutputFile) Then
                                            'rename the file to a meaning full name. Use the Document type name. Example: Estimate, Supplement-1, Photograph-1
1290                                        If m_oRSDocuments.Fields("DocumentName").Value = "Supplement" Then
                                                'append the supplement sequence number
1300                                            sDocumentDesc = m_oRSDocuments.Fields("DocumentName").Value & "-" & m_oRSDocuments.Fields("SupplementSeqNumber").Value & ".pdf"
1310                                        Else
1320                                            Select Case sFileExt
                                                    Case "jpg", "bmp", "tif", "gif":
                                                        'append the image count
1330                                                    sDocumentDesc = m_oRSDocuments.Fields("DocumentName").Value & "-" & iImageCount & ".pdf"
1340                                                Case Else
1350                                                    sDocumentDesc = m_oRSDocuments.Fields("DocumentName").Value & ".pdf"
1360                                            End Select
1370                                        End If
                                            
                                            'get the distinct name. Sometime we may have two Supplement-1. One from CCC and the other from
                                            '  user upload.
1380                                        sDocumentDesc = getDistinctFileName(g_sAttachmentsPath, sDocumentDesc)
                                            
                                            'now move the file to the attachments folder
1390                                        g_objFSO.MoveFile sPDFOutputFile, sDocumentDesc
                                            
                                            'record the attachment
1400                                        aAttachments(iAttachCount) = sDocumentDesc
1410                                    Else
1420                                        m_sExceptions = m_sExceptions & "PDF Printer did not produce an output. Skipping " & g_objFSO.BuildPath(m_sImageBasePath, Replace(m_oRSDocuments.Fields("ImageLocation").Value, "\\", "\")) & ". DocumentID:" & CStr(lDocumentID) & vbCrLf
1430                                        Trace "PDF Printer did not produce a PDF File. DocumentID:" & CStr(lDocumentID)
1440                                        GoTo nextDoc
1450                                    End If
1460                                Else
                                        'multiple files exist which might not be from the current process. Some other process is printing to the pdf printer
1470                                    Err.Raise vbError + 103, "ShipDocuments", "Multiple files exist in the PDF output folder. Expecting one file"
1480                                End If
1490                                Set oPDFOutputFolder = Nothing
                                    
1500                            Else
noConvert:
1510                                If m_oRSDocuments.Fields("DocumentName").Value = "Supplement" Then
                                        'append the supplement sequence number
1520                                    sDocumentDesc = m_oRSDocuments.Fields("DocumentName").Value & "-" & m_oRSDocuments.Fields("SupplementSeqNumber").Value & "." & sFileExt
1530                                Else
1540                                    Select Case sFileExt
                                            Case "jpg", "bmp", "tif", "gif":
                                                'append the image count
1550                                            iImageCount = iImageCount + 1
1560                                            sDocumentDesc = m_oRSDocuments.Fields("DocumentName").Value & "-" & iImageCount & "." & sFileExt
1570                                            If FileLen(sImageLocation) > 200000 Then
1580                                              showStatus "Reducing image size " & sImageLocation & "..."
1590                                              If ReduceImageSize(sImageLocation) = False Then
1600                                                  m_sExceptions = m_sExceptions & "Error reducing image size. " & g_objFSO.BuildPath(m_sImageBasePath, Replace(m_oRSDocuments.Fields("ImageLocation").Value, "\\", "\")) & ". DocumentID:" & CStr(lDocumentID) & vbCrLf
1610                                                  Trace "Error reducing image size. " & g_objFSO.BuildPath(m_sImageBasePath, Replace(m_oRSDocuments.Fields("ImageLocation").Value, "\\", "\")) & ". DocumentID:" & CStr(lDocumentID)
1620                                                  GoTo nextDoc
1630                                              End If
1640                                            End If
1650                                        Case Else
1660                                            sDocumentDesc = m_oRSDocuments.Fields("DocumentName").Value & "." & sFileExt
1670                                    End Select
1680                                End If
                                    
                                    'get the distinct name. Sometime we may have two Supplement-1. One from CCC and the other from
                                    '  user upload.
1690                                sDocumentDesc = getDistinctFileName(g_sAttachmentsPath, sDocumentDesc)
                                    
                                    'now move the file to the attachments folder
1700                                g_objFSO.MoveFile sImageLocation, sDocumentDesc
                                    
                                    'record the attachment
1710                                aAttachments(iAttachCount) = sDocumentDesc
                                    
1720                            End If
                                
1730                            Trace "Attachment " & m_oRSDocuments.Fields("ImageLocation").Value & " -> " & sDocumentDesc
                                
1740                            iAttachCount = iAttachCount + 1
                                
nextDoc:
                                
1750                            m_oRSDocuments.MoveNext
1760                        Wend
                            
                            
                            'now we have all the attachments for the current LynxID. Ship the attachments to CEI
                            'If getEnvSetting("CEIDestination/Email", "enabled") = "true" And Join(aAttachments, "") <> "" Then
1770                        If Join(aAttachments, "") <> "" Then
1780                            showStatus "Sending email. Lynx id:" & CStr(lLynxID)
1790                            m_oRSDocuments.MoveFirst
                                
                                'resolve the subject line
1800                            sEmailSubjectResolved = resolveMacros(sEmailSubject, m_oRSDocuments)
                                
1810                            Trace "Email subject (after macro resolve): " & sEmailSubjectResolved
                                
1820                            Trace "Number of recipients: " & CStr(oRecipients.Length)
                                
1830                            If oRecipients.Length > 0 Then
                                    'send email
1840                                If sendEmail(sEmailFrom, oRecipients, sEmailSubjectResolved, sEmailReplyTo, _
                                              sEmailImportance, sEmailBody, aAttachments, CLng(sEmailMaxSize)) Then
                                      'add the stuff to the daily log and journal
1850                                  m_oRSDocuments.MoveFirst
1860                                  add2AppLog m_oRSDocuments
1870                                  saveAppLog
1880                                End If
1890                            End If
                                
                                
1900                        End If
1910                    Next
1920                Next
1930          End If
1940      End If
          
1950      If m_sExceptions <> "" Then
              'some exceptions were generated. Report this to APD IT group
1960          ReDim aAttachments(0)
1970          sendEmail m_sErrorEmailFrom, m_oErrorEmailTo, "CEI - Documents sent with Exceptions", "", "", _
                      "Documents were send to CEI with the following exceptions:" & vbCrLf & m_sExceptions, aAttachments
1980      End If
          
errHandler:
1990      Set oXML = Nothing
2000      Set oDistinctLynxIDs = Nothing
2010      Set oRecipient = Nothing
2020      Set oRecipients = Nothing
2030      Set oPDFOutputFolder = Nothing
          
2040      If Err.Number <> 0 Then
2050          handleError "An error occured in " & PROC_NAME & " [Line#" & CStr(Erl) & " " & Err.Description
2060      End If
End Sub

'***************************************************************************************
' Procedure : resolveMacros
' DateTime  : 4/19/2006 15:56
' Author    : Ramesh Vishegu
' Purpose   : Resolve any macros in a string. Macros look like $MacroName$ and MacroNames
'             correspond to a field in the recordset
'***************************************************************************************
'
Private Function resolveMacros(strText As String, objRS As Recordset) As String
          Const PROC_NAME As String = MODULE_NAME & ":resolveMacros()"
10        On Error GoTo errHandler
          Dim sRet As String
          Dim sMacroName As String
          Dim iStart As Integer
          Dim iEnd As Integer
          
20        If strText <> "" Then
30            sRet = strText
40            iStart = InStr(1, sRet, "$")
50            While iStart > 0
                  'macro may exist. try to get them and resolve
60                iEnd = InStr(iStart + 1, sRet, "$")
70                sMacroName = Mid(sRet, iStart + 1, iEnd - iStart - 1)
                  
80                sRet = Replace(sRet, "$" & sMacroName & "$", getMacroValue(sMacroName, objRS))
                  
90                iStart = InStr(1, sRet, "$")
100           Wend
110       End If
          
120       resolveMacros = sRet
errHandler:
130       If Err.Number <> 0 Then
140           handleError "An error occured in " & PROC_NAME & " [Line#" & CStr(Erl) & " " & Err.Description
150       End If
End Function

'***************************************************************************************
' Procedure : getMacroValue
' DateTime  : 4/19/2006 16:39
' Author    : Ramesh Vishegu
' Purpose   : Retrieve the field value based on the macro name (field name)
'***************************************************************************************
'
Private Function getMacroValue(strMacroName As String, objRS As Recordset) As String
          Const PROC_NAME As String = MODULE_NAME & ":getMacroValue()"
10        On Error GoTo errHandler
          Dim sRet As String
          Dim i As Integer
          Dim oField As ADODB.Field
          
20        For Each oField In objRS.Fields
30            If oField.Name = strMacroName Then
40                If oField.Type = adDBTimeStamp Then
50                    sRet = Format(oField.Value, "mm-dd-yyyy")
60                Else
70                    sRet = CStr(oField.Value)
80                End If
90                Exit For
100           End If
110       Next
120       getMacroValue = sRet
errHandler:
130       If Err.Number <> 0 Then
140           handleError "An error occured in " & PROC_NAME & " [Line#" & CStr(Erl) & " " & Err.Description
150       End If
End Function

'***************************************************************************************
' Procedure : getDistinctFileName
' DateTime  : 4/19/2006 16:40
' Author    : Ramesh Vishegu
' Purpose   : Gets a distinct file name. If a file with the same name exist in the
'             destination folder, this proc will keep appending -<counter> to the base
'             file name until it is not found.
'***************************************************************************************
'
Private Function getDistinctFileName(strFolderPath As String, strFileName As String) As String
          Const PROC_NAME As String = MODULE_NAME & ":getDistinctFileName()"
          Dim i As Integer
          Dim sRet As String
          Dim sBaseFileName As String
          Dim sFileExt As String
          
10        i = 0
          
20        sRet = g_objFSO.BuildPath(strFolderPath, strFileName)
          
30        sBaseFileName = g_objFSO.GetBaseName(strFileName)
40        sFileExt = g_objFSO.GetExtensionName(strFileName)
          
50        While g_objFSO.FileExists(sRet)
60            i = i + 1
70            sRet = g_objFSO.BuildPath(strFolderPath, sBaseFileName & "-" & CStr(i) & "." & sFileExt)
80        Wend
          
90        getDistinctFileName = sRet
End Function

Private Function loadAppLog()
          Const PROC_NAME As String = "loadAppLog()"
          Dim oRoot As IXMLDOMElement
          Dim oNode As IXMLDOMElement
10        If g_objFSO.FileExists(g_sDailyLogFile) Then
20            m_oDailyAppLogXML.Load g_sDailyLogFile
              
30            If m_oDailyAppLogXML.parseError.errorCode <> 0 Then
40                m_oDailyAppLogXML.loadXML ("<DailyLog date='" & Format(Now, "mm-dd-yyyy") & "'/>")
50            End If
60        Else
70            m_oDailyAppLogXML.loadXML ("<DailyLog date='" & Format(Now, "mm-dd-yyyy") & "'/>")
80        End If
          
90        Set oRoot = m_oDailyAppLogXML.firstChild
100       Set m_oSessionNode = oRoot.selectSingleNode("//Session[@id='" & m_sSessionID & "']")
110       If m_oSessionNode Is Nothing Then
120           Set m_oSessionNode = m_oDailyAppLogXML.createElement("Session")
130           m_oSessionNode.setAttribute "id", m_sSessionID
140           m_oSessionNode.setAttribute "time", Format(Now, "hh:nn:ss")
150           oRoot.appendChild m_oSessionNode
160       End If
          
170       If g_objFSO.FileExists(g_sJournalLogFile) Then
180           m_oJournalAppLogXML.Load g_sJournalLogFile
              
190           If m_oJournalAppLogXML.parseError.errorCode <> 0 Then
200               m_oJournalAppLogXML.loadXML ("<WildcatJournal/>")
210           End If
220       Else
230           m_oJournalAppLogXML.loadXML ("<WildcatJournal/>")
240       End If
errHandler:
250       If Err.Number <> 0 Then
260           handleError "An error occured in " & PROC_NAME & " [Line#" & CStr(Erl) & " " & Err.Description
270       End If
End Function

Private Function add2AppLog(ByRef oRS As ADODB.Recordset)
          Const PROC_NAME As String = "add2AppLog()"
10        On Error GoTo errHandler
          Dim oNode As IXMLDOMElement
          Dim oNode2 As IXMLDOMElement
          Dim oRootJournal As IXMLDOMElement
          Dim oJournalNode As IXMLDOMElement
20        If Not oRS Is Nothing Then
30            If oRS.State = adStateOpen Then
40                Set oRootJournal = m_oJournalAppLogXML.firstChild
                  
50                Set oNode = m_oDailyAppLogXML.createElement("Package")
60                oNode.setAttribute "LynxID", oRS.Fields("LynxID").Value
70                While Not oRS.EOF
                      'write the daily log
80                    Set oNode2 = m_oDailyAppLogXML.createElement("Document")
90                    oNode2.setAttribute "documentID", oRS.Fields("DocumentID").Value
                      
100                   oNode.appendChild oNode2.cloneNode(False)
110                   oRS.MoveNext
120               Wend
130               m_oSessionNode.appendChild oNode.cloneNode(True)
                  
                  'write the journal entry
140               oNode.setAttribute "SessionID", m_sSessionID
150               oRootJournal.appendChild oNode.cloneNode(True)
160           End If
170       End If
          
errHandler:
180       Set oNode = Nothing
190       Set oNode2 = Nothing
200       Set oRootJournal = Nothing
210       Set oJournalNode = Nothing
220       If Err.Number <> 0 Then
230           handleError "An error occured in " & PROC_NAME & " [Line#" & CStr(Erl) & " " & Err.Description
240       End If
End Function

Private Function saveAppLog()
10        If g_objFSO.FileExists(g_sDailyLogFile) Then Kill g_sDailyLogFile
20        m_oDailyAppLogXML.Save g_sDailyLogFile
          
30        If g_objFSO.FileExists(g_sJournalLogFile) Then Kill g_sJournalLogFile
40        m_oJournalAppLogXML.Save g_sJournalLogFile
End Function


'***************************************************************************************
' Procedure : sendEmail
' DateTime  : 4/19/2006 16:39
' Author    : Ramesh Vishegu
' Purpose   : Send email using CDO
'***************************************************************************************
'
Public Function sendEmail(sEmailFrom As String, ByRef oRecipients As IXMLDOMNodeList, sEmailSubjectResolved As String, sEmailReplyTo As String, _
                                  sEmailImportance As String, sEmailBody As String, aAttachments() As String, Optional lMaxSize As Long = 0) As Boolean
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & ":sendEmail()"
          Dim iMsg As Message
          Dim iConf As Configuration
          Dim Flds As Fields
          Dim oBodyPart As IBodyPart
          Dim oBodyPartAttachments As IBodyPart
          Dim obodyPartDisclaimer As IBodyPart
          Dim i As Integer
          Dim oRecipient As IXMLDOMNode
          Dim stm As ADODB.Stream
          Dim aAttachmentsPerEmail() As String
          Dim sEmailAttachment As String
          Dim lEmailSize As Long
          Dim lFileSize As Long
          Dim iIndex As Integer
          Dim bAttachmentSent As Boolean
          Dim strException As String
          Dim bEmailSent As Boolean
          
20        ReDim aAttachmentsPerEmail(1)
          'group the attachments so as to be within the SMTP size limits
30        If lMaxSize > 0 Then
40            If IsArray(aAttachments) Then
50                If UBound(aAttachments) > 0 Then
60                    ReDim aAttachmentsPerEmail(UBound(aAttachments))
70                    lEmailSize = 0
80                    iIndex = 0
90                    For i = LBound(aAttachments) To UBound(aAttachments)
100                       If aAttachments(i) <> "" Then
110                           If g_objFSO.FileExists(aAttachments(i)) Then
120                               lFileSize = FileLen(aAttachments(i))
130                               If lFileSize < lMaxSize Then 'current file is less than the max email limit
140                                   lEmailSize = lEmailSize + lFileSize
150                                   If lEmailSize > lMaxSize Then
160                                       iIndex = iIndex + 1
170                                       lEmailSize = 0
180                                       sEmailAttachment = ""
190                                   End If
200                                   sEmailAttachment = sEmailAttachment & aAttachments(i) + "|"
210                                   aAttachmentsPerEmail(iIndex) = sEmailAttachment
220                               Else 'current file exceeds the email limit
230                                   strException = strException & "Single attachment exceeds maximum per-email size limit." & vbCrLf & _
                                                                    "  Email subject: " & sEmailSubjectResolved & vbCrLf & _
                                                                    "  Attachment: " + aAttachments(i) & vbCrLf & _
                                                                    "  File Size: " & CStr(lFileSize) & "; max limit: " & CStr(lMaxSize) & vbCrLf
240                                   aAttachmentsPerEmail(iIndex) = ""
250                               End If
260                           End If
270                       End If
280                   Next
290               End If
300           End If
310       End If
          
320       If sEmailSubjectResolved = "CEI - Documents sent with Exceptions" Or Join(aAttachmentsPerEmail, "") <> "" Then
330           Set iConf = CreateObject("CDO.Configuration")
340           Set Flds = iConf.Fields
              
          
              'set the configuration field values
350           With Flds
360             .Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = cdoSendUsingPort  ' cdoSendUsingPort
370             .Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = g_sSMTPServerName 'sftmfnolprdweb1
380             .Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 10     ' quick timeout
390             .Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = cdoNTLM
400             .Update
410           End With
          
420           For Each oRecipient In oRecipients
                  'compose the message
430               Set iMsg = CreateObject("CDO.Message")
440               With iMsg
450                   Set .Configuration = iConf
460                   .To = oRecipient.Text
470                   .From = sEmailFrom
480                   .Subject = sEmailSubjectResolved
490                   .ReplyTo = sEmailReplyTo
                      
500                   .TextBody = sEmailBody
                      
510                   bAttachmentSent = False
                      
520                   For iIndex = LBound(aAttachmentsPerEmail) To UBound(aAttachmentsPerEmail)
530                       If aAttachmentsPerEmail(iIndex) <> "" Then
540                           aAttachments = Split(aAttachmentsPerEmail(iIndex), "|")
550                           For i = LBound(aAttachments) To UBound(aAttachments)
560                               If aAttachments(i) <> "" Then
570                                   If g_objFSO.FileExists(aAttachments(i)) Then
580                                       .AddAttachment (aAttachments(i))
590                                   Else
                                          'MsgBox "File attachment not found." & vbCrLf & aAttachments(i)
600                                       aAttachments(i) = ""
610                                       GoTo cleanUp
620                                   End If
630                               End If
640                           Next
                              
650                           .Send
660                           bEmailSent = True
                              
670                           Trace "Email sent to " & oRecipient.Text & vbCrLf & _
                                    "  Subject: " & sEmailSubjectResolved & vbCrLf & _
                                    "  Attachments: " & vbCrLf & "    " & _
                                    Join(aAttachments, vbCrLf & "    ")
                              
680                           bAttachmentSent = True
690                           .Attachments.DeleteAll
700                       End If
710                   Next
                      
720                   If bAttachmentSent = False Then
                          'email with no attachments
730                       .Send
740                       bEmailSent = True
                          
750                       Trace "Email sent to " & oRecipient.Text & vbCrLf & _
                                "  Subject: " & sEmailSubjectResolved & vbCrLf & _
                                "  Attachments: " & vbCrLf & "    " & _
                                Join(aAttachments, vbCrLf & "    ")
760                   End If
                      
770               End With
780               Set iMsg = Nothing
790           Next
800       End If
cleanUp:
810       Set iConf = Nothing
          
820       If strException <> "" Then
830           ReDim aAttachments(0)
840           sendEmail m_sErrorEmailFrom, m_oErrorEmailTo, "CEI - Documents sent with Exceptions", "", "", _
                            "Documents were send to CEI with the following exceptions:" & vbCrLf & strException, aAttachments
850       End If
errHandler:
860       sendEmail = bEmailSent
870       If Err.Number <> 0 Then
              'handleError "An error occured in " & PROC_NAME & " [Line#" & CStr(Erl) & " " & Err.Description
880           Trace "Error sending email at Line #" & CStr(Erl) & ". Error description: " & Err.Description
890       End If
End Function


Public Sub handleError(strErrMsg As String)
          Dim aAttachments() As String
10        m_bError = True
20        If g_blnAutoRun Then
30            If Not g_objEvents Is Nothing Then
40                Trace strErrMsg
50                ReDim aAttachments(1)
60                sendEmail m_sErrorEmailFrom, m_oErrorEmailTo, m_sErrorEmailSubject, "", "high", strErrMsg, aAttachments
70                Unload Me
80            End If
90        Else
100           MsgBox strErrMsg, vbCritical
110           Unload Me
120       End If
End Sub


Private Sub enableControls(bEnabled As Boolean)
10        cmbEnvironment.Enabled = bEnabled
20        cmbInsuranceCo.Enabled = bEnabled
30        txtDocumentID.Enabled = bEnabled
40        txtLynxID.Enabled = bEnabled
50        txtFrom.Enabled = bEnabled
60        txtTo.Enabled = bEnabled
70        opDateRange.Enabled = bEnabled
80        opDocumentID.Enabled = bEnabled
90        opLynxID.Enabled = bEnabled
100       cmdClose.Enabled = bEnabled
110       cmdProcess.Enabled = bEnabled
120       chkSendAgain.Enabled = bEnabled
          
130       If bEnabled = True Then
140           If opDateRange.Value = True Then opDateRange_Click
150           If opDocumentID.Value = True Then opDocumentID_Click
160           If opLynxID.Value = True Then opLynxID_Click
170       End If
End Sub

Private Sub PDFCreator1_eError()
    handleError "ERROR [" & PDFCreator1.cErrorDetail("Number") & "]: " & PDFCreator1.cErrorDetail("Description")
End Sub

Private Sub PDFCreator1_eReady()
    PDFCreator1.cPrinterStop = True
End Sub

