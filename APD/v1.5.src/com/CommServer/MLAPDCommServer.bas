Attribute VB_Name = "MLAPDCommServer"
'********************************************************************************
'* Component LAPDCommServer : Module MLAPDCommServer
'*
'* Global Consts and Utility Methods
'*
'********************************************************************************
Option Explicit

Public Const APP_NAME As String = "LAPDCommServer."

Public Const LAPDCommServer_FirstError As Long = &H80064800

Public g_objEvents As Object 'SiteUtilities.CEvents
Public g_objDataAccessor As Object 'DataAccessor.CDataAccessor

Public g_blnDebugMode As Boolean
Public g_strSupportDocPath As String

'API declares
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

'********************************************************************************
'* Initialize Global Objects and Variables
'********************************************************************************
Public Sub InitializeGlobals()

          'Create private member DataAccessor.
10        Set g_objDataAccessor = CreateObjectEx("DataAccessor.CDataAccessor")
          'Share DataAccessor's events object.
20        Set g_objEvents = g_objDataAccessor.mEvents
          
          'Use our own log file.
30        g_objEvents.ComponentInstance = "Comm Server"

          'Get path to support document directory
40        g_strSupportDocPath = App.Path & "\" & Left(APP_NAME, Len(APP_NAME) - 1)

          'Initialize our member components first, so we have logging set up.
50        g_objDataAccessor.InitEvents App.Path & "\..\config\config.xml"

          'Get debug mode status.
60        g_blnDebugMode = g_objEvents.IsDebugMode

End Sub

'********************************************************************************
'* Terminate Global Objects
'********************************************************************************
Public Sub TerminateGlobals()
10        Set g_objEvents = Nothing
20        Set g_objDataAccessor = Nothing
End Sub

'********************************************************************************
'* Returns the requested configuration setting.
'********************************************************************************
Public Function GetConfig(ByVal strSetting As String)
10        GetConfig = g_objEvents.mSettings.GetParsedSetting(strSetting)
End Function

'********************************************************************************
'* Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
'* Parameters:  strObjectName = The object to call CreateObject for.
'* Purpose:     Wraps CreateObject() with better debug information.
'* Returns:     The Object created or Nothing.
'********************************************************************************
Public Function CreateObjectEx(strObjectName) As Object
          Dim obj As Object
          
10        On Error GoTo ErrorHandler
          
20        Set CreateObjectEx = Nothing
          
30        Set obj = CreateObject(strObjectName)
          
40        If obj Is Nothing Then
50            Err.Raise LAPDCommServer_FirstError + 10, "", "CreateObject('" & strObjectName & "') returned Nothing."
60        End If
          
70        Set CreateObjectEx = obj
          
ErrorHandler:

80        Set obj = Nothing
          
90        If Err.Number <> 0 Then
100           Err.Raise Err.Number, "CreateObjectEx('" & strObjectName & "') " & Err.Source, Err.Description & " " & strObjectName
110       End If
          
End Function

'********************************************************************************
'* Syntax:      DoSleep( 500 )
'* Parameters:  lngMS - milliseconds to sleep.
'* Purpose:     Wraps Sleep() with calls to VB DoEvents().
'* Returns:     Nothing.
'********************************************************************************
Public Sub DoSleep(ByVal lngMS As Long)
          Dim lCount As Long
10        For lCount = 1 To lngMS \ 10
20            Sleep 10
30            DoEvents
40        Next
End Sub

