<!--
This template is used to cleanup the shop electronic assignment XML. GO Biztalk has
a specific format of data that needs to be sent and if the XML schema changes a bit
the electronic assignments don't go out. This template will allow us to control the
electronic data transmitted regardless of the data given the database.
 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" indent="no" encoding="UTF-8" omit-xml-declaration="yes"/>
  <xsl:template match="/Assignment">
    <xsl:element name="Assignment">
      <xsl:for-each select="@*">
        <xsl:choose>
          <!-- AssignmentSequenceNumber is not part of the Biztalk XML schema. So remove it -->
          <xsl:when test="name(.) = 'AssignmentSequenceNumber'"/>
          <!-- Environment is not part of the Biztalk XML schema. So remove it -->
          <xsl:when test="name(.) = 'Environment'"/>
          <xsl:otherwise>
            <xsl:copy-of select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
      <xsl:for-each select="Claim">
        <xsl:call-template name="claim"/>
      </xsl:for-each>
      <xsl:for-each select="Coverage">
        <xsl:call-template name="coverage"/>
      </xsl:for-each>
      <xsl:for-each select="Vehicle">
        <xsl:call-template name="vehicle"/>
      </xsl:for-each>
      <xsl:for-each select="VehicleContact">
        <xsl:call-template name="vehicleContact"/>
      </xsl:for-each>
      <xsl:for-each select="Shop">
        <xsl:call-template name="shop"/>
      </xsl:for-each>
      <xsl:for-each select="Owner">
        <xsl:call-template name="owner"/>
      </xsl:for-each>
      <xsl:for-each select="LynxRep">
        <xsl:call-template name="LynxRep"/>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
  <!-- Process the claim node -->
  <xsl:template name="claim">
    <xsl:element name="Claim">
      <xsl:for-each select="@*">
        <xsl:choose>
          <!-- InsuranceCompanyAddress1 is not part of the Biztalk XML schema. So remove it -->
          <xsl:when test="name(.) = 'InsuranceCompanyAddress1'"/>
          <!-- InsuranceCompanyAddress2 is not part of the Biztalk XML schema. So remove it -->
          <xsl:when test="name(.) = 'InsuranceCompanyAddress2'"/>
          <!-- InsuranceCompanyAddressCity is not part of the Biztalk XML schema. So remove it -->
          <xsl:when test="name(.) = 'InsuranceCompanyAddressCity'"/>
          <!-- InsuranceCompanyAddressState is not part of the Biztalk XML schema. So remove it -->
          <xsl:when test="name(.) = 'InsuranceCompanyAddressState'"/>
          <!-- InsuranceCompanyAddressZip is not part of the Biztalk XML schema. So remove it -->
          <xsl:when test="name(.) = 'InsuranceCompanyAddressZip'"/>
          <xsl:otherwise>
            <xsl:copy-of select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
  <!-- Process the coverage node -->
  <xsl:template name="coverage">
    <xsl:element name="Coverage">
      <xsl:for-each select="@*">
        <xsl:choose>
          <!-- Claim number can only be 14 chars long. CCC electronic assignment limitation. -->
          <xsl:when test="name(.) = 'ClaimNumber' and /Assignment/@DeliveryMethod = 'CCC' and string-length(.) &gt; 14">
            <xsl:attribute name="ClaimNumber">
              <xsl:value-of select="concat(substring(., 1, 10), '...')"/>
            </xsl:attribute>
          </xsl:when>
          <!-- Claim number can only be 14 chars long. CCC electronic assignment limitation. -->
          <xsl:when test="name(.) = 'ClaimNumber' and /Assignment/@DeliveryMethod = 'ADP' and string-length(.) &gt; 17">
            <xsl:attribute name="ClaimNumber">
              <xsl:value-of select="concat(substring(., 1, 14), '...')"/>
            </xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
  <!-- Process the vehicle node -->
  <xsl:template name="vehicle">
    <xsl:element name="Vehicle">
      <xsl:for-each select="@*">
        <xsl:choose>
          <!-- AdminFeeComments is not part of the Biztalk XML schema. So remove it -->
          <xsl:when test="name(.) = 'AdminFeeComments'"/>
          <!-- ClaimAspectID is not part of the Biztalk XML schema. So remove it -->
          <xsl:when test="name(.) = 'ClaimAspectID'"/>
          <!-- Claim Number was too long to be inclued in the Coverage/@ClaimNumber. We will append its actual value to the Shop Remarks -->
          <xsl:when test="name(.) = 'ShopRemarks' and string-length(/Assignment/Coverage/@ClaimNumber) &gt; 14">
            <xsl:attribute name="ShopRemarks">
              <xsl:value-of select="concat(., ' Full Claim Number: ', /Assignment/Coverage/@ClaimNumber)"/>
            </xsl:attribute>
          </xsl:when>
          <!--06Mar2012 - TVD - Added for Elephants use of Passthru-->
		  <!-- SourceApplicationPassthruData is not part of the Biztalk XML schema. So remove it -->
		  <xsl:when test="name(.) = 'SourceApplicationPassthruData'"/>
          <xsl:otherwise>
            <xsl:copy-of select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
  <!-- Process the vehicle contact node -->
  <xsl:template name="vehicleContact">
    <xsl:element name="VehicleContact">
      <xsl:for-each select="@*">
        <xsl:copy-of select="."/>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
  <!-- Process the shop node -->
  <xsl:template name="shop">
    <xsl:element name="Shop">
      <xsl:for-each select="@*">
        <xsl:copy-of select="."/>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
  <!-- Process the owner node -->
  <xsl:template name="owner">
    <xsl:element name="Owner">
      <xsl:for-each select="@*">
        <xsl:copy-of select="."/>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
  <!-- Process the LynxRep node -->
  <xsl:template name="LynxRep">
    <xsl:element name="LynxRep">
      <xsl:for-each select="@*">
        <xsl:copy-of select="."/>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>



