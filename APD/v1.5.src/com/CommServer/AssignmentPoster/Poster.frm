VERSION 5.00
Begin VB.Form Poster 
   Caption         =   "Form1"
   ClientHeight    =   8490
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11775
   LinkTopic       =   "Form1"
   ScaleHeight     =   8490
   ScaleWidth      =   11775
   StartUpPosition =   3  'Windows Default
   Begin VB.ComboBox Test 
      Height          =   315
      Left            =   1440
      Style           =   2  'Dropdown List
      TabIndex        =   11
      Top             =   1800
      Width           =   4455
   End
   Begin VB.CommandButton Post 
      Caption         =   "Post"
      Height          =   375
      Left            =   120
      TabIndex        =   10
      Top             =   8040
      Width           =   1455
   End
   Begin VB.TextBox XML 
      Height          =   5655
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   9
      Text            =   "Poster.frx":0000
      Top             =   2280
      Width           =   11535
   End
   Begin VB.TextBox URL 
      Height          =   285
      Left            =   1440
      TabIndex        =   7
      Top             =   1440
      Width           =   3495
   End
   Begin VB.TextBox Machine 
      Height          =   285
      Left            =   1440
      TabIndex        =   5
      Top             =   1080
      Width           =   1695
   End
   Begin VB.TextBox Environment 
      Height          =   285
      Left            =   1440
      TabIndex        =   3
      Top             =   720
      Width           =   1695
   End
   Begin VB.CommandButton LoadConfig 
      Caption         =   "Load"
      Height          =   375
      Left            =   5040
      TabIndex        =   2
      Top             =   120
      Width           =   855
   End
   Begin VB.TextBox Config 
      Height          =   285
      Left            =   1440
      TabIndex        =   0
      Text            =   "D:\websites\apd\v1.1.0\config\config.xml"
      Top             =   120
      Width           =   3495
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "Test Data"
      Height          =   255
      Left            =   240
      TabIndex        =   12
      Top             =   1800
      Width           =   1095
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Caption         =   "PPG URL"
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   1440
      Width           =   1095
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Machine"
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Environment"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   720
      Width           =   1095
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Config"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "Poster"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim objEvents As Object
Dim strXML() As String

Private Sub Form_Unload(Cancel As Integer)
    Set objEvents = Nothing
End Sub

Private Sub LoadConfig_Click()

    On Error GoTo ErrorHandler
    
    'Get path to the config file.
    Dim strPath As String
    strPath = Config.Text
    
    'If relative, start with app path.
    If InStr(strPath, ":") = 0 Then
        If Left(strPath, 1) = "\" Then
            strPath = App.Path & strPath
        Else
            strPath = App.Path & "\" & strPath
        End If
    End If

    'Create and initialize event handler.
    If objEvents Is Nothing Then
        Set objEvents = CreateObjectEx("SiteUtilities.CEvents")
    End If
    
    objEvents.Initialize Config.Text
    
    'Fill in edit boxes from config.
    Machine.Text = objEvents.mSettings.Machine
    Environment.Text = objEvents.mSettings.Environment
    URL.Text = objEvents.mSettings.GetParsedSetting("ShopAssignment/ElectronicTransmissionURL")
    
    'Load the XML from a file.
    Dim objDom As New MSXML2.DOMDocument
    LoadXmlFile objDom, App.Path & "\Test.xml", "LoadConfig_Click()", "Assignment"
    
    Dim objList As IXMLDOMNodeList
    Dim objNode As IXMLDOMNode
    Dim intIdx As Integer
    
    Test.Clear
    
    Set objList = objDom.selectNodes("/Root/Test")
    
    ReDim strXML(objList.length)
    intIdx = 0
    
    For Each objNode In objList
        Test.AddItem objNode.Attributes(0).Text
        strXML(intIdx) = objNode.firstChild().XML
        intIdx = intIdx + 1
    Next
    
    'XML.Text = objDom.XML
    'Set objDom = Nothing
    
ErrorHandler:

    If Err.Number <> 0 Then
        MsgBox Err.Description
    End If

End Sub

Private Sub Post_Click()

    On Error GoTo ErrorHandler
    
    Dim objDom As New MSXML2.DOMDocument
    LoadXml objDom, XML.Text, "Post_Click", "Shop Assignment"
    
    Dim lngResult As Long
    lngResult = XmlHttpPost(URL.Text, objDom.XML, objEvents)
    MsgBox lngResult
    
ErrorHandler:

    If Err.Number <> 0 Then
        MsgBox Err.Description
    End If

End Sub

Private Sub Test_Click()
    XML.Text = strXML(Test.ListIndex)
End Sub
