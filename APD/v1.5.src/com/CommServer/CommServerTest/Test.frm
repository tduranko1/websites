VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   6975
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9615
   LinkTopic       =   "Form1"
   ScaleHeight     =   6975
   ScaleWidth      =   9615
   StartUpPosition =   3  'Windows Default
   Begin VB.ComboBox cmbLynxID 
      Height          =   315
      ItemData        =   "Test.frx":0000
      Left            =   960
      List            =   "Test.frx":0010
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   120
      Width           =   5055
   End
   Begin VB.TextBox txtResults 
      Height          =   5535
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   2
      Text            =   "Test.frx":003B
      Top             =   1320
      Width           =   9375
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Go"
      Height          =   495
      Left            =   2280
      TabIndex        =   3
      Top             =   600
      Width           =   1215
   End
   Begin VB.TextBox txtUserID 
      Height          =   285
      Left            =   960
      TabIndex        =   1
      Text            =   "6"
      Top             =   840
      Width           =   1215
   End
   Begin VB.TextBox txtVehicleID 
      Height          =   285
      Left            =   960
      TabIndex        =   0
      Text            =   "1"
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "User ID"
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   840
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "Lynx ID"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Vehicle ID"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   480
      Width           =   735
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Combo1_Change()

End Sub

Private Sub Command1_Click()
    On Error Resume Next

    Dim objCommSvr As Object
    Set objCommSvr = CreateObject("LAPDCommServer.CCommServer")
    
    txtResults.Text = "Running..."
    
    objCommSvr.AssignShop cmbLynxID.ItemData(cmbLynxID.ListIndex), _
        txtVehicleID.Text, txtUserID.Text
    
    If Err.Number <> 0 Then
        txtResults.Text = Err.Description
    Else
        txtResults.Text = "No Errors"
    End If
    
End Sub


