Attribute VB_Name = "MDataPresenter"
'********************************************************************************
'* Component DataPresenter : Module MDataPresenter
'*
'* Global Consts and Utility Methods
'*
'********************************************************************************
Option Explicit

Public Const APP_NAME As String = "DataPresenter."

Public Const DataPresenter_FirstError As Long = vbObjectError + &H23000

'********************************************************************************
'* Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
'* Parameters:  strObjectName = The object to call CreateObject for.
'* Purpose:     Wraps CreateObject() with better debug information.
'* Returns:     The Object created or Nothing.
'********************************************************************************
Public Function CreateObjectEx(strObjectName) As Object
10        On Error GoTo ErrorHandler
          
20        Set CreateObjectEx = Nothing
          
          Dim obj As Object
30        Set obj = CreateObject(strObjectName)
          
40        If obj Is Nothing Then
50            Err.Raise DataPresenter_FirstError + 10, "", "CreateObject('" & strObjectName & "') returned Nothing."
60        End If
          
70        Set CreateObjectEx = obj
80        Exit Function
          
ErrorHandler:
90        On Error GoTo 0
100       Err.Raise Err.Number, "CreateObjectEx('" & strObjectName & "') " & Err.Source, Err.Description & " " & strObjectName
End Function

