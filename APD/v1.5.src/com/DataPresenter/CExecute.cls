VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 1  'NoTransaction
END
Attribute VB_Name = "CExecute"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Component DataPresenter : Class CExecute
'*
'* DataPresenter is the topmost generic interface between ASP and the DB.
'* It is meant to be used in lieu of specific business objects whenever practical.
'*
'* It is also meant to be a starting point for constructing business objects.
'* When creating a new a business object you can begin by copying the
'* DataPresenter project, and then procede by modifying one or more of the
'* Execute**** methods below.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CExecute."

'Internal error codes for this class.
Private Enum ErrorCodes
    eBadProcedureString = DataPresenter_FirstError + &H100
    eBadStyleSheetString
End Enum

'********************************************************************************
'* Private member vars
'********************************************************************************

'See comments below for ConnectionStringParent Properties.
Private mstrConnectionStringParent As String

'********************************************************************************
'* CSiteUtilities Declaration and initialization
'*
'* I envision that CSiteUtilities will some day stabilize enough to install it
'* with straight COM instead of COM+ for better performance.  However, while still
'* in early development I will keep it as a COM+ component for maintainability.
'* With COM+ you must use late binding rather than early binding.
'********************************************************************************
Public mToolkit As SiteUtilities.CWebToolkit

Private Sub Class_Initialize()
10        Set mToolkit = New SiteUtilities.CWebToolkit
End Sub

Private Sub Class_Terminate()
10        Set mToolkit = Nothing
End Sub

'********************************************************************************
' Methods propogated from mToolkit - See SiteUtilities for full comments.
'********************************************************************************

'Initializes the web toolkit.  Must be called first.
Public Sub Initialize(ByVal strWebRoot As String, Optional ByVal strSessionKey As String = "", Optional ByVal strUserId As String = "")
10        mToolkit.Initialize strWebRoot, strSessionKey, strUserId
End Sub

'Initializes the web toolkit.  Must be called first.
Public Sub InitializeEx( _
    ByVal strWebRoot As String, _
    ByVal strSessionKey As String, _
    Optional ByVal strWindowID As String = "0", _
    Optional ByVal strUserId As String = "")
          
10        mToolkit.InitializeEx strWebRoot, strSessionKey, strWindowID, strUserId
End Sub

'Used to set parameters to stub into any style sheet used
Public Sub AddXslParam(ByVal strName As String, ByVal strValue As String)
10        mToolkit.AddXslParam strName, strValue
End Sub

'Throws if the passed expression evaluates as false.
Public Sub Assert(ByVal blnAssertion As Boolean, ByVal strMsg As String)
10        mToolkit.mEvents.Assert blnAssertion, strMsg
End Sub

'Gets a value from the ini DOM document.  Looks first
'in the proper Instance, then the Machine, then the
'Environment and then lastly the Application.
Public Property Get Setting(ByVal strXPath) As String
10        Setting = mToolkit.mEvents.mSettings.GetParsedSetting(strXPath)
End Property

'Used to determine if we are in debug mode or not.
Public Property Get IsDebugMode() As Boolean
10        IsDebugMode = mToolkit.mEvents.IsDebugMode()
End Property

'Logs debug info to the log file for this object instance.
Public Sub Trace(ByVal strMsg As String, Optional ByVal strSource As String)
10        If mToolkit.mEvents.IsDebugMode Then
20            mToolkit.mEvents.Trace strMsg, strSource
30        End If
End Sub

'Transforms the passed XML with the passed style sheet.
Public Function TransformXML(ByRef strXml As String, ByVal strStyleSheet As String) As String
10        TransformXML = mToolkit.TransformXML(strXml, strStyleSheet)
End Function

'********************************************************************************
'* Connection String Parent
'*
'* This property allows the caller to direct DataPresenter to other databases
'* than the APD database by pointing to connect strings other than the defaults.
'*
'* For instance, set this property to "PartnerSettings" and DataPresenter will
'* use the connection strings under the <PartnerSettings> element in config.xml.
'********************************************************************************
Public Property Get ConnectionStringParent() As String
          'Strip off the trailing '/' if present
10        If Right(mstrConnectionStringParent, 1) = "/" Then
20            ConnectionStringParent = Left(mstrConnectionStringParent, Len(mstrConnectionStringParent) - 1)
30        Else
40            ConnectionStringParent = mstrConnectionStringParent
50        End If
End Property

Public Property Let ConnectionStringParent(ByVal strNewParent As String)
          'Add a '/' if not passed.
10        If strNewParent <> "" And Right(strNewParent, 1) <> "/" Then
20            strNewParent = strNewParent & "/"
30        End If
40        mstrConnectionStringParent = strNewParent
End Property

'********************************************************************************
'* Private Validation Routines
'********************************************************************************
Private Sub ValidateProcedure(ByRef strProcedure As String)
10        If Len(strProcedure) <= 0 Then
20            Err.Raise eBadProcedureString, "ValidateProcedure", "No procedure name passed."
30        ElseIf InStr(strProcedure, "usp") = 0 Then
40            Err.Raise eBadProcedureString, "ValidateProcedure", "Bad Procedure name (no 'usp')."
50        End If
End Sub

Private Sub ValidateStyleSheet(ByRef strStyleSheet As String)
10        If Len(strStyleSheet) > 0 Then
20            If StrComp(Right(strStyleSheet, 4), ".xsl", vbTextCompare) <> 0 Then
30                Err.Raise eBadStyleSheetString, "ValidateStyleSheet", "Passed Style Sheet name invalid."
40            End If
50        End If
End Sub

'********************************************************************************
'* Syntax:      obj.ExecuteSp( "uspDoSomething", params )
'* Parameters:  strProcedure - Stored Procedure name.
'*              Params - Variable length param array.
'* Purpose:     Calls a stored procedure.
'* Returns:     The number of records modified (whatever is passed back).
'********************************************************************************
Public Function ExecuteSp( _
    ByVal strProcedure As String, _
    ParamArray Params() As Variant) As Long
          
          Const PROC_NAME As String = MODULE_NAME & "ExecuteSp: "
          
          Dim blnDebugMode As Boolean
          Dim strParams As String
          
10        On Error GoTo ErrHandler
          
20        ValidateProcedure strProcedure
          
          'Get debug mode status.
30        blnDebugMode = mToolkit.mEvents.IsDebugMode
          
          'When cut and pasting for business objects:
          'For inbound business rules, work with Params here.
          
          'Convert ParamArray to SQL param string.
40        If UBound(Params) > -1 Then
50            strParams = mToolkit.ParseParamsToSQL(CVar(Params))
60        End If

          'Add passed arguments to debug info.
70        If blnDebugMode Then mToolkit.mEvents.Trace _
              "Procedure = " & strProcedure & vbCrLf & _
              "  Params=" & strParams, PROC_NAME & "Started"
          
          'Create and initialize DataAccessor
          Dim objData As DataAccessor.CDataAccessor
80        Set objData = mToolkit.CreateDataAccessor(mstrConnectionStringParent & "ConnectionStringStd")
          
          'Make the procedure call.
90        If UBound(Params) > -1 Then
100           ExecuteSp = objData.ExecuteSp(strProcedure, strParams)
110       Else
120           ExecuteSp = objData.ExecuteSp(strProcedure)
130       End If
          
140       If blnDebugMode Then mToolkit.mEvents.Trace "", PROC_NAME & "Finished"
          
ErrHandler:

150       Set objData = Nothing
          
160       If Err.Number <> 0 Then
170           mToolkit.mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & " SP=" & strProcedure
180       End If
          
End Function

'********************************************************************************
'* Syntax:      obj.ExecuteSpStr( "uspDoSomething", "1,2,'SHAQEAL O''NEIL'" )
'* Parameters:  strProcedure - Stored Procedure name.
'*              strParams - Parameter string.  Must be already escaped for SQL!
'* Purpose:     Calls a stored procedure.
'* Returns:     The number of records modified (whatever is passed back).
'********************************************************************************
Public Function ExecuteSpStr( _
    ByVal strProcedure As String, _
    ByVal strParams As String) As Long
          
          Const PROC_NAME As String = MODULE_NAME & "ExecuteSpStr: "
          
          Dim blnDebugMode As Boolean
          
10        On Error GoTo ErrHandler
          
20        ValidateProcedure strProcedure
          
          'Get debug mode status.
30        blnDebugMode = mToolkit.mEvents.IsDebugMode
          
          'When cut and pasting for business objects:
          'For inbound business rules, work with strParams here.
          
          'Add passed arguments to debug info.
40        If blnDebugMode Then mToolkit.mEvents.Trace _
              "Procedure = " & strProcedure & vbCrLf & _
              "  Params=" & strParams, PROC_NAME & "Started"
          
          'Create and initialize DataAccessor
          Dim objData As DataAccessor.CDataAccessor
50        Set objData = mToolkit.CreateDataAccessor(mstrConnectionStringParent & "ConnectionStringStd")
          
          'Make the procedure call
60        ExecuteSpStr = objData.ExecuteSp(strProcedure, strParams)
          
70        If blnDebugMode Then mToolkit.mEvents.Trace "", PROC_NAME & "Finished"
          
ErrHandler:

80        Set objData = Nothing
          
90        If Err.Number <> 0 Then
100           mToolkit.mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & " SP=" & strProcedure
110       End If
          
End Function

'********************************************************************************
'* Syntax:      obj.ExecuteSpNp("uspDoSomething", Request.Form)
'* Parameters:  strProcedure - Stored Procedure name.
'*              strFormData - Item/Value pairs from form controls.
'* Purpose:     Calls a stored procedure using named parameters.
'* Returns:     Nothing
'********************************************************************************
Public Function ExecuteSpNp( _
    ByVal strProcedure As String, _
    ByVal strFormData As String) As Long
          
          Const PROC_NAME As String = MODULE_NAME & "ExecuteSpNp: "
          
          Dim blnDebugMode As Boolean
          
10        On Error GoTo ErrHandler
          
20        ValidateProcedure strProcedure
          
          'Get debug mode status.
30        blnDebugMode = mToolkit.mEvents.IsDebugMode
          
          'Add passed arguments to debug info.
40        If blnDebugMode Then mToolkit.mEvents.Trace _
                "Procedure = " & strProcedure & vbCrLf _
              & "  FormData = " & Replace(strFormData, "&", " "), _
              PROC_NAME & "Started"

          'Convert the passed "item=value" pairs into a collection indexed by item.
          Dim colForm As Collection
50        Set colForm = mToolkit.ParseFormDataToCollection(strFormData)
          
          'When cut and pasting for business objects:
          'For inbound business rules, work with colForm here.
          
          'Create and initialize DataAccessor
          Dim objData As DataAccessor.CDataAccessor
60        Set objData = mToolkit.CreateDataAccessor(mstrConnectionStringParent & "ConnectionStringStd")
          
          'Make the procedure call
70        ExecuteSpNp = objData.ExecuteSpNpCol(strProcedure, colForm)
          
80        If blnDebugMode Then mToolkit.mEvents.Trace "", PROC_NAME & "Finished"
          
ErrHandler:

90        Set objData = Nothing
          
100       If Err.Number <> 0 Then
110           mToolkit.mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & " SP=" & strProcedure & " FormData=" & Replace(strFormData, "&", " ")
120       End If
          
End Function

'********************************************************************************
'* Syntax:      strHTML = obj.ExecuteSpAsRS( "uspDoSomething", params )
'* Parameters:  strProcedure - Stored Procedure name.
'*              Params - Variable length param array.
'* Purpose:     Calls a stored procedure that returns an ADO Recordset.
'* Returns:     The Recordset.
'********************************************************************************
Public Function ExecuteSpAsRS( _
    ByVal strProcedure As String, _
    ParamArray Params() As Variant) As ADODB.Recordset

    Const PROC_NAME As String = MODULE_NAME & "ExecuteSpAsRS: "


    Dim blnDebugMode As Boolean
          Dim strParams As String
          
10        On Error GoTo ErrHandler
          
20        ValidateProcedure strProcedure
          
          'Get debug mode status.
30        blnDebugMode = mToolkit.mEvents.IsDebugMode
          
          'When cut and pasting for business objects:
          'For inbound business rules, work with Params here.
          
          'Convert ParamArray to SQL param string.
40        If UBound(Params) > -1 Then
50            strParams = mToolkit.ParseParamsToSQL(CVar(Params))
60        End If
          
          'Add passed arguments to debug info.
70        If blnDebugMode Then mToolkit.mEvents.Trace _
              "Procedure = " & strProcedure & vbCrLf & _
              "  Params = " & strParams, PROC_NAME & "Started"
          
          'Create and initialize DataAccessor
          Dim objData As DataAccessor.CDataAccessor
80        Set objData = mToolkit.CreateDataAccessor(mstrConnectionStringParent & "ConnectionStringStd")
          
          'Make the procedure call
          Dim objRS As ADODB.Recordset
90        If UBound(Params) > -1 Then
100           Set objRS = objData.OpenRecordsetSp(strProcedure, strParams)
110       Else
120           Set objRS = objData.OpenRecordsetSp(strProcedure)
130       End If
          
          'When cut and pasting for business objects:
          'For outbound business rules, work with Recordset here.
          
140       Set ExecuteSpAsRS = objRS
          
150       If blnDebugMode Then mToolkit.mEvents.Trace "", PROC_NAME & "Finished"
          
ErrHandler:

160       Set objData = Nothing
          
170       If Err.Number <> 0 Then
180           mToolkit.mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & " SP=" & strProcedure & " Params=" & strParams
190       End If
          
End Function

'********************************************************************************
'* Syntax:      strHTML = obj.ExecuteSpAsRS( "uspDoSomething", "1,2,'SHAQEAL O''NEIL'" )
'* Parameters:  strProcedure - Stored Procedure name.
'*              strParams - Parameter string.  Must be already escaped for SQL!
'* Purpose:     Calls a stored procedure that returns an ADO Recordset.
'* Returns:     The Recordset.
'********************************************************************************
Public Function ExecuteSpStrAsRS( _
    ByVal strProcedure As String, _
    ByVal strParams As String) As ADODB.Recordset
          
          Const PROC_NAME As String = MODULE_NAME & "ExecuteSpStrAsRS: "
          
          Dim blnDebugMode As Boolean
          
10        On Error GoTo ErrHandler
          
20        ValidateProcedure strProcedure
          
          'Get debug mode status.
30        blnDebugMode = mToolkit.mEvents.IsDebugMode
          
          'When cut and pasting for business objects:
          'For inbound business rules, work with strParams here.
          
          'Add passed arguments to debug info.
40        If blnDebugMode Then mToolkit.mEvents.Trace _
              "Procedure = " & strProcedure & vbCrLf & _
              "  Params = " & strParams, PROC_NAME & "Started"
          
          'Create and initialize DataAccessor
          Dim objData As DataAccessor.CDataAccessor
50        Set objData = mToolkit.CreateDataAccessor(mstrConnectionStringParent & "ConnectionStringStd")
          
          'Make the procedure call
          Dim objRS As ADODB.Recordset
60        Set objRS = objData.OpenRecordsetSp(strProcedure, strParams)
          
          'When cut and pasting for business objects:
          'For outbound business rules, work with Recordset here.
          
70        Set ExecuteSpStrAsRS = objRS
          
80        If blnDebugMode Then mToolkit.mEvents.Trace "", PROC_NAME & "Finished"
          
ErrHandler:

90        Set objData = Nothing
          
100       If Err.Number <> 0 Then
110           mToolkit.mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & " SP=" & strProcedure & " Params=" & strParams
120       End If
          
End Function

'********************************************************************************
'* Syntax:      strHTML = obj.ExecuteSpNpAsRS( "uspDoSomething", Request.Form )
'* Parameters:  strProcedure - Stored Procedure name.
'*              strFormData - Item/Value pairs from form controls.
'* Purpose:     Calls a stored procedure that returns an ADO Recordset.
'*              Uses named parameters to extract the parameters from the form.
'* Returns:     The Recordset.
'********************************************************************************
Public Function ExecuteSpNpAsRS( _
    ByVal strProcedure As String, _
    ByVal strFormData As String) As ADODB.Recordset
          
          Const PROC_NAME As String = MODULE_NAME & "ExecuteSpAsRS: "
          
          Dim blnDebugMode As Boolean
          Dim strParams As String
          
10        On Error GoTo ErrHandler
          
20        ValidateProcedure strProcedure
          
          'Get debug mode status.
30        blnDebugMode = mToolkit.mEvents.IsDebugMode
          
          'Add passed arguments to debug info.
40        If blnDebugMode Then mToolkit.mEvents.Trace _
                "Procedure = " & strProcedure & vbCrLf _
              & "  FormData = " & Replace(strFormData, "&", " "), _
              PROC_NAME & "Started"

          'Convert the passed "item=value" pairs into a collection indexed by item.
          Dim colForm As Collection
50        Set colForm = mToolkit.ParseFormDataToCollection(strFormData)
          
          'When cut and pasting for business objects:
          'For inbound business rules, work with colForm here.
          
          'Create and initialize DataAccessor
          Dim objData As DataAccessor.CDataAccessor
60        Set objData = mToolkit.CreateDataAccessor(mstrConnectionStringParent & "ConnectionStringStd")
          
          'Make the procedure call
          Dim objRS As ADODB.Recordset
70        Set objRS = objData.OpenRecordsetSpNpCol(strProcedure, colForm)
          
          'When cut and pasting for business objects:
          'For outbound business rules, work with Recordset here.
          
80        Set ExecuteSpNpAsRS = objRS
          
90        If blnDebugMode Then mToolkit.mEvents.Trace "", PROC_NAME & "Finished"
          
ErrHandler:

100       Set objData = Nothing
          
110       If Err.Number <> 0 Then
120           mToolkit.mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & " SP=" & strProcedure & " Params=" & strParams
130       End If
          
End Function

'********************************************************************************
'* Syntax:      strHTML = obj.ExecuteSpAsXML( "uspDoSomething", "mystyle.xsl", params )
'* Parameters:  strProcedure - Stored Procedure name.
'*              strStyleSheet - Style sheet to style by, if any.
'*              Params - Variable length param array.
'* Purpose:     Calls a stored procedure that returns XML.
'*              Transforms it if a style sheet was passed.
'* Returns:     The XML or the transformed XML.
'********************************************************************************
Public Function ExecuteSpAsXML( _
    ByVal strProcedure As String, _
    ByVal strStyleSheet As String, _
    ParamArray Params() As Variant) As String
          
          Const PROC_NAME As String = MODULE_NAME & "ExecuteSpAsXML: "
          
          Dim blnDebugMode As Boolean
          Dim strParams As String
          
10        On Error GoTo ErrHandler
          
20        ValidateProcedure strProcedure
30        ValidateStyleSheet strStyleSheet
          
          'Get debug mode status.
40        blnDebugMode = mToolkit.mEvents.IsDebugMode
          
          'When cut and pasting for business objects:
          'For inbound business rules, work with Params here.
          
          'Convert ParamArray to SQL param string.
50        If UBound(Params) > -1 Then
60            strParams = mToolkit.ParseParamsToSQL(CVar(Params))
70        End If
          
          'Add passed arguments to debug info.
80        If blnDebugMode Then mToolkit.mEvents.Trace _
              "Procedure = " & strProcedure & vbCrLf & _
              "  Params = " & strParams & vbCrLf & _
              "  StyleSheet = " & strStyleSheet, PROC_NAME & "Started"
          
          'Create and initialize DataAccessor
          Dim objData As DataAccessor.CDataAccessor
90        Set objData = mToolkit.CreateDataAccessor(mstrConnectionStringParent & "ConnectionStringXml")
          
          'Make the procedure call
          Dim strXml As String
100       If UBound(Params) > -1 Then
110           strXml = objData.OpenRecordsetAsClientSideXML(strProcedure, strParams)
120       Else
130           strXml = objData.OpenRecordsetAsClientSideXML(strProcedure)
140       End If
          
          'When cut and pasting for business objects:
          'For outbound business rules, create a DOM from strXML and work with it here.
          
          'If a style sheet name was passed, then transform the XML with it.
150       If Len(strStyleSheet) = 0 Then
160           ExecuteSpAsXML = EscapeXml(strXml)
170       Else
180           ExecuteSpAsXML = mToolkit.TransformXML(strXml, strStyleSheet)
190       End If
          
200       If blnDebugMode Then mToolkit.mEvents.Trace "", PROC_NAME & "Finished"
          
ErrHandler:

210       Set objData = Nothing
          
220       If Err.Number <> 0 Then
230           mToolkit.mEvents.Env "XML=" & strXml
240           mToolkit.mEvents.Env "XSL=" & strStyleSheet
250           mToolkit.mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & " SP=" & strProcedure
260       End If
          
End Function

'********************************************************************************
'* Syntax:      strHTML = obj.ExecuteSpAsXML( "uspDoSomething", "mystyle.xsl", params )
'* Parameters:  strProcedure - Stored Procedure name.
'*              strStyleSheet - Style sheet to style by, if any.
'*              Params - Variable length param array.
'* Purpose:     Calls a stored procedure that returns XML.
'*              Transforms it if a style sheet was passed.
'* Returns:     The XML or the transformed XML.
'********************************************************************************
Public Function ExecuteSpStrAsXML( _
    ByVal strProcedure As String, _
    ByVal strStyleSheet As String, _
    ByVal strParams As String) As String
          
          Const PROC_NAME As String = MODULE_NAME & "ExecuteSpStrAsXML: "
          
          Dim blnDebugMode As Boolean
          
10        On Error GoTo ErrHandler
          
20        ValidateProcedure strProcedure
30        ValidateStyleSheet strStyleSheet
          
          'Get debug mode status.
40        blnDebugMode = mToolkit.mEvents.IsDebugMode
          
          'When cut and pasting for business objects:
          'For inbound business rules, work with strParams here.
          
          'Add passed arguments to debug info.
50        If blnDebugMode Then mToolkit.mEvents.Trace _
              "Procedure = " & strProcedure & vbCrLf & _
              "  Params = " & strParams & vbCrLf & _
              "  StyleSheet = " & strStyleSheet, PROC_NAME & "Started"
          
          'Create and initialize DataAccessor
          Dim objData As DataAccessor.CDataAccessor
60        Set objData = mToolkit.CreateDataAccessor(mstrConnectionStringParent & "ConnectionStringXml")
          
          'Make the procedure call
          Dim strXml As String
70        strXml = objData.OpenRecordsetAsClientSideXML(strProcedure, strParams)
          
          'When cut and pasting for business objects:
          'For outbound business rules, create a DOM from strXML and work with it here.
          
          'If a style sheet name was passed, then transform the XML with it.
80        If Len(strStyleSheet) = 0 Then
90            ExecuteSpStrAsXML = EscapeXml(strXml)
100       Else
110           ExecuteSpStrAsXML = mToolkit.TransformXML(strXml, strStyleSheet)
120       End If
          
130       If blnDebugMode Then mToolkit.mEvents.Trace "", PROC_NAME & "Finished"
          
ErrHandler:

140       Set objData = Nothing
          
150       If Err.Number <> 0 Then
160           mToolkit.mEvents.Env "XML=" & strXml
170           mToolkit.mEvents.Env "XSL=" & strStyleSheet
180           mToolkit.mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & " SP=" & strProcedure
190       End If
          
End Function

'********************************************************************************
'* Syntax:      strHTML = obj.ExecuteSpNpAsXML( "uspDoSomething", Request.Form, "mystyle.xsl" )
'* Parameters:  strProcedure - Stored Procedure name.
'*              strFormData - Item/Value pairs from form controls.
'*              strStyleSheet - Style sheet to style by, if any.
'* Purpose:     Calls a stored procedure that returns XML.
'*              Transforms it if a style sheet was passed.
'*              Uses named parameters to extract the parameters from the form.
'* Returns:     The XML or the transformed XML.
'********************************************************************************
Public Function ExecuteSpNpAsXML( _
    ByVal strProcedure As String, _
    ByVal strFormData As String, _
    Optional ByVal strStyleSheet As String = "") As String
          
          Const PROC_NAME As String = MODULE_NAME & "ExecuteSpNpAsXML: "
          
          Dim blnDebugMode As Boolean
          
10        On Error GoTo ErrHandler
          
20        ValidateProcedure strProcedure
30        ValidateStyleSheet strStyleSheet
          
          'Get debug mode status.
40        blnDebugMode = mToolkit.mEvents.IsDebugMode
          
          'Add passed arguments to debug info.
50        If blnDebugMode Then mToolkit.mEvents.Trace _
                "Procedure = " & strProcedure & vbCrLf _
              & "  StyleSheet = " & strStyleSheet & vbCrLf _
              & "  FormData = " & Replace(strFormData, "&", " "), _
              PROC_NAME & "Started"

          'Convert the passed "item=value" pairs into a collection indexed by item.
          Dim colForm As Collection
60        Set colForm = mToolkit.ParseFormDataToSqlStringCollection(strFormData)
          
          'When cut and pasting for business objects:
          'For inbound business rules, work with colForm here.
          
          'Create and initialize DataAccessor
          Dim objData As DataAccessor.CDataAccessor
70        Set objData = mToolkit.CreateDataAccessor(mstrConnectionStringParent & "ConnectionStringXml")
          
          'Make the procedure call
          Dim strXml As String
80        strXml = objData.ExecuteSpNpColXML(strProcedure, colForm)
          
          'When cut and pasting for business objects:
          'For outbound business rules, create a DOM from strXML and work with it here.
          
          'If a style sheet name was passed, then transform the XML with it.
90        If Len(strStyleSheet) = 0 Then
100           ExecuteSpNpAsXML = EscapeXml(strXml)
110       Else
120           ExecuteSpNpAsXML = mToolkit.TransformXML(strXml, strStyleSheet)
130       End If
          
140       If blnDebugMode Then mToolkit.mEvents.Trace "", PROC_NAME & "Finished"
          
ErrHandler:

150       Set objData = Nothing
          
160       If Err.Number <> 0 Then
170           mToolkit.mEvents.Env "XML=" & strXml
180           mToolkit.mEvents.Env "XSL=" & strStyleSheet
190           mToolkit.mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & " SP=" & strProcedure & " FormData=" & Replace(strFormData, "&", " ")
200       End If
          
End Function


