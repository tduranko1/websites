Attribute VB_Name = "Module1"
Option Explicit

Public g_objEvents As Object
Public g_blnDebugMode As Boolean         'Are we in debug mode?? -- SiteUtilities

'********************************************************************************
'* Creates the object from the class string specified and returns it
'********************************************************************************
Public Function CreateObjectEx(strClassString As String) As Object
Const PROC_NAME As String = "CreateObjectEx()"
    On Error GoTo errHandler
    Dim strMsg As String
    
    Set CreateObjectEx = CreateObject(strClassString)
    
    Exit Function
errHandler:
    strMsg = "    Error creating """ + strClassString + """ class object"

    g_objEvents.HandleEvent Err.Number, PROC_NAME & Err.Source, Err.Description, _
        PROC_NAME & vbCrLf & strMsg
    
End Function


