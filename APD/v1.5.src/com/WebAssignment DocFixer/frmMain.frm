VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "APD Webassignment Report Fixer"
   ClientHeight    =   975
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   975
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdExit 
      Caption         =   "Exit"
      Height          =   375
      Left            =   3600
      TabIndex        =   1
      Top             =   540
      Width           =   975
   End
   Begin VB.CommandButton cmdFix 
      Caption         =   "Fix"
      Height          =   375
      Left            =   3600
      TabIndex        =   0
      Top             =   60
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Status"
      Height          =   195
      Left            =   120
      TabIndex        =   3
      Top             =   60
      Width           =   915
   End
   Begin VB.Label lblStatus 
      Height          =   495
      Left            =   120
      TabIndex        =   2
      Top             =   360
      Width           =   3375
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Compare Text

Dim m_sConnect As String

Dim m_sConfigPath As String
Dim m_objDataPresenter As Object
Dim m_sWADocMissingSP As String
Dim m_sWADocDetailSP As String
Dim m_sPartnerDB As String
Dim m_sClaimDetailSP As String
Dim m_sEmailFrom As String
Dim m_sEmailBody As String
Dim m_dteFrom As Date
Dim m_sImageRoot As String
Dim m_sEmailMonitor As String
Dim m_oFSO As New Scripting.FileSystemObject

Private Sub cmdFix_Click()
10        On Error GoTo errHandler
          Const PROC_NAME As String = "cmdFix_Click()"
          Dim objECADMgr As Object
          Dim oRS As Recordset
          Dim rsWADocMissingClaims As Recordset
          Dim rsWADocMissingClaims2 As Recordset
          Dim oWA_XML As New MSXML2.DOMDocument
          Dim oClaimXML As New MSXML2.DOMDocument
          Dim oCarrierNode As IXMLDOMElement
          Dim oCoverageNode As IXMLDOMElement
          Dim rsWAAssignments As Recordset
          Dim oVehNodes As IXMLDOMNodeList
          Dim oVehNode As IXMLDOMElement
          Dim oVehNumNode As IXMLDOMElement
          Dim lLynxID As Long
          Dim strDocPath As String
          Dim strCarrierRepEmail As String
          Dim iInsuranceCompanyID As Integer
          Dim iVehicleNumber As Integer
          Dim strClaimDetail As String
          Dim sAttachments() As String
          Dim strSubject As String
          Dim bWAFound As Boolean
          Dim sMonitorMsg As String
          Dim sReason As String
          Dim oField As Field
          Dim strExposureCD As String
          Dim blnXMLFixed As Boolean
    
    
20        oWA_XML.async = False
30        oClaimXML.async = False
    
40        ReDim sAttachments(1)
    
50        g_objEvents.trace "Getting the claims with missing webassignment document." & vbCrLf
    
          'get the claims that are missing the web assignment document
60        Set oRS = m_objDataPresenter.ExecuteSpNpAsRS(m_sWADocMissingSP, "dteFrom=" & Format(m_dteFrom, "mm/dd/yyyy"))
    
70        If oRS.State = adStateOpen Then
80            g_objEvents.trace "Number of claims with no webassignment document = " & CStr(oRS.RecordCount), PROC_NAME
  
90            sMonitorMsg = ""
  
              'duplicate the recordset
100           Set rsWADocMissingClaims = CreateObject("ADODB.Recordset")
110           For Each oField In oRS.Fields
120               With rsWADocMissingClaims.Fields
130                   .Append oField.Name, oField.Type, oField.DefinedSize, adFldIsNullable + adFldMayBeNull
140                   If oField.Type = adNumeric Then
150                       rsWADocMissingClaims.Fields(oField.Name).NumericScale = oField.NumericScale
160                       rsWADocMissingClaims.Fields(oField.Name).Precision = oField.Precision
170                   End If
180               End With
190           Next
  
200           rsWADocMissingClaims.Open
  
210           While Not oRS.EOF
220               With rsWADocMissingClaims
230                   .AddNew
240                   For Each oField In oRS.Fields
250                       .Fields(oField.Name).Value = oField.Value
260                   Next
270                   .Update
280               End With
290               oRS.MoveNext
300           Wend
  
310           Set rsWADocMissingClaims2 = rsWADocMissingClaims.Clone
  
320           While Not rsWADocMissingClaims.EOF
330               sReason = ""
340               strClaimDetail = ""
350               strSubject = ""
360               lLynxID = rsWADocMissingClaims.Fields("LynxID").Value
370               iInsuranceCompanyID = rsWADocMissingClaims.Fields("InsuranceCompanyID").Value
380               iVehicleNumber = rsWADocMissingClaims.Fields("ClaimAspectNumber").Value

390               If sMonitorMsg = "" Then
400                   sMonitorMsg = "Lynx ID    VehicleNumber  InsuranceCompanyID  WA_AssignmentReportFixed" & vbCrLf & _
                                    "---------  -------------  ------------------  ------------------------" & vbCrLf
410               End If

420               sMonitorMsg = sMonitorMsg & Format(lLynxID, "@@@@@@@@@") & "  " & _
                                              Format(iVehicleNumber, "@@@@@@@@@@@@@") & "  " & _
                                              Format(iInsuranceCompanyID, "@@@@@@@@@@@@@@@@@@") & "  "

430               g_objEvents.trace "Lynx id: " & CStr(lLynxID) & _
                                      " Insurance Company id: " & CStr(iInsuranceCompanyID) & _
                                      " Vehicle Number: " & CStr(iVehicleNumber), PROC_NAME

440               If IsNumeric(lLynxID) Then

450                   Set rsWAAssignments = m_objDataPresenter.ExecuteSpNpAsRS(m_sWADocDetailSP, "partnerDB=" & m_sPartnerDB & "&LynxID=" & CStr(lLynxID))
    
460                   bWAFound = False
    
470                   While Not rsWAAssignments.EOF
    
480                       g_objEvents.trace "XML: " + rsWAAssignments.Fields("XML").Value, PROC_NAME
  
490                       oWA_XML.loadXML rsWAAssignments.Fields("XML").Value
  
500                       Set oVehNodes = oWA_XML.selectNodes("/WebAssignment/Vehicle")
                          'make sure all the vehicle nodes in the assignment have missing webassignment document.
510                       rsWADocMissingClaims2.Filter = "LynxID='" & CStr(lLynxID) & "'"
  
520                       If rsWADocMissingClaims2.RecordCount <> oVehNodes.length Then
                              'One of the vehicles in the webassignment has the webassignment document attached. So skip.
530                           sMonitorMsg = sMonitorMsg & "Webassignment XML contains more than 1 vehicle and the assignment report may be attached to one of the other vehicles."
540                           GoTo processNext
550                       End If
              
560                       blnXMLFixed = False
  
missingVehNumFix:
570                       Set oVehNode = oWA_XML.selectSingleNode("/WebAssignment/Vehicle[VehicleNumber='" & CStr(iVehicleNumber) & "']")
  
580                       If Not oVehNode Is Nothing Then
                              'If oVehNode.Text = CStr(iVehicleNumber) Then
590                               bWAFound = True
    
600                               g_objEvents.trace "  Webassignment XML found.", PROC_NAME
    
                                  'now generate the document and attach to the claim
610                               Set objECADMgr = CreateObjectEx("LAPDEcadAccessorMgr.CWebAssignment")
  
620                               g_objEvents.trace "  Creating the webassignment report.", PROC_NAME
    
630                               strDocPath = objECADMgr.SaveWAReport(oWA_XML, lLynxID)
  
640                               Set objECADMgr = Nothing
  
650                               g_objEvents.trace "  Webassignment report was saved at " & strDocPath, PROC_NAME
    
                                  'remove the vehicles in the webassignment XML from the missing list
660                               rsWADocMissingClaims.Filter = "LynxID='" & CStr(lLynxID) & "'"
670                               While Not rsWADocMissingClaims.EOF
680                                   If Not oWA_XML.selectSingleNode("/WebAssignment/Vehicle[VehicleNumber='" & rsWADocMissingClaims.Fields("ClaimAspectNumber").Value & "']") Is Nothing Then
690                                       rsWADocMissingClaims.Delete
700                                   End If
710                                   rsWADocMissingClaims.MoveNext
720                               Wend
730                               rsWADocMissingClaims.Filter = ""
    
    
740                               If strDocPath <> "" Then
750                                   If m_oFSO.FileExists(m_sImageRoot & strDocPath) Then
  
760                                       g_objEvents.trace "  Retrieving the claim detail.", PROC_NAME

                                          'now get the carrier rep information
770                                       strClaimDetail = m_objDataPresenter.ExecuteSpNpAsXML(m_sClaimDetailSP, _
                                                                                      "LynxID=" & CStr(lLynxID) & "&InsuranceCompanyID=" & CStr(iInsuranceCompanyID))
780                                       If strClaimDetail <> "" Then
790                                           oClaimXML.loadXML strClaimDetail

800                                           If oClaimXML.parseError.errorCode = 0 Then
810                                               Set oCoverageNode = oClaimXML.selectSingleNode("/Root/Claim/Coverage")

820                                               If Not oCoverageNode Is Nothing Then
830                                                   strSubject = "Claim #: " & oCoverageNode.getAttribute("ClientClaimNumber") & " / LYNX ID: " & oClaimXML.selectSingleNode("/Root/Claim/@LynxID").Text & "-" & CStr(iVehicleNumber)
840                                               End If
  
850                                               sMonitorMsg = sMonitorMsg & "Fixed. "

      '                                            Set oCarrierNode = oClaimXML.selectSingleNode("/Root/Claim/Carrier")
      '                                            If Not oCarrierNode Is Nothing Then
      '                                                strCarrierRepEmail = oCarrierNode.getAttribute("EmailAddress")
      '                                                'strCarrierRepEmail = "rvishegu@lynxservices.com"
      '
      '                                                If InStr(1, strCarrierRepEmail, "@") > 0 Then
      '                                                    If sendMail(m_sEmailFrom, strCarrierRepEmail, strSubject, m_sEmailBody, sAttachments) Then
      '                                                        sMonitorMsg = sMonitorMsg & "Notified Carrier rep at " & strCarrierRepEmail
      '                                                    Else
      '                                                        sMonitorMsg = sMonitorMsg & "Error sending email."
      '                                                    End If
      '                                                Else
      '                                                    sMonitorMsg = sMonitorMsg & "Invalid carrier rep email address: " & strCarrierRepEmail
      '                                                End If
      '                                            End If
860                                               sMonitorMsg = sMonitorMsg & vbCrLf
870                                           End If
880                                       Else
890                                           sMonitorMsg = sMonitorMsg & "Unable to retrieve the claim detail." & vbCrLf
900                                       End If
910                                   Else
                                          'error
920                                       g_objEvents.trace strDocPath & " was not found on the storage.", PROC_NAME
930                                       sMonitorMsg = sMonitorMsg & "Could not verify the document location." & vbCrLf
940                                   End If
950                               Else
960                                   sMonitorMsg = sMonitorMsg & "Document not generated." & vbCrLf
970                               End If
    
980                               GoTo processNext
                              'End If
990                       Else
1000                          g_objEvents.trace "Vehicle " + CStr(iVehicleNumber) + " not found."
1010                      End If
  
1020                      If Not rsWAAssignments.EOF Then rsWAAssignments.MoveNext
1030                  Wend
    
1040                  If bWAFound = False And blnXMLFixed = False Then
1050                      If rsWAAssignments.State = 1 Then 'open state
1060                          rsWAAssignments.MoveFirst
1070                          If rsWAAssignments.RecordCount = 1 Then
1080                              Set oVehNode = oWA_XML.selectSingleNode("/WebAssignment/Vehicle")
1090                              If Not oVehNode Is Nothing Then
1100                                  Set oVehNumNode = oWA_XML.createElement("VehicleNumber")
1110                                  oVehNumNode.Text = iVehicleNumber
1120                                  oVehNode.appendChild oVehNumNode
                          
1130                                  g_objEvents.trace "Fixed XML: " + oWA_XML.xml, PROC_NAME
                          
1140                                  blnXMLFixed = True
                          
1150                                  GoTo missingVehNumFix
1160                              End If
1170                          End If
1180                      End If

1190                      g_objEvents.trace "  Webassignment document not found.", PROC_NAME
1200                      sMonitorMsg = sMonitorMsg & "Webassignment XML not found." & vbCrLf
1210                  End If
  
processNext:
1220                  Set rsWAAssignments = Nothing
    
1230              Else
1240                  sMonitorMsg = sMonitorMsg & "Non numeric Lynx ID" & vbCrLf
1250              End If
1260              g_objEvents.trace "", PROC_NAME

1270              If Not rsWADocMissingClaims.EOF Then rsWADocMissingClaims.MoveNext
1280          Wend
1290      End If
    
1300      If sMonitorMsg <> "" Then
1310          sendMail m_sEmailFrom, m_sEmailMonitor, "ALERT: LynxSelect - Webassignment Fix report", sMonitorMsg, sAttachments
1320      End If
    
1330      g_objEvents.trace "Application normal exit.", PROC_NAME
errHandler:

1340      Set rsWADocMissingClaims = Nothing
1350      Set oWA_XML = Nothing
1360      Set oClaimXML = Nothing
1370      Set oCarrierNode = Nothing
1380      Set oCoverageNode = Nothing
1390      Set rsWAAssignments = Nothing
1400      Set oVehNode = Nothing
    
1410      If Err.Number <> 0 Then
1420          sMonitorMsg = "An error occurred in the WA_DocFixer application." & vbCrLf & _
                              "Line #: " & CStr(Erl) & vbCrLf & _
                              "Description: " & Err.Description
1430          sendMail m_sEmailFrom, m_sEmailMonitor, "Error: LynxSelect - WA_DocFixer application :cmdFix_click()", _
                       sMonitorMsg, sAttachments
1440          g_objEvents.trace sMonitorMsg, PROC_NAME
1450          g_objEvents.trace "Application exiting with errors.", PROC_NAME
1460      End If
1470      cmdExit_Click
End Sub

Private Sub cmdExit_Click()
    Set g_objEvents = Nothing
    Set m_objDataPresenter = Nothing
    End
End Sub

Private Sub Form_Load()
10        On Error GoTo errHandler
          Dim sStartDate As String
20        m_sConfigPath = App.Path & "\..\config\config.xml"

          'Create the DataPresenter object to retrieve the recordset
30        Set m_objDataPresenter = CreateObjectEx("DataPresenter.CExecute")
40        If m_objDataPresenter Is Nothing Then
50            End
60        End If
          
70        Set g_objEvents = m_objDataPresenter.mToolkit.mEvents
80        g_objEvents.ComponentInstance = "WebAssigment DocFixer"
90        m_objDataPresenter.Initialize m_sConfigPath
100       g_blnDebugMode = g_objEvents.IsDebugMode
          
110       m_sWADocMissingSP = GetConfig("WADocFixer/WAMissingDocSP")
120       m_sWADocDetailSP = GetConfig("WADocFixer/WADocDetailSP")
130       m_sPartnerDB = GetConfig("WADocFixer/PartnerDB")
140       m_sEmailFrom = GetConfig("WADocFixer/EmailFrom")
150       m_sEmailBody = GetConfig("WADocFixer/EmailBody")
160       m_sClaimDetailSP = GetConfig("ClaimPoint/SPMap/ClaimDetail")
170       m_sImageRoot = Replace(GetConfig("Document/RootDirectory"), "\\", "\")
180       m_sEmailMonitor = GetConfig("EventHandling/Email")
          
190       If m_sEmailMonitor = "" Then m_sEmailMonitor = "rvishegu@lynxservices.com"
          
200       g_objEvents.trace "Application settings: " & vbCrLf & _
                              "    WAMissingDocSP=" & m_sWADocMissingSP & vbCrLf & _
                              "    WADocDetailsSP=" & m_sWADocDetailSP & vbCrLf & _
                              "    PartnerDB=" & m_sPartnerDB & vbCrLf & _
                              "    EmailFrom=" & m_sEmailFrom & vbCrLf & _
                              "    EmailBody=" & m_sEmailBody & vbCrLf & _
                              "    ClaimDetailSP=" & m_sClaimDetailSP & vbCrLf & _
                              "    ImageRoot=" & m_sImageRoot, "Form_Load()"
          
210       sStartDate = Command()
220       If IsDate(sStartDate) = False Then sStartDate = Format(DateAdd("d", -1, Now), "mm/dd/yyyy")
230       m_dteFrom = CDate(sStartDate)
          
240       cmdFix_Click
errHandler:
250       If Err.Number <> 0 Then
260           If Not g_objEvents Is Nothing Then
270               g_objEvents.trace "An error occured in Form_Load(). " & vbCrLf & _
                                      "Line #: " & CStr(Erl) & vbCrLf & _
                                      "Description: " & Err.Description, "Form_Load()"
280           End If
290       End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set g_objEvents = Nothing
    Set m_objDataPresenter = Nothing
End Sub

Private Sub showStatus(strMsg As String)
    lblStatus.Caption = strMsg
    DoEvents
End Sub

Private Function sendMail(sFrom, sTo As String, sSubject, sBody As String, sAttachments() As String) As Boolean
          Const PROC_NAME As String = "sendMail() "
10        On Error GoTo errHandler
          Dim objMail As CDONTS.NewMail
          Dim i As Integer
    
20        Set objMail = New CDONTS.NewMail
    
30        objMail.Subject = sSubject
40        objMail.To = sTo
50        objMail.From = sFrom
    
60        objMail.MailFormat = CdoMailFormatMime
70        objMail.BodyFormat = CdoBodyFormatText
    
80        objMail.Body = sBody
    
90        objMail.send
    
100       g_objEvents.trace "  Email sent to: " & sTo & _
                              " from: " & sFrom & _
                              " subject: " & sSubject, PROC_NAME & vbCrLf & _
                              " body: " & sBody
                        
110       sendMail = True
120       Exit Function
errHandler:
130       If Err.Number <> 0 Then
140           g_objEvents.trace "Error sending email [Line #" & CStr(Erl) & "]. Error description: " & Err.Description
150           sendMail = False
160       End If
End Function

'********************************************************************************
'* Returns the requested configuration setting.
'********************************************************************************
Public Function GetConfig(ByVal strSetting As String) As String
10        GetConfig = g_objEvents.mSettings.GetParsedSetting(strSetting)
End Function

