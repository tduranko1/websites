VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   7335
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9015
   LinkTopic       =   "Form1"
   ScaleHeight     =   7335
   ScaleWidth      =   9015
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnSendFax 
      Caption         =   "Send Fax"
      Height          =   375
      Left            =   3000
      TabIndex        =   1
      Top             =   6840
      Width           =   3015
   End
   Begin VB.TextBox txtFaxXml 
      Height          =   6615
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   120
      Width           =   8775
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btnSendFax_Click()
    On Error GoTo ErrorHandler
    
    Dim objFax As New CFaxServer
    
    objFax.SendFax "ShopAssignment", txtFaxXml.Text
    
    MsgBox "Success!"
    
ErrorHandler:
    If Err.Number <> 0 Then
        MsgBox Err.Description
    End If
    Set objFax = Nothing
End Sub
