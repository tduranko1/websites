Attribute VB_Name = "MLAPDFaxServer"
'********************************************************************************
'* Component LAPDFaxServer : Module MLAPDFaxServer
'*
'* Global Consts and Utility Methods
'*
'********************************************************************************
Option Explicit

Public Const APP_NAME As String = "LAPDFaxServer."

Public Const LAPDFaxServer_FirstError As Long = &H80064800

Public g_objEvents As Object 'SiteUtilities.CEvents
Public g_strSupportDocPath As String
Public g_blnDebugMode As Boolean

'********************************************************************************
'* Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
'* Parameters:  strObjectName = The object to call CreateObject for.
'* Purpose:     Wraps CreateObject() with better debug information.
'* Returns:     The Object created or Nothing.
'********************************************************************************
Public Function CreateObjectEx(strObjectName) As Object
          Dim obj As Object
          
10        On Error GoTo ErrorHandler
          
20        Set CreateObjectEx = Nothing
          
30        Set obj = CreateObject(strObjectName)
          
40        If obj Is Nothing Then
50            Err.Raise LAPDFaxServer_FirstError + 10, "", "CreateObject('" & strObjectName & "') returned Nothing."
60        End If
          
70        Set CreateObjectEx = obj
          
ErrorHandler:

80        Set obj = Nothing
          
90        If Err.Number <> 0 Then
100           Err.Raise Err.Number, "CreateObjectEx('" & strObjectName & "') " & Err.Source, Err.Description & " " & strObjectName
110       End If
          
End Function

