VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFaxServer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Component LAPDFaxServer : Module CFaxServer
'*
'* This class is used to send faxes.  This code is pretty ancient, and should be
'* rewritten some day with the latest RightFax API.
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CFaxServer."
Private m_strGhostscriptPath As String
Private m_strPDFTKPath As String

'Error codes specific to this class.
Private Enum Error_Codes
    eFaxServerFailed = LAPDFaxServer_FirstError + &H100
    eNoFaxServerAvailable
    eXmlParseError
    eXslParseError
    eRightFaxError
    eBookmarkNotFound
    eMethodObsolete
End Enum

'********************************************************************************
'* Initialize global objects and variables.
'********************************************************************************
Private Sub InitializeGlobals()
                    
          'Create private member events class.
10        Set g_objEvents = CreateObjectEx("SiteUtilities.CEvents")

          'Get path to support document directory
20        g_strSupportDocPath = App.Path & "\" & Left(APP_NAME, Len(APP_NAME) - 1)
          
          'Use our own log file.
30        g_objEvents.ComponentInstance = "Fax Server"

          'Initialize our member components first, so we have logging set up.
40        g_objEvents.Initialize App.Path & "\..\config\config.xml"

          'Get debug mode status.
50        g_blnDebugMode = g_objEvents.IsDebugMode

60        If g_blnDebugMode Then g_objEvents.Trace g_strSupportDocPath, "DocPath"

End Sub

'Clean up member objects.
Private Sub TerminateGlobals()
10        Set g_objEvents = Nothing
End Sub

'Ensure that member objects get cleaned up regardless of errors.
Private Sub Class_Terminate()
10        TerminateGlobals
End Sub

'********************************************************************************
'* Returns the requested configuration setting.
'********************************************************************************
Private Function GetConfig(ByVal strSetting As String)
10        GetConfig = g_objEvents.mSettings.GetParsedSetting(strSetting)
End Function

'********************************************************************************
'* Syntax:      object.SendFax "ShopAssignment", mstrData
'* Parameters:  strEntity = The entity name used to key config info from.
'*              strXmlData = The XML data used to build the fax.
'* Purpose:     Send a fax to the fax server queue.
'* Returns:     Nothing
'********************************************************************************
Public Sub SendFax( _
    ByVal strEntity As String, _
    ByVal strXmlData As String, _
    Optional ByVal strSaveToFile As String = "")

          Const PROC_NAME As String = MODULE_NAME & "SendFax: "

          Dim objWord As CWordDocument
          Dim objXmlDoc As MSXML2.DOMDocument40
          Dim objXslDoc As MSXML2.DOMDocument40
          Dim objFaxTemplateXSL As MSXML2.DOMDocument40
          Dim objCustomForm As Object
          Dim objFaxStream As Object
          Dim objFSO As Scripting.FileSystemObject
          
          Dim i As Integer, strResult As String
          Dim strInsCoID As String
          Dim strServers() As String
          Dim strErrDesc() As String

          Dim strDescription As String, strSource As String, lngNumber As Long
          Dim strFaxOutputPath(0) As String, strFaxServer As String, strFaxNumber As String
          Dim strFaxUser As String, strFaxPrinter As String, strFaxTemplate As String
          Dim intServerCount As Integer
          Dim strInsuranceCompanyID As Long
          Dim strServiceChannelCD As String
          Dim strDestinationType As String
          Dim strFaxTemplateSuffix As String
          Dim strFaxTemplateCustomFormBuilder As String
          Dim strCustomFromJobXML As String
          Dim strFaxTempFile As String
          Dim strFaxFlattenedFile As String
          
10        On Error GoTo ErrorHandler

20        ReDim strErrDesc(0)
30        ReDim strServers(0)
          
40        InitializeGlobals

          'Validate parameters
50        g_objEvents.Assert CBool(strXmlData <> ""), "XML Data was blank"
60        g_objEvents.Assert CBool(strEntity <> ""), "Entity was blank"
          
70        If g_blnDebugMode Then g_objEvents.Trace "  Assignment = " & strXmlData, PROC_NAME & "Started for " & strEntity

80        Set objXmlDoc = New MSXML2.DOMDocument40
90        Set objXslDoc = New MSXML2.DOMDocument40

          'Load up the xml document from the passed xml.
100       LoadXml objXmlDoc, strXmlData, PROC_NAME, "Passed Data"
          
          'Look for an insurance company id.  Append that to any resource file names.
110       strInsCoID = GetChildNodeText(objXmlDoc, "//@InsuranceCompanyID")
          
120       If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Insurance Co ID = " & strInsCoID
          
          'Load up the biz rules style sheet based upon the entity name.
130       LoadXmlFile objXslDoc, g_strSupportDocPath & "\" & strEntity & "Fax.xsl", _
              PROC_NAME, "Business Rules for Insurance Co " & strInsCoID
          
          'Transform the xml document with any business rules.
140       strResult = objXmlDoc.transformNode(objXslDoc)
          
150       If g_blnDebugMode Then g_objEvents.Trace "  Results = " & strResult, PROC_NAME & "Transform complete for " & strEntity
          
          'And now reload the document with the transformed XML.
160       LoadXml objXmlDoc, strResult, PROC_NAME, "Converted Data"
          
          'Extract some configuration information for this entity.
170       strFaxPrinter = GetConfig(strEntity & "/FaxPrinter")
180       strFaxUser = GetConfig(strEntity & "/FaxUser")
190       strFaxOutputPath(0) = GetConfig(strEntity & "/FaxWorkFolder") & "\"
200       strFaxServer = GetConfig(strEntity & "/FaxServer")
              
              
          ' Grab some values from the xml that will be used to see if there is a FaxTemplate Suffix node in the config file for this assignment.
210       strInsuranceCompanyID = GetChildNodeText(objXmlDoc, "//Claim/@InsuranceCompanyID", True)
220       strServiceChannelCD = GetChildNodeText(objXmlDoc, "//Vehicle/@ServiceChannelCD", True)
230       strDestinationType = GetChildNodeText(objXmlDoc, "//Assignment/@DestinationType", True)
          
240       strFaxTemplateCustomFormBuilder = GetConfig(strEntity & "/FaxTemplateCustomForm/CustomFormBuilder")
          
250       If strFaxTemplateCustomFormBuilder <> "" Then
260           Set objFaxTemplateXSL = New MSXML2.DOMDocument40
270           LoadXmlFile objFaxTemplateXSL, g_strSupportDocPath & "\" & strFaxTemplateCustomFormBuilder
              
280           If objFaxTemplateXSL.parseError <> 0 Then
290               Err.Raise eDomDataValidationFailed, PROC_NAME, strFaxTemplateCustomFormBuilder & " parse failed."
300           End If
              
310           Set objFSO = New Scripting.FileSystemObject
320           strCustomFromJobXML = objXmlDoc.transformNode(objFaxTemplateXSL)
330           If strCustomFromJobXML <> "" Then
340               Set objCustomForm = CreateObjectEx("LAPDCustomForms.CCustomForms")
350               Set objFaxStream = objCustomForm.previewForm(strCustomFromJobXML, False)
360               If Not objFaxStream Is Nothing Then
370                   strFaxTempFile = objFSO.GetBaseName(objFSO.GetTempName()) & ".pdf"
380                   strFaxTempFile = objFSO.BuildPath(objFSO.GetSpecialFolder(TemporaryFolder), objFSO.GetBaseName(strFaxTempFile) & ".pdf")
                      
390                   objFaxStream.SaveToFile strFaxTempFile
                      
400                   Set objFaxStream = Nothing
                      
410                   m_strGhostscriptPath = GetConfig("CustomForms/GhostScriptPath")
420                   m_strPDFTKPath = GetConfig("CustomForms/PDFTKPath")
                      
430                   strFaxTemplate = PDF2Tif(strFaxTempFile, m_strGhostscriptPath)
                      
440                   If strFaxTemplate <> "" Then
450                       strFaxOutputPath(0) = strFaxTemplate
                          
                          'flatten the pdf so it cannot be modified.
460                       strFaxFlattenedFile = flattenForm(strFaxTempFile, m_strPDFTKPath)
                          
                          'delete the un-flattened pdf file
470                       objFSO.DeleteFile strFaxTempFile
                          
480                       strFaxTempFile = strFaxFlattenedFile
                          
                          'move the pdf shop assignment to storage
490                       objFSO.MoveFile strFaxTempFile, strSaveToFile
500                   Else
510                       Err.Raise eFaxServerFailed, PROC_NAME, "Error converting pdf to tif."
520                   End If
530               Else
                      'fax template not generated
540                   Err.Raise eFaxServerFailed, PROC_NAME, "Error generating fax custom form."
550               End If
560           End If
570       Else
580           Err.Raise eFaxServerFailed, PROC_NAME, "NO Fax Custom form defined."
590       End If
          
          ' HACK: Turn off error handling temporarily. Could add a required parm to the GetConfig call, optional and defaulted to true
          '       so as not to affect any existing calls.  Pass false in this case, and change the underlying SiteUtilities method to only blow up
          '       when a required config item is not found.
600       On Error Resume Next
          
          ' Check for a suffix node in the config file for this assignment.
610       strFaxTemplateSuffix = GetConfig(strEntity & "/FaxTemplateSuffix/Suffix[@InsuranceCompanyID=""" & strInsuranceCompanyID & """ and @ServiceChannelCD=""" & strServiceChannelCD & """ and @DestinationType=""" & strDestinationType & """]")
          
          ' If the Suffix node was not found, clear the error and proceed without it.
620       If Err.Number = -2147089918 Then
630         Err.Clear
640       ElseIf Err.Number <> 0 Then
            ' If any other error was encountered handle normally.
650         GoTo ErrorHandler
660       End If
              
          ' Resume normal error handling.
670       On Error GoTo ErrorHandler
          
          
          'strFaxTemplate = g_strSupportDocPath & "\" & strEntity & "FaxTemplate" & strInsCoID & strFaxTemplateSuffix & ".doc"
680       strFaxNumber = GetConfig(strEntity & "/FaxNumberOverride")

          'Use fax override number if available.
690       If Len(strFaxNumber) = 0 Then
700           strFaxNumber = GetChildNode(objXmlDoc, "//@FaxNumber", True).Text
710       Else
720           g_objEvents.Trace "USING FAX SERVER OVERRIDE TO " & strFaxNumber, PROC_NAME
730       End If

          'Add on the '1' to the phone number if needed.
740       If Len(strFaxNumber) > 8 Then
750           If Left(strFaxNumber, 1) <> "1" Then
760               strFaxNumber = "1" & strFaxNumber
770           End If
780       End If

          'Trace out some of this nifty information.
790       If g_blnDebugMode Then g_objEvents.Trace _
              "FaxTemplate = " & strFaxTemplate & vbCrLf _
              & "  FaxPrinter = " & strFaxPrinter & vbCrLf _
              & "  FaxUser = " & strFaxUser & vbCrLf _
              & "  FaxNumber = " & strFaxNumber, PROC_NAME

          'Build a file output path based upon the current time and date.
          'strFaxOutputPath(0) = strFaxOutputPath(0) & _
              DatePart("yyyy", Now()) & DatePart("m", Now()) & _
              DatePart("d", Now()) & DatePart("h", Now()) & _
              DatePart("n", Now()) & DatePart("s", Now()) & _
              "LAPDFax" & GetChildNode(objXmlDoc, "//@LynxId", True).Text & ".doc"

          'Create and the MS Word object.
      '    Set objWord = New CWordDocument
      '    objWord.LoadFromTemplate strFaxTemplate
          
          'Replace bookmarks with data from the XML doc.
      '    objWord.LoadBookmarksFromXML objXmlDoc

          'If caller requested, save fax to file.
      '    If strSaveToFile <> "" Then
      '
      '        If g_blnDebugMode Then g_objEvents.Trace strSaveToFile, PROC_NAME & "Saving to File"
      '
      '        objWord.SaveToFile strSaveToFile
      '
      '    End If
      '
      '    If g_blnDebugMode Then g_objEvents.Trace strFaxOutputPath(0), PROC_NAME & "Saving to File"
      '
      '    'Print the word doc out to disc before faxing.
      '    'objWord.PrintToFile strFaxPrinter, strFaxOutputPath(0)
      '    objWord.SaveToFile strFaxOutputPath(0)
      '
      '    If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Done saving to File"
      '
      '    'Dispose of the word document
      '    Set objWord = Nothing

          'Split up the server list.
800       strServers = Split(strFaxServer, ",")
          
810       intServerCount = UBound(strServers)
          
820       ReDim strErrDesc(intServerCount)

830       On Error Resume Next

          'Loop through the servers and try them one by one.
840       For i = LBound(strServers) To UBound(strServers)

850           If g_blnDebugMode Then g_objEvents.Trace strServers(i), PROC_NAME & "Faxing to " & strFaxNumber
              
              'Try to send it.
860           QueueFax strServers(i), strFaxUser, strFaxOutputPath, _
                  GetChildNode(objXmlDoc, "//@FaxDestinationName", True).Text, strFaxNumber

              'If no problems then exit.
870           If Err.Number = 0 Then

880               On Error GoTo ErrorHandler
                  
890               If g_blnDebugMode Then g_objEvents.Trace "Server = " & strServers(i), PROC_NAME & "Success!"

900               Exit For

              'An error occurred!
910           Else

                  'Log the failure as a warning.
                  'g_objEvents.HandleEvent eFaxServerFailed, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, "Server = " & strServers(i), "", False
920               strErrDesc(i) = Err.Description

                  'If we are all out of servers to try then raise it back as an error.
930               If i = UBound(strServers) Then
940                   On Error GoTo ErrorHandler
950                   Err.Raise eNoFaxServerAvailable, PROC_NAME, strFaxServer & Err.Description
960               End If

                  'Otherwise clear the error and try the next fax server.
970               Err.Clear
980           End If

990       Next
          
          'clean up the temporary fax template custom file
1000      If Not objFSO Is Nothing Then
1010          If strFaxTempFile <> "" Then
1020              If objFSO.FileExists(strFaxTempFile) Then
1030                  objFSO.DeleteFile strFaxTempFile, True
1040              End If
1050          End If
              
1060          If strFaxOutputPath(0) <> "" Then
1070              If objFSO.FileExists(strFaxOutputPath(0)) Then
1080                  objFSO.DeleteFile strFaxOutputPath(0), True
1090              End If
1100          End If
1110      End If
          
1120      If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Exiting"
          
ErrorHandler:

          'Store the true error info in case the Kill call below tosses.
1130      strDescription = Err.Description
1140      strSource = " [Line " & Erl & "] " & PROC_NAME & Err.Source
1150      lngNumber = Err.Number

1160      Err.Clear
1170      On Error Resume Next

1180      Set objWord = Nothing
1190      Set objXmlDoc = Nothing
1200      Set objXslDoc = Nothing
1210      Set objFaxStream = Nothing
1220      Set objFSO = Nothing
        
          'The following clean up and logging code was blowing away the
          'final HandleEvent at the bottom.  This was in spite of the
          '"On Error Resume Next" above.  Thus I have disabled this block
          'of code entirely to ensure that the 'true' error is logged.
          
          'If strFaxOutputPath(0) <> "" Then
          '    Kill strFaxOutputPath(0)
          'End If

          'Log any errors that occurred above.
          'For i = LBound(strServers) To UBound(strServers)
          '    If strErrDesc(i) <> "" Then
          '        g_objEvents.HandleEvent eFaxServerFailed, PROC_NAME & "QueueFax", _
          '            strErrDesc(i), "Server = " & strServers(i), "", False
          '    End If
          'Next
          
1230      Err.Clear
          
1240      If lngNumber <> 0 Then
1250          g_objEvents.Env "FaxTemplate = " & strFaxTemplate
1260          g_objEvents.Env "FaxPrinter = " & strFaxPrinter
1270          g_objEvents.Env "FaxUser = " & strFaxUser
1280          g_objEvents.Env "FaxServerList = " & strFaxServer
              
1290          On Error GoTo 0
              
1300          g_objEvents.HandleEvent lngNumber, strSource, strDescription
1310      End If

1320      TerminateGlobals

End Sub

' The following version was written using the RightFax COM API.
' Stopped development on this because this version has to pull
' the fax as an attachment across the LAN, ewww.

'Private Sub QueueFaxAPI( _
'    ByVal strFaxServer As String, _
'    ByVal strFaxUser As String, _
'    ByRef strFaxOutputPath() As String, _
'    ByVal strDestination As String, _
'    ByVal strFaxNumber As String)
'
'    Const PROC_NAME As String = MODULE_NAME & "QueueFaxAPD"
'
'    On Error GoTo ErrorHandler
'
'    Dim blnSvrOpen As Boolean
'
'    Dim objSvr As New RFCOMAPILib.FaxServer
'    Dim objFax As RFCOMAPILib.Fax
'    Dim objAtt As RFCOMAPILib.Attachment
'
'    objSvr.AuthorizationUserID = strFaxUser
'    objSvr.AuthorizationUserPassword = ""
'    objSvr.Protocol = cpTCPIP
'    objSvr.ServerName = strFaxServer
'    objSvr.OpenServer
'    blnSvrOpen = True
'
'    Set objFax = objSvr.CreateObject(coFax)
'    Set objAtt = objFax.Attachments.Create()
'
'    objAtt.FileName = strFaxOutputPath(0)
'    objAtt.DeleteAfterSend = True
'
'    objFax.ToFaxNumber = strFaxNumber
'    objFax.ToCompany = strDestination
'    objFax.ToName = strDestination
'    objAtt.
'
'    'objFax.send
'    objFax.Save True
'
'ErrorHandler:
'
'    Set objAtt = Nothing
'    Set objFax = Nothing
'
'    If blnSvrOpen Then objSvr.CloseServer
'    Set objSvr = Nothing
'
'    If Err.Number <> 0 Then
'        g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
'    End If
'
'End Sub

'********************************************************************************
'* Syntax:      object.QueueFax
'* Parameters:  None
'* Purpose:     Send a fax to the fax server queue
'* Returns:     Nothing
'* Created:     09/13/2001
'********************************************************************************
Private Sub QueueFax( _
    ByVal strFaxServer As String, _
    ByVal strFaxUser As String, _
    ByRef strFaxOutputPath() As String, _
    ByVal strDestination As String, _
    ByVal strFaxNumber As String)
          
          Const PROC_NAME As String = MODULE_NAME & "QueueFax"

          Dim hndServer As Long
          Dim lngError As Long
          Dim lngHandle As Long
          Dim lngFI10Obj As Long
          Dim lngNI10Obj As Long

10        On Error GoTo ErrorHandler

          'Test error
          'Err.Raise eRightFaxError, PROC_NAME, "Unable to send fax.  Fax server TEST error."

20        hndServer = RFaxCreateServerHandle(strFaxServer, COMMPROTOCOL_TCPIP, lngError)
30        If (lngError <> 0) Then
40            Err.Raise eRightFaxError, PROC_NAME & "RFaxCreateServerHandle()", "Unable to send fax.  Fax server error = " & lngError
50        End If

60        If (hndServer <> 0) Then

70            lngError = RFVB_New(RFO_FAXINFO_10, lngFI10Obj)
80            If (lngError <> 0) Then
90                Err.Raise eRightFaxError, PROC_NAME & "RFVB_New()", "Unable to send fax.  Fax server error = " & lngError
100           End If

             ' If the user wants a cover sheet and we have notes, then initialize a NOTEINFO_10 object
             ' and fill it with the text from the edit control.  The RFBV_FillNoteInfo10() will word-wrap
             ' the text so as to fit on the cover sheet automatically.
          '      If (btnHasCover.Value And edtNotes.Text > "") Then
          '         lngError = RFVB_New(RFO_NOTEINFO_10, lngNI10Obj)
          '         lngError = RFVB_FillNoteInfo10(lngNI10Obj, edtNotes.Text)
          '         If (lngError <> 0) Then
          '            RFVB_Delete (lngNI10Obj)
          '            lngNI10Obj = 0
          '         End If
          '      Else
          '         lngNI10Obj = 0
          '      End If

             ' We have to initialize a FaxInfo_10 object based on the owner of the fax
             ' This is why we call the RFaxInitFax() function.  If we didn't, the FaxInfo_10
             ' object would not be setup properly.
110           lngError = RFaxInitFax(hndServer, lngFI10Obj, strFaxUser)
120           If (lngError <> 0) Then
130               Err.Raise eRightFaxError, PROC_NAME & "RFaxInitFax()", "Unable to send fax.  Fax server error = " & lngError
140           End If

150           If (lngError = 0) Then
160               lngError = RFVB_SetBString(lngFI10Obj, RFO_FI10_TO_NAME, strDestination)
170               If (lngError <> 0) Then
180                   Err.Raise eRightFaxError, PROC_NAME & "RFVB_SetBString()", "Unable to send fax.  Fax server error = " & lngError
190               End If

200               lngError = RFVB_SetBString(lngFI10Obj, RFO_FI10_TO_FAXNUM, strFaxNumber)
210               If (lngError <> 0) Then
220                   Err.Raise eRightFaxError, PROC_NAME & "RFVB_SetBString()", "Unable to send fax.  Fax server error = " & lngError
230               End If

240               lngError = RFVB_SetInteger(lngFI10Obj, RFO_FI10_PAPERNUM, -1) ' No form overlay
250               If (lngError <> 0) Then
260                   Err.Raise eRightFaxError, PROC_NAME & "RFVB_Integer()", "Unable to send fax.  Fax server error = " & lngError
270               End If

280               lngError = RFVB_SetBString(lngFI10Obj, RFO_FI10_TO_COMPANY, strDestination)
290               If (lngError <> 0) Then
300                   Err.Raise eRightFaxError, PROC_NAME & "RFVB_SetBString()", "Unable to send fax.  Fax server error = " & lngError
310               End If

          '         If (btnPreview.Value) Then
          '            Call RFVB_SetLong(lngFI10Obj, RFO_FI10_HOLD_FOR_PREVIEW, 1)
          '         Else
          '            Call RFVB_SetLong(lngFI10Obj, RFO_FI10_HOLD_FOR_PREVIEW, 0)
          '         End If

                ' Here we want to change a fax flag.  We can NOT simply set the value as the fax flags
                ' member already has values which we shouldn't mess with, else the fax will not send
                ' successfully.  The initial value for the fax flags was initialized using the RFaxInitFax()
                ' function above.
                ' To change the flag, we read the existing value, OR-on or AND-off the bit in question,
                ' and then set the new value back into the object.
          '         Call RFVB_GetLong(lngFI10Obj, RFO_FI10_FRFLAGS, l)
          '         If (btnSmartResume.Value) Then
          '            l = l Or FAXFLAG_SMARTRESUME
          '         Else
          '            l = l And (Not FAXFLAG_SMARTRESUME)
          '         End If
          '         Call RFVB_SetLong(lngFI10Obj, RFO_FI10_FRFLAGS, l)

          '         If (cmbNotification.ListIndex = 0) Then
          '            lngError = RFVB_SetInteger(lngFI10Obj, RFO_FI10_USSENDNOTIFY, 0)
          '         ElseIf (cmbNotification.ListIndex = 1) Then
          '            lngError = RFVB_SetInteger(lngFI10Obj, RFO_FI10_USSENDNOTIFY, SEND_NOTIFY_SENDOK)
          '         ElseIf (cmbNotification.ListIndex = 2) Then
          '            lngError = RFVB_SetInteger(lngFI10Obj, RFO_FI10_USSENDNOTIFY, SEND_NOTIFY_SENDERR)
          '         ElseIf (cmbNotification.ListIndex = 3) Then
          '            lngError = RFVB_SetInteger(lngFI10Obj, RFO_FI10_USSENDNOTIFY, SEND_NOTIFY_SENDERR And SEND_NOTIFY_SENDOK)
          '         End If
                ' Now send the file(s).  With the RFVB_SendFiles1() function, which uses the RFaxSendCVL() function
                ' underneith, the files we are sending cannot have embedded codes within them; well, they can
                ' but the server will not scan the files for codes.  This is due to the fact that information in the
                ' FAXINFO_10 and NOTEINFO_10 objects would be ambiguous with any embedded codes found.

320               lngError = RFVB_SendFiles1(hndServer, lngFI10Obj, lngNI10Obj, 0, "", strFaxOutputPath(), lngHandle)
330               If (lngError <> 0) Then
340                   Err.Raise eRightFaxError, PROC_NAME & "RFVB_SendFiles1()", "Unable to send fax.  Fax server error = " & lngError
350               End If
360           End If

370       End If

ErrorHandler:

380       If (lngNI10Obj <> 0) Then
390           RFVB_Delete (lngNI10Obj)
400           lngNI10Obj = 0
410       End If

420       If (lngFI10Obj <> 0) Then
430           RFVB_Delete (lngFI10Obj)
440           lngFI10Obj = 0
450       End If

460       If (hndServer <> 0) Then
470           RFaxCloseServerHandle (hndServer)
480           hndServer = 0
490       End If

500       If Err.Number <> 0 Then

              'g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
              '    "Server = " & strFaxServer & "  User = " & strFaxUser, "", False

              'We don't call HandleEvent because we want the calling
              'process to be able to supress or log errors as it sees fit.
510           Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
520       End If

End Sub

'********************************************************************************
'* Syntax:      object.SendFaxDocument "C:\MyDocument.doc", strShopFaxNumber
'* Parameters:  strFilePath = The Word document path.
'*              strShopFaxNumber = The fax number.
'* Purpose:     Send a fax to the fax server queue.
'* Returns:     Nothing
'********************************************************************************
Public Function SendFaxDocument( _
    ByVal strEntity As String, _
    ByVal strDestination As String, _
    ByVal strFilePath As String, _
    ByVal strShopFaxNumber As String) As Boolean

          Const PROC_NAME As String = MODULE_NAME & "SendFaxDocument: "

          'Dim objWord As CWordDocument
          'Dim objXmlDoc As MSXML2.DOMDocument40
          'Dim objXslDoc As MSXML2.DOMDocument40
          Dim objFSO As Scripting.FileSystemObject
          
          Dim i As Integer, strResult As String
          Dim strInsCoID As String
          Dim strServers() As String
          Dim strErrDesc() As String

          Dim strDescription As String, strSource As String, lngNumber As Long
          Dim strFaxOutputPath(0) As String, strFaxServer As String, strFaxNumber As String
          Dim strFaxUser As String, strFaxPrinter As String, strFaxTemplate As String
          Dim intServerCount As Integer
          Dim strInsuranceCompanyID As Long
          Dim strServiceChannelCD As String
          Dim strDestinationType As String
          Dim strFaxTemplateSuffix As String
          
10        On Error GoTo ErrorHandler

20        ReDim strErrDesc(0)
30        ReDim strServers(0)
          
40        SendFaxDocument = True
          
50        InitializeGlobals

          'Validate parameters
60        g_objEvents.Assert CBool(strFilePath <> ""), "Empty File path"
70        g_objEvents.Assert CBool(strShopFaxNumber <> ""), "Shop Fax number is blank"
          
80        If g_blnDebugMode Then g_objEvents.Trace "  Document Path = " & strFilePath & vbCrLf & _
                                                   "  Fax Number = " & strShopFaxNumber, PROC_NAME

          'Set objXmlDoc = New MSXML2.DOMDocument40
          'Set objXslDoc = New MSXML2.DOMDocument40
90        Set objFSO = New Scripting.FileSystemObject
          
100       If objFSO.FileExists(strFilePath) Then
              'Extract some configuration information for this entity.
110           strFaxPrinter = GetConfig(strEntity & "/FaxPrinter")
120           strFaxUser = GetConfig(strEntity & "/FaxUser")
130           strFaxOutputPath(0) = GetConfig(strEntity & "/FaxWorkFolder") & "\"
140           strFaxServer = GetConfig(strEntity & "/FaxServer")
              
              ' HACK: Turn off error handling temporarily. Could add a required parm to the GetConfig call, optional and defaulted to true
              '       so as not to affect any existing calls.  Pass false in this case, and change the underlying SiteUtilities method to only blow up
              '       when a required config item is not found.
150           On Error Resume Next
              
              ' Check for a suffix node in the config file for this assignment.
      '        strFaxTemplateSuffix = GetConfig(strEntity & "/FaxTemplateSuffix/Suffix[@InsuranceCompanyID=""" & strInsuranceCompanyID & """ and @ServiceChannelCD=""" & strServiceChannelCD & """ and @DestinationType=""" & strDestinationType & """]")
      '
      '        ' If the Suffix node was not found, clear the error and proceed without it.
      '        If Err.Number = -2147089918 Then
      '          Err.Clear
      '        ElseIf Err.Number <> 0 Then
      '          ' If any other error was encountered handle normally.
      '          GoTo ErrorHandler
      '        End If
      '
      '        ' Resume normal error handling.
      '        On Error GoTo ErrorHandler
      '
      '
      '        strFaxTemplate = g_strSupportDocPath & "\" & strEntity & "FaxTemplate" & strInsCoID & strFaxTemplateSuffix & ".doc"
              
160           strFaxNumber = GetConfig(strEntity & "/FaxNumberOverride")
          
              'Use fax override number if available.
170           If Len(strFaxNumber) = 0 Then
                  'strFaxNumber = GetChildNode(objXmlDoc, "//@FaxNumber", True).Text
180               strFaxNumber = strShopFaxNumber
190           Else
200               g_objEvents.Trace "USING FAX SERVER OVERRIDE TO " & strFaxNumber, PROC_NAME
210           End If
          
              'Add on the '1' to the phone number if needed.
220           If Len(strFaxNumber) > 8 Then
230               If Left(strFaxNumber, 1) <> "1" Then
240                   strFaxNumber = "1" & strFaxNumber
250               End If
260           End If
          
              'Trace out some of this nifty information.
270           If g_blnDebugMode Then g_objEvents.Trace _
                    "  FaxPrinter = " & strFaxPrinter & vbCrLf _
                  & "  FaxUser = " & strFaxUser & vbCrLf _
                  & "  FaxNumber = " & strFaxNumber, PROC_NAME
          
          
280           strFaxOutputPath(0) = strFilePath
              
              'Split up the server list.
290           strServers = Split(strFaxServer, ",")
              
300           intServerCount = UBound(strServers)
              
310           ReDim strErrDesc(intServerCount)
          
320           On Error Resume Next
          
              'Loop through the servers and try them one by one.
330           For i = LBound(strServers) To UBound(strServers)
          
340               If g_blnDebugMode Then g_objEvents.Trace strServers(i), PROC_NAME & "Faxing to " & strFaxNumber
                  
                  'Try to send it.
350               QueueFax strServers(i), strFaxUser, strFaxOutputPath, _
                      strDestination, strFaxNumber
          
                  'If no problems then exit.
360               If Err.Number = 0 Then
          
370                   On Error GoTo ErrorHandler
                      
380                   If g_blnDebugMode Then g_objEvents.Trace "Server = " & strServers(i), PROC_NAME & "Success!"
          
390                   Exit For
          
                  'An error occurred!
400               Else
          
                      'Log the failure as a warning.
                      'g_objEvents.HandleEvent eFaxServerFailed, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, "Server = " & strServers(i), "", False
410                   strErrDesc(i) = Err.Description
          
                      'If we are all out of servers to try then raise it back as an error.
420                   If i = UBound(strServers) Then
430                       On Error GoTo ErrorHandler
440                       Err.Raise eNoFaxServerAvailable, PROC_NAME, strFaxServer & Err.Description
450                   End If
          
                      'Otherwise clear the error and try the next fax server.
460                   Err.Clear
470               End If
          
480           Next
490       Else
              'file does not exist. throw an error
500           SendFaxDocument = False
510       End If
          
520       If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Exiting"
          
ErrorHandler:

          'Store the true error info in case the Kill call below tosses.
530       strDescription = Err.Description
540       strSource = " [Line " & Erl & "] " & PROC_NAME & Err.Source
550       lngNumber = Err.Number

560       Err.Clear
570       On Error Resume Next

          'Set objWord = Nothing
          'Set objXmlDoc = Nothing
          'Set objXslDoc = Nothing
580       Set objFSO = Nothing

590       Err.Clear
          
600       If lngNumber <> 0 Then
610           g_objEvents.Env "FaxTemplate = " & strFaxTemplate
620           g_objEvents.Env "FaxPrinter = " & strFaxPrinter
630           g_objEvents.Env "FaxUser = " & strFaxUser
640           g_objEvents.Env "FaxServerList = " & strFaxServer
              
650           On Error GoTo 0
              
660           g_objEvents.HandleEvent lngNumber, strSource, strDescription
670       End If

680       TerminateGlobals

End Function


