Attribute VB_Name = "MRF2VB"
' Returns from RFaxObj??? API calls (RF2VB.DLL)
Global Const RF2VB_SUCCESS = 0
Global Const RF2VB_ERR_INVALID_OBJECT_TYPE = 13000
Global Const RF2VB_ERR_OUTOFMEMORY = 13001
Global Const RF2VB_ERR_INVALID_MEMBER_VALUE = 13002
Global Const RF2VB_ERR_INVALID_TYPE_CONV = 13003
Global Const RF2VB_ERR_CANTWRITETEMP = 13004

Global Const RFO_FAXINFO_10 = &H10000
Global Const RFO_FAXLISTELEMENT_0 = &H20000
Global Const RFO_USERINFO_10 = &H30000
Global Const RFO_PRINTERINFO_10 = &H40000
Global Const RFO_SERVERINFO = &H50000
Global Const RFO_ARCHIVEREQUEST = &H60000
Global Const RFO_FORMLISTELEMENT_0 = &H70000
Global Const RFO_PRINTERLISTELEMENT_0 = &H80000
Global Const RFO_LIBDOCLISTELEMENT_1 = &H90000
Global Const RFO_LIBDOCLISTELEMENT_2 = &HA0000
Global Const RFO_USERLISTELEMENT_1 = &HB0000
Global Const RFO_HISTLISTELEMENT_0 = &HC0000
Global Const RFO_RFAXQUERYINFO = &HD0000
Global Const RFO_RFAXQUERYINFO2 = &HE0000
Global Const RFO_PHONELISTELEMENT_0 = &HF0000
Global Const RFO_PHONELISTELEMENT_1 = &H100000
Global Const RFO_PHONEITEM = &H110000
Global Const RFO_BILLLISTELEMENT_0 = &H120000
Global Const RFO_MESSAGEREQUEST = &H130000
Global Const RFO_WORKREQUEST = &H140000
Global Const RFO_NOTEINFO_10 = &H150000
Global Const RFO_COMPLETEREQUEST = &H160000
Global Const RFO_VALIDATEREQUEST = &H170000
Global Const RFO_AUTOPRINTREQUEST = &H180000
Global Const RFO_GETNEWFAXCOUNT = &H190000
Global Const RFO_GROUPINFO_10 = &H1A0000
Global Const RFO_COMBINEINFO = &H1B0000
Global Const RFO_SIGINFO_10 = &H1C0000
Global Const RFO_FORMINFO_10 = &H1D0000
Global Const RFO_BILLINFO_10 = &H1E0000
Global Const RFO_SERVERINFO_2 = &H1F0000
Global Const RFO_SIGLISTELEMENT_0 = &H200000
Global Const RFO_FOLDERLISTELEMENT_0 = &H210000
Global Const RFO_LIBDOCLISTELEMENT_0 = &H220000
Global Const RFO_USERLISTELEMENT_0 = &H230000
Global Const RFO_COMPLETEREQUEST2 = &H240000
Global Const RFO_SVCINFO_10 = &H250000
Global Const RFO_SVCLISTELEMENT_0 = &H260000
Global Const RFO_MSMAIL1REQ = &H270000
Global Const RFO_CCMAIL1REQ = &H280000
Global Const RFO_WPOMAIL1REQ = &H290000
Global Const RFO_GENMAIL1REQ = &H2A0000
Global Const RFO_OCRREQ = &H2B0000

Global Const RFO_FI10_HANDLE = &H10001
Global Const RFO_FI10_OWNERID = &H10002
Global Const RFO_FI10_FFR_FLAGS = &H10003
Global Const RFO_FI10_LINKS = &H10004
Global Const RFO_FI10_JOBID = &H10005
Global Const RFP_FI10_FAXTYPE = &H10006
Global Const RFO_FI10_FAXFILENAME = &H10007
Global Const RFO_FI10_INPUTFILENAME = &H10008
Global Const RFO_FI10_NUMPAGES = &H10009
Global Const RFO_FI10_PAPERNUM = &H1000A
Global Const RFO_FI10_PRINTJOBTYPE = &H1000B
Global Const RFO_FI10_FINEMODE = &H1000C
Global Const RFO_FI10_SZBFTFILE = &H1000D
Global Const RFO_FI10_ULBFTBYTES = &H1000E
Global Const RFO_FI10_SZORIGBFTNAME = &H1000F
Global Const RFO_FI10_FRFLAGS = &H10010
Global Const RFO_FI10_TO_FAXNUM = &H10011
Global Const RFO_FI10_TO_CONTACTNUM = &H10012
Global Const RFO_FI10_TO_NAME = &H10013
Global Const RFO_FI10_TO_COMPANY = &H10014
Global Const RFO_FI10_TO_CITYSTATE = &H10015
Global Const RFO_FI10_FROM_NAME = &H10016
Global Const RFO_FI10_FROM_PHONENUM = &H10017
Global Const RFO_FI10_BILLINFO1 = &H10018
Global Const RFO_FI10_BILLINFO2 = &H10019
Global Const RFO_FI10_FAXDIDNUM = &H1001A
Global Const RFO_FI10_OPERATORNUM = &H1001B
Global Const RFO_FI10_GENERALFAXNUM = &H1001C
Global Const RFO_FI10_CALL_BACK = &H1001D
Global Const RFO_FI10_FAX_DATETIME = &H1001E
Global Const RFO_FI10_SEND_TIME = &H1001F
Global Const RFO_FI10_FAX_STATUS = &H10020
Global Const RFO_FI10_FAX_ERROR_CODE = &H10021
Global Const RFO_FI10_FAX_DISPOSITION = &H10022
Global Const RFO_FI10_FAX_TERMSTAT = &H10023
Global Const RFO_FI10_REMOTEID = &H10024
Global Const RFO_FI10_FAXFILE_DBA = &H10025
Global Const RFO_FI10_FAXNOTE_DBA = &H10026
Global Const RFO_FI10_FCSFILE = &H10027
Global Const RFO_FI10_FCSTEXT = &H10028
Global Const RFO_FI10_HOLD_FOR_PREVIEW = &H10029
Global Const RFO_FI10_FAXSEND_DATETIME = &H1002A
Global Const RFO_FI10_UNIQUE_ID = &H1002B
Global Const RFO_FI10_FCSTYPE = &H1002C
Global Const RFO_FI10_EMAILTYPE = &H1002D
Global Const RFO_FI10_PRIORITY = &H1002E
Global Const RFO_FI10_SENDCHANNEL = &H1002F
Global Const RFO_FI10_CUSTOM1 = &H10030
Global Const RFO_FI10_USPAGESINFRONT = &H10031
Global Const RFO_FI10_UCMAXTRIES = &H10032
Global Const RFO_FI10_UCTRYINTERVAL = &H10033
Global Const RFO_FI10_USFOLDER = &H10034
Global Const RFO_FI10_USSENDNOTIFY = &H10035
Global Const RFO_FI10_SZSECURECSID = &H10036
Global Const RFO_FI10_SZFROMNAMECONT = &H10037
Global Const RFO_FI10_HISTCHGDATETIME = &H10038

Global Const RFO_FLE0_HANDLE = &H20001
Global Const RFO_FLE0_FLAGS = &H20002
Global Const RFO_FLE0_FAX_DATETIME = &H20003
Global Const RFO_FLE0_TO_NAME = &H20004
Global Const RFO_FLE0_TO_NUMBER = &H20005
Global Const RFO_FLE0_NUMPAGES = &H20006
Global Const RFO_FLE0_FAX_STATUS = &H20007
Global Const RFO_FLE0_FAX_ERROR_CODE = &H20008
Global Const RFO_FLE0_FAXSEND_DATETIME = &H20009
Global Const RFO_FLE0_OTHERSTUFF = &H2000A
Global Const RFO_FLE0_DISPLAYSTUFF = &H2000B
Global Const RFO_FLE0_NUMTRXRECS = &H2000C
Global Const RFO_FLE0_ULBFTBYTES = &H2000D
Global Const RFO_FLE0_SZORIGBFTNAME = &H2000E
Global Const RFO_FLE0_USPAGESINFRONT = &H2000F
Global Const RFO_FLE0_BILLINFO1 = &H20010
Global Const RFO_FLE0_BILLINFO2 = &H20011
Global Const RFO_FLE0_UNIQUE_ID = &H20012
Global Const RFO_FLE0_USFOLDERINDEX = &H20013
Global Const RFO_FLE0_HISTCHG_DATETIME = &H20014
Global Const RFO_FLE0_USERID = &H20015

Global Const RFO_UI10_HANDLE = &H30001
Global Const RFO_UI10_USNUMFAXESOWNED = &H30002
Global Const RFO_UI10_GROUPID = &H30003
Global Const RFO_UI10_USERFLAGS = &H30004
Global Const RFO_UI10_USERID = &H30005
Global Const RFO_UI10_USERNAME = &H30006
Global Const RFO_UI10_PASSWORD = &H30007
Global Const RFO_UI10_USERDIDNUMBER = &H30008
Global Const RFO_UI10_FCSMODELNAME = &H30009
Global Const RFO_UI10_NOTIFY_TYPE = &H3000A
Global Const RFO_UI10_ST_DIDNUM = &H3000B
Global Const RFO_UI10_ST_GENFAXNUM = &H3000C
Global Const RFO_UI10_ST_CALLBACK = &H3000D
Global Const RFO_UI10_ST_OPERNUM = &H3000E
Global Const RFO_UI10_ST_FROMNAME = &H3000F
Global Const RFO_UI10_ST_FROMPHONE = &H30010
Global Const RFO_UI10_ST_TO_FAXNUM = &H30011
Global Const RFO_UI10_ST_TO_CONTACTNUM = &H30012
Global Const RFO_UI10_ST_TO_NAME = &H30013
Global Const RFO_UI10_ST_TO_COMPANY = &H30014
Global Const RFO_UI10_ST_TO_CITYSTATE = &H30015
Global Const RFO_UI10_AUTORECVPRINT_ID = &H30016
Global Const RFO_UI10_DEFAULT_PRINTER = &H30017
Global Const RFO_UI10_AUTOSENTPRINT_ID = &H30018
Global Const RFO_UI10_AUTOPRINTFLAGS = &H30019
Global Const RFO_UI10_ULSUBSCRIBERID = &H3001A
Global Const RFO_UI10_ULUSERFLAGS2 = &H3001B
Global Const RFO_UI10_UCHIGHESTPRIORITY = &H3001C
Global Const RFO_UI10_UCDEFAULTPRIORITY = &H3001D
Global Const RFO_UI10_NEWFAX_NOTIFY_USER = &H3001E
Global Const RFO_UI10_ROUTETYPE = &H3001F
Global Const RFO_UI10_ROUTEFMT = &H30020
Global Const RFO_UI10_AF_PHONE = &H30021
Global Const RFO_UI10_SEND_NOTIFY = &H30022
Global Const RFO_UI10_RECEIVE_NOTIFY = &H30023
Global Const RFO_UI10_UPDATE_INTERVAL = &H30024
Global Const RFO_UI10_AUTO_DELETE = &H30025
Global Const RFO_UI10_DEFAULT_VIEW_SCALE = &H30026
Global Const RFO_UI10_AUTOOCR_FLAGS = &H30027
Global Const RFO_UI10_AUTOOCR_LAYOUT = &H30028
Global Const RFO_UI10_AUTOOCR_FORMAT = &H30029
Global Const RFO_UI10_AUTOOCR_EXT = &H3002A
Global Const RFO_UI10_AF_USER = &H3002B
Global Const RFO_UI10_ROUTE_DELETEAFTER = &H3002C
Global Const RFO_UI10_ULCHANGETAG = &H3002D
Global Const RFO_UI10_ROUTEINFO = &H3002E
Global Const RFO_UI10_DSNAME = &H3002F
Global Const RFO_UI10_FOLDERS = &H30030
Global Const RFO_UI10_RFO_STARTPAGE = &H30031
Global Const RFO_UI10_RFO_ENDPAGE = &H30032
Global Const RFO_UI10_RFO_FLAGS = &H30033
Global Const RFO_UI10_RFO_RES = &H30034
Global Const RFO_UI10_RFO_SIZE = &H30035
Global Const RFO_UI10_RFO_SOURCE = &H30036
Global Const RFO_UI10_RFO_OUTPUTBIN = &H30037
Global Const RFO_UI10_RFO_DUPLEX = &H30038
Global Const RFO_UI10_RFO_SECURITYCODE = &H30039
Global Const RFO_UI10_RFO_COPIES = &H3003A
Global Const RFO_UI10_RFO_RESERVED = &H3003B
Global Const RFO_UI10_RFO_ACCTCODE = &H3003C
Global Const RFO_UI10_RFO_PRIORITY = &H3003D
Global Const RFO_UI10_RFO_OTHER = &H3003E

Global Const RFO_PRI10_HANDLE = &H40001
Global Const RFO_PRI10_FLAGS = &H40002
Global Const RFO_PRI10_NETPRINT_ID = &H40003
Global Const RFO_PRI10_DESCRIPTION = &H40004
Global Const RFO_PRI10_SERVERNAME = &H40005
Global Const RFO_PRI10_QUEUENAME = &H40006
Global Const RFO_PRI10_PRINTERTYPE = &H40007
Global Const RFO_PRI10_DEFSIZE = &H40008
Global Const RFO_PRI10_DEFSOURCE = &H40009

Global Const RFO_SI_SERVERTYPE = &H50001
Global Const RFO_SI_SERVERVERSION = &H50002
Global Const RFO_SI_SERVERSPECIAL = &H50003
Global Const RFO_SI_MAXUSERS = &H50004
Global Const RFO_SI_VERIFYCODES = &H50005
Global Const RFO_SI_BILLDESC1 = &H50006
Global Const RFO_SI_BILLDESC2 = &H50007
Global Const RFO_SI_BUILDDATE = &H50008
Global Const RFO_SI_FLAGS = &H50009
Global Const RFO_SI_ENTERPRISE = &H5000A
Global Const RFO_SI_DOCSONDEMAND = &H5000B
Global Const RFO_SI_TELECONNECT = &H5000C

Global Const RFO_ARCREQ_FLAGS = &H60001
Global Const RFO_ARCREQ_REQUESTTYPE = &H60002
Global Const RFO_ARCREQ_HANDLE = &H60003
Global Const RFO_ARCREQ_DELETEFAX = &H60004
Global Const RFO_ARCREQ_USERID = &H60005

Global Const RFO_FRLE0_HANDLE = &H70001
Global Const RFO_FRLE0_FLAGS = &H70002
Global Const RFO_FRLE0_NUM = &H70003
Global Const RFO_FRLE0_CODE = &H70004
Global Const RFO_FRLE0_NAME = &H70005
Global Const RFO_FRLE0_FILENAME = &H70006
Global Const RFO_FRLE0_STARTPAGE = &H70007
Global Const RFO_FRLE0_NUMPAGES = &H70008
Global Const RFO_FRLE0_NEXTTYPE = &H70009
Global Const RFO_FRLE0_SECURITYID = &H7000A

Global Const RFO_PRLE0_HANDLE = &H80001
Global Const RFO_PRLE0_FLAGS = &H80002
Global Const RFO_PRLE0_NETPRINT_ID = &H80003
Global Const RFO_PRLE0_DESCRIPTION = &H80004
Global Const RFO_PRLE0_SERVERNAME = &H80005
Global Const RFO_PRLE0_QUEUENAME = &H80006
Global Const RFO_PRLE0_PRINTERTYPE = &H80007
Global Const RFO_PRLE0_DEFSIZE = &H80008
Global Const RFO_PRLE0_DEFSOURCE = &H80009

Global Const RFO_LDLE1_HANDLE = &H90001
Global Const RFO_LDLE1_FLAGS = &H90002
Global Const RFO_LDLE1_OBJECTID = &H90003
Global Const RFO_LDLE1_FILENAME = &H90004
Global Const RFO_LDLE1_DESCRIPTION = &H90005
Global Const RFO_LDLE1_NUMPAGES = &H90006
Global Const RFO_LDLE1_TIMESREQUESTEDFOD = &H90007
Global Const RFO_LDLE1_TIMESUSEDLAN = &H90008
Global Const RFO_LDLE1_EMBARGODATE = &H90009
Global Const RFO_LDLE1_EXPIREDATE = &H9000A
Global Const RFO_LDLE1_PARENTFOLDER = &H9000B
Global Const RFO_LDLE1_FOLDERID = &H9000C
Global Const RFO_LDLE1_TIMESREQUESTEDWEB = &H9000D
Global Const RFO_LDLE1_LASTUSED = &H9000E

Global Const RFO_LDLE2_HANDLE = &HA0001
Global Const RFO_LDLE2_FLAGS = &HA0002
Global Const RFO_LDLE2_OBJECTID = &HA0003
Global Const RFO_LDLE2_FILENAME = &HA0004
Global Const RFO_LDLE2_DESCRIPTION = &HA0005
Global Const RFO_LDLE2_NUMPAGES = &HA0006
Global Const RFO_LDLE2_TIMESREQUESTEDFOD = &HA0007
Global Const RFO_LDLE2_TIMESUSEDLAN = &HA0008
Global Const RFO_LDLE2_EMBARGODATE = &HA0009
Global Const RFO_LDLE2_EXPIREDATE = &HA000A
Global Const RFO_LDLE2_PARENTFOLDER = &HA000B
Global Const RFO_LDLE2_FOLDERID = &HA000C
Global Const RFO_LDLE2_TIMESREQUESTEDWEB = &HA000D
Global Const RFO_LDLE2_LASTUSED = &HA000E

Global Const RFO_ULE1_HANDLE = &HB0001
Global Const RFO_ULE1_USERID = &HB0002
Global Const RFO_ULE1_USERNAME = &HB0003
Global Const RFO_ULE1_DIDNUMBER = &HB0004
Global Const RFO_ULE1_GROUPID = &HB0005
Global Const RFO_ULE1_NUMFAXESOWNED = &HB0006
Global Const RFO_ULE1_SUBSCRIBERID = &HB0007
Global Const RFO_ULE1_ROUTETYPE = &HB0008
Global Const RFO_ULE1_ROUTEFMT = &HB0009
Global Const RFO_ULE1_USERFLAGS = &HB000A
Global Const RFO_ULE1_NOTIFY_TYPE = &HB000B
Global Const RFO_ULE1_HIGHESTPRIORITY = &HB000C
Global Const RFO_ULE1_DEFAULTPRIORITY = &HB000D

Global Const RFO_HLE0_HANDLE = &HC0001
Global Const RFO_HLE0_DATETIME = &HC0002
Global Const RFO_HLE0_TYPE = &HC0003
Global Const RFO_HLE0_TRX_ELAPSEDTIME = &HC0004
Global Const RFO_HLE0_TRX_HANGUP_STATUS = &HC0005
Global Const RFO_HLE0_TRX_BAD_PAGE_COUNT = &HC0006
Global Const RFO_HLE0_TRX_CHANNEL_USED = &HC0007
Global Const RFO_HLE0_TRX_DISPOSITION = &HC0008
Global Const RFO_HLE0_TRX_TERMSTAT = &HC0009
Global Const RFO_HLE0_TRX_REMOTEID = &HC000A
Global Const RFO_HLE0_TRX_BOARDTYPE = &HC000B
Global Const RFO_HLE0_TRX_GOOD_PAGE_COUNT = &HC000C
Global Const RFO_HLE0_TRX_REMOTESERVER = &HC000D
Global Const RFO_HLE0_TRX_ANI = &HC000E
Global Const RFO_HLE0_TRX_BTCR_STATUS = &HC000F
Global Const RFO_HLE0_TRX_BTCR_LINESTAT = &HC0010
Global Const RFO_HLE0_TRX_BTFR_STATUS = &HC0011
Global Const RFO_HLE0_TRX_BTFR_LINESTAT = &HC0012
Global Const RFO_HLE0_TRX_FLAGS = &HC0013
Global Const RFO_HLE0_TRX_GAMMAERR = &HC0014
Global Const RFO_HLE0_NETFWD_FLAGS = &HC0015
Global Const RFO_HLE0_NETFWD_PREVOWNERID = &HC0016
Global Const RFO_HLE0_NETFWD_USERSFWDTO = &HC0017
Global Const RFO_HLE0_NETFWD_NOTES = &HC0018
Global Const RFO_HLE0_ROUTE_FLAGS = &HC0019
Global Const RFO_HLE0_ROUTE_PREVOWNERID = &HC001A
Global Const RFO_HLE0_ROUTE_NOTES = &HC001B
Global Const RFO_HLE0_PRINT_FLAGS = &HC001C
Global Const RFO_HLE0_PRINT_TIMETOPRINT = &HC001D
Global Const RFO_HLE0_PRINT_ERROR1 = &HC001E
Global Const RFO_HLE0_PRINT_ERROR2 = &HC001F
Global Const RFO_HLE0_PRINT_PAGESPRINTED = &HC0020
Global Const RFO_HLE0_PRINT_COPIESPRINTED = &HC0021
Global Const RFO_HLE0_PRINT_NETPRINTID = &HC0022
Global Const RFO_HLE0_PRINT_MSG = &HC0023
Global Const RFO_HLE0_OCR_TIMETOOCR = &HC0024
Global Const RFO_HLE0_OCR_ERROR1 = &HC0025
Global Const RFO_HLE0_OCR_MSG = &HC0026
Global Const RFO_HLE0_APRVD_FLAGS = &HC0027
Global Const RFO_HLE0_APRVD_APPROVERID = &HC0028
Global Const RFO_HLE0_APRVD_NOTES = &HC0029
Global Const RFO_HLE0_DISAPRVD_FLAGS = &HC002A
Global Const RFO_HLE0_DISAPRVD_APPROVERID = &HC002B
Global Const RFO_HLE0_DISAPRVD_NOTES = &HC002C
Global Const RFO_HLE0_TRX_AOC1 = &HC002D
Global Const RFO_HLE0_TRX_AOC2 = &HC002E
Global Const RFO_HLE0_TRX_AOC3 = &HC002F
Global Const RFO_HLE0_TRX_ISDNCAUSEVALUE = &HC0030

Global Const RFO_RFQI_INFOLEVEL = &HD0001
Global Const RFO_RFQI_STARTDATEMONTH = &HD0002
Global Const RFO_RFQI_STARTDATEDAY = &HD0003
Global Const RFO_RFQI_STARTDATEYEAR = &HD0004
Global Const RFO_RFQI_ENDDATEMONTH = &HD0005
Global Const RFO_RFQI_ENDDATEDAY = &HD0006
Global Const RFO_RFQI_ENDDATEYEAR = &HD0007
Global Const RFO_RFQI_INCLUDESENT = &HD0008
Global Const RFO_RFQI_INCLUDERECV = &HD0009

Global Const RFO_RFQI2_INFOLEVEL = &HE0001
Global Const RFO_RFQI2_DATECHECKTYPE = &HE0002
Global Const RFO_RFQI2_STARTDATEMONTH = &HE0003
Global Const RFO_RFQI2_STARTDATEDAY = &HE0004
Global Const RFO_RFQI2_STARTDATEYEAR = &HE0005
Global Const RFO_RFQI2_STARTDATEHOUR = &HE0006
Global Const RFO_RFQI2_STARTDATEMINUTE = &HE0007
Global Const RFO_RFQI2_ENDDATEMONTH = &HE0008
Global Const RFO_RFQI2_ENDDATEDAY = &HE0009
Global Const RFO_RFQI2_ENDDATEYEAR = &HE000A
Global Const RFO_RFQI2_ENDDATEHOUR = &HE000B
Global Const RFO_RFQI2_ENDDATEMINUTE = &HE000C
Global Const RFO_RFQI2_INCLUDESENT = &HE000D
Global Const RFO_RFQI2_INCLUDERECV = &HE000E

Global Const RFO_PLE0_HANDLE = &HF0001
Global Const RFO_PLE0_FLAGS = &HF0002
Global Const RFO_PLE0_ID = &HF0003
Global Const RFO_PLE0_DESCRIPTION = &HF0004
Global Const RFO_PLE0_USERID = &HF0005

Global Const RFO_PLE1_HANDLE = &H100001
Global Const RFO_PLE1_FLAGS = &H100002
Global Const RFO_PLE1_ID = &H100003
Global Const RFO_PLE1_NAME = &H100004
Global Const RFO_PLE1_COMPANY = &H100005
Global Const RFO_PLE1_FAX1 = &H100006
Global Const RFO_PLE1_USERID = &H100007

Global Const RFO_PI_HANDLE = &H110001
Global Const RFO_PI_FLAGS = &H110002
Global Const RFO_PI_USERID = &H110003
Global Const RFO_PI_ENTRYID = &H110004
Global Const RFO_PI_ITEM_NAME = &H110005
Global Const RFO_PI_ITEM_COMPANY = &H110006
Global Const RFO_PI_ITEM_ADDRESS = &H110007
Global Const RFO_PI_ITEM_CITYSTATE = &H110008
Global Const RFO_PI_ITEM_FAX1 = &H110009
Global Const RFO_PI_ITEM_FAX2 = &H11000A
Global Const RFO_PI_ITEM_BILLINFO1 = &H11000B
Global Const RFO_PI_ITEM_BILLINFO2 = &H11000C
Global Const RFO_PI_ITEM_VOICENUM1 = &H11000D
Global Const RFO_PI_ITEM_VOICENUM2 = &H11000E
Global Const RFO_PI_ITEM_NOTES1 = &H11000F
Global Const RFO_PI_ITEM_SECURECSID = &H110010
Global Const RFO_PI_GROUP_KEY1 = &H110011
Global Const RFO_PI_GROUP_KEY2 = &H110012
Global Const RFO_PI_GROUP_KEY3 = &H110013
Global Const RFO_PI_GROUP_KEY4 = &H110014
Global Const RFO_PI_GROUP_KEY5 = &H110015
Global Const RFO_PI_GROUP_KEY6 = &H110016
Global Const RFO_PI_GROUP_KEY7 = &H110017
Global Const RFO_PI_GROUP_KEY8 = &H110018
Global Const RFO_PI_GROUP_KEY9 = &H110019
Global Const RFO_PI_GROUP_KEY10 = &H11001A
Global Const RFO_PI_GROUP_KEY11 = &H11001B
Global Const RFO_PI_GROUP_KEY12 = &H11001C
Global Const RFO_PI_GROUP_KEY13 = &H11001D
Global Const RFO_PI_GROUP_KEY14 = &H11001E
Global Const RFO_PI_GROUP_KEY15 = &H11001F
Global Const RFO_PI_GROUP_KEY16 = &H110020
Global Const RFO_PI_GROUP_KEY17 = &H110021
Global Const RFO_PI_GROUP_KEY18 = &H110022
Global Const RFO_PI_GROUP_KEY19 = &H110023
Global Const RFO_PI_GROUP_KEY20 = &H110024
Global Const RFO_PI_GROUP_KEY21 = &H110025
Global Const RFO_PI_GROUP_KEY22 = &H110026
Global Const RFO_PI_GROUP_KEY23 = &H110027
Global Const RFO_PI_GROUP_KEY24 = &H110028

Global Const RFO_BLE0_HANDLE = &H120001
Global Const RFO_BLE0_BILLINFO1 = &H120002
Global Const RFO_BLE0_BILLINFO2 = &H120003
Global Const RFO_BLE0_DESCRIPTION = &H120004

Global Const RFO_MSGREQ_FLAGS = &H130001
Global Const RFO_MSGREQ_REQUESTTYPE = &H130002
Global Const RFO_MSGREQ_HANDLE = &H130003
Global Const RFO_MSGREQ_DELETEFAX = &H130004
Global Const RFO_MSGREQ_MSGTYPE = &H130005
Global Const RFO_MSGREQ_MSGTEXT = &H130006
Global Const RFO_MSGREQ_USERID = &H130007

Global Const RFO_WR_FLAGS = &H140001
Global Const RFO_WR_REQUESTTYPE = &H140002
Global Const RFO_WR_HANDLE = &H140003
Global Const RFO_WR_CONVERT_SOURCEDIR = &H140004
Global Const RFO_WR_CONVERT_SOURCEFILE = &H140005
Global Const RFO_WR_CONVERT_OUTPUTDIR = &H140006
Global Const RFO_WR_CONVERT_OUTPUTFILE = &H140007
Global Const RFO_WR_CONVERT_FINEMODE = &H140008
Global Const RFO_WR_CONVERT_COVERSHEET = &H140009
Global Const RFO_WR_CONVERT_USERID = &H14000A
Global Const RFO_WR_MERGE_FORMNUM = &H14000B
Global Const RFO_WR_MERGE_USERID = &H14000C
Global Const RFO_WR_AUTOPRT = &H14000D
Global Const RFO_WR_GENMAIL1_DELETEFAX = &H14000E
Global Const RFO_WR_GENMAIL1_MSGTYPE = &H14000F
Global Const RFO_WR_GENMAIL1_MSGTEXT = &H140010
Global Const RFO_WR_GENMAIL1_USERID = &H140011
Global Const RFO_WR_GENMAIL1_MAILADDR = &H140012
Global Const RFO_WR_GENMAIL1_GENMAILTYPE = &H140013
Global Const RFO_WR_GENMAIL1_RESERVED1 = &H140014
Global Const RFO_WR_GENMAIL1_RESERVED2 = &H140015
Global Const RFO_WR_GENMAIL1_FILEFORMAT = &H140016
Global Const RFO_WR_GENMAIL1_OCRERROR = &H140017
Global Const RFO_WR_GENMAIL1_AUTOPRT = &H140018
Global Const RFO_WR_ARCHIVE_DELETEFAX = &H140019
Global Const RFO_WR_ARCHIVE_USERID = &H14001A
Global Const RFO_WR_FILESAVE_DELETEFAX = &H14001B
Global Const RFO_WR_FILESAVE_FILEFORMAT = &H14001C
Global Const RFO_WR_FILESAVE_FILEPATH = &H14001D
Global Const RFO_WR_FILESAVE_USERID = &H14001E
Global Const RFO_WR_FILESAVE_AUTOPRT = &H14001F
Global Const RFO_WR_MAKEFCS_USERID = &H140020
Global Const RFO_WR_VALIDATE_BILLINFO1 = &H140021
Global Const RFO_WR_VALIDATE_BILLINFO2 = &H140022
Global Const RFO_WR_VALIDATE_USERID = &H140023
Global Const RFO_WR_VALIDATE_PHONENUM = &H140024
Global Const RFO_WR_OCR_OCRFLAGS = &H140025
Global Const RFO_WR_OCR_STARTPAGE = &H140026
Global Const RFO_WR_OCR_ENDPAGE = &H140027
Global Const RFO_WR_OCR_LAYOUT = &H140028
Global Const RFO_WR_OCR_FORMAT = &H140029
Global Const RFO_WR_OCR_USERID = &H14002A
Global Const RFO_WR_OCR_OUTPUTNAME = &H14002B
Global Const RFO_WR_OCR_ROUTEDATA_MSMAIL1REQ = &H14002C
Global Const RFO_WR_OCR_ROUTEDATA_CCMAIL1REQ = &H14002D
Global Const RFO_WR_OCR_ROUTEDATA_WPOMAIL1REQ = &H14002E
Global Const RFO_WR_OCR_ROUTEDATA_GENMAIL1REQ = &H14002F
Global Const RFO_WR_OCR_ROUTEDATA_PRINT = &H140030
Global Const RFO_WR_FILEDEL_SOURCEDIR = &H140031
Global Const RFO_WR_FILEDEL_SOURCEFILE1 = &H140032
Global Const RFO_WR_FILEDEL_SOURCEFILE2 = &H140033
Global Const RFO_WR_FILEDEL_SOURCEFILE3 = &H140034
Global Const RFO_WR_FILEDEL_SOURCEFILE4 = &H140035
Global Const RFO_WR_FILEDEL_SOURCEFILE5 = &H140036
Global Const RFO_WR_FILEDEL_SOURCEFILE6 = &H140037
Global Const RFO_WR_FILEDEL_SOURCEFILE7 = &H140038
Global Const RFO_WR_FILEDEL_SOURCEFILE8 = &H140039
Global Const RFO_WR_FILEDEL_SOURCEFILE9 = &H14003A
Global Const RFO_WR_FILEDEL_SOURCEFILE10 = &H14003B
Global Const RFO_WR_FILEDEL_SOURCEFILE11 = &H14003C
Global Const RFO_WR_FILEDEL_SOURCEFILE12 = &H14003D

Global Const RFO_NI10_HANDLE = &H150001
Global Const RFO_NI10_TEXT = &H150002

Global Const RFO_COMPREQ_FLAGS = &H160001
Global Const RFO_COMPREQ_REQUESTTYPE = &H160002
Global Const RFO_COMPREQ_HANDLE = &H160003
Global Const RFO_COMPREQ_DELETEFAX = &H160004

Global Const RFO_VALIDREQ_FLAGS = &H170001
Global Const RFO_VALIDREQ_REQUESTTYPE = &H170002
Global Const RFO_VALIDREQ_HANDLE = &H170003
Global Const RFO_VALIDREQ_BILLINFO1 = &H170004
Global Const RFO_VALIDREQ_BILLINFO2 = &H170005
Global Const RFO_VALIDREQ_USERID = &H170006
Global Const RFO_VALIDREQ_PHONENUM = &H170007

Global Const RFO_AUTOPRT_PRINTERID = &H180001
Global Const RFO_AUTOPRT_STARTPAGE = &H180002
Global Const RFO_AUTOPRT_ENDPAGE = &H180003
Global Const RFO_AUTOPRT_PRINTCOVER = &H180004
Global Const RFO_AUTOPRT_FLAGS = &H180005
Global Const RFO_AUTOPRT_RESOLUTION = &H180006
Global Const RFO_AUTOPRT_SIZE = &H180007
Global Const RFO_AUTOPRT_SOURCE = &H180008
Global Const RFO_AUTOPRT_RESERVED2 = &H180009
Global Const RFO_AUTOPRT_DELETEFAX = &H18000A
Global Const RFO_AUTOPRT_OUTPUTBIN = &H18000B
Global Const RFO_AUTOPRT_JOBTYPE = &H18000C
Global Const RFO_AUTOPRT_PRIORITY = &H18000D
Global Const RFO_AUTOPRT_ACCTCODE = &H18000E
Global Const RFO_AUTOPRT_SECURITYCODE = &H18000F
Global Const RFO_AUTOPRT_DUPLEX = &H180010
Global Const RFO_AUTOPRT_COPIES = &H180011

Global Const RFO_GNFC_TOTALFAXCOUNT = &H190001
Global Const RFO_GNFC_TOTALPAGECOUNT = &H190002
Global Const RFO_GNFC_RECVFAXCOUNT = &H190003
Global Const RFO_GNFC_RECVPAGECOUNT = &H190004
Global Const RFO_GNFC_NEWFAXCOUNT = &H190005
Global Const RFO_GNFC_NEWPAGECOUNT = &H190006
Global Const RFO_GNFC_SENTFAXCOUNT = &H190007
Global Const RFO_GNFC_SENTPAGECOUNT = &H190008
Global Const RFO_GNFC_INPROCESSFAXCOUNT = &H190009
Global Const RFO_GNFC_INPROCESSPAGECOUNT = &H19000A
Global Const RFO_GNFC_FAILEDFAXCOUNT = &H19000B
Global Const RFO_GNFC_FAILEDPAGECOUNT = &H19000C
Global Const RFO_GNFC_SUCCESSFAXCOUNT = &H19000D
Global Const RFO_GNFC_SUCCESSPAGECOUNT = &H19000E

Global Const RFO_GI10_HANDLE = &H1A0001
Global Const RFO_GI10_MEMBERS = &H1A0002
Global Const RFO_GI10_FLAGS = &H1A0003
Global Const RFO_GI10_GROUPID = &H1A0004
Global Const RFO_GI10_ADMIN = &H1A0005
Global Const RFO_GI10_ALT_ADMINT = &H1A0006
Global Const RFO_GI10_GROUP_FCS = &H1A0007
Global Const RFO_GI10_NOTIFY_TYPE = &H1A0008
Global Const RFO_GI10_RESVFAXAGE = &H1A0009
Global Const RFO_GI10_SENTFAXAGE = &H1A000A
Global Const RFO_GI10_DELFAXAGE = &H1A000B
Global Const RFO_GI10_GROUPROUTINGCODE = &H1A000C
Global Const RFO_GI10_SFDTYPE = &H1A000D
Global Const RFO_GI10_SFDFLAGS = &H1A000E
Global Const RFO_GI10_CHANGETAG = &H1A000F
Global Const RFO_GI10_MAXCONCURRENTPAGES = &H1A0010
Global Const RFO_GI10_MAXCONCURRENTFAXES = &H1A0011
Global Const RFO_GI10_CONCURRENTDELAYTIME = &H1A0012
Global Const RFO_GI10_CONCURRENTSTARTTIME = &H1A0013
Global Const RFO_GI10_CONCURRENTENDTIME = &H1A0014
Global Const RFO_GI10_NEWFAXAGE = &H1A0015
Global Const RFO_GI10_FAILEDFAXAGE = &H1A0016
Global Const RFO_GI10_INCSENTFAXAGE = &H1A0017

Global Const RFO_CI_FAX = &H1B0001
Global Const RFO_CI_FIRSTPAGE = &H1B0002
Global Const RFO_CI_LASTPAGE = &H1B0003

Global Const RFO_SI10_HANDLE = &H1C0001
Global Const RFO_SI10_FLAGS = &H1C0002
Global Const RFO_SI10_SIGNID = &H1C0003
Global Const RFO_SI10_SIGNOWNER = &H1C0004
Global Const RFO_SI10_SIGNFILE = &H1C0005
Global Const RFO_SI10_SIGNDESC = &H1C0006
Global Const RFO_SI10_SIGNUSERS = &H1C0007

Global Const RFO_FRMI10_HANDLE = &H1D0001
Global Const RFO_FRMI10_FT_FLAGS = &H1D0002
Global Const RFO_FRMI10_FT_NUM = &H1D0003
Global Const RFO_FRMI10_FT_CODE = &H1D0004
Global Const RFO_FRMI10_FT_NAME = &H1D0005
Global Const RFO_FRMI10_FT_FILENAME = &H1D0006
Global Const RFO_FRMI10_FT_STARTPAGE = &H1D0007
Global Const RFO_FRMI10_FT_NUMPAGES = &H1D0008
Global Const RFO_FRMI10_FT_NEXTTYPE = &H1D0009
Global Const RFO_FRMI10_FT_SECURITYID = &H1D000A

Global Const RFO_BI10_HANDLE = &H1E0001
Global Const RFO_BI10_FLAGS = &H1E0002
Global Const RFO_BI10_BILL_INFO1 = &H1E0003
Global Const RFO_BI10_BILL_INFO2 = &H1E0004
Global Const RFO_BI10_DESCRIPTION = &H1E0005

Global Const RFO_SI2_SERVERTYPE = &H1F0001
Global Const RFO_SI2_SERVERVERSION = &H1F0002
Global Const RFO_SI2_SERVERSPECIAL = &H1F0003
Global Const RFO_SI2_IMAGELOCSERVER = &H1F0004
Global Const RFO_SI2_IMAGELOCVOLUME = &H1F0005
Global Const RFO_SI2_MAXUSERS = &H1F0006
Global Const RFO_SI2_VERIFYCODES = &H1F0007
Global Const RFO_SI2_BILLDESC1 = &H1F0008
Global Const RFO_SI2_BILLDESC2 = &H1F0009
Global Const RFO_SI2_REQFIELDS = &H1F000A
Global Const RFO_SI2_BUILDDATE = &H1F000B
Global Const RFO_SI2_IMAGEDIR = &H1F000C
Global Const RFO_SI2_BASERFDIR = &H1F000D
Global Const RFO_SI2_FLAGS = &H1F000E
Global Const RFO_SI2_ENTERPRISE = &H1F000F
Global Const RFO_SI2_DOCSONDEMAND = &H1F0010
Global Const RFO_SI2_TELECONNECT = &H1F0011
Global Const RFO_SI2_SATELLITE = &H1F0012
Global Const RFO_SI2_SBS = &H1F0013

Global Const RFO_SLE0_HANDLE = &H200001
Global Const RFO_SLE0_SIGNID = &H200002
Global Const RFO_SLE0_SIGNOWNER = &H200003
Global Const RFO_SLE0_SIGNDESC = &H200004
Global Const RFO_SLE0_SIGNUSERS = &H200005

Global Const RFO_FLDRLE0_HANDLE = &H210001
Global Const RFO_FLDRLE0_ID = &H210002
Global Const RFO_FLDRLE0_PARENTID = &H210003
Global Const RFO_FLDRLE0_NAME = &H210004

Global Const RFO_LDLE0_HANDLE = &H220001
Global Const RFO_LDLE0_FLAGS = &H220002
Global Const RFO_LDLE0_OBJECTID = &H220003
Global Const RFO_LDLE0_FILENAME = &H220004
Global Const RFO_LDLE0_DESCRIPTION = &H220005
Global Const RFO_LDLE0_NUMPAGES = &H220006

Global Const RFO_ULE0_HANDLE = &H230001
Global Const RFO_ULE0_USERID = &H230002
Global Const RFO_ULE0_USERNAME = &H230003
Global Const RFO_ULE0_DIDNUMBER = &H230004
Global Const RFO_ULE0_GROUPID = &H230005
Global Const RFO_ULE0_NUMFAXESOWNED = &H230006


Global Const RFO_COMPREQ2_FLAGS = &H240001
Global Const RFO_COMPREQ2_REQUESTTYPE = &H240002
Global Const RFO_COMPREQ2_HANDLE = &H240003
Global Const RFO_COMPREQ2_DELETEFAX = &H240004
Global Const RFO_COMPREQ2_USERID = &H240005

Global Const RFO_SVCI10_HANDLE = &H250001
Global Const RFO_SVCI10_FLAGS = &H250002
Global Const RFO_SVCI10_SERVICEID = &H250003
Global Const RFO_SVCI10_PROVIDER = &H250004
Global Const RFO_SVCI10_TERMINALNUMBER = &H250005
Global Const RFO_SVCI10_SERVICEPASSWORD = &H250006
Global Const RFO_SVCI10_MAXMESSAGELENGTH = &H250007
Global Const RFO_SVCI10_MAXMESSAGES = &H250008
Global Const RFO_SVCI10_SERVICETYPE = &H250009
Global Const RFO_SVCI10_MODEMCONFIG = &H25000A
Global Const RFO_SVCI10_MODEMPORT = &H25000B
Global Const RFO_SVCI10_ATINITSTRING = &H25000C
Global Const RFO_SVCI10_DIALPREFIX = &H25000D
Global Const RFO_SVCI10_MODEMFLOWCONTROL = &H25000E
Global Const RFO_SVCI10_CONNECTSPEED = &H25000F
Global Const RFO_SVCI10_CONNECTTIMEOUT = &H250010
Global Const RFO_SVCI10_TRANSACTIONTIMEOUT = &H250011
Global Const RFO_SVCI10_SMTPSENDER = &H250012

Global Const RFO_SVCLE0_HANDLE = &H260001
Global Const RFO_SVCLE0_FLAGS = &H260002
Global Const RFO_SVCLE0_SERVICEID = &H260003
Global Const RFO_SVCLE0_SERVICETYPE = &H260004

Global Const RFO_MSM1REQ_DELETEFAX = &H270001
Global Const RFO_MSM1REQ_MSGTYPE = &H270002
Global Const RFO_MSM1REQ_MSGTEXT = &H270003
Global Const RFO_MSM1REQ_USERID = &H270004
Global Const RFO_MSM1REQ_NETWORK = &H270005
Global Const RFO_MSM1REQ_POSTOFFICE = &H270006
Global Const RFO_MSM1REQ_PRINT = &H270007
Global Const RFO_MSM1REQ_FILEFORMAT = &H270008
Global Const RFO_MSM1REQ_OCRERROR = &H270009

Global Const RFO_CCM1REQ_DELETEFAX = &H280001
Global Const RFO_CCM1REQ_MSGTYPE = &H280002
Global Const RFO_CCM1REQ_MSGTEXT = &H280003
Global Const RFO_CCM1REQ_USERID = &H280004
Global Const RFO_CCM1REQ_MAILADDR = &H280005
Global Const RFO_CCM1REQ_PRINT = &H280006
Global Const RFO_CCM1REQ_FILEFORMAT = &H280007
Global Const RFO_CCM1REQ_OCRERROR = &H280008

Global Const RFO_WPOM1REQ_DELETEFAX = &H290001
Global Const RFO_WPOM1REQ_MSGTYPE = &H290002
Global Const RFO_WPOM1REQ_MSGTEXT = &H290003
Global Const RFO_WPOM1REQ_USERID = &H290004
Global Const RFO_WPOM1REQ_MAILADDR = &H290005
Global Const RFO_WPOM1REQ_PRINT = &H290006
Global Const RFO_WPOM1REQ_FILEFORMAT = &H290007
Global Const RFO_WPOM1REQ_OCRERROR = &H290008

Global Const RFO_GM1REQ_DELETEFAX = &H2A0001
Global Const RFO_GM1REQ_MSGTYPE = &H2A0002
Global Const RFO_GM1REQ_MSGTEXT = &H2A0003
Global Const RFO_GM1REQ_USERID = &H2A0004
Global Const RFO_GM1REQ_MAILADDR = &H2A0005
Global Const RFO_GM1REQ_GENMAILTYPE = &H2A0006
Global Const RFO_GM1REQ_PRINT = &H2A0007
Global Const RFO_GM1REQ_FILEFORMAT = &H2A0008
Global Const RFO_GM1REQ_OCRERROR = &H2A0009

Global Const RFO_OCRREQ_OCRFLAGS = &H2B0001
Global Const RFO_OCRREQ_STARTPAGE = &H2B0002
Global Const RFO_OCRREQ_ENDPAGE = &H2B0003
Global Const RFO_OCRREQ_LAYOUT = &H2B0004
Global Const RFO_OCRREQ_FORMAT = &H2B0005
Global Const RFO_OCRREQ_USERID = &H2B0006
Global Const RFO_OCRREQ_OUTPUTNAME = &H2B0007
Global Const RFO_OCRREQ_ROUTEDATA_MSMAIL = &H2B0008
Global Const RFO_OCRREQ_ROUTEDATA_CCMAIL = &H2B0009
Global Const RFO_OCRREQ_ROUTEDATA_WPOMAIL = &H2B000A
Global Const RFO_OCRREQ_ROUTEDATA_GENMAIL = &H2B000B
Global Const RFO_OCRREQ_ROUTEDATA_PRINT = &H2B000C

Declare Function RFVB_New Lib "RF2VB.DLL" (ByVal lObjectType As Long, pObject As Long) As Long
Declare Function RFVB_Delete Lib "RF2VB.DLL" (ByVal Object As Long) As Long
Declare Function RFVB_Clear Lib "RF2VB.DLL" (ByVal lObjectType As Long, ByVal Object As Long) As Long
Declare Function RFVB_GetCString Lib "RF2VB.DLL" (ByVal Object As Long, ByVal lMember As Long, ByVal lbuffersize As Long, ByVal lpBuffer As String) As Long
Declare Function RFVB_GetInteger Lib "RF2VB.DLL" (ByVal Object As Long, ByVal lMember As Long, ReturnInt As Integer) As Long
Declare Function RFVB_GetLong Lib "RF2VB.DLL" (ByVal Object As Long, ByVal lMember As Long, ReturnLong As Long) As Long
Declare Function RFVB_GetBString Lib "RF2VB.DLL" (ByVal Object As Long, ByVal lMember As Long) As String
Declare Function RFVB_GetStruct Lib "RF2VB.DLL" (ByVal Object As Long, ByVal lMember As Long, ByVal DestObject As Long) As Long
Declare Function RFVB_GetByte Lib "RF2VB.DLL" (ByVal Object As Long, ByVal lMember As Long, ReturnByte As Byte) As Long
Declare Function RFVB_SetBString Lib "RF2VB.DLL" (ByVal Object As Long, ByVal lMember As Long, bString As String) As Long
Declare Function RFVB_SetInteger Lib "RF2VB.DLL" (ByVal Object As Long, ByVal lMember As Long, ByVal SrcInt As Integer) As Long
Declare Function RFVB_SetLong Lib "RF2VB.DLL" (ByVal Object As Long, ByVal lMember As Long, ByVal SrcLong As Long) As Long
Declare Function RFVB_SetStruct Lib "RF2VB.DLL" (ByVal Object As Long, ByVal lMember As Long, ByVal SrcObject As Long) As Long
Declare Function RFVB_SetByte Lib "RF2VB.DLL" (ByVal Object As Long, ByVal lMember As Long, ByVal SrcByte As Byte) As Long

Declare Function RFVB_BStringToHandle Lib "RF2VB.DLL" (bString As String) As Long

' RFVB_FillNoteInfo10() is a helper function which initializes a NOTEINFO_10 object from a
' text buffer.  The function will word wrap as needed and can take single line or multi-line
' text.
Declare Function RFVB_FillNoteInfo10 Lib "RF2VB.DLL" (ByVal lNI10Object As Long, sText As String) As Long

' RFVB_QueueFile() is equivalent to copying or sending a file to the HPFAX queue.
' Unlike sending a file to the HPFAX queue, no SMB connectivity is needed, thus the
' MS Requester is not needed and it is not necessary to give anyone NT access to the
' fax server machine.  The file sent may, and usually should, include RightFAX Embedded Codes
' to control the fax.
Declare Function RFVB_QueueFile Lib "RF2VB.DLL" (ByVal hServer As Long, sUserID As String, sFile As String) As Long

' RFVB_QueueFiles() is equivalent to concatenating and copying files to the HPFAX queue.
' Unlike sending a file to the HPFAX queue, no SMB connectivity is needed, thus the
' MS Requester is not needed and it is not necessary to give anyone NT access to the
' fax server machine.  The file sent may, and usually should, include RightFAX Embedded Codes
' to control the fax.
Declare Function RFVB_QueueFiles Lib "RF2VB.DLL" (ByVal hServer As Long, sUserID As String, aFiles() As String) As Long

' RFVB_SendFiles1() is useful in sending a list of files where a large degree of control is necessary.
' A FaxInfo_10 object must be created and initialized (with RFaxInitFax() or RFaxInitFax2()).
' The files supplied in the aFiles array can be of any type supported by the RightFAX server:
' Word, Excel, PowerPoint, PCL-5, PostScript, PDF, TIFF, PCX, DCX, BMP, GIF, JPG, Text, etc.
' Please note that none of the files specified in aFiles() will be scanned for any embedded codes.
' This is due to the fact that all fax information is expected to be supplied in the FaxInfo_10 object
' and any embedded codes would be ambigous.
Declare Function RFVB_SendFiles1 Lib "RF2VB.DLL" (ByVal hServer As Long, _
                                                  ByVal lFI10Object As Long, _
                                                  ByVal lNI10Object As Long, _
                                                  ByVal fSendWithCover As Boolean, _
                                                  sCoverSheetModel As String, _
                                                  aFiles() As String, _
                                                  hNewFax As Long) As Long

Declare Function RFVB_StringFaxStatus Lib "RF2VB.DLL" (ByVal lFLE0Object As Long, usStatusType As Integer) As String

'===============================================================================================================
' RFVB_SendFiles2() is a function callable from Visual Basic which allows one to send one or more
' files to one or more destinations.  The files to be sent must be in a format convertable by
' RightFAX (PCL, PDF, PostScript, Text, ASCII, Word, Excel, PowerPoint, TIFF, JPG, etc.).  The files
' may be located on a local or network drive (as seen from the caller's context) as the function
' will take care of all file transport to the fax server (only a TCP/IP or SPX connection is necessary).
' Parameters:
'   hServer: handle to a fax server created with the RFaxCreateServerHandle API (see RFWIN32.BAS)
'   sUserID: RightFAX User ID of the user who is sending the fax.  The fax(es) are sent through
'            this user's mailbox and the fax(es) inherit the default faxing properties for this
'            user.  The user must already exist in RightFAX prior to the call.
'   sFCSNotes: Notes to be placed on the cover sheet, if cover sheets are enabled.  The notes
'              may be a maximum of 1400 characters.
'   fSendWithCover: Use True to cause the fax server to create cover sheets for each fax.  Use
'                   False if the faxes should be sent without a server generated cover sheet.
'   sCoverSheetModel: String in the form, "FILENAME.PCL".  If supplied and if fSendWithCover is True,
'                     then this file (assumed to already exist in the FCS directory on the RightFAX
'                     server) will be used as the cover sheet model.  If fSendWithCover is True and
'                     this parameter is an empty string, e.g. "", then the default cover sheet model
'                     for the user will be utilized.
'   aFiles: Array of file names.  Each file name may include a path, either local or network; however,
'           we recommend that all files have a complete path to eliminate current directory ambigiouities.
'   aDestinations: Array of SENDFILES2_DEST UDT's.  Each UDT should include at least a psToName and psToFaxNum.
'                  Additional members of the UDT may be required by the server as defined by the server's
'                  administrator.
' The following is sample code for calling the function:
'   Dim aDest(2) As SENDFILES2_DEST
'   Dim aFiles(1) As String
'   Dim hServer As Long
'   Dim lError As Long
'   Dim hNewFax As Long
'
'   aDest(0).psToName = "John"
'   aDest(0).psToFaxNum = "555-1234"
'   aDest(1).psToName = "Bob"
'   aDest(1).psToFaxNum = "555-1234"
'   aDest(2).psToName = "Dick"
'   aDest(2).psToFaxNum = "555-1234"
'   aFiles(0) = "d:\temp\ctest2.pcl"
'   aFiles(1) = "d:\temp\ctest3.pcl"
'   hServer = RFaxCreateServerHandle("ROCKY", COMMPROTOCOL_TCPIP, lError)
'   lError = RFVB_SendFiles2(hServer, "JJC", Text1.Text, True, "", aFiles(), aDest(), hNewFax)
'   RFaxCloseServerHandle (hServer)

Type SENDFILES2_DEST
   psToName As String
   psToFaxNum As String
   psToCompany As String
   psToContactNum As String
   psToCityState As String
   psBillInfo1 As String
   psBillInfo2 As String
   psUniqueID As String
   psSecureCSID As String
   fHideFromCCList As Integer
End Type

Declare Function RFVB_SendFiles2 Lib "RF2VB.DLL" (ByVal hServer As Long, _
                                                  sUserID As String, _
                                                  sFCSNotes As String, _
                                                  ByVal fSendWithCover As Boolean, _
                                                  sCoverSheetModel As String, _
                                                  aFiles() As String, _
                                                  aDestinations() As SENDFILES2_DEST) As Long
