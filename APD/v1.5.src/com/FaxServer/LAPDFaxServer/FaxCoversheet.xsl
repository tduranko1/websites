<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="UTF-8"/>
  
  <xsl:template match="/">
    <html>
      <head>
        <title>Fax Coversheet</title>
        <style>
         .header {
            background-color: #000000;
            Font: 16pt Arial Black, Arial, Helvetica, sans-serif;
            color: #FFFFFF;
         }
         td {
            font: 10pt Arial, Helvetica, sans-serif;
         }
      </style>
      </head>
      <body>
        <table border="0" cellpadding="0" cellspacing="0" style="width:100%;padding:0.25cm">
          <tr>
            <td style="width:3.5in"/>
            <td class="header" style="width:*">
              <div style="width:3in;">LYN<font color="#FF0000">X</font> Services
              </div>
            </td>
          </tr>
        </table>
        <p style="font:54pt Arial Black, Arial, Helvetica, sans-serif;">Fax</p>
        <div style="margin:0.5in;margin-top:0.1in">
        <table border="1" cellpadding="5" cellspacing="0" style="width:7in;align:center;border-collapse:collapse;">
          <tr>
            <td style="font:10pt Arial Black, Arial, Helvetica, sans-serif; width:0.25in; border:0px; border-bottom:1px solid #000000">To:</td>
            <td style="width:3.25in;border:0px; border-bottom:2px solid #000000">
              <xsl:value-of select="/ClientDocument/Send/@recipient"/>
            </td>
            <td style="font:10pt Arial Black, Arial, Helvetica, sans-serif; width:0.25in; border:0px; border-bottom:1px solid #000000">From:</td>
            <td style="width:3.25in;border:0px; border-bottom:2px solid #000000">
              <xsl:value-of select="/ClientDocument/Send/@from"/>
            </td>
          </tr>
          <tr>
            <td style="font:10pt Arial Black, Arial, Helvetica, sans-serif; border:0px; border-bottom:1px solid #000000">Fax:</td>
            <td style="border:0px; border-bottom:2px solid #000000">
              <xsl:value-of select="/ClientDocument/Send/@value"/>
            </td>
            <td style="font:10pt Arial Black, Arial, Helvetica, sans-serif; border:0px; border-bottom:1px solid #000000">Date:</td>
            <td style="border:0px; border-bottom:2px solid #000000">
              <xsl:value-of select="/ClientDocument/Send/@date"/>
            </td>
          </tr>
        </table>
        <p style="font:10pt Arial Black, Arial, Helvetica, sans-serif;">Comments:</p>
        <p style="font:10pt Arial Black, Arial, Helvetica, sans-serif;">
          <table border="0" cellpadding="0" cellspacing="0" style="width:80%;align:center;border-collapse:collapse">
            <tr>
              <td style="font-weight:bold;width:1.5in">Claim #:</td>
              <td>
                <xsl:value-of select="/ClientDocument/Claim/@ClientClaimNumber"/>
              </td>
            </tr>
            <tr>
              <td style="font-weight:bold;width:1.5in">Client Adjuster:</td>
              <td>
                <xsl:value-of select="/ClientDocument/Claim/@Adjuster"/>
              </td>
            </tr>
            <tr>
              <td style="font-weight:bold">Insured:</td>
              <td>
                <xsl:value-of select="/ClientDocument/Claim/@InsuredName"/>
              </td>
            </tr>
            <tr>
              <td style="font-weight:bold">Loss Date:</td>
              <td>
                <xsl:value-of select="/ClientDocument/Claim/@LossDate"/>
              </td>
            </tr>
            <tr>
              <td style="font-weight:bold">LYNX ID:</td>
              <td>
                <xsl:value-of select="concat(/ClientDocument/Claim/@LynxID, '-', /ClientDocument/Vehicle/@number)"/>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <br/>
                <br/>
                <br/>
                <font style="font-weight:bold">Documents attached:</font>
                <br/>
                <xsl:value-of select="/ClientDocument/AttachmentsHTML" disable-output-escaping="yes"/>
              </td>
            </tr>
          </table>
        </p>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>