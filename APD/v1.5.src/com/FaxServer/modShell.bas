Attribute VB_Name = "modShell"
Option Explicit
Option Compare Text

Private Const INFINITE = &HFFFF
Private Const STARTF_USESHOWWINDOW = &H1

Private Const STATUS_ABANDONED_WAIT_0 As Long = &H80
Private Const STATUS_WAIT_0 As Long = &H0
Private Const STATUS_USER_APC As Long = &HC0

Private Const WAIT_ABANDONED As Long = (STATUS_ABANDONED_WAIT_0 + 0)
Private Const WAIT_ABANDONED_0 As Long = (STATUS_ABANDONED_WAIT_0 + 0)
Private Const WAIT_FAILED As Long = &HFFFFFFFF
Private Const WAIT_IO_COMPLETION As Long = STATUS_USER_APC
Private Const WAIT_OBJECT_0 As Long = (STATUS_WAIT_0 + 0)
Private Const WAIT_TIMEOUT As Long = 258&

Public Const SHELL_TIMEOUT As Long = 60000 '1 minute

Public Enum enSW
    SW_HIDE = 0
    SW_NORMAL = 1
    SW_MAXIMIZE = 3
    SW_MINIMIZE = 6
End Enum

Public Type PROCESS_INFORMATION
    hProcess As Long
    hThread As Long
    dwProcessId As Long
    dwThreadId As Long
End Type

Public Type STARTUPINFO
    cb As Long
    lpReserved As String
    lpDesktop As String
    lpTitle As String
    dwX As Long
    dwY As Long
    dwXSize As Long
    dwYSize As Long
    dwXCountChars As Long
    dwYCountChars As Long
    dwFillAttribute As Long
    dwFlags As Long
    wShowWindow As Integer
    cbReserved2 As Integer
    lpReserved2 As Byte
    hStdInput As Long
    hStdOutput As Long
    hStdError As Long
End Type

Public Type SECURITY_ATTRIBUTES
    nLength As Long
    lpSecurityDescriptor As Long
    bInheritHandle As Long
End Type

Public Enum enPriority_Class
    NORMAL_PRIORITY_CLASS = &H20
    IDLE_PRIORITY_CLASS = &H40
    HIGH_PRIORITY_CLASS = &H80
End Enum

Private Declare Function CreateProcess Lib "kernel32" Alias "CreateProcessA" (ByVal lpApplicationName As String, ByVal lpCommandLine As String, lpProcessAttributes As SECURITY_ATTRIBUTES, lpThreadAttributes As SECURITY_ATTRIBUTES, ByVal bInheritHandles As Long, ByVal dwCreationFlags As Long, lpEnvironment As Any, ByVal lpCurrentDriectory As String, lpStartupInfo As STARTUPINFO, lpProcessInformation As PROCESS_INFORMATION) As Long
Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

'usage:     SuperShell filefullpath, filepath, 0, SW_NORMAL, HIGH_PRIORITY_CLASS
Public Function SuperShell(ByVal App As String, ByVal WorkDir As String, dwMilliseconds As Long, ByVal start_size As enSW, ByVal Priority_Class As enPriority_Class) As Boolean
          Dim pclass As Long
          Dim sinfo As STARTUPINFO
          Dim pinfo As PROCESS_INFORMATION
          'Not used, but needed
          Dim sec1 As SECURITY_ATTRIBUTES
          Dim sec2 As SECURITY_ATTRIBUTES
          'Set the structure size
10        sec1.nLength = Len(sec1)
20        sec2.nLength = Len(sec2)
30        sinfo.cb = Len(sinfo)
          'Set the flags
40        sinfo.dwFlags = STARTF_USESHOWWINDOW
          'Set the window's startup position
50        sinfo.wShowWindow = start_size
          'Set the priority class
60        pclass = Priority_Class
          'Start the program
70        If CreateProcess(vbNullString, App, sec1, sec2, False, pclass, 0&, WorkDir, sinfo, pinfo) Then
              'Wait
80            WaitForSingleObject pinfo.hProcess, dwMilliseconds
90            SuperShell = True
100       Else
110           SuperShell = False
120       End If
End Function


Public Function PDF2Tif(strFileName As String, strGhostscriptPath As String) As String
          Const PROC_NAME As String = "PDF2Tif()"
10        On Error GoTo errHandler
          Dim objFSO As Scripting.FileSystemObject
          Dim bRet As Boolean
          Dim strRet As String
          Dim strOutputFileName As String
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Dim strPDFFilledFile As String
          
20        strRet = "" 'convertedFileExists(strFileName, "pdf")
          
30        Set objFSO = New Scripting.FileSystemObject
          
40        If g_blnDebugMode Then g_objEvents.Trace "  PDF File to convert = " & strFileName, PROC_NAME
          
50        If objFSO.FileExists(strFileName) Then
60            strOutputFileName = getSafeFileName(objFSO.BuildPath(objFSO.GetParentFolderName(strFileName), objFSO.GetBaseName(strFileName) & ".tif"))
70            strBatchFile = objFSO.BuildPath(objFSO.GetParentFolderName(strFileName), objFSO.GetBaseName(strFileName) & ".bat")
              
80            If g_blnDebugMode Then g_objEvents.Trace "  Output File Name = " & strOutputFileName & vbCrLf & _
                                                       "  Batch File: " & strBatchFile, PROC_NAME
              
90            Set oBatFile = objFSO.OpenTextFile(strBatchFile, ForWriting, True)
100           oBatFile.WriteLine """" & strGhostscriptPath & """ -dNOPAUSE -dBATCH -q -sDEVICE=tiffg4 -sPAPERSIZE=letter -r200 -sOutputFile=""" & strOutputFileName & """ """ & strFileName & """"
110           oBatFile.Close
120           Set oBatFile = Nothing
              
              'execute the batch file
130           bRet = SuperShell(strBatchFile, objFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
              
140           DoEvents
              'Sleep 1000
              
              'cleanup
150           objFSO.DeleteFile strBatchFile
              
160           If objFSO.FileExists(strOutputFileName) Then
170               If g_blnDebugMode Then g_objEvents.Trace "  Conversion Success " & strOutputFileName, PROC_NAME
180               strRet = strOutputFileName
190           Else
200               If g_blnDebugMode Then g_objEvents.Trace "  Conversion failed. ", PROC_NAME
210               strRet = ""
220           End If
230       Else
240           bRet = False
250       End If
          
          'if bDeleteOriginal Then safeDelete strFileName
          
errHandler:
260       Set objFSO = Nothing
270       If Err.Number <> 0 Then
280           If g_blnDebugMode Then g_objEvents.Trace _
                              "Cannot convert pdf file to tif " & strFileName & vbCrLf, PROC_NAME
290           strRet = ""
300       End If
310       PDF2Tif = strRet
End Function

Public Function getSafeFileName(strFileName As String) As String
          Dim iCount As Integer
          Dim strFolderPath As String
          Dim strFileBaseName As String
          Dim strFileExt As String
          Dim strRet As String
          Dim objFSO As New Scripting.FileSystemObject
          
10        If strFileName <> "" Then
20            iCount = 1
30            strFolderPath = objFSO.GetParentFolderName(strFileName)
40            strFileBaseName = objFSO.GetBaseName(strFileName)
50            strFileExt = objFSO.GetExtensionName(strFileName)
60            strRet = strFileName
70            While objFSO.FileExists(strRet)
80                strRet = objFSO.BuildPath(strFolderPath, strFileBaseName & "_" & CStr(iCount))
90                If strFileExt <> "" Then
100                   strRet = strRet & "." & strFileExt
110               End If
120               iCount = iCount + 1
130           Wend
140       End If
150       getSafeFileName = strRet
160       Set objFSO = Nothing
End Function

Public Function flattenForm(strPDFFile As String, strPDFTKPath As String) As String
10        On Error GoTo errHandler
          Const PROC_NAME As String = "flattenForm()"
          Dim strRet As String
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Dim objFSO As New FileSystemObject
          Dim strTempFolderPath As String
          Dim strTempFile As String
          
20        strTempFolderPath = objFSO.GetSpecialFolder(TemporaryFolder)
30        strTempFile = objFSO.GetBaseName(objFSO.GetTempName())
          
40        strRet = getSafeFileName(objFSO.BuildPath(objFSO.GetParentFolderName(strPDFFile), objFSO.GetBaseName(strPDFFile) & "_flat.pdf"))
50        strBatchFile = objFSO.BuildPath(strTempFolderPath, strTempFile & ".bat")
          
60        Set oBatFile = objFSO.OpenTextFile(strBatchFile, ForWriting, True)
70        oBatFile.Write """" & strPDFTKPath & """ """ & strPDFFile & """ output """ & strRet & """ flatten dont_ask"
80        oBatFile.Close
90        Set oBatFile = Nothing

100       SuperShell strBatchFile, objFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS
          
110       Kill strBatchFile
          
120       DoEvents
          
130       If objFSO.FileExists(strRet) = False Then strRet = ""
          
140       flattenForm = strRet
errHandler:
150       If Err.Number <> 0 Then
160           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                              "PDF File = '" & strPDFFile & "'"
170       End If
End Function
