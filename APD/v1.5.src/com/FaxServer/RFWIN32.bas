Attribute VB_Name = "MRFWIN32"
' Returns from RFax??? API calls (RFWIN32.DLL)
Global Const RFAX_SUCCESS = 0&
Global Const RFAXERR_NOTFOUND = 2&
Global Const RFAXERR_PATHNOTFOUND = 3&
Global Const RFAXERR_INVALID_HANDLE = 6&         ' Invalid RFAXSERVERHANDLE passed to function
Global Const RFAXERR_NOMEM = 8&
Global Const RFAXERR_INVALID_PARM = 87&
Global Const RFAXERR_INVALID_OWNERID = 10000&
Global Const RFAXERR_CANNOTCREATEFILE = 10001&
Global Const RFAXERR_CANNOTOPENIMAGE = 10002&
Global Const RFAXERR_BADRFG3FORMAT = 10003&
Global Const RFAXERR_IOERROR = 10004&
Global Const RFAXERR_LIST_EMPTY = 10005&
Global Const RFAXERR_LIST_END = 10006&
Global Const RFAXERR_INVALID_USERID = 10007&
Global Const RFAXERR_USER_ABORTED = 10008&
Global Const RFAXERR_IPCINITFAILURE = 10009&     ' RFIPC???.NDR initialization failed
Global Const RFAXERR_IPCSETNAMEFAILURE = 10010&  ' RFIPC???.NDR SetServerName failed
Global Const RFAXERR_NDRNOTFOUND = 10011&        ' The RFIPC???.NDR cannot be found or loaded (dependant DLL missing?)
Global Const RFAXERR_NOTSUPPORTED = 10012&       ' The API call made is not supported on the current platform
Global Const RFAXERR_USERNOTEMPTY = -1012&       ' Can't delete user because they have phonebook entries or faxes
Global Const RFAXERR_NOUSERS = -1013&
Global Const RFAXERR_CANTDELETE = -1014&         ' Can't delete ADMIN or SUPERVISOR
Global Const RFAXERR_NOTCOMPLETE = -1015&        ' Can't delete received fax because it's not complete
Global Const RFAXERR_GROUPNOTEMPTY = -1021&      ' Can't delete group because it has members
Global Const RFAXERR_NOTPHONEENT = -1032&
Global Const RFAXERR_NOTPHONELIST = -1033&
Global Const RFAXERR_BLANKUSERID = -1034&        ' Can't save a phone entry with an empty User ID
Global Const RFAXERR_BLANKENTRYID = -1035&       ' Can't save a phone entry with an empty Entry ID
Global Const RFAXERR_CANTFORWARD = -1040&
Global Const RFAXERR_BADENUM = -9900&
Global Const RFAXERR_ACCESSDENIED = -9901&       ' Non-administrative user attempting to login to FaxAdmin
Global Const RFAXERR_BADPASSWORD = -9902&        ' User ID valid, password invalid
Global Const RFAXERR_BADKEY = -9903&             ' User ID, Printer ID, etc. not found
Global Const RFAXERR_BADHANDLE = -9904&          ' Invalid object handle (FAXHANDLE), cannot be NULL
Global Const RFAXERR_DUPKEY = -9905&
Global Const RFAXERR_BADPARM = -9906&
Global Const RFAXERR_FAXDELETED = -9907&
Global Const RFAXERR_COPYFAILED = -9908&
Global Const RFAXERR_OVERUSERLIMIT = -9909&
Global Const RFAXERR_LOWMEM = -9998&
Global Const RFAXERR_UNKNOWN = -9999&

' Constants used with the RFAX_USERINFO_10.userflags field
Global Const USERFLAGS_ADMIN = &H1&
Global Const USERFLAGS_CANSHELL = &H2&               ' Used to be PURGE
Global Const USERFLAGS_CANUSEHIGH = &H10&
Global Const USERFLAGS_MARKED = &H20&
Global Const USERFLAGS_UNPROTECTED = &H40&           ' Indicates that mailbox is unprotected
Global Const USERFLAGS_DONTVERIFY = &H80&            ' Indicates that user is exempt from code verification
Global Const USERFLAGS_DEFAULTFINE = &H100&          ' Set to indicate that the default mode for this user is fine mode
Global Const USERFLAGS_VIEWFIRSTONLY = &H200&        ' Set to restrict viewing to first page only
Global Const USERFLAGS_AUTOPRINT = &H400&
Global Const USERFLAGS_COVERPAGE = &H800&
Global Const USERFLAGS_AUTOFORWARD = &H1000&
Global Const USERFLAGS_AUTOUPDATE = &H2000&
Global Const USERFLAGS_ALTNOTIFY = &H4000&
Global Const USERFLAGS_FINECOVER = &H8000&           ' Set to indicate that cover pages are done in fine mode
Global Const USERFLAGS_AUTOPURGE = &H10000           ' Set to indicate that this user will not have a fax_deleted_set
Global Const USERFLAGS_MACINTOSH = &H20000           ' Set to indicate that this is a Macintosh mailbox
Global Const USERFLAGS_MUSTHAVEPASS = &H40000        ' Set to indicate that user must have non-black password
Global Const USERFLAGS_ARCHIVEON = &H80000           ' Set to turn on archive
Global Const USERFLAGS_CANCHGCOVERS = &H100000       ' Set to indicate that user can change their cover sheet
Global Const USERFLAGS_AUTOOCR = &H200000
Global Const USERFLAGS_CANOCR = &H400000
Global Const USERFLAGS_NODELETE = &H800000           ' If set, then deleting from FaxUtil doesn't work

' Constants used with the RFO_UI10_ROUTETYPE member
Global Const USER_ROUTETYPE_RF = 0
Global Const USER_ROUTETYPE_CCMAIL = 1
Global Const USER_ROUTETYPE_MSMAIL = 2
Global Const USER_ROUTETYPE_MHS = 3
Global Const USER_ROUTETYPE_FILE = 4
Global Const USER_ROUTETYPE_OCR = 5
Global Const USER_ROUTETYPE_WPOFF = 6
Global Const USER_ROUTETYPE_WPOMAIL = 6
Global Const USER_ROUTETYPE_NOTES = 7

' Constants used in the RFO_UI10_ROUTEFMT member
Global Const USER_ROUTEFMT_DCX = 0
Global Const USER_ROUTEFMT_PCX = 1
Global Const USER_ROUTEFMT_TIFF1 = 2
Global Const USER_ROUTEFMT_TAG1 = 3
Global Const USER_ROUTEFMT_TIFF1OCR = 4
Global Const USER_ROUTEFMT_TAG1OCR = 5

' Constants used with the RFO_UI10_USAUTOPRINTFLAGS member (Note that USERFLAGS_AUTOPRINT is still
' used for auto-printing of received faxes).
Global Const USERAPFLAGS_PRINTSENT = &H1       ' Auto-print of sent faxes enabled to printer ID userrec.szAutoSentPrinter
Global Const USERAPFLAGS_SENTFCS = &H2         ' Print coversheet of sent faxes
Global Const USERAPFLAGS_SENTBODY = &H4        ' Print body of sent faxes
Global Const USERAPFLAGS_SENTTRX = &H8         ' Print transmission history of sent faxes
Global Const USERAPFLAGS_RECVFCS = &H10        ' Print coversheet (First page) of received faxes
Global Const USERAPFLAGS_RECVBODY = &H20       ' Print body (pages 2 and on) of received faxes
Global Const USERAPFLAGS_RECVTRX = &H40        ' Print transmission history of received faxes
Global Const USERAPFLAGS_SENTFAILED = &H80     ' Print when send fails and goes to ED:xxx state
Global Const USERAPFLAGS_SENTSUCCESS = &H100   ' Print when send succeeds

' Constants for use with the RFO_UI10_SEND_NOTIFY member
Global Const SEND_NOTIFY_NEEDS_FCS1 = &H1     ' send msg only for first time needs_fcs
Global Const SEND_NOTIFY_NEEDS_FCS2 = &H2     ' send needs_fcs msg periodically
Global Const SEND_NOTIFY_SENDING1 = &H4       ' send msg about sending first time only
Global Const SEND_NOTIFY_SENDING2 = &H8       ' send msg about sending periodically
Global Const SEND_NOTIFY_SENDRETRY = &H10     ' send msg about sending errors & retry
Global Const SEND_NOTIFY_SENDOK = &H20        ' send msg about sending OK
Global Const SEND_NOTIFY_SENDERR = &H40       ' send msg about sending errors & abandon

' Constants for use with the RFO_UI10_RCV_NOTIFY member
Global Const RCV_NOTIFY_NEW1 = 1         ' send msg only for first time newfax
Global Const RCV_NOTIFY_NEW2 = 2         ' send chklook msg periodically

' Constants for use with the RFO_UI10_NOTIFYTYPE member and the RFaxGetMessageEvent function
Global Const NOTIFY_TYPE_NETBROADCAST = 0
Global Const NOTIFY_TYPE_CUSTOM1 = 1
Global Const NOTIFY_TYPE_CUSTOM2 = 2
Global Const NOTIFY_TYPE_CUSTOM3 = 3
Global Const NOTIFY_TYPE_CUSTOM4 = 4
Global Const NOTIFY_TYPE_CUSTOM5 = 5
Global Const NOTIFY_TYPE_CUSTOM6 = 6
Global Const NOTIFY_TYPE_CUSTOM7 = 7
Global Const NOTIFY_TYPE_CUSTOM8 = 8
Global Const NOTIFY_TYPE_CUSTOM9 = 9
Global Const NOTIFY_TYPE_CCMAIL = 10
Global Const NOTIFY_TYPE_MSMAIL = 11
Global Const NOTIFY_TYPE_GROUPWISE = 12
Global Const NOTIFY_TYPE_PAGER = 13
Global Const NOTIFY_TYPE_TRS = 14
Global Const NOTIFY_TYPE_CX3 = 15
Global Const NOTIFY_TYPE_ANY = &HFFFF

' Constants used to work with the RFO_FI10_FRFLAGS member
Global Const FAXFLAG_AUTOFORWARDED = &H1               ' set when the faxrec is autoforarded outside the system
Global Const FAXFLAG_HELD = &H2                        ' set when the <HOLD> code is prescanned
Global Const FAXFLAG_VIEWED = &H4                      ' set once the fax has been viewed
Global Const FAXFLAG_DELETED = &H8                     ' set if this fax belongs to a user's delete fax set, i.e. it is deleted
Global Const FAXFLAG_RECEIVED = &H10                   ' Set if this is a received fax.
Global Const FAXFLAG_DONT_AUDIT = &H20                 ' set for faxes forwarded within the network or on faxes deleted before they were sent
Global Const FAXFLAG_INITIALIZED = &H40                ' Set the first time we edit the FCS.
Global Const FAXFLAG_RESERVED1 = &H80                  ' Do not use
Global Const FAXFLAG_COMPLETEFCS = &H100               ' Set by server when FCS is considered complete.
Global Const FAXFLAG_WAS_FORWARDED = &H200             ' Set when fax is forwarded, cleared after first FCS edit
Global Const FAXFLAG_DELAYSEND = &H400                 ' set when the this is a delaysend fax
Global Const FAXFLAG_NOCOVER = &H800                   ' set when no cover page is desired
Global Const FAXFLAG_AUTODELETE = &H1000               ' set when user wants fax deleted after successful send
Global Const FAXFLAG_PRINTED = &H2000                  ' set once the fax has been printed
Global Const FAXFLAG_NEEDS_FCSCVT = &H4000             ' set if the fax needs a new FCS
Global Const FAXFLAG_RESERVED2 = &H8000                ' Do not use
Global Const FAXFLAG_AUTODELETEALL = &H10000           ' set when user wants fax deleted after successful OR unsuccessful send
Global Const FAXFLAG_BILLCODES_VERIFIED = &H20000      ' Set when the billing info has been validated
Global Const FAXFLAG_FINECOVER = &H40000               ' Set when the cover sheet should be imaged in fine mode
Global Const FAXFLAG_GATEWAYFAX = &H80000              ' Set when the fax was generated by the mail gateway
Global Const FAXFLAG_MULTICODES = &H100000             ' Set when an <NEWDEST> code was encountered
Global Const FAXFLAG_NEEDPHONECHK = &H200000           ' Set when the fax fields need be checked for phonebook expansion
Global Const FAXFLAG_SMARTRESUME = &H400000            ' Set when the fax should use partial-retries
Global Const FAXFLAG_HASTRXNOTES = &H800000            ' Set when a fax has notes in a Forward or Route Trx entry
Global Const FAXFLAG_WASAPPROVED = &H1000000           ' Set when a fax has been approved for sending
Global Const FAXFLAG_NEEDSAPPROVAL = &H2000000         ' Set when a fax needs apporval to be sent
Global Const FAXFLAG_GENERIC1 = &H4000000              ' Application specific flag
Global Const FAXFLAG_GENERIC2 = &H8000000              ' Application specific flag
Global Const FAXFLAG_COMPLETEEVENT = &H10000000        ' fire off a COMPLETE EVENT when this fax completes successfully or is abandoned
Global Const FAXFLAG_BROADCAST = &H20000000            ' handle as a broadcast fax
Global Const FAXFLAG_RESERVED3 = &H40000000            ' Do not use

' Constants used to work with the RFO_FI10_FAX_ERROR_CODE member
Global Const FAXERROR_NONE = 0          ' OK
Global Const FAXERROR_BUSY = 1          ' fax number busy
Global Const FAXERROR_TRXERROR = 2      ' trx/rx error
Global Const FAXERROR_POORQUALITY = 3   ' faxboard said it was rx'd w/poor quality
Global Const FAXERROR_NOANSWER = 4      ' no answer at fax number
Global Const FAXERROR_BADFCS = 5        ' bad FCS information
Global Const FAXERROR_BADCONVERT = 6    ' error occurred during conversion of body or FCS
Global Const FAXERROR_MAKEFCS = 7       ' error occurred during creation of FCS text
Global Const FAXERROR_CANTSCHD = 8      ' can't schedule for some reason
Global Const FAXERROR_UNKNOWN = 9       ' unknown error reported by fax board
Global Const FAXERROR_HUMAN = 10        ' Human answered the phone
Global Const FAXERROR_GROUP2 = 11       ' Reached a G2 fax machine
Global Const FAXERROR_LOCALINUSE = 12   ' Dest Unable to Receive
Global Const FAXERROR_LINEPROBLEM = 13  ' problem on the line
Global Const FAXERROR_BADPAPER = 14     ' invalid formtype specified
Global Const FAXERROR_BADSIG = 15       ' invalid signature used
Global Const FAXERROR_NOSIGAUTH = 16    ' no authorization for attempted signature usage
Global Const FAXERROR_UNDEF_17 = 17     ' undefined!
Global Const FAXERROR_DISCARDED = 18
Global Const FAXERROR_BADPHONE = 19     ' Invalid characters in phone number
Global Const FAXERROR_UNDEF_20 = 20     ' undefined!
Global Const FAXERROR_INVALIDCODE = 21  ' User supplied an invalid billing code
Global Const FAXERROR_BADCODE = 22      ' Error in an embedded code
Global Const FAXERROR_BADOCR = 23       ' OCR operation failed
Global Const FAXERROR_BADPRINT = 24     ' Print operation failed
Global Const FAXERROR_NOLIBAUTH = 25    ' Unauthorized user attempted NEWLIB
Global Const FAXERROR_VIEWSTAR1 = 26    ' ViewStar (Imaging system) import error
                                  
' Constants used to work with the RFAX_FAXINFO_10.fax_status field
Global Const FAXSTAT_UNBORN = 0
Global Const FAXSTAT_NEEDS_FCS = 1
Global Const FAXSTAT_NEEDS_CONVERSION = 2
Global Const FAXSTAT_NEEDS_TOBESENT = 3
Global Const FAXSTAT_IN_CONVERSION = 4
Global Const FAXSTAT_IN_SEND = 5            ' usPagesInFront indicates the percentage of pages sent so far
Global Const FAXSTAT_DONE_OK = 6            ' SUCCESSFULLY SENT OR RECEIVED
Global Const FAXSTAT_MANUALFCS = 7
Global Const FAXSTAT_IN_SCHEDULE = 8        ' usPagesInFront indicates how many pages back in the queue this fax is
Global Const FAXSTAT_DONE_ERROR = 9         ' Too many errors, will NOT be retried
Global Const FAXSTAT_DUPLICATE = 10         ' Set on a network forwarded fax
Global Const FAXSTAT_ERROR = 11             ' Had an error (see fax_error_code).  Will be retried.
Global Const FAXSTAT_ATTN = 12              ' Fax needs attention
Global Const FAXSTAT_NEEDS_ATTACH = 13
Global Const FAXSTAT_HELD = 14
Global Const FAXSTAT_INOCR = 15
Global Const FAXSTAT_INPRINT = 16
Global Const FAXSTAT_QUEPRINT = 17
Global Const FAXSTAT_QUEOCR = 18
Global Const FAXSTAT_IN_VALIDATE = 19
Global Const FAXSTAT_IN_APPROVAL = 20

' Constants used with the RFO_FI10_PRIORITY and RFO_UI10_UCDEFAULTPRIORITY members
Global Const PRIORITY_NORMAL = 0
Global Const PRIORITY_LOW = 1
Global Const PRIORITY_HIGH = 2

' Constants for the RFO_MSGREQ_REQUESTTYPE member
Global Const MSGTYPE_SENDOK = 0
Global Const MSGTYPE_CHKLOOK2 = 1
Global Const MSGTYPE_SENDERR1 = 2
Global Const MSGTYPE_SENDERR2 = 3
Global Const MSGTYPE_SENDING1 = 4
Global Const MSGTYPE_SENDING2 = 5
Global Const MSGTYPE_NEWFAX1 = 6
Global Const MSGTYPE_NEWFAX2 = 7
Global Const MSGTYPE_ABANDON1 = 8
Global Const MSGTYPE_MESSAGE1 = 9
Global Const MSGTYPE_CODEERR1 = 10
Global Const MSGTYPE_PREVIEW1 = 11
Global Const MSGTYPE_OCRDONE1 = 12
Global Const MSGTYPE_PAPERERR1 = 13
Global Const MSGTYPE_PHONEERR1 = 14

' Constants used in the usOperation field of the RFaxMarkFax() function
Global Const MARKFAX_VIEWED = 0
Global Const MARKFAX_UNVIEWED = 1
Global Const MARKFAX_PRINTED = 3
Global Const MARKFAX_NOTPRINTED = 4
Global Const MARKFAX_RELEASED = 5
Global Const MARKFAX_HELD = 6
Global Const MARKFAX_BODYPRINTED = 7
Global Const MARKFAX_APPROVED = 8
Global Const MARKFAX_DISAPPROVED = 9
Global Const MARKFAX_GENERIC1 = 10
Global Const MARKFAX_NOTGENERIC1 = 11
Global Const MARKFAX_GENERIC2 = 12
Global Const MARKFAX_NOTGENERIC2 = 13
Global Const MARKFAX_ATOMIC = &H4000       ' OR-on to MARKFAX_???? constant to force RFaxMarkFax() to work in atomic mode

' Constants used with the RFaxCreateServerHandle() function
Global Const COMMPROTOCOL_NAMEDPIPES = 1
Global Const COMMPROTOCOL_IPXOS2 = 2      ' Only available for OS/2 fax servers
Global Const COMMPROTOCOL_SPX = 3
Global Const COMMPROTOCOL_TCPIP = 4
Global Const COMMPROTOCOL_IPX = 5        ' Same as SPX on Win32 machines

' Constants used with the RFaxFile* functions
Global Const LOGDIR_NONE = 0
Global Const LOGDIR_IMAGE = 1
Global Const LOGDIR_SIG = 2
Global Const LOGDIR_PAPERS = 3
Global Const LOGDIR_FCS = 4
Global Const LOGDIR_BIN = 6
Global Const LOGDIR_OUTGOING = 7
Global Const LOGDIR_BFT = 8
Global Const LOGDIR_BOARD = 9

' Constants used in the PHONEITEM.flags and PHONELISTELEMENTx.flags field
Global Const PHONEFLAG_LIST = &H1                    ' Set if this a list of other phone_items
Global Const PHONEFLAG_PUBLISHED = &H2               ' Set if this entry is published for world use
Global Const PHONEFLAG_EXTERNALPUB = &H4             ' Set if entry should be shadowed to external system
Global Const PHONEFLAG_READONLY = &H8                ' If set, only owner can modify

' Server Handle Functions
Declare Function RFaxCreateServerHandle Lib "RFWIN32.DLL" (ByVal lpServerName As String, ByVal usCommProtocol As Integer, dwError As Long) As Long
Declare Sub RFaxCloseServerHandle Lib "RFWIN32.DLL" (ByVal hServer As Long)
Declare Function RFaxGetServerInfo Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal lObjServerInfo As Long) As Long
Declare Function RFaxGetServerInfo2 Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal lObjServerInfo2 As Long) As Long
Declare Function RFaxFileRead Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal usLogicalServerDir As Integer, ByVal sSourceFile As String, ByVal sDestinationDir As String) As Long
Declare Function RFaxFileWrite Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal usLogicalServerDir As Integer, ByVal sDestainationFile As String, ByVal sLocalFileSpec As String) As Long
Declare Function RFaxFileCopy Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal usLogServerSrcDir As Integer, ByVal sSrcFile As String) As Long

' List Functions
Declare Function RFaxCreateList Lib "RFWIN32.DLL" (ByVal usElementSize As Integer, ByVal ulStartingElementCount As Long) As Long
Declare Sub RFaxCloseListHandle Lib "RFWIN32.DLL" (ByVal hList As Long)
Declare Function RFaxListGetElementSize Lib "RFWIN32.DLL" (ByVal hList As Long, lpusElementSize As Integer) As Long
Declare Function RFaxListGetElementCount Lib "RFWIN32.DLL" (ByVal hList As Long, lpulElementCount As Long) As Long
Declare Function RFaxListSetElement Lib "RFWIN32.DLL" (ByVal hList As Long, ByVal ulIndex As Long, ByVal Object As Long) As Long
Declare Function RFaxListGetElement Lib "RFWIN32.DLL" (ByVal hList As Long, ByVal ulIndex As Long, ByVal Object As Long) As Long
Declare Function RFaxListGetFirstElement Lib "RFWIN32.DLL" (ByVal hList As Long, ByVal Object As Long) As Long
Declare Function RFaxListGetLastElement Lib "RFWIN32.DLL" (ByVal hList As Long, ByVal Object As Long) As Long
Declare Function RFaxListGetNextElement Lib "RFWIN32.DLL" (ByVal hList As Long, ByVal Object As Long) As Long
Declare Function RFaxGetFaxList Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal lpUserID As String, ByVal sInfoLevel As Integer, ByVal sFolder As Integer, ByVal sFilter As Integer, lpLH As Long) As Long
Declare Function RFaxListSetElementCount Lib "RFWIN32.DLL" (ByVal hList As Long, ByVal ulElementCount As Long) As Long
Declare Function RFaxListGetPreviousElement Lib "RFWIN32.DLL" (ByVal hList As Long, ByVal lObject As Long) As Long

' Fax Event Functions
Declare Function RFaxGetCompleteEvent Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal Object As Long) As Long
Declare Function RFaxGetArchiveEvent Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal Object As Long) As Long
Declare Function RFaxGetMessageEvent Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal usNotifyType As Integer, ByVal Object As Long) As Long
Declare Function RFaxGetValidateEvent Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal Object As Long) As Long
Declare Function RFaxGetCompletionEvent Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal lCompReqObj As Long) As Long
Declare Function RFaxGetGenericEvent Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal dwMessageTypes As Long, ByVal lWorkReqObj As Long) As Long
Declare Function RFaxGetGenericMailEvent Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal dwReserved As Long, ByVal usMailType As Integer, ByVal lWorkReqObj As Long) As Long
Declare Function RFaxSaveEvent Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal lWorkReqObj As Long) As Long
Declare Function RFaxGetEventList Lib "RFWIN32.DLL" (ByVal hServer As Long, lListHandle As Long) As Long
Declare Function RFaxSaveStatus Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hObject As Long, ByVal sStatus As Integer) As Long

' Fax Handler Functions
Declare Function RFaxLoadFax Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hFax As Long, ByVal lpLocalDir As String, ByVal Reserved1 As Long, ByVal Object As Long) As Long
Declare Function RFaxLoadFaxNotes Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hNotes As Long, ByVal Object As Long) As Long
Declare Function RFaxDeleteFax Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hFax As Long) As Long
Declare Function RFaxMarkFax Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hFax As Long, ByVal usOperation As Integer) As Long
Declare Function RFaxGetNewFax Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal lpUserID As String, ByVal lpDestDir As String, ByVal Object As Long) As Long
Declare Function RFaxInitFax Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal lFI10Obj As Long, ByVal sUserID As String) As Long
Declare Function RFaxInitFax2 Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal lFI10Obj As Long, ByVal sWhichKey As Integer, ByVal sUserID As String, ByVal sRouteInfo As String, ByVal sDSName As String, ByVal ulRoutingCode As Long, ByVal ulSubscriberID As Long) As Long
Declare Function RFaxKickFax Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hFax As Long, ByVal sUserID As String) As Long
Declare Function RFaxCombineFaxes Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal ulNumFaxes As Long, ByVal lpCombineInfo As Long, ByVal lpUserID As String, Optional ByVal pNewFaxHandle As Long) As Long
Declare Function RFaxSaveFax Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal lFI10Obj As Long, ByVal lNI10Obj As Long, hNewFaxHandle As Long) As Long
Declare Function RFaxSendCVL Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sCVLFileName As String, ByVal lFI10Obj As Long, ByVal lNI10Obj As Long, ByVal sFCSFileName As String, hNewFaxHandle As Long) As Long
Declare Function RFaxMoveFax Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hFaxHandle As Long, ByVal usFolder As Integer) As Long
Declare Function RFaxForwardFax Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hFax As Long, ByVal usFolder As Integer, ByVal bRoute As Integer, ByVal sUserList As String, ByVal sNotes As String, ByVal sOwnerID As String) As Long
Declare Function RFaxForwardFax2 Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hFax As Long, ByVal usFolder As Integer, ByVal bNoKick As Integer, ByVal bRoute As Integer, ByVal sUserList As String, ByVal sNotes As String, ByVal sOwnerID As String, hNewFax As Long) As Long
Declare Function RFaxGetNextFax Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hUser As Long, ByVal lFI10Obj As Long) As Long
Declare Function RFaxGetNextFax2 Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sUserID As String, ByVal lFI10Obj As Long) As Long
Declare Function RFaxGetFaxList2 Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sUserID As String, ByVal usInfoLevel As Integer, ByVal usFolder As Integer, ByVal usFilter As Integer, ByVal lMaxRead As Long, hList As Long) As Long
Declare Function RFaxQueryFaxesToCSV Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sCSVFileName As String, ByVal lQueryInfoObj As Long, RecordsWritten As Long) As Long
Declare Function RFaxStringTrxStatus Lib "RFWIN32.DLL" (ByVal lHLE0Obj As Long, ByVal sBuffer As String, ByVal usBuffLen As Integer) As Long
Declare Function RFaxStringFaxStatus Lib "RFWIN32.DLL" (ByVal lFLE0Obj As Long, ByVal sBuff As String, ByVal usBuffLen As Integer, usStatusType As Integer) As Long
Declare Function RFaxEnumAllFaxes Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sEnumCtrl As Integer, ByVal lpQueryInfo As Long, ByVal lpFI10 As Long) As Long
Declare Function RFaxEnumAllFaxes2 Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sEnumCtrl As Integer, ByVal lFQI2Obj As Long, ByVal lFI10Obj As Long) As Long

' History Functions
Declare Function RFaxGetHistoryList Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal lFaxHandle As Long, lpLH As Long) As Long
Declare Function RFaxSaveHistory Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal lFaxHandle As Long, ByVal fNew As Integer, lObjHLE0 As Long) As Long

' User Functions
Declare Function RFaxGetObjectCount Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal usObjectType As Integer, ByVal sUserID As String, ByVal handle As Long, lCount As Long) As Long
Declare Function RFaxVerifyVoiceUser Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sUserID As String, ByVal sPassword As String) As Long
Declare Function RFaxDeleteUser Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hUser As Long, ByVal lReserved As Long) As Long
Declare Function RFaxDeleteUser2 Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hUser As Long, ByVal dwFlags As Long, ByVal lReserved As Long) As Long
Declare Function RFaxChangePassword Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sUserID As String, ByVal sOldPassword As String, ByVal sNewPassword As String, ByVal bVerify As Integer) As Long
Declare Function RFaxGetUserList Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sInfoLevel As Integer, lpLH As Long) As Long
Declare Function RFaxLoadUser2 Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sWhichKey As Integer, ByVal hUser As Long, ByVal lpUserID As String, ByVal lpRouteInfo As String, ByVal lpDSName As String, ByVal ulRoutingCode As Long, ByVal ulSubscriberID As Long, ByVal Object As Long) As Long
Declare Function RFaxSaveUser Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal fNewUser As Integer, ByVal Object As Long) As Long
Declare Function RFaxVerifyUser Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal lpUserID As String, ByVal lpPassword As String, ByVal fAdminLogin As Integer) As Long
Declare Function RFaxGetNewFaxCount Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sUserID As String, ByVal usFolder As Integer, ByVal lGNFCObj As Long) As Long

' Misc Functions
Declare Function RFaxG3ToTIFF Lib "RFWIN32.DLL" (ByVal SourceFile1 As String, ByVal NumPages1 As Integer, ByVal SourceFile2 As String, ByVal NumPages2 As Integer, ByVal DestFile As String, ByVal ImageDesc As String, ByVal DateTime As String) As Long
Declare Function RFaxPageExtToInt Lib "RFWIN32.DLL" (ByVal lpExt As String) As Integer
Declare Function RFaxIntToImageExt Lib "RFWIN32.DLL" (ByVal iPage As Integer, ByVal lpExt As String) As String
Declare Function RFaxG3ToMFTIFF Lib "RFWIN32.DLL" (ByVal lpSourceFile1 As String, ByVal sNumPages1 As Integer, ByVal pSourceFile2 As String, ByVal sNumPages2 As Integer, ByVal lpDestFile As String, ByVal lpImageDesc As String, ByVal lpDateTime As String) As Long
Declare Function RFaxGetUniqueFileName Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal pszFileName As String) As Long
Declare Sub RFaxCTimeToValues Lib "RFWIN32.DLL" (ByVal ulCTime As Long, _
                              sYear As Integer, sMonth As Integer, sDay As Integer, _
                              sHour As Integer, sMin As Integer, sSec As Integer)
Declare Function RFaxValuesToCTime Lib "RFWIN32.DLL" (ByVal sYear As Integer, ByVal sMonth As Integer, ByVal sDay As Integer, _
                                   ByVal sHour As Integer, ByVal sMin As Integer, ByVal sSec As Integer)

' Phonebook Functions
Declare Function RFaxLoadPhoneByHandle Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal handle As Long, ByVal lPIObj As Long) As Long
Declare Function RFaxLoadPhoneByName Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sEntry As String, ByVal sUser As String, ByVal sPassword As String, ByVal lPIObj As Long) As Long
Declare Function RFaxSavePhone Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal lPIObj As Long) As Long
Declare Function RFaxUpdatePhone Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal lPIObj As Long) As Long
Declare Function RFaxDeletePhone Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal handle As Long) As Long
Declare Function RFaxGetPhoneList Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sInfoLevel As Integer, ByVal lpUserID As String, ByVal lpPassword As String, lpLH As Long) As Long
Declare Function RFaxGetPhoneList2 Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sInfoLevel As Integer, ByVal bLoadOthers As Integer, ByVal lpUserID As String, ByVal lpPassword As String, lpLH As Long) As Long

' Library Document Functions
Declare Function RFaxGetGroupList Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sInfoLevel As Integer, hList As Long) As Long
Declare Function RFaxLoadGroup Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sGroupID As String, ByVal lGI10Obj As Long) As Long
Declare Function RFaxSaveGroup Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal fNewGroup As Integer, ByVal lGI10Obj As Long) As Long
Declare Function RFaxDeleteGroup Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hGroup As Long) As Long
Declare Function RFaxLibDocFromFax Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hFax As Long, ByVal sCode As String, ByVal sDesc As String, ByVal hLibDoc As Long) As Long
Declare Function RFaxGetLibDocList Lib "RFWIN32.DLL" (ByVal hServer As Long, hList As Long) As Long
Declare Function RFaxGetLibDocList2 Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal ulLoadMask As Long, ByVal usInfoLevel As Integer, hList As Long) As Long
Declare Function RFaxGetLibDocList3 Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal ulLoadMask As Long, ByVal usInfoLevel As Integer, ByVal ulLibEnumFlags As Long, ByVal sSearchString As String, hList As Long) As Long
Declare Function RFaxLoadLibDoc Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hLibDoc As Long, ByVal lLDLE0Obj As Long) As Long
Declare Function RFaxLoadLibDoc2 Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sWhichKey As Integer, ByVal hLibDoc As Long, ByVal sDocumentID As String, ByVal lLDLE0Obj As Long) As Long
Declare Function RFaxLoadLibDoc3 Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sWhichKey As Integer, ByVal sIntendedUse As Integer, ByVal hLibDoc As Long, ByVal sDocumentID As String, ByVal lLDLE0Obj As Long) As Long
Declare Function RFaxAdjustLibDocUsage Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sWhichKey As Integer, ByVal hLibDoc As Long, ByVal sDocumentID As String, ByVal usUsageType As Integer, ByVal usUsageValue As Integer, ByVal lLDLE1Obj As Long) As Long

' Printer Functions

Declare Function RFaxLoadPrinter Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hPrinter As Long, ByVal sPrinterID As String, ByVal lPI10Obj As Long) As Long
Declare Function RFaxGetPrinterList Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal usInfoLevel As Integer, hList As Long) As Long
Declare Function RFaxSavePrinter Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal fNewPrinter As Integer, ByVal lPI10Obj As Long) As Long

' Billing Code Functions
Declare Function RFaxGetBillCodeList Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sSearchString As String, ByVal usKeyField As Integer, ByVal sInfoLevel As Integer, ByVal ulMaxRead As Long, hList As Long) As Long
Declare Function RFaxGetBillCodeList2 Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sSearchString As String, ByVal usKeyField As Integer, ByVal sInfoLevel As Integer, ByVal ulMaxRead As Long, ByVal usDirection As Integer, hList As Long) As Long
Declare Function RFaxLoadBillCode Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sWhich As Integer, ByVal hCode As Long, ByVal sBI1 As String, ByVal sBI2 As String, ByVal lBI10Obj As Long) As Long
Declare Function RFaxSaveBillCode Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal fNewCode As Integer, ByVal lBI10Obj As Long) As Long
Declare Function RFaxDeleteBillCode Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sWhich As Integer, ByVal hCode As Long, ByVal sBI1 As String, ByVal sBI2 As String) As Long

' Form Overlay Functions
Declare Function RFaxGetFormList Lib "RFWIN32.DLL" (ByVal hServer As Long, hList As Long) As Long
Declare Function RFaxLoadForm Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal sWhich As Integer, ByVal hForm As Long, ByVal sFormCode As String, ByVal iFormNum As Integer, ByVal lFI10Obj As Long) As Long
Declare Function RFaxSaveForm Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal fNewForm As Integer, ByVal lFI10Obj As Long) As Long
Declare Function RFaxDeleteForm Lib "RFWIN32.DLL" (ByVal hServer As Long, ByVal hForm As Long, ByVal fRemoveImageFile As Integer) As Long

' Fax Coversheet Functions
Declare Function RFaxGetFCSList Lib "RFWIN32.DLL" (ByVal hServer As Long, hList As Long) As Long

