VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "HTTP Poster"
   ClientHeight    =   7830
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10710
   LinkTopic       =   "Form1"
   ScaleHeight     =   7830
   ScaleWidth      =   10710
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Clear 
      Caption         =   "Clear XML"
      Height          =   375
      Left            =   5280
      TabIndex        =   4
      Top             =   7320
      Width           =   2775
   End
   Begin VB.ComboBox cmbURL 
      Height          =   315
      ItemData        =   "main.frx":0000
      Left            =   720
      List            =   "main.frx":000D
      TabIndex        =   3
      Text            =   "http://"
      Top             =   6840
      Width           =   9855
   End
   Begin VB.TextBox txtXML 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6735
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   1
      Top             =   0
      Width           =   10695
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Post"
      Height          =   375
      Left            =   2640
      TabIndex        =   0
      Top             =   7320
      Width           =   2415
   End
   Begin VB.Label Label1 
      Caption         =   "URL"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   6840
      Width           =   375
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Private Sub Clear_Click()
    txtXML.Text = ""
End Sub

Private Sub Command1_Click()

    On Error Resume Next

    XmlHttpPost cmbURL.Text, txtXML.Text

    If Err.Number = 0 Then
        MsgBox "Success!"
    Else
        MsgBox Err.Description
    End If
    
End Sub

Public Sub DoSleep(ByVal lngMS As Long)
          Dim lCount As Long
10        For lCount = 1 To lngMS \ 10
20            Sleep 10
30            DoEvents
40        Next
End Sub

Public Function XmlHttpPost( _
    ByVal strUrl As String, _
    ByVal strXml As String) As Long

          Const PROC_NAME As String = "XmlHttpPost: "

          Dim objXmlHttp As XMLHTTP
          
          'Failover vars and constants
          Dim intFailOverCount As Integer
          Const MaxFailoverAttempts = 10
          Const FailoverSleepPeriod = 1000

10        On Error GoTo ErrorHandler

20        Set objXmlHttp = New XMLHTTP40

FailOverLoop:

30        On Error GoTo FailOverCheck

40        objXmlHttp.Open "POST", strUrl, False

50        objXmlHttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
60        objXmlHttp.send strXml

70        If objXmlHttp.Status <> 200 Then
80            Err.Raise 500, PROC_NAME & "HTTP Send", _
                  "Bad status (" & objXmlHttp.Status & ")returned from post to " & strUrl
90        End If
          
FailOverCheck:

100       If Err.Number <> 0 Then
          
              'Retry loop count below max so far?
110           If intFailOverCount < MaxFailoverAttempts Then
              
120               If intFailOverCount = 0 Then
                  
130                   MsgBox "A CRITICAL HTTP POSTING ERROR HAS OCCURRED" & vbCrLf & _
                          "ENTERING FAILOVER RETRY LOOP!" & vbCrLf & Err.Description

140                   intFailOverCount = 1
                      
150               Else
160                   intFailOverCount = intFailOverCount + 1
170               End If
          
                  'Protect against errors in this code
180               On Error Resume Next

                  'Clear the error object.
190               Err.Clear

                  'Pause to give time for cluster failover to complete.
200               DoSleep (FailoverSleepPeriod)

210               Resume FailOverLoop
                  
220           Else    ' Give up!
              
                  'Add note to log that the retry count has been exceeded.
230               MsgBox "XmlHttpPost: MAXIMUM RETRY COUNT OF " & MaxFailoverAttempts & " HAS BEEN EXCEEDED! " _
                      & Err.Description
              
240               GoTo ErrorHandler
                  
250           End If
              
260       End If

270       MsgBox PROC_NAME & "HTTP Submit Successful!"

ErrorHandler:

280       Set objXmlHttp = Nothing

290       If Err.Number <> 0 Then
300           MsgBox "URL = " & strUrl
310           MsgBox "XML = " & strXml
320           MsgBox Err.Number & "[Line " & Erl & "] " & PROC_NAME & Err.Source & _
                  "Error posting to " & strUrl & " : " & Err.Description
330       End If

End Function


