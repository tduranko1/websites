Attribute VB_Name = "MLAPDPartnerData"
Option Explicit

'********************************************************************************
'* Component LAPDPartnerData : Module MLAPDPartnerData
'*
'* Global Consts and Utility Methods
'*
'********************************************************************************

Public Const APP_NAME As String = "LAPDPartnerData."

Public Const LAPDPartnerData_FirstError As Long = &H80064800

'Object used for error logging
Public mobjEvents As SiteUtilities.CEvents

'********************************************************************************
'* Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
'* Parameters:  strObjectName = The object to call CreateObject for.
'* Purpose:     Wraps CreateObject() with better debug information.
'* Returns:     The Object created or Nothing.
'********************************************************************************
Public Function CreateObjectEx(strObjectName) As Object
10        On Error GoTo ErrorHandler

20        Set CreateObjectEx = Nothing

          Dim obj As Object
30        Set obj = CreateObject(strObjectName)

40        If obj Is Nothing Then
50            Err.Raise LAPDPartnerData_FirstError + 10, "", "CreateObject('" & strObjectName & "') returned Nothing."
60        End If

70        Set CreateObjectEx = obj
80        Exit Function

ErrorHandler:
90        On Error GoTo 0
100       Err.Raise Err.Number, "CreateObjectEx('" & strObjectName & "') " & Err.Source, Err.Description & " " & strObjectName
End Function
