VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CExecute"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'********************************************************************************
'* Component PartnerDataMgr: Class CExecute
'*
'* This component does the actual work of processing transactions received from
'* our partners.  It exists separate from the PartnerData component because
'* PartnerData needs to run under different security settings.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CExecute."

'Error codes specific to this class.
Private Enum Error_Codes
    eXmlParseError = LAPDPartnerData_FirstError + 100
End Enum

'Holds the value for turning on/off the trace options -- off in production
Private mblnDebugMode As Boolean

'Used to separate XML and non-XML returning calls.
'Stoopid hack to avoid breaking the interface.
Private mblnDisableXmlReturn As Boolean

'********************************************************************************
'* Initialize and Terminate
'********************************************************************************
Private Sub InitializeGlobals()

    'Create private member CEvents object.
    Set mobjEvents = New SiteUtilities.CEvents

    'This will give partner data its own log file.
    mobjEvents.ComponentInstance = "Partner Data"

    'Initialize our member components first, so we have logging set up.
    mobjEvents.Initialize App.Path & "\..\config\config.xml"

    'Get debug mode status.
    mblnDebugMode = mobjEvents.IsDebugMode

End Sub

Private Sub TerminateGlobals()
    Set mobjEvents = Nothing
End Sub

'********************************************************************************
'*
'* Syntax:      object.SendXmlDoc strXmlDoc, strTradingPartner
'*
'* Parameters:  strXmlDoc = Xml document received from calling application
'*              strTradingPartner = Code name for Trading Partner like "CCC"
'*
'* Purpose:     Receive an Xml from an external application and then act on it
'*              based on the content itself.  Typical usage would be to send the
'*              Xml document to a stored proc for storage.
'*
'* Returns:     0 for error or non-zero for success.
'*
'********************************************************************************
Public Function SendXmlDoc(ByVal strXmlDoc As String, ByVal strTradingPartner As String) As Long
    Dim strResult As String
    
    mblnDisableXmlReturn = True
    
    strResult = SendXmlRequest(strXmlDoc, strTradingPartner)
    
    If Left(strResult, 6) = "<Error" Then
        SendXmlDoc = 0
    Else
        SendXmlDoc = 1
    End If
    
    mblnDisableXmlReturn = False
    
End Function

'********************************************************************************
'*
'* Syntax:      strResponse = object.SendXmlRequest strXmlDoc, strTradingPartner
'*
'* Parameters:  strXmlDoc = Xml document received from calling application
'*              strTradingPartner = Code name for Trading Partner like "CCC"
'*
'* Purpose:     Receive an Xml from an external application and then act on it
'*              based on the content itself.  Typical usage would be to send the
'*              Xml document to a stored proc for storage.
'*
'* Returns:     '<Error Number="" Source="" Description="" Resend="False"/>'
'*              or the requested XML if successful.
'*
'********************************************************************************
Public Function SendXmlRequest(ByVal strXmlDoc As String, ByVal strTradingPartner As String) As String
          Dim objLDM As Object
      '          Dim objPDM As PartnerDataMgr20.IExecute
          Dim objXmldoc As MSXML2.DOMDocument40
          Dim blnRaiseToCaller As Boolean

          Const PROC_NAME As String = MODULE_NAME & "SendXmlRequest: "

10        On Error GoTo ErrorHandler

20        blnRaiseToCaller = False    'The following does not relate to caller.

          'Used by our error handler in LYNX to create a log of the components work
30        InitializeGlobals

          'Add parameters to trace data.
          'I removed the XML output from this trace because it will get traced again in DataAccessor.
40        If mblnDebugMode Then mobjEvents.Trace _
              "XML from " & strTradingPartner & " at " & Now(), PROC_NAME & "Started"

50        blnRaiseToCaller = True    'The following does relate to caller.

          'Validate our parameters and trace them IF they are invalid.
60        mobjEvents.Assert CBool(Len(strXmlDoc) > 0), "No Xml Document received"
70        mobjEvents.Assert CBool(Len(strTradingPartner) > 0), "No Trading Partner value received"

          ' Load the inbound Xml into a DOM object and validate it
          ' Raise an error if there is any problem in the document structure
80        Set objXmldoc = New DOMDocument40
90        LoadXml objXmldoc, strXmlDoc, PROC_NAME, "Partner Document"
100       Set objXmldoc = Nothing 'Done with it, dispose now before partner data call.

110       blnRaiseToCaller = False    'The following does not relate to caller.
          
      '    Dim strComponentVersion As String
      '
      '    ' Grab config setting to determine whether to use .net components or vb6 components.
      '    strComponentVersion = mobjEvents.mSettings.GetParsedSetting("PartnerSettings/ComponentVersion")
      '
      '    ' These next two lines actually send the document
      '    ' The xml is loaded into our Partner Databases
      '    If strComponentVersion = ".net" Then
      '
      '      Set objPDM = New PartnerDataMgr20.Execute
      '
      '      'Load the XML into the partner database and process it.  Returns any requested XML.  May be blank.
      '      SendXmlRequest = objPDM.ConsumePartnerXmlTransaction(strXmlDoc, strTradingPartner, Not (mblnDisableXmlReturn))
      '
      '    Else
          
120         Set objLDM = CreateObjectEx("LAPDPartnerDataMgr.CExecute")

            'Load the XML into the partner database and process it.  Returns any requested XML.  May be blank.
130         SendXmlRequest = objLDM.ConsumePartnerXmlTransaction(strXmlDoc, strTradingPartner, Not (mblnDisableXmlReturn))
          
      '    End If

          
          
          
          'If we are in debug mode, then write a final trace into the log file
140       If mblnDebugMode Then mobjEvents.Trace "Returned XML = '" & SendXmlRequest & "'", _
              PROC_NAME & "Finished"

ErrorHandler:

      '    If Not objPDM Is Nothing Then
      '      objPDM.Dispose
      '      Set objPDM = Nothing
      '    End If
          
150       Set objLDM = Nothing
160       Set objXmldoc = Nothing

          'Error was raised.
170       If Err.Number <> 0 Then
          
              'Log XML.
180           mobjEvents.Env "Param XML = " & strXmlDoc
190           mobjEvents.Env "Result XML (if any) = " & SendXmlRequest
        
              'Send an Error XML element back to caller.
200           SendXmlRequest = "<Error Number='" & Err.Number _
                  & "' Source='SendXmlRequest' Description='None'" _
                  & " Resend='" & CStr(Not (blnRaiseToCaller)) & "' />"
            
210           mobjEvents.Env "Error XML = " & SendXmlRequest

              'Log and raise (perhaps).
220           mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, _
                  Err.Description, "Partner = " & strTradingPartner, "", blnRaiseToCaller
230       End If

240       TerminateGlobals

End Function



