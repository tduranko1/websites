VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   8850
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10830
   LinkTopic       =   "Form1"
   ScaleHeight     =   8850
   ScaleWidth      =   10830
   StartUpPosition =   3  'Windows Default
   Begin VB.ComboBox cmbEnv 
      Height          =   315
      Left            =   9300
      Style           =   2  'Dropdown List
      TabIndex        =   16
      Top             =   7560
      Width           =   1455
   End
   Begin VB.CommandButton btnSubmitXml 
      Caption         =   "Submit XML"
      Height          =   375
      Left            =   3960
      TabIndex        =   13
      Top             =   8160
      Width           =   1455
   End
   Begin VB.Frame Frame4 
      Caption         =   "APD->Partner"
      Height          =   1335
      Left            =   3840
      TabIndex        =   14
      Top             =   7320
      Width           =   1695
   End
   Begin VB.CommandButton AttachEstimate 
      Caption         =   "Attach Estimate"
      Height          =   375
      Left            =   5760
      TabIndex        =   7
      Top             =   8160
      Width           =   1455
   End
   Begin VB.CommandButton Request 
      Caption         =   "Request XML"
      Height          =   375
      Left            =   7560
      TabIndex        =   6
      Top             =   8160
      Width           =   1455
   End
   Begin VB.CheckBox chkSendReceipt 
      Caption         =   "Send Receipt"
      Height          =   255
      Left            =   2160
      TabIndex        =   5
      Top             =   7800
      Width           =   1335
   End
   Begin VB.ComboBox cmbSubmit 
      Height          =   315
      ItemData        =   "main.frx":0000
      Left            =   120
      List            =   "main.frx":000D
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   7800
      Width           =   1935
   End
   Begin VB.ComboBox cmbPartner 
      Height          =   315
      ItemData        =   "main.frx":0044
      Left            =   120
      List            =   "main.frx":005D
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   8160
      Width           =   1935
   End
   Begin VB.CommandButton butClear 
      Caption         =   "Clear XML"
      Height          =   375
      Left            =   9240
      TabIndex        =   2
      Top             =   8280
      Width           =   1455
   End
   Begin VB.TextBox txtXML 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7215
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   1
      Top             =   0
      Width           =   10695
   End
   Begin VB.CommandButton SubmitXML 
      Caption         =   "Submit XML"
      Height          =   375
      Left            =   2160
      TabIndex        =   0
      Top             =   8160
      Width           =   1455
   End
   Begin VB.Frame Frame1 
      Caption         =   "Partner->APD"
      Height          =   1335
      Left            =   0
      TabIndex        =   8
      Top             =   7320
      Width           =   3735
   End
   Begin VB.Frame Frame2 
      Height          =   1335
      Left            =   7440
      TabIndex        =   9
      Top             =   7320
      Width           =   1695
   End
   Begin VB.Frame Frame3 
      Height          =   1335
      Left            =   5640
      TabIndex        =   10
      Top             =   7320
      Width           =   1695
      Begin VB.TextBox txtDocumentID 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   840
         TabIndex        =   11
         Text            =   "0"
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Doc ID"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Label lblServerName 
      Height          =   195
      Left            =   9300
      TabIndex        =   17
      Top             =   7920
      Width           =   1395
   End
   Begin VB.Label Label2 
      Caption         =   "Env:"
      Height          =   195
      Left            =   9300
      TabIndex        =   15
      Top             =   7320
      Width           =   435
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim m_objFSO As New Scripting.FileSystemObject
Dim m_objConfig As New MSXML2.DOMDocument
Dim m_strServerName As String


Private Sub butClear_Click()
    txtXML.Text = ""
End Sub

Private Sub cmbEnv_Click()
    Dim objNode As IXMLDOMElement
    m_strServerName = ""
    
    Set objNode = m_objConfig.selectSingleNode("/PartnerDataPoster/Environment[@id='" & cmbEnv.Text & "']")
    
    If Not objNode Is Nothing Then
        m_strServerName = objNode.Text
        If m_strServerName <> "" Then
            lblServerName.Caption = m_strServerName
        Else
            lblServerName.Caption = "local"
        End If
    End If
End Sub

Private Sub cmbSubmit_Click()
    If cmbSubmit.Text = "APD DB + Partner DB" Then
        chkSendReceipt.Enabled = False
        chkSendReceipt.Value = 1
    ElseIf cmbSubmit.Text = "APD DB Only" Then
        chkSendReceipt.Enabled = False
        chkSendReceipt.Value = 0
    Else
        chkSendReceipt.Enabled = True
    End If
End Sub

Private Sub cmbSubmit_KeyUp(KeyCode As Integer, Shift As Integer)
    cmbSubmit_Click
End Sub
Private Sub btnSubmitXml_Click()
    On Error Resume Next
    
    Dim objPartner As Object
    
    If cmbEnv.ListIndex = -1 Then
        MsgBox "Select an environment and try again.", vbCritical
        Exit Sub
    End If
    
    Set objPartner = CreateObject("LAPDPartnerDataMgr.CExecute", m_strServerName)
    objPartner.InitiatePartnerXmlTransaction (txtXML.Text)
    

    If Err.Number = 0 Then
        MsgBox "No Errors.  Return Value = [" & lngReturnValue & "]."
    Else
        MsgBox Err.Description
    End If
  
  
End Sub
Private Sub SubmitXML_Click()

    On Error Resume Next

    Dim objPartner As Object
    Dim lngReturnValue As Long
    Dim blnSendReceipt As Boolean
    
    If cmbEnv.ListIndex = -1 Then
        MsgBox "Select an environment and try again.", vbCritical
        Exit Sub
    End If
    
    blnSendReceipt = CBool(chkSendReceipt.Value = 1)
    
    If cmbSubmit.Text = "APD DB + Partner DB" Then
    
        Set objPartner = CreateObject("LAPDPartnerData.CExecute", m_strServerName)
        lngReturnValue = objPartner.SendXmlDoc(txtXML.Text, cmbPartner.Text)
  
    ElseIf cmbSubmit.Text = "Partner DB Only" Then
    
        Set objPartner = CreateObject("LAPDPartnerDataMgr.CExecute", m_strServerName)
        lngReturnValue = objPartner.SubmitDocumentToPartnerDbOnly(txtXML.Text, cmbPartner.Text, blnSendReceipt)
    
    ElseIf cmbSubmit.Text = "APD DB Only" Then
    
        Set objPartner = CreateObject("LAPDPartnerDataMgr.CExecute", m_strServerName)
        lngReturnValue = objPartner.SubmitDocumentToApdDbOnly(txtXML.Text, cmbPartner.Text, blnSendReceipt)
    
    Else
        MsgBox "Combo Box choice not found?!"
    End If
    
    Set objPartner = Nothing
    
    If Err.Number = 0 Then
        MsgBox "No Errors.  Return Value = [" & lngReturnValue & "]."
    Else
        MsgBox Err.Description
    End If
    
End Sub

Private Sub Form_Load()
    Dim objDataNodes As IXMLDOMNodeList
    Dim objDataNode As IXMLDOMElement

    cmbPartner.ListIndex = 1
    cmbSubmit.ListIndex = 0
    
    chkSendReceipt.Value = 1
    chkSendReceipt.Enabled = False
    
    If m_objFSO.FileExists(App.Path & "\config.xml") = False Then
        MsgBox "Missing configuration file.", vbCritical
    Else
        m_objConfig.async = False
        m_objConfig.Load App.Path & "\config.xml"
        
        If m_objConfig.parseError.errorCode <> 0 Then
            MsgBox "Configuration file is malformed." & vbCrLf & m_objConfig.parseError.reason, vbOKOnly
        Else
            Set objDataNodes = m_objConfig.selectNodes("/PartnerDataPoster/Environment")
            cmbEnv.Clear
            For Each objDataNode In objDataNodes
                cmbEnv.AddItem objDataNode.getAttribute("id")
            Next
        End If
    End If
    
    
End Sub

Private Sub Request_Click()

    On Error Resume Next

    Dim objPartner As Object
    Dim strResult As String
    
    If cmbEnv.ListIndex = -1 Then
        MsgBox "Select an environment and try again.", vbCritical
        Exit Sub
    End If
    
    Set objPartner = CreateObject("LAPDPartnerData.CExecute", m_strServerName)
    strResult = objPartner.SendXmlRequest(txtXML.Text, cmbPartner.Text)
  
    Set objPartner = Nothing
    
    If Err.Number = 0 Then
        MsgBox strResult
    Else
        MsgBox Err.Description
    End If

End Sub

Private Sub AttachEstimate_Click()

    On Error Resume Next

    Dim objPartner As Object
    Dim strReturnValue As String
    
    If cmbEnv.ListIndex = -1 Then
        MsgBox "Select an environment and try again.", vbCritical
        Exit Sub
    End If
    
    Set objPartner = CreateObject("LAPDPartnerDataMgr.CPartnerDataXfer", m_strServerName)
    strReturnValue = objPartner.AttachEstimateToAPDDocument(txtXML.Text, cmbPartner.Text, txtDocumentID.Text)
    Set objPartner = Nothing
    
    If Err.Number = 0 Then
        MsgBox strReturnValue
    Else
        MsgBox Err.Description
    End If

End Sub


