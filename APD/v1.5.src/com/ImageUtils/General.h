/////////////////////////////////////////////////////////////////////////////
// General.h
//
// A place to dump general useful goodies.
//
/////////////////////////////////////////////////////////////////////////////
#ifndef GENERAL_H
#define GENERAL_H

#ifndef MAXULONG_PTR
#define ULONG_PTR    DWORD
#endif //MAXULONG_PTR

// Find these in the Platform SDK, include those directories.
#include <GdiPlus.h>
using namespace Gdiplus;

#define Abs( a ) ( ( a >= 0 ) ? a : -a )

// Receives the MIME type of an encoder and returns the class identifier
// (CLSID) of that encoder.
//
// The MIME types of the encoders built into GDI+ are as follows: 
//
// image/bmp image/jpeg image/gif image/tiff image/png 
//
// The function calls GetImageEncoders to get an array of ImageCodecInfo
// objects. If one of the ImageCodecInfo objects in that array represents 
// the requested encoder, the function returns the index of the
// ImageCodecInfo object and copies the CLSID into the variable
// pointed to by pClsid. If the function fails, it returns -1. 
//
int GetEncoderClsid(const WCHAR* format, CLSID* pClsid);

// BytesToVariantArry creates an ADSTYPE_OCTET_STRING variant
// from a given set of bytes
// This function can be found in the Platform SDK documention.
HRESULT BytesToVariantArray(
	PBYTE pValue, //Pointer to bytes to put in a variant array.
   ULONG cValueElements,//Size of pValue in bytes.
   VARIANT *pVariant //Return variant containing octet string (VT_UI1|VT_ARRAY).
);

// Returns the path to the DLL for the passed application.
BOOL GetPath(LPOLESTR szApp, LPTSTR szPath, ULONG cSize);

//  CSmartPtr mimics a built-in pointer except that it guarantees deletion
//  of the object pointed to, either on destruction of the CSmartPtr or via
//  an explicit Reset().
template<typename T> class CSmartPtr // noncopyable
{
private:

    T * ptr;

    CSmartPtr(CSmartPtr const &);
    CSmartPtr & operator=(CSmartPtr const &);

    typedef CSmartPtr<T> this_type;

public:

    typedef T element_type;

    explicit CSmartPtr(T * p = 0): ptr(p) // never throws
    {
    }

    ~CSmartPtr() // never throws
    {
        if ( ptr != 0 )
			  delete ptr;
    }

    void Reset(T * p = 0) // never throws
    {
        if(ptr != p)
        {
            this_type(p).Swap(*this);
        }
    }

    T & operator*() const // never throws
    {
        return *ptr;
    }

    T * operator->() const // never throws
    {
        return ptr;
    }

    T * Get() const // never throws
    {
        return ptr;
    }

    // implicit conversion to "bool"

    typedef T * (this_type::*unspecified_bool_type)() const;

    operator unspecified_bool_type() const // never throws
    {
        return ptr == 0? 0: &this_type::Get;
    }

    bool operator! () const // never throws
    {
        return ptr == 0;
    }

    void Swap(CSmartPtr & b) // never throws
    {
        T * tmp = b.ptr;
        b.ptr = ptr;
        ptr = tmp;
    }
};

template<typename T> inline void Swap(CSmartPtr<T> & a, CSmartPtr<T> & b) // never throws
{
    a.Swap(b);
}

// GetPointer(p) is a generic way to say p.Get()

template<typename T> inline T * GetPointer(CSmartPtr<T> const & p)
{
    return p.Get();
}

#endif // #ifndef GENERAL_H

