/////////////////////////////////////////////////////////////////////////////
// Thumbnail.cpp : Implementation of CThumbnail
//
// Used to create thumbnails for images.
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ImageUtils.h"
#include "Thumbnail.h"

// Constants to compare against config.xml.
const CBstr cbstrGrayscale = L"Grayscale";
const CBstr cbstrUniform = L"Uniform";
const CBstr cbstrOptimized = L"Optimized";

// Declare local functions.
Bitmap* ConvertImageTo8Bits( Image* pImage, EPixelConversion eConv, int* pnIterations );

/////////////////////////////////////////////////////////////////////////////
// CThumbnail
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// ErrorInfo Support
/////////////////////////////////////////////////////////////////////////////
STDMETHODIMP CThumbnail::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IThumbnail
	};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Returns any thrown error description.
/////////////////////////////////////////////////////////////////////////////
STDMETHODIMP CThumbnail::GetErrorDescription(BSTR *pstrDescription)
{
	*pstrDescription = m_strErrorDescription.AllocSysString();
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////
// Called for non-error conditions.
/////////////////////////////////////////////////////////////////////////////
HRESULT CThumbnail::Success()
{
	m_strErrorDescription = L"No Error";
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////
// Used in catch statements to store error descriptions.
/////////////////////////////////////////////////////////////////////////////
HRESULT CThumbnail::HandleError( CBstr strDescription, DWORD dwCode )
{
	m_bCanBePooled = FALSE;

	if ( strDescription.length() == 0 )
		m_strErrorDescription += L"Unspecified Error" + GetLastErrorInfo();
	else
		m_strErrorDescription += strDescription + GetLastErrorInfo();

	m_pEvents->HandleEvent( dwCode, m_strMethod, m_strFileName + "|" + m_strErrorDescription, L"", L"", false );

	return E_FAIL;
}

/////////////////////////////////////////////////////////////////////////////
// Returns the requested image reduced to the requested size.
// The returned ADODB.Stream can be drawn through Response.BinaryWrite.
/////////////////////////////////////////////////////////////////////////////
STDMETHODIMP CThumbnail::GetThumbnail( BSTR strFile, int nWidth, int nHeight, IUnknown** pUnkStream )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	Method( L"ImageUtils::GetThumbnail()" );
	m_strFileName = strFile;

	// Initialize SiteUtilities::CEvents object.
	m_pEvents = _CEventsPtr( __uuidof(CEvents) );
	_CSettingsPtr pSettings;

	// Schtuff that needs to be specifically cleaned up.
	unsigned char *pcBuff = NULL;
	ULONG_PTR gdiplusToken = NULL;
	HRESULT hResult = S_OK;

	try
	{
		wchar_t wcBuff[300];
		CBstr strEmpty( L"" );

		//////////////////////////////////////////////////
		// Initialize configuration and logging.
		
		// This will give us our own log file.
		m_pEvents->PutComponentInstance(L"Image Utils");

		GetPath( L"ImageUtils.Thumbnail.1", wcBuff, 300 );

		CBstr strConfig( wcBuff );
		strConfig += L"\\..\\config\\config.xml";

		m_pEvents->Initialize( strConfig, L"Debug", true );
		pSettings = m_pEvents->GetmSettings();

		// Get debug mode flag - disable traces if false.
		// VARIANT_BOOL 0 == FALSE, -1 == TRUE
		bool bDebugMode = (bool)( m_pEvents->GetIsDebugMode() == -1 );

		// Logically AND the app debug setting with local setting.
		CBstr strDebug = pSettings->GetParsedSetting( "ThumbNail/@Debug" );
		bDebugMode = bDebugMode && (bool)( strDebug == "True" ); 

		//////////////////////////////////////////////////
		// Validate parameters.

		if ( nWidth <= 0 )
			throw CBstr(L"Width parameter must be greater than zero.");
		if ( nHeight <= 0 )
			throw CBstr(L"Height parameter must be greater than zero.");
		if ( m_strFileName.length() < 5 )
			throw CBstr(L"File parameter must contain the path to an image file.");

		if ( bDebugMode )
			m_pEvents->Trace( CBstr(L"Creating thumbnail for file ") + strFile, Source() );

		// Determine what conversion settings to use from the config file.
		EPixelConversion eConversion = eGrayscale;

		CBstr strConversion = pSettings->GetParsedSetting( "ThumbNail/PixelConversion" );

		if ( bDebugMode )
			m_pEvents->Trace( L"", Source() + CBstr(L"Pixel Conversion: ") + strConversion );
		
		if ( strConversion == cbstrGrayscale )
			eConversion = eGrayscale;
		else if ( strConversion == cbstrUniform )
			eConversion = eUniform;
		else if ( strConversion == cbstrOptimized )
			eConversion = eOptimized;
		else
			eConversion = eNone;

		//////////////////////////////////////////////////
		// Initialize Stream and GDI+

		// The GDI+ calls require IStream.
		IStreamPtr pStream;
		CreateStreamOnHGlobal( NULL, TRUE, &pStream );

		if ( pStream == NULL )
			throw CBstr(L"Could not create IStream.");

		// Initialize GDI+.
		if ( bDebugMode )
			m_pEvents->Trace( L"", Source() + L"Initializing GDI+" );

		GdiplusStartupInput gdiplusStartupInput;
		GdiplusStartup( &gdiplusToken, &gdiplusStartupInput, NULL );

		CLSID encoderClsid;

		// Get the CLSID of the PNG encoder.
		int result = GetEncoderClsid(L"image/jpeg", &encoderClsid);

		if ( result < 0 )
			throw CBstr(L"Could not find requested encoder.");

		//////////////////////////////////////////////////
		// Implement Thumbnail caching mechanism.

		CSmartPtr<Image> pThumbnail;
		bool bGenerateNewThumbnail = true;
		CBstr strCacheEnabled = pSettings->GetParsedSetting( "ThumbNail/Cache/@Enabled" );
		CBstr strCacheDir, strCacheFile;

		if ( strCacheEnabled == "True" )
		{
			CBstr strLocal = pSettings->GetParsedSetting( "ThumbNail/Cache/@Local" );

			int nFileNamePos = m_strFileName.ReverseFind( 92 );	// '\'

			// Build a thumbnail file name that includes the thumbnail size.
			CBstr strThumbFile = CBstr(L"THMB_") + CBstr( _itow( nWidth, wcBuff, 10 ) ) +
				L"_" + CBstr( _itow( nHeight, wcBuff, 10 ) ) + L"_" +
				m_strFileName.Mid( nFileNamePos + 1 );

			// If not Local then the cache directory is the same dir in the doc store.
			if ( strLocal == "False" )
				strCacheDir = m_strFileName.Left( nFileNamePos );

			// Otherwise we are using a local dir on the COM/web server.
			else strCacheDir = pSettings->GetParsedSetting( "ThumbNail/Cache" );

			// Now we have a cache file name and path, whether it exists or not.
			strCacheFile = strCacheDir + L"\\" + strThumbFile;

			CSmartPtr<Image> pThumbTemp( new Image( strCacheFile ) );

			if ( Ok == pThumbTemp->GetLastStatus() )
			{
				eConversion = eNone;	// Disable conversion.
				bGenerateNewThumbnail = false;

				pThumbnail.Swap( pThumbTemp );

				if ( bDebugMode )
					m_pEvents->Trace( strCacheFile, Source() + L"Thumbnail Exists");
			}
		}

		//////////////////////////////////////////////////
		// Use original image to create a new thumbnail.

		if ( bGenerateNewThumbnail )
		{
			// Load the image from disk.
			if ( bDebugMode )
				m_pEvents->Trace( L"", Source() + L"Creating Images");

			CSmartPtr<Image> pImage( new Image( strFile ) );

			if ( Ok != pImage->GetLastStatus() )
				throw CBstr(L"Could not load image file.");

			// Maintain the aspect ratio of the image.
			int nCurHeight = pImage->GetHeight();
			int nCurWidth = pImage->GetWidth();

			double dHeightRatio = (double) nCurHeight / (double) nHeight;
			double dWidthRatio = (double) nCurWidth / (double) nWidth;

			if ( dHeightRatio > dWidthRatio )
				nWidth = (int)( (double) nCurWidth / dHeightRatio );
			else
				nHeight = (int)( (double) nCurHeight / dWidthRatio );

			// Create a thumbnail image.
			CSmartPtr<Image> pThumbTemp( pImage->GetThumbnailImage( nWidth, nHeight, NULL, NULL ) );
			pThumbnail.Swap( pThumbTemp );

			if ( Ok != pThumbnail->GetLastStatus() )
				throw CBstr(L"Could not create thumbnail.");

			pImage.Reset();
		}

		//////////////////////////////////////////////////
		// Write the thumbnail to the IStream.

		if ( bDebugMode )
			m_pEvents->Trace( L"", Source() + L"Creating and writing to IStream" );

		int nIterations = 0;

		// Do any conversion required by the configuration.
		if ( eConversion != eNone )
		{
			CSmartPtr<Bitmap> pThumb256( ConvertImageTo8Bits( pThumbnail.Get(), eConversion, &nIterations ) );

			if ( Ok != pThumb256->Save( pStream, &encoderClsid, NULL ) )
				throw CBstr( L"Could not save thumbnail to IStream." );

			if ( strCacheEnabled == "True" && bGenerateNewThumbnail )
				if ( Ok != pThumb256->Save( strCacheFile, &encoderClsid, NULL ) )
					throw CBstr( L"Could not save thumbnail to Cache: " ) + strCacheFile;

			pThumb256.Reset();
		}
		// No conversion - write out as-is.
		else
		{
			if ( Ok != pThumbnail->Save( pStream, &encoderClsid, NULL ) )
				throw CBstr( L"Could not save thumbnail to IStream." );

			if ( strCacheEnabled == "True" && bGenerateNewThumbnail )
				if ( Ok != pThumbnail->Save( strCacheFile, &encoderClsid, NULL ) )
					throw CBstr( L"Could not save thumbnail to Cache." );
		}

		// Write the thumbnail to the IStream.
		if ( bDebugMode )
			m_pEvents->Trace( L"", Source() + CBstr(L"Palette Iterations : ") + _itow( nIterations, wcBuff, 10 ) );

		pThumbnail.Reset();

		// Reset the stream to it's start.
		LARGE_INTEGER liPos;
		liPos.QuadPart = 0;
		pStream->Seek( liPos,  STREAM_SEEK_SET, NULL );

		//////////////////////////////////////////////////
		// Create an ADODB.Stream object to return the
		// image through.  Open and initialize it.

		if ( bDebugMode )
			m_pEvents->Trace( L"", Source() + L"Creating and writing ADODB.Stream" );

		_variant_t  var;
		_variant_t  vtEmpty( DISP_E_PARAMNOTFOUND, VT_ERROR );

		_StreamPtr pADOStream;
		pADOStream.CreateInstance( __uuidof( Stream ) );

		pADOStream->Charset = "UTF-8"; 
		pADOStream->Type = adTypeBinary;
		pADOStream->Open( vtEmpty, adModeUnknown, adOpenStreamUnspecified, strEmpty, strEmpty );

		// Create a temporary buffer to write to.
		STATSTG stStat;
		pStream->Stat( &stStat, STATFLAG_NONAME );

		int nSize = (int) stStat.cbSize.QuadPart;
		pcBuff = new unsigned char[ nSize ];

		if ( bDebugMode )
			m_pEvents->Trace( "", Source() + CBstr(L"Image Size : ") + _itow( nSize, wcBuff, 10 ) );

		// Copy from the IStream to the temp buffer.
		pStream->Read( pcBuff, nSize, NULL );

		// Copy from the temp buffer to a variant byte array.
		BytesToVariantArray( pcBuff, nSize, &var );

		// Now write from our variant byte array to the ADODB.Stream.
		pADOStream->Write( var );
		pADOStream->Position = 0;

		// Detach the stream so that the caller owns it.
		*pUnkStream = pADOStream.Detach();

		if ( bDebugMode )
			m_pEvents->Trace( L"", Source() + L"Done!" );

		hResult = Success();
	}

	catch ( _com_error e ) { hResult = HandleError( e.Description(), e.WCode() ); }
	catch ( CBstr str ) { hResult = HandleError( str ); }
	catch ( ... ) { hResult = HandleError(); }

	// Clean up
	if ( gdiplusToken != NULL )
		GdiplusShutdown(gdiplusToken);
	if ( pcBuff != NULL )
		delete [] pcBuff;

	return hResult;
}

/////////////////////////////////////////////////////////////////////////////
// Create and return an empty color palette.
/////////////////////////////////////////////////////////////////////////////
ColorPalette* GetColorPalette( UINT nColors )
{
	// Assume monochrome image
	PixelFormat bitscolordepth = PixelFormat8bppIndexed;
	ColorPalette* pPalette = NULL; // The Palette we are stealing

	// determine number of colors
	if (nColors > 2)
		bitscolordepth = PixelFormat4bppIndexed;
	if (nColors > 16)
		bitscolordepth = PixelFormat8bppIndexed;

	// Make a new Bitmap object to get its Palette
	CSmartPtr<Bitmap> pBitmap( new Bitmap( 1, 1, bitscolordepth ) ); // The source of the stolen palette

   	UINT size = pBitmap->GetPaletteSize();
   	pPalette = (ColorPalette*) malloc(size);

	pBitmap->GetPalette( pPalette, pBitmap->GetPaletteSize() ); // Grab the palette

	return pPalette; // Send the palette back
}

/////////////////////////////////////////////////////////////////////////////
// Creates a 256 color bitmap from the passed image.
// Can use Grayscale, Uniform, or Optimized palettes based upon the passed eConv.
// pnIterations is used to return the number of passes required to get the 
// optimized palette (for debug and profile purposes).
/////////////////////////////////////////////////////////////////////////////
Bitmap* ConvertImageTo8Bits( Image* pImage, EPixelConversion eConv, int* pnIterations )
{
	// make a new 8 bpp indexed bitmap the size of the source image
	int nWidth = pImage->GetWidth();
	int nHeight = pImage->GetHeight();

	// Always use PixelFormat8bppIndexed because that is the color
	// table based interface to the GIF codec.
	Bitmap* pBitmap = new Bitmap( nWidth, nHeight, PixelFormat8bppIndexed );

	// Create a color palette big enough to hold the colors we want.
	CSmartPtr<ColorPalette> pPal( GetColorPalette(256) );

	// Initialize a new color table with entries.

	BYTE cAlpha = 0xFF; // Colors are opaque

	BYTE R = 0;
	BYTE G = 0;
	BYTE B = 0;

	UINT i;

	switch ( eConv )
	{
		case eUniform:

			for ( i = 0; i < 256; i++)
			{
				R = ( i & 0x03 ) << 6;
				G = ( i & 0x1C ) << 3;
				B = ( i & 0xE0 );

				pPal->Entries[i] = Color::MakeARGB( (int)cAlpha, R, G, B );
			}
			break;

		case eGrayscale:

			for ( i = 0; i < 256; i++)
			{
				pPal->Entries[i] = Color::MakeARGB( cAlpha, (int)i, (int)i, (int)i );
			}
			break;

	}

	// We want to use GetPixel below to pull out the color data of
	// image since GetPixel isn't defined on an Image, make a copy
	// in a Bitmap instead. So now make a new Bitmap the size of the
	// image that you want to export. Otherwise, one could try to
	// interpret the native oPixel format of the image via a LockBits
	// call. Use PixelFormat32bppARGB so we can wrap a graphics
	// around it.
	CSmartPtr<Bitmap> pBmpCopy( new Bitmap( nWidth, nHeight, PixelFormat32bppARGB ) );
	{
		CSmartPtr<Graphics> g( Graphics::FromImage( pBmpCopy.Get() ) );

		g->SetPageUnit( UnitPixel );

		// Transfer the Image to the Bitmap
		g->DrawImage( pImage, 0, 0, nWidth, nHeight );
	}

	// Lock a rectangular portion of the bitmap for writing.
	BitmapData bitmapData;
	Rect rect( 0, 0, nWidth, nHeight );

	pBitmap->LockBits( &rect, ImageLockModeWrite, PixelFormat8bppIndexed, &bitmapData );

	// Write to the temporary buffer provided by LockBits.
	// One copies the pPixels from the source image in this loop.
	// Since we want an index, we convert RGB to the appropriate
	// palette index here.
	void* pPixels = bitmapData.Scan0;

	// get the pointer to the image bits - this is the unsafe operation.
	// If the Stide is negative, Scan0 points to the last scanline in
	// the buffer. To normalize the loop, obtain a pointer to the front 
	// of the buffer located (nHeight-1) scanlines previous.
	byte *pBits = NULL;
	if (bitmapData.Stride > 0)
		pBits = (byte *) pPixels;
	else
		pBits = (byte *) pPixels + bitmapData.Stride*(nHeight-1);
	UINT uStride = (UINT) Abs( bitmapData.Stride );

	// Debug palette build iterations counter.
	if ( pnIterations != NULL )
		*pnIterations = 1;

	// Some local vars to use below.
	int nRow, nCol;
	Color oPixel;
	double dLuminance;
	byte *p8bppPixel = NULL;

	switch ( eConv )
	{
		case eUniform:
		{
			for ( nRow = 0; nRow < nHeight; ++nRow )
			{
				for ( nCol = 0; nCol < nWidth; ++nCol )
				{
					p8bppPixel = pBits + nRow * uStride + nCol;

					pBmpCopy->GetPixel((int)nCol, (int)nRow, &oPixel);

					R = oPixel.GetRed();
					G = oPixel.GetGreen();
					B = oPixel.GetBlue();

					*p8bppPixel = ( R >> 6 ) + ( ( G >> 5 ) << 2 ) + ( ( B >> 5 ) << 5 );
				}
			}
			break;
		}

		case eGrayscale:
		{
			for ( nRow = 0; nRow < nHeight; ++nRow )
			{
				for ( nCol = 0; nCol < nWidth; ++nCol )
				{
					p8bppPixel = pBits + nRow * uStride + nCol;

					pBmpCopy->GetPixel((int)nCol, (int)nRow, &oPixel);

					R = oPixel.GetRed();
					G = oPixel.GetGreen();
					B = oPixel.GetBlue();

               // Use dLuminance/chrominance conversion to get grayscale.
               // Basically - turn the image into black and white TV.
               // We do not even bother to calculate Cr or Cb since we
               // are throwing away the color anyway.
               // Y = Red * 0.299 + Green * 0.587 + Blue * 0.114

               // This expression should be integer math for performance.
               // But, since GetPixel above is by far the slowest part of 
               // this loop, the expression is left as floating point
               // for clarity.
               dLuminance = (R * 0.299) + (G * 0.587) + (B *0.114);

               // Gray scale is an intensity map from black to white
               // Compute the index to the gray scale entry that
               // approximates the  dLuminance and round the index.      
  
               // Also, constrain the index choices by the number of
               // colors to do then set that oPixel's index to the byte
               // value.
               *p8bppPixel = (byte)(dLuminance + 0.5);
				}
			}
			break;
		}

		case eOptimized:
		{
			bool bFinished = false;
			int nMaxDiff = 8;

			// Iterate until we have our optimized palette.
			while ( !bFinished )
			{
				int nPalTop = 0;
				int nPal;

				bFinished = true;

				// Loop through every pixel and build the palette.
				for ( nRow = 0; nRow < nHeight; ++nRow )
				{
					for ( nCol = 0; nCol < nWidth; ++nCol )
					{
						pBmpCopy->GetPixel((int)nCol, (int)nRow, &oPixel);

						// Attempt to reuse existing pallette entries.
						for ( nPal = 0; nPal < nPalTop; nPal++ )
						{
							Color oCol( pPal->Entries[nPal] );
							if ( (
								Abs( ( (int)oPixel.GetR() - (int)oCol.GetR() ) ) +
								Abs( ( (int)oPixel.GetG() - (int)oCol.GetG() ) ) +
								Abs( ( (int)oPixel.GetB() - (int)oCol.GetB() ) ) ) <= nMaxDiff )
							{
								p8bppPixel = pBits + nRow * uStride + nCol;
								*p8bppPixel = (byte) nPal;
								break;
							}
						}

						// Add a new palette entry.
						if ( nPal >= nPalTop )
						{
							// Too many unique entries?  Start over!
							if ( nPalTop > 255 )
							{
								// Expand our color 'similarity' criteria.
								nMaxDiff = ( nMaxDiff << 1 );

								// Retry building an optimized pallette.
								nRow = nHeight;
								nCol = nWidth;
								nPalTop = 0;
								bFinished = false;

								// Increment the debug palette build iterations counter.
								if ( pnIterations != NULL )
									*pnIterations = ( *pnIterations + 1 );
							}
							// Add a new palette entry.
							else
							{
								pPal->Entries[ nPalTop ] = oPixel.GetValue();
								p8bppPixel = pBits + nRow * uStride + nCol;
								*p8bppPixel = (byte) nPalTop;
								nPalTop++;
							}
						}
					}
				}
			}
			break;
		}
	}

	// Commit the changes by unlocking the portion of the bitmap.
	pBitmap->UnlockBits(&bitmapData);

	// Set the palette into the new Bitmap object.
	pBitmap->SetPalette( pPal.Get() );

	return pBitmap;
}

