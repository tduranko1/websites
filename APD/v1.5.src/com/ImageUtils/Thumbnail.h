/////////////////////////////////////////////////////////////////////////////
// Thumbnail.h
//
// Used to create thumbnails for images.
//
/////////////////////////////////////////////////////////////////////////////
#ifndef __THUMBNAIL_H_
#define __THUMBNAIL_H_

#include "resource.h"       // main symbols

#include "General.h"
#include "ErrorInfoBase.h"

using namespace SiteUtilities;

/////////////////////////////////////////////////////////////////////////////
// Enums
/////////////////////////////////////////////////////////////////////////////

// Enumerated type used with ConverImageTo8Bits below to specify
// what type of conversion to perform.
enum EPixelConversion
{
	eNone,
	eGrayscale,
	eUniform,
	eOptimized,
};

/////////////////////////////////////////////////////////////////////////////
// Class CThumbnail
/////////////////////////////////////////////////////////////////////////////
class ATL_NO_VTABLE CThumbnail : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CThumbnail, &CLSID_Thumbnail>,
	public ISupportErrorInfo,
	public IDispatchImpl<IThumbnail, &IID_IThumbnail, &LIBID_IMAGEUTILSLib>,
	public CErrorInfoBase
{
public:

	DECLARE_REGISTRY_RESOURCEID(IDR_THUMBNAIL)

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	BEGIN_COM_MAP(CThumbnail)
		COM_INTERFACE_ENTRY(IThumbnail)
		COM_INTERFACE_ENTRY(IDispatch)
		COM_INTERFACE_ENTRY(ISupportErrorInfo)
	END_COM_MAP()

private:

	CBstr m_strFileName;
	_CEventsPtr m_pEvents;

protected:

	virtual HRESULT Success();
	virtual HRESULT HandleError( CBstr strDescription = L"", DWORD dwCode = 0x80065100 );

public:

	// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

	// IThumbnail
	STDMETHOD(GetErrorDescription)( /*[out,retval]*/ BSTR* pstrDescription );
	STDMETHOD(GetThumbnail)(/*[in]*/ BSTR strFile, /*[in]*/ int nWidth, /*[in]*/ int nHeight, /*[out,retval]*/ IUnknown** pUnkStream );

};

#endif //__THUMBNAIL_H_
