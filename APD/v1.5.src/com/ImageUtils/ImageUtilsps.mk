
ImageUtilsps.dll: dlldata.obj ImageUtils_p.obj ImageUtils_i.obj
	link /dll /out:ImageUtilsps.dll /def:ImageUtilsps.def /entry:DllMain dlldata.obj ImageUtils_p.obj ImageUtils_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del ImageUtilsps.dll
	@del ImageUtilsps.lib
	@del ImageUtilsps.exp
	@del dlldata.obj
	@del ImageUtils_p.obj
	@del ImageUtils_i.obj
