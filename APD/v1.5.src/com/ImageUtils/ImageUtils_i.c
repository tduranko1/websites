/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Mon Apr 19 16:44:09 2004
 */
/* Compiler settings for C:\websites\apd\v1.3.src\com\ImageUtils\ImageUtils.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_IThumbnail = {0x48A94F78,0x2784,0x48E1,{0x9B,0x97,0xB7,0xB7,0x93,0xB7,0xF9,0x9C}};


const IID LIBID_IMAGEUTILSLib = {0x75C90568,0xCACF,0x4758,{0xA0,0xA5,0x3B,0x37,0x8A,0x30,0x48,0x73}};


const CLSID CLSID_Thumbnail = {0x21C2A265,0x1F81,0x496A,{0x9F,0x6A,0xE6,0x0D,0x4E,0x57,0xAA,0x42}};


#ifdef __cplusplus
}
#endif

