// TestClient.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#import "..\ImageUtils.tlb" no_namespace

void main()
{
	CoInitialize(NULL);

	IThumbnailPtr pThumb( __uuidof(Thumbnail) );

	pThumb->GetThumbnail( L"C:\\Temp\\Sample.jpg", 40, 40 );

	pThumb = NULL;

	CoUninitialize();
}
