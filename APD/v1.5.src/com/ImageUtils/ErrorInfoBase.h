/////////////////////////////////////////////////////////////////////////////
//
//	Error code utility base class for use with ISupportsErrorInfo.
//
/////////////////////////////////////////////////////////////////////////////
#ifndef ERROR_INFO_BASE_H
#define ERROR_INFO_BASE_H

//#include "CBstrImpl.h"
#include "StdString.h"

typedef CStdStr<wchar_t> CBstr;
#define BOUT( s ) s.AllocSysString()

/////////////////////////////////////////////////////////////////////////////
// class CErrorInfoBase
//
// Derive your class from this one to include ISupportsErrorInfo
// and the following error info helper functions.
/////////////////////////////////////////////////////////////////////////////
class CErrorInfoBase
{
public:

	CErrorInfoBase() :
		m_bCanBePooled( TRUE )
	{
	}

	// Sets a specific error message for retrieval by the client.
	// This allows us to pass along error descriptions.
	static void SetComErrorInfo( DWORD dwHelpContext, wchar_t* cszDescription = NULL, REFGUID rGuidInterface = GUID_NULL )
	{
		ICreateErrorInfo *pcerrinfo;
		IErrorInfo *perrinfo;
		HRESULT hr;

		hr = CreateErrorInfo( &pcerrinfo );

		// set fields here by calling ICreateErrorInfo methods on pcerrinfo
		if ( cszDescription != NULL )
		{
			(void) pcerrinfo->SetDescription( cszDescription );
		}

		(void) pcerrinfo->SetHelpContext( dwHelpContext );
		(void) pcerrinfo->SetGUID( rGuidInterface );

		hr = pcerrinfo->QueryInterface( IID_IErrorInfo, (LPVOID FAR*) &perrinfo );

		if ( SUCCEEDED( hr ) )
		{
			(void) SetErrorInfo( 0, perrinfo );
			(void) perrinfo->Release();
		}
		(void) pcerrinfo->Release();

		#ifdef _DEBUG
		_CrtDbgBreak();
		#endif
	}

	// Uses GetLastError() and FormatMessage() to extract the threads
	// last-error code value and pass a message along to SetComErroInfo().
	static void SetComLastErrorInfo( DWORD dwHelpContext, REFGUID rGuidInterface )
	{
		const int cnBufSize = 400;
		LPWSTR szBuf = new WCHAR[ cnBufSize ];

		(void) FormatMessageW( 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ), // Default language
			szBuf,
			cnBufSize,
			NULL 
		);

		SetComErrorInfo( dwHelpContext, szBuf, rGuidInterface );
		delete [] szBuf;
	}

	// Get any descriptions from IErrorInfo.
	// Returns TRUE if successful.
	static BOOL GetComErrorInfo( IUnknown* pUnk, REFGUID rIID, DWORD* pdwHelpContext, BSTR* pbstrDesc = NULL )
	{
		/*lint -save -e64 -e1702 -e1037 */
		// Is an ISupportErrorInfo interface available from this object?
		ISupportErrorInfoPtr pSupport = pUnk;
		if ( pSupport != NULL )
		{
			// Verify that ISupportErrorInfo supported by this interface
			// Note that SUCCEEDED() will not work here because S_FALSE is valid.
			if ( S_OK == pSupport->InterfaceSupportsErrorInfo( rIID ) )
			{
				// Request the IErrorInfo last posted.
				// Note that SUCCEEDED() will not work here because S_FALSE is valid.
				IErrorInfoPtr pError;
				if ( S_OK == GetErrorInfo( 0, &pError ) )
				{
					if ( pdwHelpContext )
					{
						(void) pError->GetHelpContext( pdwHelpContext );
					}
					if ( pbstrDesc )
					{
						(void) pError->GetDescription( pbstrDesc );
					}
					return TRUE;
				}
			}
		}
		return FALSE;
		/*lint -restore */
	}

	// Get HelpContext from current IErrorInfo.
	static DWORD GetComErrorInfoHelpContext()
	{
		// Request the IErrorInfo last posted.
		// Note that SUCCEEDED() will not work here because S_FALSE is valid.
		IErrorInfoPtr pError;
		if ( S_OK == GetErrorInfo( 0, &pError ) )
		{
			DWORD dwHelpContext;
			(void) pError->GetHelpContext( &dwHelpContext );
			return dwHelpContext;
		}

		return 0;
	}

	// Get Description from current IErrorInfo.
	static BSTR GetComErrorInfoDescription()
	{
		// Request the IErrorInfo last posted.
		// Note that SUCCEEDED() will not work here because S_FALSE is valid.
		IErrorInfoPtr pError;
		if ( S_OK == GetErrorInfo( 0, &pError ) )
		{
			BSTR bstrDesc;
			(void) pError->GetDescription( &bstrDesc );
			return bstrDesc;
		}

		return BSTR("");
	}

	// Uses GetLastError() and FormatMessage() to extract the threads
	// last-error code value and return a message.
	static CBstr GetLastErrorInfo()
	{
		try
		{
			DWORD dwError = GetLastError();
			if ( dwError != 0 )
			{
				const int cnBufSize = 400;
				LPWSTR szBuf = new WCHAR[ cnBufSize ];

				(void) FormatMessageW( 
					FORMAT_MESSAGE_FROM_SYSTEM | 
					FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL,
					GetLastError(),
					MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ), // Default language
					szBuf,
					cnBufSize,
					NULL 
				);

				CBstr str( szBuf );
				delete [] szBuf;
				return L"  GetLastError: " + str;
			}
			else return L"  GetLastError == 0.";
		}
		catch (...) {}

		return L"  GetLastErrorInfo() failed!";
	}

	// Used to prelude any error messages with the method name.
	void Method( CBstr strName )
	{
		m_strErrorDescription = strName + L" ";
	}

	// Used to return the method name.
	CBstr Source()
	{
		return m_strMethod; //.AllocSysString();
	}	

	// Used to prelude any error messages with the method name.
	virtual HRESULT Success()
	{
		m_strErrorDescription = L"No Error";
		return S_OK;
	}

	// Used in catch statements to store error descriptions.
	virtual HRESULT HandleError( CBstr strDescription = L"" )
	{
		m_bCanBePooled = FALSE;

		if ( strDescription.length() == 0 )
			m_strErrorDescription += L"Unspecified Error" + GetLastErrorInfo();
		else
			m_strErrorDescription += strDescription + GetLastErrorInfo();
		return E_FAIL;
	}

protected:

	// Holds any internal error descriptions.
	CBstr m_strErrorDescription;
	CBstr m_strMethod;

	// Internal flag to promote killing of pooled object after an error.
	BOOL m_bCanBePooled;

};


#endif	// ERROR_INFO_BASE_H
