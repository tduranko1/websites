/////////////////////////////////////////////////////////////////////////////
// General.cpp
//
// A place to dump general useful goodies.
//
/////////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "General.h"

/////////////////////////////////////////////////////////////////////////////
// Receives the MIME type of an encoder and returns the class identifier
// (CLSID) of that encoder.
//
// The MIME types of the encoders built into GDI+ are as follows: 
//
// image/bmp image/jpeg image/gif image/tiff image/png 
//
// The function calls GetImageEncoders to get an array of ImageCodecInfo
// objects. If one of the ImageCodecInfo objects in that array represents 
// the requested encoder, the function returns the index of the
// ImageCodecInfo object and copies the CLSID into the variable
// pointed to by pClsid. If the function fails, it returns -1. 
/////////////////////////////////////////////////////////////////////////////
int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
   UINT  num = 0;          // number of image encoders
   UINT  size = 0;         // size of the image encoder array in bytes

   ImageCodecInfo* pImageCodecInfo = NULL;

   GetImageEncodersSize(&num, &size);
   if(size == 0)
      return -1;  // Failure

   pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
   if(pImageCodecInfo == NULL)
      return -1;  // Failure

   GetImageEncoders(num, size, pImageCodecInfo);

   for(UINT j = 0; j < num; ++j)
   {
      if( wcscmp(pImageCodecInfo[j].MimeType, format) == 0 )
      {
         *pClsid = pImageCodecInfo[j].Clsid;
         free(pImageCodecInfo);
         return j;  // Success
      }    
   }

   free(pImageCodecInfo);
   return -1;  // Failure
}

/////////////////////////////////////////////////////////////////////////////
// BytesToVariantArry creates an ADSTYPE_OCTET_STRING variant
// from a given set of bytes
// 
// This function can be found in the Platform SDK documention.
/////////////////////////////////////////////////////////////////////////////
HRESULT BytesToVariantArray(
	PBYTE pValue, //Pointer to bytes to put in a variant array.
   ULONG cValueElements,//Size of pValue in bytes.
   VARIANT *pVariant //Return variant containing octet string (VT_UI1|VT_ARRAY).
){
    HRESULT hr = E_FAIL;
    SAFEARRAY *pArrayVal = NULL;
    SAFEARRAYBOUND arrayBound;
    CHAR HUGEP *pArray = NULL;
    
    //Set bound for array
    arrayBound.lLbound = 0;
    arrayBound.cElements = cValueElements;
    
    //Create the safe array for the octet string. unsigned char elements;single dimension;aBound size.
    pArrayVal = SafeArrayCreate( VT_UI1, 1, &arrayBound );
    
    if (!(pArrayVal == NULL) )
    {
        hr = SafeArrayAccessData(pArrayVal, (void HUGEP * FAR *) &pArray );
        if (SUCCEEDED(hr))
        {
            //Copy the bytes to the safe array.
            memcpy( pArray, pValue, arrayBound.cElements );
            SafeArrayUnaccessData( pArrayVal );
            //Set type to array of unsigned char
            V_VT(pVariant) = VT_ARRAY | VT_UI1;
            //Assign the safe array to the array member.
            V_ARRAY(pVariant) = pArrayVal;
            hr = S_OK;
        }
        else
        {
            //Clean up if array can't be accessed.
            if ( pArrayVal )
                SafeArrayDestroy( pArrayVal );
        }
    }
    else
    {
        hr = E_OUTOFMEMORY;
    }
    
    return hr;
}

/////////////////////////////////////////////////////////////////////////////
// Returns the path to the DLL for the passed application.
/////////////////////////////////////////////////////////////////////////////
BOOL GetPath(LPOLESTR szApp, LPTSTR szPath, ULONG cSize)
{
	CLSID clsid;
	LPOLESTR pwszClsid;
	TCHAR  szKey[128];
   HKEY hKey;

   // szPath must be at least 255 char in size
   if (cSize < 255)
		return FALSE;
	
   // Get the CLSID using ProgID
	HRESULT hr = CLSIDFromProgID(szApp, &clsid);
   if (FAILED(hr))
      return FALSE;

   // Convert CLSID to String
   hr = StringFromCLSID(clsid, &pwszClsid);
   if (FAILED(hr))
      return FALSE;

   // Format Registry Key string
   wsprintf(szKey, L"CLSID\\%s\\InprocServer32", pwszClsid);

   // Open key to find path of application
   LONG lRet = RegOpenKeyEx(HKEY_CLASSES_ROOT, szKey, 0, KEY_ALL_ACCESS, &hKey);
   if (lRet != ERROR_SUCCESS) 
		return FALSE;

   // Query value of key to get Path and close the key
   lRet = RegQueryValueEx(hKey, NULL, NULL, NULL, (BYTE*)szPath, &cSize);
   RegCloseKey(hKey);
   if (lRet != ERROR_SUCCESS)
      return FALSE;

   CoTaskMemFree(pwszClsid);

   // Strip off the '/Automation' switch from the path
   WCHAR *x = wcsrchr(szPath, '/');
   if(0!= x) // If no /Automation switch on the path
   {
		int result = x - szPath; 
		szPath[result]  = '\0';  // If switch there, strip it
	}   

   // Strip off the dll name from the path
   x = wcsrchr(szPath, '\\');
   if(0!= x) // If no /Automation switch on the path
   {
		int result = x - szPath; 
		szPath[result]  = '\0';  // If switch there, strip it
	}   

   return TRUE;
}

