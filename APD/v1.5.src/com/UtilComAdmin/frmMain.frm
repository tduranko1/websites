VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmMain 
   Caption         =   "COM+ Admin Util"
   ClientHeight    =   8535
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   12405
   LinkTopic       =   "Form1"
   ScaleHeight     =   8535
   ScaleWidth      =   12405
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   240
      Top             =   7920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483624
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   16776960
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0000
            Key             =   "ArrowDown"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0452
            Key             =   "ArrowUp"
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "&Clear"
      Height          =   255
      Left            =   9480
      TabIndex        =   27
      Top             =   8160
      Width           =   1335
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "E&xit"
      Height          =   255
      Left            =   10920
      TabIndex        =   26
      Top             =   8160
      Width           =   1335
   End
   Begin TabDlg.SSTab tabLogs 
      Height          =   5775
      Left            =   6600
      TabIndex        =   25
      Top             =   1800
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   10186
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      WordWrap        =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Activity"
      TabPicture(0)   =   "frmMain.frx":08A4
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "txtLog"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Warnings"
      TabPicture(1)   =   "frmMain.frx":08C0
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "txtWarning"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Errors"
      TabPicture(2)   =   "frmMain.frx":08DC
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "txtError"
      Tab(2).ControlCount=   1
      Begin VB.TextBox txtError 
         Height          =   5295
         Left            =   -74880
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   30
         Top             =   360
         Width           =   5415
      End
      Begin VB.TextBox txtWarning 
         Height          =   5295
         Left            =   -74880
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   29
         Top             =   360
         Width           =   5415
      End
      Begin VB.TextBox txtLog 
         Height          =   5295
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   28
         Top             =   360
         Width           =   5415
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Config Apps NOT installed in COM+"
      Height          =   3135
      Left            =   120
      TabIndex        =   9
      Top             =   4920
      Width           =   6255
      Begin VB.CommandButton cmdSelectConfig 
         Caption         =   "Select All"
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   1080
         Width           =   2655
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "Add Selected To COM+"
         Height          =   375
         Left            =   120
         TabIndex        =   10
         Top             =   600
         Width           =   2655
      End
      Begin MSComctlLib.ListView lvwAppConfig 
         Height          =   2430
         Left            =   3000
         TabIndex        =   12
         Top             =   480
         Width           =   3000
         _ExtentX        =   5292
         _ExtentY        =   4286
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         _Version        =   393217
         ColHdrIcons     =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Config Apps currently installed in COM+"
      Height          =   3015
      Left            =   120
      TabIndex        =   4
      Top             =   1680
      Width           =   6255
      Begin VB.CommandButton cmdShutDown 
         Caption         =   "Shut Down Selected Apps"
         Height          =   375
         Left            =   120
         TabIndex        =   19
         Top             =   1080
         Width           =   2655
      End
      Begin VB.CommandButton cmdSelectAll 
         Caption         =   "Select All"
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   2040
         Width           =   2655
      End
      Begin VB.CommandButton cmdRemove 
         Caption         =   "Remove Selected From COM+"
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   1560
         Width           =   2655
      End
      Begin VB.CommandButton cmdApplyIdentity 
         Caption         =   "Apply Identity To Selected Apps"
         Enabled         =   0   'False
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   600
         Width           =   2655
      End
      Begin MSComctlLib.ListView lvwApp 
         Height          =   2280
         Left            =   3000
         TabIndex        =   8
         Top             =   480
         Width           =   3075
         _ExtentX        =   5424
         _ExtentY        =   4022
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         _Version        =   393217
         ColHdrIcons     =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Identity"
      Height          =   1335
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6255
      Begin VB.TextBox txtConfirm 
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   3240
         PasswordChar    =   "*"
         TabIndex        =   15
         Top             =   915
         Width           =   2775
      End
      Begin VB.TextBox txtPassword 
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   3240
         PasswordChar    =   "*"
         TabIndex        =   14
         Top             =   585
         Width           =   2775
      End
      Begin VB.TextBox txtUserID 
         BackColor       =   &H00C0C0C0&
         Enabled         =   0   'False
         Height          =   285
         Left            =   3240
         TabIndex        =   13
         Top             =   240
         Width           =   2775
      End
      Begin VB.OptionButton optInteractive 
         Caption         =   "Interactive User"
         Height          =   255
         Left            =   225
         TabIndex        =   2
         Top             =   480
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.OptionButton optUser 
         Caption         =   "This User"
         Height          =   255
         Left            =   225
         TabIndex        =   1
         Top             =   840
         Width           =   1575
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Password:"
         Height          =   255
         Left            =   1560
         TabIndex        =   18
         Top             =   615
         Width           =   1575
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Confirm Password:"
         Height          =   255
         Left            =   1440
         TabIndex        =   17
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "User:"
         Height          =   255
         Left            =   1920
         TabIndex        =   16
         Top             =   300
         Width           =   1215
      End
   End
   Begin MSComctlLib.ListView lvwComp 
      Height          =   1185
      Left            =   6720
      TabIndex        =   3
      Top             =   240
      Width           =   5475
      _ExtentX        =   9657
      _ExtentY        =   2090
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Label lblErrors 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   10560
      TabIndex        =   24
      Top             =   7680
      Width           =   1095
   End
   Begin VB.Label lblWarnings 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   8040
      TabIndex        =   23
      Top             =   7680
      Width           =   1095
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Errors:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   9480
      TabIndex        =   22
      Top             =   7680
      Width           =   855
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "Warnings:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7080
      TabIndex        =   21
      Top             =   7680
      Width           =   855
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   3960
      TabIndex        =   20
      Top             =   8160
      Width           =   4575
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private collApp As COMAdmin.COMAdminCatalogCollection
Private collComp As COMAdmin.COMAdminCatalogCollection
Private objApp As COMAdmin.COMAdminCatalogObject
Private objAppNode As MSXML2.IXMLDOMNode
Private objAppNodeList As MSXML2.IXMLDOMNodeList
Private objCatalog As COMAdmin.COMAdminCatalog
Private objComp As COMAdmin.COMAdminCatalogObject
Private objCompNode As MSXML2.IXMLDOMNode
Private objCompNodeList As MSXML2.IXMLDOMNodeList
Private objFSO As FileSystemObject
Private objXML As MSXML2.DOMDocument

Private mblnAllAppsSelected As Boolean
Private mstrLog As String
Private miAppsSelected As Integer
Private miConfigAppsSelected As Integer

Private mstrAppNames() As String

Private Enum eLog
  eNormalLog
  eErrorLog
  eWarningLog
End Enum

Private Enum eAppList
  eConfigApps
  eInstalledApps
End Enum


Private Sub cmdApplyIdentity_Click()
  Const PROC_NAME = "cmdApplyItentity_Click"
  
  Dim strErrMsg As String
  Dim Item As ListItem
  Dim strUserID As String
  Dim strAppName As String
  
  On Error GoTo errHandler
  
  lblStatus.Caption = ""
  
  If optUser.Value = True Then
    If Len(Trim(txtUserID.Text)) = 0 Or Len(Trim(txtPassword.Text)) = 0 Then
      MsgBox ("Please enter a User AND Password")
      Exit Sub
    End If
    
    If txtPassword.Text <> txtConfirm.Text Then
      MsgBox ("""Password"" and ""Confirm Password"" do not match.")
      Exit Sub
    End If
  End If
  
  mstrLog = "Identity applied to the following Applications: " & vbCrLf
  
  ' Apply identity info specified on form to COM+ applications selected in list.
  For Each Item In lvwApp.ListItems
    If Item.Checked = True Then
      strAppName = Item.Text
      For Each objApp In collApp
        If objApp.Name = strAppName Then
          objApp.Value("Identity") = IIf(optUser.Value = True, Trim(txtUserID.Text), "InteractiveUser")
          objApp.Value("Password") = IIf(optUser.Value = True, Trim(txtPassword.Text), "")
          mstrLog = mstrLog & strAppName & vbCrLf
          Exit For
        End If
      Next
    End If
  Next
  
  ' Commit cached changes to COM+
  collApp.SaveChanges
  
  LogMessage mstrLog, eNormalLog
  lblStatus.Caption = "Finished Applying Identities"
  
errHandler:
  Set Item = Nothing
  
  If Err.Number <> 0 Then
    DestroyObjects
    strErrMsg = "Error while applying Identity to COM+ Application." & vbCrLf & " Application: " & strAppName & _
                " Identity: " & txtUserID.Text & vbCrLf & Err.Description
    MsgBox PROC_NAME & vbCrLf & Err.Source & vbCrLf & strErrMsg, vbExclamation, "COM Config Error"
  End If
End Sub


Private Sub cmdAdd_Click()
  Const PROC_NAME = "cmdAdd_Click"
  
  Dim Item As ListItem
    
  Dim strAppName As String
  Dim idxList As Integer
  Dim strLogMsg As String
  Dim strErrMsg As String
  
  'On Error GoTo errHandler
  
  lblStatus.Caption = ""
  
  For idxList = lvwAppConfig.ListItems.Count To 1 Step -1
    Set Item = lvwAppConfig.ListItems(idxList)
    
    If Item.Checked = True Then
      strAppName = Item.Text
        
      ' Add a new Application and set some properties
      LogMessage "Creating application " & strAppName, eNormalLog
                 
      ' Move checkbox item from config list to installed list.
      If CreateApplication(strAppName) Then
        lvwApp.ListItems.Add , Item.Key, Item.Text
        lvwAppConfig.ListItems.Remove Item.Index
      End If
    End If
  Next
  
  lblStatus.Caption = "Finished Creating COM+ Applications"
  
errHandler:
  Set Item = Nothing
  
  If Err.Number <> 0 Then
    DestroyObjects
    strErrMsg = "Error while creating & configuring COM+ Application." & vbCrLf & " Application: " & strAppName & _
                vbCrLf & Err.Description
    MsgBox PROC_NAME & vbCrLf & Err.Source & vbCrLf & strErrMsg, vbExclamation, "COM Config Error"
  End If
 
End Sub


Private Function CreateApplication(strAppName As String) As Boolean
  Const PROC_NAME = "CreateApplication"

  Dim objAttNode As MSXML2.IXMLDOMAttribute
    
  Dim strErrMsg As String
  Dim strLogMsg As String
  Dim idxApp As Integer
  Dim vValue As Variant
  Dim strComponentName As String
    
  'On Error GoTo errHandler
    
  ' Initialize return value
  CreateApplication = False
        
      
  ' Get the config info for the newly created COM+ app from the config file
  Set objAppNode = objXML.selectSingleNode("/Root/application[@Name='" & strAppName & "']")
    
  ' Check if any components are configured for the application.  If not, bail out.
  If objAppNode.childNodes.length = 0 Then
    LogMessage "The config file contains no components to be configured in Application: " & strAppName, eWarningLog
    Exit Function
  End If
    
  ' Create the COM+ application and retain a reference.
  Set objApp = collApp.Add
             
  ' Ensure COM+ Application is available before continuing.
  If objApp Is Nothing Then
    LogMessage "Could not create COM+ Application: " & strAppName, eErrorLog
    Exit Function
  End If
         
  ' Loop through attributes on XML Application node and set corresponding COM+ Application properties.
  For Each objAttNode In objAppNode.Attributes
    If objAttNode.Name <> "OldName" Then
      Select Case objAttNode.Value
        Case "Server"
          vValue = COMAdminActivationLocal
        Case "Library"
          vValue = COMAdminActivationInproc
        Case "true"
          vValue = True
        Case "false"
          vValue = False
        Case IsNumeric(objAttNode.Value)
          vValue = CLng(objAttNode.Value)
        Case Else
          vValue = objAttNode.Value
      End Select
      
      objApp.Value(objAttNode.Name) = vValue
    End If
  Next
  
  ' Commit cached changes to COM+
  collApp.SaveChanges
                             
                              
  ' Install component(s) within matched or newly created Application.
  For Each objCompNode In objAppNode.childNodes
    
    ' Get the dll name from the config file.
    Set objAttNode = objCompNode.Attributes.getNamedItem("Name")
    If Not objAttNode Is Nothing Then
      strComponentName = App.Path & "\" & objAttNode.nodeTypedValue
      
      ' Ensure the dll exists in the app.path directory
      If objFSO.FileExists(strComponentName) Then
        ' Install the component
        LogMessage Space(3) & "Installing component " & objCompNode.Attributes(0).Text, eNormalLog
        objCatalog.InstallComponent objApp.Key, strComponentName, "", ""
      Else
        LogMessage strComponentName & " does not exist.", eErrorLog
      End If
    Else
      LogMessage "'Name' not specified for component:" & vbCrLf & objCompNode.xml, eErrorLog
    End If
   
  Next
  
  ' Get the components in the current App
  Set collComp = collApp.GetCollection("Components", objApp.Key)
  collComp.Populate
  
  ' If no components were successfully installed, delete the Application.
  If collComp.Count = 0 Then
    strLogMsg = "NO components installed in COM+ Application: " & strAppName & "."
        
    If RemoveApplication(strAppName) Then
      LogMessage strLogMsg & "  Application removed.", eNormalLog
      LogMessage strLogMsg & "  Check ComPlusConfig.xml for component nodes within this application node.", eWarningLog
    Else
      LogMessage strLogMsg & "  There was a problem deleting the Application.  Check component services for empty Application, " & _
                 strAppName & ".", eWarningLog
    End If
    
    Exit Function
  End If
    
  ' Set component level attributes.
  ' Loop through the XML component nodes of the current app node
  For Each objCompNode In objAppNode.childNodes
  
    ' Loop through the components in the current COM+ app.
    For Each objComp In collComp
      
      ' See if the current com+ component is a member of the dll specified in the name attribute of the xml component node.
      If objComp.Value("DLL") = App.Path & "\" & objCompNode.Attributes.getNamedItem("Name").nodeTypedValue Then
    
        LogMessage Space(3) & "Configuring Component: " & objComp.Name, eNormalLog
        
        ' Loop through attributes on XML component node and set corresponding COM+ component properties.
        For Each objAttNode In objCompNode.Attributes
        
          ' Skip the name attribute as it is the dll name we used in the InstallComponent method above.
          If objAttNode.Name <> "Name" Then
            Select Case objAttNode.Value
              Case "true"
                vValue = True
              Case "false"
                vValue = False
              Case IsNumeric(objAttNode.Value)
                vValue = CLng(objAttNode.Value)
              Case Else
                vValue = objAttNode.Value
            End Select
            
            objComp.Value(objAttNode.Name) = vValue
          End If
        Next
      End If
    Next
  Next
    
  ' Commit cached changes to COM+
  collComp.SaveChanges
  
  CreateApplication = True
      
errHandler:
  Set objAttNode = Nothing
  
  If Err.Number <> 0 Then
    DestroyObjects
    strErrMsg = "Error while creating & configuring COM+ Application." & vbCrLf & " Application: " & strAppName & _
                vbCrLf & Err.Description
    MsgBox PROC_NAME & vbCrLf & Err.Source & vbCrLf & strErrMsg, vbExclamation, "COM Config Error"
  End If
End Function


Private Sub cmdExit_Click()
  DestroyObjects
  Unload Me
End Sub

Private Sub cmdRemove_Click()
  Const PROC_NAME = "cmdRemove_Click"
  
  Dim oNode As MSXML2.IXMLDOMNode
  Dim Item As ListItem
  
  Dim idxList As Integer
  Dim strErrMsg  As String
  Dim strAppName As String
  
        
  On Error GoTo errHandler
       
  lblStatus.Caption = ""
  
  ' Loop through the installed list (backwards so as not to hose our index as we will be removing items)
  For idxList = lvwApp.ListItems.Count To 1 Step -1
    Set Item = lvwApp.ListItems(idxList)
        
    ' Process selected items
    If Item.Checked = True Then
      strAppName = Item.Text
      
      ' If Application removed successfully, move checkbox item from installed list to config list.
      If RemoveApplication(strAppName) Then
      
        '  If the passed in App name is an 'OldName' get the 'Name' to put in the config list.
        Set oNode = objXML.selectSingleNode("/Root/application[@OldName='" + strAppName + "']")
        If Not oNode Is Nothing Then
          strAppName = oNode.Attributes.getNamedItem("Name").nodeTypedValue
        End If
        
        lvwAppConfig.ListItems.Add , Item.Key, strAppName
        lvwApp.ListItems.Remove Item.Index
      End If
      
    End If
  Next
        
  lblStatus.Caption = "Finished removing COM+ Applications"
  
errHandler:
      
  Set oNode = Nothing
  Set Item = Nothing
  
  If Err.Number <> 0 Then
    DestroyObjects
    strErrMsg = "Error while removing COM+ Application." & vbCrLf & " Application: " & strAppName & _
                vbCrLf & Err.Description
    MsgBox PROC_NAME & vbCrLf & Err.Source & vbCrLf & strErrMsg, vbExclamation, "COM Config Error"
  End If
End Sub


Private Function RemoveApplication(strAppName As String) As Boolean
  Const PROC_NAME = "RemoveApplication"
    
  Dim objAtt As MSXML2.IXMLDOMAttribute
    
  Dim idxApp As Integer
  Dim vKey As String
  Dim strCmd As String
  Dim strDLLName As String
  Dim strUnRegDLLName As String
  Dim strErrMsg  As String
  
  On Error GoTo errHandler
  
  ' Initialize return value.
  RemoveApplication = False
  idxApp = 0
  
  'Loop through local COM+ applications and try to match the current application node in the XML data file.
  For Each objApp In collApp
        
    If strAppName = objApp.Name Then
      ' If Application already exists, shut it down and remove all components
      
      LogMessage "Shutting down " & strAppName, eNormalLog
      
      ' Shut down the indicated COM+ Application
      objCatalog.ShutdownApplication strAppName
                        
      ' Grab the CLSID of the current COM+ Application
      vKey = objApp.Key
      
      ' Grab the list of components installed in the current COM+ application
      Set collComp = collApp.GetCollection("Components", vKey)
      collComp.Populate
      
      For Each objComp In collComp
        
        LogMessage Space(3) & "Deleting component " & objComp.Name, eNormalLog
        
        ' Remove the first component in the collection until there are no components left.
        collComp.Remove 0
        
        ' Save cached changes to COM+ catalog
        collComp.SaveChanges
      
      Next
                         
      LogMessage "Deleting COM+ Application: " & objApp.Name, eNormalLog
      
      ' Delete COM+ Application
      collApp.Remove idxApp
      
      ' Commit cached changes to COM+ registry
      collApp.SaveChanges
      
      ' Manually unregister dlls.  This allegedly happens when the component is removed from COM+ ...but this is Microsoft crap.
      Set objCompNodeList = objXML.selectNodes("/Root/application[@Name='" & strAppName & "']/component")
      
      For Each objCompNode In objCompNodeList
        Set objAtt = objCompNode.Attributes.getNamedItem("Name")
        If Not objAtt Is Nothing Then
          strDLLName = objAtt.nodeTypedValue
        
          If strDLLName <> "" And strDLLName <> strUnRegDLLName Then
            LogMessage "Unregistering " & strDLLName, eNormalLog
            strCmd = "regsvr32 -u -s " & App.Path & "\" & strDLLName
            Shell strCmd, vbMinimizedNoFocus
          End If
          strUnRegDLLName = strDLLName
          strDLLName = ""
        Else
          LogMessage "Component node has no Name attribute. " & vbCrLf & objCompNode.xml, eErrorLog
        End If
      Next
            
      RemoveApplication = True
      Exit For
    End If
    
    ' Increment index value used to identify COM+ applications within the collection.
    idxApp = idxApp + 1
  Next
      
errHandler:
  Set objAtt = Nothing
    
  If Err.Number <> 0 Then
    strErrMsg = "Error while removing COM+ Application." & vbCrLf & " Application: " & strAppName & _
                vbCrLf & Err.Description
    MsgBox PROC_NAME & vbCrLf & Err.Source & vbCrLf & strErrMsg, vbExclamation, "COM Config Error"
  End If
End Function


Private Sub cmdSelectAll_Click()
  Dim Item As ListItem
  
  mblnAllAppsSelected = CBool(mblnAllAppsSelected = False)
  
  For Each Item In lvwApp.ListItems
    Item.Checked = mblnAllAppsSelected
  Next
End Sub


Private Sub cmdSelectConfig_Click()
  Dim Item As ListItem
  
  mblnAllAppsSelected = CBool(mblnAllAppsSelected = False)
  
  For Each Item In lvwAppConfig.ListItems
    Item.Checked = mblnAllAppsSelected
  Next
End Sub


Private Sub cmdShutDown_Click()
  Dim Item As ListItem
  
  lblStatus.Caption = ""
  mstrLog = "The following COM+ Applications have been shut down: " & vbCrLf
  
  ' Shutdown the COM+ application associated with each item selected in the checkbox list.
  For Each Item In lvwApp.ListItems
    If Item.Checked = True Then
      For Each objApp In collApp
        If objApp.Name = Item.Text Then
          objCatalog.ShutdownApplication (objApp.Name)
          mstrLog = mstrLog & Item.Text & vbCrLf
          Exit For
        End If
      Next
    End If
  Next
  
  collApp.SaveChanges
  
  LogMessage mstrLog, eNormalLog
  lblStatus.Caption = "Finished shutting down COM+ Applications."
End Sub


Private Sub Form_Load()

  Const PROC_NAME = "Form_Load"
  
  Dim objAtt As MSXML2.IXMLDOMAttribute
  Dim Item As ListItem
  Dim objNode As Node
  
  Dim strAppName As String
  Dim strAppOldName As String
  Dim strComponent As String
  Dim blnInCOM As Boolean
  Dim blnOldNameInCOM As Boolean
  Dim strErrMsg As String
  
      
  On Error GoTo errHandler
      
  CreateObjects
  
  lvwApp.ColumnHeaders.Add , , "Applications in COM+", lvwApp.Width * 0.9
  lvwComp.ColumnHeaders.Add , , "Components", lvwComp.Width * 0.9
  lvwAppConfig.ColumnHeaders.Add , , "Applications in COMConfig.xml", lvwAppConfig.Width * 0.9
  
  lvwApp.ColumnHeaders(1).Icon = 2
  lvwAppConfig.ColumnHeaders(1).Icon = 2
  
    
  For Each objAppNode In objAppNodeList
    ' initialize loop variables
    blnInCOM = False
    blnOldNameInCOM = False
    strAppName = ""
    strAppOldName = ""
    
    ' Get Application Name attribute value -- required.
    Set objAtt = objAppNode.Attributes.getNamedItem("Name")
    If Not objAtt Is Nothing Then
      strAppName = objAtt.nodeTypedValue
    Else
      LogMessage "Application node missing 'Name' attribute was skipped." & vbCrLf & objAppNode.xml, eErrorLog
      GoTo SkipNode
    End If
    
    ' Get Application OldName attribute value -- optional.
    Set objAtt = objAppNode.Attributes.getNamedItem("OldName")
    If Not objAtt Is Nothing Then
      strAppOldName = objAtt.nodeTypedValue
    End If
                
    ' Look for a COM+ application with a name matching either the Name or OldName attributes of this Application node.
    For Each objApp In collApp
      If objApp.Name = strAppName Then
        blnInCOM = True
        Exit For
      End If
      
      If objApp.Name = strAppOldName Then
        blnOldNameInCOM = True
        Exit For
      End If
    Next
    
    ' Populate checkbox lists.  Matches found above are added to the 'installed' list, all others are added to the 'config' list.
    If blnInCOM = True Then
      lvwApp.ListItems.Add , strAppName, strAppName
    ElseIf blnOldNameInCOM = True Then
      lvwApp.ListItems.Add , strAppOldName, strAppOldName
    Else
      lvwAppConfig.ListItems.Add , strAppName, strAppName
    End If
    
SkipNode:
  Next
  
errHandler:
  Set Item = Nothing
  Set objNode = Nothing
  Set objAtt = Nothing

  If Err.Number <> 0 Then
    DestroyObjects
    strErrMsg = "Error while removing COM+ Application." & vbCrLf & " Application: " & strAppName & _
                vbCrLf & Err.Description
    MsgBox PROC_NAME & vbCrLf & Err.Source & vbCrLf & strErrMsg, vbExclamation, "COM Config Error"
  End If
End Sub


Private Sub CreateObjects()
  Dim strConfigFile As String
  Dim strDocumentation As String
      
  Set objFSO = New FileSystemObject
  
  
  strConfigFile = App.Path & "\ComPlusConfig.xml"
  
  If Not objFSO.FileExists(strConfigFile) Then
    strDocumentation = "The configuration file, ComPlusConfig.xml, does not exist in the application directory.  If you need to " & _
                       "create ComPlusConfig.xml, use the following " & _
                       "format and place the file along with ComPlusAdmin.exe in a directory with the dlls that need to be installed and " & _
                       "configured in COM+. " & vbCrLf & vbCrLf & "<Root>" & vbCrLf & Space(3) & "<application " & _
                       "Name=""YOURCOMPLUSAPPNAME"" Activation=""SERVER/LIBRARY"">" & vbCrLf & Space(6) & "<component " & _
                       "Name=""YOURDLLNAME.dll""/>" & vbCrLf & Space(3) & "</application>" & vbCrLf & "</Root>" & vbCrLf & vbCrLf & _
                       "Add application and component nodes as needed.  " & _
                       "Add additional attributes to the application and component nodes as needed.  These attributes will represent " & _
                       "application level and component level properties respectively in COM+.  Attribute names must correspond " & _
                       "to valid COM+ Property value names and the attribute values must be valid as well.  Property and value " & _
                       "names can be found in MSDN under ComAdminCatalogObject, specifically the sections dealing with the " & _
                       "Applications Collection and the Components Collection."
                       
    MsgBox strDocumentation, vbInformation, "ComPlusConfig.xml Documentation"
                       
  End If
  
  Set objXML = New MSXML2.DOMDocument40
  objXML.Load strConfigFile
  
  Set objAppNodeList = objXML.selectNodes("/Root/application")
  Set objCatalog = New COMAdmin.COMAdminCatalog
  
  
  ' Retrieve local COM+ applications
  Set collApp = objCatalog.GetCollection("Applications")
  collApp.Populate
End Sub


Private Sub DestroyObjects()
  Set objXML = Nothing
  Set objCatalog = Nothing
  Set collApp = Nothing
  Set collComp = Nothing
  Set objAppNodeList = Nothing
  Set objApp = Nothing
  Set objComp = Nothing
  Set objAppNode = Nothing
  Set objCompNode = Nothing
  Set objFSO = Nothing
End Sub


Private Sub cmdClear_Click()
  txtLog.Text = ""
  txtWarning.Text = ""
  txtError.Text = ""
  lblStatus.Caption = ""
  lblErrors.Caption = "0"
  lblWarnings.Caption = "0"
End Sub


Private Sub Form_Unload(Cancel As Integer)
  DestroyObjects
End Sub


Private Sub lvwApp_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  If lvwApp.SortOrder = lvwAscending Then
    lvwApp.SortOrder = lvwDescending
    lvwApp.ColumnHeaders(1).Icon = 1
  Else
    lvwApp.SortOrder = lvwAscending
    lvwApp.ColumnHeaders(1).Icon = 2
  End If
End Sub

Private Sub lvwApp_ItemCheck(ByVal Item As MSComctlLib.ListItem)
  If Item.Checked = True Then
    miAppsSelected = miAppsSelected + 1
    If miAppsSelected > lvwApp.ListItems.Count Then
      miAppsSelected = lvwApp.ListItems.Count
    End If
  Else
    miAppsSelected = miAppsSelected - 1
    If miAppsSelected < 0 Then
      miAppsSelected = 0
    End If
  End If
End Sub

Private Sub lvwAppConfig_ItemCheck(ByVal Item As MSComctlLib.ListItem)
  If Item.Checked = True Then
    miConfigAppsSelected = miConfigAppsSelected + 1
    If miConfigAppsSelected > lvwApp.ListItems.Count Then
      miConfigAppsSelected = lvwApp.ListItems.Count
    End If
  Else
    miConfigAppsSelected = miAppsSelected - 1
    If miConfigAppsSelected < 0 Then
      miConfigAppsSelected = 0
    End If
  End If
End Sub

' Set Button state for buttons that are only useful if something in their associated list is checked.
Private Sub SetButtonState(list As eAppList)
   
  If list = eInstalledApps Then
    If miAppsSelected = 0 Then
      cmdApplyIdentity.Enabled = False
      cmdShutDown.Enabled = False
      cmdRemove.Enabled = False
    Else
      cmdApplyIdentity.Enabled = True
      cmdShutDown.Enabled = True
      cmdRemove.Enabled = True
    End If
  Else
    If miConfigAppsSelected = 0 Then
      cmdAdd.Enabled = False
    Else
      cmdAdd.Enabled = True
    End If
  End If
End Sub

Private Sub lvwAppConfig_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  If lvwAppConfig.SortOrder = lvwAscending Then
    lvwAppConfig.SortOrder = lvwDescending
    lvwAppConfig.ColumnHeaders(1).Icon = 1
  Else
    lvwAppConfig.SortOrder = lvwAscending
    lvwAppConfig.ColumnHeaders(1).Icon = 2
  End If
End Sub

Private Sub lvwApp_ItemClick(ByVal Item As MSComctlLib.ListItem)
  ShowComponents Item.Text
End Sub


Private Sub lvwAppConfig_ItemClick(ByVal Item As MSComctlLib.ListItem)
  ShowComponents Item.Text
End Sub


Private Sub ShowComponents(strAppName As String)
  Const PROC_NAME = "ShowComponents"
  
  Dim objAtt As MSXML2.IXMLDOMAttribute
  Dim newItem As ListItem
  
  Dim strComponent As String
  Dim strErrMsg As String
  Dim xPath As String
  
  On Error GoTo errHandler
  
  lvwComp.ListItems.Clear
  
  ' Get list of components installed in this app.
  xPath = "/Root/application[@Name=""" & strAppName & """]/component"
  Set objCompNodeList = objXML.selectNodes(xPath)
  
  ' If no components found check if app is installed in COM+ under 'OldName'
  If objCompNodeList Is Nothing Then
    xPath = "/Root/application[@OldName=""" & strAppName & """]/component"
    Set objCompNodeList = objXML.selectNodes(xPath)
  End If
  
  For Each objCompNode In objCompNodeList
    Set objAtt = objCompNode.Attributes.getNamedItem("Name")
    If Not objAtt Is Nothing Then
      strComponent = objAtt.nodeTypedValue
      Set newItem = lvwComp.ListItems.Add
      newItem.Text = strComponent
    Else
      LogMessage "Component node missing 'Name' attribute: " & vbCrLf & objCompNode.xml, eErrorLog
    End If
  Next
  
errHandler:
  Set newItem = Nothing
  Set objAtt = Nothing
  
  If Err.Number <> 0 Then
    strErrMsg = "Error while showing component list for Application: " & strAppName & _
                vbCrLf & Err.Description
    MsgBox PROC_NAME & vbCrLf & Err.Source & vbCrLf & strErrMsg, vbExclamation, "COM Config Error"
  End If
End Sub


Private Sub optInteractive_Click()
  txtUserID.Enabled = False
  txtUserID.BackColor = &HC0C0C0
  txtPassword.Enabled = False
  txtPassword.BackColor = &HC0C0C0
  txtConfirm.Enabled = False
  txtConfirm.BackColor = &HC0C0C0
  cmdApplyIdentity.Enabled = False
End Sub


Private Sub optUser_Click()
  txtUserID.Enabled = True
  txtUserID.BackColor = &HFFFFFF
  txtPassword.Enabled = True
  txtPassword.BackColor = &HFFFFFF
  txtConfirm.Enabled = True
  txtConfirm.BackColor = &HFFFFFF
  txtUserID.SetFocus
  cmdApplyIdentity.Enabled = True
End Sub


Private Sub LogMessage(sEntry As String, iLog As eLog)
  Const PROC_NAME = "LogMessage"
  
  Dim oCounter As Label
  Dim oText As TextBox
  
  Dim intCount As Integer
  Dim sPrefix As String
  Dim strErrMsg As String
  
  On Error GoTo errHandler
     
  Select Case iLog
    Case eNormalLog
      sPrefix = ""
      Set oText = txtLog
    Case eWarningLog
      sPrefix = "WARNING:  "
      Set oText = txtWarning
      Set oCounter = lblWarnings
    Case eErrorLog
      sPrefix = "ERROR:  "
      Set oText = txtError
      Set oCounter = lblErrors
  End Select
      
  If Not oCounter Is Nothing Then
    intCount = IIf(Trim(oCounter.Caption) = "", 0, CInt(oCounter.Caption))
    intCount = intCount + 1
    oCounter.Caption = CStr(intCount)
  End If
  
  oText.Text = oText.Text & sEntry & vbCrLf
  
errHandler:
  Set oCounter = Nothing
  Set oText = Nothing
  
  If Err.Number <> 0 Then
    strErrMsg = "Error while logging message: " & sEntry & vbCrLf & Err.Description
    MsgBox PROC_NAME & vbCrLf & Err.Source & vbCrLf & strErrMsg, vbExclamation, "COM Config Error"
  End If
End Sub

