VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 1  'NoTransaction
END
Attribute VB_Name = "CSession"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Component SessionManager : Class CSession
'*
'* CSession evolved from TSession and QSession in the original SessionManager.
'* Independant transactional and non-tranactional classes are no longer
'* necessary.
'*
'********************************************************************************
Option Explicit

'Name of this object
Private Const MODULE_NAME As String = APP_NAME & "CSession."

'Internal error codes for this class.
Private Enum ErrorCodes
    eBadSessionVariableName = SessionManager_FirstError + &H100
    eBadSessionKey
End Enum

'Session fields in the SessionManager Database
Public Enum enumSM_SessionMgr
    geSM_SessionKey
    geSM_VariableName
    geSM_Value
    geSM_ModifiedBy
    geSM_LastAccessDateTime
End Enum

'********************************************************************************
'* Declaration and initialization
'********************************************************************************
Private mobjEvents As SiteUtilities.CEvents
Private mobjDataAccessor As DataAccessor.CDataAccessor
Private mblnDebugMode As Boolean

'These variables used for Xsl Session calls
Private mstrXslSessionKey As String
Private mstrXslWindowID As String

Private Sub Class_Terminate()
10        TerminateGlobals
End Sub

Private Sub InitializeGlobals()

10        If mobjDataAccessor Is Nothing Then

              'Create private member DataAccessor.
20            Set mobjDataAccessor = New DataAccessor.CDataAccessor
              
              'Share DataAccessor's events object.
30            Set mobjEvents = mobjDataAccessor.mEvents
              
              'Use our own log file.
40            mobjEvents.ComponentInstance = "Session Manager"
          
              'Initialize our member components first, so we have logging set up.
50            mobjDataAccessor.InitEvents App.Path & "\..\config\config.xml", "SessionManager/Debug"
60            mobjDataAccessor.SetConnectString GetConfig("SessionManager/ConnectionString")
              
              'Get debug mode status.
70            mblnDebugMode = mobjEvents.IsDebugMode
              
80        End If
          
End Sub

Private Sub TerminateGlobals()
10        Set mobjEvents = Nothing
20        Set mobjDataAccessor = Nothing
End Sub

'********************************************************************************
'* Returns the requested configuration setting.
'********************************************************************************
Private Function GetConfig(ByVal strSetting As String)
10        GetConfig = mobjEvents.mSettings.GetParsedSetting(strSetting)
End Function

'********************************************************************************
'* Stateless method that retrieves a single session variable value.
'* strWebRoot param is no longer used.
'********************************************************************************
Public Function GetValue( _
    ByVal strSessionKey As String, _
    ByVal strVariableName As String, _
    Optional ByVal strWebRoot As String = "") As Variant

          Const PROC_NAME As String = MODULE_NAME & "GetValue: "

          Dim varData As Variant

10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Add passed arguments to debug info.
30        If mblnDebugMode Then mobjEvents.Trace _
              "SessionKey = " & strSessionKey & vbCrLf & _
              "  VarName = " & strVariableName & vbCrLf & _
              "  WebRoot = " & strWebRoot, PROC_NAME

40        If Not HasValue(strVariableName) Then
50            Err.Raise eBadSessionVariableName, PROC_NAME, "Invalid Session variable name."
60        End If

70        If Not HasValue(strSessionKey) Then
80            Err.Raise eBadSessionKey, PROC_NAME
90        End If

100       strSessionKey = DecryptSessionKey(strSessionKey)

110       varData = GetSelectedRecord(strSessionKey, strVariableName)

120       If NumRows(varData) Then
130           GetValue = varData(geSM_Value, 0)
140       End If

150       If mblnDebugMode Then mobjEvents.Trace "RetVal = " & GetValue, PROC_NAME & "Finished"

ErrorHandler:

160       If Err.Number <> 0 Then
170           mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & "SessionKey=" & strSessionKey & " VarName=" & strVariableName & " WebRoot=" & strWebRoot
180       End If

End Function

'********************************************************************************
'* Stateless method that returns a session record.  DEPRACATED.
'* strWebRoot param is no longer used.
'********************************************************************************
Public Function GetRecord( _
    ByVal strSessionKey As String, _
    ByVal strVariableName As String, _
    Optional ByVal strWebRoot As String = "") As String

          Const PROC_NAME As String = MODULE_NAME & "GetRecord: "

10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Add passed arguments to debug info.
30        If mblnDebugMode Then mobjEvents.Trace _
              "SessionKey = " & strSessionKey & vbCrLf & _
              "  VarName = " & strVariableName & vbCrLf & _
              "  WebRoot = " & strWebRoot, PROC_NAME

40        If Not HasValue(strVariableName) Then
50            Err.Raise eBadSessionVariableName, PROC_NAME, "Invalid Session variable name."
60        End If

70        If Not HasValue(strSessionKey) Then
80            Err.Raise eBadSessionKey, PROC_NAME
90        End If

100       strSessionKey = DecryptSessionKey(strSessionKey)

110       GetRecord = GetSelectedRecord(strSessionKey, strVariableName)

120       If mblnDebugMode Then mobjEvents.Trace "RetVal = " & GetRecord, PROC_NAME & "Finished"

ErrorHandler:

130       If Err.Number <> 0 Then
140           mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & "SessionKey=" & strSessionKey & " VarName=" & strVariableName & " WebRoot=" & strWebRoot
150       End If

End Function

'********************************************************************************
'* Stateless method that returns a session record.  DEPRACATED.
'* strWebRoot param is no longer used.
'********************************************************************************
Public Function GetRecords( _
    ByVal strSessionKey As String, _
    Optional ByVal strWebRoot As String = "") As Variant

          Const PROC_NAME As String = MODULE_NAME & "GetRecords: "

10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Add passed arguments to debug info.
30        If mblnDebugMode Then mobjEvents.Trace _
              "SessionKey = " & strSessionKey & vbCrLf & _
              "  WebRoot = " & strWebRoot, PROC_NAME

40        If Not HasValue(strSessionKey) Then
50            Err.Raise eBadSessionKey, PROC_NAME
60        End If

70        strSessionKey = DecryptSessionKey(strSessionKey)
80        GetRecords = GetSelectedRecords(strSessionKey)

ErrorHandler:

90        If Err.Number <> 0 Then
100           mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & "SessionKey=" & strSessionKey & " WebRoot=" & strWebRoot
110       End If

End Function

'********************************************************************************
'* Determines if a Session record already exists for the specified var name.
'* strWebRoot param is no longer used.
'********************************************************************************
Public Function Exists( _
    ByVal strSessionKey As String, _
    Optional ByVal strVariableName As String = "", _
    Optional ByVal strWebRoot As String = "") As Boolean

          Const PROC_NAME As String = MODULE_NAME & "Exists: "

10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          Dim objRs As ADODB.Recordset

          Dim varData As Variant
          Dim strProcName As String

          'Add passed arguments to debug info.
30        If mblnDebugMode Then mobjEvents.Trace _
              "SessionKey = " & strSessionKey & vbCrLf & _
              "  VarName = " & strVariableName & vbCrLf & _
              "  WebRoot = " & strWebRoot, PROC_NAME

40        If Not HasValue(strSessionKey) Then
50            Err.Raise eBadSessionKey, PROC_NAME
60        End If

70        strSessionKey = DecryptSessionKey(strSessionKey)

80        If Not HasValue(strVariableName) Then
90            strProcName = GetConfig("SessionManager/ExistSessionSP") '"uspSessionExists"

100           Set objRs = mobjDataAccessor.OpenRecordsetSp(strProcName, "'" & strSessionKey & "'")

110           If Not objRs.EOF Then
120               varData = objRs.GetRows
130           End If
140       Else
150           varData = GetSelectedRecord(strSessionKey, strVariableName)
160       End If

170       Exists = CBool(NumRows(varData) > 0)

180       If mblnDebugMode Then mobjEvents.Trace "Exists = " & Exists, PROC_NAME & "Finished"

ErrorHandler:

190       CloseObject objRs
200       Set objRs = Nothing

210       If Err.Number <> 0 Then
220           mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & "SessionKey=" & strSessionKey & " VarName=" & strVariableName & " WebRoot=" & strWebRoot
230       End If

End Function

'********************************************************************************
'* Private util method that gets the Session record for the specified
'* Session Key & Variable Name
'********************************************************************************
Private Function GetSelectedRecord( _
    ByVal strSessionKey As String, _
    ByVal strVariableName As String) As Variant

          Const PROC_NAME As String = MODULE_NAME & "GetSelectedRecord: "

          Dim objRs As ADODB.Recordset
          Dim strProcName As String
          Dim varData As Variant

10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Add passed arguments to debug info.
30        If mblnDebugMode Then mobjEvents.Trace _
              "SessionKey = " & strSessionKey & vbCrLf & _
              "  VarName = " & strVariableName, PROC_NAME

40        strProcName = GetConfig("SessionManager/SelectSessionDataSP") '"uspSessionDataGet"

50        Set objRs = mobjDataAccessor.OpenRecordsetSp(strProcName, "'" & strSessionKey & "'", "'" & strVariableName & "'")

60        If Not objRs.EOF Then
70            varData = objRs.GetRows
80        End If

90        varData = EncryptSessionKeys(varData)
100       GetSelectedRecord = varData

ErrorHandler:

110       CloseObject objRs
120       Set objRs = Nothing

130       If Err.Number <> 0 Then
140           mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & "SessionKey=" & strSessionKey & " VarName=" & strVariableName
150       End If

End Function

'********************************************************************************
'* Private util method that returns all session data for the specified key.
'* DEPRACATED.
'********************************************************************************
Private Function GetSelectedRecords(ByVal strSessionKey As String) As Variant

          Const PROC_NAME As String = MODULE_NAME & "GetSelectedRecords: "

          Dim objRs As ADODB.Recordset
          Dim strProcName As String
          Dim varData As Variant

10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Add passed arguments to debug info.
30        If mblnDebugMode Then mobjEvents.Trace _
              "SessionKey = " & strSessionKey, PROC_NAME

40        strProcName = GetConfig("SessionManager/SelectSessionDataSP") '"uspSessionDataGet"

50        Set objRs = mobjDataAccessor.OpenRecordsetSp(strProcName, "'" & strSessionKey & "'")

60        If Not objRs.EOF Then
70            varData = objRs.GetRows
80        End If

90        varData = EncryptSessionKeys(varData)

100       GetSelectedRecords = varData

ErrorHandler:

110       CloseObject objRs
120       Set objRs = Nothing

130       If Err.Number <> 0 Then
140           mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & "SessionKey=" & strSessionKey
150       End If

End Function

'********************************************************************************
'* Touch (update Date Time/stamp) the Session.  DEPRACATED
'* strWebRoot param is no longer used.
'********************************************************************************
Public Function UpdateLastAccessDateTime( _
    ByVal strSessionKey As String, _
    Optional ByVal strWebRoot As String = "")

          Const PROC_NAME As String = MODULE_NAME & "UpdateLastAccessDateTime: "

          Dim strProcName As String
          Dim varData As Variant
          Dim lngRet As Long

10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Add passed arguments to debug info.
30        If mblnDebugMode Then mobjEvents.Trace _
              "SessionKey = " & strSessionKey & vbCrLf & _
              "  WebRoot = " & strWebRoot, PROC_NAME

40        strSessionKey = DecryptSessionKey(strSessionKey)

50        strProcName = GetConfig("SessionManager/UpdateSessionSP") '"uspSessionUpd"

60        lngRet = mobjDataAccessor.ExecuteSp(strProcName, "'" & strSessionKey & "'", Null) 'use server timestamp

ErrorHandler:

70        If Err.Number <> 0 Then
80            mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & "SessionKey=" & strSessionKey & " WebRoot=" & strWebRoot
90        End If

End Function

'********************************************************************************
'* Creates a new session in DB, returns Encrypted ID.
'* strWebRoot param is no longer used.
'********************************************************************************
Public Function CreateSession( _
    ByVal strUserID As String, _
    Optional ByVal strWebRoot As String = "") As Variant

          Const PROC_NAME As String = MODULE_NAME & "CreateSession: "

          Dim objRs As ADODB.Recordset
          
          Dim strSessionKey As String
          Dim strProcName As String
          Dim varData As Variant

10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Add passed arguments to debug info.
30        If mblnDebugMode Then mobjEvents.Trace _
              "UserID = " & strUserID & vbCrLf & _
              "  WebRoot = " & strWebRoot, PROC_NAME

          'set Site Name, registry subkey setting
40        strProcName = GetConfig("SessionManager/CreateSessionSP") '"uspSessionIns"

50        If Not HasValue(strUserID) Then
60            strUserID = "0"
70        End If

80        Set objRs = mobjDataAccessor.OpenRecordsetSp(strProcName, "'" & strUserID & "'")

90        If Not objRs.EOF Then
100           varData = objRs.GetRows
110       End If

120       strSessionKey = varData(0, 0)
130       strSessionKey = EncryptSessionKey(strSessionKey)

140       CreateSession = strSessionKey

150       If mblnDebugMode Then mobjEvents.Trace "SessionKey = " & strSessionKey, PROC_NAME

ErrorHandler:

160       CloseObject objRs
170       Set objRs = Nothing

180       If Err.Number <> 0 Then
190           mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & "UserId=" & strUserID & " WebRoot=" & strWebRoot
200       End If

End Function

'********************************************************************************
'* Updates a session variable.
'* strWebRoot param is no longer used.
'********************************************************************************
Public Sub UpdateSession( _
    ByVal strSessionKey As String, _
    ByVal strVariableName As String, _
    ByVal varValue As Variant, _
    Optional ByVal strWebRoot As String = "")

          Const PROC_NAME As String = MODULE_NAME & "UpdateSession: "

10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Add passed arguments to debug info.
30        If mblnDebugMode Then mobjEvents.Trace _
              "SessionKey = " & strSessionKey & vbCrLf & _
              "  VarName = " & strVariableName & vbCrLf & _
              "  VarValue = " & CStr(varValue) & vbCrLf & _
              "  WebRoot = " & strWebRoot, PROC_NAME

          'Must have a valid session
40        If Not HasValue(strSessionKey) Then
50            Err.Raise eBadSessionKey, "HasValue( strSessionKey )"
60        End If

70        strSessionKey = DecryptSessionKey(strSessionKey)
          
80        UpdateSingleRecord strSessionKey, strVariableName, varValue

ErrorHandler:

90        If Err.Number <> 0 Then
100           mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & "SessionKey=" & strSessionKey & " VarName=" & strVariableName & " WebRoot=" & strWebRoot
110       End If

End Sub

'********************************************************************************
'* Deletes an entire session from DB.
'* Garbage cleanup is done automatically by the database, so this call is no
'*   longer used but is still valid.
'* strWebRoot param is no longer used.
'********************************************************************************
Public Sub DeleteSession( _
    ByVal strSessionKey As String, _
    Optional ByVal strWebRoot As String = "")

          Const PROC_NAME As String = MODULE_NAME & "DeleteSession: "

10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Add passed arguments to debug info.
30        If mblnDebugMode Then mobjEvents.Trace _
              "SessionKey = " & strSessionKey & vbCrLf & _
              "  WebRoot = " & strWebRoot, PROC_NAME

40        If HasValue(strSessionKey) Then
50            strSessionKey = DecryptSessionKey(strSessionKey)
60            DeleteSingleRecord strSessionKey
70        End If

ErrorHandler:

80        If Err.Number <> 0 Then
90            mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & "SessionKey=" & strSessionKey & " WebRoot=" & strWebRoot
100       End If

End Sub

'********************************************************************************
'* Support function to insert Session Variable.
'* This method is DEPRACATED as the stored procedures now decide whether to
'*   do an insert or an update.
'* strWebRoot param is no longer used.
'********************************************************************************
Private Function InsertSingleRecord( _
    ByVal strSessionKey As String, _
    ByVal strVariableName As String, _
    ByVal varValue As Variant) As Variant

          Const PROC_NAME As String = MODULE_NAME & "InsertSingleRecord: "

          Dim strProcName As String
          Dim varData As Variant
          Dim lngRet As Long

10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Add passed arguments to debug info.
30        If mblnDebugMode Then mobjEvents.Trace _
              "SessionKey = " & strSessionKey & vbCrLf & _
              "  VarName = " & strVariableName & vbCrLf & _
              "  VarValue = " & CStr(varValue), PROC_NAME

40        strProcName = GetConfig("SessionManager/InsertSessionDataSP")  '"uspSessionDataIns"

50        lngRet = mobjDataAccessor.ExecuteSp(strProcName, "'" & strSessionKey & "'", "'" & strVariableName & "'", "'" & varValue & "'", "'0'")

60        InsertSingleRecord = strSessionKey

ErrorHandler:

70        If Err.Number <> 0 Then
80            mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & "SessionKey=" & strSessionKey & " VarName=" & strVariableName & " Value=" & varValue
90        End If

End Function

'********************************************************************************
'* Support function to update Session Variable
'********************************************************************************
Private Function UpdateSingleRecord( _
    ByVal strSessionKey As String, _
    ByVal strVariableName As String, _
    ByVal varValue As Variant) As Variant

          Const PROC_NAME As String = MODULE_NAME & "UpdateSingleRecord: "

          Dim strProcName As String
          Dim varData As Variant
          Dim lngRet As Long

10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Add passed arguments to debug info.
30        If mblnDebugMode Then mobjEvents.Trace _
              "SessionKey = " & strSessionKey & vbCrLf & _
              "  VarName = " & strVariableName & vbCrLf & _
              "  VarValue = " & CStr(varValue), PROC_NAME

40        strProcName = GetConfig("SessionManager/UpdateSessionDataSP")  '"uspSessionDataUpd"

50        lngRet = mobjDataAccessor.ExecuteSp(strProcName, "'" & strSessionKey & "'", "'" & strVariableName & "'", "'" & varValue & "'")

ErrorHandler:

60        If Err.Number <> 0 Then
70            mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & "SessionKey=" & strSessionKey & " VarName=" & strVariableName & " Value=" & varValue
80        End If

End Function

'********************************************************************************
'* Support function to delete Session Variable
'* Garbage cleanup is done automatically by the database, so this call is no
'*   longer used but is still valid.
'********************************************************************************
Private Function DeleteSingleRecord(ByVal strSessionKey As String) As Variant
          Const PROC_NAME As String = MODULE_NAME & "DeleteSingleRecord: "

          Dim strProcName As String
          Dim varData As Variant

10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Add passed arguments to debug info.
30        If mblnDebugMode Then mobjEvents.Trace "SessionKey = " & strSessionKey, PROC_NAME

40        strProcName = GetConfig("SessionManager/DeleteSessionSP") '"uspSessionDel"

50        DeleteSingleRecord = mobjDataAccessor.ExecuteSp(strProcName, "'" & strSessionKey & "'")

ErrorHandler:

60        If Err.Number <> 0 Then
70            mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & "SessionKey=" & strSessionKey
80        End If

End Function

'********************************************************************************
'* Initialize CSession for stateful session calls made from style sheets.
'********************************************************************************
Public Function XslInitSession(ByVal strXslSessionKey As String, ByVal strXslWindowID As String)
          Const PROC_NAME As String = MODULE_NAME & "XslInitSession: "
          
10        On Error GoTo ErrorHandler
          
20        InitializeGlobals
          
30        mobjEvents.Assert strXslSessionKey <> "", "Passed Session Key was blank."
40        mobjEvents.Assert strXslWindowID <> "", "Passed Window ID was blank."
          
50        mstrXslSessionKey = strXslSessionKey
60        mstrXslWindowID = strXslWindowID
          
ErrorHandler:

70        If Err.Number <> 0 Then
80            mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
90        End If

End Function

'********************************************************************************
'* Returns a session variable value.  XslInitSession must be called first.
'* Used for stateful session calls made from style sheets.
'********************************************************************************
Public Function XslGetSession(ByVal strVariableName As String) As String
          Const PROC_NAME As String = MODULE_NAME & "XslGetSession: "
          
10        On Error GoTo ErrorHandler
          
20        mobjEvents.Assert mstrXslSessionKey <> "", "Session Key was blank.  InitSession must be called first."
30        mobjEvents.Assert mstrXslWindowID <> "", "Window ID was blank.  InitSession must be called first."
40        mobjEvents.Assert strVariableName <> "", "Passed variable name was blank."
          
50        XslGetSession = GetValue(mstrXslSessionKey, strVariableName & "-" & mstrXslWindowID)

ErrorHandler:

60        If Err.Number <> 0 Then
70            mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
80        End If

End Function

'********************************************************************************
'* Updates a session variable value.  XslInitSession must be called first.
'* Used for stateful session calls made from style sheets.
'********************************************************************************
Public Function XslUpdateSession(ByVal strVariableName As String, ByVal strValue As String) As String
          Const PROC_NAME As String = MODULE_NAME & "XslUpdateSession: "
          
10        On Error GoTo ErrorHandler
          
20        mobjEvents.Assert mstrXslSessionKey <> "", "Session Key was blank.  XslInitSession must be called first."
30        mobjEvents.Assert mstrXslWindowID <> "", "Window ID was blank.  XslInitSession must be called first."
40        mobjEvents.Assert strVariableName <> "", "Passed variable name was blank."
          
50        UpdateSession mstrXslSessionKey, strVariableName & "-" & mstrXslWindowID, strValue

ErrorHandler:

60        If Err.Number <> 0 Then
70            mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
80        End If

End Function

'********************************************************************************
'* Updates a session variable value with XML from a DOMNodeList.
'* XslInitSession must be called first.
'* Used for stateful session calls made from style sheets.
'********************************************************************************
Public Function XslUpdateSessionNodeList(ByVal strVariableName As String, ByRef objNodeList As Object) As String
          Const PROC_NAME As String = MODULE_NAME & "XslUpdateSessionNodeList: "
          
10        On Error GoTo ErrorHandler
          
          Dim oNodeList As IXMLDOMNodeList
          Dim oNode As IXMLDOMNode
          Dim strXml As String
          
          'Type tye passed object reference
20        Set oNodeList = objNodeList
          
30        For Each oNode In oNodeList
40            strXml = strXml & oNode.xml
50        Next
          
60        XslUpdateSession strVariableName, strXml

ErrorHandler:

70        If Err.Number <> 0 Then
80            mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
90        End If

End Function
