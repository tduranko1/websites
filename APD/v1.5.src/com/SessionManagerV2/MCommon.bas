Attribute VB_Name = "MCommon"
'********************************************************************************
'* Component SessionManager : Module MCommon
'*
'* Common utility functions
'*
'********************************************************************************
Option Explicit

'Encrpt key for session
Public Const giEncryptKey As Integer = 117

Public Const glERR_VALIDATION As Long = vbObjectError + 1
Public Const gsERR_VALIDATION As String = "Validation error"

'for error messages
Private Const MODULE_NAME As String = "MCommon."
'errors from these Methods are bubbled up to the caller

'********************************************************************************
'* Procedure:    Public Method CloseObject
'* Created on:   10/22/01
'* Module:       basCommon
'* Module File:  C:\APD Development\Com\SessionMgr\Common.bas
'*
'* Parameters:
'* ByRef oObj
'********************************************************************************
Function CloseObject(ByRef oObj As Object)
10        If Not oObj Is Nothing Then oObj.Close
End Function

'********************************************************************************
'* Procedure:    Public Method NumRows
'* Created on:   10/22/01
'* Module:       basCommon
'* Module File:  C:\APD Development\Com\SessionMgr\Common.bas
'*
'* Parameters:
'* vArray
'*
'*   Get the number of rows in an Array
'*
'********************************************************************************
Public Function NumRows(varArray) As Long
          
10        If IsArray(varArray) Then
20           If IsArray(varArray, 2) Then
30              If Not IsEmpty(varArray) Then
40                 NumRows = UBound(varArray, 2) + 1
50              Else
60                 NumRows = 0
70              End If
80           Else
90              NumRows = 1
100          End If
110       Else
120          NumRows = 0
130       End If
         
End Function

'********************************************************************************
'* Procedure:    Public Method IsArray
'* Created on:   10/22/01
'* Module:       basCommon
'* Module File:  C:\APD Development\Com\SessionMgr\Common.bas
'*
'* Parameters:
'* vArray
'* Optional ByVal iDimension
'*
'* --- Determine if the variant is an array and
'* --- optionally determine if it is an array of a specified dimension.
'*
'********************************************************************************
Public Function IsArray(varArray, Optional ByVal intDimension As Integer = 1) As Boolean
          
10        On Error GoTo ErrorHandler
          
          Dim varx As Variant
          
20        varx = UBound(varArray, intDimension)
          
          '--- No error raised so return True.
30        IsArray = True
          
40        Exit Function
ErrorHandler:
50        IsArray = False
End Function

'********************************************************************************
'* Procedure:    Public Method HasValue
'* Created on:   10/22/01
'* Module:       basCommon
'* Module File:  C:\APD Development\Com\SessionMgr\Common.bas
'*
'* Parameters:
'* vValue
'*
'* --- Determine if there is a value
'*
'********************************************************************************
Public Function HasValue(ByVal varValue) As Boolean
          
10        On Error GoTo ErrorHandler
          
20        If IsMissing(varValue) Then
30            HasValue = False
40        ElseIf IsNull(varValue) Or Len(varValue) = 0 Then
50            HasValue = False
60        Else
70            HasValue = True
80        End If
          
90    Exit Function
ErrorHandler:
100       If IsArray(varValue) Or IsObject(varValue) Then
110           HasValue = True
120       Else
130           HasValue = False
140       End If
End Function

'********************************************************************************
'* Procedure:    Public Method IsGUID
'* Created on:   10/22/01
'* Module:       basCommon
'* Module File:  C:\APD Development\Com\SessionMgr\Common.bas
'*
'* Parameters:
'* vValue
'*
'* --- Determine if the value could be a GUID
'*
'********************************************************************************
Function IsGUID(ByVal varValue As Variant) As Boolean
          
10        If Len(varValue) = 0 Then
20            IsGUID = False
30        ElseIf Left(varValue, 1) = "{" And Right(varValue, 1) = "}" Then
40            IsGUID = True
50        Else
60            IsGUID = False
70        End If

End Function

'********************************************************************************
'* Procedure:    Public Method EncryptSessionKey
'* Created on:   10/22/01
'* Module:       basCommon
'* Module File:  C:\APD Development\Com\SessionMgr\Common.bas
'*
'* Parameters:
'* sSessionKey
'*
'* --- Encrypt the Session Key value
'*
'********************************************************************************
Function EncryptSessionKey(ByVal strSessionKey As String) As String
          
10        If IsGUID(strSessionKey) Then
20            EncryptSessionKey = strSessionKey 'EncryptString(strSessionKey, giEncryptKey)
30        Else
40            EncryptSessionKey = "" 'strSessionKey
50        End If

End Function

'********************************************************************************
'* Procedure:    Public Method DecryptSessionKey
'* Created on:   10/22/01
'* Module:       basCommon
'* Module File:  C:\APD Development\Com\SessionMgr\Common.bas
'*
'* Parameters:
'* sSessionKey
'*
'* --- Decrypt the Session Key value
'*
'********************************************************************************
Function DecryptSessionKey(ByVal strSessionKey As String) As String
          
10        If IsGUID(strSessionKey) Then
20            DecryptSessionKey = strSessionKey
30        Else
              'Backwards compatibility to old session keys.
40            DecryptSessionKey = EncryptString(strSessionKey, giEncryptKey)
50        End If

End Function

'********************************************************************************
'* Procedure:    Public Method EncryptString
'* Created on:   10/22/01
'* Module:       basCommon
'* Module File:  C:\APD Development\Com\SessionMgr\Common.bas
'*
'* Parameters:
'* sText
'* iEncryptKey
'*
'* --- Encrypts the specified text using the specified encryption integer key.
'*
'********************************************************************************
Function EncryptString(ByVal strText As String, ByVal intEncryptKey As Integer) As String
          Const PROC_NAME As String = MODULE_NAME & ".EncryptString"

          Dim strTemp As String
          Dim inti As Integer
          
10        If intEncryptKey > 255 Or intEncryptKey <= 0 Then
20            intEncryptKey = 255
30        End If
          
          '--- Loop on the specified text one character at a time.
40        For inti = 1 To Len(strText)
              '--- Build the Temp string one encrypted character at a time.
50            strTemp = strTemp + Chr$(Asc(Mid(strText, inti, 1)) Xor intEncryptKey)
60        Next inti
          '--- Return the encrypted string
70        EncryptString = strTemp
          
End Function

'********************************************************************************
'* Pads a number out with zeros to make the requested number of digits.
'********************************************************************************
Private Function ZeroPad(ByVal lngValue As Long, ByVal intDigits As Integer) As String
          Dim strValue As String
          
10        strValue = CStr(lngValue)
20        While Len(strValue) < intDigits
30            strValue = "0" & strValue
40        Wend

50        ZeroPad = strValue
End Function

'********************************************************************************
'* Procedure:    Public Method EncryptSessionKeys
'* Created on:   10/22/01
'* Module:       basCommon
'* Module File:  C:\APD Development\Com\SessionMgr\Common.bas
'*
'* Parameters:
'* vData
'*
'* --- Encrypt the Session Key for each row
'*
'********************************************************************************
Public Function EncryptSessionKeys(ByVal varData As Variant) As Variant

          Dim lngRow As Long

10        For lngRow = 0 To NumRows(varData) - 1
20            varData(geSM_SessionKey, lngRow) = EncryptSessionKey(varData(geSM_SessionKey, lngRow))
30        Next

40        EncryptSessionKeys = varData
          
End Function


