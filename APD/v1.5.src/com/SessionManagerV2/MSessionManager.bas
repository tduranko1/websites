Attribute VB_Name = "MSessionManager"
'********************************************************************************
'* Component SessionManager : Module MDataAccessor
'*
'* Global Consts and Utility Methods
'*
'********************************************************************************
Option Explicit

Public Const APP_NAME = "SessionManager."

Public Const SessionManager_FirstError As Long = vbObjectError + &H22000

'********************************************************************************
'* Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
'* Parameters:  strObjectName = The object to call CreateObject for.
'* Purpose:     Wraps CreateObject() with better debug information.
'* Returns:     The Object created or Nothing.
'********************************************************************************
Public Function CreateObjectEx(strObjectName) As Object
          Dim obj As Object

10        On Error GoTo ErrorHandler

20        Set CreateObjectEx = Nothing

30        Set obj = CreateObject(strObjectName)

40        If obj Is Nothing Then
50            Err.Raise SessionManager_FirstError + 10, "", "CreateObject('" & strObjectName & "') returned Nothing."
60        End If

70        Set CreateObjectEx = obj

ErrorHandler:

80        Set obj = Nothing

90        If Err.Number <> 0 Then
100           Err.Raise Err.Number, "CreateObjectEx('" & strObjectName & "') " & Err.Source, Err.Description & " " & strObjectName
110       End If

End Function

