Attribute VB_Name = "Module1"
'********************************************************************************
'* Component ReportUtils : Module MReportUtils
'********************************************************************************
Option Explicit

Public Const APP_NAME As String = "ReportUtils."

Private Const MODULE_NAME As String = APP_NAME & "MReportUtils"

Public g_objEvents As Object
Public g_blnDebugMode As Boolean         'Are we in debug mode?? -- SiteUtilities

'Internal error codes for this module.
Private Enum EventCodes
    eFileDoesNotExist = &H80065000 + &H400
End Enum


'********************************************************************************
'* Creates the object from the class string specified and returns it
'********************************************************************************
Public Function CreateObjectEx(strClassString As String) As Object
Const PROC_NAME As String = "CreateObjectEx()"
    On Error GoTo errHandler
    Dim strMsg As String
    
    Set CreateObjectEx = CreateObject(strClassString)
    
    Exit Function
errHandler:
    strMsg = "    Error creating """ + strClassString + """ class object"

    g_objEvents.HandleEvent Err.Number, PROC_NAME & Err.Source, Err.Description, _
        PROC_NAME & vbCrLf & strMsg
    
End Function

'********************************************************************************
'* Reads in the passed file and returns a Byte array.
'********************************************************************************
Public Function ReadBinaryDataFromFile(ByVal strFileName As String) As Variant
    Const PROC_NAME As String = MODULE_NAME & "ReadBinaryDataFromFile: "

    On Error GoTo ErrorHandler

    Dim intFile As Integer
    Dim lngLen As Long
    Dim arrBytes() As Byte
    
    'Open the file.
    intFile = FreeFile()
    Open strFileName For Binary Access Read As intFile
    
    'Read in the binary data.
    lngLen = FileLen(strFileName)
    
    If lngLen < 1 Then
        Err.Raise eFileDoesNotExist, PROC_NAME & "Open", "File not found"
    End If
    
    ReDim arrBytes(lngLen - 1)
    Get intFile, , arrBytes
    
    'Close the file
    Close intFile
    
    'Return the binary data
    ReadBinaryDataFromFile = arrBytes
    
ErrorHandler:

    If Err.Number <> 0 Then
        g_objEvents.HandleEvent Err.Number, PROC_NAME & Err.Source, _
            Err.Description, "File Name = " & strFileName
    End If

End Function

'********************************************************************************
'* Returns the requested configuration setting.
'********************************************************************************
Public Function GetConfig(ByVal strSetting As String) As String
10        GetConfig = g_objEvents.mSettings.GetParsedSetting(strSetting)
End Function

