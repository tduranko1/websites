VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Dim m_sConnect As String

Dim m_rs As ADODB.Recordset
Dim m_sConfigPath As String
Dim m_objDataPresenter As Object
Dim m_sReportsDBConnectionString As String

Const ReportUtils_FirstError As Long = &H80067800
'Error codes specific to this class.
Private Enum Error_Codes
    eActiveXCreate = ReportUtils_FirstError + 100
    eInvalidConnectString
    eInvalidFile
    eCrystalError
    eInvalidReportParams
    eMissingReportParams
End Enum

Private Sub Class_Initialize()
      Const PROC_NAME As String = "Initialize: "
On Error GoTo errHandler:
10        m_sConfigPath = App.Path & "\..\config\config.xml"

          'Create the DataPresenter object to retrieve the recordset
20        Set m_objDataPresenter = CreateObjectEx("DataPresenter.CExecute")
30        If m_objDataPresenter Is Nothing Then
40            Err.Raise eActiveXCreate, "ReportUtils.clsReport::getReport()", "Error creating DataPresenter object."
50        End If
          
60        Set g_objEvents = m_objDataPresenter.mToolkit.mEvents
70        g_objEvents.ComponentInstance = "Report Utils"
80        m_objDataPresenter.Initialize m_sConfigPath
90        g_blnDebugMode = g_objEvents.IsDebugMode
          
100       m_sReportsDBConnectionString = GetConfig("ReportUtils/ConnectionStringStd")
              
errHandler:
          
110       If Err.Number <> 0 Then
120           g_objEvents.HandleEvent Err.Number, PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & vbCrLf & "ReportUtils.clsReport on class initialize"
130       End If
End Sub

Public Function getReport_new(strReportFileName As String, strStoredProc As String, strStaticParams As String, strReportParams As String, strExportFormat As String) As ADODB.Stream
10    On Error GoTo errHandler
      Const PROC_NAME As String = "ReportUtils.clsReport::getReport()"
          Dim objCRXApp As CRAXDDRT.Application
          Dim objReport As CRAXDDRT.Report
          
          Dim objFSO As Object
          Dim strOutputFileName As String
          Dim strReportParamsWork As String
          Dim strFormat As String
          Dim objExportOptions As CRAXDDRT.ExportOptions
          
          Dim objStream As ADODB.Stream
          
          
20        If g_blnDebugMode Then g_objEvents.Trace _
              "Report File Name (.rpt) = " & strReportFileName & vbCrLf & _
              "  Proc Name = " & strStoredProc & vbCrLf & _
              "  Static Parameters = " & strStaticParams & vbCrLf & _
              "  Report Params = " & strReportParams & vbCrLf & _
              "  Export Format = " & strExportFormat, PROC_NAME
          
          'Check the input parameters
30        If strReportFileName = "" Then
40            Err.Raise eInvalidReportParams, "", "The input report file specified cannot be empty in the parameters."
50        End If

60        If strStoredProc = "" Then
70            Err.Raise eInvalidReportParams, "", "The report stored proc specified cannot be empty in the parameters."
80        End If
          
90        If strExportFormat <> "DOC" And _
             strExportFormat <> "XLS" And _
             strExportFormat <> "RTF" Then
100          strFormat = "PDF"
110       Else
120           strFormat = strExportFormat
130       End If
          
          
140       If (strReportFileName <> "" And strStoredProc <> "") Then
              
150           Set objFSO = CreateObjectEx("Scripting.FileSystemObject")
160           If objFSO Is Nothing Then
170               Err.Raise eActiveXCreate, "", "Could not create FileSystemObject."
180           End If

              ' check if the report file exists
190           If Not objFSO.FileExists(strReportFileName) Then
200               Err.Raise eInvalidFile, "", "File not found. File Name: " & strReportFileName
210           End If
          
              'm_objDataPresenter.Initialize App.Path
              
              'Get the recordset based on the storedproc, static params and user selected params
220           If strStaticParams <> "" Then
230               strReportParamsWork = strStaticParams & "&"
240           Else
250               strReportParamsWork = ""
260           End If
              
270           strReportParamsWork = strReportParamsWork & strReportParams
              
280           If m_sReportsDBConnectionString <> "" Then
                  'Report utils has a connection string override. Use the override.
290               m_objDataPresenter.ConnectionStringParent = "ReportUtils"
300           End If
              
310           Set m_rs = m_objDataPresenter.ExecuteSpNpAsRS(strStoredProc, strReportParamsWork)
              
              'Create the crystal reports application object
320           Set objCRXApp = New CRAXDDRT.Application
              
              ' Open the report in the Crystal report RT
330           Set objReport = objCRXApp.OpenReport(strReportFileName)
              
              'Set the reports recordset to the one retrieved
340           objReport.Database.Tables(1).SetDataSource m_rs
              
              'Get a temporary file name to which we will export the report to. This will be .tmp extension
350           strOutputFileName = objFSO.GetSpecialFolder(2) & "\" & objFSO.GetTempName()
              
360           If g_blnDebugMode Then g_objEvents.Trace _
                  " Setting temporary output PDF File (on server) " & strOutputFileName & vbCrLf, PROC_NAME
              
              'set the reports export options
370           Set objExportOptions = objReport.ExportOptions
380           objExportOptions.DestinationType = crEDTDiskFile
390           objExportOptions.DiskFileName = strOutputFileName
              
              'set the export options
400           setExportFormat objExportOptions, strFormat
              
410           objReport.DisplayProgressDialog = False
              
              'Export the report now. Set the report progress dialogs off
420           objReport.Export (False)
              
              'release all references to crystal
430           Set objReport = Nothing
440           Set objCRXApp = Nothing
450           Set objExportOptions = Nothing
              
              'Create a binary stream to stream the pdf file to the client
460           Set objStream = CreateObjectEx("ADODB.Stream")
470           If objStream Is Nothing Then
480               Err.Raise eActiveXCreate, "", "Could not create ADODB.Stream"
490           End If
              
500           objStream.Charset = "UTF-8"
510           objStream.Type = 1 'Binary
520           objStream.Open
530           objStream.LoadFromFile strOutputFileName
              
540           If g_blnDebugMode Then g_objEvents.Trace _
                  " Loaded output PDF File into ADODB Stream object", PROC_NAME
              
              'now that the stream is loaded, delete the file in disk
550           objFSO.DeleteFile strOutputFileName
              
560           If g_blnDebugMode Then g_objEvents.Trace _
                  " Deleting the temporary File (on server) " & strOutputFileName & vbCrLf, PROC_NAME
          
570       End If

580       Set getReport_new = objStream
errHandler:

590       Set objReport = Nothing
600       Set objCRXApp = Nothing
610       Set objExportOptions = Nothing
620       Set objFSO = Nothing
630       Set objStream = Nothing
          
640       If Err.Number <> 0 Then
650           strMsg = "    ReportFileName = " + strReportFileName + vbCrLf + _
                       "    strStoredProc = " + strStoredProc + vbCrLf + _
                       "    strReportParams = " + strReportParams + vbCrLf + _
                       "    Line #: " + CStr(Erl)
                       

660           g_objEvents.HandleEvent Err.Number, PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & vbCrLf & strMsg
670       End If
End Function

Private Sub Class_Terminate()
    Set g_objEvents = Nothing
    Set m_objDataPresenter = Nothing
    Set m_rs = Nothing
End Sub

Private Sub setExportFormat(ByRef objExportOptions As CRAXDDRT.ExportOptions, ByRef strFormat As String)
10        On Error GoTo errHandler

20        If g_blnDebugMode Then g_objEvents.Trace _
              " Setting export options for " & strFormat & " format." & vbCrLf, PROC_NAME
30        If Not (objExportOptions Is Nothing) Then
40            Select Case strFormat
                  Case "DOC":
50                    objExportOptions.FormatType = crEFTWordForWindows
60                Case "XLS":
70                    objExportOptions.FormatType = crEFTExcel80Tabular
80                    objExportOptions.ExcelUseConstantColumnWidth = False
90                    objExportOptions.ExcelTabHasColumnHeadings = True
100                   objExportOptions.ExcelUseTabularFormat = False
110                   objExportOptions.ExcelUseWorksheetFunctions = False
120               Case "RTF":
130                   objExportOptions.FormatType = crEFTExactRichText
140                   objExportOptions.RTFExportAllPages = True
150               Case Else 'default is PDF format
160                   objExportOptions.FormatType = crEFTPortableDocFormat 'PDF format
170                   objExportOptions.PDFExportAllPages = True
180           End Select
190       End If
200       Exit Sub
errHandler:
210       If Err.Number <> 0 Then
220           strMsg = "    strFormat = " + strFormat + vbCrLf + _
                       "    Line #: " + CStr(Erl)
                       

230           g_objEvents.HandleEvent Err.Number, PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & vbCrLf & strMsg
240       End If
End Sub


Public Function getReport(strReportFileName As String, strStoredProc As String, strStaticParams As String, strReportParams As String, Optional strExportFormat As String) As ADODB.Stream
    If IsMissing(strExportFormat) Then
        strExportFormat = "PDF"
    End If
    
    Set getReport = getReport_new(strReportFileName, strStoredProc, strStaticParams, strReportParams, strExportFormat)

End Function

Public Function getWAReport(objRSData As ADODB.Recordset, strReportFileName As String, Optional strExportFormat As String) As ADODB.Stream
10    On Error GoTo errHandler
      Const PROC_NAME As String = "ReportUtils.clsReport::getWAReport()"
          Dim objCRXApp As CRAXDDRT.Application
          Dim objReport As CRAXDDRT.Report
          
          Dim objFSO As Object
          Dim strOutputFileName As String
          Dim strFormat As String
          Dim objExportOptions As CRAXDDRT.ExportOptions
          
          Dim objStream As ADODB.Stream
          
          
20        If g_blnDebugMode Then g_objEvents.Trace _
              "Report File Name (.rpt) = " & strReportFileName & vbCrLf & _
              "  Export Format = " & strExportFormat, PROC_NAME
          
          'Check the input parameters
30        If strReportFileName = "" Then
40            Err.Raise eInvalidReportParams, "", "The input report file specified cannot be empty in the parameters."
50        End If

60        If IsMissing(strExportFormat) Then
70            strExportFormat = "PDF"
80        End If
          
90        If strExportFormat <> "DOC" And _
             strExportFormat <> "XLS" And _
             strExportFormat <> "RTF" Then
100          strFormat = "PDF"
110       Else
120           strFormat = strExportFormat
130       End If
          
          
140       If (strReportFileName <> "") Then
              
150           Set objFSO = CreateObjectEx("Scripting.FileSystemObject")
160           If objFSO Is Nothing Then
170               Err.Raise eActiveXCreate, "", "Could not create FileSystemObject."
180           End If

              ' check if the report file exists
190           If Not objFSO.FileExists(strReportFileName) Then
200               Err.Raise eInvalidFile, "", "File not found. File Name: " & strReportFileName
210           End If
          
              'Create the Crystal report application
220           Set objCRXApp = New CRAXDDRT.Application
              
              ' Open the report in the Crystal report RT
230           Set objReport = objCRXApp.OpenReport(strReportFileName)
          
              'm_objDataPresenter.Initialize App.Path
              
              'Set the reports recordset to the one retrieved
240           objReport.Database.Tables(1).SetDataSource objRSData
              
              'Get a temporary file name to which we will export the report to. This will be .tmp extension
250           strOutputFileName = objFSO.GetSpecialFolder(2) & "\" & objFSO.GetTempName()
              
260           If g_blnDebugMode Then g_objEvents.Trace _
                  " Setting temporary output PDF File (on server) " & strOutputFileName & vbCrLf, PROC_NAME
              
              'set the reports export options
270           Set objExportOptions = objReport.ExportOptions
280           objExportOptions.DestinationType = crEDTDiskFile
290           objExportOptions.DiskFileName = strOutputFileName
              
              'set the export options
300           setExportFormat objExportOptions, strFormat
              
310           objReport.DisplayProgressDialog = False
              
              'Export the report now. Set the report progress dialogs off
320           objReport.Export (False)
              
              'release all references to Crystal
330           Set objReport = Nothing
340           Set objCRXApp = Nothing
350           Set objExportOptions = Nothing
              
              'Create a binary stream to stream the pdf file to the client
360           Set objStream = CreateObjectEx("ADODB.Stream")
370           If objStream Is Nothing Then
380               Err.Raise eActiveXCreate, "", "Could not create ADODB.Stream"
390           End If
              
400           objStream.Charset = "UTF-8"
410           objStream.Type = 1 'Binary
420           objStream.Open
430           objStream.LoadFromFile strOutputFileName
              
440           If g_blnDebugMode Then g_objEvents.Trace _
                  " Loaded output PDF File into ADODB Stream object", PROC_NAME
              
              'now that the stream is loaded, delete the file in disk
450           objFSO.DeleteFile strOutputFileName
              
460           If g_blnDebugMode Then g_objEvents.Trace _
                  " Deleting the temporary File (on server) " & strOutputFileName & vbCrLf, PROC_NAME
          
470       End If

480       Set getWAReport = objStream
errHandler:

490       Set objReport = Nothing
500       Set objCRXApp = Nothing
510       Set objExportOptions = Nothing
520       Set objFSO = Nothing
530       Set objStream = Nothing
          
540       If Err.Number <> 0 Then
550           strMsg = "    ReportFileName = " + strReportFileName + vbCrLf + _
                       "    Line #: " + CStr(Erl)
                       

560           g_objEvents.HandleEvent Err.Number, PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & vbCrLf & strMsg
570       End If
End Function


