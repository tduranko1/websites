Attribute VB_Name = "modJobs"
Option Explicit
Option Compare Text

Public Const HIGH_PRIORITY As String = "1_"
Public Const NORMAL_PRIORITY As String = "2_"
Public Const LOW_PRIORITY As String = "3_"
Public g_strQueuePath As String
Public g_strPickupPath As String
Public g_strJobQueue As String

Public Function getJobTitle(strJobID As String) As String
          Dim strRet As String
          Dim oJobsXML As MSXML2.DOMDocument
          Dim oJob As IXMLDOMElement
          
10        strRet = ""
          
20        If jobInQueue(strJobID) Then
30            strRet = strJobID
40        End If
          
50        If gobjFSO.FileExists(g_strJobQueue) Then
60            Set oJobsXML = loadXMLFile(g_strJobQueue)
70            If Not oJobsXML Is Nothing Then
80                Set oJob = oJobsXML.selectSingleNode("/Jobs/Job[@id='" & strJobID & "']")
90                If Not oJob Is Nothing Then
100                   strRet = oJob.getAttribute("description")
110               End If
120           End If
130       End If
          
140       getJobTitle = strRet
          
150       Set oJob = Nothing
160       Set oJobsXML = Nothing
End Function

Public Function getJobStatus(strJobID As String) As String
          Dim strRet As String
          Dim oJobsXML As MSXML2.DOMDocument
          Dim oJob As IXMLDOMElement
          
10        strRet = ""
          
20        If jobInQueue(strJobID) Then
30            strRet = "Awaiting Pickup"
40        End If
          
50        If gobjFSO.FileExists(g_strJobQueue) Then
60            Set oJobsXML = loadXMLFile(g_strJobQueue)
70            If Not oJobsXML Is Nothing Then
80                Set oJob = oJobsXML.selectSingleNode("/Jobs/Job[@id='" & strJobID & "']")
90                If Not oJob Is Nothing Then
100                   strRet = oJob.getAttribute("status")
110               End If
120           End If
130       End If
          
140       getJobStatus = strRet
          
150       Set oJob = Nothing
160       Set oJobsXML = Nothing
End Function

Public Function getJobCount() As Integer
          Dim oJobsXML As MSXML2.DOMDocument
          Dim oJobs As IXMLDOMNodeList
          
          Dim iJobCount As Integer
          
10        If gobjFSO.FileExists(g_strJobQueue) Then
20            Set oJobsXML = loadXMLFile(g_strJobQueue)
30            If Not oJobsXML Is Nothing Then
40                Set oJobs = oJobsXML.selectNodes("/Jobs/Job")
50                If Not oJobs Is Nothing Then
60                    iJobCount = oJobs.Length
70                End If
80            End If
90        End If
          
100       getJobCount = iJobCount
          
110       Set oJobs = Nothing
120       Set oJobsXML = Nothing
End Function

Public Function deleteJob(strJobID As String) As Boolean
          Dim bRet As Boolean
          Dim strJobFileName As String
          Dim oJobsXML As MSXML2.DOMDocument
          Dim oJob As IXMLDOMElement
          
10        bRet = False
20        strJobFileName = gobjFSO.BuildPath(g_strQueuePath, "*" & strJobID & "*")
          
30        If jobInQueue(strJobID) Then
40            If gobjFSO.FileExists(g_strJobQueue) Then
50                Set oJobsXML = loadXMLFile(g_strJobQueue)
60                If Not oJobsXML Is Nothing Then
70                    Set oJob = oJobsXML.selectSingleNode("/Jobs/Job[@id='" & strJobID & "']")
80                    If Not oJob Is Nothing Then
90                        If oJob.getAttribute("status") = "Waiting" Then
100                           oJobsXML.removeChild oJob
110                           oJobsXML.Save g_strJobQueue
                              
120                           Kill strJobFileName
                              
130                           If Dir(gobjFSO.BuildPath(g_strPickupPath, strJobID & "*.*")) <> "" Then
140                               Kill gobjFSO.BuildPath(g_strPickupPath, strJobID & "*.*")
150                           End If
                              
160                           Trace Format(Now, "mm/dd/yyyy hh:nn:ss -- ") & strJobID & " was deleted by " & g_strUserName & vbCrLf
                              
170                           bRet = True
180                       End If
190                   Else
                          'job is awaiting pickup. Quickly delete the job
200                       Kill strJobFileName
                          
210                       If Dir(gobjFSO.BuildPath(g_strPickupPath, strJobID & "*.*")) <> "" Then
220                           Kill gobjFSO.BuildPath(g_strPickupPath, strJobID & "*.*")
230                       End If
                          
240                       Trace Format(Now, "mm/dd/yyyy hh:nn:ss -- ") & strJobID & " was deleted by " & g_strUserName & vbCrLf
                          
250                       bRet = True
260                   End If
270               End If
280           Else
290               Kill strJobFileName
                  
300               bRet = True
310           End If
320       End If
          
330       deleteJob = bRet
          
340       Set oJob = Nothing
350       Set oJobsXML = Nothing
End Function

Public Function jobInQueue(strJobID As String) As Boolean
          Dim strJobHighFileName As String
          Dim strJobNormalFileName As String
          Dim strJobLowFileName As String
          
10        strJobHighFileName = gobjFSO.BuildPath(g_strQueuePath, HIGH_PRIORITY & strJobID & ".xml")
20        strJobNormalFileName = gobjFSO.BuildPath(g_strQueuePath, NORMAL_PRIORITY & strJobID & ".xml")
30        strJobLowFileName = gobjFSO.BuildPath(g_strQueuePath, LOW_PRIORITY & strJobID & ".xml")
          
40        jobInQueue = gobjFSO.FileExists(strJobHighFileName) Or _
                        gobjFSO.FileExists(strJobNormalFileName) Or _
                        gobjFSO.FileExists(strJobLowFileName)
End Function

Public Function setJobPriority(strJobID As String, strPriority As String) As Boolean
          Dim bRet As Boolean
          Dim strJobFileName As String
          Dim strJobNewFileName As String
          Dim oJobsXML As MSXML2.DOMDocument
          Dim oJob As IXMLDOMElement
          
10        bRet = False
          
20        If jobInQueue(strJobID) Then
30            If gobjFSO.FileExists(g_strJobQueue) Then
40                Set oJobsXML = loadXMLFile(g_strJobQueue)
50                If Not oJobsXML Is Nothing Then
60                    Set oJob = oJobsXML.selectSingleNode("/Jobs/Job[@id='" & strJobID & "']")
70                    If Not oJob Is Nothing Then
80                        If oJob.getAttribute("status") = "Waiting" Then
90                            oJob.setAttribute "priority", strPriority
100                           oJobsXML.Save g_strJobQueue
                              'bRet = True
110                       End If
120                   End If
130               End If
140           End If
              
              'now rename the file to match the status
150           strJobFileName = gobjFSO.BuildPath(g_strQueuePath, "*" & strJobID & "*")
              
160           strJobFileName = Dir(strJobFileName)
170           If strJobFileName <> "" Then
180               strJobFileName = gobjFSO.BuildPath(g_strQueuePath, strJobFileName)
190               strJobNewFileName = generateJobFileName(strJobID, strPriority)
200               gobjFSO.MoveFile strJobFileName, strJobNewFileName
210               Trace Format(Now, "mm/dd/yyyy hh:nn:ss -- ") & "Priority was changed for job id " & strJobID & " to " & strPriority & " by " & g_strUserName & vbCrLf
220               bRet = True
230           End If
240       End If
          
250       setJobPriority = bRet
          
260       Set oJob = Nothing
270       Set oJobsXML = Nothing
End Function
    
Public Function generateJobFileName(strJobID As String, strPriority) As String
          Dim strFileName As String
          Dim iDupCount As Integer
          Dim strPrefix As String
10        Select Case strPriority
              Case "high"
20                strPrefix = HIGH_PRIORITY
30            Case "low"
40                strPrefix = LOW_PRIORITY
50            Case Else 'default normal priority
60                strPrefix = NORMAL_PRIORITY
70        End Select
          
80        strFileName = gobjFSO.BuildPath(g_strQueuePath, strPrefix & strJobID & ".xml")
          
          'theoritically you should not have a duplicate since the file name is a GUID. Just in case.
90        iDupCount = 0
100       While gobjFSO.FileExists(strFileName)
110           iDupCount = iDupCount + 1
120           strFileName = gobjFSO.BuildPath(g_strQueuePath, strPrefix & strJobID & CStr(iDupCount) & ".xml")
130       Wend
          
140       generateJobFileName = strFileName

End Function

