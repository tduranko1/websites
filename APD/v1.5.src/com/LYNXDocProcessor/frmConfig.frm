VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmConfig 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Settings"
   ClientHeight    =   8565
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11040
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   571
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   736
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame9 
      Caption         =   " Image Max. Resolution: (px) "
      Height          =   675
      Left            =   7380
      TabIndex        =   99
      Top             =   60
      Width           =   3555
      Begin VB.TextBox txtImageMaxWidth 
         Height          =   315
         Left            =   2580
         TabIndex        =   102
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox txtImageMaxHeight 
         Height          =   315
         Left            =   780
         TabIndex        =   100
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label37 
         Caption         =   "Width:"
         Height          =   195
         Left            =   1980
         TabIndex        =   103
         Top             =   300
         Width           =   555
      End
      Begin VB.Label Label36 
         Caption         =   "Height:"
         Height          =   195
         Left            =   180
         TabIndex        =   101
         Top             =   300
         Width           =   555
      End
   End
   Begin VB.Frame Frame8 
      Caption         =   "HTTP Server "
      Height          =   675
      Left            =   7380
      TabIndex        =   96
      Top             =   840
      Width           =   3555
      Begin VB.TextBox txtHTTPLimit 
         Height          =   315
         Left            =   1560
         TabIndex        =   97
         Top             =   240
         Width           =   1875
      End
      Begin VB.Label Label39 
         Caption         =   "Upload Limit (bits):"
         Height          =   195
         Left            =   180
         TabIndex        =   98
         Top             =   300
         Width           =   1335
      End
   End
   Begin VB.Frame frmOverrides 
      Caption         =   " Overrides "
      Height          =   1395
      Left            =   120
      TabIndex        =   81
      Top             =   7080
      Width           =   7095
      Begin VB.CommandButton cmdBrowseFileCopy 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   6600
         MaskColor       =   &H80000003&
         Picture         =   "frmConfig.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   960
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.TextBox txtFileCopyOverride 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   315
         Left            =   900
         TabIndex        =   36
         TabStop         =   0   'False
         Top             =   960
         Width           =   5715
      End
      Begin VB.TextBox txtHTTPOverride 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   315
         Left            =   4440
         TabIndex        =   35
         Top             =   600
         Width           =   2535
      End
      Begin VB.TextBox txtFaxNumberOverride 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   315
         Left            =   900
         TabIndex        =   32
         Top             =   240
         Width           =   2535
      End
      Begin VB.TextBox txtEmailToOverride 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   315
         Left            =   4440
         TabIndex        =   33
         Top             =   240
         Width           =   2535
      End
      Begin VB.TextBox txtFTPOverride 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   315
         Left            =   900
         TabIndex        =   34
         Top             =   600
         Width           =   2535
      End
      Begin VB.Label Label31 
         Caption         =   "File Copy:"
         Height          =   195
         Left            =   180
         TabIndex        =   86
         Top             =   1020
         Width           =   735
      End
      Begin VB.Label Label30 
         Caption         =   "HTTP:"
         Height          =   195
         Left            =   3720
         TabIndex        =   85
         Top             =   660
         Width           =   615
      End
      Begin VB.Label Label25 
         Caption         =   "Fax #:"
         Height          =   195
         Left            =   180
         TabIndex        =   84
         Top             =   300
         Width           =   495
      End
      Begin VB.Label Label29 
         Caption         =   "Email To:"
         Height          =   195
         Left            =   3720
         TabIndex        =   83
         Top             =   300
         Width           =   675
      End
      Begin VB.Label Label5 
         Caption         =   "FTP:"
         Height          =   195
         Left            =   180
         TabIndex        =   82
         Top             =   660
         Width           =   615
      End
   End
   Begin VB.TextBox txtPDFPrinter 
      Height          =   315
      Left            =   8520
      TabIndex        =   23
      Top             =   3480
      Width           =   2415
   End
   Begin VB.CheckBox chkAppLog 
      Caption         =   "Application Log"
      Height          =   195
      Left            =   7620
      TabIndex        =   29
      Top             =   6600
      Width           =   1455
   End
   Begin VB.Frame Frame7 
      Height          =   675
      Left            =   7380
      TabIndex        =   79
      Top             =   6600
      Width           =   3555
      Begin VB.CommandButton cmdAppLogPath 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   3120
         MaskColor       =   &H80000003&
         Picture         =   "frmConfig.frx":0380
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.TextBox txtAppLogPath 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   240
         Width           =   2235
      End
      Begin VB.Label Label28 
         Caption         =   "Path:"
         Height          =   195
         Left            =   180
         TabIndex        =   80
         Top             =   300
         Width           =   615
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   " Error Notification "
      Height          =   1095
      Left            =   7380
      TabIndex        =   76
      Top             =   5400
      Width           =   3555
      Begin VB.TextBox txtErrorSubject 
         Height          =   315
         Left            =   900
         TabIndex        =   28
         Top             =   600
         Width           =   2535
      End
      Begin VB.TextBox txtErrorTo 
         Height          =   315
         Left            =   900
         TabIndex        =   27
         Top             =   240
         Width           =   2535
      End
      Begin VB.Label Label27 
         Caption         =   "Subject:"
         Height          =   195
         Left            =   180
         TabIndex        =   78
         Top             =   660
         Width           =   615
      End
      Begin VB.Label Label26 
         Caption         =   "To:"
         Height          =   195
         Left            =   180
         TabIndex        =   77
         Top             =   300
         Width           =   615
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   " Fax "
      Height          =   1035
      Left            =   120
      TabIndex        =   72
      Top             =   2640
      Width           =   7095
      Begin VB.ComboBox cmbPrinter 
         ForeColor       =   &H000000FF&
         Height          =   315
         Left            =   900
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   240
         Width           =   2595
      End
      Begin VB.TextBox txtFaxServers 
         Height          =   315
         Left            =   900
         TabIndex        =   8
         ToolTipText     =   "Comma separated list of Fax Servers "
         Top             =   600
         Width           =   6075
      End
      Begin VB.TextBox txtFaxPrinter 
         Height          =   315
         Left            =   900
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   240
         Width           =   2595
      End
      Begin VB.TextBox txtFaxUser 
         Height          =   315
         Left            =   4440
         TabIndex        =   7
         Top             =   240
         Width           =   2535
      End
      Begin VB.Label Label24 
         Caption         =   "Servers:"
         Height          =   195
         Left            =   180
         TabIndex        =   75
         Top             =   660
         Width           =   615
      End
      Begin VB.Label Label23 
         Caption         =   "Printer:"
         Height          =   195
         Left            =   180
         TabIndex        =   74
         Top             =   300
         Width           =   615
      End
      Begin VB.Label Label22 
         Caption         =   "User:"
         Height          =   195
         Left            =   3720
         TabIndex        =   73
         Top             =   300
         Width           =   735
      End
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   495
      Left            =   9660
      TabIndex        =   39
      Top             =   7920
      Width           =   1215
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Height          =   495
      Left            =   8280
      TabIndex        =   38
      Top             =   7920
      Width           =   1215
   End
   Begin VB.Frame Frame4 
      Caption         =   " Tools "
      Height          =   3195
      Left            =   120
      TabIndex        =   55
      Top             =   3780
      Width           =   7095
      Begin VB.TextBox txtFTPClient 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   57
         TabStop         =   0   'False
         Top             =   1620
         Width           =   5235
      End
      Begin VB.CommandButton cmdToolsPath 
         Appearance      =   0  'Flat
         Height          =   315
         Index           =   4
         Left            =   6540
         MaskColor       =   &H80000003&
         Picture         =   "frmConfig.frx":0700
         Style           =   1  'Graphical
         TabIndex        =   104
         Top             =   1620
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.CheckBox chkDirectExecute 
         Caption         =   "Execute Directly (no batch file)"
         Height          =   255
         Left            =   180
         TabIndex        =   95
         Top             =   240
         Width           =   3075
      End
      Begin VB.TextBox txtHTMLRight 
         Height          =   315
         Left            =   6180
         TabIndex        =   18
         Top             =   2700
         Width           =   735
      End
      Begin VB.TextBox txtHTMLBottom 
         Height          =   315
         Left            =   4560
         TabIndex        =   17
         Top             =   2700
         Width           =   735
      End
      Begin VB.TextBox txtHTMLLeft 
         Height          =   315
         Left            =   2760
         TabIndex        =   16
         Top             =   2700
         Width           =   735
      End
      Begin VB.TextBox txtHTMLTop 
         Height          =   315
         Left            =   1320
         TabIndex        =   15
         Top             =   2700
         Width           =   735
      End
      Begin VB.TextBox txtHTMLFooter 
         Height          =   315
         Left            =   4560
         TabIndex        =   14
         Top             =   2340
         Width           =   2355
      End
      Begin VB.TextBox txtHTMLHeader 
         Height          =   315
         Left            =   1320
         TabIndex        =   13
         Top             =   2340
         Width           =   2175
      End
      Begin VB.TextBox txtHTMLDoc 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   63
         TabStop         =   0   'False
         Top             =   1980
         Width           =   5235
      End
      Begin VB.CommandButton cmdToolsPath 
         Appearance      =   0  'Flat
         Height          =   315
         Index           =   3
         Left            =   6540
         MaskColor       =   &H80000003&
         Picture         =   "frmConfig.frx":0A80
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   1980
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.TextBox txtGSPath 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   59
         TabStop         =   0   'False
         Top             =   540
         Width           =   5235
      End
      Begin VB.CommandButton cmdToolsPath 
         Appearance      =   0  'Flat
         Height          =   315
         Index           =   0
         Left            =   6540
         MaskColor       =   &H80000003&
         Picture         =   "frmConfig.frx":0E00
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   540
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.CommandButton cmdToolsPath 
         Appearance      =   0  'Flat
         Height          =   315
         Index           =   1
         Left            =   6540
         MaskColor       =   &H80000003&
         Picture         =   "frmConfig.frx":1180
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   900
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.TextBox txtPDFTKPath 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   58
         TabStop         =   0   'False
         Top             =   900
         Width           =   5235
      End
      Begin VB.CommandButton cmdToolsPath 
         Appearance      =   0  'Flat
         Height          =   315
         Index           =   2
         Left            =   6540
         MaskColor       =   &H80000003&
         Picture         =   "frmConfig.frx":1500
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1260
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.TextBox txtImageMagickPath 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   56
         TabStop         =   0   'False
         Top             =   1260
         Width           =   5235
      End
      Begin VB.Label Label38 
         Caption         =   "FTP Client:"
         Height          =   195
         Left            =   180
         TabIndex        =   105
         Top             =   1680
         Width           =   1035
      End
      Begin VB.Label Label20 
         Caption         =   "Right:"
         Height          =   195
         Left            =   5640
         TabIndex        =   70
         Top             =   2760
         Width           =   435
      End
      Begin VB.Label Label19 
         Caption         =   "Bottom:"
         Height          =   195
         Left            =   3900
         TabIndex        =   69
         Top             =   2760
         Width           =   555
      End
      Begin VB.Label Label18 
         Caption         =   "Left:"
         Height          =   195
         Left            =   2340
         TabIndex        =   68
         Top             =   2760
         Width           =   315
      End
      Begin VB.Label Label17 
         Caption         =   "Margin     Top:"
         Height          =   195
         Left            =   180
         TabIndex        =   67
         Top             =   2760
         Width           =   1035
      End
      Begin VB.Label Label16 
         Caption         =   "Footer:"
         Height          =   195
         Left            =   4020
         TabIndex        =   66
         Top             =   2400
         Width           =   615
      End
      Begin VB.Label Label15 
         Caption         =   "Header:"
         Height          =   195
         Left            =   600
         TabIndex        =   65
         Top             =   2400
         Width           =   615
      End
      Begin VB.Label Label11 
         Caption         =   "HTMLDoc:"
         Height          =   195
         Left            =   180
         TabIndex        =   64
         Top             =   2040
         Width           =   1035
      End
      Begin VB.Label Label14 
         Caption         =   "Ghostscript:"
         Height          =   195
         Left            =   180
         TabIndex        =   62
         Top             =   600
         Width           =   975
      End
      Begin VB.Label Label13 
         Caption         =   "PDF Toolkit:"
         Height          =   195
         Left            =   180
         TabIndex        =   61
         Top             =   960
         Width           =   1035
      End
      Begin VB.Label Label12 
         Caption         =   "ImageMagick:"
         Height          =   195
         Left            =   180
         TabIndex        =   60
         Top             =   1320
         Width           =   1035
      End
   End
   Begin MSComDlg.CommonDialog dlg 
      Left            =   7380
      Top             =   7260
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame3 
      Caption         =   " PDF Metadata "
      Height          =   1395
      Left            =   7380
      TabIndex        =   51
      Top             =   3900
      Width           =   3555
      Begin VB.TextBox txtPDFProducer 
         Height          =   315
         Left            =   900
         TabIndex        =   26
         Top             =   960
         Width           =   2535
      End
      Begin VB.TextBox txtPDFAuthor 
         Height          =   315
         Left            =   900
         TabIndex        =   24
         Top             =   240
         Width           =   2535
      End
      Begin VB.TextBox txtPDFCreator 
         Height          =   315
         Left            =   900
         TabIndex        =   25
         Top             =   600
         Width           =   2535
      End
      Begin VB.Label Label10 
         Caption         =   "Producer:"
         Height          =   195
         Left            =   180
         TabIndex        =   54
         Top             =   1020
         Width           =   735
      End
      Begin VB.Label Label9 
         Caption         =   "Author:"
         Height          =   195
         Left            =   180
         TabIndex        =   53
         Top             =   300
         Width           =   615
      End
      Begin VB.Label Label8 
         Caption         =   "Creator:"
         Height          =   195
         Left            =   180
         TabIndex        =   52
         Top             =   660
         Width           =   615
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   " SMTP Server "
      Height          =   1755
      Left            =   7380
      TabIndex        =   48
      Top             =   1620
      Width           =   3555
      Begin VB.TextBox txtSMTPTimeout 
         Height          =   315
         Left            =   1860
         TabIndex        =   22
         Top             =   1320
         Width           =   1575
      End
      Begin VB.TextBox txtEmailFrom 
         Height          =   315
         Left            =   900
         TabIndex        =   21
         Top             =   960
         Width           =   2535
      End
      Begin VB.TextBox txtSMTPSizeLimit 
         Height          =   315
         Left            =   1860
         TabIndex        =   20
         Top             =   600
         Width           =   1575
      End
      Begin VB.TextBox txtSMTPServer 
         Height          =   315
         Left            =   900
         TabIndex        =   19
         Top             =   240
         Width           =   2535
      End
      Begin VB.Label Label33 
         Caption         =   "Timeout (seconds):"
         Height          =   195
         Left            =   180
         TabIndex        =   88
         Top             =   1380
         Width           =   1635
      End
      Begin VB.Label Label32 
         Caption         =   "From:"
         Height          =   195
         Left            =   180
         TabIndex        =   87
         Top             =   1020
         Width           =   615
      End
      Begin VB.Label Label7 
         Caption         =   "Max Email Size (bytes):"
         Height          =   195
         Left            =   180
         TabIndex        =   50
         Top             =   660
         Width           =   1695
      End
      Begin VB.Label Label6 
         Caption         =   "Server:"
         Height          =   195
         Left            =   180
         TabIndex        =   49
         Top             =   300
         Width           =   615
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   " Paths "
      Height          =   2475
      Left            =   120
      TabIndex        =   0
      Top             =   60
      Width           =   7095
      Begin VB.TextBox txtArchiveTTL 
         Height          =   315
         Left            =   1620
         TabIndex        =   92
         TabStop         =   0   'False
         Top             =   2040
         Width           =   795
      End
      Begin VB.CheckBox chkArchiveJobs 
         Caption         =   "Archive Jobs"
         Height          =   195
         Left            =   180
         TabIndex        =   91
         Top             =   1740
         Width           =   1335
      End
      Begin VB.CommandButton cmdBrowse 
         Appearance      =   0  'Flat
         Height          =   315
         Index           =   4
         Left            =   6540
         MaskColor       =   &H80000003&
         Picture         =   "frmConfig.frx":1880
         Style           =   1  'Graphical
         TabIndex        =   90
         Top             =   1680
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.TextBox txtArchivePath 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1620
         Locked          =   -1  'True
         TabIndex        =   89
         TabStop         =   0   'False
         Top             =   1680
         Width           =   4935
      End
      Begin VB.TextBox txtBadJob 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1020
         Locked          =   -1  'True
         TabIndex        =   43
         TabStop         =   0   'False
         Top             =   1320
         Width           =   5535
      End
      Begin VB.CommandButton cmdBrowse 
         Appearance      =   0  'Flat
         Height          =   315
         Index           =   3
         Left            =   6540
         MaskColor       =   &H80000003&
         Picture         =   "frmConfig.frx":1C00
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   1320
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.TextBox txtOutbox 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1020
         Locked          =   -1  'True
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   960
         Width           =   5535
      End
      Begin VB.CommandButton cmdBrowse 
         Appearance      =   0  'Flat
         Height          =   315
         Index           =   2
         Left            =   6540
         MaskColor       =   &H80000003&
         Picture         =   "frmConfig.frx":1F80
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   960
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.TextBox txtQueue 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1020
         Locked          =   -1  'True
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   600
         Width           =   5535
      End
      Begin VB.CommandButton cmdBrowse 
         Appearance      =   0  'Flat
         Height          =   315
         Index           =   1
         Left            =   6540
         MaskColor       =   &H80000003&
         Picture         =   "frmConfig.frx":2300
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   600
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.CommandButton cmdBrowse 
         Appearance      =   0  'Flat
         Height          =   315
         Index           =   0
         Left            =   6540
         MaskColor       =   &H80000003&
         Picture         =   "frmConfig.frx":2680
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.TextBox txtPickup 
         BackColor       =   &H00E0E0E0&
         Height          =   315
         Left            =   1020
         Locked          =   -1  'True
         TabIndex        =   40
         TabStop         =   0   'False
         Top             =   240
         Width           =   5535
      End
      Begin VB.Label Label35 
         Caption         =   "days"
         Height          =   195
         Left            =   2520
         TabIndex        =   94
         Top             =   2100
         Width           =   435
      End
      Begin VB.Label Label34 
         Caption         =   "Keep for:"
         Height          =   195
         Left            =   780
         TabIndex        =   93
         Top             =   2100
         Width           =   675
      End
      Begin VB.Label Label4 
         Caption         =   "Bad Job:"
         Height          =   195
         Left            =   180
         TabIndex        =   47
         Top             =   1380
         Width           =   675
      End
      Begin VB.Label Label3 
         Caption         =   "Outbox"
         Height          =   195
         Left            =   180
         TabIndex        =   46
         Top             =   1020
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "Queue:"
         Height          =   195
         Left            =   180
         TabIndex        =   45
         Top             =   660
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Pickup:"
         Height          =   195
         Left            =   180
         TabIndex        =   44
         Top             =   300
         Width           =   615
      End
   End
   Begin VB.Label Label21 
      Caption         =   "PDF Printer:"
      Height          =   195
      Left            =   7560
      TabIndex        =   71
      Top             =   3540
      Width           =   915
   End
End
Attribute VB_Name = "frmConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_oXMLConfig As MSXML2.DOMDocument
Dim m_objFSO As New Scripting.FileSystemObject

Private Sub chkArchiveJobs_Click()
    Dim blnEnabled As Boolean
    blnEnabled = (chkArchiveJobs.Value = vbChecked)
    cmdBrowse(4).Enabled = blnEnabled
    txtArchiveTTL.Enabled = blnEnabled
End Sub

Private Sub cmbPrinter_Click()
    txtFaxPrinter.Text = cmbPrinter.Text
    cmbPrinter.ForeColor = vbBlack
End Sub

Private Sub cmdAppLogPath_Click()
    Dim strPath As String
    Dim strTitle As String

    strPath = txtAppLogPath.Text
    strTitle = "Browse for Application Log folder"
    
    strPath = resolveMacro(strPath)
    
    strPath = BrowseForFolder(Me, strTitle, strPath)
    
    If Len(strPath) <> 0 Then
        txtAppLogPath.Text = strPath
        If chkAppLog.Value = vbUnchecked Then
            If MsgBox("Application log is turned OFF. Do you want to turn it ON?", vbQuestion + vbYesNo, APP_TITLE) = vbYes Then
                chkAppLog.Value = vbChecked
            End If
        End If
    End If
End Sub

Private Sub cmdBrowse_Click(Index As Integer)
    Dim strPath As String
    Dim strTitle As String

    Select Case Index
        Case 0:
            strPath = txtPickup.Text
            strTitle = "Browse for Pickup folder"
        Case 1:
            strPath = txtQueue.Text
            strTitle = "Browse for Queue folder"
        Case 2:
            strPath = txtOutbox.Text
            strTitle = "Browse for Outbox folder"
        Case 3:
            strPath = txtBadJob.Text
            strTitle = "Browse for Bad Jobs folder"
    End Select
    
    strPath = resolveMacro(strPath)
    
    strPath = BrowseForFolder(Me, strTitle, strPath)
    
    If Len(strPath) <> 0 Then
        Select Case Index
            Case 0:
                txtPickup.Text = strPath
                txtPickup.ForeColor = vbBlack
            Case 1:
                txtQueue.Text = strPath
                txtQueue.ForeColor = vbBlack
            Case 2:
                txtOutbox.Text = strPath
                txtOutbox.ForeColor = vbBlack
            Case 3:
                txtBadJob.Text = strPath
                txtBadJob.ForeColor = vbBlack
        End Select
    End If
End Sub

Private Sub cmdBrowseFileCopy_Click()
    Dim strPath As String
    Dim strTitle As String

    strPath = txtAppLogPath.Text
    
    strTitle = "Browse for File Copy Override folder"
    
    strPath = resolveMacro(strPath)
    
    strPath = BrowseForFolder(Me, strTitle, strPath)
    
    If Len(strPath) <> 0 Then
        txtFileCopyOverride.Text = strPath
    End If
End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdSave_Click()
    'validations
    If txtSMTPSizeLimit.Text <> "" And IsNumeric(txtSMTPSizeLimit.Text) = False Then
        MsgBox "Max Email Size value must be numeric", vbCritical, APP_TITLE
        Exit Sub
    End If
    
    If txtSMTPTimeout.Text <> "" And IsNumeric(txtSMTPTimeout.Text) = False Then
        MsgBox "SMTP Timeout value must be numeric", vbCritical, APP_TITLE
        Exit Sub
    End If
    
    If txtArchiveTTL.Text <> "" And IsNumeric(txtArchiveTTL.Text) = False Then
        MsgBox "Archive Keep for value must be numeric", vbCritical, APP_TITLE
        Exit Sub
    End If
    
    If txtHTTPLimit.Text <> "" And IsNumeric(txtHTTPLimit.Text) = False Then
        MsgBox "HTTP Server Upload limit value must be numeric", vbCritical, APP_TITLE
        Exit Sub
    End If
    
    If txtImageMaxHeight.Text <> "" And IsNumeric(txtImageMaxHeight.Text) = False Then
        MsgBox "Image Maximum Height value must be numeric", vbCritical, APP_TITLE
        Exit Sub
    End If
    
    If txtImageMaxWidth.Text <> "" And IsNumeric(txtImageMaxWidth.Text) = False Then
        MsgBox "Image Maximum Width value must be numeric", vbCritical, APP_TITLE
        Exit Sub
    End If
    
    If cmbPrinter.Text = "SELECT FROM LIST" Then
        MsgBox "Please select a FAX Printer from the list", vbCritical, APP_TITLE
        Exit Sub
    End If
    
    
    'save settings now
    setSetting "/DocProcessor/Paths/Pickup", "", txtPickup.Text
    setSetting "/DocProcessor/Paths/Queue", "", txtQueue.Text
    setSetting "/DocProcessor/Paths/BadJob", "", txtBadJob.Text
    setSetting "/DocProcessor/Paths/Outbox", "", txtOutbox.Text
    setSetting "/DocProcessor/Paths/Archive", "", txtArchivePath.Text
    setSetting "/DocProcessor/Paths/Archive", "enabled", IIf(chkArchiveJobs.Value = vbChecked, "true", "false")
    setSetting "/DocProcessor/Paths/Archive", "ttl_days", txtArchiveTTL.Text
    
    setSetting "/DocProcessor/Fax/FaxPrinter", "", txtFaxPrinter.Text
    setSetting "/DocProcessor/Fax/FaxServer", "", txtFaxServers.Text
    setSetting "/DocProcessor/Fax/FaxUser", "", txtFaxUser.Text
    
    setSetting "/DocProcessor/SMTPServer", "server", txtSMTPServer.Text
    setSetting "/DocProcessor/SMTPServer", "timeout", txtSMTPTimeout.Text
    setSetting "/DocProcessor/SMTPServer", "emailMaxSize", txtSMTPSizeLimit.Text
    setSetting "/DocProcessor/SMTPServer", "defaultFrom", txtEmailFrom.Text
    
    setSetting "/DocProcessor/HTTPPost", "bytesLimit", txtHTTPLimit.Text
    
    setSetting "/DocProcessor/PDFMetadata/Author", "", txtPDFAuthor.Text
    setSetting "/DocProcessor/PDFMetadata/Creator", "", txtPDFCreator.Text
    setSetting "/DocProcessor/PDFMetadata/Producer", "", txtPDFProducer.Text
    
    setSetting "/DocProcessor/Tools", "directExecute", IIf(chkDirectExecute.Value = vbChecked, "true", "false")
    setSetting "/DocProcessor/Tools/Ghostscript/Path", "", txtGSPath.Text
    setSetting "/DocProcessor/Tools/PDFTK/Path", "", txtPDFTKPath.Text
    setSetting "/DocProcessor/Tools/ImageMagick/Path", "", txtImageMagickPath.Text
    setSetting "/DocProcessor/Tools/FTPClient/Path", "", txtFTPClient.Text
    
    
    setSetting "/DocProcessor/Tools/PDFPrinter/Name", "", txtPDFPrinter.Text
    setSetting "/DocProcessor/Tools/HTML2DOC/Path", "", txtHTMLDoc.Text

    setSetting "/DocProcessor/Tools/HTML2DOC/Options", "header", txtHTMLHeader.Text
    setSetting "/DocProcessor/Tools/HTML2DOC/Options", "footer", txtHTMLFooter.Text
    setSetting "/DocProcessor/Tools/HTML2DOC/Options", "topMargin", txtHTMLTop.Text
    setSetting "/DocProcessor/Tools/HTML2DOC/Options", "leftMargin", txtHTMLLeft.Text
    setSetting "/DocProcessor/Tools/HTML2DOC/Options", "bottomMargin", txtHTMLBottom.Text
    setSetting "/DocProcessor/Tools/HTML2DOC/Options", "rightMargin", txtHTMLRight.Text
    
    setSetting "/DocProcessor/Image", "maxWidth", txtImageMaxWidth.Text
    setSetting "/DocProcessor/Image", "maxHeight", txtImageMaxHeight.Text
    
    setSetting "/DocProcessor/Error/To", "", txtErrorTo.Text
    setSetting "/DocProcessor/Error/Subject", "", txtErrorSubject.Text
    
    setSetting "/DocProcessor/AppLog", "enabled", IIf(chkAppLog.Value = vbChecked, "true", "false")
    setSetting "/DocProcessor/AppLog", "", txtAppLogPath.Text
    
    setSetting "/DocProcessor/Overrides/Fax", "", txtFaxNumberOverride.Text
    setSetting "/DocProcessor/Overrides/EmailTo", "", txtEmailToOverride.Text
    setSetting "/DocProcessor/Overrides/FTP", "", txtFTPOverride.Text
    setSetting "/DocProcessor/Overrides/HTTP", "", txtHTTPOverride.Text
    setSetting "/DocProcessor/Overrides/FileCopy", "", txtFileCopyOverride.Text
        
    saveConfig
    
    cmdClose_Click
End Sub

Private Sub cmdToolsPath_Click(Index As Integer)
10        On Error GoTo errHandler
          Dim strPath As String
          Dim strTitle As String
20        Select Case Index
              Case 0:
30                strPath = txtGSPath.Text
40                dlg.DialogTitle = "Select gswin32c.exe"
50                dlg.FileName = ""
60                dlg.Filter = "gswin32c.exe|gswin32c.exe"
70                dlg.FilterIndex = 1
80            Case 1:
90                strPath = txtPDFTKPath.Text
100               dlg.DialogTitle = "Select pdftk.exe"
110               dlg.FileName = ""
120               dlg.Filter = "pdftk.exe|pdftk.exe"
130               dlg.FilterIndex = 1
140           Case 2:
150               strPath = txtImageMagickPath.Text
160               dlg.DialogTitle = "Select ImageMagick convert.exe"
170               dlg.FileName = ""
180               dlg.Filter = "convert.exe|convert.exe"
190               dlg.FilterIndex = 1
200           Case 3:
210               strPath = txtHTMLDoc.Text
220               dlg.DialogTitle = "Select htmldoc.exe"
230               dlg.FileName = ""
240               dlg.Filter = "htmldoc.exe|htmldoc.exe"
250               dlg.FilterIndex = 1
260           Case 4:
270               strPath = txtFTPClient.Text
280               dlg.DialogTitle = "Select winscp.com"
290               dlg.FileName = ""
300               dlg.Filter = "winscp.com|winscp.com"
310               dlg.FilterIndex = 1
320       End Select
          
330       dlg.ShowOpen
          
340       If dlg.FileName <> "" Then
350           Select Case Index
                  Case 0:
360                   txtGSPath.Text = dlg.FileName
370                   txtGSPath.ForeColor = vbBlack
380               Case 1:
390                   txtPDFTKPath.Text = dlg.FileName
400                   txtPDFTKPath.ForeColor = vbBlack
410               Case 2:
420                   txtImageMagickPath.Text = dlg.FileName
430                   txtImageMagickPath.ForeColor = vbBlack
440               Case 3:
450                   txtHTMLDoc.Text = dlg.FileName
460                   txtHTMLDoc.ForeColor = vbBlack
470               Case 4:
480                   txtFTPClient.Text = dlg.FileName
490                   txtFTPClient.ForeColor = vbBlack
500           End Select
510       End If
errHandler:
End Sub

Private Sub Form_Load()
    Dim strHTMLDocHeaderHelp As String
    Dim prn As Printer
    Dim i As Integer
    Dim blnPrinterFound As Boolean
    
    loadConfig
    
    cmbPrinter.Clear
    
    For Each prn In Printers
        cmbPrinter.AddItem prn.DeviceName
    Next
    
    strHTMLDocHeaderHelp = ". = blank" & vbCrLf & _
                           "/ = n/N arabic page numbers (1/3, 2/3, 3/3)" & vbCrLf & _
                           ": = c/C arabic chapter page numbers (1/2, 2/2, 1/4, 2/4, ...)" & vbCrLf & _
                           "1 = arabic numbers (1, 2, 3, ...)" & vbCrLf & _
                           "a = lowercase letters" & vbCrLf & _
                           "A = uppercase letters" & vbCrLf & _
                           "c = current chapter heading" & vbCrLf & _
                           "C = current chapter page number (arabic)" & vbCrLf & _
                           "d = current date" & vbCrLf & _
                           "D = current date and time" & vbCrLf & _
                           "h = current heading" & vbCrLf & _
                           "i = lowercase roman numerals" & vbCrLf & _
                           "I = uppercase roman numerals" & vbCrLf & _
                           "l = logo image" & vbCrLf & _
                           "t = title text" & vbCrLf & _
                           "T = current time"
                           
    'txtHTMLFooter.ToolTipText = strHTMLDocHeaderHelp
    'txtHTMLHeader.ToolTipText = strHTMLDocHeaderHelp
    
    txtPickup.Text = getSetting("/DocProcessor/Paths/Pickup", , False)
    txtQueue.Text = getSetting("/DocProcessor/Paths/Queue", , False)
    txtBadJob.Text = getSetting("/DocProcessor/Paths/BadJob", , False)
    txtOutbox.Text = getSetting("/DocProcessor/Paths/Outbox", , False)
    txtArchivePath.Text = getSetting("/DocProcessor/Paths/Archive", , False)
    chkArchiveJobs.Value = IIf(LCase(getSetting("/DocProcessor/Paths/Archive", "enabled", False)) = "true", vbChecked, vbUnchecked)
    txtArchiveTTL.Text = getSetting("/DocProcessor/Paths/Archive", "ttl_days", False)
    'default TTL
    If IsNumeric(txtArchiveTTL.Text) = False Then txtArchiveTTL.Text = "30"
    
    txtFaxServers.Text = getSetting("/DocProcessor/Fax/FaxServer", , False)
    txtFaxPrinter.Text = getSetting("/DocProcessor/Fax/FaxPrinter", , False)
    For i = 0 To cmbPrinter.ListCount - 1
        If cmbPrinter.List(i) = txtFaxPrinter.Text Then
            cmbPrinter.ListIndex = i
            blnPrinterFound = True
            Exit For
        End If
    Next
    
    If blnPrinterFound = False Then
        cmbPrinter.AddItem "SELECT FROM LIST", 0
        cmbPrinter.ForeColor = &HFF&
        cmbPrinter.ListIndex = 0
    End If
    txtFaxUser.Text = getSetting("/DocProcessor/Fax/FaxUser", , False)
    
    txtImageMaxHeight.Text = getSetting("/DocProcessor/Image", "maxHeight", False)
    txtImageMaxWidth.Text = getSetting("/DocProcessor/Image", "maxWidth", False)
    
    If IsNumeric(txtImageMaxHeight.Text) = False Then txtImageMaxHeight.Text = "1024"
    If IsNumeric(txtImageMaxWidth.Text) = False Then txtImageMaxWidth.Text = "1024"
    
    txtHTTPLimit.Text = getSetting("/DocProcessor/HTTPPost", "bytesLimit", False)
    If IsNumeric(txtHTTPLimit.Text) = False Then txtHTTPLimit.Text = ""
    
    txtSMTPServer.Text = getSetting("/DocProcessor/SMTPServer", "server", False)
    txtSMTPTimeout.Text = getSetting("/DocProcessor/SMTPServer", "timeout", False)
    txtSMTPSizeLimit.Text = getSetting("/DocProcessor/SMTPServer", "emailMaxSize", False)
    txtEmailFrom.Text = getSetting("/DocProcessor/SMTPServer", "defaultFrom", False)
    
    txtPDFAuthor.Text = getSetting("/DocProcessor/PDFMetadata/Author", , False)
    txtPDFCreator.Text = getSetting("/DocProcessor/PDFMetadata/Creator", , False)
    txtPDFProducer.Text = getSetting("/DocProcessor/PDFMetadata/Producer", , False)
    txtGSPath.Text = getSetting("/DocProcessor/Tools/Ghostscript/Path", , False)
    txtPDFTKPath.Text = getSetting("/DocProcessor/Tools/PDFTK/Path", , False)
    txtImageMagickPath.Text = getSetting("/DocProcessor/Tools/ImageMagick/Path", , False)
    txtFTPClient.Text = getSetting("/DocProcessor/Tools/FTPClient/Path", , False)
    
    chkDirectExecute.Value = IIf(LCase(getSetting("/DocProcessor/Tools", "directExecute", False)) = "true", vbChecked, vbUnchecked)
    
    txtPDFPrinter.Text = getSetting("/DocProcessor/Tools/PDFPrinter/Name", , False)
    txtHTMLDoc.Text = getSetting("/DocProcessor/Tools/HTML2DOC/Path", , False)

    txtHTMLHeader.Text = getSetting("/DocProcessor/Tools/HTML2DOC/Options", "header", False)
    txtHTMLFooter.Text = getSetting("/DocProcessor/Tools/HTML2DOC/Options", "footer", False)
    txtHTMLTop.Text = getSetting("/DocProcessor/Tools/HTML2DOC/Options", "topMargin", False)
    txtHTMLLeft.Text = getSetting("/DocProcessor/Tools/HTML2DOC/Options", "leftMargin", False)
    txtHTMLBottom.Text = getSetting("/DocProcessor/Tools/HTML2DOC/Options", "bottomMargin", False)
    txtHTMLRight.Text = getSetting("/DocProcessor/Tools/HTML2DOC/Options", "rightMargin", False)
    
    txtErrorTo.Text = getSetting("/DocProcessor/Error/To", , False)
    txtErrorSubject.Text = getSetting("/DocProcessor/Error/Subject", , False)
    
    chkAppLog.Value = IIf(LCase(getSetting("/DocProcessor/AppLog", "enabled", False)) = "true", vbChecked, vbUnchecked)
    txtAppLogPath.Text = getSetting("/DocProcessor/AppLog", , False)
    
    txtFaxNumberOverride.Text = getSetting("/DocProcessor/Overrides/Fax", , False)
    txtEmailToOverride.Text = getSetting("/DocProcessor/Overrides/EmailTo", , False)
    txtFTPOverride.Text = getSetting("/DocProcessor/Overrides/FTP", , False)
    txtHTTPOverride.Text = getSetting("/DocProcessor/Overrides/HTTP", , False)
    txtFileCopyOverride.Text = getSetting("/DocProcessor/Overrides/FileCopy", , False)
    
    If txtFaxNumberOverride.Text <> "" Or _
        txtEmailToOverride.Text <> "" Or _
        txtFTPOverride.Text <> "" Or _
        txtHTTPOverride.Text <> "" Or _
        txtFileCopyOverride.Text <> "" Then
        frmOverrides.FontBold = True
        frmOverrides.ForeColor = vbBlue
    End If
    
    
    'mark things that are invalid
    checkPathValid txtPickup
    checkPathValid txtQueue
    checkPathValid txtOutbox
    checkPathValid txtBadJob

    checkPathValid txtAppLogPath
    
    checkFileValid txtGSPath
    checkFileValid txtPDFTKPath
    checkFileValid txtImageMagickPath
    checkFileValid txtHTMLDoc
    checkFileValid txtFTPClient
    
    checkPrinterExist txtPDFPrinter
    checkPrinterExist txtFaxPrinter
End Sub

Private Sub Form_Unload(Cancel As Integer)
    unloadConfig
End Sub

Private Sub checkPathValid(ByRef obj As TextBox)
    If obj.Text <> "" Then
        If m_objFSO.FolderExists(resolveMacro(obj.Text)) = False Then
            obj.ForeColor = vbRed
        End If
    End If
End Sub

Private Sub checkFileValid(ByRef obj As TextBox)
    If obj.Text <> "" Then
        If m_objFSO.FileExists(obj.Text) = False Then
            obj.ForeColor = vbRed
        End If
    End If
End Sub

Private Sub checkPrinterExist(ByRef obj As TextBox)
    Dim prn As Printer
    Dim bPrinterFound As Boolean
    If obj.Text <> "" Then
        bPrinterFound = False
        For Each prn In Printers
            If prn.DeviceName = obj.Text Then
                bPrinterFound = True
                Exit For
            End If
        Next
        obj.ForeColor = IIf(bPrinterFound, vbBlack, vbRed)
    End If
End Sub

Private Sub txtFaxNumberOverride_LostFocus()
    Dim strFaxNumber As String
    
    If txtFaxNumberOverride.Text <> "" Then
        strFaxNumber = txtFaxNumberOverride.Text
        strFaxNumber = Trim(strFaxNumber)
        strFaxNumber = Replace(strFaxNumber, " ", "")
        strFaxNumber = Replace(strFaxNumber, "-", "")
        strFaxNumber = Replace(strFaxNumber, "(", "")
        strFaxNumber = Replace(strFaxNumber, ")", "")
        
        txtFaxNumberOverride.Text = strFaxNumber
        
        If IsNumeric(txtFaxNumberOverride.Text) = False Then
            MsgBox "Fax number contains invalid characters. It must be 10 digits long.", vbOKOnly + vbCritical, APP_TITLE
            txtFaxNumberOverride.SelStart = 0
            txtFaxNumberOverride.SelLength = Len(txtFaxNumberOverride.Text)
            txtFaxNumberOverride.SetFocus
        End If
        
        If Len(txtFaxNumberOverride.Text) < 10 Or Len(txtFaxNumberOverride.Text) > 11 Then
            MsgBox "Fax number must be 10 digits long.", vbOKOnly + vbCritical, APP_TITLE
            txtFaxNumberOverride.SelStart = 0
            txtFaxNumberOverride.SelLength = Len(txtFaxNumberOverride.Text)
            txtFaxNumberOverride.SetFocus
        End If
    End If
End Sub

Private Sub txtPDFPrinter_Change()
    checkPrinterExist txtPDFPrinter
End Sub
