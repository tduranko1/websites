'**********************************************************************************
'  Script to monitor the LYNX Document Processor Service. The script will find the
'  oldest job in the queue. If this file stays for subsequent runs, then the queue
'  is standstill and the service is not working. If the service is set to Auto
'  mode, this script will try to restart the service automatically. If it fails,
'  the script will escalate this to the email addresses mentioned in the congif
'  file.
'
'  Author: Ramesh R. Vishegu
'  History:
'   Date       User Initial   Change
'   ---------- -------------- -----------------------------------------------------
'   06/10/2007 RV             Initial Release
'
'**********************************************************************************


'On Error Resume Next
On error goto 0

Dim strServer
Dim objFSO
Dim objConfig, objConfigRoot
Dim objProcessorConfig, objProcessorConfigRoot
Dim objCheckerXML
Dim strServiceName
Dim strConfigFile
Dim strScriptPath
Dim strScriptTitle
Dim strProcessorConfigFile
Dim strSortTempPath, strQueuePath
Dim dteLastSuccessfulJob, dteOldestJob, dteLastEscalated, dteLastRestarted
Dim strScriptRecordFile, strScriptLogFile
Dim objFolder
Dim objRecordFile
Dim strLine
Dim blnFirstRun
Dim oShell
Dim blnEscalate, intEscalateInterval
Dim blnDebug
Dim strSMTPServer
Dim dteNow
Dim strMachineName
Dim objTemp

set oShell = WScript.CreateObject("WScript.Shell")
set objFSO = WScript.CreateObject("Scripting.FileSystemObject")
set objTemp = WScript.CreateObject("WScript.Network")
if not objTemp is nothing then
   strMachineName = objTemp.ComputerName
end if
set objTemp = nothing


strServer = "."
strScriptTitle = "LYNX Document Processor Checker Script"
strScriptPath = objFSO.GetParentFolderName(WScript.ScriptFullName)
strConfigFile = "ProcMonitor.xml"
strProcessorConfigFile = "config.xml"
strScriptRecordFile = "ProcMonitorStatus.xml"
strScriptLogFile = "ProcMonitor.log"
blnFirstRun = false
dteOldestJob = CDate("1/1/1900")
blnEscalate = false
blnDebug = false

dteNow = now()

'On Error Goto 0

call main()


call quitScript()


'**********************************************
' Main subroutine
'**********************************************
sub main()
   Dim intServiceStatus 
   Dim strTempFile
   Dim strFileName
   Dim objTempFile
   Dim intJobCount
   CONST PROC_NAME = "main()"

   strConfigFile = objFSO.BuildPath(strScriptPath, strConfigFile)
   strProcessorConfigFile = objFSO.BuildPath(strScriptPath, strProcessorConfigFile)
   strScriptRecordFile = objFSO.BuildPath(strScriptPath, strScriptRecordFile)
   strScriptLogFile = objFSO.BuildPath(strScriptPath, strScriptLogFile)
   
   'check if the configuration file exists
   if objFSO.FileExists(strScriptLogFile) then
      'delete the trace log
      objFSO.DeleteFile strScriptLogFile
   end if

   trace "===== Script executed at " & formatDateTime(dteNow, 0) & " =====" & vbcrlf & vbcrlf 

   'check if the configuration file exists
   if objFSO.fileExists(strConfigFile) = false then
      showMessage "Configuration file " & strConfigFile & " does not exist.", vbCritical
      call quitScript()
   end if

   'check if the processor configuration file exists
   if objFSO.fileExists(strProcessorConfigFile) = false then
      showMessage "LYNX Document Processor configuration file " & strProcessorConfigFile & " does not exist.", vbCritical
      call quitScript()
   end if

   'load the configuration settings
   call loadConfig()

   if strServiceName = "" then
      showMessage "Service name could not be read from the configuration file.", vbcritical
      call quitScript()
   else
      'check the service status on the server
      intServiceStatus = checkService()
      select case intServiceStatus
         case -1: trace strServiceName & " not found on " & strServer & " (. means local)" & vbcrlf 
         case 0:  trace strServiceName & " found on " & strServer & " (. means local) and it is set to manual/disable mode." & vbcrlf 
         case 1:  trace strServiceName & " found on " & strServer & " (. means local) and it is not running." & vbcrlf 
         case 2:  trace strServiceName & " found on " & strServer & " (. means local) and it is running." & vbcrlf 
         case else
      end select
      if intServiceStatus < 1 then
         call quitScript()
      end if
   end if

   strSortTempPath = Replace(strSortTempPath, "$AppPath$", strScriptPath)

   'if this is the first time the script is run, the last successful job ran will be defaulted to 1/1/1900
   if dteLastSuccessfulJob = "1/1/1900" then
      blnFirstRun = true
   end if
   
   'restart the service every midnight.
   if abs(DateDiff("d", dteLastRestarted, dteNow)) >= 1 then
      strScriptLogFile = objFSO.BuildPath(strScriptPath, "ProcMonitor-restart.log")
      trace "Script is attempting to restart and cleanup the service at the scheduled midnight." & vbcrlf
      restartService()
      saveOldestJob()
      call quitScript()
   end if
   
   trace "Finding the oldest job in the queue... " & vbcrlf
   
   'set the oldest job to the one that was found in the previous run
   'dteOldestJob = dteLastSuccessfulJob

   'get the oldest job file from the queue
   set objFolder = objFSO.GetFolder(strQueuePath)
   if not objFolder is nothing then
      trace "  Found " & objFolder.Files.count & " files in the queue." & vbcrlf 
      intJobCount = 0
      for each objFile in objFolder.Files
         strFileName = objFile.Name
         strTempFile = objFSO.BuildPath(strScriptPath, strFileName)

         if objFSO.getExtensionName(strFileName) = "xml" and mid(strFileName, 2, 1) = "_" then
            intJobCount = intJobCount + 1
            'copy the file to local so we wont disturb the server
            objFile.copy strTempFile, true
            
            set objTempFile = objFSO.GetFile(strTempFile)
            
            if (dteOldestJob = CDate("1/1/1900")) then 
               dteOldestJob = objTempFile.DateLastModified
            else
               if objTempFile.DateLastModified < dteOldestJob then
                  dteOldestJob = objTempFile.DateLastModified
               end if
            end if
            objTempFile.Delete
            set objTempFile = nothing
         end if
      next
      
      if intJobCount = 0 then
         dteOldestJob = dteNow
      end if
   end if
   
   trace "  Oldest job in the queue is dated " & formatDateTime(dteOldestJob, 0) & vbcrlf & _
         "    [1/1/1900 means default oldest date]" & vbcrlf 
   
   if blnFirstRun then
      'record the oldest file and quit. Next time we will see for any pending items
      trace "Script is run for the first time. Saving the findings and will quit." & vbcrlf
      saveOldestJob()
      call quitScript()
   else
      if DateDiff("s", dteOldestJob, dteLastSuccessfulJob) >= 0 then
         'the queue seems to be stuck. no lets try to cleanup and restart the service
         trace "Queue seems to be stuck. Script will try to cleanup and restart the service. [" & DateDiff("s", dteOldestJob, dteLastSuccessfulJob) & " seconds]" & vbcrlf & _
               "  dteOldestJob: " & CStr(dteOldestJob) & vbcrlf & _
               "  dteLastSuccessfulJob: " & CStr(dteLastSuccessfulJob) & vbcrlf 
         restartService()
      else
         trace "Queue is moving. Script will save the latest findings and quit." & vbcrlf
      end if
   end if
   
   trace "Escalation is set to " & blnEscalate & vbcrlf
   
   if blnEscalate = true then
      'check to see when the previous escalate notification was sent.
      if DateDiff("n", dteLastEscalated, dteNow) >= intEscalateInterval then
         trace "Escalation notification. Time since last notification: " & DateDiff("n", dteLastEscalated, dteNow) & " min" & vbcrlf
         
         call NotifyEscalate("Server: " & strMachineName & vbcrlf & _
                             "Service Name: " & strServiceName & vbcrlf & vbcrlf & _
                             "Please manually restart this service ASAP.")
         
      else
         trace "No need to esclate this soon." & vbcrlf
      end if
   end if
   
   saveOldestJob()

end sub

sub quitScript()
   trace vbcrlf & "===== Script terminated at " & formatDateTime(dteNow, 0) & " =====" & vbcrlf 
   Wscript.Quit()
end sub



'******************************
' Load Configuration files
'******************************
sub loadConfig()
   trace "Loading script configuration file..."
   'load the script's configuration file
   set objConfig = WScript.CreateObject("MSXML2.DOMDocument")
   objConfig.async = false
   objConfig.load strConfigFile
   
   if objConfig.parseError.errorCode <> 0 Then
      showMessage "Configuration file is malformed." & vbcrlf & _
                  "     Reason: " & objConfig.parseError.reason & _
                  "     Line #: " & objConfig.parseError.line & " Position: " & objConfig.parseError.linePos, vbCritical
      call quitScript()
   end if
   
   set  objConfigRoot = objConfig.selectSingleNode("/LYNXDocProcessorChecker")
   
   trace "Done." & vbcrlf
   
   blnDebug = not CBool(objConfigRoot.getAttribute("ui"))
   
   trace "Loading the service configuration file..."
   'load the Document Processor's configuration file
   set objProcessorConfig = WScript.CreateObject("MSXML2.DOMDocument")
   objProcessorConfig.async = false
   objProcessorConfig.load strProcessorConfigFile
   
   if objProcessorConfig.parseError.errorCode <> 0 Then
      showMessage strServiceName & " configuration file is malformed." & vbcrlf & _
                  "     Reason: " & objProcessorConfig.parseError.reason & _
                  "     Line #: " & objProcessorConfig.parseError.line & " Position: " & objProcessorConfig.parseError.linePos, vbCritical
      call quitScript()
   end if
   
   set objProcessorConfigRoot = objProcessorConfig.selectSingleNode("/DocProcessor")
   
   trace "Done." & vbcrlf & _
         "Loading the script record file..."
   
   'load the script status file
   set objCheckerXML = WScript.CreateObject("MSXML2.DOMDocument")
   objCheckerXML.async = false
   
   if objFSO.FileExists(strScriptRecordFile) then
      objCheckerXML.load strScriptRecordFile
      if objCheckerXML.parseError.errorCode <> 0 Then
         trace "Loading default."
         objCheckerXML.loadXML "<Root><OldestJob>1/1/1900</OldestJob><LastEscalated>1/1/1900</LastEscalated></Root>"
      end if
   else
      trace "Loading default."
      objCheckerXML.loadXML "<Root><OldestJob>1/1/1900</OldestJob><LastEscalated>1/1/1900</LastEscalated></Root>"
   end if
   
   trace "Done." & vbcrlf 
   
   strServer = getConfigValue(objConfigRoot, "/LYNXDocProcessorChecker/Service", "server")
   strServiceName = getConfigValue(objConfigRoot, "/LYNXDocProcessorChecker/Service", "name")
   strSMTPServer = getConfigValue(objConfigRoot, "/LYNXDocProcessorChecker/Notify", "SMTPServer")
   strSortTempPath = getConfigValue(objProcessorConfigRoot, "/DocProcessor/Paths/SortTemp", "")
   strQueuePath = getConfigValue(objProcessorConfigRoot, "/DocProcessor/Paths/Queue", "")
   intEscalateInterval = getConfigValue(objConfigRoot, "/LYNXDocProcessorChecker/Notify/Escalate", "interval")
   
   if intEscalateInterval <> "" and isNumeric(intEscalateInterval) then
      intEscalateInterval = CInt(intEscalateInterval)
   else
      intEscalateInterval = 60 'default to 60 minutes
   end if
   
   dteLastSuccessfulJob = getConfigValue(objCheckerXML, "/Root/OldestJob", "")
   dteLastEscalated = getConfigValue(objCheckerXML, "/Root/LastEscalated", "")
   dteLastRestarted = getConfigValue(objCheckerXML, "/Root/LastRestarted", "")
   
   trace "Settings:" & vbcrlf & _
         "  Service Name: " & strServiceName & vbcrlf & _
         "  SortTemp Path: " & strSortTempPath & vbcrlf & _
         "  Queue Path: " & strQueuePath & vbcrlf & _
         "  Oldest Job timestamp: " & dteLastSuccessfulJob & vbcrlf & _
         "  Last Escalated timestamp: " & dteLastEscalated & vbcrlf
   
   if isDate(dteLastSuccessfulJob) then
      dteLastSuccessfulJob = CDate(dteLastSuccessfulJob)
   end if
   
   if isDate(dteLastEscalated) then
      dteLastEscalated = CDate(dteLastEscalated)
   else
      dteLastEscalated = CDate("1/1/1900")
   end if

   if isDate(dteLastRestarted) then
      dteLastRestarted = CDate(dteLastRestarted)
   else
      dteLastRestarted = dteNow
   end if
end sub


'**********************************************
' Helper function to display messages
'**********************************************
sub showMessage(strMessage, intIndicator)
   if not blnDebug then
      Msgbox strMessage & vbcrlf & vbcrlf & _
             "Script Path: " & strScriptPath, intIndicator, strScriptTitle
   else
      trace strMessage
   end if
end sub



'**********************************************
' Retrieve a configuration setting
'**********************************************
Function getConfigValue(objRoot, strXMLPath, strAttrib)
   Const PROC_NAME = "getConfigValue()"
   Dim strRet, oNode
   strRet = ""
   
   if strXMLPath <> "" then
      if not objRoot is nothing then
         Set oNode = objRoot.selectSingleNode(strXMLPath)
         if not oNode is nothing then
            if strAttrib <> "" then
               strRet = oNode.getAttribute(strAttrib)
            else
               strRet = oNode.text
            end if
         end if
      else
         showMessage "objRoot is not set.", vbInformation
      end if
   end if
   
   getConfigValue = strRet
End Function


'**********************************************
' Save the Oldest Job in the queue
'**********************************************
sub saveOldestJob()
   'record the oldest file and quit. Next time we will see for any pending items
   'set objRecordFile = objFSO.OpenTextFile(strScriptRecordFile, 2, true)
   'objRecordFile.WriteLine FormatDateTime(dteOldestJob, 0)
   'set objRecordFile = nothing
   trace vbcrlf & "Saving script record file..."
   objCheckerXML.selectSingleNode("/Root/OldestJob").text = dteOldestJob
   objCheckerXML.selectSingleNode("/Root/LastEscalated").text = dteLastEscalated
   objCheckerXML.selectSingleNode("/Root/LastRestarted").text = dteLastRestarted
   objCheckerXML.save strScriptRecordFile
   trace "Done." & vbcrlf 
end sub


'**********************************************
' Returns the status of the service
'   -1 -> Service not found
'   0  -> Service found. It is set to manual/disabled mode
'   1  -> Service found. It is active and not running
'   2  -> Service found. It is active and running
'**********************************************
function checkService()
   dim intFound
   Dim iRetCode
   Dim blnServiceFound

   intFound = -1 'not found

   Set objWMIService = GetObject("winmgmts:\\" & strServer & "\root\cimv2")
   Set colItems = objWMIService.ExecQuery("Select * from Win32_Service",,48)
   For Each objItem in colItems
      if objItem.DisplayName = strServiceName then
         if objItem.StartMode = "Auto" then
            if objItem.Started = true then
               intFound = 2
            else
               intFound = 1
            end if
         else
            intFound = 0
         end if
      end if
   Next
   
   checkService = intFound
end function


'**********************************************
' Restart the service
'**********************************************
sub restartService()
   Dim iRetCode
   Dim blnServiceFound
   trace "Restarting service" & vbcrlf & _
         "  Finding service """ & strServiceName & """..." & vbcrlf
         
   blnServiceFound = false
   Set objWMIService = GetObject("winmgmts:\\" & strServer & "\root\cimv2")
   Set colItems = objWMIService.ExecQuery("Select * from Win32_Service",,48)
   For Each objItem in colItems
      if objItem.DisplayName = strServiceName then
         blnServiceFound = true
         trace "  Service found." & vbcrlf
         if objItem.StartMode = "Auto" then
            if objItem.Started = false then
               trace "  Service is not running." & vbcrlf 
               'cleanup the sort temp folder
               cleanupSortTemp()
               ' the service has been stopped for some reason. Start it now
               trace "Starting the service..."
               iRetCode = objItem.StartService()
               trace "Return Code:" & iRetCode & vbcrlf 
               if iRetCode <> 0 then
                  'service did not start. need to escalate
                  blnEscalate = true
                  trace "  Escalate set to true." & vbcrlf 
               end if
            else
               trace "  Stopping the service..."
               ' the service seems to be stuck. Lets try to revive it.
               iRetCode = objItem.StopService()
               trace "Return Code: " & iRetCode & vbcrlf 
               if iRetCode = 0 then
                  on error Resume Next
                  cleanupProcess()
                  
                  Err.Clear
                  
                  WScript.Sleep 5000 'sleep for a second so that any other process will do its stuff

                  on error Resume Next
                  cleanupSortTemp()
                  
                  Err.Clear
                  
                  WScript.Sleep 1000 'sleep for a second so that any other process will do its stuff

                  trace "  Starting the service..."
                  iRetCode = objItem.StartService()
                  trace "Return Code: " & iRetCode & vbcrlf 
                  if iRetCode <> 0 then
                     'service did not start. need to escalate
                     blnEscalate = true
                     trace "  Escalate is set to true" & vbcrlf 
                  else
                     dteLastRestarted = dteNow
                  end if
               else
                  'error stopping the service. need to escalate
                  'showMessage "Unable to stop the service", vbCritical
                  blnEscalate = true
                  trace "  Escalate is set to true" & vbcrlf 
               end if
               
            end if
         else
            trace "  Service is not set to Auto start." & vbcrlf
         end if
         exit for
      end if
   Next
   
   if blnServiceFound = false then
      trace "  Service NOT FOUND." & vbcrlf
   end if
end sub



'**********************************************
' Clean up process
'**********************************************
sub cleanupProcess()
   'kill the processes
   Dim objDependantProcesses
   Dim objProcess
   Dim iRetCode
   Dim strProcessName
   on error resume next
   trace "  Cleaning up dependant processes..." & vbcrlf 
   set objDependantProcesses = objConfig.selectNodes("/LYNXDocProcessorChecker/DependantProcess/Process")
   for each objProcess in objDependantProcesses
      'showMessage objProcess.getAttribute("exename"), vbInformation
      strProcessName = objProcess.getAttribute("exename")
      trace "    Terminating process " & strProcessName 
      iRetCode = killProcess(strProcessName)
      if err.number <> 0 then
         trace "Failed. Return Code: " & iRetCode & vbcrlf 
         err.clear
      else
         trace "Done. Return Code: " & iRetCode & vbcrlf 
      end if
   next
end sub



'**********************************************
' Clean up files in the Sort Temp folder
'**********************************************
sub cleanupSortTemp()
   Dim objSortTempFolder
   Dim objTmpFile
   
   err.clear
   
   trace "  Cleaning up files in SortTemp folder: " & strSortTempPath & vbcrlf 
   'now clean up the sortTemp folder
   on error resume next
   set objSortTempFolder = objFSO.GetFolder(strSortTempPath)
   'for each objTmpFile in objSortTempFolder.Files
   '   objTmpFile.Delete true
   'next
   if objSortTempFolder.Files.Count > 0 then
      objFSO.DeleteFile objFSO.BuildPath(strSortTempPath, "*.*", true)
   end if

   set objSortTempFolder = objFSO.GetFolder(strSortTempPath)
   for each objTmpFile in objSortTempFolder.Files
      objTmpFile.Delete true
      if err.number <> 0 then
         trace "ERROR deleting file " & objTmpFile.Name & vbcrlf
      end if
   next
   
   if err.number <> 0 then
      trace "ERROR while deleting files from " & strSortTempPath & vbcrlf
   end if
   on error goto 0
end sub



'**********************************************
' Kill a process
'**********************************************
sub killProcess(strProcessName)
   Dim objWMISvc, objProc, colProc
   Set objWMISvc = GetObject("winmgmts:\\" & strServer & "\root\cimv2")
   
   Set colProc = objWMISvc.ExecQuery("Select * from Win32_Process where name ='" & strProcessName & "'")
   For Each objProc in colProc
      if LCase(objProc.Name) = LCase(strProcessName) then
         objProc.Terminate()
      end if
   Next
end sub



'**********************************************
' Debug log trace
'**********************************************
sub trace(strMessage)
   dim objTraceFile
   'log a debug message
   
   if strScriptLogFile <> "" then
      set objTraceFile = objFSO.OpenTextFile(strScriptLogFile, 8, true)
      objTraceFile.Write strMessage
      set objTraceFile = nothing
   end if
end sub


'**********************************************
' Send email notification
'**********************************************
Function SendMail(sTo, sFrom, sSubject, sBody , sCC , sBCC )
'   On Error goto 0
   CONST PROC_NAME = "SendMail()"
   'on error resume next
   
   Trace "Sending email..."
   Dim iMsg
   Set iMsg = WScript.CreateObject("CDO.Message")
   
   Dim iConf 
   Set iConf = WScript.CreateObject("CDO.Configuration")
   

   Dim Flds
   Set Flds = iConf.Fields
   
   With Flds
     .Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 ' cdoSendUsingPort
     .Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = strSMTPServer
     .Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 10 ' quick timeout
     .Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 0
     .Update
   End With

   With iMsg
     Set .Configuration = iConf
         .To         =  sTo
         .From       =  sFrom
         .Subject    =  sSubject
         .TextBody   =  sBody
         .CC         =  sCC
         .BCC        =  sBCC
         .Send
   End With
   
   if err.number <> 0 then
      trace "NOT SENT. Error description: " & Err.description
      Err.Clear
   else
      trace "Sent." & vbcrlf 

   end if
end Function

sub NotifyEscalate(strMessage)
   Dim strFrom, strTo, strSubject, strBody
   
   strFrom = getConfigValue(objConfigRoot, "/LYNXDocProcessorChecker/Notify/Escalate/From", "")
   strTo = getConfigValue(objConfigRoot, "/LYNXDocProcessorChecker/Notify/Escalate/To", "")
   strSubject = getConfigValue(objConfigRoot, "/LYNXDocProcessorChecker/Notify/Escalate/Subject", "")
   strBody = getConfigValue(objConfigRoot, "/LYNXDocProcessorChecker/Notify/Escalate/Body", "")
   
   strSubject = strSubject & " [" & strMachineName & "]"
   
   strBody = Replace(strBody, "|", vbcrlf)
   
   if strMessage <> "" then
      strBody = strBody & vbcrlf & strMessage
   end if
   
   Trace "Escalation email notification:" & vbcrlf & _
         "   SMTP Server: " & strSMTPServer & vbcrlf & _
         "   From: " & strFrom & vbcrlf & _
         "   To: " & strTo & vbcrlf & _
         "   Subject: " & strSubject & vbcrlf & _
         "   Body: " & strBody & vbcrlf
         
   
   if strFrom <> "" and strTo <> "" then
      call SendMail(strTo, strFrom, strSubject, strBody, "", "")
      
      dteLastEscalated = dteNow
   else
      trace "From and To values are empty. Cannot send notification." & vbcrlf 
   end if
   
end sub

sub checkError (strProcName, strPosition, strMessage)
   if err.number <> 0 then
      trace vbcrlf & "ERROR in " & strProcName & " [Position:" & strPosition & "] " & strMessage & " - " & err.description
      call quitScript()
   end if
end sub
