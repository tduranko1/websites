VERSION 5.00
Object = "{10000000-1024-11CF-A19E-0020AF333BD8}#1.0#0"; "pixfil32.ocx"
Object = "{E7BC34A0-BA86-11CF-84B1-CBC2DA68BF6C}#1.0#0"; "ntsvc.ocx"
Object = "{10000000-1006-11CF-A19E-0020AF333BD8}#1.0#0"; "pixdsp32.ocx"
Begin VB.Form frmMainSvc 
   Caption         =   "LYNX Document Processor Service"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin PixDISPLib.PixDisp dspImage 
      Height          =   495
      Left            =   2100
      TabIndex        =   0
      Top             =   1380
      Width           =   495
      _Version        =   131077
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   33
      BackColor       =   16777215
      BorderStyle     =   1
      Active          =   -1  'True
      LinkID          =   "34-57%3+!"
      Reserved1       =   1
      Reserved2       =   $"frmMainSvc.frx":0000
      PixDispZoomPercentToImg=   0   'False
      UsePrintAccel   =   0   'False
      PixDispBrightness=   127
      PixDispContrast =   127
      PixDispRedBrightness=   127
      PixDispRedContrast=   127
      PixDispGreenBrightness=   127
      PixDispGreenContrast=   127
      PixDispBlueBrightness=   127
      PixDispBlueContrast=   127
      AcceptDragFiles =   0   'False
      FireGUIEvents   =   0   'False
      ImageDataSource =   "1:&08PH.!"
      Invert          =   0   'False
      LeftButtonOption=   4
      LeftMouseBoxStyle=   1
      PrintTargetLeft =   0
      PrintTargetTop  =   0
      PrintTargetRight=   0
      PrintTargetBottom=   0
      RightButtonOption=   1
      RightMouseBoxStyle=   2
      Rotation        =   0
      ScaleToGray     =   0   'False
      ScaleToGrayLevel=   1
      ScrollBars      =   3
      WorkingRegionStyle=   1
      ZoomInOutChange =   15
   End
   Begin VB.Timer tmrSvc 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   4200
      Top             =   2700
   End
   Begin NTService.NTService NTSvc 
      Left            =   540
      Top             =   60
      _Version        =   65536
      _ExtentX        =   741
      _ExtentY        =   741
      _StockProps     =   0
      DisplayName     =   "LYNX Document Processor Service"
      ServiceName     =   "LYNXDocumentProcessorSvc"
      StartMode       =   2
   End
   Begin VB.Timer tmrJobsRefresh 
      Enabled         =   0   'False
      Interval        =   10000
      Left            =   3780
      Top             =   2700
   End
   Begin PIXFILELib.PixFile filImage 
      Left            =   0
      Top             =   0
      _Version        =   131075
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
      Active          =   -1  'True
      LinkID          =   "1:&08PH.!"
      Reserved1       =   1
      Reserved2       =   $"frmMainSvc.frx":00A8
      ImageDataSource =   "<None>"
      InputFileName   =   ""
      OutputBitsPerSample=   1
      OutputFileAppend=   0   'False
      OutputFileFormat=   24
      OutputPhotoInterp=   0
      OutputSamplesPerPixel=   1
      OutputScalingNum=   0
      OutputScalingDen=   0
      PageIndex       =   0
      JPEGCompressionFactor=   0
      OutputRotation  =   0
      OutputScalingYNum=   0
      OutputScalingYDen=   0
   End
End
Attribute VB_Name = "frmMainSvc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
 Option Explicit
Option Compare Text

Const MODULE_NAME As String = "frmMainSvc:"

Dim m_strCurrentDoc As String
Dim m_strCurrentDocXSL As String
Dim m_strCurrentDocFDF As String
Dim m_bFaxDocument As Boolean
Dim m_bPDFCreatorError As Boolean
Dim m_dteLastEmailSent As Date
Dim m_strConvertedDoc As String

Public WithEvents PDFCreator1 As PDFCreator.clsPDFCreator
Attribute PDFCreator1.VB_VarHelpID = -1
Private WithEvents m_cZ As cZip
Attribute m_cZ.VB_VarHelpID = -1
Private WithEvents m_cUnzip As cUnzip
Attribute m_cUnzip.VB_VarHelpID = -1

Private ReadyState As Boolean, DefaultPrinter As String

Private Sub filImage_Error(ByVal Number As Long, ByVal Description As String, ByVal sCode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Integer)
    Trace "        ImageBasic ERROR. Description: " & Description & vbCrLf
    CancelDisplay = True
    Err.Clear
End Sub

Private Sub Form_Load()
10        On Error GoTo errHandler
20        If g_strCommandParms = "" Then
              'Allow the service to accept start/stop and shutdown control events.
              'This must be done before the service is started
30            NTSvc.ControlsAccepted = svcCtrlStartStop + svcCtrlShutdown
              
40            NTSvc.StartService
              
50            m_bPDFCreatorError = False
              
60            Trace "Creating an instance of PDFCreator..."
70            Set PDFCreator1 = CreateObject("PDFCreator.clsPDFCreator")
80            With PDFCreator1
90                If .cStart("/NoProcessingAtStartup") = False Then
100                   If .cStart("/NoProcessingAtStartup", True) = False Then
110                       m_bPDFCreatorError = False
120                   End If
130               End If
140               .cVisible = False
                  
150               .cClearCache
160           End With
              
170           If m_bPDFCreatorError Then
180               notifyAppShutDown "LYNX Document Processor stopped due to error instanciating PDFCreator. Please kill the PDFCreator and start the service."
190               Unload Me
200               End
210           End If
              
220           m_bPDFCreatorError = False
              
230           Trace "Created" & vbCrLf
              
240           tmrSvc.Enabled = True
              
250       End If
errHandler:
260       If Err.Number <> 0 Then
270           Trace "ERROR: [Line #" & Erl & "] Description: " & Err.Description & vbCrLf
280           Unload Me
290           End
300       End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
10        Trace "Unloading application variable..."
20        If Not PDFCreator1 Is Nothing Then
30            PDFCreator1.cClose
40        End If
50        Set PDFCreator1 = Nothing
60        cleanupPDFTools
70        terminateGlobals
80        unloadConfig
          
90        Trace "Done." & vbCrLf
100       Trace "====== Application normal shutdown ======" & vbCrLf & vbCrLf
          
110       Set gobjFSO = Nothing
120       Unload Me
End Sub

Private Sub NTSvc_Continue(Success As Boolean)
10        On Error GoTo errHandler
20        tmrSvc.Enabled = True
          
30        Success = True
          
40        Call NTSvc.LogEvent(svcEventInformation, svcMessageInfo, "Service Continued.")
          
50        Trace NTSvc.DisplayName & " continued." & vbCrLf
          
errHandler:
60        If Err.Number <> 0 Then
70            Success = False
80            tmrSvc.Enabled = False
90        End If
End Sub

Private Sub NTSvc_Pause(Success As Boolean)
10        On Error GoTo errHandler
20        g_blnStopProcessing = True
          
30        Call NTSvc.LogEvent(svcEventInformation, svcMessageInfo, "Service Paused.")
          
40        Trace NTSvc.DisplayName & " paused." & vbCrLf
          
50        Success = True
          
errHandler:
60        If Err.Number <> 0 Then
70            Success = False
80        End If
End Sub

Private Sub NTSvc_Start(Success As Boolean)
10        On Error GoTo errHandler
20        g_blnStopProcessing = False
          
30        tmrSvc.Enabled = True
          
40        Call NTSvc.LogEvent(svcEventInformation, svcMessageInfo, "Service Started.")
          
50        Trace NTSvc.DisplayName & " started." & vbCrLf
          
60        Success = True
          
errHandler:
70        If Err.Number <> 0 Then
80            Success = False
90        End If
End Sub

Private Sub NTSvc_Stop()
10        On Error GoTo errHandler
20        g_blnStopProcessing = True
          
30        tmrSvc.Enabled = False
          
40        Call NTSvc.LogEvent(svcEventInformation, svcMessageInfo, "Service Stopped.")
          
50        Trace NTSvc.DisplayName & " stopped." & vbCrLf
          
60        Unload Me
          
errHandler:
70        If Err.Number <> 0 Then
80            Call NTSvc.LogEvent(svcEventInformation, svcMessageInfo, "Error stopping service.")
90        End If
End Sub

Private Sub tmrJobsRefresh_Timer()
10        reloadQueue
          
20        startProcessing
End Sub

Public Sub startProcessing()
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "startProcessing()"
          Dim oJob As IXMLDOMElement
20        tmrJobsRefresh.Enabled = False
          
30        Trace "   Start Processing jobs. " & g_objJobsQueueRoot.ChildNodes.Length & " jobs found." & vbCrLf
          
40        While g_objJobsQueueRoot.ChildNodes.Length > 0
              'process high priority job
50            Set oJob = g_objJobsQueueRoot.SelectSingleNode("/Jobs/Job[@priority = 'high']")
60            If Not oJob Is Nothing Then
70                processJob oJob
80            Else
                  'no high priority job exists. Now run the normal ones
90                Set oJob = g_objJobsQueueRoot.SelectSingleNode("/Jobs/Job[@priority = 'normal']")
100               If Not oJob Is Nothing Then
110                   processJob oJob
120               Else
                      'no normal priority job exists. Now run the low priority ones
130                   Set oJob = g_objJobsQueueRoot.SelectSingleNode("/Jobs/Job[@priority = 'low']")
140                   If Not oJob Is Nothing Then
150                       processJob oJob
160                   End If
170               End If
180           End If
              
190           Set oJob = Nothing
              
200           If g_blnStopProcessing Then
210               notifyAppShutDown "Service was paused/stopped"
220               Unload Me
230               End
240           End If
              
              'reload the queue. This way we might pick up any new job that came in while we were processing.
250           reloadQueue
260       Wend
          
errHandler:
270       tmrJobsRefresh.Enabled = True
280       If Err.Number <> 0 Then
290           Trace "   ERROR: " & PROC_NAME & " [Line # " & Erl & "] Description: " & Err.Description & vbCrLf
300           tmrJobsRefresh.Enabled = False
310           notifyAppShutDown "Error while start processing.  ERROR: " & PROC_NAME & " [Line # " & Erl & "] Description: " & Err.Description & vbCrLf
320           Unload Me
330           End
340       End If
350       Trace "   Refresh in " & CStr(tmrJobsRefresh.Interval / 1000) & " seconds. Current timestamp:" & Format(Now, "mm/dd/yyyy hh:nn:ss") & vbCrLf
End Sub

Private Sub processJob_old(ByRef oJobNode As IXMLDOMElement)
10        On Error GoTo badJob
          Const PROC_NAME As String = "processJob()"
          Dim oJobXML As MSXML2.DOMDocument
          Dim oPackage As IXMLDOMElement
          Dim oSingleDocuments As IXMLDOMNodeList
          Dim oMergeDocuments As IXMLDOMNodeList
          Dim oPDFMetadata As IXMLDOMElement
          
          Dim oDocument As IXMLDOMElement
          Dim oPDFKey As IXMLDOMElement
          Dim oPDFKeys As IXMLDOMNodeList
          Dim oPDFMetaFile As TextStream
          'Dim oEmailNodes As IXMLDOMNodeList
          'Dim oEmailNode As IXMLDOMElement
          Dim oDocumentNodes As IXMLDOMNodeList
          
          Dim oNotifyNode As IXMLDOMElement
          Dim oBadJobNode As IXMLDOMElement
          Dim oNotesMsg As IXMLDOMElement
          
          Dim pdfErr As PDFCreator.clsPDFCreatorError
          
          Dim strOutputType As String
          
          Dim strJobFileName As String
          Dim strValidationMsg As String
          
          Dim strXSLPath As String
          Dim strFDFPath As String
          Dim strHTMLOptions As String
          
          Dim aFaxDocuments() As String
          Dim aDocuments() As String
          Dim iCurDocIdx As Integer
          Dim strConvertedDoc As String
          Dim bNoConvert As Boolean
          Dim bZipArchive As Boolean
          
          Dim aMergeDocs() As String
          Dim strMergedDoc As String
          Dim strOutputFileName As String
          Dim strOutputFileNameTmp As String
      '    Dim strOutputFileType As String
          
          Dim bJobSuccess As Boolean
          Dim bBadJob As Boolean
          Dim strBadJobReason As String
          Dim strJobID As String
          Dim iDupCount As Integer
          Dim j As Integer
          
          Dim strProcessedFile As String
          
20        strJobFileName = oJobNode.getAttribute("file")
30        strJobID = oJobNode.getAttribute("id")
40        g_strJobID = strJobID
50        bBadJob = False
60        strBadJobReason = ""
70        g_strDocumentsSent = ""
80        g_strConvertErrMsg = ""
          
90        Trace "   ****************************************" & vbCrLf & "   Clearing all previous files..."
          
100       On Error Resume Next
          'clear local prefetched files
110       If Dir(gobjFSO.BuildPath(g_strSortTempPath, "*.*")) <> "" Then
120           Kill gobjFSO.BuildPath(g_strSortTempPath, "*.*")
130       End If
          
140       If Err.Number <> 0 Then
150           Trace "      Error: " & Err.Description & vbCrLf
160           Err.Clear
170       End If
          
          'clear the PDF Creator jobs (if any)
180       If Not PDFCreator1 Is Nothing Then
190           PDFCreator1.cClearCache
200           If Err.Number <> 0 Or PDFCreator1.cError.Number <> 0 Then
210               Set pdfErr = PDFCreator1.cError
220               Trace "      PDFCreator ERROR: " & pdfErr.Description & vbCrLf
230               Err.Clear
240               PDFCreator1.cErrorClear
250               Set pdfErr = Nothing
260           End If
270       End If
          
          
280       On Error GoTo badJob
          
290       Trace "Done." & vbCrLf
          
          'check if the job file exists in the physical queue.
300       If gobjFSO.FileExists(strJobFileName) Then
               'mark job as being processed
310            oJobNode.setAttribute "status", "Processing"
320            g_objJobsQueueXML.Save g_strJobQueue
               
330            Trace "   Loading job file: " & strJobFileName
               
               'load the Job XML
340            Set oJobXML = loadXMLFile(strJobFileName)
               
350            Trace " ...Done." & vbCrLf
               
360            Set oPackage = oJobXML.SelectSingleNode("/Job/Package")
370            Set oSingleDocuments = oJobXML.SelectNodes("/Job/Document[@merge = 'false' or @merge = 'f' or @merge='0' or @merge='no' or @merge='n']")
380            Set oMergeDocuments = oJobXML.SelectNodes("/Job/Document[@merge = '' or @merge = 'true' or @merge = 't' or @merge = '1' or @merge = 'yes' or @merge = 'y']")
              
390            ReDim aFaxDocuments(oSingleDocuments.Length + oMergeDocuments.Length)
400            ReDim aDocuments(oSingleDocuments.Length + oMergeDocuments.Length)
410            ReDim aMergeDocs(oSingleDocuments.Length + oMergeDocuments.Length)
               
420            If oPackage.SelectSingleNode("Fax") Is Nothing Then
430                m_bFaxDocument = False
440            Else
450                m_bFaxDocument = True
460            End If
               
470            strOutputType = IIf(IsNull(oPackage.getAttribute("type")), "pdf", oPackage.getAttribute("type"))
480            strMergedDoc = IIf(IsNull(oPackage.getAttribute("outputfile")), "", oPackage.getAttribute("outputfile"))
               
              '        'if the only child ia a fax and the output is not tif then override the output type to tif.
              '        ' This way we will not unnecessarily convert to non tif and then convert again for fax
              '        If oPackage.childNodes.Length = 1 Then
              '            strOutputType = "tif"
              '        End If
               
490            If InStr(1, strOutputType, "zip") > 0 Then bZipArchive = True
               
500            Select Case strOutputType
                   Case "pdf-zip": strOutputType = "pdf"
510                Case "tif-zip": strOutputType = "tif"
520            End Select
               
530            Trace "    Job info" & vbCrLf & _
                     "       # of documents to merge: " & oMergeDocuments.Length & vbCrLf & _
                     "       # of single document: " & oSingleDocuments.Length & vbCrLf & _
                     "       Need fax functionality: " & m_bFaxDocument & vbCrLf
               
540            If isOutputTypeSupported(strOutputType) = False Then
                   'unsupported output type. mark as bad job
550                bBadJob = True
560                strBadJobReason = "Unsupported Package output type '" & strOutputType & "'. Code Line# " & Erl
                   
570                Trace "    Unsupported Package output type " & strOutputType & vbCrLf
580                GoTo badJob
590            End If
               
               'write the PDFMetadata file
              
600            g_strPDFMetaFile = gobjFSO.BuildPath(g_strSortTempPath, "pdfmeta.txt")
610            Set oPDFMetadata = oPackage.SelectSingleNode("PDFMetadata")
620            If Not oPDFMetadata Is Nothing Then
630                Set oPDFKeys = oPDFMetadata.ChildNodes
640                Set oPDFMetaFile = gobjFSO.OpenTextFile(g_strPDFMetaFile, ForWriting, True)
650                For Each oPDFKey In oPDFKeys
660                    oPDFMetaFile.WriteLine "InfoKey: " & oPDFKey.nodeName
670                    oPDFMetaFile.WriteLine "InfoValue: " & oPDFKey.Text
680                Next
                   
                   'check if the package contained metadata for the Author/Creator/Producer. If not, then use the
                   ' values from the configuration.
690                If oPDFMetadata.SelectSingleNode("Author") Is Nothing Then
700                    oPDFMetaFile.WriteLine "InfoKey: Author"
710                    oPDFMetaFile.WriteLine "InfoValue: " & g_strPDFAuthor
720                End If
                   
730                If oPDFMetadata.SelectSingleNode("Creator") Is Nothing Then
740                    oPDFMetaFile.WriteLine "InfoKey: Creator"
750                    oPDFMetaFile.WriteLine "InfoValue: " & g_strPDFCreator
760                End If
                   
770                If oPDFMetadata.SelectSingleNode("Producer") Is Nothing Then
780                    oPDFMetaFile.WriteLine "InfoKey: Producer"
790                    oPDFMetaFile.WriteLine "InfoValue: " & g_strPDFProducer
800                End If
810                oPDFMetaFile.Close
820                Set oPDFMetaFile = Nothing
830                Set oPDFKeys = Nothing
840                Set oPDFKey = Nothing
850            Else
860                Set oPDFMetaFile = gobjFSO.OpenTextFile(g_strPDFMetaFile, ForWriting, True)
870                oPDFMetaFile.WriteLine "InfoKey: Author"
880                oPDFMetaFile.WriteLine "InfoValue: " & g_strPDFAuthor
890                oPDFMetaFile.WriteLine "InfoKey: Creator"
900                oPDFMetaFile.WriteLine "InfoValue: " & g_strPDFCreator
910                oPDFMetaFile.WriteLine "InfoKey: Producer"
920                oPDFMetaFile.WriteLine "InfoValue: " & g_strPDFProducer
930                oPDFMetaFile.Close
940                Set oPDFMetaFile = Nothing
950            End If
               
               
960            iCurDocIdx = 0
               
970            Trace "      Processing the single documents..." & vbCrLf
               
               'process single documents
980            For Each oDocument In oSingleDocuments
990                m_strCurrentDoc = ""
1000               m_strCurrentDocXSL = ""
1010               m_strCurrentDocFDF = ""
1020               m_strConvertedDoc = ""
1030               iDupCount = 1
1040               g_blnCoversheet = False
                   
1050               If oDocument.getAttribute("noconvert") = "true" Then
1060                   bNoConvert = True
1070               Else
1080                   bNoConvert = False
1090               End If
                   
1100               strOutputFileName = IIf(IsNull(oDocument.getAttribute("outputfilename")), "", oDocument.getAttribute("outputfilename"))
              '            strOutputFileType = IIf(IsNull(oDocument.getAttribute("outputformat")), "", oDocument.getAttribute("outputformat"))
                   
1110               If InStr(1, strOutputFileName, "__coversheet") > 0 Then
1120                   g_blnCoversheet = True
1130                   Trace "      Processing coversheet..." & vbCrLf
1140               Else
1150                   g_blnCoversheet = False
1160               End If
                   
1170               strValidationMsg = validateandFetchDocument(oDocument)
1180               If strValidationMsg <> "" Then
                       'bad job. something wrong. send to badjob queue
1190                   bBadJob = True
1200                   strBadJobReason = strValidationMsg & ". Code Line# " & Erl
1210                   Trace "Failed. Reason:" & strValidationMsg & vbCrLf
1220                   GoTo badJob
1230               Else
1240                   If bNoConvert = False And (gobjFSO.GetExtensionName(m_strCurrentDoc) <> strOutputType Or m_strCurrentDocFDF <> "") Then
                           'current document is not of the output format type. Convert it.
1250                       strConvertedDoc = convertDocument(m_strCurrentDoc, m_strCurrentDocXSL, m_strCurrentDocFDF, "", strOutputType, False)
1260                       If strConvertedDoc = "" Then
              '                        If m_strCurrentDocFDF = "" Then
              '                            strConvertedDoc = gobjFSO.BuildPath(g_strSortTempPath, gobjFSO.GetBaseName(m_strCurrentDoc) & "." & strOutputType)
              '                        Else
              '                            strConvertedDoc = m_strConvertedDoc 'gobjFSO.BuildPath(g_strSortTempPath, gobjFSO.GetBaseName(m_strCurrentDoc) & "_filled." & strOutputType)
              '                        End If
              '                    Else
                               'conversion error. mark as bad job
1270                           bBadJob = True
1280                           strBadJobReason = "Document conversion error. Reason: " & g_strConvertErrMsg & ". Code Line# " & Erl
1290                           Trace "      Document conversion error. Reason: " & g_strConvertErrMsg & vbCrLf
1300                           GoTo badJob
1310                       End If
1320                   Else
1330                       If gobjFSO.GetExtensionName(m_strCurrentDoc) = "pdf" Then
                               'check if current file is a valid pdf
1340                       End If
                           
              '                    If (strOutputFileType = "tif" Or _
              '                        strOutputFileType = "pdf" Or _
              '                        strOutputFileType = "jpg") Then
              '                        If (gobjFSO.GetExtensionName(m_strCurrentDoc) <> strOutputFileType) Then
              '                            strConvertedDoc = convertDocument(m_strCurrentDoc, m_strCurrentDocXSL, m_strCurrentDocFDF, "", strOutputFileType, False)
              '                            If Join(strConvertedDoc, "|") <> "" Then m_strCurrentDoc = strOutputFileNameTmp
              '                        End If
              '                    End If
                           
1350                       Select Case gobjFSO.GetExtensionName(m_strCurrentDoc)
                               Case "jpg", "jpeg", "bmp", "gif"
1360                               ReduceImageSize m_strCurrentDoc
                                   'Reduceimage Size converts the image to jpg format. so lets start using it
1370                               If gobjFSO.FileExists(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(m_strCurrentDoc), gobjFSO.GetBaseName(m_strCurrentDoc) & ".jpg")) Then
1380                                   m_strCurrentDoc = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(m_strCurrentDoc), gobjFSO.GetBaseName(m_strCurrentDoc) & ".jpg")
1390                               End If
1400                       End Select
                           
1410                       strConvertedDoc = m_strCurrentDoc
1420                   End If
              
                       
1430                   If gobjFSO.FileExists(strConvertedDoc) Then
1440                       If strOutputFileName <> "" Then
                               'clean the file name of any illegal chars
1450                           strOutputFileName = cleanFileName(strOutputFileName)
1460                           strOutputFileNameTmp = strOutputFileName
                               
                               'find unique file name. Duplicates will be marked as (1), (2)...
1470                           While gobjFSO.FileExists(gobjFSO.BuildPath(g_strSortTempPath, gobjFSO.GetBaseName(strOutputFileNameTmp) & "." & gobjFSO.GetExtensionName(strConvertedDoc)))
1480                               strOutputFileNameTmp = gobjFSO.GetBaseName(strOutputFileName) & " (" & CStr(iDupCount) & ")" & "." & gobjFSO.GetExtensionName(strOutputFileNameTmp)
1490                               iDupCount = iDupCount + 1
1500                           Wend
                               
1510                           strOutputFileName = gobjFSO.BuildPath(g_strSortTempPath, gobjFSO.GetBaseName(strOutputFileNameTmp) & "." & gobjFSO.GetExtensionName(strConvertedDoc))
                               
1520                           Trace "      Copying file " & strConvertedDoc & " to " & strOutputFileName
1530                           gobjFSO.CopyFile strConvertedDoc, strOutputFileName
                               
1540                           Trace " Done." & vbCrLf
1550                           strConvertedDoc = gobjFSO.BuildPath(g_strSortTempPath, gobjFSO.GetBaseName(strOutputFileName) & "." & gobjFSO.GetExtensionName(strConvertedDoc))
                               
1560                           oDocument.setAttribute "processedFile", strConvertedDoc
1570                       End If
1580                       aDocuments(iCurDocIdx) = strConvertedDoc
1590                   Else
                           'converted document missing. raise error
1600                       bBadJob = True
1610                       strBadJobReason = "Missing the converted document: " & strConvertedDoc & ". Code Line# " & Erl
1620                       Trace "      Missing converted document " & strConvertedDoc & vbCrLf
1630                       GoTo badJob
1640                   End If
              
1650                   If m_bFaxDocument Then
1660                       If gobjFSO.GetExtensionName(m_strCurrentDoc) <> "tif" Then
1670                           If convertedFileExists(m_strCurrentDoc, "tif") = "" Then
              '                        If strOutputType <> "tif" Then 'it is a pdf output. now convert the pdf to tif
                                   'convert the current document to tif
                                   'reducing the image size happens when converting
              '                            Select Case gobjFSO.GetExtensionName(m_strCurrentDoc)
              '                                Case "jpg", "jpeg", "bmp", "gif"
              '                                    ReduceImageSize m_strCurrentDoc
              '                                    'Reduceimage Size converts the image to jpg format. so lets start using it
              '                                    If gobjFSO.FileExists(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(m_strCurrentDoc), gobjFSO.GetBaseName(m_strCurrentDoc) & ".jpg")) Then
              '                                        m_strCurrentDoc = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(m_strCurrentDoc), gobjFSO.GetBaseName(m_strCurrentDoc) & ".jpg")
              '                                    End If
              '                            End Select
                                   
1680                               strConvertedDoc = convertDocument(m_strCurrentDoc, m_strCurrentDocXSL, m_strCurrentDocFDF, strHTMLOptions, "tif", False)
1690                               If strConvertedDoc <> "" Then
                                       'strConvertedDoc = gobjFSO.BuildPath(g_strSortTempPath, gobjFSO.GetBaseName(m_strCurrentDoc) & ".tif")
1700                                   If gobjFSO.FileExists(strConvertedDoc) Then
1710                                       If InStr(1, strOutputFileName, "__coversheet") > 0 Then
                                               'Fax coversheet must always be the first document. Rearrange the list to do so.
1720                                           For j = UBound(aFaxDocuments) To (LBound(aFaxDocuments) + 1) Step -1
1730                                               aFaxDocuments(j) = aFaxDocuments(j - 1)
1740                                           Next
1750                                           aFaxDocuments(0) = strConvertedDoc
1760                                       Else
1770                                           aFaxDocuments(iCurDocIdx) = strConvertedDoc
1780                                       End If
1790                                   Else
                                           'converted fax document missing. raise error
1800                                       bBadJob = True
1810                                       strBadJobReason = "Missing the converted fax document: " & strConvertedDoc & ". Code Line# " & Erl
1820                                       Trace "      Missing the converted fax document " & strConvertedDoc & vbCrLf
1830                                       GoTo badJob
1840                                   End If
1850                               Else
                                       'conversion error. mark as bad job
1860                                   bBadJob = True
1870                                   strBadJobReason = "Document conversion error. Reason: " & g_strConvertErrMsg & ". Code Line# " & Erl
1880                                   Trace "      Document conversion error. Reason: " & g_strConvertErrMsg & vbCrLf
1890                                   GoTo badJob
1900                               End If
1910                           Else
1920                               aFaxDocuments(iCurDocIdx) = convertedFileExists(m_strCurrentDoc, "tif")
1930                           End If
1940                       Else
1950                           aFaxDocuments(iCurDocIdx) = m_strCurrentDoc
1960                       End If
                       
1970                   End If
1980               End If
              
1990               DoEvents
              
2000               iCurDocIdx = iCurDocIdx + 1
2010           Next
                
2020           Trace "      Processing the merge documents..." & vbCrLf
               
               'process documents that need to be merged
2030           For Each oDocument In oMergeDocuments
2040               m_strCurrentDoc = ""
2050               m_strCurrentDocXSL = ""
2060               m_strCurrentDocFDF = ""
2070               m_strConvertedDoc = ""
2080               bNoConvert = True 'merge documents will always be converted
2090               strValidationMsg = validateandFetchDocument(oDocument)
2100               If strValidationMsg <> "" Then
                       'bad job. something wrong. send to badjob queue
2110                   bBadJob = True
2120                   strBadJobReason = strValidationMsg & " Code Line# " & Erl
                       
2130                   Trace "Failed. Reason:" & strValidationMsg & vbCrLf
2140                   GoTo badJob
2150               Else
2160                   If gobjFSO.GetExtensionName(m_strCurrentDoc) <> strOutputType Or m_strCurrentDocFDF <> "" Then
                           'current document is not of the output format type. Convert it.
2170                       strConvertedDoc = convertDocument(m_strCurrentDoc, m_strCurrentDocXSL, m_strCurrentDocFDF, "", strOutputType, False)
2180                       If strConvertedDoc = "" Then
              '                        If m_strCurrentDocFDF = "" Then
              '                            strConvertedDoc = m_strConvertedDoc 'gobjFSO.BuildPath(g_strSortTempPath, gobjFSO.GetBaseName(m_strCurrentDoc) & "." & strOutputType)
              '                        Else
              '                            strConvertedDoc = gobjFSO.BuildPath(g_strSortTempPath, gobjFSO.GetBaseName(m_strCurrentDoc) & "_filled." & strOutputType)
              '                        End If
              '                    Else
                               'conversion error. mark as bad job
2190                           bBadJob = True
2200                           strBadJobReason = "Document conversion error. Reason: " & g_strConvertErrMsg & ". Code Line# " & Erl
2210                           Trace "      Conversion error. Reason: " & g_strConvertErrMsg & vbCrLf
2220                           GoTo badJob
2230                       End If
2240                   Else
2250                       If gobjFSO.GetExtensionName(m_strCurrentDoc) = "pdf" Then
                               'check if current file is a valid pdf
2260                       End If
                           
2270                       Select Case gobjFSO.GetExtensionName(m_strCurrentDoc)
                               Case "jpg", "jpeg", "bmp", "gif"
2280                               ReduceImageSize m_strCurrentDoc
                                   'Reduceimage Size converts the image to jpg format. so lets start using it
2290                               If gobjFSO.FileExists(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(m_strCurrentDoc), gobjFSO.GetBaseName(m_strCurrentDoc) & ".jpg")) Then
2300                                   m_strCurrentDoc = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(m_strCurrentDoc), gobjFSO.GetBaseName(m_strCurrentDoc) & ".jpg")
2310                               End If
2320                       End Select
                           
2330                       strConvertedDoc = m_strCurrentDoc
2340                   End If
                       
2350                   If gobjFSO.FileExists(strConvertedDoc) Then
2360                       aMergeDocs(iCurDocIdx) = strConvertedDoc
2370                       oDocument.setAttribute "processedFile", strConvertedDoc
2380                   Else
                           'converted document missing. raise error
2390                       bBadJob = True
2400                       strBadJobReason = "Missing the converted document: " & strConvertedDoc & ". Code Line# " & Erl
2410                       Trace "      Missing converted document " & strConvertedDoc & vbCrLf
2420                       GoTo badJob
2430                   End If
                       
2440                   If m_bFaxDocument Then
              '                    If strOutputType <> "tif" Then 'it is a pdf output. now convert the pdf to tif
2450                           If gobjFSO.GetExtensionName(m_strCurrentDoc) <> "tif" Then
2460                               If convertedFileExists(m_strCurrentDoc, "tif") = "" Then
                                       'reducing the image size happens when converting
              '                                Select Case gobjFSO.GetExtensionName(m_strCurrentDoc)
              '                                    Case "jpg", "jpeg", "bmp", "gif"
              '                                        ReduceImageSize m_strCurrentDoc
              '                                        'Reduceimage Size converts the image to jpg format. so lets start using it
              '                                        If gobjFSO.FileExists(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(m_strCurrentDoc), gobjFSO.GetBaseName(m_strCurrentDoc) & ".jpg")) Then
              '                                            m_strCurrentDoc = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(m_strCurrentDoc), gobjFSO.GetBaseName(m_strCurrentDoc) & ".jpg")
              '                                        End If
              '                                End Select
                                       'convert the current document to tif
2470                                   strConvertedDoc = convertDocument(m_strCurrentDoc, m_strCurrentDocXSL, m_strCurrentDocFDF, "", "tif", False)
2480                                   If strConvertedDoc <> "" Then
                                           'strConvertedDoc = gobjFSO.BuildPath(g_strSortTempPath, gobjFSO.GetBaseName(m_strCurrentDoc) & ".tif")
2490                                       If gobjFSO.FileExists(strConvertedDoc) Then
2500                                           aFaxDocuments(iCurDocIdx) = strConvertedDoc
2510                                       Else
                                               'converted fax document missing. raise error
2520                                           bBadJob = True
2530                                           strBadJobReason = "Missing the converted fax document: " & strConvertedDoc & ". Code Line# " & Erl
2540                                           Trace "      Missing the converted fax document " & strConvertedDoc & vbCrLf
2550                                           GoTo badJob
2560                                       End If
2570                                   Else
                                           'conversion error. mark as bad job
2580                                       bBadJob = True
2590                                       strBadJobReason = "Document conversion error. Reason: " & g_strConvertErrMsg & ". Code Line# " & Erl
2600                                       Trace "      Document conversion error. Reason: " & g_strConvertErrMsg & vbCrLf
2610                                       GoTo badJob
2620                                   End If
2630                               Else
2640                                   aFaxDocuments(iCurDocIdx) = convertedFileExists(m_strCurrentDoc, "tif")
2650                               End If
2660                           Else
2670                               aFaxDocuments(iCurDocIdx) = m_strCurrentDoc
2680                           End If
              '                    Else
              '                        aFaxDocuments(iCurDocIdx) = strConvertedDoc
              '                    End If
2690                   End If
2700               End If
                   
2710               DoEvents
                   
2720               iCurDocIdx = iCurDocIdx + 1
2730           Next
               
               'merge the Documents
2740           If Join(aMergeDocs, "") <> "" Then
                   'there are document to merge
2750               If strMergedDoc = "" Then
2760                   strMergedDoc = gobjFSO.GetBaseName(gobjFSO.GetTempName()) & "." & strOutputType
2770               End If
                   
                   'clean the file name of any illegal chars
2780               strMergedDoc = cleanFileName(strMergedDoc)
                   
2790               DoEvents
                   'sleep for 2 seconds and let the system catch up with other stuff
2800               Sleep 2000
                   
2810               If strOutputType = "pdf" Then
2820                   If mergePDFs(aMergeDocs, strMergedDoc, g_strPDFMetaFile) Then
2830                       ReDim aMergeDocs(0)
2840                       If gobjFSO.GetParentFolderName(strMergedDoc) = "" Then
2850                           aMergeDocs(0) = gobjFSO.BuildPath(g_strSortTempPath, strMergedDoc)
2860                           If gobjFSO.GetExtensionName(strMergedDoc) = "" Then
2870                               aMergeDocs(0) = aMergeDocs(0) & ".pdf"
2880                           End If
2890                       Else
2900                           aMergeDocs(0) = strMergedDoc
2910                           If gobjFSO.GetExtensionName(strMergedDoc) = "" Then
2920                               aMergeDocs(0) = aMergeDocs(0) & ".pdf"
2930                           End If
2940                       End If
2950                   Else
                           'raise error
2960                       bBadJob = True
2970                       strBadJobReason = "Missing merged PDF document."
2980                       Trace "      Missing merged PDF document" & vbCrLf
2990                       GoTo badJob
3000                   End If
3010               ElseIf strOutputType = "tif" Then
3020                   If mergeTifs(aMergeDocs, strMergedDoc) Then
3030                       ReDim aMergeDocs(0)
3040                       If gobjFSO.GetParentFolderName(strMergedDoc) = "" Then
3050                           aMergeDocs(0) = gobjFSO.BuildPath(g_strSortTempPath, strMergedDoc)
3060                       Else
3070                           aMergeDocs(0) = strMergedDoc
3080                       End If
                           'aMergeDocs(0) = strMergedDoc 'gobjFSO.BuildPath(g_strSortTempPath, strMergedDoc)
3090                   Else
                           'raise error
3100                       bBadJob = True
3110                       strBadJobReason = "Missing merged tif document" & ". Code Line# " & Erl
3120                       Trace "      Missing merged PDF document" & vbCrLf
3130                       GoTo badJob
3140                   End If
3150               End If
3160           End If
               
               'iCurDocIdx = 0
               
              
               
               'merge Tif Documents for fax
3170           If m_bFaxDocument Then
                   
3180               DoEvents
                   'sleep for 2 seconds and let the system catch up with other stuff
3190               Sleep 2000
                   
3200               If mergeTifs(aFaxDocuments, "MergeFax.tif") Then
3210                   ReDim aFaxDocuments(0)
3220                   aFaxDocuments(0) = gobjFSO.BuildPath(g_strSortTempPath, "MergeFax.tif")
3230               Else
                       'raise error
3240                   bBadJob = True
3250                   strBadJobReason = "Missing merged tif document. Code Line# " & Erl
3260                   Trace "      Missing merged tif document." & vbCrLf
3270                   GoTo badJob
3280               End If
3290           End If
               
               'allow the merging files to take effect in the system. Sleep for a second.
3300           DoEvents
3310           Sleep 1000
               
3320           If g_strLastJobID = strJobID And DateDiff("m", g_dteLastDestinationProcessed, Now) < 1 Then
                   'something got messed up. Shut down the app
3330               Trace vbCrLf & "SOMETHING BAD HAPPENED AND THE APPLICATION IS PROCESSING THE SAME JOB OVER AND OVER AGAIN AND PROCESSING DESTINATIONS. QUITING THE APPLICATION" & vbCrLf
3340               notifyAppShutDown "Same job being processed over and over again. Shutting down the service to prevent spamming."
3350               Unload Me
3360               End
3370           Else
                   'process destinations
                   
3380               Set oDocumentNodes = oJobXML.SelectNodes("/Job/Document")
3390               bJobSuccess = processDestinations(oPackage, oDocumentNodes, aDocuments, aMergeDocs, aFaxDocuments)
3400               g_dteLastDestinationProcessed = Now
                   
3410           End If
               
3420           If bJobSuccess = False Then bBadJob = True
               
3430      Else
               'job file does not exist. remove from the jobs xml
3440           g_objJobsQueueRoot.RemoveChild oJobNode
3450           g_objJobsQueueXML.Save g_strJobQueue
               
3460           g_strConvertErrMsg = "missing job file in queue"
        
3470      End If
          
badJob:
          
3480      If bBadJob Then
3490          bJobSuccess = False
        
3500          If Err.Number <> 0 Or g_strDestinationErrMsg <> "" Or g_strConvertErrMsg <> "" Then
3510              If Err.Number <> 0 Then
3520                  strBadJobReason = strBadJobReason & vbCrLf & "      ERROR: " & PROC_NAME & " [Line #" & Erl & "] Description: " & Err.Description
3530              End If
                  
3540              strBadJobReason = strBadJobReason & "        Conversion Message: " & g_strConvertErrMsg & vbCrLf & _
                                      "        Destination Message: " & g_strDestinationErrMsg & vbCrLf
                  
3550              Trace "      " & strBadJobReason & vbCrLf
                  Dim aAttachments(0) As String
3560              sendEmail g_strDefaultEmailFrom, g_strAppErrorTo, "", "", "", "ERROR: LYNX Document Processor [" & g_sComputerName & "]", "high", _
                              strBadJobReason, aAttachments
3570          End If
              
3580          Trace "      Marking the job as bad. Reason: " & strBadJobReason & vbCrLf
              
              'mark the reason in the job
3590          Set oBadJobNode = oJobXML.SelectSingleNode("/Job/BadJob")
3600          If oBadJobNode Is Nothing Then
3610              Set oBadJobNode = oJobXML.createElement("BadJob")
3620              oBadJobNode.appendChild oJobXML.createCDATASection(Format(Now, "mm/dd/yyyy hh:nn:ss") & " " & strBadJobReason)
3630              oJobXML.FirstChild.appendChild oBadJobNode
3640          Else
3650              oBadJobNode.Text = oBadJobNode.Text & vbCrLf & Format(Now, "mm/dd/yyyy hh:nn:ss") & " " & strBadJobReason
3660          End If
        
3670          oJobXML.Save gobjFSO.BuildPath(g_strBadJobPath, gobjFSO.GetBaseName(strJobFileName) & ".xml")
        
        '4/15/08 - let the job file remain in the pickup folder itself so the support staff need to
        'move the job file only.
      '        'move the job attachments to the bad job folder
      '        strOutputFileName = gobjFSO.BuildPath(g_strPickupPath, "*" & strJobID & "*.*")
      '        strOutputFileNameTmp = Dir(strOutputFileName)
      '        While strOutputFileNameTmp <> ""
      '            Trace "        Moving attachment " & strOutputFileNameTmp & " to " & g_strBadJobPath & " ... "
      '            gobjFSO.MoveFile gobjFSO.BuildPath(g_strPickupPath, strOutputFileNameTmp), _
      '                             gobjFSO.BuildPath(g_strBadJobPath, strOutputFileNameTmp)
      '            Trace "Done." & vbCrLf
      '            strOutputFileNameTmp = Dir()
      '        Wend
        '4/15/08 - End of change
        
3680          Trace "      Deleting the job file..."
              'delete job file
3690          gobjFSO.DeleteFile strJobFileName, True
              
3700          Trace "Done." & vbCrLf
              
3710          Trace "      Removing job from queue..."
              
              'remove job from the queue
3720          g_objJobsQueueRoot.RemoveChild oJobNode
3730          g_objJobsQueueXML.Save g_strJobQueue
              
3740          Trace "Done." & vbCrLf
              
3750          g_strConvertErrMsg = strBadJobReason
3760      End If
          
3770      If bJobSuccess Then
3780          Trace "   Job successfully processed." & vbCrLf
              'remove job from the queue
3790          g_objJobsQueueRoot.RemoveChild oJobNode
3800          g_objJobsQueueXML.Save g_strJobQueue
              
              'we are done with the job. Now it is safe to delete the job as well as the attachments
              'job successfully processed. Delete from queue
3810          gobjFSO.DeleteFile strJobFileName, True
                
              'delete the attachments now.
3820          If strJobID <> "" Then
                  'delete the files in the pickup folder for this job
3830              If Dir(gobjFSO.BuildPath(g_strPickupPath, strJobID & "*.*")) <> "" Then
3840                  Trace vbCrLf & "      Deleting job attachments..."
3850                  Kill gobjFSO.BuildPath(g_strPickupPath, strJobID & "*.*")
3860                  Trace vbCrLf & "Done." & vbCrLf
3870              End If
3880          End If
                  
              'check if we need to manually clean up the processed file.
3890          Set oNotesMsg = oPackage.SelectSingleNode(".//NotesMsg")
3900          If Not oNotesMsg Is Nothing Then
                  'we need to manually delete the processed files
3910              Set oDocumentNodes = oJobXML.SelectNodes("/Job/Document")
3920              On Error Resume Next
                  
3930              If Dir(gobjFSO.BuildPath(g_strOutboxPath2External, strJobID & "*.*")) <> "" Then Kill gobjFSO.BuildPath(g_strOutboxPath2External, strJobID & "*.*")
3940          End If
3950      End If
          
          'notify job status
3960      If Not oPackage Is Nothing Then
3970          If g_strLastJobID = strJobID And DateDiff("m", g_dteLastNotify, Now) < 1 Then
                  'something got messed up and the service is running out of control.
3980              Trace vbCrLf & "SOMETHING BAD HAPPENED AND THE APPLICATION IS PROCESSING THE SAME JOB OVER AND OVER AGAIN AND NOTIFYING USER. QUITING THE APPLICATION" & vbCrLf
3990              notifyAppShutDown "Same job is being processed over and over again. Shutting down the service to prevent spamming."
4000              Unload Me
4010              End
4020          Else
4030              notifyJobStatus oPackage, bJobSuccess
4040          End If
4050      End If
          
          'writeJournal oJobNode.getAttribute("id"), oJobNode.getAttribute("description"), bJobSuccess
          
4060      g_strLastJobID = strJobID
          
4070      Trace vbCrLf

4080      Set oJobXML = Nothing
4090      Set oPackage = Nothing
4100      Set oSingleDocuments = Nothing
4110      Set oMergeDocuments = Nothing
4120      Set oDocument = Nothing
End Sub

Private Sub processJob(ByRef oJobNode As IXMLDOMElement)
10        On Error GoTo badJob
          Const PROC_NAME As String = "processJob()"
          Dim oJobXML As MSXML2.DOMDocument
          Dim oPackage As IXMLDOMElement
          'Dim oSingleDocuments As IXMLDOMNodeList
          'Dim oMergeDocuments As IXMLDOMNodeList
          Dim oPDFMetadata As IXMLDOMElement
          Dim oDocumentNodes As IXMLDOMNodeList
    
          Dim oDocument As IXMLDOMElement
          Dim oPDFKey As IXMLDOMElement
          Dim oPDFKeys As IXMLDOMNodeList
          Dim oPDFMetaFile As TextStream
          'Dim oEmailNodes As IXMLDOMNodeList
          'Dim oEmailNode As IXMLDOMElement
    
          Dim oNotifyNode As IXMLDOMElement
          Dim oBadJobNode As IXMLDOMElement
          Dim oNotesMsg As IXMLDOMElement
    
          Dim pdfErr As PDFCreator.clsPDFCreatorError
    
          Dim strOutputType As String
    
          Dim strJobFileName As String
          Dim strValidationMsg As String
    
          Dim strXSLPath As String
          Dim strFDFPath As String
          Dim strHTMLOptions As String
    
          Dim aFaxDocuments() As String
          Dim aDocuments() As String
          Dim aPDFDocuments() As String
          Dim aTIFDocuments() As String
          Dim iCurDocIdx As Integer
          Dim strConvertedDoc As String
          Dim bNoConvert As Boolean
          Dim bMerge As Boolean
          Dim bZipArchive As Boolean
    
          Dim aMergeDocs() As String
          Dim strMergedDoc As String
    
          Dim strOutputName As String
          Dim strOutputFileName As String
          Dim strOutputFileNameTmp As String
          Dim strOutputFileNameFax As String
          Dim strJobPDFFile As String
          Dim strJobTIFFile As String
      '    Dim strOutputFileType As String
    
          Dim bJobSuccess As Boolean
          Dim bBadJob As Boolean
          Dim strBadJobReason As String
          Dim strJobID As String
          Dim iDupCount As Integer
          Dim j As Integer
          Dim iDocCount As Integer
    
          Dim strProcessedFile As String
          Dim lngStart As Long
          Dim strArchivePath As String
          Dim lngJobStartTick As Long
    
20        lngJobStartTick = GetTickCount()
30        strJobFileName = oJobNode.getAttribute("file")
40        strJobID = oJobNode.getAttribute("id")
50        g_strJobID = strJobID
60        bBadJob = False
70        strBadJobReason = ""
80        g_strDocumentsSent = ""
90        g_strConvertErrMsg = ""
    
100       Trace "   ****************************************" & vbCrLf & _
                "   Job Started at " & Format(Now, "hh:nn:ss") & "" & vbCrLf & _
                "   Clearing all previous files..."
    
110       On Error Resume Next
          'clear local prefetched files
120       If Dir(gobjFSO.BuildPath(g_strSortTempPath, "*.*")) <> "" Then
130           Kill gobjFSO.BuildPath(g_strSortTempPath, "*.*")
140       End If
    
150       If Err.Number <> 0 Then
160           Trace "      Error: " & Err.Description & vbCrLf
170           Err.Clear
180       End If
    
190       Trace "Done." & vbCrLf
    
          'clear the PDF Creator jobs (if any)
      '    If Not PDFCreator1 Is Nothing Then
      '        Trace "      Clearing PDFCreator cache..."
      '        PDFCreator1.cClearCache
      '        If Err.Number <> 0 Or PDFCreator1.cError.Number <> 0 Then
      '            Set pdfErr = PDFCreator1.cError
      '            Trace " ERROR: " & pdfErr.Description & vbCrLf
      '            Err.Clear
      '            PDFCreator1.cErrorClear
      '            Set pdfErr = Nothing
      '        Else
      '            Trace "Done." & vbCrLf
      '        End If
      '    End If
    
    
200       On Error GoTo badJob
    
    
          'check if the job file exists in the physical queue.
210       If gobjFSO.FileExists(strJobFileName) Then
               'mark job as being processed
220            oJobNode.setAttribute "status", "Processing"
230            g_objJobsQueueXML.Save g_strJobQueue
   
               'copy the job file to the sorttemp folder for archiving it later
240            gobjFSO.CopyFile strJobFileName, gobjFSO.BuildPath(g_strSortTempPath, gobjFSO.GetBaseName(strJobFileName) & ".xml"), False
   
250            Trace "   Loading job file: " & strJobFileName
   
               'load the Job XML
260            Set oJobXML = loadXMLFile(strJobFileName)
   
270            Trace " ...Done." & vbCrLf
   
280            Set oPackage = oJobXML.SelectSingleNode("/Job/Package")
               'Set oSingleDocuments = oJobXML.SelectNodes("/Job/Document[@merge = 'false' or @merge = 'f' or @merge='0' or @merge='no' or @merge='n']")
               'Set oMergeDocuments = oJobXML.SelectNodes("/Job/Document[@merge = '' or @merge = 'true' or @merge = 't' or @merge = '1' or @merge = 'yes' or @merge = 'y']")
290            Set oDocumentNodes = oJobXML.SelectNodes("/Job/Document")
   
300            iDocCount = oDocumentNodes.Length
  
310            ReDim aFaxDocuments(iDocCount)
320            ReDim aDocuments(iDocCount)
330            ReDim aMergeDocs(iDocCount)
340            ReDim aPDFDocuments(iDocCount)
350            ReDim aTIFDocuments(iDocCount)
   
360            If oPackage.SelectSingleNode("Fax") Is Nothing Then
370                m_bFaxDocument = False
380            Else
390                m_bFaxDocument = True
400            End If
   
410            strOutputType = IIf(IsNull(oPackage.getAttribute("type")), "pdf", oPackage.getAttribute("type"))
420            If IsNull(oPackage.getAttribute("outputfile")) Then
430               strMergedDoc = strJobID & "." & strOutputType
440               Trace vbCrLf & "  Merged File name not specified. Defaulting to current Job id [" & strMergedDoc & "]" & vbCrLf
450            Else
460               strMergedDoc = oPackage.getAttribute("outputfile")
470            End If
   
480            If InStr(1, strOutputType, "zip") > 0 Then bZipArchive = True
   
490            Select Case strOutputType
                   Case "pdf-zip": strOutputType = "pdf"
500                Case "tif-zip": strOutputType = "tif"
510            End Select
   
520            If isOutputTypeSupported(strOutputType) = False Then
                   'unsupported output type. mark as bad job
530                bBadJob = True
540                strBadJobReason = "Unsupported Package output type '" & strOutputType & "'. Code Line# " & Erl
 
550                Trace "    Unsupported Package output type " & strOutputType & vbCrLf
560                GoTo badJob
570            End If
   
               'write the PDFMetadata file
  
580            g_strPDFMetaFile = gobjFSO.BuildPath(g_strSortTempPath, "pdfmeta.txt")
590            Set oPDFMetadata = oPackage.SelectSingleNode("PDFMetadata")
600            If Not oPDFMetadata Is Nothing Then
610                Set oPDFKeys = oPDFMetadata.ChildNodes
620                Set oPDFMetaFile = gobjFSO.OpenTextFile(g_strPDFMetaFile, ForWriting, True)
630                For Each oPDFKey In oPDFKeys
640                    oPDFMetaFile.WriteLine "InfoKey: " & oPDFKey.nodeName
650                    oPDFMetaFile.WriteLine "InfoValue: " & oPDFKey.Text
660                Next
 
                   'check if the package contained metadata for the Author/Creator/Producer. If not, then use the
                   ' values from the configuration.
670                If oPDFMetadata.SelectSingleNode("Author") Is Nothing Then
680                    oPDFMetaFile.WriteLine "InfoKey: Author"
690                    oPDFMetaFile.WriteLine "InfoValue: " & g_strPDFAuthor
700                End If
 
710                If oPDFMetadata.SelectSingleNode("Creator") Is Nothing Then
720                    oPDFMetaFile.WriteLine "InfoKey: Creator"
730                    oPDFMetaFile.WriteLine "InfoValue: " & g_strPDFCreator
740                End If
 
750                If oPDFMetadata.SelectSingleNode("Producer") Is Nothing Then
760                    oPDFMetaFile.WriteLine "InfoKey: Producer"
770                    oPDFMetaFile.WriteLine "InfoValue: " & g_strPDFProducer
780                End If
790                oPDFMetaFile.Close
800                Set oPDFMetaFile = Nothing
810                Set oPDFKeys = Nothing
820                Set oPDFKey = Nothing
830            Else
840                Set oPDFMetaFile = gobjFSO.OpenTextFile(g_strPDFMetaFile, ForWriting, True)
850                oPDFMetaFile.WriteLine "InfoKey: Author"
860                oPDFMetaFile.WriteLine "InfoValue: " & g_strPDFAuthor
870                oPDFMetaFile.WriteLine "InfoKey: Creator"
880                oPDFMetaFile.WriteLine "InfoValue: " & g_strPDFCreator
890                oPDFMetaFile.WriteLine "InfoKey: Producer"
900                oPDFMetaFile.WriteLine "InfoValue: " & g_strPDFProducer
910                oPDFMetaFile.Close
920                Set oPDFMetaFile = Nothing
930            End If
   
   
940            iCurDocIdx = 0
   
950            Trace "      Processing documents..." & vbCrLf

               'convert all documents to pdf
960            For Each oDocument In oDocumentNodes
970                m_strCurrentDoc = ""
980                m_strCurrentDocXSL = ""
990                m_strCurrentDocFDF = ""
1000               m_strConvertedDoc = ""
1010               strOutputName = ""
1020               iDupCount = 1
1030               g_blnCoversheet = False
 
1040               If (IsNull(oDocument.getAttribute("merge"))) Then
1050                  bMerge = False
1060               Else
1070                  bMerge = (oDocument.getAttribute("merge") = "true")
1080               End If
 
1090               If (IsNull(oDocument.getAttribute("noconvert"))) Then
1100                  bNoConvert = False
1110               Else
1120                  bNoConvert = (oDocument.getAttribute("noconvert") = "true")
1130               End If
 
1140               If (IsNull(oDocument.getAttribute("htmlOptions"))) Then
1150                  strHTMLOptions = ""
1160               Else
1170                  strHTMLOptions = oDocument.getAttribute("htmlOptions")
1180               End If
 
1190               Trace vbCrLf & "      Document settings: " & oDocument.XML & vbCrLf
 
      '             strOutputFileType = IIf(IsNull(oDocument.getAttribute("outputformat")), "", oDocument.getAttribute("outputformat"))
 
      '             If InStr(1, strOutputFileName, "__coversheet") > 0 Then
      '                 g_blnCoversheet = True
      '                 Trace "      Processing coversheet..." & vbCrLf
      '             Else
      '                 g_blnCoversheet = False
      '             End If
 
1200               strValidationMsg = validateandFetchDocument(oDocument)
1210               If strValidationMsg = "" Then
1220                  If IsNull(oDocument.getAttribute("outputfilename")) Then
1230                     strOutputName = oDocument.getAttribute("filename")
1240                     strOutputName = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(m_strCurrentDoc), gobjFSO.GetBaseName(strOutputName) & "." & LCase(strOutputType))
1250                  Else
1260                     strOutputName = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(m_strCurrentDoc), oDocument.getAttribute("outputfilename"))
1270                  End If
    
1280                  strOutputFileName = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(m_strCurrentDoc), gobjFSO.GetBaseName(m_strCurrentDoc) & ".pdf")
1290                  If gobjFSO.GetExtensionName(m_strCurrentDoc) <> "pdf" Then
1300                      strOutputFileName = convertDocument(m_strCurrentDoc, m_strCurrentDocXSL, m_strCurrentDocFDF, strHTMLOptions, "PDF", False)
1310                      If strOutputFileName = "" Then
1320                          If bNoConvert Then
1330                              strOutputName = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(m_strCurrentDoc), oDocument.getAttribute("outputfilename"))
1340                              GoTo skipConversion
1350                          End If
                              'error converting
1360                          Trace " ERROR converting file." & vbCrLf
1370                          bBadJob = True
1380                          GoTo badJob
1390                      End If
                          'save the converted document in the overall job pdf files
1400                      If InStr(1, strHTMLOptions, "$DONTMERGEWITHJOBDOCUMENT$", vbTextCompare) > 0 Then
1410                      Else
1420                          aPDFDocuments(iCurDocIdx) = strOutputFileName
1430                      End If
1440                  Else
1450                      strOutputFileName = m_strCurrentDoc
1460                      If isPDFProtected(strOutputFileName) = True Then
1470                          unprotectPDF strOutputFileName
1480                      End If
                          'save the converted document in the overall job pdf files
1490                      aPDFDocuments(iCurDocIdx) = strOutputFileName
1500                  End If
    
1510                  If strOutputType = "tif" Or m_bFaxDocument = True Then
1520                      strOutputFileNameFax = convertDocument(strOutputFileName, "", "", "", "TIF", False)
1530                      If strOutputFileNameFax = "" Then
                              'error converting
1540                          Trace " ERROR converting file." & vbCrLf
1550                          bBadJob = True
1560                          GoTo badJob
1570                      Else
                              'save the converted document in the overall job tif files
1580                          aTIFDocuments(iCurDocIdx) = strOutputFileNameFax
1590                      End If
1600                  End If
1610               Else
                      'error
1620                  Trace "ERROR: " & strValidationMsg & vbCrLf
1630                  bBadJob = True
1640                  GoTo badJob
1650               End If
 
skipConversion:
                   '
1660               If bNoConvert Then
1670                  If gobjFSO.FileExists(strOutputName) Then
1680                      strOutputName = getSafeFileName(strOutputName)
1690                  End If
1700                  Trace "        Copying file " & m_strCurrentDoc & " to " & strOutputName & " ..."
1710                  gobjFSO.CopyFile m_strCurrentDoc, strOutputName, False
1720                  Trace "Done. " & vbCrLf
1730                  strConvertedDoc = strOutputName
1740               Else
                      'file was converted
1750                  If bMerge = False Then
                          'need to copy the file to name requested
1760                      strOutputName = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strOutputName), gobjFSO.GetBaseName(strOutputName) & "." & strOutputType)
1770                      If strOutputType = "tif" Then
1780                          Trace "        Copying file " & strOutputFileNameFax & " to " & strOutputName & " ..."
1790                          gobjFSO.CopyFile strOutputFileNameFax, strOutputName, False
1800                          Trace "Done. " & vbCrLf
1810                          strConvertedDoc = strOutputName
1820                      Else
1830                          Trace "        Copying file " & strOutputFileName & " to " & strOutputName & " ..."
1840                          gobjFSO.CopyFile strOutputFileName, strOutputName, False
1850                          Trace "Done. " & vbCrLf
1860                          strConvertedDoc = strOutputName
1870                      End If
1880                  End If
1890               End If
 
1900               If bMerge Then
1910                  If strOutputType = "tif" Then
1920                      aMergeDocs(iCurDocIdx) = strOutputFileNameFax
1930                  Else
1940                      aMergeDocs(iCurDocIdx) = strOutputFileName
1950                  End If
1960               Else
1970                  If gobjFSO.GetBaseName(strOutputName) <> "__job_coversheet" Then
1980                      aDocuments(iCurDocIdx) = strOutputName
1990                  End If
2000               End If

2010               If m_bFaxDocument Then
2020                   If gobjFSO.GetBaseName(strOutputName) <> "__job_coversheet" Then
2030                       aFaxDocuments(iCurDocIdx) = strOutputFileNameFax
2040                   End If
2050               End If
 
2060               oDocument.setAttribute "processedFile", strConvertedDoc
 
2070               iCurDocIdx = iCurDocIdx + 1
2080               DoEvents
2090          Next
  
2100          If gobjFSO.GetParentFolderName(strMergedDoc) = "" Then
2110              strMergedDoc = gobjFSO.BuildPath(g_strSortTempPath, strMergedDoc)
2120          End If
  
2130          If gobjFSO.GetExtensionName(strMergedDoc) = "" Then
2140              strMergedDoc = strMergedDoc & "." & LCase(strOutputType)
2150          End If
  
2160          If Join(aMergeDocs, "") <> "" Then
  
2170          Trace "        Merging documents... " & vbCrLf
  
2180          lngStart = GetTickCount()
2190          Select Case strOutputType
                  Case "pdf"
2200                  If mergePDFs(aMergeDocs, strMergedDoc, g_strPDFMetaFile) = True Then
2210                      ReDim aMergeDocs(iDocCount)
2220                      aMergeDocs(0) = strMergedDoc
2230                  Else
                      'error
2240                  End If
2250              Case "tif"
2260                  If mergeTifs(aMergeDocs, strMergedDoc) = True Then
2270                      ReDim aMergeDocs(iDocCount)
2280                      aMergeDocs(0) = strMergedDoc
2290                  Else
                          'error
2300                  End If
2310          End Select
  
2320          Trace "        Merging done. [" & (GetTickCount() - lngStart) & " ms]" & vbCrLf
  
2330          Else
2340              Trace "        No documents in the merge list" & vbCrLf
2350          End If
  
2360          lngStart = GetTickCount()
2370          Trace "        Merging all documents in the job to pdf ... "
              ' merge all the pdfs for the job
2380          strJobPDFFile = gobjFSO.BuildPath(g_strSortTempPath, strJobID & ".pdf")
2390          If mergePDFs(aPDFDocuments, strJobPDFFile, g_strPDFMetaFile) = False Then
                  'error
2400          End If
2410          Trace "          Merge Done. [" & (GetTickCount() - lngStart) & " ms]" & vbCrLf
  
              ' merge all the tif for the job
2420          If strOutputType = "tif" Or m_bFaxDocument = True Then
2430              lngStart = GetTickCount()
2440              Trace "        Merging all documents in the job to tif ... "
2450              strJobTIFFile = gobjFSO.BuildPath(g_strSortTempPath, strJobID & ".tif")
2460              If mergeTifs(aTIFDocuments, strJobTIFFile) = False Then
                      'error
2470              End If
2480              Trace "          Merge Done. [" & (GetTickCount() - lngStart) & " ms]" & vbCrLf
2490          End If
  
2500          If m_bFaxDocument Then
2510              lngStart = GetTickCount()
2520              Trace "        Merging documents to be faxed ... " & vbCrLf
2530              strOutputFileNameFax = gobjFSO.BuildPath(g_strSortTempPath, strJobID & "_fax.tif")
2540              If mergeTifs(aFaxDocuments, strOutputFileNameFax) Then
2550                  ReDim aFaxDocuments(iDocCount)
2560                  aFaxDocuments(0) = strOutputFileNameFax
2570              Else
                      'error
2580              End If
2590              Trace "          Merge Done. [" & (GetTickCount() - lngStart) & " ms]" & vbCrLf
2600          End If
  
              'set the meta data for the pdf documents
2610          setPDFMetadata strJobPDFFile, g_strPDFMetaFile
  
              'set meta data for any pdf documents in the single document list (no merge)
2620          For j = LBound(aDocuments) To UBound(aDocuments)
2630              If aDocuments(j) <> "" And gobjFSO.GetExtensionName(aDocuments(j)) = "pdf" Then
2640                  setPDFMetadata aDocuments(j), g_strPDFMetaFile
2650              End If
2660          Next
  
              'set meta data for any pdf documents in the merge list
      '        For j = LBound(aMergeDocs) To UBound(aMergeDocs)
      '            If aMergeDocs(j) <> "" And gobjFSO.GetExtensionName(aMergeDocs(j)) = "pdf" Then
      '                setPDFMetadata aMergeDocs(j), g_strPDFMetaFile
      '            End If
      '        Next
  
2670           If g_strLastJobID = strJobID And DateDiff("m", g_dteLastDestinationProcessed, Now) < 1 Then
                   'something got messed up. Shut down the app
2680               Trace vbCrLf & "SOMETHING BAD HAPPENED AND THE APPLICATION IS PROCESSING THE SAME JOB OVER AND OVER AGAIN AND PROCESSING DESTINATIONS. QUITING THE APPLICATION" & vbCrLf
2690               notifyAppShutDown "Same job being processed over and over again. Shutting down the service to prevent spamming."
2700               Unload Me
2710               End
2720           Else
                   'process destinations
 
                   'Set oDocumentNodes = oJobXML.SelectNodes("/Job/Document")
2730               bJobSuccess = processDestinations(oPackage, oDocumentNodes, aDocuments, aMergeDocs, aFaxDocuments)
2740               g_dteLastDestinationProcessed = Now
 
2750               If g_blnArchive Then
                      'archiving is turned on.
                      ' Archive the job with all the converted files in the sorttemp folde
2760                  strArchivePath = gobjFSO.BuildPath(g_strArchivePath, Format(Now, "yyyymmdd"))
2770                  If gobjFSO.FolderExists(strArchivePath) = False Then
                          ' archive path does not exist. create it
2780                      BuildPath strArchivePath
2790                  End If
    
                      'zip the folder contents and store it in the archive location
2800                  zipFolder g_strSortTempPath, getSafeFileName(gobjFSO.BuildPath(strArchivePath, strJobID & ".zip"))
2810               End If
 
2820           End If
   
2830           If bJobSuccess = False Then bBadJob = True
   
2840      Else
               'job file does not exist. remove from the jobs xml
2850           g_objJobsQueueRoot.RemoveChild oJobNode
2860           g_objJobsQueueXML.Save g_strJobQueue
   
2870           g_strConvertErrMsg = "missing job file in queue"
2880      End If
    
badJob:

2890      Trace "      Bad Job: " & bBadJob & vbCrLf
2900      If bBadJob Then
2910          bJobSuccess = False
  
2920          If Err.Number <> 0 Or g_strDestinationErrMsg <> "" Or g_strConvertErrMsg <> "" Then
2930              If Err.Number <> 0 Then
2940                  strBadJobReason = strBadJobReason & vbCrLf & "      ERROR: " & PROC_NAME & " [Line #" & Erl & "] Description: " & Err.Description
2950              End If

2960              strBadJobReason = strBadJobReason & "        Conversion Message: " & g_strConvertErrMsg & vbCrLf & _
                                      "        Destination Message: " & g_strDestinationErrMsg & vbCrLf

2970              Trace "      " & strBadJobReason & vbCrLf
                  Dim aAttachments(0) As String
2980              sendEmail g_strDefaultEmailFrom, g_strAppErrorTo, "", "", "", "ERROR: LYNX Document Processor [" & g_sComputerName & "]", "high", _
                              strBadJobReason, aAttachments
2990          End If
  
3000          Trace "      Marking the job as bad. Reason: " & strBadJobReason & vbCrLf
  
              'mark the reason in the job
3010          Set oBadJobNode = oJobXML.SelectSingleNode("/Job/BadJob")
3020          If oBadJobNode Is Nothing Then
3030              Set oBadJobNode = oJobXML.createElement("BadJob")
3040              oBadJobNode.appendChild oJobXML.createCDATASection(Format(Now, "mm/dd/yyyy hh:nn:ss") & " " & strBadJobReason)
3050              oJobXML.FirstChild.appendChild oBadJobNode
3060          Else
3070              oBadJobNode.Text = oBadJobNode.Text & vbCrLf & Format(Now, "mm/dd/yyyy hh:nn:ss") & " " & strBadJobReason
3080          End If
  
              'oJobXML.Save gobjFSO.BuildPath(g_strBadJobPath, gobjFSO.GetBaseName(strJobFileName) & ".xml")
3090          gobjFSO.CopyFile strJobFileName, gobjFSO.BuildPath(g_strBadJobPath, gobjFSO.GetBaseName(strJobFileName) & ".xml"), False
  
        '4/15/08 - let the job file remain in the pickup folder itself so the support staff need to
        'move the job file only.
      '        'move the job attachments to the bad job folder
      '        strOutputFileName = gobjFSO.BuildPath(g_strPickupPath, "*" & strJobID & "*.*")
      '        strOutputFileNameTmp = Dir(strOutputFileName)
      '        While strOutputFileNameTmp <> ""
      '            Trace "        Moving attachment " & strOutputFileNameTmp & " to " & g_strBadJobPath & " ... "
      '            gobjFSO.MoveFile gobjFSO.BuildPath(g_strPickupPath, strOutputFileNameTmp), _
      '                             gobjFSO.BuildPath(g_strBadJobPath, strOutputFileNameTmp)
      '            Trace "Done." & vbCrLf
      '            strOutputFileNameTmp = Dir()
      '        Wend
        '4/15/08 - End of change
  
3100          Trace "      Deleting the job file..."
              'delete job file
3110          gobjFSO.DeleteFile strJobFileName, True
  
3120          Trace "Done." & vbCrLf
  
3130          Trace "      Removing job from queue..."
  
              'remove job from the queue
3140          g_objJobsQueueRoot.RemoveChild oJobNode
3150          g_objJobsQueueXML.Save g_strJobQueue
  
3160          Trace "Done." & vbCrLf
  
3170          g_strConvertErrMsg = strBadJobReason
3180      End If
    
3190      On Error GoTo 0
    
3200      If bJobSuccess Then
3210          Trace "   Job successfully processed." & vbCrLf
              'remove job from the queue
3220          g_objJobsQueueRoot.RemoveChild oJobNode
3230          g_objJobsQueueXML.Save g_strJobQueue
  
3240          Trace "   Updated Job Queue successfully." & vbCrLf
  
              'we are done with the job. Now it is safe to delete the job as well as the attachments
              'job successfully processed. Delete from queue
              Dim retrycount As Integer
  

3250          retrycount = 1
  
3260          Trace "   Deleting Job file " & strJobFileName
3270          On Error Resume Next
3280          While retrycount < 5
cleanupRetry:
3290              gobjFSO.DeleteFile strJobFileName, True
      
3300              If gobjFSO.FileExists(strJobFileName) Then
3310                  Trace " Retry #" & retrycount
                      'lets sleep a little longer for each retry
                      ' hoping the lock on the file will be released
3320                  Sleep 2000 * retrycount
3330                  retrycount = retrycount + 1
3340                  GoTo cleanupRetry
3350              End If
3360          Wend
  
3370          If gobjFSO.FileExists(strJobFileName) Then
3380              Trace " ERROR: Could not delete job file." & vbCrLf
3390          Else
3400              Trace " DONE." & vbCrLf
3410          End If
    
              'delete the attachments now.
3420          If strJobID <> "" Then
                  'delete the files in the pickup folder for this job
3430              If Dir(gobjFSO.BuildPath(g_strPickupPath, strJobID & "*.*")) <> "" Then
3440                  Trace vbCrLf & "    Deleting job attachments..."
3450                  Kill gobjFSO.BuildPath(g_strPickupPath, strJobID & "*.*")
3460                  Trace "Done." & vbCrLf
3470              End If
3480          End If

              'check if we need to manually clean up the processed file.
3490          Set oNotesMsg = oPackage.SelectSingleNode(".//NotesMsg")
3500          If Not oNotesMsg Is Nothing Then
                  'we need to manually delete the processed files
3510              Set oDocumentNodes = oJobXML.SelectNodes("/Job/Document")
3520              On Error Resume Next

3530              If Dir(gobjFSO.BuildPath(g_strOutboxPath2External, strJobID & "*.*")) <> "" Then
3540                  Trace "    Deleting " & gobjFSO.BuildPath(g_strOutboxPath2External, strJobID & "*.*")
3550                  Kill gobjFSO.BuildPath(g_strOutboxPath2External, strJobID & "*.*")
3560                  If Err.Number = 0 Then
3570                      Trace "ERROR: " & Err.Description & vbCrLf
3580                  Else
3590                      Trace " DONE." & vbCrLf
3600                  End If
3610              End If

3620              On Error GoTo 0

3630          End If
3640      End If
    
          'notify job status
3650      If Not oPackage Is Nothing Then
3660          If g_strLastJobID = strJobID And DateDiff("m", g_dteLastNotify, Now) < 1 Then
                  'something got messed up and the service is running out of control.
3670              Trace vbCrLf & "SOMETHING BAD HAPPENED AND THE APPLICATION IS PROCESSING THE SAME JOB OVER AND OVER AGAIN AND NOTIFYING USER. QUITING THE APPLICATION" & vbCrLf
3680              notifyAppShutDown "Same job is being processed over and over again. Shutting down the service to prevent spamming."
3690              Unload Me
3700              End
3710          Else
3720              notifyJobStatus oPackage, bJobSuccess
3730          End If
3740      End If
    
          'writeJournal oJobNode.getAttribute("id"), oJobNode.getAttribute("description"), bJobSuccess
    
3750      g_strLastJobID = strJobID
    
3760      Trace vbCrLf

3770      Trace "   Job processed in " & (GetTickCount() - lngJobStartTick) & " ms." & vbCrLf & _
                "****************************************" & vbCrLf & vbCrLf
    
3780      Set oJobXML = Nothing
3790      Set oDocumentNodes = Nothing
3800      Set oPackage = Nothing
3810      Set oDocument = Nothing
End Sub

Private Function validateandFetchDocument(ByRef oDocument As IXMLDOMElement) As String
10        On Error GoTo errHandler
          Const PROC_NAME As String = "validateandFetchDocument()"
          Dim strDocFullPath As String
          Dim strDocPath As String
          Dim strDocName As String
          Dim strDocType As String
          Dim strDocXSLPath As String
          Dim strDocXSLName As String
          Dim strLocalFile As String
          Dim blnNoConvert As Boolean
          Dim strFDFFile As String
          Dim strFDFPath As String
          
          Dim strRet As String
          
          Dim oEmbeddedData As IXMLDOMElement
          Dim oEmbeddedXSLData As IXMLDOMElement
          Dim oADOStream As ADODB.Stream
          Dim oFile As File
          
20        strRet = ""
          
30        Trace "      Validating document..."

40        strDocFullPath = IIf(IsNull(oDocument.getAttribute("filefullpath")), "", oDocument.getAttribute("filefullpath"))
50        strDocPath = IIf(IsNull(oDocument.getAttribute("filepath")), "", oDocument.getAttribute("filepath"))
60        strDocName = IIf(IsNull(oDocument.getAttribute("filename")), "", oDocument.getAttribute("filename"))
70        strDocType = IIf(IsNull(oDocument.getAttribute("filetype")), "", oDocument.getAttribute("filetype"))
80        strDocXSLPath = IIf(IsNull(oDocument.getAttribute("xslpath")), "", oDocument.getAttribute("xslpath"))
90        strFDFFile = IIf(IsNull(oDocument.getAttribute("fdfpath")), "", oDocument.getAttribute("fdfpath"))
100       blnNoConvert = oDocument.getAttribute("noconvert") = "true"
          
110       If strDocFullPath = "" Then
120           strDocFullPath = gobjFSO.BuildPath(strDocPath, strDocName)
130       End If
          
140       Set oEmbeddedData = oDocument.SelectSingleNode("Data")
150       Set oEmbeddedXSLData = oDocument.SelectSingleNode("XSLData")
          
          'do validations for the embedded document
160       If Not oEmbeddedData Is Nothing Then
170           If strDocType = "" And strDocPath = "" And strDocName = "" And strDocFullPath = "" Then
                  ' we don't know what type of embedded data we have. Bad Job. Error out
180               strRet = "Cannot determine the embedded data type."
190           Else
200               If strDocType = "" Then
                      'try guessing the type from file information
210                   If strDocFullPath <> "" Then
220                       strDocType = gobjFSO.GetExtensionName(strDocFullPath)
230                       strDocName = gobjFSO.GetBaseName(strDocFullPath) & "." & strDocType
240                       strDocPath = gobjFSO.GetParentFolderName(strDocFullPath)
250                   ElseIf strDocName <> "" Then
260                       strDocType = gobjFSO.GetExtensionName(strDocName)
270                   ElseIf strDocPath <> "" Then
280                       strDocType = gobjFSO.GetExtensionName(strDocPath)
290                   End If
300               End If
                  
310               strDocFullPath = gobjFSO.BuildPath(g_strQueuePath, strDocName)
320           End If
330       Else
              'linked document
340           If strDocType = "" And strDocPath = "" And strDocName = "" And strDocFullPath = "" Then
                  ' we don't know where the linked file is
350               strRet = "Missing linked file definition."
360           Else
370               If strDocFullPath = "" Then
380                   strDocFullPath = gobjFSO.BuildPath(strDocPath, strDocName)
390               End If
                  
400               If strDocFullPath <> "" Then
                      'check the existance of the linked file
410                   If gobjFSO.FileExists(strDocFullPath) = False Then
                          'check for the file in the pickup queue
420                       If gobjFSO.FileExists(gobjFSO.BuildPath(g_strPickupPath, gobjFSO.GetBaseName(strDocFullPath) & "." & gobjFSO.GetExtensionName(strDocFullPath))) = False Then
430                           strRet = "Linked file not found [" & strDocFullPath & "]"
440                       Else
450                           strDocFullPath = gobjFSO.BuildPath(g_strPickupPath, gobjFSO.GetBaseName(strDocFullPath) & "." & gobjFSO.GetExtensionName(strDocFullPath))
460                       End If
470                   Else
480                       strDocType = gobjFSO.GetExtensionName(strDocFullPath)
490                   End If
500               Else
510                   strRet = "Missing linked file definition"
520               End If
530           End If
540       End If
          
550       If blnNoConvert = False Then 'document needs conversion
              'check for unsupported document
560           Select Case strDocType
                  Case "doc", "xls", "txt", "rpt", "tif", "tiff", "jpg", "jpeg", "bmp", "gif", "htm"
570               Case "pdf"
580                   If strFDFFile <> "" Then
590                       strFDFPath = strFDFFile
600                       If gobjFSO.FileExists(strFDFPath) = False Then
610                           strFDFPath = gobjFSO.BuildPath(g_strPickupPath, gobjFSO.GetBaseName(strFDFFile) & "." & gobjFSO.GetExtensionName(strFDFFile))
620                       End If
                          
630                       If gobjFSO.FileExists(strFDFPath) = False Then
640                           strRet = "Unable to locate FDF File"
650                       End If
660                   End If
670               Case "xml"
680                   If (strDocXSLPath = "") And (oEmbeddedXSLData Is Nothing) Then
690                       strRet = "Missing XSL transformation file."
700                   End If
                      
710                   If oEmbeddedData Is Nothing Then
720                       If gobjFSO.FileExists(strDocXSLPath) = False Then
730                           strDocXSLPath = gobjFSO.BuildPath(g_strPickupPath, gobjFSO.GetBaseName(strDocXSLPath) & "." & gobjFSO.GetExtensionName(strDocXSLPath))
740                       End If
                      
750                       If gobjFSO.FileExists(strDocXSLPath) = False Then
760                           strRet = "Unable to locate XSL File"
770                       End If
780                   End If
790               Case Else
800                   strRet = "Unsupported embedded data type."
810           End Select
820       End If
          
830       If strRet = "" Then
              'save/fetch the document
              
840           Trace "Done. Fetching data/file..."
              
850           If strDocName = "" Then
860               strDocName = gobjFSO.GetBaseName(gobjFSO.GetTempName()) & "." & strDocType
870           End If
              
880           If Not oEmbeddedData Is Nothing Then
                  'save embedded document
890               strLocalFile = gobjFSO.BuildPath(g_strSortTempPath, strDocName)
900               Set oADOStream = New ADODB.Stream
910               oADOStream.Type = adTypeBinary
920               oADOStream.Open
930               oADOStream.Write oEmbeddedData.nodeTypedValue
940               oADOStream.Position = 0
                  
950               oADOStream.SaveToFile strLocalFile
                  
960               Set oADOStream = Nothing
                  
970               m_strCurrentDoc = strLocalFile
980           Else
                  'fetch linked document
990               strLocalFile = gobjFSO.BuildPath(g_strSortTempPath, strDocName)
                  
1000              gobjFSO.CopyFile strDocFullPath, strLocalFile
                  
1010              Set oFile = gobjFSO.GetFile(strLocalFile)
1020              oFile.Attributes = Normal
1030              Set oFile = Nothing
                  
1040              m_strCurrentDoc = strLocalFile
1050          End If
              
1060          If strDocType = "xml" Then
1070              If strDocXSLPath <> "" Then strDocXSLName = gobjFSO.GetBaseName(strDocXSLPath) & ".xsl"
1080              If Not oEmbeddedXSLData Is Nothing Then
                      'save embedded document
1090                  strLocalFile = gobjFSO.BuildPath(g_strSortTempPath, strDocXSLName)
                      
1100                  If gobjFSO.FileExists(strLocalFile) Then gobjFSO.DeleteFile strLocalFile
                      
1110                  Set oADOStream = New ADODB.Stream
1120                  oADOStream.Type = adTypeBinary
1130                  oADOStream.Open
1140                  oADOStream.Write oEmbeddedData.nodeTypedValue
1150                  oADOStream.Position = 0
                      
1160                  oADOStream.SaveToFile strLocalFile
                      
1170                  Set oADOStream = Nothing
                      
1180                  m_strCurrentDocXSL = strLocalFile
1190              Else
1200                  If strDocXSLName <> "" Then
                          'fetch linked document
1210                      strLocalFile = gobjFSO.BuildPath(g_strSortTempPath, strDocXSLName)
                          
1220                      If gobjFSO.FileExists(strLocalFile) Then gobjFSO.DeleteFile strLocalFile, True
                          
1230                      gobjFSO.CopyFile strDocXSLPath, strLocalFile
                          
1240                      Set oFile = gobjFSO.GetFile(strLocalFile)
1250                      oFile.Attributes = Normal
1260                      Set oFile = Nothing
                          
1270                      m_strCurrentDocXSL = strLocalFile
1280                  Else
1290                      m_strCurrentDocXSL = ""
1300                  End If
1310              End If
1320          End If
              
1330          If strFDFFile <> "" Then
1340              strLocalFile = gobjFSO.BuildPath(g_strSortTempPath, gobjFSO.GetBaseName(strFDFFile) & "." & gobjFSO.GetExtensionName(strFDFFile))
                  
1350              If gobjFSO.FileExists(strLocalFile) Then gobjFSO.DeleteFile strLocalFile, True
                  
1360              gobjFSO.CopyFile strFDFPath, strLocalFile
                  
1370              Set oFile = gobjFSO.GetFile(strLocalFile)
1380              oFile.Attributes = Normal
1390              Set oFile = Nothing
                  
1400              m_strCurrentDocFDF = strLocalFile
1410          End If
1420          Trace "Done." & vbCrLf
1430      End If
          
errHandler:
1440      If Err.Number <> 0 Then
1450          strRet = strRet & vbCrLf & "       ERROR in " & PROC_NAME & " [Line #" & Erl & "] Description: " & Err.Description & vbCrLf
1460          Trace strRet & vbCrLf
1470      End If
1480      validateandFetchDocument = strRet
1490      Set oEmbeddedData = Nothing
1500      Set oEmbeddedXSLData = Nothing
1510      Set oADOStream = Nothing
          
End Function

Private Function convertDocument(strFilePath As String, strXSLPath As String, strFDFPath As String, strHTMLOptions As String, strOutputType As String, bDeleteOriginal As Boolean) As String
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "convertDocument()"
          Dim bRet As Boolean
          Dim strDocType As String
          Dim strRet As String
          Dim lngStartTick As Long
          Dim lngEndTick As Long
          
20        bRet = False
30        g_strConvertErrMsg = ""
          
40        lngStartTick = GetTickCount()
          
50        strDocType = gobjFSO.GetExtensionName(strFilePath)
          
60        If strDocType = strOutputType And strFDFPath = "" Then
              'MsgBox "Input and Output file are of same type. No need for conversion.", vbInformation
70            bRet = True
80            strRet = ""
90            convertDocument = strRet
100           Exit Function
110       End If
          
120       Trace "      Converting " & strFilePath & " to " & strOutputType & vbCrLf
          
130       Select Case strOutputType
              Case "tif"
140               Select Case strDocType
                      Case "pdf"
150                       strRet = PDF2Tif(strFilePath, strFDFPath, bDeleteOriginal)
160                   Case "doc", "rtf", "txt"
170                       strRet = Word2Tif(strFilePath, bDeleteOriginal)
180                   Case "xls"
190                       strRet = Excel2Tif(strFilePath, bDeleteOriginal)
200                   Case "xml"
210                       strRet = XML2Tif(strFilePath, strXSLPath, bDeleteOriginal)
220                   Case "htm", "html"
230                       strRet = HTML2Tif(strFilePath, strHTMLOptions, bDeleteOriginal)
240                   Case "rpt"
250                       strRet = RPT2Tif(strFilePath, bDeleteOriginal)
260                   Case "jpg", "jpeg", "bmp", "gif"
270                       ReduceImageSize strFilePath
                          'Reduceimage Size converts the image to jpg format. so lets start using it
280                       If gobjFSO.FileExists(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFilePath), gobjFSO.GetBaseName(strFilePath) & ".jpg")) Then
290                           strFilePath = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFilePath), gobjFSO.GetBaseName(strFilePath) & ".jpg")
300                       End If
310                       strRet = Image2Tif(strFilePath, bDeleteOriginal)
320                   Case "tif" ' no conversion required
330                       bRet = True
340                       strRet = strFilePath
350                   Case Else 'unsupported input document
360                       Trace "      Unsupported document type. No conversion performed" & vbCrLf
370                       bRet = False
380                       strRet = ""
390               End Select
400           Case "pdf"
410               Select Case strDocType
                      Case "doc", "rtf", "txt"
420                       strRet = Word2PDF(strFilePath, bDeleteOriginal)
430                   Case "xls"
440                       strRet = Excel2PDF(strFilePath, bDeleteOriginal)
450                   Case "xml"
460                       strRet = XML2PDF(strFilePath, strXSLPath, bDeleteOriginal)
470                   Case "htm", "html"
480                       strRet = HTML2PDF(strFilePath, strHTMLOptions, bDeleteOriginal)
490                   Case "rpt"
500                       strRet = RPT2PDF(strFilePath, bDeleteOriginal)
510                   Case "jpg", "jpeg", "bmp", "gif"
520                       ReduceImageSize strFilePath
                          'Reduceimage Size converts the image to jpg format. so lets start using it
530                       If gobjFSO.FileExists(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFilePath), gobjFSO.GetBaseName(strFilePath) & ".jpg")) Then
540                           strFilePath = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFilePath), gobjFSO.GetBaseName(strFilePath) & ".jpg")
550                       End If
560                       strRet = Image2PDF(strFilePath, bDeleteOriginal)
570                   Case "tif"
580                       strRet = Tif2PDF(strFilePath, bDeleteOriginal)
590                   Case "pdf" 'no conversion required.
600                       If strFDFPath <> "" Then
610                           strRet = PDFFormFill(strFilePath, strFDFPath, True)
620                       Else
630                           bRet = True
640                           strRet = strFilePath
650                       End If
660                   Case Else 'unsupported input document
670                       Trace "      Unsupported document type. No conversion performed" & vbCrLf
680                       bRet = False
690                       strRet = ""
700               End Select
      '        Case "jpg"
      '            Select Case strDocType
      '                Case "pdf"
      '                    strRet = PDF2JPG(strFilePath, strFDFPath, bDeleteOriginal)
      '                Case "doc", "rtf", "txt"
      '                    strRet = Word2JPG(strFilePath, bDeleteOriginal)
      '                Case "xls"
      '                    strRet = Excel2JPG(strFilePath, bDeleteOriginal)
      '                Case "xml"
      '                    strRet = XML2JPG(strFilePath, strXSLPath, bDeleteOriginal)
      '                Case "htm", "html"
      '                    strRet = HTML2JPG(strFilePath, strHTMLOptions, bDeleteOriginal)
      '                Case "rpt"
      '                    strRet = RPT2JPG(strFilePath, bDeleteOriginal)
      '                Case "jpg", "jpeg", "bmp", "gif", "tif"
      '                    ReduceImageSize strFilePath
      '                    'Reduceimage Size converts the image to jpg format. so lets start using it
      '                    If gobjFSO.FileExists(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFilePath), gobjFSO.GetBaseName(strFilePath) & ".jpg")) Then
      '                        strFilePath = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFilePath), gobjFSO.GetBaseName(strFilePath) & ".jpg")
      '                    End If
      '                    strRet = Image2JPG(strFilePath, bDeleteOriginal)
      '                Case Else 'unsupported input document
      '                    Trace "      Unsupported document type. No conversion performed" & vbCrLf
      '                    bRet = False
      '                    strRet = ""
      '            End Select
710       End Select
          
720       lngEndTick = GetTickCount()
          
730       Trace "        Output: " & strRet & " [converted in " & (lngEndTick - lngStartTick) & " ms]" & vbCrLf
          
errHandler:
740       If Err.Number <> 0 Then
750           bRet = False
760           strRet = ""
770           Trace "      ERROR in " & PROC_NAME & " [Line #" & Erl & "]. Description: " & Err.Description & vbCrLf
780       End If
          
790       convertDocument = strRet
End Function

Private Function isOutputTypeSupported(strOutputType As String) As Boolean
          Dim bSupported As Boolean
10        Select Case strOutputType
              Case "pdf": bSupported = True
20            Case "tif": bSupported = True
30            Case "pdf-zip": bSupported = True
40            Case "tif-zip": bSupported = True
50            Case Else
60                bSupported = False
70        End Select
          
80        isOutputTypeSupported = bSupported
End Function

Private Function processDestinations(ByRef oPackage As IXMLDOMElement, oDocumentNodes As IXMLDOMNodeList, aDocuments() As String, aMergeDocs() As String, aFaxDocuments() As String) As Boolean
10        On Error GoTo errHandler
          Const PROC_NAME As String = "processDestinations()"
          Dim oDestinations As IXMLDOMNodeList
          Dim oDestination As IXMLDOMElement
          
          Dim bCurProcessSuccess As Boolean
          Dim bAllProcessSuccess As Boolean
          Dim strDestinationType As String
          Dim bDestProcessed As Boolean
          
20        bAllProcessSuccess = True
          
30        g_strDocumentsSent = ""
          
40        g_strDestinationErrMsg = ""
          
50        For Each oDestination In oPackage.ChildNodes
60            Select Case oDestination.NodeType
                  Case NODE_ELEMENT:
70                    strDestinationType = oDestination.nodeName
80                    bDestProcessed = False
                      
90                    Select Case strDestinationType
                          Case "Email":
100                           bDestProcessed = True
110                           bCurProcessSuccess = processEmail(oDestination, aDocuments, aMergeDocs)
120                           bAllProcessSuccess = bAllProcessSuccess And bCurProcessSuccess
130                       Case "Fax":
140                           bDestProcessed = True
150                           bCurProcessSuccess = processFax(oDestination, aFaxDocuments)
160                           bAllProcessSuccess = bAllProcessSuccess And bCurProcessSuccess
170                       Case "HTTPPost":
180                           bDestProcessed = True
190                           bCurProcessSuccess = processHTTPPost(oDestination, oDocumentNodes, aDocuments, aMergeDocs)
200                           bAllProcessSuccess = bAllProcessSuccess And bCurProcessSuccess
210                       Case "FTP":
220                           bDestProcessed = True
230                           bCurProcessSuccess = processFTP(oDestination, aDocuments, aMergeDocs)
240                           bAllProcessSuccess = bAllProcessSuccess And bCurProcessSuccess
250                       Case "FileCopy":
260                           bDestProcessed = True
270                           bCurProcessSuccess = processFileCopy(oDestination, aDocuments, aMergeDocs)
280                           bAllProcessSuccess = bAllProcessSuccess And bCurProcessSuccess
290                   End Select
                      
300                   If bDestProcessed = True Then
310                       If bCurProcessSuccess = False Then
320                           processDestinations = False
330                           Exit For
340                       End If
350                   End If
360           End Select
              'If oDestination.NodeType = NODE_ELEMENT Then
              'End If
370       Next
          
380       processDestinations = bAllProcessSuccess

errHandler:
390       Set oDestination = Nothing
400       Set oDestinations = Nothing
410       If Err.Number <> 0 Then
420           processDestinations = False
430           Trace "      ERROR in " & PROC_NAME & " [Line #" & Erl & "]. Description: " & Err.Description & vbCrLf
440       End If
End Function

Private Sub PDFCreator1_eError()
          Dim pdfErr As PDFCreator.clsPDFCreatorError
10        Set pdfErr = PDFCreator1.cError
          'handleError "ERROR [" & PDFCreator1.cErrorDetail("Number") & "]: " & PDFCreator1.cErrorDetail("Description")
20        Trace "    PDFCreator ERROR: [" & pdfErr.Number & "]: " & pdfErr.Description & vbCrLf
          
30        m_bPDFCreatorError = True
End Sub

'***************************************************************************************
' Procedure : PDFCreator1_eReady
' DateTime  : 6/15/2006 16:07
' Author    : Ramesh Vishegu
' Purpose   :
'***************************************************************************************
'
Private Sub PDFCreator1_eReady()
    Trace "      PDFCreator saved the file at " & PDFCreator1.cOutputFilename & vbCrLf
    PDFCreator1.cPrinterStop = True
End Sub

Public Sub notifyJobStatus(ByRef oPackage As IXMLDOMElement, bSuccess As Boolean)
10        On Error GoTo errHandler
          Const PROC_NAME As String = "notifyJobStatus()"
          Dim oNotifyNode As IXMLDOMElement
          Dim strFrom As String
          Dim strTo As String
          Dim strSubject As String
          Dim strBody As String
          Dim aAttachments(0) As String
          
20        If Not oPackage Is Nothing Then
30            If bSuccess Then
40                Set oNotifyNode = oPackage.SelectSingleNode("Notify/Success")
50            Else
60                Set oNotifyNode = oPackage.SelectSingleNode("Notify/Error")
70            End If
              
80            If Not oNotifyNode Is Nothing Then
                  
90                strFrom = getXMLValue(oNotifyNode, "From", "", False)
100               strTo = getXMLValue(oNotifyNode, "To", "", False)
110               strSubject = getXMLValue(oNotifyNode, "Subject", "", False)
120               strBody = getXMLValue(oNotifyNode, "Body", "", False)
                  
130               If strFrom = "" Then strFrom = g_strDefaultEmailFrom
                  
140               If strTo <> "" Then
150                   Trace "   Notifying job " & IIf(bSuccess, "Success", "FAILURE") & " status..." & vbCrLf
                      
                      'resolve macros from the subject
160                   strSubject = Replace(strSubject, "$now$", Format(Now, "mm/dd/yyyy hh:nn:ss"))
                      
                      'resolve macros from the body
170                   strBody = Replace(strBody, "$now$", Format(Now, "mm/dd/yyyy hh:nn:ss"))
180                   strBody = Replace(strBody, "$DocumentList$", g_strDocumentsSent)
190                   strBody = Replace(strBody, "$ErrorDescription$", g_strConvertErrMsg & vbCrLf & g_strDestinationErrMsg)
200                   strBody = Replace(strBody, "\n", vbCrLf)
                      
                      'send the notification
210                   sendEmail strFrom, strTo, "", "", "", strSubject, "", strBody, aAttachments
                      
220               End If
230           End If
240       End If
          
errHandler:
250       If Err.Number <> 0 Then
260           Trace "      ERROR in " & PROC_NAME & " [Line #" & Erl & "]. Description: " & Err.Description & vbCrLf
270       End If
End Sub

Public Sub writeJournal(strJobID, strJobDesc, bJobSuccess)
10        On Error GoTo errHandler
          Const PROC_NAME As String = "writeJournal()"
          Dim oJournalXML As MSXML2.DOMDocument
          Dim strJournalFile As String
          Dim oCurrentEntry As IXMLDOMElement
          
20        strJournalFile = gobjFSO.BuildPath(App.Path, "Journal.xml")
          
30        Set oJournalXML = CreateObject("MSXML2.DOMDocument")
40        oJournalXML.async = False
          
50        If gobjFSO.FileExists(strJournalFile) Then
60            oJournalXML.Load strJournalFile
              
70            If oJournalXML.parseError.errorCode <> 0 Then
                  'journal file is corrupt. make a backup and start fresh
80                gobjFSO.MoveFile strJournalFile, gobjFSO.BuildPath(App.Path, "Journal_" & Format(Now, "yyyymmddhhnnss") & ".xml")
90                oJournalXML.loadXML "<ProcessingJournal/>"
100           End If
110       Else
              'journal file does not exist. create one
120           oJournalXML.loadXML "<ProcessingJournal/>"
130       End If
          
          'log the current job
140       Set oCurrentEntry = oJournalXML.createElement("Job")
150       oCurrentEntry.setAttribute "journalId", Format(Now, "yyyymmddhhnnss")
160       oCurrentEntry.setAttribute "jobId", strJobID
170       oCurrentEntry.setAttribute "description", strJobDesc
180       oCurrentEntry.setAttribute "processedOn", Format(Now, "mm/dd/yyyy hh:nn:ss")
190       oCurrentEntry.setAttribute "status", IIf(bJobSuccess, "Success", "Failed")
200       oCurrentEntry.setAttribute "reason", IIf(bJobSuccess, "", g_strConvertErrMsg)
          
210       oJournalXML.FirstChild.appendChild oCurrentEntry
          
          'delete the old one
          'gobjFSO.DeleteFile strJournalFile
          
          'save changes
220       oJournalXML.Save strJournalFile
          
errHandler:
230       If Err.Number <> 0 Then
240           Trace "      ERROR in " & PROC_NAME & " [Line #" & Erl & "]. Description: " & Err.Description & vbCrLf
250       End If
End Sub

Private Sub tmrSvc_Timer()
10        tmrSvc.Enabled = False
20        startProcessing
End Sub

Private Sub notifyAppShutDown(strReason As String)
          Dim aAttachments(0) As String
10        If g_strAppErrorTo = "" Then g_strAppErrorTo = "rvishegu@lynxservice.com"
20        If g_strDefaultEmailFrom = "" Then g_strDefaultEmailFrom = "lynxserv@ppg.com"
          
30        If DateDiff("m", m_dteLastEmailSent, Now) > 1 Then
40            sendEmail g_strDefaultEmailFrom, g_strAppErrorTo, "", "", "", _
                        "LYNX Document Processor - Unexpected stop - " & g_sComputerName, "", strReason, aAttachments
                        
50            m_dteLastEmailSent = Now
60            Unload Me
70            End
80        Else
              'prevent spamming
90            End
100       End If
End Sub

'***************************************************************************************
' Procedure : ReduceImageSize2
' DateTime  : 10/17/2006 11:40
' Author    : Ramesh Vishegu
' Purpose   :
'***************************************************************************************
'
Public Function ReduceImageSize2(strFileName As String) As Boolean
10        On Error GoTo errHandler
          Dim strOutputFileName As String
          Dim strFileExtension As String
20        strFileExtension = gobjFSO.GetExtensionName(strFileName)
          
30        If g_intJPGMaxHeight > 0 And g_intJPGMaxWidth > 0 Then
40            If strFileExtension = "jpg" Or strFileExtension = "jpeg" Or strFileExtension = "bmp" Or strFileExtension = "gif" Then
50                strOutputFileName = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".tmp")
60                dspImage.ImageDataSource = filImage
70                With filImage
80                    .LoadFile strFileName
90                    If .PageWidth > g_intJPGMaxWidth Or .PageHeight > g_intJPGMaxHeight Then
100                       .OutputFileFormat = PixelFileOutputFormat_JPEG
110                       .OutputBitsPerSample = PixelFileBitsPerSample_8
120                       .OutputSamplesPerPixel = PixelFileSamplesPerPixel_3
130                       .OutputPhotoInterp = PixelFilePhotoInterp_RGB
140                       .OutputRotation = PixelFileOutputRotation_0
                          
150                       If .PageWidth > .PageHeight Then
160                           .OutputScalingNum = g_intJPGMaxWidth
170                           .OutputScalingDen = .PageWidth
180                       Else
190                           .OutputScalingNum = g_intJPGMaxHeight
200                           .OutputScalingDen = .PageHeight
210                       End If
                          
220                       .SavePage strOutputFileName
                          
230                       .InputFileName = ""
                          
                          'delete the original file
240                       gobjFSO.DeleteFile strFileName
                          
                          'move the converted image to the original file
250                       gobjFSO.MoveFile strOutputFileName, strFileName
260                   End If
270                   .InputFileName = ""
280                   .Clear
                      
290               End With
300           End If
310       End If
320       ReduceImageSize2 = True
errHandler:
330       If Err.Number > 0 Then
340           ReduceImageSize2 = False
350           filImage.InputFileName = ""
360           filImage.Clear
370       End If
380       filImage.InputFileName = ""
390       filImage.Clear
End Function

'***************************************************************************************
' Procedure : ReduceImageSize
' DateTime  : 3/3/2008 13:24
' Author    : Ramesh Vishegu
' Purpose   : Procedure to reduce the resolution of an image.
'***************************************************************************************
'
Public Function ReduceImageSize(strFileName As String) As Boolean
10        On Error GoTo errHandler
          Const PROC_NAME As String = "ReduceImageSize()"
          Dim bRet As Boolean
          Dim strOutputFileName As String
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Dim strCmd As String
          
20        If gobjFSO.FileExists(strFileName) Then
30            strOutputFileName = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".jpg")
              
40            strCmd = """" & g_strIMPath & """ """ & strFileName & """ -sample """ & g_intJPGMaxHeight & ">x" & g_intJPGMaxWidth & ">"" """ & strOutputFileName & """"
              
50            If g_blnDirectExecute Then
60                bRet = SuperShell(strCmd, g_strSortTempPath, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
70            Else
80                strBatchFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), "resize.bat")
                  
90                Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
100               oBatFile.WriteLine strCmd
110               oBatFile.Close
120               Set oBatFile = Nothing
                  
                  'execute the batch file
130               bRet = SuperShell(strBatchFile, gobjFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
140               Sleep 1000
150           End If
              
160           DoEvents
              
170           If gobjFSO.FileExists(strOutputFileName) = False Then
180               Err.Raise vbObjectError + 100, PROC_NAME, "Resized Image File " & strOutputFileName & " not found."
190           Else
200               bRet = True
210           End If
220       Else
230           bRet = False
240       End If
          
          'If bDeleteOriginal Then safeDelete strFileName
          
errHandler:
250       If Err.Number <> 0 Then
260           bRet = False
270           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
280           Trace g_strConvertErrMsg
290       End If
300       ReduceImageSize = bRet
End Function

Private Function tifExists(strFileName As String) As String
    Dim strTifFileName As String
    If strFileName <> "" Then
        strTifFileName = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".tif")
        If gobjFSO.FileExists(strTifFileName) Then
            tifExists = strTifFileName
        End If
    End If
End Function

Private Function zipFolder_old(strFolderName As String, strOutputName As String) As Boolean
10        On Error GoTo errHandler
          Dim lStartTick As Long
          Dim strSourceFolder As String

          
20        lStartTick = GetTickCount()
30        Trace "        Archiving files in " & strFolderName & " to " & strOutputName & " ... "
40        If gobjFSO.GetExtensionName(strOutputName) <> "zip" Then
50            strOutputName = strOutputName & ".zip"
60        End If
          
70        strSourceFolder = strFolderName & "\*.*"
          
80        Set m_cZ = New cZip
          
90        With m_cZ
100          .ZipFile = strOutputName
             
110          .AllowAppend = True
             
120          .ClearFileSpecs
130          .AddFileSpec strSourceFolder
140          .StoreFolderNames = False
150          .RecurseSubDirs = False
160          .MessageLevel = ezpAllMessages
170          .Zip
          
180          If (.Success) Then
                  'test the zip file
190               zipFolder_old = True
200               Set m_cUnzip = New cUnzip
210               m_cUnzip.ZipFile = strOutputName
220               If m_cUnzip.TestZip = True Then
230                   zipFolder_old = True
240               End If
250          Else
260              zipFolder_old = False
270          End If
280       End With
290       Trace "Done. [" & (GetTickCount() - lStartTick) & " ms]" & vbCrLf
errHandler:
300       Set m_cZ = Nothing
310       Set m_cUnzip = Nothing
320       If Err.Number <> 0 Then
330           Trace "FAILED. [Line #" & Erl & "] " & Err.Description & vbCrLf
340           Err.Clear
350       End If
End Function

Private Function zipFolder(strFolderName As String, strOutputName As String) As Boolean
10        On Error GoTo errHandler
          Dim bRet As Boolean
          Dim strOutputFileName As String
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Dim strCmd As String
          Dim lStartTick As Long
          
20        lStartTick = GetTickCount()
          
30        If gobjFSO.FolderExists(strFolderName) Then
40            Trace "        Archiving files (using 7-zip) in " & strFolderName & " to " & strOutputName & " ... "
50            strOutputFileName = gobjFSO.GetBaseName(strOutputName) & "_archive.zip"
60            strCmd = """" & g_str7zipPath & """ a -tzip """ & strOutputFileName & """ " & "*.*"
              
70            If g_blnDirectExecute Then
                  'execute the command
80                bRet = SuperShell(strCmd, strFolderName, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
90            Else
100               strBatchFile = gobjFSO.BuildPath(strFolderName, "toarchive.bat")
                  
110               Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
120               oBatFile.WriteLine strCmd
130               oBatFile.Close
140               Set oBatFile = Nothing
                  
                  'execute the batch file
150               bRet = SuperShell(strBatchFile, strFolderName, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
160               Sleep 1000
170           End If
              
180           DoEvents
              
              'cleanup
              'gobjFSO.DeleteFile strBatchFile
              'safeDelete strBatchFile
              
190           strOutputFileName = gobjFSO.BuildPath(strFolderName, strOutputFileName)
              
200           If gobjFSO.FileExists(strOutputFileName) = False Then
210               Trace "FAILED. Temporary Archive file " & strOutputFileName & " not found" & vbCrLf
220               zipFolder = False
230           Else
                  'setPDFMetadata strOutputFileName, g_strPDFMetaFile
240               If gobjFSO.FileExists(strOutputFileName) Then
250                   strOutputName = getSafeFileName(strOutputName)
260                   gobjFSO.MoveFile strOutputFileName, strOutputName
270                   zipFolder = True
280                   Trace "Done. [" & (GetTickCount() - lStartTick) & " ms]" & vbCrLf
290               Else
300                   zipFolder = False
310                   Trace "FAILED. [Line #" & Erl & "] " & Err.Description & vbCrLf
320               End If
330               bRet = True
340           End If
350       Else
360           bRet = False
370       End If
          
          'If bDeleteOriginal Then safeDelete strFileName
          
errHandler:
380       If Err.Number <> 0 Then
390           zipFolder = False
400           g_strConvertErrMsg = "Error in ZipFolder [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
410           Trace "FAILED. [Line #" & Erl & "] " & Err.Description & vbCrLf
420       End If
End Function


