Attribute VB_Name = "modXMLUtils"
Option Explicit
'Internal error codes for this module.
Public Enum XMLEventCodes
    eDomDataValidationFailed = &H80070000 + &H100
    eXmlInvalid
    eXmlMissingAttribute
    eXmlMissingElement
    eXmlFileNotFound
End Enum

Public Function loadXML(ByRef strXML As String) As MSXML2.DOMDocument
          Const PROC_NAME As String = "loadXML()"
          Dim oXML As MSXML2.DOMDocument
          
10        Set oXML = New MSXML2.DOMDocument
          
20        oXML.async = False
30        oXML.loadXML strXML
          
40        If oXML.parseError.errorCode <> 0 Then
              'raise error
50            Err.Raise XMLEventCodes.eXmlInvalid, PROC_NAME, "Malformed XML. Reason: " & oXML.parseError.reason
60        End If
          
70        Set loadXML = oXML
End Function

Public Function loadXMLFile(strFileName As String) As MSXML2.DOMDocument
          Const PROC_NAME As String = "loadXMLFile()"
10        On Error GoTo errHandler
          Dim oXML As MSXML2.DOMDocument
          
20        If gobjFSO.FileExists(strFileName) Then
30            Set oXML = New MSXML2.DOMDocument
              
40            oXML.async = False
50            oXML.Load strFileName
          
60            If oXML.parseError.errorCode <> 0 Then
                  'raise error
70                Err.Raise XMLEventCodes.eXmlInvalid, PROC_NAME, "Malformed XML. Reason: " & oXML.parseError.reason
80            End If
              
90            Set loadXMLFile = oXML
100       Else
110           Err.Raise eXmlFileNotFound, PROC_NAME, "XML File not found. " & strFileName
120       End If
errHandler:
130       If Err.Number <> 0 Then
140           If InStr(1, APP_TITLE, "Svc", vbTextCompare) > 0 Then
150               Trace "An Error occurred in " & PROC_NAME & " [Line #" & Erl & "] " & Err.Description
160           Else
170               MsgBox "An Error occurred in " & PROC_NAME & " [Line #" & Erl & "] " & Err.Description, vbCritical, APP_TITLE
180           End If
190       End If
End Function

Public Function getXMLValue(ByRef oXML As IXMLDOMNode, strPath As String, Optional strAttrib As String, Optional blnMustExist As Boolean) As String
          Const PROC_NAME As String = "getSetting()"
          Dim strRet As String
          Dim oNode As IXMLDOMElement
10        If strPath <> "" Then
20            Set oNode = oXML.selectSingleNode(strPath)
30            If Not oNode Is Nothing Then
40                If oNode.nodeType = NODE_ATTRIBUTE Then
50                    strRet = oNode.Text
60                ElseIf oNode.nodeType = NODE_ELEMENT Then
70                    If strAttrib <> "" Then
80                        strRet = oNode.getAttribute(strAttrib)
90                    Else
100                       strRet = oNode.Text
110                   End If
120               Else
130                   strRet = ""
140               End If
150           Else
160               If blnMustExist Then
170                   Err.Raise XMLEventCodes.eXmlMissingElement, PROC_NAME, "Cannot find " & strPath & "."
180               End If
190           End If
200       End If
          
210       getXMLValue = strRet
End Function

