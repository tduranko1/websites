VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPackage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Const MODULE_NAME As String = "clsPackage:"

Dim m_ePackageType As PackageTypes
Dim m_strOutputFileName As String
Dim m_strPackageOutputFileName As String
Dim m_objPackage As Collection
Dim m_objXML As MSXML2.DOMDocument

Private Sub Class_Initialize()
    Set m_objXML = New MSXML2.DOMDocument
    Set m_objPackage = New Collection
    m_ePackageType = ePackagePDF
End Sub

Private Sub Class_Terminate()
    Set m_objXML = Nothing
    Set m_objPackage = Nothing
End Sub

Public Property Get Data() As Collection

    Set Data = m_objPackage

End Property

Public Property Get PackageType() As PackageTypes

    PackageType = m_ePackageType

End Property

Public Property Let PackageType(ByVal ePackageType As PackageTypes)
    Select Case ePackageType
        Case ePackagePDF_ZIP, ePackageTIF_ZIP, ePackageZIP
            Err.Raise eNotImplemented, "PackageType", "Package not implemented"
        Case Else
            m_ePackageType = ePackageType
    End Select
End Property

Public Property Get OutputFileName() As String

    OutputFileName = m_strOutputFileName

End Property

Public Property Let OutputFileName(ByVal strOutputFileName As String)

    m_strOutputFileName = strOutputFileName

End Property


'***************************************************************************************
' Procedure : addEmail
' DateTime  : 7/10/2006 16:40
' Author    : Ramesh Vishegu
' Purpose   :
'***************************************************************************************
'
Public Function addEmail(ByVal strFrom As String, ByVal strTo As String, ByVal strCC As String, ByVal strBCC As String, _
                         ByVal strReplyTo As String, ByVal strImportance As String, ByVal strSubject As String, ByVal strBody As String, _
                         Optional ByVal lSizeLimit As Long) As Boolean
    Const PROC_NAME As String = "addEmail"
    Dim bResult As Boolean
    Dim objEmailPackage As MSXML2.IXMLDOMElement
    Dim objNode As IXMLDOMElement
    Dim strKey As String

    Set objEmailPackage = m_objXML.createElement("Email")
    
    If lSizeLimit > 0 Then
        objEmailPackage.setAttribute "SMTPEmailSizeLimit", lSizeLimit
    End If
    
    If strFrom <> "" Then
        Set objNode = m_objXML.createElement("From")
        objNode.Text = strFrom
        objEmailPackage.appendChild objNode
    End If
    
    If strTo <> "" Then
        Set objNode = m_objXML.createElement("To")
        objNode.Text = strTo
        objEmailPackage.appendChild objNode
    Else
        'raise error. no to address specified
        Err.Raise PackageErrorCodes.eMissingToAddress, PROC_NAME, "Missing To email address"
    End If
    
    If strCC <> "" Then
        Set objNode = m_objXML.createElement("CC")
        objNode.Text = strCC
        objEmailPackage.appendChild objNode
    End If
    
    If strBCC <> "" Then
        Set objNode = m_objXML.createElement("BCC")
        objNode.Text = strBCC
        objEmailPackage.appendChild objNode
    End If
    
    If strReplyTo <> "" Then
        Set objNode = m_objXML.createElement("ReplyTo")
        objNode.Text = strReplyTo
        objEmailPackage.appendChild objNode
    End If
    
    If strImportance <> "" Then
        Set objNode = m_objXML.createElement("Importance")
        objNode.Text = strImportance
        objEmailPackage.appendChild objNode
    End If
    
    If strSubject <> "" Then
        Set objNode = m_objXML.createElement("Subject")
        objNode.appendChild m_objXML.createCDATASection(strSubject)
        objEmailPackage.appendChild objNode
    End If
    
    If strBody <> "" Then
        Set objNode = m_objXML.createElement("Body")
        objNode.appendChild m_objXML.createCDATASection(strBody)
        objEmailPackage.appendChild objNode
    End If
    
    strKey = "Email_" & m_objPackage.Count
    
    m_objPackage.Add Item:=objEmailPackage, Key:=strKey
    
    bResult = True
    
    Trace "  Email Package created." & vbCrLf & _
          "    From: " & strFrom & vbCrLf & _
          "    To: " & strTo & vbCrLf & _
          "    CC: " & strCC & vbCrLf & _
          "    BCC: " & strBCC & vbCrLf & _
          "    Reply to: " & strReplyTo & vbCrLf & _
          "    Importance: " & strImportance & vbCrLf & _
          "    Subject: " & strSubject & vbCrLf & _
          "    Body: " & strBody & vbCrLf & _
          "    Size Limit: " & lSizeLimit & vbCrLf
    
    addEmail = bResult

End Function

'***************************************************************************************
' Procedure : addFax
' DateTime  : 7/10/2006 16:40
' Author    : Ramesh Vishegu
' Purpose   :
'***************************************************************************************
'
Public Function addFax(ByVal strFaxNumber As String, ByVal strFaxTitle As String) As Boolean
    Const PROC_NAME As String = "addFax"
    Dim bResult As Boolean
    Dim objFaxPackage As MSXML2.IXMLDOMElement
    Dim objNode As IXMLDOMElement
    Dim strKey As String

    Set objFaxPackage = m_objXML.createElement("Fax")
    
    strFaxNumber = Trim(strFaxNumber)
    strFaxNumber = Replace(strFaxNumber, " ", "")
    strFaxNumber = Replace(strFaxNumber, "-", "")
    strFaxNumber = Replace(strFaxNumber, "(", "")
    strFaxNumber = Replace(strFaxNumber, ")", "")
    
    If strFaxNumber <> "" Then
        If Len(strFaxNumber) > 9 Then
            objFaxPackage.setAttribute "to", strFaxNumber
        Else
            Err.Raise PackageErrorCodes.eInvalidFaxNumber, PROC_NAME, "Invalid Fax number. Must be atleast 10 digits long"
        End If
    Else
        'raise error. Invalid fax number
        Err.Raise PackageErrorCodes.eInvalidFaxNumber, PROC_NAME, "Missing Fax number"
    End If
    
    If strFaxTitle <> "" Then
        objFaxPackage.setAttribute "name", strFaxTitle
    End If

    strKey = "Fax_" & m_objPackage.Count
    
    m_objPackage.Add Item:=objFaxPackage, Key:=strKey
    
    Trace "  Fax Package created." & vbCrLf & _
          "    Fax #: " & strFaxNumber & vbCrLf & _
          "    Name : " & strFaxTitle & vbCrLf
    
    bResult = True
    
    addFax = bResult

End Function

'***************************************************************************************
' Procedure : addFTP
' DateTime  : 7/10/2006 16:40
' Author    : Ramesh Vishegu
' Purpose   :
'***************************************************************************************
'
Public Function addFTP(ByVal strFTPServer As String, ByVal strLoginID As String, ByVal strPassword As String, ByVal strPath As String) As Boolean
    Const PROC_NAME As String = "addFTP"
    Dim bResult As Boolean
    Dim objFTPPackage As MSXML2.IXMLDOMElement
    Dim objNode As IXMLDOMElement
    Dim strKey As String

    Set objFTPPackage = m_objXML.createElement("FTP")
    
    If strFTPServer <> "" Then
        Set objNode = m_objXML.createElement("remoteserver")
        objNode.Text = strFTPServer
        objFTPPackage.appendChild objNode
    Else
        'raise error. Missing FTP Server
        Err.Raise PackageErrorCodes.eMissingFTPServer, PROC_NAME, "Missing FTP Server"
    End If
    
    If strLoginID <> "" Then
        Set objNode = m_objXML.createElement("remoteUID")
        objNode.Text = strLoginID
        objFTPPackage.appendChild objNode
    End If

    If strPassword <> "" Then
        Set objNode = m_objXML.createElement("remotePWD")
        objNode.Text = strPassword
        objFTPPackage.appendChild objNode
    End If

    If strPath <> "" Then
        Set objNode = m_objXML.createElement("path")
        objNode.Text = strPath
        objFTPPackage.appendChild objNode
    End If

    strKey = "FTP_" & m_objPackage.Count
    
    m_objPackage.Add Item:=objFTPPackage, Key:=strKey
    
    Trace "  FTP Package created." & vbCrLf & _
          "    Server: " & strFTPServer & vbCrLf & _
          "    Login ID: " & strLoginID & vbCrLf & _
          "    PWD: " & Asc(strPassword) & vbCrLf & _
          "    Path: " & strPath & vbCrLf
    
    bResult = True
    addFTP = bResult

End Function

'***************************************************************************************
' Procedure : addHTTPPost
' DateTime  : 7/10/2006 16:40
' Author    : Ramesh Vishegu
' Purpose   :
'***************************************************************************************
'
Public Function addHTTPPost(ByVal strHTTPUrl As String, Optional ByVal blnFileEmbed As Boolean = False, Optional ByVal strDocumentNodeName As String = "Document", Optional ByVal strOutputPath As String = "", Optional ByVal strTemplateXML As String = "") As Boolean
    Const PROC_NAME As String = "addHTTPPost"
    Dim bResult As Boolean
    Dim objHTTPPackage As MSXML2.IXMLDOMElement
    Dim objNode As IXMLDOMElement
    Dim strKey As String
    Dim objTemplateXML As MSXML2.DOMDocument
    Dim objTextFile As TextStream

    Set objHTTPPackage = m_objXML.createElement("HTTPPost")
    
    If strHTTPUrl <> "" Then
'        If blnFileEmbed = True Then
'            Err.Raise PackageErrorCodes.eNotImplemented, PROC_NAME, "File Embed not supported in this version."
'        End If
        
        objHTTPPackage.setAttribute "FileEmbed", LCase(CStr(blnFileEmbed))
        
        objHTTPPackage.setAttribute "FilePath", strOutputPath
        
        objHTTPPackage.setAttribute "DocumentNodeName", strDocumentNodeName
        
        If strOutputPath <> "" Then
            If gobjFSO.FolderExists(strOutputPath) = False Then
                Err.Raise PackageErrorCodes.eMissingUNCPath, PROC_NAME, "Invalid UNC Path. Cannot access " & strOutputPath
            Else
                'check for write permission
                On Error GoTo noWriteAccess
                Set objTextFile = gobjFSO.OpenTextFile(gobjFSO.BuildPath(strOutputPath, "txt.txt"), ForWriting, True)
                objTextFile.Write " "
                objTextFile.Close
                Set objTextFile = Nothing
                
                gobjFSO.DeleteFile gobjFSO.BuildPath(strOutputPath, "txt.txt")
                
noWriteAccess:
                If Err.Number <> 0 Then
                    Err.Raise PackageErrorCodes.eNoWritePermission, PROC_NAME, "Unable to write to " & strOutputPath & ". Check the security permission."
                End If
            End If
        End If
        
        Set objNode = m_objXML.createElement("url")
        'objNode.Text = strHTTPUrl
        objNode.appendChild m_objXML.createCDATASection(strHTTPUrl)
        objHTTPPackage.appendChild objNode
        
        If strTemplateXML <> "" Then
            'check if the template XML is valid
            Set objTemplateXML = loadXML(strTemplateXML)
            
            If Not objTemplateXML Is Nothing Then
                'template is a valid XML.
                Set objNode = m_objXML.createElement("PostTemplate")
                objNode.appendChild objTemplateXML.firstChild
                objHTTPPackage.appendChild objNode
            End If
            Set objTemplateXML = Nothing
        End If
        
    Else
        'raise error. Missing HTTP Post URL
        Err.Raise PackageErrorCodes.eMissingHTTPPostURL, PROC_NAME, "Missing HTTPPost URL address"
    End If

    strKey = "HTTP_" & m_objPackage.Count
    
    m_objPackage.Add Item:=objHTTPPackage, Key:=strKey
    
    Trace "  HTTP Post Package created." & vbCrLf & _
          "    URL: " & strHTTPUrl & vbCrLf
    
    bResult = True
    addHTTPPost = bResult
End Function

'***************************************************************************************
' Procedure : addFileCopy
' DateTime  : 7/10/2006 16:40
' Author    : Ramesh Vishegu
' Purpose   :
'***************************************************************************************
'
Public Function addFileCopy(ByVal strUNC As String, Optional ByVal bOverwrite As Boolean = True) As Boolean
    Const PROC_NAME As String = "addFileCopy"
    Dim bResult As Boolean
    Dim objFileCopyPackage As MSXML2.IXMLDOMElement
    Dim objNode As IXMLDOMElement
    Dim strKey As String

    Set objFileCopyPackage = m_objXML.createElement("Filecopy")
    
    If strUNC <> "" Then
        Set objNode = m_objXML.createElement("UNC")
        objNode.appendChild m_objXML.createCDATASection(strUNC)
        objFileCopyPackage.appendChild objNode
    Else
        'raise error. Missing UNC Path
        Err.Raise PackageErrorCodes.eMissingUNCPath, PROC_NAME, "Missing File Copy UNC Path"
    End If

    strKey = "FILE_" & m_objPackage.Count
    
    m_objPackage.Add Item:=objFileCopyPackage, Key:=strKey
    
    Trace "  File Copy Package created." & vbCrLf & _
          "    UNC: " & strUNC & vbCrLf & _
          "    Overwrite: " & bOverwrite & vbCrLf
    
    bResult = True
    addFileCopy = bResult
End Function

'***************************************************************************************
' Procedure : addNotifySuccess
' DateTime  : 7/10/2006 16:42
' Author    : Ramesh Vishegu
' Purpose   :
'***************************************************************************************
'
Public Function addNotifySuccess(ByVal strFrom As String, ByVal strTo As String, ByVal strSubject As String, ByVal strBody As String) As Boolean
    Const PROC_NAME As String = "addNotifySuccess"
    Dim bResult As Boolean
    Dim objNotify As IXMLDOMElement
    Dim objSuccess As IXMLDOMElement
    Dim objNode As IXMLDOMElement
    Dim strKey As String
    
    If strTo <> "" Then
        Set objNotify = m_objXML.createElement("Notify")
        Set objSuccess = m_objXML.createElement("Success")
        
        Set objNode = m_objXML.createElement("From")
        objNode.Text = strFrom
        objSuccess.appendChild objNode
        
        Set objNode = m_objXML.createElement("To")
        objNode.Text = strTo
        objSuccess.appendChild objNode
        
        Set objNode = m_objXML.createElement("Subject")
        objNode.appendChild m_objXML.createCDATASection(strSubject)
        objSuccess.appendChild objNode
        
        Set objNode = m_objXML.createElement("Body")
        objNode.appendChild m_objXML.createCDATASection(strBody)
        objSuccess.appendChild objNode
        
        objNotify.appendChild objSuccess
    
        strKey = "NOTIFY_" & m_objPackage.Count
        
        m_objPackage.Add Item:=objNotify, Key:=strKey
        
        bResult = True
    
    Else
        'raise error. missing to address
        Err.Raise PackageErrorCodes.eMissingToAddress, PROC_NAME, "Missing To email address for notify success"
        bResult = False
    End If
    
    Trace "  Notify Success created." & vbCrLf & _
          "    From: " & strFrom & vbCrLf & _
          "    To: " & strTo & vbCrLf & _
          "    Subject: " & strSubject & vbCrLf & _
          "    Body: " & strBody & vbCrLf

    bResult = True
    addNotifySuccess = bResult

End Function

'***************************************************************************************
' Procedure : addNotifyError
' DateTime  : 7/10/2006 16:42
' Author    : Ramesh Vishegu
' Purpose   :
'***************************************************************************************
'
Public Function addNotifyError(ByVal strFrom As String, ByVal strTo As String, ByVal strSubject As String, ByVal strBody As String) As Boolean
    Const PROC_NAME As String = "addNotifyError"
    Dim bResult As Boolean
    Dim objNotify As IXMLDOMElement
    Dim objError As IXMLDOMElement
    Dim objNode As IXMLDOMElement
    Dim strKey As String
    
    If strTo <> "" Then
        Set objNotify = m_objXML.createElement("Notify")
        Set objError = m_objXML.createElement("Error")
        
        Set objNode = m_objXML.createElement("From")
        objNode.Text = strFrom
        objError.appendChild objNode
        
        Set objNode = m_objXML.createElement("To")
        objNode.Text = strTo
        objError.appendChild objNode
        
        Set objNode = m_objXML.createElement("Subject")
        objNode.appendChild m_objXML.createCDATASection(strSubject)
        objError.appendChild objNode
        
        Set objNode = m_objXML.createElement("Body")
        objNode.appendChild m_objXML.createCDATASection(strBody)
        objError.appendChild objNode
        
        objNotify.appendChild objError
    
        strKey = "NOTIFY_" & m_objPackage.Count
        
        m_objPackage.Add Item:=objNotify, Key:=strKey
        
        bResult = True
    
    Else
        'raise error. missing to address
        Err.Raise PackageErrorCodes.eMissingToAddress, PROC_NAME, "Missing To email address for notify error"
        bResult = False
    End If
    
    Trace "  Notify Error created." & vbCrLf & _
          "    From: " & strFrom & vbCrLf & _
          "    To: " & strTo & vbCrLf & _
          "    Subject: " & strSubject & vbCrLf & _
          "    Body: " & strBody & vbCrLf
    
    addNotifyError = bResult
End Function
