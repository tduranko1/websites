Attribute VB_Name = "modSpooler"
Option Explicit

Public g_blnAppTrace As Boolean
Public g_strAppLogFile As String
Public g_blnAppLogFolderExistChecked As Boolean

Public g_strAppTraceMsgs As String

Public Const APP_TITLE As String = "LYNXDocSpoolerSvc" ' the app title has the svc just to mask any Msgbox displayed. Messages will be logged instead.
Private Declare Function GetUserNameAPI Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long

Public Sub Trace(strMsg As String)
    If g_blnAppTrace Then
        g_strAppTraceMsgs = g_strAppTraceMsgs & strMsg
    End If
End Sub

Public Sub WriteLog(strMsg As String)
          Dim strLogFile As String
          Dim otxtFile As TextStream
          Dim strLogFolder As String
          
10        If g_blnAppTrace Then
20            If g_blnAppLogFolderExistChecked = False Then
                  'check the existance of the log path
30                If gobjFSO.FolderExists(g_strAppLogFile) = False Then
40                    If BuildPath(g_strAppLogFile) = False Then
50                        gobjFSO.CreateFolder "Logs"
60                        g_strAppLogFile = gobjFSO(App.Path, "Logs")
70                    End If
80                End If
90                g_blnAppLogFolderExistChecked = True
100           End If
              
110           strLogFile = gobjFSO.BuildPath(g_strAppLogFile, Format(Now, "yyyy-mm-dd ") & "LYNX Document Spooler.log")
              
120           Set otxtFile = gobjFSO.OpenTextFile(strLogFile, ForAppending, True)
130           otxtFile.Write strMsg
140           otxtFile.Close
150           Set otxtFile = Nothing
160       End If
End Sub

Private Function BuildPath(sPath As String) As Boolean
10        On Error GoTo errHandler
          Const PROC_NAME As String = "BuildPath()"
          Dim sFolders() As String
          Dim sCurrentFolder As String
          Dim i As Integer
          Dim bUNCPath As Boolean
          
20        bUNCPath = (Left(sPath, 2) = "\\")
              
30        sCurrentFolder = ""
          
40        If bUNCPath = False Then
              'create the logging path
50            sFolders = Split(sPath, "\")
60        Else
70            sCurrentFolder = Mid(sPath, 1, InStr(4, sPath, "\") - 1)
80            sFolders = Split(Mid(sPath, InStr(4, sPath, "\") + 1), "\")
90        End If
          
100       For i = 0 To UBound(sFolders)
110           If InStr(1, sFolders(i), ":") = 0 Then
120               If i > 0 Then sCurrentFolder = sCurrentFolder & "\"
130               sCurrentFolder = gobjFSO.BuildPath(sCurrentFolder, sFolders(i))
                  
140               If Not gobjFSO.FolderExists(sCurrentFolder) Then gobjFSO.CreateFolder (sCurrentFolder)
150           Else
160               sCurrentFolder = sCurrentFolder & sFolders(i)
170           End If
180       Next
190       BuildPath = True
errHandler:
200       If Err.Number <> 0 Then
210           BuildPath = False
220       End If
End Function

Public Function GetUserName() As String
          Dim strName As String
          Dim lngSize As Long
10        Call GetUserNameAPI(strName, lngSize)
20        strName = Space$(lngSize)
30        Call GetUserNameAPI(strName, lngSize)
40        strName = Left$(strName, Len(strName) - 1)

50        GetUserName = strName
End Function

