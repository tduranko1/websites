Attribute VB_Name = "modFaxUtils"
Option Explicit

Option Compare Text

Const MODULE_NAME As String = "modFaxUtils:"

'Error codes specific to this class.
Private Enum Error_Codes
    eFaxServerFailed = &H80064800 + &H100
    eNoFaxServerAvailable
    eXmlParseError
    eXslParseError
    eRightFaxError
    eBookmarkNotFound
    eMethodObsolete
End Enum

'********************************************************************************
'* Syntax:      object.SendFaxDocument "C:\MyDocument.doc", strFaxNumber
'* Parameters:  strFilePath = The Word document path.
'*              strFaxNumber = The fax number.
'* Purpose:     Send a fax to the fax server queue.
'* Returns:     Nothing
'********************************************************************************
Public Function SendFaxDocument( _
    ByVal strToName As String, _
    ByVal strFilePath As String, _
    ByVal strFaxNumber As String) As Boolean

          Const PROC_NAME As String = MODULE_NAME & "SendFaxDocument: "

          Dim objXmlDoc As MSXML2.DOMDocument40
          Dim objXslDoc As MSXML2.DOMDocument40
          Dim objFSO As Scripting.FileSystemObject

          Dim i As Integer, strResult As String
          Dim strInsCoID As String
          Dim strServers() As String
          Dim strErrDesc() As String

          Dim strDescription As String, strSource As String, lngNumber As Long
          Dim strFaxOutputPath(0) As String, strFaxServer As String
          Dim strFaxUser As String, strFaxPrinter As String, strFaxTemplate As String
          Dim intServerCount As Integer
          Dim strInsuranceCompanyID As Long
          Dim strServiceChannelCD As String
          Dim strDestinationType As String
          Dim strFaxTemplateSuffix As String

10        On Error GoTo ErrorHandler

20        ReDim strErrDesc(0)
30        ReDim strServers(0)

40        SendFaxDocument = True

      '    InitializeGlobals
      '
      '    'Validate parameters
      '    g_objEvents.Assert CBool(strFilePath <> ""), "Empty File path"
      '    g_objEvents.Assert CBool(strFaxNumber <> ""), "Shop Fax number is blank"
      '
      '    If g_blnDebugMode Then g_objEvents.Trace "  Document Path = " & strFilePath & vbCrLf & _
      '                                             "  Fax Number = " & strFaxNumber, PROC_NAME

50        Set objXmlDoc = New MSXML2.DOMDocument40
60        Set objXslDoc = New MSXML2.DOMDocument40
70        Set objFSO = New Scripting.FileSystemObject

80        If objFSO.FileExists(strFilePath) Then
              'Extract some configuration information for this entity.
90            strFaxPrinter = g_strFaxPrinter
100           strFaxUser = g_strFaxUser
110           strFaxOutputPath(0) = g_strFaxWorkFolder & "\"
120           strFaxServer = g_strFaxServerList

130           On Error Resume Next

              'Use fax override number if available.
140           If Len(g_strFaxNumberOverride) > 0 Then
150               strFaxNumber = g_strFaxNumberOverride
      '            g_objEvents.Trace "USING FAX SERVER OVERRIDE TO " & strFaxNumber, PROC_NAME
160           End If

              'Add on the '1' to the phone number if needed.
170           If Len(strFaxNumber) > 8 Then
180               If Left(strFaxNumber, 1) <> "1" Then
190                   strFaxNumber = "1" & strFaxNumber
200               End If
210           End If

      '        'Trace out some of this nifty information.
      '        If g_blnDebugMode Then g_objEvents.Trace _
      '              "  FaxPrinter = " & strFaxPrinter & vbCrLf _
      '            & "  FaxUser = " & strFaxUser & vbCrLf _
      '            & "  FaxNumber = " & strFaxNumber, PROC_NAME


220           strFaxOutputPath(0) = strFilePath

              'Split up the server list.
230           strServers = Split(strFaxServer, ",")

240           intServerCount = UBound(strServers)

250           ReDim strErrDesc(intServerCount)

260           On Error Resume Next

              'Loop through the servers and try them one by one.
270           For i = LBound(strServers) To UBound(strServers)

      '            If g_blnDebugMode Then g_objEvents.Trace strServers(i), PROC_NAME & "Faxing to " & strFaxNumber

                  'Try to send it.
280               QueueFax strServers(i), strFaxUser, strFaxOutputPath, _
                      strToName, strFaxNumber

                  'If no problems then exit.
290               If Err.Number = 0 Then

300                   On Error GoTo ErrorHandler
                      
310                   SendFaxDocument = True

      '                If g_blnDebugMode Then g_objEvents.Trace "Server = " & strServers(i), PROC_NAME & "Success!"

320                   Exit For

                  'An error occurred!
330               Else

340                   SendFaxDocument = False
                      
                      'Log the failure as a warning.
                      'g_objEvents.HandleEvent eFaxServerFailed, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, "Server = " & strServers(i), "", False
350                   strErrDesc(i) = Err.Description

                      'If we are all out of servers to try then raise it back as an error.
360                   If i = UBound(strServers) Then
370                       On Error GoTo ErrorHandler
       '                   Err.Raise eNoFaxServerAvailable, PROC_NAME, strFaxServer & Err.Description
380                   End If

                      'Otherwise clear the error and try the next fax server.
390                   Err.Clear
400               End If

410           Next
420       Else
              'file does not exist. throw an error
430           SendFaxDocument = False
440       End If

      '    If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Exiting"

ErrorHandler:

          'Store the true error info in case the Kill call below tosses.
450       strDescription = Err.Description
460       strSource = " [Line " & Erl & "] " & PROC_NAME & Err.Source
470       lngNumber = Err.Number

480       Err.Clear
490       On Error Resume Next

500       Set objXmlDoc = Nothing
510       Set objXslDoc = Nothing
520       Set objFSO = Nothing

530       Err.Clear

540       If lngNumber <> 0 Then
550           On Error GoTo 0
560           g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & PROC_NAME & " Line #" & Erl & ". Description: " & Err.Description
570       End If

End Function


'********************************************************************************
'* Syntax:      object.QueueFax
'* Parameters:  None
'* Purpose:     Send a fax to the fax server queue
'* Returns:     Nothing
'* Created:     09/13/2001
'********************************************************************************
Private Sub QueueFax( _
    ByVal strFaxServer As String, _
    ByVal strFaxUser As String, _
    ByRef strFaxOutputPath() As String, _
    ByVal strDestination As String, _
    ByVal strFaxNumber As String)
          
          Const PROC_NAME As String = MODULE_NAME & "QueueFax"

          Dim hndServer As Long
          Dim lngError As Long
          Dim lngHandle As Long
          Dim lngFI10Obj As Long
          Dim lngNI10Obj As Long

10        On Error GoTo ErrorHandler

20        hndServer = RFaxCreateServerHandle(strFaxServer, COMMPROTOCOL_TCPIP, lngError)
30        If (lngError <> 0) Then
40            Err.Raise eRightFaxError, PROC_NAME & "RFaxCreateServerHandle()", "Unable to send fax.  Fax server error = " & lngError
50        End If

60        If (hndServer <> 0) Then

70            lngError = RFVB_New(RFO_FAXINFO_10, lngFI10Obj)
80            If (lngError <> 0) Then
90                Err.Raise eRightFaxError, PROC_NAME & "RFVB_New()", "Unable to send fax.  Fax server error = " & lngError
100           End If


             ' We have to initialize a FaxInfo_10 object based on the owner of the fax
             ' This is why we call the RFaxInitFax() function.  If we didn't, the FaxInfo_10
             ' object would not be setup properly.
110           lngError = RFaxInitFax(hndServer, lngFI10Obj, strFaxUser)
120           If (lngError <> 0) Then
130               Err.Raise eRightFaxError, PROC_NAME & "RFaxInitFax()", "Unable to send fax.  Fax server error = " & lngError
140           End If

150           If (lngError = 0) Then
160               lngError = RFVB_SetBString(lngFI10Obj, RFO_FI10_TO_NAME, strDestination)
170               If (lngError <> 0) Then
180                   Err.Raise eRightFaxError, PROC_NAME & "RFVB_SetBString()", "Unable to send fax.  Fax server error = " & lngError
190               End If

200               lngError = RFVB_SetBString(lngFI10Obj, RFO_FI10_TO_FAXNUM, strFaxNumber)
210               If (lngError <> 0) Then
220                   Err.Raise eRightFaxError, PROC_NAME & "RFVB_SetBString()", "Unable to send fax.  Fax server error = " & lngError
230               End If

240               lngError = RFVB_SetInteger(lngFI10Obj, RFO_FI10_PAPERNUM, -1) ' No form overlay
250               If (lngError <> 0) Then
260                   Err.Raise eRightFaxError, PROC_NAME & "RFVB_Integer()", "Unable to send fax.  Fax server error = " & lngError
270               End If

280               lngError = RFVB_SetBString(lngFI10Obj, RFO_FI10_TO_COMPANY, strDestination)
290               If (lngError <> 0) Then
300                   Err.Raise eRightFaxError, PROC_NAME & "RFVB_SetBString()", "Unable to send fax.  Fax server error = " & lngError
310               End If

                ' Now send the file(s).  With the RFVB_SendFiles1() function, which uses the RFaxSendCVL() function
                ' underneith, the files we are sending cannot have embedded codes within them; well, they can
                ' but the server will not scan the files for codes.  This is due to the fact that information in the
                ' FAXINFO_10 and NOTEINFO_10 objects would be ambiguous with any embedded codes found.

320               lngError = RFVB_SendFiles1(hndServer, lngFI10Obj, lngNI10Obj, 0, "", strFaxOutputPath(), lngHandle)
330               If (lngError <> 0) Then
340                   Err.Raise eRightFaxError, PROC_NAME & "RFVB_SendFiles1()", "Unable to send fax.  Fax server error = " & lngError
350               End If
360           End If

370       End If

ErrorHandler:

380       If (lngNI10Obj <> 0) Then
390           RFVB_Delete (lngNI10Obj)
400           lngNI10Obj = 0
410       End If

420       If (lngFI10Obj <> 0) Then
430           RFVB_Delete (lngFI10Obj)
440           lngFI10Obj = 0
450       End If

460       If (hndServer <> 0) Then
470           RFaxCloseServerHandle (hndServer)
480           hndServer = 0
490       End If

500       If Err.Number <> 0 Then

              'g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
              '    "Server = " & strFaxServer & "  User = " & strFaxUser, "", False

              'We don't call HandleEvent because we want the calling
              'process to be able to supress or log errors as it sees fit.
510           Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
520       End If

End Sub
