Attribute VB_Name = "modDocService"
Option Explicit
Option Compare Text
Const MODULE_NAME As String = "modDocService"
Public Const APP_TITLE As String = "LYNXDocProcSvc"
'Paths
Private Const blnInitialTrace As Boolean = False
'Public g_strPickupPath As String
Public g_strOutboxPath As String
Public g_strBadJobPath As String
Public g_strSortTempPath As String
Public g_strArchivePath As String
Public g_blnArchive As Boolean

'This is the UNC Path the callers will access the file. Make sure this is shared and have read, write, and delete permissions set to Everyone
Public g_strOutboxPath2External As String

'SMTP settings
Public g_strSMTPServer As String
Public g_lngSMTPEmailSize As Long
Public g_intSMTPTimeout As Integer
Public g_strDefaultEmailFrom As String

'HTTP Post settings
Public g_lngPostBytesLimit As Long

'Fax Server settings
Public g_strFaxServerList As String
Public g_strFaxPrinter As String
Public g_strFaxUser As String
Public g_strFaxWorkFolder As String

'PDF Metadata settings
Public g_strPDFAuthor As String
Public g_strPDFCreator As String
Public g_strPDFProducer As String

'Tools
Public g_strGSPath As String
Public g_strPDFTKPath As String
Public g_strIMPath As String
Public g_strPDFPrinterName As String
Public g_strHTMLDoc As String
Public g_blnDirectExecute As Boolean
Public g_str7zipPath As String
Public g_strFTPClient As String

'HTMLDoc settings
Public g_strHTMLHeader As String
Public g_strHTMLFooter As String
Public g_strHTMLTopMargin As String
Public g_strHTMLLeftMargin As String
Public g_strHTMLBottomMargin As String
Public g_strHTMLRightMargin As String

'Error Notification
Public g_strAppErrorTo As String
Public g_strAppErrorSubject As String

'Jobs Queue
Public g_objJobsQueueXML As MSXML2.DOMDocument
Public g_objJobsQueueRoot As IXMLDOMNode
Public g_iHighPriorityCount As Integer
Public g_iNormalPriorityCount As Integer
Public g_iLowPriorityCount As Integer
Public g_strPDFMetaFile As String

'Overrides
Public g_strFaxNumberOverride As String
Public g_strEmailToOverride As String
Public g_strFTPOverride As String
Public g_strHTTPOverride As String
Public g_strFileCopyOverride As String

Public g_intJPGMaxHeight As Long
Public g_intJPGMaxWidth As Long

'Application Log
Public g_blnAppTrace As Boolean
Public g_strAppLogFile As String
Public g_blnAppLogFolderExistChecked As Boolean


Public g_strConvertErrMsg As String
Public g_strDocumentsSent As String
Public g_strDestinationErrMsg As String
Public g_blnStopProcessing As Boolean
Public g_strCommandParms As String
Public g_strUserName As String

Public g_dteLastNotify As Date
Public g_strLastJobID As String
Public g_dteLastDestinationProcessed As Date
Public g_sComputerName As String

Public g_blnOverrides As Boolean
Public g_blnCoversheet As Boolean

Public g_strJobID As String

Public Const SHELL_TIMEOUT As Long = 120000 '2 minute

Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long


Sub Main()
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & ":main()"
          Dim bSettingsLoaded As Boolean
          
20        g_sComputerName = GetWinComputerName
          
30        g_strCommandParms = Command
          
40        InitialTrace "Application started at " & Format(Now, "mm/dd/yyyy hh:nn:ss") & vbCrLf & _
                       "Command line: " & g_strCommandParms & vbCrLf & _
                       "Initializing globals..."
          
50        If App.PrevInstance Then
60            InitialTrace "Another instance of this application is already running. Cannot have multiple instances." & vbCrLf
70            End
80        End If
          
          'initialize globals
90        initGlobals
          
100       InitialTrace "  DONE." & vbCrLf & _
                       "Loading configuration file..."
          
          'load the configuration file
110       loadConfig
          
120       InitialTrace "  DONE." & vbCrLf & _
                       "Reading settings from configuration file..."

          'load setting from the config
130       loadSettings
          
140       InitialTrace "  DONE."
          
150       If g_strCommandParms <> "" Then
160           InitialTrace "Processing command line " & g_strCommandParms
170           processCommandLine
              
180           InitialTrace "  DONE."
190       End If
          
200       InitialTrace "Normal Application log begins."
          
210       Trace vbCrLf & vbCrLf & "====== LYNX Document Processor application executed " & Format(Now, "mm/dd/yyyy hh:nn:ss") & " ======" & vbCrLf
          
220       Trace "Command line parameters: " & IIf(g_strCommandParms <> "", g_strCommandParms, "NONE.") & vbCrLf
          

230       If validateSettings() = False Then
240           Trace "====== LYNX Document Processor application shutdown " & Format(Now, "mm/dd/yyyy hh:nn:ss") & " ======" & vbCrLf
250           appTerminate
260       Else
270           Trace "Configuration settings validated." & vbCrLf
280       End If
          
          'initialize the queue list
290       Set g_objJobsQueueXML = New MSXML2.DOMDocument
          
          'load the queue
300       If gobjFSO.FileExists(g_strJobQueue) Then
310           Set g_objJobsQueueXML = loadXMLFile(g_strJobQueue)
320       Else
330           Set g_objJobsQueueXML = loadXML("<Jobs/>")
340       End If
          
350       Set g_objJobsQueueRoot = g_objJobsQueueXML.SelectSingleNode("/Jobs")

          'trace the override and limit settings
360       Trace "***********************************************************************" & vbCrLf & _
                "* Special settings (if any):" & vbCrLf
          
370       If g_strFaxNumberOverride <> "" Then Trace "  FAX override to: " & g_strFaxNumberOverride & vbCrLf
380       If g_strEmailToOverride <> "" Then Trace "  Email override to: " & g_strEmailToOverride & vbCrLf
390       If g_strFTPOverride <> "" Then Trace "  FTP override to: " & g_strFTPOverride & vbCrLf
400       If g_strHTTPOverride <> "" Then Trace "  HTTP override to: " & g_strHTTPOverride & vbCrLf
410       If g_strFileCopyOverride <> "" Then Trace "  File Copy override to: " & g_strFileCopyOverride & vbCrLf
420       If g_intJPGMaxHeight > 0 Then Trace "  Image Max Height: " & g_intJPGMaxHeight & vbCrLf
430       If g_intJPGMaxWidth > 0 Then Trace "  Image Max Width: " & g_intJPGMaxWidth & vbCrLf
440       If g_lngSMTPEmailSize > 0 Then Trace "  Max Email Size: " & g_lngSMTPEmailSize & vbCrLf
450       If g_intSMTPTimeout > 0 Then Trace "  SMTP Timeout: " & g_intSMTPTimeout & vbCrLf
460       If g_lngPostBytesLimit > 0 Then Trace "  Max HTTP POST bytes: " & g_lngPostBytesLimit & vbCrLf
470       Trace "  Job Archive: " & g_blnArchive & vbCrLf
480       Trace "  Direct Execution of tools: " & g_blnDirectExecute & vbCrLf
490       Trace "  Application Error Email to: " & g_strAppErrorTo & vbCrLf
          
500       Trace "***********************************************************************" & vbCrLf
          
          'load the form so the service will stay up and running
510       Load frmMainSvc
          
520       reloadQueue
          
530       frmMainSvc.tmrSvc.Enabled = True
errHandler:
540       If Err.Number <> 0 Then
550           appError "Error in " & PROC_NAME & " [Line #" & Erl & "] Reason: " & Err.Description & vbCrLf
560           appTerminate
570       End If
End Sub

Private Sub appTerminate()
10        End
End Sub

Public Sub loadSettings()
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & ":loadSettings()"
          
          'load settings
20        g_strQueuePath = resolveMacro(getSetting("/DocProcessor/Paths/Queue", , True))
30        g_strPickupPath = resolveMacro(getSetting("/DocProcessor/Paths/Pickup", , True))
40        g_strOutboxPath = resolveMacro(getSetting("/DocProcessor/Paths/Outbox", , True))
50        g_strBadJobPath = resolveMacro(getSetting("/DocProcessor/Paths/BadJob", , True))
60        g_strSortTempPath = resolveMacro(getSetting("/DocProcessor/Paths/SortTemp", , True))
          
70        g_blnArchive = IIf(getSetting("/DocProcessor/Paths/Archive", "enabled", False) = "true", True, False)
80        g_strArchivePath = resolveMacro(getSetting("/DocProcessor/Paths/Archive", , False))
          
90        If g_blnArchive Then
100           If g_strArchivePath = "" Then g_strArchivePath = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(g_strQueuePath), "Archive")
110           BuildPath g_strArchivePath
120       End If
          
130       g_strOutboxPath2External = "\\" & GetWinComputerName() & "\" & gobjFSO.GetBaseName(g_strOutboxPath)
          
          'SMTP settings
140       g_strSMTPServer = resolveMacro(getSetting("/DocProcessor/SMTPServer", "server", False))
150       g_intSMTPTimeout = IIf(getSetting("/DocProcessor/SMTPServer", "timeout", False) <> "", CInt(getSetting("/DocProcessor/SMTPServer", "timeout", False)), 300)
          
          'HTTP Post bytes limit. Basically an IIS limit. IIS upload limit can be increased by changing "AspMaxRequestEntityAllowed" attribute
          ' in C:\windows\sytem32\inetserv\metabase.xml file. The data specified in this metabase file in bytes.
160       g_lngPostBytesLimit = IIf(getSetting("/DocProcessor/HTTPPost", "bytesLimit", False) <> "", CLng(getSetting("/DocProcessor/HTTPPost", "bytesLimit", False)), 2097152)
          
170       If g_strSMTPServer = "local" Or g_strSMTPServer = "" Then
              'use the local machine name
180           g_strSMTPServer = g_strMachineName
190       End If
          
200       g_strDefaultEmailFrom = getSetting("/DocProcessor/SMTPServer", "defaultFrom", False)
          
210       If g_strDefaultEmailFrom = "" Then
220           g_strDefaultEmailFrom = "lynxserv@ppg.com"
230       End If
          
240       g_lngSMTPEmailSize = resolveMacro(getSetting("/DocProcessor/SMTPServer", "emailMaxSize", False))
250       If g_lngSMTPEmailSize < 0 Then
260           g_lngSMTPEmailSize = 0 'no limit
270       End If
          
          'Fax Server settings
280       g_strFaxPrinter = resolveMacro(getSetting("/DocProcessor/Fax/FaxPrinter", , True))
290       g_strFaxServerList = resolveMacro(getSetting("/DocProcessor/Fax/FaxServer", , True))
300       g_strFaxUser = resolveMacro(getSetting("/DocProcessor/Fax/FaxUser", , True))
310       g_strFaxWorkFolder = gobjFSO.GetSpecialFolder(TemporaryFolder).Path
              
          'PDF Metadata
320       g_strPDFAuthor = resolveMacro(getSetting("/DocProcessor/PDFMetadata/Author", , False))
330       g_strPDFCreator = resolveMacro(getSetting("/DocProcessor/PDFMetadata/Creator", , False))
340       g_strPDFProducer = resolveMacro(getSetting("/DocProcessor/PDFMetadata/Producer", , False))
          
          'Tools
350       g_strGSPath = resolveMacro(getSetting("/DocProcessor/Tools/Ghostscript/Path", , True))
360       g_strPDFTKPath = resolveMacro(getSetting("/DocProcessor/Tools/PDFTK/Path", , True))
370       g_strIMPath = resolveMacro(getSetting("/DocProcessor/Tools/ImageMagick/Path", , True))
380       g_strPDFPrinterName = getSetting("/DocProcessor/Tools/PDFPrinter/Name", "", True)
390       g_strHTMLDoc = getSetting("/DocProcessor/Tools/HTML2DOC/Path", "", True)
400       g_str7zipPath = getSetting("/DocProcessor/Tools/Zip7/Path", "", True)
410       g_strFTPClient = getSetting("/DocProcessor/Tools/FTPClient/Path", "", True)
          
420       g_blnDirectExecute = (getSetting("/DocProcessor/Tools", "directExecute", False) = "true")
          
          'HTML Doc settings
430       g_strHTMLHeader = getSetting("/DocProcessor/Tools/HTML2DOC/Options", "header", False)
440       g_strHTMLFooter = getSetting("/DocProcessor/Tools/HTML2DOC/Options", "footer", False)
450       g_strHTMLTopMargin = getSetting("/DocProcessor/Tools/HTML2DOC/Options", "topMargin", False)
460       g_strHTMLLeftMargin = getSetting("/DocProcessor/Tools/HTML2DOC/Options", "leftMargin", False)
470       g_strHTMLBottomMargin = getSetting("/DocProcessor/Tools/HTML2DOC/Options", "bottomMargin", False)
480       g_strHTMLRightMargin = getSetting("/DocProcessor/Tools/HTML2DOC/Options", "rightMargin", False)
          
          'Error Notification
490       g_strAppErrorTo = getSetting("/DocProcessor/Error/To", , False)
500       g_strAppErrorSubject = getSetting("/DocProcessor/Error/Subject", , False)
          
          'overrides
510       g_strFaxNumberOverride = resolveMacro(getSetting("/DocProcessor/Overrides/Fax", , False))
520       g_strEmailToOverride = resolveMacro(getSetting("/DocProcessor/Overrides/EmailTo", , False))
530       g_strFTPOverride = resolveMacro(getSetting("/DocProcessor/Overrides/FTP", , False))
540       g_strHTTPOverride = resolveMacro(getSetting("/DocProcessor/Overrides/HTTP", , False))
550       g_strFileCopyOverride = resolveMacro(getSetting("/DocProcessor/Overrides/FileCopy", , False))
          
          'image restrictions
560       If getSetting("/DocProcessor/Image", "maxHeight", False) <> "" Then
570           g_intJPGMaxHeight = getSetting("/DocProcessor/Image", "maxHeight", False)
580       Else
590           g_intJPGMaxHeight = 0
600       End If
          
610       If getSetting("/DocProcessor/Image", "maxWidth", False) <> "" Then
620           g_intJPGMaxWidth = getSetting("/DocProcessor/Image", "maxWidth", False)
630       Else
640           g_intJPGMaxWidth = 0
650       End If
          
          
          'App Logs
660       g_blnAppTrace = Not (getSetting("/DocProcessor/AppLog", "enabled", False) = "false") 'default turned on
670       g_strAppLogFile = getSetting("/DocProcessor/AppLog", , False)
          
680       If g_strAppLogFile = "" And g_blnAppTrace = True Then
690           g_strAppLogFile = "C:\Logs\LYNX Doc Processor"
700       End If
          
710       g_strJobQueue = gobjFSO.BuildPath(g_strQueuePath, "Jobs.xml")
          
errHandler:
720       If Err.Number <> 0 Then
730           appError "Error in " & PROC_NAME & " [Line #" & Erl & "] Reason: " & Err.Description & vbCrLf
740           appTerminate
750       End If
End Sub

Private Function validateSettings() As Boolean
          Dim bValid As Boolean
          Dim prnt As Printer
          Dim bPDFPrinterFound As Boolean
          
10        bValid = True
          
          'must have a PDF Printer (PDFCreator) installed
20        If g_strPDFPrinterName = "" Then
              '**** Error. Missing PDF Printer
30            Trace "ERROR: PDF Printer not defined in configuration."
40            bValid = False
50        Else
              'check the printer list if the printer exists
              
60            bPDFPrinterFound = False
70            For Each prnt In Printers
80                If prnt.DeviceName = g_strPDFPrinterName Then
90                    bPDFPrinterFound = True
100                   Exit For
110               End If
120           Next
              
130           If bPDFPrinterFound = False Then
                  '**** Error. Missing PDF Printer.
140               Trace "ERROR: Missing PDF Printer [" & g_strPDFPrinterName & "]" & vbCrLf
150               bValid = False
160           End If
170       End If
          
180       If g_strGSPath = "" Then
              '**** Error
190           Trace "ERROR: Ghostscript executable not defined in configuration." & vbCrLf
200           bValid = False
210       End If
          
220       If gobjFSO.FileExists(g_strGSPath) = False Then
              '**** Error
230           Trace "ERROR: Missing Ghostscript executable [" & g_strGSPath & "]" & vbCrLf
240           bValid = False
250       End If
          
260       If g_strPDFTKPath = "" Then
              '**** Error
270           Trace "ERROR: PDFTK executable not defined in configuration." & vbCrLf
280           bValid = False
290       End If
          
300       If gobjFSO.FileExists(g_strPDFTKPath) = False Then
              '**** Error
310           Trace "ERROR: Missing PDFTK executable [" & g_strPDFTKPath & "]" & vbCrLf
320           bValid = False
330       End If
          
340       If g_strIMPath = "" Then
              '**** Error
350           Trace "ERROR: ImageMagick executable not defined in configuration." & vbCrLf
360           bValid = False
370       End If
          
380       If gobjFSO.FileExists(g_strIMPath) = False Then
              '**** Error
390           Trace "ERROR: Missing ImageMagick executable [" & g_strIMPath & "]" & vbCrLf
400           bValid = False
410       End If
          
420       If g_str7zipPath = "" Then
              '**** Error
430           Trace "ERROR: 7-zip command line executable (7za.exe) not defined in configuration." & vbCrLf
440           bValid = False
450       End If
          
460       If gobjFSO.FileExists(g_str7zipPath) = False Then
              '**** Error
470           Trace "ERROR: Missing 7-zip command line executable [" & g_str7zipPath & "]" & vbCrLf
480           bValid = False
490       End If
          
500       If g_strFTPClient = "" Then
              '**** Error
510           Trace "ERROR: WinSCP.exe FTP Client not defined in configuration." & vbCrLf
520           bValid = False
530       End If
          
540       If gobjFSO.FileExists(g_strFTPClient) = False Then
              '**** Error
550           Trace "ERROR: Missing WinSCP.exe command line executable [" & g_strFTPClient & "]" & vbCrLf
560           bValid = False
570       End If
          
580       If gobjFSO.FolderExists(g_strPickupPath) = False Then
              '**** Error
590           Trace "ERROR: Folder does not exist. " & g_strPickupPath & "" & vbCrLf
600           bValid = False
610       End If
          
620       If gobjFSO.FolderExists(g_strQueuePath) = False Then
              '**** Error
630           Trace "ERROR: Folder does not exist. " & g_strQueuePath & "" & vbCrLf
640           bValid = False
650       End If
          
660       If gobjFSO.FolderExists(g_strOutboxPath) = False Then
              '**** Error
670           Trace "ERROR: Folder does not exist. " & g_strOutboxPath & "" & vbCrLf
680           bValid = False
690       End If
          
700       If gobjFSO.FolderExists(g_strOutboxPath2External) = False Then
              '**** Error
710           Trace "ERROR: Missing share " & g_strOutboxPath2External & "" & vbCrLf
720           bValid = False
730       End If
          
740       If gobjFSO.FolderExists(g_strBadJobPath) = False Then
              '**** Error
750           Trace "ERROR: Folder does not exist. " & g_strBadJobPath & "" & vbCrLf
760           bValid = False
770       End If
          
780       If gobjFSO.FolderExists(g_strSortTempPath) = False Then
              '**** Error
790           Trace "ERROR: Folder does not exist. " & g_strSortTempPath & "" & vbCrLf
800           bValid = False
810       End If
          
820       validateSettings = bValid
End Function

Public Sub reloadQueue()
10        Trace "   Reloading jobs queue..."
          'load the jobs
20        g_iHighPriorityCount = getJobsQueue("high")
30        g_iNormalPriorityCount = getJobsQueue("normal")
40        g_iLowPriorityCount = getJobsQueue("low")
50        Trace "Done [" & CStr(g_iHighPriorityCount) & " high; " & CStr(g_iNormalPriorityCount) & " normal; " & CStr(g_iLowPriorityCount) & " low]." & vbCrLf
End Sub

Private Function getJobsQueue(strPriority As String) As Integer
          Dim strFileName As String
          Dim strFilePattern As String
          Dim iRowIndex As Integer
          Dim strPriorityTxt As String
          Dim strPriorityPrefix As String
          Dim strJobID As String
          Dim objJobQueueNode As IXMLDOMElement
          Dim objJobNode As IXMLDOMElement
          Dim objJobXML As MSXML2.DOMDocument
          Dim strJobDesc As String
          Dim dteFileModified As Date
          
          Dim iRet As Integer
          
10        Select Case strPriority
              Case "high":
20                strPriorityPrefix = HIGH_PRIORITY
30            Case "normal":
40                strPriorityPrefix = NORMAL_PRIORITY
50            Case "low":
60                strPriorityPrefix = LOW_PRIORITY
70        End Select
          
80        strFilePattern = gobjFSO.BuildPath(g_strQueuePath, strPriorityPrefix & "*.*")
90        strFileName = Dir(strFilePattern)
100       While strFileName <> ""
110           strFileName = gobjFSO.BuildPath(g_strQueuePath, strFileName)
120           strJobID = Mid(gobjFSO.GetBaseName(strFileName), 3)
130           dteFileModified = FileDateTime(strFileName)
              
              'let us wait for the job file write to complete and we will give about 10 seconds
140           If DateDiff("s", dteFileModified, Now) >= 10 Then
150               Set objJobNode = g_objJobsQueueXML.SelectSingleNode("/Jobs/Job[@id='" & strJobID & "']")
160               If objJobNode Is Nothing Then
                      'job not in queue. add to the queue now
170                   Set objJobXML = loadXMLFile(strFileName)
180                   If Not objJobXML Is Nothing Then
                          'reuse the objJobNode object
190                       Set objJobNode = objJobXML.SelectSingleNode("/Job")
200                       If Not objJobNode Is Nothing Then
210                           strJobDesc = IIf(IsNull(objJobNode.getAttribute("description")), "", objJobNode.getAttribute("description"))
220                       End If
                          
                          'add job to the queue
230                       Set objJobQueueNode = g_objJobsQueueXML.createElement("Job")
240                       objJobQueueNode.setAttribute "id", strJobID
250                       objJobQueueNode.setAttribute "description", strJobDesc
260                       objJobQueueNode.setAttribute "priority", LCase(strPriority)
270                       objJobQueueNode.setAttribute "status", "Waiting"
280                       objJobQueueNode.setAttribute "file", strFileName
                          
290                       g_objJobsQueueRoot.appendChild objJobQueueNode
300                   End If
310               Else
                      'job already defined in the queue. check if its priority changed
320                   strPriorityTxt = IIf(IsNull(objJobNode.getAttribute("status")), "", objJobNode.getAttribute("status"))
                      
330                   If strPriorityTxt <> "Processing" And strPriorityTxt <> strPriority Then
340                       objJobNode.setAttribute "priority", LCase(strPriority)
350                       objJobNode.setAttribute "file", strFileName
360                   End If
370               End If
380           End If
              
390           strFileName = Dir()
400       Wend
          
410       g_objJobsQueueXML.Save g_strJobQueue
          
420       getJobsQueue = g_objJobsQueueRoot.SelectNodes("Job[@priority='" & strPriority & "']").Length
End Function

'***************************************************************************************
' Procedure : appError
' DateTime  : 7/7/2006 10:49
' Author    : Ramesh Vishegu
' Purpose   : Use this procedure to log messages before the application has initialized
'***************************************************************************************
'
Public Sub appError(strMsg)
          Dim oTxtFile As TextStream
          
10        Set oTxtFile = gobjFSO.OpenTextFile(gobjFSO.BuildPath(App.Path, "Error.log"), ForWriting, True)
20        oTxtFile.WriteLine strMsg
30        oTxtFile.Close
40        Set oTxtFile = Nothing
End Sub

'***************************************************************************************
' Procedure : Trace
' DateTime  : 7/7/2006 10:54
' Author    : Ramesh Vishegu
' Purpose   : Application log
'***************************************************************************************
'
Public Sub Trace(strMsg As String)
          Dim strLogFile As String
          Dim oTxtFile As TextStream
          Dim strLogFolder As String
    
10        If g_blnAppTrace Then
20            If g_blnAppLogFolderExistChecked = False Then
                  'check the existance of the log path
30                If gobjFSO.FolderExists(g_strAppLogFile) = False Then
40                    If BuildPath(g_strAppLogFile) = False Then
50                        gobjFSO.CreateFolder "Logs"
60                        g_strAppLogFile = gobjFSO(App.Path, "Logs")
70                    End If
80                End If
90                g_blnAppLogFolderExistChecked = True
100           End If
        
110           strLogFile = gobjFSO.BuildPath(g_strAppLogFile, Format(Now, "yyyy-mm-dd ") & "LYNX Document Processor.log")
        
        
120           Set oTxtFile = gobjFSO.OpenTextFile(strLogFile, ForAppending, True)
130           oTxtFile.Write strMsg
140           oTxtFile.Close
150           Set oTxtFile = Nothing
160       End If
End Sub

Private Sub processCommandLine()
          'StartService
10        Select Case UCase(g_strCommandParms)
              Case "-I", "/I", "/INSTALL", "-INSTALL"
20                Load frmMainSvc
30                If frmMainSvc.NTSvc.Install Then
40                    MsgBox frmMainSvc.NTSvc.DisplayName & " installed successfully.", vbInformation
50                Else
60                    MsgBox frmMainSvc.NTSvc.DisplayName & " failed to install.", vbCritical
70                End If
80                Unload frmMainSvc
90                appTerminate
100           Case "-U", "/U", "/UNINSTALL", "-UNINSTALL"
110               Load frmMainSvc
120               If frmMainSvc.NTSvc.Uninstall Then
130                   MsgBox frmMainSvc.NTSvc.DisplayName & " uninstalled successfully.", vbInformation
140               Else
150                   MsgBox frmMainSvc.NTSvc.DisplayName & " failed to uninstall.", vbCritical
160               End If
170               Unload frmMainSvc
180               appTerminate
190           Case "-DEBUG", "/DEBUG"
200               Load frmMainSvc
210               frmMainSvc.NTSvc.Interactive = True
220               frmMainSvc.NTSvc.Debug = True
230           Case ""
                  'default pass-through
240           Case Else
250               MsgBox "Invalid command option!" & vbCrLf & "try -install, -uninstall, or -debug.", vbInformation
                  'Unload Me
260               appTerminate
270       End Select
End Sub

Private Sub InitialTrace(strMsg As String)
          Dim strFileName As String
          Dim oTxtFile As TextStream
          
10        If blnInitialTrace Then
20            If gobjFSO Is Nothing Then Set gobjFSO = New Scripting.FileSystemObject
30            strFileName = gobjFSO.BuildPath(App.Path, APP_TITLE & Format(Now, " yyyymmdd ") & " initial trace.log")
              
40            Set oTxtFile = gobjFSO.OpenTextFile(strFileName, ForAppending, True)
50            oTxtFile.WriteLine strMsg
60            oTxtFile.Close
70            Set oTxtFile = Nothing
80        End If
End Sub

'********************************************************************
'* Syntax:  GetWinComputerName
'* Params:  None
'* Purpose: This procedure retrieves a NetBIOS name
'*              associated with the local computer.
'* Returns: An string - the name.
'********************************************************************
Public Function GetWinComputerName() As String
          Const PROC_NAME As String = MODULE_NAME & "GetWinComputerName()"
10        On Error GoTo errHandler
          Dim strName As String
          Dim lngCnt As Long
          
20        strName = Space(255)
30        lngCnt = GetComputerName(strName, 255)
          
40        If lngCnt <> 0 Then
50            strName = Left(strName, InStr(strName, Chr$(0)) - 1)
60        End If
          
70        GetWinComputerName = strName
80        Exit Function
errHandler:
90        If Err.Number <> 0 Then
100           GetWinComputerName = ""
110       End If
End Function

'Public Sub cleanupProcesses()
'    killProcess "pdfcreator.exe"
'    killProcess "pdfspo~1.exe"
'    killProcess "gswin32c.exe"
'    killProcess "pdftk.exe"
'    killProcess "convert.exe"
'    killProcess "htmldoc.exe"
'    killProcess "winword.exe"
'End Sub
'
'
'Public Sub killProcess(strProcessName)
'   Dim objWMISvc, objProc, colProc
'   Set objWMISvc = GetObject("winmgmts:\\" & strServer & "\root\cimv2")
'
'   Set colProc = objWMISvc.ExecQuery("Select * from Win32_Process where name ='" & strProcessName & "'")
'   For Each objProc In colProc
'      If LCase(objProc.Name) = LCase(strProcessName) Then
'         objProc.Terminate()
'      End If
'   Next
'End Sub

