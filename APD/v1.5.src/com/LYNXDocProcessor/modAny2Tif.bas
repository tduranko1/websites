Attribute VB_Name = "modAny2Tif"
Option Explicit
Option Compare Text

Const MODULE_NAME As String = "modAny2Tif:"
Dim m_PDF2TifAlternateInUse As Boolean

Public Function PDF2Tif(strFileName As String, strFDFPath As String, bDeleteOriginal As Boolean) As String
          Const PROC_NAME As String = MODULE_NAME & "PDF2Tif()"
10        On Error GoTo errHandler
          Dim bRet As Boolean
          Dim strRet As String
          Dim strOutputFileName As String
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Dim strPDFFilledFile As String
          Dim strCmd As String
          
20        strRet = "" 'convertedFileExists(strFileName, "pdf")
          
30        If gobjFSO.FileExists(strFileName) Then
40            If strFDFPath <> "" Then
                  'fill the pdf form
50                strPDFFilledFile = PDFFormFill(strFileName, strFDFPath, False)
                  
60                If strPDFFilledFile <> "" Then
70                    strFileName = strPDFFilledFile
80                Else
90                    Trace "Unable to fill the pdf form." & vbCrLf
100                   GoTo errHandler
110               End If
120           End If
130           strOutputFileName = getSafeFileName(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".tif"))
              
              'strCmd = """" & g_strGSPath & """ -dNOPAUSE -dBATCH -q -sDEVICE=tiffg4 -sPAPERSIZE=letter -r200 -sOutputFile=""" & strOutputFileName & """ """ & strFileName & """"
140           strCmd = """" & g_strGSPath & """ -dNOPAUSE -dBATCH -q -sDEVICE=tiffg4 -r200 -sOutputFile=""" & strOutputFileName & """ """ & strFileName & """"
              
150           If g_blnDirectExecute Then
160               bRet = SuperShell(strCmd, g_strSortTempPath, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
170           Else
180               strBatchFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), "totif.bat")
                  
190               Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
200               oBatFile.WriteLine strCmd
210               oBatFile.Close
220               Set oBatFile = Nothing
                  
                  'execute the batch file
230               bRet = SuperShell(strBatchFile, gobjFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
240               Sleep 1000
250           End If
260           DoEvents
              
              'cleanup
              'gobjFSO.DeleteFile strBatchFile
              'safeDelete strBatchFile
              
270           If gobjFSO.FileExists(strOutputFileName) = False And m_PDF2TifAlternateInUse = False Then
280               m_PDF2TifAlternateInUse = True
290               strRet = PDF2Tif2(strFileName, strFDFPath, bDeleteOriginal)
300           Else
310               strRet = strOutputFileName
320           End If
330       Else
340           bRet = False
350       End If
          
          'if bDeleteOriginal Then safeDelete strFileName
          
errHandler:
360       If Err.Number <> 0 Then
370           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
380           Trace g_strConvertErrMsg
390           strRet = ""
400       End If
410       PDF2Tif = strRet
End Function

Public Function PDF2Tif2(strFileName As String, strFDFPath As String, bDeleteOriginal As Boolean) As String
          Const PROC_NAME As String = MODULE_NAME & "PDF2Tif2()"
10        On Error GoTo errHandler
          Dim aDocuments(0) As String
          Dim strPDFMetaFile As String
          Dim strMergeFileName As String
          Dim bRet As Boolean
          Dim strRet As String
          
20        strRet = ""
          
          'fix the pdf by using pdf toolkit
30        aDocuments(0) = strFileName
          
40        strMergeFileName = getSafeFileName(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & "__tmp." & gobjFSO.GetExtensionName(strFileName)))
          
50        If gobjFSO.FileExists(g_strPDFMetaFile) Then strPDFMetaFile = g_strPDFMetaFile
          
60        If mergePDFs(aDocuments, strMergeFileName, strPDFMetaFile) Then
70           If gobjFSO.FileExists(strMergeFileName) Then
80               gobjFSO.DeleteFile strFileName, True
90               gobjFSO.MoveFile strMergeFileName, strFileName
                 
                 'now use the original pdf2tif method
100              strRet = PDF2Tif(strFileName, strFDFPath, bDeleteOriginal)
110          End If
120       End If
           
errHandler:
130       If Err.Number <> 0 Then
140           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
150           Trace g_strConvertErrMsg
160           bRet = False
170           strRet = ""
180       End If
190       m_PDF2TifAlternateInUse = False
200       PDF2Tif2 = strRet
End Function

Public Function Word2Tif(strFileName As String, bDeleteOriginal As Boolean) As String
          Const PROC_NAME As String = MODULE_NAME & "Word2Tif()"
10        On Error GoTo errHandler
          Dim strPDFFileName As String
          Dim strOutputFileName As String
          Dim blnConverted As Boolean
          
20        strPDFFileName = convertedFileExists(strFileName, "pdf")
30        If strPDFFileName = "" Then
40            strPDFFileName = Word2PDF(strFileName, bDeleteOriginal)
50            blnConverted = True
60        End If
70        If strPDFFileName <> "" Then
80            DoEvents
90            If blnConverted Then Sleep 1000
              
100           If convertedFileExists(strPDFFileName, "tif") = "" Then
110               strOutputFileName = PDF2Tif(strPDFFileName, "", bDeleteOriginal)
120           Else
130               strOutputFileName = convertedFileExists(strPDFFileName, "tif")
140           End If
              
150           If gobjFSO.FileExists(strOutputFileName) Then Word2Tif = strOutputFileName
              
              'gobjFSO.DeleteFile strPDFFileName
              'If bDeleteOriginal Then safeDelete strFileName
160       End If
errHandler:
170       If Err.Number <> 0 Then
180           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
190           Trace g_strConvertErrMsg
200       End If
End Function

Public Function Excel2Tif(strFileName As String, bDeleteOriginal As Boolean) As String
          Const PROC_NAME As String = MODULE_NAME & "Excel2Tif()"
10        On Error GoTo errHandler
          Dim strPDFFileName As String
          Dim strOutputFileName As String
          Dim blnConverted As Boolean
          
20        strPDFFileName = convertedFileExists(strFileName, "pdf")
30        If strPDFFileName = "" Then
40            strPDFFileName = Excel2PDF(strFileName, bDeleteOriginal)
50            blnConverted = True
60        End If
70        If strPDFFileName <> "" Then
80            DoEvents
90            If blnConverted Then Sleep 1000
              'strPDFFileName = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".pdf")
              
100           If convertedFileExists(strPDFFileName, "tif") = "" Then
110               strOutputFileName = PDF2Tif(strPDFFileName, "", bDeleteOriginal)
120           Else
130               strOutputFileName = convertedFileExists(strPDFFileName, "tif")
140           End If
              
150           If gobjFSO.FileExists(strOutputFileName) Then Excel2Tif = strOutputFileName
              
              'gobjFSO.DeleteFile strPDFFileName
              'If bDeleteOriginal Then safeDelete strFileName
160       End If
errHandler:
170       If Err.Number <> 0 Then
180           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
190           Trace g_strConvertErrMsg
200       End If
End Function

Public Function RPT2Tif(strFileName As String, bDeleteOriginal As Boolean) As Boolean
10        RPT2Tif = False
End Function

Public Function XML2Tif(strFileName As String, strXSLPath As String, bDeleteOriginal As Boolean) As String
          Const PROC_NAME As String = MODULE_NAME & "XML2Tif()"
          Dim strHTMFile As String
          Dim strPDFFileName As String
          Dim strOutputFileName As String
          Dim bRet As Boolean
          Dim strRet As String
          Dim blnConverted As Boolean
          
10        If strXSLPath <> "" Then
20            strHTMFile = convertedFileExists(strFileName, "htm")
30            If strHTMFile = "" Then
40                strHTMFile = XML2HTM(strFileName, strXSLPath, bDeleteOriginal)
50                blnConverted = True
60            End If
70            If strHTMFile <> "" Then
80                DoEvents
90                If blnConverted Then Sleep 500
100               If gobjFSO.FileExists(strHTMFile) Then
110                   blnConverted = False
120                   strPDFFileName = convertedFileExists(strHTMFile, "pdf")
130                   If strPDFFileName = "" Then
140                       strPDFFileName = Word2PDF(strHTMFile, bDeleteOriginal)
150                       blnConverted = True
160                   End If
170                   If strPDFFileName <> "" Then
180                       DoEvents
190                       If blnConverted Then Sleep 500
                          'strPDFFileName = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".pdf")
200                       strOutputFileName = convertedFileExists(strPDFFileName, "tif")
210                       If strOutputFileName = "" Then
220                           strOutputFileName = PDF2Tif(strPDFFileName, "", bDeleteOriginal)
230                       End If
                          
240                       If gobjFSO.FileExists(strOutputFileName) = False Then
250                           g_strConvertErrMsg = g_strConvertErrMsg & vbCrLf & "Error converting XML to Tif." & vbCrLf
260                       Else
270                           strRet = strOutputFileName
280                       End If
290                   End If
                      'gobjFSO.DeleteFile strHTMFile, True
                      'safeDelete strHTMFile
                      
300                   If gobjFSO.FileExists(strPDFFileName) Then
                          'gobjFSO.DeleteFile strPDFFileName, True
                          'safeDelete strPDFFileName
310                   End If
320               Else
330                   g_strConvertErrMsg = g_strConvertErrMsg & vbCrLf & "Unable to find the converted file " & strHTMFile & vbCrLf
340               End If
350           End If
360       End If
          
370       If bRet = False Then
              'g_strConvertErrMsg = ""
      '        If Word2PDF(strFileName, bDeleteOriginal) Then
      '            strPDFFileName = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".pdf")
      '            bRet = PDF2Tif(strPDFFileName, bDeleteOriginal)
      '        End If
380       End If
          
          'If bDeleteOriginal Then safeDelete strFileName
errHandler:
390       If Err.Number <> 0 Then
400           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
410           Trace g_strConvertErrMsg
420       End If
430       XML2Tif = strRet
End Function

Public Function Image2Tif(strFileName As String, bDeleteOriginal As Boolean) As String
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "Image2Tif()"
          Dim strTifFileName As String
          Dim strOutputPDFFileName As String
          Dim bRet As Boolean
          Dim strRet As String
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Dim strCmd As String
          
20        bRet = False
          
30        strTifFileName = convertedFileExists(strFileName, "tif")
          
40        If strTifFileName = "" Then
50            strTifFileName = getSafeFileName(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".tif"))
              
              'first convert the image to pdf
60            strOutputPDFFileName = getSafeFileName(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(gobjFSO.GetTempName()) & ".pdf"))
              
70            strCmd = """" & g_strIMPath & """ -sample ""480>x480>"" """ & strFileName & """ """ & strOutputPDFFileName & """"
80            If g_blnDirectExecute Then
90                bRet = SuperShell(strCmd, g_strSortTempPath, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
100           Else
110               strBatchFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), "topdf.bat")
          
120               Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
130               oBatFile.WriteLine strCmd
140               oBatFile.Close
150               Set oBatFile = Nothing
          
                  'execute the batch file
160               bRet = SuperShell(strBatchFile, gobjFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
170               Sleep 1000
180           End If

190           DoEvents

200           If gobjFSO.FileExists(strOutputPDFFileName) = False Then
210               Err.Raise vbObjectError + 100, PROC_NAME, "Converted File " & strOutputPDFFileName & " not found."
220           End If
              
              'strCmd = """" & g_strGSPath & """ -q -dNOPAUSE -dBATCH -sDEVICE=tiffg4 -r200 -sPAPERSIZE=letter -sOutputFile=""" & strTifFileName & """ """ & strOutputPDFFileName & """"
230           strCmd = """" & g_strGSPath & """ -q -dNOPAUSE -dBATCH -sDEVICE=tiffg4 -r200 -sOutputFile=""" & strTifFileName & """ """ & strOutputPDFFileName & """"
              
240           If g_blnDirectExecute Then
250               bRet = SuperShell(strCmd, g_strSortTempPath, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
260           Else
                  'now convert the pdf file to tif
270               strBatchFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), "totif.bat")
                  
280               Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
290               oBatFile.WriteLine strCmd
          '        oBatFile.WriteLine """" & g_strIMPath & """ """ & strFileName & """ -colorspace Gray """ & strTifFileName & """"
300               oBatFile.Close
310               Set oBatFile = Nothing
                  
                  'execute the batch file
320               bRet = SuperShell(strBatchFile, gobjFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
330               Sleep 1000
              
340           End If
              
350           DoEvents
              
360           If gobjFSO.FileExists(strTifFileName) = False Then
370               Err.Raise vbObjectError + 100, PROC_NAME, "Converted File " & strTifFileName & " not found."
380           Else
                  'delete the temporary file
390               gobjFSO.DeleteFile strOutputPDFFileName
400               strRet = strTifFileName
410           End If
420       Else
430           strRet = strTifFileName
440       End If
errHandler:
450       If Err.Number <> 0 Then
460           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
470           Trace g_strConvertErrMsg
480           strRet = ""
490           Err.Clear
500       End If
510       Image2Tif = strRet
          
End Function

Public Function HTML2Tif(strFileName As String, strHTMLOptions As String, bDeleteOriginal As Boolean) As String
          Const PROC_NAME As String = MODULE_NAME & "HTML2Tif()"
10        On Error GoTo errHandler
          Dim strPDFFile As String
          Dim bRet As Boolean
          Dim blnConverted As Boolean
          
20        strPDFFile = convertedFileExists(strFileName, "pdf")
30        If strPDFFile = "" Then
40            strPDFFile = HTML2PDF(strFileName, strHTMLOptions, bDeleteOriginal)
50            blnConverted = True
60        End If
          
70        If strPDFFile <> "" Then
80            DoEvents
90            If blnConverted Then Sleep 500
100           If gobjFSO.FileExists(strPDFFile) Then
110               If convertedFileExists(strPDFFile, "tif") = "" Then
120                   HTML2Tif = PDF2Tif(strPDFFile, "", bDeleteOriginal)
130               Else
140                   HTML2Tif = convertedFileExists(strPDFFile, "tif")
150               End If
                  
                  'safeDelete strPDFFile
160           Else
170               bRet = False
180           End If
190       End If
          
          'If bDeleteOriginal Then safeDelete strFileName
errHandler:
200       If Err.Number <> 0 Then
210           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
220           Trace g_strConvertErrMsg
230       End If
End Function

Public Function mergeTifs(aDocuments() As String, strMergeFileName As String) As Boolean
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "mergeTifs()"
          Dim i As Integer
          Dim iPage As Integer
          
20        Trace "      Merging documents to Tif... " & vbCrLf
          
30        strMergeFileName = gobjFSO.BuildPath(g_strSortTempPath, gobjFSO.GetBaseName(strMergeFileName) & ".tif")
          
40        If gobjFSO.FileExists(strMergeFileName) Then gobjFSO.DeleteFile strMergeFileName, True
50        For i = 0 To UBound(aDocuments)
60            If aDocuments(i) <> "" Then
70                If gobjFSO.FileExists(aDocuments(i)) Then
80                    With frmMainSvc.filImage
90                        Trace "        Merging document """ & aDocuments(i) & """ to " & strMergeFileName & "... " & vbCrLf
100                       .InputFileName = aDocuments(i)
                          '.OutputFileFormat = PixelFileOutputFormat_TIFFG4
                          
110                       setImageBasicOutputTif
                          
120                       .OutputFileAppend = True
                          
130                       Trace "          Number of pages: " & .PageCount & vbCrLf
140                       For iPage = 1 To .PageCount
150                           Trace "            Merging page: " & iPage & vbCrLf
160                           .PageIndex = iPage
170                           .SavePage strMergeFileName
180                       Next
                          
190                       .InputFileName = ""
200                   End With
210               Else
220                   Trace "        Document to merge """ & aDocuments(i) & """ does not exists." & vbCrLf
230               End If
240           End If
250       Next
          
260       If gobjFSO.FileExists(strMergeFileName) Then
270           Trace "      Merge to Tif done. " & strMergeFileName & vbCrLf
280           mergeTifs = True
290       Else
300           Trace "      Merge to Tif failed." & vbCrLf
310           mergeTifs = False
320       End If
errHandler:
330       If Err.Number <> 0 Then
340           Trace "Failed. ERROR: " & PROC_NAME & " [Line #" & Erl & "]. Description: " & Err.Description & vbCrLf
350           mergeTifs = False
360       End If
End Function

Public Function splitTIF(strFileName As String, lngSplitSize As Long) As String()
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "splitTIF()"
          Dim aRet() As String
          Dim lngSplitSizeBuffered As Long
          Dim strSplitFileName As String
          Dim lngSplitCounter As Long
          Dim lngPageIdx As Long
          Dim lngPageCount As Long
          Dim strInputBaseName As String
          Dim strInputPath As String
          Dim strInputExt As String
          Dim lngFileSize As Long
          Dim strTmpFile As String
          Dim lngCurFileSize As Long
'          Dim nBPS As Integer, nSPP As Integer
          
20        lngSplitSizeBuffered = lngSplitSize - 10000
          
30        Trace "      Splitting TIF document " & strFileName & " ... "
          
40        lngSplitCounter = 1
50        strInputPath = gobjFSO.GetParentFolderName(strFileName)
60        strInputBaseName = gobjFSO.GetBaseName(strFileName)
70        strInputExt = LCase(gobjFSO.GetExtensionName(strFileName))
          
80        With frmMainSvc
90            .dspImage.ImageDataSource = .filImage
100           .filImage.InputFileName = strFileName
110           .filImage.OutputFileAppend = True
              
120           lngPageCount = .filImage.PageCount
130           lngFileSize = 0
              
140           strSplitFileName = gobjFSO.BuildPath(strInputPath, strInputBaseName & Format(lngSplitCounter, "__##00") & "." & strInputExt)
              
150           strTmpFile = gobjFSO.BuildPath(strInputPath, gobjFSO.GetTempName())
              
160           setImageBasicOutputTif
              
              'burst the tif file
170           For lngPageIdx = 1 To lngPageCount
180               .filImage.PageIndex = lngPageIdx
                  
190               If Dir(strTmpFile) <> "" Then Kill strTmpFile
                  
200               .filImage.SavePage strTmpFile
                  
210               lngCurFileSize = FileLen(strTmpFile)

220               If (lngFileSize + lngCurFileSize) >= lngSplitSizeBuffered Then
230                   lngSplitCounter = lngSplitCounter + 1
240                   strSplitFileName = gobjFSO.BuildPath(strInputPath, strInputBaseName & Format(lngSplitCounter, "__##00") & "." & strInputExt)
250                   lngFileSize = 0
260               End If
                  
270               .filImage.SavePage strSplitFileName
                  
280               lngFileSize = FileLen(strSplitFileName)
                  
290               DoEvents
300           Next
              
310           If Dir(strTmpFile) <> "" Then Kill strTmpFile
              
320           .filImage.InputFileName = ""
330           .filImage.Clear
              
              'load the split files into an array
340           strSplitFileName = gobjFSO.BuildPath(strInputPath, strInputBaseName & "__*." & strInputExt)
350           strTmpFile = Dir(strSplitFileName)
             
              
              
360           lngPageCount = 0
              
370           While strTmpFile <> ""
380               ReDim Preserve aRet(lngPageCount)
390               aRet(lngPageCount) = gobjFSO.BuildPath(strInputPath, strTmpFile)
400               lngPageCount = lngPageCount + 1
410               strTmpFile = Dir()
420           Wend
              
430           If lngPageCount > 0 Then
440               Trace "Done. " & vbCrLf
450           Else
460               Trace "Failed." & vbCrLf
470           End If
              
480       End With
          
errHandler:
490       If Err.Number <> 0 Then
500           Trace "Failed. ERROR: " & PROC_NAME & " [Line #" & Erl & "]. Description: " & Err.Description & vbCrLf
510       End If
520       splitTIF = aRet
End Function

Private Sub setImageBasicOutputTif()
    With frmMainSvc.filImage
    .OutputFileFormat = PixelFileOutputFormat_TIFFG4
    .OutputBitsPerSample = PixelFileBitsPerSample_1
    .OutputSamplesPerPixel = PixelFileSamplesPerPixel_1
    .OutputPhotoInterp = PixelFilePhotoInterp_Bitonal_White_on_Black
    End With
End Sub


