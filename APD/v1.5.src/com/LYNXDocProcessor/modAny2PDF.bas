Attribute VB_Name = "modAny2PDF"
Option Explicit
Option Compare Text
Const MODULE_NAME As String = "modAny2PDF:"
Public g_objWordApp As Word.Application
Public g_objExcelApp As Excel.Application
Private strDefaultPrinter As String

Public Function Tif2PDF(strFileName As String, bDeleteOriginal As Boolean) As String
10        On Error GoTo errHandler
          Const PROC_NAME As String = "Tif2PDF()"
          Dim strOutputFileName As String
          Dim strCmd As String
          Dim bRet As Boolean
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          
20        If strFileName <> "" Then
30            If gobjFSO.FileExists(strFileName) Then
40                strOutputFileName = getSafeFileName(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".pdf"))
                  
      '            frmMainSvc.dspImage.ImageDataSource = frmMainSvc.filImage
      '
      '            With frmMainSvc.filImage
      '                .InputFileName = strFileName
      '
      '                'ImageBasic PDF writter can only output as black on white
      '                .OutputFileFormat = PixelFileOutputFormat_PDF_G4
      '                .OutputBitsPerSample = PixelFileBitsPerSample_1
      '                .OutputSamplesPerPixel = PixelFileSamplesPerPixel_1
      '                .OutputPhotoInterp = PixelFilePhotoInterp_Bitonal_White_on_Black
      '
      '                .SaveDocument (strOutputFileName)
      '
      '                .InputFileName = ""
      '                .Clear
      '
      '                If gobjFSO.FileExists(strOutputFileName) Then
      '                    Tif2PDF = strOutputFileName
      '                End If
      '            End With
              
                'If bDeleteOriginal Then safeDelete strFileName
                
50              strCmd = """" & g_strIMPath & """ """ & strFileName & """ """ & strOutputFileName & """"
                
60                If g_blnDirectExecute Then
                      
                      'execute the command
70                    bRet = SuperShell(strCmd, g_strSortTempPath, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
80                Else
90                    strBatchFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), "topdf.bat")
                      
100                   Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
110                   oBatFile.WriteLine strCmd
120                   oBatFile.Close
130                   Set oBatFile = Nothing
                      
                      'execute the batch file
140                   bRet = SuperShell(strBatchFile, gobjFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
150                   Sleep 1000
160               End If
                  
170               DoEvents
                  
                  'cleanup
                  'gobjFSO.DeleteFile strBatchFile
                  'safeDelete strBatchFile
                  
180               If gobjFSO.FileExists(strOutputFileName) = False Then
190                   Err.Raise vbObjectError + 100, PROC_NAME, "Converted File " & strOutputFileName & " not found."
200               Else
                      'setPDFMetadata strOutputFileName, g_strPDFMetaFile
210                   If gobjFSO.FileExists(strOutputFileName) Then Tif2PDF = strOutputFileName
220                   bRet = True
230               End If
240           Else
250               g_strConvertErrMsg = "Missing Input Tif file." & vbCrLf
260               Trace g_strConvertErrMsg
270               Tif2PDF = ""
280           End If
290       Else
300           g_strConvertErrMsg = "Missing Input Tif file." & vbCrLf
310           Trace g_strConvertErrMsg
320           Tif2PDF = ""
330       End If
errHandler:
340       If Err.Number <> 0 Then
350           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
360           Trace g_strConvertErrMsg
370           Tif2PDF = ""
380       End If
End Function

Public Function Word2PDF(strFileName As String, bDeleteOriginal As Boolean, Optional strHTMLOptions As String = "") As String
10        On Error GoTo errHandler
          Const PROC_NAME As String = "Word2PDF()"
          Dim objWordDoc As Word.Document
          Dim strOutputFileName As String
          Dim strActivePrinter As String
          Dim iWaitCounter As Integer
          Dim blnHTMLDoc As Boolean
          
          
          Dim strHeader As String
          Dim strFooter As String
          Dim strTopMargin As String
          Dim strLeftMargin As String
          Dim strBottomMargin As String
          Dim strRightMargin As String
          Dim strDocTitle As String
          Dim aOptions() As String
          Dim i As Integer
          Dim strKey As String
          Dim strValue As String
          Dim aKeyValue() As String
          Dim strPageMetric As String
          Dim strMargin As String
          
          'check the default printer. If the default is not the pdf printer then make it.
          'If Printer.DeviceName <> g_sPDFPrinterName Then SetDefaultPrinter g_sPDFPrinterName
          
20        iWaitCounter = 10
          
30        strOutputFileName = getSafeFileName(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".pdf"))
          
40        blnHTMLDoc = ((gobjFSO.GetExtensionName(strFileName) = "htm") Or (gobjFSO.GetExtensionName(strFileName) = "html"))
          
50        If g_objWordApp Is Nothing Then
              'create the word application
60            Set g_objWordApp = CreateObject("Word.Application")
              
70            g_objWordApp.DisplayAlerts = wdAlertsNone
80            g_objWordApp.FeatureInstall = 0 'msoFeatureInstallNone
90            g_objWordApp.PrintPreview = False
100           g_objWordApp.Visible = True
              
110           g_objWordApp.Application.Options.UpdateLinksAtOpen = True
120           g_objWordApp.Application.Options.SaveNormalPrompt = False
130           g_objWordApp.Application.Options.SavePropertiesPrompt = False
140           g_objWordApp.Application.DisplayAlerts = wdAlertsNone
150           g_objWordApp.Application.FeatureInstall = 0 'msoFeatureInstallNone
160           g_objWordApp.Application.PrintPreview = False
170           g_objWordApp.Application.Visible = False
180       End If
          
          'open the word document
190       Set objWordDoc = g_objWordApp.Documents.Open( _
                              Filename:=strFileName, _
                              ConfirmConversions:=False, _
                              ReadOnly:=(Not blnHTMLDoc), _
                              AddToRecentFiles:=False, _
                              PasswordDocument:="", _
                              PasswordTemplate:="", _
                              Revert:=True)
                              
200       DoEvents
          
210       If blnHTMLDoc Then
             
              'set the default html options
220           strHeader = g_strHTMLHeader
230           strFooter = g_strHTMLFooter
240           strTopMargin = g_strHTMLTopMargin
250           strLeftMargin = g_strHTMLLeftMargin
260           strBottomMargin = g_strHTMLBottomMargin
270           strRightMargin = g_strHTMLRightMargin
280           strDocTitle = ""
              
290           aOptions = Split(strHTMLOptions, "|")
              
              'override the html options with user options
300           For i = 0 To UBound(aOptions) - 1
310               aKeyValue = Split(aOptions(i), "=")
320               Select Case aKeyValue(0)
                      Case "header"
330                       strHeader = aKeyValue(1)
340                   Case "footer"
350                       strFooter = aKeyValue(1)
360                   Case "topMargin"
370                       strTopMargin = aKeyValue(1)
380                   Case "leftMargin"
390                       strLeftMargin = aKeyValue(1)
400                   Case "bottomMargin"
410                       strBottomMargin = aKeyValue(1)
420                   Case "rightMargin"
430                       strRightMargin = aKeyValue(1)
440               End Select
450           Next
             
460           If strHeader <> "" Or _
                  strFooter <> "" Or _
                  strTopMargin <> "" Or _
                  strLeftMargin <> "" Or _
                  strBottomMargin <> "" Or _
                  strRightMargin <> "" Then
                  
                  'set the page margins
      '            setDocPageMargin objWordDoc, "top", strTopMargin
      '            setDocPageMargin objWordDoc, "left", strLeftMargin
      '            setDocPageMargin objWordDoc, "bottom", strBottomMargin
      '            setDocPageMargin objWordDoc, "right", strRightMargin
                  
470               objWordDoc.PageSetup.TopMargin = margin2Points(strTopMargin)
480               objWordDoc.PageSetup.LeftMargin = margin2Points(strLeftMargin)
490               objWordDoc.PageSetup.BottomMargin = margin2Points(strBottomMargin)
500               objWordDoc.PageSetup.RightMargin = margin2Points(strRightMargin)
                  
                  'set the header and footer distance to zero.
510               objWordDoc.PageSetup.HeaderDistance = InchesToPoints("0")
520               objWordDoc.PageSetup.FooterDistance = InchesToPoints("0")
                  
                  'set the default header
530               If Len(strHeader) <= 3 Then strHeader = ""
                  
                  'create the page header
540               If strHeader <> "" Then
550                   objWordDoc.PageSetup.HeaderDistance = InchesToPoints("0.25")

560                   With objWordDoc.Sections(1).Headers(wdHeaderFooterPrimary).Range
570                       With .Font
580                           .Bold = True
590                           .Name = "Arial"
600                           .Size = 10
610                       End With

620                       .ParagraphFormat.SpaceAfter = 12

630                       With .Borders(wdBorderBottom)
640                           .LineStyle = wdLineStyleSingle
650                           .LineWidth = wdLineWidth150pt
660                       End With
                          
670                       .Text = strHeader
680                   End With
690               End If
              
                  'set the page footer
700               If strFooter <> "" Then
710                   If g_blnCoversheet Then
720                       If objWordDoc.Sections(1).Footers(wdHeaderFooterPrimary).Exists Then
730                           Trace "      Processing coversheet. No footer text will be printed." & vbCrLf
740                           objWordDoc.PageSetup.FooterDistance = InchesToPoints("0.01")
750                           Trace "      Set the footer distance to 0.01in." & vbCrLf
760                           objWordDoc.Sections(1).Footers(wdHeaderFooterPrimary).Range.Delete
770                           Trace "      Deleted the footer range." & vbCrLf
780                           objWordDoc.Sections(1).Footers(wdHeaderFooterPrimary).Range.ShowAll = False
790                           Trace "      Setting the footer range showAll to false." & vbCrLf
                              'NormalTemplate.AutoTextEntries("Page X of Y").Delete
                              'Trace "      Deleting the NormalTemplate Page footer." & vbCrLf
800                       End If
810                   Else
820                       objWordDoc.PageSetup.FooterDistance = InchesToPoints("0.25")
                          
830                       With objWordDoc.Sections(1).Footers(wdHeaderFooterPrimary)
840                           With .Range
850                               .ShowAll = True
                                  
860                               With .Font
870                                   .Name = "Arial"
880                                   .Size = 10
890                               End With
900                               With .Borders(wdBorderTop)
910                                   .LineStyle = wdLineStyleDot
920                                   .LineWidth = wdLineWidth050pt
930                               End With
                                  
940                               .ParagraphFormat.Alignment = wdAlignParagraphRight
                                  
950                           End With
                              
                              
                              'If g_blnCoversheet Then
                              '    Trace "      Processing coversheet. No footer text will be printed." & vbCrLf
                              '    .Range.Delete
                              'Else
960                               NormalTemplate.AutoTextEntries("Page X of Y").Insert .Range
                              'End If
                              
970                       End With
980                   End If
990               End If
1000          End If
1010      End If
          
1020      objWordDoc.Application.Options.UpdateFieldsAtPrint = True
1030      objWordDoc.Application.Options.UpdateLinksAtPrint = True
          
1040      strDefaultPrinter = g_objWordApp.ActivePrinter
1050      SetPrinter g_strPDFPrinterName
          
1060      With frmMainSvc.PDFCreator1
1070          .cOption("UseAutosave") = 1
1080          .cOption("UseAutosaveDirectory") = 1
1090          .cOption("AutosaveDirectory") = gobjFSO.GetParentFolderName(strOutputFileName)
1100          .cOption("AutosaveFilename") = gobjFSO.GetBaseName(strOutputFileName) '& ".pdf"
1110          .cOption("AutosaveFormat") = 0
              '.cClearCache
1120          DoEvents
1130          objWordDoc.PrintOut Background:=False
1140          DoEvents
1150          .cPrinterStop = False
1160      End With
                              
1170      DoEvents
          
1180      objWordDoc.Close wdDoNotSaveChanges
          
          'destroy the word document
1190      Set objWordDoc = Nothing
          
          'g_objWordApp.Quit wdDoNotSaveChanges
          'Set g_objWordApp = Nothing
          
          'wait 60 seconds for pickup
1200      iWaitCounter = 60
1210      While gobjFSO.FileExists(strOutputFileName) = False And iWaitCounter > 0
1220          Sleep 1000
1230          DoEvents
1240          iWaitCounter = iWaitCounter - 1
1250      Wend
          
          
          'wait for the job completion
1260      iWaitCounter = 60
1270      If gobjFSO.FileExists(strOutputFileName) Then
1280          While FileInUse(strOutputFileName) And iWaitCounter > 0
1290              Sleep 1000
1300              DoEvents
1310              iWaitCounter = iWaitCounter - 1
1320          Wend
              
1330          If FileInUse(strOutputFileName) Then
1340              Err.Raise vbObjectError + 100, PROC_NAME, "File in use"
1350          End If
              
              'setPDFMetadata strOutputFileName, g_strPDFMetaFile
              
              'Word2PDF = True
1360          If gobjFSO.FileExists(strOutputFileName) Then Word2PDF = strOutputFileName
1370      Else
              'frmMainSvc.PDFCreator1.cClearCache
              'frmMainSvc.PDFCreator1.cPrinterStop = True
1380          Err.Raise vbObjectError + 100, PROC_NAME, "Converted File " & strOutputFileName & " not found."
1390      End If
          
          'If bDeleteOriginal Then safeDelete strFileName
errHandler:
1400      If Err.Number <> 0 Then
1410          g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
1420          Trace g_strConvertErrMsg
1430          If Not objWordDoc Is Nothing Then
1440              objWordDoc.Close wdDoNotSaveChanges
1450              Set objWordDoc = Nothing
1460          End If
1470          Word2PDF = ""
1480      End If
          
      '    Set objWordDoc = Nothing
      '    g_objWordApp.Quit saveChanges:=False
      '    Set g_objWordApp = Nothing
End Function

Public Function Excel2PDF(strFileName As String, bDeleteOriginal As Boolean) As String
10        On Error GoTo errHandler
          Const PROC_NAME As String = "Excel2PDF()"
          Dim objExcelWB As Excel.Workbook
          Dim strOutputFileName As String
          Dim strActivePrinter As String
          Dim iWaitCounter As Integer
          
20        iWaitCounter = 60
          
30        strOutputFileName = getSafeFileName(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".pdf"))
          
40        If g_objExcelApp Is Nothing Then
              'create the word application
50            Set g_objExcelApp = CreateObject("Excel.Application")
              
60            g_objExcelApp.DisplayAlerts = False
70            g_objExcelApp.FeatureInstall = 0 'msoFeatureInstallNone
80            g_objExcelApp.Visible = False
              
90            g_objExcelApp.Application.DisplayAlerts = False
100           g_objExcelApp.Application.FeatureInstall = 0 'msoFeatureInstallNone
110           g_objExcelApp.Application.Visible = False
120           g_objExcelApp.Interactive = False
130       End If
          
          'open the excel document
140       Set objExcelWB = g_objExcelApp.Workbooks.Open( _
                              Filename:=strFileName, _
                              ReadOnly:=True, _
                              AddToMru:=False, _
                              Editable:=False, _
                              Notify:=False, _
                              Password:="")
                              
150       DoEvents
          
160       With frmMainSvc.PDFCreator1
170           .cOption("UseAutosave") = 1
180           .cOption("UseAutosaveDirectory") = 1
190           .cOption("AutosaveDirectory") = gobjFSO.GetParentFolderName(strOutputFileName)
200           .cOption("AutosaveFilename") = gobjFSO.GetBaseName(strOutputFileName) '& ".pdf"
210           .cOption("AutosaveFormat") = 0
              '.cClearCache
220           DoEvents
230           objExcelWB.PrintOut ActivePrinter:=g_strPDFPrinterName
240           DoEvents
250           .cPrinterStop = False
260       End With
                              
270       DoEvents
          
280       objExcelWB.Close xlDoNotSaveChanges
          
          'destroy the word document
290       Set objExcelWB = Nothing
          
          'wait for pickup
300       iWaitCounter = 60
310       While gobjFSO.FileExists(strOutputFileName) = False And iWaitCounter > 0
320           Sleep 1000
330           DoEvents
340           iWaitCounter = iWaitCounter - 1
350       Wend
          
          
          'wait for the job completion
360       iWaitCounter = 60
370       If gobjFSO.FileExists(strOutputFileName) Then
380           While FileInUse(strOutputFileName) And iWaitCounter > 0
390               Sleep 1000
400               iWaitCounter = iWaitCounter - 1
410           Wend
              
420           If FileInUse(strOutputFileName) Then
430               Err.Raise vbObjectError + 100, PROC_NAME, "File in use"
440           End If

450           'setPDFMetadata strOutputFileName, g_strPDFMetaFile
              
460           If gobjFSO.FileExists(strOutputFileName) Then Excel2PDF = strOutputFileName
470       Else
480           Err.Raise vbObjectError + 100, PROC_NAME, "Converted File " & strOutputFileName & " not found."
490       End If
          
          'If bDeleteOriginal Then safeDelete strFileName
          
errHandler:
500       If Err.Number <> 0 Then
510           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
520           Trace g_strConvertErrMsg
530           If Not objExcelWB Is Nothing Then
540               objExcelWB.Close xlDoNotSaveChanges
550               Set objExcelWB = Nothing
560           End If
570           Excel2PDF = ""
580       End If
          
590       If Not objExcelWB Is Nothing Then
600           objExcelWB.Close xlDoNotSaveChanges
610           Set objExcelWB = Nothing
620       End If
630       g_objExcelApp.Quit
640       Set g_objExcelApp = Nothing
End Function

Public Function RPT2PDF(strFileName As String, bDeleteOriginal As Boolean) As String
10        RPT2PDF = ""
End Function

Public Function XML2PDF(strFileName As String, strXSLPath As String, bDeleteOriginal As Boolean) As String
          Const PROC_NAME As String = "XML2PDF()"
10        On Error GoTo errHandler
          Dim strHTMFile As String
          Dim bRet As Boolean
          Dim strOutputFileName As String
          Dim strTmpFileName As String
          
          'strOutputFileName = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".pdf")
20        If strXSLPath <> "" Then
              'strHTMFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".htm")
30            strHTMFile = XML2HTM(strFileName, strXSLPath, bDeleteOriginal)
40            If strHTMFile <> "" Then
50                If gobjFSO.FileExists(strHTMFile) Then
60                    strOutputFileName = Word2PDF(strHTMFile, bDeleteOriginal)
70                End If
                  'gobjFSO.DeleteFile strHTMFile, True
                  'safeDelete strHTMFile
80            End If
90        Else
100           strOutputFileName = Word2PDF(strFileName, bDeleteOriginal)
110       End If
          
120       If gobjFSO.FileExists(strOutputFileName) = False Then
      '        setPDFMetadata strOutputFileName, g_strPDFMetaFile
      '    Else
130           Err.Raise vbObjectError + 100, PROC_NAME, "XML2PDF: Converted File " & strOutputFileName & " not found."
140           strOutputFileName = ""
150       End If
          
          'If bDeleteOriginal Then safeDelete strFileName
          'If bDeleteOriginal Then safeDelete strXSLPath
errHandler:
160       If Err.Number <> 0 Then
170           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
180           Trace g_strConvertErrMsg
190       End If
200       XML2PDF = strOutputFileName
End Function

Public Function XML2HTM(strFileName As String, strXSLFileName As String, bDeleteOriginal As Boolean) As String
          Const PROC_NAME As String = "XML2HTML()"
10        On Error GoTo errHandler
          Dim oXML As MSXML2.DOMDocument
          Dim oXSL As MSXML2.DOMDocument
          Dim strOutputFileName As String
          Dim strTransformationOutput As String
          Dim oHTMFile As TextStream
          
20        strOutputFileName = getSafeFileName(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".htm"))
          
30        Set oXML = New MSXML2.DOMDocument
40        Set oXSL = New MSXML2.DOMDocument
          
50        oXML.async = False
60        oXSL.async = False
          
70        Set oXML = loadXMLFile(strFileName)
          
80        Set oXSL = loadXMLFile(strXSLFileName)
          
90        If (Not oXML Is Nothing) And (Not oXSL Is Nothing) Then
          
100           strTransformationOutput = oXML.transformNode(oXSL)
              
110           Set oHTMFile = gobjFSO.OpenTextFile(strOutputFileName, ForWriting, True)
120           oHTMFile.Write strTransformationOutput
130           oHTMFile.Close
140           Set oHTMFile = Nothing
              
              'If bDeleteOriginal Then safeDelete strFileName
              
150           If gobjFSO.FileExists(strOutputFileName) Then XML2HTM = strOutputFileName
160       End If
errHandler:
170       Set oXML = Nothing
180       Set oXSL = Nothing
          
190       If Err.Number <> 0 Then
200           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
210           Trace g_strConvertErrMsg
220           XML2HTM = ""
230       End If
End Function

Public Function Image2PDF(strFileName As String, bDeleteOriginal As Boolean) As String
10        On Error GoTo errHandler
          Const PROC_NAME As String = "Image2PDF()"
          Dim bRet As Boolean
          Dim strOutputFileName As String
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Dim strCmd As String
          
20        If gobjFSO.FileExists(strFileName) Then
30            strOutputFileName = getSafeFileName(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".pdf"))
              
40            If g_blnDirectExecute Then
50                strCmd = """" & g_strIMPath & """ -sample """ & g_intJPGMaxWidth & ">x" & g_intJPGMaxHeight & ">"" """ & strFileName & """ """ & strOutputFileName & """"
                  'execute the command
60                bRet = SuperShell(strCmd, g_strSortTempPath, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
70            Else
80                strBatchFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), "topdf.bat")
                  
90                Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
100               oBatFile.WriteLine """" & g_strIMPath & """ -sample """ & g_intJPGMaxWidth & ">x" & g_intJPGMaxHeight & ">"" """ & strFileName & """ """ & strOutputFileName & """"
110               oBatFile.Close
120               Set oBatFile = Nothing
                  
                  'execute the batch file
130               bRet = SuperShell(strBatchFile, gobjFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
140               Sleep 1000
150           End If
              
160           DoEvents
              
              'cleanup
              'gobjFSO.DeleteFile strBatchFile
              'safeDelete strBatchFile
              
170           If gobjFSO.FileExists(strOutputFileName) = False Then
180               Err.Raise vbObjectError + 100, PROC_NAME, "Converted File " & strOutputFileName & " not found."
190           Else
                  'setPDFMetadata strOutputFileName, g_strPDFMetaFile
200               If gobjFSO.FileExists(strOutputFileName) Then Image2PDF = strOutputFileName
210               bRet = True
220           End If
230       Else
240           bRet = False
250       End If
          
          'If bDeleteOriginal Then safeDelete strFileName
          
errHandler:
260       If Err.Number <> 0 Then
270           strOutputFileName = ""
280           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
290           Trace g_strConvertErrMsg
300       End If
310       Image2PDF = strOutputFileName
End Function

Public Function HTML2PDF(strFileName As String, strHTMLOptions As String, bDeleteOriginal As Boolean) As String
          Const PROC_NAME As String = "HTML2PDF()"
          
10        HTML2PDF = Word2PDF(strFileName, bDeleteOriginal, strHTMLOptions)
End Function

Public Function HTML2PDF2(strFileName As String, strHTMLOptions As String, bDeleteOriginal As Boolean) As String
          Const PROC_NAME As String = "HTML2PDF2()"
10        On Error GoTo errHandler
          Dim bRet As Boolean
          Dim strOutputFileName As String
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Dim strHeader As String
          Dim strFooter As String
          Dim strTopMargin As String
          Dim strLeftMargin As String
          Dim strBottomMargin As String
          Dim strRightMargin As String
          Dim strDocTitle As String
          Dim aOptions() As String
          Dim i As Integer
          Dim strKey As String
          Dim strValue As String
          Dim aKeyValue() As String
          Dim strCmd As String
          
20        If gobjFSO.FileExists(strFileName) Then
30            strOutputFileName = getSafeFileName(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".pdf"))
40            strBatchFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), "topdf.bat")
              
              'set the default html options
50            strHeader = g_strHTMLHeader
60            strFooter = g_strHTMLFooter
70            strTopMargin = g_strHTMLTopMargin
80            strLeftMargin = g_strHTMLLeftMargin
90            strBottomMargin = g_strHTMLBottomMargin
100           strRightMargin = g_strHTMLRightMargin
110           strDocTitle = ""
              
120           aOptions = Split(strHTMLOptions, "|")
              
130           For i = 0 To UBound(aOptions) - 1
140               aKeyValue = Split(aOptions(i), "=")
150               Select Case aKeyValue(0)
                      Case "header"
160                       strHeader = aKeyValue(1)
170                   Case "footer"
180                       strFooter = aKeyValue(1)
190                   Case "topMargin"
200                       strTopMargin = aKeyValue(1)
210                   Case "leftMargin"
220                       strLeftMargin = aKeyValue(1)
230                   Case "bottomMargin"
240                       strBottomMargin = aKeyValue(1)
250                   Case "rightMargin"
260                       strRightMargin = aKeyValue(1)
270               End Select
280           Next
              
290           strCmd = """" & g_strHTMLDoc & """ --webpage --header " & strHeader & " --footer " & strFooter & _
                       " --embedfonts --jpeg=95 --top " & strTopMargin & " --left " & strLeftMargin & _
                       " --bottom " & strBottomMargin & " --right " & strRightMargin & _
                       " --linkcolor 0000FF --headfootsize 10 " & _
                       " --datadir " & gobjFSO.GetParentFolderName(g_strHTMLDoc) & _
                       " -t pdf14 -f " & """" & strOutputFileName & """ """ & strFileName & """"
              
300           If g_blnDirectExecute Then
                  'execute the command directly
310               bRet = SuperShell(strCmd, g_strSortTempPath, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
320           Else
330               Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
340               oBatFile.WriteLine strCmd
350               oBatFile.Close
360               Set oBatFile = Nothing
                  
                  'execute the batch file
370               bRet = SuperShell(strBatchFile, gobjFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
380               Sleep 1000
390           End If
              
400           DoEvents
              
              'cleanup
              'gobjFSO.DeleteFile strBatchFile
              'safeDelete strBatchFile
              
410           If gobjFSO.FileExists(strOutputFileName) = False Then
420               Err.Raise vbObjectError + 100, PROC_NAME, "Converted File " & strOutputFileName & " not found."
430           Else
                  'setPDFMetadata strOutputFileName, g_strPDFMetaFile
440               If gobjFSO.FileExists(strOutputFileName) = False Then strOutputFileName = ""
450           End If
460       Else
470           bRet = False
480           strOutputFileName = ""
490       End If
          
          'If bDeleteOriginal Then safeDelete strFileName
          
errHandler:
500       If Err.Number <> 0 Then
510           bRet = False
520           strOutputFileName = ""
530           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
540           Trace g_strConvertErrMsg
550       End If
          
560       HTML2PDF2 = strOutputFileName
End Function

Private Sub SetPrinter(Printername As String)
10        With g_objWordApp.Dialogs(wdDialogFilePrintSetup)
20            .Printer = Printername
30            .DoNotSetAsSysDefault = True
40            .Execute
50        End With
End Sub

Public Sub cleanupPDFTools()
10        If Not g_objWordApp Is Nothing Then
              'quit word application without saving any changes.
20            g_objWordApp.Quit saveChanges:=False
30        End If
40        Set g_objWordApp = Nothing

50        If Not g_objExcelApp Is Nothing Then
              'quit excel application without saving any changes.
60            g_objExcelApp.Quit
70        End If
80        Set g_objExcelApp = Nothing
End Sub

Public Function setPDFMetadata(strFileName As String, strMetaFile As String) As Boolean
          'pdftk mydoc.pdf update_info new_info.txt output mydoc.no_metadata.pdf
          'info.txt
          'InfoKey: Creator
          'InfoValue: PDF Creator
          
          Dim strOutputFile As String
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Dim bRet As Boolean
          Dim lngStart As Long
          Dim strCmd As String
          
10        lngStart = GetTickCount()
          
20        Trace "        Setting PDF metadata for " & strFileName & " ... "
          
30        strBatchFile = gobjFSO.BuildPath(g_strSortTempPath, "setMeta.bat")
40        strOutputFile = getSafeFileName(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & ".tmp"))
50        strCmd = """" & g_strPDFTKPath & """ """ & strFileName & """ update_info """ & strMetaFile & """" & _
                   " output """ & strOutputFile & """"
          
60        If g_blnDirectExecute Then
70            bRet = SuperShell(strCmd, g_strSortTempPath, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
80        Else
          
90            Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
100           oBatFile.WriteLine strCmd
110           oBatFile.Close
120           Set oBatFile = Nothing
              
              'execute the batch file
130           bRet = SuperShell(strBatchFile, gobjFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
140           Sleep 1000
150       End If
          
160       DoEvents

          'cleanup
          'gobjFSO.DeleteFile strBatchFile
          'safeDelete strBatchFile
          
170       If gobjFSO.FileExists(strOutputFile) Then
              'delete the original file
180           safeDelete strFileName
          
190           gobjFSO.MoveFile strOutputFile, strFileName
              
200           Trace "Done."
              
210           setPDFMetadata = True
220       Else
230           Trace "FAILED."
240           setPDFMetadata = False
250       End If
          
260       Trace " [" & (GetTickCount() - lngStart) & " ms]" & vbCrLf
End Function

Public Function mergePDFs(aDocuments() As String, strMergeFileName As String, strMetaFile As String) As Boolean
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "mergePDFs()"
          Dim strMergedFile As String
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Dim strInputFiles As String
          Dim i As Integer
          Dim bRet As Boolean
          Dim strCmd As String
          
20        Trace "          Merging documents to pdf... "
30        strMergedFile = gobjFSO.GetBaseName(strMergeFileName) & ".pdf" 'gobjFSO.BuildPath(g_strSortTempPath, gobjFSO.GetBaseName(strMergeFileName) & ".pdf")
          
40        If gobjFSO.FileExists(strMergedFile) Then gobjFSO.DeleteFile strMergedFile, True
          
50        strBatchFile = gobjFSO.BuildPath(g_strSortTempPath, "merge.bat")
          
60        For i = 0 To UBound(aDocuments)
70            If aDocuments(i) <> "" Then
80                If gobjFSO.FileExists(aDocuments(i)) Then
90                    strInputFiles = strInputFiles & """" & gobjFSO.GetBaseName(aDocuments(i)) & "." & gobjFSO.GetExtensionName(aDocuments(i)) & """ "
100               End If
110           End If
120       Next
          
130       strCmd = """" & g_strPDFTKPath & """ " & strInputFiles & " cat output """ & strMergedFile & """ dont_ask"
          
140       If g_blnDirectExecute Then
150           bRet = SuperShell(strCmd, g_strSortTempPath, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
160       Else
170           Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
180           oBatFile.WriteLine strCmd
190           oBatFile.Close
200           Set oBatFile = Nothing
              
              'execute the batch file
210           bRet = SuperShell(strBatchFile, gobjFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
220           Sleep 1000
230       End If
          
240       DoEvents

          'cleanup
          'gobjFSO.DeleteFile strBatchFile
          'safeDelete strBatchFile
          
250       If gobjFSO.FileExists(gobjFSO.BuildPath(g_strSortTempPath, gobjFSO.GetBaseName(strMergeFileName) & ".pdf")) Then
260           If gobjFSO.GetParentFolderName(strMergeFileName) = "" Then strMergeFileName = gobjFSO.BuildPath(g_strSortTempPath, strMergeFileName)
              
              'setPDFMetadata strMergeFileName, strMetaFile
270           Trace "Done." & vbCrLf
280           mergePDFs = True
290       Else
300           Trace "FAILED." & vbCrLf
310           mergePDFs = False
320       End If
errHandler:
330       If Err.Number <> 0 Then
340           Trace "Failed. ERROR: " & PROC_NAME & " [Line #" & Erl & "]. Description: " & Err.Description & vbCrLf
350           mergePDFs = False
360       End If
End Function

Public Function splitPDF(strFileName As String, lngSplitSize As Long) As String()
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "splitPDF()"
          Dim oBatFile As TextStream
          Dim strParentFolderName As String
          Dim strBatchFile As String
          Dim strBurstFormat As String
          Dim bRet As Boolean
          Dim aBurstFiles() As String
          Dim strBurstFile As String
          Dim strPageNumFormat As String
          Dim lBurstFileCount As Long
          Dim lFileSize As Long
          Dim lCombinedBatchSize As Long
          Dim i As Integer
          Dim iBatchCount As Integer
          Dim strBatchFileName As String
          Dim lngSplitSizeBuffered As Long
          Dim strCmdLine As String
          Dim aRet() As String
          Dim strCmd As String
          
          'When combining files there may be a slight increase in the combined size.
          '  We will buffer some arbitrary value
20        lngSplitSizeBuffered = lngSplitSize - 10000
          
30        Trace "      Splitting PDF document " & strFileName & " ... "
          
40        strParentFolderName = gobjFSO.GetParentFolderName(strFileName)
          
50        strBatchFile = gobjFSO.BuildPath(strParentFolderName, "pdfburst.bat")
60        strPageNumFormat = "%%04d"
70        strBurstFormat = gobjFSO.BuildPath(strParentFolderName, gobjFSO.GetBaseName(strFileName) & "_pg" & strPageNumFormat & ".pdf")
          
80        strCmd = """" & g_strPDFTKPath & """ """ & strFileName & """ burst output """ & strBurstFormat & """"
          
      '    There seems to be an issue with the page format and direct execute. Shell might think the % as the
      '     command line input. Need to find a escape mechanism. The batch file works just fine.
      '    If g_blnDirectExecute Then
      '        bRet = SuperShell(strCmd, strParentFolderName, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
      '    Else
90            Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
100           oBatFile.WriteLine strCmd
110           oBatFile.Close
120           Set oBatFile = Nothing
              
130           bRet = SuperShell(strBatchFile, strParentFolderName, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
140           Sleep 1000
      '    End If
          
150       DoEvents
          
160       strBurstFormat = Replace(strBurstFormat, strPageNumFormat, "*")
          
170       lBurstFileCount = 0
180       strBurstFile = Dir(strBurstFormat)
          
190       If strBurstFile <> "" Then
              'splitting success.
200           Trace "Done." & vbCrLf
210       End If
          
220       While strBurstFile <> ""
230           ReDim Preserve aBurstFiles(lBurstFileCount)
240           aBurstFiles(lBurstFileCount) = strBurstFile
250           strBurstFile = Dir()
260           lBurstFileCount = lBurstFileCount + 1
270       Wend
          
280       If lBurstFileCount > 0 Then
290           Trace "      Document was split into " & CStr(lBurstFileCount) & " pages." & vbCrLf & _
                    "      Merging them back to honor the size limit (" & CStr(lngSplitSizeBuffered) & " bytes) ..."
300       End If
          
310       Kill strBatchFile
          
320       If lBurstFileCount > 0 Then
330           iBatchCount = 1
340           strBatchFileName = gobjFSO.GetBaseName(strFileName) & "__" & Format(iBatchCount, "##00") & ".pdf"
              'now combine them
350           strCmdLine = """" & g_strPDFTKPath & """ "
360           Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
              'oBatFile.Write """" & g_strPDFTKPath & """ "
              
370           For i = 0 To lBurstFileCount - 1
380               lFileSize = FileLen(gobjFSO.BuildPath(strParentFolderName, aBurstFiles(i)))
                  
390               If ((lCombinedBatchSize + lFileSize) < lngSplitSize) And Len(strCmdLine & """" & aBurstFiles(i) & """ ") < 1000 Then
                      'oBatFile.Write """" & aBurstFiles(i) & """ "
400                   strCmdLine = strCmdLine & """" & aBurstFiles(i) & """ "
410                   lCombinedBatchSize = lCombinedBatchSize + lFileSize
420               Else
430                   oBatFile.Write strCmdLine & " cat output """ & strBatchFileName & """" & vbCrLf
440                   strCmdLine = """" & g_strPDFTKPath & """ """ & aBurstFiles(i) & """ "
450                   lCombinedBatchSize = lFileSize
460                   iBatchCount = iBatchCount + 1
470                   strBatchFileName = gobjFSO.GetBaseName(strFileName) & "__" & Format(iBatchCount, "##00") & ".pdf"
480               End If
490           Next
500           oBatFile.Write strCmdLine & " cat output """ & strBatchFileName & """" & vbCrLf
510           oBatFile.Close
              
              'oBatFile.WriteLine """" & g_strPDFTKPath & """ """ & strFileName & """ burst output """ & strBurstFormat & """"
520           Set oBatFile = Nothing
              
530           bRet = SuperShell(strBatchFile, strParentFolderName, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
              
540           DoEvents
              
550           Sleep 1000
              
              'delete the burst files
560           Kill strBurstFormat
              
570           lBurstFileCount = 0
580           strBurstFormat = gobjFSO.BuildPath(strParentFolderName, gobjFSO.GetBaseName(strFileName) & "__*.pdf")
590           strBurstFile = Dir(strBurstFormat)
              
600           If strBurstFile <> "" Then
610               Trace "Done." & vbCrLf & _
                        "      Merged Documents: " & vbCrLf
620           End If
630           While strBurstFile <> ""
640               ReDim Preserve aRet(lBurstFileCount)
650               Trace "         " & strBurstFile & vbCrLf
660               aRet(lBurstFileCount) = gobjFSO.BuildPath(strParentFolderName, strBurstFile)
                  'setPDFMetadata aRet(lBurstFileCount), g_strPDFMetaFile
670               strBurstFile = Dir()
680               lBurstFileCount = lBurstFileCount + 1
690           Wend
700       End If
          
errHandler:
710       If Err.Number <> 0 Then
720           Trace "Failed. ERROR: " & PROC_NAME & " [Line #" & Erl & "]. Description: " & Err.Description & vbCrLf
730       End If
          
740       splitPDF = aRet
End Function

Private Sub setDocPageMargin(ByRef objDoc As Document, ByVal strWhichMargin As String, ByVal strValue As String)
          Dim strMargin As String
          Dim strPageMetric As String
          
          Dim sngMargin As Single
          
10        If Not objDoc Is Nothing Then
              'calculate the margin value
20            If IsNumeric(strValue) Then
30                sngMargin = InchesToPoints(CSng(strValue))
40            Else
50                strPageMetric = Right(strValue, 2)
60                strMargin = Left(strValue, Len(strValue) - 2)
                  
70                If IsNumeric(strMargin) = False Then
80                    strMargin = "0.5"
90                    strPageMetric = "in"
100               End If
110               Select Case strPageMetric
                      Case "cm":
120                       sngMargin = CentimetersToPoints(strMargin)
130                   Case "mm":
140                       sngMargin = MillimetersToPoints(strMargin)
150                   Case Else
160                       sngMargin = InchesToPoints(strMargin)
170               End Select
180           End If
              
190           With objDoc.PageSetup
200               Select Case strWhichMargin
                      Case "top":
210                       .TopMargin = sngMargin
220                   Case "left":
230                       .LeftMargin = sngMargin
240                   Case "bottom":
250                       .BottomMargin = sngMargin
260                   Case "right":
270                       .RightMargin = sngMargin
280               End Select
290           End With
300       End If
End Sub

Private Function margin2Points(ByVal strValue As String) As Single
          Dim strMargin As String
          Dim strPageMetric As String
          
          Dim sngMargin As Single
          
          'calculate the margin value
10        If IsNumeric(strValue) Then
20            sngMargin = InchesToPoints(CSng(strValue))
30        Else
40            strPageMetric = Right(strValue, 2)
50            strMargin = Left(strValue, Len(strValue) - 2)
              
60            If IsNumeric(strMargin) = False Then
70                strMargin = "0.5"
80                strPageMetric = "in"
90            End If
              
100           If CSng(strMargin) > 0 Then
                  'convert margin to points.
110               Select Case strPageMetric
                      Case "cm":
120                       sngMargin = CInt(72 * CSng(strMargin) / 2.54)
130                   Case "mm":
140                       sngMargin = CInt((72 * CSng(strMargin)) / 25.4)
150                   Case Else
160                       sngMargin = CInt(72 * CSng(strMargin))
170               End Select
180           Else
190               sngMargin = 0
200           End If
210       End If
220       margin2Points = sngMargin
End Function

'***************************************************************************************
' Procedure : PDFFormFill
' DateTime  : 7/28/2008 14:36
' Author    : Ramesh Vishegu
' Purpose   : Fills a pdf file with data and flattens it out if requested
'***************************************************************************************
'
Public Function PDFFormFill(strPDFFile As String, strFDFFile As String, blnFlatten As Boolean) As String
10        On Error GoTo errHandler
          
          Dim strReturn As String
          Dim blnReturn As Boolean
          Dim strOutputFileName As String
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Dim strCmd As String
          Const PROC_NAME As String = "PDFFormFill()"
          
20        strReturn = ""
          
30        Trace "      Fill PDF document " & strPDFFile & " with " & strFDFFile & " [Flatten=" + blnFlatten + "] ... "
          
40        If strPDFFile <> "" And strFDFFile <> "" Then
50            strOutputFileName = getSafeFileName(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strPDFFile), gobjFSO.GetBaseName(strPDFFile) & "_filled.pdf"))
60            strCmd = """" & g_strPDFTKPath & """ """ & strPDFFile & """ fill_form """ & strFDFFile & """ output """ & strOutputFileName & """"
70            If blnFlatten Then
80                strCmd = strCmd & " flatten"
90            End If
              
100           If g_blnDirectExecute Then
110               blnReturn = SuperShell(strCmd, g_strSortTempPath, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
120           Else
130               strBatchFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strPDFFile), "pdfformfill.bat")
140               Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
150               oBatFile.Write """" & g_strPDFTKPath & """ """ & strPDFFile & """ fill_form """ & strFDFFile & """ output """ & strOutputFileName & """"
160               If blnFlatten Then
170                   oBatFile.Write " flatten"
180               End If
190               oBatFile.Write vbCrLf
200               oBatFile.Close
210               Set oBatFile = Nothing
                            
                  'execute the batch file
220               blnReturn = SuperShell(strBatchFile, gobjFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
230               Sleep 1000
240           End If
                        
250           DoEvents
              
260           If gobjFSO.FileExists(strOutputFileName) Then
270               strReturn = strOutputFileName
280               Trace "Done. " & vbCrLf
290           End If
300       End If
310       PDFFormFill = strReturn
          
errHandler:
320       If Err.Number <> 0 Then
330           PDFFormFill = strReturn
340           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
350           Trace "Failed." & g_strConvertErrMsg
360       End If
End Function

Public Function isPDFProtected(strFileName As String) As Boolean
10        On Error GoTo errHandler
          Dim blnProtected As Boolean
          Dim strCmd As String
          Dim strPDFDataFile As String
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Const PROC_NAME As String = "isPDFProtected()"
          
20        blnProtected = True
          
30        Trace "      Is PDF document " & strFileName & " protected? "
          
40        If (strFileName <> "" And gobjFSO.FileExists(strFileName)) Then
50            strPDFDataFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & "__data.txt")
              
60            strCmd = """" & g_strPDFTKPath & """ """ & strFileName & """ dump_data output """ & strPDFDataFile & """ dont_ask"
              
70            If g_blnDirectExecute Then
80                SuperShell strCmd, g_strSortTempPath, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True
90            Else
100               strBatchFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), "ispdfprotected.bat")
110               Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
120               oBatFile.Write strCmd
130               oBatFile.Close
140               Set oBatFile = Nothing
                            
                  'execute the batch file
150               SuperShell strBatchFile, gobjFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS
160               Sleep 1000
170           End If
              
              'if the pdf file is protected then pdftk will error out and no data file will be created.
180           blnProtected = (gobjFSO.FileExists(strPDFDataFile) = False)
              
190           Trace IIf(blnProtected, "Yes", "No") & vbCrLf
200       End If
errHandler:
210       If Err.Number <> 0 Then
220           Trace "Failed check. ERROR: " & PROC_NAME & " [Line #" & Erl & "]. Description: " & Err.Description & vbCrLf
230       End If
          
240       isPDFProtected = blnProtected
End Function

Public Function unprotectPDF(strFileName As String) As Boolean
10        On Error GoTo errHandler
          Dim strUnprotectedPDFFileName As String
          Dim strCmd As String
          Dim blnReturn As Boolean
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Const PROC_NAME As String = "unprotectPDF()"
          
20        blnReturn = False
          
30        Trace "      Unprotecting PDF document " & strFileName & " ... "
          
40        If (strFileName <> "" And gobjFSO.FileExists(strFileName)) Then
50            strUnprotectedPDFFileName = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), gobjFSO.GetBaseName(strFileName) & "__unprotected.pdf")
              
60            strCmd = """" & g_strGSPath & """ -q -dSAFER -dNOPAUSE -dBATCH -sOutputFile=""" & strUnprotectedPDFFileName & """ -sDEVICE=pdfwrite """ & strFileName & """"
              
70            If g_blnDirectExecute Then
80                SuperShell strCmd, gobjFSO.GetParentFolderName(strFileName), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True
90            Else
100               strBatchFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), "unprotectpdf.bat")
110               Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
120               oBatFile.Write strCmd
130               oBatFile.Close
140               Set oBatFile = Nothing
                            
                  'execute the batch file
150               SuperShell strBatchFile, gobjFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS
160               Sleep 1000
170           End If
              
180           If gobjFSO.FileExists(strUnprotectedPDFFileName) Then
                  'delete the original file
190               gobjFSO.DeleteFile strFileName, True
                  
                  'rename the unprotected file as the original file
200               gobjFSO.MoveFile strUnprotectedPDFFileName, strFileName
210               blnReturn = True
220               Trace "Success." & vbCrLf
230           Else
240               blnReturn = False
250               Trace "Failed. Missing unprotected file." & vbCrLf
260           End If
270       Else
280           Trace "Invalid file." & vbCrLf
290       End If
errHandler:
300       If Err.Number <> 0 Then
310           Trace "Failed. ERROR: " & PROC_NAME & " [Line #" & Erl & "]. Description: " & Err.Description & vbCrLf
320       End If
330       unprotectPDF = blnReturn
End Function
