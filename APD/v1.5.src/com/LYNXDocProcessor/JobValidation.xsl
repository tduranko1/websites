<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"   xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text" encoding="UTF-8" omit-xml-declaration="yes"/>
<xsl:decimal-format NaN="0"/>

<xsl:template match="/Job">
    <!-- must contain a package -->
    <xsl:if test="count(Package) = 0">
        <xsl:text>Missing Package information.|</xsl:text>
    </xsl:if>
    
    <!-- Package validations -->
    <xsl:if test="Package/@type != 'pdf' and Package/@type != 'tif' and Package/@type != 'pdf-zip' and Package/@type != 'tif-zip'">
        <xsl:text>Unsupported Package type.|</xsl:text>
    </xsl:if>
    
    <!-- Package must contain atleast one supported destination method -->
    <xsl:if test="count(Package/Email) + count(Package/Fax) + count(Package/FTP) + count(Package/Filecopy) + count(Package/HTTPPost) = 0">
        <xsl:text>Package must contain one or more the destination methods (Email/Fax/FTP/Filecopy/HTTPPost).|</xsl:text>
    </xsl:if>

    <!-- Validate emails -->
    <xsl:for-each select="//Email">
        <xsl:call-template name="EmailValidation"/>
    </xsl:for-each>

    <xsl:for-each select="//Success">
        <xsl:call-template name="EmailValidation"/>
    </xsl:for-each>

    <xsl:for-each select="//Error">
        <xsl:call-template name="EmailValidation"/>
    </xsl:for-each>
    
    <!-- Validate phone type fields -->
    <xsl:if test="count(Package/Fax) &gt; 0">
        <xsl:for-each select="Package/Fax">
            <xsl:call-template name="PhoneValidation"/>
        </xsl:for-each>
    </xsl:if>

    <!-- Validate FTP fields -->
    <xsl:if test="count(Package/FTP) &gt; 0">
        <xsl:for-each select="Package/FTP">
            <xsl:call-template name="FTPValidation"/>
        </xsl:for-each>
    </xsl:if>
    
    <!-- Validate File copy destination UNC path -->
    <xsl:if test="count(Package/Filecopy) &gt; 0">
        <xsl:for-each select="Package/Filecopy">
            <xsl:call-template name="FilecopyValidation"/>
        </xsl:for-each>
    </xsl:if>

    <!-- Validate HTTP Post destination URL -->
    <xsl:if test="count(Package/HTTPPost) &gt; 0">
        <xsl:for-each select="Package/HTTPPost">
            <xsl:call-template name="HTTPPostValidation"/>
        </xsl:for-each>
    </xsl:if>
    
    <!-- Document Validations -->
    <xsl:choose>
        <xsl:when test="count(Document) = 0">
            <xsl:text>No documents to process.|</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:for-each select="Document">
                <xsl:call-template name="DocumentValidation"/>
            </xsl:for-each>
        </xsl:otherwise>
    </xsl:choose>

</xsl:template>

<xsl:template name="EmailValidation">
    <!-- Email must contain the To address -->
    <xsl:if test="To = ''">
        <xsl:text>Missing To information in the Email.|</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template name="PhoneValidation">
    <!-- Phone number must be atleast 10 digits long -->
    <xsl:if test="Package/Fax/@to = ''">
        <xsl:text>Missing Fax destination number.|</xsl:text>
    </xsl:if>

    <xsl:if test="string-length(Package/Fax/@to) &lt; 10">
        <xsl:text>Fax destination number must be atleast 10 digits long.|</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template name="FTPValidation">
    <!-- FTP server address, logon id and password must be defined -->
    <xsl:if test="@remoteServer = ''">
        <xsl:text>Missing FTP remote server.|</xsl:text>
    </xsl:if>
    <xsl:if test="@remoteUID = ''">
        <xsl:text>Missing FTP remote server logon user id.|</xsl:text>
    </xsl:if>
    <xsl:if test="@remotePWD = ''">
        <xsl:text>Missing FTP remote server logon password.|</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template name="FilecopyValidation">
    <!-- File copy operation must specify the UNC destination folder -->
    <xsl:if test="UNC = ''">
        <xsl:text>Missing UNC Path for File copy.|</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template name="HTTPPostValidation">
    <!-- HTTP Post must specify the destination URL. -->
    <xsl:if test="@url = ''">
        <xsl:text>Missing URL for HTTPPost.|</xsl:text>
    </xsl:if>
</xsl:template>

<xsl:template name="DocumentValidation">
    <!--
        One of the following is required:
        1. filefullpath specified
        2. filepath and filename specified.
        
        * if Data node exists then the filetype must be specified
        * if filetype = xml then xslpath or XSLData must be specified.
    -->
    <xsl:variable name="fullpath">
        <xsl:choose>
            <xsl:when test="@filefullpath != ''"><xsl:value-of select="@filefullpath"/></xsl:when>
            <xsl:when test="@filepath != '' and @filename != ''"><xsl:value-of select="concat(@filepath, @filename)"/></xsl:when>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="filetype">
        <xsl:if test="@filetype != ''"><xsl:value-of select="@filetype"/></xsl:if>
    </xsl:variable>
    <xsl:variable name="fileext">
        <xsl:value-of select="substring($fullpath, string-length($fullpath) - 2)"/>
    </xsl:variable>
    
    <xsl:variable name="filetype2">
        <xsl:choose>
            <xsl:when test="$filetype != ''">
                <xsl:value-of select="translate($filetype, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="translate($fileext, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="xslpath"><xsl:value-of select="@xslpath"/></xsl:variable>
    <xsl:variable name="xsldata"><xsl:value-of select="substring(XSLData, 1, 10)"/></xsl:variable>
    <xsl:variable name="docdata"><xsl:value-of select="substring(Data, 1, 10)"/></xsl:variable>
    
    <xsl:if test="$fullpath != '' and $filetype = ''">
        <!-- the fourth character from the right must be . -->
        <xsl:if test="substring($fullpath, string-length($fullpath) - 3, 1) != '.'">
            <xsl:text>Cannot determine the embedded data type from the filefullpath or filepath/filename for Document node #</xsl:text><xsl:value-of select="position()"/><xsl:text>.|</xsl:text>
        </xsl:if>
    </xsl:if>
    <xsl:if test="$filetype2 = ''">
        <xsl:text>Missing file type for Document node #</xsl:text><xsl:value-of select="position()"/><xsl:text>.|</xsl:text>
    </xsl:if>
    <xsl:if test="$docdata = ''">
        <xsl:if test="$fullpath = ''">
            <xsl:text>Missing Data node or missing file UNC path information in either filefullpath or filepath/filename for Document node #</xsl:text><xsl:value-of select="position()"/><xsl:text>.|</xsl:text>
        </xsl:if>
        <xsl:if test="$filetype2 = ''">
            <xsl:text>Missing file type for Document node #</xsl:text><xsl:value-of select="position()"/><xsl:text>.|</xsl:text>
        </xsl:if>
    </xsl:if>
    <xsl:if test="$docdata != '' and @filetype = ''">
        <xsl:text>Missing file type for Document node #</xsl:text><xsl:value-of select="position()"/><xsl:text>.|</xsl:text>
    </xsl:if>

    <!-- check for unsupported document -->
    <xsl:choose>
        <xsl:when test="$filetype2 = 'pdf'"/>
        <xsl:when test="$filetype2 = 'doc' or $filetype2 = 'rtf'"/>
        <xsl:when test="$filetype2 = 'xls'"/>
        <xsl:when test="$filetype2 = 'txt'"/>
        <xsl:when test="$filetype2 = 'htm' or $filetype2 = 'html'"/>
        <xsl:when test="$filetype2 = 'xml'">
            <xsl:if test="$xslpath = '' and $xsldata = ''">
                <xsl:text>Missing XSL transformation file name / XSLData for Document node#</xsl:text><xsl:value-of select="position()"/><xsl:text>.|</xsl:text>
            </xsl:if>
        </xsl:when>
        <xsl:when test="$filetype2 = 'rpt'"/>
        <xsl:when test="$filetype2 = 'tif' or $filetype2 = 'tiff'"/>
        <xsl:when test="$filetype2 = 'jpg' or $filetype2 = 'jpeg'"/>
        <xsl:when test="$filetype2 = 'bmp'"/>
        <xsl:when test="$filetype2 = 'gif'"/>
        <xsl:otherwise>
            <xsl:text>Unsupported document type "</xsl:text><xsl:value-of select="$filetype2"/><xsl:text>"; Document node#</xsl:text><xsl:value-of select="position()"/><xsl:text>.|</xsl:text>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>