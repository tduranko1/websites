Attribute VB_Name = "modZipUtils"
Option Explicit
Option Compare Text

Public Function splitZIP(strFileName As String, lngSplitSize As Long) As String()

End Function

Public Function zipFiles(strFileNames() As String, strOutputName As String) As Boolean

End Function

Private Function zipFolder_old(strFolderName As String, strOutputName As String) As Boolean
10        On Error GoTo errHandler
          Dim lStartTick As Long
          Dim strSourceFolder As String
          Dim m_cZ As cZip
          Dim m_cUnzip As cUnzip

          
20        lStartTick = GetTickCount()
30        Trace "        Archiving files in " & strFolderName & " to " & strOutputName & " ... "
40        If gobjFSO.GetExtensionName(strOutputName) <> "zip" Then
50            strOutputName = strOutputName & ".zip"
60        End If
          
70        strSourceFolder = strFolderName & "\*.*"
          
80        Set m_cZ = New cZip
          
90        With m_cZ
100          .ZipFile = strOutputName
             
110          .AllowAppend = True
             
120          .ClearFileSpecs
130          .AddFileSpec strFolderName
140          .StoreFolderNames = True
150          .RecurseSubDirs = True
160          .MessageLevel = ezpAllMessages
170          .Zip
          
180          If (.Success) Then
                  'test the zip file
190               zipFolder_old = True
200               Set m_cUnzip = New cUnzip
210               m_cUnzip.ZipFile = strOutputName
220               If m_cUnzip.TestZip = True Then
230                   zipFolder_old = True
240               End If
250          Else
260              zipFolder_old = False
270          End If
280       End With
290       Trace "Done. [" & (GetTickCount() - lStartTick) & " ms]" & vbCrLf
errHandler:
300       Set m_cZ = Nothing
310       Set m_cUnzip = Nothing
320       If Err.Number <> 0 Then
330           Trace "FAILED. [Line #" & Erl & "] " & Err.Description & vbCrLf
340           Err.Clear
350       End If
End Function

Private Function zipFolder(strFolderName As String, strOutputName As String) As Boolean
10        On Error GoTo errHandler
          Dim bRet As Boolean
          Dim strOutputFileName As String
          Dim strBatchFile As String
          Dim oBatFile As TextStream
          Dim strCmd As String
          Dim lStartTick As Long
          
20        If gobjFSO.FolderExists(strFolderName) Then
30            Trace "        Archiving files in " & strFolderName & " to " & strOutputName & " ... "
40            strOutputFileName = gobjFSO.GetBaseName(strOutputName) & "_archive.zip"
50            strCmd = """" & g_str7zipPath & """ a -tzip """ & strOutputFileName & """ " & "*.*"
              
60            If g_blnDirectExecute Then
                  'execute the command
70                bRet = SuperShell(strCmd, strFolderName, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
80            Else
90                strBatchFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFolderName), "toarchive.bat")
                  
100               Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
110               oBatFile.WriteLine strCmd
120               oBatFile.Close
130               Set oBatFile = Nothing
                  
                  'execute the batch file
140               bRet = SuperShell(strBatchFile, gobjFSO.GetParentFolderName(strFolderName), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
150               Sleep 1000
160           End If
              
170           DoEvents
              
              'cleanup
              'gobjFSO.DeleteFile strBatchFile
              'safeDelete strBatchFile
              
180           strOutputFileName = gobjFSO.BuildPath(strFolderName, strOutputFileName)
              
190           If gobjFSO.FileExists(strOutputFileName) = False Then
200               Trace "FAILED. Temporary Archive file " & strOutputFileName & " not found" & vbCrLf
210               zipFolder = False
220           Else
                  'setPDFMetadata strOutputFileName, g_strPDFMetaFile
230               If gobjFSO.FileExists(strOutputFileName) Then
240                   strOutputName = getSafeFileName(strOutputName)
250                   gobjFSO.MoveFile gobjFSO.BuildPath(strFolderName, strOutputFileName), strOutputName
260                   zipFolder = True
270                   Trace "Done. [" & (GetTickCount() - lStartTick) & " ms]" & vbCrLf
280               Else
290                   zipFolder = False
300                   Trace "FAILED. [Line #" & Erl & "] " & Err.Description & vbCrLf
310               End If
320               bRet = True
330           End If
340       Else
350           bRet = False
360       End If
          
          'If bDeleteOriginal Then safeDelete strFileName
          
errHandler:
370       If Err.Number <> 0 Then
380           zipFolder = False
390           g_strConvertErrMsg = "Error in ZipFolder [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
400           Trace "FAILED. [Line #" & Erl & "] " & Err.Description & vbCrLf
410       End If
End Function

