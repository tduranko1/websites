VERSION 5.00
Object = "{DE8CE233-DD83-481D-844C-C07B96589D3A}#1.1#0"; "VBALSG~1.OCX"
Begin VB.Form frmMain 
   Caption         =   "LYNX Document Processor"
   ClientHeight    =   4335
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   7680
   LinkTopic       =   "Form1"
   ScaleHeight     =   289
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   512
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer trmRefresh 
      Enabled         =   0   'False
      Interval        =   10000
      Left            =   7200
      Top             =   3840
   End
   Begin VB.Frame frmTools 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   555
      Left            =   60
      TabIndex        =   1
      Top             =   3720
      Width           =   7215
      Begin VB.CommandButton cmdRefresh 
         Caption         =   "Refresh"
         Height          =   495
         Left            =   3180
         TabIndex        =   5
         Top             =   60
         Width           =   1215
      End
      Begin VB.CommandButton cmdPriority 
         Caption         =   "Set Priority"
         Enabled         =   0   'False
         Height          =   495
         Left            =   1320
         TabIndex        =   4
         Top             =   60
         Width           =   1215
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "Delete"
         Enabled         =   0   'False
         Height          =   495
         Left            =   60
         TabIndex        =   3
         Top             =   60
         Width           =   1215
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   495
         Left            =   5820
         TabIndex        =   2
         Top             =   60
         Width           =   1215
      End
   End
   Begin vbAcceleratorSGrid6.vbalGrid grdQueue 
      Height          =   3435
      Left            =   0
      TabIndex        =   0
      Top             =   60
      Width           =   7635
      _ExtentX        =   13467
      _ExtentY        =   6059
      RowMode         =   -1  'True
      GridLines       =   -1  'True
      BackgroundPictureHeight=   0
      BackgroundPictureWidth=   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeaderHotTrack  =   0   'False
      HeaderFlat      =   -1  'True
      ScrollBarStyle  =   2
      DisableIcons    =   -1  'True
      HideGroupingBox =   -1  'True
   End
   Begin VB.Menu mnuProcessor 
      Caption         =   "Processor"
      Begin VB.Menu mnuRefresh 
         Caption         =   "Refresh"
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSettings 
         Caption         =   "Settings"
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu mnuPriority 
      Caption         =   "Priority"
      Visible         =   0   'False
      Begin VB.Menu mnuPriorityHigh 
         Caption         =   "High"
      End
      Begin VB.Menu mnuPriorityNormal 
         Caption         =   "Normal"
      End
      Begin VB.Menu mnuPriorityLow 
         Caption         =   "Low"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Compare Text

Private Sub cmdClose_Click()
    End
End Sub

Private Sub cmdDelete_Click()
    Dim strJobID As String
    
    If MsgBox("Do you want to delete this job?", vbYesNo + vbExclamation, APP_TITLE) = vbYes Then
        strJobID = grdQueue.CellText(grdQueue.SelectedRow, grdQueue.SelectedCol)
        
        If strJobID <> "" Then
            If deleteJob(strJobID) Then
                trmRefresh_Timer
            End If
        End If
    End If
End Sub

Private Sub cmdPriority_Click()
    trmRefresh.Enabled = False
    PopupMenu mnuPriority
    trmRefresh.Enabled = True
End Sub

Private Sub cmdRefresh_Click()
    trmRefresh_Timer
End Sub

Private Sub Form_Load()
    With grdQueue
        .AddColumn "title", "Document Title", , , 225
        .AddColumn "status", "Status", , , 100
        .AddColumn "createdon", "Created On", , , 120
        .AddColumn "priority", "Priority", , , 50
        .AddColumn "id", "Job ID", , , 0
        .DefaultRowHeight = 18
    End With
    initGlobals
    
    g_strUserName = GetUserName()
    
    g_strQueuePath = resolveMacro(getSetting("/DocProcessor/Paths/Queue", , False))
    g_strJobQueue = gobjFSO.BuildPath(g_strQueuePath, "jobs.xml")
    g_strPickupPath = resolveMacro(getSetting("/DocProcessor/Paths/Pickup", , False))
    
    g_blnAppTrace = (LCase(getSetting("/DocProcessor/AppLog", "enabled", False)) = "true")
    g_strAppLogFile = getSetting("/DocProcessor/AppLog", , False)
    
    
    If g_strAppLogFile = "" And g_blnAppTrace = True Then
        g_strAppLogFile = "C:\Logs\LYNX Doc Processor"
    End If
    
    g_strJobQueue = gobjFSO.BuildPath(g_strQueuePath, "Jobs.xml")
    
    trmRefresh_Timer
End Sub

Private Sub Form_Resize()
    frmTools.Top = Me.ScaleHeight - frmTools.Height - 5
    frmTools.Left = 0
    frmTools.Width = Me.ScaleWidth
    grdQueue.Height = Me.ScaleHeight - 45
    grdQueue.Width = Me.ScaleWidth
    cmdClose.Left = Me.Width - cmdClose.Width - 150
End Sub

Private Sub Form_Unload(Cancel As Integer)
    terminateGlobals
End Sub

Private Sub grdQueue_SelectionChange(ByVal lRow As Long, ByVal lCol As Long)
    cmdDelete.Enabled = True
    cmdPriority.Enabled = True
End Sub

Private Sub mnuExit_Click()
    cmdClose_Click
End Sub

Private Sub mnuPriorityHigh_Click()
    Dim strJobID As String
    If grdQueue.SelectedRow > 0 Then
        strJobID = grdQueue.CellText(grdQueue.SelectedRow, 5)
        
        If strJobID <> "" Then
            If setJobPriority(strJobID, "High") Then
                trmRefresh_Timer
            End If
        End If
    End If
End Sub

Private Sub mnuPriorityLow_Click()
    Dim strJobID As String
    If grdQueue.SelectedRow > 0 Then
        strJobID = grdQueue.CellText(grdQueue.SelectedRow, 5)
        
        If strJobID <> "" Then
            If setJobPriority(strJobID, "Low") Then
                trmRefresh_Timer
            End If
        End If
    End If
End Sub

Private Sub mnuPriorityNormal_Click()
    Dim strJobID As String
    If grdQueue.SelectedRow > 0 Then
        strJobID = grdQueue.CellText(grdQueue.SelectedRow, 5)
        
        If strJobID <> "" Then
            If setJobPriority(strJobID, "Normal") Then
                trmRefresh_Timer
            End If
        End If
    End If
End Sub

Private Sub mnuSettings_Click()
    trmRefresh.Enabled = False
    
    frmConfig.Show vbModal
    
    trmRefresh.Enabled = True
End Sub

Private Sub trmRefresh_Timer()
    Dim strCaption As String
    
    Dim iRowSelected As Integer
    
    Me.MousePointer = vbHourglass
    
    iRowSelected = grdQueue.SelectedRow
    
    strCaption = Me.Caption
    
    Me.Caption = strCaption & " [Refreshing]"
    
    trmRefresh.Enabled = False
    
    grdQueue.Clear False
    
    getJobsQueue ("High")
    
    getJobsQueue ("Normal")
    
    getJobsQueue ("Low")
    
    trmRefresh.Enabled = True
    
    If iRowSelected > 0 And grdQueue.Rows >= iRowSelected Then
        grdQueue.SelectedRow = iRowSelected
    End If
    
    Me.Caption = strCaption
    
    Me.MousePointer = vbDefault
End Sub

Private Sub getJobsQueue(strPriority As String)
    Dim strFileName As String
    Dim strFilePattern As String
    Dim iRowIndex As Integer
    Dim strPriorityTxt As String
    Dim strPriorityPrefix As String
    Dim strJobID As String
    Dim dtCreatedOn As Date
    Dim oFile As File
    Dim iForeColor As Long
    Dim strJobStatus As String
    
    Select Case strPriority
        Case "high":
            strPriorityPrefix = HIGH_PRIORITY
            iForeColor = vbMagenta
        Case "normal":
            strPriorityPrefix = NORMAL_PRIORITY
            iForeColor = vbBlue
        Case "low":
            strPriorityPrefix = LOW_PRIORITY
            iForeColor = vbBlack
    End Select
    
    strFilePattern = gobjFSO.BuildPath(g_strQueuePath, strPriorityPrefix & "*.*")
    strFileName = Dir(strFilePattern)
    While strFileName <> ""
        strFileName = gobjFSO.BuildPath(g_strQueuePath, strFileName)
        Set oFile = gobjFSO.GetFile(strFileName)
        dtCreatedOn = oFile.DateCreated
        Set oFile = Nothing
        strJobID = Mid(gobjFSO.GetBaseName(strFileName), 3)
        grdQueue.AddRow
        iRowIndex = grdQueue.Rows
        With grdQueue
            strJobStatus = getJobStatus(strJobID)
            
            If strJobStatus = "Processing" Then
                .CellForeColor(iRowIndex, 1) = vbRed
                .CellForeColor(iRowIndex, 2) = vbRed
                .CellForeColor(iRowIndex, 3) = vbRed
                .CellForeColor(iRowIndex, 4) = vbRed
                .CellForeColor(iRowIndex, 5) = vbRed
            Else
                .CellForeColor(iRowIndex, 1) = iForeColor
                .CellForeColor(iRowIndex, 2) = iForeColor
                .CellForeColor(iRowIndex, 3) = iForeColor
                .CellForeColor(iRowIndex, 4) = iForeColor
                .CellForeColor(iRowIndex, 5) = iForeColor
            End If
            
            .CellText(iRowIndex, 1) = getJobTitle(strJobID)
            .CellText(iRowIndex, 2) = strJobStatus
            .CellText(iRowIndex, 3) = Format(dtCreatedOn, "mm/dd/yyyy hh:nn:ss")
            .CellText(iRowIndex, 4) = strPriority
            .CellText(iRowIndex, 5) = strJobID
        End With
        strFileName = Dir()
    Wend
End Sub

