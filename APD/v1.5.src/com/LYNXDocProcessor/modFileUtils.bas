Attribute VB_Name = "modFileUtils"
Option Explicit
Option Compare Text

Public Const ERROR_SHARING_VIOLATION         As Long = &H20
Public Const ERROR_INSUFFICIENT_BUFFER       As Long = 122
Public Const ERROR_MORE_DATA                 As Long = 234
Public Const ERROR_NO_MORE_ITEMS             As Long = &H103
Public Const ERROR_KEY_NOT_FOUND             As Long = &H2
Public Const ERROR_ALREADY_EXISTS            As Long = 183&
Public Const ERROR_ACCESS_DENIED             As Long = 5&

Public Const GENERIC_READ                    As Long = &H80000000
Public Const INVALID_HANDLE_VALUE            As Long = -1
Public Const OPEN_EXISTING                   As Long = &H3

Public Const FILE_ATTRIBUTE_ARCHIVE          As Long = &H20
Public Const FILE_ATTRIBUTE_COMPRESSED       As Long = &H800
Public Const FILE_ATTRIBUTE_DIRECTORY        As Long = &H10
Public Const FILE_ATTRIBUTE_HIDDEN           As Long = &H2
Public Const FILE_ATTRIBUTE_NORMAL           As Long = &H80
Public Const FILE_ATTRIBUTE_READONLY         As Long = &H1
Public Const FILE_ATTRIBUTE_SYSTEM           As Long = &H4

Public Const FORMAT_MESSAGE_FROM_SYSTEM      As Long = &H1000
Public Const FORMAT_MESSAGE_IGNORE_INSERTS   As Long = &H200
Public Const LANG_NEUTRAL                    As Long = &H0

Public Declare Function CreateFile Lib "kernel32.dll" Alias "CreateFileA" (ByVal lpFileName As String, ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, ByVal lpSecurityAttributes As Long, ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long) As Long
Public Declare Function CloseHandle Lib "kernel32.dll" (ByVal hObject As Long) As Long
Public Declare Function FormatMessage Lib "kernel32.dll" Alias "FormatMessageA" (ByVal dwFlags As Long, ByVal lpSource As Long, ByVal dwMessageId As Long, ByVal dwLanguageId As Long, ByVal lpBuffer As String, nSize As Long, Arguments As Long) As Long

Public Function FileInUse(ByVal sFileName As String, Optional ByRef sErrorMsg As String = vbNullString) As Boolean
          Dim lFile As Long, lError As Long, lResult As Long, lBufLen As Long, sBuffer As String
10        On Error GoTo Err_FileInUse
20        lFile = CreateFile(sFileName, GENERIC_READ, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0)
30        If lFile = INVALID_HANDLE_VALUE Then
40        lError = Err.LastDllError
50        If lError = ERROR_SHARING_VIOLATION Then
60        FileInUse = True
70        Else
80        sBuffer = String(256, 0)
90        lResult = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, lError, 0, sBuffer, Len(sBuffer), 0)
100       If lResult > 0 Then
110       sErrorMsg = Left$(sBuffer, lResult - 1)
120       Else
130       sErrorMsg = "Unknown error!"
140       End If
150       End If
160       Else
170       CloseHandle lFile
180       End If
Exit_FileInUse:
190       On Error GoTo 0
200       Exit Function
          
Err_FileInUse:
210       If lFile = INVALID_HANDLE_VALUE Then
220       CloseHandle lFile
230       End If
240       If Err.Number <> 0 Then
250       sErrorMsg = Err.Description
260       End If
270       On Error GoTo 0
End Function

Public Function safeDelete(strFileName As String) As Boolean
          Dim iTimeout As Integer
10        iTimeout = 60
          
20        If gobjFSO.FileExists(strFileName) Then
30            While FileInUse(strFileName) And iTimeout > 0
40                Sleep 1000
50                iTimeout = iTimeout - 1
60            Wend
              
70            If iTimeout > 0 Then
80                gobjFSO.DeleteFile strFileName, True
90                safeDelete = True
100           Else
110               safeDelete = False
120           End If
130       Else
140           safeDelete = True
150       End If
End Function

Public Function cleanFileName(strFileName As String) As String
          Dim strBaseFileName As String
          Dim strRet As String
          Dim i As Integer
          Dim sChar As String
          
10        If strFileName <> "" Then
20            strRet = gobjFSO.GetParentFolderName(strFileName)
30            strBaseFileName = gobjFSO.GetBaseName(strFileName)
              
40            If gobjFSO.GetExtensionName(strFileName) <> "" Then
50                strBaseFileName = strBaseFileName & "." & gobjFSO.GetExtensionName(strFileName)
60            End If
              
              'replace illegal chars
70            strBaseFileName = Replace(strBaseFileName, "\", "_")
80            strBaseFileName = Replace(strBaseFileName, "/", "_")
90            strBaseFileName = Replace(strBaseFileName, ":", "_")
100           strBaseFileName = Replace(strBaseFileName, "*", "_")
110           strBaseFileName = Replace(strBaseFileName, "?", "_")
120           strBaseFileName = Replace(strBaseFileName, """", "_")
130           strBaseFileName = Replace(strBaseFileName, "<", "_")
140           strBaseFileName = Replace(strBaseFileName, ">", "_")
150           strBaseFileName = Replace(strBaseFileName, "|", "_")
              
              'now replace any extended chars
160           For i = 1 To Len(strBaseFileName)
170               sChar = Mid(strBaseFileName, i, 1)
180               If Asc(sChar) < 32 Or Asc(sChar) > 122 Then
190                   strBaseFileName = Replace(strBaseFileName, sChar, "_")
200               End If
210           Next
              
220           strRet = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), strBaseFileName)
230       End If
          
240       cleanFileName = strRet
End Function

Public Function splitFile(strFileName As String, lngSplitSize As Long) As String()
          Dim strFileExt As String
          Dim strRet(0) As String
          
10        If gobjFSO.FileExists(strFileName) Then
20            strFileExt = gobjFSO.GetExtensionName(strFileName)
30            If strFileExt = "tif" Then
40                splitFile = splitTIF(strFileName, lngSplitSize)
50            ElseIf strFileExt = "pdf" Then
60                splitFile = splitPDF(strFileName, lngSplitSize)
70            Else
80                strRet(0) = strFileName
90                splitFile = strRet
100           End If
110       End If
End Function

Public Function getSafeFileName(strFileName As String) As String
          Dim iCount As Integer
          Dim strFolderPath As String
          Dim strFileBaseName As String
          Dim strFileExt As String
          Dim strRet As String
          
10        If strFileName <> "" Then
20            iCount = 1
30            strFolderPath = gobjFSO.GetParentFolderName(strFileName)
40            strFileBaseName = gobjFSO.GetBaseName(strFileName)
50            strFileExt = gobjFSO.GetExtensionName(strFileName)
60            strRet = strFileName
70            While gobjFSO.FileExists(strRet)
80                strRet = gobjFSO.BuildPath(strFolderPath, strFileBaseName & "_" & CStr(iCount))
90                If strFileExt <> "" Then
100                   strRet = strRet & "." & strFileExt
110               End If
120               iCount = iCount + 1
130           Wend
140       End If
150       getSafeFileName = strRet
End Function

Public Function convertedFileExists(strOriginalFileName As String, strConvertedFileFormat As String) As String
          Dim strConvertedFileName As String
10        If strOriginalFileName <> "" Then
20            strConvertedFileName = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strOriginalFileName), gobjFSO.GetBaseName(strOriginalFileName) & "." & strConvertedFileFormat)
30            If gobjFSO.FileExists(strConvertedFileName) Then
40                convertedFileExists = strConvertedFileName
50            End If
60        End If
End Function

Public Function BuildPath(sPath As String) As Boolean
10        On Error GoTo errHandler
          Const PROC_NAME As String = "BuildPath()"
          Dim sFolders() As String
          Dim sCurrentFolder As String
          Dim i As Integer
          Dim bUNCPath As Boolean
          
20        bUNCPath = (Left(sPath, 2) = "\\")
              
30        sCurrentFolder = ""
          
40        If bUNCPath = False Then
              'create the logging path
50            sFolders = Split(sPath, "\")
60        Else
70            sCurrentFolder = Mid(sPath, 1, InStr(4, sPath, "\") - 1)
80            sFolders = Split(Mid(sPath, InStr(4, sPath, "\") + 1), "\")
90        End If
          
100       For i = 0 To UBound(sFolders)
110           If InStr(1, sFolders(i), ":") = 0 Then
120               If i > 0 Then sCurrentFolder = sCurrentFolder & "\"
130               sCurrentFolder = gobjFSO.BuildPath(sCurrentFolder, sFolders(i))
                  
140               If Not gobjFSO.FolderExists(sCurrentFolder) Then gobjFSO.CreateFolder (sCurrentFolder)
150           Else
160               sCurrentFolder = sCurrentFolder & sFolders(i)
170           End If
180       Next
190       BuildPath = True
errHandler:
200       If Err.Number <> 0 Then
210           BuildPath = False
220       End If
End Function

