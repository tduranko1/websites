Attribute VB_Name = "modConfig"
Option Explicit
Option Compare Text

Const MODULE_NAME As String = "modConfig:"

Private mobjConfigXML As MSXML2.DOMDocument
Private mstrConfigFile As String

Private Enum EventCodes
    eMissingConfig = &H80070000 + &H100
    eConfigNotLoaded
End Enum

Public Sub loadConfig()
          Const PROC_NAME As String = MODULE_NAME & "loadConfig()"
          
10        mstrConfigFile = gobjFSO.BuildPath(App.Path, "config.xml")
20        If gobjFSO.FileExists(mstrConfigFile) Then
30            Set mobjConfigXML = loadXMLFile(mstrConfigFile)
40        Else
50            Err.Raise eMissingConfig, PROC_NAME, "Missing Configuration file."
60        End If
End Sub

Public Function getSetting(strPath As String, Optional strAttrib As String, Optional blnMustExist As Boolean) As String
          Const PROC_NAME As String = "getSetting()"
          Dim strRet As String
          Dim oNode As IXMLDOMElement
10        If Not mobjConfigXML Is Nothing Then
20            If strPath <> "" Then
30                Set oNode = mobjConfigXML.selectSingleNode(strPath)
40                If Not oNode Is Nothing Then
50                    If oNode.nodeType = NODE_ATTRIBUTE Then
60                        strRet = oNode.Text
70                    ElseIf oNode.nodeType = NODE_ELEMENT Then
80                        If strAttrib <> "" Then
90                            strRet = IIf(IsNull(oNode.getAttribute(strAttrib)), "", oNode.getAttribute(strAttrib))
100                       Else
110                           strRet = oNode.Text
120                       End If
130                   Else
140                       strRet = ""
150                   End If
160               Else
170                   If blnMustExist Then
180                       Err.Raise eXmlMissingElement, PROC_NAME, "Cannot find " & strPath & "."
190                   End If
200               End If
210           End If
220       Else
230           Err.Raise eConfigNotLoaded, PROC_NAME, "Configuration file not loaded."
240       End If
          
250       getSetting = strRet
End Function

Public Sub setSetting(strPath As String, strAttrib As String, strValue As String)
          Const PROC_NAME As String = "setSetting()"
          Dim strRet As String
          Dim oNode As IXMLDOMElement
10        If Not mobjConfigXML Is Nothing Then
20            If strPath <> "" Then
30                Set oNode = mobjConfigXML.selectSingleNode(strPath)
40                If Not oNode Is Nothing Then
50                    If oNode.nodeType = NODE_ELEMENT Then
60                        If strAttrib <> "" Then
70                            oNode.setAttribute strAttrib, strValue
80                        Else
90                            oNode.Text = strValue
100                       End If
110                   End If
120               End If
130           End If
140       Else
150           Err.Raise eConfigNotLoaded, PROC_NAME, "Configuration file not loaded."
160       End If
End Sub

Public Sub saveConfig()
10        If Not mobjConfigXML Is Nothing Then
20            mobjConfigXML.Save mstrConfigFile
30        End If
End Sub

Public Function unloadConfig()
10        Set mobjConfigXML = Nothing
End Function

Public Function resolveMacro(strValue) As String
          Dim strRet As String
          
          'application path
10        strRet = Replace(strValue, "$AppPath$", App.Path)
          
          'now
20        strRet = Replace(strRet, "$now$", Format(Now, "mm/dd/yyyy hh:nn:ss"))
          
30        resolveMacro = strRet
End Function

