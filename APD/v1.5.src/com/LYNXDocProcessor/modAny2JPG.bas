Attribute VB_Name = "modAny2JPG"
Option Explicit
Option Compare Text

Const MODULE_NAME As String = "modAny2Tif:"

Public Function Excel2JPG(strFileName As String, bDeleteOriginal As Boolean) As String()
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "Excel2JPG"
          Dim strRet() As String
          Dim strPDFFile As String
          Dim blnConverted As Boolean
          
20        strPDFFile = convertedFileExists(strFileName, "pdf")
30        If strPDFFile = "" Then
40            strPDFFile = Excel2PDF(strFileName, bDeleteOriginal)
50            blnConverted = True
60        End If
          
70        If strPDFFile <> "" Then
80            DoEvents
90            If blnConverted Then Sleep 500
              
100           strRet = PDF2JPG(strFileName, "", bDeleteOriginal)
              
110       End If
errHandler:
120       If Err.Number <> 0 Then
130           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
140           Trace g_strConvertErrMsg
150           ReDim strRet(0)
160       End If
170       Excel2JPG = strRet
End Function

Public Function HTML2JPG(strFileName As String, strHTMLOptions As String, bDeleteOriginal As Boolean) As String()
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "HTML2JPG"
          Dim strRet() As String
          Dim strPDFFile As String
          Dim blnConverted As Boolean
          
20        strPDFFile = convertedFileExists(strFileName, "pdf")
30        If strPDFFile = "" Then
40            strPDFFile = HTML2PDF(strFileName, strHTMLOptions, bDeleteOriginal)
50            blnConverted = True
60        End If
          
70        If strPDFFile <> "" Then
80            DoEvents
90            If blnConverted Then Sleep 500
              
100           strRet = PDF2JPG(strFileName, "", bDeleteOriginal)
              
110       End If
errHandler:
120       If Err.Number <> 0 Then
130           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
140           Trace g_strConvertErrMsg
150           ReDim strRet(0)
160       End If
170       HTML2JPG = strRet
End Function

Public Function Image2JPG(strFileName As String, bDeleteOriginal As Boolean) As String()
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "Image2JPG"
          Dim strRet() As String
          Dim strPDFFile As String
          Dim blnConverted As Boolean
          Dim strBatchFile As String
          Dim strOutputFileFormat As String
          Dim strOutputFileName As String
          Dim oBatFile As TextStream
          Dim bRet As Boolean
          Dim iPageCount As Integer
          Dim strBurstFile As String
          Dim strCmd As String
          
20        Trace "      Bursting PDF document " & strFileName & " into JPG ... "
30        strBatchFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), "tojpg.bat")
40        strOutputFileFormat = gobjFSO.GetBaseName(gobjFSO.GetTempName())
50        strOutputFileName = getSafeFileName(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), strOutputFileFormat & ".jpg"))
          
60        strCmd = """" & g_strIMPath & """ """ & strFileName & " -trim -resample ""150x150"" """ & strOutputFileName & """"
          
70        If g_blnDirectExecute Then
80            bRet = SuperShell(strCmd, g_strSortTempPath, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
90        Else
100           Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
110           oBatFile.WriteLine strCmd
120           oBatFile.Close
130           Set oBatFile = Nothing
              
140           bRet = SuperShell(strBatchFile, gobjFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
150           Sleep 1000
160       End If
          
170       DoEvents
          
180       strOutputFileFormat = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), strOutputFileFormat & "*")
                    
190       iPageCount = 0
200       strBurstFile = Dir(strOutputFileFormat)
                    
210       If strBurstFile <> "" Then
              'splitting success.
220           Trace "Done." & vbCrLf
230       End If
                    
240       While strBurstFile <> ""
250           ReDim Preserve strRet(iPageCount)
260           strRet(iPageCount) = strBurstFile
270           strBurstFile = Dir()
280           iPageCount = iPageCount + 1
290       Wend
                    
300       If iPageCount > 0 Then
310           Trace "      Document was split into " & CStr(iPageCount) & " pages." & vbCrLf
320       End If
errHandler:
330       If Err.Number <> 0 Then
340           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
350           Trace g_strConvertErrMsg
360           ReDim strRet(0)
370       End If
380       Image2JPG = strRet
End Function

Public Function PDF2JPG(strFileName As String, strFDFPath As String, bDeleteOriginal As Boolean) As String()
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "Excel2JPG"
          Dim strRet() As String
          Dim oBatFile As TextStream
          Dim strOutputFileName As String
          Dim strOutputFileFormat As String
          Dim bRet As Boolean
          Dim iPageCount As Integer
          Dim strPDFFilledFile As String
          Dim strBatchFile As String
          Dim strBurstFile As String
          Dim strCmd As String
          
20        If strFileName <> "" Then
          
30            If strFDFPath <> "" Then
40                strPDFFilledFile = PDFFormFill(strFileName, strFDFPath, False)
                                    
50                If strPDFFilledFile <> "" Then
60                    strFileName = strPDFFilledFile
70                Else
80                    Trace "Unable to fill the pdf form." & vbCrLf
90                    GoTo errHandler
100               End If
110           End If
              
120           Trace "      Bursting PDF document " & strFileName & " into JPG ... "
130           strBatchFile = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), "tojpg.bat")
140           strOutputFileFormat = gobjFSO.GetBaseName(gobjFSO.GetTempName())
150           strOutputFileName = getSafeFileName(gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), strOutputFileFormat & ".jpg"))
              
160           strCmd = """" & g_strIMPath & """ """ & strFileName & " -trim -resample ""150x150"" """ & strOutputFileName & """"
              
170           If g_blnDirectExecute Then
180               bRet = SuperShell(strCmd, g_strSortTempPath, SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS, True)
190           Else
200               Set oBatFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
210               oBatFile.WriteLine strCmd
220               oBatFile.Close
230               Set oBatFile = Nothing
                  
240               bRet = SuperShell(strBatchFile, gobjFSO.GetParentFolderName(strBatchFile), SHELL_TIMEOUT, SW_HIDE, NORMAL_PRIORITY_CLASS)
                  
250               Sleep 500
260           End If
270           DoEvents
              
280           strOutputFileFormat = gobjFSO.BuildPath(gobjFSO.GetParentFolderName(strFileName), strOutputFileFormat & "*")
                        
290           iPageCount = 0
300           strBurstFile = Dir(strOutputFileFormat)
                        
310           If strBurstFile <> "" Then
                  'splitting success.
320               Trace "Done." & vbCrLf
330           End If
                        
340           While strBurstFile <> ""
350               ReDim Preserve strRet(iPageCount)
360               strRet(iPageCount) = strBurstFile
370               strBurstFile = Dir()
380               iPageCount = iPageCount + 1
390           Wend
                        
400           If iPageCount > 0 Then
410               Trace "      Document was split into " & CStr(iPageCount) & " pages." & vbCrLf
420           End If
              
430       End If

errHandler:
440       If Err.Number <> 0 Then
450           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
460           Trace g_strConvertErrMsg
470           ReDim strRet(0)
480       End If
490       PDF2JPG = strRet
End Function

Public Function RPT2JPG(strFileName As String, bDeleteOriginal As Boolean) As String()
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "RPT2JPG"
          Dim strRet() As String
          'Not yet implemented
errHandler:
20        If Err.Number <> 0 Then
30            g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
40            Trace g_strConvertErrMsg
50            ReDim strRet(0)
60        End If
70        RPT2JPG = strRet
End Function

Public Function Tif2JPG(strFileName As String, bDeleteOriginal As Boolean) As String()
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "Tif2JPG"
          Dim strRet() As String
          Dim strPDFFile As String
          Dim blnConverted As Boolean
          
20        strRet = Image2JPG(strFileName, bDeleteOriginal)
errHandler:
30        If Err.Number <> 0 Then
40            g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
50            Trace g_strConvertErrMsg
60            ReDim strRet(0)
70        End If
80        Tif2JPG = strRet
End Function

Public Function Word2JPG(strFileName As String, bDeleteOriginal As Boolean) As String()
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "Word2JPG"
          Dim strRet() As String
          Dim strPDFFile As String
          Dim blnConverted As Boolean
          
20        strPDFFile = convertedFileExists(strFileName, "pdf")
30        If strPDFFile = "" Then
40            strPDFFile = Word2PDF(strFileName, bDeleteOriginal)
50            blnConverted = True
60        End If
          
70        If strPDFFile <> "" Then
80            DoEvents
90            If blnConverted Then Sleep 500
              
100           strRet = PDF2JPG(strFileName, "", bDeleteOriginal)
              
110       End If
errHandler:
120       If Err.Number <> 0 Then
130           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
140           Trace g_strConvertErrMsg
150           ReDim strRet(0)
160       End If
170       Word2JPG = strRet
End Function

Public Function XML2JPG(strFileName As String, strXSLFileName As String, bDeleteOriginal As Boolean) As String()
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & "XML2JPG"
          Dim strRet() As String
          Dim strPDFFile As String
          Dim blnConverted As Boolean
          
20        strPDFFile = convertedFileExists(strFileName, "pdf")
30        If strPDFFile = "" Then
40            strPDFFile = XML2PDF(strFileName, strXSLFileName, bDeleteOriginal)
50            blnConverted = True
60        End If
          
70        If strPDFFile <> "" Then
80            DoEvents
90            If blnConverted Then Sleep 500
              
100           strRet = PDF2JPG(strFileName, "", bDeleteOriginal)
              
110       End If
errHandler:
120       If Err.Number <> 0 Then
130           g_strConvertErrMsg = "Error in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
140           Trace g_strConvertErrMsg
150           ReDim strRet(0)
160       End If
170       XML2JPG = strRet
End Function
