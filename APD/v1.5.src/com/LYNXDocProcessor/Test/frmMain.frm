VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   495
      Left            =   1740
      TabIndex        =   0
      Top             =   1380
      Width           =   1215
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
    Dim objJob As clsJob
    Dim objPkg As clsPackage
    Dim bResult As Boolean
    Dim strText As String
    Dim strTemplateXML As String
    
    strText = "Test comment"
    
    strTemplateXML = "<DocumentTransfer Partner=""CCAV"">" & _
                      "<Header>" & _
                        "<InsuranceCarrierID>$InsuranceCompanyID$</InsuranceCarrierID>" & _
                        "<ClaimNumber>$ClientClaimNumber$</ClaimNumber>" & _
                        "<LynxID>$LynxID$</LynxID>" & _
                      "</Header>" & _
                      "<NumberOfDocuments>$DocumentCount$</NumberOfDocuments>" & _
                      "<Document DocumentID=""$DocumentID$"">" & _
                        "<VehicleNumber>$VehicleNumber$</VehicleNumber>" & _
                        "<VehicleYear>$VehicleYear$</VehicleYear>" & _
                        "<VehicleMake>$VehicleMake$</VehicleMake>" & _
                        "<VehicleModel>$VehicleModel$</VehicleModel>" & _
                        "<CreateDateTime>" & _
                          "<DateTime Date=""$CreateDate$"" Time=""$CreateTime$"" TimeZone=""$TimeZone$""/>" & _
                        "</CreateDateTime>" & _
                        "<SequenceNumber>$SequenceNumber$</SequenceNumber>" & _
                        "<DocumentType>$DocumentType$</DocumentType>" & _
                        "<FilePath>$FilePath$</FilePath>" & _
                      "</Document>" & _
                    "</DocumentTransfer>"
    
'    Dim objStream1 As ADODB.Stream
'    Dim objStream2 As ADODB.Stream
    
    Set objJob = New clsJob  'CreateObject("LYNXDocSpooler.clsJob")
        
    objJob.newJob "Test Job"
    
    Set objPkg = objJob.Package
    
    objPkg.PackageType = PackageTypes.ePackagePDF  'PackageTypes.ePackagePDF
    objPkg.OutputFileName = "Closing Document.pdf"
    objPkg.addEmail "lynxserv@ppg.com", "rvishegu@lynxservices.com", "", "", "", "", "test email", "Closing documents"
    'objPkg.addFax "239-479-6193", "test"
    'objPkg.addFileCopy "c:\temp\IMPS\temp"
    objPkg.addHTTPPost "http://localhost/test.asp", False, strTemplateXML
    
    strTemplateXML = "<Document DocumentID="""">" & _
                     "<CreateDateTime>" & _
                     "<DateTime Date="""" Time="""" TimeZone=""""/>" & _
                     "</CreateDateTime>" & _
                     "<SequenceNumber>{EstSuppSequenceNumber if DocumentType is DigitalImage or EstimatePI otherwise blank}</SequenceNumber>" & _
                     "<DocumentType>{DigitalImage|EstimatePI|Document:{DocumentType}(ie. Document:Summary Sheet)}</DocumentType>" & _
                     "<FilePath>{fully qualified UNC Path to where processed(converted file) resides. (file will be deleted by Partner after sending)}</FilePath>" & _
                     "</Document>"
    
    objJob.addDocumentLinked "C:\temp\IMPS\CEI Wildcat Docs\705131.doc", , False, False, , "12343.pdf", strTemplateXML
    
    'objJob.addDocumentLinked "C:\temp\IMPS\CEI Wildcat Docs\E20050314102802LYNXID374715V002Doc001.xml", _
                           "C:\Websites\apd\v1.4.src\com\LYNXDocProcessor\badjob\PrintImage.xsl"
    
'    Set objStream1 = New ADODB.Stream
'    objStream1.Type = adTypeBinary
'    objStream1.Open
'    objStream1.LoadFromFile "C:\temp\IMPS\CEI Wildcat Docs\E20050314102802LYNXID374715V002Doc001.xml"
'
'    Set objStream2 = New ADODB.Stream
'    objStream2.Type = adTypeBinary
'    objStream2.Open
'    objStream2.LoadFromFile "C:\Websites\apd\v1.4.src\com\LYNXDocProcessor\badjob\PrintImage.xsl"
'
'    objJob.addDocumentEmbed objStream1, "E20050314102802LYNXID374715V002Doc001.xml", objStream2

    objJob.addFreeFormText strText, False, False, "", "file.pdf"
    
    objPkg.addNotifySuccess "", "rvishegu@lynxservices.com", "Closing Documents sent successfully", "Closing Documents sent successfully"
    objPkg.addNotifyError "", "rvishegu@lynxservices.com", "Error: Sending Closing Documents", "Error: Sending Closing Documents"
    
    bResult = objJob.submitJob
    
'    Set objStream1 = Nothing
'    Set objStream2 = Nothing
    Set objPkg = Nothing
    
    Set objJob = Nothing

    If bResult Then
        MsgBox "Job submitted successfully", vbInformation
    Else
        MsgBox "Error submitting job", vbError
    End If
End Sub
