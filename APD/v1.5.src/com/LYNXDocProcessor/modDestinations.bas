Attribute VB_Name = "modDestinations"
Option Explicit
Option Compare Text

Const MODULE_NAME As String = "modDestinations:"

Public Function processEmail(ByRef oEmailDestination As IXMLDOMElement, aSingleAttachments() As String, aMergedDocuments() As String) As Boolean
          Const PROC_NAME As String = MODULE_NAME & "processEmail()"
10        On Error GoTo errHandler
          Dim strFrom As String
          Dim strTo As String
          Dim strCC As String
          Dim strBCC As String
          Dim strReplyTo As String
          Dim strImportance As String
          Dim strSubject As String
          Dim strBody As String
          Dim lngEmailSizeLimit As Long
          Dim strEmailSizeLimit As String
          Dim strMsg As String
          Dim aAttachments() As String
          Dim iAttachmentsCount As Integer
          Dim iAttachmentsIdx As Integer
          Dim i As Integer
          Dim j As Integer
          Dim lngEmailSize As Long
          Dim lngFileSize As Long
          Dim aSplitFiles() As String
          Dim aAllAttachments() As String
          Dim lStartTick As Long
          
20        If Not oEmailDestination Is Nothing Then
30            lStartTick = GetTickCount()
40            Trace "      Processing email destination... " & vbCrLf
50            strFrom = getXMLValue(oEmailDestination, "From")
60            strTo = getXMLValue(oEmailDestination, "To")
70            strCC = getXMLValue(oEmailDestination, "CC")
80            strBCC = getXMLValue(oEmailDestination, "BCC")
90            strReplyTo = getXMLValue(oEmailDestination, "ReplyTo")
100           strImportance = getXMLValue(oEmailDestination, "Importance")
110           strSubject = getXMLValue(oEmailDestination, "Subject")
120           strBody = getXMLValue(oEmailDestination, "Body")
130           strEmailSizeLimit = getXMLValue(oEmailDestination, "SMTPEmailSizeLimit")
              
140           g_blnOverrides = False
              
              'If no from value specified then use the default
150           If strFrom = "" Then strFrom = g_strDefaultEmailFrom
              
              'check if we have override
160           If g_strEmailToOverride <> "" Then
170               Trace "      OVERRIDING DESTINATION FROM THE CONFIGURATION." & vbCrLf
180               strTo = g_strEmailToOverride
190               strCC = ""
200               strBCC = ""
210               strReplyTo = ""
220               g_blnOverrides = True
230           End If
              
              'if no value specfied for the email limit then use default
240           If strEmailSizeLimit <> "" Then
250               If IsNumeric(strEmailSizeLimit) Then
260                   lngEmailSizeLimit = CLng(getXMLValue(oEmailDestination, "SMTPEmailSizeLimit"))
270               Else
280                   lngEmailSizeLimit = CLng(g_lngSMTPEmailSize)
290               End If
300           Else
310               lngEmailSizeLimit = CLng(g_lngSMTPEmailSize)
320           End If
              
330           If strTo = "" Then
340               strMsg = "No Email To address specified."
350               Trace "      No 'To' address specified. NO EMAIL SENT."
360               GoTo noEmailSend
370           End If
              
380           If strSubject = "" And strBody = "" And Join(aSingleAttachments, "") = "" And Join(aMergedDocuments, "") = "" Then
                  'nothing to send. skip it
390               strMsg = "No value found in the email subject and body and no attachments. No need to send email."
400               Trace "      Email does not contain any data. NO EMAIL SENT."
410               GoTo noEmailSend
420           End If
              
430           Select Case strImportance
                  Case "high", "low", "normal": 'no need to change
440               Case Else: strImportance = "normal"
450           End Select
              
              'build the attachments array
460           iAttachmentsCount = UBound(aSingleAttachments) + UBound(aMergedDocuments)
              'ReDim aAttachments(iAttachmentsCount)
470           iAttachmentsIdx = 0
              
480           lngEmailSize = 0
490           lngFileSize = 0
              
              
      '        'Add the single attachments
      '        If Trim(Join(aSingleAttachments, "")) <> "" Then
      '            For i = LBound(aSingleAttachments) To UBound(aSingleAttachments)
      '                If (aSingleAttachments(i) <> "") Then
      '                    If gobjFSO.FileExists(aSingleAttachments(i)) Then
      '                        aAttachments(iAttachmentsIdx) = aSingleAttachments(i)
      '                        iAttachmentsIdx = iAttachmentsIdx + 1
      '                    End If
      '                End If
      '            Next
      '        End If
      '
      '        'Add the merged attachments
      '        If Trim(Join(aMergedDocuments, "")) <> "" Then
      '            For i = LBound(aMergedDocuments) To UBound(aMergedDocuments)
      '                If (aMergedDocuments(i) <> "") Then
      '                    If gobjFSO.FileExists(aMergedDocuments(i)) Then
      '                        lngFileSize = FileLen(aMergedDocuments(i))
      '
      '                        If (lngFileSize >= lngEmailSizeLimit) Then
      '                            'single attachment exceeds the email size limit. We need to split them
      '                            aSplitFiles = splitFile(aMergedDocuments(i), lngEmailSizeLimit)
      '
      '                            If IsArray(aSplitFiles) Then
      '                                aAttachments = Split(Join(aAttachments, "|") + "|" + Join(aSplitFiles, "|"), "|")
      '                                If UBound(aSplitFiles) > 0 Then
      '                                    iAttachmentsCount = UBound(aAttachments) + UBound(aSplitFiles)
      '                                    ReDim Preserve aAttachments(iAttachmentsCount)
      '                                    j = 0
      '                                    For j = LBound(aSplitFiles) To UBound(aSplitFiles)
      '                                        aAttachments(i + j) = aSplitFiles(j)
      '                                        iAttachmentsIdx = iAttachmentsIdx + 1
      '                                    Next
      '                                End If
      '                            End If
      '                        Else
      '                            aAttachments(iAttachmentsIdx) = aMergedDocuments(i)
      '                            iAttachmentsIdx = iAttachmentsIdx + 1
      '                        End If
      '                    End If
      '                End If
      '            Next
      '        End If
              
              'join both the single attachment and merge attachment arrays
500           aAllAttachments = Split(Join(aSingleAttachments, "|") + "|" + Join(aMergedDocuments, "|"), "|")
              
              'split single attachments to honor the SMTP size limit if needed
510           If Trim(Join(aAllAttachments, "")) <> "" Then
520               For i = LBound(aAllAttachments) To UBound(aAllAttachments)
530                   If (aAllAttachments(i) <> "") Then
540                       If gobjFSO.FileExists(aAllAttachments(i)) Then
550                           lngFileSize = FileLen(aAllAttachments(i))

560                           If (lngFileSize >= lngEmailSizeLimit) Then
                                  'single attachment exceeds the email size limit. We need to split them
570                               aSplitFiles = splitFile(aAllAttachments(i), lngEmailSizeLimit)

580                               If IsArray(aSplitFiles) Then
590                                   aAttachments = Split(Join(aAttachments, "|") + "|" + Join(aSplitFiles, "|"), "|")
                                      'iAttachmentsIdx = iAttachmentsIdx + UBound(aAttachments) + 1
600                               End If
610                           Else
620                               aAttachments = Split(Join(aAttachments, "|") + "|" + aAllAttachments(i), "|")
                                  'aAttachments(iAttachmentsIdx) = aMergedDocuments(i)
                                  'iAttachmentsIdx = iAttachmentsIdx + 1
630                           End If
640                       End If
650                   End If
660               Next
670           End If
              
680           If g_blnOverrides Then
690               Trace "        Email overridden. Values in CC and BCC will be erased." & vbCrLf
700               strCC = ""
710               strBCC = ""
720           End If
              
730           If sendEmail(strFrom, strTo, strCC, strBCC, _
                                       strReplyTo, strSubject, strImportance, _
                                       strBody, aAttachments, lngEmailSizeLimit) Then
740               processEmail = True
750               g_strDocumentsSent = g_strDocumentsSent & vbCrLf & "Sent via email to " & strTo & " [CC:" & strCC & " BCC:" & strBCC & "]: " & vbCrLf & getDocumentBaseNames(aAttachments)
760           Else
770               processEmail = False
780           End If
              
noEmailSend:
790           If strMsg <> "" Then 'non-critical messages. already traced
                  'trace and exit function
800               processEmail = True
810           End If
              
820           Trace "      Process Email in " & (GetTickCount() - lStartTick) & " ms" & vbCrLf
              
830       End If
          
errHandler:
840       If Err.Number <> 0 Then
850           g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & PROC_NAME & " [Line #" & Erl & "] " & strMsg & vbCrLf
860           processEmail = False
870       End If
880       DoEvents
End Function

Public Function sendEmail(sEmailFrom As String, sEmailTo As String, sEmailCC As String, sEmailBCC As String, sEmailReplyTo As String, sEmailSubject As String, _
                                  sEmailImportance As String, sEmailBody As String, aAttachments() As String, Optional lMaxSize As Long = 0) As Boolean
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & ":sendEmail()"
          Dim iMsg As Message
          Dim iConf As Configuration
          Dim Flds As Object
          Dim i As Integer
          Dim oRecipient As IXMLDOMNode
          Dim stm As ADODB.Stream
    
          Dim aAttachmentsPerEmail() As String
          Dim sEmailAttachment As String
          Dim lEmailSize As Long
          Dim lFileSize As Long
          Dim iIndex As Integer
          Dim bAttachmentSent As Boolean
          Dim strException As String
          Dim bEmailSent As Boolean
          Dim strAttachment As String
          Dim lStartTick As Long
          Dim iRetryCount As Integer
          Dim blnHTMLContent As Boolean
    
20        lStartTick = GetTickCount()
    
30        iRetryCount = 3

40        ReDim aAttachmentsPerEmail(1)
          'group the attachments so as to be within the SMTP size limits
50        If lMaxSize > 0 Then
60            If IsArray(aAttachments) Then
70                If UBound(aAttachments) > 0 Then
80                    ReDim aAttachmentsPerEmail(UBound(aAttachments))
90                    lEmailSize = 0
100                   iIndex = 0
110                   For i = LBound(aAttachments) To UBound(aAttachments)
120                       If aAttachments(i) <> "" Then
130                           If gobjFSO.FileExists(aAttachments(i)) Then
140                               lFileSize = FileLen(aAttachments(i))
150                               If lFileSize < lMaxSize Then 'current file is less than the max email limit
160                                   If (lEmailSize + lFileSize) < lMaxSize Then
170                                       lEmailSize = lEmailSize + lFileSize
180                                       sEmailAttachment = sEmailAttachment & aAttachments(i) + "|"
190                                       aAttachmentsPerEmail(iIndex) = sEmailAttachment
200                                   Else
210                                       lEmailSize = lFileSize
220                                       iIndex = iIndex + 1
230                                       sEmailAttachment = aAttachments(i) + "|"
240                                       aAttachmentsPerEmail(iIndex) = sEmailAttachment
250                                   End If
260                               Else 'current file exceeds the email limit
270                                   strException = strException & "Single attachment exceeds maximum per-email size limit." & vbCrLf & _
                                                                    "  Email subject: " & sEmailSubject & vbCrLf & _
                                                                    "  Attachment: " + aAttachments(i) & vbCrLf & _
                                                                    "  File Size: " & CStr(lFileSize) & "; max limit: " & CStr(lMaxSize) & vbCrLf
280                                   aAttachmentsPerEmail(iIndex) = ""
290                                   GoTo cleanUp
300                               End If
310                           End If
320                       End If
330                   Next
340               End If
350           End If
360       End If
    
370       If InStr(1, sEmailBody, "<HTML>", vbTextCompare) >= 0 And _
             InStr(1, sEmailBody, "</HTML>", vbTextCompare) > 0 Then
380           blnHTMLContent = True
390       End If
    
400       Trace "      Email settings: " & vbCrLf & _
                "        SMTP Server: " & g_strSMTPServer & vbCrLf & _
                "        SMTP Size limit: " & lMaxSize & vbCrLf & _
                "        From       : " & sEmailFrom & vbCrLf & _
                "        To         : " & sEmailTo & vbCrLf & _
                "        CC         : " & sEmailCC & vbCrLf & _
                "        BCC        : " & sEmailBCC & vbCrLf & _
                "        Reply To   : " & sEmailReplyTo & vbCrLf & _
                "        Priority   : " & sEmailImportance & vbCrLf & _
                "        Subject    : " & sEmailSubject & vbCrLf & _
                "        HTML Body  : " & blnHTMLContent & vbCrLf & _
                "        Body       : " & Replace(sEmailBody, vbCrLf, "                     " & vbCrLf) & vbCrLf & _
                "        Attachments: <added below if any>" & vbCrLf
          

          'replace the | char with CRLF
410       sEmailBody = Replace(sEmailBody, "|", vbCrLf)
    
doRetry:
420       Set iConf = CreateObject("CDO.Configuration")
430       Set Flds = iConf.Fields
    


          'set the configuration field values
440       With Flds
450         .Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = cdoSendUsingPort  '2 ' cdoSendUsingPort
460         .Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = g_strSMTPServer 'sftmfnolprdweb1
470         .Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = g_intSMTPTimeout ' timeout in seconds
480         .Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = cdoNTLM
      
490         If sEmailImportance <> "" Then
500           Select Case sEmailImportance
                  Case "high"
510                   .Item("urn:schemas:httpmail:importance") = cdoImportanceValues.cdoHigh
520                   .Item("urn:schemas:mailheader:X-MSMail-Priority") = "High"
530                   .Item("urn:schemas:mailheader:X-Priority") = 2
540               Case "low"
550                   .Item("urn:schemas:httpmail:importance") = cdoImportanceValues.cdoLow
560                   .Item("urn:schemas:mailheader:X-MSMail-Priority") = "Low"
570                   .Item("urn:schemas:mailheader:X-Priority") = 0
580           End Select
590         End If
600         .Update
610       End With

          'compose the message
620       Set iMsg = CreateObject("CDO.Message")
630       With iMsg
640           Set .Configuration = iConf
650           .From = sEmailFrom
660           .To = sEmailTo
670           .CC = sEmailCC
680           .BCC = sEmailBCC
690           .Subject = sEmailSubject
700           .ReplyTo = sEmailReplyTo

              '.TextBody = sEmailBody

710           bAttachmentSent = False

720           For iIndex = LBound(aAttachmentsPerEmail) To UBound(aAttachmentsPerEmail)
730               lEmailSize = 0
740               strAttachment = vbCrLf
750               If aAttachmentsPerEmail(iIndex) <> "" Then
760                   aAttachments = Split(aAttachmentsPerEmail(iIndex), "|")
770                   For i = LBound(aAttachments) To UBound(aAttachments)
780                       If aAttachments(i) <> "" Then
790                           If gobjFSO.FileExists(aAttachments(i)) Then
800                               lFileSize = FileLen(aAttachments(i))
810                               .AddAttachment (aAttachments(i))
820                               strAttachment = strAttachment & "  * " & gobjFSO.GetBaseName(aAttachments(i)) & "." & gobjFSO.GetExtensionName(aAttachments(i)) & vbCrLf
830                               Trace "        Adding attachment [file size: " & lFileSize & " bytes]: " & aAttachments(i) & vbCrLf
840                               lEmailSize = lEmailSize + lFileSize
850                           Else
860                               aAttachments(i) = ""
870                               GoTo cleanUp
880                           End If
890                       End If
900                   Next
                
910                   If blnHTMLContent = True Then
920                       .HTMLBody = Replace(sEmailBody, "$AttachmentList$", strAttachment)
930                   Else
940                       .TextBody = Replace(sEmailBody, "$AttachmentList$", strAttachment)
950                   End If
                
960                   Trace "     Sending email [approx. total weight: " & lEmailSize & " bytes]..."

970                   .send
                
980                   Trace "Sent. [" & (GetTickCount() - lStartTick) & " ms]" & vbCrLf
990                   lStartTick = GetTickCount()
                
1000                  bEmailSent = True

                      'Trace "Email sent to " & oRecipient.Text & vbCrLf & _
                            "  Subject: " & sEmailSubjectResolved & vbCrLf & _
                            "  Attachments: " & vbCrLf & "    " & _
                            Join(aAttachments, vbCrLf & "    ")

1010                  bAttachmentSent = True
1020                  .Attachments.DeleteAll
1030              End If
1040          Next

1050          If bAttachmentSent = False Then
                  'email with no attachments
1060              If blnHTMLContent = True Then
1070                  .HTMLBody = Replace(sEmailBody, "$AttachmentList$", strAttachment)
1080              Else
1090                  .TextBody = Replace(sEmailBody, "$AttachmentList$", strAttachment)
1100              End If
            
1110              Trace "     Sending email..."
1120              .send
1130              Trace "Sent. [" & (GetTickCount() - lStartTick) & " ms]" & vbCrLf
1140              bEmailSent = True

1150          End If

1160      End With
1170      Set iMsg = Nothing
cleanUp:
1180      Set iConf = Nothing

1190      If strException <> "" Then
1200          ReDim aAttachments(0)
              'sendEmail m_sErrorEmailFrom, m_oErrorEmailTo, "CEI Wildcat - Documents sent with Exceptions", "", "", _
                            "Documents were send to CEI with the following exceptions:" & vbCrLf & strException, aAttachments
1210          bEmailSent = False
1220          Trace "       NO EMAIL SENT. Exception occured. " & strException & vbCrLf
1230          g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & PROC_NAME & " [Line # " & Erl & "] " & strException
1240      End If
errHandler:
1250      Set iMsg = Nothing
1260      Set iConf = Nothing
1270      Set Flds = Nothing
1280      Set stm = Nothing
    
    
1290      If Err.Number <> 0 Then
              'handleError "An error occured in " & PROC_NAME & " [Line#" & CStr(Erl) & " " & Err.Description
              'Trace "Error sending email at Line #" & CStr(Erl) & ". Error description: " & Err.Description
1300          If Err.Description = "The transport lost its connection to the server" Then
1310              If iRetryCount > 0 Then
1320                  Trace vbCrLf & "       " & Err.Description & "  Retrying ..." & vbCrLf
1330                  iRetryCount = iRetryCount - 1
1340                  Sleep 150
1350                  GoTo doRetry
1360              End If
1370          End If
        
1380          sendEmail = False
1390          Trace "       NO EMAIL SENT. Error occured at Line #" & Erl & " Description: " & Err.Description & vbCrLf
1400          g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & PROC_NAME & " [Line # " & Erl & "] " & Err.Description & vbCrLf
1410          bEmailSent = False
1420      End If
1430      sendEmail = bEmailSent
End Function

Public Function processFax(ByRef oFaxDestination As IXMLDOMElement, aFaxDocuments() As String) As Boolean
10        On Error GoTo errHandler
          Const PROC_NAME As String = MODULE_NAME & ":processFax()"
          Dim strFaxNumber As String
          Dim strFaxToName As String
          Dim lStartTick As Long
          
20        lStartTick = GetTickCount()
          
30        Trace "     Processing fax destination..." & vbCrLf
40        strFaxToName = IIf(IsNull(oFaxDestination.getAttribute("name")), "", oFaxDestination.getAttribute("name"))
50        strFaxNumber = IIf(IsNull(oFaxDestination.getAttribute("to")), "", oFaxDestination.getAttribute("to"))
          
60        If strFaxNumber <> "" Then
              
              'check if we have override
70            If g_strFaxNumberOverride <> "" Then
80                Trace "      OVERRIDING DESTINATION FROM THE CONFIGURATION." & vbCrLf
90                strFaxNumber = g_strFaxNumberOverride
100           End If
              
110           Trace "       Fax Number: " & strFaxNumber & vbCrLf & _
                    "       Name      : " & strFaxToName & vbCrLf
              
120           If SendFaxDocument(strFaxToName, aFaxDocuments(0), strFaxNumber) Then
130               Trace "      Fax sent successfully. [" & (GetTickCount() - lStartTick) & " ms]" & vbCrLf
140               processFax = True
150               g_strDocumentsSent = g_strDocumentsSent & vbCrLf & "Sent via Fax [" & strFaxNumber & "]: " & vbCrLf & getDocumentBaseNames(aFaxDocuments)
160           Else
170               Trace "      FAX FAILED. [" & (GetTickCount() - lStartTick) & " ms]" & vbCrLf
180               processFax = False
190           End If
              
200       Else
              'raise error. no fax number specified
210           processFax = False
220           Trace "      ERROR: Fax NOT sent. No Fax number provided." & vbCrLf
230           g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & PROC_NAME & " [Line # " & Erl & "] Missing Fax Number"
240       End If
errHandler:
250       If Err.Number <> 0 Then
260           processFax = False
270           Trace "      FAX FAILED due to error at Line #" & Erl & ". Reason: " & Err.Description & vbCrLf
280           g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & PROC_NAME & " [Line # " & Erl & "] " & Err.Description & vbCrLf
290       End If
End Function

Public Function processFileCopy(ByRef oFileCopyDestination As IXMLDOMElement, aSingleAttachments() As String, aMergedDocuments() As String) As Boolean
10        On Error GoTo errHandler
          Const PROC_NAME As String = ":processFileCopy()"
          Dim bOverwrite As Boolean
          Dim strUNCDestination As String
          Dim oUNCNode As IXMLDOMElement
          Dim strDestinationFile As String
          Dim strSourceBaseName As String
          Dim i As Integer
          Dim oFile As File
          Dim lStartTick As Long
          
20        lStartTick = GetTickCount()
30        Trace "      Processing File Copy..." & vbCrLf
40        If Not oFileCopyDestination Is Nothing Then
50            bOverwrite = IIf(IsNull(oFileCopyDestination.getAttribute("overwrite")), False, oFileCopyDestination.getAttribute("overwrite"))
              
60            Set oUNCNode = oFileCopyDestination.SelectSingleNode("UNC")
              
70            If Not oUNCNode Is Nothing Then
80                strUNCDestination = oUNCNode.Text
90                If strUNCDestination <> "" Then
                      'check if we have override
100                   If g_strFileCopyOverride <> "" Then
110                       strUNCDestination = g_strFileCopyOverride
120                       Trace "      OVERRIDING DESTINATION FROM THE CONFIGURATION." & vbCrLf
130                   End If
                      
140                   Trace "        Settings:" & vbCrLf & _
                            "          Destination: " & strUNCDestination & vbCrLf & _
                            "          Overwrite  : " & bOverwrite & vbCrLf
                            
150                   If gobjFSO.FolderExists(strUNCDestination) Then
                      
160                       g_strDocumentsSent = g_strDocumentsSent & vbCrLf & "File copy: "
                          'copy the single documents
170                       For i = 0 To UBound(aSingleAttachments)
180                           If aSingleAttachments(i) <> "" Then
190                               If gobjFSO.FileExists(aSingleAttachments(i)) Then
                                      'now copy the file to the destination
200                                   strSourceBaseName = gobjFSO.GetBaseName(aSingleAttachments(i)) & "." & gobjFSO.GetExtensionName(aSingleAttachments(i))
210                                   strDestinationFile = gobjFSO.BuildPath(strUNCDestination, strSourceBaseName)
                                      
220                                   If gobjFSO.FileExists(strDestinationFile) Then
                                          'file exist. check if overwrite was set
230                                       If bOverwrite Then
240                                           Set oFile = gobjFSO.GetFile(strDestinationFile)
250                                           oFile.Attributes = Normal
260                                           Set oFile = Nothing

270                                           Trace "       Deleting file " & strDestinationFile & "..."
280                                           gobjFSO.DeleteFile strDestinationFile, True
290                                           Trace "Done." & vbCrLf
300                                       End If
310                                   End If
                                      
320                                   Trace "       Copying file " & aSingleAttachments(i) & " to " & strDestinationFile & " ..."
330                                   gobjFSO.CopyFile aSingleAttachments(i), strDestinationFile
340                                   Trace "Done. [" & (GetTickCount() - lStartTick) & " ms]" & vbCrLf
350                                   lStartTick = GetTickCount()
                                      
360                                   g_strDocumentsSent = g_strDocumentsSent & vbCrLf & aSingleAttachments(i)
370                               Else
380                                   processFileCopy = False
390                                   Exit Function
400                               End If
410                           End If
420                       Next
                          
                          'copy the merged documents
430                       For i = 0 To UBound(aMergedDocuments)
440                           If aMergedDocuments(i) <> "" Then
450                               If gobjFSO.FileExists(aMergedDocuments(i)) Then
                                      'now copy the file to the destination
460                                   strSourceBaseName = gobjFSO.GetBaseName(aMergedDocuments(i)) & "." & gobjFSO.GetExtensionName(aMergedDocuments(i))
470                                   strDestinationFile = gobjFSO.BuildPath(strUNCDestination, strSourceBaseName)
                                      
480                                   If gobjFSO.FileExists(strDestinationFile) Then
                                          'file exist. check if overwrite was set
490                                       If bOverwrite Then
500                                           Set oFile = gobjFSO.GetFile(strDestinationFile)
510                                           oFile.Attributes = Normal
520                                           Set oFile = Nothing
                                              
530                                           Trace "       Deleting file " & strDestinationFile & "..."
540                                           gobjFSO.DeleteFile strDestinationFile, True
550                                           Trace "Done. [" & (GetTickCount() - lStartTick) & " ms]" & vbCrLf
560                                           lStartTick = GetTickCount()
570                                       End If
580                                   End If
                                      
590                                   Trace "       Copying file " & aMergedDocuments(i) & " to " & strDestinationFile & " ..."
600                                   gobjFSO.CopyFile aMergedDocuments(i), strDestinationFile
610                                   Trace "Done. [" & (GetTickCount() - lStartTick) & " ms]" & vbCrLf
620                                   lStartTick = GetTickCount()
                                      
630                                   g_strDocumentsSent = g_strDocumentsSent & vbCrLf & aMergedDocuments(i)
640                               Else
650                                   processFileCopy = False
660                                   Exit Function
670                               End If
680                           End If
690                       Next
                          
700                       processFileCopy = True
710                   Else
720                       processFileCopy = False
730                       g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & PROC_NAME & " [Line # " & Erl & "] " & "Destination folder does not exist. " & strUNCDestination
740                       Trace "       ERROR: Destination folder does not exist. " & strUNCDestination & vbCrLf
750                   End If
760               Else
770                   processFileCopy = False
780                   g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & PROC_NAME & " [Line # " & Erl & "] Destination folder not specified."
790                   Trace "       ERROR: Destination folder not specified. " & vbCrLf
800               End If
810           Else
820               processFileCopy = False
830               g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & PROC_NAME & " [Line # " & Erl & "] Missing Destination definition."
840               Trace "       ERROR: Destination not defined." & vbCrLf
850           End If
860       End If
errHandler:
870       If Err.Number <> 0 Then
880           processFileCopy = False
890           g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & PROC_NAME & " [Line # " & Erl & "] " & Err.Description & vbCrLf
900           Trace "       ERROR in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
910       End If
End Function

Public Function processHTTPPost(ByRef oDestination As IXMLDOMElement, ByRef oDocumentNodes As IXMLDOMNodeList, ByRef aDocuments() As String, ByRef aMergeDocs() As String) As Boolean
10        On Error GoTo errHandler
          Const PROC_NAME As String = ":processHTTPPost()"
          Dim objXMLHTTPPost As MSXML2.XMLHTTP40
          Dim objPostXML As IXMLDOMElement
          Dim objNode As IXMLDOMElement
          Dim oDocument As IXMLDOMElement
          Dim oCurDocument As IXMLDOMElement
          Dim objXML As MSXML2.DOMDocument
          Dim oPostDocuments As IXMLDOMNodeList
          Dim oProcessedDoc As IXMLDOMElement
          Dim oTemplateFilePath As IXMLDOMElement
          'Dim objPostData As MSXML2.DOMDocument
          Dim objXMLRet As MSXML2.DOMDocument
          Dim objErrorNode As IXMLDOMElement
          Dim objPostDataStream As TextStream
          Dim objXML2Post As MSXML2.DOMDocument
          
          Dim strURL As String
          Dim blnEmbed As Boolean
          Dim strDocumentNodeName As String
          Dim strDocumentDetails As String
          Dim aDocumentDetail() As String
          Dim strFileLinkPath As String
          Dim strKey As String
          Dim strSourceFile As String
          Dim strDestinationFile As String
          Dim strDestinationFile2Caller As String
          Dim iFileCounter As Integer
          Dim strResponseText As String
          Dim iDupCount As Integer
          Dim strPostXML As String
          Dim strPostFileName As String
          Dim lStartTick As Long
          
20        lStartTick = GetTickCount()
          
30        Trace "      Processing HTTP Post..." & vbCrLf
          
40        If Not oDestination Is Nothing Then
50            Set objNode = oDestination.SelectSingleNode("url")
60            Set objPostXML = oDestination.SelectSingleNode("PostTemplate")
70            blnEmbed = (oDestination.getAttribute("FileEmbed") = "true")
              
80            strFileLinkPath = oDestination.getAttribute("FilePath")
              
90            strDocumentNodeName = oDestination.getAttribute("DocumentNodeName")
              
100           If strDocumentNodeName = "" Then strDocumentNodeName = "Document"
              
110           If strFileLinkPath = "" Then strFileLinkPath = g_strOutboxPath2External
              
120           If Not objNode Is Nothing Then
130               strURL = objNode.Text
140               Set objXML = New MSXML2.DOMDocument
                  
150               Trace "         POST URL: " & strURL & vbCrLf
                  
160               If Trim(strURL) <> "" Then
170                   If Not oDocumentNodes Is Nothing Then
180                       If objPostXML Is Nothing Then 'No Post XML template provided. Build a generic template
                              'Generic template:
                              ' <Documents>
                              '    <Document>
                              '       <FilePath><![CDATA[[]]></FilePath> <!-- UNC Path of the document. Can be used from another computer -->
                              '       <PassThru><![CDATA[[]]></PassThru> <!-- Any CDATA specified within the document. Used as a passthru -->
                              '    </Document>
                              ' </Documents

buildGenericPostXML:
190                           Trace "      Building a generic Document Post XML." & vbCrLf
200                           objXML.loadXML "<Documents/>"
                              
appendDocuments:
210                           Set objPostXML = objXML.SelectSingleNode("//Documents")
                              
                              'build a document node for each document processed
220                           For Each oProcessedDoc In oDocumentNodes
230                               Set oDocument = objXML.createElement(strDocumentNodeName)
240                               Set objNode = objXML.createElement("FilePath")
                                  
250                               strSourceFile = oProcessedDoc.getAttribute("processedFile")
260                               strDestinationFile = gobjFSO.BuildPath(strFileLinkPath, g_strJobID & "-" & iFileCounter & "." & gobjFSO.GetExtensionName(strSourceFile))
                                  
                                  'check if the destination file already exists. Normally, this would not happen because
                                  ' the job id is GUID and they are supposed to be unique. We use this as the destination
                                  ' file name.
270                               iDupCount = 0
280                               While gobjFSO.FileExists(strDestinationFile)
                                      'destination file exists. find a unique file name by appending the duplicate counter.
290                                   iDupCount = iDupCount + 1
300                                   strDestinationFile = gobjFSO.BuildPath(strFileLinkPath, g_strJobID & "-" & iDupCount & "." & gobjFSO.GetExtensionName(strSourceFile))
310                               Wend
                                  
320                               If gobjFSO.FileExists(strSourceFile) = False Then
330                                   Trace "      Source file " & strSourceFile & " not found." & vbCrLf
340                                   processHTTPPost = False
350                                   GoTo errHandler
360                               End If
                                  
                                  'copy the source to destination
370                               gobjFSO.CopyFile strSourceFile, strDestinationFile, True
                                  
380                               objNode.appendChild objXML.createCDATASection(strDestinationFile)
390                               oDocument.appendChild objNode
                                  
400                               Set objNode = objXML.createElement("PassThru")
410                               objNode.appendChild objXML.createCDATASection(oProcessedDoc.Text)
420                               oDocument.appendChild objNode
                                  
430                               objPostXML.appendChild oDocument
                                  
440                               DoEvents
450                           Next
                              
460                           strPostXML = objXML.XML
470                       ElseIf objPostXML.ChildNodes.Length = 0 Then
                              'PostXML node was found and it contained no XML template
480                           Trace "      Post XML contains no child node or contains a text." & vbCrLf
490                           GoTo buildGenericPostXML
500                       Else
                              ' We have a post template. Now update the template with the documents processed location
510                           Set objPostXML = objPostXML.FirstChild
520                           Set oPostDocuments = objPostXML.SelectNodes(".//" & strDocumentNodeName)
530                           iFileCounter = 0
                              
540                           On Error Resume Next
                              'clean up the outbox for this job
550                           If gobjFSO.BuildPath(strFileLinkPath, g_strJobID & "*.*") <> "" Then Kill gobjFSO.BuildPath(strFileLinkPath, g_strJobID & "*.*")
                              
560                           Err.Clear
570                           On Error GoTo errHandler
                              
580                           If oPostDocuments.Length > 0 Then
590                               For Each oDocument In oPostDocuments
600                                   iFileCounter = iFileCounter + 1
610                                   strKey = IIf(IsNull(oDocument.getAttribute("key")), "", oDocument.getAttribute("key"))
620                                   If strKey <> "" Then
630                                       If Left(strKey, 1) = "$" Then
                                              'key is a macro.
640                                           Select Case strKey
                                                  Case "$JOBDOCUMENT$":
650                                                   attachDocument gobjFSO.BuildPath(g_strSortTempPath, g_strJobID & ".pdf"), "", True, iFileCounter, oDocument
660                                           End Select
670                                       Else
680                                           strKey = strKey & "=" & oDocument.getAttribute(strKey)
690                                           For Each oProcessedDoc In oDocumentNodes
700                                               If InStr(1, oProcessedDoc.Text, strKey, vbTextCompare) > 0 Then
710                                                   If blnEmbed Then
720                                                       strDestinationFile2Caller = attachDocument(oProcessedDoc.getAttribute("processedFile"), strFileLinkPath, blnEmbed, iFileCounter, oDocument)
730                                                   Else
740                                                       strSourceFile = oProcessedDoc.getAttribute("processedFile")
750                                                       strDestinationFile = gobjFSO.BuildPath(strFileLinkPath, g_strJobID & "-" & iFileCounter & "." & gobjFSO.GetExtensionName(strSourceFile))
                                                          
                                                          'check if the destination file already exists. Normally, this would not happen because
                                                          ' the job id is GUID and they are supposed to be unique. We use this as the destination
                                                          ' file name.
760                                                       iDupCount = 0
770                                                       While gobjFSO.FileExists(strDestinationFile)
                                                              'destination file exists. find a unique file name by appending the duplicate counter.
780                                                           iDupCount = iDupCount + 1
790                                                           strDestinationFile = gobjFSO.BuildPath(strFileLinkPath, g_strJobID & "-" & iDupCount & "." & gobjFSO.GetExtensionName(strSourceFile))
800                                                       Wend
                                                          
                                                          'copy the source to destination
810                                                       gobjFSO.CopyFile strSourceFile, strDestinationFile, True
                                                          
                                                          'this is how the file can be accessed from external machine (not within the same machine that runs this service)
820                                                       strDestinationFile2Caller = gobjFSO.BuildPath(strFileLinkPath, gobjFSO.GetBaseName(strSourceFile) & "." & gobjFSO.GetExtensionName(strSourceFile))
                                                          
                                                          'add this document reference to the POST template
830                                                       Set oTemplateFilePath = oDocument.SelectSingleNode("FilePath")
                                                          
840                                                       If oTemplateFilePath Is Nothing Then
850                                                           Set oTemplateFilePath = objXML.createElement("FilePath")
860                                                           oDocument.appendChild oTemplateFilePath
870                                                       End If
                                                          
880                                                       oTemplateFilePath.Text = ""
890                                                       oTemplateFilePath.appendChild objXML.createCDATASection(strDestinationFile)
900                                                   End If
                                                      
                                                      'the document on the POST XML was updated. We can stop looping the processed documents now
                                                      ' and continue to match the next document in the POST XML.
910                                                   Exit For
920                                               End If
930                                           Next
940                                       End If
950                                   Else
960                                       Trace "      POST template Document node contains no key information to link to the actual document." & vbCrLf
970                                   End If
                                      'oDocument.removeAttribute "key"
980                                   DoEvents
990                               Next
                                  
1000                              strPostXML = objPostXML.XML
1010                          Else
                                  'No Documents node found in the Post XML. Append a generic template.
1020                              objPostXML.appendChild objXML.createElement("Documents")
1030                              objXML.loadXML objPostXML.XML
1040                              GoTo appendDocuments
1050                          End If
1060                      End If
                          
1070                      If g_strHTTPOverride <> "" Then
1080                          strURL = g_strHTTPOverride
1090                          Trace "      OVERRIDING HTTP Post DESTINATION FROM THE CONFIGURATION." & g_strHTTPOverride & vbCrLf
1100                      End If
                          
1110                      strPostFileName = getSafeFileName(gobjFSO.BuildPath(g_strSortTempPath, g_strJobID & "_http_post.txt"))
1120                      Set objPostDataStream = gobjFSO.OpenTextFile(strPostFileName, ForWriting, True)
1130                      objPostDataStream.Write strPostXML
1140                      objPostDataStream.Close
1150                      Set objPostDataStream = Nothing
                          
1160                      Trace "         Post data Saved to : " & gobjFSO.GetBaseName(strPostFileName) & "." & gobjFSO.GetExtensionName(strPostFileName)
                          
1170                      If Len(strPostXML) > g_lngPostBytesLimit Then
1180                          Trace "         POST data exceeds the maximum limit of " & g_lngPostBytesLimit & ". If the URL is served by IIS, you may need to bump up the default upload limit and change the Document processor config file." & vbCrLf & _
                                    "         SKIPPING THE POST JOB." & vbCrLf
1190                          processHTTPPost = False
1200                          GoTo errHandler
1210                      End If
                          
                          'post the xml to the webserver
1220                      Set objXMLHTTPPost = New MSXML2.XMLHTTP40
1230                      objXMLHTTPPost.Open "POST", strURL, False
1240                      objXMLHTTPPost.setRequestHeader "Content-Length", Len(strPostXML)
1250                      objXMLHTTPPost.send strPostXML
                          
1260                      strResponseText = Trim(objXMLHTTPPost.responseText)
                          
1270                      Trace vbCrLf & _
                                "         POST Result" & vbCrLf & _
                                "         ----------------" & vbCrLf & _
                                "           Status       : " & objXMLHTTPPost.Status & vbCrLf & _
                                "           Response Text: " & strResponseText & vbCrLf
                                
1280                      If strResponseText <> "" Then
                              'check for any error
                              
                              'check if the return is an XML
1290                          Set objXMLRet = New MSXML2.DOMDocument
1300                          objXMLRet.async = False
1310                          objXMLRet.loadXML strResponseText
1320                          If objXMLRet.parseError.errorCode = 0 Then
                                  'yep. it is an XML. Check if an Error node exist and if it has any message
1330                              Set objErrorNode = objXMLRet.SelectSingleNode("//Error")
1340                              If Not objErrorNode Is Nothing Then
1350                                  If objErrorNode.Text <> "" Then
1360                                      processHTTPPost = False
1370                                      g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & "HTTP Post received error XML. Message: " & objErrorNode.Text & vbCrLf
1380                                      Trace "      ERROR: HTTP Post received error XML. Message: " & objErrorNode.Text & vbCrLf
1390                                      GoTo errHandler
1400                                  End If
1410                              End If
1420                          End If
1430                      End If
                          
1440                      If objXMLHTTPPost.Status = 200 Then
1450                          processHTTPPost = True
1460                      Else
1470                          processHTTPPost = False
1480                      End If
1490                  Else
1500                      processHTTPPost = False
1510                      g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & "No documents were passed in" & vbCrLf
1520                      Trace "      ERROR: oDocumentNodes object is nothing." & vbCrLf
1530                  End If
1540              Else ' strURL is empty
1550                  processHTTPPost = False
1560                  g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & "URL to post to is empty." & vbCrLf
1570                  Trace "      ERROR: URL to post is empty." & vbCrLf
1580              End If
1590          Else ' no URL node
1600              processHTTPPost = False
1610              g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & "URL to post to is not found." & vbCrLf
1620              Trace "      ERROR: URL to post is not found" & vbCrLf
1630          End If
1640      Else
1650          processHTTPPost = False
1660          g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & "No destination provided." & vbCrLf
1670          Trace "      ERROR: Destination node is nothing" & vbCrLf
1680      End If
          
1690      Trace "      HTTP Post performed in " & (GetTickCount() - lStartTick) & " ms." & vbCrLf
          
errHandler:
1700      Set objXMLHTTPPost = Nothing
1710      Set objPostXML = Nothing
1720      Set objXML2Post = Nothing
1730      Set objNode = Nothing
1740      Set oDocument = Nothing
1750      Set oCurDocument = Nothing
1760      Set objXML = Nothing
1770      Set oPostDocuments = Nothing
1780      Set oProcessedDoc = Nothing
1790      Set oTemplateFilePath = Nothing
1800      Set objXMLRet = Nothing
1810      Set objErrorNode = Nothing
          
1820      If Err.Number <> 0 Then
1830          processHTTPPost = False
1840          g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & PROC_NAME & " [Line # " & Erl & "] " & Err.Description & vbCrLf
1850          Trace "       ERROR in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
1860      End If
1870      DoEvents
End Function

Private Function getDocumentBaseNames(ByRef aDocuments() As String) As String
          Dim strRet As String
          Dim i As Integer
          
10        strRet = vbCrLf
          
20        For i = LBound(aDocuments) To UBound(aDocuments)
30            If aDocuments(i) <> "" Then
40                strRet = strRet & gobjFSO.GetBaseName(aDocuments(i)) & "." & gobjFSO.GetExtensionName(aDocuments(i)) & vbCrLf
50            End If
60        Next
          
70        getDocumentBaseNames = strRet
          
End Function

Private Function attachDocument(strSourceFile As String, strFilePath As String, _
                                blnEmbed As Boolean, iFileCounter As Integer, _
                                ByRef oDocument As IXMLDOMElement) As String
          Dim strDestinationFile As String
          Dim iDupCount As Integer
          Dim strDestinationFile2Caller As String
          Dim oADOStream As ADODB.Stream
          'Dim objNode As IXMLDOMElement
          
10        If blnEmbed Then
              'embed the file into the XML Element
20            oDocument.dataType = "bin.base64"
              
30            Set oADOStream = New ADODB.Stream
40            oADOStream.Type = adTypeBinary
50            oADOStream.Open
              
60            oADOStream.LoadFromFile strSourceFile
70            oDocument.nodeTypedValue = oADOStream.Read(-1)
80            oADOStream.Close
90            Set oADOStream = Nothing
              
100           oDocument.setAttribute "FileName", gobjFSO.GetBaseName(strSourceFile)
110           oDocument.setAttribute "FileExt", UCase(gobjFSO.GetExtensionName(strSourceFile))
120           oDocument.setAttribute "FileLength", FileLen(strSourceFile)
              
130           attachDocument = strSourceFile
140       End If

End Function

Public Function processFTP(ByRef oFTPDestination As IXMLDOMElement, aSingleAttachments() As String, aMergedDocuments() As String) As Boolean
          Const PROC_NAME As String = MODULE_NAME & "processFTP()"
10        On Error GoTo ftpFailed
          Dim strFTPServer As String
          Dim strFTPServerPort As String
          Dim strFTPOptions As String
          Dim strLogin As String
          Dim strPassword As String
          Dim strPath As String
          Dim strFTPURL As String
          Dim iAttachment As Integer
          Dim iAttachmentCount As Integer
          Dim blnRet As Boolean
          Dim lStartTick As Long
          Dim strFTPScript As String
          Dim strFTPResult As String
          Dim strFTPTempPath As String
          Dim strTemp() As String
          Dim strCmd As String
          Dim strLogFile As String
    
          Dim strBatchFile As String
          Dim bRet As Boolean
          Dim oTextFile As TextStream
          Dim blnUseINI As Boolean
          Dim strSessionName As String
          Dim iStartPos As Integer
          Dim iEndPos As Integer

          Dim objShell As Object
          Dim objExec As Object
    
    
20        lStartTick = GetTickCount()
    
30        strFTPServer = getXMLValue(oFTPDestination, "remoteserver")
40        strLogin = getXMLValue(oFTPDestination, "remoteUID")
50        strPassword = getXMLValue(oFTPDestination, "remotePWD")
60        strPath = getXMLValue(oFTPDestination, "path")
    
70        blnRet = True
80        blnUseINI = False
    
90        If InStr(1, strFTPServer, "$INI$", vbTextCompare) > 0 Then blnUseINI = True
    
100       If blnUseINI = False Then
110           strFTPURL = buildFTPURL(strFTPServer, strLogin, strPassword, strPath)
120       Else
130           strFTPServer = Mid(strFTPServer, 6)
140       End If
    
150       Trace "      FTP Server: " & strFTPServer & vbCrLf & _
                "      Login:      " & strLogin & vbCrLf & _
                "      PWD:        " & strPassword & vbCrLf & _
                "      Directory:  " & strPath & vbCrLf & _
                "      URL (generated): " & strFTPURL & vbCrLf & _
                "      INI mode: " & blnUseINI & vbCrLf
    
160       If g_strFTPOverride <> "" Then
170           Trace "      OVERRIDING DESTINATION FROM THE CONFIGURATION. FTP Server: " & g_strFTPOverride & vbCrLf
180           strFTPServer = g_strFTPOverride
              'strLogin = "anonymous"
              'strPassword = "" ' some default anonymous email address
190           blnUseINI = False
200           strFTPURL = strFTPServer '"ftp://" & strLogin & "@" & strFTPServer
210       End If
    
220       If strFTPServer <> "" Then
230               iAttachmentCount = 0
            
240               If blnUseINI = True Then
                      'write the ini file to the file system
250                   strBatchFile = getSafeFileName(gobjFSO.BuildPath(g_strSortTempPath, "ftpSettings.ini"))

260                   Set oTextFile = gobjFSO.OpenTextFile(strBatchFile, ForWriting, True)
270                   oTextFile.WriteLine Replace(strFTPServer, "|", vbCrLf)
280                   oTextFile.Close
290                   Set oTextFile = Nothing
                
300                   iStartPos = InStr(1, strFTPServer, "[Sessions\", vbTextCompare)
310                   iEndPos = InStr(1, strFTPServer, "]", vbTextCompare) - iStartPos - Len("[Sessions\")
                
320                   strFTPURL = Mid(strFTPServer, _
                                      iStartPos + Len("[Sessions\"), _
                                      iEndPos)
                
330               End If

340               strFTPScript = ""
350               strFTPScript = "option batch abort" & vbCrLf & _
                                  "option confirm off" & vbCrLf & _
                                  "open " & strFTPURL & vbCrLf & _
                                  "option transfer automatic" & vbCrLf & _
                                  "lcd """ & g_strSortTempPath & """" & vbCrLf


                  'change remote directory
360               If strPath <> "" Then
370                   strFTPScript = strFTPScript & "cd """ & strPath & """" & vbCrLf
380               End If
            
                  'put the merge files
390               For iAttachment = LBound(aMergedDocuments) To UBound(aMergedDocuments)
400                   If Trim(aMergedDocuments(iAttachment)) <> "" And gobjFSO.FileExists(aMergedDocuments(iAttachment)) Then
410                       strFTPScript = strFTPScript & "put """ & gobjFSO.GetBaseName(aMergedDocuments(iAttachment)) & "." & gobjFSO.GetExtensionName(aMergedDocuments(iAttachment)) & """" & vbCrLf
420                       iAttachmentCount = iAttachmentCount + 1
430                   End If
440               Next

                  'put the single documents
450               For iAttachment = LBound(aSingleAttachments) To UBound(aSingleAttachments)
460                   If Trim(aSingleAttachments(iAttachment)) <> "" And gobjFSO.FileExists(aSingleAttachments(iAttachment)) Then
470                       strFTPScript = strFTPScript & "put """ & gobjFSO.GetBaseName(aSingleAttachments(iAttachment)) & "." & gobjFSO.GetExtensionName(aSingleAttachments(iAttachment)) & """" & vbCrLf
480                       iAttachmentCount = iAttachmentCount + 1
490                   End If
500               Next

510               strFTPScript = strFTPScript & "close" & vbCrLf & _
                                                  "exit" & vbCrLf
520               strFTPResult = ""

530               strCmd = g_strFTPClient
540               If blnUseINI = True Then strCmd = strCmd & " /ini=" & strBatchFile
550               strCmd = strCmd & " /timeout=10 /log=" & getSafeFileName(gobjFSO.BuildPath(g_strSortTempPath, "\log.txt"))
            
560               Trace "      Generated FTP WinSCP script: " & vbCrLf & _
                        strFTPScript & vbCrLf & _
                        "      Executing FTP Command: " & strCmd & vbCrLf

570               Set objShell = CreateObject("WScript.Shell")
580               Set objExec = objShell.exec(strCmd)

590               objExec.StdIn.Write strFTPScript

600               While (Not objExec.StdOut.AtEndOfStream)
610                   strFTPResult = strFTPResult & objExec.StdOut.ReadAll()
620               Wend

630               Trace "      FTP Result:" & vbCrLf & strFTPResult

640               Set objShell = Nothing
650               Set objExec = Nothing

660               If InStr(1, strFTPResult, "Connection failed", vbTextCompare) > 0 Or _
                      InStr(1, strFTPResult, "Connection timed out", vbTextCompare) > 0 Or _
                      InStr(1, strFTPResult, "cannot find", vbTextCompare) > 0 Or _
                      InStr(1, strFTPResult, "does not exist", vbTextCompare) > 0 Or _
                      InStr(1, strFTPResult, "system error", vbTextCompare) > 0 Then
670                   blnRet = False
680                   Trace "      FTP script resulted in a failure indication." & vbCrLf
690                   GoTo ftpFailed
700               Else
710                   blnRet = True
720               End If

730               strTemp = Split(strFTPResult, "100%", , vbTextCompare)

740               If iAttachmentCount <> UBound(strTemp) Then
750                   Trace "      # of attachments uploaded does not match the number of file in the job." & strFTPResult & vbCrLf
760                   blnRet = False
770               Else
780                   Trace "FTP Done." & vbCrLf
790               End If
800       Else
810           Trace "      FTP SERVER INFORMATION NOT PROVIDED." & vbCrLf
820           blnRet = False
830           g_strDestinationErrMsg = "FTP Server information not provided" & vbCrLf
840       End If
850       Trace "      Process FTP in " & (GetTickCount() - lStartTick) & " ms" & vbCrLf
ftpFailed:
860       Set objShell = Nothing
870       Set objExec = Nothing
    
880       If Err.Number <> 0 Or blnRet = False Then
890           Trace vbCrLf
900           blnRet = False
910           g_strDestinationErrMsg = g_strDestinationErrMsg & vbCrLf & PROC_NAME & " [Line # " & Erl & "] " & Err.Description & vbCrLf
920           Trace "       ERROR in " & PROC_NAME & " [Line #" & Erl & "] Description:" & Err.Description & vbCrLf
930       End If
940       DoEvents
    
950       processFTP = blnRet
End Function


Private Function buildFTPURL(strFTPServer As String, strFTPLogon As String, strFTPPassword As String, strFTPPath As String) As String
          Dim re As RegExp
          Dim matches As MatchCollection
          Dim match As Object
          Dim strProtocol As String
          Dim strServer As String
          Dim strPort As String
          Dim strRet As String
          
          'fix the logon
10        strFTPLogon = EscapeString(strFTPLogon)
          
          'fix the password
20        strFTPPassword = EscapeString(strFTPPassword)
          
          'fix the path
30        If strFTPPath <> "" Then
40            If Left(strFTPPath, 1) <> "/" Then
50                strFTPPath = "/" & strFTPPath
60            End If
70        End If
          
80        Set re = New RegExp
          
90        re.IgnoreCase = True
100       re.Global = True
110       re.Pattern = "(ftp://|sftp://){0,}(\d{1,}.\d{1,}.\d{1,}.\d{1,}|\w{1,}):{0,1}(\d{0,})$"
120       Set matches = re.Execute(strFTPServer)
130       If matches.Count > 0 Then
140           Set match = matches(0)
150           If match.SubMatches.Count = 3 Then
160               strProtocol = match.SubMatches(0)
170               strServer = match.SubMatches(1)
180               strPort = match.SubMatches(2)
190               If strProtocol = "" Then strProtocol = "ftp://"
                  
200               strRet = strProtocol & strFTPLogon & ":" & strFTPPassword & "@" & strServer
210               'If strPort <> "" Then strRet = strRet & ":" & strPort
                  
220               If strFTPPath <> "" Then strRet = strRet & strFTPPath
                  
230           End If
240       Else
250           strRet = "ftp://" & strFTPLogon & ":" & strFTPPassword & "@" & strFTPServer & "/" & strFTPPath
260       End If
          
270       buildFTPURL = strRet
End Function

Public Function EscapeString(ByRef strIn As String) As String
          Dim strOut As String
          Dim intChar As Integer
          Dim strEscape As String
          Dim i As Integer
          
10        strOut = strIn
          
20        For i = Len(strIn) To 1 Step -1
30            intChar = Asc(Mid(strIn, i, 1))
40            If ((intChar < 48) And (intChar <> 32)) _
                  Or ((intChar > 57) And (intChar < 65)) _
                  Or ((intChar > 90) And (intChar < 97)) _
                  Or (intChar > 122) Then
                  
50                strEscape = Hex(intChar)
60                If Len(strEscape) < 2 Then
70                    strEscape = "%0" & strEscape
80                Else
90                    strEscape = "%" & strEscape
100               End If
110               strOut = Left(strOut, i - 1) + strEscape _
                      + Right(strOut, Len(strOut) - i)
120           End If
130       Next
140       EscapeString = Replace(strOut, " ", "+")
End Function




