Attribute VB_Name = "modShell"
Option Explicit
Option Compare Text

Private Const INFINITE = &HFFFF
Private Const STARTF_USESHOWWINDOW = &H1

Private Const STATUS_ABANDONED_WAIT_0 As Long = &H80
Private Const STATUS_WAIT_0 As Long = &H0
Private Const STATUS_USER_APC As Long = &HC0

Private Const WAIT_ABANDONED As Long = (STATUS_ABANDONED_WAIT_0 + 0)
Private Const WAIT_ABANDONED_0 As Long = (STATUS_ABANDONED_WAIT_0 + 0)
Private Const WAIT_FAILED As Long = &HFFFFFFFF
Private Const WAIT_IO_COMPLETION As Long = STATUS_USER_APC
Private Const WAIT_OBJECT_0 As Long = (STATUS_WAIT_0 + 0)
Private Const WAIT_TIMEOUT As Long = 258&


Public Enum enSW
    SW_HIDE = 0
    SW_NORMAL = 1
    SW_MAXIMIZE = 3
    SW_MINIMIZE = 6
End Enum

Public Type PROCESS_INFORMATION
    hProcess As Long
    hThread As Long
    dwProcessId As Long
    dwThreadId As Long
End Type

Public Type STARTUPINFO
    cb As Long
    lpReserved As String
    lpDesktop As String
    lpTitle As String
    dwX As Long
    dwY As Long
    dwXSize As Long
    dwYSize As Long
    dwXCountChars As Long
    dwYCountChars As Long
    dwFillAttribute As Long
    dwFlags As Long
    wShowWindow As Integer
    cbReserved2 As Integer
    lpReserved2 As Byte
    hStdInput As Long
    hStdOutput As Long
    hStdError As Long
End Type

Public Type SECURITY_ATTRIBUTES
    nLength As Long
    lpSecurityDescriptor As Long
    bInheritHandle As Long
End Type

Public Enum enPriority_Class
    NORMAL_PRIORITY_CLASS = &H20
    IDLE_PRIORITY_CLASS = &H40
    HIGH_PRIORITY_CLASS = &H80
End Enum

Private Type LUID
    LowPart As Long
    HighPart As Long
End Type

Private Type LUID_AND_ATTRIBUTES
    pLuid As LUID
    Attributes As Long
End Type

Private Type TOKEN_PRIVILEGES
    PrivilegeCount As Long
    TheLuid As LUID
    Attributes As Long
End Type



Private Declare Function CreateProcess Lib "kernel32" Alias "CreateProcessA" (ByVal lpApplicationName As String, ByVal lpCommandLine As String, lpProcessAttributes As SECURITY_ATTRIBUTES, lpThreadAttributes As SECURITY_ATTRIBUTES, ByVal bInheritHandles As Long, ByVal dwCreationFlags As Long, lpEnvironment As Any, ByVal lpCurrentDriectory As String, lpStartupInfo As STARTUPINFO, lpProcessInformation As PROCESS_INFORMATION) As Long
Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Private Declare Function TerminateProcess Lib "kernel32" (ByVal hProcess As Long, ByVal uExitCode As Long) As Long
Private Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, lpdwProcessId As Long) As Long
Private Declare Function AdjustTokenPrivileges Lib "advapi32.dll" (ByVal TokenHandle As Long, ByVal DisableAllPrivileges As Long, NewState As TOKEN_PRIVILEGES, ByVal BufferLength As Long, PreviousState As TOKEN_PRIVILEGES, ReturnLength As Long) As Long
Private Declare Function OpenProcessToken Lib "advapi32.dll" (ByVal ProcessHandle As Long, ByVal DesiredAccess As Long, TokenHandle As Long) As Long
Private Declare Function LookupPrivilegeValue Lib "advapi32.dll" Alias "LookupPrivilegeValueA" (ByVal lpSystemName As String, ByVal lpName As String, lpLuid As LUID) As Long
Private Declare Function GetCurrentProcess Lib "kernel32" () As Long

Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long

Public Declare Function GetTickCount Lib "kernel32" () As Long

'usage:     SuperShell filefullpath, filepath, 0, SW_NORMAL, HIGH_PRIORITY_CLASS
' the blnCleanup will be used when executing a dos application directly rather than
' using a bat file to execute it.
Public Function SuperShell(ByVal App As String, ByVal WorkDir As String, dwMilliseconds As Long, ByVal start_size As enSW, ByVal Priority_Class As enPriority_Class, Optional ByVal blnCleanup As Boolean = False) As Boolean
          Dim pclass As Long
          Dim sInfo As STARTUPINFO
          Dim pinfo As PROCESS_INFORMATION
          'Not used, but needed
          Dim sec1 As SECURITY_ATTRIBUTES
          Dim sec2 As SECURITY_ATTRIBUTES
          Dim lngProcessID As Long
          'Set the structure size
10        sec1.nLength = Len(sec1)
20        sec2.nLength = Len(sec2)
30        sInfo.cb = Len(sInfo)
          'Set the flags
40        sInfo.dwFlags = STARTF_USESHOWWINDOW
          'Set the window's startup position
50        sInfo.wShowWindow = start_size
          'Set the priority class
60        pclass = Priority_Class
          'Start the program
70        If CreateProcess(vbNullString, App, sec1, sec2, False, pclass, 0&, WorkDir, sInfo, pinfo) Then
80            lngProcessID = pinfo.dwProcessId
              'Wait
90            WaitForSingleObject pinfo.hProcess, dwMilliseconds
              
100           If blnCleanup Then
                  'kill the process id that was created
110               ProcessTerminate lngProcessID
120           End If

130           SuperShell = True
140       Else
150           SuperShell = False
160       End If
End Function

Function ProcessTerminate(Optional lProcessID As Long, Optional lHwndWindow As Long) As Boolean
          Dim lhwndProcess As Long
          Dim lExitCode As Long
          Dim lRetVal As Long
          Dim lhThisProc As Long
          Dim lhTokenHandle As Long
          Dim tLuid As LUID
          Dim tTokenPriv As TOKEN_PRIVILEGES, tTokenPrivNew As TOKEN_PRIVILEGES
          Dim lBufferNeeded As Long
          
          Const PROCESS_ALL_ACCESS = &H1F0FFF, PROCESS_TERMINATE = &H1
          Const ANYSIZE_ARRAY = 1, TOKEN_ADJUST_PRIVILEGES = &H20
          Const TOKEN_QUERY = &H8, SE_DEBUG_NAME As String = "SeDebugPrivilege"
          Const SE_PRIVILEGE_ENABLED = &H2

10        On Error Resume Next
20        If lHwndWindow Then
              'Get the process ID from the window handle
30            lRetVal = GetWindowThreadProcessId(lHwndWindow, lProcessID)
40        End If
          
50        If lProcessID Then
              'Give Kill permissions to this process
60            lhThisProc = GetCurrentProcess
              
70            OpenProcessToken lhThisProc, TOKEN_ADJUST_PRIVILEGES Or TOKEN_QUERY, lhTokenHandle
80            LookupPrivilegeValue "", SE_DEBUG_NAME, tLuid
              'Set the number of privileges to be change
90            tTokenPriv.PrivilegeCount = 1
100           tTokenPriv.TheLuid = tLuid
110           tTokenPriv.Attributes = SE_PRIVILEGE_ENABLED
              'Enable the kill privilege in the access token of this process
120           AdjustTokenPrivileges lhTokenHandle, False, tTokenPriv, Len(tTokenPrivNew), tTokenPrivNew, lBufferNeeded

              'Open the process to kill
130           lhwndProcess = OpenProcess(PROCESS_TERMINATE, 0, lProcessID)
          
140           If lhwndProcess Then
                  'Obtained process handle, kill the process
150               ProcessTerminate = CBool(TerminateProcess(lhwndProcess, lExitCode))
160               Call CloseHandle(lhwndProcess)
170           End If
180       End If
190       On Error GoTo 0
End Function
