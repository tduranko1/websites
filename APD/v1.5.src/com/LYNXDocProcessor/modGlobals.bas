Attribute VB_Name = "modGlobals"
Option Explicit

Const MODULE_NAME As String = "modGlobals:"

Public gobjFSO As Scripting.FileSystemObject

Private Type GUID
    Data1 As Long
    Data2 As Integer
    Data3 As Integer
    Data4(7) As Byte
End Type

Private Declare Function CoCreateGuid Lib "OLE32.dll" (pGuid As GUID) As Long
Private Declare Function StringFromGUID2 Lib "OLE32.dll" (ByRef rguid As GUID, ByVal lpsz As String, ByVal cchMax As Long) As Integer
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long

Public g_strMachineName As String

Public Function GenerateGUID() As String
          Dim udtGUID As GUID
          Dim strNewGUID As String
          Dim iChars As Integer
          
10        If (CoCreateGuid(udtGUID) = 0) Then
          
20            strNewGUID = Space(100)
              
              ' convert binary GUID to string form
30            iChars = StringFromGUID2(udtGUID, strNewGUID, Len(strNewGUID))
              ' convert string to ANSI
40            strNewGUID = StrConv(strNewGUID, vbFromUnicode)
              ' remove trailing null character
50            strNewGUID = Left(strNewGUID, iChars - 1)
              ' MSI likes only uppercase letters in GUID
60            strNewGUID = UCase(strNewGUID)
              
70            GenerateGUID = strNewGUID
              
80        End If
End Function

Public Function GenerateUniqueID() As String
          Dim strGUID As String
10        strGUID = GenerateGUID()
20        strGUID = Replace(strGUID, "{", "")
30        strGUID = Replace(strGUID, "}", "")
40        strGUID = Replace(strGUID, "-", "")
50        GenerateUniqueID = strGUID
End Function

Public Sub initGlobals()
10        Set gobjFSO = New Scripting.FileSystemObject
20        g_strMachineName = GetWinComputerName()
30        loadConfig
End Sub

Public Sub terminateGlobals()
          'Set gobjFSO = Nothing
10        unloadConfig
End Sub

'********************************************************************
'* Syntax:  GetWinComputerName
'* Params:  None
'* Purpose: This procedure retrieves a NetBIOS name
'*              associated with the local computer.
'* Returns: An string - the name.
'********************************************************************
Public Function GetWinComputerName() As String
          Const PROC_NAME As String = MODULE_NAME & "GetWinComputerName()"
10        On Error GoTo errHandler
          Dim strName As String
          Dim lngCnt As Long
          
20        strName = Space(255)
30        lngCnt = GetComputerName(strName, 255)
          
40        If lngCnt <> 0 Then
50            strName = Left(strName, InStr(strName, Chr$(0)) - 1)
60        End If
          
70        GetWinComputerName = strName
80        Exit Function
errHandler:
90        If Err.Number <> 0 Then
100           GetWinComputerName = ""
110       End If
End Function

