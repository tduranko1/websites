Attribute VB_Name = "MDocumentMgr"
'********************************************************************************
'* Component DocumentMgr : Module MDocumentMgr
'*
'* Global Consts and Utility Methods
'*
'********************************************************************************
Option Explicit

Public Const APP_NAME As String = "DocumentMgr."
Private Const MODULE_NAME As String = APP_NAME & "MDocumentMgr."

Public Const DocumentMgr_FirstError As Long = &H80064800

Public Enum DocumentMgr_ErrorCodes

    'General error codes applicable to all modules of this component
    eNull = DocumentMgr_FirstError + &H80

    'Module specific error ranges.
    MDocumentMgr_BAS_ErrorCodes = DocumentMgr_FirstError + &H100
    CDocumentMgr_CLS_ErrorCodes = DocumentMgr_FirstError + &H180

End Enum

'********************************************************************
'* Global Vars
'********************************************************************

Public g_objEvents As Object 'SiteUtilities.CEvents
Public g_objDataAccessor As Object 'DataAccessor.CDataAccessor

Public g_blnDebugMode As Boolean
Public g_strSupportDocPath As String

Public g_strPartnerConnStringXml As String   'Current strConn to DataAccesor for PARTNER XML data
Public g_strPartnerConnStringStd As String   'Current strConn to DataAccesor for PARTNER recordset data
Public g_strApdConnStringXml As String      'Current strConn to DataAccessor for LYNX XML data
Public g_strApdConnStringStd As String      'Current strConn to DataAccessor for LYNX recordset data

'********************************************************************
'* Util methods to prevent modification of
'* any error information stored in the Err object.
'********************************************************************
Type typError
    Number As Long
    Description As String
    Source As String
End Type

Public objError As typError

Public Sub PushErr()
10        objError.Number = Err.Number
20        objError.Description = Err.Description
30        objError.Source = Err.Source
40        Err.Clear
End Sub

Public Sub PopErr()
10        Err.Number = objError.Number
20        Err.Description = objError.Description
30        Err.Source = objError.Source
End Sub

'********************************************************************************
'* Initialize Global Objects and Variables
'********************************************************************************
Public Sub InitializeGlobals()

          'Create private member DataAccessor.
10        Set g_objDataAccessor = CreateObjectEx("DataAccessor.CDataAccessor")

          'Share DataAccessor's events object.
20        Set g_objEvents = g_objDataAccessor.mEvents

          'Get path to support document directory
30        g_strSupportDocPath = App.Path & "\" & Left(APP_NAME, Len(APP_NAME) - 1)

          'This will give partner data its own log file.
40        g_objEvents.ComponentInstance = "Document Manager"

          'Initialize our member components first, so we have logging set up.
50        g_objDataAccessor.InitEvents App.Path & "\..\config\config.xml"

          'Get debug mode status.
60        g_blnDebugMode = g_objEvents.IsDebugMode
          
          'This connection points the component at the appropriate partner dBase
70        g_strPartnerConnStringStd = GetConfig("PartnerSettings/ConnectionStringStd")
80        g_strPartnerConnStringXml = GetConfig("PartnerSettings/ConnectionStringXml")

          'Lookup connection strings for the LYNX APD database
90        g_strApdConnStringStd = GetConfig("ConnectionStringStd")
100       g_strApdConnStringXml = GetConfig("ConnectionStringXml")

110       If g_blnDebugMode Then g_objEvents.Trace g_strSupportDocPath, "DocPath"
          
End Sub

'********************************************************************************
'* Terminate Global Objects
'********************************************************************************
Public Sub TerminateGlobals()
10        Set g_objEvents = Nothing
20        Set g_objDataAccessor = Nothing
End Sub

'********************************************************************************
'* Returns the requested configuration setting.
'********************************************************************************
Public Function GetConfig(ByVal strSetting As String)
10        GetConfig = g_objEvents.mSettings.GetParsedSetting(strSetting)
End Function

'********************************************************************************
'* Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
'* Parameters:  strObjectName = The object to call CreateObject for.
'* Purpose:     Wraps CreateObject() with better debug information.
'* Returns:     The Object created or Nothing.
'********************************************************************************
Public Function CreateObjectEx(strObjectName) As Object
          Dim obj As Object
          
10        On Error GoTo ErrorHandler
          
20        Set CreateObjectEx = Nothing
          
30        Set obj = CreateObject(strObjectName)
          
40        If obj Is Nothing Then
50            Err.Raise DocumentMgr_FirstError + 10, "", "CreateObject('" & strObjectName & "') returned Nothing."
60        End If
          
70        Set CreateObjectEx = obj
          
ErrorHandler:

80        Set obj = Nothing
          
90        If Err.Number <> 0 Then
100           Err.Raise Err.Number, "CreateObjectEx('" & strObjectName & "') " & Err.Source, Err.Description & " " & strObjectName
110       End If
          
End Function

'********************************************************************************
'* Construct for the invoice the actual filename as it will be stored on disk.
'********************************************************************************
Public Function BuildFileName(ByVal strEntity As String, ByVal strLynxID As String) As String
    Dim x As Integer
    Dim strImageFolder As String
    Dim strLynxIDLastFour As String
    Dim strDateTime As String
    Dim lngHypenPos As Long

    'Use the current date time to make the file name unique.
    strDateTime = Format(Now, "yyyymmddhhmmss")

    'Get the last 4 digits of the base lynx id for the file folder hierarchy. The lynxid should be in one of two formats:
    ' 123456 or 123456-1
    lngHypenPos = InStr(strLynxID, "-")
    
    If lngHypenPos > 0 Then
      strLynxIDLastFour = Mid(strLynxID, lngHypenPos - 4, 4)
    Else
      strLynxIDLastFour = Right(strLynxID, 4)
    End If
            
    ' Build a path into the document storage directory structure based on the last four digits of the lynxID.
    For x = 1 To 4
        strImageFolder = strImageFolder & "\" & Mid(strLynxIDLastFour, x, 1)
    Next
    
    strImageFolder = strImageFolder & "\"

    'Put it all together now...
    BuildFileName = strImageFolder & strEntity & strDateTime & "LYNXID" & strLynxID & "Doc.doc"

End Function



