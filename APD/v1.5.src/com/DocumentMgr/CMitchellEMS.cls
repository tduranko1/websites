VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMitchellEMS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Class CMitchellEMS
'*
'* This class is used to go out to our Mitchell Utramate estimating package
'* server and pull (or in the future, push) estimate data from EMS files.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CMitchellEMS."

'Error codes specific to this class.
Private Enum Error_Codes
    eNull = &H80065000 + &H200
End Enum

'Enum for indexing into the variant array returned from GetEstimateTotals()
Private Enum EstimateTotals
    eGrossTotal = 0
    eGrossBetterment
    eGrossDeductible
    eOtherAdjustments
    eNetTotal
    eGrossRelatedPriorDamage
    eGrossUnrelatedPriorDamage
    eGrossAppearanceAllowance
End Enum

Private mobjSystem As Scripting.FileSystemObject

Private Sub Class_Initialize()
10        Set mobjSystem = New Scripting.FileSystemObject
End Sub

Private Sub Class_Terminate()
10        Set mobjSystem = Nothing
20        TerminateGlobals
End Sub

'********************************************************************************
'* Pulls estimate totals from EMS for selected LynxID.
'* Will look for the most recent export on the server matching this LynxID.
'* Returned variant array can be indexed by the EstimateTotals enum above.
'********************************************************************************
Public Function GetEstimateTotals( _
    ByVal strLynxID As String, _
    Optional ByVal strEstPackage As String = "UltraMate" _
) As Variant

          Const PROC_NAME As String = MODULE_NAME & "GetEstimateTotals: "

          'Objects that require clean-up.
          Dim objDB As Database
          Dim objRS As Recordset
          
          'Primitives that don't.
          Dim strDataPath As String
          Dim strExportPath As String
          Dim strDaysAllowed As String
          Dim intDaysAllowed As Integer
          Dim intCurrentDay As Integer
          Dim datSearch As Date
          Dim datCurrent As Date
          Dim strDateSearch As String
          Dim varReturn() As Variant
          
          Const strTTL As String = ".TTL"
          Const strDBF As String = ".DBF"
          
10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Trace and assert params.
30        If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Started for LynxID = " & strLynxID
          
40        g_objEvents.Assert CBool(strLynxID <> ""), "No LynxID Passed!"
          
50        ReDim varReturn(eGrossAppearanceAllowance)
          
          'Extract the path to the Mitchell server export directory from the config.xml file.
60        strExportPath = GetConfig("EstimatingPackages/" & strEstPackage & "/ExportPath")
          
          'Get the age in days to allow for EMS files, assert, and convert.
70        strDaysAllowed = GetConfig("EstimatingPackages/" & strEstPackage & "/AgeInDaysAllowed")
          
80        g_objEvents.Assert IsNumeric(strDaysAllowed), _
              "AgeInDaysAllowed pulled from config.xml was non-numeric."
              
90        intDaysAllowed = CInt(strDaysAllowed)
          
          'Start search at today, and move back a maximum of intDaysAllowed as needed.
100       For intCurrentDay = -1 To intDaysAllowed
          
              'Start from folder root.
110           strDataPath = strExportPath
              
              'After first attempt look in the dated subfolders.
120           If intCurrentDay <> -1 Then
              
                  'This subtracts days from the current date, wonder of all wonders.
130               datSearch = Now - intCurrentDay
                  
                  'Build out path to the EMS totals file.
140               strDataPath = strExportPath & Format(datSearch, "yyyymmdd") & "\"
                  
150           End If
              
              'See if the file exists for the current day.
160           If mobjSystem.FileExists(strDataPath & strLynxID & strTTL) Then
              
                  'Copy the TTL file to a DBF file so that DBEngine will accept it.
170               mobjSystem.CopyFile strDataPath & strLynxID & strTTL, _
                      strDataPath & strLynxID & strDBF, True
                  
                  'Open the TTL file.
180               Set objDB = DBEngine.OpenDatabase(strDataPath, False, True, "dBASE IV;")
190               Set objRS = objDB.OpenRecordset("select * from " & strLynxID & strDBF)
                  
                  'Extract the values we require.
200               varReturn(eGrossTotal) = CCur(objRS!G_TTL_AMT)
210               varReturn(eGrossBetterment) = CCur(objRS!G_BETT_AMT)
220               varReturn(eGrossDeductible) = CCur(objRS!G_DED_AMT)
230               varReturn(eNetTotal) = CCur(objRS!N_TTL_AMT)
240               varReturn(eGrossRelatedPriorDamage) = CCur(objRS!G_RPD_AMT)
250               varReturn(eGrossUnrelatedPriorDamage) = CCur(objRS!G_UPD_AMT)
260               varReturn(eGrossAppearanceAllowance) = CCur(objRS!G_AA_AMT)
                  
                  'Kill DB Objects
270               Set objRS = Nothing
280               Set objDB = Nothing
                  
                  'Do any calculations.
290               varReturn(eOtherAdjustments) = _
                      varReturn(eGrossRelatedPriorDamage) + _
                      varReturn(eGrossUnrelatedPriorDamage) - _
                      varReturn(eGrossAppearanceAllowance)
       
300               Exit For
              
310           End If
          
320       Next
          
330       GetEstimateTotals = varReturn
          
      'Error handler localized to data retrieval and validation.
ErrorHandler:

          'Object clean-up
340       Set objRS = Nothing
350       Set objDB = Nothing
          
360       If Err.Number <> 0 Then
370           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
380       End If

End Function

