VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnitTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Class CUnitTest
'*
'* This purpose of this class is to establish a standard unit test interface.
'*
'* A client test application will will instantiate and call this interface in
'* turn for all components that implement it.  Results for each component will
'* be displayed to the tester.
'*
'* To implement for your component, include a copy of this file into your project.
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & ".CUnitTest."

'The number of tests called from this CUnitTest implementation.
Private Const mcintNumberOfTests As Integer = 6

Private mstrResults As String
Private mintErrors As String

'********************************************************************************
'* Returns a count of the number of tests that can be run for this application.
'********************************************************************************
Public Function NumTests() As Integer
10        NumTests = mcintNumberOfTests
End Function

'********************************************************************************
'* Returns the indexed test description.
'********************************************************************************
Public Function TestDesc(ByVal intIndex As Integer) As String
          Const PROC_NAME As String = MODULE_NAME & "TestDesc: "
          
10        Select Case intIndex
          
              'CLS modules
              Case 1: TestDesc = APP_NAME & "CDocMgr"
20            Case 2: TestDesc = APP_NAME & "CField"
30            Case 3: TestDesc = APP_NAME & "CMitchellEMS"
40            Case 4: TestDesc = APP_NAME & "CUpload"
50            Case 5: TestDesc = APP_NAME & "CWordDocument"
60            Case 6: TestDesc = APP_NAME & "CPathwaysEMS"
              
        Case Else:
70                Err.Raise vbObjectError + 1, PROC_NAME, "Passed test index out of range!"
80        End Select
          
End Function

'********************************************************************************
'* Runs the indexed test.
'* Returns the results as a giant formatted string.
'* Returns an error count through intErrors.
'********************************************************************************
Public Function RunTest(ByVal intIndex As Integer, ByRef intErrors As Integer) As String
          Const PROC_NAME As String = MODULE_NAME & "RunTest: "

10        Init intIndex 'Initialize results counters.
          
          'Run the test.
20        Select Case intIndex
          
              'CLS modules
              Case 1: RunTest_CDocMgr (intErrors)
30            Case 2: RunTest_CField (intErrors)
40            Case 3: RunTest_CMitchellEMS (intErrors)
50            Case 4: RunTest_CUpload (intErrors)
60            Case 5: RunTest_CWordDocument (intErrors)
70            Case 6: RunTest_CPathwaysEMS (intErrors)

              
        Case Else:
80                Err.Raise vbObjectError + 1, PROC_NAME, "Passed test index out of range!"
90        End Select
          
100       RunTest = Done(intErrors)
End Function

'********************************************************************************
'* Private results formatting helpers
'********************************************************************************

'Initialize results
Private Sub Init(ByVal intIndex As Integer)
10        mstrResults = vbCrLf & "TESTING : " & TestDesc(intIndex) & vbCrLf
20        mintErrors = 0
End Sub

Private Sub Success(ByVal strLine As String)
10        mstrResults = mstrResults & "SUCCESS : " & strLine & vbCrLf
End Sub

Private Sub Verify(ByVal strLine As String)
10        mstrResults = mstrResults & "VERIFY : " & strLine & "  Does this look right?" & vbCrLf
End Sub

Private Sub Failure(ByVal strLine As String)
10        mintErrors = mintErrors + 1
20        mstrResults = mstrResults & "FAILURE : " & strLine & vbCrLf
End Sub

'Add new results line.
Private Function Done(ByRef intErrors As Integer) As String
10        intErrors = mintErrors
20        Done = mstrResults
End Function

'********************************************************************************
'* ADD PRIVATE TEST METHODS BELOW THIS LINE.
'********************************************************************************

'********************************************************************************
'* Tests Class Module CDocMgr
'********************************************************************************
Private Function RunTest_CDocMgr(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CDocMgr
20        If obj Is Nothing Then
30            Failure "Could not create CDocMgr"
40        Else
50            Success "Created CDocMgr object."
60        End If
          
          'Other CDocMgr specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CField
'********************************************************************************
Private Function RunTest_CField(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CField
20        If obj Is Nothing Then
30            Failure "Could not create CField"
40        Else
50            Success "Created CField object."
60        End If
          
          'Other CField specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CMitchellEMS
'********************************************************************************
Private Function RunTest_CMitchellEMS(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CMitchellEMS
20        If obj Is Nothing Then
30            Failure "Could not create CMitchellEMS"
40        Else
50            Success "Created CMitchellEMS object."
60        End If
          
          'Other CMitchellEMS specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CUpload
'********************************************************************************
Private Function RunTest_CUpload(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CUpload
20        If obj Is Nothing Then
30            Failure "Could not create CUpload"
40        Else
50            Success "Created CUpload object."
60        End If
          
          'Other CUpload specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CWordDocument
'********************************************************************************
Private Function RunTest_CWordDocument(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CWordDocument
20        If obj Is Nothing Then
30            Failure "Could not create CWordDocument"
40        Else
50            Success "Created CWordDocument object."
60        End If
          
          'Other CWordDocument specific tests:
          
70        Set obj = Nothing
End Function
'********************************************************************************
'* Tests Class Module CPathwaysEMS
'********************************************************************************
Private Function RunTest_CPathwaysEMS(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CPathwaysEMS
20        If obj Is Nothing Then
30            Failure "Could not create CPathwaysEMS"
40        Else
50            Success "Created CPathwaysEMS object."
60        End If
          
          'Other CPathwaysEMS specific tests:
          
70        Set obj = Nothing
End Function



