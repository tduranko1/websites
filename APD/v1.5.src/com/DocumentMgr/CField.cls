VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CField"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'********************************************************************************
'* Field class represents interface to data passed within one field.
'* Mostly this is just a data container.
'********************************************************************************
Option Explicit
    
' Name of the field defined in form
Public Name As String

' Directory that file existed in on visitors computer
Public FileDir As String

' Extension of the file "GIF"
Public FileExt As String

' Name of the file "Photo.gif"
Public FileName As String

' Content / Mime type of file "image/gif"
Public ContentType As String
                        
' Unicode value of field (used for normal form fields - not files)
Public Value As String

' Binary data passed with field (for files)
Public BinaryData As Variant

' byte size of value or binary data
Public Length As Long

' Full path to file on visitors computer
Private mstrPath As String

Public Property Get BLOB() As Variant
10        BLOB = BinaryData
End Property

Public Property Let FilePath(ByRef pstrPath As String)
          
10        mstrPath = pstrPath
          
          ' Parse File Ext
20        If Not InStrRev(pstrPath, ".") = 0 Then
30            FileExt = Mid(pstrPath, InStrRev(pstrPath, ".") + 1)
40            FileExt = UCase(FileExt)
50        End If
          
          ' Parse File Name
60        If Not InStrRev(pstrPath, "\") = 0 Then
70            FileName = Mid(pstrPath, InStrRev(pstrPath, "\") + 1)
80        End If
          
          ' Parse File Dir
90        If Not InStrRev(pstrPath, "\") = 0 Then
100           FileDir = Mid(pstrPath, 1, InStrRev(pstrPath, "\") - 1)
110       End If
          
End Property

Public Property Get FilePath() As String
10        FilePath = mstrPath
End Property

