VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUpload"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'********************************************************************************
'* Upload class retrieves multi-part form data posted to web page
'* and parses it into objects that are easy to interface with.
'********************************************************************************
Option Explicit

Private mbinData As Variant         ' bytes visitor sent to server
Private mlngChunkIndex As Long      ' byte where next chunk starts
Private mlngBytesReceived As Long   ' length of data
Private mstrDelimiter As String     ' Delimiter between multipart/form-data (43 chars)

Private CR As Variant ' ANSI Carriage Return
Private LF As Variant ' ANSI Line Feed
Private CRLF As Variant ' ANSI Carriage Return & Line Feed

Private mobjFieldAry() As Object      ' Array to hold field objects
Private mlngCount As Long          ' Number of fields parsed

'********************************************************************************
'* Class Initialize
'********************************************************************************
Private Sub Class_Initialize()
          
          ' Redimension array with nothing
10        ReDim mobjFieldAry(0)
          
          ' Compile ANSI equivilants of carriage returns and line feeds
20        CR = ChrB(Asc(vbCr))    ' vbCr      Carriage Return
30        LF = ChrB(Asc(vbLf))    ' vbLf      Line Feed
40        CRLF = CR & LF          ' vbCrLf    Carriage Return & Line Feed

End Sub

'********************************************************************************
'* Class Terminate
'********************************************************************************
Private Sub Class_Terminate()
          
          Dim llngIndex As Long   ' Current Field Index
          
          ' Loop through fields
10        For llngIndex = 0 To mlngCount - 1
              
              ' Release field object
20            Set mobjFieldAry(llngIndex) = Nothing
              
30        Next
          
          ' Redimension array and remove all data within
40        ReDim mobjFieldAry(0)
          
End Sub

'********************************************************************************
'* ParseRequestData is called from your ASP to process any uploaded documents.
'* An example of an ASP that calls this:
'*
'*    Dim lBytes, oUpload, sFileName
'*
'*    lBytes = Request.TotalBytes
'*
'*    ' Instantiate Upload Class
'*    Set oUpload = Server.CreateObject("PFileUpload.CUpload")
'*
'*    oUpload.ParseRequestData lBytes, Request.BinaryRead(lBytes)
'*
'*    ' Grab the file name
'*    sFileName = oUpload.Fields("File1").FileName
'*
'*    ' Release upload object from memory
'*    Set oUpload = Nothing
'*
'********************************************************************************
Public Sub ParseRequestData(ByVal lngBytes As Long, ByRef varData As Variant)

          ' Set field count to zero
10        mlngCount = 0
          
          ' Determine number bytes visitor sent
20        mlngBytesReceived = lngBytes
          
          ' Store bytes recieved from visitor
30        mbinData = varData
          
          ' Parse out the delimiter
40        Call ParseDelimiter
          
          ' Parse the data
50        Call ParseData

End Sub

'********************************************************************************
'* Delimiter seperates multiple pieces of form data
'*    "around" 43 characters in length
'*    next character afterwards is carriage return (except last line has two --)
'*    first part of delmiter is dashes followed by hex number
'*    hex number is possibly the browsers session id?
'*
'* Examples:
'*
'* -----------------------------7d230d1f940246
'* -----------------------------7d22ee291ae0114
'*
'********************************************************************************
Private Sub ParseDelimiter()

10        mstrDelimiter = MidB(mbinData, 1, InStrB(1, mbinData, CRLF) - 1)
          
End Sub

'********************************************************************************
'* This procedure loops through each section (chunk) found within the
'* delimiters and sends them to the parse chunk routine
'********************************************************************************
Private Sub ParseData()

          Dim llngStart As Long       ' start position of chunk data
          Dim llngLength As Long      ' Length of chunk
          Dim llngEnd As Long         ' Last position of chunk data
          Dim lbinChunk As Variant    ' Binary contents of chunk
          
          ' Initialize at first character
10        llngStart = 1
          
          ' Find start position
20        llngStart = InStrB(llngStart, mbinData, mstrDelimiter & CRLF)
          
          ' While the start posotion was found
30        While Not llngStart = 0
              
              ' Find the end position (after the start position)
40            llngEnd = InStrB(llngStart + 1, mbinData, mstrDelimiter) - 2
              
              ' Determine Length of chunk
50            llngLength = llngEnd - llngStart
              
              ' Pull out the chunk
60            lbinChunk = MidB(mbinData, llngStart, llngLength)
              
              ' Parse the chunk
70            Call ParseChunk(lbinChunk)
              
              ' Look for next chunk after the start position
80            llngStart = InStrB(llngStart + 1, mbinData, mstrDelimiter & CRLF)
              
90        Wend
          
End Sub

'********************************************************************************
'* This procedure gets a chunk passed to it and parses its contents.
'* There is a general format that the chunk follows.
'*
'* First, the deliminator appears
'*
'* Next, headers are listed on each line that define properties of the chunk.
'*
'*   Content-Disposition: form-data: name="File1"; filename="C:\Photo.gif"
'*   Content-Type: image/gif
'*
'* After this, a blank line appears and is followed by the binary data.
'*
'********************************************************************************
Private Sub ParseChunk(ByRef pbinChunk As Variant)

          Dim lstrName As String          ' Name of field
          Dim lstrFileName As String      ' File name of binary data
          Dim lstrContentType As String   ' Content type of binary data
          Dim lbinData As Variant         ' Binary data
          Dim lstrDisposition As String   ' Content Disposition
          Dim lstrValue As String         ' Value of field
          
          ' Parse out the content dispostion
10        lstrDisposition = ParseDisposition(pbinChunk)

          ' And Parse the Name
20        lstrName = ParseName(lstrDisposition)

          ' And the file name
30        lstrFileName = ParseFileName(lstrDisposition)

          ' Parse out the Content Type
40        lstrContentType = ParseContentType(pbinChunk)
          
          ' If the content type is not defined, then assume the
          ' field is a normal form field
50        If lstrContentType = "" Then

              ' Parse Binary Data as Unicode
60            lstrValue = CStrU(ParseBinaryData(pbinChunk))
          
          ' Else assume the field is binary data
70        Else
              
              ' Parse Binary Data
80            lbinData = ParseBinaryData(pbinChunk)

90        End If
          
          ' Add a new field
100       Call AddField(lstrName, lstrFileName, lstrContentType, lstrValue, lbinData)
          
End Sub

'********************************************************************************
'* Adds a new clsField object to the field array.
'********************************************************************************
Private Sub AddField( _
    ByRef pstrName As String, _
    ByRef pstrFileName As String, _
    ByRef pstrContentType As String, _
    ByRef pstrValue As String, _
    ByRef pbinData As Variant)

          Dim lobjField As CField      ' Field object class
          
          ' Add a new index to the field array
          ' Make certain not to destroy current fields
10        ReDim Preserve mobjFieldAry(mlngCount)

          ' Create new field object
20        Set lobjField = New CField
          
          ' Set field properties
30        lobjField.Name = pstrName
40        lobjField.FilePath = pstrFileName
50        lobjField.ContentType = pstrContentType

          ' If field is not a binary file
60        If LenB(pbinData) = 0 Then
              
70            lobjField.BinaryData = ChrB(0)
80            lobjField.Value = pstrValue
90            lobjField.Length = Len(pstrValue)

          ' Else field is a binary file
100       Else

110           lobjField.BinaryData = pbinData
120           lobjField.Length = LenB(pbinData)
130           lobjField.Value = ""

140       End If

          ' Set field array index to new field
150       Set mobjFieldAry(mlngCount) = lobjField
          
          ' Incriment field count
160       mlngCount = mlngCount + 1
          
End Sub

'********************************************************************************
'* Parses binary content of the chunk
'********************************************************************************
Private Function ParseBinaryData(ByRef pbinChunk As Variant) As Variant

          Dim llngStart As Long  ' Start Position

          ' Find first occurence of a blank line
10        llngStart = InStrB(1, pbinChunk, CRLF & CRLF)
          
          ' If it doesn't exist, then return nothing
20        If llngStart = 0 Then Exit Function
          
          ' Incriment start to pass carriage returns and line feeds
30        llngStart = llngStart + 4
          
          ' Return the last part of the chunk after the start position
40        ParseBinaryData = MidB(pbinChunk, llngStart)
          
End Function

'********************************************************************************
'* Parses the content type of a binary file.
'* example: image/gif is the content type of a GIF image.
'********************************************************************************
Private Function ParseContentType(ByRef pbinChunk) As String
          
          Dim llngStart As Long   ' Start Position
          Dim llngEnd As Long     ' End Position
          Dim llngLength As Long  ' Length
          
          ' Fid the first occurance of a line starting with Content-Type:
10        llngStart = InStrB(1, pbinChunk, CRLF & CStrB("Content-Type:"), vbTextCompare)
          
          ' If not found, return nothing
20        If llngStart = 0 Then Exit Function
          
          ' Find the end of the line
30        llngEnd = InStrB(llngStart + 15, pbinChunk, CR)
          
          ' If not found, return nothing
40        If llngEnd = 0 Then Exit Function
          
          ' Adjust start position to start after the text "Content-Type:"
50        llngStart = llngStart + 15
          
          ' If the start position is the same or past the end, return nothing
60        If llngStart >= llngEnd Then Exit Function
          
          ' Determine length
70        llngLength = llngEnd - llngStart
          
          ' Pull out content type
          ' Convert to unicode
          ' Trim out whitespace
          ' Return results
80        ParseContentType = Trim(CStrU(MidB(pbinChunk, llngStart, llngLength)))

End Function

'********************************************************************************
'* Parses the content-disposition from a chunk of data
'*
'* Example:
'*
'*   Content-Disposition: form-data: name="File1"; filename="C:\Photo.gif"
'*
'*   Would Return:
'*       form-data: name="File1"; filename="C:\Photo.gif"
'********************************************************************************
Private Function ParseDisposition(ByRef pbinChunk As Variant) As String

          Dim llngStart As Long  ' Start Position
          Dim llngEnd As Long    ' End Position
          Dim llngLength As Long ' Length
          
          ' Find first occurance of a line starting with Content-Disposition:
10        llngStart = InStrB(1, pbinChunk, CRLF & CStrB("Content-Disposition:"), vbTextCompare)
          
          ' If not found, return nothing
20        If llngStart = 0 Then Exit Function
          
          ' Find the end of the line
30        llngEnd = InStrB(llngStart + 22, pbinChunk, CRLF)
          
          ' If not found, return nothing
40        If llngEnd = 0 Then Exit Function
          
          ' Adjust start position to start after the text "Content-Disposition:"
50        llngStart = llngStart + 22
          
          ' If the start position is the same or past the end, return nothing
60        If llngStart >= llngEnd Then Exit Function
          
          ' Determine Length
70        llngLength = llngEnd - llngStart
          
          ' Pull out content disposition
          ' Convert to Unicode
          ' Return Results
80        ParseDisposition = CStrU(MidB(pbinChunk, llngStart, llngLength))

End Function

'********************************************************************************
'* Parses the name of the field from the content disposition
'*
'* Example
'*
'*   form-data: name="File1"; filename="C:\Photo.gif"
'*
'*   Would Return:
'*       File1
'********************************************************************************
Private Function ParseName(ByRef pstrDisposition As String) As String
          
          Dim llngStart As Long  ' Start Position
          Dim llngEnd As Long     ' End Position
          Dim llngLength As Long  ' Length
          
          ' Find first occurance of text name="
10        llngStart = InStr(1, pstrDisposition, "name=""", vbTextCompare)
          
          ' If not found, return nothing
20        If llngStart = 0 Then Exit Function
          
          ' Find the closing quote
30        llngEnd = InStr(llngStart + 6, pstrDisposition, """")
          
          ' If not found, return nothing
40        If llngEnd = 0 Then Exit Function
          
          ' Adjust start position to start after the text name="
50        llngStart = llngStart + 6
          
          ' If the start position is the same or past the end, return nothing
60        If llngStart >= llngEnd Then Exit Function
          
          ' Determine Length
70        llngLength = llngEnd - llngStart
          
          ' Pull out field name
          ' Return results
80        ParseName = Mid(pstrDisposition, llngStart, llngLength)
          
End Function

'********************************************************************************
'* Parses the name of the field from the content disposition
'*
'* Example
'*
'*   form-data: name="File1"; filename="C:\Photo.gif"
'*
'*   Would Return:
'*       C:\Photo.gif
'********************************************************************************
Private Function ParseFileName(ByRef pstrDisposition As String) As String
          
          Dim llngStart As Long   ' Start Position
          Dim llngEnd As Long     ' End Position
          Dim llngLength As Long  ' Length
          
          ' Find first occurance of text filename="
10        llngStart = InStr(1, pstrDisposition, "filename=""", vbTextCompare)
          
          ' If not found, return nothing
20        If llngStart = 0 Then Exit Function
          
          ' Find the closing quote
30        llngEnd = InStr(llngStart + 10, pstrDisposition, """")
          
          ' If not found, return nothing
40        If llngEnd = 0 Then Exit Function
          
          ' Adjust start position to start after the text filename="
50        llngStart = llngStart + 10
          
          ' If the start position is the same of past the end, return nothing
60        If llngStart >= llngEnd Then Exit Function
          
          ' Determine length
70        llngLength = llngEnd - llngStart
          
          ' Pull out file name
          ' Return results
80        ParseFileName = Mid(pstrDisposition, llngStart, llngLength)
          
End Function

'********************************************************************************
'* Returns the number of fields found
'********************************************************************************
Public Property Get Count() As Long
10        Count = mlngCount
End Property

'********************************************************************************
'* Returns the specified field.  Can be an index or field name.
'********************************************************************************
Public Property Get Fields(ByVal pstrName As String) As CField

          Dim llngIndex As Long  ' Index of current field
          
          ' If a number was passed
10        If IsNumeric(pstrName) Then
              
20            llngIndex = CLng(pstrName)
              
              ' If programmer requested an invalid number
30            If llngIndex > mlngCount - 1 Or llngIndex < 0 Then
                  ' Raise an error
40                Call Err.Raise(vbObjectError + 1, "clsUpload.asp", "Object does not exist within the ordinal reference.")
50                Exit Property
60            End If
                  
              ' Return the field class for the index specified
70            Set Fields = mobjFieldAry(pstrName)
80            Exit Property
          
          ' Else a field name was passed
90        Else
          
              ' convert name to lowercase
100           pstrName = LCase(pstrName)
              
              ' Loop through each field
110           For llngIndex = 0 To mlngCount - 1
                  
                  ' If name matches current fields name in lowercase
120               If LCase(mobjFieldAry(llngIndex).Name) = pstrName Then
                      
                      ' Return Field Class
130                   Set Fields = mobjFieldAry(llngIndex)
140                   Exit Property
                      
150               End If
              
160           Next
          
170       End If

          ' If matches were not found, return an empty field
180       Set Fields = New CField
          
      '       ' ERROR ON NonExistant:
      '       ' If matches were not found, raise an error of a non-existent field
      '       Call Err.Raise(vbObjectError + 1, "clsUpload.asp", "Object does not exist within the ordinal reference.")
      '       Exit Property

End Property

'********************************************************************************
'* Converts an ANSI string to Unicode
'* Best used for small strings
'********************************************************************************
Private Function CStrU(ByRef pstrANSI As String) As String
          
          Dim llngLength As Long  ' Length of ANSI string
          Dim llngIndex As Long   ' Current position
          
          ' determine length
10        llngLength = LenB(pstrANSI)
          
          ' Loop through each character
20        For llngIndex = 1 To llngLength
          
              ' Pull out ANSI character
              ' Get Ascii value of ANSI character
              ' Get Unicode Character from Ascii
              ' Append character to results
30            CStrU = CStrU & Chr(AscB(MidB(pstrANSI, llngIndex, 1)))
          
40        Next

End Function

'********************************************************************************
'* Converts a Unicode string to ANSI
'* Best used for small strings
'********************************************************************************
Private Function CStrB(ByRef pstrUnicode As String) As Variant

          Dim llngLength As Long ' Length of ANSI string
          Dim llngIndex As Long   ' Current position
          
          ' determine length
10        llngLength = Len(pstrUnicode)
          
          ' Loop through each character
20        For llngIndex = 1 To llngLength
          
              ' Pull out Unicode character
              ' Get Ascii value of Unicode character
              ' Get ANSI Character from Ascii
              ' Append character to results
30            CStrB = CStrB & ChrB(Asc(Mid(pstrUnicode, llngIndex, 1)))
          
40        Next
          
End Function
