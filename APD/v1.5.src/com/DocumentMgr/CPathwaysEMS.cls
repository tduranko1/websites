VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPathwaysEMS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Class CPathwaysEMS
'*
'* This class is used to go out to our Mitchell Utramate estimating package
'* server and pull (or in the future, push) estimate data from EMS files.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CPathwaysEMS."

'Error codes specific to this class.
Private Enum Error_Codes
    eNull = &H80065000 + &H200
End Enum

'Enum for indexing into the variant array returned from GetEstimateTotals()
Private Enum EstimateTotals
    eGrossTotal = 0
    eGrossBetterment
    eGrossDeductible
    eOtherAdjustments
    eNetTotal
    eGrossRelatedPriorDamage
    eGrossUnrelatedPriorDamage
    eGrossAppearanceAllowance
End Enum

Private mobjSystem As Scripting.FileSystemObject

Private Sub Class_Initialize()
10        Set mobjSystem = New Scripting.FileSystemObject
End Sub

Private Sub Class_Terminate()
10        Set mobjSystem = Nothing
20        TerminateGlobals
End Sub

'********************************************************************************
'* Pulls estimate totals from EMS for selected LynxID.
'* Will look for the most recent export on the server matching this LynxID.
'* Returned variant array can be indexed by the EstimateTotals enum above.
'********************************************************************************
Public Function GetEstimateTotals( _
    ByVal strLynxID As String, _
    ByVal strSuppNo As String, _
    Optional ByVal strEstPackage As String = "Pathways" _
) As Variant

          Const PROC_NAME As String = MODULE_NAME & "GetEstimateTotals: "

          'Objects that require clean-up.
          'Dim objDB As adodb.Database
          Dim objRS As ADODB.Recordset
          
          'Primitives that don't.
          Dim strDataPath As String
          Dim strExportPath As String
          Dim strDBPathwaysEMS As String
          Dim strFilter As String
          
          Dim varReturn() As Variant
          
10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Trace and assert params.
30        If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Started for LynxID = " & strLynxID & " Supplement No = " & strSuppNo
          
40        g_objEvents.Assert CBool(strLynxID <> ""), "No LynxID Passed!"
          
50        ReDim varReturn(eGrossAppearanceAllowance)
          
          'Extract the path to the Mitchell server export directory from the config.xml file.
60        strExportPath = GetConfig("EstimatingPackages/" & strEstPackage & "/ExportPath")
          
          'Extract the Pathways EMS database file name
70        strDBPathwaysEMS = GetConfig("EstimatingPackages/" & strEstPackage & "/DBFileName")
          
80        If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Using Pathways Export path = " & strExportPath & " DBFileName = " & strDBPathwaysEMS
          
90        If mobjSystem.FileExists(strDBPathwaysEMS) Then
100           Set objRS = CreateObject("ADODB.Recordset")
110           objRS.Open mobjSystem.BuildPath(strExportPath, strDBPathwaysEMS), , adOpenDynamic, adLockOptimistic
              
120           If strSuppNo = "" Then
130               strFilter = "LynxID='" & strLynxID & "' and SupplementNo=0"
140           Else
150               strFilter = "LynxID='" & strLynxID & "' and SupplementNo=" & strSuppNo
160           End If
              
170           objRS.Filter = strFilter
180           If Not objRS.EOF Then
                  'Extract the values we require.
190               varReturn(eGrossTotal) = CCur(objRS!RepairTotal)
200               varReturn(eGrossBetterment) = CCur(objRS!Betterment)
210               varReturn(eGrossDeductible) = CCur(objRS!Deductible)
220               varReturn(eNetTotal) = CCur(objRS!NetTotal)
230               varReturn(eGrossRelatedPriorDamage) = CCur(objRS!GrossRPD)
240               varReturn(eGrossUnrelatedPriorDamage) = CCur(objRS!GrossUPD)
250               varReturn(eGrossAppearanceAllowance) = CCur(objRS!GrossAA)
260               varReturn(eOtherAdjustments) = CCur(objRS!OtherAdj)
270           End If
              'Kill DB Objects
280           Set objRS = Nothing
290       End If
          
300       GetEstimateTotals = varReturn
          
      'Error handler localized to data retrieval and validation.
ErrorHandler:

          'Object clean-up
310       Set objRS = Nothing
          'Set objDB = Nothing
          
320       If Err.Number <> 0 Then
330           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
340       End If

End Function



