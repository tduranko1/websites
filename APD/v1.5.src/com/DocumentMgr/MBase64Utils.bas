Attribute VB_Name = "MBase64Utils"
'********************************************************************************
'* Component PartnerDataMgr : Module MBase64Utils
'*
'* Utility methods used with Base64 encoding.
'* Utility methods used with CDATA encoding.
'* Utility methods for text and binary file access.
'* Utility methods for decoding HexBinary data
'*
'* Requires: Microsoft Scripting Runtime
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "MBase64Utils."

'Internal error codes for this module.
Private Enum EventCodes
    eFileDoesNotExist = &H80065000 + &H400
    eHexDataInvalid
End Enum

'********************************************************************************
'* Reads in the passed file and returns a Byte array.
'********************************************************************************
Public Function ReadBinaryDataFromFile(ByVal strFileName As String) As Variant
          Const PROC_NAME As String = MODULE_NAME & "ReadBinaryDataFromFile: "

10        On Error GoTo ErrorHandler

20        If g_blnDebugMode Then g_objEvents.Trace strFileName, PROC_NAME & " executed."
    
30        g_objEvents.Assert strFileName <> "", "File Name param was blank."
    
          Dim intFile As Integer
          Dim lngLen As Long
          Dim arrBytes() As Byte
    
          'Open the file.
40        intFile = FreeFile()
50        Open strFileName For Binary Access Read As intFile
    
          'Read in the binary data.
60        lngLen = FileLen(strFileName)
    
70        If lngLen < 1 Then
80            Err.Raise eFileDoesNotExist, PROC_NAME & "Open", "File not found"
90        End If
    
100       ReDim arrBytes(lngLen - 1)
110       Get intFile, , arrBytes
    
          'Close the file
120       Close intFile
    
          'Return the binary data
130       ReadBinaryDataFromFile = arrBytes
    
ErrorHandler:

140       If Err.Number <> 0 Then
150           Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description & " File Name = " & strFileName
160       End If

End Function

'********************************************************************************
'* Writes the passed Byte Array to the passed file.
'********************************************************************************
Public Sub WriteBinaryDataToFile(ByVal strFileName As String, ByRef arrBuffer() As Byte)
          Const PROC_NAME As String = MODULE_NAME & "WriteBinaryDataToFile: "

          Dim intFile As Integer

10        On Error GoTo ErrorHandler
    
20        If g_blnDebugMode Then g_objEvents.Trace strFileName, PROC_NAME & " executed."
    
30        g_objEvents.Assert strFileName <> "", "File Name param was blank."
    
          'Open the file.
40        intFile = FreeFile()
    
50        Open strFileName For Binary As intFile
    
          'Write the binary data.
60        Put intFile, , arrBuffer
    
          'Close the file.
70        Close intFile
    
ErrorHandler:

80        If Err.Number <> 0 Then
90            Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description & " File Name = " & strFileName
100       End If

End Sub

'********************************************************************************
'* Reads binary data from a Base64 encoded XML DOM element.
'********************************************************************************
Public Function ReadBinaryDataFromDomElement(ByRef objElement As MSXML2.IXMLDOMElement) As Variant
          Const PROC_NAME As String = MODULE_NAME & "ReadBinaryDataFromDomElement: "

10        On Error GoTo ErrorHandler
    
20        If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & " executed."
    
          Dim varType As Variant
    
          'Save the data type for restoration later.
30        varType = objElement.dataType
    
          'Ensure the type is base64.
40        objElement.dataType = "bin.base64"
    
          'Now get the binary data.
50        ReadBinaryDataFromDomElement = objElement.nodeTypedValue
    
          'Restore the data type.
60        If Not IsNull(varType) Then
70            objElement.dataType = varType
80        Else
90            objElement.dataType = ""
100       End If

ErrorHandler:

110       If Err.Number <> 0 Then
120           Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
130       End If

End Function

'********************************************************************************
'* Writes binary data to a Base64 encoded XML DOM element.
'********************************************************************************
Public Sub WriteBinaryDataToDomElement(ByRef objElement As MSXML2.IXMLDOMElement, ByRef varBuffer As Variant)
          Const PROC_NAME As String = MODULE_NAME & "WriteBinaryDataToDomElement: "

10        On Error GoTo ErrorHandler
  
20        If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & " executed."
    
          Dim varType As Variant
    
          'Save the data type for restoration later.
30        varType = objElement.dataType
    
          'Ensure the type is base64.
40        objElement.dataType = "bin.base64"
    
          'Now store the binary data.
50        objElement.nodeTypedValue = varBuffer
  
          'Restore the data type.
60        If Not IsNull(varType) Then
70            objElement.dataType = varType
80        Else
90            objElement.dataType = ""
100       End If
  
ErrorHandler:

110       If Err.Number <> 0 Then
120           Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
130       End If
  
End Sub

'********************************************************************************
'* Reads in a text file from the passed file name and returns a String that
'* contains the entire contents of the file.
'********************************************************************************
Public Function ReadTextDataFromFile(ByVal strFileName As String) As String
          Const PROC_NAME As String = MODULE_NAME & "ReadTextDataFromFile: "

10        On Error GoTo ErrorHandler

20        If g_blnDebugMode Then g_objEvents.Trace strFileName, PROC_NAME & " executed."
    
30        g_objEvents.Assert strFileName <> "", "File Name param was blank."
    
          Dim objSystem As New Scripting.FileSystemObject
          Dim objStream As TextStream
    
40        Set objStream = objSystem.OpenTextFile(strFileName, ForReading)
    
50        ReadTextDataFromFile = objStream.ReadAll()

60        objStream.Close
    
ErrorHandler:

70        Set objStream = Nothing
80        Set objSystem = Nothing

90        If Err.Number <> 0 Then
100           Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description & " File Name = " & strFileName
110       End If

End Function

'********************************************************************************
'* Writes a text file from the passed String and file name.
'********************************************************************************
Public Sub WriteTextDataToFile(ByVal strFileName As String, ByVal strData As String)
          Const PROC_NAME As String = MODULE_NAME & "WriteTextDataToFile: "

10        On Error GoTo ErrorHandler

20        If g_blnDebugMode Then g_objEvents.Trace strFileName, PROC_NAME & " executed."
    
30        g_objEvents.Assert strFileName <> "", "File Name param was blank."

          Dim objSystem As New Scripting.FileSystemObject
          Dim objStream As TextStream

40        Set objStream = objSystem.CreateTextFile(strFileName, True)

50        objStream.WriteLine strData
60        objStream.Close

ErrorHandler:

70        Set objSystem = Nothing
80        Set objStream = Nothing

90        If Err.Number <> 0 Then
100           Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description & " File Name = " & strFileName
110       End If

End Sub

'********************************************************************************
'* Reads a text string from a CDATA element.
'********************************************************************************
Public Function ReadTextFromCDATA(ByRef objCdata As MSXML2.IXMLDOMCDATASection) As String
          Const PROC_NAME As String = MODULE_NAME & "ReadTextFromCDATA: "

10        On Error GoTo ErrorHandler
    
20        If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & " executed."
    
30        g_objEvents.Assert Not (objCdata Is Nothing), "DOM node param was Nothing."
    
40        ReadTextFromCDATA = objCdata.Data

ErrorHandler:

50        If Err.Number <> 0 Then
60            Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
70        End If

End Function

'********************************************************************************
'* Writes the passed string to the passed CDATA element.
'********************************************************************************
Public Sub WriteTextToCDATA(ByRef objCdata As MSXML2.IXMLDOMCDATASection, ByRef strData As String)
          Const PROC_NAME As String = MODULE_NAME & "WriteTextToCDATA: "

10        On Error GoTo ErrorHandler
  
20        If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & " executed."
    
30        g_objEvents.Assert Not (objCdata Is Nothing), "DOM node param was Nothing."
    
40        objCdata.Data = strData
  
ErrorHandler:

50        If Err.Number <> 0 Then
60            Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
70        End If
  
End Sub

'********************************************************************************
'*  Decode a hexbinary
'********************************************************************************
Public Function DecodeHexBin(ByVal HexString As String, ByRef buffer() As Byte) As Long
        Const PROC_NAME As String = MODULE_NAME & "DecodeHexBin: "
        Dim hexValue, HexStringToDecode As String
        Dim intValue, ptr, i, bufferSize As Integer
  
10      On Error GoTo ErrorHandler
  
20      If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & " executed."
  
30      HexStringToDecode = UCase(Trim(HexString))
  
40      g_objEvents.Assert ValidateHexString(HexStringToDecode), "Hex encoded data contains invalid character."
  
        'Determine size of buffer needed.
50      bufferSize = CInt(Len(HexString) / 2)
60      ReDim buffer(bufferSize) As Byte
  
        'Now walk through and perform decoding
70      ptr = 0
80      For i = 1 To Len(HexString) Step 2
90        hexValue = "&H" & Mid(HexString, i, 2) & "&"
100       intValue = CInt(Val(hexValue))
110       buffer(ptr) = intValue
120       ptr = ptr + 1
130     Next i
  
140     DecodeHexBin = bufferSize
  
ErrorHandler:
150     If Err.Number <> 0 Then
160       Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
170     End If
  
  
End Function

'********************************************************************************
'*  Encode a byte buffer into HexBin string
'********************************************************************************
Public Function EncodeHexBin(ByVal bufferSize As Long, ByRef buffer() As Byte) As String
        Const PROC_NAME As String = MODULE_NAME & "EncodeHexBin: "
  
        Dim hexValue As String, HexString As String
        Dim intValue As Integer, ptr As Long
        Dim strTempFile As String
        Dim objStream As New ADODB.Stream
 
          
    
10      On Error GoTo ErrorHandler


20      objStream.Type = adTypeText
30      objStream.Open
  
40      HexString = ""
  
50      For ptr = 0 To bufferSize - 1
60        intValue = buffer(ptr)
70        If intValue <= 15 Then
80          hexValue = "0" & Hex(intValue)
90        Else
100         hexValue = Hex(intValue)
110       End If
120       objStream.WriteText hexValue, adWriteChar
130     Next ptr
140     objStream.Position = 0
150     EncodeHexBin = objStream.ReadText(adReadAll)
160     objStream.Close
ErrorHandler:
170     If Err.Number <> 0 Then
180       Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
190     End If
200     Set objStream = Nothing
  
End Function

Private Function ValidateHexString(ByVal HexString) As Boolean
        Dim ptr As Integer
        Dim intAscVal As Integer
        Dim blnOk As Boolean
  
10      blnOk = True ' Innocent until proven guilt
  
20      For ptr = 1 To Len(HexString)
30        intAscVal = Asc(Mid(HexString, ptr, 1))
40        If (intAscVal < 48 Or intAscVal > 70 Or (intAscVal > 57 And intAscVal < 65)) Then
            ' Not a valid Hex Character
            ' Seems its guilty afterall
50          blnOk = False
60          Exit For
70        End If
80      Next ptr
  
90      ValidateHexString = blnOk
End Function

