VERSION 5.00
Begin VB.Form frmMain 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CCC Pathways Helper"
   ClientHeight    =   3105
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   ControlBox      =   0   'False
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3105
   ScaleWidth      =   4680
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   495
      Left            =   3300
      TabIndex        =   12
      Top             =   2460
      Width           =   1215
   End
   Begin VB.Timer tmrStartApp 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   540
      Top             =   2580
   End
   Begin VB.Timer tmrExecute 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   120
      Top             =   2580
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Height          =   495
      Left            =   360
      TabIndex        =   10
      Top             =   2460
      Width           =   1215
   End
   Begin VB.TextBox txtNetworkPath 
      Height          =   285
      Left            =   1440
      TabIndex        =   9
      Top             =   1695
      Width           =   3135
   End
   Begin VB.TextBox txtLocalPath 
      Height          =   285
      Left            =   1440
      TabIndex        =   8
      Top             =   1335
      Width           =   3135
   End
   Begin VB.ComboBox cmbMode 
      Height          =   315
      ItemData        =   "Form1.frx":058A
      Left            =   1440
      List            =   "Form1.frx":0594
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   960
      Width           =   2775
   End
   Begin VB.CommandButton cmdExecute 
      Caption         =   "Execute"
      Height          =   495
      Left            =   1740
      TabIndex        =   3
      Top             =   2460
      Width           =   1215
   End
   Begin VB.Label lblStatus 
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   2100
      Width           =   4395
   End
   Begin VB.Label Label6 
      Caption         =   "Network Path:"
      Height          =   195
      Left            =   120
      TabIndex        =   7
      Top             =   1740
      Width           =   1275
   End
   Begin VB.Label Label5 
      Caption         =   "Local EMS Path:"
      Height          =   195
      Left            =   120
      TabIndex        =   6
      Top             =   1380
      Width           =   1275
   End
   Begin VB.Label Label4 
      Caption         =   "Mode:"
      Height          =   195
      Left            =   120
      TabIndex        =   4
      Top             =   1020
      Width           =   675
   End
   Begin VB.Label Label3 
      Caption         =   "Copy the local EMS file set to network"
      Height          =   255
      Left            =   180
      TabIndex        =   2
      Top             =   600
      Width           =   4395
   End
   Begin VB.Label Label2 
      Caption         =   "Read the EMS Content and rename the file set with LYNX Id"
      Height          =   255
      Left            =   180
      TabIndex        =   1
      Top             =   360
      Width           =   4395
   End
   Begin VB.Label Label1 
      Caption         =   "CCC Pathways Helper will do the following:"
      Height          =   255
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   4395
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Compare Text

Dim strMode As String
Dim strLocalPath As String
Dim strNetworkPath As String
Dim strArchiveNetworkPath As String
Dim strTmpPath As String
Dim blnStop As Boolean
Dim oFSO As New Scripting.FileSystemObject
Dim m_executeCounter As Integer
Const ExecuteInterval As Integer = 300 ' 5 minutes

Public WithEvents m_frmSysTray As frmSysTray
Attribute m_frmSysTray.VB_VarHelpID = -1

Private Sub cmdClose_Click()
    'm_frmSysTray.showBalloonTip "Hello there", "CCC Pathways EMS Helper", NIIF_INFO, 50
    Me.Hide
End Sub

Private Sub cmdExecute_Click()
    Dim strSourcePath As String
    Dim strAd1FilePath As String
    Dim strAd1List As New Scripting.Dictionary
    Dim Index As Long
    Dim strTmp As String
    Dim dt As Date
    Dim strSource, strDestination As String
    Dim strMessage As String
    Dim lngCount As Long
    
    On Error GoTo errHandler
    
    strTmpPath = oFSO.GetSpecialFolder(2) 'system temp folder
    strSourcePath = oFSO.BuildPath(strLocalPath, "*.ad1")
    
    If blnStop = True Then Exit Sub
    
    cmdSave.Enabled = False
    cmdExecute.Enabled = False
    lngCount = 0
    
    'strMode = "analyst"
    strMode = cmbMode.Text
    strLocalPath = txtLocalPath.Text
    strNetworkPath = txtNetworkPath.Text
    
    If strMode = "Support" Then
        'delete any local files greater than 7 days
        strAd1FilePath = Dir(strSourcePath)
        
        showStatus "Deleting older files.."
        
        While strAd1FilePath <> ""
            strAd1FilePath = oFSO.BuildPath(strLocalPath, strAd1FilePath)
            dt = FileDateTime(strAd1FilePath)
            If Abs(DateDiff("d", dt, Now())) > 7 Then
                On Error Resume Next
                Kill oFSO.BuildPath(strLocalPath, oFSO.GetBaseName(strAd1FilePath) & "*.*")
            End If
            strAd1FilePath = Dir()
        Wend
        
        On Error GoTo errHandler
        
        'copy all the network files to local
        strAd1FilePath = Dir(oFSO.BuildPath(strNetworkPath, "*.*"))
        While strAd1FilePath <> ""
            If blnStop = False Then
                strSource = oFSO.BuildPath(strNetworkPath, strAd1FilePath)
                strDestination = oFSO.BuildPath(strLocalPath, oFSO.GetFileName(strAd1FilePath))
                
                If oFSO.FileExists(strDestination) = False Then
                    showStatus "Copying " & oFSO.GetFileName(strSource)
                    oFSO.CopyFile strSource, strDestination
                End If
                lngCount = lngCount + 1
                
                strAd1FilePath = Dir()
            End If
            DoEvents
        Wend
        m_frmSysTray.showBalloonTip "Synchronized " & CStr(lngCount) & " EMS Files from network.", "CCC Pathways EMS Helper", NIIF_INFO, 50
    Else
        strAd1FilePath = Dir(strSourcePath)
        strAd1List.RemoveAll
        Index = 1
        lngCount = 0
        
        showStatus "Searching..."
        
        While strAd1FilePath <> ""
            strAd1List.Add Index, oFSO.BuildPath(strLocalPath, strAd1FilePath)
            strAd1FilePath = Dir()
            Index = Index + 1
        Wend
        
        For Index = 1 To strAd1List.Count
            If oFSO.FileExists(strAd1List.Item(Index)) Then
                If blnStop = False Then
                    ProcessFile strAd1List.Item(Index)
                    lngCount = lngCount + 1
                End If
            End If
        Next
        
        m_frmSysTray.showBalloonTip "Moved " & CStr(lngCount) & " EMS Files to network.", "CCC Pathways EMS Helper", NIIF_INFO, 50
    End If

errHandler:
    If Err.Number <> 0 Then
        m_frmSysTray.showBalloonTip "Execute: [Line #" & CStr(Erl) & " ] " & Err.Description, "CCC Pathways EMS Helper", NIIF_ERROR, 50
    End If
    
    cmdSave.Enabled = True
    cmdExecute.Enabled = True
    
    showStatus "Done."
    
End Sub

Private Sub cmdSave_Click()
    saveConfig
    Me.Hide
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim strCmd As String
    loadConfig
    
    strCmd = Command
    
    If strCmd <> "" Then
        If strCmd <> strMode Then
            If strCmd <> "Analyst" Then strCmd = "Support"
            strMode = strCmd
        End If
    End If
    
    txtLocalPath.Text = strLocalPath
    txtNetworkPath.Text = strNetworkPath
    
    For i = 0 To cmbMode.ListCount - 1
        If cmbMode.List(i) = strMode Then
            cmbMode.ListIndex = i
            Exit For
        End If
    Next
    
    blnStop = False
    
    tmrStartApp.Enabled = True
    tmrExecute.Interval = 1000 ' execute every 5 minutes
    
    If strCmd <> "" Then
        'tmrExecute.Enabled = True
    End If
End Sub

Private Sub ProcessFile(strAd1File As String)
    Dim dt As Date
    Dim strLynxID As String
    dt = FileDateTime(strAd1File)
    If Abs(DateDiff("n", dt, Now())) > 1 Then
        showStatus "Reading data " & oFSO.GetFileName(strAd1File) & "..."
        strLynxID = GetLynxID(strAd1File)
        showStatus "Moving File set " & oFSO.GetBaseName(strAd1File) & "..."
        MoveFileSet strAd1File, strLynxID
    End If
End Sub

Private Function GetLynxID(strAd1File As String) As String
    Dim con As ADODB.Connection
    Dim rsPathwaysData As ADODB.Recordset
    Dim sLynxID As String
    Dim strTmpFile As String
    Dim strTmpFile2 As String
    Dim strRet As String
    Dim strEnvFile As String
    
    On Error GoTo errHandler
    
    strRet = ""
    
    strTmpFile = oFSO.BuildPath(strTmpPath, "ad1.dbf")
    strTmpFile2 = oFSO.BuildPath(strTmpPath, "env.dbf")
    
    If oFSO.FileExists(strTmpFile) = True Then
        Kill strTmpFile
    End If
    
    If oFSO.FileExists(strTmpFile2) = True Then
        Kill strTmpFile2
    End If
    
    FileCopy strAd1File, strTmpFile
    
    strEnvFile = oFSO.BuildPath(oFSO.GetParentFolderName(strAd1File), oFSO.GetBaseName(strAd1File) & ".env")
    
    If oFSO.FileExists(strEnvFile) Then
        FileCopy strEnvFile, strTmpFile2
    End If
    
    Set con = CreateObject("ADODB.Connection")
    con.Open "Driver={Microsoft dBASE Driver (*.dbf)};" & _
                      "Dbq=" & strTmpPath
    
    Set rsPathwaysData = CreateObject("ADODB.Recordset")
    rsPathwaysData.Open "Select * from ad1.dbf", con, , , adCmdText
    
    If rsPathwaysData.State = adStateOpen Then
        If Not IsNull(rsPathwaysData.fields("CLM_NO").Value) Then
            strRet = rsPathwaysData.fields("CLM_NO").Value
        End If
    End If
    
    rsPathwaysData.Close
    
    If oFSO.FileExists(strEnvFile) Then
        rsPathwaysData.Open "Select * from env.dbf", con, , , adCmdText
        
        If rsPathwaysData.State = adStateOpen Then
            If Not IsNull(rsPathwaysData.fields("SUPP_NO").Value) Then
                strRet = strRet & " " & rsPathwaysData.fields("SUPP_NO").Value
            End If
        End If
    End If
    Set rsPathwaysData = Nothing
    Set con = Nothing

errHandler:
    If Err.Number <> 0 Then
        m_frmSysTray.showBalloonTip "GetLynxID: [Line #" & CStr(Erl) & " ] " & Err.Description, "CCC Pathways EMS Helper", NIIF_ERROR, 50
    End If
    
    GetLynxID = strRet
End Function

Function MoveFileSet(strAd1File As String, strLynxID As String)
    Dim strSourceFile As String
    Dim strSourcePath As String
    Dim strSourceBaseName As String
    Dim strDestinationFile As String
    
    On Error GoTo errHandler
    
    strSourceBaseName = oFSO.GetBaseName(strAd1File)
    strSourcePath = oFSO.BuildPath(oFSO.GetParentFolderName(strAd1File), strSourceBaseName & "*.*")
    strSourceFile = Dir(strSourcePath)
    
    While strSourceFile <> ""
        strSourceFile = oFSO.BuildPath(oFSO.GetParentFolderName(strAd1File), strSourceBaseName & "." & oFSO.GetExtensionName(strSourceFile))
        strDestinationFile = oFSO.BuildPath(strNetworkPath, strLynxID & "." & oFSO.GetExtensionName(strSourceFile))
        If oFSO.FileExists(strDestinationFile) Then oFSO.DeleteFile strDestinationFile, True
        oFSO.MoveFile strSourceFile, strDestinationFile
        strSourceFile = Dir()
    Wend
    

errHandler:
    If Err.Number <> 0 Then
        m_frmSysTray.showBalloonTip "MoveFileset: [Line #" & CStr(Erl) & " ] " & Err.Description, "CCC Pathways EMS Helper", NIIF_ERROR, 50
    End If
    
End Function

Private Sub loadConfig()
    Dim xmlDoc As New MSXML2.DOMDocument
    Dim objNode As IXMLDOMElement
    
    xmlDoc.async = False
    xmlDoc.Load App.Path & "\config.xml"
    
    Set objNode = xmlDoc.selectSingleNode("//Mode")
    If Not objNode Is Nothing Then
        strMode = objNode.Text
    End If

    Set objNode = xmlDoc.selectSingleNode("//EMSOut")
    If Not objNode Is Nothing Then
        strLocalPath = objNode.Text
    End If

    Set objNode = xmlDoc.selectSingleNode("//Network")
    If Not objNode Is Nothing Then
        strNetworkPath = objNode.Text
    End If
    
    Set objNode = xmlDoc.selectSingleNode("//Archive")
    If Not objNode Is Nothing Then
        strArchiveNetworkPath = objNode.Text
    End If
End Sub


Private Sub saveConfig()
    Dim xmlDoc As New MSXML2.DOMDocument
    Dim objNode As IXMLDOMElement
    
    xmlDoc.async = False
    xmlDoc.Load App.Path & "\config.xml"
    
    Set objNode = xmlDoc.selectSingleNode("//Mode")
    If Not objNode Is Nothing Then
        objNode.Text = cmbMode.Text
    End If

    Set objNode = xmlDoc.selectSingleNode("//EMSOut")
    If Not objNode Is Nothing Then
        objNode.Text = txtLocalPath.Text
    End If

    Set objNode = xmlDoc.selectSingleNode("//Network")
    If Not objNode Is Nothing Then
        objNode.Text = txtNetworkPath.Text
    End If
    
    xmlDoc.save App.Path & "\config.xml"
End Sub

Private Sub showStatus(strMessage As String)
    lblStatus.Caption = strMessage
    lblStatus.Refresh
    DoEvents
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    blnStop = True
End Sub

Private Sub tmrExecute_Timer()
    m_executeCounter = m_executeCounter - 1
    If m_executeCounter = 0 Then
        tmrExecute.Enabled = False
        cmdExecute_Click
        m_executeCounter = ExecuteInterval
        tmrExecute.Enabled = True
    End If
End Sub

Private Sub tmrStartApp_Timer()
    tmrStartApp.Enabled = False
    Set m_frmSysTray = New frmSysTray
    With m_frmSysTray
        .AddMenuItem "&View Settings", "open", True
        .AddMenuItem "-"
        .AddMenuItem "&Exit", "exit"
        .ToolTip = "CCC Pathways EMS Helper"
        .IconHandle = Me.Icon.Handle
    End With
              
    Me.Hide
    
    m_executeCounter = ExecuteInterval
    tmrExecute.Enabled = True
    
End Sub

Private Sub m_frmSysTray_MenuClick(ByVal lIndex As Long, ByVal sKey As String)
10        On Error GoTo errHandler
          Dim i As Integer
20        Select Case sKey
              Case "open"
                  'If Not frmShutDownConfirm.Visible Then
30                Me.Show
40            Case "main"
50                Me.Show
80            Case "exit"
                  'LockComputer
90                blnStop = True
140               For i = Forms.Count - 1 To 1 Step -1
150                   Unload Forms(i)
160               Next
170               Unload Me
180               End
190       End Select
200       Exit Sub
errHandler:
End Sub

Private Sub m_frmSysTray_SysTrayDoubleClick(ByVal eButton As MouseButtonConstants)
    'If Not frmShutDownConfirm.Visible Then
    Me.Show
End Sub

Private Sub m_frmSysTray_SysTrayMouseDown(ByVal eButton As MouseButtonConstants)
10        If (eButton = vbRightButton) Then
20            m_frmSysTray.ShowMenu
30        End If
End Sub


Private Sub showError(strMessage As String)
    If Not m_frmSysTray Is Nothing Then
        m_frmSysTray.showBalloonTip strMessage, "CCC Pathways EMS Helper", NIIF_ERROR
    End If
End Sub
