VERSION 5.00
Begin VB.Form frmSysTray 
   Caption         =   "Sys Tray Interface"
   ClientHeight    =   1920
   ClientLeft      =   5610
   ClientTop       =   3360
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   1920
   ScaleWidth      =   4680
   Begin VB.Menu mnuPopup 
      Caption         =   "&Popup"
      Begin VB.Menu mnuSysTray 
         Caption         =   ""
         Index           =   0
      End
   End
End
Attribute VB_Name = "frmSysTray"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Const MODULE_NAME As String = "frmSysTray::"

Private Declare Function Shell_NotifyIconA Lib "shell32.dll" _
   (ByVal dwMessage As Long, lpData As NOTIFYICONDATAA) As Long
   
Private Declare Function Shell_NotifyIconW Lib "shell32.dll" _
   (ByVal dwMessage As Long, lpData As NOTIFYICONDATAW) As Long


Private Const NIF_ICON = &H2
Private Const NIF_MESSAGE = &H1
Private Const NIF_TIP = &H4
Private Const NIF_STATE = &H8
Private Const NIF_INFO = &H10

Private Const NIM_ADD = &H0
Private Const NIM_MODIFY = &H1
Private Const NIM_DELETE = &H2
Private Const NIM_SETFOCUS = &H3
Private Const NIM_SETVERSION = &H4

Private Const NOTIFYICON_VERSION = 3

Private Type NOTIFYICONDATAA
   cbSize As Long             ' 4
   hwnd As Long               ' 8
   uID As Long                ' 12
   uFlags As Long             ' 16
   uCallbackMessage As Long   ' 20
   hIcon As Long              ' 24
   szTip As String * 128      ' 152
   dwState As Long            ' 156
   dwStateMask As Long        ' 160
   szInfo As String * 256     ' 416
   uTimeOutOrVersion As Long  ' 420
   szInfoTitle As String * 64 ' 484
   dwInfoFlags As Long        ' 488
   guidItem As Long           ' 492
End Type
Private Type NOTIFYICONDATAW
   cbSize As Long             ' 4
   hwnd As Long               ' 8
   uID As Long                ' 12
   uFlags As Long             ' 16
   uCallbackMessage As Long   ' 20
   hIcon As Long              ' 24
   szTip(0 To 255) As Byte    ' 280
   dwState As Long            ' 284
   dwStateMask As Long        ' 288
   szInfo(0 To 511) As Byte   ' 800
   uTimeOutOrVersion As Long  ' 804
   szInfoTitle(0 To 127) As Byte ' 932
   dwInfoFlags As Long        ' 936
   guidItem As Long           ' 940
End Type


Private nfIconDataA As NOTIFYICONDATAA
Private nfIconDataW As NOTIFYICONDATAW

Private Const NOTIFYICONDATAA_V1_SIZE_A = 88
Private Const NOTIFYICONDATAA_V1_SIZE_U = 152
Private Const NOTIFYICONDATAA_V2_SIZE_A = 488
Private Const NOTIFYICONDATAA_V2_SIZE_U = 936

Private Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long

Private Const WM_MOUSEMOVE = &H200
Private Const WM_LBUTTONDBLCLK = &H203
Private Const WM_LBUTTONDOWN = &H201
Private Const WM_LBUTTONUP = &H202
Private Const WM_RBUTTONDBLCLK = &H206
Private Const WM_RBUTTONDOWN = &H204
Private Const WM_RBUTTONUP = &H205

Private Const WM_USER = &H400

Private Const NIN_SELECT = WM_USER
Private Const NINF_KEY = &H1
Private Const NIN_KEYSELECT = (NIN_SELECT Or NINF_KEY)
Private Const NIN_BALLOONSHOW = (WM_USER + 2)
Private Const NIN_BALLOONHIDE = (WM_USER + 3)
Private Const NIN_BALLOONTIMEOUT = (WM_USER + 4)
Private Const NIN_BALLOONUSERCLICK = (WM_USER + 5)

' Version detection:
Private Declare Function GetVersion Lib "kernel32" () As Long

Public Event SysTrayMouseDown(ByVal eButton As MouseButtonConstants)
Public Event SysTrayMouseUp(ByVal eButton As MouseButtonConstants)
Public Event SysTrayMouseMove()
Public Event SysTrayDoubleClick(ByVal eButton As MouseButtonConstants)
Public Event MenuClick(ByVal lIndex As Long, ByVal sKey As String)
Public Event BalloonShow()
Public Event BalloonHide()
Public Event BalloonTimeOut()
Public Event BalloonClicked()

Public Enum EBalloonIconTypes
   NIIF_NONE = 0
   NIIF_INFO = 1
   NIIF_WARNING = 2
   NIIF_ERROR = 3
   NIIF_NOSOUND = &H10
End Enum

Private m_bAddedMenuItem As Boolean
Private m_iDefaultIndex As Long

Private m_bUseUnicode As Boolean
Private m_bSupportsNewVersion As Boolean

Public Sub showBalloonTip( _
      ByVal sMessage As String, _
      Optional ByVal sTitle As String, _
      Optional ByVal eIcon As EBalloonIconTypes, _
      Optional ByVal lTimeOutMs = 30000 _
   )
      Dim lR As Long
10       If (m_bSupportsNewVersion) Then
20          If (m_bUseUnicode) Then
30             stringToArray sMessage, nfIconDataW.szInfo, 512
40             stringToArray sTitle, nfIconDataW.szInfoTitle, 128
50             nfIconDataW.uTimeOutOrVersion = lTimeOutMs
60             nfIconDataW.dwInfoFlags = eIcon
70             nfIconDataW.uFlags = NIF_INFO
80             lR = Shell_NotifyIconW(NIM_MODIFY, nfIconDataW)
90          Else
100            nfIconDataA.szInfo = sMessage
110            nfIconDataA.szInfoTitle = sTitle
120            nfIconDataA.uTimeOutOrVersion = lTimeOutMs
130            nfIconDataA.dwInfoFlags = eIcon
140            nfIconDataA.uFlags = NIF_INFO
150            lR = Shell_NotifyIconA(NIM_MODIFY, nfIconDataA)
160         End If
170      Else
            ' can't do it, fail silently.
180      End If
End Sub

Public Property Get ToolTip() As String
      Dim sTip As String
      Dim iPos As Long
10        sTip = nfIconDataA.szTip
20        iPos = InStr(sTip, Chr$(0))
30        If (iPos <> 0) Then
40            sTip = Left$(sTip, iPos - 1)
50        End If
60        ToolTip = sTip
End Property
Public Property Let ToolTip(ByVal sTip As String)
10       If (m_bUseUnicode) Then
20          stringToArray sTip, nfIconDataW.szTip, unicodeSize(IIf(m_bSupportsNewVersion, 128, 64))
30          nfIconDataW.uFlags = NIF_TIP
40          Shell_NotifyIconW NIM_MODIFY, nfIconDataW
50       Else
60          If (sTip & Chr$(0) <> nfIconDataA.szTip) Then
70             nfIconDataA.szTip = sTip & Chr$(0)
80             nfIconDataA.uFlags = NIF_TIP
90             Shell_NotifyIconA NIM_MODIFY, nfIconDataA
100         End If
110      End If
End Property

Public Property Get IconHandle() As Long
10        IconHandle = nfIconDataA.hIcon
End Property
Public Property Let IconHandle(ByVal hIcon As Long)
10       If (m_bUseUnicode) Then
20          If (hIcon <> nfIconDataW.hIcon) Then
30             nfIconDataW.hIcon = hIcon
40             nfIconDataW.uFlags = NIF_ICON
50             Shell_NotifyIconW NIM_MODIFY, nfIconDataW
60          End If
70       Else
80          If (hIcon <> nfIconDataA.hIcon) Then
90             nfIconDataA.hIcon = hIcon
100            nfIconDataA.uFlags = NIF_ICON
110            Shell_NotifyIconA NIM_MODIFY, nfIconDataA
120         End If
130      End If
End Property

Public Function AddMenuItem(ByVal sCaption As String, Optional ByVal sKey As String = "", Optional ByVal bDefault As Boolean = False) As Long
      Dim iIndex As Long
10        If Not (m_bAddedMenuItem) Then
20            iIndex = 0
30            m_bAddedMenuItem = True
40        Else
50            iIndex = mnuSysTray.UBound + 1
60            Load mnuSysTray(iIndex)
70        End If
80        mnuSysTray(iIndex).Visible = True
90        mnuSysTray(iIndex).Tag = sKey
100       mnuSysTray(iIndex).Caption = sCaption
110       If (bDefault) Then
120           m_iDefaultIndex = iIndex
130       End If
140       AddMenuItem = iIndex
End Function

Private Function ValidIndex(ByVal lIndex As Long) As Boolean
10        ValidIndex = (lIndex >= mnuSysTray.LBound And lIndex <= mnuSysTray.UBound)
End Function

Public Sub EnableMenuItem(ByVal lIndex As Long, ByVal bState As Boolean)
10        If (ValidIndex(lIndex)) Then
20            mnuSysTray(lIndex).Enabled = bState
30        End If
End Sub

Public Function RemoveMenuItem(ByVal iIndex As Long) As Long
      Dim i As Long
10       If ValidIndex(iIndex) Then
20          If (iIndex = 0) Then
30             mnuSysTray(0).Caption = ""
40          Else
               ' remove the item:
50             For i = iIndex + 1 To mnuSysTray.UBound
60                mnuSysTray(iIndex - 1).Caption = mnuSysTray(iIndex).Caption
70                mnuSysTray(iIndex - 1).Tag = mnuSysTray(iIndex).Tag
80             Next i
90             Unload mnuSysTray(mnuSysTray.UBound)
100         End If
110      End If
End Function

Public Property Get DefaultMenuIndex() As Long
10       DefaultMenuIndex = m_iDefaultIndex
End Property

Public Property Let DefaultMenuIndex(ByVal lIndex As Long)
10       If (ValidIndex(lIndex)) Then
20          m_iDefaultIndex = lIndex
30       Else
40          m_iDefaultIndex = 0
50       End If
End Property

Public Function ShowMenu()
10       SetForegroundWindow Me.hwnd
20       If (m_iDefaultIndex > -1) Then
30          Me.PopupMenu mnuPopup, 0, , , mnuSysTray(m_iDefaultIndex)
40       Else
50          Me.PopupMenu mnuPopup, 0
60       End If
End Function

Private Sub Form_Load()
         ' Get version:
         Dim lMajor As Long
         Dim lMinor As Long
         Dim bIsNt As Long
10       GetWindowsVersion lMajor, lMinor, , , bIsNt

20       If (bIsNt) Then
30          m_bUseUnicode = True
40          If (lMajor >= 5) Then
               ' 2000 or XP
50             m_bSupportsNewVersion = True
60          End If
70       ElseIf (lMajor = 4) And (lMinor = 90) Then
            ' Windows ME
80          m_bSupportsNewVersion = True
90       End If
         
         
         'Add the icon to the system tray...
         Dim lR As Long
         
100      If (m_bUseUnicode) Then
110         With nfIconDataW
120            .hwnd = Me.hwnd
130            .uID = Me.Icon
140            .uFlags = NIF_ICON Or NIF_MESSAGE Or NIF_TIP
150            .uCallbackMessage = WM_MOUSEMOVE
160            .hIcon = Me.Icon.Handle
170            stringToArray App.FileDescription, .szTip, unicodeSize(IIf(m_bSupportsNewVersion, 128, 64))
180            If (m_bSupportsNewVersion) Then
190               .uTimeOutOrVersion = NOTIFYICON_VERSION
200            End If
210            .cbSize = nfStructureSize
220         End With
230         lR = Shell_NotifyIconW(NIM_ADD, nfIconDataW)
240         If (m_bSupportsNewVersion) Then
250            Shell_NotifyIconW NIM_SETVERSION, nfIconDataW
260         End If
270      Else
280         With nfIconDataA
290            .hwnd = Me.hwnd
300            .uID = Me.Icon
310            .uFlags = NIF_ICON Or NIF_MESSAGE Or NIF_TIP
320            .uCallbackMessage = WM_MOUSEMOVE
330            .hIcon = Me.Icon.Handle
340            .szTip = App.FileDescription & Chr$(0)
350            If (m_bSupportsNewVersion) Then
360               .uTimeOutOrVersion = NOTIFYICON_VERSION
370            End If
380            .cbSize = nfStructureSize
390         End With
400         lR = Shell_NotifyIconA(NIM_ADD, nfIconDataA)
410         If (m_bSupportsNewVersion) Then
420            lR = Shell_NotifyIconA(NIM_SETVERSION, nfIconDataA)
430         End If
440      End If
         
End Sub

Private Sub stringToArray( _
      ByVal sString As String, _
      bArray() As Byte, _
      ByVal lMaxSize As Long _
   )
      Dim b() As Byte
      Dim i As Long
      Dim j As Long
10       If Len(sString) > 0 Then
20          b = sString
30          For i = LBound(b) To UBound(b)
40             bArray(i) = b(i)
50             If (i = (lMaxSize - 2)) Then
60                Exit For
70             End If
80          Next i
90          For j = i To lMaxSize - 1
100            bArray(j) = 0
110         Next j
120      End If
End Sub
Private Function unicodeSize(ByVal lSize As Long) As Long
10       If (m_bUseUnicode) Then
20          unicodeSize = lSize * 2
30       Else
40          unicodeSize = lSize
50       End If
End Function

Private Property Get nfStructureSize() As Long
10       If (m_bSupportsNewVersion) Then
20          If (m_bUseUnicode) Then
30             nfStructureSize = NOTIFYICONDATAA_V2_SIZE_U
40          Else
50             nfStructureSize = NOTIFYICONDATAA_V2_SIZE_A
60          End If
70       Else
80          If (m_bUseUnicode) Then
90             nfStructureSize = NOTIFYICONDATAA_V1_SIZE_U
100         Else
110            nfStructureSize = NOTIFYICONDATAA_V1_SIZE_A
120         End If
130      End If
End Property

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
      Dim lX As Long
         ' VB manipulates the x value according to scale mode:
         ' we must remove this before we can interpret the
         ' message windows was trying to send to us:
10       lX = ScaleX(x, Me.ScaleMode, vbPixels)
20       Select Case lX
         Case WM_MOUSEMOVE
30          RaiseEvent SysTrayMouseMove
40       Case WM_LBUTTONUP
50          RaiseEvent SysTrayMouseDown(vbLeftButton)
60       Case WM_LBUTTONUP
70          RaiseEvent SysTrayMouseUp(vbLeftButton)
80       Case WM_LBUTTONDBLCLK
90          RaiseEvent SysTrayDoubleClick(vbLeftButton)
100      Case WM_RBUTTONDOWN
110         RaiseEvent SysTrayMouseDown(vbRightButton)
120      Case WM_RBUTTONUP
130         RaiseEvent SysTrayMouseUp(vbRightButton)
140      Case WM_RBUTTONDBLCLK
150         RaiseEvent SysTrayDoubleClick(vbRightButton)
160      Case NIN_BALLOONSHOW
170         RaiseEvent BalloonShow
180      Case NIN_BALLOONHIDE
190         RaiseEvent BalloonHide
200      Case NIN_BALLOONTIMEOUT
210         RaiseEvent BalloonTimeOut
220      Case NIN_BALLOONUSERCLICK
230         RaiseEvent BalloonClicked
240      End Select

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
10       If (m_bUseUnicode) Then
20          Shell_NotifyIconW NIM_DELETE, nfIconDataW
30       Else
40          Shell_NotifyIconA NIM_DELETE, nfIconDataA
50       End If
End Sub

Private Sub mnuSysTray_Click(Index As Integer)
10       RaiseEvent MenuClick(Index, mnuSysTray(Index).Tag)
End Sub

Private Sub GetWindowsVersion( _
      Optional ByRef lMajor = 0, _
      Optional ByRef lMinor = 0, _
      Optional ByRef lRevision = 0, _
      Optional ByRef lBuildNumber = 0, _
      Optional ByRef bIsNt = False _
   )
      Dim lR As Long
10       lR = GetVersion()
20       lBuildNumber = (lR And &H7F000000) \ &H1000000
30       If (lR And &H80000000) Then lBuildNumber = lBuildNumber Or &H80
40       lRevision = (lR And &HFF0000) \ &H10000
50       lMinor = (lR And &HFF00&) \ &H100
60       lMajor = (lR And &HFF)
70       bIsNt = ((lR And &H80000000) = 0)
End Sub


