Attribute VB_Name = "MEventLoggers"
'********************************************************************************
'* Component SiteUtilities : Module MEventLoggers
'*
'* Utility methods for logging events.
'*
'* Note that these methods have been modified to no longer raise errors
'* to the caller.  This protects from logging conflicts (such as developers
'* peeking in to the log files) from bubbling up to the users.  However,
'* they do still log to the NT event log so errors will still become apparent.
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "MEventLoggers."

Declare Function timeGetTime Lib "winmm.dll" () As Long

'********************************************************************
'* Util methods to prevent calls to this module from modifying
'* any error information stored in the Err object.
'********************************************************************
Type typError
    Number As Long
    Description As String
    Source As String
End Type

Public Sub PushErr(ByRef Error As typError)
10        Error.Number = Err.Number
20        Error.Description = Err.Description
30        Error.Source = Err.Source
End Sub

Public Sub PopErr(ByRef Error As typError)
10        Err.Number = Error.Number
20        Err.Description = Error.Description
30        Err.Source = Error.Source
End Sub

'********************************************************************
'* Syntax:  LogNTEvent strError, eLogEventTypeError
'* Params:  strMsg - detailed description of the error
'*          intEventType - event type (error, warning, info)
'* Purpose: Logs an event to the local machine NT log.
'* Returns: Nothing
'* NOTE:    When debugging SiteUtilities in VB, events that come
'*          through here do not actually make it to the event log.
'********************************************************************
Public Sub LogNTEvent(ByVal strMsg As String, ByVal intEventType As Integer)

          Dim Error As typError
10        PushErr Error
              
20        On Error Resume Next
          
          Dim intWinVersion As Integer
30        intWinVersion = GetWinVersion
         
40        Select Case intWinVersion
              Case WinNt40, Win2000
50                App.StartLogging "", vbLogToNT
60                App.LogEvent strMsg, intEventType
70        End Select
          
80        PopErr Error
          
End Sub

'********************************************************************
'* Syntax:  LogToFile strMsg, strFileName
'* Params:  strMsg - detailed description of the error.
'*          strFileName - file to append to.
'* Purpose: Logs an event to a log file.
'* Returns: Nothing
'********************************************************************
Public Sub LogToFile(ByVal strMsg As String, ByVal strFileName As String, Optional ByVal strSource As String)
          Const PROC_NAME As String = MODULE_NAME & "LogToFile: "
          
          Dim Error As typError
10        PushErr Error
          
          Dim objSystem As Scripting.FileSystemObject
          Dim objText As Scripting.TextStream
          
20        On Error GoTo ErrorHandler
          
          'Open/Create the text file to log to.
          'Note that we append to the file if it already exists.
30        Set objSystem = New Scripting.FileSystemObject
          
40        If objSystem.FileExists(strFileName) Then
50            Set objText = objSystem.OpenTextFile(strFileName, ForAppending)
60        Else
70            Set objText = objSystem.CreateTextFile(strFileName, False)
80        End If
          
          'Uset timeGetTime to get the milliseconds since the app was started.
          Dim lngTime As Long
90        lngTime = timeGetTime()
          
100       If Len(strSource) = 0 Then
110           strSource = App.EXEName
120       End If
          
          'Build and write log string.
130       objText.WriteLine vbCrLf & "[" & CStr(lngTime) & " ms] " & strSource
          
140       If Len(strMsg) > 0 Then
150           objText.WriteLine "  " & strMsg
160       End If
          
170       objText.Close
          
ErrorHandler:

180       Set objText = Nothing
190       Set objSystem = Nothing
          
200       If Err.Number <> 0 Then
210           LogNTEvent PROC_NAME & " Error: " & Err.Description, eLogEventTypeError
              'Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
220       End If
          
230       PopErr Error
          
End Sub

'********************************************************************
'* Syntax:  SendEmail "from@this.com", "to@this.com", "subject", "email body"
'* Params:  strFrom - email address to put in the from field.
'*          strTo - email address to send the email to
'*          strSubject - subject of the email
'*          strBody - email body
'*          blnAsHTML - specifies body contents as text or html
'*          enmEmailImportance - specifies importance level of email
'* Purpose: Sends an email using CDO.
'* Returns: Nothing
'* Notes:   Requires Microsoft CDO for NTS 1.2
'********************************************************************
Public Sub SendEmail( _
    ByVal strFrom As String, ByVal strTo As String, _
    ByVal strSubject As String, ByVal strBody As String, _
    Optional ByVal blnAsHTML As Boolean = False, _
    Optional ByVal intImportance As Integer = 1)
          
          Const PROC_NAME As String = MODULE_NAME & "SendEmail: "
          
          Dim Error As typError
10        PushErr Error
          
          Dim objMail As CDONTS.NewMail
          
20        On Error GoTo ErrorHandler
          
30        Set objMail = New CDONTS.NewMail
          
40        objMail.Subject = strSubject
50        objMail.To = strTo
60        objMail.From = strFrom
          
          'Init as text or HTML based upon boolean.
70        If (blnAsHTML = True) Then
80            objMail.MailFormat = CdoMailFormatMime
90            objMail.BodyFormat = CdoBodyFormatHTML
100       Else
110           objMail.MailFormat = CdoMailFormatText
120           objMail.BodyFormat = CdoBodyFormatText
130       End If
          
140       objMail.Body = strBody
150       objMail.Importance = intImportance
160       objMail.send
          
ErrorHandler:

170       Set objMail = Nothing
              
180       If Err.Number <> 0 Then
190           LogNTEvent PROC_NAME & " Error: " & Err.Description, eLogEventTypeError
              'Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
200       End If
          
210       PopErr Error
          
End Sub

'********************************************************************
'* Syntax:  PostMsmqMessage ".\$Private\apd_events", "label", "message body"
'* Params:  strQueuePath - MSMQ queue identifier
'*          strLabel - Message label
'*          strBody - Message body
'* Purpose: Posts an MSMQ message to a queue
'* Returns: Nothing
'* Notes:   Requires Miscrosoft Message Queue 2.0 Object library
'********************************************************************
Public Sub PostMsmqMessage( _
    ByVal strQueuePath As String, _
    ByVal strLabel As String, _
    ByVal strBody As String)

          Const PROC_NAME As String = MODULE_NAME & "PostMsmqMessage: "
          
          Dim Error As typError
10        PushErr Error
          
          Dim objQueue As Object 'MSMQQueue
          Dim objMessage As Object 'MSMQMessage
          Dim objQueueInfo As Object 'MSMQQueueInfo
              
20        On Error GoTo ErrorHandler
          
30        Set objQueueInfo = CreateObject("MSMQ.MSMQQueueInfo") 'New MSMQQueueInfo
          
40        objQueueInfo.PathName = strQueuePath

50        Set objQueue = objQueueInfo.Open(2, 0) ' MQ_SEND_ACCESS, MQ_DENY_NONE)
60        Set objMessage = CreateObject("MSMQ.MSMQMessage") 'New MSMQ.MSMQMessage
          
70        objMessage.Label = strLabel
80        objMessage.Body = strBody
90        objMessage.Delivery = 1 'MQMSG_DELIVERY_RECOVERABLE
100       objMessage.send objQueue, 3 'MQ_SINGLE_MESSAGE
          
110       objQueue.Close
          
ErrorHandler:

120       Set objQueueInfo = Nothing
130       Set objQueue = Nothing
140       Set objMessage = Nothing
          
150       If Err.Number <> 0 Then
160           LogNTEvent PROC_NAME & " Error: " & Err.Description, eLogEventTypeError
              'Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
170       End If
          
180       PopErr Error
          
End Sub
    




