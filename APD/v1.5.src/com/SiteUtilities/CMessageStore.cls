VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMessageStore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Component SiteUtilities : Class CMessageStore
'*
'* This class is used to extract and format internal and external messages
'* from an event message xml file (config/error.xml).
'*
'* It can be used in conjunction with CXmlCache for efficient DOM access.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CMessageStore."

'Loggable event types
Public Enum enmNTEventTypes
    eLogEventTypeError = vbLogEventTypeError    '1
    eLogEventTypeWarning = vbLogEventTypeWarning    '2
    eLogEventTypeInformation = vbLogEventTypeInformation    '4
    eLogEventTypeNull = 6   'Picked six so could do numeric compares of these
End Enum

'Member variables
Private mstrXmlFile As String
Private mdomErrors As MSXML2.DOMDocument40
Private mobjXmlCache As CXmlCache
Private mintInitCount As Integer

'Internal error codes for this class.
Private Enum ErrorCodes
    eDOMDocument40NotSet = SiteUtilities_FirstError + &H100
    eNoRangeElements
    eInvalidRange
    eNoDefaultEventFound
    eBadEventSeverity
    eMethodDepracated
End Enum

Private Sub Class_Terminate()
10        Set mdomErrors = Nothing
End Sub

'********************************************************************************
'* Syntax:      objMsgStore.SetDocument( domErrors )
'* Parameters:  domErrors the DOMDocument40
'* Purpose:     Initializes this object with a DOM document.
'* Returns:     Nothing
'********************************************************************************
Public Sub SetDocument(ByRef domErrors As Object)
10        Err.Raise eMethodDepracated, MODULE_NAME & "SetDocument()", "Method no longer allowed."
End Sub

'********************************************************************************
'* Syntax:      If objMsgStore.HasDocument() Then
'* Parameters:  None
'* Purpose:     Determines if the document has been set.
'* Returns:     True if the document has been set.
'********************************************************************************
Public Function HasDocument() As Boolean
10        HasDocument = CBool(Len(mstrXmlFile) > 4)
End Function

'********************************************************************************
'* Internal function used to check that SetDocument was called.
'********************************************************************************
Private Sub CheckDocument()
10        If Not HasDocument() Then
20            Err.Raise eDOMDocument40NotSet, MODULE_NAME & "CheckDocument()", MODULE_NAME & "SetDocument() not called properly for CMessageStore.  Initialization issue?"
30        End If
End Sub

'********************************************************************************
'* Syntax:      objMsgStore.ExtractMessage( Err.Number, Err.Description, strInternal, strExternal )
'* Parameters:  intErrCode - the error to lookup.
'*              strErrDesc - the error description to parse.
'*              strInternal - the extracted internal message.
'*              strExternal - the extracted external message.
'* Purpose:     Extracts the internal and external messages for the passed error.
'* Returns:     Nothing.
'********************************************************************************
Public Sub ExtractMessage( _
    ByVal lngErrCode As Long, _
    ByVal strErrDesc As String, _
    ByRef strInternal As String, _
    ByRef strExternal As String, _
    ByRef intEventType As Integer)

          Const PROC_NAME As String = MODULE_NAME & "ExtractMessage: "

          Dim objRangeList As IXMLDOMNodeList
          Dim objRangeNode As IXMLDOMNode
          Dim objEventNode As IXMLDOMElement

          Dim strRangeName As String, strRangeStart As String, strRangeEnd As String
          Dim strEventPath As String, strSeverity As String

          Dim lngStart As Long, lngEnd As Long
          Dim lngAdjCode As Long

10        On Error GoTo ErrorHandler

20        CaptureDocument

          'Get a list of ranges to loop through.
30        Set objRangeList = mdomErrors.selectNodes("Root/Range")

40        If objRangeList.length = 0 Then
50            Err.Raise eNoRangeElements, PROC_NAME & "selectNodes()", "Found no range nodes in the message store DOM document."
60        End If

          'Loop through the ranges and find one that matches the passed error code.
70        For Each objRangeNode In objRangeList

              'Get the name, start and end attributes for this range.
80            strRangeName = GetChildNodeText(objRangeNode, "@name", True)
90            strRangeStart = GetChildNodeText(objRangeNode, "@start", True)
100           strRangeEnd = GetChildNodeText(objRangeNode, "@end", True)

              'Hex values begin with 'H'.  Add the '&' since this is an special character in XML.
110           If Left(strRangeStart, 1) = "H" Then
120               strRangeStart = "&" & strRangeStart
130           End If
140           If Left(strRangeEnd, 1) = "H" Then
150               strRangeEnd = "&" & strRangeEnd
160           End If

              'Get decimal values for the range start and end and validate them.
170           lngStart = CDec(strRangeStart)
180           lngEnd = CDec(strRangeEnd)

              'Check to see if our error falls into this range.
              'Note that since we start at vbObjectError we are talking negative numbers.
190           If lngErrCode >= lngStart And lngErrCode <= lngEnd Then

                  'For ease in developing, the individual event Codes
                  'within a range are offset from the start of that range.
                  'Adjust the error Code accordingly.
200               lngAdjCode = lngErrCode - lngStart

                  'Create a path to that individual event code.
210               strEventPath = "Root/Range[@name='" & strRangeName & "']/Event[@code='" & lngAdjCode & "']"

                  'Try to find that event in the DOM.
220               Set objEventNode = mdomErrors.selectSingleNode(strEventPath)

                  'Couldn't find a specific node?  Try to find it with the code in hexadecimal.
230               If objEventNode Is Nothing Then

                      'Create a path to that individual event code.
240                   strEventPath = "Root/Range[@name='" & strRangeName & "']/Event[@code='H" & Hex(lngAdjCode) & "']"

                      'Try to find that event in the DOM.
250                   Set objEventNode = mdomErrors.selectSingleNode(strEventPath)

                      'Couldn't find a specific node?  Take the default for this range.
260                   If objEventNode Is Nothing Then

                          'Create a path to the default event code.
270                       strEventPath = "Root/Range[@name='" & strRangeName & "']/Event[@code='Default']"

                          'Try to find the default event in the DOM.
280                       Set objEventNode = mdomErrors.selectSingleNode(strEventPath)

                          'Enforce presence of default events.
290                       If objEventNode Is Nothing Then
300                           Err.Raise eNoDefaultEventFound, PROC_NAME, "No default Event found for Range " & strRangeName
310                       End If

320                   End If

330               End If

340               Exit For

350           End If

360       Next

          'If we found no range that fits the event code then use the
          'default for the whole app, which is found just below the root.
370       If objEventNode Is Nothing Then

              'Create a path to the app default event code.
380           strEventPath = "Root/Event[@code='Default']"

              'Try to find the default event in the DOM.
390           Set objEventNode = mdomErrors.selectSingleNode(strEventPath)

              'Enforce presence of default event at root.
400           If objEventNode Is Nothing Then
410               Err.Raise eNoDefaultEventFound, PROC_NAME, "No default Event found at Root"
420           End If

430       End If

          'Get the severity for this event.
440       strSeverity = GetChildNodeText(objEventNode, "@severity", True)

450       Select Case strSeverity
              Case "error"
460               intEventType = eLogEventTypeError
470           Case "warning"
480               intEventType = eLogEventTypeWarning
490           Case "information"
500               intEventType = eLogEventTypeInformation
510           Case "null"
520               intEventType = eLogEventTypeNull
530           Case Else
540               Err.Raise eBadEventSeverity, PROC_NAME, "An event had a severity of '" & strSeverity _
                      & "', which is not one of 'error', 'warning' or 'information'."
550       End Select

          'We now assume we have found an event.  Do the extractions.
560       strInternal = ParseMessage(objEventNode, "Internal", strErrDesc)
570       strExternal = ParseMessage(objEventNode, "External", strErrDesc)

ErrorHandler:

580       Set objRangeList = Nothing
590       Set objRangeNode = Nothing
600       Set objEventNode = Nothing
          
610       ReleaseDocument

620       If Err.Number <> 0 Then
630           LogNTEvent PROC_NAME & " Error: " & Err.Description, eLogEventTypeError
640           Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
650       End If

End Sub

'********************************************************************************
'* Internal method that combines the message found in the DOM
'* with the contents of the error description.
'********************************************************************************
Private Function ParseMessage( _
    ByRef objEventNode As IXMLDOMElement, _
    ByVal strName As String, _
    ByVal strDesc As String) As String

          Const PROC_NAME As String = MODULE_NAME & "ParseMessage: "

          Dim strMsg As String
          Dim strChunk As String
          Dim i As Integer
          Dim intPos As Integer

10        On Error GoTo ErrorHandler

          'Find the child element with name strName.
          Dim objChild As IXMLDOMNode
20        Set objChild = objEventNode.selectSingleNode(strName)

          'Return a blank string if child not found.
30        If objChild Is Nothing Then

40            ParseMessage = ""

          'Otherwise we have some work to do.
50        Else

60            strMsg = objChild.Text

              'Is the element blank?
70            If Len(strMsg) = 0 Then

                  'Then return the error description 'as is'.
80                ParseMessage = strDesc

              'Otherwise parse the string for '%#' to
              'replace with chunks from the description.
90            Else

                  'Max out at 20 replacements.
100               For i = 1 To 20

110                   intPos = InStr(strMsg, "%" & i)
120                   strChunk = GetMsgChunk(strDesc)

130                   If intPos > 0 Then
140                       strMsg = Left(strMsg, intPos - 1) & strChunk & Mid(strMsg, intPos + 2)
150                   End If

160               Next

170               ParseMessage = strMsg

180           End If

190       End If

ErrorHandler:

200       Set objChild = Nothing

210       If Err.Number <> 0 Then
220           LogNTEvent PROC_NAME & " Error: " & Err.Description, eLogEventTypeError
230           Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
240       End If

End Function

'********************************************************************************
'* Internal method that extracts all of the passed string
'* up to the first pipe.  The passed string is modified by
'* removing the extracted portion and the pipe.  If there
'* were no pipes then the passed string is retured and the
'* passed string is cleared.
'********************************************************************************
Private Function GetMsgChunk(ByRef strMsg As String) As String

          Dim intPos As Integer
10        intPos = InStr(strMsg, "|")

20        If intPos > 0 Then
30            GetMsgChunk = Left(strMsg, intPos - 1)
40            strMsg = Mid(strMsg, intPos + 1)
50        Else
60            GetMsgChunk = strMsg
70            strMsg = ""
80        End If

End Function

'********************************************************************************
'* Sets the path to source xml file.
'********************************************************************************
Friend Sub SetXmlFile(ByVal strXmlFile As String)
10        mstrXmlFile = strXmlFile
End Sub

'********************************************************************************
'* Sets the reference to the xml cache object.
'********************************************************************************
Friend Sub SetXmlCache(ByRef objXmlCache As CXmlCache)
10        Set mobjXmlCache = objXmlCache
End Sub

'********************************************************************************
'* Captures the source dom from the xml cache.
'* Used reference counting, so call as many times as you need to as
'* long as you match it up with Capture/Release pairs.
'********************************************************************************
Private Sub CaptureDocument()
10        If mdomErrors Is Nothing Then
20            Set mdomErrors = mobjXmlCache.GetDocument(mstrXmlFile)
30            mintInitCount = 0
40        End If
          
50        mintInitCount = mintInitCount + 1
End Sub

'********************************************************************************
'* Releases the source dom from the xml cache.
'********************************************************************************
Private Sub ReleaseDocument()
10        If mintInitCount <= 1 Then
20            Set mdomErrors = Nothing
30            mintInitCount = 0
40        Else
50            mintInitCount = mintInitCount - 1
60        End If
End Sub

