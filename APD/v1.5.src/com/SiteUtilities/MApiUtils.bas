Attribute VB_Name = "MApiUtils"
'********************************************************************************
'* Component SiteUtilities : Module MApiUtils
'*
'* Utility methods for calling the Windows API.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "MApiUtils."

Public Enum enmWinVersions
   WinNt40 = 1
   Win95 = 2
   Win98 = 3
   Win2000 = 4
End Enum

'Windows API Constants
Private Const VER_PLATFORM_WIN32_NT = 2
Private Const VER_PLATFORM_WIN32_WINDOWS = 1
Private Const VER_PLATFORM_WIN32s = 0

'Windows API Declarations
Private Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (lpVersionInformation As OSVERSIONINFO) As Long
Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Function FormatMessageAPI Lib "kernel32" Alias "FormatMessageA" (ByVal dwFlags As Long, lpSource As Any, ByVal dwMessageId As Long, ByVal dwLanguageId As Long, ByVal lpBuffer As String, ByVal nSize As Long, Arguments As Long) As Long

Public Declare Function timeGetTime Lib "winmm.dll" () As Long

Type OSVERSIONINFO 'for GetVersionEx API call
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As Long
    szCSDVersion As String * 128
End Type

'********************************************************************
'* Syntax:  GetWinVersion
'* Params:  None
'* Purpose: This procedure retrieves the windows version
'*              information via an API call.
'* Returns: An integer representing the windows version.
'*              The versions are represented in an enum.
'********************************************************************
Public Function GetWinVersion() As enmWinVersions
          Dim osvi As OSVERSIONINFO
          Dim strCSDVersion As String
          
10        osvi.dwOSVersionInfoSize = Len(osvi)
          
20        If GetVersionEx(osvi) = 0 Then
30            Exit Function
40        End If
          
50        Select Case osvi.dwPlatformId
              'NT 4.0
              Case VER_PLATFORM_WIN32_NT
60                Select Case osvi.dwMajorVersion
                      Case 4
70                        GetWinVersion = WinNt40
80                    Case 5
90                        GetWinVersion = Win2000
100               End Select
              'Windows 95
110           Case VER_PLATFORM_WIN32_WINDOWS
120               If osvi.dwMajorVersion > 4 Or (osvi.dwMajorVersion = 4 And osvi.dwMinorVersion > 0) Then
130                   GetWinVersion = Win98
140               Else
150                   GetWinVersion = Win95
160               End If
170       End Select
          
End Function

'********************************************************************
'* Syntax:  GetWinComputerName
'* Params:  None
'* Purpose: This procedure retrieves a NetBIOS name
'*              associated with the local computer.
'* Returns: An string - the name.
'********************************************************************
Public Function GetWinComputerName() As String
          Dim strName As String
          Dim lngCnt As Long
          
10        strName = Space(255)
20        lngCnt = GetComputerName(strName, 255)
          
30        If lngCnt <> 0 Then
40            strName = Left(strName, InStr(strName, Chr$(0)) - 1)
50        End If
          
60        GetWinComputerName = strName
End Function

'********************************************************************
'* Syntax:  FormatApiErrorMessage
'* Params:  lngErrNumber - The API error number
'* Purpose: Retrieves and formats API error messages
'* Returns: An string - the formatted error.
'********************************************************************
Public Function FormatApiErrorMessage(ByVal lngErrNumber As Long) As String
          Dim strBuffer As String * 512, strMsg As String
10        On Error GoTo ErrorHandler
          
20        FormatMessageAPI &H1000, Null, lngErrNumber, 0, strBuffer, 512, 0
          
30        strMsg = strBuffer
          'Strange but necessary manipulations
40        strMsg = Replace(strMsg, vbNewLine, "")
50        strMsg = Replace(strMsg, Chr(0), "")
          
60        FormatApiErrorMessage = strMsg
ErrorHandler:
End Function

