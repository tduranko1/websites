VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnitTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Class CUnitTest
'*
'* This purpose of this class is to establish a standard unit test interface.
'*
'* A client test application will will instantiate and call this interface in
'* turn for all components that implement it.  Results for each component will
'* be displayed to the tester.
'*
'* To implement for your component, include a copy of this file into your project.
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & ".CUnitTest."

'The number of tests called from this CUnitTest implementation.
Private Const mcintNumberOfTests As Integer = 11

Private mstrResults As String
Private mintErrors As String

'********************************************************************************
'* Returns a count of the number of tests that can be run for this application.
'********************************************************************************
Public Function NumTests() As Integer
10        NumTests = mcintNumberOfTests
End Function

'********************************************************************************
'* Returns the indexed test description.
'********************************************************************************
Public Function TestDesc(ByVal intIndex As Integer) As String
          Const PROC_NAME As String = MODULE_NAME & "TestDesc: "
          
10        Select Case intIndex
          
              'BAS modules
              Case 1: TestDesc = APP_NAME & "MApiUtils"
20            Case 2: TestDesc = APP_NAME & "MDomUtils"
30            Case 3: TestDesc = APP_NAME & "MEscapeUtils"
40            Case 4: TestDesc = APP_NAME & "MEventLoggers"
50            Case 5: TestDesc = APP_NAME & "MSiteUtilities"
              
              'CLS modules
60            Case 6: TestDesc = APP_NAME & "CEvents"
70            Case 7: TestDesc = APP_NAME & "CMessageStore"
80            Case 8: TestDesc = APP_NAME & "CSettings"
90            Case 9: TestDesc = APP_NAME & "CTransaction"
100           Case 10: TestDesc = APP_NAME & "CWebToolkit"
110           Case 11: TestDesc = APP_NAME & "CXmlCache"
              
120           Case Else:
130               Err.Raise vbObjectError + 1, PROC_NAME, "Passed test index out of range!"
140       End Select
          
End Function

'********************************************************************************
'* Runs the indexed test.
'* Returns the results as a giant formatted string.
'* Returns an error count through intErrors.
'********************************************************************************
Public Function RunTest(ByVal intIndex As Integer, ByRef intErrors As Integer) As String
          Const PROC_NAME As String = MODULE_NAME & "RunTest: "

10        Init intIndex 'Initialize results counters.
          
          'Run the test.
20        Select Case intIndex
          
              'BAS modules
              Case 1: RunTest_MApiUtils (intErrors)
30            Case 2: RunTest_MDomUtils (intErrors)
40            Case 3: RunTest_MEscapeUtils (intErrors)
50            Case 4: RunTest_MEventLoggers (intErrors)
60            Case 5: RunTest_MSiteUtilities (intErrors)
              
              'CLS modules
70            Case 6: RunTest_CEvents (intErrors)
80            Case 7: RunTest_CMessageStore (intErrors)
90            Case 8: RunTest_CSettings (intErrors)
100           Case 9: RunTest_CTransaction (intErrors)
110           Case 10: RunTest_CWebToolkit (intErrors)
120           Case 11: RunTest_CXmlCache (intErrors)
              
130           Case Else:
140               Err.Raise vbObjectError + 1, PROC_NAME, "Passed test index out of range!"
150       End Select
          
160       RunTest = Done(intErrors)
End Function

'********************************************************************************
'* Private results formatting helpers
'********************************************************************************

'Initialize results
Private Sub Init(ByVal intIndex As Integer)
10        mstrResults = vbCrLf & "TESTING : " & TestDesc(intIndex) & vbCrLf
20        mintErrors = 0
End Sub

Private Sub Success(ByVal strLine As String)
10        mstrResults = mstrResults & "SUCCESS : " & strLine & vbCrLf
End Sub

Private Sub Verify(ByVal strLine As String)
10        mstrResults = mstrResults & "VERIFY : " & strLine & "  Does this look right?" & vbCrLf
End Sub

Private Sub Failure(ByVal strLine As String)
10        mintErrors = mintErrors + 1
20        mstrResults = mstrResults & "FAILURE : " & strLine & vbCrLf
End Sub

'Add new results line.
Private Function Done(ByRef intErrors As Integer) As String
10        intErrors = mintErrors
20        Done = mstrResults
End Function

'********************************************************************************
'* ADD PRIVATE TEST METHODS BELOW THIS LINE.
'********************************************************************************

'********************************************************************************
'* Tests Module MApiUtils.bas
'********************************************************************************
Private Function RunTest_MApiUtils(ByRef intErrors As Integer) As String
    On Error Resume Next
    
    ' Test GetWinVersion()
    
    Dim eVersion As enmWinVersions
    Dim strTemp As String
    
    eVersion = GetWinVersion()
    
    If eVersion = Win2000 Then
        Success "GetWinVersion() returned 'Win2000'."
    Else
        Failure "GetWinVersion() returned '" & eVersion & "'."
    End If
    
    ' Test GetWinComputerName()
    
    strTemp = GetWinComputerName()
    
    If strTemp = "" Then
        Failure "GetWinComputerName() returned nothing."
    Else
        Verify "GetWinComputerName() returned '" & strTemp & "'."
    End If
    
    ' Test FormatApiErrorMessage()
    
    strTemp = FormatApiErrorMessage(10)
    
    If strTemp = "" Then
        Failure "FormatApiErrorMessage() returned nothing."
    ElseIf InStr(strTemp, "environment") < 1 Then
        Failure "FormatApiErrorMessage() returned nothing."
    Else
        Success "FormatApiErrorMessage() working."
    End If
    
End Function

'********************************************************************************
'* Tests Module MDomUtils.bas
'********************************************************************************
Private Function RunTest_MDomUtils(ByRef intErrors As Integer) As String
    On Error Resume Next
    
    Dim objDom As New MSXML2.DOMDocument40
    Dim strXmlFile As String
    Dim strXml As String
    
    strXmlFile = App.Path & "\UnitTestData\MDomUtilsTest.xml"
    
    ' Test LoadXmlFile
    
    LoadXmlFile objDom, strXmlFile, "RunTest_MDomUtils", "Test Xml File"
    
    If objDom.documentElement.nodeName <> "Root" Then
        Failure "LoadXmlFile could not load " & strXmlFile
    Else
        Success "LoadXmlFile loaded " & strXmlFile
    End If
    
    ' Test LoadXml
    
    strXml = objDom.xml
    
    LoadXml objDom, strXml, "RunTest_MDomUtils", "Test Xml"
    
    If objDom.documentElement.nodeName <> "Root" Then
        Failure "LoadXmlFile could not load XML string."
    Else
        Success "LoadXmlFile loaded XML string."
    End If
    
    Set objDom = Nothing
    
End Function

'********************************************************************************
'* Tests Module MEscapeUtils.bas
'********************************************************************************
Private Function RunTest_MEscapeUtils(ByRef intErrors As Integer) As String
          
10        Success "TBD!"
          
End Function

'********************************************************************************
'* Tests Module MEventLoggers.bas
'********************************************************************************
Private Function RunTest_MEventLoggers(ByRef intErrors As Integer) As String
          
10        Success "TBD!"
          
End Function

'********************************************************************************
'* Tests Module MSiteUtilities.bas
'********************************************************************************
Private Function RunTest_MSiteUtilities(ByRef intErrors As Integer) As String
          
10        Success "TBD!"
          
End Function

'********************************************************************************
'* Tests Class Module CEvents
'********************************************************************************
Private Function RunTest_CEvents(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CEvents
20        If obj Is Nothing Then
30            Failure "Could not create CEvents"
40        Else
50            Success "Created CEvents object."
60        End If
          
          'Other CEvents specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CMessageStore
'********************************************************************************
Private Function RunTest_CMessageStore(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CMessageStore
20        If obj Is Nothing Then
30            Failure "Could not create CMessageStore"
40        Else
50            Success "Created CMessageStore object."
60        End If
          
          'Other CMessageStore specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CSettings
'********************************************************************************
Private Function RunTest_CSettings(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CSettings
20        If obj Is Nothing Then
30            Failure "Could not create CSettings"
40        Else
50            Success "Created CSettings object."
60        End If
          
          'Other CSettings specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CTransaction
'********************************************************************************
Private Function RunTest_CTransaction(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CTransaction
20        If obj Is Nothing Then
30            Failure "Could not create CTransaction"
40        Else
50            Success "Created CTransaction object."
60        End If
          
          'Other CTransaction specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CWebToolkit
'********************************************************************************
Private Function RunTest_CWebToolkit(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CWebToolkit
20        If obj Is Nothing Then
30            Failure "Could not create CWebToolkit"
40        Else
50            Success "Created CWebToolkit object."
60        End If
          
          'Other CWebToolkit specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CXmlCache
'********************************************************************************
Private Function RunTest_CXmlCache(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CXmlCache
20        If obj Is Nothing Then
30            Failure "Could not create CXmlCache"
40        Else
50            Success "Created CXmlCache object."
60        End If
          
          'Other CXmlCache specific tests:
          
70        Set obj = Nothing
End Function

