VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CXmlCache"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Component SiteUtilities : Class CXmlCache
'*
'* This class is used to cache XML files through DOM documents.
'* It used to do all the cacheing internally, but this has been moved to
'* a C++ component for object pooling.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CXmlCache."

'Internal error codes for this class.
Private Enum ErrorCodes
    eNull = SiteUtilities_FirstError + &H400
End Enum

'DEPRACATED, but kept around for binary compatability.
Type typFileInfo
    strFile As String
    objDate As Date
    dom As Object
End Type

'Pooled C++ component used to cache DOM objects.
Private mobjXmlCache As BaseUtilitiesLib.XmlCache

Private Sub Class_Initialize()
10        Set mobjXmlCache = New BaseUtilitiesLib.XmlCache
End Sub

Private Sub Class_Terminate()
10        Set mobjXmlCache = Nothing
End Sub

'********************************************************************************
'* Syntax:      object.GetDocument( "C:\xml\myfile.xml" )
'* Parameters:  strFileName = Path to the file we want to open.
'* Purpose:     This procedure retrieves an XML file as a DOM Document.
'*                  It caches retrieved files for performance reasons,
'*                  reloading them when their file date has changed.
'* Returns:     The DOMDocument40 for the retrieved file.
'********************************************************************************
Public Function GetDocument(ByVal strFileName As String) As Object
          Const PROC_NAME As String = MODULE_NAME & "GetDocument: "
          
10        On Error GoTo ErrorHandler

20        Set GetDocument = mobjXmlCache.GetDom(strFileName)
          
ErrorHandler:

30        If Err.Number <> 0 Then
          
              Dim strDesc As String
40            strDesc = Err.Description & vbCrLf & _
                  "BaseUtilErr = '" & mobjXmlCache.GetErrorDescription() & "'" & vbCrLf & _
                  "FileName = '" & strFileName & "'"

50            LogNTEvent PROC_NAME & " Error: " & strDesc, eLogEventTypeError
              
60            Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, strDesc
70        End If

End Function

