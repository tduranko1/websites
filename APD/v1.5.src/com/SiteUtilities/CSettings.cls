VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Component SiteUtilities : Class CSettings
'*
'* This class is used to access hierarchical XML settings files for applications.
'* It can be used in conjunction with CXmlCache for efficient DOM access.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CSettings."

Private Declare Function GetComputerNameA Lib "kernel32" (ByVal lpBuffer As String, nSize As Long) As Long

'Member variables
Private mdomSettings As MSXML2.DOMDocument40
Private mstrXmlFile As String
Private mobjXmlCache As CXmlCache
Private mintInitCount As Integer

Private mstrApplication As String
Private mstrEnvironment As String
Private mstrMachine As String
Private mstrInstance As String

'Internal error codes for this class.
Private Enum ErrorCodes
    eDOMDocument40NotSet = SiteUtilities_FirstError + &H200
    eIdentityNotSet
    eNodeNotFound
    eMachineNameNotFound
    eInstanceNotFound
    eInstanceNodeNotFound
    eInstanceNameAttNotFound
    eMethodDepracated
End Enum

Private Sub Class_Terminate()
10        Set mdomSettings = Nothing
End Sub

'********************************************************************************
'* Access to private members via properties
'********************************************************************************
Public Property Get Application() As String
10       Application = mstrApplication
End Property

Public Property Get Environment() As String
10       Environment = mstrEnvironment
End Property

Public Property Get Machine() As String
10       Machine = mstrMachine
End Property

Public Property Get Instance() As String
10       Instance = mstrInstance
End Property

'********************************************************************************
'* Syntax:      object.SetDocument( domSettings )
'* Parameters:  domSettings the DOMDocument40
'* Purpose:     Initializes this object with a DOM document.
'* Returns:     Nothing
'********************************************************************************
Public Sub SetDocument(ByRef domSettings As Object)
10        Err.Raise eMethodDepracated, MODULE_NAME & "SetDocument()", "Method no longer allowed."
End Sub

'********************************************************************************
'* Syntax:      If objMsgStore.HasDocument() Then
'* Parameters:  None
'* Purpose:     Determines if the document has been set.
'* Returns:     True if the document has been set.
'********************************************************************************
Private Function HasDocument() As Boolean
10        HasDocument = CBool(Len(mstrXmlFile) > 4)
End Function

'********************************************************************************
'* Internal function used to check that SetDocument was called.
'********************************************************************************
Private Sub CheckDocument()
10    If Not HasDocument() Then
20        Err.Raise eDOMDocument40NotSet, MODULE_NAME & "CheckDocument()", MODULE_NAME & "SetDocument() not called properly for CSettings.  Initialization issue?"
30    End If
End Sub

'********************************************************************************
'* Syntax:      object.SetIdentity( "APD", "SFTMDEVWEB", "v1.0.0" )
'* Parameters:  strApplication - the application name
'*              strMachine - the name of the machine COM is running on.
'* Purpose:     Gets a value from the ini DOM document.
'* Returns:     The value.
'********************************************************************************
Public Function SetIdentity(ByVal strMachine As String, ByVal strInstance As String)

          Dim objMachineNodeList As IXMLDOMNodeList
          Dim objMachineNode As IXMLDOMNode
          Dim objInstanceNode As IXMLDOMNode
          Dim objNameAtt As IXMLDOMAttribute

10        CaptureDocument
          
20        mstrMachine = strMachine
30        mstrInstance = strInstance
40        mstrApplication = GetRawSetting("Root/Application/@name")

          'Check to ensure that passed machine name was found in the config document.

          Dim strPath As String
50        strPath = "Root/Application/Environment/Machine[@name='" & strMachine & "']"

60        Set objMachineNodeList = mdomSettings.selectNodes(strPath)

70        If objMachineNodeList.length = 0 Then
80            Err.Raise eMachineNameNotFound, MODULE_NAME & "SetIdentity()", "Machine name not found in configuration DOM."
90        End If

          'If there is more than one machine element matching this machine, then
          'enforce presence of instance elements under that machine.

100       If objMachineNodeList.length > 1 Then

              Dim blnFoundInstance As Boolean

110           blnFoundInstance = False

120           For Each objMachineNode In objMachineNodeList

                  'Check for Instance node under machine node.
130               Set objInstanceNode = objMachineNode.selectSingleNode("Instance")

140               If objInstanceNode Is Nothing Then
150                   Err.Raise eInstanceNodeNotFound, MODULE_NAME & "SetIdentity()", "Instance not found in configuration DOM for Machine '" & strMachine & "'.  There must be an 'Instance' element under all machines that have multiple machine elements in the DOM."
160               Else

                      'Check for name attribute under instance.
170                   Set objNameAtt = objInstanceNode.Attributes.getNamedItem("name")

180                   If objNameAtt Is Nothing Then
190                       Err.Raise eInstanceNameAttNotFound, MODULE_NAME & "SetIdentity()", "Instance element Machine[@name='" & strMachine & "']/Instance[@name='" & strInstance & "'] did not have a 'name' attribute."
200                   End If

                      'If the found instance name matches the passed one then flag it.
210                   If objNameAtt.Value = strInstance Then
220                       blnFoundInstance = True
230                   End If

240               End If

250           Next

              'Throw if the passed instance was never found for the machine.
260           If blnFoundInstance = False Then
270               Err.Raise eInstanceNotFound, MODULE_NAME & "SetIdentity()", "Instance element Machine[@name='" & strMachine & "']/Instance[@name='" & strInstance & "'] not found in the configuration DOM."
280           End If

              'Get Environment name
290           mstrEnvironment = GetRawSetting("Root/Application/Environment[Machine[@name='" & strMachine & "'][Instance[@name='" & strInstance & "']]]/@name")

300       Else
              'Get Environment name.
310           mstrEnvironment = GetRawSetting("Root/Application/Environment[Machine[@name='" & strMachine & "']]/@name")
320       End If

330       Set objMachineNodeList = Nothing
340       Set objMachineNode = Nothing
350       Set objInstanceNode = Nothing
360       Set objNameAtt = Nothing

370       ReleaseDocument
          
End Function

'********************************************************************************
'* Internal function used to check that SetIdenity was called.
'********************************************************************************
Private Sub CheckIdentity()
10        If Len(mstrMachine) = 0 Then
20            Err.Raise eIdentityNotSet, "CheckIdentity", MODULE_NAME & "Machine name was blank."
30        End If
End Sub

'********************************************************************************
'* Syntax:      objNode = object.GetRawNode( "/Root/Elem/@Att" )
'* Parameters:  strXPath = XPath to the DOM node to retrieve.
'* Purpose:     Retrieves a DOM node from the ini DOM document.
'* Returns:     The node.
'********************************************************************************
Public Function GetRawNode(ByVal strXPath As String) As Object
          Const PROC_NAME As String = MODULE_NAME & "GetRawNode: "

10        On Error GoTo ErrorHandler

20        CaptureDocument

30        Set GetRawNode = mdomSettings.selectSingleNode(strXPath)

ErrorHandler:

40        ReleaseDocument
          
50        If Err.Number <> 0 Then
60            LogNTEvent PROC_NAME & " Error: " & Err.Description, eLogEventTypeError
70            Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
80        End If
End Function

'********************************************************************************
'* Syntax:      strSetting = object.GetSetting( "/Root/Elem/@Att" )
'* Parameters:  strXPath = XPath to the value to retrieve.
'* Purpose:     Gets a value from the ini DOM document.
'* Returns:     The value.
'********************************************************************************
Public Function GetRawSetting(ByVal strXPath As String) As String
          Const PROC_NAME As String = MODULE_NAME & "GetRawSetting: "

10        On Error GoTo ErrorHandler

          Dim objNode As Object
20        Set objNode = GetRawNode(strXPath)

30        If Not (objNode Is Nothing) Then
40            GetRawSetting = objNode.nodeValue()
50        End If

ErrorHandler:

60        Set objNode = Nothing

70        If Err.Number <> 0 Then
80            LogNTEvent PROC_NAME & " Error: " & Err.Description, eLogEventTypeError
90            Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
100       End If

End Function

'********************************************************************************
'* Syntax:      objNode = object.GetParsedNode( "@ConnectionString" )
'* Parameters:  strXPath = XPath to the DOM node to retrieve.
'* Purpose:     Retrieves a DOM node from the ini DOM document.
'*                  Looks first in the proper Instance, then the Machine, then
'*                  the Environment and then lastly the Application.
'* Returns:     The node.
'********************************************************************************
Public Function GetParsedNode(ByVal strXPath As String) As Object
          Const PROC_NAME As String = MODULE_NAME & "GetParsedNode: "

          Dim objNode As Object

10        On Error GoTo ErrorHandler

20        CaptureDocument
30        CheckIdentity

          Dim strAppPath As String
          Dim strEnvPath As String, strEnvInstPath As String
          Dim strMachOnly As String, strMachPath As String, strMachInstPath As String
          Dim strInstPath As String
          Dim blnFoundInst As Boolean

          'Strip off any leading slashes - this disables
          '"anywhere" searches with xpaths that begin with "//"
40        While Left(strXPath, 1) = "/"
50            strXPath = Mid(strXPath, 2)
60        Wend

          'Build ini file xpath hierarchies.
70        strAppPath = "Root/Application/"
80        strEnvPath = strAppPath & "Environment[Machine[@name='" & mstrMachine & "']]/"
90        strEnvInstPath = strAppPath & "Environment[Machine[@name='" & mstrMachine & "'][Instance[@name='" & mstrInstance & "']]]/"
100       strMachOnly = "//Machine[@name='" & mstrMachine & "'][Instance[@name='" & mstrInstance & "']]"
110       strMachPath = strAppPath & "Environment/Machine[@name='" & mstrMachine & "']/"
120       strMachInstPath = strAppPath & "Environment/Machine[@name='" & mstrMachine & "'][Instance[@name='" & mstrInstance & "']]/"
130       strInstPath = strMachPath & "Instance[@name='" & mstrInstance & "']/"

140       Set objNode = mdomSettings.selectSingleNode(strMachOnly)
150       blnFoundInst = Not (objNode Is Nothing)

          'Try to resolve xpath at each level in turn.

          'Try full path to instance.
160       Set objNode = mdomSettings.selectSingleNode(strInstPath & strXPath)

          'Try machine level above instance.
170       If objNode Is Nothing Then
180           Set objNode = mdomSettings.selectSingleNode(strMachInstPath & strXPath)
190       End If

          'Try machine only, but only if an instance wasn't found.
200       If blnFoundInst = False And objNode Is Nothing Then
210           Set objNode = mdomSettings.selectSingleNode(strMachPath & strXPath)
220       End If

          'Try environment above machine and instance.
230       If objNode Is Nothing Then
240           Set objNode = mdomSettings.selectSingleNode(strEnvInstPath & strXPath)
250       End If

          'Try any environment with machine, but only if an instance wasn't found
260       If blnFoundInst = False And objNode Is Nothing Then
270           Set objNode = mdomSettings.selectSingleNode(strEnvPath & strXPath)
280       End If

          'Try application defaults.
290       If objNode Is Nothing Then
300           Set objNode = mdomSettings.selectSingleNode(strAppPath & strXPath)
310       End If

          'Now blow up if absolutely nothing found.
320       If objNode Is Nothing Then
330           Err.Raise eNodeNotFound, "NodeSearch", "Could not find specified node " & strXPath & " within DOM at ANY level."
340       End If

350       Set GetParsedNode = objNode

ErrorHandler:

360       Set objNode = Nothing
          
370       ReleaseDocument

380       If Err.Number <> 0 Then
390           LogNTEvent PROC_NAME & " Error: " & Err.Description, eLogEventTypeError
400           Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
410       End If
End Function

'********************************************************************************
'* Syntax:      strSetting = object.GetSetting( "/Root/Elem/@Att" )
'* Parameters:  strXPath = XPath to the value to retrieve.
'* Purpose:     Gets a value from the ini DOM document.
'*                  Looks first in the proper Instance, then the Machine, then
'*                  the Environment and then lastly the Application.
'* Returns:     The value.
'********************************************************************************
Public Function GetParsedSetting(ByVal strXPath As String) As String
          Const PROC_NAME As String = MODULE_NAME & "GetParsedSetting: "

10        On Error GoTo ErrorHandler

          Dim objNode As Object
20        Set objNode = GetParsedNode(strXPath)

30        If Not (objNode Is Nothing) Then
40            GetParsedSetting = objNode.Text
50        End If

ErrorHandler:

60        Set objNode = Nothing

70        If Err.Number <> 0 Then
80            LogNTEvent PROC_NAME & " Error: " & Err.Description, eLogEventTypeError
90            Err.Raise Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
100       End If

End Function

'********************************************************************************
'* Sets the path to source xml file.
'********************************************************************************
Friend Sub SetXmlFile(ByVal strXmlFile As String)
10        mstrXmlFile = strXmlFile
End Sub

'********************************************************************************
'* Sets the reference to the xml cache object.
'********************************************************************************
Friend Sub SetXmlCache(ByRef objXmlCache As CXmlCache)
10        Set mobjXmlCache = objXmlCache
End Sub

'********************************************************************************
'* Captures the source dom from the xml cache.
'* Used reference counting, so call as many times as you need to as
'* long as you match it up with Capture/Release pairs.
'********************************************************************************
Private Sub CaptureDocument()
10        If mdomSettings Is Nothing Then
20            Set mdomSettings = mobjXmlCache.GetDocument(mstrXmlFile)
30            mintInitCount = 0
40        End If
          
50        mintInitCount = mintInitCount + 1
End Sub

'********************************************************************************
'* Releases the source dom from the xml cache.
'********************************************************************************
Private Sub ReleaseDocument()
10        If mintInitCount <= 1 Then
20            Set mdomSettings = Nothing
30            mintInitCount = 0
40        Else
50            mintInitCount = mintInitCount - 1
60        End If
End Sub


'********************************************************************************
'* GetServerName
'*      Returns the server name
'*      WJC - 7/25/2011
'********************************************************************************
Public Function GetServerName() As String

    Dim strServerName As String
    Dim strResult As String * 255
    
    GetComputerNameA strResult, 255
    strServerName = Left$(strResult, InStr(strResult, Chr$(0)) - 1)

    GetServerName = strServerName

End Function

