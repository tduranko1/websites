VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CWebToolkit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Component SiteUtilities : Class CWebToolkit
'*
'* This class contains generic utility methods for use in business objects and
'* data presentation objects built for web-based applications.
'*
'* By modularizing as much code as possible into this class, we reduce the
'* level of maintenance required for such business and presentation objects.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CWebToolkit."

'Internal error codes for this class.
Private Enum ErrorCodes
    eSessionKeyBlank = SiteUtilities_FirstError + &H300
    eSessionParameterNotFound
    eXmlLoadError
    eXslLoadError
    eCrudVarMissingSelect
    eCrudXmlMissingItem
    eNotInitialized
    eBlankParamName
    eRedirectFileNotFound
End Enum

'********************************************************************************
'* Objects that will need clean-up
'********************************************************************************
Public mEvents As CEvents
Private mcolXslParams As Scripting.Dictionary
Private mobjDataAccessor As Object
Private mobjXslCache As BaseUtilitiesLib.XslCache
Private mobjSessionMgr As Object

'********************************************************************************
'* Primitives that don't need clean-up
'********************************************************************************
Private mstrUserId As String
Private mstrSessionKey As String
Private mstrWebRoot As String
Private mstrWindowID As String
Private mstrDebugID As String

'********************************************************************************
'* Construction
'********************************************************************************
Private Sub Class_Initialize()
10        Set mEvents = New CEvents
20        Set mcolXslParams = New Scripting.Dictionary
30        Set mobjXslCache = New BaseUtilitiesLib.XslCache
40        mstrDebugID = "Debug"
End Sub

'********************************************************************************
'* Destruction
'********************************************************************************
Private Sub Class_Terminate()
10        mcolXslParams.removeAll
          
20        Set mEvents = Nothing
30        Set mcolXslParams = Nothing
40        Set mobjDataAccessor = Nothing
50        Set mobjXslCache = Nothing
60        Set mobjSessionMgr = Nothing
End Sub

'********************************************************************************
'* Syntax:      obj.Initialize(strWebRoot, strSessionKey, strUserId)
'* Parameters:  strWebRoot - Path to the website root.
'*              strSessionKey - Key returned from SessionManager object.  Optional.
'*                  This will be used for automatic XSLT and CRUD functionality.
'*              strUserId - User Id - Optional.
'*                  This will be used for automatic XSLT and CRUD functionality.
'*              strDebugId - Element looked up in the ini file to enable debug mode.
'*                  This parameter is no longer used.
'* Purpose:     Initializes the toolkit.
'* Returns:     Nothing
'********************************************************************************
Public Sub Initialize( _
    ByVal strWebRoot As String, _
    Optional ByVal strSessionKey As String = "", _
    Optional ByVal strUserId As String = "", _
    Optional ByVal strDebugId As String = "Debug")

          Const PROC_NAME As String = MODULE_NAME & "Initialize: "

10        On Error GoTo ErrorHandler

          'Set the events object website root.
20        mEvents.Initialize strWebRoot, strDebugId

          'Add passed arguments to debug info.
30        If mEvents.IsDebugMode Then
40            mEvents.Trace "WebRoot = " & strWebRoot & vbCrLf _
                  & "  SessionKey = " & strSessionKey & vbCrLf _
                  & "  UserId = " & strUserId, PROC_NAME
50        End If

60        mcolXslParams.removeAll

          'Store these for later.
70        mstrUserId = strUserId
80        mstrSessionKey = strSessionKey
90        mstrWebRoot = strWebRoot
100       mstrWindowID = "0"
110       mstrDebugID = strDebugId

ErrorHandler:

120       If Err.Number <> 0 Then
130           mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "WebRoot=" & strWebRoot & " SessionKey=" & strSessionKey & _
                  "UserId=" & strUserId & " DebugId=" & strDebugId
140       End If

End Sub

'********************************************************************************
'* Syntax:      obj.InitializeEx(strWebRoot, strSessionKey, strWindowID, strUserId)
'* Parameters:  strWebRoot - Path to the website root.
'*              strSessionKey - Key returned from SessionManager object.  Optional.
'*                  This will be used for automatic XSLT and CRUD functionality.
'*              strWindowID - Window ID used for multiple window sessions.
'*              strUserId - User Id - Optional.
'*                  This will be used for automatic XSLT and CRUD functionality.
'* Purpose:     Initializes the toolkit.
'* Returns:     Nothing
'********************************************************************************
Public Sub InitializeEx( _
    ByVal strWebRoot As String, _
    ByVal strSessionKey As String, _
    Optional ByVal strWindowID As String = "0", _
    Optional ByVal strUserId As String = "")

          Const PROC_NAME As String = MODULE_NAME & "InitializeEx: "

10        On Error GoTo ErrorHandler

          'Set the events object website root.
20        mEvents.Initialize strWebRoot, "WebDebug"

          'Add passed arguments to debug info.
30        If mEvents.IsDebugMode Then
40            mEvents.Trace "WebRoot = " & strWebRoot & vbCrLf _
                  & "  SessionKey = " & strSessionKey & vbCrLf _
                  & "  WindowID = " & strWindowID & vbCrLf _
                  & "  UserId = " & strUserId, PROC_NAME
50        End If

60        mcolXslParams.removeAll

          'Store these for later.
70        mstrUserId = strUserId
80        mstrSessionKey = strSessionKey
90        mstrWebRoot = strWebRoot
100       mstrWindowID = strWindowID
110       mstrDebugID = "WebDebug"

ErrorHandler:

120       If Err.Number <> 0 Then
130           mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "WebRoot=" & strWebRoot & " SessionKey=" & strSessionKey & _
                  "WindowID=" & strWindowID & " UserId=" & strUserId
140       End If

End Sub

'********************************************************************************
'* Quick check to ensure that this object has been initialized.
'********************************************************************************
Public Function IsInitialized() As Boolean
10        IsInitialized = mEvents.IsInitialized
End Function

'********************************************************************************
'* Syntax:      obj.AddXslParam("SomeVariable", strSomeVariable)
'* Parameters:  strName - The variable name.
'*              strValue - The value to set for that variable.
'* Purpose:     Used to set parameters to stub into any style sheet used within
'*              subsequent calls to TransformXML.  Note that the web root, session
'*              key and user id passed within Initialize are automatically stubbed.
'* Returns:     Nothing.
'********************************************************************************
Public Sub AddXslParam(ByVal strName As String, ByVal strValue As String)
          Const PROC_NAME As String = MODULE_NAME & "AddXslParam: "

10        On Error GoTo ErrorHandler

20        If Not IsInitialized Then
30            Err.Raise eNotInitialized, "IsInitialized", "You must call Initialize before " & PROC_NAME
40        End If

50        If mEvents.IsDebugMode Then mEvents.Trace "Name=" & strName & " Value=" & strValue

60        If Len(strName) = 0 Then
70            Err.Raise eBlankParamName, "", "Passed name is blank"
80        End If

90        mcolXslParams.Add strName, strValue

ErrorHandler:

100       If Err.Number <> 0 Then
110           mEvents.Env "WebSite=" & mEvents.WebRoot
120           mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  PROC_NAME & " Name=" & strName & " Value=" & strValue
130       End If

End Sub

'********************************************************************************
'* Syntax:      Set objData = objToolkit.CreateDataAccessor()
'* Parameters:  strConnectStringID - The connect string to look for in the ini
'*              file (if not 'ConnectionStringXML')
'* Purpose:     Returns a created and initialized DataAccessor object.
'* Returns:     The DataAccessor object.
'********************************************************************************
Public Function CreateDataAccessor(Optional strConnectStringID As String = "ConnectionStringXml") As Object
          Const PROC_NAME As String = MODULE_NAME & "CreateDataAccessor: "

10        On Error GoTo ErrorHandler

          'Note that mobjDataAccessor is a private class member.
          'We save it for later CRUD permissions lookups.
20        Set mobjDataAccessor = CreateObjectEx("DataAccessor.CDataAccessor")

          'Initialize the DataAccessor object before returning it.
30        mobjDataAccessor.mEvents.ComponentInstance = mEvents.ComponentInstance
40        mobjDataAccessor.InitEvents mEvents.WebRoot, mstrDebugID, False
          
          'Set the connect string from the config file.
50        mobjDataAccessor.SetConnectString mEvents.mSettings.GetParsedSetting(strConnectStringID)
          
          'Return the DataAccessor to the caller.
60        Set CreateDataAccessor = mobjDataAccessor

ErrorHandler:
70        If Err.Number <> 0 Then
80            mEvents.Env "WebSite=" & mEvents.WebRoot
90            mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
100       End If
End Function

'********************************************************************************
'* Private function used to create data accessor if not already done by client.
'********************************************************************************
Private Function GetDataAccessor() As Object
10        If mobjDataAccessor Is Nothing Then
20            Set mobjDataAccessor = CreateDataAccessor()
30        End If
40        Set GetDataAccessor = mobjDataAccessor
End Function

'********************************************************************************
'* Syntax:      Set colForm = objToolkit.ParseFormDataToCollection( strFormData )
'* Parameters:  The form data.
'* Purpose:     Converts get/post form submission data into a collection.
'* Returns:     Returns a collection containing the item=value pairs.
'********************************************************************************
Public Function ParseFormDataToCollection(ByRef strFormData As String) As Collection
10        Set ParseFormDataToCollection = PrivateParseFormDataToCollection(strFormData, False)
End Function

'********************************************************************************
'* Syntax:      Set colForm = objToolkit.ParseFormDataToSqlStringCollection( strFormData )
'* Parameters:  The form data.
'* Purpose:     Converts get/post form submission data into a collection.
'*              This version of the above method escapes single quotes,
'*              For client side XML calls that get converted back to strings
'*              in order to add the little 'FOR XML EXPLICIT' moniker.
'* Returns:     Returns a collection containing the item=value pairs.
'********************************************************************************
Public Function ParseFormDataToSqlStringCollection(ByRef strFormData As String) As Collection
10        Set ParseFormDataToSqlStringCollection = PrivateParseFormDataToCollection(strFormData, True)
End Function

'Internal function that does the work for the above methods.
'Had to separate the above into two different methods to avoid
'breaking binary compatibility.
Private Function PrivateParseFormDataToCollection(ByRef strFormData As String, ByVal blnSQLQueryString As Boolean) As Collection
          Const PROC_NAME As String = MODULE_NAME & "ParseFormDataToCollection: "

          Dim colForm As New Collection
          Dim x As Long
          Dim arrForm() As String
          Dim arrData() As String
          Dim strValue As String
          Dim strItem As String

10        On Error GoTo ErrorHandler

          'First split the form data on the '&' to get the item=value pairs.
20        arrForm = Split(strFormData, "&", -1, 1)

30        If UBound(arrForm) > -1 Then

40            For x = LBound(arrForm) To UBound(arrForm)

                  'Now split the form data on the '=' to break the pairs.
50                arrData = Split(arrForm(x), "=", -1, 1)

60                If UBound(arrData) > 0 Then

70                    strValue = UnEscapeString(arrData(1))
80                    strItem = "@" & UnEscapeString(arrData(0))

90                    If blnSQLQueryString Then
100                       strValue = SQLQueryString(strValue)
110                   End If

120                   colForm.Add strValue, strItem
130               End If

140           Next
150       End If

160       Set PrivateParseFormDataToCollection = colForm

ErrorHandler:

170       Set colForm = Nothing

180       If Err.Number <> 0 Then
190           mEvents.Env "WebSite=" & mEvents.WebRoot
200           mEvents.Env "FormData=" & strFormData
210           mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, _
                  Err.Description & " [is the item '" & strItem & "' duplicated in the form?]"
220       End If
End Function

'********************************************************************************
'* Syntax:      strHTML = objToolkit.TransformXML(strXML, strStyleSheet)
'* Parameters:  strXML - The XML to transorm.
'*              strStyleSheet - The style sheet to transform with.
'*              colXslParams - Parameter values indexed by key names.
'*                  Note that colParams will be cleared within this method.
'* Purpose:     Transforms the passed XML with the passed style sheet.
'* Returns:     The transformed XML.
'********************************************************************************
Public Function TransformXML( _
    ByRef strXml As String, _
    ByVal strStyleSheet As String) As Variant

          Const PROC_NAME As String = MODULE_NAME & "TransformXML: "

          Dim objProc As MSXML2.IXSLProcessor
          Dim objDom As MSXML2.DOMDocument40
          Dim objSession As Object

          Dim blnDebugMode As Boolean
          Dim blnNeedsSession As Boolean
          Dim strXslPath As String

10        On Error GoTo ErrorHandler

          'Get debug mode status.
20        blnDebugMode = mEvents.IsDebugMode

30        If blnDebugMode Then mEvents.Trace "", PROC_NAME & "Started"

          'Build full path to style sheet.
40        strXslPath = mEvents.WebRoot & "xsl\" & strStyleSheet

50        If blnDebugMode Then mEvents.Trace "StyleSheet = " & strXslPath _
              & " Is Cached = " & mobjXslCache.GetIsCached(strXslPath), _
              PROC_NAME & "domXSL.loadXML"

60        Set objProc = mobjXslCache.createProcessor(strXslPath)
          
          'Automatically create xsl params for web root and session key.
70        On Error Resume Next
80        If Len(mstrWindowID) > 0 Then mcolXslParams.Add "WindowID", mstrWindowID
90        If Len(mstrUserId) > 0 Then mcolXslParams.Add "UserId", mstrUserId
100       On Error GoTo ErrorHandler
          
          'Make Session Manager available if required.
110       blnNeedsSession = CBool(mobjXslCache.GetNeedsSession(strXslPath) = 1)
          
120       If blnDebugMode Then mEvents.Trace "Requires Session = " & blnNeedsSession, PROC_NAME
          
130       If blnNeedsSession Then
          
140           Set objSession = CreateObjectEx("SessionManager.CSession")
150           objSession.XslInitSession mstrSessionKey, mstrWindowID
              
160           objProc.addObject objSession, "http://lynx.apd/session-manager"
          
170       End If

          'Stuffs param and variable elements within the style sheet.
180       SetXslParams objProc, mobjXslCache.GetParamList(strXslPath)

          'Stuffs crud param and variable elements within the style sheet.
190       SetCrudParams objProc, mobjXslCache.GetCrudList(strXslPath)

200       If blnDebugMode Then mEvents.Trace "", PROC_NAME & "objProc.Transform"
          
          'Load the param XML into a DOM
210       Set objDom = New MSXML2.DOMDocument40
220       LoadXml objDom, strXml, PROC_NAME, "Xml Param"

          'Transform the XML with the XSL style sheet.
230       objProc.input = objDom
240       objProc.Transform
250       TransformXML = objProc.output
          
260       If blnDebugMode Then mEvents.Trace "", PROC_NAME & "Finished"

ErrorHandler:

270       Set objProc = Nothing
280       Set objDom = Nothing
290       Set objSession = Nothing

300       If Err.Number <> 0 Then
310           mEvents.Env "WebSite=" & mEvents.WebRoot
320           mEvents.Env "XML=" & strXml
330           mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description & vbCrLf & _
                  "      XslCacheErr = '" & mobjXslCache.GetErrorDescription() & "'", _
                  PROC_NAME & " strStyleSheet=" & strStyleSheet
340       End If

End Function

'********************************************************************************
'* Syntax:      SetXslParams( objXSL, colParams )
'* Parameters:  domXSL - Style sheet DOM object.
'*              colParams - Parameter values indexed by key names.
'*                  Note that colParams will be cleared within this method.
'* Purpose:     Stuffs param and variable elements within the style sheet.
'* Returns:     Zilch.
'********************************************************************************
Private Sub SetXslParams(ByRef objProc As MSXML2.IXSLProcessor, ByVal strList As String)
          Const PROC_NAME As String = MODULE_NAME & "SetXslParams: "

          Dim objNode As IXMLDOMNode

          Dim strKey As String
          Dim strItem As String
          Dim blnDebugMode As Boolean

10        On Error GoTo ErrorHandler

          'Get debug mode status.
20        blnDebugMode = mEvents.IsDebugMode

30        If blnDebugMode Then mEvents.Trace "", PROC_NAME & "Started"

          'Loop through the passed param list and search for params & variables.
40        While mcolXslParams.Count > 0

50            strKey = mcolXslParams.Keys(0)
60            strItem = mcolXslParams.Items(0)
70            mcolXslParams.Remove (strKey)
              
80            If InStr(strList, strKey) = 0 Then
              
                  'For now we allow params not found.  Mark it in the debug log instead.
90                If blnDebugMode Then mEvents.Trace "Key = " & strKey & "  Item = " & strItem & " : Could not find Param/Variable in Style sheet [just a warning, not an error condition].", PROC_NAME & "selectSingleNode"

                  'If we were going to raise an error...
                  'Err.Raise eSessionParameterNotFound, "selectSingleNode", "xsl:param/xsl:variable '" & strKey & "' not found in the style sheet."
                  
100           Else
              
110               If blnDebugMode Then mEvents.Trace "Key = " & strKey & "  Item = " & strItem & " : Found Param/Variable in Style sheet.", PROC_NAME & "selectSingleNode"
                  
120               objProc.addParameter strKey, strItem
                  
130           End If
              
140       Wend

150       If blnDebugMode Then mEvents.Trace "", PROC_NAME & "Finished"

ErrorHandler:

160       Set objNode = Nothing

170       If Err.Number <> 0 Then
180           mEvents.Env "WebSite=" & mEvents.WebRoot
190           mEvents.Env "Key=" & strKey
200           mEvents.Env "Item=" & strItem
210           mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
220       End If

End Sub

'********************************************************************************
'* Stuffs CRUD params within the style sheet.
'********************************************************************************
Private Sub SetCrudParams(ByRef objProc As MSXML2.IXSLProcessor, ByVal strList As String)
          Const PROC_NAME As String = MODULE_NAME & "SetCrudParams: "

          Dim domCRUD As MSXML2.DOMDocument40
          Dim objVarList As IXMLDOMNodeList
          Dim objVar As IXMLDOMNode

          Dim blnDebugMode As Boolean

          Dim strCRUD As String, strCrudSp As String
          Dim strCrudXML As String, strEntity As String, intIdx As Integer
          Dim strListItems() As String, strCruds() As String, strEntities() As String

10        On Error GoTo ErrorHandler

          'Get debug mode status.
20        blnDebugMode = mEvents.IsDebugMode

30        If blnDebugMode Then mEvents.Trace "", PROC_NAME & "Started"
          
          'We don't throw an error on this because we want to be able
          'to develop without session data being present.
40        If mstrUserId <> "" And strList <> "" Then
          
              'Split up the passed CRUD list into CRUDs and Entities.
              'There should be exactly 1 "|" in there somewhere.
50            strListItems = Split(strList, "|")
              
60            mEvents.Assert UBound(strListItems) = 1, "Must be exactly one '|' in the CRUD list (" & strList & ")."
              
              'Split up both the CRUDs and the Entities.  Compare the counts.
70            strCruds = Split(strListItems(0), ",")
80            strEntities = Split(strListItems(1), ",")
              
90            mEvents.Assert UBound(strCruds) = UBound(strEntities), "CRUD list and Entity list must be the same size (" & strList & ")."

              'Create a DOM document for the CRUD XML.
100           Set domCRUD = New MSXML2.DOMDocument40

              'Get the stored proc name to call.
110           strCrudSp = mEvents.mSettings.GetParsedSetting("Crud/SelectCrudSP")

              'Get the CRUD info in XML form from the database.
120           strCrudXML = GetDataAccessor().OpenRecordsetAsClientSideXML( _
                  mEvents.mSettings.GetParsedSetting("Crud/SelectCrudSP"), _
                  "'" & strListItems(1) & "'", mstrUserId)

              'Load it into the DOM with error checking.
130           LoadXml domCRUD, strCrudXML, PROC_NAME, "CRUD"

              'Loop through each requested entity and build up the CRUD string.
140           For intIdx = LBound(strEntities) To UBound(strEntities)
              
150               strEntity = strEntities(intIdx)
160               Set objVar = GetChildNode(domCRUD, "//Crud[@Entity='" & strEntity & "']")
              
170               strCRUD = IIf(objVar.Attributes.getNamedItem("Insert").Text = "1", "C", "_") _
                          & IIf(objVar.Attributes.getNamedItem("Select").Text = "1", "R", "_") _
                          & IIf(objVar.Attributes.getNamedItem("Update").Text = "1", "U", "_") _
                          & IIf(objVar.Attributes.getNamedItem("Delete").Text = "1", "D", "_")
                          
180               objProc.addParameter strCruds(intIdx), strCRUD
                  
190           Next
              
200           If blnDebugMode Then mEvents.Trace domCRUD.xml, PROC_NAME & "SelectCrudSP"

210       End If

220       If blnDebugMode Then mEvents.Trace "", PROC_NAME & "Finished"

ErrorHandler:

230       If Err.Number <> 0 Then
240           mEvents.Env "WebSite=" & mEvents.WebRoot
250           mEvents.Env "Entities=" & strList
260           mEvents.Env "UserID=" & mstrUserId
270           If Not domCRUD Is Nothing Then
280               mEvents.Env "CrudXML=" & domCRUD.xml
290           End If
300       End If

310       Set domCRUD = Nothing
320       Set objVarList = Nothing
330       Set objVar = Nothing

340       If Err.Number <> 0 Then
350           mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
360       End If
End Sub

'********************************************************************************
'* Syntax:      ParseParamsToSQL( Params )
'* Parameters:  Params - a Variant array.
'* Purpose:     Converts passed variant to a SQL parameter string as best it can.
'* Returns:     The SQL parameter string.
'********************************************************************************
Public Function ParseParamsToSQL(ByVal Params As Variant) As String
          Const PROC_NAME As String = MODULE_NAME & "ParseParamsToSQL: "

          Dim varParams As Variant
          Dim strValue As String
          Dim strSQL As String
          Dim i As Integer

10        On Error GoTo ErrorHandler

          'Fix any ParamArray pass-through problems.
20        If UBound(Params) = -1 Then
30            Exit Function
40        ElseIf (VarType(Params(0)) And vbArray) Then
50            varParams = Params(0)
60        Else
70            varParams = Params
80        End If

          'Convert the Variants to a SQL query string.
90        If UBound(varParams) <> -1 Then
100           For i = LBound(varParams) To UBound(varParams)
110               strValue = CStr(varParams(i))

                  'Blank values become NULL.
120               If Len(strValue) = 0 Then
130                   strSQL = strSQL & " NULL,"

                  'Already quoted value stay as is.
140               ElseIf (Left(strValue, 1) = "'") And (Right(strValue, 1) = "'") Then
150                   strSQL = strSQL & strValue & ","

                  'Escape out single quotes and wrap the whole with quotes.
160               Else
170                   strSQL = strSQL & " '" & SQLQueryString(strValue) & "',"
180               End If
190           Next
200           strSQL = Left(strSQL, Len(strSQL) - 1)
210       End If

220       If mEvents.IsDebugMode() Then mEvents.Trace "SQL Params = " & strSQL, PROC_NAME

230       ParseParamsToSQL = strSQL

ErrorHandler:

240       If Err.Number <> 0 Then
250           mEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, "SQL Params = " & strSQL
260       End If

End Function

