VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 4  'RequiresNewTransaction
END
Attribute VB_Name = "CTransaction"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* CTransaction
'*
'* We use a separate CTransaction class to avoid having to have separate
'* classes for transactional and non-transactional methods.  Use as follows:
'*
'* Dim objTrans As CTransaction, obj1 As SomeObj1, obj2 As SomeObj2
'* On Error GoTo ErrorHandler
'*
'*  Set objTrans = CreateObjectEx("CTransaction")
'*  Set obj1 = Trans.CreateInstance("SomeObject1ID")
'*  Set obj2 = Trans.CreateInstance("SomeObject2ID")
'*
'* ... Do Stuff ...
'*
'*  'NOTE: Do not use SetComplete.  The transaction
'*  'will commit when objTrans is destroyed.
'*  'objTrans.SetComplete 'Commit Transaction
'*
'* ... Exit Function ...
'*
'*  ErrorHandler:
'*    objTrans.SetAbort 'Abort Transaction
'*
'*  Set objTrans = Nothing
'*  Set obj1 = Nothing
'*  Set obj2 = Nothing
'*
'********************************************************************************
Option Explicit

'********************************************************************************
'* Syntax:      Set objTrans = CreateObjectEx("CTransaction")
'* Parameters:  strProgID - The ID of the object to create.
'* Purpose:     Use this method to create all objects that you want
'*              wrapped in the transaction.
'* Returns:     The created Object.
'********************************************************************************
Public Function CreateInstance(ByVal strProgID As String) As Object
10        On Error GoTo ErrorHandler

20        Set CreateInstance = CreateObject(strProgID)

30        Exit Function
ErrorHandler:
40        Err.Raise Err.Number, Err.Source, Err.Description & _
              ": CTransaction.CreateInstance('" & strProgID & "')"
End Function

'********************************************************************************
'* Syntax:      SetComplete()
'* Parameters:  None
'* Purpose:     Called during cleanup to mark the transaction as complete.
'* Returns:     Zilch
'********************************************************************************
Public Sub SetComplete()
          Dim objCtx As Object
10        Set objCtx = GetObjectContext

20        If Not objCtx Is Nothing Then
30            objCtx.SetComplete
40            Set objCtx = Nothing
50        End If
End Sub

'********************************************************************************
'* Syntax:      SetAbort()
'* Parameters:  None
'* Purpose:     Called during error handlers to abort the transaction.
'* Returns:     Zippo
'********************************************************************************
Public Sub SetAbort()
          Dim objCtx As Object
10        Set objCtx = GetObjectContext

20        If Not objCtx Is Nothing Then
30            objCtx.SetAbort
40            Set objCtx = Nothing
50        End If
End Sub


