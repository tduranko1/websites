using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Threading;
using System.Diagnostics;
using System.Xml;
using System.Net;

namespace APDDocumentPrinter
{
   class Progam
   {
      static string strMachineName = "";
      static string strEnv = "";
      static clsXML configXML = new clsXML();
      static string strConfigPath;
      static string strMonitorPath = "";
      static string strPrinterName = "";
      static string strLastNotify = "";
      static DateTime dtLastNotify;
      static string strSMPTHost = "";
      static string strNotifyEmailFrom = "";
      static string strNotifyEmailTo = "";
      static string strNotifyEmailSubject = "";
      static string strNotifyEmailBody = "";
      static string strMailroomTo = "";

      static string strAcroRd32Path = "";
      static string strHTTPPostURL = "";
      static string strHTTPPostTemplateXML = "";
      static string strArchivePath = "";
      static string strArchiveTTLDays = "";
      static Dictionary<int, string> arrFiles = new Dictionary<int, string>();
      static Int32 intFilesFound;
      static Int32 intFilesProcessed;
      static StringBuilder sbBatchData = new StringBuilder();

      static void Main(string[] args)
      {
         //command line options
         if (args.Length > 0) strEnv = args[0];

         if (strEnv == "") strEnv = "DEV";

         strMachineName = Environment.MachineName;

         // config path
         strConfigPath = Environment.CurrentDirectory;

         writeLog("***** Application start " + DateTime.Now.ToString() + " *****\nLoading configuration file...");

         //load the configuration file
         if (configXML.loadConfig(Environment.CurrentDirectory + "\\config.xml"))
         {
            configXML.environment = strEnv;

            //load the settings
            strMonitorPath = configXML.getSetting("Monitor", "path");
            strLastNotify = configXML.getSetting("/Monitor", "lastrun");

            if (strLastNotify != "" && IsDate(strLastNotify) == true)
            {
               dtLastNotify = System.DateTime.Parse(strLastNotify);
            }

            strPrinterName = configXML.getSetting("Printer", "name");
            strAcroRd32Path = configXML.getSetting("AcroRd32", "path");

            strSMPTHost = configXML.getSetting("Notify", "SMTPServer");
            strNotifyEmailFrom = configXML.getSetting("Notify/Email", "from");
            strNotifyEmailTo = configXML.getSetting("Notify/Email", "to");
            strNotifyEmailSubject = configXML.getSetting("Notify/Email", "subject");
            strNotifyEmailBody = configXML.getSetting("Notify/Email");
            strMailroomTo = configXML.getSetting("Notify/Mailroom", "to");

            strHTTPPostURL = configXML.getSetting("HTTPPost/url");
            strHTTPPostTemplateXML = configXML.getSetting("HTTPPost/PostTemplate");
            strArchiveTTLDays = configXML.getSetting("Archive", "ttl_days");

            writeLog("Environment: " + strEnv + "\n" +
                     "Monitor Path: " + strMonitorPath + "\n" +
                     "Last Run: " + strLastNotify + "\n" +
                     "Last Run (date): " + dtLastNotify.ToString() + "\n" +
                     "Printer: " + strPrinterName + "\n" + 
                     "SMTP Host: " + strSMPTHost + "\n" +
                     "  Notify From: " + strNotifyEmailFrom + "\n" +
                     "  Notify To: " + strNotifyEmailTo + "\n" +
                     "  Notify Subject: " + strNotifyEmailSubject + "\n" +
                     "  Notify Body: " + strNotifyEmailBody + "\n" + 
                     "  Mailroom To: " + strMailroomTo + "\n" +
                     "AcroRd.exe path: " + strAcroRd32Path + "\n" +
                     "HTTP Post URL: " + strHTTPPostURL + "\n" + 
                     "HTTP Post Template XML: " + strHTTPPostTemplateXML + "\n" +
                     "Archive TTL Days: " + strArchiveTTLDays + "\n\n");

            if (System.IO.Directory.Exists(strMonitorPath))
            {
               if (validate() == true)
               {
                  //process folder
                  writeLog("Processing files in the Monitor Path...");
                  try
                  {
                     if (processFiles() == true)
                     {
                        sendEmail("APD Document Printer completed successfully." +
                                  "\nApplication executed from: " + strMachineName +
                                  "\nEnvironment: " + strEnv + 
                                  "\nMonitor Path: " + strMonitorPath + 
                                  "\nFiles Found: " + Convert.ToString(intFilesFound) +
                                  "\nFiles Processed: " + Convert.ToString(intFilesProcessed));
                     }
                     else
                     {
                        sendEmail("APD Document Printer failed." +
                                  "\nApplication executed from: " + strMachineName +
                                  "\nEnvironment: " + strEnv +
                                  "\nMonitor Path: " + strMonitorPath +
                                  "\nFiles Found: " + Convert.ToString(intFilesFound) +
                                  "\nFiles Processed: " + Convert.ToString(intFilesProcessed));
                     }

                     if (intFilesProcessed > 0)
                     {
                        sbBatchData.AppendLine("___ [" + Convert.ToString(intFilesProcessed) + "] TOTAL NOTIFICATIONS IN BATCH\n");
                        emailBatchHeader();
                     }

                     archiveCleanup();

                     //save the current run
                     configXML.setSetting("Monitor", DateTime.Now.ToString(), "lastrun");
                     configXML.saveConfig();
                  }
                  catch (System.Exception ex)
                  {
                     writeLog("ERROR: \n" +
                                       "  Source: " + ex.Source + "\n" +
                                       "  Message: " + ex.Message);
                  }
               }
            }
            else
            {
               //myLog.WriteEntry("APD Document Printer: Monitor path does not exist [" + strMonitorPath + "]", EventLogEntryType.Error);
               writeLog("APD Document Printer: ERROR: Monitor path does not exist [" + strMonitorPath + "]");
            }
         }
         else
         {
            //myLog.WriteEntry("APD Document Printer: Error loading [" + strMonitorPath + "]", EventLogEntryType.Error);
            writeLog("APD Document Printer: ERROR: Error loading [" + strMonitorPath + "]");
         }

         writeLog("***** Application shutdown *****\n\n");
      }

      private static void archiveCleanup()
      {
         if (strArchiveTTLDays != "" && strArchivePath != "")
         {
            int intArchiveTTLDays;
            try
            {
               intArchiveTTLDays = -1 * Convert.ToInt32(strArchiveTTLDays);
            }
            catch (Exception e)
            {
               //default the time to live days to 30.
               intArchiveTTLDays = -30;
            }

            if (Directory.Exists(strArchivePath) == true)
            {
               string[] strFiles = Directory.GetFiles(strArchivePath);
               DateTime fileModified;
               DateTime thresholdDate = DateTime.Now.AddDays(intArchiveTTLDays);
               foreach (string strFile in strFiles)
               {
                  fileModified = File.GetLastWriteTime(strFile);
                  if (fileModified <= thresholdDate)
                  {
                     File.Delete(strFile);
                  }
               }
            }
         }
      }

      static bool validate()
      {
         //validate the settings
         if (strMonitorPath == "" || Directory.Exists(strMonitorPath) == false)
         {
            writeLog("Monitor path is incorrect or does not exist.");
            return false;
         }

         if (strPrinterName == "") 
         {
            writeLog("Printer name is incorrect.");
            return false;
         }

         if (File.Exists(strAcroRd32Path) == false)
         {
            writeLog(strAcroRd32Path + " does not exist.");
            return false;
         }

         strArchivePath = strMonitorPath + "\\Archive";
         return true;
      }

      static bool processFiles()
      {
         string[] strFiles = Directory.GetFiles(strMonitorPath);
         //Int16 intCount = 0;
         intFilesFound = 0;
         intFilesProcessed = 0;

         foreach (string strFile in strFiles)
         {
            FileSystemInfo fsi = new FileInfo(strFile);
            if (fsi.Extension == ".pdf")
            {
               arrFiles.Add(intFilesFound, fsi.FullName);
               intFilesFound++;
               //processFile(fsi.FullName);
            }
         }

         writeLog("  Number of pdf files in " + strMonitorPath + ": " + intFilesFound + "\n");

         if (intFilesFound > 0)
         {
            sbBatchData.AppendLine(String.Format("    {0,-21} {1,-31} {2,-8} {3,-2}", "Policy Number", "Claim Number", "Lynx ID", "Veh#"));
            sbBatchData.AppendLine("");
         }

         for (int i = 0; i < arrFiles.Count; i++)
         {
            if (processFile(arrFiles[i]) == false)
            {
               return false;
            }
            intFilesProcessed++;
            writeLog("\n");
         }

         return true;

      }

      static Boolean processFile(string strFileName)
      {
          if (printFile(strFileName) == false)
          {
              emailNotify();
              return false;
          }
         if (postFile(strFileName) == false)
         {
            emailNotify();
            return false;
         }

         if (archiveFile(strFileName) == false)
         {
            File.Delete(strFileName);
         }

         FileInfo fi = new FileInfo(strFileName);
         string strControlFile = strFileName.Replace(fi.Extension, ".xml");
         if (File.Exists(strControlFile) == true)
         {
            if (archiveFile(strControlFile) == false)
            {
               File.Delete(strFileName);
            }
         }

         return true;
      }

      private static bool archiveFile(string strFileName)
      {
         try
         {
            if (Directory.Exists(strArchivePath) == false)
            {
               Directory.CreateDirectory(strArchivePath);
               writeLog("Archive folder " + strArchivePath + " created.");
            }

            FileInfo fi = new FileInfo(strFileName);
            string strDestinationFile = strArchivePath + "\\" + fi.Name;
            if (File.Exists(strDestinationFile) == true)
            {
               File.Delete(strDestinationFile);
            }
            File.Move(strFileName, strDestinationFile);
            writeLog(strFileName + " archived to " + strDestinationFile);
         }
         catch (Exception e)
         {
            writeLog("ArchiveFile failed. " + e.Message);
         }
         return true;
      }

      static Boolean printFile(string strFileName)
      {
         writeLog(string.Format("Printing file {0} to {1}", strFileName, strPrinterName));
         if (RunExecutable(strAcroRd32Path, string.Format(@"/t ""{0}"" ""{1}"" ", strFileName, strPrinterName)) == true)
         {
            writeLog("Print successful.");
            return true;
         }
         else
         {
            writeLog("Print failed.");
            return false;
         }
      }

      static Boolean postFile(string strFileName) 
      {
         string strAPDDataFile = strFileName.Replace(".pdf", ".xml");
         
         if (strHTTPPostURL == "" || strHTTPPostURL.IndexOf("http") == -1)
         {
            writeLog("Invalid URL specified: " + strHTTPPostURL);
            return false;
         }

         if (File.Exists(strAPDDataFile) == true)
         {
            XmlDocument xmlData = new XmlDocument();
            xmlData.Load(strAPDDataFile);

            string strLynxID = "";
            string strVehNum = "";
            string strClaimAspectServiceChannelID = "";
            string strDirectionCD = "";
            string strDocumentType = "";
            string strUserID = "";
            string strPolicyNumber = "";
            string strClaimNumber = "";

            XmlElement xmlNode;

            xmlNode = (XmlElement)xmlData.SelectSingleNode("//APD");
            if (xmlNode != null)
            {
               strLynxID = xmlNode.GetAttribute("Lynxid").ToString();
               strVehNum = xmlNode.GetAttribute("vehNum").ToString();
               strClaimAspectServiceChannelID = xmlNode.GetAttribute("ClaimAspectServiceChannelID").ToString();
               strDirectionCD = xmlNode.GetAttribute("direction").ToString();
               strDocumentType = xmlNode.GetAttribute("documenttype").ToString();
               strUserID = xmlNode.GetAttribute("userid").ToString();
               strPolicyNumber = xmlNode.GetAttribute("PolicyNumber").ToString();
               strClaimNumber = xmlNode.GetAttribute("ClientClaimNumber").ToString();
            }


            XmlDocument xmlPostData = new XmlDocument();
            xmlPostData.LoadXml(strHTTPPostTemplateXML);

            XmlElement xmlRoot = (XmlElement)xmlPostData.SelectSingleNode("root");

            if (xmlRoot != null)
            {
               xmlRoot.SetAttribute("LynxID", strLynxID);
               xmlRoot.SetAttribute("PertainsTo", "veh" + strVehNum);
               xmlRoot.SetAttribute("ClaimAspectServiceChannelID", strClaimAspectServiceChannelID);
               xmlRoot.SetAttribute("UserID", strUserID);
            }

            StringBuilder sb = new StringBuilder();
            int bufferSize = 1000;
            byte[] buffer = new byte[bufferSize];
            int readBytes = 0;
            using (XmlWriter writer = XmlWriter.Create(sb))
            {
               System.IO.FileInfo fileInfo = new FileInfo(strFileName);

               FileStream inputFile = new FileStream(strFileName, FileMode.Open, FileAccess.Read, FileShare.Read);
               BinaryReader br = new BinaryReader(inputFile);

               writer.WriteStartDocument();
               writer.WriteStartElement("File");
               writer.WriteAttributeString("FileName", fileInfo.Name.Replace(fileInfo.Extension, ""));
               writer.WriteAttributeString("FileExt", fileInfo.Extension.Replace(".", ""));
               writer.WriteAttributeString("FileLength", fileInfo.Length.ToString());
               writer.WriteAttributeString("key", "");


               do
               {
                  readBytes = br.Read(buffer, 0, bufferSize);
                  writer.WriteBase64(buffer, 0, readBytes);
               } while (bufferSize <= readBytes);

               br.Close();

               writer.WriteEndElement();
               writer.Flush();
            }

            XmlDocument xmlAttachment = new XmlDocument();
            xmlAttachment.LoadXml(sb.ToString());

            try
            {
               XmlElement xmlBase64File = (XmlElement)xmlAttachment.SelectSingleNode("//File");
               if (xmlBase64File != null)
               {
                  XmlElement xmlFile = (XmlElement)xmlPostData.SelectSingleNode("/root/File");
                  xmlFile.SetAttribute("FileName", xmlBase64File.GetAttribute("FileName"));
                  xmlFile.SetAttribute("FileExt", xmlBase64File.GetAttribute("FileExt"));
                  xmlFile.SetAttribute("FileLength", xmlBase64File.GetAttribute("FileLength"));
                  xmlFile.SetAttribute("key", "$JOBDOCUMENT$");
                  xmlFile.SetAttribute("xmlns:dt", "urn:schemas-microsoft-com:datatypes");
                  xmlFile.SetAttribute("_dt", "bin.base64");
                  xmlFile.InnerText = xmlBase64File.InnerText;
               }
            }
            catch (Exception e)
            {
               writeLog(e.Message);
            }

            try
            {
               HttpWebRequest request = null;
               HttpWebResponse response = null;
               Stream requestStream = null;
               Stream responseStream = null;
               ASCIIEncoding encoder = new ASCIIEncoding();

               //work around to make the attribute include a namespace that is compatable with bin.base64
               string strXML = xmlPostData.DocumentElement.OuterXml;
               strXML = strXML.Replace(@"_dt=""", @"dt:dt=""");

               byte[] requestBuffer = encoder.GetBytes(strXML);

               request = (HttpWebRequest)WebRequest.Create(strHTTPPostURL);
               request.Method = "POST";
               request.ContentLength = strXML.Length;
               requestStream = request.GetRequestStream();
               requestStream.Write(requestBuffer, 0, requestBuffer.Length);
               requestStream.Close();

               response = (HttpWebResponse)request.GetResponse();
               responseStream = response.GetResponseStream();
               writeLog("Status Code: " + response.StatusCode + "\n" + responseStream.ToString());
               if (response.StatusCode != HttpStatusCode.OK)
               {
                  writeLog("HTTP Post failed.\n" +
                            "Code: " + response.StatusCode + "\n" +
                            "Description: " + responseStream.ToString());
                  return false;
               }

               sbBatchData.AppendLine(String.Format("___ {0,-21} {1,-31} {2,-8} {3,-2}", strPolicyNumber, strClaimNumber, strLynxID, strVehNum));
               sbBatchData.AppendLine("");
            }
            catch (Exception e)
            {
               writeLog("HTTP Post failed.\n" +
                        "Description: " + e.Message);

               return false;
            }


            writeLog("Posting file " + strFileName);

            return true;
         }

         return true;
      }

      static void sendEmail(string strEmailBody)
      {
         string strAttachments = "";
         MailMessage msg = new MailMessage();
         MailAddress addFrom = new MailAddress(strNotifyEmailFrom);

         writeLog("***** Email Notification " + DateTime.Now.ToString() + "*****\n");

         msg.From = addFrom;
         if (strNotifyEmailTo.IndexOf(";") > 0)
         {
            string strDelimiter = ", :;";
            char[] delimiters = strDelimiter.ToCharArray();
            string[] arrMailTo = strNotifyEmailTo.Split(delimiters);
            int intCount = arrMailTo.Length;
            for (int i = 0; i < intCount; i++)
            {
               if (arrMailTo[i].Trim() != "")
               {
                  msg.To.Add(new MailAddress(arrMailTo[i]));
                  writeLog("   To: " + arrMailTo[i]);
               }
            }
         }
         else
         {
            msg.To.Add(new MailAddress(strNotifyEmailTo));
            writeLog("   To: " + strNotifyEmailTo);
         }

         msg.Subject = (string)strNotifyEmailSubject;
         msg.Body = strEmailBody;

         SmtpClient mailer = new SmtpClient(strSMPTHost);
         mailer.Send(msg);

         //myLog.WriteEntry("APD Document Printer: Sent email with " + iFileCount + " attachments.\n" + strAttachments, EventLogEntryType.Information);
         writeLog("APD Document Printer: Email Sent");

         configXML.setSetting("Monitor", DateTime.Now.ToString(), "lastnotify");

      }

      static void writeLog(string strMessage)
      {
         string strLogFile = Environment.CurrentDirectory + "\\" + DateTime.Now.ToString("yyyy-MM") + ".log";
         TextWriter tw = new StreamWriter(strLogFile, true);
         tw.WriteLine(strMessage);
         tw.Close();

         Console.WriteLine(strMessage);
      }

      public static Boolean IsDate(Object obj)
      {
         string strDate = obj.ToString();
         try
         {
            DateTime dt = DateTime.Parse(strDate);
            if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
               return true;
            return false;
         }
         catch
         {
            return false;
         }
      }

      private static Boolean RunExecutable(string executable, string arguments) 
      {
         try
         {
            ProcessStartInfo starter = new ProcessStartInfo(executable, arguments);
            starter.CreateNoWindow = true;
            starter.RedirectStandardOutput = true;
            starter.UseShellExecute = false;
            Process process = new Process();
            process.StartInfo = starter;
            process.Start();
            //Thread.Sleep(15000);
            process.WaitForExit();
            //process.WaitForInputIdle();
            //process.CloseMainWindow();
            //wait for the thread to shutdown
            //Thread.Sleep(10000);
            if (process.ExitCode != 0)
            {
               //throw new Exception(string.Format(@"""{0}"" exited with ExitCode {1}. Output: {2}", executable, process.ExitCode, buffer.ToString()));
               return false;
            }
            else
            {
               return true;
            }
         }
         catch (Exception e)
         {
            writeLog("RunExecutable Error: " + e.Message + "\nStack Trace: " + e.StackTrace);
            return false;
         }
      }   

      private static void emailNotify()
      {
         
      }

      private static void emailBatchHeader()
      {
         MailMessage msg = new MailMessage();
         MailAddress addFrom = new MailAddress(strNotifyEmailFrom);

         writeLog("***** Mailroom Notification " + DateTime.Now.ToString() + "*****\n");

         msg.From = addFrom;
         if (strMailroomTo.IndexOf(";") > 0)
         {
            string strDelimiter = ", :;";
            char[] delimiters = strDelimiter.ToCharArray();
            string[] arrMailTo = strMailroomTo.Split(delimiters);
            int intCount = arrMailTo.Length;
            
            writeLog("Mail To count: " + intCount.ToString());

            for (int i = 0; i < intCount; i++)
            {
               if (arrMailTo[i].Trim() != "")
               {
                  writeLog("Adding email to: " + arrMailTo[i]);
                  msg.To.Add(new MailAddress(arrMailTo[i]));
               }
            }
         }
         else
         {
            writeLog("Email to: " + strMailroomTo);
            msg.To.Add(new MailAddress(strMailroomTo));
         }

         msg.Subject = "APD Document Notification Batch Header";
         msg.Body = "APD Document Notification Batch Header\n" +
                     "Batch Control Header/Report\n\n" +
                     "Batch ID: " + DateTime.Now.ToString("yyyymmdd") + "\n\n" +
                     "Batch Mailing Date: " + DateTime.Now.ToString("d") + "\n\n" +
                     "Please check against the following, to confirm the following batch of notifications were mailed.\n\n" +
                     sbBatchData.ToString() + "\n\n" +
                     "Acknowledgement of Mailing\n\n" +
                     "The above listed notifications have been mailed via USPS on the mailing date shown above.\n\n" +
                     "LYNX Services Mailroom\n\n\n" +
                     "_____________________________\n" +
                     "Authorized Signature\n\n" +
                     "________________\n" +
                     "Date\n\n\n" +
                     "Please forward this completed report to:\n\n" +
                     "APD Operations Support\n" +
                     "[Auto-Owners APD]\n[aoiapd@lynxservices.com]";

         SmtpClient mailer = new SmtpClient(strSMPTHost);
         mailer.Send(msg);

         //myLog.WriteEntry("APD Document Printer: Sent email with " + iFileCount + " attachments.\n" + strAttachments, EventLogEntryType.Information);
         writeLog("APD Document Printer: Mailroom Email Sent");
      }
   }
}
