using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace APDDocumentPrinter
{
   class clsXML
   {
      private XmlDocument xmlConfig;
      private Boolean blnConfigLoaded = false;
      private string strConfig = "";
      private string _environment = "";

      public string environment {
         get {
            return _environment;
         }

         set {
            _environment = value;
         }
      }

      public Boolean loadConfig(string strConfigPath)
      {
         if (File.Exists(strConfigPath))
         {
            //config file exists. load it
            xmlConfig = new XmlDocument();
            try
            {
               xmlConfig.Load(strConfigPath);
               blnConfigLoaded = true;
               strConfig = strConfigPath;
            }
            catch (XmlException ex)
            {
               //catch any exception with the xml
               Console.WriteLine("APD Document Printer:loadConfig()\n" + "Error in config file " + strConfigPath + " [Line# " + ex.LineNumber + "] Message: " + ex.Message);
            }

         }

         return blnConfigLoaded;
      }

      public string getSetting(string strXPath, string strAttrib)
      {
         string strReturn = "";

         if (strXPath != "" && blnConfigLoaded == true)
         {
            strXPath = "//Environment[@id='" + environment + "']/" + strXPath;
            XmlElement nde = (XmlElement)xmlConfig.SelectSingleNode(strXPath);
            if (nde != null)
            {
               if (strAttrib != "")
               {
                  strReturn = nde.GetAttribute(strAttrib);
               }
               else
               {
                  strReturn = nde.InnerText;
               }
            }
         }

         return strReturn;
      }

      public string getSetting(string strXPath)
      {
         return getSetting(strXPath, "");
      }

      public void setSetting(string strXPath, string strValue, string strAttrib)
      {
         string strReturn = "";

         if (strXPath != "" && blnConfigLoaded == true)
         {
            XmlElement nde = (XmlElement)xmlConfig.SelectSingleNode(strXPath);
            if (nde != null)
            {
               if (strAttrib != "")
               {
                  nde.SetAttribute(strAttrib, strValue);
               }
               else
               {
                  nde.InnerText = strValue;
               }
            }
         }
      }

      public void setSetting(string strXPath, string strValue)
      {
         setSetting(strXPath, strValue, "");
      }

      public void saveConfig()
      {
         if (blnConfigLoaded)
            xmlConfig.Save(strConfig);
      }
   }
}
