VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDocument"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Component LAPDEcadAccessor: Class CDocument
'*
'* LYNX APD clients (Insurance Companies) require the ability to view
'* information about their claims. This information will be accessible from
'* the LYNX Services web site. For the purpose of this document, this web
'* site will be known as Client Access.
'*
'* LYNX APD Client Access will be hosted and developed by E-Commerce
'* Application Development (ECAD). Initially the site will be read-only and
'* simply provide Insurance companies access to claim information.
'*
'* This component is used to return binary documents to ECAD.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CDocument."

'********************************************************************************
'* Backup global termination
'********************************************************************************
Private Sub Class_Terminate()
10        TerminateGlobals
End Sub

'********************************************************************************
'* Syntax:      arrByte = GetDocument("<Document ImageLocation="c:\temp.tif"/>")
'*
'* Parameters:  strParamXML - the only parameter, in XML format.
'*
'* Purpose:     Retrieves an APD document in binary form of a byte array.
'*              This is for all documents except CCC print images.
'*
'* Returns:     The byte array containing the binary data.
'*
'********************************************************************************
Public Function GetDocument(ByVal strParamXML As String) As Variant
          Const PROC_NAME As String = MODULE_NAME & "GetDocument: "

          Dim objMgr As Object
          Dim blnDebugMode As Boolean
          
10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Get config debug mode status.
30        blnDebugMode = mobjEvents.IsDebugMode

          'Check passed parameters.
40        mobjEvents.Assert Len(strParamXML) > 0, "Nothing passed for strParamXML."

          'Add a trace mesage to the debug log.
50        If blnDebugMode Then mobjEvents.Trace "Param XML = '" & strParamXML & "'", PROC_NAME & "Started"
          
          'Create and call LAPDEcadAccessorMgr to do the real work.
60        Set objMgr = CreateObjectEx("LAPDEcadAccessorMgr.CDocument")
          
70        GetDocument = objMgr.GetDocument(strParamXML)
          
          'Add another trace message to note exit?
80        If blnDebugMode Then mobjEvents.Trace "", PROC_NAME & "Finished"
          
ErrorHandler:

90        Set objMgr = Nothing
          
100       If Err.Number <> 0 Then
110           mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "Param XML = '" & strParamXML & "'"
120       End If
              
End Function

'********************************************************************************
'* Syntax:      strXML = GetDocumentXML("<Document ImageLocation="c:\temp.tif"/>")
'*
'* Parameters:  strParamXML - the only parameter, in XML format.
'*
'* Purpose:     Retrieves an XML APD document as a string.
'*              Currently this document is CCC print images only, but this may
'*              change in the future.
'*
'* Returns:     The XML.
'*
'********************************************************************************
Public Function GetDocumentXML(ByVal strParamXML As String) As String
          Const PROC_NAME As String = MODULE_NAME & "GetDocumentXML: "

          Dim objMgr As Object
          Dim blnDebugMode As Boolean
          
10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Get config debug mode status.
30        blnDebugMode = mobjEvents.IsDebugMode

          'Check passed parameters.
40        mobjEvents.Assert Len(strParamXML) > 0, "Nothing passed for strParamXML."

          'Add a trace mesage to the debug log.
50        If blnDebugMode Then mobjEvents.Trace "Param XML = '" & strParamXML & "'", PROC_NAME & "Started"
          
          'Create and call LAPDEcadAccessorMgr to do the real work.
60        Set objMgr = CreateObjectEx("LAPDEcadAccessorMgr.CDocument")
          
70        GetDocumentXML = objMgr.GetDocumentXML(strParamXML)
          
          'Add another trace message to note exit?
80        If blnDebugMode Then mobjEvents.Trace "Return = " & GetDocumentXML, PROC_NAME & "Finished"
          
ErrorHandler:

90        Set objMgr = Nothing
          
100       If Err.Number <> 0 Then
110           mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "Param XML = '" & strParamXML & "'"
120       End If
              
End Function

