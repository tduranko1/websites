Attribute VB_Name = "MLAPDEcadAccessor"
'********************************************************************************
'*
'* Component LAPDEcadAccessor : Module MLAPDEcadAccessor
'*
'* Global Consts and Utility Methods
'*
'********************************************************************************
Option Explicit

Public Const APP_NAME As String = "LAPDEcadAccessor."

Public Const LAPDEcadAccessor_FirstError As Long = &H80066000

'Error codes specific to LAPDEcadAccessor
Public Enum EcadAccessorErrors
    eCreateObjectExError = LAPDEcadAccessor_FirstError + &H100
End Enum

'********************************************************************************
'* Global events and data access objects.
'********************************************************************************
Public mobjEvents As SiteUtilities.CEvents

'********************************************************************************
'* Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
'* Parameters:  strObjectName = The object to call CreateObject for.
'* Purpose:     Wraps CreateObject() with better debug information.
'* Returns:     The Object created or Nothing.
'********************************************************************************
Public Function CreateObjectEx(strObjectName) As Object
          Dim obj As Object
          
10        On Error GoTo ErrorHandler
          
20        Set CreateObjectEx = Nothing
          
30        Set obj = CreateObject(strObjectName)
          
40        If obj Is Nothing Then
50            Err.Raise eCreateObjectExError, "", "CreateObject('" & strObjectName & "') returned Nothing."
60        End If
          
70        Set CreateObjectEx = obj
          
ErrorHandler:

80        Set obj = Nothing
          
90        If Err.Number <> 0 Then
100           Err.Raise Err.Number, "CreateObjectEx('" & strObjectName & "') " & Err.Source, Err.Description & " " & strObjectName
110       End If
          
End Function

'********************************************************************************
'* Initializes global objects and variables
'********************************************************************************
Public Sub InitializeGlobals()

          'Create events object.
10        Set mobjEvents = New SiteUtilities.CEvents

          'This will give partner data its own log file.
20        mobjEvents.ComponentInstance = "Client Access"

          'Initialize our member components first, so we have logging set up.
30        mobjEvents.Initialize App.Path & "\..\config\config.xml"

End Sub

'********************************************************************************
'* Terminates global objects and variables
'********************************************************************************
Public Sub TerminateGlobals()
10        Set mobjEvents = Nothing
End Sub

'********************************************************************************
'* Returns the requested configuration setting.
'********************************************************************************
Public Function GetConfig(ByVal strSetting As String)
10        GetConfig = mobjEvents.mSettings.GetParsedSetting(strSetting)
End Function


