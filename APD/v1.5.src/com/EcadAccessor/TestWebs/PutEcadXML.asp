<%@ Language=VBScript %>
<% Option Explicit %>
<%
    '********************************************************************************
    '* PutEcadXML.asp
    '*
    '* This ASP demonstrates and tests the PutXML method of the CData class
    '* module of the LAPDEcadAccessor component.
    '*
    '* jcarpenter@lynxservices.com
    '********************************************************************************

	On Error Resume Next

    Dim strResult, strParamXML, strEntityIdx

    Call PutEcadXML

    If Err.Number <> 0 Then
        strResult = Err.Description
    End If

    Sub PutEcadXML()

        On Error GoTo 0

        strResult = ""
        strEntityIdx = Request("selEntityIdx")
        strParamXML = Request("txtParamXML")

        If Len(strParamXML) = 0 Then
            strParamXML = Request("selEntity")
        End If

        If Len(strParamXML)>0 Then

            Dim objEcad
            Set objEcad = CreateObject("LAPDEcadAccessor.CData")

            objEcad.PutXML( strParamXML )
            strResult = "Success!"

            Set objEcad = Nothing

        Else

            strEntityIdx = "0"
            strParamXML = ""

        End If

    End Sub
%>

<html>
<head>
    <script language="JavaScript">

        // Select the last chosen combo box item.
        function InitForm()
        {
            Form1.selEntity.selectedIndex = <%= strEntityIdx %>;
        }

        // Save the currently selected combo box item index before submitting.
        function SubmitForm( combo )
        {
            if ( combo == true )
                Form1.txtParamXML.value = Form1.selEntity.value;
            Form1.selEntityIdx.value = Form1.selEntity.selectedIndex;
            Form1.submit();
        }

    </script>
</head>
<body OnLoad="InitForm();">
    <strong>LYNX / ECAD Accessor Test for CData.PutXML()</strong>
    <form name="Form1" method="POST" action="PutEcadXML.asp">
		<table width="%100">
			<tr>
                <td>Entity<br>
                    <select name="selEntity" onChange="SubmitForm(true)">
                        <option value='' selected>Select an entity</option>
                        <option value='<SubmitComment InsuranceCompanyID="145" LynxID="6705" UserName="Shannon Domico" UserEmail="sdomico@ppg.com" Comment="I have reviewed the claim and I have some serious issues with using remanufactured GM parts on a 2001 Porche Boxter.  Please contact me immediately."/>'>SubmitComment</option>
                    </select>
                    <input name="selEntityIdx" type="hidden"/>
                    <input name="selRetry" type="button" value="Retry" onClick="SubmitForm(false)"/>
                </td>
			</tr>
            <tr>
                <td>Param&nbsp;XML<br>
                    <textarea name="txtParamXML" wrap="on" rows="4" cols="100"><%= strParamXML %></textarea>
                </td>
            </tr>
			<tr>
                <td>Result<br>
                    <textarea name="txtResult" wrap="on" rows="20" cols="100"><%= strResult %></textarea>
                </td>
			</tr>
		</table>
	</form>
</body>
</html>
