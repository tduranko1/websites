<%@ Language=VBScript %>
<% Option Explicit %>
<%
    '********************************************************************************
    '* GetEcadXML.asp
    '*
    '* This ASP demonstrates and tests the GetXML method of the CData class
    '* module of the LAPDEcadAccessor component.
    '*
    '* jcarpenter@lynxservices.com
    '********************************************************************************

	On Error Resume Next

    Dim strResultXML, strParamXML, strEntityIdx

    Call GetEcadXML

    If Err.Number <> 0 Then
        strResultXML = Err.Description
    End If

    Sub GetEcadXML()

        On Error GoTo 0

        'Extract the query data.

        strResultXML = ""
        strEntityIdx = Request("selEntityIdx")
        strParamXML = Request("txtParamXML")

        If Len(strParamXML) = 0 Then
            strParamXML = Request("selEntity")
        End If

        If Len(strParamXML)>0 Then

            Const strTab = "   "

            '********************************************************
            '* Retrieve the XML through LAPDEcadAccessor.

            Dim objEcad
            Set objEcad = CreateObject("LAPDEcadAccessor.CData")

            strResultXML = objEcad.GetXML( strParamXML )

            Set objEcad = Nothing

            '********************************************************
            '* Format the XML for friendlier viewing.

            Dim strIndent
            Dim str
            Dim strFinal
            Dim bDie

            str = strResultXML
            bDie = False

            While ( ( Len(str) > 1 ) And ( Left(str,1) = "<" ) And Not bDie )

                Dim bClose
                bClose = CBool( Mid(str,2,1) = "/" )

                Dim nEnd
                nEnd = InStr(1,str,">",1)

                If ( nEnd = -1 ) Then
                    bDie = True
                Else

                    Dim bSelfClose
                    bSelfClose = CBool( Mid(str,nEnd - 1, 1) = "/" )

                    If bClose And bSelfClose Then
                        bDie = True
                    Else

                        If bClose Then
                            strIndent = Left( strIndent, Len(strIndent) - Len(strTab) )
                            strFinal = strFinal & strIndent & Left( str, nEnd ) & vbCrLf
                        ElseIf Not bSelfClose Then
                            strFinal = strFinal & strIndent & Left( str, nEnd ) & vbCrLf
                            strIndent = strIndent & strTab
                        Else
                            strFinal = strFinal & strIndent & Left( str, nEnd ) & vbCrLf
                        End If

                        str = Mid( str, nEnd + 1 )
                    End If

                End If

            WEnd

            strResultXML = strFinal

        Else

            strEntityIdx = "0"
            strParamXML = ""
            strResultXML = ""

        End If

    End Sub
%>

<html>
<head>
    <script language="JavaScript">

        // Select the last chosen combo box item.
        function InitForm()
        {
            Form1.selEntity.selectedIndex = <%= strEntityIdx %>;
        }

        // Save the currently selected combo box item index before submitting.
        function SubmitForm( combo )
        {
            if ( combo == true )
                Form1.txtParamXML.value = Form1.selEntity.value;
            Form1.selEntityIdx.value = Form1.selEntity.selectedIndex;
            Form1.submit();
        }

    </script>
</head>
<body OnLoad="InitForm();">
    <strong>LYNX / ECAD Accessor Test for CData.GetXML()</strong>
    <form name="Form1" method="POST" action="GetEcadXML.asp">
		<table width="%100">
			<tr>
                <td>Entity<br>
                    <select name="selEntity" onChange="SubmitForm(true)">
                        <option value='' selected>Select an entity</option>
                        <option value='<ClaimSearch InsuranceCompanyID="145" LynxID="" ClaimNumber="" InvolvedNameFirst="John" InvolvedNameLast=""/>'>ClaimSearch</option>
                        <option value='<ClaimDetail InsuranceCompanyID="145" LynxID="6705"/>'>ClaimDetail</option>
                        <option value='<ClaimHistory InsuranceCompanyID="145" LynxID="6705"/>'>ClaimHistory</option>
                        <option value='<ClaimWitness InsuranceCompanyID="145" InvolvedID="204"/>'>ClaimWitness</option>
                        <option value='<ClaimPedestrian InsuranceCompanyID="145" InvolvedID="209"/>'>ClaimPedestrian</option>
                        <option value='<InsuranceCompanyList/>'>InsuranceCompanyList</option>
                        <option value='<DocumentList InsuranceCompanyID="145" LynxID="6156"/>'>DocumentList</option>
                        <option value='<PropertyList InsuranceCompanyID="145" LynxID="6156"/>'>PropertyList</option>
                        <option value='<PropertyDetail InsuranceCompanyID="145" LynxID="6156" PropertyNumber="2"/>'>PropertyDetail</option>
                        <option value='<PropertyInvolved InsuranceCompanyID="145" InvolvedID="800"/>'>PropertyInvolved</option>
                        <option value='<VehicleList InsuranceCompanyID="145" LynxID="6708"/>'>VehicleList</option>
                        <option value='<VehicleDetail InsuranceCompanyID="145" LynxID="6156" VehicleNumber="1"/>'>VehicleDetail</option>
                        <option value='<VehicleInvolved InsuranceCompanyID="145" InvolvedID="202"/>'>VehicleInvolved</option>
                    </select>
                    <input name="selEntityIdx" type="hidden"/>
                    <input name="selRetry" type="button" value="Retry" onClick="SubmitForm(false)"/>
                </td>
			</tr>
            <tr>
                <td>Param&nbsp;XML<br>
                    <textarea name="txtParamXML" wrap="on" rows="2" cols="100"><%= strParamXML %></textarea>
                </td>
            </tr>
			<tr>
                <td>Result&nbsp;XML<br>
                    <textarea name="txtResultXML" wrap="off" rows="30" cols="100"><%= strResultXML %></textarea>
                </td>
			</tr>
		</table>
	</form>
</body>
</html>
