<%@ Language=VBScript %>
<% Option Explicit %>
<%
    '********************************************************************************
    '* GetEcadDocumentXML.asp
    '*
    '* This ASP demonstrates and tests the GetDocumentXML method of the CDocument class
    '* module of the LAPDEcadAccessor component.
    '*
    '* jcarpenter@lynxservices.com
    '********************************************************************************

	On Error Resume Next

    Dim strResultXML, strParamXML, strError, strHTML

    Call GetEcadDocumentXML

    If Err.Number <> 0 Then
        strError = Replace( Err.Description, vbCrLf, "" )
    End If

    Sub GetEcadDocumentXML()

        On Error GoTo 0

        'Extract the query data.

        strResultXML = ""
        strParamXML = Request("txtParamXML")

        If Len(strParamXML)>0 Then

            '********************************************************
            '* Retrieve the XML through LAPDEcadAccessor.

            Dim objEcad
            Set objEcad = CreateObject("LAPDEcadAccessor.CDocument")

            strResultXML = objEcad.GetDocumentXML( strParamXML )

            Set objEcad = Nothing

            '********************************************************
            '* Style it for friendlier display

            If Request("blnViewData") = "true" Then

                Dim xmlDoc, xslDoc, docPath

                Set xmlDoc = CreateObject("MSXML2.DOMDocument")
                Set xslDoc = CreateObject("MSXML2.DOMDocument")

                xmlDoc.LoadXml strResultXML
                xslDoc.Load Server.mapPath("PrintImageView.xsl")

                strHTML = xmlDoc.transformNode(xslDoc)

                Set xmlDoc = Nothing
                Set xslDoc = Nothing

            End If

        Else

            'Demo parameters, will work against APD DEV database.
            strParamXML = "<Document ImageLocation='\6\7\0\5\E20021028142129LYNXID6705V001Doc002.xml'/>"
            strResultXML = ""

        End If

    End Sub
%>

<html>
<head>
    <script language="JavaScript">

        function InitForm()
        {
            var strError = "<%= strError %>";
            if ( strError != "" )
                alert( strError );
        }

        function Submit( blnViewData )
        {
            Form1.blnViewData.value = blnViewData;
            Form1.submit();
        }

    </script>
</head>
<body OnLoad="InitForm();">
    <strong>LYNX / ECAD Accessor Test for CDocument.GetDocumentXML()</strong>
    <form name="Form1" method="POST" action="GetEcadDocumentXML.asp">
		<table width="%100">
            <tr>
                <td>Param&nbsp;XML<br>
                    <textarea name="txtParamXML" wrap="on" rows="2" cols="100"><%= strParamXML %></textarea>
                </td>
            </tr>
            <tr>
                <td><input type="button" value="Retrieve XML" name="btnRetrieveXML" OnClick="Submit('false');"/></td>
            </tr>
            <tr>
                <td>Result&nbsp;XML<br>
                    <textarea name="txtResultXML" wrap="off" rows="30" cols="100"><%= strResultXML %></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" value="View Data" name="btnViewData" OnClick="Submit('true');"/>
                    <input type="hidden" name="blnViewData"/>
                </td>
            </tr>
		</table>
	</form>
<%
    'Display document data.
    If Request("blnViewData") = "true" Then

        Response.Write strHTML

    End If
%>
</body>
</html>
