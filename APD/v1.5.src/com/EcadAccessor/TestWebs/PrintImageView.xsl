<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace">

<msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[

        function getPageInfo( nodeList )
        {
            var nde = nodeList.nextNode().firstChild;
            var ndeType = nde.nodeType;
            if (ndeType == 4) { //CDATA
                var retVal = "\n" + stripEmptyLines(nde.nodeTypedValue);
                return retVal;
            }
        }

        function stripEmptyLines(str){
            var tmpArr;
            var tmpLine = "                                                                                ";

            str = str.replace(/\t/g, "");
            tmpArr = str.split("\n");

            var arrLen = tmpArr.length;

            //chop the top empty lines
            for (var i = 0; i < arrLen; i++) {
                if (tmpLine.indexOf(tmpArr[0]) != -1)
                    tmpArr.splice(0, 1);
                else
                    break;
            }

            //chop the bottom empty lines
            for (var i = tmpArr.length - 1; i >= 0; i--) {
                if (tmpLine.indexOf(tmpArr[tmpArr.length-1]) != -1)
                    tmpArr.splice(tmpArr.length-1, 1);
                else
                    break;
            }

            return tmpArr.join("\n");
        }

  ]]>
</msxsl:script>

<xsl:template match="/PrintImage">
    <style>
        .myPage {
            border: 1px solid #000000;
            border-bottom:3px solid #000000;
            border-right:3px solid #000000;
            background-color: #FFF8DC;
            padding:15px;
        }
        .pageContent {
            background-color: #FFFFFF;
             height:100%;
        }
        pre {
            font-family: Courier New;
            font-size: 10pt;
            cursor:default;
            width: 100%;
            background: #FFFFFF;
        }
    </style>

    <table border="0" cellspacing="0" cellpadding="0" style="height:100%">
      <tr>
        <td>
          <div id="divDoc" class="autoflowDiv" style="height:100%;overflow:auto;padding:5px;">
            <div id="doc" name="doc">
               <xsl:for-each select="Page">
                   <table border="0" cellspacing="0" cellpadding="0" class="myPage">
                       <xsl:attribute name="id"><xsl:value-of select="concat('pg', string(position()))"/></xsl:attribute>
                       <tr><td nowrap="">
                       <span class="pageContent">
                           <xsl:attribute name="id"><xsl:value-of select="concat('pgContent', string(position()))"/></xsl:attribute>
                           <pre>
                               <xsl:value-of select="user:getPageInfo(.)"/>
                           </pre>
                       </span>
                       </td></tr></table>
                   <br/>
               </xsl:for-each>
            </div>
          </div>
        </td>
      </tr>
    </table>

</xsl:template>

</xsl:stylesheet>
