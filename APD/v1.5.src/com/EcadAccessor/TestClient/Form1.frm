VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   1845
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5895
   LinkTopic       =   "Form1"
   ScaleHeight     =   1845
   ScaleWidth      =   5895
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton GetConfig 
      Caption         =   "Get Config"
      Height          =   375
      Left            =   4680
      TabIndex        =   1
      Top             =   120
      Width           =   1095
   End
   Begin VB.TextBox ConfigXml 
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Text            =   "<Config Path='ClaimPoint/ShopSignUp/NotificationEmail' />"
      Top             =   120
      Width           =   4455
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub GetConfig_Click()

    On Error GoTo ErrorHandler
    
    Dim obj As Object
    Dim str As String
    
    Set obj = CreateObject("LAPDEcadAccessor.CData")
    
    str = obj.GetXML(ConfigXml.Text)
    
    MsgBox "Config = '" & str & "'"
    
ErrorHandler:

    Set obj = Nothing
    
    If Err.Number <> 0 Then
        MsgBox "Error = '" & Err.Description & "'"
    End If
    
End Sub
