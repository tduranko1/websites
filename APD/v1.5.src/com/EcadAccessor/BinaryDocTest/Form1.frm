VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   1275
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9975
   LinkTopic       =   "Form1"
   ScaleHeight     =   1275
   ScaleWidth      =   9975
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtXML 
      Height          =   615
      Left            =   840
      MultiLine       =   -1  'True
      TabIndex        =   1
      Text            =   "Form1.frx":0000
      Top             =   120
      Width           =   9015
   End
   Begin VB.CommandButton btnExecute 
      Caption         =   "Execute"
      Height          =   375
      Left            =   4440
      TabIndex        =   0
      Top             =   840
      Width           =   1215
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Param XML"
      Height          =   495
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'********************************************************************************
'* ClientAccessDocTest
'*
'* This simple test client shows how the LAPDEcadAccessor component can
'* be used to extract a binary document from the LYNX APD application and
'* store it to disk.
'*
'* jcarpenter@lynxservices.com
'********************************************************************************
Option Explicit

Private Sub btnExecute_Click()
    Dim objEcad As Object
    Dim varTemp() As Byte
    
    On Error GoTo ErrorHandler
    
    Set objEcad = CreateObject("LAPDEcadAccessor.CDocument")
    
    'Get the binary data from the component.
    varTemp = objEcad.GetDocument(txtXML.Text)
    
    'Write the binary data to disk.
    WriteBinaryDataToFile App.Path & "/temp.tif", varTemp
    
ErrorHandler:
    
    If Err.Number <> 0 Then
        MsgBox Err.Description
    Else
        MsgBox "Done! Wrote temp.tif out to disk."
    End If
    
    Set objEcad = Nothing

End Sub

'Writes the passed Byte Array to the passed file.
Public Sub WriteBinaryDataToFile(ByVal strFileName As String, ByRef arrBuffer() As Byte)
    Dim intFile As Integer
    intFile = FreeFile()
    Open strFileName For Binary As intFile
    Put intFile, , arrBuffer
    Close intFile
End Sub

