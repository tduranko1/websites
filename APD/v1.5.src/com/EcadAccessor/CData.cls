VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Component LAPDEcadAccessor: Class CData
'*
'* This component is a simple wrapper for LAPDEcadAccessorMgr so to allow
'* the two components to run under different user IDs.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CData."

'********************************************************************************
'* Backup global termination
'********************************************************************************
Private Sub Class_Terminate()
10        TerminateGlobals
End Sub

'********************************************************************************
'* Syntax:      objEcad.GetXML( "<Entity param1='p1' param2='p2'/>" )
'*
'* Parameters:  strParamXML - the only parameter, in XML format.
'*
'* Purpose:     Returns XML data required by ECAD for our Client Access app.
'*
'* Returns:     The XML data, minus any reference and metadata.
'*
'********************************************************************************
Public Function GetXML(ByVal strParamXML As String) As String
          Const PROC_NAME As String = MODULE_NAME & "GetXML: "

          Dim objMgr As Object
          Dim blnDebugMode As Boolean
          
10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Get config debug mode status.
30        blnDebugMode = mobjEvents.IsDebugMode

          'Check passed parameters.
40        mobjEvents.Assert Len(strParamXML) > 0, "Nothing passed for strParamXML."

          'Add a trace mesage to the debug log.
50        If blnDebugMode Then mobjEvents.Trace "Param XML = '" & strParamXML & "'", PROC_NAME & "Started"
          
          'Create and call LAPDEcadAccessorMgr to do the real work.
60        Set objMgr = CreateObjectEx("LAPDEcadAccessorMgr.CData")
          
70        GetXML = objMgr.GetXML(strParamXML)
          
          'Add another trace message to note exit?
80        If blnDebugMode Then mobjEvents.Trace "Return = " & GetXML, PROC_NAME & "Finished"
          
ErrorHandler:

90        Set objMgr = Nothing
          
100       If Err.Number <> 0 Then
110           mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "Param XML = '" & strParamXML & "'"
120       End If
          
End Function

'********************************************************************************
'* Syntax:      objEcad.PutXML( "<Entity param1='p1' param2='p2'/>" )
'*
'* Parameters:  strParamXML - the data to add, in XML format.
'*
'* Purpose:     Calls APD DB stored procedures to add data to APD.
'*              This was designed primarly for the Add Comments functionality
'*              in Client Access, but can be extended for anything.
'*
'* Returns:     The "number of rows affected" returned from the database.
'*              This value should be used for debugging purposes only, as
'*              it is subject to change.
'*
'********************************************************************************
Public Function PutXML(ByVal strParamXML As String) As Long
          Const PROC_NAME As String = MODULE_NAME & "PutXML: "

          Dim objMgr As Object
          Dim blnDebugMode As Boolean
          
10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Get config debug mode status.
30        blnDebugMode = mobjEvents.IsDebugMode

          'Check passed parameters.
40        mobjEvents.Assert Len(strParamXML) > 0, "Nothing passed for strParamXML."

          'Add a trace mesage to the debug log.
50        If blnDebugMode Then mobjEvents.Trace "Param XML = '" & strParamXML & "'", PROC_NAME & "Started"
          
          'Create and call LAPDEcadAccessorMgr to do the real work.
60        Set objMgr = CreateObjectEx("LAPDEcadAccessorMgr.CData")
          
70        PutXML = objMgr.PutXML(strParamXML)
          
          'Add another trace message to note exit?
80        If blnDebugMode Then mobjEvents.Trace "Return = " & PutXML, PROC_NAME & "Finished"
          
ErrorHandler:

90        Set objMgr = Nothing
          
100       If Err.Number <> 0 Then
110           mobjEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "Param XML = '" & strParamXML & "'"
120       End If
          
End Function

