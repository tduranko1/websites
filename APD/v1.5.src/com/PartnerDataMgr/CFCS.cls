VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFCS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'********************************************************************************
'* Component LAPDPartnerDataMgr : Class CFCS
'*
'* Processing methods specific to FCS
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CFCS."
Implements CPartner


Private clngFCSSystemUserID As String

Private Enum FCS_Error_Codes
    eBadAssignmentCode = CAutoVerse_CLS_ErrorCodes
    eAssignmentInvalid
    eAttachmentIdNotFound
    eClaimNumberInvalid
    eNoPassThruData
    eBadPassThruDataFormat
    eUnhandledAutoverseNack
    eBlankCefReturnedForEstimate
    eXmlTransactionTooLarge
    eInternalError
    eInvalidEventCode
    eMissingExpectedDocumentNode
    
End Enum

Private mobjPartner As New CPartner

Private Function CPartner_ApdDatabaseTransfer() As String
        Const PROC_NAME As String = MODULE_NAME & "ApdDatabaseTransfer: "
  
10      On Error GoTo ErrorHandler
  
20      CPartner_ApdDatabaseTransfer = ""

30      clngFCSSystemUserID = GetDefaultPartnerUserID("FCS")

40      If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & " Started."
  
50      Select Case mobjPartner.DocTypeEnum
          Case eReceipt
60          ProcessReceipt
70        Case Else
80          Err.Raise eBadDocumentType, PROC_NAME, "We don't process doc type '" & _
                            mobjPartner.DocTypeString & "' [" & mobjPartner.DocTypeEnum & _
                            "] for partner " & g_strPartner
90      End Select
  
ErrorHandler:
100     If Err.Number <> 0 Then
110       g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
120     End If
End Function

Private Function CPartner_InitProcessDocument() As Boolean
          Const PROC_NAME As String = MODULE_NAME & "InitProcessDocument: "
          Dim strAssignmentIDTokens() As String
          
10        On Error GoTo ErrorHandler

20        If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & " Started"
          
          'Extract the doc type from the passed root element.
30        mobjPartner.RawDocTypeString = _
              g_objPassedDocument.documentElement.nodeName
      
          'Extract the LYNXID and Vehicle number.
40        'mobjPartner.ExtractLynxIDAndVehNum
          strAssignmentIDTokens = Split(GetChildNodeText(g_objPassedDocument, "//Header/ResponseFor", True), "-", , vbTextCompare)
          
          g_strLynxID = strAssignmentIDTokens(0)
          g_strVehicleNumber = strAssignmentIDTokens(1)
          
          'Are we configured to process this document at this time?
50        CPartner_InitProcessDocument = mobjPartner.CanProcessDocument()
          
60        If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & " Finished"
          
ErrorHandler:

70        If Err.Number <> 0 Then
80            g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
90        End If

End Function

Private Sub ProcessReceipt()
        Const PROC_NAME As String = MODULE_NAME & "ProcessReceipt: "
  
        Dim strReturnCode As String, strErrDesc As String, strNote As String
        Dim strClaimAspectID As String, strClaimAspectStatusID As String
        Dim objClaimVehicleInfo As New DOMDocument40
  
10      On Error GoTo ErrorHandler
  
20      g_objDataAccessor.SetConnectString g_strLYNXConnStringXML
  
30      LoadXml objClaimVehicleInfo, g_objDataAccessor.ExecuteSpNamedParamsXML(GetConfig("ClaimPoint/SPMap/VehicleList"), "@LynxID", g_strLynxID, "@InsuranceCompanyID", 185), PROC_NAME
  
40      strClaimAspectID = GetChildNodeText(objClaimVehicleInfo, "//Vehicle[@VehicleNumber='" & g_strVehicleNumber & "']/@ClaimAspectID")
50      strClaimAspectStatusID = GetChildNodeText(objClaimVehicleInfo, "//Vehicle[@VehicleNumber='" & g_strVehicleNumber & "']/@StatusID")
  
60      g_objEvents.Assert (strClaimAspectID <> ""), "Could not determine Claim Aspect for Receipt received."
    
70      strReturnCode = GetChildNodeText(g_objPassedDocument, "//ReturnCode")
  
80      Select Case LCase(strReturnCode)
          Case "success"
            ' Assignment successful
90          strNote = "Assignment to FCS successfully sent"
100       Case Else
110         strNote = "Assignment rejected by FCS"
120         g_objEvents.HandleEvent eNackReceived, PROC_NAME, strNote & ": " & strReturnCode & " -- " & strErrDesc, , , False
130     End Select
140     g_objDataAccessor.SetConnectString g_strLYNXConnStringStd
  
150     g_objDataAccessor.ExecuteSpNamedParams GetConfig("ClaimPoint/SPMap/NoteInsDetail"), _
                      "@ClaimAspectID", strClaimAspectID, _
                      "@NoteTypeID", 1, _
                      "@StatusID", strClaimAspectStatusID, _
                      "@Note", strNote, _
                      "@UserID", clngFCSSystemUserID
                
ErrorHandler:
160     Set objClaimVehicleInfo = Nothing
170     If Err.Number <> 0 Then
180       g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
190     End If
End Sub

Private Function CPartner_PartnerDatabaseTransfer(ByVal blnSendReceipt As Boolean) As Boolean
    Const PROC_NAME As String = MODULE_NAME & "PartnerDatabaseTransfer: "
    
    Dim strError As String
    
    On Error GoTo ErrorHandler
    
    If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & " Started"
    
    CPartner_PartnerDatabaseTransfer = True
    
    mobjPartner.StoreDocumentInPartnerDb
    
    If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & " Finished"
    
ErrorHandler:

    If Err.Number <> 0 Then
        g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
    End If
End Function
  
  
Private Function CPartner_InsertPrintImageIntoAPD(ByVal strXML As String, ByVal lngPartnerTransID As Long) As Long
  CPartner_InsertPrintImageIntoAPD = mobjPartner.InsertPrintImageIntoAPD(strXML, lngPartnerTransID)
End Function
Private Sub CPartner_SendHeartBeatRequest()
  mobjPartner.SendHeartBeatRequest
End Sub

Private Function CPartner_TransferToPartner() As String
  mobjPartner.TransferToPartner
End Function

Private Function CPartner_PartnerDatabaseTransferV2(ByVal blnSendReceipt As Boolean) As Boolean
    Const PROC_NAME As String = MODULE_NAME & "PartnerDatabaseTransferV2: "
    Err.Raise eNotImplemented, PROC_NAME, "NOT IMPLEMENTED"
End Function

Private Function CPartner_ApdDatabaseTransferV2() As String
    Const PROC_NAME As String = MODULE_NAME & "ApdDatabaseTransferV2: "
    Err.Raise eNotImplemented, PROC_NAME, "NOT IMPLEMENTED"
End Function

Private Function CPartner_InitProcessDocumentV2(ByVal strTradingPartner As String, ByVal strPassedDocument As String) As Boolean
    Const PROC_NAME As String = MODULE_NAME & "InitProcessDocumentV2: "
    Err.Raise eNotImplemented, PROC_NAME, "NOT IMPLEMENTED"
End Function

Private Function CPartner_InsertPrintImageIntoAPDV2(ByVal strXML As String, ByVal lngPartnerTransID As Long) As Long
    Const PROC_NAME As String = MODULE_NAME & "InsertPrintImageIntoAPDV2: "
    Err.Raise eNotImplemented, PROC_NAME, "NOT IMPLEMENTED"
End Function

