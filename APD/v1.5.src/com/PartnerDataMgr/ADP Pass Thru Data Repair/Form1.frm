VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form Form1 
   Caption         =   "ADP Pass Thru Data Repair"
   ClientHeight    =   7920
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9735
   LinkTopic       =   "Form1"
   ScaleHeight     =   7920
   ScaleWidth      =   9735
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton ViewImage 
      Caption         =   "Save Image to Disk"
      Height          =   375
      Left            =   5880
      TabIndex        =   20
      Top             =   600
      Width           =   1575
   End
   Begin VB.TextBox SqlQuery2 
      Height          =   615
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   19
      Top             =   7200
      Width           =   9495
   End
   Begin VB.CommandButton PassThru 
      Caption         =   "Set Lynx Header Values"
      Height          =   375
      Left            =   7560
      TabIndex        =   18
      Top             =   600
      Width           =   2055
   End
   Begin VB.ComboBox APD 
      Height          =   315
      ItemData        =   "Form1.frx":0000
      Left            =   6960
      List            =   "Form1.frx":000D
      TabIndex        =   17
      Text            =   "DB"
      Top             =   6720
      Width           =   1575
   End
   Begin VB.CommandButton QueryApd 
      Caption         =   "Query"
      Height          =   375
      Left            =   8640
      TabIndex        =   16
      Top             =   6720
      Width           =   975
   End
   Begin VB.CommandButton Search 
      Caption         =   "Search"
      Height          =   375
      Left            =   5880
      TabIndex        =   15
      Top             =   6720
      Width           =   975
   End
   Begin VB.TextBox AssignmentID 
      Height          =   375
      Left            =   4800
      TabIndex        =   11
      Top             =   6720
      Width           =   975
   End
   Begin VB.TextBox VehNum 
      Height          =   375
      Left            =   2880
      TabIndex        =   10
      Top             =   6720
      Width           =   615
   End
   Begin VB.TextBox LynxID 
      Height          =   375
      Left            =   840
      TabIndex        =   9
      Top             =   6720
      Width           =   1095
   End
   Begin VB.CommandButton Minus 
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8760
      TabIndex        =   8
      Top             =   120
      Width           =   375
   End
   Begin VB.CommandButton Plus 
      Caption         =   "+"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9240
      TabIndex        =   7
      Top             =   120
      Width           =   375
   End
   Begin RichTextLib.RichTextBox XML 
      Height          =   5535
      Left            =   120
      TabIndex        =   6
      Top             =   1080
      Width           =   9495
      _ExtentX        =   16748
      _ExtentY        =   9763
      _Version        =   393217
      ScrollBars      =   3
      TextRTF         =   $"Form1.frx":002C
   End
   Begin VB.CommandButton Clear 
      Caption         =   "Clear"
      Height          =   375
      Left            =   4800
      TabIndex        =   5
      Top             =   600
      Width           =   975
   End
   Begin VB.CommandButton Copy 
      Caption         =   "Copy"
      Height          =   375
      Left            =   3720
      TabIndex        =   4
      Top             =   600
      Width           =   975
   End
   Begin VB.CommandButton SelectAll 
      Caption         =   "Select All"
      Height          =   375
      Left            =   2640
      TabIndex        =   3
      Top             =   600
      Width           =   975
   End
   Begin VB.ComboBox DB 
      Height          =   315
      ItemData        =   "Form1.frx":00AE
      Left            =   120
      List            =   "Form1.frx":00C4
      TabIndex        =   2
      Text            =   "DB"
      Top             =   600
      Width           =   1335
   End
   Begin VB.CommandButton Run 
      Caption         =   "RUN"
      Height          =   375
      Left            =   1560
      TabIndex        =   1
      Top             =   600
      Width           =   975
   End
   Begin VB.TextBox Query 
      Height          =   375
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   0
      Text            =   "Form1.frx":010A
      Top             =   120
      Width           =   8535
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Assignment ID"
      Height          =   255
      Left            =   3600
      TabIndex        =   14
      Top             =   6840
      Width           =   1095
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Veh Num"
      Height          =   255
      Left            =   2040
      TabIndex        =   13
      Top             =   6840
      Width           =   735
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Lynx ID"
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   6840
      Width           =   615
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    DB.ListIndex = 5
    APD.ListIndex = 2
End Sub


Private Sub Run_Click()

    On Error Resume Next
    
    DoQuery
    
    If Err.Number <> 0 Then
        MsgBox Err.Description
    End If
    
End Sub

Private Sub DoQuery()

    Dim objGet As Object
    Dim objRS As ADODB.Recordset
    Dim strXml As String
    
    On Error Resume Next
    
    Set objGet = CreateObject("DataAccessor.CDataAccessor")

    objGet.SetConnectString GetConnStr(DB.Text)
    
    Set objRS = objGet.OpenRecordsetSp(Query.Text)
    
    If objRS.EOF And objRS.BOF Then
        MsgBox "No results returned - check query."
    Else
        strXml = objRS.Fields.Item(0)
    End If
    
    Clipboard.Clear
    Clipboard.SetText strXml
    
    XML.Text = strXml
    
    Set objRS = Nothing
    Set objGet = Nothing

    If Err.Number <> 0 Then
        MsgBox Err.Description
    End If

End Sub

Private Function GetConnStr(ByVal strDB As String)
    
    On Error Resume Next
    
    Select Case strDB
        Case "APD DEV": GetConnStr = "provider=SQLOLEDB.1;data source=SFTMAPDDEVDB1,1466;initial catalog=udb_apd_dev;TRUSTED_CONNECTION=yes;"
        Case "APD TST": GetConnStr = "provider=SQLOLEDB.1;data source=SFTMAPDDEVDB1,1466;initial catalog=udb_apd_test;TRUSTED_CONNECTION=yes;"
        Case "APD PRD": GetConnStr = "provider=SQLOLEDB.1;data source=SFTMAPDPRDSQL.nac.ppg.com,1459;initial catalog=udb_apd;TRUSTED_CONNECTION=yes;"
        Case "Partner DEV":  GetConnStr = "provider=SQLOLEDB.1;data source=SFTMAPDDEVDB1,1466;initial catalog=udb_partner_dev;TRUSTED_CONNECTION=yes;"
        Case "Partner TST":  GetConnStr = "provider=SQLOLEDB.1;data source=SFTMAPDDEVDB1,1466;initial catalog=udb_partner_test;TRUSTED_CONNECTION=yes;"
        Case "Partner PRD":  GetConnStr = "provider=SQLOLEDB.1;data source=SFTMAPDPRDSQL.nac.ppg.com,1459;initial catalog=udb_partner;TRUSTED_CONNECTION=yes;"
    End Select

    If Err.Number <> 0 Then
        MsgBox Err.Description
    End If

End Function

Private Sub SelectAll_Click()
    XML.SetFocus
    XML.SelStart = 0
    XML.SelLength = Len(XML.Text)
End Sub

Private Sub Copy_Click()
    Clipboard.Clear
    Clipboard.SetText XML.Text
End Sub

Private Sub Clear_Click()
    XML.Text = ""
End Sub

Private Sub Plus_Click()
    Dim str As String
    Dim idx As Integer, idx2 As Integer
    Dim lng As Long
    
    On Error Resume Next
    
    str = Query.Text
    idx = InStr(1, str, "'")
    idx2 = InStr(idx + 1, str, "'")
    
    lng = CLng(Mid(str, idx + 1, idx2 - idx - 1))
    lng = lng + 1
    
    str = Left(str, idx) + CStr(lng) + "'"
    Query.Text = str
    
    If Err.Number <> 0 Then
        MsgBox Err.Description
    End If
    
End Sub

Private Sub Minus_Click()
    Dim str As String
    Dim idx As Integer, idx2 As Integer
    Dim lng As Long
    
    On Error Resume Next
    
    str = Query.Text
    idx = InStr(1, str, "'")
    idx2 = InStr(idx + 1, str, "'")
    
    lng = CLng(Mid(str, idx + 1, idx2 - idx - 1))
    lng = lng - 1
    
    str = Left(str, idx) + CStr(lng) + "'"
    Query.Text = str

    If Err.Number <> 0 Then
        MsgBox Err.Description
    End If

End Sub

Private Sub Search_Click()
    Dim str As String
    Dim idx As Integer, idx2 As Integer, idx3 As Integer
    Dim lng As Long
    
    On Error Resume Next
    
    SqlQuery2.Text = ""
    
    str = XML.Text
    idx = InStr(1, str, "LYNX ID:") + Len("LYNX ID:")
    
    If idx > 10 Then
    
        idx2 = InStr(idx + 1, str, "-")
        idx3 = InStr(idx2 + 1, str, "<")
        
        LynxID.Text = Mid(str, idx, idx2 - idx)
        VehNum.Text = Mid(str, idx2 + 1, idx3 - idx2 - 1)
        
    Else
    
        MsgBox "Could not find 'LYNX ID:' in the XML." & vbCrLf _
            & "Generating from entered Lynx ID and Vehicle Number."
        
    End If
    
    SqlQuery2.Text = "SELECT AssignmentID FROM utb_assignment asg " & _
        "JOIN utb_claim_aspect ca ON asg.ClaimAspectID = ca.ClaimAspectID " & _
        "WHERE ca.LynxID = '" & LynxID.Text & "' AND ca.ClaimAspectNumber = '" & VehNum.Text & "'"

    If Err.Number <> 0 Then
        MsgBox Err.Description
    End If

End Sub

Private Sub QueryApd_Click()
    Dim objGet As Object
    Dim objRS As ADODB.Recordset
    Dim strXml As String
    
    On Error Resume Next
    
    If SqlQuery2.Text = "" Then
        MsgBox "Must extract Lynx ID and Vehicle Number from XML first." & vbCrLf _
            & "As and alternate, type them in and click 'Search'"
    End If
    
    Set objGet = CreateObject("DataAccessor.CDataAccessor")

    objGet.SetConnectString GetConnStr(APD.Text)
    
    Set objRS = objGet.OpenRecordsetSp(SqlQuery2.Text)
    
    If objRS.EOF And objRS.BOF Then
        MsgBox "No results returned - check query."
    Else
        objRS.MoveLast
        AssignmentID.Text = objRS.Fields.Item(0)
    End If
    
    Set objRS = Nothing
    Set objGet = Nothing

    If Err.Number <> 0 Then
        MsgBox Err.Description
    End If

End Sub

Private Sub PassThru_Click()

    On Error Resume Next
    
    Dim oDom As New MSXML2.DOMDocument40
    oDom.LoadXml XML.Text
    
    oDom.documentElement.Attributes.getNamedItem("LynxID").Text = LynxID.Text
    oDom.documentElement.Attributes.getNamedItem("VehicleID").Text = VehNum.Text
    oDom.documentElement.Attributes.getNamedItem("AssignmentID").Text = AssignmentID.Text
    
    XML.Text = oDom.XML
    Set oDom = Nothing

    If Err.Number <> 0 Then
        MsgBox Err.Description
    End If

End Sub

Private Sub ViewImage_Click()

    On Error Resume Next
    
    Dim oDom As New MSXML2.DOMDocument40
    Dim arBuff() As Byte
    Dim sFile As String
    
    LoadXml oDom, XML.Text, "ViewImage", "XML Document"
    
    sFile = GetChildNodeText(oDom, "//ImageFileName", True) & "." _
        & GetChildNodeText(oDom, "//ImageFileType", True)
        
    arBuff = ReadBinaryDataFromDomElement(GetChildNode(oDom, "//ImageFile", True))
    
    WriteBinaryDataToFile sFile, arBuff
   
    Set oDom = Nothing

    If Err.Number <> 0 Then
        MsgBox Err.Description
    End If

End Sub
