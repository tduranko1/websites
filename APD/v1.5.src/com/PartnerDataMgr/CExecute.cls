VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CExecute"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'********************************************************************************
'* Component PartnerDataMgr: Class CExecute
'*
'* This component does the actual work of processing transactions received from
'* our partners.  It exists separate from the PartnerData component because
'* PartnerData needs to run under different security settings.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CExecute."

'Error codes private to this module.
Private Enum CExecute_Error_Codes
    eMethodObjsolete = CExecute_CLS_ErrorCodes
End Enum

'These are used to control which databases the data is submitted to.
'Used these instead of method parameters to avoid breaking the interface.
Private mblnPartnerDbXfer As Boolean
Private mblnApdDbXfer As Boolean
Private mblnSendReceipt As Boolean

Private Sub Class_Initialize()
          'Default is to submit to both databases.
          'They will only be overrid by backoffice document submission.
10        mblnPartnerDbXfer = True
20        mblnApdDbXfer = True
30        mblnSendReceipt = True
End Sub

Private Sub Class_Terminate()
          'Ensure that object clean-up happens in spite of errors.
10        TerminateGlobals
End Sub

'********************************************************************************
'* Obsoleted - call ConsumePartnerXmlTransaction below instead.
'* Added a new method rather than break binary compatability.
'********************************************************************************
Public Function SendPartnerDocAsXml(ByVal strXMLDoc As String, ByVal strTradingPartner As String) As Long
10        Err.Raise eMethodObjsolete, "SendPartnerDocAsXml", "Method Obsolete - call ConsumePartnerXmlTransaction instead."
End Function

'********************************************************************************
'*
'* Syntax:      object.ConsumePartnerXmlTransaction strXmlDoc, strTradingPartner
'*
'* Parameters:  strXmlDoc = Xml document received from calling application
'*              strTradingPartner = Code name for Trading Partner like "CCC"
'*
'* Purpose:     Receive an Xml from an external application and then act on it
'*              based on the content itself.  Typical usage would be to send the
'*              Xml document to a stored proc for storage
'*              Another usage would be to feed the content of the Xml into another component
'*              for other actions.
'*
'* Returns:     Blank or any response XML.
'*
'* This function processes an Xml document sent from outside of PPG,
'* Specifically from one of our partners (CCC, ADP, SG, etc.)
'*
'********************************************************************************
Public Function ConsumePartnerXmlTransaction( _
    ByVal strXMLDoc As String, _
    ByVal strTradingPartner As String, _
    ByVal blnReturnXml As Boolean) As String
          
          Const PROC_NAME As String = MODULE_NAME & "ConsumePartnerXmlTransaction: "

          'This bool is used to add additional control over when we raise to the caller.
          Dim blnRaise As Boolean
          Dim blnContinue As Boolean
          
          Dim objPartner As CPartner

10        On Error GoTo ErrorHandler

20        ConsumePartnerXmlTransaction = ""
          
          'Raise to the caller for any problems with the passed parameters.
30        blnRaise = True

          'Store the received date/time for later.
40        g_varDocumentReceivedDate = Now

          ' Put these in global scope for access by all procedures
          ' including "InitalizeGlobals"
50        g_strPartner = strTradingPartner
60        g_strPassedDocument = strXMLDoc

70        InitializeGlobals

          'Add parameters to trace data.
80        If g_blnDebugMode Then g_objEvents.Trace _
              " Partner Document Received from " & strTradingPartner & " at " & Now() & vbCrLf _
              & strXMLDoc, PROC_NAME & "Started"
              
          'Validate our parameters and trace them IF they are invalid.
90        g_objEvents.Assert Len(strTradingPartner) > 0, "A blank Trading Partner string received"

100       g_strPartnerSmallID = GetConfig("PartnerSettings/" & strTradingPartner & "/@SmallID")

          'Get a partner object to work with.
110       Set objPartner = GetPartnerObject()
          
120       If strTradingPartner = "ADP" Then
              'Processing of document by partner specific class...
130           If objPartner.InitProcessDocumentV2(strTradingPartner, strXMLDoc) Then
              
                  'Do the partner database submission?
140               If mblnPartnerDbXfer Then
150                   blnContinue = objPartner.PartnerDatabaseTransferV2(mblnSendReceipt)
160               Else
170                   blnContinue = True
180               End If
                  
                  'Once the document has successfully validated and stored in the
                  'partner database then we consider any issues to be internal.
190               blnRaise = False
                  
                  'Do the APD database submission?
200               If mblnApdDbXfer And blnContinue Then
210                   ConsumePartnerXmlTransaction = objPartner.ApdDatabaseTransferV2
220               End If
                  
230           End If
240       Else
              ' Dump the passed in values into the bit bucket
250           strXMLDoc = ""
260           strTradingPartner = ""
                  
              'Processing of document by partner specific class...
270           If objPartner.InitProcessDocument() Then
              
                  'Do the partner database submission?
280               If mblnPartnerDbXfer Then
290                   blnContinue = objPartner.PartnerDatabaseTransfer(mblnSendReceipt)
300               Else
310                   blnContinue = True
320               End If
                  
                  'Once the document has successfully validated and stored in the
                  'partner database then we consider any issues to be internal.
330               blnRaise = False
                  
                  'Do the APD database submission?
340               If mblnApdDbXfer And blnContinue Then
350                   ConsumePartnerXmlTransaction = objPartner.ApdDatabaseTransfer
360               End If
                  
370           End If
380       End If

ErrorHandler:

390       Set objPartner = Nothing
          
400       mblnPartnerDbXfer = True
410       mblnApdDbXfer = True

420       If Err.Number <> 0 Then
          
              'Special cases where resubmission from Glaxis will do no good:
430           Select Case Err.Number
                  Case eDuplicateDocument, eLynxIdInvalid
440                   blnRaise = False
450           End Select
              
              'If the caller is expecting XML then return an error XML string.
460           If blnReturnXml Then
470               ConsumePartnerXmlTransaction = "<Error Number='" _
                  & Err.Number & "' Source='SendXmlRequest' Description='Error in processing occurred.'" _
                  & " Resend='True' />"
480           End If
              
490           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "XML document from " & g_strPartner & ":" & vbCrLf & g_strPassedDocument, "", blnRaise
              
500           Err.Clear
              
510       End If

520       TerminateGlobals

End Function

'********************************************************************************
'*
'* Syntax:      object.SendHeartBeatRequest()
'*
'* Purpose:     Network Status Test
'*
'********************************************************************************
Public Function SendHeartBeatRequest(ByVal strTradingPartner As String)
          Const PROC_NAME As String = MODULE_NAME & "SendHeartBeatRequest: "

          Dim objPartner As CPartner

10        On Error GoTo ErrorHandler

20        g_strPartner = strTradingPartner
30        g_strPassedDocument = "<Root/>"

40        InitializeGlobals

          'Add parameters to trace data.
50        If g_blnDebugMode Then g_objEvents.Trace _
              " HeartBeat outbound to " & g_strPartner & " at " & Now() & vbCrLf, _
              PROC_NAME & "Started"

          'Validate our parameters and trace them IF they are invalid.
60        g_objEvents.Assert CBool(Len(g_strPartner) > 0), "No Trading Partner value received"

          'Get a partner object to work with.
70        Set objPartner = GetPartnerObject()
          
80        objPartner.SendHeartBeatRequest

ErrorHandler:

90        Set objPartner = Nothing

100       If Err.Number <> 0 Then
110           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "Trading Partner: " & strTradingPartner
120       End If

130       TerminateGlobals

End Function

'********************************************************************************
'* Backoffice utility for submitting to Partner database only.
'********************************************************************************
Public Function SubmitDocumentToPartnerDbOnly( _
    ByVal strXMLDoc As String, _
    ByVal strTradingPartner As String, _
    ByVal blnSendReceipt As Boolean) As Long
          
          Dim strResult As String
          
10        mblnPartnerDbXfer = True
20        mblnApdDbXfer = False
30        mblnSendReceipt = blnSendReceipt
          
40        strResult = ConsumePartnerXmlTransaction(strXMLDoc, strTradingPartner, False)
          
50        If Left(strResult, 6) = "<Error" Then
60            SubmitDocumentToPartnerDbOnly = 0
70        Else
80            SubmitDocumentToPartnerDbOnly = 1
90        End If
End Function

'********************************************************************************
'* Backoffice utility for submitting to APD database only.
'********************************************************************************
Public Function SubmitDocumentToApdDbOnly( _
    ByVal strXMLDoc As String, _
    ByVal strTradingPartner As String, _
    ByVal blnSendReceipt As Boolean) As Long
          
          Dim strResult As String
          
10        mblnPartnerDbXfer = False
20        mblnApdDbXfer = True
30        mblnSendReceipt = blnSendReceipt
          
40        strResult = ConsumePartnerXmlTransaction(strXMLDoc, strTradingPartner, False)
          
50        If Left(strResult, 6) = "<Error" Then
60            SubmitDocumentToApdDbOnly = 0
70        Else
80            SubmitDocumentToApdDbOnly = 1
90        End If
          
End Function

'********************************************************************************
'* Returns a CPartner based upon g_strPartner
'********************************************************************************
Private Function GetPartnerObject() As CPartner

    Select Case UCase(g_strPartner)
        Case "CCC"
            Set GetPartnerObject = New CCCC
        Case "ADP"
            Set GetPartnerObject = New CADP
        Case "CCAV"
            Set GetPartnerObject = New CAutoverse
        Case "GRANGEOHIO"
            Set GetPartnerObject = New CGrange
        Case "MITCHELL"
            Set GetPartnerObject = New CMitchell
        Case "SCENEGENESIS"
            Set GetPartnerObject = New CSceneGenesis
        Case "FCS"
            Set GetPartnerObject = New CFCS
        Case Else
            Err.Raise eInvalidTradingPartner, MODULE_NAME & "GetPartnerObject()", _
                "Trading partner " & g_strPartner & " passed was unknown."
    End Select

End Function

'********************************************************************************
'*
'* Syntax:      object.InitiatePartnerXmlTransaction strXmlDoc
'*
'* Parameters:  strXmlDoc = Xml document received from calling application
'*
'* Purpose:     Receive an Xml from an internal application and then kick off
'*              a transaction with an external (partner) application.
'*
'* Returns:     Blank or any response XML.
'*
'********************************************************************************
Public Function InitiatePartnerXmlTransaction(ByVal strXMLDoc As String) As String
          
          Const PROC_NAME As String = MODULE_NAME & "InitiatePartnerXmlTransaction: "

          'Object vars that need clean up.
          Dim objPartner As CPartner

10        On Error GoTo ErrorHandler

20        InitiatePartnerXmlTransaction = ""
          
          'Store the received date/time for later.
30        g_varDocumentReceivedDate = Now

          ' Put these in global scope for access by all procedures
          ' including "InitalizeGlobals"
40        g_strPassedDocument = strXMLDoc

          ' Dump the passed in values into the bit bucket
50        strXMLDoc = ""

60        InitializeGlobals

          'Add parameter to trace data.
70        If g_blnDebugMode Then g_objEvents.Trace g_strPassedDocument, PROC_NAME & "Started"
              
          'Validate our parameters and trace them IF they are invalid.
80        g_objEvents.Assert Len(g_strPassedDocument) > 0, "A blank Xml Document was received"
          
          'Extract the partner from the XML, i.e. "SceneGenesis".
90        g_strPartner = GetChildNodeText(g_objPassedDocument, "//@Partner", True)

          'Get a little partner string from the config, i.e. "SG".
100       g_strPartnerSmallID = GetConfig("PartnerSettings/" & g_strPartner & "/@SmallID")

          'Get a partner object to work with.
110       Set objPartner = GetPartnerObject()
          
          'Processing of document by partner specific class...
120       InitiatePartnerXmlTransaction = objPartner.TransferToPartner()

ErrorHandler:

130       Set objPartner = Nothing
          
140       If Err.Number <> 0 Then
150           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "XML document from " & g_strPartner & ":" & vbCrLf & g_strPassedDocument
160       End If

170       TerminateGlobals

End Function


'---------------------------------------------------------------------------------------
' Procedure : GetUnprocessedPartnerData
' DateTime  : 11/23/2004 14:33
' Author    : csr0901
' Purpose   : Gets a list of udb_partner.transaction_header records with 'unk' LynxIDs.
'             These records represent data sent to Lynx by a shop before that shop
'             received an electronic assignment from Lynx.  Due to that the incoming
'             data contained no LynxID, VehicleNumber, nor AssignmentID.  That data is
'             now stuck in the partner database until someone manually supplies the
'             missing required data listed above.
'---------------------------------------------------------------------------------------
'
Public Function GetUnprocessedPartnerData(ByVal strWebRoot As String, ByVal strSessionKey As String, _
                                          ByVal strWindowID As String, ByVal strUserID As String, _
                                          Optional strErrorXML As String = "") As String
                                                
          Const PROC_NAME As String = MODULE_NAME & "GetUnprocessedPartnerData: "

          'Objects that need cleaning up
          Dim objDataDOM As MSXML2.DOMDocument40
          Dim objReturnDOM As MSXML2.DOMDocument40
          Dim objErrorDOM As MSXML2.DOMDocument40
          Dim objAttribute As MSXML2.IXMLDOMAttribute
          Dim objNodeList As MSXML2.IXMLDOMNodeList
          Dim objNode As MSXML2.IXMLDOMNode
          Dim objRootElement As MSXML2.IXMLDOMElement
          Dim objExe As Object
          Dim objRS As ADODB.Recordset
          
          'Primitives that do not
          Dim lngLynxID As Long
          Dim strConnect As String
          Dim strProcName As String
          Dim strDataSource As String
          Dim strDataSourceCode As String
          Dim strPartnerTransID As String
          Dim strTransactionDate As String
          Dim strTransactionSource As String
          Dim strTransactionType As String
          Dim strClaimNo As String
          Dim strInsdLN As String
          Dim strOwnerLN As String
          Dim strModelYear As String
          Dim strMake As String
          Dim strModel As String
          Dim strBodyStyle As String
             
10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Initialize DOM objects.
30        Set objDataDOM = New MSXML2.DOMDocument40
40        Set objReturnDOM = New MSXML2.DOMDocument40
          
50        g_objDataAccessor.SetConnectString g_strPartnerConnStringStd
                 
          'Get the procedure name from the config XML.
60        strProcName = "uspTransactionHeaderUnprocessedGetDetail"

          'Get the XML from the database.
70        Set objRS = g_objDataAccessor.OpenRecordsetSp(strProcName)

          'Add a trace mesage to the debug log.
80        If g_blnDebugMode Then g_objEvents.Trace PROC_NAME & "After DB Execute."

          'Creates an emp.
90        LoadXml objReturnDOM, "<Root></Root>", PROC_NAME, "Parameter"
                  
          'Get the root element of the return XML.
100       Set objRootElement = objReturnDOM.documentElement
          
110       While Not objRS.EOF
                    
            'Loads and validates the xml.
120         LoadXml objDataDOM, objRS.fields("XML").Value, PROC_NAME, "data"
            
            ' Get the values from the recordset and embedded XML that will be returned to the caller.
130         strPartnerTransID = CStr(objRS.fields("PartnerTransID").Value)
140         strTransactionDate = CStr(objRS.fields("TransactionDate").Value)
150         strTransactionSource = CStr(objRS.fields("TransactionSource").Value)
160         strTransactionType = CStr(objRS.fields("TransactionType").Value)
170         strClaimNo = GetChildNodeText(objDataDOM, "//CLM_NO")
180         strInsdLN = GetChildNodeText(objDataDOM, "//INSD_LN")
190         strOwnerLN = GetChildNodeText(objDataDOM, "//OWNER_LN")
200         strModelYear = GetChildNodeText(objDataDOM, "//V_MODEL_YR")
210         strMake = GetChildNodeText(objDataDOM, "//V_MAKEDESC")
220         strModel = GetChildNodeText(objDataDOM, "//V_MODEL")
230         strBodyStyle = GetChildNodeText(objDataDOM, "//V_BSTYLE")
            
            ' Create a node to hold the retrieved values for this record and add it to the Root.
240         Set objNode = AddElement(objRootElement, "PartnerRecord")
            
            ' Add the retrieved values as attributes of the newly created node.
250         AddAttribute objNode, "PartnerTransID", strPartnerTransID
260         AddAttribute objNode, "TransactionDate", strTransactionDate
270         AddAttribute objNode, "TransactionSource", strTransactionSource
280         AddAttribute objNode, "TransactionType", strTransactionType
290         AddAttribute objNode, "ClaimNo", strClaimNo
300         AddAttribute objNode, "InsdLN", strInsdLN
310         AddAttribute objNode, "OwnerLN", strOwnerLN
320         AddAttribute objNode, "Year", strModelYear
330         AddAttribute objNode, "Make", strMake
340         AddAttribute objNode, "Model", strModel
350         AddAttribute objNode, "BodyStyle", strBodyStyle
              
360         objRS.MoveNext
370       Wend
          
          ' If there are errors append them to the xml to be returned.
380       If strErrorXML <> vbNullString Then
          
390         Set objErrorDOM = New MSXML2.DOMDocument40
                  
            ' Load and validate the passed xml error string.
400         LoadXml objErrorDOM, strErrorXML, PROC_NAME, "errors"
            
            ' Grab the Errors node which contains the individual Error nodes.
410         Set objNode = objErrorDOM.selectSingleNode("/Root/Errors")
            
            ' Append the Errrors node to the Root node of the XML to be transformed
420         objRootElement.appendChild objNode
            
430       End If
          
          ' Create an instance of DataPresenter to perform the XML to HTML transformation.
440       Set objExe = CreateObject("DataPresenter.CExecute")
          
          ' Initialize DataPresenter.
450       objExe.InitializeEx strWebRoot, strSessionKey, strWindowID, strUserID
                      
          'Perform the transformation and return the resultant HTML.
460       GetUnprocessedPartnerData = objExe.TransformXML(objReturnDOM.xml, "UnprocessedPartnerData.xsl")

          'Add another trace message to note exit?
470       If g_blnDebugMode Then g_objEvents.Trace "Return = " & GetUnprocessedPartnerData, PROC_NAME & "Finished"
          

ErrorHandler:

          'Object clean up.
480       Set objDataDOM = Nothing
490       Set objErrorDOM = Nothing
500       Set objReturnDOM = Nothing
510       Set objAttribute = Nothing
520       Set objNodeList = Nothing
530       Set objNode = Nothing
540       Set objRootElement = Nothing
550       Set objExe = Nothing
          
560       If Not objRS Is Nothing Then
570         objRS.Close
580         Set objRS = Nothing
590       End If
          
          'Everything below will be sent out when error occur...
600       If Err.Number <> 0 Then
610           g_objEvents.Env "Stored Proc = " & strProcName
620           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
630       End If

640       TerminateGlobals
          
End Function

'---------------------------------------------------------------------------------------
' Procedure : FixUnprocessedPartnerData
' DateTime  : 11/23/2004 14:26
' Author    : Dan Price
' Parameters: strData - delimited string of the format: PartnerTransID,LynxID,VehicleNumber;...
'           : strWebRoot - required because
' Purpose   : This function parse strData resolving the AssignmentID associated with the each passed
'             LynxID/VehicleNumber combination, pulling the partner XML assocaited with PartnerTransID,
'             inserting the LynxID, VehicleNumber, and AssignmentID appropriately into the partner XML,
'             submit the corrected partnerXML to the APD database, and finally update the Partner record,
'             with the appropriate values.
'---------------------------------------------------------------------------------------
'
Public Function FixUnprocessedPartnerData(ByVal strData As String, ByVal strWebRoot As String, ByVal strSessionKey As String, _
                                          ByVal strWindowID As String, ByVal strUserID As String) As String
                                          
        Const PROC_NAME As String = MODULE_NAME & "FixUnprocessedPartnerData: "
        
        ' Objects that require cleaning up.
        Dim objAttribute As MSXML2.IXMLDOMAttribute
        Dim objDataDOM As New MSXML2.DOMDocument40
        Dim objNode As MSXML2.IXMLDOMNode
        Dim objReturnDOM As New MSXML2.DOMDocument40
        Dim objErrorNode As MSXML2.IXMLDOMNode
        Dim objRS As ADODB.Recordset
        
        ' Primitives that do not.
        Dim intErrorCount As Integer
        Dim lngIter As Long
        Dim lngUBound As Long
        Dim strAssignmentID As String
        Dim strErrorXML As String
        Dim strLynxID As Long
        Dim strPartner As String
        Dim strPartnerTransID As String
        Dim strResult As String
        Dim strTransactionType As String
        Dim strVehicleNumber As Integer
        Dim strXML As String
        Dim varrData As Variant
        Dim varrDataRow As Variant
        
                 
10      On Error GoTo ErrorHandler

20      InitializeGlobals
        
        ' Initialize local objects and variables.
30      intErrorCount = 0
40      LoadXml objReturnDOM, "<Root><Errors></Errors></Root>", PROC_NAME, "ReturnData"
50      Set objErrorNode = objReturnDOM.selectSingleNode("/Root/Errors")
            
        ' Remove trailing semicolon (;) if it exists.
60      If Right(strData, 1) = ";" Then strData = Left(strData, Len(strData) - 1)
        
        ' Break the data into individual PartnerTransID,LynxID,VehicleNumber strings.
70      varrDataRow = Split(strData, ";")
        
        ' Get the upper bound of the newly created array.
80      lngUBound = UBound(varrDataRow)
          
        ' Loop through the array.
90      For lngIter = 0 To lngUBound
        
          ' Break the data row into individual data items.
100       varrData = Split(varrDataRow(lngIter), ",")
          
          ' Extract the individual data items.
110       strPartnerTransID = varrData(0)
120       strLynxID = varrData(1)
130       strVehicleNumber = varrData(2)
          
          ' Point data accessor to the APD database.
140       g_objDataAccessor.SetConnectString g_strLYNXConnStringStd
          
          ' Temporarily turn off error handling
150       On Error Resume Next
              
          'Get the assignment ID of the assignment for this vehicle.
160       strAssignmentID = g_objDataAccessor.ExecuteSpNamedParams(GetConfig("ClaimPoint/SPMap/AssignmentDecodeSP"), _
                                                             "@LynxID", CLng(strLynxID), _
                                                             "@VehicleNumber", CInt(strVehicleNumber), _
                                                             "@AssignmentSuffix", "")
          
          ' If any error occured, assume an AssignmentID could not be found for the passed LynxID/Vehicle Number.
          ' This will be handled below due to the fact that strAssignmentID is still empty.
170       Err.Clear
          
          ' Resume normal error handling.
180       On Error GoTo ErrorHandler
                                                             
          ' Continue processing this partner record only if an AssignmentID was found.
190       If strAssignmentID <> vbNullString Then
            
            ' Point data accessor to the Partner database.
200         g_objDataAccessor.SetConnectString g_strPartnerConnStringStd
              
            ' Get the transaction header record.
210         Set objRS = g_objDataAccessor.OpenRecordsetNamedParams("uspTransactionHeaderUnprocessedGetDetail", _
                                                                   "@PartnerTransID", CLng(strPartnerTransID))
            
            ' Make sure a record still exists for this PartnerTransID.
220         If Not objRS.EOF Then
              ' Load xml string into the DOM.
230           LoadXml objDataDOM, objRS.fields("XML").Value, PROC_NAME, "utb_transaction_header.xml"
                      
              ' Get some values from the retrieved record.
240           strPartner = objRS.fields("TransactionSource").Value
250           strTransactionType = objRS.fields("TransactionType").Value
                              
260           If strPartner = "ADP" Then
              
                ' Set the missing values in the XML.
270             objDataDOM.documentElement.Attributes.getNamedItem("LynxID").Text = strLynxID
280             objDataDOM.documentElement.Attributes.getNamedItem("VehicleID").Text = strVehicleNumber
290             objDataDOM.documentElement.Attributes.getNamedItem("AssignmentID").Text = strAssignmentID
                
                ' Hack neceesary because InitializeGlobals() assumes any XML was passed in original call,
                ' however here we pull the XML from the database after InitializeGlobals has already been called.
300             If g_objPassedDocument Is Nothing Then Set g_objPassedDocument = New MSXML2.DOMDocument40
310             LoadXml g_objPassedDocument, objDataDOM.xml, PROC_NAME, "PartnerTransID=" & strPartnerTransID
                
                ' Submit the xml back thru the system
320             strResult = ConsumePartnerXmlTransaction(objDataDOM.xml, strPartner, False)
                      
                ' Handle any error return from the data submission process.
330             If Left(strResult, 6) = "<Error" Then
        
                  ' PartnerTransID not found.  Create a node to hold the error text and add it to the Root.
340               Set objNode = AddElement(objErrorNode, "Error")
350               objNode.Text = "The specified " & strTransactionType & " (PartnerTransID=" & strPartnerTransID & ") could " & _
                                 "not be successfully loaded into the APD database."
360               intErrorCount = intErrorCount + 1
                  'g_objevents.HandleEvent
370             Else
                  ' Point data accessor to the Partner database.
380               g_objDataAccessor.SetConnectString g_strPartnerConnStringStd
                  
                  ' Delete the original Partner record.
390               g_objDataAccessor.ExecuteSpNamedParams "uspTransactionHeaderDel", "@PartnerTransID", strPartnerTransID
                
400             End If
                
410           Else
                ' Not ADP data.
420               Set objNode = AddElement(objErrorNode, "Error")
430               objNode.Text = "This process is currently only supported for ADP estimates.  Could not attach this " & _
                                 strTransactionType & " to LynxID: " & strLynxID & "-" & strVehicleNumber & "."
440               intErrorCount = intErrorCount + 1
                  'g_objevents.HandleEvent
450           End If
              
460         Else
              ' PartnerTransID not found.  Create a node to hold the error text and add it to the Root.
470           Set objNode = AddElement(objErrorNode, "Error")
480           objNode.Text = "The specified " & strTransactionType & " (PartnerTransID=" & strPartnerTransID & ") could not be found in the " & _
                             "Partner database.  Check your claim, someone may have already fixed the data you are attempting to fix."
490           intErrorCount = intErrorCount + 1
              'g_objevents.HandleEvent
500         End If
            
510       Else
            ' AssignmentID not found.  Create a node to hold the error text and add it to the Root.
520         Set objNode = AddElement(objErrorNode, "Error")
530         objNode.Text = "No Assignment ID could be found for claim, " & strLynxID & "-" & strVehicleNumber & ".  Please ensure you " & _
                           "entered a valid LynxID/Vehicle Number combination and that that vehicle's assignment is still valid."
540         intErrorCount = intErrorCount + 1
            'g_objevents.HandleEvent
550       End If
          
560     Next
                 
        ' Grab error XML.  If there were no errors set the value to null
570     strErrorXML = IIf(intErrorCount = 0, vbNullString, objReturnDOM.xml)
                 
        ' Call GetUnprocessedPartnerData to get the updated list of partner records an perorm the transformation to HTML.  This is a
        ' little hokey but these are highly specalized procedures, this provides a way of including any error xml in the
        ' transformation, and saves the web page a round trip.  Nice rationalization, huh?
580     FixUnprocessedPartnerData = GetUnprocessedPartnerData(strWebRoot, strSessionKey, strWindowID, strUserID, strErrorXML)

         
ErrorHandler:
                    
          'Object clean up.
590       If Not objRS Is Nothing Then
600         If objRS.State = adStateOpen Then objRS.Close
610         Set objRS = Nothing
620       End If
          
630       Set objDataDOM = Nothing
640       Set objNode = Nothing
650       Set objReturnDOM = Nothing
660       Set objErrorNode = Nothing
          
          'Everything below will be sent out when error occur...
670       If Err.Number <> 0 Then
680           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
690       End If

700       TerminateGlobals
        
End Function


