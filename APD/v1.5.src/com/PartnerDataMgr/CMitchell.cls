VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMitchell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'********************************************************************************
'* Component LAPDPartnerDataMgr : Class CMitchell
'*
'* Processing methods specific to Mitchell
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CMitchell."

Implements CPartner

'********************************************************************************
'* This method initializes processing of the documents.
'* Returns True if processing should continue.
'********************************************************************************
Public Function CPartner_InitProcessDocument() As Boolean
10        Err.Raise eNotImplemented, MODULE_NAME & "InitProcessDocument()", "Method not implemented"
20        CPartner_InitProcessDocument = False
End Function

'********************************************************************************
'* This method processing documents to the partner database.
'********************************************************************************
Public Function CPartner_PartnerDatabaseTransfer(ByVal blnSendReceipt As Boolean) As Boolean
10        CPartner_PartnerDatabaseTransfer = False
20        Err.Raise eNotImplemented, MODULE_NAME & "PartnerDatabaseTransfer()", "Method not implemented"
End Function

'********************************************************************************
'* This method processing documents to the APD database.
'********************************************************************************
Public Function CPartner_ApdDatabaseTransfer() As String
10        Err.Raise eNotImplemented, MODULE_NAME & "ApdDatabaseTransfer()", "Method not implemented"
End Function

'********************************************************************************
'* This method transmits documents to the partner.
'********************************************************************************
Public Function CPartner_TransferToPartner() As String
10        Err.Raise eNotImplemented, MODULE_NAME & "TransferToPartner()", "Method not implemented"
End Function

Public Function CPartner_InsertPrintImageIntoAPD( _
    ByVal strXML As String, _
    ByVal lngPartnerTransID As Long) As Long
          
10        Err.Raise eNotImplemented, MODULE_NAME & "InsertPrintImageIntoAPD()", "Method not implemented"
End Function

Public Sub CPartner_SendHeartBeatRequest()
10        Err.Raise eNotImplemented, MODULE_NAME & "SendHeartBeatRequest()", "Method not implemented"
End Sub


Private Function CPartner_PartnerDatabaseTransferV2(ByVal blnSendReceipt As Boolean) As Boolean
    Const PROC_NAME As String = MODULE_NAME & "PartnerDatabaseTransferV2: "
    Err.Raise eNotImplemented, PROC_NAME, "NOT IMPLEMENTED"
End Function

Private Function CPartner_ApdDatabaseTransferV2() As String
    Const PROC_NAME As String = MODULE_NAME & "ApdDatabaseTransferV2: "
    Err.Raise eNotImplemented, PROC_NAME, "NOT IMPLEMENTED"
End Function

Private Function CPartner_InitProcessDocumentV2(ByVal strTradingPartner As String, ByVal strPassedDocument As String) As Boolean
    Const PROC_NAME As String = MODULE_NAME & "InitProcessDocumentV2: "
    Err.Raise eNotImplemented, PROC_NAME, "NOT IMPLEMENTED"
End Function

Private Function CPartner_InsertPrintImageIntoAPDV2(ByVal strXML As String, ByVal lngPartnerTransID As Long) As Long
    Const PROC_NAME As String = MODULE_NAME & "InsertPrintImageIntoAPDV2: "
    Err.Raise eNotImplemented, PROC_NAME, "NOT IMPLEMENTED"
End Function

