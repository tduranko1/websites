VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnitTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Class CUnitTest
'*
'* This purpose of this class is to establish a standard unit test interface.
'*
'* A client test application will will instantiate and call this interface in
'* turn for all components that implement it.  Results for each component will
'* be displayed to the tester.
'*
'* To implement for your component, include a copy of this file into your project.
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & ".CUnitTest."

'The number of tests called from this CUnitTest implementation.
Private Const mcintNumberOfTests As Integer = 8

Private mstrResults As String
Private mintErrors As String

'********************************************************************************
'* Returns a count of the number of tests that can be run for this application.
'********************************************************************************
Public Function NumTests() As Integer
10        NumTests = mcintNumberOfTests
End Function

'********************************************************************************
'* Returns the indexed test description.
'********************************************************************************
Public Function TestDesc(ByVal intIndex As Integer) As String
          Const PROC_NAME As String = MODULE_NAME & "TestDesc: "
          
10        Select Case intIndex
          
              'CLS modules
              Case 1: TestDesc = APP_NAME & "CADP"
20            Case 2: TestDesc = APP_NAME & "CAutoVerse"
30            Case 3: TestDesc = APP_NAME & "CCCC"
40            Case 4: TestDesc = APP_NAME & "CExecute"
50            Case 5: TestDesc = APP_NAME & "CMitchell"
60            Case 6: TestDesc = APP_NAME & "CPartner"
70            Case 7: TestDesc = APP_NAME & "CPartnerDataXfer"
80            Case 8: TestDesc = APP_NAME & "CSceneGenesis"
              
        Case Else:
90                Err.Raise vbObjectError + 1, PROC_NAME, "Passed test index out of range!"
100       End Select
          
End Function

'********************************************************************************
'* Runs the indexed test.
'* Returns the results as a giant formatted string.
'* Returns an error count through intErrors.
'********************************************************************************
Public Function RunTest(ByVal intIndex As Integer, ByRef intErrors As Integer) As String
          Const PROC_NAME As String = MODULE_NAME & "RunTest: "

10        Init intIndex 'Initialize results counters.
          
          'Run the test.
20        Select Case intIndex
          
              'BAS modules
              Case 1: RunTest_CADP (intErrors)
30            Case 2: RunTest_CAutoVerse (intErrors)
40            Case 3: RunTest_CCCC (intErrors)
50            Case 4: RunTest_CExecute (intErrors)
60            Case 5: RunTest_CMitchell (intErrors)
70            Case 6: RunTest_CPartner (intErrors)
80            Case 7: RunTest_CPartnerDataXfer (intErrors)
90            Case 8: RunTest_CSceneGenesis (intErrors)
              
        Case Else:
100               Err.Raise vbObjectError + 1, PROC_NAME, "Passed test index out of range!"
110       End Select
          
120       RunTest = Done(intErrors)
End Function

'********************************************************************************
'* Private results formatting helpers
'********************************************************************************

'Initialize results
Private Sub Init(ByVal intIndex As Integer)
10        mstrResults = vbCrLf & "TESTING : " & TestDesc(intIndex) & vbCrLf
20        mintErrors = 0
End Sub

Private Sub Success(ByVal strLine As String)
10        mstrResults = mstrResults & "SUCCESS : " & strLine & vbCrLf
End Sub

Private Sub Verify(ByVal strLine As String)
10        mstrResults = mstrResults & "VERIFY : " & strLine & "  Does this look right?" & vbCrLf
End Sub

Private Sub Failure(ByVal strLine As String)
10        mintErrors = mintErrors + 1
20        mstrResults = mstrResults & "FAILURE : " & strLine & vbCrLf
End Sub

'Add new results line.
Private Function Done(ByRef intErrors As Integer) As String
10        intErrors = mintErrors
20        Done = mstrResults
End Function

'********************************************************************************
'* ADD PRIVATE TEST METHODS BELOW THIS LINE.
'********************************************************************************

'********************************************************************************
'* Tests Class Module CADP
'********************************************************************************
Private Function RunTest_CADP(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CADP
20        If obj Is Nothing Then
30            Failure "Could not create CADP"
40        Else
50            Success "Created CADP object."
60        End If
          
          'Other CADP specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CAutoVerse
'********************************************************************************
Private Function RunTest_CAutoVerse(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CAutoVerse
20        If obj Is Nothing Then
30            Failure "Could not create CAutoVerse"
40        Else
50            Success "Created CAutoVerse object."
60        End If
          
          'Other CAutoVerse specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CCCC
'********************************************************************************
Private Function RunTest_CCCC(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CCCC
20        If obj Is Nothing Then
30            Failure "Could not create CCCC"
40        Else
50            Success "Created CCCC object."
60        End If
          
          'Other CCCC specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CExecute
'********************************************************************************
Private Function RunTest_CExecute(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CExecute
20        If obj Is Nothing Then
30            Failure "Could not create CExecute"
40        Else
50            Success "Created CExecute object."
60        End If
          
          'Other CExecute specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CMitchell
'********************************************************************************
Private Function RunTest_CMitchell(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CMitchell
20        If obj Is Nothing Then
30            Failure "Could not create CMitchell"
40        Else
50            Success "Created CMitchell object."
60        End If
          
          'Other CMitchell specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CPartner
'********************************************************************************
Private Function RunTest_CPartner(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CPartner
20        If obj Is Nothing Then
30            Failure "Could not create CPartner"
40        Else
50            Success "Created CPartner object."
60        End If
          
          'Other CPartner specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CPartnerDataXfer
'********************************************************************************
Private Function RunTest_CPartnerDataXfer(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CPartnerDataXfer
20        If obj Is Nothing Then
30            Failure "Could not create CPartnerDataXfer"
40        Else
50            Success "Created CPartnerDataXfer object."
60        End If
          
          'Other CPartnerDataXfer specific tests:
          
70        Set obj = Nothing
End Function

'********************************************************************************
'* Tests Class Module CSceneGenesis
'********************************************************************************
Private Function RunTest_CSceneGenesis(ByRef intErrors As Integer) As String
10        On Error Resume Next
          
          'General object creation test:
          Dim obj As New CSceneGenesis
20        If obj Is Nothing Then
30            Failure "Could not create CSceneGenesis"
40        Else
50            Success "Created CSceneGenesis object."
60        End If
          
          'Other CSceneGenesis specific tests:
          
70        Set obj = Nothing
End Function


