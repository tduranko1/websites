Attribute VB_Name = "MEscapeUtils"
'********************************************************************************
'* Component SiteUtilities : Module MstrEscapeUtils
'*
'* Utility methods for escaping strings.
'* Note that these type of functions are relatively expensive CPU wise in VB.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "MstrEscapeUtils."

'********************************************************************************
'* Returns string suitable for SQL queries by replacing all single quotes
'* with 2 single quotes so that they will be accurately interpreted by the
'* SQL engine as a literal single quote rather than a string delimiter
'********************************************************************************
Public Function SQLQueryString(ByRef strIn As String) As String
10        SQLQueryString = Replace(strIn, "'", "''")
End Function

'********************************************************************************
'* Escapes a string using URL type escaping (i.e. '%20' for a space ).
'*
'* Replaces all non-alphanumeric characters in a string as strEscaped sequences
'* compatible with strEscaped URL strings.  That is, all special characters are
'* replaced with %HH where HH is the two digit hex value of the character.
'* Note that although we use the same mechanism as URL escaping, we are more
'* regiorous in our interpretation of "special" characters.  For example,
'* dashes and underscores will be strEscaped although they are not typically
'* strEscaped in URL strings.
'********************************************************************************
Public Function EscapeString(ByRef strIn As String) As String
          Dim strOut As String
          Dim intChar As Integer
          Dim strEscape As String
          Dim i As Integer
          
10        strOut = strIn
          
20        For i = Len(strIn) To 1 Step -1
30            intChar = Asc(Mid(strIn, i, 1))
40            If ((intChar < 48) And (intChar <> 32)) _
                  Or ((intChar > 57) And (intChar < 65)) _
                  Or ((intChar > 90) And (intChar < 97)) _
                  Or (intChar > 122) Then
                  
50                strEscape = Hex(intChar)
60                If Len(strEscape) < 2 Then
70                    strEscape = "%0" & strEscape
80                Else
90                    strEscape = "%" & strEscape
100               End If
110               strOut = Left(strOut, i - 1) + strEscape _
                      + Right(strOut, Len(strOut) - i)
120           End If
130       Next
140       EscapeString = Replace(strOut, " ", "+")
End Function

'********************************************************************************
'* Un-Escapes a string of URL type escaping (i.e. '%20' for a space ).
'*
'* Replaces all strEscape sequences within a string with the character represented
'* by the strEscape sequence.  An strEscape sequence has the form %HH where HH is a
'* two-digit hexidecimal number representing the ASCII code of the original
'* character.  Note that this function does not properly check if the string
'* was validly strEscaped in the first place.  When processing a string that
'* contains literal %'s this function will behave unpredictably.
'********************************************************************************
Public Function UnEscapeString(ByRef strIn As String) As String
          Dim strOut As String, i As Integer
          
10        strOut = Replace(strIn, "+", " ")
          
20        For i = 1 To Len(strOut)
              
30            If Mid(strOut, i, 1) = "%" Then
                  
40                If Mid(strOut, i + 1, 1) = "u" Then
                      'Convert unicode character.
50                    strOut = Left(strOut, i - 1) _
                          & ChrW(CInt("&H" & Mid(strOut, i + 2, 4))) _
                          & Right(strOut, Len(strOut) - i - 5)
60                Else
                      'Convert standard byte character.
70                    strOut = Left(strOut, i - 1) _
                          & Chr(CInt("&H" & Mid(strOut, i + 1, 2))) _
                          & Right(strOut, Len(strOut) - i - 2)
80                End If
              
90            End If
          
100       Next
110       UnEscapeString = strOut
End Function

'********************************************************************************
'* Escapes a string of URL type escaping (i.e. '%20' for a space )
'* except this does NOT un-escape those characters not allowed in XML.
'* Thus this function is the nearly the same as UnEscapeString.
'********************************************************************************
Public Function UnEscapeStringAsXmlCompliant(ByRef strIn As String) As String
          Dim strOut As String, i As Integer
          Dim strCharacter As String, iLen As Integer
          
10        strOut = Replace(strIn, "+", " ")
          
20        For i = 1 To Len(strOut)
              
30            If Mid(strOut, i, 1) = "%" Then
                  
40                If Mid(strOut, i + 1, 1) = "u" Then
                      'Convert unicode character.
50                    strCharacter = ChrW(CInt("&H" & Mid(strOut, i + 2, 4)))
60                    iLen = 5
70                Else
                      'Convert standard byte character.
80                    strCharacter = Chr(CInt("&H" & Mid(strOut, i + 1, 2)))
90                    iLen = 2
100               End If

110               Select Case strCharacter
                      Case "<"
120                   Case ">"
130                   Case "&"
140                   Case Else
150                       strOut = Left(strOut, i - 1) _
                              & strCharacter _
                              & Right(strOut, Len(strOut) - i - iLen)
160               End Select
                  
170           End If
          
180       Next
190       UnEscapeStringAsXmlCompliant = strOut
End Function

'********************************************************************************
'* Removes strange formatting characters from XML.
'*
'* This function removes characters that are non-standard.
'* It was created to strip these characters out of XML passed to our partners,
'* whose mainframes could not handle 'A0' in spite of it being valid XML.
'********************************************************************************
Public Function StripNonStdChars(ByRef strIn As String) As String
          Dim strOut As String, i As Integer
          Dim intChar As Integer, iLen As Integer
          
10        strOut = strIn
20        For i = 1 To Len(strOut)
          
30            intChar = Asc(Mid(strOut, i, 1))
              
40            If (intChar < 32) Or (intChar > 126) Then
                  
50                strOut = Left(strOut, i - 1) & " " & Mid(strOut, i + 1)
                  
60            End If
70        Next
          
80        StripNonStdChars = strOut
End Function

'********************************************************************************
'* Escapes an XML string of any markup characters (i.e. '&gt;' for '>')
'********************************************************************************
Public Function EscapeXmlMarkup(ByRef strIn As String) As String
10        EscapeXmlMarkup = Replace(Replace(Replace(Replace(Replace( _
              strIn, "&", "&amp;"), ">", "&gt;"), """", "&quot;"), "'", "&apos;"), "<", "&lt;")
End Function

'********************************************************************************
'* UnEscapes an XML string of any markup characters (i.e. '>' for '&gt;')
'********************************************************************************
Public Function UnEscapeXmlMarkup(ByRef strIn As String) As String
10        UnEscapeXmlMarkup = Replace(Replace(Replace(Replace(Replace( _
              strIn, "&lt;", "<"), "&gt;", ">"), "&quot;", """"), "&apos;", "'"), "&amp;", "&")
End Function


