Attribute VB_Name = "MDomUtils"
'********************************************************************************
'* Component EcadAccessor : Module MDomUtils
'*
'* Utility methods used with DOM Documents.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "MDomUtils."

'Internal error codes for this module.
Public Enum EventCodes
    eDomDataValidationFailed = &H80065000 + &H100
    eXmlInvalid
    eXmlMissingAttribute
    eXmlMissingElement
    eXmlEmptyNodeSet
End Enum

'********************************************************************************
'* Loads the passed Dom from the passed string and reports any errors.
'********************************************************************************
Public Sub LoadXml( _
    ByRef objDom As MSXML2.DOMDocument40, _
    ByRef strXml As String, _
    Optional ByVal strCallerName As String, _
    Optional ByVal strXmlDescription As String)

          Dim blnValidXml As Boolean

          'loadXml returns true if the load succeeded.
10        blnValidXml = objDom.LoadXml(strXml)

          'We can double check for problems by looking at the parse error code.
20        blnValidXml = blnValidXml And CBool(objDom.parseError.errorCode = 0)

          'If either test proved problematic, raise an error.
30        If Not blnValidXml Then
40            Err.Raise eXmlInvalid, strCallerName & ":LoadXml", _
                  "The " & strXmlDescription & " xml could not be loaded because:" _
                  & vbCrLf & FormatDomParseError(objDom.parseError)
50        End If

End Sub

'********************************************************************************
'* Loads the passed Dom from the passed file name string and reports any errors.
'********************************************************************************
Public Sub LoadXmlFile( _
    ByRef objDom As MSXML2.DOMDocument40, _
    ByRef strFileName As String, _
    Optional ByVal strCallerName As String, _
    Optional ByVal strXmlDescription As String)

          Dim blnValidXml As Boolean

          'loadXml returns true if the load succeeded.
10        blnValidXml = objDom.Load(strFileName)

          'We can double check for problems by looking at the parse error code.
20        blnValidXml = blnValidXml And CBool(objDom.parseError.errorCode = 0)

          'If either test proved problematic, raise an error.
30        If Not blnValidXml Then
40            Err.Raise eXmlInvalid, strCallerName & ":LoadXmlFile", _
                  "The " & strXmlDescription & " xml could not be loaded because:" _
                  & vbCrLf & FormatDomParseError(objDom.parseError) & "      File: " & strFileName
50        End If

End Sub

'********************************************************************************
'* Returns the passed XML escaped of any bad characters.
'* Cheese alert - we do this just by loading it into
'* a DOM and then converting it back into a string.
'********************************************************************************
Public Function EscapeXml(ByRef strXml As String) As String
          Dim objDom As New MSXML2.DOMDocument40
10        LoadXml objDom, strXml, "CleanUpXml"
20        EscapeXml = objDom.xml
30        Set objDom = Nothing
End Function

'********************************************************************************
'* Returns the specified child node of the passed node.
'********************************************************************************
Public Function GetChildNode( _
    ByRef objNode As IXMLDOMNode, _
    ByVal strPath As String, _
    Optional ByVal blnMustExist As Boolean = False) As IXMLDOMNode

          Dim objChild As IXMLDOMNode
10        Set objChild = objNode.selectSingleNode(strPath)

20        If objChild Is Nothing Then
30            If blnMustExist = True Then
40                Err.Raise eXmlMissingElement, "GetChildNode('" & strPath & "')", objNode.nodeName & " is missing a '" & strPath & "' child node."
50            End If
60        Else
70            Set GetChildNode = objChild
80        End If

90        Set objChild = Nothing
End Function

'********************************************************************************
'* Returns the specified child node text of the passed node.
'********************************************************************************
Public Function GetChildNodeText( _
    ByRef objNode As IXMLDOMNode, _
    ByVal strPath As String, _
    Optional ByVal blnMustExist As Boolean = False) As String

          Dim objChild As IXMLDOMNode
10        Set objChild = GetChildNode(objNode, strPath, blnMustExist)

20        If objChild Is Nothing Then
30            GetChildNodeText = ""
40        Else
50            GetChildNodeText = objChild.Text
60        End If

End Function

'********************************************************************************
'* Returns a node list for specified XPATH of the passed node.
'********************************************************************************
Public Function GetChildNodeList(ByRef objNode As IXMLDOMNode, ByVal strPath As String, Optional ByVal blnMustExist = False) As IXMLDOMNodeList

        Dim objChildList As IXMLDOMNodeList
10      Set objChildList = objNode.selectNodes(strPath)
  
20      If objChildList Is Nothing Then
30        If blnMustExist Then
40          Err.Raise eXmlEmptyNodeSet, "GetChildNodeList('" & strPath & "')", objNode.nodeName & " is missing a '" & strPath & "' child nodes."
50        End If
60      ElseIf objChildList.length = 0 And blnMustExist Then
70        Err.Raise eXmlEmptyNodeSet, "GetChildNodeList('" & strPath & "')", objNode.nodeName & " is missing a '" & strPath & "' child nodes."
80      Else
90        Set GetChildNodeList = objChildList
100     End If
  
110     Set objChildList = Nothing
  

End Function

'********************************************************************************
'* Syntax:      ValidateDomData( objDom, "Validate.xsl" )
'* Parameters:  domXML - the xml to validate.
'*              strXslFileName - the xsl file to validate with.
'*              objEvents - events object borrowed from caller.
'* Purpose:     Uses a style sheet to validate the passed Dom.
'*              This style sheet will transform to an empty string if everything
'*              validates just fine, or will return error strings compatible
'*              with SiteUtilities.CEvents.HandleMultiEvents().
'* Returns:     Nothing.  Will Raise if the validation failed.
'********************************************************************************
Public Function ValidateDomData( _
    ByRef domXML As MSXML2.DOMDocument40, _
    ByRef strXslFileName As String, _
    ByRef objEvents As Object, _
    Optional ByVal blnRaise As Boolean = True) As String

          Const PROC_NAME As String = MODULE_NAME & "ValidateDomData: "

          Dim domXSL As MSXML2.DOMDocument40

          Dim strResult As String
          Dim g_blnDebugMode As Boolean

10        On Error GoTo ErrorHandler

          'Get debug mode status.
20        g_blnDebugMode = objEvents.IsDebugMode

30        If g_blnDebugMode Then objEvents.Trace "XML = " & domXML.xml, _
              PROC_NAME & "Validating XML versus file " & strXslFileName

          'Load the XSL to validate with.
40        Set domXSL = New MSXML2.DOMDocument40

50        LoadXmlFile domXSL, strXslFileName, PROC_NAME, "Validation XSL"

          'Transform the XML with the XSL style sheet.
60        strResult = domXML.transformNode(domXSL)

70        If Left(strResult, 1) = "|" Then
80            strResult = Mid(strResult, 2)
90        End If
          
100       ValidateDomData = strResult

          'If the result has contents then pass along then raise.
110       If Len(strResult) > 0 Then
120           objEvents.HandleMultiEvents eDomDataValidationFailed, PROC_NAME, strResult
130       Else
140           If g_blnDebugMode Then objEvents.Trace "", PROC_NAME & "Passed Data Validation"
150       End If

ErrorHandler:

160       Set domXSL = Nothing

170       If Err.Number <> 0 Then
180           objEvents.Env "XML =" & domXML.xml
190           objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "XSL file =" & strXslFileName, "", blnRaise
200       End If

End Function

'********************************************************************************
'* Adds an attribute to the passed dom element
'********************************************************************************
Public Sub AddAttribute( _
    ByRef objElem As IXMLDOMElement, _
    ByVal strName As String, _
    ByVal strValue As String)

          Dim objAtt As MSXML2.IXMLDOMAttribute

10        Set objAtt = objElem.ownerDocument.createAttribute(strName)
20        objAtt.Value = strValue
30        objElem.setAttributeNode objAtt

40        Set objAtt = Nothing

End Sub

'********************************************************************************
'* Adds an attribute to the passed dom element
'********************************************************************************
Public Function AddElement( _
    ByRef objElem As IXMLDOMElement, _
    ByVal strName As String) As MSXML2.IXMLDOMElement

          Dim objNewElem As MSXML2.IXMLDOMElement

10        Set objNewElem = objElem.ownerDocument.createElement(strName)
20        objElem.appendChild objNewElem

30        Set AddElement = objNewElem
40        Set objNewElem = Nothing

End Function

'********************************************************************************
'* Renames an attribute of the passed dom element
'********************************************************************************
Public Sub RenameAttribute( _
    ByRef objElem As IXMLDOMElement, _
    ByVal strPath As String, _
    ByVal strNewName As String)

          Dim objAtt As MSXML2.IXMLDOMAttribute
10        Set objAtt = GetChildNode(objElem, strPath, False)

20        If Not objAtt Is Nothing Then

              Dim strValue As String
30            strValue = objAtt.Text

40            objElem.removeAttribute objAtt.Name
50            Set objAtt = Nothing

60            AddAttribute objElem, strNewName, strValue

70        End If

End Sub

'********************************************************************************
'* Renames all matching attributes below in the passed dom element.
'********************************************************************************
Public Sub RenameAllAttributes( _
    ByRef objElem As IXMLDOMElement, _
    ByVal strOldName As String, _
    ByVal strNewName As String)

          'Rename any child attribute that matches.
10        RenameAttribute objElem, "@" & strOldName, strNewName

          'Now recursively do the same through all children.
          Dim objChild As MSXML2.IXMLDOMNode
20        If objElem.childNodes.length > 0 Then
30            For Each objChild In objElem.childNodes
40                If Not objChild Is Nothing Then
50                    RenameAllAttributes objChild, strOldName, strNewName
60                End If
70            Next
80        End If

End Sub

'********************************************************************************
'* Removes all blank attributes of the passed element.
'********************************************************************************
Public Sub RemoveBlankAttributes(ByRef objElem As IXMLDOMElement)
          Dim objAtt As MSXML2.IXMLDOMAttribute
          Dim intIdx As Integer
          
10        While intIdx < objElem.Attributes.length
20            Set objAtt = objElem.Attributes(intIdx)
30            If objAtt.Text = "" Then
40                objElem.removeAttribute (objAtt.nodeName)
50            Else
60                intIdx = intIdx + 1
70            End If
80        Wend

End Sub

'********************************************************************************
'* Removes all blank attributes below in the passed dom element.
'********************************************************************************
Public Sub RemoveAllChildBlankAttributes(ByRef objElem As IXMLDOMElement)

          'Remove any blank child attributes.
10        RemoveBlankAttributes objElem

          'Now recursively do the same through all children.
          Dim objChild As MSXML2.IXMLDOMNode
20        If objElem.childNodes.length > 0 Then
30            For Each objChild In objElem.childNodes
40                If Not objChild Is Nothing Then
50                    RemoveBlankAttributes objChild
60                End If
70            Next
80        End If

End Sub

'********************************************************************************
'* Syntax:      MapAPDReferenceData( objData, "DataMap.xml" )
'* Parameters:  objDom - the DOM document to convert.
'*              strMapFile - the absolute path to the map file.
'* Purpose:     Uses the map file to convert codes to reference values.
'*              Requires that the DOM be formatted to the APD DB standard.
'* Returns:     Nothing.
'********************************************************************************
Public Sub MapAPDReferenceData( _
    ByRef objDom As MSXML2.DOMDocument40, _
    ByVal strMapFile As String, _
    ByRef objEvents As Object)

          Const PROC_NAME As String = MODULE_NAME & "MapAPDReferenceData: "

          'Object variable declarations
          Dim objMapDom As MSXML2.DOMDocument40

          Dim objMapList As MSXML2.IXMLDOMNodeList
          Dim objAttParentList As MSXML2.IXMLDOMNodeList
          Dim objReplaceList As MSXML2.IXMLDOMNodeList

          Dim objMapNode As MSXML2.IXMLDOMNode
          Dim objAttParentNode As MSXML2.IXMLDOMNode
          Dim objReplaceNode As MSXML2.IXMLDOMNode

10        On Error GoTo ErrorHandler

20        Set objMapDom = New MSXML2.DOMDocument40

          'Load up the map file into a dom.
30        LoadXmlFile objMapDom, strMapFile, PROC_NAME, "Map"

          'Get all Map elements in the map file.
40        Set objMapList = objMapDom.selectNodes("//Map")

          Dim strCodeAtt As String, strParent As String, strCode As String
          Dim strList As String, strMapAtt As String, strValue As String

          'Loop through map elements.
50        For Each objMapNode In objMapList

              'Get a list of Replace elements under the map element.
60            Set objReplaceList = objMapNode.selectNodes("Replace")

              'Extract map attributes.
70            strCodeAtt = GetChildNodeText(objMapNode, "@Att", True)
80            strParent = GetChildNodeText(objMapNode, "@Parent", False)
90            strList = GetChildNodeText(objMapNode, "@List", True)
100           strMapAtt = GetChildNodeText(objMapNode, "@MapAtt", True)

              'Enforce some kind of parential presence.
110           If strParent = "" Then
120               strParent = "Root"
130           End If

              'Get a list of attributes matching the code for this map element.
140           Set objAttParentList = objDom.selectNodes("//" & strParent & "//*[@" & strCodeAtt & "]")

              'Loop through attributes list.
150           For Each objAttParentNode In objAttParentList

                  'Get the code value from the code attribute.
160               strCode = GetChildNodeText(objAttParentNode, "@" & strCodeAtt, True)

                  'Convert codes to upper, to cover for Test Track 874.
                  'strCode = UCase(strCode)

                  'If strList is blank then just copy the value of Att.
170               If strList = "" Then
180                   strValue = strCode
                  'Otherwise go look up the reference value.
190               Else
                      'Get a reference value for this code.
200                   If Len(strCode) > 0 Then
210                       strValue = GetChildNodeText(objDom, "/Root/Reference" _
                              & "[@List='" & strList & "']" _
                              & "[@ReferenceID='" & strCode & "']" _
                              & "/@Name", False)
220                   Else
230                       strValue = "" 'Empty
240                   End If
250               End If

                  'Loop through the replace elements and do any search-and-replace.
260               For Each objReplaceNode In objReplaceList
270                   strValue = LTrim(RTrim(Replace(strValue, _
                          GetChildNodeText(objReplaceNode, "@Find", True), _
                          GetChildNodeText(objReplaceNode, "@Insert", True))))
280               Next

                  'Because different stored procedures return the reference data
                  'in different formats (i.e. List="Gender" vs. List="GenderCD")
                  'we have to allow for more than one way to convert a code.
                  'Grrrr... so only add an attribute if it doesn't already exist
                  'AND have a non-blank value.
290               If GetChildNodeText(objAttParentNode, "@" & strMapAtt, False) = "" Then

                      'Create a new attribute and add it to the parent element.
300                   AddAttribute objAttParentNode, strMapAtt, strValue

310               End If

320           Next

330       Next

ErrorHandler:

340       Set objMapDom = Nothing

350       Set objMapList = Nothing
360       Set objMapNode = Nothing
370       Set objAttParentList = Nothing
380       Set objAttParentNode = Nothing
390       Set objReplaceList = Nothing
400       Set objReplaceNode = Nothing

410       If Err.Number <> 0 Then
420           objEvents.Env "XML =" & objDom.xml
430           objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "Map file =" & strMapFile
440       End If

End Sub

'********************************************************************************
'* Formats a friendly error string from the passed dom parse error object.
'********************************************************************************
Public Function FormatDomParseError(ByRef objErr As IXMLDOMParseError) As String
10        FormatDomParseError = _
                "      Error code (" & objErr.errorCode & ") was raised during parsing." & vbCrLf _
              & "      File location: line = " & objErr.Line & " and column = " & objErr.linepos & "." & vbCrLf _
              & "      Bad XML: '" & objErr.srcText & "'" & vbCrLf _
              & "      Reason given: " & objErr.reason
End Function

'********************************************************************************
'* Transforms the passed XML DOM to an XML DOM via the passed XSL file.
'* Validates the original XML and XSL documents and raises intelligent errors.
'* Does not validate the returned DOM beyond that done by MSXML.
'********************************************************************************
Public Function TransformDomAsDom( _
    ByRef domXML As MSXML2.DOMDocument40, _
    ByVal strXslPath As String, _
    ByRef objEvents As Object) As MSXML2.DOMDocument40

          Const PROC_NAME As String = MODULE_NAME & "TransformXmlAsDom: "

          'Objects that need clean-up.
          Dim domXSL As MSXML2.DOMDocument40
          Dim domResult As MSXML2.DOMDocument40
          
          'Primitives that dont.
          Dim blnDebugMode As Boolean

10        On Error GoTo ErrorHandler

20        objEvents.Assert Not domXML Is Nothing, "Passed XML DOM was Nothing."
30        objEvents.Assert strXslPath <> "", "Passed XSL file path string was blank."
          
          'Get debug mode status.
40        blnDebugMode = objEvents.IsDebugMode

50        Set domXSL = New MSXML2.DOMDocument40
60        Set domResult = New MSXML2.DOMDocument40

70        If blnDebugMode Then objEvents.Trace "StyleSheet = " & strXslPath, PROC_NAME

          'Load and validate the style sheet.
80        LoadXmlFile domXSL, strXslPath, PROC_NAME, "XSL Style Sheet"

          'Transform the XML with the XSL style sheet.
90        domXML.transformNodeToObject domXSL, domResult
          
100       Set TransformDomAsDom = domResult
          
ErrorHandler:

          'Object clean-up
110       Set domXSL = Nothing
120       Set domResult = Nothing

130       If Err.Number <> 0 Then
140           objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "strXslPath=" & strXslPath
150       End If

End Function

'********************************************************************************
'* Transforms the passed XML string to an XML DOM via the passed XSL file.
'* Validates the original XML and XSL documents and raises intelligent errors.
'* Does not validate the returned DOM beyond that done by MSXML.
'********************************************************************************
Public Function TransformXmlAsDom( _
    ByRef strXml As String, _
    ByVal strXslPath As String, _
    ByRef objEvents As Object) As MSXML2.DOMDocument40

          Const PROC_NAME As String = MODULE_NAME & "TransformXmlAsDom: "

10        On Error GoTo ErrorHandler

          'Objects that need clean-up.
          Dim domXML As New MSXML2.DOMDocument40

20        objEvents.Assert strXml <> "", "Passed XML string was blank."
          
          'Load and validate the param XML
30        LoadXml domXML, strXml, PROC_NAME, "Source XML"

40        Set TransformXmlAsDom = TransformDomAsDom(domXML, strXslPath, objEvents)
          
ErrorHandler:

          'Object clean-up
50        Set domXML = Nothing

60        If Err.Number <> 0 Then
70            objEvents.Env "XML=" & strXml
80            objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "strXslPath=" & strXslPath
90        End If

End Function

'********************************************************************************
'* Transforms the passed XML string to an XML DOM via the passed XSL file.
'* Validates all three documents and raises intelligent errors.
'********************************************************************************
Public Function TransformDomAsXml( _
    ByRef domXML As MSXML2.DOMDocument40, _
    ByVal strXslPath As String, _
    ByRef objEvents As Object) As String

10        TransformDomAsXml = TransformDomAsDom(domXML, strXslPath, objEvents).xml

End Function

'********************************************************************************
'* Transforms the passed XML string to an XML DOM via the passed XSL file.
'* Validates all three documents and raises intelligent errors.
'********************************************************************************
Public Function TransformXmlAsXml( _
    ByRef strXml As String, _
    ByVal strXslPath As String, _
    ByRef objEvents As Object) As String

          Const PROC_NAME As String = MODULE_NAME & "TransformXmlAsDom: "

10        On Error GoTo ErrorHandler

          'Objects that need clean-up.
          Dim domXML As New MSXML2.DOMDocument40

20        objEvents.Assert strXml <> "", "Passed XML string was blank."
          
          'Load and validate the param XML
30        LoadXml domXML, strXml, PROC_NAME, "Source XML"

40        TransformXmlAsXml = TransformDomAsXml(domXML, strXslPath, objEvents)
          
ErrorHandler:

          'Object clean-up
50        Set domXML = Nothing

60        If Err.Number <> 0 Then
70            objEvents.Env "XML=" & strXml
80            objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "strXslPath=" & strXslPath
90        End If

End Function





