Attribute VB_Name = "MLAPDPartnerDataMgr"
'********************************************************************************
'* Component LAPDPartnerDataMgr : Module MLAPDPartnerDataMgr
'*
'* Global Consts and Utility Methods
'*
'********************************************************************************
Option Explicit

Public Const APP_NAME As String = "LAPDPartnerDataMgr."
Private Const MODULE_NAME As String = APP_NAME & "MLAPDPartnerDataMgr."

Public Const LAPDPartnerDataMgr_FirstError As Long = &H80067000

Public Enum MLAPDPartnerDataMgr_ErrorCodes

    'General error codes applicable to all modules of this component
    eInvalidTradingPartner = LAPDPartnerDataMgr_FirstError + &H80
    eCDataTagNotFound
    eLynxIdInvalid
    eBadTradingPartner
    eBadDocumentType
    eDuplicateDocument
    eNackReceived
    eNotImplemented
    eBadSequenceNumber
    eUnableToInitializeTrans
    eNoRecordsReturned
    eLynxIdUnknown

    'Module specific error ranges.
    MPartnerDataMgr_BAS_ErrorCodes = LAPDPartnerDataMgr_FirstError + &H100
    CPartnerDataXfer_CLS_ErrorCodes = LAPDPartnerDataMgr_FirstError + &H180
    CAPD_CLS_ErrorCodes = LAPDPartnerDataMgr_FirstError + &H200
    CCCC_CLS_ErrorCodes = LAPDPartnerDataMgr_FirstError + &H280
    CExecute_CLS_ErrorCodes = LAPDPartnerDataMgr_FirstError + &H300
    CSceneGenesis_CLS_ErrorCodes = LAPDPartnerDataMgr_FirstError + &H380
    CPartner_CLS_ErrorCodes = LAPDPartnerDataMgr_FirstError + &H400
    CAutoVerse_CLS_ErrorCodes = LAPDPartnerDataMgr_FirstError + &H480
    CGrange_CLS_ErrorCodes = LAPDPartnerDataMgr_FirstError + &H560

End Enum

Public g_strSupportDocPath As String     'Location of the LAPDPartnerDataMgr.xml

Public g_objEvents As SiteUtilities.CEvents
Public g_blnDebugMode As Boolean         'Are we in debug mode?? -- SiteUtilities
Public g_objDataAccessor As DataAccessor.CDataAccessor

Public g_strPartnerConnStringXml As String   'Current strConn to DataAccesor for PARTNER XML data
Public g_strPartnerConnStringStd As String   'Current strConn to DataAccesor for PARTNER recordset data
Public g_strLYNXConnStringXML As String      'Current strConn to DataAccessor for LYNX XML data
Public g_strLYNXConnStringStd As String      'Current strConn to DataAccessor for LYNX recordset data

Public g_strPartner As String           'Trading partner name
Public g_strPartnerSmallID As String    'Small version of partner name (i.e. "SG" instead of "SceneGenesis")

'Things we need to know about the user and trading partner
Public g_objPassedDocument As MSXML2.DOMDocument40     'Holds the document passed into this component
Public g_strPassedDocument As String

Public g_strLynxID As String        'LynxID associated with the inbound document as in 6008 only
Public g_strActualLynxID As String  'Concatenated version from the Xml as in : 6008-1
Public g_strVehicleNumber As String 'Vehicle Number
Public g_strAssignmentID As String  'Assignment ID
Public g_strTaskID As String
Public g_strActionCode As String    'Action Code: "N" for new assignment, "C" for cancellation.
Public g_strStatus As String


Public g_varDocumentReceivedDate As Variant 'The date/time that the document was received.
Public g_varTransactionDate As Variant      'The date/time that the document was sent.

Public g_blnUsePartnerTypesForDocTypes As Boolean

'This counter used to ensure the globals get intialized
'and desposed of only once.  This is required because various
'parts of the ECAD stuff have different entry points.
Private mintInitCount As Integer

'API declares
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

'********************************************************************
'* Util methods to prevent modification of
'* any error information stored in the Err object.
'********************************************************************
Type typError
    Number As Long
    Description As String
    Source As String
End Type

Public objError As typError

Public Sub PushErr()
10        objError.Number = Err.Number
20        objError.Description = Err.Description
30        objError.Source = Err.Source
40        Err.Clear
End Sub

Public Sub PopErr()
10        Err.Number = objError.Number
20        Err.Description = objError.Description
30        Err.Source = objError.Source
End Sub

'********************************************************************************
'* Returns true if the globals have been intialized.
'********************************************************************************
Public Property Get IsInitialized() As Boolean
10        IsInitialized = CBool(mintInitCount <> 0)
End Property

'********************************************************************************
'* Initialize Globals
'********************************************************************************
Public Sub InitializeGlobals()

10        If Not IsInitialized Then
          
              'Create private member DataAccessor.
20            Set g_objDataAccessor = New DataAccessor.CDataAccessor
          
              'Share DataAccessor's events object.
30            Set g_objEvents = g_objDataAccessor.mEvents
          
              'Get path to support document directory
40            g_strSupportDocPath = App.Path & "\" & Left(APP_NAME, Len(APP_NAME) - 1)
          
              'This will give partner data its own log file.
50            g_objEvents.ComponentInstance = "Partner Data"
          
              'Initialize our member components first, so we have logging set up.
60            g_objDataAccessor.InitEvents App.Path & "\..\config\config.xml"
          
              'Get debug mode status.
70            g_blnDebugMode = g_objEvents.IsDebugMode
              
              'g_objEvents.Assert g_strPassedDocument <> "", "Passed XML document string was blank."
80            If g_strPassedDocument <> "" Then
          
                  'Create a DOM document to hold the passed in document for reference
90                Set g_objPassedDocument = New MSXML2.DOMDocument40
100               LoadXml g_objPassedDocument, g_strPassedDocument, "InitializeGlobals()", "Passed"
                  
110           End If
          
              'This connection points the component at the appropriate partner dBase
120           g_strPartnerConnStringStd = GetConfig("PartnerSettings/ConnectionStringStd")
130           g_strPartnerConnStringXml = GetConfig("PartnerSettings/ConnectionStringXml")
          
              'Lookup connection strings for the LYNX APD database
140           g_strLYNXConnStringStd = GetConfig("ConnectionStringStd")
150           g_strLYNXConnStringXML = GetConfig("ConnectionStringXml")
          
160           If g_blnDebugMode Then g_objEvents.Trace g_strSupportDocPath, "DocPath"
              
170       End If

180       mintInitCount = mintInitCount + 1
          
End Sub

'********************************************************************************
'* Terminate Globals
'********************************************************************************
Public Sub TerminateGlobals()

10        If mintInitCount <= 1 Then
          
20            Set g_objEvents = Nothing
30            Set g_objDataAccessor = Nothing
40            Set g_objPassedDocument = Nothing
              
50            mintInitCount = 0
60        Else
70            mintInitCount = mintInitCount - 1
80        End If

End Sub

'********************************************************************************
'* Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
'* Parameters:  strObjectName = The object to call CreateObject for.
'* Purpose:     Wraps CreateObject() with better debug information.
'* Returns:     The Object created or Nothing.
'********************************************************************************
Public Function CreateObjectEx(strObjectName) As Object

10        On Error GoTo ErrorHandler

20        Set CreateObjectEx = Nothing

          Dim obj As Object
30        Set obj = CreateObject(strObjectName)

40        If obj Is Nothing Then
50            Err.Raise LAPDPartnerDataMgr_FirstError + 10, "", "CreateObject('" & strObjectName & "') returned Nothing."
60        End If

70        Set CreateObjectEx = obj
80        Exit Function

ErrorHandler:
90        On Error GoTo 0
100       Err.Raise Err.Number, "CreateObjectEx('" & strObjectName & "') " & Err.Source, Err.Description & " " & strObjectName

End Function

Public Function GetDefaultPartnerUserID(ByVal strLogonID As String, Optional ByVal strApplicationCD As String = "APD") As Long
  
        Dim objResults As New DOMDocument40
  On Error GoTo ErrorHandler:
10      g_objDataAccessor.SetConnectString g_strLYNXConnStringXML
  
20      LoadXml objResults, g_objDataAccessor.ExecuteSpNamedParamsXML(GetConfig("ClaimPoint/WebAssignment/GetUserDetailSP"), "@Login", strLogonID, "@ApplicationCD", strApplicationCD), "GetDefaultPartnerUserID", "Partner User Detail"

30      GetDefaultPartnerUserID = GetChildNodeText(objResults, "/Root/User/@UserID")
ErrorHandler:
40      Set objResults = Nothing
50      If Err.Number <> 0 Then
60        g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & Err.Source, Err.Description
70      End If
  
End Function



'********************************************************************************
'* Returns the requested configuration setting.
'********************************************************************************
Public Function GetConfig(ByVal strSetting As String)
10        GetConfig = g_objEvents.mSettings.GetParsedSetting(strSetting)
End Function

'********************************************************************************
'* Builds up a date string to be used in document file names.
'********************************************************************************
Public Function BuildFileNameDate() As String
10        BuildFileNameDate = ConstructXmlDate(Now) & ConstructXmlTime(Now)
End Function

'********************************************************************************
'* Syntax:      Set strTime = ConstructXmlTime(Now)
'* Parameters:  t - the time to construct from.
'* Purpose:     Builds a specifically formatted time string, according to our
'*              XML docs, as "mmddyyyy".
'* Returns:     The time string.
'********************************************************************************
Public Function ConstructXmlDate(d) As String
          Const PROC_NAME As String = MODULE_NAME & "ConstructXmlDate: "

10        On Error GoTo ErrorHandler

          'These are all used in constructing the file name for an image file storage name
          Dim strMonth As String, strDay As String, strYear As String
          Dim strCurrDate As String

          Dim strBaseDate As String
20        strBaseDate = d
30        strYear = DatePart("yyyy", strBaseDate)
40        strMonth = DatePart("m", strBaseDate)
50        strDay = DatePart("d", strBaseDate)

          ' Zero fill the month if less than 10
60        If CInt(strMonth) < 10 Then
70           strMonth = "0" & strMonth
80        End If

          'Zero fill the day of the month if less than 10
90        If CInt(strDay) < 10 Then
100           strDay = "0" & strDay
110       End If

120       strCurrDate = strMonth & strDay & strYear
          ConstructXmlDate = strCurrDate

ErrorHandler:

130       If Err.Number <> 0 Then
140           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
150       End If

End Function

'********************************************************************************
'* Syntax:      Set strTime = ConstructXmlTime(Now)
'* Parameters:  t - the time to construct from.
'* Purpose:     Builds a specifically formatted time string, according to our
'*              XML docs, as "hhmmss"
'* Returns:     The time string.
'********************************************************************************
Public Function ConstructXmlTime(t) As String
          Const PROC_NAME As String = MODULE_NAME & "ConstructXmlTime: "

10        On Error GoTo ErrorHandler

          Dim strHours As String, strMinutes As String, strSeconds As String
          Dim strBaseDate As String, strCurrTime As String

20        strBaseDate = t
30        strHours = DatePart("h", strBaseDate)
40        strMinutes = DatePart("n", strBaseDate)
50        strSeconds = DatePart("s", strBaseDate)

          'Zero fill the hour if less than 10
60        If CInt(strHours) < 10 Then
70            strHours = "0" & strHours
80        End If

          'Zero fill the minutes if less than 10
90        If CInt(strMinutes) < 10 Then
100           strMinutes = "0" & strMinutes
110       End If

          'Zero fill the seconds if less than 10
120       If CInt(strSeconds) < 10 Then
130           strSeconds = "0" & strSeconds
140       End If

150       strCurrTime = strHours & strMinutes & strSeconds

          ConstructXmlTime = strCurrTime

ErrorHandler:

160       If Err.Number <> 0 Then
170           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
180       End If

End Function

'********************************************************************************
'* Syntax:      strSqlDateTime = ConstructSqlDateTime("12032003","013425")
'* Parameters:  strXmlDate = Date in format "mmddyyyy"
'*              strXmlTime = Time in format "hhmmss"
'*              blnNowAsDefault = If true, will use Now if no values passed.
'* Purpose:     Builds a specifically formatted time string, according to our
'*              SQL specs, as "yyyy-mm-ddThh:mm:ss"
'* Returns:     The date-time string.
'********************************************************************************
Public Function ConstructSqlDateTime( _
    ByVal strXmlDate As String, _
    ByVal strXmlTime As String, _
    Optional ByVal blnNowAsDefault As Boolean = True) As String
              
          Dim strDateTime As String
          Dim strTemp As String
          
          'Should be in format 'mmddyyyy' as per spec.
10        If Len(strXmlDate) = 8 Then
20            strDateTime = Right(strXmlDate, 4) & "-" _
                  & Left(strXmlDate, 2) & "-" _
                  & Mid(strXmlDate, 3, 2)
30        ElseIf blnNowAsDefault Then
40            strDateTime = Format(Now, "yyyy-mm-dd")
50        End If
              
          'Dont bother with time if we dont have date.
60        If Len(strDateTime) > 0 Then
          
              'Should be in format 'hhmmss' as per spec.
70            If Len(strXmlTime) = 6 Then
80                strDateTime = strDateTime & "T" _
                      & Left(strXmlTime, 2) & ":" _
                      & Mid(strXmlTime, 3, 2) & ":" _
                      & Right(strXmlTime, 2)
              'If date was passed but no time then use default of 0 time.
90            ElseIf Len(strXmlDate) > 0 Then
100               strDateTime = strDateTime & "T00:00:00"
              'If no date or time were passed then use now if allowed.
110           ElseIf blnNowAsDefault Then
120               strDateTime = strDateTime & Format(Now, "Thh:mm:ss")
130           End If
              
140       End If
              
          ConstructSqlDateTime = strDateTime

End Function

'********************************************************************************
'* Spits recordset contents out to the debug log.
'********************************************************************************
Public Sub DebugRS(ByRef RS As ADODB.Recordset)
          Dim str As String
          Dim strValue As String
          Dim fld As ADODB.Field
          
10        RS.MoveFirst
          
20        str = "Record Count = " & RS.RecordCount & vbCrLf
30        str = str & "  Fields = "
          
40        For Each fld In RS.fields
50            str = str & fld.Name & ", "
60        Next
          
70        str = str & vbCrLf
          
80        While Not RS.EOF
90            str = str & "  Data: "
100           For Each fld In RS.fields
110               If Not IsNull(fld.Value) Then
120                   strValue = LTrim(RTrim(Replace(fld.Value, vbCrLf, "")))
130                   If Len(strValue) > 0 Then
140                       If Left(strValue, 1) = "<" And Right(strValue, 1) = ">" Then
150                           str = str & "(XML NOT LOGGED), "
160                       Else
170                           str = str & "'" & fld.Value & "', "
180                       End If
190                   Else
200                       str = str & "'' , "
210                   End If
220               Else
230                   str = str & "NULL, "
240               End If
250           Next
260           str = str & vbCrLf
              
270           RS.MoveNext
280       Wend
          
290       g_objEvents.Trace str, MODULE_NAME & "Recordset Contents"

300       RS.MoveFirst
          
End Sub

'********************************************************************************
'* Pads a number out with zeros to make the requested number of digits.
'********************************************************************************
Public Function ZeroPad(ByVal lngValue As Long, ByVal intDigits As Integer) As String
          Dim strValue As String
          
10        strValue = CStr(lngValue)
20        While Len(strValue) < intDigits
30            strValue = "0" & strValue
40        Wend

50        ZeroPad = strValue
End Function

'********************************************************************************
'* AsNumber - returns a Long based upon the first numeric portion of str.
'********************************************************************************
Public Function AsNumber(ByVal str As String) As Long
          Dim i As Integer
10        AsNumber = 0
20        For i = 1 To Len(str)
30            If IsNumeric(Left(str, i)) Then
40                AsNumber = CLng(Left(str, i))
50            Else
60                Exit For
70            End If
80        Next
End Function

'********************************************************************************
'* Syntax:      DoSleep( 500 )
'* Parameters:  lngMS - milliseconds to sleep.
'* Purpose:     Wraps Sleep() with calls to VB DoEvents().
'* Returns:     Nothing.
'********************************************************************************
Public Sub DoSleep(ByVal lngMS As Long)
          Dim lCount As Long
10        For lCount = 1 To lngMS \ 10
20            Sleep 10
30            DoEvents
40        Next
End Sub

