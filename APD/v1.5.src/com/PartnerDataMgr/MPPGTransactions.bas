Attribute VB_Name = "MPPGTransactions"
'********************************************************************************
'* Module MPPGTransactions
'*
'* Code common to PPG transactions.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "MPPGTransactions."

'Internal error codes for this module.
Private Enum EventCodes
    eElectronicPostBadStatus = &H80065000 + &H300
End Enum

'********************************************************************************
'* Syntax:      BuildTransactionForPPG
'*
'* Parameters:  strTransactionType - A token identifying the transaction type.
'*              strMessageBody - The XML to put within the wrapper.
'*              strDestination - A token indentfying the message destination.
'*              strReferenceId - An internal identifyer for the message.
'*
'* Purpose:     Wraps the passed XML with a PPG SOAP style wrapper.
'*
'* Returns:     The completed XML.
'*
'* Requires:    Settings in the config.xml file.
'********************************************************************************
Public Function BuildTransactionForPPG( _
    ByVal strTransactionType As String, _
    ByVal strMessageBody As String, _
    ByVal strDestination As String, _
    ByVal strReferenceId As String) As String

          Const PROC_NAME As String = MODULE_NAME & "BuildTransactionForPPG: "

          Dim strTrans As String
          Dim strEnvelopeId As String
          Dim strOriginationId As String
          Dim strDestinationId As String

          Dim blnUseSOAP As Boolean
          Dim blnUseCDATA As Boolean

10        On Error GoTo ErrorHandler

          'Add parameters to trace data.
20        If g_blnDebugMode Then g_objEvents.Trace _
              "TransactionType = '" & strTransactionType & "'  Dest = '" & strDestination _
              & "'  RefId = '" & strReferenceId & "'", PROC_NAME & "Started"

          'Get SOAP and CDATA settings from configuration.
30        blnUseSOAP = CBool(GetConfig("PPG_SOAP_Header/UseSOAP") = "True")
40        blnUseCDATA = CBool(GetConfig("PPG_SOAP_Header/UseCDATA") = "True")

50        If g_blnDebugMode Then g_objEvents.Trace "SOAP = '" & blnUseSOAP _
              & "'  CDATA = '" & blnUseCDATA, PROC_NAME & "Config"

          'This configuration setting allows us to disable the SOAP envelope if need be.
60        If Not blnUseSOAP Then
70            BuildTransactionForPPG = strMessageBody
80        Else

              'Get source and dest PIDs from the config file.
90            strOriginationId = GetConfig("PPG_SOAP_Header/OriginationPID")
100           strDestinationId = GetConfig("PPG_SOAP_Header/DestinationPID[@name='" & strDestination & "']")

              'Generate the envelope id.
110           strEnvelopeId = strOriginationId & "#" & strDestinationId & "#" & strReferenceId & "#" & Now

              'Start building the transaction XML.
120           strTrans = "<StdEnvelope EnvelopeID='" & strEnvelopeId & "' Version='1.0'>"

              'Add in the environment code.
130           strTrans = strTrans & "<Header><Delivery><Environment>" _
                  & GetConfig("@EnvironmentCode") _
                  & "</Environment>"

              'Add in the origination company.
140           strTrans = strTrans & "<OriginationCompany>" _
                  & GetConfig("PPG_SOAP_Header/OriginationCompany") _
                  & "</OriginationCompany>"

              'Add in the origination ID.
150           strTrans = strTrans & "<OriginationPID>" & strOriginationId & "</OriginationPID>"

              'Add in the destination company and Id.
160           strTrans = strTrans & "<DestinationCompany>" & strDestination & "</DestinationCompany>" _
                  & "<DestinationPID>" & strDestinationId & "</DestinationPID>"

              'Add in the transaction type.
170           strTrans = strTrans & "<TransactionType>" & strTransactionType & "</TransactionType>"

              'Add in the sender's reference Id.
180           strTrans = strTrans & "<SendersReferenceID>" & strReferenceId & "</SendersReferenceID>"

              'Add in the current time and date.
190           strTrans = strTrans & "<TransactionDateTime>" & Now & "</TransactionDateTime>"

              'Close out the header, begin the message body.
200           strTrans = strTrans & "</Delivery></Header><MessageBody>"

              'Add in CDATA tag if specified in configuration.
210           If blnUseCDATA Then
220               strTrans = strTrans & "<![CDATA["
230           End If

              'Add in the message body.
240           strTrans = strTrans & strMessageBody

              'Close CDATA tag if specified in configuration.
250           If blnUseCDATA Then
260               strTrans = strTrans & "]]>"
270           End If

              'Close the message and envelope tags - we are done!
280           strTrans = strTrans & "</MessageBody></StdEnvelope>"

290           If g_blnDebugMode Then g_objEvents.Trace "XML = '" & strTrans, PROC_NAME & "Envelope"

300           BuildTransactionForPPG = strTrans

310       End If

ErrorHandler:

320       If Err.Number <> 0 Then
330           g_objEvents.Env "Message = " & strMessageBody
340           g_objEvents.Env "Result = " & strTrans
350           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
360       End If

End Function

'********************************************************************************
'* Syntax:      XmlHttpPost
'* Parameters:  strUrl - The URL to post to.
'*              strXml - The XML data to POST.
'*              g_objEvents - An instance of SiteUtilities::CEvents to use.
'* Purpose:     Posts the passed data to a url.
'* Returns:     The status.
'********************************************************************************
Public Function XmlHttpPost( _
    ByVal strUrl As String, _
    ByVal strXML As String) As Long

          Const PROC_NAME As String = MODULE_NAME & "XmlHttpPost: "

          Dim objXmlHttp As XMLHTTP
          
          'Failover vars and constants
          Dim intFailOverCount As Integer
          Const MaxFailoverAttempts = 10
          Const FailoverSleepPeriod = 1000

10        On Error GoTo ErrorHandler

20        If g_blnDebugMode Then g_objEvents.Trace "URL = '" & strUrl & "'", PROC_NAME

30        Set objXmlHttp = New XMLHTTP

FailOverLoop:

40        On Error GoTo FailOverCheck

50        objXmlHttp.Open "POST", strUrl, False

60        objXmlHttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
70        objXmlHttp.send strXML

80        If objXmlHttp.Status <> 200 Then
90            Err.Raise eElectronicPostBadStatus, PROC_NAME & "HTTP Send", _
                  "Bad status returned from post to " & strUrl
100       End If
          
FailOverCheck:

110       If Err.Number <> 0 Then
          
              'Retry loop count below max so far?
120           If intFailOverCount < MaxFailoverAttempts Then
              
130               If intFailOverCount = 0 Then
                  
                      'Per TestTrack 1782 - Disable all error and warning emails that no longer hold relevance.
                      'Thus Reduced the following message from an event to a trace message.

140                   If g_blnDebugMode Then g_objEvents.Trace _
                          "A CRITICAL HTTP POSTING ERROR HAS OCCURRED" & vbCrLf & _
                          "    ENTERING FAILOVER RETRY LOOP", PROC_NAME & Err.Source

                      'g_objEvents.HandleEvent Err.Number, PROC_NAME & Err.Source, Err.Description, _
                      '    "XmlHttpPost: A CRITICAL ERROR HAS OCCURRED HTTP POSTING - " & _
                      '    "ENTERING FAILOVER RETRY LOOP", "", False
                          
150                   intFailOverCount = 1
                      
160               Else
170                   intFailOverCount = intFailOverCount + 1
180               End If
          
                  'Protect against errors in this code
190               On Error Resume Next

                  'Clear the error object.
200               Err.Clear

                  'Pause to give time for cluster failover to complete.
210               DoSleep (FailoverSleepPeriod)

220               Resume FailOverLoop
                  
230           Else    ' Give up!
              
                  'Add note to log that the retry count has been exceeded.
240               g_objEvents.Env "XmlHttpPost: MAXIMUM RETRY COUNT OF " & MaxFailoverAttempts & " HAS BEEN EXCEEDED!"
              
250               GoTo ErrorHandler
                  
260           End If
              
270       End If

280       If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "HTTP Submit Successful!"

ErrorHandler:

290       Set objXmlHttp = Nothing

300       If Err.Number <> 0 Then
310           g_objEvents.Env "URL = " & strUrl
320           g_objEvents.Env "XML = " & strXML
330           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, _
                  "Error posting to " & strUrl & " : " & Err.Description
340       End If

End Function
