<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output method="text" encoding="UTF-8"/>
  <xsl:template match="//EMS">
     <!-- Validating the INSP_CODE for containing DRP or I -->
     <xsl:if test="((//INSP_CODE != 'DRP') and (//INSP_CODE!='I'))">
        <xsl:text>Inspection Location Code '</xsl:text>
        <xsl:value-of select="//INSP_CODE"/>
        <xsl:text>' is invalid for LYNX Services.</xsl:text>
     </xsl:if>
     <xsl:if test="((VEH/V_MODEL_YR='') and (VEH/V_MAKECODE='') and (VEH/V_MODEL=''))"><xsl:text>Vehicle Not Defined in Assignment</xsl:text></xsl:if>     
  </xsl:template>
</xsl:stylesheet>