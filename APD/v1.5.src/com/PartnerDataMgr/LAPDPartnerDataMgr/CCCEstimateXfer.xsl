<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"   xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
								xmlns:cccxsl="http://www.lynxservices.com/ccc"
                                xmlns:cu="urn:the-xml-files:xslt">

<xsl:import href="EstXferUtils.xsl"/>

<xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes"/>
<xsl:decimal-format NaN="0"/>

<!--- This styesheet transforms incoming CCC Data V1.1.0 to APD's Common Estimate Format -->


<!--- Scripting necessary for the mathematical calculations in the summary -->
<msxsl:script language="JScript" implements-prefix="cccxsl">

<![CDATA[
	var BodyMaterialAmount, BodyMaterialHours, BodyMaterialRate;
	var DiscountTaxAmount, DiscountNonTaxAmount, TotalDiscountAmount, DiscountBase, DiscountPct;
	var MarkupTaxAmount, MarkupNonTaxAmount, TotalMarkupAmount, MarkupBase, MarkupPct;
	var PaintMaterialAmount, PaintMaterialHours, PaintMaterialRate;
	var TotalParts, TotalTaxParts, TotalNonTaxParts, TotalOtherTaxParts, TotalOtherNonTaxParts;
	var TotalMaterials;
	var TotalPartsMaterials;

	var BodyLaborAmount, BodyLaborHours, BodyLaborRate;
	var FrameLaborAmount, FrameLaborHours, FrameLaborRate;
	var MechLaborAmount, MechLaborHours, MechLaborRate;
	var PaintLaborAmount, PaintLaborHours, PaintLaborRate;
	var TotalOtherLabor;
	var TotalLaborAmount;

	var StorageAmount;
	var SubletPASAmount, SubletOTSLAmount, SubletAmount;
	var TowingAmount;
	var OtherAddlChargesAmount;
	var TotalAddlCharges;

	var AppearanceAllowanceAmount;
	var BettermentAmount;
	var DeductibleAmount;
	var TotalAdjustmentAmount;

	var CountyTaxAmount, CountyTaxBase, CountyTaxPct;
	var MunicipalTaxAmount, MunicipalTaxBase, MunicipalTaxPct;
	var OtherTaxAmount;
	var SalesTaxAmount, SalesTaxBase, SalesTaxPct;
	var TotalTaxAmount;

	var NetTotalAmount;
	var RepairTotalAmount;
	var SubtotalAmount;
	var ZZVarianceAmount;

	var DetailPartTaxTotal, DetailPartNonTaxTotal, DetailPartOtherTaxTotal, DetailPartOtherNonTaxTotal;
	var DetailBodyLaborHours, DetailFrameLaborHours, DetailMechLaborHours, DetailPaintLaborHours;
	var DetailSubletTotal, DetailBettermentTotal;
	var DetailUnknownTotal;
	var DetailOEMParts, DetailLKQParts, DetailAFMKParts, DetailRemanParts;
	var Detail2WAlign, Detail4WAlign;

	var RepairAmountTotal, ReplaceAmountTotal;
	
	var CurrentLineNumber;
	var TaxRateWork;
	
	function setvar(varname, value) {
		eval(varname + ' = ' + value);
		return '';
	}

	function getvar(varname) {
		return eval(eval(varname));
	}
		
	function cccreplace(instring) {
   		var rexp = /#38;/g;					//Create regular expression pattern.
   		return(instring.replace(rexp, ""));	//Do the Replace and return
	}
  
    function getSecretData(strSecretField, iIndex){
        var aFields = null;
        var sData = "";
        if (strSecretField != ""){
            aFields = strSecretField.split("-");
            if (aFields.length > iIndex){
                sData = aFields[iIndex];
            }
        }
      return sData;
    }
    
]]>
</msxsl:script>

<xsl:variable name="source" select="'CCC'"/>
<xsl:variable name="estimatepackage" select="'Pathways'"/>
<xsl:variable name="version" select="'1.1.3'"/>
<xsl:variable name="versiondate" select="'03/06/2006'"/>

<xsl:template match="/EstimateTransaction0110">
	<xsl:call-template name="EstimateData"/>
</xsl:template>

<xsl:template name="EstimateData">
	<APDEstimate>
		<xsl:attribute name="Source"><xsl:value-of select="$source"/></xsl:attribute>
		<xsl:attribute name="EstimatePackage"><xsl:value-of select="$estimatepackage"/></xsl:attribute>
		<xsl:attribute name="SoftwareVersion"><xsl:value-of select="Estimate0110/SoftwareVersion"/></xsl:attribute>
		<xsl:attribute name="StylesheetVersion"><xsl:value-of select="$version"/></xsl:attribute>
		<xsl:attribute name="StylesheetDate"><xsl:value-of select="$versiondate"/></xsl:attribute>

		<xsl:apply-templates select="Estimate0110" mode="Header"/>
		<xsl:apply-templates select="Estimate0110" mode="Detail"/>
 		<xsl:apply-templates select="Estimate0110" mode="Profile"/>
		<xsl:apply-templates select="Estimate0110" mode="Summary"/>
		<xsl:apply-templates select="Estimate0110" mode="Audit"/> 
	</APDEstimate>	
</xsl:template>


<!--- Transform Header Information -->
<xsl:template match="Estimate0110" mode="Header">
	<Header>
		<xsl:attribute name="EstimateType">
			<xsl:choose>
				<xsl:when test="TransactionType='E'">Estimate</xsl:when>
				<xsl:when test="TransactionType='S'">Supplement</xsl:when>
				<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
			</xsl:choose> 
		</xsl:attribute>
		<xsl:attribute name="SupplementNumber">
			<xsl:choose>
				<xsl:when test="TransactionType='E'">0</xsl:when>
				<xsl:otherwise><xsl:value-of select="EstimateSupplementNmbr"/></xsl:otherwise>
			</xsl:choose> 
		</xsl:attribute>
		<xsl:attribute name="WrittenDate"><xsl:value-of select="substring(TransactionCreateDateTime/DlDate/DateTime/Date, 1, 2)"/>/<xsl:value-of select="substring(TransactionCreateDateTime/DlDate/DateTime/Date, 3, 2)"/>/<xsl:value-of select="substring(TransactionCreateDateTime/DlDate/DateTime/Date, 5, 4)"/><xsl:text> </xsl:text><xsl:value-of select="substring(TransactionCreateDateTime/DlDate/DateTime/Time, 1, 2)"/>:<xsl:value-of select="substring(TransactionCreateDateTime/DlDate/DateTime/Time, 3, 2)"/>:<xsl:value-of select="substring(TransactionCreateDateTime/DlDate/DateTime/Time, 5, 2)"/></xsl:attribute>
		<xsl:attribute name="AssignmentDate">
			<xsl:if test="Administrative/ClaimInformation/AssignmentDate/DlDate/Date">
				<xsl:value-of select="substring(Administrative/ClaimInformation/AssignmentDate/DlDate/Date, 1, 2)"/>/<xsl:value-of select="substring(Administrative/ClaimInformation/AssignmentDate/DlDate/Date, 3, 2)"/>/<xsl:value-of select="substring(Administrative/ClaimInformation/AssignmentDate/DlDate/Date, 5, 4)"/>
			</xsl:if>
		</xsl:attribute>
		<xsl:attribute name="ClaimantName"><xsl:value-of select="cccxsl:cccreplace(string(Administrative/Party[PartyRoleType/@code='CM']//Customer/CustomerName))"/></xsl:attribute>
		<xsl:attribute name="ClaimNumber"><xsl:value-of select="cccxsl:cccreplace(string(Administrative/ClaimInformation/ClaimNumber))"/></xsl:attribute>
		<xsl:attribute name="InsuranceCompanyName"><xsl:value-of select="cccxsl:cccreplace(string(Administrative/InsuranceCompany/InsuranceCompanyDetails/Customer/CustomerName))"/></xsl:attribute>
		<xsl:attribute name="InsuredName"><xsl:value-of select="cccxsl:cccreplace(string(Administrative/Party[PartyRoleType/@code='IN' or PartyRoleType/@code='VI']//Customer/CustomerName))"/></xsl:attribute>
		<xsl:attribute name="LossDate">
			<xsl:if test="Administrative/Loss/LossDateTime/DlDate/Date">
				<xsl:value-of select="substring(Administrative/Loss/LossDateTime/DlDate/Date, 1, 2)"/>/<xsl:value-of select="substring(Administrative/Loss/LossDateTime/DlDate/Date, 3, 2)"/>/<xsl:value-of select="substring(Administrative/Loss/LossDateTime/DlDate/Date, 5, 4)"/>
				<xsl:if test="Administrative/Loss/LossDateTime/DlDate/Time">
					<xsl:text>  </xsl:text><xsl:value-of select="substring(Administrative/Loss/LossDateTime/DlDate/Time, 1, 2)"/>:<xsl:value-of select="substring(Administrative/Loss/LossDateTime/DlDate/Time, 3, 2)"/>:<xsl:value-of select="substring(Administrative/Loss/LossDateTime/DlDate/Time, 5, 2)"/>
				</xsl:if>
			</xsl:if>
		</xsl:attribute>
		<xsl:attribute name="LossType">
			<xsl:choose>
				<xsl:when test="Administrative/ClaimInformation/LossCategoryCode/@code='C'">Collision</xsl:when>
				<xsl:when test="Administrative/ClaimInformation/LossCategoryCode/@code='L'">Liability</xsl:when>
				<xsl:when test="Administrative/ClaimInformation/LossCategoryCode/@code='M'">Comprehensive</xsl:when>
				<xsl:when test="Administrative/ClaimInformation/LossCategoryCode/@code='O'">Other</xsl:when>
				<xsl:when test="Administrative/ClaimInformation/LossCategoryCode/@code='P'">Property</xsl:when>
				<xsl:when test="Administrative/ClaimInformation/LossCategoryCode/@code='U'">Unknown</xsl:when>
				<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
			</xsl:choose> 
		</xsl:attribute>
		<xsl:attribute name="PolicyNumber"><xsl:value-of select="cccxsl:cccreplace(string(Administrative/Policy/PolicyNumber))"/></xsl:attribute>
		<xsl:attribute name="Remarks"><xsl:value-of select="cccxsl:cccreplace(string(Administrative/ClaimInformation/ClaimNotes))"/></xsl:attribute>
		<xsl:attribute name="RepairDays"><xsl:value-of select="Administrative/RepairOrder/DaysToRepair"/></xsl:attribute>
		<xsl:attribute name="SettlementType"/>
    <xsl:attribute name="LynxID"><xsl:value-of select="cccxsl:getSecretData(string(SecretField), 0)"/></xsl:attribute>
    <xsl:attribute name="VehicleNumber"><xsl:value-of select="cccxsl:getSecretData(string(SecretField), 1)"/></xsl:attribute>
    <xsl:attribute name="AssignmentID"><xsl:value-of select="cccxsl:getSecretData(string(SecretField), 2)"/></xsl:attribute>

		<Appraiser>
			<xsl:attribute name="AppraiserName"><xsl:value-of select="cccxsl:cccreplace(string(Administrative/Appraiser/AppraiserName/Person/Customer/CustomerName))"/></xsl:attribute>
			<xsl:attribute name="AppraiserLicenseNumber"><xsl:value-of select="cccxsl:cccreplace(string(Administrative/Appraiser/AppraiserLicenseNumber))"/></xsl:attribute>
		</Appraiser>	

		<Estimator>
			<xsl:choose>
				<xsl:when test="Administrative/Appraiser/AppraiserCompanyName/Customer/TypeCode='IAC'">
					<xsl:apply-templates mode="Estimator" select="Administrative/Appraiser/AppraiserCompanyName/Customer"/>
				</xsl:when>
				<xsl:when test="Administrative/Appraiser/AppraiserCompanyName/Customer/TypeCode='RF'">
					<xsl:apply-templates mode="Estimator" select="Administrative/RepairFacility/RepairFacilityInformation/Customer"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="CompanyAddress"/>
					<xsl:attribute name="CompanyAreaCode"/>
					<xsl:attribute name="CompanyCity"/>
					<xsl:attribute name="CompanyName">! Not Found In Estimate Data !</xsl:attribute>
					<xsl:attribute name="CompanyExchangeNumber"/>
					<xsl:attribute name="CompanyState"/>
					<xsl:attribute name="CompanyUnitNumber"/>
					<xsl:attribute name="CompanyZip"/>
				</xsl:otherwise>
			</xsl:choose>
		</Estimator>

		<Inspection>
			<xsl:attribute name="InspectionAddress"><xsl:value-of select="cccxsl:cccreplace(string(concat(Administrative/InspectionLocation/InspectionLocationAddress/Address/Addr1, Administrative/InspectionLocation/InspectionLocationAddress/Address/Addr2)))"/></xsl:attribute>
			<xsl:attribute name="InspectionAreaCode"><xsl:value-of select="Administrative/InspectionLocation/Person/Customer/CustomerContact[RoleType/@code ='B01' or RoleType/@code='B02']/Contact[ContactType/@code='T']/Telecom/AreaCode"/></xsl:attribute>			
			<xsl:attribute name="InspectionCity"><xsl:value-of select="cccxsl:cccreplace(string(Administrative/InspectionLocation/InspectionLocationAddress/Address/City))"/></xsl:attribute>
			<xsl:attribute name="InspectionExchangeNumber"><xsl:value-of select="substring(Administrative/InspectionLocation/Person/Customer/CustomerContact[RoleType/ @code ='B01' or RoleType/@code='B02']/Contact[ContactType='T']/Telecom/TelecomNum, 1, 3)"/></xsl:attribute>			
			<xsl:attribute name="InspectionLocation">
				<xsl:if test="Administrative/Appraiser/AppraiserCompanyName/Customer/TypeCode='RF'">
					<xsl:value-of select="cccxsl:cccreplace(string(Administrative/RepairFacility/RepairFacilityInformation/Customer/CustomerName))"/>
				</xsl:if>
			</xsl:attribute>
			<xsl:attribute name="InspectionState"><xsl:value-of select="Administrative/InspectionLocation/InspectionLocationAddress/Address/State/Code"/></xsl:attribute>
			<xsl:attribute name="InspectionType"><xsl:value-of select="cccxsl:cccreplace(string(Administrative/InspectionLocation/InspectionCodeDescription))"/></xsl:attribute>
			<xsl:attribute name="InspectionUnitNumber"><xsl:value-of select="substring(Administrative/InspectionLocation/Person/Customer/CustomerContact[RoleType/@code='B01' or RoleType/@code='B02']/Contact[ContactType/@code='T']/Telecom/TelecomNum, 4, 4)"/></xsl:attribute>			
			<xsl:attribute name="InspectionZip"><xsl:value-of select="Administrative/InspectionLocation/InspectionLocationAddress/Address/Zip/ZipCode"/></xsl:attribute>
		</Inspection>

		<Owner>
			<xsl:attribute name="OwnerAddress"><xsl:value-of select="cccxsl:cccreplace(string(concat(Administrative/Party[PartyRoleType/@code='VI' or PartyRoleType/@code='VO']//Customer/CustomerAddress[1]/Address/Addr1, Administrative/Party[PartyRoleType/@code='VI']//Customer/CustomerAddress[1] /Address/Addr2)))"/></xsl:attribute>
			<xsl:attribute name="OwnerCity"><xsl:value-of select="cccxsl:cccreplace(string(Administrative/Party[PartyRoleType/@code='VI' or PartyRoleType/@code='VO']//Customer/CustomerAddress[1]/Address/City))"/></xsl:attribute>
			<xsl:attribute name="OwnerDayAreaCode"><xsl:value-of select="Administrative/Party[PartyRoleType/@code='VI' or PartyRoleType/@code='VO']//Customer/CustomerContact[RoleType/@code ='B01' or RoleType/@code='B02' or RoleType/@code='D01']/Contact[ContactType/@code='T']/Telecom/AreaCode"/></xsl:attribute>
			<xsl:attribute name="OwnerDayExchangeNumber"><xsl:value-of select="substring(Administrative/Party[PartyRoleType/@code='VI' or PartyRoleType/@code='VO']//Customer/CustomerContact[RoleType/@code ='B01' or RoleType/@code='B02' or RoleType/@code='D01']/Contact[ContactType/@code='T']/Telecom/TelecomNum, 1, 3)"/></xsl:attribute>
			<xsl:attribute name="OwnerDayExtensionNumber"><xsl:value-of select="Administrative/Party[PartyRoleType/@code='VI' or PartyRoleType/@code='VO']//Customer/CustomerContact[RoleType/@code='B01' or RoleType/@code='B02' or RoleType/@code='D01']/Contact[ContactType/@code='T']/Telecom/Extn"/></xsl:attribute>
			<xsl:attribute name="OwnerDayUnitNumber"><xsl:value-of select="substring(Administrative/Party[PartyRoleType/@code='VI' or PartyRoleType/@code='VO']//Customer/CustomerContact[RoleType/@code ='B01' or RoleType/@code='B02' or RoleType/@code='D01']/Contact[ContactType/@code='T']/Telecom/TelecomNum, 4, 4)"/></xsl:attribute>
			<xsl:attribute name="OwnerEveningAreaCode"><xsl:value-of select="Administrative/Party[PartyRoleType/@code='VI' or PartyRoleType/@code='VO']//Customer/CustomerContact[RoleType/@code='H01' or RoleType/@code='H02' or RoleType/@code='E01']/Contact[ContactType/@code='T']/Telecom/AreaCode"/></xsl:attribute>
			<xsl:attribute name="OwnerEveningExchangeNumber"><xsl:value-of select="substring(Administrative/Party[PartyRoleType/@code='VI' or PartyRoleType/@code='VO']//Customer/CustomerContact[RoleType/@code ='H01' or RoleType/@code='H02' or RoleType/@code='E01']/Contact[ContactType/@code='T']/Telecom/TelecomNum, 1, 3)"/></xsl:attribute>
			<xsl:attribute name="OwnerEveningExtensionNumber"><xsl:value-of select="Administrative/Party[PartyRoleType/@code='VI' or PartyRoleType/@code='VO']//Customer/CustomerContact[RoleType/@code='H01' or RoleType/@code='H02' or RoleType/@code='E01']/Contact[ContactType/@code='T']/Telecom/Extn"/></xsl:attribute>
			<xsl:attribute name="OwnerEveningUnitNumber"><xsl:value-of select="substring(Administrative/Party[PartyRoleType/@code='VI' or PartyRoleType/@code='VO']//Customer/CustomerContact[RoleType/@code='H01' or RoleType/@code='H02' or RoleType/@code='E01']/Contact[ContactType/@code='T']/Telecom/TelecomNum, 4, 4)"/></xsl:attribute>
			<xsl:attribute name="OwnerName"><xsl:value-of select="cccxsl:cccreplace(string(Administrative/Party[PartyRoleType/@code='VI' or PartyRoleType/@code='VO']//Customer/CustomerName))"/></xsl:attribute>
			<xsl:attribute name="OwnerState"><xsl:value-of select="Administrative/Party[PartyRoleType/@code='VI' or PartyRoleType/@code='VO']//Customer/CustomerAddress[1]/Address/State/Code"/></xsl:attribute>
			<xsl:attribute name="OwnerZip"><xsl:value-of select="Administrative/Party[PartyRoleType/@code='VI' or PartyRoleType/@code='VO']//Customer/CustomerAddress[1]/Address/Zip/ZipCode"/></xsl:attribute>
		</Owner>

		<Shop>
			<xsl:attribute name="ShopAddress"><xsl:value-of select="cccxsl:cccreplace(string(concat(Administrative/RepairFacility/RepairFacilityInformation/Customer/CustomerAddress[AddressRoleType/@code='OT']/Address/Addr1,  Administrative/RepairFacility/RepairFacilityInformation/Customer/CustomerAddress[AddressRoleType/@code='OT']/Address/Addr2)))"/></xsl:attribute>
			<xsl:attribute name="ShopAreaCode"><xsl:value-of select="Administrative/RepairFacility/RepairFacilityInformation/Customer/CustomerContact[RoleType/@code='B01' or RoleType/@code='B02' or RoleType/@code='UN']/Contact[ContactType/@code='T']/Telecom/AreaCode"/></xsl:attribute>
			<xsl:attribute name="ShopCity"><xsl:value-of select="cccxsl:cccreplace(string(Administrative/RepairFacility/RepairFacilityInformation/Customer/CustomerAddress[AddressRoleType/@code='OT']/Address/City))"/></xsl:attribute>
			<xsl:attribute name="ShopExchangeNumber"><xsl:value-of select="substring(Administrative/RepairFacility/RepairFacilityInformation/Customer/CustomerContact[RoleType/@code='B01' or RoleType/@code='B02' or RoleType/@code='UN']/Contact[ContactType/@code='T']/Telecom/TelecomNum, 1, 3)"/></xsl:attribute>
			<xsl:attribute name="ShopName"><xsl:value-of select="cccxsl:cccreplace(string(Administrative/RepairFacility/RepairFacilityInformation/Customer/CustomerName))"/></xsl:attribute>
			<xsl:attribute name="ShopRegistrationNumber"><xsl:value-of select="Administrative/RepairFacility/CompanyLicenseNumber"/></xsl:attribute>
			<xsl:attribute name="ShopState"><xsl:value-of select="Administrative/RepairFacility/RepairFacilityInformation/Customer/CustomerAddress[AddressRoleType/@code='OT']/Address/State/Code"/></xsl:attribute>
			<xsl:attribute name="ShopUnitNumber"><xsl:value-of select="substring(Administrative/RepairFacility/RepairFacilityInformation/Customer/CustomerContact[RoleType/@code='B01' or RoleType/@code='B02' or RoleType/@code='UN']/Contact[ContactType/@code='T']/Telecom/TelecomNum, 4, 4)"/></xsl:attribute>
			<xsl:attribute name="ShopZip"><xsl:value-of select="Administrative/RepairFacility/RepairFacilityInformation/Customer/CustomerAddress[AddressRoleType/@code='OT']/Address/Zip/ZipCode"/></xsl:attribute>
		</Shop>

		<Vehicle>
			<xsl:attribute name="VehicleBodyStyle"><xsl:value-of select="cccxsl:cccreplace(string(Vehicle/BodyStyle))"/></xsl:attribute>
			<xsl:attribute name="VehicleColor"><xsl:value-of select="cccxsl:cccreplace(string(Vehicle/ExternalColor))"/></xsl:attribute>
			<xsl:attribute name="VehicleEngineType"><xsl:value-of select="cccxsl:cccreplace(string(Vehicle/EngineSize))"/></xsl:attribute>
			<xsl:attribute name="VehicleMake"><xsl:value-of select="cccxsl:cccreplace(string(Vehicle/MakeCode))"/></xsl:attribute>
			<xsl:attribute name="VehicleMileage">
				<xsl:if test="number(translate(Vehicle/Mileage, ',', '')) &gt; 0"><xsl:value-of select="translate(Vehicle/Mileage, ',', '')"/></xsl:if>
			</xsl:attribute>
			<xsl:attribute name="VehicleModel"><xsl:value-of select="cccxsl:cccreplace(string(Vehicle/Model))"/></xsl:attribute>
			<xsl:attribute name="VehicleYear"><xsl:value-of select="Vehicle/ModelYear"/></xsl:attribute>
			<xsl:attribute name="Vin"><xsl:value-of select="Vehicle/Vin"/></xsl:attribute>

			<xsl:for-each select="Vehicle/ImpactPoint">
				<ImpactPoint>
					<xsl:attribute name="ImpactArea">
						<xsl:choose>
							<xsl:when test="ImpactAreaCode/@code='1'"><xsl:value-of select="'Right Front'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='2'"><xsl:value-of select="'Right Front Pillar'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='3'"><xsl:value-of select="'Right T-Bone'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='4'"><xsl:value-of select="'Right Quarter Post'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='5'"><xsl:value-of select="'Right Rear'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='6'"><xsl:value-of select="'Rear'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='7'"><xsl:value-of select="'Left Rear'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='8'"><xsl:value-of select="'Left Quarter Post'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='9'"><xsl:value-of select="'Left T-Bone'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='10'"><xsl:value-of select="'Left Front Pillar'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='11'"><xsl:value-of select="'Left Front'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='12'"><xsl:value-of select="'Front'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='13'"><xsl:value-of select="'Rollover'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='14'"><xsl:value-of select="'Unknown'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='15'"><xsl:value-of select="'Total Loss'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='16'"><xsl:value-of select="'Non-Collision'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='25'"><xsl:value-of select="'Hood'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='26'"><xsl:value-of select="'Deck Lid'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='27'"><xsl:value-of select="'Roof'"/></xsl:when>
							<xsl:when test="ImpactAreaCode/@code='28'"><xsl:value-of select="'Undercarriage'"/></xsl:when>
							<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="PrimaryImpactFlag">
						<xsl:choose>
							<xsl:when test="ImpactPointNumber='1'">Yes</xsl:when>
							<xsl:otherwise>No</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
				</ImpactPoint>
			</xsl:for-each>

			<xsl:for-each select="Vehicle/Options//VehicleOption">
				<VehicleOption>
					<xsl:attribute name="Option"><xsl:value-of select="cccxsl:cccreplace(string(OptionDescription))"/></xsl:attribute>
				</VehicleOption>
			</xsl:for-each>			
		</Vehicle>
	</Header>	
</xsl:template>

<xsl:template match="Customer" mode="Estimator">
	<xsl:attribute name="CompanyAddress"><xsl:value-of select="cccxsl:cccreplace(string(concat(CustomerAddress[AddressRoleType/@code='MA']/Address/Addr1, CustomerAddress[AddressRoleType/@code='MA']/Address/Addr2)))"/></xsl:attribute>
	<xsl:attribute name="CompanyAreaCode"><xsl:value-of select="CustomerContact[RoleType/@code ='B01' or RoleType/@code='B02' or RoleType/@code='UN']/Contact[ContactType/@code='T']/Telecom/AreaCode"/></xsl:attribute>
	<xsl:attribute name="CompanyCity"><xsl:value-of select="cccxsl:cccreplace(string(CustomerAddress[AddressRoleType/@code='MA']/Address/City))"/></xsl:attribute>
	<xsl:attribute name="CompanyName"><xsl:value-of select="cccxsl:cccreplace(string(CustomerName))"/></xsl:attribute>
	<xsl:attribute name="CompanyExchangeNumber"><xsl:value-of select="substring(CustomerContact[RoleType/@code ='B01' or RoleType/@code='B02' or RoleType/@code='UN']/Contact[ContactType/@code='T']/Telecom/TelecomNum, 1, 3)"/></xsl:attribute>
	<xsl:attribute name="CompanyState"><xsl:value-of select="CustomerAddress[AddressRoleType/@code='MA']/Address/State/Code"/></xsl:attribute>
	<xsl:attribute name="CompanyUnitNumber"><xsl:value-of select="substring(CustomerContact[RoleType/@code ='B01' or RoleType/@code='B02' or RoleType/@code='UN']/Contact[ContactType/@code='T']/Telecom/TelecomNum, 4, 4)"/></xsl:attribute>
	<xsl:attribute name="CompanyZip"><xsl:value-of select="CustomerAddress[AddressRoleType/@code='MA']/Address/Zip/ZipCode"/></xsl:attribute>
</xsl:template>


<!--- Transform Detail Line Information -->
<xsl:template match="Estimate0110" mode="Detail">
	<!--- First set up running totals for various detail components.  These will be used for the audit later -->

	<xsl:value-of select="cccxsl:setvar('DetailPartTaxTotal', 0)"/>
	<xsl:value-of select="cccxsl:setvar('DetailPartNonTaxTotal', 0)"/>
	<xsl:value-of select="cccxsl:setvar('DetailPartOtherTaxTotal', 0)"/>
	<xsl:value-of select="cccxsl:setvar('DetailPartOtherNonTaxTotal', 0)"/>
	<xsl:value-of select="cccxsl:setvar('DetailBodyLaborHours', 0)"/>
	<xsl:value-of select="cccxsl:setvar('DetailFrameLaborHours', 0)"/>
	<xsl:value-of select="cccxsl:setvar('DetailMechLaborHours', 0)"/>
	<xsl:value-of select="cccxsl:setvar('DetailPaintLaborHours', 0)"/>
	<xsl:value-of select="cccxsl:setvar('DetailSubletTotal', 0)"/>
	<xsl:value-of select="cccxsl:setvar('DetailBettermentTotal', 0)"/>
	<xsl:value-of select="cccxsl:setvar('DetailUnknownTotal', 0)"/>
	<xsl:value-of select="cccxsl:setvar('DetailOEMParts', 0)"/>
	<xsl:value-of select="cccxsl:setvar('DetailLKQParts', 0)"/>
	<xsl:value-of select="cccxsl:setvar('DetailAFMKParts', 0)"/>
	<xsl:value-of select="cccxsl:setvar('DetailRemanParts', 0)"/>
	<xsl:value-of select="cccxsl:setvar('Detail2WAlign', 0)"/>
	<xsl:value-of select="cccxsl:setvar('Detail4WAlign', 0)"/>

	<Detail>
		<xsl:for-each select="DetailLine">
			<DetailLine>
				<xsl:attribute name="DetailNumber"><xsl:value-of select="DisplayLineNumber"/></xsl:attribute>
				<xsl:attribute name="Comment"><xsl:value-of select="cccxsl:cccreplace(string(EstimateLineNote))"/></xsl:attribute>
				<xsl:attribute name="Description"><xsl:value-of select="cccxsl:cccreplace(string(LineDescription))"/></xsl:attribute>
				<xsl:attribute name="OperationCode">
					<xsl:choose>
						<xsl:when test=".//Sublet/SubletAmtIndicator='Y'">Sublet</xsl:when>
						<xsl:when test="not(Part/ActualPartPrice) and not(Labor) and not(Sublet/SubletAmtIndicator='Y')">Comment</xsl:when>
						<xsl:when test="not(.//OperationCode)"/>								
						<xsl:when test=".//OperationCode/@code='OP0'"/>
						<xsl:when test=".//OperationCode/@code='OP2'">Remove/Install</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP4'">Alignment</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP6'">Refinish</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP9'">Repair</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP10'">Repair</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP11'">Replace</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP12'">Replace</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP13'">Other</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP14'">Other</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP15'">Blend</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP16'">Sublet</xsl:when>
						<xsl:when test=".//OperationCode/@code='OP26'">Refinish</xsl:when>
						<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>				
				<xsl:attribute name="LineAdded">
					<xsl:if test="LineAddedIndicator!='E01'">
						<xsl:value-of select="LineAddedIndicator"/>
					</xsl:if>
				</xsl:attribute>
				<xsl:attribute name="ManualLineFlag">
					<xsl:choose>
						<xsl:when test="LineSource='Manual'">Yes</xsl:when>
						<xsl:otherwise>No</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:if test="BettermentCalcType">
					<xsl:attribute name="BettermentType"><xsl:value-of select="BettermentCalcType/@code"/></xsl:attribute>
					<xsl:attribute name="BettermentPercent"><xsl:value-of select="number(BettermentDepreciationPercent) * 100"/></xsl:attribute>
					<xsl:attribute name="BettermentAmount"><xsl:value-of select="BettermentAmt"/></xsl:attribute>
					<xsl:attribute name="BettermentTaxable">
						<xsl:choose>
							<xsl:when test="BettermentTaxedIndicator='Y'">Yes</xsl:when>
							<xsl:otherwise>No</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>			
				</xsl:if>

				<!--- Part Information -->
				<xsl:if test="Part/ActualPartPrice">
					<Part>
						<xsl:attribute name="APDPartCategory">
							<xsl:choose>
								<xsl:when test="(Part/PartTypeCode/@code='PAS' or Sublet/SubletAmt) and not(Part/PriceColumn='Standard')">Sublet</xsl:when>
								<xsl:when test="(Part/PartTypeCode/@code='PAO' or not(Part/PartTypeCode) or (Part/PartTypeCode/@code='PAS' and Part/PriceColumn='Standard')) and Part/PartTaxedIndicator='Y'">OtherTaxableParts</xsl:when>
								<xsl:when test="(Part/PartTypeCode/@code='PAO' or not(Part/PartTypeCode) or (Part/PartTypeCode/@code='PAS' and Part/PriceColumn='Standard')) and Part/PartTaxedIndicator='N'">OtherNonTaxableParts</xsl:when>
								<xsl:when test="Part/PartTaxedIndicator='Y'">TaxableParts</xsl:when>
								<xsl:when test="Part/PartTaxedIndicator='N'">NonTaxableParts</xsl:when>
								<xsl:otherwise/>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="DBPartPrice">
							<xsl:choose>
								<xsl:when test="Part/UserChangedPartPriceIndicator='Y' and Part/DBPartPrice"><xsl:value-of select="number(Part/DBPartPrice) * number(Part/PartQuantity)"/></xsl:when>
								<xsl:otherwise/>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="PartIncludedFlag">
							<xsl:choose>
								<xsl:when test="Part/PartInclIndicator='Y'">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="PartPrice"><xsl:value-of select="number(Part/ActualPartPrice) * number(Part/PartQuantity)"/></xsl:attribute>
						<xsl:attribute name="PartTaxedFlag">
							<xsl:choose>
								<xsl:when test="Sublet/SubletAmtTaxedIndicator='Y'">Yes</xsl:when>
								<xsl:when test="Part/PartTaxedIndicator='Y'">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="PartType">
							<xsl:choose>
								<xsl:when test="Part/PartTypeCode/@code='PAN'">New</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAP'">New</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAL'">LKQ</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAA'">Aftermarket</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAM'">Reconditioned</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAR'">Reconditioned</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAC'">Reconditioned</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAG'">Glass</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAO' or ((Part/PartTypeCode/@code='PAS' or not(Part/PartTypeCode)) and Part/PriceColumn='Standard')">Other</xsl:when>
								<xsl:when test="Part/PartTypeCode/@code='PAS'">Sublet</xsl:when>
								<xsl:when test="not(Part/PartTypeCode)">Unspecified</xsl:when>	
								<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="PriceChangedFlag">
							<xsl:choose>
								<xsl:when test="Part/UserChangedPartPriceIndicator='Y' and Part/DBPartPrice">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="Quantity"><xsl:value-of select="Part/PartQuantity"/></xsl:attribute>
					</Part>
					
					<!--- Update running totals with any part price.  Make sure we put it in the right buckets -->
					<xsl:if test="Part/PartInclIndicator='N'">
						<xsl:choose>
							<xsl:when test="(Part/PartTypeCode/@code='PAS' or Sublet/SubletAmt) and not(Part/PriceColumn='Standard')"><xsl:value-of select="cccxsl:setvar('DetailSubletTotal', round((cccxsl:getvar('DetailSubletTotal') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:when>
							<xsl:when test="(Part/PartTypeCode/@code='PAO' or not(Part/PartTypeCode) or (Part/PartTypeCode/@code='PAS' and Part/PriceColumn='Standard')) and Part/PartTaxedIndicator='Y'"><xsl:value-of select="cccxsl:setvar('DetailPartOtherTaxTotal', round((cccxsl:getvar('DetailPartOtherTaxTotal') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:when>
							<xsl:when test="(Part/PartTypeCode/@code='PAO' or not(Part/PartTypeCode) or (Part/PartTypeCode/@code='PAS' and Part/PriceColumn='Standard')) and Part/PartTaxedIndicator='N'"><xsl:value-of select="cccxsl:setvar('DetailPartOtherNonTaxTotal', round((cccxsl:getvar('DetailPartOtherNonTaxTotal') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:when>
							<xsl:when test="Part/PartTaxedIndicator='Y'"><xsl:value-of select="cccxsl:setvar('DetailPartTaxTotal', round((cccxsl:getvar('DetailPartTaxTotal') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:when>
							<xsl:when test="Part/PartTaxedIndicator='N'"><xsl:value-of select="cccxsl:setvar('DetailPartNonTaxTotal', round((cccxsl:getvar('DetailPartNonTaxTotal') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="cccxsl:setvar('DetailUnknownTotal', round((cccxsl:getvar('DetailUnknownTotal') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					
					<xsl:if test="Part/PartInclIndicator='N'">
						<xsl:choose>
							<xsl:when test="Part/PartTypeCode/@code='PAN'"><xsl:value-of select="cccxsl:setvar('DetailOEMParts', round((cccxsl:getvar('DetailOEMParts') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:when>
							<xsl:when test="Part/PartTypeCode/@code='PAL'"><xsl:value-of select="cccxsl:setvar('DetailLKQParts', round((cccxsl:getvar('DetailLKQParts') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:when>
							<xsl:when test="Part/PartTypeCode/@code='PAA'"><xsl:value-of select="cccxsl:setvar('DetailAFMKParts', round((cccxsl:getvar('DetailAFMKParts') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:when>
							<xsl:when test="Part/PartTypeCode/@code='PAM'"><xsl:value-of select="cccxsl:setvar('DetailRemanParts', round((cccxsl:getvar('DetailRemanParts') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:when>
						</xsl:choose>
					</xsl:if>

					<xsl:if test="Part/PartInclIndicator='N' and BettermentCalcType">
						<xsl:value-of select="cccxsl:setvar('DetailBettermentTotal', round((cccxsl:getvar('DetailBettermentTotal') + number(BettermentAmt)) * 100) div 100)"/>
					</xsl:if>

					<xsl:variable name="LineDescription" select="translate(cccxsl:cccreplace(string(LineDescription)), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
					
					<xsl:if test="$LineDescription = 'two wheel align' or $LineDescription = 'two wheel alignment' or $LineDescription = 'front wheel alignment' or $LineDescription = '2 wheel align' or $LineDescription = '2 wheel alignment' or $LineDescription = '2-wheel align' or $LineDescription = '2-wheel alignment'"><xsl:value-of select="cccxsl:setvar('Detail2WAlign', round((cccxsl:getvar('Detail2WAlign') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:if>

					<xsl:if test="$LineDescription = 'four wheel align' or $LineDescription = 'four wheel alignment' or $LineDescription = '4 wheel align' or $LineDescription = '4 wheel alignment' or $LineDescription = '4-wheel align' or $LineDescription = '4-wheel alignment'"><xsl:value-of select="cccxsl:setvar('Detail4WAlign', round((cccxsl:getvar('Detail4WAlign') + number(Part/ActualPartPrice) * number(Part/PartQuantity)) * 100) div 100)"/></xsl:if>
				</xsl:if>

				<!--- Labor Information -->
				<xsl:for-each select="Labor">
					<Labor>
						<xsl:attribute name="DBLaborHours">
							<xsl:choose>
								<xsl:when test="UserChangedLaborHoursIndicator='Y'"><xsl:value-of select="DBLaborHours"/></xsl:when>
								<xsl:otherwise/>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborAmount"><xsl:value-of select="LaborAmt"/></xsl:attribute>
						<xsl:attribute name="LaborHoursChangedFlag">
							<xsl:choose>
								<xsl:when test="UserChangedLaborHoursIndicator='Y'">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborHours"><xsl:value-of select="ActualLaborHours"/></xsl:attribute>
						<xsl:attribute name="LaborIncludedFlag">
							<xsl:choose>
								<xsl:when test="LaborInclIndicator='Y'">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborTaxedFlag">
							<xsl:choose>
								<xsl:when test="LaborTaxedIndicator='Y'">Yes</xsl:when>
								<xsl:otherwise>No</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="LaborType">
							<xsl:choose>
								<xsl:when test="LaborType/@code='LAB'">Body</xsl:when>
								<xsl:when test="LaborType/@code='LAD'">Mechanical</xsl:when>
								<xsl:when test="LaborType/@code='LAE'">Mechanical</xsl:when>
								<xsl:when test="LaborType/@code='LAR'">Refinish</xsl:when>
								<xsl:when test="LaborType/@code='LAS'">Frame</xsl:when>
								<xsl:when test="LaborType/@code='LAF'">Frame</xsl:when>
								<xsl:when test="LaborType/@code='LAM'">Mechanical</xsl:when>
								<xsl:when test="LaborType/@code='LAG'">Body</xsl:when>
								<xsl:when test="LaborType/@code='LAU'">Paintless Dent Repair</xsl:when>
								<xsl:when test="LaborType/@code='LA1'">User Defined</xsl:when>
								<xsl:when test="LaborType/@code='LA2'">User Defined</xsl:when>
								<xsl:when test="LaborType/@code='LA3'">User Defined</xsl:when>
								<xsl:when test="LaborType/@code='LA4'">User Defined</xsl:when>
								<xsl:otherwise>! NOT DEFINED !</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</Labor>
					
					<!--- Update running totals with any part price.  Make sure we put it in the right bucket -->
					<xsl:if test="LaborInclIndicator='N'">
						<xsl:choose>
							<xsl:when test="LaborType/@code='LAB'"><xsl:value-of select="cccxsl:setvar('DetailBodyLaborHours', round((cccxsl:getvar('DetailBodyLaborHours') + number(ActualLaborHours)) * 10) div 10)"/></xsl:when>
							<xsl:when test="LaborType/@code='LAF' or LaborType/@code='LAS'"><xsl:value-of select="cccxsl:setvar('DetailFrameLaborHours', round((cccxsl:getvar('DetailFrameLaborHours') + number(ActualLaborHours)) * 10) div 10)"/></xsl:when>
							<xsl:when test="LaborType/@code='LAD' or LaborType/@code='LAE' or LaborType/@code='LAM'"><xsl:value-of select="cccxsl:setvar('DetailMechLaborHours', round((cccxsl:getvar('DetailMechLaborHours') + number(ActualLaborHours)) * 10) div 10)"/></xsl:when>
							<xsl:when test="LaborType/@code='LAR'"><xsl:value-of select="cccxsl:setvar('DetailPaintLaborHours', round((cccxsl:getvar('DetailPaintLaborHours') + number(ActualLaborHours)) * 10) div 10)"/></xsl:when>
						</xsl:choose>
					</xsl:if>					
				</xsl:for-each>
			</DetailLine>
		</xsl:for-each>
		
		<!--- Check to see if there are any additional charges in the Subtotal Information and generate a line item for each of them.  CCC does not generate a line item in the estimate data for this, -->
		<!--- but they do appear in the printed image.  In order to as closely match these two, this is what we must do. -->
		<xsl:if test="Subtotals[TotalType/@code='OTAC']">
			<xsl:value-of select="cccxsl:setvar('CurrentLineNumber', number(format-number(DetailLine[last()]/DisplayLineNumber, '#####0')) + 1)"/>			
			<DetailLine>
				<xsl:attribute name="DetailNumber"><xsl:value-of select="cccxsl:getvar('CurrentLineNumber')"/></xsl:attribute>
				<xsl:attribute name="Comment"/>
				<xsl:attribute name="Description">Other Charges</xsl:attribute>
				<xsl:attribute name="OperationCode">Comment</xsl:attribute>				
				<xsl:attribute name="LineAdded">	</xsl:attribute>
			</DetailLine>
			<xsl:for-each select="Subtotals[TotalType/@code='OTAC' and TotalTypeCode]">
				<xsl:value-of select="cccxsl:setvar('CurrentLineNumber', cccxsl:getvar('CurrentLineNumber') + 1)"/>			
				<DetailLine>
					<xsl:attribute name="DetailNumber"><xsl:value-of select="cccxsl:getvar('CurrentLineNumber')"/></xsl:attribute>
					<xsl:attribute name="Comment"/>
					<xsl:attribute name="Description">
						<xsl:choose>
                            		<xsl:when test="TotalTypeCode/@code='EPC'">E.P.C.</xsl:when>
                            		<xsl:when test="TotalTypeCode/@code='OTST'">Storage</xsl:when>
                            		<xsl:when test="TotalTypeCode/@code='OTTW'">Towing</xsl:when>
							<xsl:otherwise>Unidentified Charge</xsl:otherwise>
                        		</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="OperationCode"/>				
					<xsl:attribute name="LineAdded">	</xsl:attribute>

					<!--- Part Information -->
					<Part>
						<xsl:attribute name="APDPartCategory">
							<xsl:choose>
	                            		<xsl:when test="TotalTypeCode/@code='OTST'">Storage</xsl:when>
	                            		<xsl:when test="TotalTypeCode/@code='OTTW'">Towing</xsl:when>
								<xsl:otherwise>Other</xsl:otherwise>
	                        		</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="DBPartPrice"/>
						<xsl:attribute name="PartIncludedFlag">No</xsl:attribute>
						<xsl:attribute name="PartPrice"><xsl:value-of select="TotalTypeAmount"/></xsl:attribute>
						<xsl:attribute name="PartTaxedFlag">No	</xsl:attribute>
						<xsl:attribute name="PartType"/>
						<xsl:attribute name="PriceChangedFlag">No</xsl:attribute>
						<xsl:attribute name="Quantity">1</xsl:attribute>
					</Part>		
				</DetailLine>
			</xsl:for-each>
		</xsl:if>
	</Detail>
</xsl:template>


<!--- Transform Profile Information -->
<xsl:template match="Estimate0110" mode="Profile">

	<!--- Obtain tax rates to be used later -->
	<xsl:variable name="TaxSales" select="number(ProfileRates/Profile[1]/ProfileTax[TaxItem='LS']/TaxRate)"/>
	<xsl:variable name="TaxCounty" select="number(ProfileRates/Profile[1]/ProfileTax[TaxItem='CT']/TaxRate)"/>
	<xsl:variable name="TaxMunicipal" select="number(ProfileRates/Profile[1]/ProfileTax[TaxItem='MP']/TaxRate)"/>
	
	<Profiles>
		<ProfileItem Category="Parts" Type="OEM">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType='PAN']/PartTax/Tax/TaxIndicator='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="ProfileRates/PartProfile[PartType='PAN']/PartTax/Tax/TaxIndicator='Y'">
				<xsl:attribute name="TaxPercent">
					<xsl:value-of select="cccxsl:setvar('TaxRateWork', 0)"/>
					
					<xsl:for-each select="ProfileRates/PartProfile[PartType='PAN']/PartTax/Tax">
						<xsl:if test="TaxIndicator='Y'">
							<xsl:choose>
                                		<xsl:when test="TaxType/@code='LS'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxSales)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='CT'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxCounty)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='MP'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxMunicipal)"/></xsl:when>
	                            </xsl:choose>							
						</xsl:if>
					</xsl:for-each>
					
					<xsl:value-of select="cccxsl:getvar('TaxRateWork')"/>
				</xsl:attribute>			
			</xsl:if>
			<xsl:attribute name="MarkupPercent">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAN']/MarkupAmount &gt; 0)"><xsl:value-of select="round((Subtotals[TotalTypeCode/@code='PAN']/MarkupAmount div Subtotals[TotalTypeCode/@code='PAN']/Amount * 100) * 1000) div 1000"/></xsl:when>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAN']/NonTaxableMarkupAmount &gt; 0)"><xsl:value-of select="round((Subtotals[TotalTypeCode/@code='PAN']/NonTaxableMarkupAmount div Subtotals[TotalTypeCode/@code='PAN']/NonTaxableAmount * 100) * 1000) div 1000"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>			
			</xsl:attribute>
			<xsl:attribute name="DiscountPercent">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAN']/DiscountAmount &gt; 0)"><xsl:value-of select="round((-1 * Subtotals[TotalTypeCode/@code='PAN']/DiscountAmount div Subtotals[TotalTypeCode/@code='PAN']/Amount * 100) * 1000) div 1000"/></xsl:when>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAN']/NonTaxableDiscountAmount &gt; 0)"><xsl:value-of select="round((-1 * Subtotals[TotalTypeCode/@code='PAN']/NonTaxableDiscountAmount div Subtotals[TotalTypeCode/@code='PAN']/NonTaxableAmount * 100) * 1000) div 1000"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>			
			</xsl:attribute>			
		</ProfileItem>
		<ProfileItem Category="Parts" Type="LKQ">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType='PAL']/PartTax/Tax/TaxIndicator='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="ProfileRates/PartProfile[PartType='PAL']/PartTax/Tax/TaxIndicator='Y'">
				<xsl:attribute name="TaxPercent">
					<xsl:value-of select="cccxsl:setvar('TaxRateWork', 0)"/>
					
					<xsl:for-each select="ProfileRates/PartProfile[PartType='PAL']/PartTax/Tax">
						<xsl:if test="TaxIndicator='Y'">
							<xsl:choose>
                                		<xsl:when test="TaxType/@code='LS'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxSales)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='CT'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxCounty)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='MP'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxMunicipal)"/></xsl:when>
	                            </xsl:choose>							
						</xsl:if>
					</xsl:for-each>
					
					<xsl:value-of select="cccxsl:getvar('TaxRateWork')"/>
				</xsl:attribute>			
			</xsl:if>
			<xsl:attribute name="MarkupPercent">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAL']/MarkupAmount &gt; 0)"><xsl:value-of select="round((Subtotals[TotalTypeCode/@code='PAL']/MarkupAmount div Subtotals[TotalTypeCode/@code='PAL']/Amount * 100) * 1000) div 1000"/></xsl:when>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAL']/NonTaxableMarkupAmount &gt; 0)"><xsl:value-of select="round((Subtotals[TotalTypeCode/@code='PAL']/NonTaxableMarkupAmount div Subtotals[TotalTypeCode/@code='PAL']/NonTaxableAmount * 100) * 1000) div 1000"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>			
			</xsl:attribute>
			<xsl:attribute name="DiscountPercent">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAL']/DiscountAmount &gt; 0)"><xsl:value-of select="round((-1 * Subtotals[TotalTypeCode/@code='PAL']/DiscountAmount div Subtotals[TotalTypeCode/@code='PAL']/Amount * 100) * 1000) div 1000"/></xsl:when>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAL']/NonTaxableDiscountAmount &gt; 0)"><xsl:value-of select="round((-1 * Subtotals[TotalTypeCode/@code='PAL']/NonTaxableDiscountAmount div Subtotals[TotalTypeCode/@code='PAL']/NonTaxableAmount * 100) * 1000) div 1000"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>			
			</xsl:attribute>			
		</ProfileItem>
		<ProfileItem Category="Parts" Type="Aftermarket">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType='PAA']/PartTax/Tax/TaxIndicator='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="ProfileRates/PartProfile[PartType='PAA']/PartTax/Tax/TaxIndicator='Y'">
				<xsl:attribute name="TaxPercent">
					<xsl:value-of select="cccxsl:setvar('TaxRateWork', 0)"/>
					
					<xsl:for-each select="ProfileRates/PartProfile[PartType='PAA']/PartTax/Tax">
						<xsl:if test="TaxIndicator='Y'">
							<xsl:choose>
                                		<xsl:when test="TaxType/@code='LS'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxSales)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='CT'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxCounty)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='MP'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxMunicipal)"/></xsl:when>
	                            </xsl:choose>							
						</xsl:if>
					</xsl:for-each>
					
					<xsl:value-of select="cccxsl:getvar('TaxRateWork')"/>
				</xsl:attribute>			
			</xsl:if>
			<xsl:attribute name="MarkupPercent">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAA']/MarkupAmount &gt; 0)"><xsl:value-of select="round((Subtotals[TotalTypeCode/@code='PAA']/MarkupAmount div Subtotals[TotalTypeCode/@code='PAA']/Amount * 100) * 1000) div 1000"/></xsl:when>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAA']/NonTaxableMarkupAmount &gt; 0)"><xsl:value-of select="round((Subtotals[TotalTypeCode/@code='PAA']/NonTaxableMarkupAmount div Subtotals[TotalTypeCode/@code='PAA']/NonTaxableAmount * 100) * 1000) div 1000"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>			
			</xsl:attribute>
			<xsl:attribute name="DiscountPercent">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAA']/DiscountAmount &gt; 0)"><xsl:value-of select="round((-1 * Subtotals[TotalTypeCode/@code='PAA']/DiscountAmount div Subtotals[TotalTypeCode/@code='PAA']/Amount * 100) * 1000) div 1000"/></xsl:when>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAA']/NonTaxableDiscountAmount &gt; 0)"><xsl:value-of select="round((-1 * Subtotals[TotalTypeCode/@code='PAA']/NonTaxableDiscountAmount div Subtotals[TotalTypeCode/@code='PAA']/NonTaxableAmount * 100) * 1000) div 1000"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>			
			</xsl:attribute>			
		</ProfileItem>
		<ProfileItem Category="Parts" Type="Recycled">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType='PAM']/PartTax/Tax/TaxIndicator='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="ProfileRates/PartProfile[PartType='PAM']/PartTax/Tax/TaxIndicator='Y'">
				<xsl:attribute name="TaxPercent">
					<xsl:value-of select="cccxsl:setvar('TaxRateWork', 0)"/>
					
					<xsl:for-each select="ProfileRates/PartProfile[PartType='PAM']/PartTax/Tax">
						<xsl:if test="TaxIndicator='Y'">
							<xsl:choose>
                                		<xsl:when test="TaxType/@code='LS'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxSales)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='CT'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxCounty)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='MP'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxMunicipal)"/></xsl:when>
	                            </xsl:choose>							
						</xsl:if>
					</xsl:for-each>
					
					<xsl:value-of select="cccxsl:getvar('TaxRateWork')"/>
				</xsl:attribute>			
			</xsl:if>
			<xsl:attribute name="MarkupPercent">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAM']/MarkupAmount &gt; 0)"><xsl:value-of select="round((Subtotals[TotalTypeCode/@code='PAM']/MarkupAmount div Subtotals[TotalTypeCode/@code='PAM']/Amount * 100) * 1000) div 1000"/></xsl:when>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAM']/NonTaxableMarkupAmount &gt; 0)"><xsl:value-of select="round((Subtotals[TotalTypeCode/@code='PAM']/NonTaxableMarkupAmount div Subtotals[TotalTypeCode/@code='PAM']/NonTaxableAmount * 100) * 1000) div 1000"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>			
			</xsl:attribute>
			<xsl:attribute name="DiscountPercent">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAM']/DiscountAmount &gt; 0)"><xsl:value-of select="round((-1 * Subtotals[TotalTypeCode/@code='PAM']/DiscountAmount div Subtotals[TotalTypeCode/@code='PAM']/Amount * 100) * 1000) div 1000"/></xsl:when>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='PAM']/NonTaxableDiscountAmount &gt; 0)"><xsl:value-of select="round((-1 * Subtotals[TotalTypeCode/@code='PAM']/NonTaxableDiscountAmount div Subtotals[TotalTypeCode/@code='PAM']/NonTaxableAmount * 100) * 1000) div 1000"/></xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>			
			</xsl:attribute>			
		</ProfileItem>
		<ProfileItem Category="Parts" Type="Front Glass">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType='PAN']/PartTax/Tax/TaxIndicator='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="ProfileRates/PartProfile[PartType='PAN']/PartTax/Tax/TaxIndicator='Y'">
				<xsl:attribute name="TaxPercent">
					<xsl:value-of select="cccxsl:setvar('TaxRateWork', 0)"/>
					
					<xsl:for-each select="ProfileRates/PartProfile[PartType='PAN']/PartTax/Tax">
						<xsl:if test="TaxIndicator='Y'">
							<xsl:choose>
                                		<xsl:when test="TaxType/@code='LS'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxSales)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='CT'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxCounty)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='MP'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxMunicipal)"/></xsl:when>
	                            </xsl:choose>							
						</xsl:if>
					</xsl:for-each>
					
					<xsl:value-of select="cccxsl:getvar('TaxRateWork')"/>
				</xsl:attribute>			
			</xsl:if>
			<xsl:attribute name="MarkupPercent"/>
			<xsl:attribute name="DiscountPercent"/>
		</ProfileItem>
		<ProfileItem Category="Labor" Type="Body">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborTaxableIndicator='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborTaxableIndicator='T'">
				<xsl:attribute name="TaxPercent">
					<xsl:value-of select="cccxsl:setvar('TaxRateWork', 0)"/>
					
					<xsl:for-each select="ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborTax/Tax">
						<xsl:if test="TaxIndicator='Y'">
							<xsl:choose>
                                		<xsl:when test="TaxType/@code='LS'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxSales)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='CT'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxCounty)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='MP'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxMunicipal)"/></xsl:when>
	                            </xsl:choose>							
						</xsl:if>
					</xsl:for-each>
					
					<xsl:value-of select="cccxsl:getvar('TaxRateWork')"/>
				</xsl:attribute>			
			</xsl:if>
			<xsl:attribute name="Rate"><xsl:value-of select="ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborHourlyRate"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Labor" Type="Frame">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborTaxableIndicator='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborTaxableIndicator='T'">
				<xsl:attribute name="TaxPercent">
					<xsl:value-of select="cccxsl:setvar('TaxRateWork', 0)"/>
					
					<xsl:for-each select="ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborTax/Tax">
						<xsl:if test="TaxIndicator='Y'">
							<xsl:choose>
                                		<xsl:when test="TaxType/@code='LS'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxSales)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='CT'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxCounty)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='MP'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxMunicipal)"/></xsl:when>
	                            </xsl:choose>							
						</xsl:if>
					</xsl:for-each>
					
					<xsl:value-of select="cccxsl:getvar('TaxRateWork')"/>
				</xsl:attribute>			
			</xsl:if>
			<xsl:attribute name="Rate"><xsl:value-of select="ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborHourlyRate"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Labor" Type="Mechanical">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborTaxableIndicator='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborTaxableIndicator='T'">
				<xsl:attribute name="TaxPercent">
					<xsl:value-of select="cccxsl:setvar('TaxRateWork', 0)"/>
					
					<xsl:for-each select="ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborTax/Tax">
						<xsl:if test="TaxIndicator='Y'">
							<xsl:choose>
                                		<xsl:when test="TaxType/@code='LS'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxSales)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='CT'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxCounty)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='MP'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxMunicipal)"/></xsl:when>
	                            </xsl:choose>							
						</xsl:if>
					</xsl:for-each>
					
					<xsl:value-of select="cccxsl:getvar('TaxRateWork')"/>
				</xsl:attribute>			
			</xsl:if>
			<xsl:attribute name="Rate"><xsl:value-of select="ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborHourlyRate"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Labor" Type="Refinish">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborTaxableIndicator='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborTaxableIndicator='T'">
				<xsl:attribute name="TaxPercent">
					<xsl:value-of select="cccxsl:setvar('TaxRateWork', 0)"/>
					
					<xsl:for-each select="ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborTax/Tax">
						<xsl:if test="TaxIndicator='Y'">
							<xsl:choose>
                                		<xsl:when test="TaxType/@code='LS'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxSales)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='CT'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxCounty)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='MP'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxMunicipal)"/></xsl:when>
	                            </xsl:choose>							
						</xsl:if>
					</xsl:for-each>
					
					<xsl:value-of select="cccxsl:getvar('TaxRateWork')"/>
				</xsl:attribute>			
			</xsl:if>
			<xsl:attribute name="Rate"><xsl:value-of select="ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborHourlyRate"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Materials" Type="Paint1Stage">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/MaterialsTaxableIndicator='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/MaterialsTaxableIndicator='T'">
				<xsl:attribute name="TaxPercent">
					<xsl:value-of select="cccxsl:setvar('TaxRateWork', 0)"/>
					
					<xsl:for-each select="ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/MaterialTax/Tax">
						<xsl:if test="TaxIndicator='Y'">
							<xsl:choose>
                                		<xsl:when test="TaxType/@code='LS'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxSales)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='CT'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxCounty)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='MP'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxMunicipal)"/></xsl:when>
	                            </xsl:choose>							
						</xsl:if>
					</xsl:for-each>
					
					<xsl:value-of select="cccxsl:getvar('TaxRateWork')"/>
				</xsl:attribute>			
			</xsl:if>
			<xsl:attribute name="Rate"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/LaborRate"/></xsl:attribute>
			<xsl:attribute name="MaxDollars"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/CalcMaxDollars"/></xsl:attribute>
			<xsl:attribute name="MaxHours"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/CalcLaborMaxHours"/></xsl:attribute>
			<xsl:attribute name="CalcMethod"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/CalcCode/@code"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Materials" Type="Paint2Stage">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/MaterialsProfile[MaterialType/@code='MA2S']/MaterialsTaxableIndicator='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="ProfileRates/MaterialsProfile[MaterialType/@code='MA2S']/MaterialsTaxableIndicator='T'">
				<xsl:attribute name="TaxPercent">
					<xsl:value-of select="cccxsl:setvar('TaxRateWork', 0)"/>
					
					<xsl:for-each select="ProfileRates/MaterialsProfile[MaterialType/@code='MA2S']/MaterialTax/Tax">
						<xsl:if test="TaxIndicator='Y'">
							<xsl:choose>
                                		<xsl:when test="TaxType/@code='LS'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxSales)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='CT'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxCounty)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='MP'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxMunicipal)"/></xsl:when>
	                            </xsl:choose>							
						</xsl:if>
					</xsl:for-each>
					
					<xsl:value-of select="cccxsl:getvar('TaxRateWork')"/>
				</xsl:attribute>			
			</xsl:if>
			<xsl:attribute name="Rate"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MA2S']/LaborRate"/></xsl:attribute>
			<xsl:attribute name="MaxDollars"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MA2S']/CalcMaxDollars"/></xsl:attribute>
			<xsl:attribute name="MaxHours"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MA2S']/CalcLaborMaxHours"/></xsl:attribute>
			<xsl:attribute name="CalcMethod"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MA2S']/CalcCode/@code"/></xsl:attribute>
		</ProfileItem>
		<ProfileItem Category="Materials" Type="Paint3Stage">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/MaterialsProfile[MaterialType/@code='MA3S']/MaterialsTaxableIndicator='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="ProfileRates/MaterialsProfile[MaterialType/@code='MA3S']/MaterialsTaxableIndicator='T'">
				<xsl:attribute name="TaxPercent">
					<xsl:value-of select="cccxsl:setvar('TaxRateWork', 0)"/>
					
					<xsl:for-each select="ProfileRates/MaterialsProfile[MaterialType/@code='MA3S']/MaterialTax/Tax">
						<xsl:if test="TaxIndicator='Y'">
							<xsl:choose>
                                		<xsl:when test="TaxType/@code='LS'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxSales)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='CT'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxCounty)"/></xsl:when>
                                		<xsl:when test="TaxType/@code='MP'"><xsl:value-of select="cccxsl:setvar('TaxRateWork', cccxsl:getvar('TaxRateWork') + $TaxMunicipal)"/></xsl:when>
	                            </xsl:choose>							
						</xsl:if>
					</xsl:for-each>
					
					<xsl:value-of select="cccxsl:getvar('TaxRateWork')"/>
				</xsl:attribute>			
			</xsl:if>
			<xsl:attribute name="Rate"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MA3S']/LaborRate"/></xsl:attribute>
			<xsl:attribute name="MaxDollars"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MA3S']/CalcMaxDollars"/></xsl:attribute>
			<xsl:attribute name="MaxHours"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MA3S']/CalcLaborMaxHours"/></xsl:attribute>
			<xsl:attribute name="CalcMethod"><xsl:value-of select="ProfileRates/MaterialsProfile[MaterialType/@code='MA3S']/CalcCode/@code"/></xsl:attribute>
		</ProfileItem>				
	</Profiles>
</xsl:template>


<!--- Transform Summary Information -->
<xsl:template match="Estimate0110" mode="Summary">
	<!--  Initialize variables needed later -->
	
	<xsl:value-of select="cccxsl:setvar('RepairAmountTotal', 0)"/>
	<xsl:value-of select="cccxsl:setvar('ReplaceAmountTotal', 0)"/>
	
	
	<!--- First we need to get together all the values we need to build the summary -->

	<!--- Part Costs -->

	<!--- Taxable Discount Amount (Parts) -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='PAN']/DiscountAmount"><xsl:value-of select="cccxsl:setvar('DiscountTaxAmount', -1 * (number(Subtotals[TotalTypeCode/@code='PAN']/DiscountAmount)))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('DiscountTaxAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Non-Taxable Discount Amount (Parts) -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='PAN']/NonTaxableDiscountAmount"><xsl:value-of select="cccxsl:setvar('DiscountNonTaxAmount', -1 * (number(Subtotals[TotalTypeCode/@code='PAN']/NonTaxableDiscountAmount)))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('DiscountNonTaxAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Discount Amount Add Taxable and Nontaxable discounts -->
	<xsl:value-of select="cccxsl:setvar('TotalDiscountAmount', round((cccxsl:getvar('DiscountTaxAmount') + cccxsl:getvar('DiscountNonTaxAmount')) * 100) div 100)"/>

	<!--- Discount Base (Parts) -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='PAT']/DiscountAmount"><xsl:value-of select="cccxsl:setvar('DiscountBase', round((sum(Subtotals[TotalTypeCode/@code='PAN']/Amount) + sum(Subtotals[TotalTypeCode/@code='PAN']/NonTaxableAmount)) * 100) div 100)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('DiscountBase', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Discount Percent (Parts) -->
	<xsl:choose>
       	<xsl:when test="cccxsl:getvar('DiscountBase')='0'"><xsl:value-of select="cccxsl:setvar('DiscountPct', 0)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('DiscountPct', round((-1 * cccxsl:getvar('TotalDiscountAmount') div cccxsl:getvar('DiscountBase') * 100) * 1000) div 1000)"/></xsl:otherwise>
     </xsl:choose>
			
	<!--- Taxable Markup Amount (Parts) -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='PAN']/MarkupAmount"><xsl:value-of select="cccxsl:setvar('MarkupTaxAmount', number(Subtotals[TotalTypeCode/@code='PAN']/MarkupAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('MarkupTaxAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Non-Taxable Markup Amount (Parts) -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='PAN']/NonTaxableMarkupAmount"><xsl:value-of select="cccxsl:setvar('MarkupNonTaxAmount', number(Subtotals[TotalTypeCode/@code='PAN']/NonTaxableMarkupAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('MarkupNonTaxAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Markup Amount Add Taxable and Nontaxable markups -->
	<xsl:value-of select="cccxsl:setvar('TotalMarkupAmount', round((cccxsl:getvar('MarkupTaxAmount') + cccxsl:getvar('MarkupNonTaxAmount')) * 100) div 100)"/>

	<!--- Markup Base (Parts) -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='PAT']/MarkupAmount"><xsl:value-of select="cccxsl:setvar('MarkupBase', round((sum(Subtotals[TotalTypeCode/@code='PAN']/Amount) + sum(Subtotals[TotalTypeCode/@code='PAN']/NonTaxableAmount)) * 100) div 100)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('MarkupBase', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Markup Percent (Parts) -->
	<xsl:choose>
       	<xsl:when test="cccxsl:getvar('MarkupBase')='0'"><xsl:value-of select="cccxsl:setvar('MarkupPct', 0)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('MarkupPct', round((cccxsl:getvar('TotalMarkupAmount') div cccxsl:getvar('MarkupBase') * 100) * 1000) div 1000)"/></xsl:otherwise>
     </xsl:choose>


	<!--- Total Taxable Parts -->	
	<xsl:value-of select="cccxsl:setvar('TotalTaxParts', round((sum(Subtotals[TotalType/@code='PA' and TotalTypeCode/@code!='PAT' and TotalTypeCode/@code!='PAS' and TotalTypeCode/@code!='PAO']/TotalAmount) - cccxsl:getvar('DiscountTaxAmount') - cccxsl:getvar('MarkupTaxAmount'))  * 100) div 100)"/> 

	<!--- Total Non-Taxable Parts -->	
	<xsl:value-of select="cccxsl:setvar('TotalNonTaxParts', round((sum(Subtotals[TotalType/@code='PA' and TotalTypeCode/@code!='PAT' and TotalTypeCode/@code!='PAS' and TotalTypeCode/@code!='PAO']/TotalNonTaxableAmount) - cccxsl:getvar('DiscountNonTaxAmount') - cccxsl:getvar('MarkupNonTaxAmount'))  * 100) div 100)"/>

	<!--- Other Taxable Parts (For CCC these are Miscellaneous Taxable Parts from Pathways) -->	
	<xsl:choose> 
       	<xsl:when test="Subtotals[TotalTypeCode/@code='PAO']/Amount"><xsl:value-of select="cccxsl:setvar('TotalOtherTaxParts', round(sum(Subtotals[TotalTypeCode/@code='PAO']/TotalAmount)  * 100) div 100)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('TotalOtherTaxParts', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Other NonTaxable Parts (For CCC these are Miscellaneous NonTaxable Parts from Pathways) -->	
	<xsl:choose> 
       	<xsl:when test="Subtotals[TotalTypeCode/@code='PAO']/TotalNonTaxableAmount"><xsl:value-of select="cccxsl:setvar('TotalOtherNonTaxParts', round(sum(Subtotals[TotalTypeCode/@code='PAO']/TotalNonTaxableAmount)  * 100) div 100)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('TotalOtherNonTaxParts', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Parts -->	
	<xsl:value-of select="cccxsl:setvar('TotalParts', round((cccxsl:getvar('TotalTaxParts') + cccxsl:getvar('TotalNonTaxParts') + cccxsl:getvar('TotalDiscountAmount') + cccxsl:getvar('TotalMarkupAmount') + cccxsl:getvar('TotalOtherTaxParts') + cccxsl:getvar('TotalOtherNonTaxParts')) * 100) div 100)"/> 


	<!--- Body Materials Amount (Note we classify anything not Paint Supplies as Body Materials)  -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalType/@code='MA' and (TotalTypeCode/@code!='MAPA' and TotalTypeCode/@code!='MAT')]/TotalTypeAmount"><xsl:value-of select="cccxsl:setvar('BodyMaterialAmount', sum(Subtotals[TotalType/@code='MA' and TotalTypeCode/@code!='MAPA' and TotalTypeCode/@code!='MAT']/TotalTypeAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('BodyMaterialAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Body Material Hours... Note we check for TotalTypeAmount as CCC has a tendancy to send hours but 0 for amount -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalType/@code='MA' and (TotalTypeCode/@code!='MAPA' and TotalTypeCode/@code!='MAT')]/TotalTypeHours and Subtotals[TotalType/@code='MA' and (TotalTypeCode/@code!='MAPA' and TotalTypeCode/@code!='MAT')]/TotalTypeAmount"><xsl:value-of select="cccxsl:setvar('BodyMaterialHours', sum(Subtotals[TotalType/@code='MA' and (TotalTypeCode/@code!='MAPA' and TotalTypeCode/@code!='MAT')]/TotalTypeHours))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('BodyMaterialHours', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Body Material Rate -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/MaterialsProfile[MaterialType/@code='MASH']/LaborRate"><xsl:value-of select="cccxsl:setvar('BodyMaterialRate', number(ProfileRates/MaterialsProfile[MaterialType/@code='MASH']/LaborRate))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('BodyMaterialRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Paint Materials Amount -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='MAPA']/TotalTypeAmount"><xsl:value-of select="cccxsl:setvar('PaintMaterialAmount', number(Subtotals[TotalTypeCode/@code='MAPA']/TotalTypeAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('PaintMaterialAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Paint Material Hours ... Note we check for TotalTypeAmount as CCC has a tendancy to send hours but 0 for amount -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='MAPA']/TotalTypeHours and Subtotals[TotalTypeCode/@code='MAPA']/TotalTypeAmount"><xsl:value-of select="cccxsl:setvar('PaintMaterialHours', number(Subtotals[TotalTypeCode/@code='MAPA']/TotalTypeHours))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('PaintMaterialHours', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Paint Material Rate -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/LaborRate"><xsl:value-of select="cccxsl:setvar('PaintMaterialRate', number(ProfileRates/MaterialsProfile[MaterialType/@code='MAPA']/LaborRate))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('PaintMaterialRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Materials -->	
	<xsl:value-of select="cccxsl:setvar('TotalMaterials', round((cccxsl:getvar('BodyMaterialAmount') + cccxsl:getvar('PaintMaterialAmount')) * 100) div 100)"/> 

	<!--- Total Parts and Materials -->	
	<xsl:value-of select="cccxsl:setvar('TotalPartsMaterials', round((cccxsl:getvar('TotalParts') + cccxsl:getvar('TotalMaterials')) * 100) div 100)"/> 


	<!-- Labor Costs -->

	<!--- Body Labor Amount -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='LAB']/TotalTypeAmount"><xsl:value-of select="cccxsl:setvar('BodyLaborAmount', number(Subtotals[TotalTypeCode/@code='LAB']/TotalTypeAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('BodyLaborAmount', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Body Labor Hours -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='LAB']/TotalTypeHours"><xsl:value-of select="cccxsl:setvar('BodyLaborHours', number(Subtotals[TotalTypeCode/@code='LAB']/TotalTypeHours))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('BodyLaborHours', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Body Labor Rate -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborHourlyRate"><xsl:value-of select="cccxsl:setvar('BodyLaborRate', number(ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborHourlyRate))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('BodyLaborRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Frame Labor Amount -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='LAF' or TotalTypeCode/@code='LAS']/TotalTypeAmount"><xsl:value-of select="cccxsl:setvar('FrameLaborAmount', sum(Subtotals[TotalTypeCode/@code='LAF' or TotalTypeCode/@code='LAS']/TotalTypeAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('FrameLaborAmount', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Frame Labor Hours -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='LAF' or TotalTypeCode/@code='LAS']/TotalTypeHours"><xsl:value-of select="cccxsl:setvar('FrameLaborHours', sum(Subtotals[TotalTypeCode/@code='LAF' or TotalTypeCode/@code='LAS']/TotalTypeHours))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('FrameLaborHours', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Frame Labor Rate -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborHourlyRate"><xsl:value-of select="cccxsl:setvar('FrameLaborRate', number(ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborHourlyRate))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('FrameLaborRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!---Mechanical Labor Amount -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='LAD' or TotalTypeCode/@code='LAE' or TotalTypeCode/@code='LAM']/TotalTypeAmount"><xsl:value-of select="cccxsl:setvar('MechLaborAmount', sum(Subtotals[TotalTypeCode/@code='LAD' or TotalTypeCode/@code='LAE' or TotalTypeCode/@code='LAM']/TotalTypeAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('MechLaborAmount', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Mechanical Labor Hours -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='LAD' or TotalTypeCode/@code='LAE' or TotalTypeCode/@code='LAM']/TotalTypeHours"><xsl:value-of select="cccxsl:setvar('MechLaborHours', sum(Subtotals[TotalTypeCode/@code='LAD' or TotalTypeCode/@code='LAE' or TotalTypeCode/@code='LAM']/TotalTypeHours))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('MechLaborHours', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Mechanical Labor Rate -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborHourlyRate"><xsl:value-of select="cccxsl:setvar('MechLaborRate', number(ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborHourlyRate))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('MechLaborRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Paint Labor Amount -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='LAR']/TotalTypeAmount"><xsl:value-of select="cccxsl:setvar('PaintLaborAmount', number(Subtotals[TotalTypeCode/@code='LAR']/TotalTypeAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('PaintLaborAmount', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Paint Labor Hours -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='LAR']/TotalTypeHours"><xsl:value-of select="cccxsl:setvar('PaintLaborHours', number(Subtotals[TotalTypeCode/@code='LAR']/TotalTypeHours))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('PaintLaborHours', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Paint Labor Rate -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborHourlyRate"><xsl:value-of select="cccxsl:setvar('PaintLaborRate', number(ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborHourlyRate))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('PaintLaborRate', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Labor Amount -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='LAT']/TotalTypeAmount"><xsl:value-of select="cccxsl:setvar('TotalLaborAmount', number(Subtotals[TotalTypeCode/@code='LAT']/TotalTypeAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('TotalLaborAmount', 0)"/></xsl:otherwise>
     </xsl:choose> 

	<!--- Other Labor (For CCC this is the difference between the Total Labor retrieved above and that calculated from the other values) -->	
	<xsl:value-of select="cccxsl:setvar('TotalOtherLabor', round((cccxsl:getvar('TotalLaborAmount') - (cccxsl:getvar('BodyLaborAmount') + cccxsl:getvar('FrameLaborAmount') + cccxsl:getvar('MechLaborAmount') + cccxsl:getvar('PaintLaborAmount'))) * 100) div 100)"/>
	
	
	<!--- Additional Charges -->
	
	<!--- Storage Amount -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='OTST']/TotalTypeAmount"><xsl:value-of select="cccxsl:setvar('StorageAmount', number(Subtotals[TotalTypeCode/@code='OTST']/TotalTypeAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('StorageAmount', 0)"/></xsl:otherwise>
     </xsl:choose>


	<!--- Sublet Amount..To get sublet we need to add SubletParts, and OtherTotalSublet together -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='PAS']/TotalTypeAmount"><xsl:value-of select="cccxsl:setvar('SubletPASAmount', number(Subtotals[TotalTypeCode/@code='PAS']/TotalTypeAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('SubletPASAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<xsl:choose>
       	<xsl:when test="Subtotals[TotalType/@code='OTSL']/TotalTypeAmount"><xsl:value-of select="cccxsl:setvar('SubletOTSLAmount', number(Subtotals[TotalType/@code='OTSL']/TotalTypeAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('SubletOTSLAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Sublet -->	
	<xsl:value-of select="cccxsl:setvar('SubletAmount', round((cccxsl:getvar('SubletPASAmount') + cccxsl:getvar('SubletOTSLAmount')) * 100) div 100)"/> 


	<!--- Towing Amount -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalTypeCode/@code='OTTW']/TotalTypeAmount"><xsl:value-of select="cccxsl:setvar('TowingAmount', number(Subtotals[TotalTypeCode/@code='OTTW']/TotalTypeAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('TowingAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Other Additional Charges -->
	<xsl:choose>
       	<xsl:when test="Subtotals[TotalType/@code='OTAC' and TotalTypeCode and TotalTypeCode/@code!='OTST' and TotalTypeCode/@code!='OTTW']/TotalTypeAmount"><xsl:value-of select="cccxsl:setvar('OtherAddlChargesAmount', number(Subtotals[TotalType/@code='OTAC']/TotalTypeAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('OtherAddlChargesAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Additional Charges (Storage, Sublet, Towing, and Other added together) -->	
	<xsl:value-of select="cccxsl:setvar('TotalAddlCharges', cccxsl:getvar('StorageAmount') + cccxsl:getvar('SubletAmount') + cccxsl:getvar('TowingAmount') + cccxsl:getvar('OtherAddlChargesAmount'))"/>


	<!--- Subtotal -->	
	<xsl:value-of select="cccxsl:setvar('SubtotalAmount', round((cccxsl:getvar('TotalPartsMaterials') + cccxsl:getvar('TotalLaborAmount') + cccxsl:getvar('TotalAddlCharges')) * 100) div 100)"/> 


	<!--- Taxes -->
	
	<!--- County Tax Base -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/TaxTierProfile[TaxType/@code='CT']"><xsl:value-of select="cccxsl:setvar('CountyTaxBase', number(ProfileRates/TaxTierProfile[TaxType/@code='CT']/TaxTier/TaxThreshold))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('CountyTaxBase', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- County Tax Percent -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/TaxTierProfile[TaxType/@code='CT']"><xsl:value-of select="cccxsl:setvar('CountyTaxPct', round((ProfileRates/TaxTierProfile[TaxType/@code='CT']/TaxTier/TaxRate) * 1000) div 1000)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('CountyTaxPct', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- County Tax Amount -->
	<xsl:choose>
       	<xsl:when test="cccxsl:getvar('CountyTaxBase')='0'"><xsl:value-of select="cccxsl:setvar('CountyTaxAmount', '0')"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('CountyTaxAmount', round(cccxsl:getvar('CountyTaxBase') * (cccxsl:getvar('CountyTaxPct') div 100) * 100) div 100)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Municipal Tax Base -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/TaxTierProfile[TaxType/@code='MP']"><xsl:value-of select="cccxsl:setvar('MunicipalTaxBase', number(ProfileRates/TaxTierProfile[TaxType/@code='MP']/TaxTier/TaxThreshold))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('MunicipalTaxBase', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Municipal Tax Percent -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/TaxTierProfile[TaxType/@code='MP']"><xsl:value-of select="cccxsl:setvar('MunicipalTaxPct', round((ProfileRates/TaxTierProfile[TaxType/@code='MP']/TaxTier/TaxRate) * 1000) div 1000)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('MunicipalTaxPct', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Municipal Tax Amount -->
	<xsl:choose>
       	<xsl:when test="cccxsl:getvar('MunicipalTaxBase')='0'"><xsl:value-of select="cccxsl:setvar('MunicipalTaxAmount', '0')"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('MunicipalTaxAmount', round(cccxsl:getvar('MunicipalTaxBase') * (cccxsl:getvar('MunicipalTaxPct') div 100) * 100) div 100)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Sales Tax Base -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/TaxTierProfile[TaxType/@code='LS']"><xsl:value-of select="cccxsl:setvar('SalesTaxBase', number(ProfileRates/TaxTierProfile[TaxType/@code='LS']/TaxTier/TaxThreshold))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('SalesTaxBase', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Sales Tax Percent -->
	<xsl:choose>
       	<xsl:when test="ProfileRates/TaxTierProfile[TaxType/@code='LS']"><xsl:value-of select="cccxsl:setvar('SalesTaxPct', round((ProfileRates/TaxTierProfile[TaxType/@code='LS']/TaxTier/TaxRate) *1000) div 1000)"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('SalesTaxPct', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Sales Tax Amount -->
	<xsl:choose>
       	<xsl:when test="cccxsl:getvar('SalesTaxBase')='0'"><xsl:value-of select="cccxsl:setvar('SalesTaxAmount', '0')"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('SalesTaxAmount', round(cccxsl:getvar('SalesTaxBase') * (cccxsl:getvar('SalesTaxPct') div 100) * 100) div 100)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Tax Amount -->
	<xsl:choose>
       	<xsl:when test="Totals/TaxAmount"><xsl:value-of select="cccxsl:setvar('TotalTaxAmount', number(Totals/TaxAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('TotalTaxAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Other Taxes -->
	<xsl:value-of select="cccxsl:setvar('OtherTaxAmount', round((cccxsl:getvar('TotalTaxAmount') - cccxsl:getvar('CountyTaxAmount') - cccxsl:getvar('MunicipalTaxAmount') - cccxsl:getvar('SalesTaxAmount')) * 100) div 100)"/> 
	
	<!--- Total Repair Cost -->
	<xsl:value-of select="cccxsl:setvar('RepairTotalAmount', round((cccxsl:getvar('SubtotalAmount') + cccxsl:getvar('TotalTaxAmount')) * 100) div 100)"/> 

			
	<!--- Adjustments -->
	
	<!--- Appearance Allowance -->
	<xsl:choose>
       	<xsl:when test="Totals/AppearanceAllowanceAmount"><xsl:value-of select="cccxsl:setvar('AppearanceAllowanceAmount', -1 * number(Totals/AppearanceAllowanceAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('AppearanceAllowanceAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Betterment -->
	<xsl:choose>
       	<xsl:when test="Totals/BettermentAmount"><xsl:value-of select="cccxsl:setvar('BettermentAmount', number(Totals/BettermentAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('BettermentAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Deductible -->
	<xsl:choose>
       	<xsl:when test="Totals/DeductibleAmount"><xsl:value-of select="cccxsl:setvar('DeductibleAmount', number(Totals/DeductibleAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('DeductibleAmount', 0)"/></xsl:otherwise>
     </xsl:choose>

	<!--- Total Adjustment (Deductible, Appearance Allowance, and Betterment added together -->	
	<xsl:value-of select="cccxsl:setvar('TotalAdjustmentAmount', cccxsl:getvar('AppearanceAllowanceAmount') + cccxsl:getvar('BettermentAmount') + cccxsl:getvar('DeductibleAmount'))"/>

		
	<!--- Net Total -->
	<xsl:choose>
       	<xsl:when test="Totals/TotalAmount"><xsl:value-of select="cccxsl:setvar('NetTotalAmount', number(Totals/NetTotalAmount))"/></xsl:when>
		<xsl:otherwise><xsl:value-of select="cccxsl:setvar('NetTotalAmount', 0)"/></xsl:otherwise>
     </xsl:choose>


	<!--- Variance Amount  (This is a "bit bucket" to capture variances between what I calculate and what the "real" estimate total is) -->
	<xsl:value-of select="cccxsl:setvar('ZZVarianceAmount', round((cccxsl:getvar('RepairTotalAmount') - cccxsl:getvar('AppearanceAllowanceAmount') - cccxsl:getvar('BettermentAmount') - cccxsl:getvar('DeductibleAmount') - cccxsl:getvar('NetTotalAmount')) * 100) div 100)"/> 


	<!--- Continue building output -->
	<Summary>
		<LineItem Type="PartsTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="Yes">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TotalTaxParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('TotalTaxParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsNonTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="No">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TotalNonTaxParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('TotalNonTaxParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsMarkup" TransferLine="Yes" Category="PC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="cccxsl:getvar('MarkupTaxAmount') &gt;= cccxsl:getvar('MarkupNonTaxAmount')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TotalMarkupAmount')"/></xsl:attribute>
			<xsl:attribute name="Percent"><xsl:value-of select="cccxsl:getvar('MarkupPct')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('MarkupBase')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsDiscount" TransferLine="Yes" Category="PC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="cccxsl:getvar('DiscountTaxAmount') &lt;= cccxsl:getvar('DiscountNonTaxAmount')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TotalDiscountAmount')"/></xsl:attribute>
			<xsl:attribute name="Percent"><xsl:value-of select="cccxsl:getvar('DiscountPct')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('DiscountBase')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsOtherTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="Yes">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TotalOtherTaxParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('TotalOtherTaxParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsOtherNonTaxable" TransferLine="Yes" Category="PC" Note="" Taxable="No">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TotalOtherNonTaxParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('TotalOtherNonTaxParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TotalParts')"/></xsl:attribute>
		</LineItem>
		
		<LineItem Type="Body" TransferLine="Yes" Category="MC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/MaterialsProfile[MaterialType/@code='MASH']/MaterialTax/Tax/TaxIndicator='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('BodyMaterialAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="cccxsl:getvar('BodyMaterialHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('BodyMaterialRate')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Paint" TransferLine="Yes" Category="MC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/MaterialsProfile[MaterialType/@code='MA2S']/MaterialTax/Tax/TaxIndicator='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('PaintMaterialAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="cccxsl:getvar('PaintMaterialHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('PaintMaterialRate')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="SuppliesTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TotalMaterials')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="PartsAndMaterialTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TotalPartsMaterials')"/></xsl:attribute>
		</LineItem>
		
		<LineItem Type="Body" TransferLine="Yes" Category="LC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborTaxableIndicator='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('BodyLaborAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="cccxsl:getvar('BodyLaborHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('BodyLaborRate')"/></xsl:attribute>
			<xsl:attribute name="RepairHours">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='LAB']/TotalTypeRepairHours) &gt; 0 and ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborHourlyRate">
						<xsl:value-of select="number(Subtotals[TotalTypeCode/@code='LAB']/TotalTypeRepairHours)"/>
						<xsl:value-of select="cccxsl:setvar('RepairAmountTotal', round((cccxsl:getvar('RepairAmountTotal') + number(Subtotals[TotalTypeCode/@code='LAB']/TotalTypeRepairHours) * number(ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborHourlyRate)) * 100) div 100)"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ReplaceHours">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='LAB']/TotalTypeReplaceHours) &gt; 0 and ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborHourlyRate">
						<xsl:value-of select="number(Subtotals[TotalTypeCode/@code='LAB']/TotalTypeReplaceHours)"/>
						<xsl:value-of select="cccxsl:setvar('ReplaceAmountTotal',  round((cccxsl:getvar('ReplaceAmountTotal') + number(Subtotals[TotalTypeCode/@code='LAB']/TotalTypeReplaceHours) * number(ProfileRates/LaborProfile[LaborType/@code='LAB']/LaborHourlyRate)) * 100) div 100)"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
		</LineItem>
		<LineItem Type="Mechanical" TransferLine="Yes" Category="LC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborTaxableIndicator='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('MechLaborAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="cccxsl:getvar('MechLaborHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('MechLaborRate')"/></xsl:attribute>
			<xsl:attribute name="RepairHours">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='LAM']/TotalTypeRepairHours) &gt; 0 and ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborHourlyRate">
						<xsl:value-of select="number(Subtotals[TotalTypeCode/@code='LAM']/TotalTypeRepairHours)"/>
						<xsl:value-of select="cccxsl:setvar('RepairAmountTotal', round((cccxsl:getvar('RepairAmountTotal') + number(Subtotals[TotalTypeCode/@code='LAM']/TotalTypeRepairHours) * number(ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborHourlyRate)) * 100) div 100)"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ReplaceHours">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='LAM']/TotalTypeReplaceHours) &gt; 0 and ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborHourlyRate">
						<xsl:value-of select="number(Subtotals[TotalTypeCode/@code='LAM']/TotalTypeReplaceHours)"/>
						<xsl:value-of select="cccxsl:setvar('ReplaceAmountTotal',  round((cccxsl:getvar('ReplaceAmountTotal') + number(Subtotals[TotalTypeCode/@code='LAM']/TotalTypeReplaceHours) * number(ProfileRates/LaborProfile[LaborType/@code='LAM']/LaborHourlyRate)) * 100) div 100)"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
		</LineItem>
		<LineItem Type="Frame" TransferLine="Yes" Category="LC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborTaxableIndicator='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('FrameLaborAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="cccxsl:getvar('FrameLaborHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('FrameLaborRate')"/></xsl:attribute>
			<xsl:attribute name="RepairHours">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='LAF']/TotalTypeRepairHours) &gt; 0 and ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborHourlyRate">
						<xsl:value-of select="number(Subtotals[TotalTypeCode/@code='LAF']/TotalTypeRepairHours)"/>
						<xsl:value-of select="cccxsl:setvar('RepairAmountTotal', round((cccxsl:getvar('RepairAmountTotal') + number(Subtotals[TotalTypeCode/@code='LAF']/TotalTypeRepairHours) * number(ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborHourlyRate)) * 100) div 100)"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ReplaceHours">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='LAF']/TotalTypeReplaceHours) &gt; 0 and ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborHourlyRate">
						<xsl:value-of select="number(Subtotals[TotalTypeCode/@code='LAF']/TotalTypeReplaceHours)"/>
						<xsl:value-of select="cccxsl:setvar('ReplaceAmountTotal',  round((cccxsl:getvar('ReplaceAmountTotal') + number(Subtotals[TotalTypeCode/@code='LAF']/TotalTypeReplaceHours) * number(ProfileRates/LaborProfile[LaborType/@code='LAF']/LaborHourlyRate)) * 100) div 100)"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
		</LineItem>
		<LineItem Type="Paint" TransferLine="Yes" Category="LC" Note="">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborTaxableIndicator='T'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('PaintLaborAmount')"/></xsl:attribute>
			<xsl:attribute name="Hours"><xsl:value-of select="cccxsl:getvar('PaintLaborHours')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('PaintLaborRate')"/></xsl:attribute>
			<xsl:attribute name="RepairHours">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='LAR']/TotalTypeRepairHours) &gt; 0 and ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborHourlyRate">
						<xsl:value-of select="number(Subtotals[TotalTypeCode/@code='LAR']/TotalTypeRepairHours)"/>
						<xsl:value-of select="cccxsl:setvar('RepairAmountTotal', round((cccxsl:getvar('RepairAmountTotal') + number(Subtotals[TotalTypeCode/@code='LAR']/TotalTypeRepairHours) * number(ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborHourlyRate)) * 100) div 100)"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ReplaceHours">
				<xsl:choose>
                    	<xsl:when test="number(Subtotals[TotalTypeCode/@code='LAR']/TotalTypeReplaceHours) &gt; 0 and ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborHourlyRate">
						<xsl:value-of select="number(Subtotals[TotalTypeCode/@code='LAR']/TotalTypeReplaceHours)"/>
						<xsl:value-of select="cccxsl:setvar('ReplaceAmountTotal',  round((cccxsl:getvar('ReplaceAmountTotal') + number(Subtotals[TotalTypeCode/@code='LAR']/TotalTypeReplaceHours) * number(ProfileRates/LaborProfile[LaborType/@code='LAR']/LaborHourlyRate)) * 100) div 100)"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
		</LineItem>
		<LineItem Type="Refinish" TransferLine="Yes" Category="LC" Note="" Taxable="Yes">
			<xsl:attribute name="ExtendedAmount">0</xsl:attribute>
			<xsl:attribute name="Hours">0</xsl:attribute>
			<xsl:attribute name="UnitAmount">0</xsl:attribute>
		</LineItem>
		<LineItem Type="Other" TransferLine="Yes" Category="LC" Note="" Taxable="Yes">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TotalOtherLabor')"/></xsl:attribute>
			<xsl:attribute name="Hours">0</xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('TotalOtherLabor')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="LaborTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TotalLaborAmount')"/></xsl:attribute>
		</LineItem>
			
          <xsl:if test="cccxsl:getvar('TowingAmount') &gt; 0">
			<LineItem Type="Towing" TransferLine="Yes" Category="AC" Note="">
				<xsl:attribute name="Taxable">
					<xsl:choose>
	                    	<xsl:when test="ProfileRates/AdditionalCharges[AddtlChargesID='OTTW']/Tax/TaxIndicator='Y'">Yes</xsl:when>
	                		<xsl:otherwise>No</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TowingAmount')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('TowingAmount')"/></xsl:attribute>
			</LineItem>
          </xsl:if>
          <xsl:if test="cccxsl:getvar('StorageAmount') &gt; 0">
			<LineItem Type="Storage" TransferLine="Yes" Category="AC" Note="">
				<xsl:attribute name="Taxable">
					<xsl:choose>
	                    	<xsl:when test="ProfileRates/AdditionalCharges[AddtlChargesID='OTST']/Tax/TaxIndicator='Y'">Yes</xsl:when>
	                		<xsl:otherwise>No</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('StorageAmount')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('StorageAmount')"/></xsl:attribute>
			</LineItem>
          </xsl:if>
          <xsl:if test="cccxsl:getvar('SubletAmount') &gt; 0">
			<LineItem Type="Sublet" TransferLine="Yes" Category="AC" Note="">
				<xsl:attribute name="Taxable">
					<xsl:choose>
	                    	<xsl:when test="ProfileRates/PartProfile[PartType='PAS']/PartTax//Tax/TaxIndicator='Y'">Yes</xsl:when>
	                		<xsl:otherwise>No</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('SubletAmount')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('SubletAmount')"/></xsl:attribute>
			</LineItem>
          </xsl:if>
          <xsl:if test="cccxsl:getvar('OtherAddlChargesAmount') &gt; 0">
			<LineItem Type="Other" TransferLine="Yes" Category="AC" Note="">
				<xsl:attribute name="Taxable">
					<xsl:choose>
	                    	<xsl:when test="ProfileRates/AdditionalCharges[AddtlChargesID='EPC']/Tax/TaxIndicator='Y'">Yes</xsl:when>
	                		<xsl:otherwise>No</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('OtherAddlChargesAmount')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('OtherAddlChargesAmount')"/></xsl:attribute>
			</LineItem>
          </xsl:if>
		<LineItem Type="AdditionalChargesTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TotalAddlCharges')"/></xsl:attribute>
		</LineItem>

		<LineItem Type="SubTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('SubtotalAmount')"/></xsl:attribute>
		</LineItem>

          <xsl:if test="cccxsl:getvar('SalesTaxAmount') &gt; 0">
			<LineItem Type="Sales" TransferLine="Yes" Category="TX" Note="">
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('SalesTaxAmount')"/></xsl:attribute>
				<xsl:attribute name="Percent"><xsl:value-of select="cccxsl:getvar('SalesTaxPct')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('SalesTaxBase')"/></xsl:attribute>
				<xsl:attribute name="TaxBase">
					<xsl:choose>
	                    	<xsl:when test="cccxsl:getvar('SalesTaxBase') = cccxsl:getvar('TotalParts')">Parts</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('SalesTaxBase') = cccxsl:getvar('TotalMaterials')">Materials</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('SalesTaxBase') = cccxsl:getvar('TotalPartsMaterials')">PartsAndMaterials</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('SalesTaxBase') = cccxsl:getvar('TotalLaborAmount')">Labor</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('SalesTaxBase') = cccxsl:getvar('TotalAddlCharges')">AdditionalCharges</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('SalesTaxBase') = cccxsl:getvar('SubtotalAmount')">Subtotal</xsl:when>
						<xsl:otherwise>Other</xsl:otherwise>
	                	</xsl:choose>
				</xsl:attribute>
			</LineItem>
          </xsl:if>
		<xsl:if test="cccxsl:getvar('CountyTaxAmount') &gt; 0">
			<LineItem Type="County" TransferLine="Yes" Category="TX" Note="">
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('CountyTaxAmount')"/></xsl:attribute>
				<xsl:attribute name="Percent"><xsl:value-of select="cccxsl:getvar('CountyTaxPct')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('CountyTaxBase')"/></xsl:attribute>
				<xsl:attribute name="TaxBase">
					<xsl:choose>
	                    	<xsl:when test="cccxsl:getvar('CountyTaxBase') = cccxsl:getvar('TotalParts')">Parts</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('CountyTaxBase') = cccxsl:getvar('TotalMaterials')">Materials</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('CountyTaxBase') = cccxsl:getvar('TotalPartsMaterials')">PartsAndMaterials</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('CountyTaxBase') = cccxsl:getvar('TotalLaborAmount')">Labor</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('CountyTaxBase') = cccxsl:getvar('TotalAddlCharges')">AdditionalCharges</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('CountyTaxBase') = cccxsl:getvar('SubtotalAmount')">Subtotal</xsl:when>
						<xsl:otherwise>Other</xsl:otherwise>
	                	</xsl:choose>
				</xsl:attribute>
			</LineItem>
		</xsl:if>
          <xsl:if test="cccxsl:getvar('MunicipalTaxAmount') &gt; 0">
			<LineItem Type="Municipal" TransferLine="Yes" Category="TX" Note="">
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('MunicipalTaxAmount')"/></xsl:attribute>
				<xsl:attribute name="Percent"><xsl:value-of select="cccxsl:getvar('MunicipalTaxPct')"/></xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('MunicipalTaxBase')"/></xsl:attribute>
				<xsl:attribute name="TaxBase">
					<xsl:choose>
	                    	<xsl:when test="cccxsl:getvar('MunicipalTaxBase') = cccxsl:getvar('TotalParts')">Parts</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('MunicipalTaxBase') = cccxsl:getvar('TotalMaterials')">Materials</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('MunicipalTaxBase') = cccxsl:getvar('TotalPartsMaterials')">PartsAndMaterials</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('MunicipalTaxBase') = cccxsl:getvar('TotalLaborAmount')">Labor</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('MunicipalTaxBase') = cccxsl:getvar('TotalAddlCharges')">AdditionalCharges</xsl:when>
	                    	<xsl:when test="cccxsl:getvar('MunicipalTaxBase') = cccxsl:getvar('SubtotalAmount')">Subtotal</xsl:when>
						<xsl:otherwise>Other</xsl:otherwise>
	                	</xsl:choose>
				</xsl:attribute>
			</LineItem>
          </xsl:if>
          <xsl:if test="cccxsl:getvar('OtherTaxAmount') &gt; 0">
			<LineItem Type="Other" TransferLine="Yes" Category="TX">
				<xsl:attribute name="Note">
					<xsl:text>Result from: </xsl:text>
					<xsl:if test="ProfileRates/TaxTierProfile[TaxType/@code='TL']">
						<xsl:text>Total Loss Tax of </xsl:text><xsl:value-of select="round(ProfileRates/TaxTierProfile[TaxType/@code='TL']/TaxTier/TaxRate * 100) div 100"/><xsl:text>% applied to </xsl:text><xsl:value-of select="format-number(ProfileRates/TaxTierProfile[TaxType/@code='TL']/TaxTier/TaxThreshold, '#.00')"/><xsl:text>; </xsl:text>
					</xsl:if>
					<xsl:if test="ProfileRates/TaxTierProfile[TaxType/@code='O1']">
						<xsl:text>Other Tax of </xsl:text><xsl:value-of select="round(ProfileRates/TaxTierProfile[TaxType/@code='O1']/TaxTier/TaxRate *100) div 100"/><xsl:text>% applied to </xsl:text><xsl:value-of select="ProfileRates/TaxTierProfile[TaxType/@code='O1']/TaxTier/TaxThreshold"/><xsl:text>; </xsl:text>
					</xsl:if>
					<xsl:if test="ProfileRates/TaxTierProfile[TaxType/@code='O2']">
						<xsl:text>Other Tax of </xsl:text><xsl:value-of select="round(ProfileRates/TaxTierProfile[TaxType/@code='O2']/TaxTier/TaxRate *100) div 100"/><xsl:text>% applied to </xsl:text><xsl:value-of select="ProfileRates/TaxTierProfile[TaxType/@code='O2']/TaxTier/TaxThreshold"/><xsl:text>; </xsl:text>
					</xsl:if>
				</xsl:attribute>
				<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('OtherTaxAmount')"/></xsl:attribute>
				<xsl:attribute name="Percent">100</xsl:attribute>
				<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('OtherTaxAmount')"/></xsl:attribute>
				<xsl:attribute name="TaxBase">Other</xsl:attribute>
			</LineItem>
          </xsl:if>
		<LineItem Type="TaxTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TotalTaxAmount')"/></xsl:attribute>
		</LineItem>

		<LineItem Type="RepairTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('RepairTotalAmount')"/></xsl:attribute>
		</LineItem>

		<LineItem Type="AppearanceAllowance" TransferLine="Yes" Category="AJ" Note="">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('AppearanceAllowanceAmount')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('AppearanceAllowanceAmount')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Betterment" TransferLine="Yes" Category="AJ" Note="">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('BettermentAmount')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('BettermentAmount')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Deductible" TransferLine="Yes" Category="AJ" Note="">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('DeductibleAmount')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('DeductibleAmount')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="Other" TransferLine="Yes" Category="AJ" Note="">
			<xsl:attribute name="ExtendedAmount">0</xsl:attribute>
			<xsl:attribute name="UnitAmount">0</xsl:attribute>
		</LineItem>
		<LineItem Type="AdjustmentTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('TotalAdjustmentAmount')"/></xsl:attribute>
		</LineItem>


		<LineItem Type="NetTotal" TransferLine="Yes" Category="TT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('NetTotalAmount')"/></xsl:attribute>
		</LineItem>


		<LineItem Type="Variance" TransferLine="Yes" Category="VA">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('ZZVarianceAmount')"/></xsl:attribute>
		</LineItem>
		
		<LineItem Type="OEMParts" TransferLine="Yes" Category="OT">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType='PAN']/PartTax/Tax/TaxIndicator='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('DetailOEMParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('DetailOEMParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="LKQParts" TransferLine="Yes" Category="OT">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType='PAL']/PartTax/Tax/TaxIndicator='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('DetailLKQParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('DetailLKQParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="AFMKParts" TransferLine="Yes" Category="OT">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType='PAA']/PartTax/Tax/TaxIndicator='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('DetailAFMKParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('DetailAFMKParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="RemanParts" TransferLine="Yes" Category="OT">
			<xsl:attribute name="Taxable">
				<xsl:choose>
                    	<xsl:when test="ProfileRates/PartProfile[PartType='PAM']/PartTax/Tax/TaxIndicator='Y'">Yes</xsl:when>
                		<xsl:otherwise>No</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('DetailRemanParts')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('DetailRemanParts')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="RepairLabor" TransferLine="Yes" Category="OT">
			<xsl:attribute name="Taxable">No</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('RepairAmountTotal')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('RepairAmountTotal')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="ReplaceLabor" TransferLine="Yes" Category="OT">
			<xsl:attribute name="Taxable">No</xsl:attribute>
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('ReplaceAmountTotal')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('ReplaceAmountTotal')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="2 Wheel Alignment" TransferLine="No" Category="OT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('Detail2WAlign')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('Detail2WAlign')"/></xsl:attribute>
		</LineItem>
		<LineItem Type="4 Wheel Alignment" TransferLine="No" Category="OT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('Detail4WAlign')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('Detail4WAlign')"/></xsl:attribute>
		</LineItem>
		
		<LineItem Type="Clear Coat" TransferLine="No" Category="OT">
			<xsl:attribute name="Hours">
				<xsl:value-of select="sum(//DetailLine[cu:isClearCoat(string(LineDescription)) = true()]/Labor/ActualLaborHours)"/>
            </xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//DetailLine[cu:isClearCoat(string(LineDescription)) = true()]/Labor/LaborAmt)) +                                        cu:Number(sum(//DetailLine[cu:isClearCoat(string(LineDescription)) = true()]/Part/ActualPartPrice)))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Waste Management" TransferLine="No" Category="OT">
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//DetailLine[cu:isWaste(string(LineDescription)) = true()]/Labor/LaborAmt)) + cu:Number(sum(//DetailLine[cu:isWaste(string(LineDescription)) = true()]/Part/ActualPartPrice)))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Car Cover" TransferLine="No" Category="OT">
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//DetailLine[cu:isCarCover(string(LineDescription)) = true()]/Labor/LaborAmt)) + cu:Number(sum(//DetailLine[cu:isCarCover(string(LineDescription)) = true()]/Part/ActualPartPrice)))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Manual Line" TransferLine="No" Category="OT">
			<xsl:attribute name="Count">
				<xsl:value-of select="count(//DetailLine[cu:isManualLine(string(LineSource)) = true()])"/>
            </xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//DetailLine[cu:isManualLine(string(LineSource)) = true()]/Labor/LaborAmt)) + cu:Number(sum(//DetailLine[cu:isManualLine(string(LineSource)) = true()]/Part/ActualPartPrice)))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Painted Pinstripe" TransferLine="No" Category="OT">
			<xsl:attribute name="Hours">
				<xsl:value-of select="cu:Number(sum(//DetailLine[cu:isPaintedPinstripe(string(LineDescription)) = true()]/Labor/ActualLaborHours))"/>
            </xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//DetailLine[cu:isPaintedPinstripe(string(LineDescription)) = true()]/Labor/LaborAmt)) + cu:Number(sum(//DetailLine[cu:isPaintedPinstripe(string(LineDescription)) = true()]/Part/ActualPartPrice)))"/>
            </xsl:attribute>
			<xsl:attribute name="Panels">
				<xsl:value-of select="count(//DetailLine[cu:isPaintedPinstripe(string(LineDescription)) = true()])"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Taped Pinstripe" TransferLine="No" Category="OT">
			<xsl:attribute name="Hours">
				<xsl:value-of select="sum(//DetailLine[cu:isTapedPinstripe(string(LineDescription)) = true()]/Labor/ActualLaborHours)"/>
            </xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//DetailLine[cu:isTapedPinstripe(string(LineDescription)) = true()]/Labor/LaborAmt)) + cu:Number(sum(//DetailLine[cu:isTapedPinstripe(string(LineDescription)) = true()]/Part/ActualPartPrice)))"/>
            </xsl:attribute>
			<xsl:attribute name="Panels">
				<xsl:value-of select="count(//DetailLine[cu:isTapedPinstripe(string(LineDescription)) = true()])"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Freon" TransferLine="No" Category="OT">
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//DetailLine[cu:isFreon(string(LineDescription)) = true()]/Labor/LaborAmt)) + cu:Number(sum(//DetailLine[cu:isFreon(string(LineDescription)) = true()]/Part/ActualPartPrice)))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Towing" TransferLine="No" Category="OT">
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//DetailLine[cu:isTowing(string(LineDescription)) = true()]/Labor/LaborAmt)) + cu:Number(sum(//DetailLine[cu:isTowing(string(LineDescription)) = true()]/Part/ActualPartPrice)))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="Setup and Measure" TransferLine="No" Category="OT">
			<xsl:attribute name="Amount">
				<xsl:value-of select="cu:formatCurrency(cu:Number(sum(//DetailLine[cu:isSetupMeasure(string(LineDescription)) = true()]/Labor/LaborAmt)) + cu:Number(sum(//DetailLine[cu:isSetupMeasure(string(LineDescription)) = true()]/Part/ActualPartPrice)))"/>
            </xsl:attribute>
        </LineItem>
		<LineItem Type="DeductiblesApplied" TransferLine="Yes" Category="OT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="cccxsl:getvar('DeductibleAmount')"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="cccxsl:getvar('DeductibleAmount')"/></xsl:attribute>
        </LineItem>			
		<LineItem Type="LimitEffect" TransferLine="Yes" Category="OT">
			<xsl:attribute name="ExtendedAmount"/>
			<xsl:attribute name="UnitAmount"/>
        </LineItem>			
		<LineItem Type="NetTotalEffect" TransferLine="Yes" Category="OT">
			<xsl:attribute name="ExtendedAmount"><xsl:value-of select="number(cccxsl:getvar('RepairTotalAmount')) - number(cccxsl:getvar('DeductibleAmount'))"/></xsl:attribute>
			<xsl:attribute name="UnitAmount"><xsl:value-of select="number(cccxsl:getvar('RepairTotalAmount')) - number(cccxsl:getvar('DeductibleAmount'))"/></xsl:attribute>
        </LineItem>			
	</Summary>
</xsl:template>


<!--- Compile Audit Information -->
<xsl:template match="Estimate0110" mode="Audit">
	<Audit>
		<AuditItem Name="VerifyDetailPartTaxTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="cccxsl:getvar('DetailPartTaxTotal') = cccxsl:getvar('TotalTaxParts')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="cccxsl:getvar('DetailPartTaxTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="cccxsl:getvar('TotalTaxParts')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyDetailPartNonTaxTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="cccxsl:getvar('DetailPartNonTaxTotal') = cccxsl:getvar('TotalNonTaxParts')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="cccxsl:getvar('DetailPartNonTaxTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="cccxsl:getvar('TotalNonTaxParts')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyDetailPartOtherTaxTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="cccxsl:getvar('DetailPartOtherTaxTotal') = cccxsl:getvar('TotalOtherTaxParts')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="cccxsl:getvar('DetailPartOtherTaxTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="cccxsl:getvar('TotalOtherTaxParts')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyDetailPartOtherNonTaxTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="cccxsl:getvar('DetailPartOtherNonTaxTotal') = cccxsl:getvar('TotalOtherNonTaxParts')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="cccxsl:getvar('DetailPartOtherNonTaxTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="cccxsl:getvar('TotalOtherNonTaxParts')"/></xsl:attribute>
		</AuditItem>

		<AuditItem Name="VerifyBodyLaborHours">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="cccxsl:getvar('DetailBodyLaborHours') = cccxsl:getvar('BodyLaborHours')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="cccxsl:getvar('DetailBodyLaborHours')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="cccxsl:getvar('BodyLaborHours')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyFrameLaborHours">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="cccxsl:getvar('DetailFrameLaborHours') = cccxsl:getvar('FrameLaborHours')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="cccxsl:getvar('DetailFrameLaborHours')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="cccxsl:getvar('FrameLaborHours')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyMechLaborHours">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="cccxsl:getvar('DetailMechLaborHours') = cccxsl:getvar('MechLaborHours')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="cccxsl:getvar('DetailMechLaborHours')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="cccxsl:getvar('MechLaborHours')"/></xsl:attribute>
		</AuditItem>
		<AuditItem Name="VerifyPaintLaborHours">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="cccxsl:getvar('DetailPaintLaborHours') = cccxsl:getvar('PaintLaborHours')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="cccxsl:getvar('DetailPaintLaborHours')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="cccxsl:getvar('PaintLaborHours')"/></xsl:attribute>
		</AuditItem>
		
		<AuditItem Name="VerifyDetailSubletTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="cccxsl:getvar('DetailSubletTotal') = cccxsl:getvar('SubletAmount')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="cccxsl:getvar('DetailSubletTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="cccxsl:getvar('SubletAmount')"/></xsl:attribute>
		</AuditItem>		

		<AuditItem Name="VerifyBettermentTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="cccxsl:getvar('DetailBettermentTotal') = cccxsl:getvar('BettermentAmount')">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="cccxsl:getvar('DetailBettermentTotal')"/></xsl:attribute>
			<xsl:attribute name="SummaryTotal"><xsl:value-of select="cccxsl:getvar('BettermentAmount')"/></xsl:attribute>
		</AuditItem>

		<AuditItem Name="VerifyNoDetailUnknownTotal">
			<xsl:attribute name="Passed">
				<xsl:choose>
                    	<xsl:when test="cccxsl:getvar('DetailUnknownTotal') = 0">Yes</xsl:when>
					<xsl:otherwise>No</xsl:otherwise>
                	</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="DetailTotal"><xsl:value-of select="cccxsl:getvar('DetailUnknownTotal')"/></xsl:attribute>
		</AuditItem>		
	</Audit> 
</xsl:template>

</xsl:stylesheet>
