<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:msxsl="urn:schemas-microsoft-com:xslt"
      xmlns:cu="urn:the-xml-files:xslt" >

<!-- The following functions are common to both APD and CCC Estimates (and any in future). -->

<msxsl:script language="JScript" implements-prefix="cu">
<![CDATA[
    function Number(dValue){
        var dRet = 0;
        if (isNaN(dValue) == false){
            dRet = dValue;
        }
        return dRet;
    }

    //round to 2 decimal places
    function formatCurrency(dValue){
        var dRet = 0;
        if (isNaN(dValue) == false && dValue > 0){
            dRet = Math.ceil(dValue * 100) / 100;
        }
        return dRet;
    }

    function isClearCoat(strDescription){
        var bRet = false;
        if (strDescription != ""){
            strDescription = strDescription.toLowerCase();
            if (strDescription.indexOf("clear coat") != -1 ||
                strDescription.indexOf("clearcoat") != -1 ||
                strDescription.indexOf("cl coat") != -1){
                bRet = true;
            }
        }
        return bRet;
    }

    function isWaste(strDescription){
        var bRet = false;
        if (strDescription != ""){
            strDescription = strDescription.toLowerCase();
            if (((strDescription.indexOf("haz") != -1) &&
                 ((strDescription.indexOf("waste") != -1) ||
                  (strDescription.indexOf("wste") != -1)  ||
                  (strDescription.indexOf("mat") != -1) )) ||
                (strDescription.indexOf("waste removal") != -1) ||
                (strDescription.indexOf("environmental") != -1) ||
                (strDescription.indexOf("epa") != -1) ||
                (strDescription.indexOf("e.p.a") != -1) ||
                (strDescription.indexOf("epc") != -1) ||
                (strDescription.indexOf("e.p.c") != -1)){
                bRet = true;
            }
        }
        return bRet;
    }

    function isCarCover(strDescription){
        var bRet = false;
        if (strDescription != ""){
            strDescription = strDescription.toLowerCase();
            if (strDescription.indexOf("car cover") != -1 ||
                strDescription.indexOf("cover car") != -1 ||
                strDescription.indexOf("masking") != -1 ||
                strDescription.indexOf("overspray") != -1){
                bRet = true;
            }
        }
        return bRet;
    }

    function isManualLine(strDescription){
        var bRet = false;
        if (strDescription != ""){
            strDescription = strDescription.toLowerCase();
            if (strDescription.indexOf("manual") != -1){
                bRet = true;
            }
        }
        return bRet;
    }

    function isPaintedPinstripe(strDescription){
        var bRet = false;
        if (strDescription != ""){
            strDescription = strDescription.toLowerCase();
            if (strDescription.indexOf("paint") != -1 &&
                (strDescription.indexOf("pinstripe") != -1 ||
                strDescription.indexOf("pin stripe") != -1)){
                bRet = true;
            }
        }
        return bRet;
    }

    function isTapedPinstripe(strDescription){
        var bRet = false;
        if (strDescription != ""){
            strDescription = strDescription.toLowerCase();
            if (strDescription.indexOf("tape") != -1 &&
                (strDescription.indexOf("pinstripe") != -1 ||
                strDescription.indexOf("pin stripe") != -1)){
                bRet = true;
            }
        }
        return bRet;
    }

    function isFreon(strDescription){
        var bRet = false;
        if (strDescription != ""){
            strDescription = strDescription.toLowerCase();
            if (strDescription.indexOf("freon") != -1 ||
                strDescription.indexOf("r134") != -1 ||
                strDescription.indexOf("r-134") != -1 ||
                strDescription.indexOf("r12") != -1 ||
                strDescription.indexOf("r-12") != -1){
                bRet = true;
            }
        }
        return bRet;
    }

    function isTowing(strDescription){
        var bRet = false;
        if (strDescription != ""){
            strDescription = strDescription.toLowerCase();
            if (strDescription.indexOf("tow") != -1 ||
                strDescription.indexOf("transport") != -1){
                bRet = true;
            }
        }
        return bRet;
    }

    function isSetupMeasure(strDescription){
        var bRet = false;
        if (strDescription != ""){
            strDescription = strDescription.toLowerCase();
            if (strDescription.indexOf("setup") != -1 ||
                strDescription.indexOf("set up") != -1 ||
                strDescription.indexOf("set-up") != -1){
                bRet = true;
            }
        }
        return bRet;
    }
]]>
</msxsl:script>

</xsl:stylesheet>
