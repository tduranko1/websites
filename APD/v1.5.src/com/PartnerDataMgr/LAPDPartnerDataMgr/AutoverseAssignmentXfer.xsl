<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:head="http://www.cccis.com/interfaces/iatpa/header" xmlns:com="http://www.cccis.com/interfaces/iatpa/commontypes">
  <xsl:import href="AutoverseUtils.xsl"/>
  <xsl:output method="xml" indent="no" encoding="UTF-8" omit-xml-declaration="yes"/>
  <!-- ================================================== -->
  <!-- Converts a Scene Genesis Assignment SGTransaction  -->
  <!-- Into an XML format compatible with                 -->
  <!-- LAPDECADAccessor.CWebAssignment.ApdLoadAssignment  -->
  <!-- ================================================== -->
  <xsl:template match="/">
    <xsl:element name="WebAssignment">
      <!-- For add new user params -->
      <xsl:attribute name="ApplicationCD">CCAV</xsl:attribute>
      <xsl:attribute name="ApplicationID">6</xsl:attribute>
      <!-- ================================================== -->
      <!-- CLAIM ELEMENT                                      -->
      <!-- ================================================== -->
      <!-- For uspFNOLLoadClaim -->
      <!--<xsl:element name="Test"><xsl:value-of select="//head:TransactionHeader/head:InsCoCode"/></xsl:element>      -->
      <xsl:element name="Claim">
        <xsl:attribute name="AssignmentAtSelectionFlag">0</xsl:attribute>
        <xsl:attribute name="AssignmentDescription">Autoverse Assignment</xsl:attribute>
        <xsl:attribute name="SourceApplicationCD">CCAV</xsl:attribute>
        <!--
             Lets pass the Insurance company name as the client code so when the com tries to create the adjuster
             it will use this insurance company name and find the default office id the new user will belong to.
         -->
        <xsl:attribute name="ClientOfficeID"><xsl:value-of select="//INS_CO_NM"/></xsl:attribute>
        <xsl:attribute name="SourceApplicationPassThruData">
          <xsl:value-of select="//SourcePassThruData"/>
        </xsl:attribute>
        <xsl:attribute name="NewClaimFlag">1</xsl:attribute>
        <xsl:attribute name="NoticeMethodID">5</xsl:attribute>
        <xsl:attribute name="FNOLUserID"/>
        <xsl:attribute name="AgentName">
          <xsl:value-of select="//AD1/AGT_CT_FN"/>
          <xsl:text/>
          <xsl:value-of select="//AD1/AGT_CT_LN"/>
        </xsl:attribute>
        <xsl:attribute name="AgentPhone">
          <xsl:value-of select="//AD1/AGT_PH1"/>
        </xsl:attribute>
        <xsl:attribute name="AgentExt">
          <xsl:value-of select="//AD1/AGT_PH1X"/>
        </xsl:attribute>
        <xsl:call-template name="CarrierRep"/>
        <xsl:call-template name="Caller"/>
        <xsl:choose>
          <xsl:when test="//LOSS_CAT='C'">
            <xsl:attribute name="CollisionDeductibleAmt">
              <xsl:value-of select="//DED_AMT"/>
            </xsl:attribute>
            <xsl:attribute name="ComprehensiveDeductibleAmt"/>
          </xsl:when>
          <xsl:when test="//LOSS_CAT='M'">
            <xsl:attribute name="CollisionDeductibleAmt"/>
            <xsl:attribute name="ComprehensiveDeductibleAmt">
              <xsl:value-of select="//DED_AMT"/>
            </xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="CollisionDeductibleAmt"/>
            <xsl:attribute name="ComprehensiveDeductibleAmt"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:call-template name="ContactPerson"/>
        <xsl:attribute name="CoverageClaimNumber">
          <xsl:value-of select="//AD1/CLM_NO"/>
        </xsl:attribute>
        <xsl:attribute name="DataSource">Autoverse Assignment</xsl:attribute>
        <xsl:attribute name="InsuranceCompanyID">
          <xsl:value-of select="//LynxInsCoID"/>
        </xsl:attribute>
        <xsl:call-template name="LossInfo"/>
        <xsl:attribute name="NoticeDate"/>
        <xsl:attribute name="PoliceDepartmentName"/>
        <xsl:attribute name="PolicyNumber">
          <xsl:value-of select="//AD1/POLICY_NO"/>
        </xsl:attribute>
        <xsl:attribute name="Remarks"/>
        <xsl:attribute name="RentalCoverageFlag"/>
        <xsl:attribute name="RentalDays"/>
        <xsl:attribute name="RentalDayAmount"/>
        <xsl:attribute name="RentalMaxAmount"/>
        <xsl:attribute name="RentalDaysAuthorized"/>
        <xsl:attribute name="RentalInstructions"/>
        <xsl:attribute name="RoadLocationID"/>
        <xsl:attribute name="RoadTypeID"/>
        <xsl:attribute name="TimeStarted"/>
        <xsl:attribute name="TimeFinished"/>
        <xsl:attribute name="TripPurpose"/>
        <xsl:attribute name="WeatherConditionID"/>
        <xsl:element name="Coverage">
          <xsl:attribute name="ClientCoverageTypeID"/>
          <xsl:attribute name="CoverageTypeCD">
            <xsl:choose>
              <xsl:when test="//AD1/LOSS_CAT='C' or //AD1/LOSS_CAT='O' or //AD1/LOSS_CAT='U'">COLL</xsl:when>
              <xsl:when test="//AD1/LOSS_CAT='M'">COMP</xsl:when>
              <xsl:when test="//AD1/LOSS_CAT='P'">LIAB</xsl:when>
              <xsl:when test="//AD1/LOSS_CAT='L'">LIAB</xsl:when>
              <xsl:otherwise/>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="DeductibleAmt">
            <xsl:value-of select="//DED_AMT"/>
          </xsl:attribute>
          <xsl:attribute name="LimitAmt"/>
          <xsl:attribute name="LimitDailyAmt"/>
          <xsl:attribute name="MaximumDays"/>
        </xsl:element>
        <xsl:element name="Vehicle">
          <xsl:attribute name="VehicleNumber">
            <xsl:choose>
              <!-- Third party -->
              <xsl:when test="//LOSS_CAT='P' or //LOSS_CAT='L'">2</xsl:when>
              <!-- First party -->
              <xsl:when test="//LOSS_CAT='C' or //LOSS_CAT='M' or //LOSS_CAT='O' or //LOSS_CAT='U'">1</xsl:when>
              <xsl:otherwise>1</xsl:otherwise>
          </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="AirBagDriverFront"/>
          <xsl:attribute name="AirBagDriverSide"/>
          <xsl:attribute name="AirBagHeadliner"/>
          <xsl:attribute name="AirBagPassengerFront"/>
          <xsl:attribute name="AirBagPassengerSide"/>
          <xsl:attribute name="AssignmentTypeID">
            <xsl:choose>
              <xsl:when test="//AD2/INSP_CODE='DRP'">4</xsl:when>
              <xsl:when test="//AD2/INSP_CODE='I'">6</xsl:when>
            </xsl:choose>
          </xsl:attribute>
          <xsl:call-template name="VehicleInfo"/>
          <xsl:call-template name="VehicleContact"/>
          <xsl:attribute name="CoverageProfileCD">
            <xsl:choose>
              <xsl:when test="//AD1/LOSS_CAT='C'">COLL</xsl:when>
              <xsl:when test="//AD1/LOSS_CAT='M'">COMP</xsl:when>
              <xsl:when test="//AD1/LOSS_CAT='P'">LIAB</xsl:when>
              <xsl:when test="//AD1/LOSS_CAT='L'">LIAB</xsl:when>
              <xsl:otherwise/>
            </xsl:choose>
          </xsl:attribute>
          <xsl:attribute name="Drivable">
            <xsl:choose>
              <xsl:when test="//ENV/RO_ID='Y'">1</xsl:when>
              <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:call-template name="ImpactLocations"/>
          <xsl:attribute name="ImpactSpeed"/>
          <xsl:call-template name="VehicleLocation"/>
          <xsl:attribute name="NADAId"/>
          <xsl:attribute name="PermissionToDrive"/>
          <xsl:attribute name="PostedSpeed"/>
          <xsl:attribute name="PriorDamage"/>
          <xsl:attribute name="Remarks">
            <xsl:value-of select="//DMG_MEMO"/>
            <xsl:if test="//V_MEMO!=''">
              <xsl:text>|</xsl:text>
              <xsl:value-of select="//V_MEMO"/>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="RentalDaysAuthorized"/>
          <xsl:attribute name="RentalInstructions"/>
          <xsl:attribute name="ShopLocationID"/>
          <xsl:attribute name="ShopRemarks">
            <xsl:value-of select="//INS_MEMO"/>
          </xsl:attribute>
          <xsl:call-template name="InvolvedElements"/>
        </xsl:element>
      </xsl:element>
      <xsl:element name="BeginInsert"/>
    </xsl:element>
  </xsl:template>
  <xsl:template name="InvolvedElements">
    <xsl:choose>
      <xsl:when test="//LOSS_CAT='P' or //LOSS_CAT='L'">
       <!-- Third Party Claims -->
        <xsl:call-template name="InvolvedInsured">
          <xsl:with-param name="vInvolvedTypeOwner">0</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="InvolvedOwner"/>
      </xsl:when>
      <!-- Added Loss Category codes O and U to be treated as first party per Jonathan's email. See checked in remarks/TrackIt WO MOC -->
      <xsl:when test="//LOSS_CAT='C' or //LOSS_CAT='M' or //LOSS_CAT='O' or //LOSS_CAT='U'">
       <!-- Claim is a first party claim (Collision or Comprehensive -->
        <xsl:choose>
          <xsl:when test="((//INSD_LN=//OWNR_LN) and (//INSD_FN=//OWNR_FN))">
            <xsl:call-template name="InvolvedInsured">
              <xsl:with-param name="vInvolvedTypeOwner">1</xsl:with-param>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="((//INSD_LN='') and (//INSD_FN=''))">
            <xsl:call-template name="InvolvedOwner">
              <xsl:with-param name="vInvolvedTypeInsured">1</xsl:with-param>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="InvolvedInsured">
              <xsl:with-param name="vInvolvedTypeOwner">0</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="InvolvedOwner">
              <xsl:with-param name="vInvolvedTypeInsured">0</xsl:with-param>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
    </xsl:choose>
    <xsl:if test="//CLMT_LN!='' or //CLMT_FN!=''">
      <xsl:call-template name="InvolvedClaimant"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="ImpactLocations">
    <xsl:attribute name="ImpactLocations">
      <xsl:call-template name="TranslateImpactLocation">
        <xsl:with-param name="ImpactLoc">
          <xsl:value-of select="number(//IMPACT_1)"/>
        </xsl:with-param>
      </xsl:call-template>
      <xsl:if test="//IMPACT_1!=//IMPACT_2">
        <xsl:if test="string-length(//IMPACT_2) &gt; 0">
          <xsl:text>,</xsl:text>
          <xsl:call-template name="ParseImpact">
            <xsl:with-param name="pointer">1</xsl:with-param>
            <xsl:with-param name="impactlocs">
              <xsl:value-of select="//IMPACT_2"/>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:if>
      </xsl:if>
    </xsl:attribute>
  </xsl:template>
  <xsl:template name="ParseImpact">
    <xsl:param name="pointer"/>
    <xsl:param name="impactlocs"/>
    <xsl:call-template name="TranslateImpactLocation">
      <xsl:with-param name="ImpactLoc">
        <xsl:value-of select="number(substring($impactlocs,$pointer,2))"/>
      </xsl:with-param>
    </xsl:call-template>
    <xsl:if test="string-length(//IMPACT_2)&gt;($pointer+2)">
      <xsl:text>,</xsl:text>
      <xsl:call-template name="ParseImpact">
        <xsl:with-param name="pointer">
          <xsl:value-of select="($pointer + 2)"/>
        </xsl:with-param>
        <xsl:with-param name="impactlocs">
          <xsl:value-of select="$impactlocs"/>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
