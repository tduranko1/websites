<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:user="http://mycompany.com/mynamespace">

  <xsl:output method="xml" indent="no" encoding="UTF-8" omit-xml-declaration="yes"/>

  <msxsl:script language="JScript" implements-prefix="user">

    function GetDate()
    {
        var date = new Date();
        var result = (date.getMonth() &lt; 9 ? "0" : "") + (date.getMonth() + 1);
        result += (date.getDate() &lt; 10 ? "0" : "") + (date.getDate());
        result += date.getFullYear();
        return result;
    }

    function GetTime()
    {
        var time = new Date();
        var result = (time.getHours() &lt; 10 ? "0" : "") + (time.getHours());
        result += (time.getMinutes() &lt; 10 ? "0" : "") + (time.getMinutes());
        result += (time.getSeconds() &lt; 10 ? "0" : "") + (time.getSeconds());
        return result;
    }

  </msxsl:script>

  <!-- SG Transaction Header -->
  <xsl:template match="/">

    <xsl:element name="SGTransaction">

      <xsl:attribute name="Source">LYNX</xsl:attribute>
      <xsl:attribute name="TransactionType">ShopList</xsl:attribute>
      <xsl:attribute name="Version">2.0.0</xsl:attribute>
      <xsl:attribute name="Originator"><xsl:value-of select="/ShopList/SGTransaction/@Originator"/></xsl:attribute>
      <xsl:attribute name="TransactionID">SL<xsl:value-of select="/ShopList/SGTransaction/@TransactionDate"/><xsl:value-of select="/ShopList/SGTransaction/@TransactionTime"/></xsl:attribute>
      <xsl:attribute name="TransactionDate"><xsl:value-of select="user:GetDate()"/></xsl:attribute>
      <xsl:attribute name="TransactionTime"><xsl:value-of select="user:GetTime()"/></xsl:attribute>
      <xsl:attribute name="AssignmentID"></xsl:attribute>
      <xsl:attribute name="LynxID"></xsl:attribute>
      <xsl:attribute name="VehicleID"></xsl:attribute>
      <xsl:attribute name="TaskID"></xsl:attribute>

      <xsl:element name="TransactionData">
        <xsl:element name="ShopList">
          <xsl:attribute name="ListForSearch"><xsl:value-of select="/ShopList/SGTransaction/@TransactionID"/></xsl:attribute>
          <xsl:apply-templates select="/ShopList/Root/Shop"/>
        </xsl:element>
      </xsl:element>

    </xsl:element>

  </xsl:template>

  <!-- Shops in the Shop List -->
  <xsl:template match="Shop">

    <xsl:element name="Shop">

      <xsl:attribute name="ShopID"><xsl:value-of select="@ShopLocationID"/></xsl:attribute>

      <xsl:element name="Distance"><xsl:value-of select="@Distance"/></xsl:element>
      <xsl:element name="ShopName"><xsl:value-of select="@Name"/></xsl:element>

      <xsl:element name="Address">
        <xsl:element name="Addr1"><xsl:value-of select="@Address1"/></xsl:element>
        <xsl:element name="Addr2"><xsl:value-of select="@Address2"/></xsl:element>
        <xsl:element name="City"><xsl:value-of select="@AddressCity"/></xsl:element>
        <xsl:element name="State"><xsl:value-of select="@AddressState"/></xsl:element>
        <xsl:element name="Zip"><xsl:value-of select="@AddressZip"/></xsl:element>
        <xsl:element name="Country">USA</xsl:element>
      </xsl:element>

      <xsl:element name="Telecom">
        <xsl:element name="AreaCode"><xsl:value-of select="@PhoneAreaCode"/></xsl:element>
        <xsl:element name="TelecomNum"><xsl:value-of select="@PhoneExchangeNumber"/><xsl:value-of select="@PhoneUnitNumber"/></xsl:element>
        <xsl:element name="Extn"><xsl:value-of select="@PhoneExtensionNumber"/></xsl:element>
      </xsl:element>

    </xsl:element>

  </xsl:template>

</xsl:stylesheet>

