<?xml version="1.0" encoding="UTF-8"?>

<!-- GrangeAssignmentValidate.xsl

    This XSL is used to validate XML data passed from a Grange.

    We this XSL to replace hardcoded compiled VB COM components.

    If the XML validates successfully then the result will be a blank
string.

    If there are errors then the return string takes the form of an argument
        passable to SiteUtilities.CEvents.HandleMultiEvents.  This means
that
        the resulting string must take the form of a '|' delimited list:

    C#|error message[|...]
    C = 'I','E' or 'B' for 'internal', 'external' or 'both'
    # = '1','2',or '4' for 'error', 'warning' or 'information'
-->

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!-- Turn off that annoying <?xml ... ?> tag at the top of the output
-->
    <xsl:output method="text" omit-xml-declaration="yes"/>

    <xsl:template match="//TransactionHeader0100 ">

		<xsl:if test="ActionCode != 'New' and ActionCode != 'Cancel'">
			<xsl:text>|B1Invalid Action Code: '</xsl:text><xsl:value-of select="ActionCode"/><xsl:text>'.  Valid Action Codes are 'New' and 'Cancel'.</xsl:text>
		</xsl:if>

		<xsl:apply-templates select="Assignment0100"/>
    </xsl:template>

    <xsl:template match="Assignment0100">
	   
	   <xsl:if test="0=string-length(Claim/ClaimNumber)">
          <xsl:text>|B1|No Claim Number was passed.</xsl:text>
        </xsl:if>

	   <xsl:call-template name="CheckState">
          <xsl:with-param name="Name">LossState</xsl:with-param>
          <xsl:with-param name="State"><xsl:value-of select="Claim/LossState"/></xsl:with-param>
        </xsl:call-template>
  		
				
        <xsl:if test="false=contains('CMLPUO', Claim/LossCategoryCode/@code)">
          <xsl:text>|B1|LossCategoryCode must contain one of the following: C, M, L, P, U, O.</xsl:text>
        </xsl:if>

	   <xsl:if test="0=string-length(AssignedTo/AssignedToCompany)">
          <xsl:text>|B1|No Assigned To Company was passed.</xsl:text>
        </xsl:if>

	   <xsl:if test="0=string-length(//Vehicle/VehicleDrivable)">
          <xsl:text>|B1|No Drivable indicator was passed.</xsl:text>
        </xsl:if>

        <xsl:if test="string-length(Policy/RentalAmt) &gt; 0">		   
		   <xsl:call-template name="CheckNumber">
	          <xsl:with-param name="Name">RentalAmt</xsl:with-param>
	          <xsl:with-param name="CheckValue"><xsl:value-of select="Policy/RentalAmt"/></xsl:with-param>
	        </xsl:call-template>	
 	   </xsl:if>

        <xsl:if test="string-length(Policy/RentalDayAmt) &gt; 0">
		   <xsl:call-template name="CheckNumber">
	          <xsl:with-param name="Name">RentalDayAmt</xsl:with-param>
	          <xsl:with-param name="CheckValue"><xsl:value-of select="Policy/RentalDayAmt"/></xsl:with-param>
	        </xsl:call-template>
	  </xsl:if>
		 
	  <xsl:for-each select="//Vehicle/ImpactPoint">
		<xsl:variable name="ImpactPoint" select="./ImpactAreaCode/@code"/>
		
		<xsl:if test="count(../ImpactPoint/ImpactAreaCode[@code=$ImpactPoint]) &gt; 1">
			<xsl:text>|B1|A vehicle in this claim has duplicate impact point area codes: </xsl:text><xsl:value-of select="$ImpactPoint"/>
		</xsl:if>
	  </xsl:for-each>
	
	  <xsl:if test="count(//Party/PartyRoleType[@code='VI' or @code='IN']) &gt; 1">
			<xsl:text>|B1|More than one insured ('VI', 'IN') party was specified.  Claim data must include exactly 1 insured. </xsl:text>
	  </xsl:if>
	
	 <xsl:if test="count(//Party/PartyRoleType[@code='VI' or @code='IN']) = 0">
	    <xsl:text>|B1|No insured ('VI', 'IN') party was specified.  Claim data must include exactly 1 insured. </xsl:text>
	 </xsl:if>

	  
	<!--	
	  <xsl:variable name="ImpactPoints">
          <xsl:for-each select="Vehicle/ImpactPoint/ImpactAreaCode">
            <xsl:value-of select="@code"/>
          </xsl:for-each>
        </xsl:variable>

        <xsl:if test="$ImpactPoints=''">
          <xsl:text>|B1|Vehicle must have at least one Impact Point with a code.</xsl:text>
        </xsl:if>		
	-->
	
       <xsl:if test="string-length(//Vehicle/Vin)>17">
          <xsl:text>|B1|Vehicle VIN can not be longer than 17 characters.</xsl:text>
       </xsl:if>

       <xsl:if test="string-length(//Vehicle/ExternalColor)>20">
          <xsl:text>|B1|Vehicle color can not be longer than 20 characters.</xsl:text>
       </xsl:if>		

<!--
        <xsl:if test="8!=string-length(Assignment/AssignmentDate/DateTime/@Date)">
          <xsl:text>|B1|Assignment date must have a valid date.</xsl:text>
        </xsl:if>

        <xsl:if test="8=string-length(Assignment/AssignmentDate/DateTime/@Date)">
          <xsl:variable name="AssignmentMonth" select="number(substring(Assignment/AssignmentDate/DateTime/@Date, 1, 2))"/>
          <xsl:variable name="AssignmentDay" select="number(substring(Assignment/AssignmentDate/DateTime/@Date, 3, 2))"/>
          <xsl:variable name="AssignmentYear" select="number(substring(Assignment/AssignmentDate/DateTime/@Date, 5, 4))"/>

          <xsl:if test="not($AssignmentMonth &gt; 0 and $AssignmentMonth &lt; 13) or not($AssignmentDay &gt; 0 and $AssignmentDay &lt; 32) or not($AssignmentYear &gt; 0)">
               <xsl:text>|B1|Assignment date must have a valid date.</xsl:text>
          </xsl:if>
        </xsl:if>

        <xsl:if test="6!=string-length(Assignment/AssignmentDate/DateTime/@Time)">
          <xsl:text>|B1|Assignment time must have a valid time.</xsl:text>
        </xsl:if>

        <xsl:if test="6=string-length(Assignment/AssignmentDate/DateTime/@Time)">
          <xsl:variable name="AssignmentHour" select="number(substring(Assignment/AssignmentDate/DateTime/@Time, 1, 2))"/>
          <xsl:variable name="AssignmentMinute" select="number(substring(Assignment/AssignmentDate/DateTime/@Time, 3, 2))"/>
          <xsl:variable name="AssignmentSecond" select="number(substring(Assignment/AssignmentDate/DateTime/@Time, 5, 2))"/>
  
          <xsl:if test="not($AssignmentHour >= 0 and $AssignmentHour &lt; 24) or not($AssignmentMinute >= 0 and $AssignmentMinute &lt; 60) or not($AssignmentSecond >= 0 and $AssignmentSecond &lt;  0)"> 
               <xsl:text>|B1|Assignment time must have a valid time.</xsl:text>
          </xsl:if>
        </xsl:if>

        <xsl:if test="( (Assignment/AssignmentCode!='New') and (Assignment/AssignmentCode!='Cancel') )">
          <xsl:text>|B1|Assignment code must be 'New' or 'Cancel'.</xsl:text>
        </xsl:if>

        <xsl:if test="(0=string-length(Assignment/AssignedBy/ContactPerson/Person/@FirstName )) and (0=string-length(Assignment/AssignedBy/ContactPerson/Person/@LastName))">
          <xsl:text>|B1|Assigned By Contact Person must have a first and/or last name.</xsl:text>
        </xsl:if>

        <xsl:if test="(0=string-length(CarrierRep/ContactPerson/Person/@FirstName)) and (0=string-length(CarrierRep/ContactPerson/Person/@LastName))">
          <xsl:text>|B1|Carrier Rep must have a first and/or last name.</xsl:text>
        </xsl:if>

        <xsl:if test="3>string-length(CarrierRep/ContactPerson/ContactEmail)">
          <xsl:text>|B1|Carrier Rep must have an email address.</xsl:text>
        </xsl:if>

        <xsl:if test="0 = string-length(CarrierRep/GrangeUniqueId)">
          <xsl:text>|B1|Carrier Rep must have a Grange Unique Id.</xsl:text>
        </xsl:if>

       <xsl:if test="8!=string-length(Loss/LossDateTime/DateTime/@Date)">
          <xsl:text>|B1|Loss date must have a valid date.</xsl:text>
        </xsl:if>

        <xsl:if test="8=string-length(Loss/LossDateTime/DateTime/@Date)">
          <xsl:variable name="LossMonth" select="number(substring(Loss/LossDateTime/DateTime/@Date, 1, 2))"/>
          <xsl:variable name="LossDay" select="number(substring(Loss/LossDateTime/DateTime/@Date, 3, 2))"/>
          <xsl:variable name="LossYear" select="number(substring(Loss/LossDateTime/DateTime/@Date, 5, 4))"/>

          <xsl:if test="not($LossMonth &gt; 0 and $LossMonth &lt; 13) or not($LossDay &gt; 0 and $LossDay &lt; 32) or not($LossYear &gt; 0)">
            <xsl:text>|B1|Loss date must be a valid date.</xsl:text>
          </xsl:if>
        </xsl:if>

        <xsl:if test="0 &lt; string-length(Loss/LossDateTime/DateTime/@Time)">
          <xsl:variable name="LossHour"select="number(substring(Loss/LossDateTime/DateTime/@Time, 1, 2))"/>
          <xsl:variable name="LossMinute" select="number(substring(Loss/LossDateTime/DateTime/@Time, 3, 2))"/>
          <xsl:variable name="LossSecond" select="number(substring(Loss/LossDateTime/DateTime/@Time, 5, 2))"/>

          <xsl:if test="not($LossHour >= 0 and $LossHour &lt; 24) or not($LossMinute >= 0 and $LossMinute &lt; 60) or  not($LossSecond >= 0 and $LossSecond &lt; 60)">
            <xsl:text>|B1|Loss time must be a valid time.</xsl:text>
          </xsl:if>
        </xsl:if>

        <xsl:if test="string-length(Loss/LossDescription)>1000">
          <xsl:text>|B1|Loss Description can not be longer than 1000 characters.</xsl:text>
        </xsl:if>

        <xsl:call-template name="CheckState">
          <xsl:with-param name="Name">Loss Address State</xsl:with-param>
          <xsl:with-param name="State"><xsl:value-of select="Loss/Address/State"/></xsl:with-param>
        </xsl:call-template>

        <xsl:if test="( (Vehicle/CoverageProfileCD!='C') and (Vehicle/CoverageProfileCD!='M') and (Vehicle/CoverageProfileCD!='L') and (Vehicle/CoverageProfileCD!='O') )">
          <xsl:text>|B1|Vehicle Coverage Profile Code must be 'C', 'M', 'L' or 'O'.</xsl:text>
        </xsl:if>

        <xsl:if test="((Vehicle/CoverageProfileCD='C') and (0=string-length(Policy/CollisionDeductibleAmt))) or ((Vehicle/CoverageProfileCD='M') and (0=string-length(Policy/ComprehensiveDeductibleAmt)))">
          <xsl:text>|B1|Must have at Policy Deductible Amount.</xsl:text>
        </xsl:if>

        <xsl:if test="(0=string-length(Insured/BusinessName)) and (0=string-length(Insured/ContactPerson/Person/@FirstName)) and (0=string-length(Insured/ContactPerson/Person/@LastName))">
          <xsl:text>|B1|Insured must have a person name or business name.</xsl:text>
        </xsl:if>

        <xsl:if test="(0=string-length(Owner/BusinessName)) and (0=string-length(Owner/ContactPerson/Person/@FirstName)) and (0=string-length(Owner/ContactPerson/Person/@LastName))">
          <xsl:text>|B1|Owner must have a person name or business name.</xsl:text>
        </xsl:if>

        <xsl:if test="(0=string-length(Vehicle/ExposureCD)) or ((Vehicle/ExposureCD!='1') and (Vehicle/ExposureCD!='3') and (Vehicle/ExposureCD!='N') )">
          <xsl:text>|B1|Vehicle Exposure Code must be '1', '3' or 'N'.</xsl:text>
        </xsl:if>

        <xsl:if test="(0=string-length(Vehicle/ContactPerson/Person/@FirstName)) and (0=string-length(Vehicle/ContactPerson/Person/@LastName))">
          <xsl:text>|B1|Vehicle must have a contact person name.</xsl:text>
        </xsl:if>

        <xsl:if test="string-length(Vehicle/ContactPerson/Person/@FirstName) &gt; 50">
          <xsl:text>|B1|Vehicle contact first name can not be longer than 50 characters.</xsl:text> 
        </xsl:if>

        <xsl:if test="string-length(Vehicle/ContactPerson/Person/@LastName) &gt; 50">
          <xsl:text>|B1|Vehicle contact last name can not be longer than 50 characters.</xsl:text>
        </xsl:if>

        <xsl:variable name="VehPhone">
          <xsl:for-each select="Vehicle/ContactPerson/ContactTelephone/Telecom">
            <xsl:if test="(string-length(AreaCode)=3) and (string-length(TelecomNum)=7) and boolean(number(AreaCode)) and boolean(number(TelecomNum))">1</xsl:if>
          </xsl:for-each>
        </xsl:variable>

        <xsl:if test="0=string-length($VehPhone)">
          <xsl:text>|B1|Vehicle Contact must have at least one complete and valid phone number.</xsl:text>
        </xsl:if>

        <xsl:if test="0=string-length(Vehicle/Make)">
          <xsl:text>|B1|Vehicle Make must have a value.</xsl:text>
        </xsl:if>

        <xsl:if test="0=string-length(Vehicle/Model)">
          <xsl:text>|B1|Vehicle Model must have a value.</xsl:text>
        </xsl:if>

        <xsl:if test="4!=string-length(Vehicle/VehicleYear) or not(boolean(number(Vehicle/VehicleYear)))">
          <xsl:text>|B1|Vehicle Year must be 4 digit numeric and non-zero.</xsl:text>
        </xsl:if>

        <xsl:if test="( ('Y'!=string(Vehicle/Driveable)) and ('N'!=string(Vehicle/Driveable)) )">
          <xsl:text>|B1|Vehicle must have a 'Y' or 'N' Drivable element.</xsl:text>
        </xsl:if>

        <xsl:if test="(string-length(Vehicle/LicensePlateNumber)=0) and (string-length(Vehicle/Vin)=0)">
          <xsl:text>|B1|Vehicle must have EITHER a license plate number OR a VIN (or partial VIN).</xsl:text>
        </xsl:if>       

        <xsl:if test="string-length(Vehicle/LicensePlateState)!=0 and string-length(Vehicle/LicensePlateState)!=2">
          <xsl:text>|B1|Vehicle license plate state must be two characters.</xsl:text>
        </xsl:if>

        <xsl:if test="(0=string-length(Vehicle/ShopLocationID)) or not(boolean(number(Vehicle/ShopLocationID)))">
          <xsl:text>|B1|Vehicle Shop Location ID must have a numeric value.</xsl:text>
        </xsl:if>
     
        
      -->
    </xsl:template>

    <!-- Utility template for checking for valid states -->
    <xsl:template name="CheckState">
      <xsl:param name="State"/>
      <xsl:param name="Name"/>

      <xsl:choose>
        <xsl:when test="$State='AK'"/>
        <xsl:when test="$State='AL'"/>
        <xsl:when test="$State='AR'"/>
        <xsl:when test="$State='AZ'"/>
        <xsl:when test="$State='CA'"/>
        <xsl:when test="$State='CO'"/>
        <xsl:when test="$State='CT'"/>
        <xsl:when test="$State='DC'"/>
        <xsl:when test="$State='DE'"/>
        <xsl:when test="$State='FL'"/>
        <xsl:when test="$State='GA'"/>
        <xsl:when test="$State='HI'"/>
        <xsl:when test="$State='IA'"/>
        <xsl:when test="$State='ID'"/>
        <xsl:when test="$State='IL'"/>
        <xsl:when test="$State='IN'"/>
        <xsl:when test="$State='KS'"/>
        <xsl:when test="$State='KY'"/>
        <xsl:when test="$State='LA'"/>
        <xsl:when test="$State='MA'"/>
        <xsl:when test="$State='MD'"/>
        <xsl:when test="$State='ME'"/>
        <xsl:when test="$State='MI'"/>
        <xsl:when test="$State='MN'"/>
        <xsl:when test="$State='MO'"/>
        <xsl:when test="$State='MS'"/>
        <xsl:when test="$State='MT'"/>
        <xsl:when test="$State='NC'"/>
        <xsl:when test="$State='ND'"/>
        <xsl:when test="$State='NE'"/>
        <xsl:when test="$State='NH'"/>
        <xsl:when test="$State='NJ'"/>
        <xsl:when test="$State='NM'"/>
        <xsl:when test="$State='NV'"/>
        <xsl:when test="$State='NY'"/>
        <xsl:when test="$State='OH'"/>
        <xsl:when test="$State='OK'"/>
        <xsl:when test="$State='OR'"/>
        <xsl:when test="$State='PA'"/>
        <xsl:when test="$State='RI'"/>
        <xsl:when test="$State='SC'"/>
        <xsl:when test="$State='SD'"/>
        <xsl:when test="$State='TN'"/>
        <xsl:when test="$State='TX'"/>
        <xsl:when test="$State='UT'"/>
        <xsl:when test="$State='VA'"/>
        <xsl:when test="$State='VT'"/>
        <xsl:when test="$State='WA'"/>
        <xsl:when test="$State='WI'"/>
        <xsl:when test="$State='WV'"/>
        <xsl:when test="$State='WY'"/>
        <xsl:otherwise>
          <xsl:text>|B1|</xsl:text>
          <xsl:value-of select="$Name"/>
          <xsl:text> contained an invalid state code: '</xsl:text>
          <xsl:value-of select="$State"/>
          <xsl:text>'.</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:template>

	<xsl:template name="CheckNumber">
	     <xsl:param name="Name"/>
		<xsl:param name="CheckValue"/>
		<xsl:param name="pos" select="1"/>
		<xsl:param name="dots" select="0"/>
		
		<xsl:variable name="Message">		
			<xsl:text>|B1|</xsl:text>
          	<xsl:value-of select="$Name"/>
          	<xsl:text> is not a valid numeric value: '</xsl:text>
          	<xsl:value-of select="$CheckValue"/>
          	<xsl:text>'.</xsl:text>
		</xsl:variable>
		
		<xsl:variable name="valid">		
			<xsl:choose>
		        	<xsl:when test="contains('0123456789.', substring($CheckValue, $pos, 1)) = false">false</xsl:when>
				<xsl:when test="$dots &gt; 1">false</xsl:when>
				<xsl:otherwise>true</xsl:otherwise>
		     </xsl:choose>		
		</xsl:variable>	
		
		<xsl:if test="$valid = 'false'"><xsl:value-of select="$Message"/></xsl:if>		
		
		<xsl:if test="$pos &lt;= string-length($CheckValue) and $valid = 'true'">
			<xsl:call-template name="CheckNumber">
			     <xsl:with-param name="Name" select="$Name"/>
				<xsl:with-param name="CheckValue" select="$CheckValue"/>
				<xsl:with-param name="pos" select="$pos + 1"/>
				<xsl:with-param name="dots">
					<xsl:choose>
			            	<xsl:when test="substring($CheckValue, $pos, 1) = '.'"><xsl:value-of select="$dots+1"/></xsl:when>
			               <xsl:otherwise><xsl:value-of select="$dots"/></xsl:otherwise>
			          </xsl:choose>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>	
	</xsl:template>	

</xsl:stylesheet>

