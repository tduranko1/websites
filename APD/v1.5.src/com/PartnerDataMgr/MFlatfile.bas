Attribute VB_Name = "MFlatfile"
'********************************************************************************
'* Component PartnerDataMgr: Module MFlatfile
'*
'* Flatfile utility functions.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "MFlatfile."

'********************************************************************************
'*
'* Syntax:      ConvertTextToXML "PrintImage", 66, g_objEvents
'*
'* Parameters:  strFlatfile - The flatfile data to convert.
'*              strRootNode - The node name for the root node.
'*              intLinesPerPage - lines per page in the flat file.
'*              objEvents - the SiteUtilities.CEvents class from the parent.
'*
'* Purpose:     Chops up a flatfile by pages and saves the pages as XML.
'*
'* Returns:     Nothing.
'*
'********************************************************************************
Public Sub ConvertFlatfileToXML( _
    ByVal enmDocType As EDocumentType, _
    ByVal strFlatfile As String, _
    ByVal strRootNode As String, _
    ByVal intLinesPerPage As Integer, _
    ByVal strDocumentType As String, _
    ByVal strFileName As String)

          Const PROC_NAME As String = MODULE_NAME & "ConvertFlatfileToXML: "

          'Dim lngPageStart As Long
          'Dim lngPageEnd As Long
          'Dim blnDone As Boolean
          Dim strXML As String
          'Dim i As Integer
          'Dim intPageNum As Integer
          Dim strPath As String
          'Dim strSearchFor As String
          'Dim strPage() As String
          'Dim strLines() As String
          'Dim intLineCounter As Integer
          'Dim intLine As Integer
          Dim strTemp As String
          'Dim iPageNum As Integer
          'Dim iLine As Integer
          'Dim iPageLen As Integer
          'Dim strLine As String
          'Dim iMaxLines As Long
          Dim iLineLen As Integer
          Dim iPos As Long
          Dim iCurPagePos As Long
          Dim iCurLinePos As Long
          Dim strCurChar As String
          Dim strCurLine As String

          

10        On Error GoTo ErrorHandler

          'Add a trace message to note start of method.
20        If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Started" & vbNewLine

30        strPath = Replace((GetConfig("Document/RootDirectory") & strFileName), "\\", "\")

          'lngPageEnd = 1
          'intPageNum = 1
          
40        strTemp = Trim(strFlatfile)
          
          'iPageNum = 1
          'iLine = 1
50        iLineLen = 79
          'iMaxLines = 10000
60        iCurPagePos = 1
70        iCurLinePos = 1
80        strCurLine = ""
          
          'Add root document element.
90        strXML = "<" & strRootNode & ">" & vbCrLf
100       strXML = strXML & "<Page Num='" & iCurPagePos & "'><![CDATA[" & vbCrLf
          
110       For iPos = 1 To Len(strTemp)
120           strCurChar = Mid(strTemp, iPos, 1)
              
130           If Asc(strCurChar) >= 32 Then
140               strCurLine = strCurLine & strCurChar
150               If Len(strCurLine) >= iLineLen Then
160                   strXML = strXML & strCurLine & vbCrLf
170                   strCurLine = ""
180                   iCurLinePos = iCurLinePos + 1
190                   If iCurLinePos >= intLinesPerPage Then
200                       iCurPagePos = iCurPagePos + 1
210                       strXML = strXML & vbCrLf & "]]></Page><Page Num=""" & iCurPagePos & """><![CDATA[" & vbCrLf
220                       iCurLinePos = 0
230                   End If
240               End If
250           End If
260       Next
270       strXML = strXML & strCurLine & "]]></Page>" & vbCrLf
          
      '    While Len(strTemp) > 0 And iMaxLines > 0
      '        If iPageNum Mod 6 = 0 And iLine = iPageLen - 1 Then
      '            strLine = Mid(strTemp, 1, 53)
      '            strTemp = Mid(strTemp, 54)
      '        Else
      '            strLine = Mid(strTemp, 1, 79)
      '            strTemp = Mid(strTemp, 80)
      '        End If
      '
      '        strXML = strXML & strLine & vbCrLf
      '        iLine = iLine + 1
      '
      '        If iLine = iPageLen Then
      '            iPageNum = iPageNum + 1
      '            strXML = strXML & "]]></Page><Page Num='" & iPageNum & "'><![CDATA[" & vbCrLf
      '            iLine = 1
      '        End If
      '
      '        iMaxLines = iMaxLines - 1
      '    Wend
      '
      '    strXML = strXML & "]]></Page>"
          
      '    If g_blnDebugMode Then g_objEvents.Trace "", "strXml = """ & strXml & """" & vbCrLf
          
          
          'strFlatfile = Replace(strFlatfile, Chr(10), vbCrLf, , , vbBinaryCompare)

          'Add root document element.
      '    strXml = "<" & strRootNode & ">" & vbCrLf
          'check if the data contains a CRLF or just a LF
      '    If InStr(lngPageEnd + 1, strFlatfile, vbCrLf) > 0 Then
      '        'old method
      '        strSearchFor = vbCrLf
      '    Else
      '        'new method. This will happen when the data was extracted from an XML node.
      '        strSearchFor = vbLf
      '    End If
          
      '    'new method to split the lines into pages.
      '    strLines = Split(strFlatfile, strSearchFor, , vbBinaryCompare)
          
      '    ReDim strPage(intLinesPerPage)
      '    intLineCounter = 0
      '    intPageNum = 1
      '    For intLine = LBound(strLines) To UBound(strLines)
      '        strPage(intLineCounter) = strLines(intLine)
      '        If intLineCounter = (intLinesPerPage - 1) Or intLine = UBound(strLines) Then
      '            strXml = strXml & "<Page Num='" & intPageNum & "'><![CDATA[" _
      '                & Join(strPage, vbCrLf) _
      '                & "]]></Page>" & vbCrLf
      '
      '            ReDim strPage(intLinesPerPage)
      '            intPageNum = intPageNum + 1
      '            intLineCounter = -1
      '        End If
      '
      '        intLineCounter = intLineCounter + 1
      '    Next
          
      '    'Old method. Seems to be buggy when the text has square brackets and it was not creating all pages.
      '    ' Also concat string one line at a time is slower. VB6 known issue
      '    'Loop through all the pages in the document and break them out one at a time.
      '    Do Until blnDone
      '
      '        lngPageStart = lngPageEnd
      '
      '        'Loop through the specified lines per page.
      '        For i = 1 To intLinesPerPage
      '            lngPageEnd = InStr(lngPageEnd + 1, strFlatfile, strSearchFor)
      '
      '            If lngPageEnd = 0 Then lngPageEnd = InStr(lngPageEnd + 1, strFlatfile, vbLf)
      '
      '            If lngPageEnd <= 1 Then
      '                blnDone = True
      '                Exit For
      '            End If
      '        Next i
      '
      '        'Found a valid page?
      '        If lngPageEnd > lngPageStart Then
      '
      '            lngPageEnd = lngPageEnd + 1
      '
      '            strXml = strXml & "<Page Num='" & intPageNum & "'><![CDATA[" _
      '                & Mid(strFlatfile, lngPageStart, lngPageEnd - lngPageStart) _
      '                & "]]></Page>" & vbCrLf
      '
      '            intPageNum = intPageNum + 1
      '
      '        End If
      '
      '    Loop
      '
          'Close root document element.
280       strXML = strXML & "</" & strRootNode & ">" & vbCrLf
          
          'Now write the data out to disk.
290       WriteTextDataToFile strPath, strXML
          
          'Add another trace message to note exit?
300       If g_blnDebugMode Then g_objEvents.Trace "Return = " & MODULE_NAME & " - ", PROC_NAME & " finished!"

ErrorHandler:

          'Everything below will be sent out when an error occurs . . .
310       If Err.Number <> 0 Then
320           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
330       End If

End Sub

'********************************************************************************
'*
'* Syntax:      ConvertXMLToFlatfile "PrintImage", 66, g_objEvents
'*
'* Parameters:  strFlatfile - The flatfile data to convert.
'*              strRootNode - The node name for the root node.
'*              intLinesPerPage - lines per page in the flat file.
'*              objEvents - the SiteUtilities.CEvents class from the parent.
'*
'* Purpose:     Chops up a flatfile by pages and saves the pages as XML.
'*
'* Returns:     Nothing.
'*
'********************************************************************************
Public Function ConvertXMLToFlatfile(ByVal strXML As String) As String

          Const PROC_NAME As String = MODULE_NAME & "ConvertXMLToFlatfile: "

10        On Error GoTo ErrorHandler
          
          Dim strResult As String
          Dim lngStart As Long, lngEnd As Long
          
20        lngStart = 1
          Const strStart As String = "<![CDATA["
          Const strEnd As String = "]]>"
          
30        lngStart = InStr(1, strXML, strStart)
40        While lngStart <> 0
          
50            lngEnd = InStr(lngStart, strXML, strEnd)
60            lngStart = lngStart + Len(strStart)
              
70            strResult = strResult & Mid(strXML, lngStart, lngEnd - lngStart)
              
80            lngStart = InStr(lngEnd, strXML, strStart)
              
90        Wend

100       ConvertXMLToFlatfile = strResult

ErrorHandler:

110       If Err.Number <> 0 Then
120           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, "XML = " & strXML
130       End If

End Function



