VERSION 5.00
Object = "{E7BC34A0-BA86-11CF-84B1-CBC2DA68BF6C}#1.0#0"; "NTSVC.ocx"
Begin VB.Form FNetworkStatusCheck 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADP/SG Network Status Test Service"
   ClientHeight    =   4080
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6960
   Icon            =   "FNetworkStatusCheck.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4080
   ScaleWidth      =   6960
   StartUpPosition =   2  'CenterScreen
   Begin NTService.NTService NTService1 
      Left            =   4080
      Top             =   120
      _Version        =   65536
      _ExtentX        =   741
      _ExtentY        =   741
      _StockProps     =   0
      ServiceName     =   "APD-SG-NetworkStatusTest"
      StartMode       =   3
   End
   Begin VB.CommandButton TestNow 
      Caption         =   "Now"
      Height          =   375
      Left            =   1560
      TabIndex        =   4
      Top             =   120
      Width           =   615
   End
   Begin VB.CommandButton btnClearLog 
      Caption         =   "Clear Log"
      Height          =   375
      Left            =   5640
      TabIndex        =   3
      Top             =   120
      Width           =   1215
   End
   Begin VB.CommandButton StopTest 
      Caption         =   "Stop"
      Height          =   375
      Left            =   840
      TabIndex        =   2
      Top             =   120
      Width           =   615
   End
   Begin VB.CommandButton StartTest 
      Caption         =   "Start"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   615
   End
   Begin VB.TextBox txtStatus 
      Height          =   3375
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   600
      Width           =   6735
   End
   Begin VB.Timer TestTimer 
      Interval        =   500
      Left            =   3480
      Top             =   120
   End
End
Attribute VB_Name = "FNetworkStatusCheck"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "FNetworkStatusCheck."

Private Declare Function timeGetTime Lib "winmm.dll" () As Long

'Error codes added for WinInet errors.
Private Enum ErrorCodes
    eErrNoLogPath = vbObjectError + 1000
    eErrBadTimerInterval
End Enum

Private Sub Form_Load()
    Const PROC_NAME As String = MODULE_NAME & "Form_Load"
    
    On Error GoTo ErrHandler
    
    Dim strDisplayName As String
    strDisplayName = NTService1.DisplayName
    
    StatusMessage "Form Loading"
    
    Me.Visible = False
    
    If LCase(Command) = "-install" Then
    
        StatusMessage "-install switch detected."
    
        ' enable interaction with desktop
        NTService1.Interactive = True
        
        If NTService1.Install Then
            MsgBox strDisplayName & " installed successfully"
            StatusMessage "Installed successfully."
        Else
            MsgBox strDisplayName & " failed to install"
            StatusMessage "Failed to install."
        End If
        End
        
    ElseIf LCase(Command) = "-uninstall" Then
    
        StatusMessage "-uninstall switch detected."
        
        If NTService1.Uninstall Then
            MsgBox strDisplayName & " uninstalled successfully"
            StatusMessage "Uninstalled successfully."
        Else
            MsgBox strDisplayName & " failed to uninstall"
            StatusMessage "Failed to uninstall."
        End If
        End
        
    ElseIf LCase(Command) = "-debug" Then
    
        StatusMessage "-debug switch detected"
        
        NTService1.Debug = True
        Me.Visible = True
    
    ElseIf Command <> "" Then
    
        StatusMessage "Invalid command option."
    
        MsgBox "Invalid command option!" & vbCrLf & "try -install, -uninstall, or -debug."
        End
        
    End If
    
    StatusMessage "Loading configuration"
    
    'Set timer interval.  Normall you might pull this from the registry or wherever.
    TestTimer.Interval = 60 * 60 ' 1 hour in seconds.
    
    ' enable Pause/Continue. Must be set before StartService
    ' is called or in design mode
    StatusMessage "Enabling control mode"
    
    NTService1.ControlsAccepted = svcCtrlPauseContinue
    
    ' connect service to Windows NT services controller
    StatusMessage "Starting"
    
    NTService1.StartService
    
    StatusMessage "Exiting form load successfully."
    StatusMessage "-------------------------------"
    
    Exit Sub
ErrHandler:
    StatusMessage "Error: " & Err.Description
    'Call LogError(Err.Number, Err.Description, Err.Source, PROC_NAME, NTService1.Interactive, False)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    StatusMessage "-------------------------------"
    StatusMessage "Form Unloading."
    
End Sub

Private Sub StatusMessage(strMsg As String)
    If NTService1.Debug = True Then
        txtStatus.Text = txtStatus.Text + strMsg + vbCrLf
    End If
End Sub

Private Sub NTService1_Continue(Success As Boolean)
    Const PROC_NAME As String = MODULE_NAME & "NTService1_Continue"
    On Error GoTo ErrHandler
    
    StatusMessage "Running..."
    
    StartTest_Click
    
    Success = True
    Call NTService1.LogEvent(svcEventInformation, svcMessageInfo, "Service continued")
    
    Exit Sub
ErrHandler:
    StatusMessage "Error: " & Err.Description
    'Call LogError(Err.Number, Err.Description, Err.Source, PROC_NAME, NTService1.Interactive, False)
End Sub

Private Sub NTService1_Control(ByVal e As Long)
    Const PROC_NAME As String = MODULE_NAME & "NTService1_Control"
    On Error GoTo ErrHandler
    
    StatusMessage NTService1.DisplayName & " Control signal " & e

    Exit Sub
ErrHandler:
    StatusMessage "Error: " & Err.Description
    'Call LogError(Err.Number, Err.Description, Err.Source, PROC_NAME, NTService1.Interactive, False)
End Sub

Private Sub NTService1_Pause(Success As Boolean)
    Const PROC_NAME As String = MODULE_NAME & "NTService1_Pause"
    On Error GoTo ErrHandler
    
    StopTest_Click

    StatusMessage "Paused"
    Call NTService1.LogEvent(svcEventError, svcMessageError, "Service paused")
    Success = True
    
    Exit Sub
ErrHandler:
    StatusMessage "Error: " & Err.Description
    'Call LogError(Err.Number, Err.Description, Err.Source, PROC_NAME, NTService1.Interactive, False)
End Sub

Private Sub NTService1_Start(Success As Boolean)
    Const PROC_NAME As String = MODULE_NAME & "NTService1_Start"
    On Error GoTo ErrHandler
    
    StatusMessage "Running..."
    
    If NTService1.Debug Then
        StopTest_Click
    Else
        StartTest_Click
    End If

    Success = True
    
    Exit Sub
ErrHandler:
    StatusMessage "Error: " & Err.Description
    'Call LogError(Err.Number, Err.Description, Err.Source, PROC_NAME, NTService1.Interactive, False)
End Sub

Private Sub NTService1_Stop()
    Const PROC_NAME As String = MODULE_NAME & "NTService1_Stop"
    On Error GoTo ErrHandler
    
    StopTest_Click

    StatusMessage "Stopped"
    Unload Me
    
    Exit Sub
ErrHandler:
    StatusMessage "Error: " & Err.Description
    'Call LogError(Err.Number, Err.Description, Err.Source, PROC_NAME, NTService1.Interactive, False)
End Sub

Private Sub TestTimer_Timer()
    'Only fall through to InternalTestTimer every 1000 timer hits.
    'This allows us to extend the max timer interval.
    Static lngCount As Long
    lngCount = lngCount + 1
    If lngCount >= 1000 Then
        lngCount = 0
        InternalTestTimer
    End If
End Sub

Private Sub InternalTestTimer()
    'This Subroutine gets called by the Test Timer.
    Const PROC_NAME As String = MODULE_NAME & "TestTimer_Timer"
    On Error GoTo ErrHandler
    
    StatusMessage "------------------------------------"
    StatusMessage "TestTimer : " + Format(Now(), "hh:mm:ss")
    
    Dim objPartner As Object
    Set objPartner = CreateObject("LAPDPartnerDataMgr.CExecute")
    
    objPartner.SendHeartBeatRequest "SceneGenesis"
    
    Set objPartner = Nothing
    
    StatusMessage "Success!"

    Exit Sub
    
ErrHandler:

    Set objPartner = Nothing
    
    StatusMessage "Error: " & Err.Description
    'Call LogError(Err.Number, Err.Description, Err.Source, PROC_NAME, NTService1.Interactive, False)
    
End Sub

Private Sub TestNow_Click()
    InternalTestTimer
End Sub

Private Sub StartTest_Click()
    TestTimer.Enabled = True
    StatusMessage "Test Timer Enabled"
End Sub

Private Sub StopTest_Click()
    TestTimer.Enabled = False
    StatusMessage "Test Timer Disabled"
End Sub

Private Sub btnClearLog_Click()
    txtStatus.Text = ""
End Sub

Private Sub LogErrorToEvent(lngNumber As Long, strSrc As String, strDesc As String, strProc As String)
    StatusMessage "Error #" & lngNumber & vbCrLf & "     in " & strProc & vbCrLf & "     in call to " & strSrc & vbCrLf & "     " & strDesc
    'Call LogError(lngNumber, strDesc, strSrc, strProc, NTService1.Interactive, False)
End Sub
