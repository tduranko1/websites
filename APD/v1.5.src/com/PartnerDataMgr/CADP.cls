VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CADP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'********************************************************************************
'* Component LAPDPartnerDataMgr : Class CAPD
'*
'* Processing methods specific to APD
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CAPD."

Implements CPartner

'Error codes private to this module.
Private Enum APD_Error_Codes
    eNull = CAPD_CLS_ErrorCodes
End Enum

'These are error codes that will come
'back in negative acknowledgements from APD.
Private Enum CAPD_NACK_ErrorCodes
    eNullNack
End Enum

Private mobjPartner As New CPartner
Private mobjPassedDocument As MSXML2.DOMDocument40

Private Function CPartner_InsertPrintImageIntoAPDV2(ByVal strXML As String, ByVal lngPartnerTransID As Long) As Long
          
10        CPartner_InsertPrintImageIntoAPDV2 = _
              mobjPartner.InsertPrintImageIntoAPDV2(strXML, lngPartnerTransID)

End Function

Public Function CPartner_PartnerDatabaseTransferV2(ByVal blnSendReceipt As Boolean) As Boolean
          Const PROC_NAME As String = MODULE_NAME & "PartnerDatabaseTransferV2: "

10        On Error GoTo ErrorHandler
          
20        Select Case mobjPartner.DocTypeEnum
          
              Case eDigitalImage

30                mobjPartner.StoreDocumentInPartnerDb_v2
                  
40            Case ePrintImage

50                mobjPartner.ExtractSequenceNumber_v2 "//EstimateIdentifier"
60                mobjPartner.StorePrintImageInPartnerDB_v2
                  
70            Case eEstimateData

80                mobjPartner.ExtractSequenceNumber_v2 "//TRANS_TYPE", "//SUPP_NO"
90                mobjPartner.StoreEstimateDataInPartnerDB_v2
100               If blnSendReceipt Then mobjPartner.SendAReceipt_v2 "ADPReceipt.XML"
                  
110           Case Else

120               mobjPartner.StoreDocumentInPartnerDb_v2

130       End Select
          
          'An ADP transaction was missing CLIENT_PASSTHRU_DATA.  To ahead and store
          'it in udb_partner and notify development to correct the XML and resubmit it.
140       If mobjPartner.LynxID = "UNK" And mobjPartner.VehicleNumber = "UNK" And mobjPartner.AssignmentID = "0" Then

150           g_objEvents.HandleEvent eLynxIdUnknown, PROC_NAME, _
                  mobjPartner.PartnerTransID(), "", "", False

160           CPartner_PartnerDatabaseTransferV2 = False     'Do not process in APD.
170       Else
180           CPartner_PartnerDatabaseTransferV2 = True      'Process in APD.
190       End If
          
ErrorHandler:

200       If Err.Number <> 0 Then
210           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
220       End If
End Function

Public Function CPartner_ApdDatabaseTransferV2() As String
          Const PROC_NAME As String = MODULE_NAME & "ApdDatabaseTransferV2: "
          
10        On Error GoTo ErrorHandler
          
20        Select Case mobjPartner.DocTypeEnum

              Case eReceipt

30                mobjPartner.ProcessReceipt_v2 "//FailureReasonCode", "//FailureReasonText"

40            Case eDigitalImage

50                mobjPartner.ProcessBinaryFiles_v2 mobjPassedDocument, "//DigitalImageFile", _
                      "//CreateDateTime/DateTime/@Date", "//CreateDateTime/DateTime/@Time", _
                      "PDF", mobjPartner.DocTypeString, eDigitalImage
                      
60                mobjPartner.SendWorkflowNotification_v2 "ElectronicDocumentReceived", _
                      "SHOP (" & mobjPartner.TradingPartner & "): Digital images received for Vehicle " & CInt(mobjPartner.VehicleNumber)

70            Case eEstimateData, ePrintImage

80                mobjPartner.ProcessEstimate_v2 Me

90        End Select
          
ErrorHandler:

100       If Err.Number <> 0 Then
110           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
120       End If
End Function

Public Function CPartner_InitProcessDocumentV2(ByVal strTradingPartner As String, ByVal strPassedDocument As String) As Boolean
          Const PROC_NAME As String = MODULE_NAME & "InitProcessDocumentV2: "
          
10        On Error GoTo ErrorHandler
          
20        mobjPartner.TradingPartner = strTradingPartner
30        mobjPartner.PassedDocument = strPassedDocument
          
40        Set mobjPassedDocument = New MSXML2.DOMDocument40
50        LoadXml mobjPassedDocument, strPassedDocument
          
          'Extract the doc type from the passed root element.
60        mobjPartner.RawDocTypeString = _
              GetChildNodeText(mobjPassedDocument, "/ADPTransaction/@TransactionType", True) _
      
          'Extract the LYNXID and Vehicle number
70        mobjPartner.ExtractLynxIDAndVehNum_v2
          
          'Are we configured to process this document at this time?
80        CPartner_InitProcessDocumentV2 = mobjPartner.CanProcessDocument_v2()
          
ErrorHandler:

90        If Err.Number <> 0 Then
100           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
110       End If

End Function

'********************************************************************************
'* This method transmits documents to the partner.
'********************************************************************************
Public Function CPartner_TransferToPartner() As String
10        Err.Raise eNotImplemented, MODULE_NAME & "TransferToPartner()", "Method not implemented"
End Function

'********************************************************************************
'* This method initializes processing of the documents.
'* Returns True if processing should continue.
'********************************************************************************
Public Function CPartner_InitProcessDocument() As Boolean
          Const PROC_NAME As String = MODULE_NAME & "InitProcessDocument: "
          
10        On Error GoTo ErrorHandler
          
          'Extract the doc type from the passed root element.
20        mobjPartner.RawDocTypeString = _
              GetChildNodeText(g_objPassedDocument, "/ADPTransaction/@TransactionType", True) _
      
          'Extract the LYNXID and Vehicle number
30        mobjPartner.ExtractLynxIDAndVehNum
          
          'Are we configured to process this document at this time?
40        CPartner_InitProcessDocument = mobjPartner.CanProcessDocument()
          
ErrorHandler:

50        If Err.Number <> 0 Then
60            g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
70        End If
          
End Function

'********************************************************************************
'* This method processing documents to the partner database.
'********************************************************************************
Public Function CPartner_PartnerDatabaseTransfer(ByVal blnSendReceipt As Boolean) As Boolean
          Const PROC_NAME As String = MODULE_NAME & "PartnerDatabaseTransfer: "

10        On Error GoTo ErrorHandler
          
20        Select Case mobjPartner.DocTypeEnum
          
              Case eDigitalImage

30                mobjPartner.StoreDocumentInPartnerDb
                  
40            Case ePrintImage

50                mobjPartner.ExtractSequenceNumber "//EstimateIdentifier"
60                mobjPartner.StorePrintImageInPartnerDB
                  
70            Case eEstimateData

80                mobjPartner.ExtractSequenceNumber "//TRANS_TYPE", "//SUPP_NO"
90                mobjPartner.StoreEstimateDataInPartnerDB
100               If blnSendReceipt Then mobjPartner.SendAReceipt "ADPReceipt.XML"
                  
110           Case Else

120               mobjPartner.StoreDocumentInPartnerDb

130       End Select
          
          'An ADP transaction was missing CLIENT_PASSTHRU_DATA.  To ahead and store
          'it in udb_partner and notify development to correct the XML and resubmit it.
140       If g_strLynxID = "UNK" And g_strVehicleNumber = "UNK" And g_strAssignmentID = "0" Then

150           g_objEvents.HandleEvent eLynxIdUnknown, PROC_NAME, _
                  mobjPartner.PartnerTransID(), "", "", False

160           CPartner_PartnerDatabaseTransfer = False    'Do not process in APD.
170       Else
180           CPartner_PartnerDatabaseTransfer = True     'Process in APD.
190       End If
          
ErrorHandler:

200       If Err.Number <> 0 Then
210           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
220       End If
          
End Function

'********************************************************************************
'* This method processing documents to the APD database.
'********************************************************************************
Public Function CPartner_ApdDatabaseTransfer() As String
          Const PROC_NAME As String = MODULE_NAME & "ApdDatabaseTransfer: "
          
10        On Error GoTo ErrorHandler
          
20        Select Case mobjPartner.DocTypeEnum

              Case eReceipt

30                mobjPartner.ProcessReceipt "//FailureReasonCode", "//FailureReasonText"

40            Case eDigitalImage

                  'mobjPartner.ProcessBinaryFiles g_objPassedDocument, "//DigitalImageFile", _
                      "//CreateDateTime/DateTime/@Date", "//CreateDateTime/DateTime/@Time", _
                      "PDF", mobjPartner.DocTypeString, eDigitalImage
50                mobjPartner.ProcessBinaryFiles mobjPassedDocument, "//DigitalImageFile", _
                      "//CreateDateTime/DateTime/@Date", "//CreateDateTime/DateTime/@Time", _
                      "PDF", mobjPartner.DocTypeString, eDigitalImage
                      
60                mobjPartner.SendWorkflowNotification "ElectronicDocumentReceived", _
                      "SHOP (" & g_strPartner & "): Digital images received for Vehicle " & CInt(g_strVehicleNumber)

70            Case eEstimateData, ePrintImage

80                mobjPartner.ProcessEstimate Me

90        End Select
          
ErrorHandler:

100       If Err.Number <> 0 Then
110           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
120       End If
          
End Function

'********************************************************************************
'* Purpose: Inserts an estimate print image into the APD document system.
'* Returns: The APD document ID through lngAPDDocumentID
'********************************************************************************
Public Function CPartner_InsertPrintImageIntoAPD( _
    ByVal strXML As String, _
    ByVal lngPartnerTransID As Long) As Long
          
10        CPartner_InsertPrintImageIntoAPD = _
              mobjPartner.InsertPrintImageIntoAPD(strXML, lngPartnerTransID)
End Function

'********************************************************************************
'* Send a HeartBeat request to Partner.
'********************************************************************************
Public Sub CPartner_SendHeartBeatRequest()
10        Err.Raise eNotImplemented, MODULE_NAME & "SendHeartBeatRequest()", "Method not implemented"
End Sub

