VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCCC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'********************************************************************************
'* Component LAPDPartnerDataMgr : Class CCCC
'*
'* Processing methods specific to CCC
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CCCC."

Implements CPartner

'Error codes private to this module.
Private Enum CCC_Error_Codes
    eFailedAddElement = CCCC_CLS_ErrorCodes
End Enum

'These are error codes that will come
'back in negative acknowledgements from CCC.
Private Enum CCC_NACK_ErrorCodes
    Invalid_Data_Assignment = "00102"
    Invalid_External_Alias_1 = "00103"
    Invalid_External_Alias_2 = "00213"
    Processing_Exception_Occurred = "00200"
    Invalid_Data_Exception = "00202"
End Enum

Private mobjPartner As New CPartner

'********************************************************************************
'* This method transmits documents to the partner.
'********************************************************************************
Public Function CPartner_TransferToPartner() As String
10        Err.Raise eNotImplemented, MODULE_NAME & "TransferToPartner()", "Method not implemented"
End Function

'********************************************************************************
'* This method initializes processing of the documents.
'* Returns True if processing should continue.
'********************************************************************************
Public Function CPartner_InitProcessDocument() As Boolean
          Const PROC_NAME As String = MODULE_NAME & "InitProcessDocument: "
          
10        On Error GoTo ErrorHandler
          
          'Extract the doc type from the passed root element.
20        mobjPartner.RawDocTypeString = _
              g_objPassedDocument.documentElement.baseName

          'Extract the LYNXID and Vehicle number for reference by any procedure
30        ExtractLynxIDAndVehNum mobjPartner.DocTypeEnum
          
          'Are we configured to process this document at this time?
40        CPartner_InitProcessDocument = mobjPartner.CanProcessDocument()
          
ErrorHandler:

50        If Err.Number <> 0 Then
60            g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
70        End If
          
End Function

'********************************************************************************
'* This method processing documents to the partner database.
'********************************************************************************
Public Function CPartner_PartnerDatabaseTransfer(ByVal blnSendReceipt As Boolean) As Boolean
          Const PROC_NAME As String = MODULE_NAME & "PartnerDatabaseTransfer: "

          Dim objElem As IXMLDOMElement
          
10        CPartner_PartnerDatabaseTransfer = True
          
20        On Error GoTo ErrorHandler
          
          'Backwards compatibility and cover up hack for Business Events:
          'If the CCC "Super Secret Echo Field" does not come back then we
          'hit the APD DB to get the AssignmentID back.  Then the LynxID,
          'VehNum and Suffix are used to create the echo field here.

30        Select Case mobjPartner.DocTypeEnum

              Case eDigitalImage
              
40                mobjPartner.StoreDocumentInPartnerDb
50                If blnSendReceipt Then SendAReceipt
                  
60            Case ePrintImage

70                ExtractSequenceNumber
80                mobjPartner.StorePrintImageInPartnerDB
90                If blnSendReceipt Then SendAReceipt
                  
100           Case eEstimateData

110               ExtractSequenceNumber
120               mobjPartner.StoreEstimateDataInPartnerDB
130               If blnSendReceipt Then SendAReceipt
                  
140           Case eBusinessEvent
              
150               mobjPartner.StoreDocumentInPartnerDb
160               If blnSendReceipt Then SendAReceipt
                  
170           Case Else

180               mobjPartner.StoreDocumentInPartnerDb
                  
190       End Select
          
ErrorHandler:

200       Set objElem = Nothing
          
210       If Err.Number <> 0 Then
220           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
230       End If
          
End Function

'********************************************************************************
'* Used for backwards compatibility problems with partner_db
'********************************************************************************
Private Sub AddEchoField(ByVal strElement As String, ByVal strParent As String)
          Const PROC_NAME As String = MODULE_NAME & "AddEchoField: "
          
10        g_objEvents.Trace "Looking for " & strElement & " within " & strParent, PROC_NAME

          Dim objElement As IXMLDOMElement
          Dim objParent As IXMLDOMElement
          
20        Set objElement = GetChildNode(g_objPassedDocument, "//" & strElement, False)
          
30        If objElement Is Nothing Then
          
40            Set objParent = GetChildNode(g_objPassedDocument, "//" & strParent, True)
50            Set objElement = AddElement(objParent, strElement)
              
60            If objElement Is Nothing Then
70                Err.Raise eFailedAddElement, PROC_NAME, "Failed to add element " & strElement & " to " & strParent
80            End If
              
90            objElement.Text = g_strLynxID & "-" & g_strVehicleNumber & "-" & g_strAssignmentID

100       End If
          
110       Set objElement = Nothing
120       Set objParent = Nothing
          
End Sub

'********************************************************************************
'* This method processing documents to the APD database.
'********************************************************************************
Public Function CPartner_ApdDatabaseTransfer() As String
          Const PROC_NAME As String = MODULE_NAME & "ApdDatabaseTransfer: "
          
10        On Error GoTo ErrorHandler
          
20        Select Case mobjPartner.DocTypeEnum

              Case eReceipt
                  
30                mobjPartner.ProcessReceipt "//ErrorCode", "//ErrorDescription"

40            Case eBusinessEvent

                  'There are no specific handlers for business events required
                  'because we only receive one business event from CCC.

50                mobjPartner.SendWorkflowNotification "AssignmentDownloadedByShop", _
                      "SHOP (CCC): Assignment for Vehicle " & CInt(g_strVehicleNumber) & " " & _
                      GetChildNode(g_objPassedDocument.documentElement, "//EventNotes", True).Text

60            Case eDigitalImage

70                mobjPartner.ProcessBinaryFiles g_objPassedDocument, "//DigitalImageFile", _
                      "//CreateDate", "//CreateTime", "JPG", mobjPartner.DocTypeString, _
                      eDigitalImage
                      
80                mobjPartner.SendWorkflowNotification "ElectronicDocumentReceived", _
                      "SHOP (" & g_strPartner & "): Digital images received for Vehicle " & CInt(g_strVehicleNumber)

90            Case eEstimateData, ePrintImage

100               mobjPartner.ProcessEstimate Me

110       End Select
          
ErrorHandler:

120       If Err.Number <> 0 Then
130           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
140       End If
          
End Function

'********************************************************************************
'* Purpose: Inserts an estimate print image into the APD document system.
'* Returns: The APD document ID through lngAPDDocumentID
'********************************************************************************
Public Function CPartner_InsertPrintImageIntoAPD( _
    ByVal strXml As String, _
    ByVal lngPartnerTransID As Long) As Long
    
          Const PROC_NAME As String = MODULE_NAME & ":CPartner_InsertPrintImageIntoAPD "
          'Grab the flatfile text from within the CDATA tags.
          Dim lngStart As Long, lngEnd As Long
          Dim objPrintImage As MSXML2.DOMDocument40
          Dim objImageFile As IXMLDOMElement
          Dim strPrintImage As String
    
10        g_objEvents.Trace "Loading Print Image XML", PROC_NAME
    
20        Set objPrintImage = New MSXML2.DOMDocument40
30        objPrintImage.async = False
40        LoadXml objPrintImage, strXml
    
50        Set objImageFile = objPrintImage.selectSingleNode("//ImageFile")
60        If Not objImageFile Is Nothing Then
70            strPrintImage = objImageFile.Text
80        Else
90            g_objEvents.Trace "Cannot find the ImageFile node in the print image. Will use the old method (string comparison) now.", PROC_NAME
100           lngStart = InStr(1, strXml, "CDATA[", vbBinaryCompare) + 6
110           lngEnd = InStr(1, strXml, "]]", vbBinaryCompare)
    
120           If lngStart <= 0 Or lngEnd <= 0 Or lngEnd <= lngStart Then
130               Err.Raise eCDataTagNotFound, "PrintImage", "CDATA tag not found in print image."
140           End If
        
150           strPrintImage = Mid(strXml, lngStart, (lngEnd - lngStart))
160       End If
    
170       Set objPrintImage = Nothing


          'Get the document file name.
          Dim strFileName As String
180       strFileName = mobjPartner.BuildFinalFileName(1, Format(Now, "yyyymmddhhmmss"), "PrintImage")

          'Convert the flatfile to XML and dump it into the document system.
      '    ConvertFlatfileToXML ePrintImage, _
              Mid(strXml, lngStart, (lngEnd - lngStart)), _
              "PrintImage", 66, "Estimate", strFileName

          Dim strImageType As String
190       strImageType = "XML"
    
200       If InStr(1, strPrintImage, "Job Number", vbTextCompare) > 0 Then
210           If g_blnDebugMode Then g_objEvents.Trace _
                    " Processing CCC XML based print image ", PROC_NAME & "Started"
              
              'content is in the old ez-net format with plain text.
220           ConvertFlatfileToXML ePrintImage, _
                  strPrintImage, _
                  "PrintImage", 66, "Estimate", strFileName
    
230           If g_blnDebugMode Then g_objEvents.Trace _
                    " Processing CCC XML based print image ", PROC_NAME & "Completed"
240       Else
250           If g_blnDebugMode Then g_objEvents.Trace _
                    " Processing CCC PDF based print image ", PROC_NAME & "Started"
        
              'this could be from CCC One
              Dim arrBuffer() As Byte
              Dim strPath As String
260           arrBuffer = ReadBinaryDataFromDomElement(objImageFile)
270           strFileName = Replace(strFileName, ".xml", ".pdf")
280           strPath = Replace((GetConfig("Document/RootDirectory") & strFileName), "\\", "\")
290           WriteBinaryDataToFile strPath, arrBuffer
        
300           strImageType = "PDF"
310           If g_blnDebugMode Then g_objEvents.Trace _
                    " Processing CCC PDF based print image ", PROC_NAME & "Completed"
320       End If
        
          'Insert new document record into APD database
330       CPartner_InsertPrintImageIntoAPD = mobjPartner.InsertLAPDDocument( _
              g_objDataAccessor, _
              ePrintImage, _
              CLng(g_strAssignmentID), _
              CLng(g_strLynxID), _
              CInt(g_strVehicleNumber), _
              mobjPartner.EstimateSequenceNumber, _
              mobjPartner.BuildPartnerTransID(), _
              strFileName, _
              strImageType, _
              "", _
              False)

End Function

'********************************************************************************
'* Extracts the LynxID and Vehicle Number from the Lynx specified reference ID.
'*
'* This routine is now modified to handle two versions of CCC transactions:
'* those that include the echo field and those that don't.  This makes the logic
'* rather messy, but it will save resubmission of docs by hand in PRD.
'********************************************************************************
Private Sub ExtractLynxIDAndVehNum(ByVal enmDocType As EDocumentType)
          Const PROC_NAME As String = MODULE_NAME & "ExtractLynxIDAndVehNum: "

10        On Error GoTo ErrorHandler

          Dim intLynxIDStart As Integer
          Dim intLynxIDEnd As Integer
          Dim intVehNumEnd As Integer
          
          Dim blnNeedAssignmentID As Boolean
          Dim strResults As String

20        intLynxIDStart = 0       ' This is where the LynxID begins
30        blnNeedAssignmentID = False
          
          'New "Secret Echo Field" extraction.
40        If enmDocType = eReceipt Then
50            g_strActualLynxID = GetChildNodeText(g_objPassedDocument, "//PartnerUniqueId")
60        ElseIf enmDocType = eBusinessEvent Then
70            blnNeedAssignmentID = True 'Flag that we dont have the assignment ID.
80            g_strActualLynxID = GetChildNodeText(g_objPassedDocument, "//CustomerRefId", True)
              'Old version still had the colon.  New version does not.  See Test Track 2037.
90            If InStr(1, g_strActualLynxID, ": ") > 0 Then
100               g_strActualLynxID = Mid(g_strActualLynxID, InStr(1, g_strActualLynxID, ": ") + 2)
110           ElseIf InStr(1, g_strActualLynxID, "ID ") > 0 Then
120               g_strActualLynxID = Mid(g_strActualLynxID, InStr(1, g_strActualLynxID, "ID ") + 3)
130           End If
140       ElseIf enmDocType = eDigitalImage Or enmDocType = ePrintImage Then
150           g_strActualLynxID = GetChildNodeText(g_objPassedDocument, "//InsuranceUniqueID")
160       ElseIf enmDocType = eEstimateData Then
170           g_strActualLynxID = GetChildNodeText(g_objPassedDocument, "//SecretField")
180       Else
190           Err.Raise eBadDocumentType, "", "Unknown document type: '" & enmDocType & "'."
200       End If
          
          'Backwards compatibility to old legacy documents
210       If Len(g_strActualLynxID) = 0 Then
220           blnNeedAssignmentID = True 'Flag that we dont have the assignment ID.
230           If enmDocType = eReceipt Then
240               g_strActualLynxID = GetChildNodeText(g_objPassedDocument, "//CustomerRefId", True)
250           ElseIf enmDocType = eDigitalImage Or enmDocType = ePrintImage Then
260               g_strActualLynxID = GetChildNodeText(g_objPassedDocument, "//ClaimNumber", True)
270           ElseIf enmDocType = eEstimateData Then
280               g_strActualLynxID = GetChildNodeText(g_objPassedDocument, "//ReceiverTransactionId", True)
290               intLynxIDStart = InStr(g_strActualLynxID, "#")
300           End If
310       End If

          'Split up the values by the dashes
          Dim strValues() As String
320       strValues = Split(g_strActualLynxID, "-")
          
330       g_objEvents.Assert UBound(strValues) > 0, "Invalid Document Ref ID (" & g_strActualLynxID & ")"
          
          'Extract the LynxID
340       g_strLynxID = AsNumber(strValues(0))

          'Extracts and zero fill the vehicle number
350       g_strVehicleNumber = ZeroPad(AsNumber(strValues(1)), 3)

          'Extract the assignment id.
          'Do we need to get it from the database using LynxID, VehNum and Suffix?
360       If UBound(strValues) < 2 Then
              
              Dim strEnd As String, intSuffixLen As Integer
370           strEnd = strValues(1)
380           intSuffixLen = Len(strEnd) - Len(CStr(CInt(g_strVehicleNumber)))
              
390           If intSuffixLen >= 0 Then
400               strEnd = Right(strEnd, intSuffixLen)
                  'Take only the first character, and cover for estimates with their '#' character.
410               strEnd = IIf((Left(strEnd, 1) = "#"), "", Left(strEnd, 1))
420           End If
              
430           g_objDataAccessor.SetConnectString g_strLYNXConnStringStd
              
440           g_strAssignmentID = g_objDataAccessor.ExecuteSpNamedParams( _
                  GetConfig("PartnerSettings/AssignmentDecodeSP"), _
                      "@LynxID", CLng(g_strLynxID), _
                      "@VehicleNumber", CInt(g_strVehicleNumber), _
                      "@AssignmentSuffix", strEnd)
450       Else
460           g_strAssignmentID = AsNumber(strValues(2))
470       End If
          
          'Extract the assignment / cancel character.
          'Do we need to get it from the database using LynxID, VehNum and Suffix?
480       If UBound(strValues) > 2 Then
490           g_strActionCode = Left(strValues(3), 1)
500       End If
510       If g_strActionCode <> "C" Then
520           g_strActionCode = "N"
530       End If
          
          'Extract the transaction sent date/time.
          Dim strDate As String, strTime As String
          
540       strDate = GetChildNodeText(g_objPassedDocument, "//TransactionDate", True)
550       strTime = GetChildNodeText(g_objPassedDocument, "//TransactionTime", True)
          
560       g_varTransactionDate = Left(strDate, 2) & "/" & Mid(strDate, 3, 2) & "/" & Right(strDate, 4) _
              & " " & Left(strTime, 2) & ":" & Right(strTime, 2)
          
ErrorHandler:

570       strResults = "LynxID=" & g_strLynxID & " VehNum=" & g_strVehicleNumber _
              & " AssignmentID=" & g_strAssignmentID
          
580       If Err.Number <> 0 Then
590           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, _
                  "Error extracting LynxID, VehNum and AssignmentID from " & _
                  g_strActualLynxID & " : " & Err.Description, strResults
600       ElseIf g_blnDebugMode Then
610           g_objEvents.Trace strResults, PROC_NAME & g_strActualLynxID
620       End If

End Sub

'********************************************************************************
'* Extracts the Estimate Sequence Number from the Print Image or Estimate Data and stores it in intEstimateSequenceNumber.
'* The function returns true if a value is found, false if not
'********************************************************************************
Private Sub ExtractSequenceNumber()
          Const PROC_NAME As String = MODULE_NAME & "ExtractSequenceNumber: "

10        On Error GoTo ErrorHandler

          Dim nodCurrent  As MSXML2.IXMLDOMNodeList   ' The nodes we're looking at
          Dim strNodeText As String                   ' Text value of node we're looking at
          Dim blnExtractSequenceNumber As Boolean

          ' Set default return value
20        blnExtractSequenceNumber = False

          'CCC Print Image
30        If mobjPartner.DocTypeEnum = ePrintImage Then
          
40            mobjPartner.ExtractSequenceNumber "//EstimateIdentifier"
50            blnExtractSequenceNumber = True

          'Estimate Data
60        ElseIf mobjPartner.DocTypeEnum = eEstimateData Then

70            Set nodCurrent = g_objPassedDocument.getElementsByTagName("TransactionType")
80            strNodeText = nodCurrent.Item(0).nodeTypedValue     ' This is the value as it comes from the XML document

              ' In a ED, the estimate Supplement Identifier comes from a combination of two elements, one which designates
              ' E or S (for estimate and supplement), and a second which designates the number.  If the value of the first
              ' element is "E", just set the sequence number to 0.  Other, we'll need to get the sequence number from
              ' the second element.

90            If strNodeText = "E" Then
100               mobjPartner.EstimateSequenceNumber = 0
110               blnExtractSequenceNumber = True
120           End If

130           If strNodeText = "S" Then
                  ' This is a supplement, get the Supplement Sequence Nubmber
140               Set nodCurrent = g_objPassedDocument.getElementsByTagName("EstimateSupplementNmbr")
150               strNodeText = nodCurrent.Item(0).nodeTypedValue     ' This is the value as it comes from the XML document

160               If IsNumeric(strNodeText) Then
170                   mobjPartner.EstimateSequenceNumber = CInt(strNodeText)
180                   blnExtractSequenceNumber = True
190               End If
200           End If

          'Unknown
210       Else
220           Err.Raise eBadDocumentType, "", "Unknown document type : '" & mobjPartner.DocTypeString & "'."
230       End If

240       If Not blnExtractSequenceNumber Then
250           Err.Raise eBadSequenceNumber, PROC_NAME, "Unable to extract Estimate Sequence Number"
260       End If

ErrorHandler:

270       If Err.Number <> 0 Then
280           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
290       End If

End Sub

'********************************************************************************
'* Send a receipt back to CCC.
'********************************************************************************
Private Sub SendAReceipt()
          Const PROC_NAME As String = MODULE_NAME & "SendAReceipt: "

10        On Error GoTo ErrorHandler

          Dim objLYNXReceipt As DOMDocument40

          Dim strWrappedReceipt As String
          Dim strPost2URL As String
          Dim strTransType As String

20        If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Preparing receipt"

          'First load the receipt template from disk.

30        Set objLYNXReceipt = New MSXML2.DOMDocument40
40        LoadXmlFile objLYNXReceipt, g_strSupportDocPath & "\CCCReceipt.XML", "SendAReceipt", "Receipt"

          'Fill out the template with values from the submitted document.

50        GetChildNode(objLYNXReceipt, "//SenderTransactionId", True).Text = _
              GetChildNodeText(g_objPassedDocument, "//SenderTransactionId", True)

60        GetChildNode(objLYNXReceipt, "//ReceiverTransactionId", True).Text = _
              GetChildNodeText(g_objPassedDocument, "//ReceiverTransactionId", True)

70        GetChildNode(objLYNXReceipt, "//TransactionDate", True).Text = ConstructXmlDate(Now())

80        GetChildNode(objLYNXReceipt, "//TransactionTime", True).Text = ConstructXmlTime(Now())

90        GetChildNode(objLYNXReceipt, "//CustomerRefId", True).Text = _
              g_strLynxID & "-" & CInt(g_strVehicleNumber)

          'The CCC transaction type is the root node name minus the 4 digit version number.
100       strTransType = g_objPassedDocument.documentElement.baseName
110       strTransType = Left(strTransType, Len(strTransType) - 4)

120       GetChildNode(objLYNXReceipt, "//TransactionType", True).Text = strTransType

130       GetChildNode(objLYNXReceipt, "//ErrorCode", True).Text = "00000"

140       GetChildNode(objLYNXReceipt, "//ErrorDescription", True).Text = mobjPartner.DocTypeString & " processed"

150       GetChildNode(objLYNXReceipt, "//EventCreatedDateTime/DlDate/Date", True).Text = _
              ConstructXmlDate(Now()) & " " & ConstructXmlTime(Now())

          'Build the SOAP Envelope and put the receipt into the envelope
160       strWrappedReceipt = BuildTransactionForPPG( _
              "Receipt", _
              objLYNXReceipt.xml, _
              g_strPartner, _
              g_strLynxID & "-" & g_strVehicleNumber)

          'Determine the URL for posting depending on the environment
170       strPost2URL = GetConfig("ShopAssignment/ElectronicTransmissionURL")

          'Do the post to PPG
180       XmlHttpPost strPost2URL, strWrappedReceipt

190       If g_blnDebugMode Then g_objEvents.Trace "URL = " & strPost2URL, PROC_NAME & "Receipt sent via HTTP"

ErrorHandler:

200       Set objLYNXReceipt = Nothing

210       If Err.Number <> 0 Then
220           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description
230       End If

End Sub

'********************************************************************************
'* Send a HeartBeat request to Partner.
'********************************************************************************
Public Sub CPartner_SendHeartBeatRequest()
10        Err.Raise eNotImplemented, MODULE_NAME & "SendHeartBeatRequest()", "Method not implemented"
End Sub


Private Function CPartner_PartnerDatabaseTransferV2(ByVal blnSendReceipt As Boolean) As Boolean
    Const PROC_NAME As String = MODULE_NAME & "PartnerDatabaseTransferV2: "
    Err.Raise eNotImplemented, PROC_NAME, "NOT IMPLEMENTED"
End Function

Private Function CPartner_ApdDatabaseTransferV2() As String
    Const PROC_NAME As String = MODULE_NAME & "ApdDatabaseTransferV2: "
    Err.Raise eNotImplemented, PROC_NAME, "NOT IMPLEMENTED"
End Function

Private Function CPartner_InitProcessDocumentV2(ByVal strTradingPartner As String, ByVal strPassedDocument As String) As Boolean
    Const PROC_NAME As String = MODULE_NAME & "InitProcessDocumentV2: "
    Err.Raise eNotImplemented, PROC_NAME, "NOT IMPLEMENTED"
End Function

Private Function CPartner_InsertPrintImageIntoAPDV2(ByVal strXml As String, ByVal lngPartnerTransID As Long) As Long
    Const PROC_NAME As String = MODULE_NAME & "InsertPrintImageIntoAPDV2: "
    Err.Raise eNotImplemented, PROC_NAME, "NOT IMPLEMENTED"
End Function

