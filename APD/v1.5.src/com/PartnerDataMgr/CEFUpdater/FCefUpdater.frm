VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FCefUpdater 
   Caption         =   "Estimate Data CEF Updater"
   ClientHeight    =   7815
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9615
   LinkTopic       =   "Form1"
   ScaleHeight     =   7815
   ScaleWidth      =   9615
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox cbxCefExistsInPartner 
      Caption         =   "CEF column exists in partner"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   1200
      Width           =   2415
   End
   Begin VB.CommandButton btnHelp 
      Caption         =   "Help"
      Height          =   360
      Left            =   7920
      TabIndex        =   12
      Top             =   1200
      Width           =   1335
   End
   Begin VB.CheckBox cbxForceCefUpdateToPartner 
      Caption         =   "Force CEF update to partner"
      Height          =   255
      Left            =   2760
      TabIndex        =   11
      Top             =   1425
      Width           =   2520
   End
   Begin VB.CheckBox cbxDoApdUpdate 
      Caption         =   "Do estimate update to APD"
      Height          =   255
      Left            =   5400
      TabIndex        =   10
      Top             =   1200
      Width           =   2295
   End
   Begin VB.CheckBox cbxDoPartnerUpdate 
      Caption         =   "Do CEF update to partner"
      Height          =   255
      Left            =   2760
      TabIndex        =   9
      Top             =   1080
      Width           =   2415
   End
   Begin VB.TextBox txtIdx 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5640
      TabIndex        =   6
      Text            =   "1"
      Top             =   480
      Width           =   735
   End
   Begin VB.CommandButton btnStep 
      Caption         =   "Step"
      Height          =   375
      Left            =   6480
      TabIndex        =   5
      Top             =   480
      Width           =   1335
   End
   Begin VB.CommandButton btnAll 
      Caption         =   "All"
      Height          =   375
      Left            =   7920
      TabIndex        =   4
      Top             =   480
      Width           =   1335
   End
   Begin MSComctlLib.ListView lvwEst 
      Height          =   5910
      Left            =   120
      TabIndex        =   3
      Top             =   1785
      Width           =   9375
      _ExtentX        =   16536
      _ExtentY        =   10425
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.CommandButton btnPRD 
      Caption         =   "PRD"
      Height          =   375
      Left            =   3240
      TabIndex        =   2
      Top             =   480
      Width           =   1335
   End
   Begin VB.CommandButton btnTST 
      Caption         =   "TST"
      Height          =   375
      Left            =   1800
      TabIndex        =   1
      Top             =   480
      Width           =   1335
   End
   Begin VB.CommandButton btnDEV 
      Caption         =   "DEV"
      Height          =   375
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Caption         =   "Click to extract estimate list from APD database"
      Height          =   855
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   4695
   End
   Begin VB.Frame Frame2 
      Caption         =   "Click to process estimates"
      Height          =   855
      Left            =   4920
      TabIndex        =   8
      Top             =   120
      Width           =   4575
      Begin VB.Label Label1 
         Caption         =   "IDX"
         Height          =   195
         Left            =   285
         TabIndex        =   13
         Top             =   435
         Width           =   315
      End
   End
End
Attribute VB_Name = "FCefUpdater"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'********************************************************************************
'* Form FCefUpdater
'*
'* Ye olde app for creating CEFs for old estimates.
'* See help.html for more details.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = "FCefUpdater."

Private Enum EColumns
    eIdx
    eDocumentID
    eReferenceID
    ePartnerID
    eSource
    eDate
    eExists
    eResult
End Enum

'Objects used by this app.
Private moData As DataAccessor.CDataAccessor
Private moEvents As SiteUtilities.CEvents
Private moSettings As SiteUtilities.CSettings

Private msEnv As String
Private miCount As Integer

Private Sub cbxCefExistsInPartner_Click()
    If cbxCefExistsInPartner.Value = 0 Then
        cbxDoPartnerUpdate.Value = 0
        cbxForceCefUpdateToPartner = 0
    End If
End Sub

'********************************************************************************
'* Initialize objects and form controls.
'********************************************************************************
Private Sub Form_Load()

    Set moData = New DataAccessor.CDataAccessor
    moData.InitEvents App.Path & "\config.xml"
    
    Set moEvents = moData.mEvents
    Set moSettings = moEvents.mSettings
    
    cbxCefExistsInPartner.Value = 1
    cbxDoPartnerUpdate.Value = 1
    cbxForceCefUpdateToPartner = 0
    
    lvwEst.View = lvwReport
    lvwEst.Visible = True
    
    Dim colHdr As ColumnHeader
    
    Set colHdr = lvwEst.ColumnHeaders.Add()
    colHdr.Text = "idx"
    colHdr.Width = 500
    
    Set colHdr = lvwEst.ColumnHeaders.Add()
    colHdr.Text = "DocID"
    colHdr.Width = 700
    
    Set colHdr = lvwEst.ColumnHeaders.Add()
    colHdr.Text = "ReferenceID"
    colHdr.Width = 1600
    
    Set colHdr = lvwEst.ColumnHeaders.Add()
    colHdr.Text = "PartnerID"
    colHdr.Width = 900
    
    Set colHdr = lvwEst.ColumnHeaders.Add()
    colHdr.Text = "Src"
    colHdr.Width = 550
    
    Set colHdr = lvwEst.ColumnHeaders.Add()
    colHdr.Text = "Date"
    colHdr.Width = 2100
    
    Set colHdr = lvwEst.ColumnHeaders.Add()
    colHdr.Text = "Exist"
    colHdr.Width = 700
    
    Set colHdr = lvwEst.ColumnHeaders.Add()
    colHdr.Text = "Result"
    colHdr.Width = 2000
    
    Set colHdr = Nothing
End Sub

'********************************************************************************
'* Object clean up.
'********************************************************************************
Private Sub Form_Unload(Cancel As Integer)
    Set moSettings = Nothing
    Set moEvents = Nothing
    Set moData = Nothing
End Sub

'********************************************************************************
'* Spits recordset contents out to the debug log.
'********************************************************************************
Private Sub DebugRS(ByRef RS As ADODB.Recordset)
    Dim str As String
    Dim strValue As String
    Dim fld As ADODB.Field
    
    RS.MoveFirst
    
    str = "Record Count = " & RS.RecordCount & vbCrLf
    str = str & "  Fields = "
    
    For Each fld In RS.Fields
        str = str & fld.Name & ", "
    Next
    
    str = str & vbCrLf
    
    While Not RS.EOF
        str = str & "  Data: "
        For Each fld In RS.Fields
            If Not IsNull(fld.Value) Then
                strValue = LTrim(RTrim(Replace(fld.Value, vbCrLf, "")))
                If Len(strValue) > 0 Then
                    If Left(strValue, 1) = "<" And Right(strValue, 1) = ">" Then
                        str = str & "(XML NOT LOGGED), "
                    Else
                        str = str & "'" & fld.Value & "', "
                    End If
                Else
                    str = str & "'' , "
                End If
            Else
                str = str & "NULL, "
            End If
        Next
        str = str & vbCrLf
        
        RS.MoveNext
    Wend
    
    moEvents.Trace str, MODULE_NAME & "Recordset Contents"

    RS.MoveFirst
    
End Sub

'********************************************************************************
'* Environment specific buttons
'********************************************************************************
Private Sub btnDEV_Click()
    LoadEstimateList "DEV"
End Sub

Private Sub btnTST_Click()
    LoadEstimateList "TST"
End Sub

Private Sub btnPRD_Click()
    LoadEstimateList "PRD"
End Sub

'********************************************************************************
'* This routing hits the appropriate APD database and extracts a list
'* of estimates to examine.  The list view is filled up with the results.
'********************************************************************************
Private Function LoadEstimateList(ByVal sEnv As String)
    Const PROC_NAME As String = MODULE_NAME & "LoadEstimateList: "
    
    On Error GoTo ErrorHandler
    
    Dim oRS As ADODB.Recordset
    Dim oItem As ListItem
    Dim iIdx As Integer

    If moEvents.IsDebugMode Then moEvents.Trace "", PROC_NAME & "Loading estimate documents for environment " & sEnv
    
    msEnv = sEnv
    lvwEst.ListItems.Clear
    txtIdx.Text = "1"
    
    moData.SetConnectString moSettings.GetParsedSetting( _
        "Environment[@EnvironmentCode='" & msEnv & "']/APDSettings/ConnectionStringStd")
    
    Set oRS = moData.OpenRecordsetSp( _
        "select DocumentID, ExternalReferenceDocumentID, DocumentSourceID, CreatedDate from utb_document " & _
        "where (DocumentTypeID = 3 Or DocumentTypeID = 4 Or DocumentTypeID = 10 Or DocumentTypeID = 14) " & _
        "and ( DocumentSourceID = 1 or DocumentSourceID = 2 ) " & _
        "and CreatedDate > '2003-05-29' " & _
        "and EnabledFlag = 1 order by CreatedDate")
    
    If Err.Number <> 0 Then
        MsgBox Err.Description
    Else
        
        If moEvents.IsDebugMode Then DebugRS oRS
        
        iIdx = 1
        oRS.MoveFirst
        
        While Not oRS.EOF
        
            Set oItem = lvwEst.ListItems.Add(, , CString(iIdx))
            
            oItem.ListSubItems.Add , , CString(oRS!DocumentID)
            oItem.ListSubItems.Add , , CString(oRS!ExternalReferenceDocumentID)
            oItem.ListSubItems.Add , , PartnerID(oRS!ExternalReferenceDocumentID)
            oItem.ListSubItems.Add , , IIf((oRS!DocumentSourceID = 1), "ADP", "CCC")
            oItem.ListSubItems.Add , , CString(oRS!CreatedDate)
            
            oRS.MoveNext
            
            miCount = iIdx
            iIdx = iIdx + 1
            
        Wend
    
    End If
    
ErrorHandler:

    If Not oRS Is Nothing Then
        If oRS.State = adStateOpen Then oRS.Close
    End If

    Set oRS = Nothing
    Set oItem = Nothing

End Function

'********************************************************************************
'* Safely converts DB result variants to strings
'********************************************************************************
Private Function CString(ByVal vData As Variant) As String
    If IsNull(vData) Then
        CString = ""
    Else
        CString = CStr(vData)
    End If
End Function

'********************************************************************************
'* Strips the partner ID off the end of the "1234;ED-ID:5678" strings.
'********************************************************************************
Private Function PartnerID(ByVal vData As Variant) As String
    Dim sData As String
    Dim iPos As Integer
    
    PartnerID = ""
    
    If Not IsNull(vData) Then
        sData = CStr(vData)
        iPos = InStr(sData, ":")
        If iPos > 0 Then
            PartnerID = Mid(sData, iPos + 1)
        End If
    End If
End Function

'********************************************************************************
'* Clicked to process the next estimate.
'********************************************************************************
Private Sub btnStep_Click()
    Dim iIdx As Integer
    
    'Get the next step, process it, then increment step counter.
    iIdx = CInt(txtIdx.Text)
    If iIdx > miCount Then
        MsgBox "Past end of list."
    Else
        ProcessAnEstimate iIdx
        txtIdx.Text = CStr(iIdx + 1)
    End If
End Sub

'********************************************************************************
'* Clicked to process all estimates.
'********************************************************************************
Private Sub btnAll_Click()
    Dim iIdx As Integer
    
    For iIdx = 1 To miCount
    
        txtIdx.Text = iIdx
        ProcessAnEstimate iIdx
        
        'Redraw the screen
        txtIdx.Refresh
        lvwEst.Refresh
    Next
End Sub

'********************************************************************************
'* Called to process a single estimate.
'********************************************************************************
Private Sub ProcessAnEstimate(ByVal iIdx As Integer)

    On Error GoTo ErrorHandler
    
    Dim oItem As ListItem
    Dim oRS As ADODB.Recordset
    Dim oResultDom As MSXML2.DOMDocument40
    Dim oNodeList As MSXML2.IXMLDOMNodeList
    Dim oNode As MSXML2.IXMLDOMNode
    
    Dim lPartnerID As Long
    Dim sPartnerID As String
    
    Dim sXml As String
    Dim sCef As String

    Set oItem = lvwEst.ListItems.Item(iIdx)
    
    sPartnerID = oItem.SubItems(ePartnerID)
    
    'Must have a partner ID
    If sPartnerID = "" Or sPartnerID = "0" Then
        oItem.SubItems(eResult) = "Blank PartnerID"
        oItem.SubItems(eExists) = "UNK"
    Else
        lPartnerID = CLng(sPartnerID)
        
        'Extract the XML and CEF XML from the partner database.
        moData.SetConnectString moSettings.GetParsedSetting( _
            "Environment[@EnvironmentCode='" & msEnv & "']/PartnerSettings/ConnectionStringStd")
        
        If cbxCefExistsInPartner.Value = 1 Then
        
            Set oRS = moData.OpenRecordsetSp( _
                "select XML, CEFXML from utb_transaction_header " & _
                "where PartnerTransID = " & lPartnerID)
                
        Else
        
            Set oRS = moData.OpenRecordsetSp( _
                "select XML from utb_transaction_header " & _
                "where PartnerTransID = " & lPartnerID)
        
        End If
        
        'Check to ensure that results were returned.
        If oRS.BOF And oRS.EOF Then
            oItem.SubItems(eResult) = "Missing Partner Record"
            oItem.SubItems(eExists) = "UNK"
        Else
        
            'Does the CEF column exist?
            If cbxCefExistsInPartner.Value = 1 Then
            
                sCef = CString(oRS!CEFXML)
                
                If sCef <> "" Then
                    oItem.SubItems(eExists) = "Yes"
                Else
                    oItem.SubItems(eExists) = "No"
                End If
                
            Else
            
                sCef = ""
                oItem.SubItems(eExists) = "No"
                
            End If
            
            'If the CEF already exists then do nothing.
            If sCef <> "" And cbxForceCefUpdateToPartner.Value <> 1 Then
            
                'Load the old CEF up into the results dom.
                Set oResultDom = New MSXML2.DOMDocument40
                LoadXml oResultDom, sCef
                
            Else
            
                'Generate a new CEF.
                sXml = CString(oRS!xml)
                
                'Transform the XML to get the CEF
                Set oResultDom = TransformXmlAsDom(sXml, _
                    moSettings.GetParsedSetting("PartnerXslPath") & _
                    oItem.SubItems(eSource) & "EstimateXfer.xsl", moEvents)
                
                'Are we supposed to update the CEF?
                If cbxDoPartnerUpdate.Value = 1 Then
                
                    moData.ExecuteSp _
                        "update utb_transaction_header set CEFXML = '" & _
                        SQLQueryString(oResultDom.xml) & "' where PartnerTransID = " & lPartnerID
                
                End If
                
            End If
                
            'Are we supposed to update the APD estimate data?
            If cbxDoApdUpdate.Value = 1 Then
            
                moData.SetConnectString moSettings.GetParsedSetting( _
                    "Environment[@EnvironmentCode='" & msEnv & "']/APDSettings/ConnectionStringStd")
                
                ' Get a list of line items for the summary.
                Set oNodeList = oResultDom.selectNodes("//Summary/LineItem[@TransferLine='Yes'][@Category='OT']")
            
                For Each oNode In oNodeList
                
                    Dim iTaxableFlag As Integer
                    Dim sTaxTypeCD As String
                    
                    ' Have to set the Database insertion values
                    iTaxableFlag = IIf(GetChildNodeText(oNode, "@Taxable") = "Yes", 1, 0)
            
                    Select Case GetChildNodeText(oNode, "@TaxBase")
                        Case "Parts":               sTaxTypeCD = "PT"
                        Case "Materials":           sTaxTypeCD = "SP"
                        Case "PartsAndMaterials":   sTaxTypeCD = "MT"
                        Case "Labor":               sTaxTypeCD = "LB"
                        Case "AdditionalCharges":   sTaxTypeCD = "AC"
                        Case "Subtotal":            sTaxTypeCD = "ST"
                        Case Else:                  sTaxTypeCD = "OT"
                    End Select
            
                    ' Insert the record
                    moData.ExecuteSp _
                        moSettings.GetParsedSetting("Estimate/InsertSummarySPName"), _
                        0, _
                        "DEFAULT", _
                        "'" & SQLQueryString(GetChildNodeText(oNode, "@Type")) & "'", _
                        "'" & SQLQueryString(GetChildNodeText(oNode, "@Category")) & "'", _
                        oItem.SubItems(eDocumentID), _
                        SQLQueryString(GetChildNodeText(oNode, "@ExtendedAmount")), _
                        SQLQueryString(GetChildNodeText(oNode, "@Hours")), _
                        SQLQueryString(GetChildNodeText(oNode, "@Percent")), _
                        iTaxableFlag, _
                        SQLQueryString(GetChildNodeText(oNode, "@UnitAmount")), _
                        "'" & SQLQueryString(GetChildNodeText(oNode, "@Note")) & "'", _
                        SQLQueryString(GetChildNodeText(oNode, "@ExtendedAmount")), _
                        SQLQueryString(GetChildNodeText(oNode, "@Hours")), _
                        SQLQueryString(GetChildNodeText(oNode, "@Percent")), _
                        iTaxableFlag, _
                        SQLQueryString(GetChildNodeText(oNode, "@UnitAmount")), _
                        "'" & sTaxTypeCD & "'", _
                        "DEFAULT", _
                        "'" & moSettings.GetParsedSetting("WorkFlow/NTUserID") & "'", _
                        "DEFAULT"
                Next
            
            End If
                
            oItem.SubItems(eResult) = "Success"
                
            moEvents.DumpLogBufferToDisk
                
        End If
        
    End If

ErrorHandler:
    
    If Not oRS Is Nothing Then
        If oRS.State = adStateOpen Then oRS.Close
    End If

    Set oRS = Nothing
    Set oResultDom = Nothing
    Set oNodeList = Nothing
    Set oNode = Nothing
    
    If Err.Number <> 0 Then
        oItem.SubItems(eResult) = "ERROR!"
        MsgBox "Step " & iIdx & " : " & Err.Description
    End If

    Set oItem = Nothing
    
End Sub

Private Sub btnHelp_Click()
    Dim oShell As Object
    Set oShell = CreateObject("WSCript.shell")
    
    oShell.Run "explorer " & App.Path & "\help.html"
    
    Set oShell = Nothing
End Sub


