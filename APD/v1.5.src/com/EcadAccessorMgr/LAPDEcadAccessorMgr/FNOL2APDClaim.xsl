<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" omit-xml-declaration="yes" indent="yes"/>
  <xsl:decimal-format NaN="0"/>
  <!-- We will transform the FNOL claim XML to match the uspClaimGetDetailXML output -->
  <!-- Output the Root node -->
  <xsl:template match="/">
    <xsl:element name="Root">
      <xsl:attribute name="LynxID">
        <xsl:value-of select="//Record/LynxID"/>
      </xsl:attribute>
      <xsl:call-template name="ClaimData"/>
    </xsl:element>
  </xsl:template>
  <!-- Output the claim data -->
  <xsl:template name="ClaimData">
    <xsl:element name="Claim">
      <xsl:attribute name="LynxID">
        <xsl:value-of select="//Record/LynxID"/>
      </xsl:attribute>
      <xsl:attribute name="LossCity">
        <xsl:value-of select="//Record/LossCity"/>
      </xsl:attribute>
      <xsl:attribute name="LossDate">
        <xsl:value-of select="//Record/LossDate"/>
      </xsl:attribute>
      <xsl:attribute name="LossDescription">
        <xsl:value-of select="//Record/LossDescription"/>
      </xsl:attribute>
      <xsl:attribute name="LossState">
        <xsl:value-of select="//Record/LossState"/>
      </xsl:attribute>
      <xsl:attribute name="PolicyNumber">
        <xsl:value-of select="//Record/PolicyNumber"/>
      </xsl:attribute>
      <xsl:attribute name="CoverageCD">
        <xsl:value-of select="//Record/ProfileCD"/>
      </xsl:attribute>
      <xsl:attribute name="Remarks">
        <xsl:value-of select="//Record/RemarksToAdjuster"/>
      </xsl:attribute>
      <!-- Insured data -->
      <xsl:call-template name="InsuredData"/>
      <!-- Vehicle data -->
      <xsl:call-template name="VehicleData"/>
      <!-- Coverage data -->
      <xsl:call-template name="CoverageData"/>
    </xsl:element>
  </xsl:template>
  <!-- Insured data -->
  <xsl:template name="InsuredData">
    <xsl:variable name="InsuredID" select="//Record/Insured"/>
    <xsl:variable name="InsuredVehicleID" select="//Record/InsuredVehicle"/>
    <xsl:element name="Insured">
      <xsl:attribute name="id">
        <xsl:value-of select="$InsuredID"/>
      </xsl:attribute>
      <xsl:attribute name="NameFirst">
        <xsl:value-of select="//Record/Involved[@id=$InsuredID]/FirstName"/>
      </xsl:attribute>
      <xsl:attribute name="NameLast">
        <xsl:value-of select="//Record/Involved[@id=$InsuredID]/LastName"/>
      </xsl:attribute>
      <xsl:attribute name="Phone1AreaCode">
        <xsl:call-template name="phoneAreaCode">
          <xsl:with-param name="val" select="//Record/Involved[@id=$InsuredID]/PhoneNumber1"/>
        </xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="Phone1Exchange">
        <xsl:call-template name="phoneExchange">
          <xsl:with-param name="val" select="//Record/Involved[@id=$InsuredID]/PhoneNumber1"/>
        </xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="Phone1UnitNumber">
        <xsl:call-template name="phoneUnitNumber">
          <xsl:with-param name="val" select="//Record/Involved[@id=$InsuredID]/PhoneNumber1"/>
        </xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="Phone2AreaCode">
        <xsl:call-template name="phoneAreaCode">
          <xsl:with-param name="val" select="//Record/Involved[@id=$InsuredID]/PhoneNumber2"/>
        </xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="Phone2Exchange">
        <xsl:call-template name="phoneExchange">
          <xsl:with-param name="val" select="//Record/Involved[@id=$InsuredID]/PhoneNumber2"/>
        </xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="Phone2UnitNumber">
        <xsl:call-template name="phoneUnitNumber">
          <xsl:with-param name="val" select="//Record/Involved[@id=$InsuredID]/PhoneNumber2"/>
        </xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="Phone3AreaCode">
        <xsl:call-template name="phoneAreaCode">
          <xsl:with-param name="val" select="//Record/Involved[@id=$InsuredID]/PhoneNumber3"/>
        </xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="Phone3Exchange">
        <xsl:call-template name="phoneExchange">
          <xsl:with-param name="val" select="//Record/Involved[@id=$InsuredID]/PhoneNumber3"/>
        </xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="Phone3UnitNumber">
        <xsl:call-template name="phoneUnitNumber">
          <xsl:with-param name="val" select="//Record/Involved[@id=$InsuredID]/PhoneNumber3"/>
        </xsl:call-template>
      </xsl:attribute>
    </xsl:element>
  </xsl:template>
  <!-- Vehicle data -->
  <xsl:template name="VehicleData">
    <xsl:for-each select="//Property[@type='vehicle']">
      <xsl:element name="Vehicle">
        <xsl:attribute name="id">
          <xsl:value-of select="@id"/>
        </xsl:attribute>
        <xsl:attribute name="exposurecd">
          <xsl:choose>
            <xsl:when test="@relation = 'first'">1</xsl:when>
            <xsl:otherwise>3</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="id">
          <xsl:value-of select="@id"/>
        </xsl:attribute>
        <xsl:attribute name="year">
          <xsl:if test="Year != '' and Year != 'UNKNOWN'">
            <xsl:value-of select="Year"/>
          </xsl:if>
        </xsl:attribute>
        <xsl:attribute name="make">
          <xsl:if test="Make != '' and Make != 'UNKNOWN'">
            <xsl:value-of select="Make"/>
          </xsl:if>
        </xsl:attribute>
        <xsl:attribute name="model">
          <xsl:if test="Model != '' and Model != 'UNKNOWN'">
            <xsl:value-of select="Model"/>
          </xsl:if>
        </xsl:attribute>
        <xsl:attribute name="drivable">
          <xsl:choose>
            <xsl:when test="Drivable = 'YES'">1</xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="mileage">
          <xsl:if test="Mileage != '' and Mileage != 'UNKNOWN'">
            <xsl:value-of select="Mileage"/>
          </xsl:if>
        </xsl:attribute>
        <xsl:attribute name="licensePlate">
          <xsl:if test="LicensePlateNumber != '' and LicensePlateNumber != 'UNKNOWN'">
            <xsl:value-of select="LicensePlateNumber"/>
          </xsl:if>
        </xsl:attribute>
        <xsl:attribute name="licensePlateState">
          <xsl:if test="LicensePlateState != '' and LicensePlateState != 'UNKNOWN'">
            <xsl:value-of select="LicensePlateState"/>
          </xsl:if>
        </xsl:attribute>
        <xsl:attribute name="vin">
          <xsl:if test="VIN != '' and VIN != 'UNKNOWN'">
            <xsl:value-of select="VIN"/>
          </xsl:if>
        </xsl:attribute>
        <xsl:attribute name="primaryDamages">
          <xsl:value-of select="APDTriageQuestions/ImpactLocationPrimary"/>
        </xsl:attribute>
        <xsl:attribute name="secondaryDamages">
          <xsl:value-of select="APDTriageQuestions/ImpactLocationSecondary"/>
        </xsl:attribute>
        <xsl:attribute name="damageDescription">
          <xsl:value-of select="DamageDescription"/>
        </xsl:attribute>
        <xsl:if test="Owner != ''">
        <!-- Owner data -->
          <xsl:call-template name="PropertyOwnerData">
            <xsl:with-param name="OwnerID" select="Owner"/>
          </xsl:call-template>
        </xsl:if>
        <!-- Involved data -->
        <xsl:call-template name="InvolvedData"/>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>
  <!-- Coverage data -->
  <xsl:template name="CoverageData">
    <xsl:for-each select="//Coverage">
      <xsl:element name="Coverage">
        <xsl:attribute name="code">
          <xsl:value-of select="@type"/>
        </xsl:attribute>
        <xsl:attribute name="deductibleAmt">
          <xsl:value-of select="DeductibleAmt"/>
        </xsl:attribute>
        <xsl:attribute name="limitAmt">
          <xsl:value-of select="LimitAmt"/>
        </xsl:attribute>
        <xsl:attribute name="dailyLimitAmt">
          <xsl:value-of select="number(DailyLimitAmt)"/>
        </xsl:attribute>
        <xsl:attribute name="maxDays">
          <xsl:value-of select="MaximumDays"/>
        </xsl:attribute>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>
  <!-- Property Owner data -->
  <xsl:template name="PropertyOwnerData">
    <xsl:param name="OwnerID"/>
    <!-- <xsl:variable name="OwnerID" select="@id"/> -->
    <xsl:for-each select="//Involved[@id = $OwnerID]">
      <xsl:variable name="InvolvedID" select="@id"/>
      <xsl:element name="Owner">
        <xsl:attribute name="id">
          <xsl:value-of select="$InvolvedID"/>
        </xsl:attribute>
        <xsl:attribute name="types">
          <xsl:for-each select="InvolvedTypes/InvolvedType">
            <xsl:value-of select="@type"/>
            <xsl:if test="position() != last()">
              <xsl:text>,</xsl:text>
            </xsl:if>
          </xsl:for-each>
        </xsl:attribute>
        <xsl:attribute name="NameFirst">
          <xsl:value-of select="FirstName"/>
        </xsl:attribute>
        <xsl:attribute name="NameLast">
          <xsl:value-of select="LastName"/>
        </xsl:attribute>
        <xsl:attribute name="Phone1AreaCode">
          <xsl:call-template name="phoneAreaCode">
            <xsl:with-param name="val" select="PhoneNumber1"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone1Exchange">
          <xsl:call-template name="phoneExchange">
            <xsl:with-param name="val" select="PhoneNumber1"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone1UnitNumber">
          <xsl:call-template name="phoneUnitNumber">
            <xsl:with-param name="val" select="PhoneNumber1"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone2AreaCode">
          <xsl:call-template name="phoneAreaCode">
            <xsl:with-param name="val" select="PhoneNumber2"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone2Exchange">
          <xsl:call-template name="phoneExchange">
            <xsl:with-param name="val" select="PhoneNumber2"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone2UnitNumber">
          <xsl:call-template name="phoneUnitNumber">
            <xsl:with-param name="val" select="PhoneNumber2"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone3AreaCode">
          <xsl:call-template name="phoneAreaCode">
            <xsl:with-param name="val" select="PhoneNumber3"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone3Exchange">
          <xsl:call-template name="phoneExchange">
            <xsl:with-param name="val" select="PhoneNumber3"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone3UnitNumber">
          <xsl:call-template name="phoneUnitNumber">
            <xsl:with-param name="val" select="PhoneNumber3"/>
          </xsl:call-template>
        </xsl:attribute>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>
  <!-- Involved data -->
  <xsl:template name="InvolvedData">
    <xsl:variable name="propertyID" select="@id"/>
    <xsl:for-each select="//Involved[InvolvedTypes/InvolvedType[@propertyid = $propertyID]]">
      <xsl:variable name="InvolvedID" select="@id"/>
      <xsl:element name="Involved">
        <xsl:attribute name="id">
          <xsl:value-of select="$InvolvedID"/>
        </xsl:attribute>
        <xsl:attribute name="types">
          <xsl:for-each select="InvolvedTypes/InvolvedType">
            <xsl:value-of select="@type"/>
            <xsl:if test="position() != last()">
              <xsl:text>,</xsl:text>
            </xsl:if>
          </xsl:for-each>
        </xsl:attribute>
        <xsl:attribute name="NameFirst">
          <xsl:value-of select="FirstName"/>
        </xsl:attribute>
        <xsl:attribute name="NameLast">
          <xsl:value-of select="LastName"/>
        </xsl:attribute>
        <xsl:attribute name="Phone1AreaCode">
          <xsl:call-template name="phoneAreaCode">
            <xsl:with-param name="val" select="PhoneNumber1"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone1Exchange">
          <xsl:call-template name="phoneExchange">
            <xsl:with-param name="val" select="PhoneNumber1"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone1UnitNumber">
          <xsl:call-template name="phoneUnitNumber">
            <xsl:with-param name="val" select="PhoneNumber1"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone2AreaCode">
          <xsl:call-template name="phoneAreaCode">
            <xsl:with-param name="val" select="PhoneNumber2"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone2Exchange">
          <xsl:call-template name="phoneExchange">
            <xsl:with-param name="val" select="PhoneNumber2"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone2UnitNumber">
          <xsl:call-template name="phoneUnitNumber">
            <xsl:with-param name="val" select="PhoneNumber2"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone3AreaCode">
          <xsl:call-template name="phoneAreaCode">
            <xsl:with-param name="val" select="PhoneNumber3"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone3Exchange">
          <xsl:call-template name="phoneExchange">
            <xsl:with-param name="val" select="PhoneNumber3"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="Phone3UnitNumber">
          <xsl:call-template name="phoneUnitNumber">
            <xsl:with-param name="val" select="PhoneNumber3"/>
          </xsl:call-template>
        </xsl:attribute>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>
  <!-- Utility templates -->
  <xsl:template name="phoneAreaCode">
    <xsl:param name="val"/>
    <xsl:variable name="cleanedVal" select="translate($val, '()-', '')"/>
    <xsl:value-of select="substring($cleanedVal, 1, 3)"/>
  </xsl:template>
  <xsl:template name="phoneExchange">
    <xsl:param name="val"/>
    <xsl:variable name="cleanedVal" select="translate($val, '()-', '')"/>
    <xsl:value-of select="substring($cleanedVal, 4, 3)"/>
  </xsl:template>
  <xsl:template name="phoneUnitNumber">
    <xsl:param name="val"/>
    <xsl:variable name="cleanedVal" select="translate($val, '()-', '')"/>
    <xsl:value-of select="substring($cleanedVal, 7)"/>
  </xsl:template>
</xsl:stylesheet>