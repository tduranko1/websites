Attribute VB_Name = "MRecordsetUtils"
'********************************************************************************
'* Module MRecordsetUtils
'*
'* Utility methods for dealing with recordsets.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "MDomUtils."

'Internal error codes for this module.
'Private Enum EventCodes
'    eNull = MRecordsetUtils_BAS_ErrorCodes
'End Enum

'********************************************************************************
'* Spits recordset contents out to the debug log.
'********************************************************************************
Public Sub DebugRS(ByRef RS As ADODB.Recordset)
          Dim str As String
          Dim strValue As String
          Dim fld As ADODB.Field
          
10        RS.MoveFirst
          
20        str = "Record Count = " & RS.RecordCount & vbCrLf
30        str = str & "  Fields = "
          
40        For Each fld In RS.Fields
50            str = str & fld.Name & ", "
60        Next
          
70        str = str & vbCrLf
          
80        While Not RS.EOF
90            str = str & "  Data: "
100           For Each fld In RS.Fields
110               If Not IsNull(fld.Value) Then
120                   strValue = LTrim(RTrim(Replace(fld.Value, vbCrLf, "")))
130                   If Len(strValue) > 0 Then
140                       If Left(strValue, 1) = "<" And Right(strValue, 1) = ">" Then
150                           str = str & "(XML NOT LOGGED), "
160                       Else
170                           str = str & "'" & fld.Value & "', "
180                       End If
190                   Else
200                       str = str & "'' , "
210                   End If
220               Else
230                   str = str & "NULL, "
240               End If
250           Next
260           str = str & vbCrLf
              
270           RS.MoveNext
280       Wend
          
290       g_objEvents.Trace str, MODULE_NAME & "Recordset Contents"

300       RS.MoveFirst
          
End Sub



