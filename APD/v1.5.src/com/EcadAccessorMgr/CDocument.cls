VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDocument"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Component LAPDEcadAccessor: Class CDocument
'*
'* LYNX APD clients (Insurance Companies) require the ability to view
'* information about their claims. This information will be accessible from
'* the LYNX Services web site. For the purpose of this document, this web
'* site will be known as Client Access.
'*
'* LYNX APD Client Access will be hosted and developed by E-Commerce
'* Application Development (ECAD). Initially the site will be read-only and
'* simply provide Insurance companies access to claim information.
'*
'* This component is used to return bulk documents and reports to ECAD.
'*
'********************************************************************************
Option Explicit

Private Const MODULE_NAME As String = APP_NAME & "CDocument."

'Internal error codes for this class.
Private Enum EventCodes
    eReturnedStreamIsBlank = CDocument_CLS_ErrorCodes
End Enum

'********************************************************************************
'* Backup global termination
'********************************************************************************
Private Sub Class_Terminate()
10        TerminateGlobals
End Sub

'********************************************************************************
'* Syntax:      arrByte = GetDocument("<Document ImageLocation="c:\temp.tif"/>")
'*
'* Parameters:  strParamXML - the only parameter, in XML format.
'*
'* Purpose:     Retrieves an APD document in binary form of a byte array.
'*              This is for all documents except CCC print images.
'*
'* Returns:     The byte array containing the binary data.
'*
'********************************************************************************
Public Function GetDocument(ByVal strParamXML As String) As Variant
          Const PROC_NAME As String = MODULE_NAME & "GetDocument: "
          
          'Object declarations
          Dim objParamDom As MSXML2.DOMDocument40
          Dim objReportUtils As Object
          Dim objImageUtils As Object
          Dim objStream As Object
          Dim objAdoStream As ADODB.Stream
          
          'Variable declarations
          Dim strFilePath As String
          Dim strDocumentRoot As String
          Dim varBinaryData As Variant
          
10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Check passed parameters.
30        g_objEvents.Assert Len(strParamXML) > 0, "Nothing passed for strParamXML."

          'Add a trace mesage to the debug log.
40        If g_blnDebugMode Then g_objEvents.Trace "Param XML = '" & strParamXML & "'", PROC_NAME & "Started"

          'Initialize DOM objects.
50        Set objParamDom = New MSXML2.DOMDocument40

          'Loads and validates the xml.
60        LoadXml objParamDom, strParamXML, PROC_NAME, "Passed Parameter"
          
          '******************************************************
          '* Entity Specific Code - select on the entity
          
70        Select Case objParamDom.documentElement.nodeName
          
              Case "Document"
          
                  'Get the file path from the parameter
80                strFilePath = GetChildNodeText(objParamDom.documentElement, "@ImageLocation", True)
                  
                  'Get the document root from the config file.
90                strDocumentRoot = GetConfig("Document/RootDirectory")
                  
                  'Combine and escape out the double slashes.
100               strFilePath = Replace(strDocumentRoot & strFilePath, "\\", "\")
                  
110               If g_blnDebugMode Then g_objEvents.Trace strFilePath, "Reading file."
                  
                  'Return the results.
120               GetDocument = ReadBinaryDataFromFile(strFilePath)
                  
130           Case "Report"
              
                  'Create the report utilities object.
140               Set objReportUtils = CreateObjectEx("ReportUtils.clsReport")
                  
150               If g_blnDebugMode Then g_objEvents.Trace "", "Calling GetReport()"
                  
                  'Call it to stream the report file.
160               Set objStream = objReportUtils.getReport( _
                      App.Path & GetConfig("ClaimPoint/ReportPath") & _
                      GetChildNodeText(objParamDom, "/Report/@sReportFileName", True), _
                      GetChildNodeText(objParamDom, "/Report/@sStoredProc", True), _
                      GetChildNodeText(objParamDom, "/Report/@sReportParams", True), _
                      GetChildNodeText(objParamDom, "/Report/@sReportStaticParams", True), _
                      GetChildNodeText(objParamDom, "/Report/@sReportFormat", False) _
                  )
                  
170               If objStream Is Nothing Then
180                   Err.Raise eReturnedStreamIsBlank, PROC_NAME, "ADO Stream returned from ReportUtils is blank."
190               End If
                  
200               If g_blnDebugMode Then g_objEvents.Trace "", "Received Stream"
                  
                  'Read all the data from the stream.
210               GetDocument = objStream.Read()
                  
220               objStream.Close
230               Set objStream = Nothing
              
                  'Clean up and go home.
240               Set objReportUtils = Nothing
          
250           Case "Thumbnail"
              
                  'Create the report utilities object.
260               Set objImageUtils = CreateObjectEx("ImageUtils.Thumbnail")
                  
                  'Get the file path from the parameter
270               strFilePath = GetChildNodeText(objParamDom.documentElement, "@ImageLocation", True)
                  
                  'Get the document root from the config file.
280               strDocumentRoot = GetConfig("Document/RootDirectory")
                  
                  'Combine and escape out the double slashes.
290               strFilePath = Replace(strDocumentRoot & strFilePath, "\\", "\")
                  
300               If g_blnDebugMode Then g_objEvents.Trace "", "Calling GetThumbnail()"
                  
                  'Get the thumbnail height and width from the param XML.
                  'If not passed, pull the default from the config file.
                  Dim strWidth As String, strHeight As String
                  
310               strHeight = GetChildNodeText(objParamDom.documentElement, "@Height", False)
320               strWidth = GetChildNodeText(objParamDom.documentElement, "@Width", False)
                  
330               If strHeight = "" Then strHeight = GetConfig("ClaimPoint/Thumbnail/@Height")
340               If strWidth = "" Then strWidth = GetConfig("ClaimPoint/Thumbnail/@Width")
                  
                  'Call ImateUtils component to generate the thumbnail image.
350               Set objAdoStream = objImageUtils.GetThumbnail(strFilePath, strHeight, strWidth)

360               If g_blnDebugMode Then g_objEvents.Trace "", "Received Stream"
              
                  'Read all the data from the stream.
370               GetDocument = objAdoStream.Read()
                  
380               objAdoStream.Close
              
                  'Clean up and go home.
390               Set objImageUtils = Nothing
              
400       End Select
          
          '* End Entity Specific Code
          '******************************************************
          
          'Add a trace mesage to the debug log.
410       If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Exiting"
          
          'Do not exit - fall into the error handler below for one stop object cleanup code.

ErrorHandler:

          'Object clean up.
420       Set objParamDom = Nothing
430       Set objReportUtils = Nothing
440       Set objImageUtils = Nothing
450       Set objStream = Nothing
460       Set objAdoStream = Nothing

          'Everything below will be sent out when error occur...
470       If Err.Number <> 0 Then
480           g_objEvents.Env "File Path = " & strFilePath
490           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "Param XML = '" & strParamXML & "'"
500       End If

510       TerminateGlobals
          
End Function

'********************************************************************************
'* Syntax:      strXML = GetDocumentXML("<Document ImageLocation="c:\temp.tif"/>")
'*
'* Parameters:  strParamXML - the only parameter, in XML format.
'*
'* Purpose:     Retrieves an XML APD document as a string.
'*              Currently this document is CCC print images only, but this may
'*              change in the future.
'*
'* Returns:     The XML.
'*
'********************************************************************************
Public Function GetDocumentXML(ByVal strParamXML As String) As String
          Const PROC_NAME As String = MODULE_NAME & "GetDocumentXML: "
          
          'Object declarations
          Dim objParamDom As MSXML2.DOMDocument40
          Dim objResultDom As MSXML2.DOMDocument40

          'Variable declarations
          Dim strFilePath As String
          Dim strDocumentRoot As String

10        On Error GoTo ErrorHandler

20        InitializeGlobals
          
          'Check passed parameters.
30        g_objEvents.Assert Len(strParamXML) > 0, "Nothing passed for strParamXML."

          'Add a trace mesage to the debug log.
40        If g_blnDebugMode Then g_objEvents.Trace "Param XML = '" & strParamXML & "'", PROC_NAME & "Started"

          'Initialize DOM objects.
50        Set objParamDom = New MSXML2.DOMDocument40
60        Set objResultDom = New MSXML2.DOMDocument40

          'Loads and validates the xml.
70        LoadXml objParamDom, strParamXML, PROC_NAME, "Passed Parameter"
          
          'Get the file path from the parameter
80        strFilePath = GetChildNode(objParamDom.documentElement, "@ImageLocation", True).Text
          
          'Get the document root from the config file.
90        strDocumentRoot = GetConfig("Document/RootDirectory")
          
          'Combine and escape out the double slashes.
100       strFilePath = Replace(strDocumentRoot & strFilePath, "\\", "\")
          
          'Load up the xml file from disk.
110       LoadXmlFile objResultDom, strFilePath, PROC_NAME, "Document"
          
          'Now return the escaped XML.
120       GetDocumentXML = objResultDom.xml
          
          'Add a trace mesage to the debug log.
130       If g_blnDebugMode Then g_objEvents.Trace "", PROC_NAME & "Exiting"
          
          'Do not exit - fall into the error handler below for one stop object cleanup code.

ErrorHandler:

          'Object clean up.
140       Set objParamDom = Nothing
150       Set objResultDom = Nothing

          'Everything below will be sent out when error occur...
160       If Err.Number <> 0 Then
170           g_objEvents.Env "File Path = " & strFilePath
180           g_objEvents.HandleEvent Err.Number, "[Line " & Erl & "] " & PROC_NAME & Err.Source, Err.Description, _
                  "Param XML = '" & strParamXML & "'"
190       End If

200       TerminateGlobals
          
End Function

