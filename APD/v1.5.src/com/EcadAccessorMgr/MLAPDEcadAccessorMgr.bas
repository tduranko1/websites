Attribute VB_Name = "MLAPDEcadAccessorMgr"
'********************************************************************************
'*
'* Component LAPDEcadAccessorMgr : Module MLAPDEcadAccessorMgr
'*
'* Global Consts and Utility Methods
'*
'********************************************************************************
Option Explicit

Public Const APP_NAME As String = "LAPDEcadAccessorMgr."

Public Const LAPDEcadAccessorMgr_FirstError As Long = &H80066000

'Error codes specific to LAPDEcadAccessor
Public Enum LAPDEcadAccessorMgr_ErrorCodes

    eSQLServerErrorMin = &H80050000
    eSQLServerErrorMax = &H8005FFFF
    eSQLAssignmentIDNotFound = &H80050072

    'General error codes applicable to all modules of this component
    eCreateObjectExError = LAPDEcadAccessorMgr_FirstError

    'Module specific error ranges.
    CWebAssignment_CLS_ErrorCodes = LAPDEcadAccessorMgr_FirstError + &H80
    CData_CLS_ErrorCodes = LAPDEcadAccessorMgr_FirstError + &H100
    CDocument_CLS_ErrorCodes = LAPDEcadAccessorMgr_FirstError + &H180
    
End Enum

Public Enum EDatabase
    eFNOL_Recordset
    ePartner_Recordset
    eAPD_Recordset
    eAPD_XML
End Enum


Private Declare Function CoCreateGuid Lib "ole32.dll" (pGUID As GUID) As Long
Private Declare Function StringFromGUID2 Lib "ole32.dll" (pGUID As GUID, ByVal pBuffer As String, ByVal cbSize As Long) As Long

'GUID Creation Type, Constants, and APIs
Private Type GUID
    Data1 As Long
    Data2 As Integer
    Data3 As Integer
    Data4(0 To 7) As Byte
End Type

Const S_OK = &H0

'********************************************************************************
'* Global events and data access objects.
'********************************************************************************
Public g_objEvents As SiteUtilities.CEvents
Public g_objDataAccessor As DataAccessor.CDataAccessor

Public g_strSupportDocPath As String
Public g_blnDebugMode As Boolean

'This counter used to ensure the globals get intialized
'and desposed of only once.  This is required because various
'parts of the ECAD stuff have different entry points.
Private mintInitCount As Integer

'********************************************************************
'* Util methods to prevent modification of
'* any error information stored in the Err object.
'********************************************************************
Type typError
    Number As Long
    Description As String
    Source As String
End Type

Public Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

Public Sub PushErr(ByRef Error As typError)
10        Error.Number = Err.Number
20        Error.Description = Err.Description
30        Error.Source = Err.Source
End Sub

Public Sub PopErr(ByRef Error As typError)
10        Err.Number = Error.Number
20        Err.Description = Error.Description
30        Err.Source = Error.Source
End Sub

'********************************************************************************
'* Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
'* Parameters:  strObjectName = The object to call CreateObject for.
'* Purpose:     Wraps CreateObject() with better debug information.
'* Returns:     The Object created or Nothing.
'********************************************************************************
Public Function CreateObjectEx(strObjectName) As Object
          Dim obj As Object
          
10        On Error GoTo ErrorHandler
          
20        Set CreateObjectEx = Nothing
          
30        Set obj = CreateObject(strObjectName)
          
40        If obj Is Nothing Then
50            Err.Raise eCreateObjectExError, "", "CreateObject('" & strObjectName & "') returned Nothing."
60        End If
          
70        Set CreateObjectEx = obj
          
ErrorHandler:

80        Set obj = Nothing
          
90        If Err.Number <> 0 Then
100           Err.Raise Err.Number, "CreateObjectEx('" & strObjectName & "') " & Err.Source, Err.Description & " " & strObjectName
110       End If
          
End Function

'********************************************************************************
'* Initializes global objects and variables
'********************************************************************************
Public Sub InitializeGlobals()

10        If mintInitCount = 0 Then
          
              'Create DataAccessor.
20            Set g_objDataAccessor = New DataAccessor.CDataAccessor
              
              'Share DataAccessor's events object.
30            Set g_objEvents = g_objDataAccessor.mEvents
          
              'Get path to support document directory
40            g_strSupportDocPath = App.Path & "\" & Left(APP_NAME, Len(APP_NAME) - 1)
          
              'This will give partner data its own log file.
50            g_objEvents.ComponentInstance = "Client Access"
          
              'Initialize our member components first, so we have logging set up.
60            g_objDataAccessor.InitEvents App.Path & "\..\config\config.xml"
              
              'Get config debug mode status.
70            g_blnDebugMode = g_objEvents.IsDebugMode
              
80        End If
          
90        mintInitCount = mintInitCount + 1


End Sub

'********************************************************************************
'* Terminates global objects and variables
'********************************************************************************
Public Sub TerminateGlobals()

10        If mintInitCount <= 1 Then

20            Set g_objEvents = Nothing
30            Set g_objDataAccessor = Nothing
40            mintInitCount = 0
              
50        Else
60            mintInitCount = mintInitCount - 1
70        End If
          
End Sub

'********************************************************************************
'* Returns the requested configuration setting.
'********************************************************************************
Public Function GetConfig(ByVal strSetting As String) As String
10        GetConfig = g_objEvents.mSettings.GetParsedSetting(strSetting)
End Function

'********************************************************************************
'* Points the global data accessor object at the proper database.
'********************************************************************************
Public Sub ConnectToDB(ByVal eDB As EDatabase)
          Dim strConnect As String
          Dim strConfig As String
          
          'Get configuration settings for FNOL database connect string.
10        Select Case eDB
              Case eFNOL_Recordset
20                strConfig = "ClaimPoint/WebAssignment/ConnectionStringStd"
30            Case ePartner_Recordset
40                strConfig = "PartnerSettings/ConnectionStringStd"
50            Case eAPD_Recordset
60                strConfig = "ConnectionStringStd"
70            Case eAPD_XML
80                strConfig = "ConnectionStringXml"
90        End Select
          
100       strConnect = GetConfig(strConfig)
110       g_objEvents.Assert Len(strConnect) > 0, "Blank connection string returned from config file: " & strConfig

          'Set the connect string.
120       g_objDataAccessor.SetConnectString strConnect
End Sub


'********************************************************************************
'* Pads a number out with zeros to make the requested number of digits.
'********************************************************************************
Public Function ZeroPad(ByVal lngValue As Long, ByVal intDigits As Integer) As String
          Dim strValue As String
          
10        strValue = CStr(lngValue)
20        While Len(strValue) < intDigits
30            strValue = "0" & strValue
40        Wend

50        ZeroPad = strValue
End Function

'********************************************************************************
'* Generates GUIDs
'********************************************************************************
Public Function GenerateGUID() As Variant
          Dim tGuid As GUID
          Dim sGuid As String

          'create the 128 bit integer
10        If CoCreateGuid(tGuid) = S_OK Then
              'create a buffer of suitable size
20            sGuid = String$(80, Chr$(0))
              'convert the 128 bit integer into a Unicode string
30            If StringFromGUID2(tGuid, sGuid, 80) > 0 Then
40                GenerateGUID = StrConv(sGuid, vbFromUnicode)
50            End If
60        End If
End Function 'GenerateGUID


