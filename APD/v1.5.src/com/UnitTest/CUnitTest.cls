VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnitTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Class CUnitTest
'*
'* This purpose of this class is to establish a standard unit test interface.
'*
'* A client test application will will instantiate and call this interface in
'* turn for all components that implement it.  Results for each component will
'* be displayed to the tester.
'*
'* To implement for your component, include a copy of this file into your project.
'* Then begin adding your specific tests at the bottom of the file, and call
'* them from RunTest.
'********************************************************************************
Option Explicit

Private Const APP_NAME As String = "UnitTest"
Private Const MODULE_NAME As String = APP_NAME & ".CUnitTest."

'The number of tests called from this CUnitTest implementation.
Private Const mcintNumberOfTests = 0

'********************************************************************************
'* Returns a count of the number of tests that can be run for this application.
'********************************************************************************
Public Function NumTests() As Integer
    NumTests = mcintNumberOfTests
End Function

'********************************************************************************
'* Returns the indexed test description.
'********************************************************************************
Public Function TestDesc(ByVal intIndex As Integer) As String
    Const PROC_NAME As String = MODULE_NAME & "TestDesc: "
    
    Select Case intIndex
        'Case 1:
        '   TestDescription = APP_NAME & "SomeModule"
        Case Else:
            Err.Raise vbObjectError + 1, PROC_NAME, "Passed test index out of range!"
    End Select
    
End Function

'********************************************************************************
'* Runs the indexed test.
'* Returns the results as a giant formatted string.
'* Returns an error count through intErrors.
'********************************************************************************
Public Function RunTest(ByVal intIndex As Integer, ByRef intErrors As Integer) As String
    Const PROC_NAME As String = MODULE_NAME & "RunTest: "

    'Run the test.
    Select Case intIndex
        'Case 1:
        '   RunTest = SomeModule.RunTest( m_intErrorCount( intIndex ), m_strResults( intIndex ) )
        Case Else:
            Err.Raise vbObjectError + 1, PROC_NAME, "Passed test index out of range!"
    End Select
    
End Function

'********************************************************************************
'* ADD PRIVATE TEST METHODS BELOW THIS LINE.
'********************************************************************************



