VERSION 5.00
Begin VB.Form TestForm 
   Caption         =   "APD Unit Test Suite"
   ClientHeight    =   7215
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8655
   LinkTopic       =   "Form1"
   ScaleHeight     =   7215
   ScaleWidth      =   8655
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox ErrorCount 
      Height          =   375
      Left            =   7560
      TabIndex        =   10
      Text            =   "0"
      Top             =   1320
      Width           =   975
   End
   Begin VB.TextBox Results 
      Height          =   5295
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   8
      Text            =   "Form.frx":0000
      Top             =   1800
      Width           =   8415
   End
   Begin VB.CommandButton RunOneTest 
      Caption         =   "Run One Test"
      Height          =   375
      Left            =   4800
      TabIndex        =   7
      Top             =   1080
      Width           =   1695
   End
   Begin VB.CommandButton RunAllTests 
      Caption         =   "Run All Tests"
      Height          =   375
      Left            =   4800
      TabIndex        =   5
      Top             =   600
      Width           =   1695
   End
   Begin VB.CommandButton TestAllComponents 
      Caption         =   "Test All Components"
      Height          =   375
      Left            =   1200
      TabIndex        =   4
      Top             =   120
      Width           =   1695
   End
   Begin VB.ComboBox TestList 
      Height          =   315
      Left            =   1200
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   1080
      Width           =   3375
   End
   Begin VB.ComboBox ComponentList 
      Height          =   315
      ItemData        =   "Form.frx":0020
      Left            =   1200
      List            =   "Form.frx":004E
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   600
      Width           =   3375
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Caption         =   "Error Count"
      Height          =   255
      Left            =   7560
      TabIndex        =   11
      Top             =   960
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "Results"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   1440
      Width           =   975
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Global"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Test"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   1080
      Width           =   975
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Component"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   975
   End
End
Attribute VB_Name = "TestForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim mintNumTests As Integer
Dim mobjUnitTest As Object

Private Sub Form_Load()
    ComponentList.ListIndex = 0
    ComponentList_Click
End Sub

Private Sub Form_Unload(Cancel As Integer)
    KillTestObject
End Sub

Private Sub GetTestObject(ByVal strComponent As String)
    On Error Resume Next
    
    Set mobjUnitTest = Nothing
    Set mobjUnitTest = CreateObject(strComponent & ".CUnitTest")
    
    If mobjUnitTest Is Nothing Then
        Results.Text = Results.Text & vbCrLf & "FAILURE: Could not create " & strComponent & ".CUnitTest !" & vbCrLf
        ErrorCount.Text = CInt(ErrorCount.Text) + 1
    End If
    
    mintNumTests = mobjUnitTest.NumTests()
    
End Sub

Private Sub KillTestObject()
    Set mobjUnitTest = Nothing
End Sub

Private Sub ComponentList_Click()
    Dim intIdx As Integer
    
    GetTestObject (ComponentList.Text)
    TestList.Clear
    
    For intIdx = 1 To mintNumTests
        TestList.AddItem mobjUnitTest.TestDesc(intIdx)
    Next

    TestList.ListIndex = 0
End Sub

Private Sub RunOneTest_Click()
    On Error Resume Next
    
    Dim intErrors As Integer
    
    Results.Text = ""
    
    GetTestObject (ComponentList.Text)
    
    Results.Text = mobjUnitTest.RunTest(TestList.ListIndex + 1, intErrors)
    
    If Err.Number <> 0 Then
        MsgBox Err.Description
        Err.Clear
    End If
    
    ErrorCount.Text = intErrors
    
    KillTestObject
    
    If Err.Number <> 0 Then
        MsgBox Err.Description
        Err.Clear
    End If

End Sub

Private Sub RunAllTests_Click()
    On Error Resume Next
    
    Dim intErrors As Integer
    Dim intList As Integer
    
    Results.Text = ""
    ErrorCount.Text = "0"
    
    GetTestObject (ComponentList.Text)
    
    For intList = 1 To TestList.ListCount
        Results.Text = Results.Text & mobjUnitTest.RunTest(intList, intErrors)
        ErrorCount.Text = CInt(ErrorCount.Text) + intErrors
    Next
    
    KillTestObject
    
    If Err.Number <> 0 Then
        MsgBox Err.Description
        Err.Clear
    End If

End Sub

Private Sub TestAllComponents_Click()
    On Error Resume Next
    
    Dim intErrors As Integer
    Dim intList As Integer
    Dim intComp As Integer
    
    Results.Text = ""
    ErrorCount.Text = "0"
    
    For intComp = 0 To ComponentList.ListCount - 1
    
        GetTestObject (ComponentList.List(intComp))
        
        For intList = 1 To mintNumTests
            Results.Text = Results.Text & mobjUnitTest.RunTest(intList, intErrors)
            ErrorCount.Text = CInt(ErrorCount.Text) + intErrors
        Next
        
        KillTestObject
        
        Results.Text = Results.Text & vbCrLf & "==============================================" & vbCrLf

    Next
    
    If Err.Number <> 0 Then
        MsgBox Err.Description
        Err.Clear
    End If

End Sub
